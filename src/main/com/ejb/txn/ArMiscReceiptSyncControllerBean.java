/*
 * ArMiscReceiptsSyncControllerBean
 *
 * Created on November 9, 2005, 1:01 PM
 *
 * @author  Dann Ryan M. Hilario
 */

package com.ejb.txn;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.*;
import com.ejb.ar.*;
import com.ejb.gl.*;
import com.ejb.inv.*;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.exception.AdPRFCoaGlCustomerDepositAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ArINVOverapplicationNotAllowedException;
import com.ejb.exception.ArRCTInvoiceHasNoWTaxCodeException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;

import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.ArModAppliedInvoiceDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.ArModInvoicePaymentScheduleDetails;
import com.util.ArModReceiptDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModTagListDetails;

/**
 * @ejb:bean name="ArMiscReceiptSyncControllerBean" type="Stateless"
 *           view-type="service-endpoint"
 *
 * @wsee:port-component name="ArMiscReceiptSync"
 *
 * @jboss:port-component uri="omega-ejb/ArMiscReceiptSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.ArMiscReceiptSyncWS"
 *
 */

public class ArMiscReceiptSyncControllerBean implements SessionBean {

	private SessionContext ctx;
	private String DATE_FORMAT_OUTPUT = "MM/dd/yyyy";

	ArReceiptEntryControllerHome homeRCT = null;
	ArReceiptEntryController ejbRCT = null;

	InvRepItemCostingControllerHome homeRIC = null;
	InvRepItemCostingController ejbRIC = null;

	/**
	 * @ejb:interface-method
	 **/
	public int setArReceiptAllNewAndVoid(String[] newRR, String[] voidRR, String BR_BRNCH_CODE, Integer AD_CMPNY,
			String CASHIER) {

		Debug.print("ArMiscReceiptSyncControllerBean setInvMiscReceiptAllNewAndVoid");

		try {

			homeRCT = (ArReceiptEntryControllerHome) com.util.EJBHomeFactory
					.lookUpHome("ejb/ArReceiptEntryControllerEJB", ArReceiptEntryControllerHome.class);

		} catch (Exception e) {

		}

		try {

			ejbRCT = homeRCT.create();

		} catch (Exception e) {
		}

		int success = 0;

		// ejbRCT.saveArRctEntry(details, BA_NM, FC_NM, CST_CSTMR_CODE, RB_NM, aiList,
		// isDraft, AD_BRNCH, AD_CMPNY)

		return success;

	}

	/**
	 * @ejb:interface-method
	 **/
	public int setArMiscReceiptAllNewAndVoid(String[] newRR, String[] voidRR, String BR_BRNCH_CODE, Integer AD_CMPNY,
			String CASHIER) {

		Debug.print("ArMiscReceiptSyncControllerBean setInvMiscReceiptAllNewAndVoid");

		LocalAdBranchHome adBranchHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalArReceiptBatchHome arReceiptBatchHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoiceBatchHome arInvoiceBatchHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalAdBranchCustomerHome adBranchCustomerHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;

		try {

			adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
					LocalAdBranchHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			arReceiptHome = (LocalArReceiptHome) EJBHomeFactory.lookUpLocalHome(LocalArReceiptHome.JNDI_NAME,
					LocalArReceiptHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
					LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
							LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory.lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME,
					LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory
					.lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			arCustomerHome = (LocalArCustomerHome) EJBHomeFactory.lookUpLocalHome(LocalArCustomerHome.JNDI_NAME,
					LocalArCustomerHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			arReceiptBatchHome = (LocalArReceiptBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory.lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME,
					LocalArInvoiceHome.class);
			arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome) EJBHomeFactory
					.lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			invLocationHome = (LocalInvLocationHome) EJBHomeFactory.lookUpLocalHome(LocalInvLocationHome.JNDI_NAME,
					LocalInvLocationHome.class);
			adBranchCustomerHome = (LocalAdBranchCustomerHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
					LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			Integer AD_BRNCH = adBranch.getBrCode();

			int success = 0;

			if (adPreference.getPrfMiscPosDiscountAccount() != null
					&& adPreference.getPrfMiscPosGiftCertificateAccount() != null
					&& adPreference.getPrfMiscPosServiceChargeAccount() != null
					&& adPreference.getPrfMiscPosDineInChargeAccount() != null) {

				// new receipts

				HashMap receiptNumbers = new HashMap();
				HashMap invoiceNumbers = new HashMap();
				String fundTransferNumber = null;
				String firstNumber = "";
				String lastNumber = "";

				for (int i = 0; i < newRR.length; i++) {

					ArModReceiptDetails arModReceiptDetails = receiptDecode(newRR[i]);

					if (i == 0)
						firstNumber = arModReceiptDetails.getRctNumber();
					if (i == newRR.length - 1)
						lastNumber = arModReceiptDetails.getRctNumber();

					if (arModReceiptDetails.getRctPosOnAccount() == 0) {

						// generate receipt number

						String generatedReceipt = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						LocalArReceipt arExistingReceipt = null;

						Iterator rctIter = receiptNumbers.values().iterator();
						while (rctIter.hasNext()) {

							try {

								ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) rctIter.next();

								arExistingReceipt = arReceiptHome.findByRctDateAndRctNumberAndCstCustomerCodeAndBrCode(
										arModReceiptDetails.getRctDate(), arModUploadReceiptDetails.getRctNumber(),
										arModReceiptDetails.getRctCstCustomerCode(), AD_BRNCH, AD_CMPNY);

								break;

							} catch (FinderException ex) {

								continue;

							}

						}

						if (arExistingReceipt == null) {

							ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
							arModUploadReceiptDetails.setRctNumber(generatedReceipt);
							arModUploadReceiptDetails
									.setRctPosDiscount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							receiptNumbers.put(generatedReceipt, arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);

							LocalArReceipt arReceipt = arReceiptHome.create("MISC", "POS Sales " + new Date(),
									arModReceiptDetails.getRctDate(), generatedReceipt, null, null, null, null, null,
									null, null, null, totalAmount, 0d, 0d, 0d, 0d, 0d, 0d, null, 1, null, "CASH",
									EJBCommon.FALSE, 0, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE,
									EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
									EJBCommon.FALSE, null, null, null, null, null, CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
									EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null, null,
									EJBCommon.FALSE, EJBCommon.FALSE, null, AD_BRNCH, AD_CMPNY);

							LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
									.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
							// glFunctionalCurrency.addArReceipt(arReceipt);
							arReceipt.setGlFunctionalCurrency(glFunctionalCurrency);

							System.out.println("arModReceiptDetails.getRctCstCustomerCode(): "
									+ arModReceiptDetails.getRctCstCustomerCode());
							LocalArCustomer arCustomer = arCustomerHome
									.findByCstCustomerCode(arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
							// arCustomer.addArReceipt(arReceipt);
							arReceipt.setArCustomer(arCustomer);

							String bankAccount = "";
							if (BR_BRNCH_CODE.equals("Manila-Smoking Lounge")) {
								bankAccount = "Allied Bank-IPT Smoking";
							} else if (BR_BRNCH_CODE.equals("Manila-Cigar Shop")) {
								bankAccount = "Allied Bank Cigar shop";
							} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Domestic")) {
								bankAccount = "Terminal II Domestic";
							} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Intl")) {
								bankAccount = "Term2 International";
							} else if (BR_BRNCH_CODE.equals("Cebu-Banilad")) {
								bankAccount = "Metrobank Banilad";
							} else if (BR_BRNCH_CODE.equals("Cebu-Gorordo")) {
								bankAccount = "Metrobank-Gorordo";
							} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Domestic")) {
								bankAccount = "Metrobank Mactan Domestic";
							} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Intl")) {
								bankAccount = "Metrobank I Mactan Int'l";
							} else if (BR_BRNCH_CODE.equals("Cebu-Supercat")) {
								bankAccount = "Metrobank Supercat";
							} else {
								bankAccount = arCustomer.getAdBankAccount().getBaName();
							}

							LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(bankAccount, AD_CMPNY);
							// adBankAccount.addArReceipt(arReceipt);
							arReceipt.setAdBankAccount(adBankAccount);

							LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
							// arTaxCode.addArReceipt(arReceipt);
							arReceipt.setArTaxCode(arTaxCode);

							LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome
									.findByWtcName("NONE", AD_CMPNY);
							// arWithholdingTaxCode.addArReceipt(arReceipt);
							arReceipt.setArWithholdingTaxCode(arWithholdingTaxCode);

							LocalArReceiptBatch arReceiptBatch = null;
							try {
								arReceiptBatch = arReceiptBatchHome.findByRbName("POS BATCH", AD_BRNCH, AD_CMPNY);
							} catch (FinderException ex) {
								arReceiptBatch = arReceiptBatchHome.create("POS BATCH", "POS BATCH", "OPEN", "MISC",
										EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);

							}
							// arReceiptBatch.addArReceipt(arReceipt);
							arReceipt.setArReceiptBatch(arReceiptBatch);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();
							short lineNumber = 0;
							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(++lineNumber,
										arModInvoiceLineItemDetails.getIliQuantity(),
										arModInvoiceLineItemDetails.getIliUnitPrice(),
										EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
												/ (1 + arReceipt.getArTaxCode().getTcRate() / 100), (short) 2),
										EJBCommon.roundIt(
												arModInvoiceLineItemDetails.getIliAmount()
														- (arModInvoiceLineItemDetails.getIliAmount()
																/ (1 + arReceipt.getArTaxCode().getTcRate() / 100)),
												(short) 2),
										EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

								// arReceipt.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setArReceipt(arReceipt);

								LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
										.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								System.out.println("Item Default Location! " + invItem.getIiDefaultLocation());
								System.out.println("2 Item Name! " + invItem.getIiName());
								System.out.println("3 Item Name! " + arModInvoiceLineItemDetails.getIliIiName());
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}

								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);
								// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvItemLocation(invItemLocation);

								if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

									arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

								}
							}

						} else {

							ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) receiptNumbers
									.get(arExistingReceipt.getRctNumber());

							arModUploadReceiptDetails
									.setRctPosDiscount(
											arModUploadReceiptDetails.getRctPosDiscount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDiscount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(
											arModUploadReceiptDetails.getRctPosScAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosScAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(
											arModUploadReceiptDetails.getRctPosDcAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDcAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							receiptNumbers.put(arExistingReceipt.getRctNumber(), arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);
							arExistingReceipt.setRctAmount(arExistingReceipt.getRctAmount() + totalAmount);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();

							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								System.out.println(arModInvoiceLineItemDetails.getIliIiName());
								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);

								LocalArInvoiceLineItem arExistingInvoiceLineItem = null;

								try {
									arExistingInvoiceLineItem = arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(
											arExistingReceipt.getRctCode(), invItemLocation.getIlCode(),
											arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								} catch (FinderException ex) {

								}

								if (arExistingInvoiceLineItem == null) {
									System.out.println(arModInvoiceLineItemDetails.getIliIiName());
									LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
											(short) (arExistingReceipt.getArInvoiceLineItems().size() + 1),
											arModInvoiceLineItemDetails.getIliQuantity(),
											arModInvoiceLineItemDetails.getIliUnitPrice(),
											EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100),
													(short) 2),
											EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100)),
													(short) 2),
											EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

									// arExistingReceipt.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setArReceipt(arExistingReceipt);

									LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
											.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
									// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

									// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvItemLocation(invItemLocation);
									if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

										arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

									}
								} else {

									arExistingInvoiceLineItem.setIliQuantity(arExistingInvoiceLineItem.getIliQuantity()
											+ arModInvoiceLineItemDetails.getIliQuantity());
									arExistingInvoiceLineItem
											.setIliUnitPrice((arExistingInvoiceLineItem.getIliUnitPrice()
													+ arModInvoiceLineItemDetails.getIliUnitPrice()) / 2);
									arExistingInvoiceLineItem
											.setIliAmount(arExistingInvoiceLineItem.getIliAmount() + EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100),
													(short) 2));
									arExistingInvoiceLineItem.setIliTaxAmount(arExistingInvoiceLineItem
											.getIliTaxAmount()
											+ EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100)),
													(short) 2));

								}
							}
						}

					} else {

						// generate Invoice number

						String generatedInvoice = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR INVOICE",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						LocalArInvoice arExistingInvoice = null;

						Iterator invIter = invoiceNumbers.values().iterator();
						while (invIter.hasNext()) {

							try {

								ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invIter.next();

								arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndCstCustomerCode(
										arModUploadReceiptDetails.getRctNumber(), (byte) 0,
										arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);

								break;

							} catch (FinderException ex) {

								continue;

							}

						}

						if (arExistingInvoice == null) {

							ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
							arModUploadReceiptDetails.setRctNumber(generatedInvoice);
							arModUploadReceiptDetails
									.setRctPosDiscount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							invoiceNumbers.put(generatedInvoice, arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);

							LocalArInvoice arInvoice = arInvoiceHome.create("ITEMS", (byte) 0,
									"POS Sales " + new Date().toString(), arModReceiptDetails.getRctDate(),
									generatedInvoice, null, null, null, null, totalAmount, 0d, 0d, 0d, 0d, 0d, null, 1,
									null, 0d, 0d, null, null, null, null, null, null, null, null, null, null, null,
									null, null, null, null, null, null, null, null, null, EJBCommon.FALSE, null,
									EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
									null, 0d, null, null, null, null, CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
									EJBCommon.FALSE, null, null, null, (byte) 0, (byte) 0, null,
									arModReceiptDetails.getRctDate(), AD_BRNCH, AD_CMPNY);

							/*
							 * RctPaymentMethod is Used Temporarily for Payment Term Field
							 */
							if (adCompany.getCmpShortName().toLowerCase().equals("tinderbox")
									|| adCompany.getCmpShortName().toLowerCase().equals("hs")) {
								LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName("IMMEDIATE",
										AD_CMPNY);
								// adPaymentTerm.addArInvoice(arInvoice);
								arInvoice.setAdPaymentTerm(adPaymentTerm);
							} else {
								if (arModReceiptDetails.getRctPaymentMethod() == null
										|| arModReceiptDetails.getRctPaymentMethod().equals("")) {
									LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName("IMMEDIATE",
											AD_CMPNY);
									// adPaymentTerm.addArInvoice(arInvoice);
									arInvoice.setAdPaymentTerm(adPaymentTerm);
								} else {
									LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome
											.findByPytName(arModReceiptDetails.getRctPaymentMethod(), AD_CMPNY);
									// adPaymentTerm.addArInvoice(arInvoice);
									arInvoice.setAdPaymentTerm(adPaymentTerm);
								}
							}

							LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
									.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
							// glFunctionalCurrency.addArInvoice(arInvoice);
							arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

							System.out.println("POS Customer: " + arModReceiptDetails.getRctCstCustomerCode());
							LocalArCustomer arCustomer = arCustomerHome
									.findByCstCustomerCode(arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
							// arCustomer.addArInvoice(arInvoice);
							arInvoice.setArCustomer(arCustomer);

							if (adCompany.getCmpShortName().toLowerCase().equals("tinderbox")
									|| adCompany.getCmpShortName().toLowerCase().equals("hs")) {
								LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
								// arTaxCode.addArInvoice(arInvoice);
								arInvoice.setArTaxCode(arTaxCode);
							} else {
								try {
									LocalArTaxCode arTaxCode = arTaxCodeHome
											.findByTcName(arModReceiptDetails.getRctTcName(), AD_CMPNY);
									// arTaxCode.addArInvoice(arInvoice);
									arInvoice.setArTaxCode(arTaxCode);
								} catch (Exception ex) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
									// arTaxCode.addArInvoice(arInvoice);
									arInvoice.setArTaxCode(arTaxCode);
								}
							}

							LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome
									.findByWtcName("NONE", AD_CMPNY);
							// arWithholdingTaxCode.addArInvoice(arInvoice);
							arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);

							LocalArInvoiceBatch arInvoiceBatch = null;
							try {
								arInvoiceBatch = arInvoiceBatchHome.findByIbName("POS BATCH", AD_BRNCH, AD_CMPNY);
							} catch (FinderException ex) {
								arInvoiceBatch = arInvoiceBatchHome.create("POS BATCH", "POS BATCH", "OPEN", "MISC",
										EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);

							}
							// arInvoiceBatch.addArInvoice(arInvoice);
							arInvoice.setArInvoiceBatch(arInvoiceBatch);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();
							short lineNumber = 0;
							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(++lineNumber,
										arModInvoiceLineItemDetails.getIliQuantity(),
										arModInvoiceLineItemDetails.getIliUnitPrice(),
										EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
												/ (1 + arInvoice.getArTaxCode().getTcRate() / 100), (short) 2),
										EJBCommon.roundIt(
												arModInvoiceLineItemDetails.getIliAmount()
														- (arModInvoiceLineItemDetails.getIliAmount()
																/ (1 + arInvoice.getArTaxCode().getTcRate() / 100)),
												(short) 2),
										EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

								// arInvoice.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setArInvoice(arInvoice);

								LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
										.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);
								// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvItemLocation(invItemLocation);
								if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

									arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

								}
							}

						} else {

							ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invoiceNumbers
									.get(arExistingInvoice.getInvNumber());

							arModUploadReceiptDetails
									.setRctPosDiscount(
											arModUploadReceiptDetails.getRctPosDiscount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDiscount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(
											arModUploadReceiptDetails.getRctPosScAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosScAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(
											arModUploadReceiptDetails.getRctPosDcAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDcAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							invoiceNumbers.put(arExistingInvoice.getInvNumber(), arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);
							arExistingInvoice.setInvAmountDue(arExistingInvoice.getInvAmountDue() + totalAmount);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();

							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();

								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);

								LocalArInvoiceLineItem arExistingInvoiceLineItem = null;

								try {
									arExistingInvoiceLineItem = arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(
											arExistingInvoice.getInvCode(), invItemLocation.getIlCode(),
											arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								} catch (FinderException ex) {

								}

								if (arExistingInvoiceLineItem == null) {

									LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
											(short) (arExistingInvoice.getArInvoiceLineItems().size() + 1),
											arModInvoiceLineItemDetails.getIliQuantity(),
											arModInvoiceLineItemDetails.getIliUnitPrice(),
											EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100),
													(short) 2),
											EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100)),
													(short) 2),
											EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

									// arExistingInvoice.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setArInvoice(arExistingInvoice);

									LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
											.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
									// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

									// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvItemLocation(invItemLocation);
									if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

										arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

									}
								} else {

									arExistingInvoiceLineItem.setIliQuantity(arExistingInvoiceLineItem.getIliQuantity()
											+ arModInvoiceLineItemDetails.getIliQuantity());
									arExistingInvoiceLineItem
											.setIliUnitPrice((arExistingInvoiceLineItem.getIliUnitPrice()
													+ arModInvoiceLineItemDetails.getIliUnitPrice()) / 2);
									arExistingInvoiceLineItem
											.setIliAmount(arExistingInvoiceLineItem.getIliAmount() + EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100),
													(short) 2));
									arExistingInvoiceLineItem.setIliTaxAmount(arExistingInvoiceLineItem
											.getIliTaxAmount()
											+ EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100)),
													(short) 2));

								}
							}
						}

					}

				}

				// for each receipt generate distribution records

				Iterator rctIter = receiptNumbers.values().iterator();

				while (rctIter.hasNext()) {

					ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) rctIter.next();

					LocalArReceipt arReceipt = arReceiptHome
							.findByRctNumberAndBrCode(arModUploadReceiptDetails.getRctNumber(), AD_BRNCH, AD_CMPNY);
					arReceipt.setRctReferenceNumber(firstNumber + "-" + lastNumber);

					Iterator lineIter = arReceipt.getArInvoiceLineItems().iterator();

					double TOTAL_TAX = 0;
					double TOTAL_LINE = 0;

					while (lineIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) lineIter.next();

						TOTAL_LINE += arInvoiceLineItem.getIliTaxAmount() + arInvoiceLineItem.getIliAmount();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
											invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalAdBranchItemLocation adBranchItemLocation = null;

						try {

							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (arInvoiceLineItem.getIliEnableAutoBuild() == 0
								|| arInvoiceLineItem.getIliEnableAutoBuild() == 1
								|| arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

							if (adBranchItemLocation != null) {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							} else {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
										arReceipt, AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							}

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(
									invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
									arReceipt, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arReceipt,
									AD_BRNCH, AD_CMPNY);

						}

						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

					}

					// add tax distribution if necessary

					if (!arReceipt.getArTaxCode().getTcType().equals("NONE")
							&& !arReceipt.getArTaxCode().getTcType().equals("EXEMPT")) {

						if (adCompany.getCmpShortName().toLowerCase().equals("tinderbox")) {
							String brTaxCOA = this.getBranchTaxCOA(BR_BRNCH_CODE,
									arReceipt.getArTaxCode().getGlChartOfAccount().getCoaSegment1(),
									arReceipt.getArTaxCode().getGlChartOfAccount().getCoaSegment3(),
									arReceipt.getArTaxCode().getGlChartOfAccount().getCoaSegment4(),
									arReceipt.getArTaxCode().getGlChartOfAccount().getCoaSegment5());

							this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
									glChartOfAccountHome.findByCoaAccountNumber(brTaxCOA, AD_CMPNY).getCoaCode(),
									EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);
						} else {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
									arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE,
									arReceipt, AD_BRNCH, AD_CMPNY);

						}

					}

					// add cash distribution

					this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE, arReceipt.getRctAmount(),
							arReceipt.getAdBankAccount().getBaCoaGlCashAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
							AD_CMPNY);

					if (arModUploadReceiptDetails.getRctPosDiscount() != 0) {
						// add discount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
								arModUploadReceiptDetails.getRctPosDiscount(),
								adPreference.getPrfMiscPosDiscountAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosScAmount() != 0) {
						// add sc amount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "SERVCE CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosScAmount(),
								adPreference.getPrfMiscPosServiceChargeAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDcAmount() != 0) {
						// add dc amount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "DINEIN CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosDcAmount(),
								adPreference.getPrfMiscPosDineInChargeAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					// add forex gain/loss

					double forexGainLoss = TOTAL_LINE
							- (arReceipt.getRctAmount() + arModUploadReceiptDetails.getRctPosDiscount()
									- arModUploadReceiptDetails.getRctPosScAmount()
									- arModUploadReceiptDetails.getRctPosDcAmount());
					if (forexGainLoss != 0) {
						this.addArDrEntry(arReceipt.getArDrNextLine(), "FOREX",
								forexGainLoss > 0 ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(forexGainLoss),
								adPreference.getPrfMiscPosGiftCertificateAccount(), EJBCommon.FALSE, arReceipt,
								AD_BRNCH, AD_CMPNY);
					}

				}

				Iterator invIter = invoiceNumbers.values().iterator();

				while (invIter.hasNext()) {

					ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invIter.next();

					LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
							arModUploadReceiptDetails.getRctNumber(), (byte) 0, AD_BRNCH, AD_CMPNY);
					arInvoice.setInvReferenceNumber(firstNumber + "-" + lastNumber);

					short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
					double TOTAL_PAYMENT_SCHEDULE = 0d;

					GregorianCalendar gcPrevDateDue = new GregorianCalendar();
					GregorianCalendar gcDateDue = new GregorianCalendar();
					gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

					Collection adPaymentSchedules = arInvoice.getAdPaymentTerm().getAdPaymentSchedules();

					Iterator i = adPaymentSchedules.iterator();

					while (i.hasNext()) {

						LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule) i.next();

						// get date due

						if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")) {

							gcDateDue.setTime(arInvoice.getInvEffectivityDate());
							gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

						} else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")) {

							gcDateDue = gcPrevDateDue;
							gcDateDue.add(Calendar.MONTH, 1);
							gcPrevDateDue = gcDateDue;

						} else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

							gcDateDue = gcPrevDateDue;

							if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
								if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31
										&& gcPrevDateDue.get(Calendar.DATE) > 15
										&& gcPrevDateDue.get(Calendar.DATE) < 31) {
									gcDateDue.add(Calendar.DATE, 16);
								} else {
									gcDateDue.add(Calendar.DATE, 15);
								}
							} else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
								if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
										&& gcPrevDateDue.get(Calendar.DATE) == 14) {
									gcDateDue.add(Calendar.DATE, 14);
								} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
										&& gcPrevDateDue.get(Calendar.DATE) >= 15
										&& gcPrevDateDue.get(Calendar.DATE) < 28) {
									gcDateDue.add(Calendar.DATE, 13);
								} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29
										&& gcPrevDateDue.get(Calendar.DATE) >= 15
										&& gcPrevDateDue.get(Calendar.DATE) < 29) {
									gcDateDue.add(Calendar.DATE, 14);
								} else {
									gcDateDue.add(Calendar.DATE, 15);
								}
							}

							gcPrevDateDue = gcDateDue;

						}

						// create a payment schedule

						double PAYMENT_SCHEDULE_AMOUNT = 0;

						// if last payment schedule subtract to avoid rounding difference error

						if (i.hasNext()) {

							PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount()
									/ arInvoice.getAdPaymentTerm().getPytBaseAmount()) * arInvoice.getInvAmountDue(),
									precisionUnit);

						} else {

							PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_PAYMENT_SCHEDULE;

						}

						LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = arInvoicePaymentScheduleHome.create(
								gcDateDue.getTime(), adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT, 0d,
								EJBCommon.FALSE, (short) 0, gcDateDue.getTime(), 0d, 0d, AD_CMPNY);

						// arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);
						arInvoicePaymentSchedule.setArInvoice(arInvoice);

						TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;

					}

					Iterator lineIter = arInvoice.getArInvoiceLineItems().iterator();

					double TOTAL_TAX = 0;
					double TOTAL_LINE = 0;

					while (lineIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) lineIter.next();

						TOTAL_LINE += arInvoiceLineItem.getIliTaxAmount() + arInvoiceLineItem.getIliAmount();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
											invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalAdBranchItemLocation adBranchItemLocation = null;

						try {

							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (arInvoiceLineItem.getIliEnableAutoBuild() == 0
								|| arInvoiceLineItem.getIliEnableAutoBuild() == 1
								|| arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

							if (adBranchItemLocation != null) {

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

							} else {

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
										arInvoice, AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

							}

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(
									invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
									arInvoice, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice,
									AD_BRNCH, AD_CMPNY);

						}

						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

					}

					// add tax distribution if necessary

					if (!arInvoice.getArTaxCode().getTcType().equals("NONE")
							&& !arInvoice.getArTaxCode().getTcType().equals("EXEMPT")) {

						if (adCompany.getCmpShortName().toLowerCase().equals("tinderbox")) {
							String brTaxCOA = this.getBranchTaxCOA(BR_BRNCH_CODE,
									arInvoice.getArTaxCode().getGlChartOfAccount().getCoaSegment1(),
									arInvoice.getArTaxCode().getGlChartOfAccount().getCoaSegment3(),
									arInvoice.getArTaxCode().getGlChartOfAccount().getCoaSegment4(),
									arInvoice.getArTaxCode().getGlChartOfAccount().getCoaSegment5());

							this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
									glChartOfAccountHome.findByCoaAccountNumber(brTaxCOA, AD_CMPNY).getCoaCode(),
									EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
						} else {

							this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
									arInvoice.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE,
									arInvoice, AD_BRNCH, AD_CMPNY);

						}

					}

					// add cash distribution

					LocalAdBranchCustomer adBranchCustomer = null;

					try {

						adBranchCustomer = adBranchCustomerHome
								.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if (adBranchCustomer != null) {

						this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
								arInvoice.getInvAmountDue(), adBranchCustomer.getBcstGlCoaReceivableAccount(),
								EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);

					} else {
						this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
								arInvoice.getInvAmountDue(), arInvoice.getArCustomer().getCstGlCoaReceivableAccount(),
								EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDiscount() != 0) {
						// add discount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
								arModUploadReceiptDetails.getRctPosDiscount(),
								adPreference.getPrfMiscPosDiscountAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosScAmount() != 0) {
						// add sc amount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "SERVCE CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosScAmount(),
								adPreference.getPrfMiscPosServiceChargeAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDcAmount() != 0) {
						// add dc amount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "DINEIN CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosDcAmount(),
								adPreference.getPrfMiscPosDineInChargeAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					// add forex gain/loss

					double forexGainLoss = TOTAL_LINE
							- (arInvoice.getInvAmountDue() + arModUploadReceiptDetails.getRctPosDiscount()
									- arModUploadReceiptDetails.getRctPosScAmount()
									- arModUploadReceiptDetails.getRctPosDcAmount());
					if (forexGainLoss != 0) {
						this.addArDrEntry(arInvoice.getArDrNextLine(), "FOREX",
								forexGainLoss > 0 ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(forexGainLoss),
								adPreference.getPrfMiscPosGiftCertificateAccount(), EJBCommon.FALSE, arInvoice,
								AD_BRNCH, AD_CMPNY);
					}

				}

				success = 1;
				return success;
			} else {

				System.out.println("NULL ACCOUNTs");
				System.out.println(success);
				return success;

			}

		} catch (Exception ex) {

			ex.printStackTrace();
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method
	 **/
	public int setArMiscReceiptAllNewAndVoidUS(String[] newRR, String[] voidRR, String BR_BRNCH_CODE, Integer AD_CMPNY,
			String CASHIER) {

		Debug.print("ArMiscReceiptSyncControllerBean setInvMiscReceiptAllNewAndVoid");

		LocalAdBranchHome adBranchHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalArReceiptBatchHome arReceiptBatchHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoiceBatchHome arInvoiceBatchHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalAdBranchCustomerHome adBranchCustomerHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;

		try {

			adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
					LocalAdBranchHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			arReceiptHome = (LocalArReceiptHome) EJBHomeFactory.lookUpLocalHome(LocalArReceiptHome.JNDI_NAME,
					LocalArReceiptHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
					LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
							LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory.lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME,
					LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory
					.lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			arCustomerHome = (LocalArCustomerHome) EJBHomeFactory.lookUpLocalHome(LocalArCustomerHome.JNDI_NAME,
					LocalArCustomerHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			arReceiptBatchHome = (LocalArReceiptBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory.lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME,
					LocalArInvoiceHome.class);
			arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome) EJBHomeFactory
					.lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			invLocationHome = (LocalInvLocationHome) EJBHomeFactory.lookUpLocalHome(LocalInvLocationHome.JNDI_NAME,
					LocalInvLocationHome.class);
			adBranchCustomerHome = (LocalAdBranchCustomerHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
					LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			Integer AD_BRNCH = adBranch.getBrCode();

			int success = 0;

			if (adPreference.getPrfMiscPosDiscountAccount() != null
					&& adPreference.getPrfMiscPosGiftCertificateAccount() != null
					&& adPreference.getPrfMiscPosServiceChargeAccount() != null
					&& adPreference.getPrfMiscPosDineInChargeAccount() != null) {

				// new receipts

				HashMap receiptNumbers = new HashMap();
				HashMap invoiceNumbers = new HashMap();
				String fundTransferNumber = null;
				String firstNumber = "";
				String lastNumber = "";

				double salesTax = 0;
				double preparedTax = 0;
				double salesTax2 = 0;
				double preparedTax2 = 0;

				double salesTaxInvoice2 = 0;
				double preparedTaxInvoice2 = 0;

				for (int i = 0; i < newRR.length; i++) {

					ArModReceiptDetails arModReceiptDetails = receiptDecodeUS(newRR[i]);

					if (i == 0)
						firstNumber = arModReceiptDetails.getRctNumber();
					if (i == newRR.length - 1)
						lastNumber = arModReceiptDetails.getRctNumber();

					if (arModReceiptDetails.getRctPosOnAccount() == 0) {

						// generate receipt number

						String generatedReceipt = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						LocalArReceipt arExistingReceipt = null;

						Iterator rctIter = receiptNumbers.values().iterator();
						while (rctIter.hasNext()) {

							try {

								ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) rctIter.next();

								arExistingReceipt = arReceiptHome.findByRctDateAndRctNumberAndCstCustomerCodeAndBrCode(
										arModReceiptDetails.getRctDate(), arModUploadReceiptDetails.getRctNumber(),
										arModReceiptDetails.getRctCstCustomerCode(), AD_BRNCH, AD_CMPNY);

								break;

							} catch (FinderException ex) {

								continue;

							}

						}

						if (arExistingReceipt == null) {

							ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
							arModUploadReceiptDetails.setRctNumber(generatedReceipt);
							arModUploadReceiptDetails
									.setRctPosDiscount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							receiptNumbers.put(generatedReceipt, arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);

							LocalArReceipt arReceipt = arReceiptHome.create("MISC", "POS Sales " + new Date(),
									arModReceiptDetails.getRctDate(), generatedReceipt, null, null, null, null, null,
									null, null, null, totalAmount, 0d, 0d, 0d, 0d, 0d, 0d, null, 1, null, "CASH",
									EJBCommon.FALSE, 0, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE,
									EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
									EJBCommon.FALSE, null, null, null, null, null, CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
									EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null, null,
									EJBCommon.FALSE, EJBCommon.FALSE, null, AD_BRNCH, AD_CMPNY);

							LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
									.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
							// glFunctionalCurrency.addArReceipt(arReceipt);
							arReceipt.setGlFunctionalCurrency(glFunctionalCurrency);

							System.out.println("arModReceiptDetails.getRctCstCustomerCode(): "
									+ arModReceiptDetails.getRctCstCustomerCode());
							LocalArCustomer arCustomer = arCustomerHome
									.findByCstCustomerCode(arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
							// arCustomer.addArReceipt(arReceipt);
							arReceipt.setArCustomer(arCustomer);

							String bankAccount = "";
							if (BR_BRNCH_CODE.equals("Manila-Smoking Lounge")) {
								bankAccount = "Allied Bank-IPT Smoking";
							} else if (BR_BRNCH_CODE.equals("Manila-Cigar Shop")) {
								bankAccount = "Allied Bank Cigar shop";
							} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Domestic")) {
								bankAccount = "Terminal II Domestic";
							} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Intl")) {
								bankAccount = "Term2 International";
							} else if (BR_BRNCH_CODE.equals("Cebu-Banilad")) {
								bankAccount = "Metrobank Banilad";
							} else if (BR_BRNCH_CODE.equals("Cebu-Gorordo")) {
								bankAccount = "Metrobank-Gorordo";
							} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Domestic")) {
								bankAccount = "Metrobank Mactan Domestic";
							} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Intl")) {
								bankAccount = "Metrobank I Mactan Int'l";
							} else if (BR_BRNCH_CODE.equals("Cebu-Supercat")) {
								bankAccount = "Metrobank Supercat";
							} else {
								bankAccount = arCustomer.getAdBankAccount().getBaName();
							}

							LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(bankAccount, AD_CMPNY);
							// adBankAccount.addArReceipt(arReceipt);
							arReceipt.setAdBankAccount(adBankAccount);

							boolean menuItem = true;
							System.out.println(
									"arModReceiptDetails.getRctSource(): " + arModReceiptDetails.getRctSource());
							if (arModReceiptDetails.getRctSource().trim().equalsIgnoreCase("R")) {
								System.out.println("****");
								LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE 7%", AD_CMPNY);
								// arTaxCode.addArReceipt(arReceipt);
								arReceipt.setArTaxCode(arTaxCode);
							} else {
								System.out.println("-------");
								LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
								// arTaxCode.addArReceipt(arReceipt);
								arReceipt.setArTaxCode(arTaxCode);
							}

							LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome
									.findByWtcName("NONE", AD_CMPNY);
							// arWithholdingTaxCode.addArReceipt(arReceipt);
							arReceipt.setArWithholdingTaxCode(arWithholdingTaxCode);

							LocalArReceiptBatch arReceiptBatch = null;
							try {
								arReceiptBatch = arReceiptBatchHome.findByRbName("POS BATCH", AD_BRNCH, AD_CMPNY);
							} catch (FinderException ex) {
								arReceiptBatch = arReceiptBatchHome.create("POS BATCH", "POS BATCH", "OPEN", "MISC",
										EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);

							}
							// arReceiptBatch.addArReceipt(arReceipt);
							arReceipt.setArReceiptBatch(arReceiptBatch);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();
							short lineNumber = 0;

							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								double taxRate = 0;
								System.out.println("arModInvoiceLineItemDetails.getIliTaxCode(): "
										+ arModInvoiceLineItemDetails.getIliTaxCode());
								if (arModInvoiceLineItemDetails.getIliTaxCode().contains("Prepared Food Tax")) {
									taxRate = 7;
									System.out.println("taxRate: " + taxRate);
									System.out.println("Formula: " + (1 + taxRate / 100));
									System.out.println("arModInvoiceLineItemDetails.getIliAmount(): "
											+ arModInvoiceLineItemDetails.getIliAmount());
									preparedTax = preparedTax
											+ (arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100))
											- EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount(), (short) 2);
									System.out.println("preparedTax: " + preparedTax);
								} else if (arModInvoiceLineItemDetails.getIliTaxCode().contains("Sales Tax")) {
									taxRate = 5;
									salesTax = salesTax
											+ (arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100))
											- EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount(), (short) 2);
									System.out.println("salesTax: " + salesTax);
								} else {
									taxRate = arExistingReceipt.getArTaxCode().getTcRate();
								}

								System.out.println("taxRate: " + taxRate);
								LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(++lineNumber,
										arModInvoiceLineItemDetails.getIliQuantity(),
										arModInvoiceLineItemDetails.getIliUnitPrice(),
										EJBCommon.roundIt(
												arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100),
												(short) 2),
										(arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100)) - EJBCommon
												.roundIt(arModInvoiceLineItemDetails.getIliAmount(), (short) 2),
										EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);
								System.out.println("tax: " + EJBCommon.roundIt(
										arModInvoiceLineItemDetails.getIliAmount()
												- (arModInvoiceLineItemDetails.getIliAmount() / (1 + taxRate / 100)),
										(short) 2));
								// arReceipt.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setArReceipt(arReceipt);

								LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
										.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);
								if (invUnitOfMeasure.getUomName().contains("MENU")) {
									menuItem = true;
								}
								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								System.out.println("Item Default Location! " + invItem.getIiDefaultLocation());
								System.out.println("2 Item Name! " + invItem.getIiName());
								System.out.println("3 Item Name! " + arModInvoiceLineItemDetails.getIliIiName());
								System.out.println("3 Item Name! " + arModInvoiceLineItemDetails.getIliLocName());
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}

								// System.out.println("arModInvoiceLineItemDetails.getIliLocName(): " +
								// arModInvoiceLineItemDetails.getIliLocName());
								// System.out.println("invLocation.getLocName(): " + invLocation.getLocName());
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);
								// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvItemLocation(invItemLocation);

								if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

									arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

								}
							}

						} else {

							ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) receiptNumbers
									.get(arExistingReceipt.getRctNumber());

							arModUploadReceiptDetails
									.setRctPosDiscount(
											arModUploadReceiptDetails.getRctPosDiscount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDiscount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(
											arModUploadReceiptDetails.getRctPosScAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosScAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(
											arModUploadReceiptDetails.getRctPosDcAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDcAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							receiptNumbers.put(arExistingReceipt.getRctNumber(), arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);
							arExistingReceipt.setRctAmount(arExistingReceipt.getRctAmount() + totalAmount);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();

							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								System.out.println(arModInvoiceLineItemDetails.getIliIiName());
								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);

								LocalArInvoiceLineItem arExistingInvoiceLineItem = null;

								try {
									arExistingInvoiceLineItem = arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(
											arExistingReceipt.getRctCode(), invItemLocation.getIlCode(),
											arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								} catch (FinderException ex) {

								}

								if (arExistingInvoiceLineItem == null) {
									System.out.println(arModInvoiceLineItemDetails.getIliIiName());
									double taxRate = 0;
									if (arModInvoiceLineItemDetails.getIliTaxCode().contains("Prepared Food Tax")) {
										taxRate = 7;
										preparedTax = preparedTax
												+ (arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100))
												- EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount(),
														(short) 2);
									} else if (arModInvoiceLineItemDetails.getIliTaxCode().contains("Sales Tax")) {
										taxRate = 5;
										salesTax = salesTax
												+ (arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100))
												- EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount(),
														(short) 2);
										System.out.println("salesTax: " + salesTax);
									} else {
										taxRate = arExistingReceipt.getArTaxCode().getTcRate();
									}
									LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
											(short) (arExistingReceipt.getArInvoiceLineItems().size() + 1),
											arModInvoiceLineItemDetails.getIliQuantity(),
											arModInvoiceLineItemDetails.getIliUnitPrice(),
											EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100),
													(short) 2),
											(arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100))
													- EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount(),
															(short) 2),
											EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

									// arExistingReceipt.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setArReceipt(arExistingReceipt);

									LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
											.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
									// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

									// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvItemLocation(invItemLocation);
									if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

										arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

									}
								} else {

									arExistingInvoiceLineItem.setIliQuantity(arExistingInvoiceLineItem.getIliQuantity()
											+ arModInvoiceLineItemDetails.getIliQuantity());
									arExistingInvoiceLineItem
											.setIliUnitPrice((arExistingInvoiceLineItem.getIliUnitPrice()
													+ arModInvoiceLineItemDetails.getIliUnitPrice()) / 2);
									arExistingInvoiceLineItem
											.setIliAmount(arExistingInvoiceLineItem.getIliAmount() + EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100),
													(short) 2));
									arExistingInvoiceLineItem.setIliTaxAmount(arExistingInvoiceLineItem
											.getIliTaxAmount()
											+ EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100)),
													(short) 2));

								}

							}
						}

					} else {

						// generate Invoice number

						String generatedInvoice = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR INVOICE",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						LocalArInvoice arExistingInvoice = null;

						Iterator invIter = invoiceNumbers.values().iterator();
						while (invIter.hasNext()) {

							try {

								ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invIter.next();

								arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndCstCustomerCode(
										arModUploadReceiptDetails.getRctNumber(), (byte) 0,
										arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);

								break;

							} catch (FinderException ex) {

								continue;

							}

						}

						if (arExistingInvoice == null) {

							ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
							arModUploadReceiptDetails.setRctNumber(generatedInvoice);
							arModUploadReceiptDetails
									.setRctPosDiscount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							invoiceNumbers.put(generatedInvoice, arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);

							LocalArInvoice arInvoice = arInvoiceHome.create("ITEMS", (byte) 0,
									"POS Sales " + new Date().toString(), arModReceiptDetails.getRctDate(),
									generatedInvoice, null, null, null, null, totalAmount, 0d, 0d, 0d, 0d, 0d, null, 1,
									null, 0d, 0d, null, null, null, null, null, null, null, null, null, null, null,
									null, null, null, null, null, null, null, null, null, EJBCommon.FALSE, null,
									EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
									null, 0d, null, null, null, null, CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
									EJBCommon.FALSE, null, null, null, (byte) 0, (byte) 0, null,
									arModReceiptDetails.getRctDate(), AD_BRNCH, AD_CMPNY);

							/*
							 * RctPaymentMethod is Used Temporarily for Payment Term Field
							 */
							if (adCompany.getCmpShortName().toLowerCase().equals("tinderbox")
									|| adCompany.getCmpShortName().toLowerCase().equals("hs")) {
								LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName("IMMEDIATE",
										AD_CMPNY);
								// adPaymentTerm.addArInvoice(arInvoice);
								arInvoice.setAdPaymentTerm(adPaymentTerm);
							} else {
								if (arModReceiptDetails.getRctPaymentMethod() == null
										|| arModReceiptDetails.getRctPaymentMethod().equals("")) {
									LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName("IMMEDIATE",
											AD_CMPNY);
									// adPaymentTerm.addArInvoice(arInvoice);
									arInvoice.setAdPaymentTerm(adPaymentTerm);
								} else {
									LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome
											.findByPytName(arModReceiptDetails.getRctPaymentMethod(), AD_CMPNY);
									// adPaymentTerm.addArInvoice(arInvoice);
									arInvoice.setAdPaymentTerm(adPaymentTerm);
								}
							}

							LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
									.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
							// glFunctionalCurrency.addArInvoice(arInvoice);
							arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

							System.out.println("POS Customer: " + arModReceiptDetails.getRctCstCustomerCode());
							LocalArCustomer arCustomer = arCustomerHome
									.findByCstCustomerCode(arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
							// arCustomer.addArInvoice(arInvoice);
							arInvoice.setArCustomer(arCustomer);

							if (arModReceiptDetails.getRctSource().trim().equalsIgnoreCase("R")) {
								if (adCompany.getCmpShortName().toLowerCase().equals("tinderbox")
										|| adCompany.getCmpShortName().toLowerCase().equals("hs")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE 7%", AD_CMPNY);
									// arTaxCode.addArInvoice(arInvoice);
									arInvoice.setArTaxCode(arTaxCode);
								} else {
									try {
										LocalArTaxCode arTaxCode = arTaxCodeHome
												.findByTcName(arModReceiptDetails.getRctTcName(), AD_CMPNY);
										// arTaxCode.addArInvoice(arInvoice);
										arInvoice.setArTaxCode(arTaxCode);
									} catch (Exception ex) {
										LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE 7%",
												AD_CMPNY);
										// arTaxCode.addArInvoice(arInvoice);
										arInvoice.setArTaxCode(arTaxCode);
									}
								}
							} else {
								if (adCompany.getCmpShortName().toLowerCase().equals("tinderbox")
										|| adCompany.getCmpShortName().toLowerCase().equals("hs")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
									// arTaxCode.addArInvoice(arInvoice);
									arInvoice.setArTaxCode(arTaxCode);
								} else {
									try {
										LocalArTaxCode arTaxCode = arTaxCodeHome
												.findByTcName(arModReceiptDetails.getRctTcName(), AD_CMPNY);
										// arTaxCode.addArInvoice(arInvoice);
										arInvoice.setArTaxCode(arTaxCode);
									} catch (Exception ex) {
										LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE",
												AD_CMPNY);
										// arTaxCode.addArInvoice(arInvoice);
										arInvoice.setArTaxCode(arTaxCode);
									}
								}
							}

							LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome
									.findByWtcName("NONE", AD_CMPNY);
							// arWithholdingTaxCode.addArInvoice(arInvoice);
							arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);

							LocalArInvoiceBatch arInvoiceBatch = null;
							try {
								arInvoiceBatch = arInvoiceBatchHome.findByIbName("POS BATCH", AD_BRNCH, AD_CMPNY);
							} catch (FinderException ex) {
								arInvoiceBatch = arInvoiceBatchHome.create("POS BATCH", "POS BATCH", "OPEN", "MISC",
										EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);

							}
							// arInvoiceBatch.addArInvoice(arInvoice);
							arInvoice.setArInvoiceBatch(arInvoiceBatch);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();
							short lineNumber = 0;
							boolean menuItem = false;
							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();

								double taxRate = 0;
								if (arModInvoiceLineItemDetails.getIliTaxCode().contains("Prepared Food Tax")) {
									taxRate = 7;
									preparedTax = preparedTax
											+ (arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100))
											- EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount(), (short) 2);
								} else if (arModInvoiceLineItemDetails.getIliTaxCode().contains("Sales Tax")) {
									taxRate = 5;
									salesTax = salesTax
											+ (arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100))
											- EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount(), (short) 2);
									System.out.println("salesTax: " + salesTax);
								} else {
									taxRate = arExistingInvoice.getArTaxCode().getTcRate();
								}

								LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(++lineNumber,
										arModInvoiceLineItemDetails.getIliQuantity(),
										arModInvoiceLineItemDetails.getIliUnitPrice(),
										EJBCommon.roundIt(
												arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100),
												(short) 2),
										(arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100)) - EJBCommon
												.roundIt(arModInvoiceLineItemDetails.getIliAmount(), (short) 2),
										EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

								// arInvoice.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setArInvoice(arInvoice);

								LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
										.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

								if (invUnitOfMeasure.getUomName().contains("MENU")) {
									menuItem = true;
								}

								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);
								// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvItemLocation(invItemLocation);
								if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

									arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

								}
							}

						} else {

							ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invoiceNumbers
									.get(arExistingInvoice.getInvNumber());

							arModUploadReceiptDetails
									.setRctPosDiscount(
											arModUploadReceiptDetails.getRctPosDiscount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDiscount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(
											arModUploadReceiptDetails.getRctPosScAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosScAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(
											arModUploadReceiptDetails.getRctPosDcAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDcAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							invoiceNumbers.put(arExistingInvoice.getInvNumber(), arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);
							arExistingInvoice.setInvAmountDue(arExistingInvoice.getInvAmountDue() + totalAmount);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();

							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();

								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);

								LocalArInvoiceLineItem arExistingInvoiceLineItem = null;

								try {
									arExistingInvoiceLineItem = arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(
											arExistingInvoice.getInvCode(), invItemLocation.getIlCode(),
											arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								} catch (FinderException ex) {

								}

								if (arExistingInvoiceLineItem == null) {

									double taxRate = 0;
									if (arModInvoiceLineItemDetails.getIliTaxCode().contains("Prepared Food Tax")) {
										taxRate = 7;
										preparedTax = preparedTax
												+ (arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100))
												- EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount(),
														(short) 2);
									} else if (arModInvoiceLineItemDetails.getIliTaxCode().contains("Sales Tax")) {
										taxRate = 5;
										salesTax = salesTax
												+ (arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100))
												- EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount(),
														(short) 2);
										System.out.println("salesTax: " + salesTax);
									} else {
										taxRate = arExistingInvoice.getArTaxCode().getTcRate();
									}

									LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
											(short) (arExistingInvoice.getArInvoiceLineItems().size() + 1),
											arModInvoiceLineItemDetails.getIliQuantity(),
											arModInvoiceLineItemDetails.getIliUnitPrice(),
											EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100),
													(short) 2),
											(arModInvoiceLineItemDetails.getIliAmount() * (1 + taxRate / 100))
													- EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount(),
															(short) 2),
											EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

									// arExistingInvoice.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setArInvoice(arExistingInvoice);

									LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
											.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
									// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

									// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvItemLocation(invItemLocation);
									if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

										arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

									}
								} else {

									arExistingInvoiceLineItem.setIliQuantity(arExistingInvoiceLineItem.getIliQuantity()
											+ arModInvoiceLineItemDetails.getIliQuantity());
									arExistingInvoiceLineItem
											.setIliUnitPrice((arExistingInvoiceLineItem.getIliUnitPrice()
													+ arModInvoiceLineItemDetails.getIliUnitPrice()) / 2);
									arExistingInvoiceLineItem
											.setIliAmount(arExistingInvoiceLineItem.getIliAmount() + EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100),
													(short) 2));
									arExistingInvoiceLineItem.setIliTaxAmount(arExistingInvoiceLineItem
											.getIliTaxAmount()
											+ EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100)),
													(short) 2));

								}
							}
						}

					}

				}

				// for each receipt generate distribution records

				Iterator rctIter = receiptNumbers.values().iterator();

				while (rctIter.hasNext()) {

					ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) rctIter.next();

					LocalArReceipt arReceipt = arReceiptHome
							.findByRctNumberAndBrCode(arModUploadReceiptDetails.getRctNumber(), AD_BRNCH, AD_CMPNY);
					arReceipt.setRctReferenceNumber(firstNumber + "-" + lastNumber);

					Iterator lineIter = arReceipt.getArInvoiceLineItems().iterator();

					double TOTAL_TAX = 0;
					double TOTAL_LINE = 0;

					while (lineIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) lineIter.next();

						TOTAL_LINE += arInvoiceLineItem.getIliTaxAmount() + arInvoiceLineItem.getIliAmount();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
											invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalAdBranchItemLocation adBranchItemLocation = null;

						try {

							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (arInvoiceLineItem.getIliEnableAutoBuild() == 0
								|| arInvoiceLineItem.getIliEnableAutoBuild() == 1
								|| arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

							if (adBranchItemLocation != null) {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							} else {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
										arReceipt, AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							}

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(
									invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
									arReceipt, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arReceipt,
									AD_BRNCH, AD_CMPNY);

						}

						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

						if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTaxCode()
								.contains("Prepared Food Tax")) {
							preparedTax2 = preparedTax2 + arInvoiceLineItem.getIliTaxAmount();
						}

						if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTaxCode().contains("Sales Tax")) {
							salesTax2 = salesTax2 + arInvoiceLineItem.getIliTaxAmount();
						}

					}

					// add tax distribution if necessary
					System.out.println("preparedTax2: " + preparedTax2);
					System.out.println("preparedTax: " + preparedTax);
					System.out.println("salesTax2: " + salesTax2);
					if (!arReceipt.getArTaxCode().getTcType().equals("NONE")
							&& !arReceipt.getArTaxCode().getTcType().equals("EXEMPT")) {

						if (adCompany.getCmpShortName().toLowerCase().equals("tinderbox")) {
							String brTaxCOA = this.getBranchTaxCOA(BR_BRNCH_CODE,
									arReceipt.getArTaxCode().getGlChartOfAccount().getCoaSegment1(),
									arReceipt.getArTaxCode().getGlChartOfAccount().getCoaSegment3(),
									arReceipt.getArTaxCode().getGlChartOfAccount().getCoaSegment4(),
									arReceipt.getArTaxCode().getGlChartOfAccount().getCoaSegment5());
							/***** Dogdog *****/
							System.out.println("ADD TAX: " + preparedTax);

							System.out.println("Prep and Sales Tax");

							LocalArTaxCode arTaxCodePreparedFoodTax = null;
							if (preparedTax == 0 && salesTax == 0) {
								System.out.println("Normal");
								this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
										arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE,
										arReceipt, AD_BRNCH, AD_CMPNY);
							} else {
								if (preparedTax != 0) {
									try {

										arTaxCodePreparedFoodTax = arTaxCodeHome.findByTcName("Prepared Food Tax",
												AD_CMPNY);

									} catch (FinderException ex) {

									}

									if (arTaxCodePreparedFoodTax != null) {
										this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE,
												preparedTax2,
												arTaxCodePreparedFoodTax.getGlChartOfAccount().getCoaCode(),
												EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);
									}

								}
								System.out.println("ADD TAX salesTax: " + salesTax);
								System.out.println("ADD TAX salesTax2: " + salesTax2);
								LocalArTaxCode arTaxCodeSalesTax = null;
								if (salesTax2 != 0) {
									try {

										arTaxCodeSalesTax = arTaxCodeHome.findByTcName("Sales Tax", AD_CMPNY);
										System.out.println("Find Sales Tax");

									} catch (FinderException ex) {

									}

									if (arTaxCodeSalesTax != null) {
										System.out.println("Create Sales Tax");
										this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE,
												salesTax2, arTaxCodeSalesTax.getGlChartOfAccount().getCoaCode(),
												EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);
									}

								}
							}

							/*
							 * else{ System.out.println("Normal");
							 * this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE,
							 * TOTAL_TAX, arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(),
							 * EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY); }
							 */

						} else {
							if (preparedTax == 0 && salesTax == 0) {
								System.out.println("Normal 2");
								this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
										arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE,
										arReceipt, AD_BRNCH, AD_CMPNY);

							} else {
								System.out.println("Prep and Sales Tax");
								LocalArTaxCode arTaxCodePreparedFoodTax = null;
								if (preparedTax != 0) {
									try {

										arTaxCodePreparedFoodTax = arTaxCodeHome.findByTcName("Prepared Food Tax",
												AD_CMPNY);

									} catch (FinderException ex) {

									}

									if (arTaxCodePreparedFoodTax != null) {
										this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE,
												preparedTax2,
												arTaxCodePreparedFoodTax.getGlChartOfAccount().getCoaCode(),
												EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);
									}

								}
								System.out.println("ADD TAX salesTax: " + salesTax);
								System.out.println("ADD TAX salesTax2: " + salesTax2);
								LocalArTaxCode arTaxCodeSalesTax = null;
								if (salesTax != 0) {
									try {

										arTaxCodeSalesTax = arTaxCodeHome.findByTcName("Sales Tax", AD_CMPNY);
										System.out.println("Find Sales Tax: " + salesTax2);
									} catch (FinderException ex) {

									}

									if (arTaxCodeSalesTax != null) {
										this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE,
												salesTax2, arTaxCodeSalesTax.getGlChartOfAccount().getCoaCode(),
												EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);
									}

								}
							}

							/*
							 * this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE,
							 * TOTAL_TAX, arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(),
							 * EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);
							 */

						}

					}

					// add cash distribution

					this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE, arReceipt.getRctAmount(),
							arReceipt.getAdBankAccount().getBaCoaGlCashAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
							AD_CMPNY);

					if (arModUploadReceiptDetails.getRctPosDiscount() != 0) {
						// add discount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
								arModUploadReceiptDetails.getRctPosDiscount(),
								adPreference.getPrfMiscPosDiscountAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosScAmount() != 0) {
						// add sc amount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "SERVCE CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosScAmount(),
								adPreference.getPrfMiscPosServiceChargeAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDcAmount() != 0) {
						// add dc amount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "DINEIN CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosDcAmount(),
								adPreference.getPrfMiscPosDineInChargeAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					// add forex gain/loss

					double forexGainLoss = TOTAL_LINE
							- (arReceipt.getRctAmount() + arModUploadReceiptDetails.getRctPosDiscount()
									- arModUploadReceiptDetails.getRctPosScAmount()
									- arModUploadReceiptDetails.getRctPosDcAmount());
					if (forexGainLoss != 0) {
						this.addArDrEntry(arReceipt.getArDrNextLine(), "FOREX",
								forexGainLoss > 0 ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(forexGainLoss),
								adPreference.getPrfMiscPosGiftCertificateAccount(), EJBCommon.FALSE, arReceipt,
								AD_BRNCH, AD_CMPNY);
					}

				}

				Iterator invIter = invoiceNumbers.values().iterator();

				while (invIter.hasNext()) {

					ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invIter.next();

					LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
							arModUploadReceiptDetails.getRctNumber(), (byte) 0, AD_BRNCH, AD_CMPNY);
					arInvoice.setInvReferenceNumber(firstNumber + "-" + lastNumber);

					short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
					double TOTAL_PAYMENT_SCHEDULE = 0d;

					GregorianCalendar gcPrevDateDue = new GregorianCalendar();
					GregorianCalendar gcDateDue = new GregorianCalendar();
					gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

					Collection adPaymentSchedules = arInvoice.getAdPaymentTerm().getAdPaymentSchedules();

					Iterator i = adPaymentSchedules.iterator();

					while (i.hasNext()) {

						LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule) i.next();

						// get date due

						if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")) {

							gcDateDue.setTime(arInvoice.getInvEffectivityDate());
							gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

						} else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")) {

							gcDateDue = gcPrevDateDue;
							gcDateDue.add(Calendar.MONTH, 1);
							gcPrevDateDue = gcDateDue;

						} else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

							gcDateDue = gcPrevDateDue;

							if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
								if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31
										&& gcPrevDateDue.get(Calendar.DATE) > 15
										&& gcPrevDateDue.get(Calendar.DATE) < 31) {
									gcDateDue.add(Calendar.DATE, 16);
								} else {
									gcDateDue.add(Calendar.DATE, 15);
								}
							} else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
								if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
										&& gcPrevDateDue.get(Calendar.DATE) == 14) {
									gcDateDue.add(Calendar.DATE, 14);
								} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
										&& gcPrevDateDue.get(Calendar.DATE) >= 15
										&& gcPrevDateDue.get(Calendar.DATE) < 28) {
									gcDateDue.add(Calendar.DATE, 13);
								} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29
										&& gcPrevDateDue.get(Calendar.DATE) >= 15
										&& gcPrevDateDue.get(Calendar.DATE) < 29) {
									gcDateDue.add(Calendar.DATE, 14);
								} else {
									gcDateDue.add(Calendar.DATE, 15);
								}
							}

							gcPrevDateDue = gcDateDue;

						}

						// create a payment schedule

						double PAYMENT_SCHEDULE_AMOUNT = 0;

						// if last payment schedule subtract to avoid rounding difference error

						if (i.hasNext()) {

							PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount()
									/ arInvoice.getAdPaymentTerm().getPytBaseAmount()) * arInvoice.getInvAmountDue(),
									precisionUnit);

						} else {

							PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_PAYMENT_SCHEDULE;

						}

						LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = arInvoicePaymentScheduleHome.create(
								gcDateDue.getTime(), adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT, 0d,
								EJBCommon.FALSE, (short) 0, gcDateDue.getTime(), 0d, 0d, AD_CMPNY);

						// arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);
						arInvoicePaymentSchedule.setArInvoice(arInvoice);

						TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;

					}

					Iterator lineIter = arInvoice.getArInvoiceLineItems().iterator();

					double TOTAL_TAX = 0;
					double TOTAL_LINE = 0;

					while (lineIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) lineIter.next();

						TOTAL_LINE += arInvoiceLineItem.getIliTaxAmount() + arInvoiceLineItem.getIliAmount();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
											invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalAdBranchItemLocation adBranchItemLocation = null;

						try {

							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (arInvoiceLineItem.getIliEnableAutoBuild() == 0
								|| arInvoiceLineItem.getIliEnableAutoBuild() == 1
								|| arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

							if (adBranchItemLocation != null) {

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

							} else {

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
										arInvoice, AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

							}

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(
									invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
									arInvoice, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice,
									AD_BRNCH, AD_CMPNY);

						}

						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

						if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTaxCode()
								.contains("Prepared Food Tax")) {
							preparedTaxInvoice2 = preparedTaxInvoice2 + arInvoiceLineItem.getIliTaxAmount();
						}

						if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTaxCode().contains("Sales Tax")) {
							salesTaxInvoice2 = salesTaxInvoice2 + arInvoiceLineItem.getIliTaxAmount();
						}
					}

					// add tax distribution if necessary

					if (!arInvoice.getArTaxCode().getTcType().equals("NONE")
							&& !arInvoice.getArTaxCode().getTcType().equals("EXEMPT")) {

						if (adCompany.getCmpShortName().toLowerCase().equals("tinderbox")) {
							String brTaxCOA = this.getBranchTaxCOA(BR_BRNCH_CODE,
									arInvoice.getArTaxCode().getGlChartOfAccount().getCoaSegment1(),
									arInvoice.getArTaxCode().getGlChartOfAccount().getCoaSegment3(),
									arInvoice.getArTaxCode().getGlChartOfAccount().getCoaSegment4(),
									arInvoice.getArTaxCode().getGlChartOfAccount().getCoaSegment5());
							/*
							 * this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE,
							 * TOTAL_TAX, glChartOfAccountHome.findByCoaAccountNumber(brTaxCOA,
							 * AD_CMPNY).getCoaCode(), EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
							 */

							LocalArTaxCode arTaxCodePreparedFoodTax = null;
							if (preparedTax != 0) {
								try {

									arTaxCodePreparedFoodTax = arTaxCodeHome.findByTcName("Prepared Food Tax",
											AD_CMPNY);

								} catch (FinderException ex) {

								}

								if (arTaxCodePreparedFoodTax != null) {
									this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE,
											preparedTaxInvoice2,
											arTaxCodePreparedFoodTax.getGlChartOfAccount().getCoaCode(),
											EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
								}

							}
							System.out.println("ADD TAX salesTax: " + salesTax);
							LocalArTaxCode arTaxCodeSalesTax = null;
							if (salesTax != 0) {
								try {

									arTaxCodeSalesTax = arTaxCodeHome.findByTcName("Sales Tax", AD_CMPNY);

								} catch (FinderException ex) {

								}

								if (arTaxCodeSalesTax != null) {
									this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE,
											salesTaxInvoice2, arTaxCodeSalesTax.getGlChartOfAccount().getCoaCode(),
											EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
								}

							}
						} else {
							LocalArTaxCode arTaxCodePreparedFoodTax = null;
							if (preparedTax != 0) {
								try {

									arTaxCodePreparedFoodTax = arTaxCodeHome.findByTcName("Prepared Food Tax",
											AD_CMPNY);

								} catch (FinderException ex) {

								}

								if (arTaxCodePreparedFoodTax != null) {
									this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE,
											preparedTaxInvoice2,
											arTaxCodePreparedFoodTax.getGlChartOfAccount().getCoaCode(),
											EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
								}

							}
							System.out.println("ADD TAX salesTax: " + salesTax);
							LocalArTaxCode arTaxCodeSalesTax = null;
							if (salesTax != 0) {
								try {

									arTaxCodeSalesTax = arTaxCodeHome.findByTcName("Sales Tax", AD_CMPNY);

								} catch (FinderException ex) {

								}

								if (arTaxCodeSalesTax != null) {
									this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE,
											salesTaxInvoice2, arTaxCodeSalesTax.getGlChartOfAccount().getCoaCode(),
											EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
								}

							}
							/*
							 * if(preparedTax!=0){ this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX",
							 * EJBCommon.FALSE, preparedTaxInvoice2,
							 * adPreference.getPrfInvPreparedFoodTaxCodeAccount(), EJBCommon.FALSE,
							 * arInvoice, AD_BRNCH, AD_CMPNY); }
							 * 
							 * if(salesTax!=0){ this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX",
							 * EJBCommon.FALSE, salesTaxInvoice2,
							 * adPreference.getPrfInvSalesTaxCodeAccount(), EJBCommon.FALSE, arInvoice,
							 * AD_BRNCH, AD_CMPNY); }
							 */
							/*
							 * this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE,
							 * TOTAL_TAX, arInvoice.getArTaxCode().getGlChartOfAccount().getCoaCode(),
							 * EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
							 */

						}

					}

					// add cash distribution

					LocalAdBranchCustomer adBranchCustomer = null;

					try {

						adBranchCustomer = adBranchCustomerHome
								.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if (adBranchCustomer != null) {

						this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
								arInvoice.getInvAmountDue(), adBranchCustomer.getBcstGlCoaReceivableAccount(),
								EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);

					} else {
						this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
								arInvoice.getInvAmountDue(), arInvoice.getArCustomer().getCstGlCoaReceivableAccount(),
								EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDiscount() != 0) {
						// add discount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
								arModUploadReceiptDetails.getRctPosDiscount(),
								adPreference.getPrfMiscPosDiscountAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosScAmount() != 0) {
						// add sc amount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "SERVCE CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosScAmount(),
								adPreference.getPrfMiscPosServiceChargeAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDcAmount() != 0) {
						// add dc amount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "DINEIN CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosDcAmount(),
								adPreference.getPrfMiscPosDineInChargeAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					// add forex gain/loss

					double forexGainLoss = TOTAL_LINE
							- (arInvoice.getInvAmountDue() + arModUploadReceiptDetails.getRctPosDiscount()
									- arModUploadReceiptDetails.getRctPosScAmount()
									- arModUploadReceiptDetails.getRctPosDcAmount());
					if (forexGainLoss != 0) {
						this.addArDrEntry(arInvoice.getArDrNextLine(), "FOREX",
								forexGainLoss > 0 ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(forexGainLoss),
								adPreference.getPrfMiscPosGiftCertificateAccount(), EJBCommon.FALSE, arInvoice,
								AD_BRNCH, AD_CMPNY);
					}

				}

				success = 1;
				return success;
			} else {

				System.out.println("NULL ACCOUNTs");
				System.out.println(success);
				return success;

			}

		} catch (Exception ex) {

			ex.printStackTrace();
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method
	 **/
	public int setArMiscReceiptAllNewAndVoidWithExpiryDate(String[] newRR, String[] voidRR, String BR_BRNCH_CODE,
			Integer AD_CMPNY, String CASHIER) {
		// WEB SERVICE POINT
		Debug.print("ArMiscReceiptSyncControllerBean setInvMiscReceiptAllNewAndVoid");

		InvItemEntryControllerHome homeII = null;
		InvItemEntryController ejbII = null;

		LocalAdBranchHome adBranchHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalArReceiptBatchHome arReceiptBatchHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoiceBatchHome arInvoiceBatchHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalAdBranchCustomerHome adBranchCustomerHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdBranchBankAccountHome adBranchBankAccountHome = null;
		LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;
		LocalInvTagHome invTagHome = null;

		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalArSalespersonHome arSalespersonHome = null;
		try {

			homeRIC = (InvRepItemCostingControllerHome) com.util.EJBHomeFactory
					.lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

			homeII = (InvItemEntryControllerHome) com.util.EJBHomeFactory.lookUpHome("ejb/InvItemEntryControllerEJB",
					InvItemEntryControllerHome.class);

		} catch (Exception e) {

		}

		try {

			System.out.println("1-------------->homeRIC.create()");
			ejbRIC = homeRIC.create();
			ejbII = homeII.create();
			System.out.println("2-------------->homeRIC.create()");
		} catch (Exception e) {
			System.out.println("3-------------->homeRIC.create()");
		}

		try {

			adApprovalHome = (LocalAdApprovalHome) EJBHomeFactory.lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME,
					LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);

			adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);
			adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
					LocalAdBranchHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			arReceiptHome = (LocalArReceiptHome) EJBHomeFactory.lookUpLocalHome(LocalArReceiptHome.JNDI_NAME,
					LocalArReceiptHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
					LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
							LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory.lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME,
					LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory
					.lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			arCustomerHome = (LocalArCustomerHome) EJBHomeFactory.lookUpLocalHome(LocalArCustomerHome.JNDI_NAME,
					LocalArCustomerHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			arReceiptBatchHome = (LocalArReceiptBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory.lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME,
					LocalArInvoiceHome.class);
			arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome) EJBHomeFactory
					.lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			invLocationHome = (LocalInvLocationHome) EJBHomeFactory.lookUpLocalHome(LocalInvLocationHome.JNDI_NAME,
					LocalInvLocationHome.class);
			adBranchCustomerHome = (LocalAdBranchCustomerHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
					LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

			adBranchBankAccountHome = (LocalAdBranchBankAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);

			invTagHome = (LocalInvTagHome) EJBHomeFactory.lookUpLocalHome(LocalInvTagHome.JNDI_NAME,
					LocalInvTagHome.class);

			arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
					.lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			Integer AD_BRNCH = adBranch.getBrCode();

			int success = 0;

			if (adPreference.getPrfMiscPosDiscountAccount() != null
					&& adPreference.getPrfMiscPosGiftCertificateAccount() != null
					&& adPreference.getPrfMiscPosServiceChargeAccount() != null
					&& adPreference.getPrfMiscPosDineInChargeAccount() != null) {

				// new receipts

				HashMap receiptNumbers = new HashMap();
				HashMap invoiceNumbers = new HashMap();
				String fundTransferNumber = null;
				String firstNumber = "";
				String lastNumber = "";

				for (int i = 0; i < newRR.length; i++) {

					ArModReceiptDetails arModReceiptDetails = receiptDecodeWithExpiryDate(newRR[i]);
					String reference_number = arModReceiptDetails.getRctNumber();
					String customerName = arModReceiptDetails.getRctCstName();
					String customerAddress = arModReceiptDetails.getRctCustomerAddress();

					// Commented to be able to create receipt in each transaction
					// if (i == 0) firstNumber = arModReceiptDetails.getRctNumber();
					// if (i == newRR.length - 1) lastNumber = arModReceiptDetails.getRctNumber();

					if (arModReceiptDetails.getRctPosOnAccount() == 0) {

						// generate receipt number

						String generatedReceipt = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						LocalArReceipt arExistingReceipt = null;
						/*
						 * Commented to be able to create receipt each transaction Iterator rctIter =
						 * receiptNumbers.values().iterator(); while (rctIter.hasNext()) {
						 * 
						 * try {
						 * 
						 * ArModReceiptDetails arModUploadReceiptDetails =
						 * (ArModReceiptDetails)rctIter.next();
						 * 
						 * arExistingReceipt =
						 * arReceiptHome.findByRctDateAndRctNumberAndCstCustomerCodeAndBrCode(
						 * arModReceiptDetails.getRctDate(), arModUploadReceiptDetails.getRctNumber(),
						 * arModReceiptDetails.getRctCstCustomerCode(), AD_BRNCH, AD_CMPNY);
						 * 
						 * break;
						 * 
						 * } catch (FinderException ex) {
						 * 
						 * continue;
						 * 
						 * }
						 * 
						 * }
						 */

						Collection existingReceipts = arReceiptHome.findByRctRfrncNmbrAndBrCode(reference_number,
								AD_BRNCH, AD_CMPNY);

						if (arExistingReceipt == null || existingReceipts.size() <= 0) {
							System.out.println(generatedReceipt);
							ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
							arModUploadReceiptDetails.setRctNumber(generatedReceipt);
							arModUploadReceiptDetails
									.setRctPosDiscount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));

							receiptNumbers.put(generatedReceipt, arModUploadReceiptDetails);

							System.out.println(arModReceiptDetails.getRctConversionRate()
									+ " <`<CONV RATE:MISC:POS DISC>> " + arModReceiptDetails.getRctPosDiscount());

							double rctAmount = EJBCommon.roundIt(arModReceiptDetails.getRctPosTotalAmount()
									* arModReceiptDetails.getRctConversionRate(), (short) 2);
							System.out.println("create Misc Receipt: " + rctAmount);

							LocalArReceipt arReceipt = arReceiptHome.create("MISC", "POS Sales " + new Date(),
									arModReceiptDetails.getRctDate(), generatedReceipt, null, null, null,
									arModReceiptDetails.getRctChequeNumber(), arModReceiptDetails.getRctVoucherNumber(),
									arModReceiptDetails.getRctCardNumber1(), arModReceiptDetails.getRctCardNumber2(),
									arModReceiptDetails.getRctCardNumber3(), rctAmount,
									arModReceiptDetails.getRctAmountCash(), arModReceiptDetails.getRctAmountCheque(),
									arModReceiptDetails.getRctAmountVoucher(), arModReceiptDetails.getRctAmountCard1(),
									arModReceiptDetails.getRctAmountCard2(), arModReceiptDetails.getRctAmountCard3(),
									null, 1, null, "CASH", EJBCommon.FALSE, 0, null, null, EJBCommon.FALSE, null,
									EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
									EJBCommon.FALSE, EJBCommon.FALSE, null, null, null, null, null, CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
									EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, customerName,
									customerAddress, EJBCommon.FALSE, EJBCommon.FALSE, null, AD_BRNCH, AD_CMPNY);

							arReceipt.setRctReferenceNumber(reference_number);
							LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
									.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
							// glFunctionalCurrency.addArReceipt(arReceipt);
							arReceipt.setGlFunctionalCurrency(glFunctionalCurrency);
							System.out.println("customer: " + arModReceiptDetails.getRctCstCustomerCode());
							LocalArCustomer arCustomer = arCustomerHome
									.findByCstCustomerCode(arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
							System.out.println("customerrrrrrrrrr: " + arCustomer.getCstCode());
							// arCustomer.addArReceipt(arReceipt);
							arReceipt.setArCustomer(arCustomer);

							if (!arModReceiptDetails.getRctSlpSalespersonCode().equals("")) {
								LocalArSalesperson arSalesperson = arSalespersonHome.findBySlpSalespersonCode(
										arModReceiptDetails.getRctSlpSalespersonCode(), AD_CMPNY);

								arReceipt.setArSalesperson(arSalesperson);

							}

							LocalAdBankAccount adBankAccount = adBankAccountHome
									.findByBaName(arCustomer.getAdBankAccount().getBaName(), AD_CMPNY);
							// adBankAccount.addArReceipt(arReceipt);
							arReceipt.setAdBankAccount(adBankAccount);

							// card 1 bank account
							if (!arModReceiptDetails.getRctCardType1().equals("")
									&& arModReceiptDetails.getRctAmountCard1() > 0) {
								System.out.println(arModReceiptDetails.getRctCardType1() + " 1");
								adBankAccount = adBankAccountHome.findByBaName(arModReceiptDetails.getRctCardType1(),
										AD_CMPNY);
								arReceipt.setAdBankAccountCard1(adBankAccount);
							}

							// card 2 bank account
							if (!arModReceiptDetails.getRctCardType2().equals("")
									&& arModReceiptDetails.getRctAmountCard2() > 0) {
								System.out.println(arModReceiptDetails.getRctCardType2() + " 2");
								adBankAccount = adBankAccountHome.findByBaName(arModReceiptDetails.getRctCardType2(),
										AD_CMPNY);
								arReceipt.setAdBankAccountCard2(adBankAccount);
							}

							// card 3 bank account
							if (!arModReceiptDetails.getRctCardType3().equals("")
									&& arModReceiptDetails.getRctAmountCard3() > 0) {
								System.out.println(arModReceiptDetails.getRctCardType3() + " 3");
								adBankAccount = adBankAccountHome.findByBaName(arModReceiptDetails.getRctCardType3(),
										AD_CMPNY);
								arReceipt.setAdBankAccountCard3(adBankAccount);
							}

							LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(arModReceiptDetails.getRctTcName(),
									AD_CMPNY);

							arReceipt.setArTaxCode(arTaxCode);

							LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome
									.findByWtcName("NONE", AD_CMPNY);
							// arWithholdingTaxCode.addArReceipt(arReceipt);
							arReceipt.setArWithholdingTaxCode(arWithholdingTaxCode);

							System.out.println("BATCH---------------->" + arModReceiptDetails.getRctPOSName());
							LocalArReceiptBatch arReceiptBatch = null;
							try {
								arReceiptBatch = arReceiptBatchHome.findByRbName(arModReceiptDetails.getRctPOSName(),
										AD_BRNCH, AD_CMPNY);
							} catch (FinderException ex) {
								arReceiptBatch = arReceiptBatchHome.create(arModReceiptDetails.getRctPOSName(),
										arModReceiptDetails.getRctPOSName(), "OPEN", "MISC",
										EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);

							}
							// arReceiptBatch.addArReceipt(arReceipt);
							arReceipt.setArReceiptBatch(arReceiptBatch);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();
							short lineNumber = 0;
							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();

								short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

								double ILI_AMNT = 0d;
								double ILI_TAX_AMNT = 0d;

								double ILI_NET_AMOUNT = arModInvoiceLineItemDetails.getIliAmount()
										- arModInvoiceLineItemDetails.getIliTotalDiscount();

								// calculate net amount

								// LocalArTaxCode arTaxCode = arReceipt.getArTaxCode();

								if (arTaxCode.getTcType().equals("INCLUSIVE")) {

									ILI_AMNT = EJBCommon.roundIt(ILI_NET_AMOUNT / (1 + (arTaxCode.getTcRate() / 100)),
											precisionUnit);

								} else {

									// tax exclusive, none, zero rated or exempt

									ILI_AMNT = ILI_NET_AMOUNT;

								}

								// calculate tax

								if (!arTaxCode.getTcType().equals("NONE") && !arTaxCode.getTcType().equals("EXEMPT")) {

									if (arTaxCode.getTcType().equals("INCLUSIVE")) {

										ILI_TAX_AMNT = EJBCommon.roundIt(ILI_NET_AMOUNT - ILI_AMNT, precisionUnit);

									} else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

										ILI_TAX_AMNT = EJBCommon.roundIt(ILI_NET_AMOUNT * arTaxCode.getTcRate() / 100,
												precisionUnit);

									} else {

										// tax none zero-rated or exempt

									}

								}

								LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(++lineNumber,
										arModInvoiceLineItemDetails.getIliQuantity(),
										arModInvoiceLineItemDetails.getIliUnitPrice(), ILI_AMNT, ILI_TAX_AMNT,
										EJBCommon.FALSE, 0, 0, 0, 0, arModInvoiceLineItemDetails.getIliTotalDiscount(),
										EJBCommon.TRUE, AD_CMPNY);

								arInvoiceLineItem.setArReceipt(arReceipt);

								System.out.println("UOM NAME=" + arModInvoiceLineItemDetails.getIliUomName());
								System.out.println("AD_CMPNY=" + AD_CMPNY);
								LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
										.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								System.out.println("--------");
								// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);
								arInvoiceLineItem.setIliImei(arModInvoiceLineItemDetails.getILI_IMEI());

								// create tags for IMEI

								System.out.println(arModInvoiceLineItemDetails.getIliMisc() + "----------");
								arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
								System.out.println("arModInvoiceLineItemDetails.getIliIiCode: "
										+ arModInvoiceLineItemDetails.getIliCode());
								System.out.println("arModInvoiceLineItemDetails.getIliIiName: "
										+ arModInvoiceLineItemDetails.getIliIiName());
								// LocalInvItem invItem =
								// invItemHome.findByIiName(arModInvoiceLineItemDetails.getIliIiName().trim().toString(),
								// AD_CMPNY);
								LocalInvItem invItem = invItemHome
										.findByIiCode(arModInvoiceLineItemDetails.getIliCode(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}

								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiCodeAndLocName(
										arModInvoiceLineItemDetails.getIliCode(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);
								// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvItemLocation(invItemLocation);

								if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

									arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

								}
								// new code
								// add serial into inv tag
								if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc() == 1) {

									ArrayList tagList = arModInvoiceLineItemDetails.getiIliTagList();

									System.out.print("misc receipt sync taglist size: " + tagList.size());
									Iterator iTag = tagList.iterator();

									while (iTag.hasNext()) {

										InvModTagListDetails details = (InvModTagListDetails) iTag.next();

										LocalInvTag invTag = invTagHome.create(details.getTgPropertyCode(),
												details.getTgSerialNumber(), details.getTgDocumentNumber(),
												details.getTgExpiryDate(), details.getTgSpecs(), AD_CMPNY,
												details.getTgTransactionDate(), "IN");

										invTag.setArInvoiceLineItem(arInvoiceLineItem);
										invTag.setInvItemLocation(arInvoiceLineItem.getInvItemLocation());
									}

								}

							}

						}

					} else {

						// generate Invoice number

						String generatedInvoice = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR INVOICE",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						/*
						 * LocalArInvoice arExistingInvoice = null;
						 * 
						 * Iterator invIter = invoiceNumbers.values().iterator(); while
						 * (invIter.hasNext()) {
						 * 
						 * try {
						 * 
						 * ArModReceiptDetails arModUploadReceiptDetails =
						 * (ArModReceiptDetails)invIter.next();
						 * 
						 * arExistingInvoice =
						 * arInvoiceHome.findByInvNumberAndInvCreditMemoAndCstCustomerCode(
						 * arModUploadReceiptDetails.getRctNumber(), (byte)0,
						 * arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
						 * 
						 * break;
						 * 
						 * } catch (FinderException ex) {
						 * 
						 * continue;
						 * 
						 * }
						 * 
						 * 
						 * }
						 * 
						 * if (arExistingInvoice == null) {
						 * 
						 * ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
						 * arModUploadReceiptDetails.setRctNumber(generatedInvoice);
						 * arModUploadReceiptDetails.setRctPosDiscount(EJBCommon.roundIt(
						 * arModReceiptDetails.getRctPosDiscount() *
						 * arModReceiptDetails.getRctConversionRate(), (short)2));
						 * arModUploadReceiptDetails.setRctPosScAmount(EJBCommon.roundIt(
						 * arModReceiptDetails.getRctPosScAmount() *
						 * arModReceiptDetails.getRctConversionRate(), (short)2));
						 * arModUploadReceiptDetails.setRctPosDcAmount(EJBCommon.roundIt(
						 * arModReceiptDetails.getRctPosDcAmount() *
						 * arModReceiptDetails.getRctConversionRate(), (short)2));
						 * invoiceNumbers.put(generatedInvoice, arModUploadReceiptDetails);
						 * System.out.println(arModReceiptDetails.getRctConversionRate()
						 * +" <<CONV RATE:INVC:POS DISC>> " + arModReceiptDetails.getRctPosDiscount());
						 * 
						 * double totalAmount =
						 * EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount() -
						 * arModReceiptDetails.getRctPosVoidAmount()) *
						 * arModReceiptDetails.getRctConversionRate(), (short)2);
						 * 
						 * LocalArInvoice arInvoice = arInvoiceHome.create( (byte)0, "POS Sales " + new
						 * Date().toString(), arModReceiptDetails.getRctDate(), generatedInvoice,
						 * null,null, null, null, totalAmount, 0d,0d, null, 1, 0d, 0d, null, null, null,
						 * null, null, null, null, null, null, null, null, null, null, null, null, null,
						 * null, null, null, null, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
						 * EJBCommon.FALSE, null, 0d, null, null, null, null, CASHIER,
						 * EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
						 * EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
						 * EJBCommon.FALSE, null, null, (byte)0, (byte)0, null,
						 * arModReceiptDetails.getRctDate(), AD_BRNCH, AD_CMPNY );
						 * 
						 * 
						 * arInvoice.setInvReferenceNumber( reference_number); LocalAdPaymentTerm
						 * adPaymentTerm = adPaymentTermHome.findByPytName("IMMEDIATE", AD_CMPNY);
						 * //adPaymentTerm.addArInvoice(arInvoice);
						 * arInvoice.setAdPaymentTerm(adPaymentTerm);
						 * 
						 * LocalGlFunctionalCurrency glFunctionalCurrency =
						 * glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().
						 * getFcName(), AD_CMPNY); //glFunctionalCurrency.addArInvoice(arInvoice);
						 * arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);
						 * 
						 * LocalArCustomer arCustomer =
						 * arCustomerHome.findByCstCustomerCode(arModReceiptDetails.
						 * getRctCstCustomerCode(), AD_CMPNY); //arCustomer.addArInvoice(arInvoice);
						 * arInvoice.setArCustomer(arCustomer);
						 * 
						 * 
						 * LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE",
						 * AD_CMPNY); //arTaxCode.addArReceipt(arReceipt);
						 * arInvoice.setArTaxCode(arTaxCode);
						 * 
						 * 
						 * LocalArWithholdingTaxCode arWithholdingTaxCode =
						 * arWithholdingTaxCodeHome.findByWtcName("NONE", AD_CMPNY);
						 * //arWithholdingTaxCode.addArInvoice(arInvoice);
						 * arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);
						 * 
						 * LocalArInvoiceBatch arInvoiceBatch = null; try { arInvoiceBatch =
						 * arInvoiceBatchHome.findByIbName("POS BATCH", AD_BRNCH, AD_CMPNY); } catch
						 * (FinderException ex) { arInvoiceBatch =
						 * arInvoiceBatchHome.create("POS BATCH", "POS BATCH", "OPEN", "MISC",
						 * EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);
						 * 
						 * } //arInvoiceBatch.addArInvoice(arInvoice);
						 * arInvoice.setArInvoiceBatch(arInvoiceBatch);
						 * 
						 * 
						 * Iterator iter = arModReceiptDetails.getInvIliList().iterator(); short
						 * lineNumber = 0; while (iter.hasNext()) {
						 * 
						 * ArModInvoiceLineItemDetails arModInvoiceLineItemDetails =
						 * (ArModInvoiceLineItemDetails)iter.next(); double taxAmount =
						 * EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount() -
						 * (arModInvoiceLineItemDetails.getIliAmount() / (1 +
						 * arInvoice.getArTaxCode().getTcRate() / 100)), (short)2); double amount =
						 * EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount() / (1 +
						 * arInvoice.getArTaxCode().getTcRate() / 100), (short)2);
						 * LocalArInvoiceLineItem arInvoiceLineItem =
						 * arInvoiceLineItemHome.create(++lineNumber,
						 * arModInvoiceLineItemDetails.getIliQuantity(),
						 * arModInvoiceLineItemDetails.getIliUnitPrice(), amount, taxAmount,
						 * EJBCommon.FALSE, 0, 0, 0, 0, arModUploadReceiptDetails.getRctPosDiscount(),
						 * AD_CMPNY);
						 * 
						 * //arInvoice.addArInvoiceLineItem(arInvoiceLineItem);
						 * arInvoiceLineItem.setArInvoice(arInvoice);
						 * 
						 * LocalInvUnitOfMeasure invUnitOfMeasure =
						 * invUnitOfMeasureHome.findByUomName(arModInvoiceLineItemDetails.getIliUomName(
						 * ), AD_CMPNY); //invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
						 * arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);
						 * 
						 * arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
						 * 
						 * LocalInvItem invItem =
						 * invItemHome.findByIiName(arModInvoiceLineItemDetails.getIliIiName(),
						 * AD_CMPNY); LocalInvLocation invLocation = null; try { if
						 * (invItem.getIiDefaultLocation() != null) invLocation =
						 * invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation()); } catch
						 * (FinderException ex) { } LocalInvItemLocation invItemLocation =
						 * invItemLocationHome.findByIiNameAndLocName(arModInvoiceLineItemDetails.
						 * getIliIiName(), invLocation == null ?
						 * arModInvoiceLineItemDetails.getIliLocName() : invLocation.getLocName(),
						 * AD_CMPNY); //invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
						 * arInvoiceLineItem.setInvItemLocation(invItemLocation); if
						 * (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {
						 * 
						 * arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);
						 * 
						 * } }
						 * 
						 * } else {
						 * 
						 * ArModReceiptDetails arModUploadReceiptDetails =
						 * (ArModReceiptDetails)invoiceNumbers.get(arExistingInvoice.getInvNumber());
						 * 
						 * arModUploadReceiptDetails.setRctPosDiscount(arModUploadReceiptDetails.
						 * getRctPosDiscount() +
						 * EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount() *
						 * arModReceiptDetails.getRctConversionRate(), (short)2));
						 * arModUploadReceiptDetails.setRctPosScAmount(arModUploadReceiptDetails.
						 * getRctPosScAmount() +
						 * EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount() *
						 * arModReceiptDetails.getRctConversionRate(), (short)2));
						 * arModUploadReceiptDetails.setRctPosDcAmount(arModUploadReceiptDetails.
						 * getRctPosDcAmount() +
						 * EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount() *
						 * arModReceiptDetails.getRctConversionRate(), (short)2));
						 * invoiceNumbers.put(arExistingInvoice.getInvNumber(),
						 * arModUploadReceiptDetails);
						 * System.out.println(arModReceiptDetails.getRctConversionRate()
						 * +" <<CONV RATE:INVC:POS DISC>> " + arModReceiptDetails.getRctPosDiscount());
						 * 
						 * double totalAmount =
						 * EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount() -
						 * arModReceiptDetails.getRctPosVoidAmount()) *
						 * arModReceiptDetails.getRctConversionRate(), (short)2);
						 * arExistingInvoice.setInvAmountDue(arExistingInvoice.getInvAmountDue() +
						 * totalAmount);
						 * 
						 * Iterator iter = arModReceiptDetails.getInvIliList().iterator();
						 * 
						 * 
						 * while (iter.hasNext()) {
						 * 
						 * ArModInvoiceLineItemDetails arModInvoiceLineItemDetails =
						 * (ArModInvoiceLineItemDetails)iter.next();
						 * 
						 * LocalInvItem invItem =
						 * invItemHome.findByIiName(arModInvoiceLineItemDetails.getIliIiName(),
						 * AD_CMPNY); LocalInvLocation invLocation = null; try { if
						 * (invItem.getIiDefaultLocation() != null) invLocation =
						 * invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation()); } catch
						 * (FinderException ex) { } LocalInvItemLocation invItemLocation =
						 * invItemLocationHome.findByIiNameAndLocName(arModInvoiceLineItemDetails.
						 * getIliIiName(), invLocation == null ?
						 * arModInvoiceLineItemDetails.getIliLocName() : invLocation.getLocName(),
						 * AD_CMPNY);
						 * 
						 * LocalArInvoiceLineItem arExistingInvoiceLineItem = null;
						 * 
						 * try { arExistingInvoiceLineItem =
						 * arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(arExistingInvoice.
						 * getInvCode(), invItemLocation.getIlCode(),
						 * arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY); } catch
						 * (FinderException ex) {
						 * 
						 * }
						 * 
						 * if (arExistingInvoiceLineItem == null) {
						 * 
						 * LocalArInvoiceLineItem arInvoiceLineItem =
						 * arInvoiceLineItemHome.create((short)(arExistingInvoice.getArInvoiceLineItems(
						 * ).size() + 1), arModInvoiceLineItemDetails.getIliQuantity(),
						 * arModInvoiceLineItemDetails.getIliUnitPrice(),
						 * EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount() / (1 +
						 * arExistingInvoice.getArTaxCode().getTcRate() / 100), (short)2),
						 * EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount() -
						 * (arModInvoiceLineItemDetails.getIliAmount() / (1 +
						 * arExistingInvoice.getArTaxCode().getTcRate() / 100)), (short)2),
						 * EJBCommon.FALSE, 0, 0, 0, 0, 0, AD_CMPNY);
						 * 
						 * //arExistingInvoice.addArInvoiceLineItem(arInvoiceLineItem);
						 * arInvoiceLineItem.setArInvoice(arExistingInvoice);
						 * 
						 * LocalInvUnitOfMeasure invUnitOfMeasure =
						 * invUnitOfMeasureHome.findByUomName(arModInvoiceLineItemDetails.getIliUomName(
						 * ), AD_CMPNY); //invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
						 * arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);
						 * arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
						 * //invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
						 * arInvoiceLineItem.setInvItemLocation(invItemLocation); if
						 * (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {
						 * 
						 * arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);
						 * 
						 * } } else {
						 * 
						 * arExistingInvoiceLineItem.setIliQuantity(arExistingInvoiceLineItem.
						 * getIliQuantity() + arModInvoiceLineItemDetails.getIliQuantity());
						 * arExistingInvoiceLineItem.setIliUnitPrice((arExistingInvoiceLineItem.
						 * getIliUnitPrice() + arModInvoiceLineItemDetails.getIliUnitPrice()) / 2);
						 * arExistingInvoiceLineItem.setIliAmount(arExistingInvoiceLineItem.getIliAmount
						 * () + EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount() / (1 +
						 * arExistingInvoice.getArTaxCode().getTcRate() / 100), (short)2));
						 * arExistingInvoiceLineItem.setIliTaxAmount(arExistingInvoiceLineItem.
						 * getIliTaxAmount() +
						 * EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount() -
						 * (arModInvoiceLineItemDetails.getIliAmount() / (1 +
						 * arExistingInvoice.getArTaxCode().getTcRate() / 100)), (short)2));
						 * 
						 * } } }
						 */

					}

				}

				// for each receipt generate distribution records

				Iterator rctIter = receiptNumbers.values().iterator();

				while (rctIter.hasNext()) {

					ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) rctIter.next();

					LocalArReceipt arReceipt = arReceiptHome
							.findByRctNumberAndBrCode(arModUploadReceiptDetails.getRctNumber(), AD_BRNCH, AD_CMPNY);
					// arReceipt.setRctReferenceNumber(firstNumber + "-" + lastNumber);

					Iterator lineIter = arReceipt.getArInvoiceLineItems().iterator();

					double TOTAL_TAX = 0;
					double TOTAL_LINE = 0;
					double TOTAL_DISCOUNT = 0;

					while (lineIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) lineIter.next();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
											invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							/*
							 * //check if rmning vl is not zero and rmng qty is 0
							 * if(invCosting.getCstRemainingQuantity() <= 0 &&
							 * invCosting.getCstRemainingValue() <=0){
							 * 
							 * HashMap criteria = new HashMap(); criteria.put("itemName",
							 * invItemLocation.getInvItem().getIiName()); criteria.put("location",
							 * invItemLocation.getInvLocation().getLocName());
							 * 
							 * ArrayList branchList = new ArrayList();
							 * 
							 * AdBranchDetails mdetails = new AdBranchDetails();
							 * mdetails.setBrCode(AD_BRNCH); branchList.add(mdetails);
							 * 
							 * ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);
							 * 
							 * invCosting = invCostingHome.
							 * getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
							 * arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
							 * invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY); }
							 */
							if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {
								COST = ejbII.getInvIiUnitCostByIiNameAndUomName(
										invItemLocation.getInvItem().getIiName(),
										arInvoiceLineItem.getInvUnitOfMeasure().getUomName(), arReceipt.getRctDate(),
										AD_CMPNY);

							}

							else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

								COST = Math.abs(this.getInvFifoCost(invCosting.getCstDate(),
										invCosting.getInvItemLocation().getIlCode(), arInvoiceLineItem.getIliQuantity(),
										arInvoiceLineItem.getIliUnitPrice(), false, AD_BRNCH, AD_CMPNY));

							else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))

								COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalAdBranchItemLocation adBranchItemLocation = null;

						try {

							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if ((arInvoiceLineItem.getIliEnableAutoBuild() == 0
								|| arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock"))
								&& arInvoiceLineItem.getInvItemLocation().getInvItem()
										.getIiNonInventoriable() == EJBCommon.FALSE) {

							if (adBranchItemLocation != null) {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							} else {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
										arReceipt, AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							}

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(
									invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
									arReceipt, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arReceipt,
									AD_BRNCH, AD_CMPNY);

						}

						TOTAL_LINE += arInvoiceLineItem.getIliAmount();

						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();
						TOTAL_DISCOUNT += arInvoiceLineItem.getIliTotalDiscount();

					}

					// add tax distribution if necessary

					if (!arReceipt.getArTaxCode().getTcType().equals("NONE")
							&& !arReceipt.getArTaxCode().getTcType().equals("EXEMPT")) {
						System.out.println("pasok 2");
						LocalAdBranchArTaxCode adBranchTaxCode = null;
						Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
						try {
							adBranchTaxCode = adBranchArTaxCodeHome
									.findBtcByTcCodeAndBrCode(arReceipt.getArTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (adBranchTaxCode != null) {
							this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
									adBranchTaxCode.getBtcGlCoaTaxCode(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
									AD_CMPNY);

						} else {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
									arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE,
									arReceipt, AD_BRNCH, AD_CMPNY);
						}
					}

					// add cash distribution
					LocalAdBranchBankAccount adBranchBankAccount = null;
					LocalAdBranchBankAccount adBranchBankAccountCard1 = null;
					LocalAdBranchBankAccount adBranchBankAccountCard2 = null;
					LocalAdBranchBankAccount adBranchBankAccountCard3 = null;

					if (arReceipt.getRctAmountCard1() > 0) {

						try {
							adBranchBankAccountCard1 = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(
									arReceipt.getAdBankAccountCard1().getBaCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {
						}

						if (adBranchBankAccount != null) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 1", EJBCommon.TRUE,
									arReceipt.getRctAmountCard1(), adBranchBankAccountCard1.getBbaGlCoaCashAccount(),
									EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 1", EJBCommon.TRUE,
									arReceipt.getRctAmountCard1(),
									arReceipt.getAdBankAccountCard1().getBaCoaGlCashAccount(), EJBCommon.FALSE,
									arReceipt, AD_BRNCH, AD_CMPNY);

						}
					}

					if (arReceipt.getRctAmountCard2() > 0) {

						try {
							adBranchBankAccountCard2 = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(
									arReceipt.getAdBankAccountCard2().getBaCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {
						}

						if (adBranchBankAccount != null) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 2", EJBCommon.TRUE,
									arReceipt.getRctAmountCard2(), adBranchBankAccountCard2.getBbaGlCoaCashAccount(),
									EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 2", EJBCommon.TRUE,
									arReceipt.getRctAmountCard2(),
									arReceipt.getAdBankAccountCard2().getBaCoaGlCashAccount(), EJBCommon.FALSE,
									arReceipt, AD_BRNCH, AD_CMPNY);

						}
					}

					if (arReceipt.getRctAmountCard3() > 0) {

						try {
							adBranchBankAccountCard3 = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(
									arReceipt.getAdBankAccountCard3().getBaCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {
						}

						if (adBranchBankAccount != null) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 3", EJBCommon.TRUE,
									arReceipt.getRctAmountCard3(), adBranchBankAccountCard3.getBbaGlCoaCashAccount(),
									EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "CARD 3", EJBCommon.TRUE,
									arReceipt.getRctAmountCard3(),
									arReceipt.getAdBankAccountCard3().getBaCoaGlCashAccount(), EJBCommon.FALSE,
									arReceipt, AD_BRNCH, AD_CMPNY);

						}
					}

					// add cash distribution

					try {
						adBranchBankAccount = adBranchBankAccountHome
								.findBbaByBaCodeAndBrCode(arReceipt.getAdBankAccount().getBaCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (adBranchBankAccount != null) {

						this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE,
								TOTAL_LINE + TOTAL_TAX - TOTAL_DISCOUNT - arReceipt.getRctAmountVoucher()
										- arReceipt.getRctAmountCheque() - arReceipt.getRctAmountCard1()
										- arReceipt.getRctAmountCard2() - arReceipt.getRctAmountCard3(),
								adBranchBankAccount.getBbaGlCoaCashAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);

						if (arReceipt.getRctAmountCheque() > 0) {
							this.addArDrEntry(arReceipt.getArDrNextLine(), "CHEQUE", EJBCommon.TRUE,
									arReceipt.getRctAmountCheque(), adBranchBankAccount.getBbaGlCoaCashAccount(),
									EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

						}

						if (arReceipt.getRctAmountVoucher() > 0) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "VOUCHER", EJBCommon.TRUE,
									arReceipt.getRctAmountVoucher(),
									adBranchBankAccount.getBbaGlCoaSalesDiscountAccount(), EJBCommon.FALSE, arReceipt,
									AD_BRNCH, AD_CMPNY);

						}

						if (TOTAL_DISCOUNT > 0) {
							this.addArDrEntry(arReceipt.getArDrNextLine(), "SALES DISCOUNT", EJBCommon.TRUE,
									TOTAL_DISCOUNT, adBranchBankAccount.getBbaGlCoaSalesDiscountAccount(),
									EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

						}

					} else {

						this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE,
								TOTAL_LINE + TOTAL_TAX - TOTAL_DISCOUNT - arReceipt.getRctAmountVoucher()
										- arReceipt.getRctAmountCheque() - arReceipt.getRctAmountCard1()
										- arReceipt.getRctAmountCard2() - arReceipt.getRctAmountCard3(),
								arReceipt.getAdBankAccount().getBaCoaGlCashAccount(), EJBCommon.FALSE, arReceipt,
								AD_BRNCH, AD_CMPNY);

						if (arReceipt.getRctAmountCheque() > 0) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "CHEQUE", EJBCommon.TRUE,
									arReceipt.getRctAmountCheque(),
									arReceipt.getAdBankAccount().getBaCoaGlCashAccount(), EJBCommon.FALSE, arReceipt,
									AD_BRNCH, AD_CMPNY);

						}

						if (arReceipt.getRctAmountVoucher() > 0) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "VOUCHER", EJBCommon.TRUE,
									arReceipt.getRctAmountVoucher(),
									arReceipt.getAdBankAccount().getBaCoaGlSalesDiscount(), EJBCommon.FALSE, arReceipt,
									AD_BRNCH, AD_CMPNY);

						}

						if (TOTAL_DISCOUNT > 0) {
							this.addArDrEntry(arReceipt.getArDrNextLine(), "SALES DISCOUNT", EJBCommon.TRUE,
									TOTAL_DISCOUNT, arReceipt.getAdBankAccount().getBaCoaGlSalesDiscount(),
									EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

						}

					}

					Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  J11");
					// set receipt amount

					arReceipt.setRctAmount(TOTAL_LINE + TOTAL_TAX);

					// generate approval status

					if (adPreference.getPrfInvEnablePosAutoPostUpload() == EJBCommon.TRUE) {

						this.executeArRctPost(arReceipt.getRctCode(), arReceipt.getRctLastModifiedBy(), AD_BRNCH,
								AD_CMPNY);
						arReceipt.setRctApprovalStatus("N/A");
					} else {
						arReceipt.setRctApprovalStatus(null);
					}

					// set receipt approval status

				}

				/*
				 * Iterator invIter = invoiceNumbers.values().iterator();
				 * 
				 * while (invIter.hasNext()) {
				 * 
				 * ArModReceiptDetails arModUploadReceiptDetails =
				 * (ArModReceiptDetails)invIter.next();
				 * 
				 * LocalArInvoice arInvoice =
				 * arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
				 * arModUploadReceiptDetails.getRctNumber(), (byte)0 , AD_BRNCH, AD_CMPNY ); //
				 * arInvoice.setInvReferenceNumber(firstNumber + "-" + lastNumber);
				 * 
				 * short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY); double
				 * TOTAL_PAYMENT_SCHEDULE = 0d;
				 * 
				 * GregorianCalendar gcPrevDateDue = new GregorianCalendar(); GregorianCalendar
				 * gcDateDue = new GregorianCalendar();
				 * gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());
				 * 
				 * Collection adPaymentSchedules =
				 * arInvoice.getAdPaymentTerm().getAdPaymentSchedules();
				 * 
				 * Iterator i = adPaymentSchedules.iterator();
				 * 
				 * while (i.hasNext()) {
				 * 
				 * LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)i.next();
				 * 
				 * // get date due
				 * 
				 * if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")){
				 * 
				 * gcDateDue.setTime(arInvoice.getInvEffectivityDate());
				 * gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());
				 * 
				 * } else if
				 * (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")){
				 * 
				 * gcDateDue = gcPrevDateDue; gcDateDue.add(Calendar.MONTH, 1); gcPrevDateDue =
				 * gcDateDue;
				 * 
				 * } else
				 * if(arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {
				 * 
				 * gcDateDue = gcPrevDateDue;
				 * 
				 * if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
				 * if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31 &&
				 * gcPrevDateDue.get(Calendar.DATE) > 15 && gcPrevDateDue.get(Calendar.DATE) <
				 * 31){ gcDateDue.add(Calendar.DATE, 16); } else { gcDateDue.add(Calendar.DATE,
				 * 15); } } else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
				 * if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28 &&
				 * gcPrevDateDue.get(Calendar.DATE) == 14) { gcDateDue.add(Calendar.DATE, 14); }
				 * else if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28 &&
				 * gcPrevDateDue.get(Calendar.DATE) >= 15 && gcPrevDateDue.get(Calendar.DATE) <
				 * 28) { gcDateDue.add(Calendar.DATE, 13); } else if
				 * (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29 &&
				 * gcPrevDateDue.get(Calendar.DATE) >= 15 && gcPrevDateDue.get(Calendar.DATE) <
				 * 29) { gcDateDue.add(Calendar.DATE, 14); } else { gcDateDue.add(Calendar.DATE,
				 * 15); } }
				 * 
				 * gcPrevDateDue = gcDateDue;
				 * 
				 * }
				 * 
				 * // create a payment schedule
				 * 
				 * double PAYMENT_SCHEDULE_AMOUNT = 0;
				 * 
				 * // if last payment schedule subtract to avoid rounding difference error
				 * 
				 * if (i.hasNext()) {
				 * 
				 * PAYMENT_SCHEDULE_AMOUNT =
				 * EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount() /
				 * arInvoice.getAdPaymentTerm().getPytBaseAmount()) *
				 * arInvoice.getInvAmountDue(), precisionUnit);
				 * 
				 * } else {
				 * 
				 * PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() -
				 * TOTAL_PAYMENT_SCHEDULE;
				 * 
				 * }
				 * 
				 * LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
				 * arInvoicePaymentScheduleHome.create(gcDateDue.getTime(),
				 * adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT, 0d,
				 * EJBCommon.FALSE, AD_CMPNY);
				 * 
				 * //arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);
				 * arInvoicePaymentSchedule.setArInvoice(arInvoice);
				 * 
				 * TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;
				 * 
				 * }
				 * 
				 * Iterator lineIter = arInvoice.getArInvoiceLineItems().iterator();
				 * 
				 * double TOTAL_TAX = 0; double TOTAL_LINE = 0;
				 * 
				 * while (lineIter.hasNext()) {
				 * 
				 * LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)
				 * lineIter.next();
				 * 
				 * TOTAL_LINE += arInvoiceLineItem.getIliTaxAmount() +
				 * arInvoiceLineItem.getIliAmount();
				 * 
				 * LocalInvItemLocation invItemLocation =
				 * arInvoiceLineItem.getInvItemLocation();
				 * 
				 * // add cost of sales distribution and inventory
				 * 
				 * double COST = 0d;
				 * 
				 * try {
				 * 
				 * LocalInvCosting invCosting = invCostingHome.
				 * getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(
				 * arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
				 * invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
				 * 
				 * COST = Math.abs(invCosting.getCstRemainingValue() /
				 * invCosting.getCstRemainingQuantity());
				 * 
				 * } catch (FinderException ex) {
				 * 
				 * COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
				 * 
				 * }
				 * 
				 * double QTY_SLD =
				 * this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure
				 * (), arInvoiceLineItem.getInvItemLocation().getInvItem(),
				 * arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
				 * 
				 * LocalAdBranchItemLocation adBranchItemLocation = null;
				 * 
				 * try {
				 * 
				 * adBranchItemLocation =
				 * adBranchItemLocationHome.findBilByIlCodeAndBrCode(arInvoiceLineItem.
				 * getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
				 * 
				 * } catch(FinderException ex) {
				 * 
				 * }
				 * 
				 * if (arInvoiceLineItem.getIliEnableAutoBuild() == 0 ||
				 * arInvoiceLineItem.getIliEnableAutoBuild() == 1 ||
				 * arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals(
				 * "Stock")) {
				 * 
				 * if(adBranchItemLocation != null) {
				 * 
				 * this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
				 * COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(),
				 * arInvoice, AD_BRNCH, AD_CMPNY);
				 * 
				 * this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY",
				 * EJBCommon.FALSE, COST * QTY_SLD,
				 * adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH,
				 * AD_CMPNY);
				 * 
				 * } else {
				 * 
				 * this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
				 * COST * QTY_SLD,
				 * arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
				 * arInvoice, AD_BRNCH, AD_CMPNY);
				 * 
				 * this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY",
				 * EJBCommon.FALSE, COST * QTY_SLD,
				 * arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(),
				 * arInvoice, AD_BRNCH, AD_CMPNY);
				 * 
				 * }
				 * 
				 * // add quantity to item location committed quantity
				 * 
				 * double convertedQuantity =
				 * this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure
				 * (), arInvoiceLineItem.getInvItemLocation().getInvItem(),
				 * arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
				 * invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity
				 * () + convertedQuantity);
				 * 
				 * }
				 * 
				 * // add inventory sale distributions
				 * 
				 * if(adBranchItemLocation != null) {
				 * 
				 * this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
				 * arInvoiceLineItem.getIliAmount(),
				 * adBranchItemLocation.getBilCoaGlSalesAccount(), arInvoice, AD_BRNCH,
				 * AD_CMPNY);
				 * 
				 * } else {
				 * 
				 * this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
				 * arInvoiceLineItem.getIliAmount(),
				 * arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice,
				 * AD_BRNCH, AD_CMPNY);
				 * 
				 * }
				 * 
				 * TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();
				 * 
				 * }
				 * 
				 * // add tax distribution if necessary
				 * 
				 * if (!arInvoice.getArTaxCode().getTcType().equals("NONE") &&
				 * !arInvoice.getArTaxCode().getTcType().equals("EXEMPT")) {
				 * LocalAdBranchArTaxCode adBranchTaxCode = null;
				 * Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J"); try {
				 * adBranchTaxCode =
				 * adBranchArTaxCodeHome.findBtcByTcCodeAndBrCode(arInvoice.getArTaxCode().
				 * getTcCode(), AD_BRNCH, AD_CMPNY);
				 * 
				 * } catch(FinderException ex) {
				 * 
				 * }
				 * 
				 * if(adBranchTaxCode != null){ this.addArDrEntry(arInvoice.getArDrNextLine(),
				 * "TAX", EJBCommon.FALSE, TOTAL_TAX, adBranchTaxCode.getBtcGlCoaTaxCode(),
				 * EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
				 * 
				 * } else {
				 * 
				 * this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE,
				 * TOTAL_TAX, arInvoice.getArTaxCode().getGlChartOfAccount().getCoaCode(),
				 * EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY); }
				 * 
				 * }
				 * 
				 * // add cash distribution
				 * 
				 * LocalAdBranchCustomer adBranchCustomer = null;
				 * 
				 * try {
				 * 
				 * adBranchCustomer =
				 * adBranchCustomerHome.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().
				 * getCstCode(), AD_BRNCH, AD_CMPNY);
				 * 
				 * } catch(FinderException ex) {
				 * 
				 * }
				 * 
				 * if(adBranchCustomer != null) {
				 * 
				 * this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
				 * arInvoice.getInvAmountDue(),
				 * adBranchCustomer.getBcstGlCoaReceivableAccount(), EJBCommon.FALSE, arInvoice,
				 * AD_BRNCH, AD_CMPNY);
				 * 
				 * } else { this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE",
				 * EJBCommon.TRUE, arInvoice.getInvAmountDue(),
				 * arInvoice.getArCustomer().getCstGlCoaReceivableAccount(), EJBCommon.FALSE,
				 * arInvoice, AD_BRNCH, AD_CMPNY); }
				 * 
				 * 
				 * System.out.println("Add Discount2: " +
				 * arModUploadReceiptDetails.getRctPosDiscount()); if
				 * (arModUploadReceiptDetails.getRctPosDiscount() != 0) { // add discount.
				 * System.out.println("DISCOUNT INVOICE");
				 * this.addArDrEntry(arInvoice.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
				 * arModUploadReceiptDetails.getRctPosDiscount(),
				 * adPreference.getPrfMiscPosDiscountAccount(), EJBCommon.FALSE, arInvoice,
				 * AD_BRNCH, AD_CMPNY); }
				 * 
				 * if (arModUploadReceiptDetails.getRctPosScAmount() != 0) { // add sc amount
				 * this.addArDrEntry(arInvoice.getArDrNextLine(), "SERVCE CHARGE",
				 * EJBCommon.FALSE, arModUploadReceiptDetails.getRctPosScAmount(),
				 * adPreference.getPrfMiscPosServiceChargeAccount(), EJBCommon.FALSE, arInvoice,
				 * AD_BRNCH, AD_CMPNY); }
				 * 
				 * if (arModUploadReceiptDetails.getRctPosDcAmount() != 0) { // add dc amount
				 * this.addArDrEntry(arInvoice.getArDrNextLine(), "DINEIN CHARGE",
				 * EJBCommon.FALSE, arModUploadReceiptDetails.getRctPosDcAmount(),
				 * adPreference.getPrfMiscPosDineInChargeAccount(), EJBCommon.FALSE, arInvoice,
				 * AD_BRNCH, AD_CMPNY); }
				 * 
				 * 
				 * // add forex gain/loss
				 * 
				 * double forexGainLoss = TOTAL_LINE - (arInvoice.getInvAmountDue() +
				 * arModUploadReceiptDetails.getRctPosDiscount() -
				 * arModUploadReceiptDetails.getRctPosScAmount() -
				 * arModUploadReceiptDetails.getRctPosDcAmount()); if (forexGainLoss != 0) {
				 * this.addArDrEntry(arInvoice.getArDrNextLine(), "FOREX", forexGainLoss > 0 ?
				 * EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(forexGainLoss),
				 * adPreference.getPrfMiscPosGiftCertificateAccount(), EJBCommon.FALSE,
				 * arInvoice, AD_BRNCH, AD_CMPNY); }
				 * 
				 * }
				 */
				System.out.println("gate 4 part 1");
				success = 1;
				return success;

			} else {
				System.out.println("gate 4 part 2");
				System.out.println("NULL ACCOUNTs");
				System.out.println(success);
				return success;

			}

		} catch (Exception ex) {

			System.out.println("gate 4 err");
			ex.printStackTrace();
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method
	 **/
	public int setArMiscReceiptAllNewAndVoidWithExpiryDateEnableItemAutoBuild(String[] newRR, String[] voidRR,
			String BR_BRNCH_CODE, Integer AD_CMPNY, String CASHIER) {

		Debug.print("ArMiscReceiptSyncControllerBean setInvMiscReceiptAllNewAndVoid");

		LocalAdBranchHome adBranchHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalArReceiptBatchHome arReceiptBatchHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoiceBatchHome arInvoiceBatchHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalAdBranchCustomerHome adBranchCustomerHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		try {

			adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
					LocalAdBranchHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			arReceiptHome = (LocalArReceiptHome) EJBHomeFactory.lookUpLocalHome(LocalArReceiptHome.JNDI_NAME,
					LocalArReceiptHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
					LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
							LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory.lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME,
					LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory
					.lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			arCustomerHome = (LocalArCustomerHome) EJBHomeFactory.lookUpLocalHome(LocalArCustomerHome.JNDI_NAME,
					LocalArCustomerHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			arReceiptBatchHome = (LocalArReceiptBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory.lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME,
					LocalArInvoiceHome.class);
			arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome) EJBHomeFactory
					.lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			invLocationHome = (LocalInvLocationHome) EJBHomeFactory.lookUpLocalHome(LocalInvLocationHome.JNDI_NAME,
					LocalInvLocationHome.class);
			adBranchCustomerHome = (LocalAdBranchCustomerHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
					LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			Integer AD_BRNCH = adBranch.getBrCode();

			int success = 0;

			if (adPreference.getPrfMiscPosDiscountAccount() != null
					&& adPreference.getPrfMiscPosGiftCertificateAccount() != null
					&& adPreference.getPrfMiscPosServiceChargeAccount() != null
					&& adPreference.getPrfMiscPosDineInChargeAccount() != null) {

				// new receipts

				HashMap receiptNumbers = new HashMap();
				HashMap invoiceNumbers = new HashMap();
				String fundTransferNumber = null;
				String firstNumber = "";
				String lastNumber = "";

				for (int i = 0; i < newRR.length; i++) {

					ArModReceiptDetails arModReceiptDetails = receiptDecodeWithExpiryDate(newRR[i]);

					if (i == 0)
						firstNumber = arModReceiptDetails.getRctNumber();

					if (i == newRR.length - 1)
						lastNumber = arModReceiptDetails.getRctNumber();

					String reference_number = arModReceiptDetails.getRctNumber();

					if (arModReceiptDetails.getRctPosOnAccount() == 0) {

						// generate receipt number

						String generatedReceipt = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						LocalArReceipt arExistingReceipt = null;

						Iterator rctIter = receiptNumbers.values().iterator();
						while (rctIter.hasNext()) {

							try {

								ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) rctIter.next();

								arExistingReceipt = arReceiptHome.findByRctDateAndRctNumberAndCstCustomerCodeAndBrCode(
										arModReceiptDetails.getRctDate(), arModUploadReceiptDetails.getRctNumber(),
										arModReceiptDetails.getRctCstCustomerCode(), AD_BRNCH, AD_CMPNY);

								break;

							} catch (FinderException ex) {

								continue;

							}

						}

						if (arExistingReceipt == null) {

							ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
							arModUploadReceiptDetails.setRctNumber(generatedReceipt);
							arModUploadReceiptDetails
									.setRctPosDiscount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));

							receiptNumbers.put(generatedReceipt, arModUploadReceiptDetails);

							System.out.println(arModReceiptDetails.getRctConversionRate()
									+ " <<CONV RATE:MISC:POS DISC>> " + arModReceiptDetails.getRctPosDiscount());

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);

							LocalArReceipt arReceipt = arReceiptHome.create("MISC", "POS Sales " + new Date(),
									arModReceiptDetails.getRctDate(), generatedReceipt, null, null, null, null, null,
									null, null, null, totalAmount, 0d, 0d, 0d, 0d, 0d, 0d, null, 1, null, "CASH",
									EJBCommon.FALSE, 0, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE,
									EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
									EJBCommon.FALSE, null, null, null, null, null, CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
									EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null, null,
									EJBCommon.FALSE, EJBCommon.FALSE, null, AD_BRNCH, AD_CMPNY);

							arReceipt.setRctReferenceNumber(reference_number);
							LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
									.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
							// glFunctionalCurrency.addArReceipt(arReceipt);
							arReceipt.setGlFunctionalCurrency(glFunctionalCurrency);

							LocalArCustomer arCustomer = arCustomerHome
									.findByCstCustomerCode(arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
							// arCustomer.addArReceipt(arReceipt);
							arReceipt.setArCustomer(arCustomer);

							String bankAccount = "";
							if (BR_BRNCH_CODE.equals("Manila-Smoking Lounge")) {
								bankAccount = "Allied Bank-IPT Smoking";
							} else if (BR_BRNCH_CODE.equals("Manila-Cigar Shop")) {
								bankAccount = "Allied Bank Cigar shop";
							} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Domestic")) {
								bankAccount = "Terminal II Domestic";
							} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Intl")) {
								bankAccount = "Term2 International";
							} else if (BR_BRNCH_CODE.equals("Cebu-Banilad")) {
								bankAccount = "Metrobank Banilad";
							} else if (BR_BRNCH_CODE.equals("Cebu-Gorordo")) {
								bankAccount = "Metrobank-Gorordo";
							} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Domestic")) {
								bankAccount = "Metrobank Mactan Domestic";
							} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Intl")) {
								bankAccount = "Metrobank I Mactan Int'l";
							} else if (BR_BRNCH_CODE.equals("Cebu-Supercat")) {
								bankAccount = "Metrobank Supercat";
							} else {
								bankAccount = arCustomer.getAdBankAccount().getBaName();
							}

							LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(bankAccount, AD_CMPNY);
							// adBankAccount.addArReceipt(arReceipt);
							arReceipt.setAdBankAccount(adBankAccount);

							if (adCompany.getCmpShortName().toLowerCase().equals("vertext")) {
								if (BR_BRNCH_CODE.equals("HO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Main", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CENTRO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Centro", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROBINSON")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Robinson",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROUTA")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Routa", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CAPITOL")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Capitol",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);

								}

							} else {
								LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
								// arTaxCode.addArReceipt(arReceipt);
								arReceipt.setArTaxCode(arTaxCode);

							}

							LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome
									.findByWtcName("NONE", AD_CMPNY);
							// arWithholdingTaxCode.addArReceipt(arReceipt);
							arReceipt.setArWithholdingTaxCode(arWithholdingTaxCode);

							LocalArReceiptBatch arReceiptBatch = null;
							try {
								arReceiptBatch = arReceiptBatchHome.findByRbName("POS BATCH", AD_BRNCH, AD_CMPNY);
							} catch (FinderException ex) {
								arReceiptBatch = arReceiptBatchHome.create("POS BATCH", "POS BATCH", "OPEN", "MISC",
										EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);

							}
							// arReceiptBatch.addArReceipt(arReceipt);
							arReceipt.setArReceiptBatch(arReceiptBatch);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();
							short lineNumber = 0;
							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(++lineNumber,
										arModInvoiceLineItemDetails.getIliQuantity(),
										arModInvoiceLineItemDetails.getIliUnitPrice(),
										EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
												/ (1 + arReceipt.getArTaxCode().getTcRate() / 100), (short) 2),
										EJBCommon.roundIt(
												arModInvoiceLineItemDetails.getIliAmount()
														- (arModInvoiceLineItemDetails.getIliAmount()
																/ (1 + arReceipt.getArTaxCode().getTcRate() / 100)),
												(short) 2),
										EJBCommon.TRUE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

								// arReceipt.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setArReceipt(arReceipt);

								LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
										.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);
								arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
								System.out.println("arModInvoiceLineItemDetails.getIliIiName: "
										+ arModInvoiceLineItemDetails.getIliIiName());
								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}

								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);
								// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvItemLocation(invItemLocation);

								if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

									arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

								}
							}

						} else {

							ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) receiptNumbers
									.get(arExistingReceipt.getRctNumber());

							arModUploadReceiptDetails
									.setRctPosDiscount(
											arModUploadReceiptDetails.getRctPosDiscount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDiscount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(
											arModUploadReceiptDetails.getRctPosScAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosScAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(
											arModUploadReceiptDetails.getRctPosDcAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDcAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							receiptNumbers.put(arExistingReceipt.getRctNumber(), arModUploadReceiptDetails);
							System.out.println(arModReceiptDetails.getRctConversionRate()
									+ " <<CONV RATE:MISC2:POS DISC>> " + arModReceiptDetails.getRctPosDiscount());

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);
							arExistingReceipt.setRctAmount(arExistingReceipt.getRctAmount() + totalAmount);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();

							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								System.out.println(arModInvoiceLineItemDetails.getIliIiName());
								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);

								LocalArInvoiceLineItem arExistingInvoiceLineItem = null;

								try {
									arExistingInvoiceLineItem = arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(
											arExistingReceipt.getRctCode(), invItemLocation.getIlCode(),
											arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								} catch (FinderException ex) {

								}

								if (arExistingInvoiceLineItem == null) {
									System.out.println(arModInvoiceLineItemDetails.getIliIiName());
									LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
											(short) (arExistingReceipt.getArInvoiceLineItems().size() + 1),
											arModInvoiceLineItemDetails.getIliQuantity(),
											arModInvoiceLineItemDetails.getIliUnitPrice(),
											EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100),
													(short) 2),
											EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100)),
													(short) 2),
											EJBCommon.TRUE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

									// arExistingReceipt.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setArReceipt(arExistingReceipt);

									LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
											.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
									// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

									// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvItemLocation(invItemLocation);
									arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
									if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

										arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

									}
								} else {

									arExistingInvoiceLineItem.setIliQuantity(arExistingInvoiceLineItem.getIliQuantity()
											+ arModInvoiceLineItemDetails.getIliQuantity());
									arExistingInvoiceLineItem
											.setIliUnitPrice((arExistingInvoiceLineItem.getIliUnitPrice()
													+ arModInvoiceLineItemDetails.getIliUnitPrice()) / 2);
									arExistingInvoiceLineItem
											.setIliAmount(arExistingInvoiceLineItem.getIliAmount() + EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100),
													(short) 2));
									arExistingInvoiceLineItem.setIliTaxAmount(arExistingInvoiceLineItem
											.getIliTaxAmount()
											+ EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100)),
													(short) 2));

								}
							}
						}

					} else {

						// generate Invoice number

						String generatedInvoice = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR INVOICE",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						LocalArInvoice arExistingInvoice = null;

						Iterator invIter = invoiceNumbers.values().iterator();
						while (invIter.hasNext()) {

							try {

								ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invIter.next();

								arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndCstCustomerCode(
										arModUploadReceiptDetails.getRctNumber(), (byte) 0,
										arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);

								break;

							} catch (FinderException ex) {

								continue;

							}

						}

						if (arExistingInvoice == null) {

							ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
							arModUploadReceiptDetails.setRctNumber(generatedInvoice);
							arModUploadReceiptDetails
									.setRctPosDiscount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							invoiceNumbers.put(generatedInvoice, arModUploadReceiptDetails);
							System.out.println(arModReceiptDetails.getRctConversionRate()
									+ " <<CONV RATE:INVC:POS DISC>> " + arModReceiptDetails.getRctPosDiscount());

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);

							LocalArInvoice arInvoice = arInvoiceHome.create("ITEMS", (byte) 0,
									"POS Sales " + new Date().toString(), arModReceiptDetails.getRctDate(),
									generatedInvoice, null, null, null, null, totalAmount, 0d, 0d, 0d, 0d, 0d, null, 1,
									null, 0d, 0d, null, null, null, null, null, null, null, null, null, null, null,
									null, null, null, null, null, null, null, null, null, EJBCommon.FALSE, null,
									EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
									null, 0d, null, null, null, null, CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
									EJBCommon.FALSE, null, null, null, (byte) 0, (byte) 0, null,
									arModReceiptDetails.getRctDate(), AD_BRNCH, AD_CMPNY);
							arInvoice.setInvReferenceNumber(reference_number);
							LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName("IMMEDIATE", AD_CMPNY);
							// adPaymentTerm.addArInvoice(arInvoice);
							arInvoice.setAdPaymentTerm(adPaymentTerm);

							LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
									.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
							// glFunctionalCurrency.addArInvoice(arInvoice);
							arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

							LocalArCustomer arCustomer = arCustomerHome
									.findByCstCustomerCode(arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
							// arCustomer.addArInvoice(arInvoice);
							arInvoice.setArCustomer(arCustomer);

							if (adCompany.getCmpShortName().toLowerCase().equals("vertext")) {
								if (BR_BRNCH_CODE.equals("HO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Main", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CENTRO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Centro", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROBINSON")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Robinson",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROUTA")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Routa", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CAPITOL")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Capitol",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);

								}

							} else {
								LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
								// arTaxCode.addArReceipt(arReceipt);
								arInvoice.setArTaxCode(arTaxCode);

							}
							/*
							 * LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE",
							 * AD_CMPNY); //arTaxCode.addArInvoice(arInvoice);
							 * arInvoice.setArTaxCode(arTaxCode);
							 */

							LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome
									.findByWtcName("NONE", AD_CMPNY);
							// arWithholdingTaxCode.addArInvoice(arInvoice);
							arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);

							LocalArInvoiceBatch arInvoiceBatch = null;
							try {
								arInvoiceBatch = arInvoiceBatchHome.findByIbName("POS BATCH", AD_BRNCH, AD_CMPNY);
							} catch (FinderException ex) {
								arInvoiceBatch = arInvoiceBatchHome.create("POS BATCH", "POS BATCH", "OPEN", "MISC",
										EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);

							}
							// arInvoiceBatch.addArInvoice(arInvoice);
							arInvoice.setArInvoiceBatch(arInvoiceBatch);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();
							short lineNumber = 0;
							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(++lineNumber,
										arModInvoiceLineItemDetails.getIliQuantity(),
										arModInvoiceLineItemDetails.getIliUnitPrice(),
										EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
												/ (1 + arInvoice.getArTaxCode().getTcRate() / 100), (short) 2),
										EJBCommon.roundIt(
												arModInvoiceLineItemDetails.getIliAmount()
														- (arModInvoiceLineItemDetails.getIliAmount()
																/ (1 + arInvoice.getArTaxCode().getTcRate() / 100)),
												(short) 2),
										EJBCommon.TRUE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

								// arInvoice.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setArInvoice(arInvoice);

								LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
										.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

								arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());

								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);
								// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvItemLocation(invItemLocation);
								if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

									arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

								}
							}

						} else {

							ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invoiceNumbers
									.get(arExistingInvoice.getInvNumber());

							arModUploadReceiptDetails
									.setRctPosDiscount(
											arModUploadReceiptDetails.getRctPosDiscount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDiscount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(
											arModUploadReceiptDetails.getRctPosScAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosScAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(
											arModUploadReceiptDetails.getRctPosDcAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDcAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							invoiceNumbers.put(arExistingInvoice.getInvNumber(), arModUploadReceiptDetails);
							System.out.println(arModReceiptDetails.getRctConversionRate()
									+ " <<CONV RATE:INVC:POS DISC>> " + arModReceiptDetails.getRctPosDiscount());

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);
							arExistingInvoice.setInvAmountDue(arExistingInvoice.getInvAmountDue() + totalAmount);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();

							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();

								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);

								LocalArInvoiceLineItem arExistingInvoiceLineItem = null;

								try {
									arExistingInvoiceLineItem = arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(
											arExistingInvoice.getInvCode(), invItemLocation.getIlCode(),
											arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								} catch (FinderException ex) {

								}

								if (arExistingInvoiceLineItem == null) {

									LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
											(short) (arExistingInvoice.getArInvoiceLineItems().size() + 1),
											arModInvoiceLineItemDetails.getIliQuantity(),
											arModInvoiceLineItemDetails.getIliUnitPrice(),
											EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100),
													(short) 2),
											EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100)),
													(short) 2),
											EJBCommon.TRUE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

									// arExistingInvoice.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setArInvoice(arExistingInvoice);

									LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
											.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
									// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);
									arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
									// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvItemLocation(invItemLocation);
									if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

										arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

									}
								} else {

									arExistingInvoiceLineItem.setIliQuantity(arExistingInvoiceLineItem.getIliQuantity()
											+ arModInvoiceLineItemDetails.getIliQuantity());
									arExistingInvoiceLineItem
											.setIliUnitPrice((arExistingInvoiceLineItem.getIliUnitPrice()
													+ arModInvoiceLineItemDetails.getIliUnitPrice()) / 2);
									arExistingInvoiceLineItem
											.setIliAmount(arExistingInvoiceLineItem.getIliAmount() + EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100),
													(short) 2));
									arExistingInvoiceLineItem.setIliTaxAmount(arExistingInvoiceLineItem
											.getIliTaxAmount()
											+ EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100)),
													(short) 2));

								}
							}
						}

					}

				}

				// for each receipt generate distribution records

				Iterator rctIter = receiptNumbers.values().iterator();

				while (rctIter.hasNext()) {

					ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) rctIter.next();

					LocalArReceipt arReceipt = arReceiptHome
							.findByRctNumberAndBrCode(arModUploadReceiptDetails.getRctNumber(), AD_BRNCH, AD_CMPNY);
					arReceipt.setRctReferenceNumber(arModUploadReceiptDetails.getRctReferenceNumber());

					Iterator lineIter = arReceipt.getArInvoiceLineItems().iterator();

					double TOTAL_TAX = 0;
					double TOTAL_LINE = 0;

					while (lineIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) lineIter.next();

						TOTAL_LINE += arInvoiceLineItem.getIliTaxAmount() + arInvoiceLineItem.getIliAmount();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
											invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalAdBranchItemLocation adBranchItemLocation = null;

						try {

							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (arInvoiceLineItem.getIliEnableAutoBuild() == 0
								|| arInvoiceLineItem.getIliEnableAutoBuild() == 1
								|| arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

							if (adBranchItemLocation != null) {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							} else {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
										arReceipt, AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							}

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(
									invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
									arReceipt, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arReceipt,
									AD_BRNCH, AD_CMPNY);

						}

						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

					}

					// add tax distribution if necessary

					if (!arReceipt.getArTaxCode().getTcType().equals("NONE")
							&& !arReceipt.getArTaxCode().getTcType().equals("EXEMPT")) {

						this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
								arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE, arReceipt,
								AD_BRNCH, AD_CMPNY);

					}

					// add cash distribution

					this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE, arReceipt.getRctAmount(),
							arReceipt.getAdBankAccount().getBaCoaGlCashAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
							AD_CMPNY);
					System.out.println("Add Discount: " + arModUploadReceiptDetails.getRctPosDiscount());
					if (arModUploadReceiptDetails.getRctPosDiscount() != 0) {
						// add discount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
								arModUploadReceiptDetails.getRctPosDiscount(),
								adPreference.getPrfMiscPosDiscountAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosScAmount() != 0) {
						// add sc amount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "SERVCE CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosScAmount(),
								adPreference.getPrfMiscPosServiceChargeAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDcAmount() != 0) {
						// add dc amount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "DINEIN CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosDcAmount(),
								adPreference.getPrfMiscPosDineInChargeAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					// add forex gain/loss

					double forexGainLoss = TOTAL_LINE
							- (arReceipt.getRctAmount() + arModUploadReceiptDetails.getRctPosDiscount()
									- arModUploadReceiptDetails.getRctPosScAmount()
									- arModUploadReceiptDetails.getRctPosDcAmount());
					if (forexGainLoss != 0) {
						this.addArDrEntry(arReceipt.getArDrNextLine(), "FOREX",
								forexGainLoss > 0 ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(forexGainLoss),
								adPreference.getPrfMiscPosGiftCertificateAccount(), EJBCommon.FALSE, arReceipt,
								AD_BRNCH, AD_CMPNY);
					}

				}

				Iterator invIter = invoiceNumbers.values().iterator();

				while (invIter.hasNext()) {

					ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invIter.next();

					LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
							arModUploadReceiptDetails.getRctNumber(), (byte) 0, AD_BRNCH, AD_CMPNY);
					// arInvoice.setInvReferenceNumber(firstNumber + "-" + lastNumber);

					short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
					double TOTAL_PAYMENT_SCHEDULE = 0d;

					GregorianCalendar gcPrevDateDue = new GregorianCalendar();
					GregorianCalendar gcDateDue = new GregorianCalendar();
					gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

					Collection adPaymentSchedules = arInvoice.getAdPaymentTerm().getAdPaymentSchedules();

					Iterator i = adPaymentSchedules.iterator();

					while (i.hasNext()) {

						LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule) i.next();

						// get date due

						if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")) {

							gcDateDue.setTime(arInvoice.getInvEffectivityDate());
							gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

						} else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")) {

							gcDateDue = gcPrevDateDue;
							gcDateDue.add(Calendar.MONTH, 1);
							gcPrevDateDue = gcDateDue;

						} else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

							gcDateDue = gcPrevDateDue;

							if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
								if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31
										&& gcPrevDateDue.get(Calendar.DATE) > 15
										&& gcPrevDateDue.get(Calendar.DATE) < 31) {
									gcDateDue.add(Calendar.DATE, 16);
								} else {
									gcDateDue.add(Calendar.DATE, 15);
								}
							} else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
								if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
										&& gcPrevDateDue.get(Calendar.DATE) == 14) {
									gcDateDue.add(Calendar.DATE, 14);
								} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
										&& gcPrevDateDue.get(Calendar.DATE) >= 15
										&& gcPrevDateDue.get(Calendar.DATE) < 28) {
									gcDateDue.add(Calendar.DATE, 13);
								} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29
										&& gcPrevDateDue.get(Calendar.DATE) >= 15
										&& gcPrevDateDue.get(Calendar.DATE) < 29) {
									gcDateDue.add(Calendar.DATE, 14);
								} else {
									gcDateDue.add(Calendar.DATE, 15);
								}
							}

							gcPrevDateDue = gcDateDue;

						}

						// create a payment schedule

						double PAYMENT_SCHEDULE_AMOUNT = 0;

						// if last payment schedule subtract to avoid rounding difference error

						if (i.hasNext()) {

							PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount()
									/ arInvoice.getAdPaymentTerm().getPytBaseAmount()) * arInvoice.getInvAmountDue(),
									precisionUnit);

						} else {

							PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_PAYMENT_SCHEDULE;

						}

						LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = arInvoicePaymentScheduleHome.create(
								gcDateDue.getTime(), adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT, 0d,
								EJBCommon.FALSE, (short) 0, gcDateDue.getTime(), 0d, 0d, AD_CMPNY);

						// arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);
						arInvoicePaymentSchedule.setArInvoice(arInvoice);

						TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;

					}

					Iterator lineIter = arInvoice.getArInvoiceLineItems().iterator();

					double TOTAL_TAX = 0;
					double TOTAL_LINE = 0;

					while (lineIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) lineIter.next();

						TOTAL_LINE += arInvoiceLineItem.getIliTaxAmount() + arInvoiceLineItem.getIliAmount();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
											invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalAdBranchItemLocation adBranchItemLocation = null;

						try {

							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (arInvoiceLineItem.getIliEnableAutoBuild() == 0
								|| arInvoiceLineItem.getIliEnableAutoBuild() == 1
								|| arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

							if (adBranchItemLocation != null) {

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

							} else {

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
										arInvoice, AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

							}

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(
									invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
									arInvoice, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice,
									AD_BRNCH, AD_CMPNY);

						}

						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

					}

					// add tax distribution if necessary

					if (!arInvoice.getArTaxCode().getTcType().equals("NONE")
							&& !arInvoice.getArTaxCode().getTcType().equals("EXEMPT")) {

						this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
								arInvoice.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE, arInvoice,
								AD_BRNCH, AD_CMPNY);

					}

					// add cash distribution

					LocalAdBranchCustomer adBranchCustomer = null;

					try {

						adBranchCustomer = adBranchCustomerHome
								.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if (adBranchCustomer != null) {

						this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
								arInvoice.getInvAmountDue(), adBranchCustomer.getBcstGlCoaReceivableAccount(),
								EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);

					} else {
						this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
								arInvoice.getInvAmountDue(), arInvoice.getArCustomer().getCstGlCoaReceivableAccount(),
								EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
					}

					System.out.println("Add Discount2: " + arModUploadReceiptDetails.getRctPosDiscount());
					if (arModUploadReceiptDetails.getRctPosDiscount() != 0) {
						// add discount.
						System.out.println("DISCOUNT INVOICE");
						this.addArDrEntry(arInvoice.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
								arModUploadReceiptDetails.getRctPosDiscount(),
								adPreference.getPrfMiscPosDiscountAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosScAmount() != 0) {
						// add sc amount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "SERVCE CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosScAmount(),
								adPreference.getPrfMiscPosServiceChargeAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDcAmount() != 0) {
						// add dc amount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "DINEIN CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosDcAmount(),
								adPreference.getPrfMiscPosDineInChargeAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					// add forex gain/loss

					double forexGainLoss = TOTAL_LINE
							- (arInvoice.getInvAmountDue() + arModUploadReceiptDetails.getRctPosDiscount()
									- arModUploadReceiptDetails.getRctPosScAmount()
									- arModUploadReceiptDetails.getRctPosDcAmount());
					if (forexGainLoss != 0) {
						this.addArDrEntry(arInvoice.getArDrNextLine(), "FOREX",
								forexGainLoss > 0 ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(forexGainLoss),
								adPreference.getPrfMiscPosGiftCertificateAccount(), EJBCommon.FALSE, arInvoice,
								AD_BRNCH, AD_CMPNY);
					}

				}

				success = 1;
				return success;
			} else {

				System.out.println("NULL ACCOUNTs");
				System.out.println(success);
				return success;

			}

		} catch (Exception ex) {

			ex.printStackTrace();
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method
	 **/
	public int setArMiscReceiptAllNewAndVoidWithExpiryDateAndEntries(String[] newRR, String[] voidRR,
			String BR_BRNCH_CODE, Integer AD_CMPNY, String CASHIER) {

		Debug.print("ArMiscReceiptSyncControllerBean setInvMiscReceiptAllNewAndVoid");

		LocalAdBranchHome adBranchHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalArReceiptBatchHome arReceiptBatchHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoiceBatchHome arInvoiceBatchHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalAdBranchCustomerHome adBranchCustomerHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		try {

			adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
					LocalAdBranchHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			arReceiptHome = (LocalArReceiptHome) EJBHomeFactory.lookUpLocalHome(LocalArReceiptHome.JNDI_NAME,
					LocalArReceiptHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
					LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
							LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory.lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME,
					LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory
					.lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			arCustomerHome = (LocalArCustomerHome) EJBHomeFactory.lookUpLocalHome(LocalArCustomerHome.JNDI_NAME,
					LocalArCustomerHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			arReceiptBatchHome = (LocalArReceiptBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory.lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME,
					LocalArInvoiceHome.class);
			arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome) EJBHomeFactory
					.lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			invLocationHome = (LocalInvLocationHome) EJBHomeFactory.lookUpLocalHome(LocalInvLocationHome.JNDI_NAME,
					LocalInvLocationHome.class);
			adBranchCustomerHome = (LocalAdBranchCustomerHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
					LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			Integer AD_BRNCH = adBranch.getBrCode();

			int success = 0;

			if (adPreference.getPrfMiscPosDiscountAccount() != null
					&& adPreference.getPrfMiscPosGiftCertificateAccount() != null
					&& adPreference.getPrfMiscPosServiceChargeAccount() != null
					&& adPreference.getPrfMiscPosDineInChargeAccount() != null) {

				// new receipts

				HashMap receiptNumbers = new HashMap();
				HashMap invoiceNumbers = new HashMap();
				String fundTransferNumber = null;
				String firstNumber = "";
				String lastNumber = "";

				for (int i = 0; i < newRR.length; i++) {

					ArModReceiptDetails arModReceiptDetails = receiptDecodeWithExpiryDateAndEntries(newRR[i]);

					if (i == 0)
						firstNumber = arModReceiptDetails.getRctNumber();
					if (i == newRR.length - 1)
						lastNumber = arModReceiptDetails.getRctNumber();

					if (arModReceiptDetails.getRctPosOnAccount() == 0) {

						// generate receipt number

						String generatedReceipt = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						LocalArReceipt arExistingReceipt = null;

						Iterator rctIter = receiptNumbers.values().iterator();
						while (rctIter.hasNext()) {

							try {

								ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) rctIter.next();

								arExistingReceipt = arReceiptHome.findByRctDateAndRctNumberAndCstCustomerCodeAndBrCode(
										arModReceiptDetails.getRctDate(), arModUploadReceiptDetails.getRctNumber(),
										arModReceiptDetails.getRctCstCustomerCode(), AD_BRNCH, AD_CMPNY);

								break;

							} catch (FinderException ex) {

								continue;

							}

						}

						if (arExistingReceipt == null) {

							ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
							arModUploadReceiptDetails.setRctNumber(generatedReceipt);
							arModUploadReceiptDetails
									.setRctPosDiscount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							receiptNumbers.put(generatedReceipt, arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);

							LocalArReceipt arReceipt = arReceiptHome.create("MISC", "POS Sales " + new Date(),
									arModReceiptDetails.getRctDate(), generatedReceipt, null, null, null, null, null,
									null, null, null, totalAmount, 0d, 0d, 0d, 0d, 0d, 0d, null, 1, null, "CASH",
									EJBCommon.FALSE, 0, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE,
									EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
									EJBCommon.FALSE, null, null, null, null, null, CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
									EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null, null,
									EJBCommon.FALSE, EJBCommon.FALSE, null, AD_BRNCH, AD_CMPNY);

							LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
									.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
							// glFunctionalCurrency.addArReceipt(arReceipt);
							arReceipt.setGlFunctionalCurrency(glFunctionalCurrency);

							LocalArCustomer arCustomer = arCustomerHome
									.findByCstCustomerCode(arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
							// arCustomer.addArReceipt(arReceipt);
							arReceipt.setArCustomer(arCustomer);

							String bankAccount = "";
							if (BR_BRNCH_CODE.equals("Manila-Smoking Lounge")) {
								bankAccount = "Allied Bank-IPT Smoking";
							} else if (BR_BRNCH_CODE.equals("Manila-Cigar Shop")) {
								bankAccount = "Allied Bank Cigar shop";
							} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Domestic")) {
								bankAccount = "Terminal II Domestic";
							} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Intl")) {
								bankAccount = "Term2 International";
							} else if (BR_BRNCH_CODE.equals("Cebu-Banilad")) {
								bankAccount = "Metrobank Banilad";
							} else if (BR_BRNCH_CODE.equals("Cebu-Gorordo")) {
								bankAccount = "Metrobank-Gorordo";
							} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Domestic")) {
								bankAccount = "Metrobank Mactan Domestic";
							} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Intl")) {
								bankAccount = "Metrobank I Mactan Int'l";
							} else if (BR_BRNCH_CODE.equals("Cebu-Supercat")) {
								bankAccount = "Metrobank Supercat";
							} else {
								bankAccount = arCustomer.getAdBankAccount().getBaName();
							}

							LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(bankAccount, AD_CMPNY);
							// adBankAccount.addArReceipt(arReceipt);
							arReceipt.setAdBankAccount(adBankAccount);

							if (adCompany.getCmpShortName().toLowerCase().equals("vertext")) {
								if (BR_BRNCH_CODE.equals("HO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Main", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CENTRO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Centro", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROBINSON")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Robinson",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROUTA")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Routa", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CAPITOL")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Capitol",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);

								}

							} else {
								LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
								// arTaxCode.addArReceipt(arReceipt);
								arReceipt.setArTaxCode(arTaxCode);

							}

							/*
							 * LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE",
							 * AD_CMPNY); //arTaxCode.addArReceipt(arReceipt);
							 * arReceipt.setArTaxCode(arTaxCode);
							 */

							LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome
									.findByWtcName("NONE", AD_CMPNY);
							// arWithholdingTaxCode.addArReceipt(arReceipt);
							arReceipt.setArWithholdingTaxCode(arWithholdingTaxCode);

							LocalArReceiptBatch arReceiptBatch = null;
							try {
								arReceiptBatch = arReceiptBatchHome.findByRbName("POS BATCH", AD_BRNCH, AD_CMPNY);
							} catch (FinderException ex) {
								arReceiptBatch = arReceiptBatchHome.create("POS BATCH", "POS BATCH", "OPEN", "MISC",
										EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);

							}
							// arReceiptBatch.addArReceipt(arReceipt);
							arReceipt.setArReceiptBatch(arReceiptBatch);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();
							short lineNumber = 0;
							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(++lineNumber,
										arModInvoiceLineItemDetails.getIliQuantity(),
										arModInvoiceLineItemDetails.getIliUnitPrice(),
										EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
												/ (1 + arReceipt.getArTaxCode().getTcRate() / 100), (short) 2),
										EJBCommon.roundIt(
												arModInvoiceLineItemDetails.getIliAmount()
														- (arModInvoiceLineItemDetails.getIliAmount()
																/ (1 + arReceipt.getArTaxCode().getTcRate() / 100)),
												(short) 2),
										EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

								// arReceipt.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setArReceipt(arReceipt);

								LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
										.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);
								arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}

								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);
								// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvItemLocation(invItemLocation);

								if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

									arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

								}
							}

						} else {

							ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) receiptNumbers
									.get(arExistingReceipt.getRctNumber());

							arModUploadReceiptDetails
									.setRctPosDiscount(
											arModUploadReceiptDetails.getRctPosDiscount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDiscount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(
											arModUploadReceiptDetails.getRctPosScAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosScAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(
											arModUploadReceiptDetails.getRctPosDcAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDcAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							receiptNumbers.put(arExistingReceipt.getRctNumber(), arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);
							arExistingReceipt.setRctAmount(arExistingReceipt.getRctAmount() + totalAmount);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();

							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								System.out.println(arModInvoiceLineItemDetails.getIliIiName());
								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);

								LocalArInvoiceLineItem arExistingInvoiceLineItem = null;

								try {
									arExistingInvoiceLineItem = arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(
											arExistingReceipt.getRctCode(), invItemLocation.getIlCode(),
											arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								} catch (FinderException ex) {

								}

								if (arExistingInvoiceLineItem == null) {
									System.out.println(arModInvoiceLineItemDetails.getIliIiName());
									LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
											(short) (arExistingReceipt.getArInvoiceLineItems().size() + 1),
											arModInvoiceLineItemDetails.getIliQuantity(),
											arModInvoiceLineItemDetails.getIliUnitPrice(),
											EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100),
													(short) 2),
											EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100)),
													(short) 2),
											EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

									// arExistingReceipt.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setArReceipt(arExistingReceipt);

									LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
											.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
									// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

									// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvItemLocation(invItemLocation);
									arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
									if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

										arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

									}
								} else {

									arExistingInvoiceLineItem.setIliQuantity(arExistingInvoiceLineItem.getIliQuantity()
											+ arModInvoiceLineItemDetails.getIliQuantity());
									arExistingInvoiceLineItem
											.setIliUnitPrice((arExistingInvoiceLineItem.getIliUnitPrice()
													+ arModInvoiceLineItemDetails.getIliUnitPrice()) / 2);
									arExistingInvoiceLineItem
											.setIliAmount(arExistingInvoiceLineItem.getIliAmount() + EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100),
													(short) 2));
									arExistingInvoiceLineItem.setIliTaxAmount(arExistingInvoiceLineItem
											.getIliTaxAmount()
											+ EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100)),
													(short) 2));

								}
							}
						}

					} else {

						// generate Invoice number

						String generatedInvoice = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR INVOICE",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						LocalArInvoice arExistingInvoice = null;

						Iterator invIter = invoiceNumbers.values().iterator();
						while (invIter.hasNext()) {

							try {

								ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invIter.next();

								arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndCstCustomerCode(
										arModUploadReceiptDetails.getRctNumber(), (byte) 0,
										arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);

								break;

							} catch (FinderException ex) {

								continue;

							}

						}

						if (arExistingInvoice == null) {

							ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
							arModUploadReceiptDetails.setRctNumber(generatedInvoice);
							arModUploadReceiptDetails
									.setRctPosDiscount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							invoiceNumbers.put(generatedInvoice, arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);

							LocalArInvoice arInvoice = arInvoiceHome.create("ITEMS", (byte) 0,
									"POS Sales " + new Date().toString(), arModReceiptDetails.getRctDate(),
									generatedInvoice, null, null, null, null, totalAmount, 0d, 0d, 0d, 0d, 0d, null, 1,
									null, 0d, 0d, null, null, null, null, null, null, null, null, null, null, null,
									null, null, null, null, null, null, null, null, null, EJBCommon.FALSE, null,
									EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
									null, 0d, null, null, null, null, CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
									EJBCommon.FALSE, null, null, null, (byte) 0, (byte) 0, null,
									arModReceiptDetails.getRctDate(), AD_BRNCH, AD_CMPNY);

							LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName("IMMEDIATE", AD_CMPNY);
							// adPaymentTerm.addArInvoice(arInvoice);
							arInvoice.setAdPaymentTerm(adPaymentTerm);

							LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
									.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
							// glFunctionalCurrency.addArInvoice(arInvoice);
							arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

							LocalArCustomer arCustomer = arCustomerHome
									.findByCstCustomerCode(arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
							// arCustomer.addArInvoice(arInvoice);
							arInvoice.setArCustomer(arCustomer);

							if (adCompany.getCmpShortName().toLowerCase().equals("vertext")) {
								if (BR_BRNCH_CODE.equals("HO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Main", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CENTRO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Centro", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROBINSON")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Robinson",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROUTA")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Routa", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CAPITOL")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Capitol",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);

								}

							} else {
								LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
								// arTaxCode.addArReceipt(arReceipt);
								arInvoice.setArTaxCode(arTaxCode);

							}
							/*
							 * LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE",
							 * AD_CMPNY); //arTaxCode.addArInvoice(arInvoice);
							 * arInvoice.setArTaxCode(arTaxCode);
							 */

							LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome
									.findByWtcName("NONE", AD_CMPNY);
							// arWithholdingTaxCode.addArInvoice(arInvoice);
							arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);

							LocalArInvoiceBatch arInvoiceBatch = null;
							try {
								arInvoiceBatch = arInvoiceBatchHome.findByIbName("POS BATCH", AD_BRNCH, AD_CMPNY);
							} catch (FinderException ex) {
								arInvoiceBatch = arInvoiceBatchHome.create("POS BATCH", "POS BATCH", "OPEN", "MISC",
										EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);

							}
							// arInvoiceBatch.addArInvoice(arInvoice);
							arInvoice.setArInvoiceBatch(arInvoiceBatch);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();
							short lineNumber = 0;
							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(++lineNumber,
										arModInvoiceLineItemDetails.getIliQuantity(),
										arModInvoiceLineItemDetails.getIliUnitPrice(),
										EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
												/ (1 + arInvoice.getArTaxCode().getTcRate() / 100), (short) 2),
										EJBCommon.roundIt(
												arModInvoiceLineItemDetails.getIliAmount()
														- (arModInvoiceLineItemDetails.getIliAmount()
																/ (1 + arInvoice.getArTaxCode().getTcRate() / 100)),
												(short) 2),
										EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

								// arInvoice.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setArInvoice(arInvoice);

								LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
										.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

								arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());

								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);
								// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvItemLocation(invItemLocation);
								if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

									arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

								}
							}

						} else {

							ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invoiceNumbers
									.get(arExistingInvoice.getInvNumber());

							arModUploadReceiptDetails
									.setRctPosDiscount(
											arModUploadReceiptDetails.getRctPosDiscount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDiscount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(
											arModUploadReceiptDetails.getRctPosScAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosScAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(
											arModUploadReceiptDetails.getRctPosDcAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDcAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							invoiceNumbers.put(arExistingInvoice.getInvNumber(), arModUploadReceiptDetails);

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);
							arExistingInvoice.setInvAmountDue(arExistingInvoice.getInvAmountDue() + totalAmount);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();

							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();

								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);

								LocalArInvoiceLineItem arExistingInvoiceLineItem = null;

								try {
									arExistingInvoiceLineItem = arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(
											arExistingInvoice.getInvCode(), invItemLocation.getIlCode(),
											arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								} catch (FinderException ex) {

								}

								if (arExistingInvoiceLineItem == null) {

									LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
											(short) (arExistingInvoice.getArInvoiceLineItems().size() + 1),
											arModInvoiceLineItemDetails.getIliQuantity(),
											arModInvoiceLineItemDetails.getIliUnitPrice(),
											EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100),
													(short) 2),
											EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100)),
													(short) 2),
											EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

									// arExistingInvoice.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setArInvoice(arExistingInvoice);

									LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
											.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
									// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);
									arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
									// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvItemLocation(invItemLocation);
									if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

										arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

									}
								} else {

									arExistingInvoiceLineItem.setIliQuantity(arExistingInvoiceLineItem.getIliQuantity()
											+ arModInvoiceLineItemDetails.getIliQuantity());
									arExistingInvoiceLineItem
											.setIliUnitPrice((arExistingInvoiceLineItem.getIliUnitPrice()
													+ arModInvoiceLineItemDetails.getIliUnitPrice()) / 2);
									arExistingInvoiceLineItem
											.setIliAmount(arExistingInvoiceLineItem.getIliAmount() + EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100),
													(short) 2));
									arExistingInvoiceLineItem.setIliTaxAmount(arExistingInvoiceLineItem
											.getIliTaxAmount()
											+ EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100)),
													(short) 2));

								}
							}
						}

					}

				}

				// for each receipt generate distribution records

				Iterator rctIter = receiptNumbers.values().iterator();

				while (rctIter.hasNext()) {

					ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) rctIter.next();

					LocalArReceipt arReceipt = arReceiptHome
							.findByRctNumberAndBrCode(arModUploadReceiptDetails.getRctNumber(), AD_BRNCH, AD_CMPNY);
					arReceipt.setRctReferenceNumber(arModUploadReceiptDetails.getRctNumber());

					Iterator lineIter = arReceipt.getArInvoiceLineItems().iterator();

					double TOTAL_TAX = 0;
					double TOTAL_LINE = 0;

					while (lineIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) lineIter.next();

						TOTAL_LINE += arInvoiceLineItem.getIliTaxAmount() + arInvoiceLineItem.getIliAmount();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
											invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalAdBranchItemLocation adBranchItemLocation = null;

						try {

							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (arInvoiceLineItem.getIliEnableAutoBuild() == 0
								|| arInvoiceLineItem.getIliEnableAutoBuild() == 1
								|| arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

							if (adBranchItemLocation != null) {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							} else {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
										arReceipt, AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							}

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(
									invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
									arReceipt, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arReceipt,
									AD_BRNCH, AD_CMPNY);

						}

						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

					}

					// add tax distribution if necessary

					if (!arReceipt.getArTaxCode().getTcType().equals("NONE")
							&& !arReceipt.getArTaxCode().getTcType().equals("EXEMPT")) {

						this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
								arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE, arReceipt,
								AD_BRNCH, AD_CMPNY);

					}

					// add cash distribution

					this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE, arReceipt.getRctAmount(),
							arReceipt.getAdBankAccount().getBaCoaGlCashAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
							AD_CMPNY);

					System.out.println("ADD DISCOUNT: " + arModUploadReceiptDetails.getRctPosDiscount());
					if (arModUploadReceiptDetails.getRctPosDiscount() != 0) {
						// add discount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
								arModUploadReceiptDetails.getRctPosDiscount(),
								adPreference.getPrfMiscPosDiscountAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosScAmount() != 0) {
						// add sc amount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "SERVCE CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosScAmount(),
								adPreference.getPrfMiscPosServiceChargeAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDcAmount() != 0) {
						// add dc amount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "DINEIN CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosDcAmount(),
								adPreference.getPrfMiscPosDineInChargeAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					// add forex gain/loss

					double forexGainLoss = TOTAL_LINE
							- (arReceipt.getRctAmount() + arModUploadReceiptDetails.getRctPosDiscount()
									- arModUploadReceiptDetails.getRctPosScAmount()
									- arModUploadReceiptDetails.getRctPosDcAmount());
					if (forexGainLoss != 0) {
						this.addArDrEntry(arReceipt.getArDrNextLine(), "FOREX",
								forexGainLoss > 0 ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(forexGainLoss),
								adPreference.getPrfMiscPosGiftCertificateAccount(), EJBCommon.FALSE, arReceipt,
								AD_BRNCH, AD_CMPNY);
					}

				}

				Iterator invIter = invoiceNumbers.values().iterator();

				while (invIter.hasNext()) {

					ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invIter.next();

					LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
							arModUploadReceiptDetails.getRctNumber(), (byte) 0, AD_BRNCH, AD_CMPNY);
					arInvoice.setInvReferenceNumber(arModUploadReceiptDetails.getRctNumber());

					short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
					double TOTAL_PAYMENT_SCHEDULE = 0d;

					GregorianCalendar gcPrevDateDue = new GregorianCalendar();
					GregorianCalendar gcDateDue = new GregorianCalendar();
					gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

					Collection adPaymentSchedules = arInvoice.getAdPaymentTerm().getAdPaymentSchedules();

					Iterator i = adPaymentSchedules.iterator();

					while (i.hasNext()) {

						LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule) i.next();

						// get date due

						if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")) {

							gcDateDue.setTime(arInvoice.getInvEffectivityDate());
							gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

						} else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")) {

							gcDateDue = gcPrevDateDue;
							gcDateDue.add(Calendar.MONTH, 1);
							gcPrevDateDue = gcDateDue;

						} else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

							gcDateDue = gcPrevDateDue;

							if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
								if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31
										&& gcPrevDateDue.get(Calendar.DATE) > 15
										&& gcPrevDateDue.get(Calendar.DATE) < 31) {
									gcDateDue.add(Calendar.DATE, 16);
								} else {
									gcDateDue.add(Calendar.DATE, 15);
								}
							} else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
								if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
										&& gcPrevDateDue.get(Calendar.DATE) == 14) {
									gcDateDue.add(Calendar.DATE, 14);
								} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
										&& gcPrevDateDue.get(Calendar.DATE) >= 15
										&& gcPrevDateDue.get(Calendar.DATE) < 28) {
									gcDateDue.add(Calendar.DATE, 13);
								} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29
										&& gcPrevDateDue.get(Calendar.DATE) >= 15
										&& gcPrevDateDue.get(Calendar.DATE) < 29) {
									gcDateDue.add(Calendar.DATE, 14);
								} else {
									gcDateDue.add(Calendar.DATE, 15);
								}
							}

							gcPrevDateDue = gcDateDue;

						}

						// create a payment schedule

						double PAYMENT_SCHEDULE_AMOUNT = 0;

						// if last payment schedule subtract to avoid rounding difference error

						if (i.hasNext()) {

							PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount()
									/ arInvoice.getAdPaymentTerm().getPytBaseAmount()) * arInvoice.getInvAmountDue(),
									precisionUnit);

						} else {

							PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_PAYMENT_SCHEDULE;

						}

						LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = arInvoicePaymentScheduleHome.create(
								gcDateDue.getTime(), adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT, 0d,
								EJBCommon.FALSE, (short) 0, gcDateDue.getTime(), 0d, 0d, AD_CMPNY);

						// arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);
						arInvoicePaymentSchedule.setArInvoice(arInvoice);

						TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;

					}

					Iterator lineIter = arInvoice.getArInvoiceLineItems().iterator();

					double TOTAL_TAX = 0;
					double TOTAL_LINE = 0;

					while (lineIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) lineIter.next();

						TOTAL_LINE += arInvoiceLineItem.getIliTaxAmount() + arInvoiceLineItem.getIliAmount();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
											invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalAdBranchItemLocation adBranchItemLocation = null;

						try {

							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (arInvoiceLineItem.getIliEnableAutoBuild() == 0
								|| arInvoiceLineItem.getIliEnableAutoBuild() == 1
								|| arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

							if (adBranchItemLocation != null) {

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

							} else {

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
										arInvoice, AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

							}

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(
									invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
									arInvoice, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice,
									AD_BRNCH, AD_CMPNY);

						}

						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

					}

					// add tax distribution if necessary

					if (!arInvoice.getArTaxCode().getTcType().equals("NONE")
							&& !arInvoice.getArTaxCode().getTcType().equals("EXEMPT")) {

						this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
								arInvoice.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE, arInvoice,
								AD_BRNCH, AD_CMPNY);

					}

					// add cash distribution

					LocalAdBranchCustomer adBranchCustomer = null;

					try {

						adBranchCustomer = adBranchCustomerHome
								.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if (adBranchCustomer != null) {

						this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
								arInvoice.getInvAmountDue(), adBranchCustomer.getBcstGlCoaReceivableAccount(),
								EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);

					} else {
						this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
								arInvoice.getInvAmountDue(), arInvoice.getArCustomer().getCstGlCoaReceivableAccount(),
								EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDiscount() != 0) {
						// add discount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
								arModUploadReceiptDetails.getRctPosDiscount(),
								adPreference.getPrfMiscPosDiscountAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosScAmount() != 0) {
						// add sc amount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "SERVCE CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosScAmount(),
								adPreference.getPrfMiscPosServiceChargeAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDcAmount() != 0) {
						// add dc amount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "DINEIN CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosDcAmount(),
								adPreference.getPrfMiscPosDineInChargeAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					// add forex gain/loss

					double forexGainLoss = TOTAL_LINE
							- (arInvoice.getInvAmountDue() + arModUploadReceiptDetails.getRctPosDiscount()
									- arModUploadReceiptDetails.getRctPosScAmount()
									- arModUploadReceiptDetails.getRctPosDcAmount());
					if (forexGainLoss != 0) {
						this.addArDrEntry(arInvoice.getArDrNextLine(), "FOREX",
								forexGainLoss > 0 ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(forexGainLoss),
								adPreference.getPrfMiscPosGiftCertificateAccount(), EJBCommon.FALSE, arInvoice,
								AD_BRNCH, AD_CMPNY);
					}

				}

				success = 1;
				return success;
			} else {

				System.out.println("NULL ACCOUNTs");
				System.out.println(success);
				return success;

			}

		} catch (Exception ex) {

			ex.printStackTrace();
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method
	 **/
	public int setArMiscReceiptAllNewAndVoidWithSalesperson(String[] newRR, String[] voidRR, String BR_BRNCH_CODE,
			Integer AD_CMPNY, String CASHIER) {

		Debug.print("ArMiscReceiptSyncControllerBean setInvMiscReceiptAllNewAndVoidWitSalesperson");

		LocalAdBranchHome adBranchHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalArReceiptBatchHome arReceiptBatchHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoiceBatchHome arInvoiceBatchHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalAdBranchCustomerHome adBranchCustomerHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalArSalespersonHome arSalespersonHome = null;

		try {

			adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
					LocalAdBranchHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			arReceiptHome = (LocalArReceiptHome) EJBHomeFactory.lookUpLocalHome(LocalArReceiptHome.JNDI_NAME,
					LocalArReceiptHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
					LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
							LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory.lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME,
					LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome) EJBHomeFactory
					.lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			arCustomerHome = (LocalArCustomerHome) EJBHomeFactory.lookUpLocalHome(LocalArCustomerHome.JNDI_NAME,
					LocalArCustomerHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			arReceiptBatchHome = (LocalArReceiptBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory.lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME,
					LocalArInvoiceHome.class);
			arInvoiceBatchHome = (LocalArInvoiceBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome) EJBHomeFactory
					.lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			invLocationHome = (LocalInvLocationHome) EJBHomeFactory.lookUpLocalHome(LocalInvLocationHome.JNDI_NAME,
					LocalInvLocationHome.class);
			adBranchCustomerHome = (LocalAdBranchCustomerHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
					LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);
			arSalespersonHome = (LocalArSalespersonHome) EJBHomeFactory
					.lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			Integer AD_BRNCH = adBranch.getBrCode();

			int success = 0;

			if (adPreference.getPrfMiscPosDiscountAccount() != null
					&& adPreference.getPrfMiscPosGiftCertificateAccount() != null
					&& adPreference.getPrfMiscPosServiceChargeAccount() != null
					&& adPreference.getPrfMiscPosDineInChargeAccount() != null) {

				// new receipts

				HashMap receiptNumbers = new HashMap();
				HashMap invoiceNumbers = new HashMap();
				String fundTransferNumber = null;
				String firstNumber = "";
				String lastNumber = "";

				for (int i = 0; i < newRR.length; i++) {

					ArModReceiptDetails arModReceiptDetails = receiptDecodeWithSalesperson(newRR[i]);

					if (i == 0)
						firstNumber = arModReceiptDetails.getRctNumber();
					if (i == newRR.length - 1)
						lastNumber = arModReceiptDetails.getRctNumber();

					if (arModReceiptDetails.getRctPosOnAccount() == 0) {

						// generate receipt number

						String generatedReceipt = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arReceiptHome.findByRctNumberAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedReceipt = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						LocalArReceipt arExistingReceipt = null;

						Iterator rctIter = receiptNumbers.values().iterator();
						while (rctIter.hasNext()) {

							try {

								ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) rctIter.next();

								arExistingReceipt = arReceiptHome.findByRctDateAndRctNumberAndCstCustomerCodeAndBrCode(
										arModReceiptDetails.getRctDate(), arModUploadReceiptDetails.getRctNumber(),
										arModReceiptDetails.getRctCstCustomerCode(), AD_BRNCH, AD_CMPNY);

								break;

							} catch (FinderException ex) {

								continue;

							}

						}

						if (arExistingReceipt == null) {

							ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
							arModUploadReceiptDetails.setRctNumber(generatedReceipt);
							arModUploadReceiptDetails
									.setRctPosDiscount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							receiptNumbers.put(generatedReceipt, arModUploadReceiptDetails);

							System.out.println(arModReceiptDetails.getRctConversionRate()
									+ " <<CONV RATE:MISC:POS DISC>> " + arModReceiptDetails.getRctPosDiscount());

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);

							LocalArReceipt arReceipt = arReceiptHome.create("MISC", "POS Sales " + new Date(),
									arModReceiptDetails.getRctDate(), generatedReceipt, null, null, null, null, null,
									null, null, null, totalAmount, 0d, 0d, 0d, 0d, 0d, 0d, null, 1, null, "CASH",
									EJBCommon.FALSE, 0, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE,
									EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
									EJBCommon.FALSE, null, null, null, null, null,

									CASHIER, EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
									EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null, null,
									EJBCommon.FALSE, EJBCommon.FALSE, null, AD_BRNCH, AD_CMPNY);

							LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
									.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
							// glFunctionalCurrency.addArReceipt(arReceipt);
							arReceipt.setGlFunctionalCurrency(glFunctionalCurrency);

							LocalArCustomer arCustomer = arCustomerHome
									.findByCstCustomerCode(arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
							// arCustomer.addArReceipt(arReceipt);
							arReceipt.setArCustomer(arCustomer);

							if (!arModReceiptDetails.getRctSlpSalespersonCode().equals("null")
									&& !arModReceiptDetails.getRctSlpSalespersonCode().equals("")) {
								try {
									// rowen 2
									LocalArSalesperson arSalesperson = arSalespersonHome.findByPrimaryKey(
											Integer.parseInt(arModReceiptDetails.getRctSlpSalespersonCode()));
									arReceipt.setArSalesperson(arSalesperson);
								} catch (FinderException ex) {

								}

							}

							String bankAccount = "";
							if (BR_BRNCH_CODE.equals("Manila-Smoking Lounge")) {
								bankAccount = "Allied Bank-IPT Smoking";
							} else if (BR_BRNCH_CODE.equals("Manila-Cigar Shop")) {
								bankAccount = "Allied Bank Cigar shop";
							} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Domestic")) {
								bankAccount = "Terminal II Domestic";
							} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Intl")) {
								bankAccount = "Term2 International";
							} else if (BR_BRNCH_CODE.equals("Cebu-Banilad")) {
								bankAccount = "Metrobank Banilad";
							} else if (BR_BRNCH_CODE.equals("Cebu-Gorordo")) {
								bankAccount = "Metrobank-Gorordo";
							} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Domestic")) {
								bankAccount = "Metrobank Mactan Domestic";
							} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Intl")) {
								bankAccount = "Metrobank I Mactan Int'l";
							} else if (BR_BRNCH_CODE.equals("Cebu-Supercat")) {
								bankAccount = "Metrobank Supercat";
							} else {
								bankAccount = arCustomer.getAdBankAccount().getBaName();
							}

							LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(bankAccount, AD_CMPNY);
							// adBankAccount.addArReceipt(arReceipt);
							arReceipt.setAdBankAccount(adBankAccount);

							// Rowen arReceipt.setArSalesperson("4");

							if (adCompany.getCmpShortName().toLowerCase().equals("vertext")) {
								if (BR_BRNCH_CODE.equals("HO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Main", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CENTRO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Centro", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROBINSON")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Robinson",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROUTA")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Routa", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CAPITOL")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Capitol",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);
								} else {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arReceipt.setArTaxCode(arTaxCode);

								}

							} else {
								LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
								// arTaxCode.addArReceipt(arReceipt);
								arReceipt.setArTaxCode(arTaxCode);

							}

							/*
							 * LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE",
							 * AD_CMPNY); //arTaxCode.addArReceipt(arReceipt);
							 * arReceipt.setArTaxCode(arTaxCode);
							 */

							LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome
									.findByWtcName("NONE", AD_CMPNY);
							// arWithholdingTaxCode.addArReceipt(arReceipt);
							arReceipt.setArWithholdingTaxCode(arWithholdingTaxCode);

							LocalArReceiptBatch arReceiptBatch = null;
							try {
								arReceiptBatch = arReceiptBatchHome.findByRbName("POS BATCH", AD_BRNCH, AD_CMPNY);
							} catch (FinderException ex) {
								arReceiptBatch = arReceiptBatchHome.create("POS BATCH", "POS BATCH", "OPEN", "MISC",
										EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);

							}
							// arReceiptBatch.addArReceipt(arReceipt);
							arReceipt.setArReceiptBatch(arReceiptBatch);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();
							short lineNumber = 0;
							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(++lineNumber,
										arModInvoiceLineItemDetails.getIliQuantity(),
										arModInvoiceLineItemDetails.getIliUnitPrice(),
										EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
												/ (1 + arReceipt.getArTaxCode().getTcRate() / 100), (short) 2),
										EJBCommon.roundIt(
												arModInvoiceLineItemDetails.getIliAmount()
														- (arModInvoiceLineItemDetails.getIliAmount()
																/ (1 + arReceipt.getArTaxCode().getTcRate() / 100)),
												(short) 2),
										EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

								// arReceipt.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setArReceipt(arReceipt);

								LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
										.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);
								arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
								System.out.println("arModInvoiceLineItemDetails.getIliIiName: "
										+ arModInvoiceLineItemDetails.getIliIiName());
								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}

								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);
								// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvItemLocation(invItemLocation);

								if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

									arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

								}
							}

						} else {

							ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) receiptNumbers
									.get(arExistingReceipt.getRctNumber());

							arModUploadReceiptDetails
									.setRctPosDiscount(
											arModUploadReceiptDetails.getRctPosDiscount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDiscount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(
											arModUploadReceiptDetails.getRctPosScAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosScAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(
											arModUploadReceiptDetails.getRctPosDcAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDcAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							receiptNumbers.put(arExistingReceipt.getRctNumber(), arModUploadReceiptDetails);
							System.out.println(arModReceiptDetails.getRctConversionRate()
									+ " <<CONV RATE:MISC2:POS DISC>> " + arModReceiptDetails.getRctPosDiscount());

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);
							arExistingReceipt.setRctAmount(arExistingReceipt.getRctAmount() + totalAmount);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();

							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								System.out.println(arModInvoiceLineItemDetails.getIliIiName());
								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);

								LocalArInvoiceLineItem arExistingInvoiceLineItem = null;

								try {
									arExistingInvoiceLineItem = arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(
											arExistingReceipt.getRctCode(), invItemLocation.getIlCode(),
											arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								} catch (FinderException ex) {

								}

								if (arExistingInvoiceLineItem == null) {
									System.out.println(arModInvoiceLineItemDetails.getIliIiName());
									LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
											(short) (arExistingReceipt.getArInvoiceLineItems().size() + 1),
											arModInvoiceLineItemDetails.getIliQuantity(),
											arModInvoiceLineItemDetails.getIliUnitPrice(),
											EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100),
													(short) 2),
											EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100)),
													(short) 2),
											EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

									// arExistingReceipt.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setArReceipt(arExistingReceipt);

									LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
											.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
									// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

									// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvItemLocation(invItemLocation);
									arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
									if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

										arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

									}
								} else {

									arExistingInvoiceLineItem.setIliQuantity(arExistingInvoiceLineItem.getIliQuantity()
											+ arModInvoiceLineItemDetails.getIliQuantity());
									arExistingInvoiceLineItem
											.setIliUnitPrice((arExistingInvoiceLineItem.getIliUnitPrice()
													+ arModInvoiceLineItemDetails.getIliUnitPrice()) / 2);
									arExistingInvoiceLineItem
											.setIliAmount(arExistingInvoiceLineItem.getIliAmount() + EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100),
													(short) 2));
									arExistingInvoiceLineItem.setIliTaxAmount(arExistingInvoiceLineItem
											.getIliTaxAmount()
											+ EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingReceipt.getArTaxCode().getTcRate() / 100)),
													(short) 2));

								}
							}
						}

					} else {

						// generate Invoice number

						String generatedInvoice = null;

						LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
						LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

						try {

							adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR INVOICE",
									AD_CMPNY);

						} catch (FinderException ex) {

						}

						try {

							adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
									.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH,
											AD_CMPNY);

						} catch (FinderException ex) {

						}

						while (true) {

							if (adBranchDocumentSequenceAssignment == null
									|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adDocumentSequenceAssignment.getDsaNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adDocumentSequenceAssignment.getDsaNextSequence();
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon
											.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
											adBranchDocumentSequenceAssignment.getBdsNextSequence(), (byte) 0, AD_BRNCH,
											AD_CMPNY);
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									generatedInvoice = adBranchDocumentSequenceAssignment.getBdsNextSequence();
									adBranchDocumentSequenceAssignment
											.setBdsNextSequence(EJBCommon.incrementStringNumber(
													adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}
						LocalArInvoice arExistingInvoice = null;

						Iterator invIter = invoiceNumbers.values().iterator();
						while (invIter.hasNext()) {

							try {

								ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invIter.next();

								arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndCstCustomerCode(
										arModUploadReceiptDetails.getRctNumber(), (byte) 0,
										arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);

								break;

							} catch (FinderException ex) {

								continue;

							}

						}

						if (arExistingInvoice == null) {

							ArModReceiptDetails arModUploadReceiptDetails = new ArModReceiptDetails();
							arModUploadReceiptDetails.setRctNumber(generatedInvoice);
							arModUploadReceiptDetails
									.setRctPosDiscount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDiscount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosScAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(EJBCommon.roundIt(arModReceiptDetails.getRctPosDcAmount()
											* arModReceiptDetails.getRctConversionRate(), (short) 2));
							invoiceNumbers.put(generatedInvoice, arModUploadReceiptDetails);
							System.out.println(arModReceiptDetails.getRctConversionRate()
									+ " <<CONV RATE:INVC:POS DISC>> " + arModReceiptDetails.getRctPosDiscount());

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);

							LocalArInvoice arInvoice = arInvoiceHome.create("ITEMS", (byte) 0,
									"POS Sales " + new Date().toString(), arModReceiptDetails.getRctDate(),
									generatedInvoice, null, null, null, null, totalAmount, 0d, 0d, 0d, 0d, 0d, null, 1,
									null, 0d, 0d, null, null, null, null, null, null, null, null, null, null, null,
									null, null, null, null, null, null, null, null, null, EJBCommon.FALSE, null,
									EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
									null, 0d, null, null, null, null, CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER,
									EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, null, null,
									EJBCommon.FALSE, null, null, null, (byte) 0, (byte) 0, null,
									arModReceiptDetails.getRctDate(), AD_BRNCH, AD_CMPNY);

							LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName("IMMEDIATE", AD_CMPNY);
							// adPaymentTerm.addArInvoice(arInvoice);
							arInvoice.setAdPaymentTerm(adPaymentTerm);

							LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
									.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
							// glFunctionalCurrency.addArInvoice(arInvoice);
							arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

							LocalArCustomer arCustomer = arCustomerHome
									.findByCstCustomerCode(arModReceiptDetails.getRctCstCustomerCode(), AD_CMPNY);
							// arCustomer.addArInvoice(arInvoice);
							arInvoice.setArCustomer(arCustomer);

							if (!arModReceiptDetails.getRctSlpSalespersonCode().equals("null")
									&& !arModReceiptDetails.getRctSlpSalespersonCode().equals("")) {
								try {
									// rowen 2
									LocalArSalesperson arSalesperson = arSalespersonHome.findByPrimaryKey(
											Integer.parseInt(arModReceiptDetails.getRctSlpSalespersonCode()));
									arInvoice.setArSalesperson(arSalesperson);
								} catch (FinderException ex) {

								}

							}

							if (adCompany.getCmpShortName().toLowerCase().equals("vertext")) {
								if (BR_BRNCH_CODE.equals("HO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Main", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CENTRO")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Centro", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROBINSON")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Robinson",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-ROUTA")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Routa", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else if (BR_BRNCH_CODE.equals("OL-CAPITOL")) {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INC - Capitol",
											AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);
								} else {
									LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
									// arTaxCode.addArReceipt(arReceipt);
									arInvoice.setArTaxCode(arTaxCode);

								}

							} else {
								LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE", AD_CMPNY);
								// arTaxCode.addArReceipt(arReceipt);
								arInvoice.setArTaxCode(arTaxCode);

							}
							/*
							 * LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("VAT INCLUSIVE",
							 * AD_CMPNY); //arTaxCode.addArInvoice(arInvoice);
							 * arInvoice.setArTaxCode(arTaxCode);
							 */

							LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome
									.findByWtcName("NONE", AD_CMPNY);
							// arWithholdingTaxCode.addArInvoice(arInvoice);
							arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);

							LocalArInvoiceBatch arInvoiceBatch = null;
							try {
								arInvoiceBatch = arInvoiceBatchHome.findByIbName("POS BATCH", AD_BRNCH, AD_CMPNY);
							} catch (FinderException ex) {
								arInvoiceBatch = arInvoiceBatchHome.create("POS BATCH", "POS BATCH", "OPEN", "MISC",
										EJBCommon.getGcCurrentDateWoTime().getTime(), CASHIER, AD_BRNCH, AD_CMPNY);

							}
							// arInvoiceBatch.addArInvoice(arInvoice);
							arInvoice.setArInvoiceBatch(arInvoiceBatch);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();
							short lineNumber = 0;
							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();
								LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(++lineNumber,
										arModInvoiceLineItemDetails.getIliQuantity(),
										arModInvoiceLineItemDetails.getIliUnitPrice(),
										EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
												/ (1 + arInvoice.getArTaxCode().getTcRate() / 100), (short) 2),
										EJBCommon.roundIt(
												arModInvoiceLineItemDetails.getIliAmount()
														- (arModInvoiceLineItemDetails.getIliAmount()
																/ (1 + arInvoice.getArTaxCode().getTcRate() / 100)),
												(short) 2),
										EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

								// arInvoice.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setArInvoice(arInvoice);

								LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
										.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

								arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());

								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);
								// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
								arInvoiceLineItem.setInvItemLocation(invItemLocation);
								if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

									arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

								}
							}

						} else {

							ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invoiceNumbers
									.get(arExistingInvoice.getInvNumber());

							arModUploadReceiptDetails
									.setRctPosDiscount(
											arModUploadReceiptDetails.getRctPosDiscount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDiscount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosScAmount(
											arModUploadReceiptDetails.getRctPosScAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosScAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							arModUploadReceiptDetails
									.setRctPosDcAmount(
											arModUploadReceiptDetails.getRctPosDcAmount()
													+ EJBCommon.roundIt(
															arModReceiptDetails.getRctPosDcAmount()
																	* arModReceiptDetails.getRctConversionRate(),
															(short) 2));
							invoiceNumbers.put(arExistingInvoice.getInvNumber(), arModUploadReceiptDetails);
							System.out.println(arModReceiptDetails.getRctConversionRate()
									+ " <<CONV RATE:INVC:POS DISC>> " + arModReceiptDetails.getRctPosDiscount());

							double totalAmount = EJBCommon.roundIt((arModReceiptDetails.getRctPosTotalAmount()
									- arModReceiptDetails.getRctPosVoidAmount())
									* arModReceiptDetails.getRctConversionRate(), (short) 2);
							arExistingInvoice.setInvAmountDue(arExistingInvoice.getInvAmountDue() + totalAmount);

							Iterator iter = arModReceiptDetails.getInvIliList().iterator();

							while (iter.hasNext()) {

								ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = (ArModInvoiceLineItemDetails) iter
										.next();

								LocalInvItem invItem = invItemHome
										.findByIiName(arModInvoiceLineItemDetails.getIliIiName(), AD_CMPNY);
								LocalInvLocation invLocation = null;
								try {
									if (invItem.getIiDefaultLocation() != null)
										invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
								} catch (FinderException ex) {
								}
								LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
										arModInvoiceLineItemDetails.getIliIiName(),
										invLocation == null ? arModInvoiceLineItemDetails.getIliLocName()
												: invLocation.getLocName(),
										AD_CMPNY);

								LocalArInvoiceLineItem arExistingInvoiceLineItem = null;

								try {
									arExistingInvoiceLineItem = arInvoiceLineItemHome.findByRctCodeAndIlCodeAndUomName(
											arExistingInvoice.getInvCode(), invItemLocation.getIlCode(),
											arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
								} catch (FinderException ex) {

								}

								if (arExistingInvoiceLineItem == null) {

									LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
											(short) (arExistingInvoice.getArInvoiceLineItems().size() + 1),
											arModInvoiceLineItemDetails.getIliQuantity(),
											arModInvoiceLineItemDetails.getIliUnitPrice(),
											EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100),
													(short) 2),
											EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100)),
													(short) 2),
											EJBCommon.FALSE, 0, 0, 0, 0, 0, EJBCommon.TRUE, AD_CMPNY);

									// arExistingInvoice.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setArInvoice(arExistingInvoice);

									LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome
											.findByUomName(arModInvoiceLineItemDetails.getIliUomName(), AD_CMPNY);
									// invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);
									arInvoiceLineItem.setIliMisc(arModInvoiceLineItemDetails.getIliMisc());
									// invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
									arInvoiceLineItem.setInvItemLocation(invItemLocation);
									if (invItemLocation.getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE) {

										arInvoiceLineItem.setIliEnableAutoBuild(EJBCommon.TRUE);

									}
								} else {

									arExistingInvoiceLineItem.setIliQuantity(arExistingInvoiceLineItem.getIliQuantity()
											+ arModInvoiceLineItemDetails.getIliQuantity());
									arExistingInvoiceLineItem
											.setIliUnitPrice((arExistingInvoiceLineItem.getIliUnitPrice()
													+ arModInvoiceLineItemDetails.getIliUnitPrice()) / 2);
									arExistingInvoiceLineItem
											.setIliAmount(arExistingInvoiceLineItem.getIliAmount() + EJBCommon.roundIt(
													arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100),
													(short) 2));
									arExistingInvoiceLineItem.setIliTaxAmount(arExistingInvoiceLineItem
											.getIliTaxAmount()
											+ EJBCommon.roundIt(arModInvoiceLineItemDetails.getIliAmount()
													- (arModInvoiceLineItemDetails.getIliAmount()
															/ (1 + arExistingInvoice.getArTaxCode().getTcRate() / 100)),
													(short) 2));

								}
							}
						}

					}

				}

				// for each receipt generate distribution records

				Iterator rctIter = receiptNumbers.values().iterator();

				while (rctIter.hasNext()) {

					ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) rctIter.next();

					LocalArReceipt arReceipt = arReceiptHome
							.findByRctNumberAndBrCode(arModUploadReceiptDetails.getRctNumber(), AD_BRNCH, AD_CMPNY);
					arReceipt.setRctReferenceNumber(firstNumber + "-" + lastNumber);

					Iterator lineIter = arReceipt.getArInvoiceLineItems().iterator();

					double TOTAL_TAX = 0;
					double TOTAL_LINE = 0;

					while (lineIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) lineIter.next();

						TOTAL_LINE += arInvoiceLineItem.getIliTaxAmount() + arInvoiceLineItem.getIliAmount();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
											invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalAdBranchItemLocation adBranchItemLocation = null;

						try {

							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (arInvoiceLineItem.getIliEnableAutoBuild() == 0
								|| arInvoiceLineItem.getIliEnableAutoBuild() == 1
								|| arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

							if (adBranchItemLocation != null) {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							} else {

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
										arReceipt, AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arReceipt,
										AD_BRNCH, AD_CMPNY);

							}

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(
									invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
									arReceipt, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arReceipt,
									AD_BRNCH, AD_CMPNY);

						}

						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

					}

					// add tax distribution if necessary

					if (!arReceipt.getArTaxCode().getTcType().equals("NONE")
							&& !arReceipt.getArTaxCode().getTcType().equals("EXEMPT")) {

						this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
								arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE, arReceipt,
								AD_BRNCH, AD_CMPNY);

					}

					// add cash distribution

					this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE, arReceipt.getRctAmount(),
							arReceipt.getAdBankAccount().getBaCoaGlCashAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
							AD_CMPNY);
					System.out.println("Add Discount: " + arModUploadReceiptDetails.getRctPosDiscount());
					if (arModUploadReceiptDetails.getRctPosDiscount() != 0) {
						// add discount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
								arModUploadReceiptDetails.getRctPosDiscount(),
								adPreference.getPrfMiscPosDiscountAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosScAmount() != 0) {
						// add sc amount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "SERVCE CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosScAmount(),
								adPreference.getPrfMiscPosServiceChargeAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDcAmount() != 0) {
						// add dc amount
						this.addArDrEntry(arReceipt.getArDrNextLine(), "DINEIN CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosDcAmount(),
								adPreference.getPrfMiscPosDineInChargeAccount(), EJBCommon.FALSE, arReceipt, AD_BRNCH,
								AD_CMPNY);
					}

					// add forex gain/loss

					double forexGainLoss = TOTAL_LINE
							- (arReceipt.getRctAmount() + arModUploadReceiptDetails.getRctPosDiscount()
									- arModUploadReceiptDetails.getRctPosScAmount()
									- arModUploadReceiptDetails.getRctPosDcAmount());
					if (forexGainLoss != 0) {
						this.addArDrEntry(arReceipt.getArDrNextLine(), "FOREX",
								forexGainLoss > 0 ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(forexGainLoss),
								adPreference.getPrfMiscPosGiftCertificateAccount(), EJBCommon.FALSE, arReceipt,
								AD_BRNCH, AD_CMPNY);
					}

				}

				Iterator invIter = invoiceNumbers.values().iterator();

				while (invIter.hasNext()) {

					ArModReceiptDetails arModUploadReceiptDetails = (ArModReceiptDetails) invIter.next();

					LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
							arModUploadReceiptDetails.getRctNumber(), (byte) 0, AD_BRNCH, AD_CMPNY);
					arInvoice.setInvReferenceNumber(firstNumber + "-" + lastNumber);

					short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
					double TOTAL_PAYMENT_SCHEDULE = 0d;

					GregorianCalendar gcPrevDateDue = new GregorianCalendar();
					GregorianCalendar gcDateDue = new GregorianCalendar();
					gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

					Collection adPaymentSchedules = arInvoice.getAdPaymentTerm().getAdPaymentSchedules();

					Iterator i = adPaymentSchedules.iterator();

					while (i.hasNext()) {

						LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule) i.next();

						// get date due

						if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")) {

							gcDateDue.setTime(arInvoice.getInvEffectivityDate());
							gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

						} else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")) {

							gcDateDue = gcPrevDateDue;
							gcDateDue.add(Calendar.MONTH, 1);
							gcPrevDateDue = gcDateDue;

						} else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

							gcDateDue = gcPrevDateDue;

							if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
								if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31
										&& gcPrevDateDue.get(Calendar.DATE) > 15
										&& gcPrevDateDue.get(Calendar.DATE) < 31) {
									gcDateDue.add(Calendar.DATE, 16);
								} else {
									gcDateDue.add(Calendar.DATE, 15);
								}
							} else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
								if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
										&& gcPrevDateDue.get(Calendar.DATE) == 14) {
									gcDateDue.add(Calendar.DATE, 14);
								} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28
										&& gcPrevDateDue.get(Calendar.DATE) >= 15
										&& gcPrevDateDue.get(Calendar.DATE) < 28) {
									gcDateDue.add(Calendar.DATE, 13);
								} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29
										&& gcPrevDateDue.get(Calendar.DATE) >= 15
										&& gcPrevDateDue.get(Calendar.DATE) < 29) {
									gcDateDue.add(Calendar.DATE, 14);
								} else {
									gcDateDue.add(Calendar.DATE, 15);
								}
							}

							gcPrevDateDue = gcDateDue;

						}

						// create a payment schedule

						double PAYMENT_SCHEDULE_AMOUNT = 0;

						// if last payment schedule subtract to avoid rounding difference error

						if (i.hasNext()) {

							PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount()
									/ arInvoice.getAdPaymentTerm().getPytBaseAmount()) * arInvoice.getInvAmountDue(),
									precisionUnit);

						} else {

							PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_PAYMENT_SCHEDULE;

						}

						LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = arInvoicePaymentScheduleHome.create(
								gcDateDue.getTime(), adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT, 0d,
								EJBCommon.FALSE, (short) 0, gcDateDue.getTime(), 0d, 0d, AD_CMPNY);

						// arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);
						arInvoicePaymentSchedule.setArInvoice(arInvoice);

						TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;

					}

					Iterator lineIter = arInvoice.getArInvoiceLineItems().iterator();

					double TOTAL_TAX = 0;
					double TOTAL_LINE = 0;

					while (lineIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) lineIter.next();

						TOTAL_LINE += arInvoiceLineItem.getIliTaxAmount() + arInvoiceLineItem.getIliAmount();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

						// add cost of sales distribution and inventory

						double COST = 0d;

						try {

							LocalInvCosting invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
											invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						} catch (FinderException ex) {

							COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						}

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalAdBranchItemLocation adBranchItemLocation = null;

						try {

							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (arInvoiceLineItem.getIliEnableAutoBuild() == 0
								|| arInvoiceLineItem.getIliEnableAutoBuild() == 1
								|| arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

							if (adBranchItemLocation != null) {

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

							} else {

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "COGS", EJBCommon.TRUE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(),
										arInvoice, AD_BRNCH, AD_CMPNY);

								this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
										COST * QTY_SLD,
										arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice,
										AD_BRNCH, AD_CMPNY);

							}

							// add quantity to item location committed quantity

							double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
							invItemLocation.setIlCommittedQuantity(
									invItemLocation.getIlCommittedQuantity() + convertedQuantity);

						}

						// add inventory sale distributions

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(), adBranchItemLocation.getBilCoaGlSalesAccount(),
									arInvoice, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arInvoice.getArDrNextLine(), "REVENUE", EJBCommon.FALSE,
									arInvoiceLineItem.getIliAmount(),
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice,
									AD_BRNCH, AD_CMPNY);

						}

						TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

					}

					// add tax distribution if necessary

					if (!arInvoice.getArTaxCode().getTcType().equals("NONE")
							&& !arInvoice.getArTaxCode().getTcType().equals("EXEMPT")) {

						this.addArDrEntry(arInvoice.getArDrNextLine(), "TAX", EJBCommon.FALSE, TOTAL_TAX,
								arInvoice.getArTaxCode().getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE, arInvoice,
								AD_BRNCH, AD_CMPNY);

					}

					// add cash distribution

					LocalAdBranchCustomer adBranchCustomer = null;

					try {

						adBranchCustomer = adBranchCustomerHome
								.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if (adBranchCustomer != null) {

						this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
								arInvoice.getInvAmountDue(), adBranchCustomer.getBcstGlCoaReceivableAccount(),
								EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);

					} else {
						this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE", EJBCommon.TRUE,
								arInvoice.getInvAmountDue(), arInvoice.getArCustomer().getCstGlCoaReceivableAccount(),
								EJBCommon.FALSE, arInvoice, AD_BRNCH, AD_CMPNY);
					}

					System.out.println("Add Discount2: " + arModUploadReceiptDetails.getRctPosDiscount());
					if (arModUploadReceiptDetails.getRctPosDiscount() != 0) {
						// add discount.
						System.out.println("DISCOUNT INVOICE");
						this.addArDrEntry(arInvoice.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
								arModUploadReceiptDetails.getRctPosDiscount(),
								adPreference.getPrfMiscPosDiscountAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosScAmount() != 0) {
						// add sc amount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "SERVCE CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosScAmount(),
								adPreference.getPrfMiscPosServiceChargeAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					if (arModUploadReceiptDetails.getRctPosDcAmount() != 0) {
						// add dc amount
						this.addArDrEntry(arInvoice.getArDrNextLine(), "DINEIN CHARGE", EJBCommon.FALSE,
								arModUploadReceiptDetails.getRctPosDcAmount(),
								adPreference.getPrfMiscPosDineInChargeAccount(), EJBCommon.FALSE, arInvoice, AD_BRNCH,
								AD_CMPNY);
					}

					// add forex gain/loss

					double forexGainLoss = TOTAL_LINE
							- (arInvoice.getInvAmountDue() + arModUploadReceiptDetails.getRctPosDiscount()
									- arModUploadReceiptDetails.getRctPosScAmount()
									- arModUploadReceiptDetails.getRctPosDcAmount());
					if (forexGainLoss != 0) {
						this.addArDrEntry(arInvoice.getArDrNextLine(), "FOREX",
								forexGainLoss > 0 ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(forexGainLoss),
								adPreference.getPrfMiscPosGiftCertificateAccount(), EJBCommon.FALSE, arInvoice,
								AD_BRNCH, AD_CMPNY);
					}

				}

				success = 1;
				return success;
			} else {

				System.out.println("NULL ACCOUNTs");
				System.out.println(success);
				return success;

			}

		} catch (Exception ex) {

			ex.printStackTrace();
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}
	}

	/*
		*//**
			 * @ejb:interface-method view-type="remote"
			 **/

	/*
	 * public Integer saveArRctEntry(com.util.ArReceiptDetails details, String
	 * BA_NM, String FC_NM, String CST_CSTMR_CODE, String RB_NM, ArrayList aiList,
	 * boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
	 * GlobalRecordAlreadyDeletedException, GlobalDocumentNumberNotUniqueException,
	 * GlobalConversionDateNotExistException,
	 * GlobalTransactionAlreadyApprovedException,
	 * GlobalTransactionAlreadyPendingException,
	 * GlobalTransactionAlreadyPostedException,
	 * GlobalTransactionAlreadyVoidException,
	 * ArINVOverapplicationNotAllowedException,
	 * GlobalTransactionAlreadyLockedException,
	 * GlobalNoApprovalRequesterFoundException,
	 * GlobalNoApprovalApproverFoundException, ArRCTInvoiceHasNoWTaxCodeException,
	 * GlJREffectiveDateNoPeriodExistException,
	 * GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
	 * GlobalBranchAccountNumberInvalidException,
	 * GlobalRecordAlreadyAssignedException,
	 * AdPRFCoaGlCustomerDepositAccountNotFoundException {
	 * 
	 * Debug.print("ArReceiptEntryControllerBean saveArRctEntry");
	 * 
	 * LocalArReceiptHome arReceiptHome = null; LocalArReceiptBatchHome
	 * arReceiptBatchHome = null; LocalAdCompanyHome adCompanyHome = null;
	 * LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
	 * LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome =
	 * null; LocalAdBranchDocumentSequenceAssignmentHome
	 * adBranchDocumentSequenceAssignmentHome = null; LocalAdBankAccountHome
	 * adBankAccountHome = null; LocalArCustomerHome arCustomerHome = null;
	 * LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
	 * LocalArDistributionRecordHome arDistributionRecordHome = null;
	 * LocalAdApprovalHome adApprovalHome = null; LocalAdAmountLimitHome
	 * adAmountLimitHome = null; LocalAdApprovalUserHome adApprovalUserHome = null;
	 * LocalAdApprovalQueueHome adApprovalQueueHome = null; LocalAdPreferenceHome
	 * adPreferenceHome = null;
	 * 
	 * LocalArReceipt arReceipt = null;
	 * 
	 * // Initialize EJB Home
	 * 
	 * try {
	 * 
	 * arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
	 * arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME,
	 * LocalArReceiptBatchHome.class); adCompanyHome =
	 * (LocalAdCompanyHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	 * glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME,
	 * LocalGlFunctionalCurrencyHome.class); adDocumentSequenceAssignmentHome =
	 * (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,
	 * LocalAdDocumentSequenceAssignmentHome.class);
	 * adBranchDocumentSequenceAssignmentHome =
	 * (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
	 * LocalAdBranchDocumentSequenceAssignmentHome.class); adBankAccountHome =
	 * (LocalAdBankAccountHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME,
	 * LocalAdBankAccountHome.class); arCustomerHome =
	 * (LocalArCustomerHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
	 * glFunctionalCurrencyRateHome =
	 * (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME,
	 * LocalGlFunctionalCurrencyRateHome.class); arDistributionRecordHome =
	 * (LocalArDistributionRecordHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME,
	 * LocalArDistributionRecordHome.class); adApprovalHome =
	 * (LocalAdApprovalHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
	 * adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME,
	 * LocalAdAmountLimitHome.class); adApprovalUserHome =
	 * (LocalAdApprovalUserHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME,
	 * LocalAdApprovalUserHome.class); adApprovalQueueHome =
	 * (LocalAdApprovalQueueHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME,
	 * LocalAdApprovalQueueHome.class); adPreferenceHome =
	 * (LocalAdPreferenceHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
	 * LocalAdPreferenceHome.class);
	 * 
	 * } catch (NamingException ex) {
	 * 
	 * throw new EJBException(ex.getMessage());
	 * 
	 * }
	 * 
	 * try {
	 * 
	 * 
	 * // validate if receipt is already deleted
	 * 
	 * try {
	 * 
	 * if (details.getRctCode() != null) {
	 * 
	 * arReceipt = arReceiptHome.findByPrimaryKey(details.getRctCode());
	 * 
	 * }
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * throw new GlobalRecordAlreadyDeletedException();
	 * 
	 * }
	 * 
	 * // validate if receipt is already posted, void, approved or pending
	 * 
	 * if (details.getRctCode() != null && details.getRctVoid() == EJBCommon.FALSE)
	 * {
	 * 
	 * if (arReceipt.getRctApprovalStatus() != null) {
	 * 
	 * if (arReceipt.getRctApprovalStatus().equals("APPROVED") ||
	 * arReceipt.getRctApprovalStatus().equals("N/A")) {
	 * 
	 * throw new GlobalTransactionAlreadyApprovedException();
	 * 
	 * } else if (arReceipt.getRctApprovalStatus().equals("PENDING")) {
	 * 
	 * throw new GlobalTransactionAlreadyPendingException();
	 * 
	 * }
	 * 
	 * }
	 * 
	 * if (arReceipt.getRctPosted() == EJBCommon.TRUE) {
	 * 
	 * throw new GlobalTransactionAlreadyPostedException();
	 * 
	 * } else if (arReceipt.getRctVoid() == EJBCommon.TRUE) {
	 * 
	 * throw new GlobalTransactionAlreadyVoidException();
	 * 
	 * }
	 * 
	 * }
	 * 
	 * // check void
	 * 
	 * if (details.getRctCode() != null && details.getRctVoid() == EJBCommon.TRUE) {
	 * 
	 * if (arReceipt.getRctVoid() == EJBCommon.TRUE) {
	 * 
	 * throw new GlobalTransactionAlreadyVoidException();
	 * 
	 * }
	 * 
	 * // check if receipt is already deposited
	 * 
	 * if (!arReceipt.getCmFundTransferReceipts().isEmpty()){
	 * 
	 * throw new GlobalRecordAlreadyAssignedException ();
	 * 
	 * }
	 * 
	 * if (arReceipt.getRctPosted() == EJBCommon.TRUE) {
	 * 
	 * // generate approval status
	 * 
	 * String RCT_APPRVL_STATUS = null;
	 * 
	 * if (!isDraft) {
	 * 
	 * LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
	 * 
	 * // check if ar receipt approval is enabled
	 * 
	 * if (adApproval.getAprEnableArReceipt() == EJBCommon.FALSE) {
	 * 
	 * RCT_APPRVL_STATUS = "N/A";
	 * 
	 * } else {
	 * 
	 * // check if receipt is self approved
	 * 
	 * LocalAdAmountLimit adAmountLimit = null;
	 * 
	 * try {
	 * 
	 * adAmountLimit =
	 * adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR RECEIPT", "REQUESTER",
	 * details.getRctLastModifiedBy(), AD_CMPNY);
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * throw new GlobalNoApprovalRequesterFoundException();
	 * 
	 * }
	 * 
	 * if (arReceipt.getRctAmount() <= adAmountLimit.getCalAmountLimit()) {
	 * 
	 * RCT_APPRVL_STATUS = "N/A";
	 * 
	 * } else {
	 * 
	 * // for approval, create approval queue
	 * 
	 * Collection adAmountLimits =
	 * adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AR RECEIPT",
	 * adAmountLimit.getCalAmountLimit(), AD_CMPNY);
	 * 
	 * if (adAmountLimits.isEmpty()) {
	 * 
	 * Collection adApprovalUsers =
	 * adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
	 * adAmountLimit.getCalCode(), AD_CMPNY);
	 * 
	 * if (adApprovalUsers.isEmpty()) {
	 * 
	 * throw new GlobalNoApprovalApproverFoundException();
	 * 
	 * }
	 * 
	 * Iterator j = adApprovalUsers.iterator();
	 * 
	 * while (j.hasNext()) {
	 * 
	 * LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
	 * 
	 * LocalAdApprovalQueue adApprovalQueue =
	 * adApprovalQueueHome.create("AR RECEIPT", arReceipt.getRctCode(),
	 * arReceipt.getRctNumber(), arReceipt.getRctDate(),
	 * adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
	 * 
	 * adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
	 * 
	 * }
	 * 
	 * } else {
	 * 
	 * boolean isApprovalUsersFound = false;
	 * 
	 * Iterator i = adAmountLimits.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();
	 * 
	 * if (arReceipt.getRctAmount() <= adNextAmountLimit.getCalAmountLimit()) {
	 * 
	 * Collection adApprovalUsers =
	 * adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
	 * adAmountLimit.getCalCode(), AD_CMPNY);
	 * 
	 * Iterator j = adApprovalUsers.iterator();
	 * 
	 * while (j.hasNext()) {
	 * 
	 * isApprovalUsersFound = true;
	 * 
	 * LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
	 * 
	 * LocalAdApprovalQueue adApprovalQueue =
	 * adApprovalQueueHome.create("AR RECEIPT", arReceipt.getRctCode(),
	 * arReceipt.getRctNumber(), arReceipt.getRctDate(),
	 * adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
	 * 
	 * adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
	 * 
	 * }
	 * 
	 * break;
	 * 
	 * } else if (!i.hasNext()) {
	 * 
	 * Collection adApprovalUsers =
	 * adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
	 * adNextAmountLimit.getCalCode(), AD_CMPNY);
	 * 
	 * Iterator j = adApprovalUsers.iterator();
	 * 
	 * while (j.hasNext()) {
	 * 
	 * isApprovalUsersFound = true;
	 * 
	 * LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
	 * 
	 * LocalAdApprovalQueue adApprovalQueue =
	 * adApprovalQueueHome.create("AR RECEIPT", arReceipt.getRctCode(),
	 * arReceipt.getRctNumber(), arReceipt.getRctDate(),
	 * adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH,
	 * AD_CMPNY);
	 * 
	 * adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
	 * 
	 * }
	 * 
	 * break;
	 * 
	 * }
	 * 
	 * adAmountLimit = adNextAmountLimit;
	 * 
	 * }
	 * 
	 * if (!isApprovalUsersFound) {
	 * 
	 * throw new GlobalNoApprovalApproverFoundException();
	 * 
	 * }
	 * 
	 * }
	 * 
	 * RCT_APPRVL_STATUS = "PENDING"; } } }
	 * 
	 * 
	 * 
	 * // reverse distribution records
	 * 
	 * Collection arDistributionRecords = arReceipt.getArDistributionRecords();
	 * ArrayList list = new ArrayList();
	 * 
	 * Iterator i = arDistributionRecords.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * LocalArDistributionRecord arDistributionRecord =
	 * (LocalArDistributionRecord)i.next();
	 * 
	 * list.add(arDistributionRecord);
	 * 
	 * }
	 * 
	 * i = list.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * LocalArDistributionRecord arDistributionRecord =
	 * (LocalArDistributionRecord)i.next(); System.out.println("Check Point A");
	 * this.addArDrEntry(arReceipt.getArDrNextLine(),
	 * arDistributionRecord.getDrClass(), arDistributionRecord.getDrDebit() ==
	 * EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
	 * arDistributionRecord.getDrAmount(), EJBCommon.TRUE,
	 * arDistributionRecord.getGlChartOfAccount().getCoaCode(), arReceipt,
	 * arDistributionRecord.getArAppliedInvoice(), AD_BRNCH, AD_CMPNY);
	 * 
	 * }
	 * 
	 * LocalAdPreference adPreference =
	 * adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	 * 
	 * if (RCT_APPRVL_STATUS != null && RCT_APPRVL_STATUS.equals("N/A") &&
	 * adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
	 * 
	 * arReceipt.setRctVoid(EJBCommon.TRUE);
	 * this.executeArRctPost(arReceipt.getRctCode(), details.getRctLastModifiedBy(),
	 * AD_BRNCH, AD_CMPNY);
	 * 
	 * }
	 * 
	 * // set void approval status
	 * 
	 * arReceipt.setRctVoidApprovalStatus(RCT_APPRVL_STATUS);
	 * 
	 * } else {
	 * 
	 * // release invoice lock
	 * 
	 * Collection arLockedAppliedInvoices = arReceipt.getArAppliedInvoices();
	 * 
	 * Iterator aiIter = arLockedAppliedInvoices.iterator();
	 * 
	 * while (aiIter.hasNext()) {
	 * 
	 * LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)
	 * aiIter.next();
	 * 
	 * arAppliedInvoice.getArInvoicePaymentSchedule().setIpsLock(EJBCommon.FALSE);
	 * 
	 * }
	 * 
	 * }
	 * 
	 * arReceipt.setRctVoid(EJBCommon.TRUE);
	 * arReceipt.setRctLastModifiedBy(details.getRctLastModifiedBy());
	 * arReceipt.setRctDateLastModified(details.getRctDateLastModified());
	 * 
	 * return arReceipt.getRctCode();
	 * 
	 * }
	 * 
	 * // validate if document number is unique document number is automatic then
	 * set next sequence
	 * 
	 * LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment =
	 * null; LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
	 * 
	 * if (details.getRctCode() == null) {
	 * 
	 * try {
	 * 
	 * adDocumentSequenceAssignment =
	 * adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT", AD_CMPNY);
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * }
	 * 
	 * try {
	 * 
	 * adBranchDocumentSequenceAssignment =
	 * adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
	 * adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * }
	 * 
	 * LocalArReceipt arExistingReceipt = null;
	 * 
	 * try {
	 * 
	 * arExistingReceipt = arReceiptHome.findByRctNumberAndBrCode(
	 * details.getRctNumber(), AD_BRNCH, AD_CMPNY);
	 * 
	 * } catch (FinderException ex) { }
	 * 
	 * if (arExistingReceipt != null) {
	 * 
	 * throw new GlobalDocumentNumberNotUniqueException();
	 * 
	 * }
	 * 
	 * if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType()
	 * == 'A' && (details.getRctNumber() == null ||
	 * details.getRctNumber().trim().length() == 0)) {
	 * 
	 * while (true) {
	 * 
	 * if (adBranchDocumentSequenceAssignment == null ||
	 * adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
	 * 
	 * try {
	 * 
	 * arReceiptHome.findByRctNumberAndBrCode(adDocumentSequenceAssignment.
	 * getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
	 * adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.
	 * incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * details.setRctNumber(adDocumentSequenceAssignment.getDsaNextSequence());
	 * adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.
	 * incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
	 * break;
	 * 
	 * }
	 * 
	 * } else {
	 * 
	 * try {
	 * 
	 * arReceiptHome.findByRctNumberAndBrCode(adBranchDocumentSequenceAssignment.
	 * getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
	 * adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.
	 * incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()
	 * ));
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * details.setRctNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence())
	 * ; adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.
	 * incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()
	 * )); break;
	 * 
	 * }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * } else {
	 * 
	 * LocalArReceipt arExistingReceipt = null;
	 * 
	 * try {
	 * 
	 * arExistingReceipt = arReceiptHome.findByRctNumberAndBrCode(
	 * details.getRctNumber(), AD_BRNCH, AD_CMPNY);
	 * 
	 * } catch (FinderException ex) { }
	 * 
	 * if (arExistingReceipt != null &&
	 * !arExistingReceipt.getRctCode().equals(details.getRctCode())) {
	 * 
	 * throw new GlobalDocumentNumberNotUniqueException();
	 * 
	 * }
	 * 
	 * if (arReceipt.getRctNumber() != details.getRctNumber() &&
	 * (details.getRctNumber() == null || details.getRctNumber().trim().length() ==
	 * 0)) {
	 * 
	 * details.setRctNumber(arReceipt.getRctNumber());
	 * 
	 * }
	 * 
	 * }
	 * 
	 * 
	 * // validate if conversion date exists
	 * 
	 * try {
	 * 
	 * if (details.getRctConversionDate() != null) {
	 * 
	 * 
	 * LocalGlFunctionalCurrency glValidateFunctionalCurrency =
	 * glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY); LocalAdCompany
	 * adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	 * 
	 * if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {
	 * 
	 * LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	 * glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency
	 * .getFcCode(), details.getRctConversionDate(), AD_CMPNY);
	 * 
	 * } else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){
	 * 
	 * LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	 * glFunctionalCurrencyRateHome.findByFcCodeAndDate(
	 * adCompany.getGlFunctionalCurrency().getFcCode(),
	 * details.getRctConversionDate(), AD_CMPNY);
	 * 
	 * }
	 * 
	 * }
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * throw new GlobalConversionDateNotExistException();
	 * 
	 * }
	 * 
	 * 
	 * // used in checking if receipt should re-generate distribution records and
	 * re-calculate taxes boolean isRecalculate = true;
	 * 
	 * // create receipt System.out.println("insert line herer"); if
	 * (details.getRctCode() == null) {
	 * 
	 * arReceipt = arReceiptHome.create("COLLECTION", details.getRctDescription(),
	 * details.getRctDate(), details.getRctNumber(),
	 * details.getRctReferenceNumber(), details.getRctAmount(),
	 * details.getRctConversionDate(), details.getRctConversionRate(), null,
	 * details.getRctPaymentMethod(), EJBCommon.FALSE, 0d, null, null,
	 * EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
	 * null, details.getRctCreatedBy(), details.getRctDateCreated(),
	 * details.getRctLastModifiedBy(), details.getRctDateLastModified(), null, null,
	 * null, null, EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null,
	 * AD_BRNCH, AD_CMPNY);
	 * 
	 * } else {
	 * 
	 * // check if critical fields are changed
	 * 
	 * if (!arReceipt.getAdBankAccount().getBaName().equals(BA_NM) ||
	 * !arReceipt.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE) ||
	 * aiList.size() != arReceipt.getArAppliedInvoices().size()) {
	 * System.out.println("recalculate"); isRecalculate = true;
	 * 
	 * } else if (aiList.size() == arReceipt.getArAppliedInvoices().size()) {
	 * System.out.println("loop get list"); Iterator aiIter =
	 * arReceipt.getArAppliedInvoices().iterator(); Iterator aiListIter =
	 * aiList.iterator();
	 * 
	 * while (aiIter.hasNext()) { System.out.println("0.1"); LocalArAppliedInvoice
	 * arAppliedInvoice = (LocalArAppliedInvoice)aiIter.next();
	 * System.out.println("1"); ArModAppliedInvoiceDetails mdetails =
	 * (ArModAppliedInvoiceDetails)aiListIter.next(); System.out.println("1.1"); if
	 * (!arAppliedInvoice.getArInvoicePaymentSchedule().getIpsCode().equals(mdetails
	 * .getAiIpsCode()) || arAppliedInvoice.getAiApplyAmount() !=
	 * mdetails.getAiApplyAmount() || arAppliedInvoice.getAiCreditableWTax() !=
	 * mdetails.getAiCreditableWTax() || arAppliedInvoice.getAiDiscountAmount() !=
	 * mdetails.getAiDiscountAmount() || arAppliedInvoice.getAiAppliedDeposit() !=
	 * mdetails.getAiAppliedDeposit() ||
	 * arAppliedInvoice.getAiAllocatedPaymentAmount() !=
	 * mdetails.getAiAllocatedPaymentAmount()) {
	 * 
	 * isRecalculate = true; break;
	 * 
	 * }
	 * 
	 * isRecalculate = false;
	 * 
	 * } System.out.println("loop get list end"); } else {
	 * 
	 * isRecalculate = false;
	 * 
	 * }
	 * 
	 * arReceipt.setRctDescription(details.getRctDescription());
	 * arReceipt.setRctDate(details.getRctDate());
	 * arReceipt.setRctNumber(details.getRctNumber());
	 * arReceipt.setRctReferenceNumber(details.getRctReferenceNumber());
	 * arReceipt.setRctAmount(details.getRctAmount());
	 * arReceipt.setRctConversionDate(details.getRctConversionDate());
	 * arReceipt.setRctConversionRate(details.getRctConversionRate());
	 * arReceipt.setRctPaymentMethod(details.getRctPaymentMethod());
	 * arReceipt.setRctLastModifiedBy(details.getRctLastModifiedBy());
	 * arReceipt.setRctDateLastModified(details.getRctDateLastModified());
	 * arReceipt.setRctReasonForRejection(null); arReceipt.setRctCustomerName(null);
	 * 
	 * }
	 * 
	 * LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM,
	 * AD_CMPNY); adBankAccount.addArReceipt(arReceipt);
	 * 
	 * LocalGlFunctionalCurrency glFunctionalCurrency =
	 * glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
	 * glFunctionalCurrency.addArReceipt(arReceipt);
	 * 
	 * LocalArCustomer arCustomer =
	 * arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
	 * arCustomer.addArReceipt(arReceipt);
	 * 
	 * System.out.println("enter"); System.out.println(details.getRctCustomerName()
	 * + "this"); if(details.getRctCustomerName().length() > 0 &&
	 * !arCustomer.getCstName().equals(details.getRctCustomerName())) {
	 * arReceipt.setRctCustomerName(details.getRctCustomerName()); }
	 * 
	 * try {
	 * 
	 * LocalArReceiptBatch arReceiptBatch = arReceiptBatchHome.findByRbName(RB_NM,
	 * AD_BRNCH, AD_CMPNY); arReceiptBatch.addArReceipt(arReceipt);
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * }
	 * 
	 * if (isRecalculate) {
	 * 
	 * // remove all distribution records
	 * 
	 * Collection arDistributionRecords = arReceipt.getArDistributionRecords();
	 * 
	 * Iterator i = arDistributionRecords.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * LocalArDistributionRecord arDistributionRecord =
	 * (LocalArDistributionRecord)i.next();
	 * 
	 * i.remove();
	 * 
	 * arDistributionRecord.remove();
	 * 
	 * }
	 * 
	 * 
	 * // release ips locks and remove all applied invoices
	 * 
	 * Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();
	 * 
	 * i = arAppliedInvoices.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();
	 * 
	 * arAppliedInvoice.getArInvoicePaymentSchedule().setIpsLock(EJBCommon.FALSE);
	 * 
	 * i.remove();
	 * 
	 * arAppliedInvoice.remove();
	 * 
	 * }
	 * 
	 * double totalScAmount = 0; // add new applied vouchers and distribution record
	 * 
	 * i = aiList.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * ArModAppliedInvoiceDetails mAiDetails = (ArModAppliedInvoiceDetails)
	 * i.next();
	 * 
	 * LocalArAppliedInvoice arAppliedInvoice = this.addArAiEntry(mAiDetails,
	 * arReceipt, AD_CMPNY);
	 * 
	 * // create cred. withholding tax distribution record if necessary
	 * 
	 * if (mAiDetails.getAiCreditableWTax() > 0) {
	 * 
	 * Integer WTC_COA_CODE = null;
	 * 
	 * if (arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getArWithholdingTaxCode().getGlChartOfAccount() != null) {
	 * 
	 * WTC_COA_CODE = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getArWithholdingTaxCode().getGlChartOfAccount().getCoaCode();
	 * 
	 * } else {
	 * 
	 * LocalAdPreference adPreference =
	 * adPreferenceHome.findByPrfAdCompany(AD_CMPNY); WTC_COA_CODE =
	 * adPreference.getArWithholdingTaxCode().getGlChartOfAccount().getCoaCode();
	 * 
	 * } System.out.println("Check Point B");
	 * this.addArDrEntry(arReceipt.getArDrNextLine(), "W-TAX", EJBCommon.TRUE,
	 * arAppliedInvoice.getAiCreditableWTax(), EJBCommon.FALSE, WTC_COA_CODE,
	 * arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	 * 
	 * }
	 * 
	 * // create discount distribution records if necessary
	 * 
	 * if (arAppliedInvoice.getAiDiscountAmount() != 0) {
	 * 
	 * short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
	 * 
	 * // get discount percent
	 * 
	 * double DISCOUNT_PERCENT =
	 * EJBCommon.roundIt(arAppliedInvoice.getAiDiscountAmount() /
	 * (arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax()
	 * + arAppliedInvoice.getAiDiscountAmount() +
	 * arAppliedInvoice.getAiAppliedDeposit()), (short)6); DISCOUNT_PERCENT =
	 * EJBCommon.roundIt(DISCOUNT_PERCENT * ((arAppliedInvoice.getAiApplyAmount() +
	 * arAppliedInvoice.getAiCreditableWTax() +
	 * arAppliedInvoice.getAiDiscountAmount() +
	 * arAppliedInvoice.getAiAppliedDeposit()) /
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvAmountDue
	 * ()), (short)6);
	 * 
	 * Collection arDiscountDistributionRecords =
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getArDistributionRecords();
	 * 
	 * // get total debit and credit for rounding difference calculation
	 * 
	 * double TOTAL_DEBIT = 0d; double TOTAL_CREDIT = 0d; boolean
	 * isRoundingDifferenceCalculated = false;
	 * 
	 * Iterator j = arDiscountDistributionRecords.iterator();
	 * 
	 * while (j.hasNext()) {
	 * 
	 * LocalArDistributionRecord arDiscountDistributionRecord =
	 * (LocalArDistributionRecord)j.next();
	 * 
	 * if (arDiscountDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
	 * 
	 * TOTAL_DEBIT += EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount() *
	 * DISCOUNT_PERCENT, precisionUnit);
	 * 
	 * } else {
	 * 
	 * TOTAL_CREDIT += EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount()
	 * * DISCOUNT_PERCENT, precisionUnit);
	 * 
	 * }
	 * 
	 * }
	 * 
	 * 
	 * j = arDiscountDistributionRecords.iterator();
	 * 
	 * while (j.hasNext()) {
	 * 
	 * LocalArDistributionRecord arDiscountDistributionRecord =
	 * (LocalArDistributionRecord)j.next();
	 * 
	 * if (arDiscountDistributionRecord.getDrClass().equals("RECEIVABLE")) continue;
	 * 
	 * double DR_AMNT = EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount()
	 * * DISCOUNT_PERCENT, precisionUnit);
	 * 
	 * // calculate rounding difference if necessary
	 * 
	 * if (arDiscountDistributionRecord.getDrDebit() == EJBCommon.FALSE &&
	 * TOTAL_DEBIT != TOTAL_CREDIT && !isRoundingDifferenceCalculated) {
	 * 
	 * DR_AMNT = DR_AMNT + TOTAL_DEBIT - TOTAL_CREDIT;
	 * 
	 * isRoundingDifferenceCalculated = true;
	 * 
	 * }
	 * 
	 * if (arDiscountDistributionRecord.getDrClass().equals("REVENUE")) {
	 * System.out.println("Check Point C");
	 * this.addArDrEntry(arReceipt.getArDrNextLine(), "SALES DISCOUNT",
	 * EJBCommon.TRUE, DR_AMNT, EJBCommon.FALSE,
	 * arReceipt.getAdBankAccount().getBaCoaGlSalesDiscount(), arReceipt,
	 * arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	 * 
	 * } else {
	 * 
	 * System.out.println("Check Point D");
	 * this.addArDrEntry(arReceipt.getArDrNextLine(),
	 * arDiscountDistributionRecord.getDrClass(),
	 * arDiscountDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE
	 * : EJBCommon.TRUE, DR_AMNT, EJBCommon.FALSE,
	 * arDiscountDistributionRecord.getGlChartOfAccount().getCoaCode(), arReceipt,
	 * arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	 * 
	 * }
	 * 
	 * } }
	 * 
	 * // create applied deposit distribution records if necessary
	 * 
	 * if (arAppliedInvoice.getAiAppliedDeposit() != 0) {
	 * 
	 * LocalAdPreference adPreference =
	 * adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	 * 
	 * if(adPreference.getPrfArGlCoaCustomerDepositAccount() == null) throw new
	 * AdPRFCoaGlCustomerDepositAccountNotFoundException();
	 * System.out.println("Check Point E");
	 * this.addArDrEntry(arReceipt.getArDrNextLine(), "APPLIED DEPOSIT",
	 * EJBCommon.TRUE, arAppliedInvoice.getAiAppliedDeposit(), EJBCommon.FALSE,
	 * adPreference.getPrfArGlCoaCustomerDepositAccount(), arReceipt,
	 * arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	 * 
	 * }
	 * 
	 * // Get Service Charge Records Collection arScDistributionRecords =
	 * arDistributionRecordHome.findDrsByDrClassAndInvCode("SC",
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(),
	 * AD_CMPNY);
	 * 
	 * Iterator scIter = arScDistributionRecords.iterator();
	 * 
	 * double applyAmount = arAppliedInvoice.getAiApplyAmount() +
	 * arAppliedInvoice.getAiCreditableWTax() +
	 * arAppliedInvoice.getAiDiscountAmount() +
	 * arAppliedInvoice.getAiAppliedDeposit(); double dueAmount =
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvAmountDue
	 * ();
	 * 
	 * double scAmount = 0;
	 * 
	 * while(scIter.hasNext()){
	 * 
	 * LocalArDistributionRecord arScDistributionRecord =
	 * (LocalArDistributionRecord)scIter.next(); scAmount +=
	 * arScDistributionRecord.getDrAmount() * (applyAmount/dueAmount);
	 * System.out.println("Check Point F");
	 * this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE",
	 * EJBCommon.FALSE,arScDistributionRecord.getDrAmount() *
	 * (applyAmount/dueAmount),EJBCommon.FALSE,
	 * arScDistributionRecord.getDrScAccount(), arReceipt, arAppliedInvoice,
	 * AD_BRNCH, AD_CMPNY); System.out.println("Check Point G");
	 * this.addArDrEntry(arReceipt.getArDrNextLine(), "SC", EJBCommon.TRUE,
	 * arScDistributionRecord.getDrAmount() * (applyAmount/dueAmount),
	 * EJBCommon.FALSE, arScDistributionRecord.getGlChartOfAccount().getCoaCode(),
	 * arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	 * 
	 * }
	 * 
	 * totalScAmount += scAmount;
	 * 
	 * 
	 * 
	 * 
	 * // get receivable account TODO: ALWAYS GETTING ERROR BELOW try{
	 * 
	 * System.out.println("first inv:" +
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode());
	 * LocalArDistributionRecord arDistributionRecord =
	 * arDistributionRecordHome.findByDrClassAndInvCode("RECEIVABLE",
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(),
	 * AD_CMPNY); System.out.println("Check Point H");
	 * 
	 * 
	 * this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE", EJBCommon.FALSE,
	 * arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax()
	 * + arAppliedInvoice.getAiDiscountAmount() +
	 * arAppliedInvoice.getAiAppliedDeposit() , EJBCommon.FALSE,
	 * arDistributionRecord.getGlChartOfAccount().getCoaCode(), arReceipt,
	 * arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	 * 
	 * 
	 * } catch (Exception ex){ System.out.println("error:" + ex.toString());
	 * System.out.println("MULTIPLE RECEIVABLE");
	 * 
	 * Collection arDrReceivables =
	 * arDistributionRecordHome.findDrsByDrClassAndInvCode("RECEIVABLE",
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(),
	 * AD_CMPNY); Iterator x = arDrReceivables.iterator(); double
	 * reamingAppliedAmount = arAppliedInvoice.getAiApplyAmount();
	 * 
	 * double amountRemaining = 0d; double totalInvAmount = 0d; double totalPaid =
	 * 0d; double amountDue = 0d; double distributionAmount = 0d;
	 * System.out.println("reamingAppliedAmount="+reamingAppliedAmount);
	 * 
	 * //get total invoice amount while(x.hasNext()){ LocalArDistributionRecord
	 * arDistributionRecord = (LocalArDistributionRecord)x.next(); totalInvAmount +=
	 * EJBCommon.roundIt(arDistributionRecord.getDrAmount(),
	 * this.getGlFcPrecisionUnit(AD_CMPNY)); } totalInvAmount =
	 * EJBCommon.roundIt(totalInvAmount, this.getGlFcPrecisionUnit(AD_CMPNY));
	 * 
	 * System.out.println("tsad"); //get total paid amountDue =
	 * mAiDetails.getAiIpsAmountDue(); totalPaid = EJBCommon.roundIt(totalInvAmount
	 * - amountDue, this.getGlFcPrecisionUnit(AD_CMPNY));
	 * 
	 * 
	 * amountRemaining = reamingAppliedAmount; x = arDrReceivables.iterator();
	 * 
	 * while(x.hasNext()){ LocalArDistributionRecord arDistributionRecord =
	 * (LocalArDistributionRecord)x.next();
	 * System.out.println("-------->arDistributionRecord.getDrAmount()="+
	 * arDistributionRecord.getDrAmount());
	 * System.out.println("-------->reamingAppliedAmount="+reamingAppliedAmount);
	 * System.out.println("-------->amount remaining=" + amountRemaining);
	 * System.out.println(" the amount due is : " + amountDue);
	 * System.out.println(" the total paid is : " + totalPaid);
	 * 
	 * if(amountRemaining <= 0 ){ System.out.println("break"); break; }
	 * 
	 * distributionAmount = EJBCommon.roundIt(arDistributionRecord.getDrAmount()-
	 * totalPaid, this.getGlFcPrecisionUnit(AD_CMPNY)); if(distributionAmount <= 0){
	 * totalPaid -=arDistributionRecord.getDrAmount(); continue; }
	 * if(amountRemaining >= distributionAmount){
	 * System.out.println("Still Remain");
	 * 
	 * this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE", EJBCommon.FALSE,
	 * distributionAmount , EJBCommon.FALSE,
	 * arDistributionRecord.getGlChartOfAccount().getCoaCode(), arReceipt,
	 * arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	 * 
	 * 
	 * System.out.println("amount remain:" + amountRemaining);
	 * 
	 * }else{ System.out.println("zero remaining"); System.out.println("dr amount: "
	 * + arDistributionRecord.getDrAmount() + "  and amount rem: "+
	 * amountRemaining); this.addArDrEntry(arReceipt.getArDrNextLine(),
	 * "RECEIVABLE", EJBCommon.FALSE, amountRemaining , EJBCommon.FALSE,
	 * arDistributionRecord.getGlChartOfAccount().getCoaCode(), arReceipt,
	 * arAppliedInvoice, AD_BRNCH, AD_CMPNY); } System.out.println("finished");
	 * 
	 * System.out.println("the total paid is: " + totalPaid);
	 * System.out.println("the distribute : " + distributionAmount);
	 * System.out.println("the amount remaining : " +amountRemaining);
	 * 
	 * amountRemaining -= distributionAmount ;
	 * System.out.println("the amount remaining now  : " +amountRemaining);
	 * totalPaid = 0;
	 * 
	 * }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * }
	 * 
	 * 
	 * 
	 * // reverse deferred tax if necessary
	 * 
	 * LocalArTaxCode arTaxCode =
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArTaxCode();
	 * 
	 * if (!arTaxCode.getTcType().equals("NONE") &&
	 * !arTaxCode.getTcType().equals("EXEMPT") && arTaxCode.getTcInterimAccount() !=
	 * null) {
	 * 
	 * try {
	 * 
	 * LocalArDistributionRecord arDeferredDistributionRecord =
	 * arDistributionRecordHome.findByDrClassAndInvCode("DEFERRED TAX",
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(),
	 * AD_CMPNY);
	 * 
	 * double DR_AMNT =
	 * EJBCommon.roundIt((arDeferredDistributionRecord.getDrAmount() /
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvAmountDue
	 * ()) * (arAppliedInvoice.getAiApplyAmount() +
	 * arAppliedInvoice.getAiCreditableWTax() +
	 * arAppliedInvoice.getAiDiscountAmount() +
	 * arAppliedInvoice.getAiAppliedDeposit()),
	 * this.getGlFcPrecisionUnit(AD_CMPNY)); System.out.println("Check Point I");
	 * this.addArDrEntry(arReceipt.getArDrNextLine(), "DEFERRED TAX",
	 * EJBCommon.TRUE, DR_AMNT, EJBCommon.FALSE,
	 * arDeferredDistributionRecord.getGlChartOfAccount().getCoaCode(), arReceipt,
	 * arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	 * 
	 * 
	 * System.out.println("Check Point J");
	 * this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE,
	 * DR_AMNT, EJBCommon.FALSE,
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArTaxCode().
	 * getGlChartOfAccount().getCoaCode(), arReceipt, arAppliedInvoice, AD_BRNCH,
	 * AD_CMPNY);
	 * 
	 * 
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * // create cash distribution record System.out.println(
	 * "-------------------------------->arReceipt.getRctAmount()="+arReceipt.
	 * getRctAmount()); if(arReceipt.getRctAmount() != 0){
	 * System.out.println("Check Point K");
	 * this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE,
	 * arReceipt.getRctAmount(), EJBCommon.FALSE,
	 * arReceipt.getAdBankAccount().getBaCoaGlCashAccount(), arReceipt, null,
	 * AD_BRNCH, AD_CMPNY); } }
	 * 
	 * // generate approval status
	 * 
	 * String RCT_APPRVL_STATUS = null;
	 * 
	 * if (!isDraft) {
	 * 
	 * LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
	 * 
	 * // check if ar receipt approval is enabled
	 * 
	 * if (adApproval.getAprEnableArReceipt() == EJBCommon.FALSE) {
	 * 
	 * RCT_APPRVL_STATUS = "N/A";
	 * 
	 * } else {
	 * 
	 * // check if receipt is self approved
	 * 
	 * LocalAdAmountLimit adAmountLimit = null;
	 * 
	 * try {
	 * 
	 * adAmountLimit =
	 * adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR RECEIPT", "REQUESTER",
	 * details.getRctLastModifiedBy(), AD_CMPNY);
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * throw new GlobalNoApprovalRequesterFoundException();
	 * 
	 * }
	 * 
	 * if (arReceipt.getRctAmount() <= adAmountLimit.getCalAmountLimit()) {
	 * 
	 * RCT_APPRVL_STATUS = "N/A";
	 * 
	 * } else {
	 * 
	 * // for approval, create approval queue
	 * 
	 * Collection adAmountLimits =
	 * adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AR RECEIPT",
	 * adAmountLimit.getCalAmountLimit(), AD_CMPNY);
	 * 
	 * if (adAmountLimits.isEmpty()) {
	 * 
	 * Collection adApprovalUsers =
	 * adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
	 * adAmountLimit.getCalCode(), AD_CMPNY);
	 * 
	 * if (adApprovalUsers.isEmpty()) {
	 * 
	 * throw new GlobalNoApprovalApproverFoundException();
	 * 
	 * }
	 * 
	 * Iterator j = adApprovalUsers.iterator();
	 * 
	 * while (j.hasNext()) {
	 * 
	 * LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
	 * 
	 * LocalAdApprovalQueue adApprovalQueue =
	 * adApprovalQueueHome.create("AR RECEIPT", arReceipt.getRctCode(),
	 * arReceipt.getRctNumber(), arReceipt.getRctDate(),
	 * adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
	 * 
	 * adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
	 * 
	 * }
	 * 
	 * } else {
	 * 
	 * boolean isApprovalUsersFound = false;
	 * 
	 * Iterator i = adAmountLimits.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();
	 * 
	 * if (arReceipt.getRctAmount() <= adNextAmountLimit.getCalAmountLimit()) {
	 * 
	 * Collection adApprovalUsers =
	 * adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
	 * adAmountLimit.getCalCode(), AD_CMPNY);
	 * 
	 * Iterator j = adApprovalUsers.iterator();
	 * 
	 * while (j.hasNext()) {
	 * 
	 * isApprovalUsersFound = true;
	 * 
	 * LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
	 * 
	 * LocalAdApprovalQueue adApprovalQueue =
	 * adApprovalQueueHome.create("AR RECEIPT", arReceipt.getRctCode(),
	 * arReceipt.getRctNumber(), arReceipt.getRctDate(),
	 * adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
	 * 
	 * adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
	 * 
	 * }
	 * 
	 * break;
	 * 
	 * } else if (!i.hasNext()) {
	 * 
	 * Collection adApprovalUsers =
	 * adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
	 * adNextAmountLimit.getCalCode(), AD_CMPNY);
	 * 
	 * Iterator j = adApprovalUsers.iterator();
	 * 
	 * while (j.hasNext()) {
	 * 
	 * isApprovalUsersFound = true;
	 * 
	 * LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
	 * 
	 * LocalAdApprovalQueue adApprovalQueue =
	 * adApprovalQueueHome.create("AR RECEIPT", arReceipt.getRctCode(),
	 * arReceipt.getRctNumber(), arReceipt.getRctDate(),
	 * adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH,
	 * AD_CMPNY);
	 * 
	 * adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
	 * 
	 * }
	 * 
	 * break;
	 * 
	 * }
	 * 
	 * adAmountLimit = adNextAmountLimit;
	 * 
	 * }
	 * 
	 * if (!isApprovalUsersFound) {
	 * 
	 * throw new GlobalNoApprovalApproverFoundException();
	 * 
	 * }
	 * 
	 * }
	 * 
	 * RCT_APPRVL_STATUS = "PENDING"; } } }
	 * 
	 * LocalAdPreference adPreference =
	 * adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	 * 
	 * if (RCT_APPRVL_STATUS != null && RCT_APPRVL_STATUS.equals("N/A") &&
	 * adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
	 * 
	 * this.executeArRctPost(arReceipt.getRctCode(),
	 * arReceipt.getRctLastModifiedBy(), AD_BRNCH, AD_CMPNY);
	 * 
	 * }
	 * 
	 * // set receipt approval status
	 * 
	 * arReceipt.setRctApprovalStatus(RCT_APPRVL_STATUS);
	 * 
	 * 
	 * 
	 * return arReceipt.getRctCode();
	 * 
	 * 
	 * } catch (Exception ex) {
	 * 
	 * ex.printStackTrace(); ctx.setRollbackOnly(); throw new EJBException
	 * (ex.getMessage());
	 * 
	 * }
	 * 
	 * 
	 * }
	 * 
	 * 
	 * private void addArDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double
	 * DR_AMNT, byte DR_RVRSD, Integer COA_CODE, LocalArReceipt arReceipt,
	 * LocalArAppliedInvoice arAppliedInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
	 * throws GlobalBranchAccountNumberInvalidException {
	 * 
	 * Debug.print("ArReceiptEntryControllerBean addArDrEntry");
	 * 
	 * LocalArDistributionRecordHome arDistributionRecordHome = null;
	 * LocalGlChartOfAccountHome glChartOfAccountHome = null; LocalAdCompanyHome
	 * adCompanyHome = null;
	 * 
	 * 
	 * // Initialize EJB Home
	 * 
	 * try {
	 * 
	 * arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME,
	 * LocalArDistributionRecordHome.class); glChartOfAccountHome =
	 * (LocalGlChartOfAccountHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME,
	 * LocalGlChartOfAccountHome.class); adCompanyHome =
	 * (LocalAdCompanyHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	 * 
	 * 
	 * } catch (NamingException ex) {
	 * 
	 * throw new EJBException(ex.getMessage());
	 * 
	 * }
	 * 
	 * try {
	 * 
	 * // get company
	 * 
	 * LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	 * 
	 * LocalGlChartOfAccount glChartOfAccount =
	 * glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH,
	 * AD_CMPNY);
	 * 
	 * // create distribution record
	 * 
	 * LocalArDistributionRecord arDistributionRecord =
	 * arDistributionRecordHome.create( DR_LN, DR_CLSS, DR_DBT,
	 * EJBCommon.roundIt(DR_AMNT,
	 * adCompany.getGlFunctionalCurrency().getFcPrecision()), EJBCommon.FALSE,
	 * DR_RVRSD, AD_CMPNY);
	 * 
	 * arReceipt.addArDistributionRecord(arDistributionRecord);
	 * glChartOfAccount.addArDistributionRecord(arDistributionRecord);
	 * 
	 * // to be used by gl journal interface for cross currency receipts if
	 * (arAppliedInvoice != null) {
	 * 
	 * arAppliedInvoice.addArDistributionRecord(arDistributionRecord);
	 * 
	 * }
	 * 
	 * } catch (Exception ex) {
	 * 
	 * ex.printStackTrace(); ctx.setRollbackOnly(); throw new EJBException
	 * (ex.getMessage());
	 * 
	 * }
	 * 
	 * }
	 * 
	 * 
	 * 
	 * private void executeArRctPost(Integer RCT_CODE, String USR_NM, Integer
	 * AD_BRNCH, Integer AD_CMPNY) throws GlobalRecordAlreadyDeletedException,
	 * GlobalTransactionAlreadyPostedException,
	 * GlobalTransactionAlreadyVoidPostedException,
	 * GlJREffectiveDateNoPeriodExistException,
	 * GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException {
	 * 
	 * Debug.print("ArReceiptEntryControllerBean executeArRctPost");
	 * 
	 * LocalArReceiptHome arReceiptHome = null; LocalAdCompanyHome adCompanyHome =
	 * null; LocalAdPreferenceHome adPreferenceHome = null; LocalGlSetOfBookHome
	 * glSetOfBookHome = null; LocalGlAccountingCalendarValueHome
	 * glAccountingCalendarValueHome = null; LocalGlJournalHome glJournalHome =
	 * null; LocalGlJournalBatchHome glJournalBatchHome = null;
	 * LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
	 * LocalGlJournalLineHome glJournalLineHome = null; LocalGlJournalSourceHome
	 * glJournalSourceHome = null; LocalGlJournalCategoryHome glJournalCategoryHome
	 * = null; LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
	 * LocalArDistributionRecordHome arDistributionRecordHome = null;
	 * LocalGlChartOfAccountHome glChartOfAccountHome = null; LocalAdBankAccountHome
	 * adBankAccountHome = null; LocalAdBankAccountBalanceHome
	 * adBankAccountBalanceHome = null; LocalGlForexLedgerHome glForexLedgerHome =
	 * null; LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
	 * 
	 * LocalArReceipt arReceipt = null;
	 * 
	 * // Initialize EJB Home
	 * 
	 * try {
	 * 
	 * arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
	 * adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	 * adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
	 * LocalAdPreferenceHome.class); glSetOfBookHome =
	 * (LocalGlSetOfBookHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
	 * glAccountingCalendarValueHome =
	 * (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME,
	 * LocalGlAccountingCalendarValueHome.class); glJournalHome =
	 * (LocalGlJournalHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
	 * glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME,
	 * LocalGlJournalBatchHome.class); glSuspenseAccountHome =
	 * (LocalGlSuspenseAccountHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME,
	 * LocalGlSuspenseAccountHome.class); glJournalLineHome =
	 * (LocalGlJournalLineHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,
	 * LocalGlJournalLineHome.class); glJournalSourceHome =
	 * (LocalGlJournalSourceHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME,
	 * LocalGlJournalSourceHome.class); glJournalCategoryHome =
	 * (LocalGlJournalCategoryHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME,
	 * LocalGlJournalCategoryHome.class); glFunctionalCurrencyHome =
	 * (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME,
	 * LocalGlFunctionalCurrencyHome.class); arDistributionRecordHome =
	 * (LocalArDistributionRecordHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME,
	 * LocalArDistributionRecordHome.class); glChartOfAccountHome =
	 * (LocalGlChartOfAccountHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME,
	 * LocalGlChartOfAccountHome.class); adBankAccountHome =
	 * (LocalAdBankAccountHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME,
	 * LocalAdBankAccountHome.class); adBankAccountBalanceHome =
	 * (LocalAdBankAccountBalanceHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME,
	 * LocalAdBankAccountBalanceHome.class); glForexLedgerHome =
	 * (LocalGlForexLedgerHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME,
	 * LocalGlForexLedgerHome.class); glFunctionalCurrencyRateHome =
	 * (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME,
	 * LocalGlFunctionalCurrencyRateHome.class);
	 * 
	 * } catch (NamingException ex) {
	 * 
	 * throw new EJBException(ex.getMessage());
	 * 
	 * }
	 * 
	 * try {
	 * 
	 * // validate if receipt is already deleted
	 * 
	 * try {
	 * 
	 * arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * throw new GlobalRecordAlreadyDeletedException();
	 * 
	 * }
	 * 
	 * // validate if receipt is already posted
	 * 
	 * if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() ==
	 * EJBCommon.TRUE) {
	 * 
	 * throw new GlobalTransactionAlreadyPostedException();
	 * 
	 * // validate if receipt void is already posted
	 * 
	 * } else if (arReceipt.getRctVoid() == EJBCommon.TRUE &&
	 * arReceipt.getRctVoidPosted() == EJBCommon.TRUE) {
	 * 
	 * throw new GlobalTransactionAlreadyVoidPostedException();
	 * 
	 * }
	 * 
	 * // post receipt
	 * 
	 * Collection arDepositReceipts = null; LocalArReceipt arDepositReceipt = null;
	 * LocalArCustomer arCustomer = arReceipt.getArCustomer();
	 * 
	 * if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() ==
	 * EJBCommon.FALSE) {
	 * 
	 * 
	 * if (arReceipt.getRctType().equals("COLLECTION")) {
	 * 
	 * double RCT_CRDTS = 0d; double RCT_AI_APPLD_DPSTS = 0d;
	 * 
	 * // increase amount paid in invoice payment schedules and invoice
	 * 
	 * Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();
	 * 
	 * Iterator i = arAppliedInvoices.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();
	 * 
	 * LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
	 * arAppliedInvoice.getArInvoicePaymentSchedule();
	 * 
	 * double AMOUNT_PAID = arAppliedInvoice.getAiApplyAmount() +
	 * arAppliedInvoice.getAiCreditableWTax() +
	 * arAppliedInvoice.getAiDiscountAmount() +
	 * arAppliedInvoice.getAiAppliedDeposit();
	 * 
	 * RCT_CRDTS += this.convertForeignToFunctionalCurrency(arAppliedInvoice.
	 * getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().
	 * getFcCode(), arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getGlFunctionalCurrency().getFcName(),
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getInvConversionDate(),
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getInvConversionRate(), arAppliedInvoice.getAiDiscountAmount() +
	 * arAppliedInvoice.getAiCreditableWTax(), AD_CMPNY);
	 * 
	 * RCT_AI_APPLD_DPSTS +=
	 * this.convertForeignToFunctionalCurrency(arAppliedInvoice.
	 * getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().
	 * getFcCode(), arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getGlFunctionalCurrency().getFcName(),
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getInvConversionDate(),
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getInvConversionRate(), arAppliedInvoice.getAiAppliedDeposit(), AD_CMPNY);
	 * 
	 * arInvoicePaymentSchedule.setIpsAmountPaid(EJBCommon.roundIt(
	 * arInvoicePaymentSchedule.getIpsAmountPaid() + AMOUNT_PAID,
	 * this.getGlFcPrecisionUnit(AD_CMPNY)));
	 * 
	 * arInvoicePaymentSchedule.getArInvoice().setInvAmountPaid(EJBCommon.roundIt(
	 * arInvoicePaymentSchedule.getArInvoice().getInvAmountPaid() + AMOUNT_PAID,
	 * this.getGlFcPrecisionUnit(AD_CMPNY)));
	 * 
	 * // release invoice lock
	 * 
	 * arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);
	 * 
	 * }
	 * 
	 * // increase customer applied deposit
	 * 
	 * try {
	 * 
	 * arDepositReceipts =
	 * arReceiptHome.findOpenDepositEnabledPostedRctByCstCustomerCode(arCustomer.
	 * getCstCustomerCode(), AD_CMPNY);
	 * 
	 * } catch (FinderException ex) { }
	 * 
	 * i = arDepositReceipts.iterator();
	 * 
	 * while(i.hasNext()) {
	 * 
	 * if(RCT_AI_APPLD_DPSTS == 0) break;
	 * 
	 * arDepositReceipt = (LocalArReceipt)i.next();
	 * 
	 * double temp = 0d;
	 * 
	 * // update applied deposit
	 * 
	 * if((arDepositReceipt.getRctAmount() -
	 * arDepositReceipt.getRctAppliedDeposit()) < RCT_AI_APPLD_DPSTS){
	 * 
	 * temp = arDepositReceipt.getRctAmount() -
	 * arDepositReceipt.getRctAppliedDeposit(); RCT_AI_APPLD_DPSTS -= temp;
	 * arDepositReceipt.setRctAppliedDeposit(arDepositReceipt.getRctAppliedDeposit()
	 * + temp);
	 * 
	 * } else {
	 * 
	 * temp = RCT_AI_APPLD_DPSTS; RCT_AI_APPLD_DPSTS -= temp;
	 * arDepositReceipt.setRctAppliedDeposit(arDepositReceipt.getRctAppliedDeposit()
	 * + temp);
	 * 
	 * }
	 * 
	 * }
	 * 
	 * // decrease customer balance
	 * 
	 * double RCT_AMNT =
	 * this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().
	 * getFcCode(), arReceipt.getGlFunctionalCurrency().getFcName(),
	 * arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
	 * arReceipt.getRctAmount(), AD_CMPNY);
	 * 
	 * this.post(arReceipt.getRctDate(), (RCT_AMNT + RCT_CRDTS + RCT_AI_APPLD_DPSTS)
	 * * -1, arReceipt.getArCustomer(), AD_CMPNY);
	 * 
	 * 
	 * }
	 * 
	 * // increase bank balance
	 * 
	 * LocalAdBankAccount adBankAccount =
	 * adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());
	 * 
	 * try {
	 * 
	 * // find bankaccount balance before or equal receipt date
	 * 
	 * Collection adBankAccountBalances =
	 * adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.
	 * getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);
	 * 
	 * if (!adBankAccountBalances.isEmpty()) {
	 * 
	 * // get last check
	 * 
	 * ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
	 * 
	 * LocalAdBankAccountBalance adBankAccountBalance =
	 * (LocalAdBankAccountBalance)adBankAccountBalanceList.get(
	 * adBankAccountBalanceList.size() - 1);
	 * 
	 * if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {
	 * 
	 * // create new balance
	 * 
	 * LocalAdBankAccountBalance adNewBankAccountBalance =
	 * adBankAccountBalanceHome.create( arReceipt.getRctDate(),
	 * adBankAccountBalance.getBabBalance() + arReceipt.getRctAmount(), "BOOK",
	 * AD_CMPNY);
	 * 
	 * adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
	 * 
	 * } else { // equals to check date
	 * 
	 * adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() +
	 * arReceipt.getRctAmount());
	 * 
	 * }
	 * 
	 * } else {
	 * 
	 * // create new balance
	 * 
	 * LocalAdBankAccountBalance adNewBankAccountBalance =
	 * adBankAccountBalanceHome.create( arReceipt.getRctDate(),
	 * (arReceipt.getRctAmount()), "BOOK", AD_CMPNY);
	 * 
	 * adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
	 * 
	 * }
	 * 
	 * // propagate to subsequent balances if necessary
	 * 
	 * adBankAccountBalances =
	 * adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate
	 * (), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);
	 * 
	 * Iterator i = adBankAccountBalances.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * LocalAdBankAccountBalance adBankAccountBalance =
	 * (LocalAdBankAccountBalance)i.next();
	 * 
	 * adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() +
	 * arReceipt.getRctAmount());
	 * 
	 * }
	 * 
	 * } catch (Exception ex) {
	 * 
	 * ex.printStackTrace();
	 * 
	 * }
	 * 
	 * // set receipt post status
	 * 
	 * arReceipt.setRctPosted(EJBCommon.TRUE); arReceipt.setRctPostedBy(USR_NM);
	 * arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
	 * 
	 * 
	 * } else if (arReceipt.getRctVoid() == EJBCommon.TRUE &&
	 * arReceipt.getRctVoidPosted() == EJBCommon.FALSE) {// void receipt
	 * 
	 * 
	 * if (arReceipt.getRctType().equals("COLLECTION")) {
	 * 
	 * double RCT_CRDTS = 0d; double RCT_AI_APPLD_DPSTS = 0d;
	 * 
	 * // decrease amount paid in invoice payment schedules and invoice
	 * 
	 * Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();
	 * 
	 * Iterator i = arAppliedInvoices.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();
	 * 
	 * LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
	 * arAppliedInvoice.getArInvoicePaymentSchedule();
	 * 
	 * double AMOUNT_PAID = arAppliedInvoice.getAiApplyAmount() +
	 * arAppliedInvoice.getAiCreditableWTax() +
	 * arAppliedInvoice.getAiDiscountAmount() +
	 * arAppliedInvoice.getAiAppliedDeposit();
	 * 
	 * RCT_CRDTS += this.convertForeignToFunctionalCurrency(arAppliedInvoice.
	 * getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().
	 * getFcCode(), arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getGlFunctionalCurrency().getFcName(),
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getInvConversionDate(),
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getInvConversionRate(), arAppliedInvoice.getAiDiscountAmount() +
	 * arAppliedInvoice.getAiCreditableWTax(), AD_CMPNY);
	 * 
	 * RCT_AI_APPLD_DPSTS +=
	 * this.convertForeignToFunctionalCurrency(arAppliedInvoice.
	 * getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().
	 * getFcCode(), arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getGlFunctionalCurrency().getFcName(),
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getInvConversionDate(),
	 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
	 * getInvConversionRate(), arAppliedInvoice.getAiAppliedDeposit(), AD_CMPNY);
	 * 
	 * 
	 * arInvoicePaymentSchedule.setIpsAmountPaid(EJBCommon.roundIt(
	 * arInvoicePaymentSchedule.getIpsAmountPaid() - AMOUNT_PAID,
	 * this.getGlFcPrecisionUnit(AD_CMPNY)));
	 * 
	 * arInvoicePaymentSchedule.getArInvoice().setInvAmountPaid(EJBCommon.roundIt(
	 * arInvoicePaymentSchedule.getArInvoice().getInvAmountPaid() - AMOUNT_PAID,
	 * this.getGlFcPrecisionUnit(AD_CMPNY)));
	 * 
	 * }
	 * 
	 * // decrease customer applied deposit
	 * 
	 * try {
	 * 
	 * arDepositReceipts =
	 * arReceiptHome.findAppliedDepositEnabledPostedRctByCstCustomerCode(arCustomer.
	 * getCstCustomerCode(), AD_CMPNY);
	 * 
	 * } catch (FinderException ex) { }
	 * 
	 * i = arDepositReceipts.iterator();
	 * 
	 * while(i.hasNext()) {
	 * 
	 * if(RCT_AI_APPLD_DPSTS == 0) break;
	 * 
	 * arDepositReceipt = (LocalArReceipt)i.next();
	 * 
	 * double temp = 0d;
	 * 
	 * // update applied deposit if(arDepositReceipt.getRctAppliedDeposit() >=
	 * RCT_AI_APPLD_DPSTS) {
	 * 
	 * temp = RCT_AI_APPLD_DPSTS; RCT_AI_APPLD_DPSTS -= temp;
	 * arDepositReceipt.setRctAppliedDeposit(arDepositReceipt.getRctAppliedDeposit()
	 * - temp);
	 * 
	 * } else {
	 * 
	 * temp = arDepositReceipt.getRctAppliedDeposit(); RCT_AI_APPLD_DPSTS -= temp;
	 * arDepositReceipt.setRctAppliedDeposit(arDepositReceipt.getRctAppliedDeposit()
	 * - temp);
	 * 
	 * }
	 * 
	 * }
	 * 
	 * // increase customer balance
	 * 
	 * double RCT_AMNT =
	 * this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().
	 * getFcCode(), arReceipt.getGlFunctionalCurrency().getFcName(),
	 * arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
	 * arReceipt.getRctAmount(), AD_CMPNY);
	 * 
	 * this.post(arReceipt.getRctDate(), RCT_AMNT + RCT_CRDTS + RCT_AI_APPLD_DPSTS,
	 * arReceipt.getArCustomer(), AD_CMPNY);
	 * 
	 * }
	 * 
	 * // decrease bank balance
	 * 
	 * LocalAdBankAccount adBankAccount =
	 * adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());
	 * 
	 * try {
	 * 
	 * // find bankaccount balance before or equal receipt date
	 * 
	 * Collection adBankAccountBalances =
	 * adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.
	 * getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);
	 * 
	 * if (!adBankAccountBalances.isEmpty()) {
	 * 
	 * // get last check
	 * 
	 * ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
	 * 
	 * LocalAdBankAccountBalance adBankAccountBalance =
	 * (LocalAdBankAccountBalance)adBankAccountBalanceList.get(
	 * adBankAccountBalanceList.size() - 1);
	 * 
	 * if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {
	 * 
	 * // create new balance
	 * 
	 * LocalAdBankAccountBalance adNewBankAccountBalance =
	 * adBankAccountBalanceHome.create( arReceipt.getRctDate(),
	 * adBankAccountBalance.getBabBalance() - arReceipt.getRctAmount(), "BOOK",
	 * AD_CMPNY);
	 * 
	 * adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
	 * 
	 * } else { // equals to check date
	 * 
	 * adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() -
	 * arReceipt.getRctAmount());
	 * 
	 * }
	 * 
	 * } else {
	 * 
	 * // create new balance
	 * 
	 * LocalAdBankAccountBalance adNewBankAccountBalance =
	 * adBankAccountBalanceHome.create( arReceipt.getRctDate(), (0 -
	 * arReceipt.getRctAmount()), "BOOK", AD_CMPNY);
	 * 
	 * adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
	 * 
	 * }
	 * 
	 * // propagate to subsequent balances if necessary
	 * 
	 * adBankAccountBalances =
	 * adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate
	 * (), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);
	 * 
	 * Iterator i = adBankAccountBalances.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * LocalAdBankAccountBalance adBankAccountBalance =
	 * (LocalAdBankAccountBalance)i.next();
	 * 
	 * adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() -
	 * arReceipt.getRctAmount());
	 * 
	 * }
	 * 
	 * } catch (Exception ex) {
	 * 
	 * ex.printStackTrace();
	 * 
	 * }
	 * 
	 * // set receipt post status
	 * 
	 * arReceipt.setRctVoidPosted(EJBCommon.TRUE); arReceipt.setRctPostedBy(USR_NM);
	 * arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
	 * 
	 * }
	 * 
	 * // post to gl if necessary
	 * 
	 * LocalAdPreference adPreference =
	 * adPreferenceHome.findByPrfAdCompany(AD_CMPNY); LocalAdCompany adCompany =
	 * adCompanyHome.findByPrimaryKey(AD_CMPNY);
	 * 
	 * if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
	 * 
	 * // validate if date has no period and period is closed
	 * 
	 * LocalGlSetOfBook glJournalSetOfBook = null;
	 * 
	 * try {
	 * 
	 * glJournalSetOfBook = glSetOfBookHome.findByDate(arReceipt.getRctDate(),
	 * AD_CMPNY);
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * throw new GlJREffectiveDateNoPeriodExistException();
	 * 
	 * }
	 * 
	 * LocalGlAccountingCalendarValue glAccountingCalendarValue =
	 * glAccountingCalendarValueHome.findByAcCodeAndDate(
	 * glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
	 * arReceipt.getRctDate(), AD_CMPNY);
	 * 
	 * 
	 * if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
	 * glAccountingCalendarValue.getAcvStatus() == 'C' ||
	 * glAccountingCalendarValue.getAcvStatus() == 'P') {
	 * 
	 * throw new GlJREffectiveDatePeriodClosedException();
	 * 
	 * }
	 * 
	 * // check if invoice is balance if not check suspense posting
	 * 
	 * LocalGlJournalLine glOffsetJournalLine = null;
	 * 
	 * Collection arDistributionRecords = null;
	 * 
	 * if (arReceipt.getRctVoid() == EJBCommon.FALSE) {
	 * 
	 * arDistributionRecords =
	 * arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.
	 * FALSE, arReceipt.getRctCode(), AD_CMPNY);
	 * 
	 * } else {
	 * 
	 * arDistributionRecords =
	 * arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.
	 * TRUE, arReceipt.getRctCode(), AD_CMPNY);
	 * 
	 * }
	 * 
	 * 
	 * Iterator j = arDistributionRecords.iterator();
	 * 
	 * double TOTAL_DEBIT = 0d; double TOTAL_CREDIT = 0d;
	 * 
	 * while (j.hasNext()) {
	 * 
	 * LocalArDistributionRecord arDistributionRecord =
	 * (LocalArDistributionRecord)j.next();
	 * 
	 * double DR_AMNT = 0d;
	 * 
	 * if (arDistributionRecord.getArAppliedInvoice() != null) {
	 * 
	 * LocalArInvoice arInvoice =
	 * arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().
	 * getArInvoice();
	 * 
	 * DR_AMNT =
	 * this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().
	 * getFcCode(), arInvoice.getGlFunctionalCurrency().getFcName(),
	 * arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
	 * arDistributionRecord.getDrAmount(), AD_CMPNY);
	 * 
	 * } else {
	 * 
	 * DR_AMNT =
	 * this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().
	 * getFcCode(), arReceipt.getGlFunctionalCurrency().getFcName(),
	 * arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
	 * arDistributionRecord.getDrAmount(), AD_CMPNY);
	 * 
	 * }
	 * 
	 * if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
	 * 
	 * TOTAL_DEBIT += DR_AMNT;
	 * 
	 * } else {
	 * 
	 * TOTAL_CREDIT += DR_AMNT;
	 * 
	 * }
	 * 
	 * }
	 * 
	 * TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT,
	 * adCompany.getGlFunctionalCurrency().getFcPrecision()); TOTAL_CREDIT =
	 * EJBCommon.roundIt(TOTAL_CREDIT,
	 * adCompany.getGlFunctionalCurrency().getFcPrecision());
	 * 
	 * if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
	 * TOTAL_DEBIT != TOTAL_CREDIT) {
	 * 
	 * LocalGlSuspenseAccount glSuspenseAccount = null;
	 * 
	 * try {
	 * 
	 * glSuspenseAccount =
	 * glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES",
	 * "SALES RECEIPTS", AD_CMPNY);
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * throw new GlobalJournalNotBalanceException();
	 * 
	 * }
	 * 
	 * if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
	 * 
	 * glOffsetJournalLine =
	 * glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
	 * EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
	 * 
	 * } else {
	 * 
	 * glOffsetJournalLine =
	 * glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
	 * EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
	 * 
	 * }
	 * 
	 * LocalGlChartOfAccount glChartOfAccount =
	 * glSuspenseAccount.getGlChartOfAccount();
	 * glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
	 * 
	 * 
	 * } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
	 * TOTAL_DEBIT != TOTAL_CREDIT) {
	 * 
	 * throw new GlobalJournalNotBalanceException();
	 * 
	 * }
	 * 
	 * 
	 * // create journal batch if necessary
	 * 
	 * LocalGlJournalBatch glJournalBatch = null; java.text.SimpleDateFormat
	 * formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
	 * 
	 * try {
	 * 
	 * if (adPreference.getPrfEnableArReceiptBatch() == EJBCommon.TRUE) {
	 * 
	 * glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " +
	 * formatter.format(new Date()) + " " +
	 * arReceipt.getArReceiptBatch().getRbName(), AD_BRNCH, AD_CMPNY);
	 * 
	 * } else {
	 * 
	 * glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " +
	 * formatter.format(new Date()) + " SALES RECEIPTS", AD_BRNCH, AD_CMPNY);
	 * 
	 * }
	 * 
	 * 
	 * } catch (FinderException ex) { }
	 * 
	 * if (adPreference.getPrfEnableGlJournalBatch() == EJBCommon.TRUE &&
	 * glJournalBatch == null) {
	 * 
	 * if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {
	 * 
	 * glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " +
	 * formatter.format(new Date()) + " " +
	 * arReceipt.getArReceiptBatch().getRbName(), "JOURNAL IMPORT", "CLOSED",
	 * EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
	 * 
	 * } else {
	 * 
	 * glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " +
	 * formatter.format(new Date()) + " SALES RECEIPTS", "JOURNAL IMPORT", "CLOSED",
	 * EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
	 * 
	 * }
	 * 
	 * 
	 * }
	 * 
	 * // create journal entry String customerName = arReceipt.getRctCustomerName()
	 * == null ? arReceipt.getArCustomer().getCstName() :
	 * arReceipt.getRctCustomerName();
	 * 
	 * LocalGlJournal glJournal =
	 * glJournalHome.create(arReceipt.getRctReferenceNumber(),
	 * arReceipt.getRctDescription(), arReceipt.getRctDate(), 0.0d, null,
	 * arReceipt.getRctNumber(), null, 1d, "N/A", null, 'N', EJBCommon.TRUE,
	 * EJBCommon.FALSE, USR_NM, new Date(), USR_NM, new Date(), null, null, USR_NM,
	 * EJBCommon.getGcCurrentDateWoTime().getTime(),
	 * arReceipt.getArCustomer().getCstTin(), customerName, EJBCommon.FALSE, null,
	 * null,null,null, null,null,null, AD_BRNCH, AD_CMPNY);
	 * 
	 * LocalGlJournalSource glJournalSource =
	 * glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
	 * glJournalSource.addGlJournal(glJournal);
	 * 
	 * LocalGlFunctionalCurrency glFunctionalCurrency =
	 * glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().
	 * getFcName(), AD_CMPNY); glFunctionalCurrency.addGlJournal(glJournal);
	 * 
	 * LocalGlJournalCategory glJournalCategory =
	 * glJournalCategoryHome.findByJcName("SALES RECEIPTS", AD_CMPNY);
	 * glJournalCategory.addGlJournal(glJournal);
	 * 
	 * if (glJournalBatch != null) {
	 * 
	 * glJournalBatch.addGlJournal(glJournal);
	 * 
	 * }
	 * 
	 * 
	 * // create journal lines
	 * 
	 * j = arDistributionRecords.iterator(); boolean firstFlag = true;
	 * 
	 * while (j.hasNext()) {
	 * 
	 * LocalArDistributionRecord arDistributionRecord =
	 * (LocalArDistributionRecord)j.next();
	 * 
	 * double DR_AMNT = 0d; LocalArInvoice arInvoice = null;
	 * 
	 * if (arDistributionRecord.getArAppliedInvoice() != null) {
	 * 
	 * arInvoice =
	 * arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().
	 * getArInvoice();
	 * 
	 * DR_AMNT =
	 * this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().
	 * getFcCode(), arInvoice.getGlFunctionalCurrency().getFcName(),
	 * arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
	 * arDistributionRecord.getDrAmount(), AD_CMPNY);
	 * 
	 * } else {
	 * 
	 * DR_AMNT =
	 * this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().
	 * getFcCode(), arReceipt.getGlFunctionalCurrency().getFcName(),
	 * arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
	 * arDistributionRecord.getDrAmount(), AD_CMPNY);
	 * 
	 * }
	 * 
	 * LocalGlJournalLine glJournalLine =
	 * glJournalLineHome.create(arDistributionRecord.getDrLine(),
	 * arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
	 * 
	 * arDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
	 * 
	 * glJournal.addGlJournalLine(glJournalLine);
	 * 
	 * arDistributionRecord.setDrImported(EJBCommon.TRUE);
	 * 
	 * // for FOREX revaluation
	 * 
	 * int FC_CODE = arDistributionRecord.getArAppliedInvoice() != null ?
	 * arInvoice.getGlFunctionalCurrency().getFcCode().intValue() :
	 * arReceipt.getGlFunctionalCurrency().getFcCode().intValue();
	 * 
	 * if((FC_CODE != adCompany.getGlFunctionalCurrency().getFcCode().intValue()) &&
	 * glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
	 * (FC_CODE ==
	 * glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().
	 * intValue())){
	 * 
	 * double CONVERSION_RATE = arDistributionRecord.getArAppliedInvoice() != null ?
	 * arInvoice.getInvConversionRate() : arReceipt.getRctConversionRate();
	 * 
	 * Date DATE = arDistributionRecord.getArAppliedInvoice() != null ?
	 * arInvoice.getInvConversionDate() : arReceipt.getRctConversionDate();
	 * 
	 * if (DATE != null && (CONVERSION_RATE == 0 || CONVERSION_RATE == 1)){
	 * 
	 * CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
	 * glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
	 * glJournal.getJrConversionDate(), AD_CMPNY);
	 * 
	 * } else if (CONVERSION_RATE == 0) {
	 * 
	 * CONVERSION_RATE = 1;
	 * 
	 * }
	 * 
	 * Collection glForexLedgers = null;
	 * 
	 * DATE = arDistributionRecord.getArAppliedInvoice() != null ?
	 * arInvoice.getInvDate() : arReceipt.getRctDate();
	 * 
	 * try {
	 * 
	 * glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
	 * DATE, glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);
	 * 
	 * } catch(FinderException ex) {
	 * 
	 * }
	 * 
	 * LocalGlForexLedger glForexLedger = (glForexLedgers.isEmpty() ||
	 * glForexLedgers == null) ? null : (LocalGlForexLedger)
	 * glForexLedgers.iterator().next();
	 * 
	 * int FRL_LN = (glForexLedger != null &&
	 * glForexLedger.getFrlDate().compareTo(DATE) == 0) ?
	 * glForexLedger.getFrlLine().intValue() + 1 : 1;
	 * 
	 * // compute balance double COA_FRX_BLNC = glForexLedger == null ? 0 :
	 * glForexLedger.getFrlBalance(); double FRL_AMNT =
	 * arDistributionRecord.getDrAmount();
	 * 
	 * if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase(
	 * "ASSET")) FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT
	 * : (- 1 * FRL_AMNT)); else FRL_AMNT = (glJournalLine.getJlDebit() ==
	 * EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);
	 * 
	 * COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
	 * 
	 * double FRX_GN_LSS = 0d;
	 * 
	 * if (glOffsetJournalLine != null && firstFlag) {
	 * 
	 * if(glOffsetJournalLine.getGlChartOfAccount().getCoaAccountType().
	 * equalsIgnoreCase("ASSET"))
	 * 
	 * FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ?
	 * glOffsetJournalLine.getJlAmount() : (- 1 *
	 * glOffsetJournalLine.getJlAmount()));
	 * 
	 * else
	 * 
	 * FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 *
	 * glOffsetJournalLine.getJlAmount()) : glOffsetJournalLine.getJlAmount());
	 * 
	 * firstFlag = false;
	 * 
	 * }
	 * 
	 * glForexLedger = glForexLedgerHome.create(DATE, new Integer (FRL_LN), "OR",
	 * FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, FRX_GN_LSS, AD_CMPNY);
	 * 
	 * glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
	 * 
	 * // propagate balances try{
	 * 
	 * glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
	 * glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
	 * glForexLedger.getFrlAdCompany());
	 * 
	 * } catch (FinderException ex) {
	 * 
	 * }
	 * 
	 * Iterator itrFrl = glForexLedgers.iterator();
	 * 
	 * while (itrFrl.hasNext()) {
	 * 
	 * glForexLedger = (LocalGlForexLedger) itrFrl.next(); FRL_AMNT =
	 * arDistributionRecord.getDrAmount();
	 * 
	 * if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase(
	 * "ASSET")) FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT
	 * : (- 1 * FRL_AMNT)); else FRL_AMNT = (glJournalLine.getJlDebit() ==
	 * EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);
	 * 
	 * glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);
	 * 
	 * }
	 * 
	 * } }
	 * 
	 * if (glOffsetJournalLine != null) {
	 * 
	 * glJournal.addGlJournalLine(glOffsetJournalLine);
	 * 
	 * }
	 * 
	 * // post journal to gl
	 * 
	 * Collection glJournalLines = glJournal.getGlJournalLines();
	 * 
	 * Iterator i = glJournalLines.iterator();
	 * 
	 * while (i.hasNext()) {
	 * 
	 * LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();
	 * 
	 * // post current to current acv
	 * 
	 * this.postToGl(glAccountingCalendarValue, glJournalLine.getGlChartOfAccount(),
	 * true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
	 * 
	 * 
	 * // post to subsequent acvs (propagate)
	 * 
	 * Collection glSubsequentAccountingCalendarValues =
	 * glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
	 * glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
	 * glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);
	 * 
	 * Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
	 * 
	 * while (acvsIter.hasNext()) {
	 * 
	 * LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
	 * (LocalGlAccountingCalendarValue)acvsIter.next();
	 * 
	 * this.postToGl(glSubsequentAccountingCalendarValue,
	 * glJournalLine.getGlChartOfAccount(), false, glJournalLine.getJlDebit(),
	 * glJournalLine.getJlAmount(), AD_CMPNY);
	 * 
	 * }
	 * 
	 * // post to subsequent years if necessary
	 * 
	 * Collection glSubsequentSetOfBooks =
	 * glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.
	 * getGlAccountingCalendar().getAcYear(), AD_CMPNY);
	 * 
	 * if (!glSubsequentSetOfBooks.isEmpty() &&
	 * glJournalSetOfBook.getSobYearEndClosed() == 1) {
	 * 
	 * adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY); LocalGlChartOfAccount
	 * glRetainedEarningsAccount =
	 * glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(
	 * ), AD_CMPNY);
	 * 
	 * Iterator sobIter = glSubsequentSetOfBooks.iterator();
	 * 
	 * while (sobIter.hasNext()) {
	 * 
	 * LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
	 * 
	 * String ACCOUNT_TYPE =
	 * glJournalLine.getGlChartOfAccount().getCoaAccountType();
	 * 
	 * // post to subsequent acvs of subsequent set of book(propagate)
	 * 
	 * Collection glAccountingCalendarValues =
	 * glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.
	 * getGlAccountingCalendar().getAcCode(), AD_CMPNY);
	 * 
	 * Iterator acvIter = glAccountingCalendarValues.iterator();
	 * 
	 * while (acvIter.hasNext()) {
	 * 
	 * LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
	 * (LocalGlAccountingCalendarValue)acvIter.next();
	 * 
	 * if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
	 * ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
	 * 
	 * this.postToGl(glSubsequentAccountingCalendarValue,
	 * glJournalLine.getGlChartOfAccount(), false, glJournalLine.getJlDebit(),
	 * glJournalLine.getJlAmount(), AD_CMPNY);
	 * 
	 * } else { // revenue & expense
	 * 
	 * this.postToGl(glSubsequentAccountingCalendarValue, glRetainedEarningsAccount,
	 * false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
	 * 
	 * }
	 * 
	 * }
	 * 
	 * if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
	 * 
	 * }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * } catch (Exception ex) {
	 * 
	 * ex.printStackTrace(); ctx.setRollbackOnly(); throw new EJBException
	 * (ex.getMessage());
	 * 
	 * }
	 * 
	 * }
	 * 
	 * private double convertForeignToFunctionalCurrency(Integer FC_CODE, String
	 * FC_NM, Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer
	 * AD_CMPNY) {
	 * 
	 * Debug.print("ArReceiptEntryControllerBean convertForeignToFunctionalCurrency"
	 * );
	 * 
	 * 
	 * LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
	 * LocalAdCompanyHome adCompanyHome = null;
	 * 
	 * LocalAdCompany adCompany = null;
	 * 
	 * // Initialize EJB Homes
	 * 
	 * try {
	 * 
	 * glFunctionalCurrencyRateHome =
	 * (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME,
	 * LocalGlFunctionalCurrencyRateHome.class); adCompanyHome =
	 * (LocalAdCompanyHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	 * 
	 * } catch (NamingException ex) {
	 * 
	 * throw new EJBException(ex.getMessage());
	 * 
	 * }
	 * 
	 * // get company and extended precision
	 * 
	 * try {
	 * 
	 * adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	 * 
	 * } catch (Exception ex) {
	 * 
	 * throw new EJBException(ex.getMessage());
	 * 
	 * }
	 * 
	 * 
	 * // Convert to functional currency if necessary
	 * 
	 * if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
	 * 
	 * AMOUNT = AMOUNT * CONVERSION_RATE;
	 * 
	 * } else if (CONVERSION_DATE != null) {
	 * 
	 * try {
	 * 
	 * // Get functional currency rate
	 * 
	 * LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
	 * 
	 * if (!FC_NM.equals("USD")) {
	 * 
	 * glReceiptFunctionalCurrencyRate =
	 * glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, CONVERSION_DATE,
	 * AD_CMPNY);
	 * 
	 * AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();
	 * 
	 * }
	 * 
	 * // Get set of book functional currency rate if necessary
	 * 
	 * if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
	 * 
	 * LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
	 * glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.
	 * getGlFunctionalCurrency(). getFcCode(), CONVERSION_DATE, AD_CMPNY);
	 * 
	 * AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();
	 * 
	 * }
	 * 
	 * 
	 * } catch (Exception ex) {
	 * 
	 * throw new EJBException(ex.getMessage());
	 * 
	 * }
	 * 
	 * }
	 * 
	 * return EJBCommon.roundIt(AMOUNT,
	 * adCompany.getGlFunctionalCurrency().getFcPrecision());
	 * 
	 * }
	 * 
	 * 
	 * 
	 * private void postToGl(LocalGlAccountingCalendarValue
	 * glAccountingCalendarValue, LocalGlChartOfAccount glChartOfAccount, boolean
	 * isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
	 * 
	 * Debug.print("ArReceiptEntryControllerBean postToGl");
	 * 
	 * LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
	 * LocalAdCompanyHome adCompanyHome = null;
	 * 
	 * 
	 * // Initialize EJB Home
	 * 
	 * try {
	 * 
	 * glChartOfAccountBalanceHome =
	 * (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME,
	 * LocalGlChartOfAccountBalanceHome.class); adCompanyHome =
	 * (LocalAdCompanyHome)EJBHomeFactory.
	 * lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	 * 
	 * } catch (NamingException ex) {
	 * 
	 * throw new EJBException(ex.getMessage());
	 * 
	 * }
	 * 
	 * try {
	 * 
	 * LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	 * 
	 * LocalGlChartOfAccountBalance glChartOfAccountBalance =
	 * glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
	 * glAccountingCalendarValue.getAcvCode(), glChartOfAccount.getCoaCode(),
	 * AD_CMPNY);
	 * 
	 * String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType(); short
	 * FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
	 * 
	 * 
	 * 
	 * if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
	 * isDebit == EJBCommon.TRUE) || (!ACCOUNT_TYPE.equals("ASSET") &&
	 * !ACCOUNT_TYPE.equals("EXPENSE") && isDebit == EJBCommon.FALSE)) {
	 * 
	 * glChartOfAccountBalance.setCoabEndingBalance(
	 * EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT,
	 * FC_EXTNDD_PRCSN));
	 * 
	 * if (!isCurrentAcv) {
	 * 
	 * glChartOfAccountBalance.setCoabBeginningBalance(
	 * EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() +
	 * JL_AMNT, FC_EXTNDD_PRCSN));
	 * 
	 * }
	 * 
	 * 
	 * } else {
	 * 
	 * glChartOfAccountBalance.setCoabEndingBalance(
	 * EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT,
	 * FC_EXTNDD_PRCSN));
	 * 
	 * if (!isCurrentAcv) {
	 * 
	 * glChartOfAccountBalance.setCoabBeginningBalance(
	 * EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() -
	 * JL_AMNT, FC_EXTNDD_PRCSN));
	 * 
	 * }
	 * 
	 * }
	 * 
	 * if (isCurrentAcv) {
	 * 
	 * if (isDebit == EJBCommon.TRUE) {
	 * 
	 * glChartOfAccountBalance.setCoabTotalDebit(
	 * EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT,
	 * FC_EXTNDD_PRCSN));
	 * 
	 * } else {
	 * 
	 * glChartOfAccountBalance.setCoabTotalCredit(
	 * EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT,
	 * FC_EXTNDD_PRCSN)); }
	 * 
	 * }
	 * 
	 * } catch (Exception ex) {
	 * 
	 * Debug.printStackTrace(ex); throw new EJBException(ex.getMessage());
	 * 
	 * }
	 * 
	 * 
	 * }
	 * 
	 * 
	 *//**
		 * @ejb:interface-method view-type="remote"
		 * @jboss:method-attributes read-only="true"
		 **//*
			 * public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE,
			 * Integer AD_CMPNY) throws GlobalConversionDateNotExistException {
			 * 
			 * Debug.print("ArReceiptEntryControllerBean getFrRateByFrNameAndFrDate");
			 * 
			 * LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
			 * LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
			 * LocalAdCompanyHome adCompanyHome = null;
			 * 
			 * // Initialize EJB Home
			 * 
			 * try {
			 * 
			 * glFunctionalCurrencyRateHome =
			 * (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			 * lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME,
			 * LocalGlFunctionalCurrencyRateHome.class); glFunctionalCurrencyHome =
			 * (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			 * lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME,
			 * LocalGlFunctionalCurrencyHome.class); adCompanyHome =
			 * (LocalAdCompanyHome)EJBHomeFactory.
			 * lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			 * 
			 * } catch (NamingException ex) {
			 * 
			 * throw new EJBException(ex.getMessage());
			 * 
			 * }
			 * 
			 * try {
			 * 
			 * LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			 * LocalGlFunctionalCurrency glFunctionalCurrency =
			 * glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			 * 
			 * double CONVERSION_RATE = 1;
			 * 
			 * // Get functional currency rate
			 * 
			 * if (!FC_NM.equals("USD")) {
			 * 
			 * LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
			 * glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.
			 * getFcCode(), CONVERSION_DATE, AD_CMPNY);
			 * 
			 * CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
			 * 
			 * }
			 * 
			 * // Get set of book functional currency rate if necessary
			 * 
			 * if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
			 * 
			 * LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
			 * glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.
			 * getGlFunctionalCurrency(). getFcCode(), CONVERSION_DATE, AD_CMPNY);
			 * 
			 * CONVERSION_RATE = CONVERSION_RATE /
			 * glCompanyFunctionalCurrencyRate.getFrXToUsd();
			 * 
			 * }
			 * 
			 * return CONVERSION_RATE;
			 * 
			 * } catch (Exception ex) {
			 * 
			 * Debug.printStackTrace(ex); throw new EJBException(ex.getMessage());
			 * 
			 * }
			 * 
			 * }
			 * 
			 * 
			 * private LocalArAppliedInvoice addArAiEntry(ArModAppliedInvoiceDetails
			 * mdetails, LocalArReceipt arReceipt, Integer AD_CMPNY) throws
			 * ArINVOverapplicationNotAllowedException,
			 * GlobalTransactionAlreadyLockedException, ArRCTInvoiceHasNoWTaxCodeException {
			 * 
			 * Debug.print("ArReceiptEntryControllerBean addArAiEntry");
			 * 
			 * LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
			 * LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
			 * LocalAdCompanyHome adCompanyHome = null; LocalAdPreferenceHome
			 * adPreferenceHome = null;
			 * 
			 * 
			 * // Initialize EJB Home
			 * 
			 * try {
			 * 
			 * arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
			 * lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME,
			 * LocalArAppliedInvoiceHome.class); arInvoicePaymentScheduleHome =
			 * (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
			 * lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME,
			 * LocalArInvoicePaymentScheduleHome.class); adCompanyHome =
			 * (LocalAdCompanyHome)EJBHomeFactory.
			 * lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			 * adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			 * lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
			 * LocalAdPreferenceHome.class);
			 * 
			 * } catch (NamingException ex) {
			 * 
			 * throw new EJBException(ex.getMessage());
			 * 
			 * }
			 * 
			 * try {
			 * 
			 * // get company
			 * 
			 * LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			 * LocalAdPreference adPreference =
			 * adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			 * 
			 * // get functional currency name
			 * 
			 * String FC_NM = adCompany.getGlFunctionalCurrency().getFcName();
			 * 
			 * 
			 * // validate overapplication
			 * 
			 * LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
			 * arInvoicePaymentScheduleHome.findByPrimaryKey(mdetails.getAiIpsCode());
			 * 
			 * if (EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue() -
			 * arInvoicePaymentSchedule.getIpsAmountPaid(),
			 * this.getGlFcPrecisionUnit(AD_CMPNY)) <
			 * EJBCommon.roundIt(mdetails.getAiApplyAmount() +
			 * mdetails.getAiCreditableWTax() + mdetails.getAiDiscountAmount() +
			 * mdetails.getAiAppliedDeposit(), this.getGlFcPrecisionUnit(AD_CMPNY))) {
			 * 
			 * throw new
			 * ArINVOverapplicationNotAllowedException(arInvoicePaymentSchedule.getArInvoice
			 * ().getInvNumber() + "-" + arInvoicePaymentSchedule.getIpsNumber());
			 * 
			 * }
			 * 
			 * // validate if ips already locked
			 * 
			 * if (arInvoicePaymentSchedule.getIpsLock() == EJBCommon.TRUE) {
			 * 
			 * throw new
			 * GlobalTransactionAlreadyLockedException(arInvoicePaymentSchedule.getArInvoice
			 * ().getInvNumber() + "-" + arInvoicePaymentSchedule.getIpsNumber());
			 * 
			 * }
			 * 
			 * // validate invoice wtax code if necessary
			 * 
			 * if (mdetails.getAiCreditableWTax() != 0 &&
			 * arInvoicePaymentSchedule.getArInvoice().getArWithholdingTaxCode().
			 * getGlChartOfAccount() == null && (adPreference.getArWithholdingTaxCode() ==
			 * null || adPreference.getArWithholdingTaxCode().getGlChartOfAccount() ==
			 * null)) {
			 * 
			 * throw new
			 * ArRCTInvoiceHasNoWTaxCodeException(arInvoicePaymentSchedule.getArInvoice().
			 * getInvNumber() + "-" + arInvoicePaymentSchedule.getIpsNumber());
			 * 
			 * }
			 * 
			 * double AI_FRX_GN_LSS = 0d;
			 * 
			 * if (!FC_NM.equals(arInvoicePaymentSchedule.getArInvoice().
			 * getGlFunctionalCurrency().getFcName()) ||
			 * !FC_NM.equals(arReceipt.getGlFunctionalCurrency().getFcName())) {
			 * 
			 * double AI_ALLCTD_PYMNT_AMNT =
			 * this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().
			 * getFcCode(), arReceipt.getGlFunctionalCurrency().getFcName(),
			 * arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
			 * mdetails.getAiAllocatedPaymentAmount(), AD_CMPNY);
			 * 
			 * double AI_APPLY_AMNT =
			 * this.convertForeignToFunctionalCurrency(arInvoicePaymentSchedule.getArInvoice
			 * ().getGlFunctionalCurrency().getFcCode(),
			 * arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName()
			 * , arInvoicePaymentSchedule.getArInvoice().getInvConversionDate(),
			 * arInvoicePaymentSchedule.getArInvoice().getInvConversionRate(),
			 * mdetails.getAiApplyAmount(), AD_CMPNY);
			 * 
			 * double AI_CRDTBL_W_TX =
			 * this.convertForeignToFunctionalCurrency(arInvoicePaymentSchedule.getArInvoice
			 * ().getGlFunctionalCurrency().getFcCode(),
			 * arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName()
			 * , arInvoicePaymentSchedule.getArInvoice().getInvConversionDate(),
			 * arInvoicePaymentSchedule.getArInvoice().getInvConversionRate(),
			 * mdetails.getAiCreditableWTax(), AD_CMPNY);
			 * 
			 * double AI_DSCNT_AMNT =
			 * this.convertForeignToFunctionalCurrency(arInvoicePaymentSchedule.getArInvoice
			 * ().getGlFunctionalCurrency().getFcCode(),
			 * arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName()
			 * , arInvoicePaymentSchedule.getArInvoice().getInvConversionDate(),
			 * arInvoicePaymentSchedule.getArInvoice().getInvConversionRate(),
			 * mdetails.getAiDiscountAmount(), AD_CMPNY);
			 * 
			 * double AI_APPLD_DPST =
			 * this.convertForeignToFunctionalCurrency(arInvoicePaymentSchedule.getArInvoice
			 * ().getGlFunctionalCurrency().getFcCode(),
			 * arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName()
			 * , arInvoicePaymentSchedule.getArInvoice().getInvConversionDate(),
			 * arInvoicePaymentSchedule.getArInvoice().getInvConversionRate(),
			 * mdetails.getAiAppliedDeposit(), AD_CMPNY);
			 * 
			 * 
			 * AI_FRX_GN_LSS = EJBCommon.roundIt((AI_ALLCTD_PYMNT_AMNT + AI_CRDTBL_W_TX +
			 * AI_DSCNT_AMNT + AI_APPLD_DPST) - (AI_APPLY_AMNT + AI_CRDTBL_W_TX +
			 * AI_DSCNT_AMNT + AI_APPLD_DPST), this.getGlFcPrecisionUnit(AD_CMPNY));
			 * 
			 * }
			 * 
			 * // create applied voucher
			 * 
			 * LocalArAppliedInvoice arAppliedInvoice =
			 * arAppliedInvoiceHome.create(mdetails.getAiApplyAmount(),
			 * mdetails.getAiCreditableWTax(), mdetails.getAiDiscountAmount(),
			 * mdetails.getAiAppliedDeposit(), mdetails.getAiAllocatedPaymentAmount(),
			 * AI_FRX_GN_LSS, AD_CMPNY);
			 * 
			 * arReceipt.addArAppliedInvoice(arAppliedInvoice);
			 * arInvoicePaymentSchedule.addArAppliedInvoice(arAppliedInvoice);
			 * 
			 * // lock voucher
			 * 
			 * // arInvoicePaymentSchedule.setIpsLock(EJBCommon.TRUE);
			 * 
			 * return arAppliedInvoice;
			 * 
			 * } catch (Exception ex) {
			 * 
			 * Debug.printStackTrace(ex); throw new EJBException(ex.getMessage());
			 * 
			 * }
			 * 
			 * }
			 * 
			 * 
			 * 
			 * private void post(Date RCT_DT, double RCT_AMNT, LocalArCustomer arCustomer,
			 * Integer AD_CMPNY) {
			 * 
			 * Debug.print("ArReceiptEntryControllerBean post");
			 * 
			 * LocalArCustomerBalanceHome arCustomerBalanceHome = null;
			 * 
			 * // Initialize EJB Home
			 * 
			 * try {
			 * 
			 * arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
			 * lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME,
			 * LocalArCustomerBalanceHome.class);
			 * 
			 * } catch (Exception ex) {
			 * 
			 * Debug.printStackTrace(ex); throw new EJBException(ex.getMessage());
			 * 
			 * }
			 * 
			 * try {
			 * 
			 * // find customer balance before or equal invoice date
			 * 
			 * Collection arCustomerBalances =
			 * arCustomerBalanceHome.findByBeforeOrEqualInvDateAndCstCustomerCode(RCT_DT,
			 * arCustomer.getCstCustomerCode(), AD_CMPNY);
			 * 
			 * if (!arCustomerBalances.isEmpty()) {
			 * 
			 * // get last invoice
			 * 
			 * ArrayList arCustomerBalanceList = new ArrayList(arCustomerBalances);
			 * 
			 * LocalArCustomerBalance arCustomerBalance =
			 * (LocalArCustomerBalance)arCustomerBalanceList.get(arCustomerBalanceList.size(
			 * ) - 1);
			 * 
			 * if (arCustomerBalance.getCbDate().before(RCT_DT)) {
			 * 
			 * // create new balance
			 * 
			 * LocalArCustomerBalance arNewCustomerBalance = arCustomerBalanceHome.create(
			 * RCT_DT, arCustomerBalance.getCbBalance() + RCT_AMNT, AD_CMPNY);
			 * 
			 * arCustomer.addArCustomerBalance(arNewCustomerBalance);
			 * 
			 * } else { // equals to invoice date
			 * 
			 * arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + RCT_AMNT);
			 * 
			 * }
			 * 
			 * } else {
			 * 
			 * // create new balance
			 * 
			 * LocalArCustomerBalance arNewCustomerBalance = arCustomerBalanceHome.create(
			 * RCT_DT, RCT_AMNT, AD_CMPNY);
			 * 
			 * arCustomer.addArCustomerBalance(arNewCustomerBalance);
			 * 
			 * }
			 * 
			 * // propagate to subsequent balances if necessary
			 * 
			 * arCustomerBalances =
			 * arCustomerBalanceHome.findByAfterInvDateAndCstCustomerCode(RCT_DT,
			 * arCustomer.getCstCustomerCode(), AD_CMPNY);
			 * 
			 * Iterator i = arCustomerBalances.iterator();
			 * 
			 * while (i.hasNext()) {
			 * 
			 * LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance)i.next();
			 * 
			 * arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + RCT_AMNT);
			 * 
			 * }
			 * 
			 * } catch (Exception ex) {
			 * 
			 * Debug.printStackTrace(ex); throw new EJBException(ex.getMessage());
			 * 
			 * }
			 * 
			 * }
			 * 
			 */
	private ArModReceiptDetails receiptDecodeWithSalesperson(String receipt) throws Exception {

		Debug.print("ArMiscReceiptSyncControllerBean receiptDecodeWithExpiryDate");

		String separator = "$";
		ArModReceiptDetails arModReceiptDetails = new ArModReceiptDetails();

		// Remove first $ character
		receipt = receipt.substring(1);

		// customer code
		int start = 0;
		int nextIndex = receipt.indexOf(separator, start);
		int length = nextIndex - start;
		arModReceiptDetails.setRctCstCustomerCode(receipt.substring(start, start + length));

		// payment type
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// or number
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctNumber(receipt.substring(start, start + length));
		System.out.println("or-" + receipt.substring(start, start + length));

		// waiter/salesperson

		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctSlpSalespersonCode(receipt.substring(start, start + length));
		System.out.println("Ar_Code: " + arModReceiptDetails.getRctSlpSalespersonCode().toString());

		// receipt source
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// currency
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctFcName(receipt.substring(start, start + length));

		// currency rate
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctConversionRate(Double.parseDouble(receipt.substring(start, start + length)));

		// cash amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCashAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// check amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCheckAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// card amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCardAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// total amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosTotalAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// discount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosDiscount(Double.parseDouble(receipt.substring(start, start + length)));

		// void
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// void reason
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// void amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosVoidAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// date
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		sdf.setLenient(false);
		try {
			arModReceiptDetails.setRctDate(sdf.parse(receipt.substring(start, start + length)));
			System.out.println("DAte-" + arModReceiptDetails.getRctDate());
		} catch (Exception ex) {

			throw ex;
		}

		// location
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// payment method
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// sc amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		try {
			arModReceiptDetails.setRctPosScAmount(Double.parseDouble(receipt.substring(start, start + length)));
		} catch (Exception ex) {

		}

		// dc amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		try {
			arModReceiptDetails.setRctPosDcAmount(Double.parseDouble(receipt.substring(start, start + length)));
		} catch (Exception ex) {

		}

		// on account
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosOnAccount((byte) Integer.parseInt(receipt.substring(start, start + length)));

		// end separator
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		String lineSeparator = "~";
		// begin lineSeparator
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(lineSeparator, start);
		length = nextIndex - start;

		ArrayList invIliList = new ArrayList();

		while (true) {
			String asdf = "";
			String asdf2 = "";
			ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = new ArModInvoiceLineItemDetails();

			// begin separator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;

			// line number
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliLine(Short.parseShort(receipt.substring(start, start + length)));

			// item name
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliIiName(receipt.substring(start, start + length));
			System.out.println(arModInvoiceLineItemDetails.getIliIiName());
			// quantity
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliQuantity(Double.parseDouble(receipt.substring(start, start + length)));

			// price
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliUnitPrice(Double.parseDouble(receipt.substring(start, start + length)));

			// total
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliAmount(Double.parseDouble(receipt.substring(start, start + length)));

			// uom
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliUomName(receipt.substring(start, start + length));

			// location
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliLocName(receipt.substring(start, start + length));

//			Expiry Date
			start = nextIndex + 1;
			nextIndex = receipt.indexOf("$~", start);
			length = nextIndex - start;

			asdf = receipt.substring(start);
			// MessageBox.Show("asdf: " + asdf);
			if (asdf != "~~") {
				asdf = asdf.substring(1, asdf.length() - 2);

				asdf2 = expiryDates2("$" + asdf + "fin$");
				System.out.println("KABOOM!    " + asdf2);

				arModInvoiceLineItemDetails.setIliMisc(asdf2);
				System.out.println("asdf2: " + asdf2);
			}
			invIliList.add(arModInvoiceLineItemDetails);

			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(lineSeparator, start);
			length = nextIndex - start;

			int tempStart = nextIndex + 1;
			if (receipt.indexOf(separator, tempStart) == -1)
				break;

		}

		arModReceiptDetails.setInvIliList(invIliList);

		return arModReceiptDetails;

	}

	private String getBranchTaxCOA(String BR_BRNCH_CODE, String segment1, String segment3, String segment4,
			String segment5) {

		String segment2 = "";

		System.out.println("BR_BRNCH_CODE: " + BR_BRNCH_CODE);

		if (BR_BRNCH_CODE.equals("Manila-Smoking Lounge")) {
			segment1 = "MA";
			segment2 = "SL";
		} else if (BR_BRNCH_CODE.equals("Manila-Cigar Shop")) {
			segment1 = "MA";
			segment2 = "CS";
		} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Domestic")) {
			segment1 = "MA";
			segment2 = "TD";
		} else if (BR_BRNCH_CODE.equals("Manila-Term.#2 Intl")) {
			segment1 = "MA";
			segment2 = "TI";
		} else if (BR_BRNCH_CODE.equals("Cebu-Banilad")) {
			segment1 = "CE";
			segment2 = "BA";
		} else if (BR_BRNCH_CODE.equals("Cebu-Gorordo")) {
			segment1 = "CE";
			segment2 = "GO";
		} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Domestic")) {
			segment1 = "CE";
			segment2 = "MD";
		} else if (BR_BRNCH_CODE.equals("Cebu-Mactan Intl")) {
			segment1 = "CE";
			segment2 = "MI";
		} else if (BR_BRNCH_CODE.equals("Manila-Zara Marketing")) {
			segment1 = "MA";
			segment2 = "ZA";
		} else if (BR_BRNCH_CODE.equals("Cebu-Supercat")) {
			segment1 = "CE";
			segment2 = "SU";
		} else {
			segment1 = "HQ";
			segment2 = "HQ";
		}

		System.out.println("segment1: " + segment1);
		System.out.println("segment2: " + segment2);

		return segment1 + "-" + segment2 + "-" + segment3 + "-" + segment4 + "-" + segment5;
	}

	private ArModReceiptDetails receiptDecode(String receipt) throws Exception {

		Debug.print("ArMiscReceiptSyncControllerBean receiptDecode");

		String separator = "$";
		ArModReceiptDetails arModReceiptDetails = new ArModReceiptDetails();

		// Remove first $ character
		receipt = receipt.substring(1);

		// customer code
		int start = 0;
		int nextIndex = receipt.indexOf(separator, start);
		int length = nextIndex - start;
		arModReceiptDetails.setRctCstCustomerCode(receipt.substring(start, start + length));

		// payment type / TAX CODE
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctTcName(receipt.substring(start, start + length));

		// or number
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctNumber(receipt.substring(start, start + length));
		System.out.println("or-" + receipt.substring(start, start + length));

		// receipt source
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// currency
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctFcName(receipt.substring(start, start + length));

		// currency rate
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctConversionRate(Double.parseDouble(receipt.substring(start, start + length)));

		// cash amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCashAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// check amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCheckAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// card amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCardAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// total amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosTotalAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// discount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosDiscount(Double.parseDouble(receipt.substring(start, start + length)));

		// void
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// void reason
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// void amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosVoidAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// date
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		sdf.setLenient(false);
		try {
			arModReceiptDetails.setRctDate(sdf.parse(receipt.substring(start, start + length)));
			System.out.println("DAte-" + arModReceiptDetails.getRctDate());
		} catch (Exception ex) {

			throw ex;
		}

		// location
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		/*
		 * RctPaymentMethod is Used Temporarily for Payment Term Field
		 */
		// payment method / Payment Term
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPaymentMethod(receipt.substring(start, start + length));

		// sc amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		try {
			arModReceiptDetails.setRctPosScAmount(Double.parseDouble(receipt.substring(start, start + length)));
		} catch (Exception ex) {

		}

		// dc amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		try {
			arModReceiptDetails.setRctPosDcAmount(Double.parseDouble(receipt.substring(start, start + length)));
		} catch (Exception ex) {

		}

		// on account
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosOnAccount((byte) Integer.parseInt(receipt.substring(start, start + length)));

		// end separator
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		String lineSeparator = "~";
		// begin lineSeparator
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(lineSeparator, start);
		length = nextIndex - start;

		ArrayList invIliList = new ArrayList();

		while (true) {

			ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = new ArModInvoiceLineItemDetails();

			// begin separator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;

			// line number
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliLine(Short.parseShort(receipt.substring(start, start + length)));

			// item name
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliIiName(receipt.substring(start, start + length));
			System.out.println(arModInvoiceLineItemDetails.getIliIiName());
			// quantity
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliQuantity(Double.parseDouble(receipt.substring(start, start + length)));

			// price
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliUnitPrice(Double.parseDouble(receipt.substring(start, start + length)));

			// total
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliAmount(Double.parseDouble(receipt.substring(start, start + length)));

			// uom
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliUomName(receipt.substring(start, start + length));

			// location
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliLocName(receipt.substring(start, start + length));

			invIliList.add(arModInvoiceLineItemDetails);

			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(lineSeparator, start);
			length = nextIndex - start;

			int tempStart = nextIndex + 1;
			if (receipt.indexOf(separator, tempStart) == -1)
				break;

		}

		arModReceiptDetails.setInvIliList(invIliList);

		return arModReceiptDetails;

	}

	private ArModReceiptDetails receiptDecodeUS(String receipt) throws Exception {

		Debug.print("ArMiscReceiptSyncControllerBean receiptDecode");

		String separator = "$";
		ArModReceiptDetails arModReceiptDetails = new ArModReceiptDetails();

		// Remove first $ character
		receipt = receipt.substring(1);

		// customer code
		int start = 0;
		int nextIndex = receipt.indexOf(separator, start);
		int length = nextIndex - start;
		arModReceiptDetails.setRctCstCustomerCode(receipt.substring(start, start + length));

		// payment type / TAX CODE
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctTcName(receipt.substring(start, start + length));

		// or number
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctNumber(receipt.substring(start, start + length));
		System.out.println("or-" + receipt.substring(start, start + length));

		// receipt source
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctSource(receipt.substring(start, start + length));

		// currency
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctFcName(receipt.substring(start, start + length));

		// currency rate
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctConversionRate(Double.parseDouble(receipt.substring(start, start + length)));

		// cash amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCashAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// check amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCheckAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// card amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCardAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// total amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosTotalAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// discount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosDiscount(Double.parseDouble(receipt.substring(start, start + length)));

		// void
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// void reason
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// void amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosVoidAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// date
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		sdf.setLenient(false);
		try {
			arModReceiptDetails.setRctDate(sdf.parse(receipt.substring(start, start + length)));
			System.out.println("DAte-" + arModReceiptDetails.getRctDate());
		} catch (Exception ex) {

			throw ex;
		}

		// location
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		/*
		 * RctPaymentMethod is Used Temporarily for Payment Term Field
		 */
		// payment method / Payment Term
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPaymentMethod(receipt.substring(start, start + length));

		// sc amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		try {
			arModReceiptDetails.setRctPosScAmount(Double.parseDouble(receipt.substring(start, start + length)));
		} catch (Exception ex) {

		}

		// dc amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		try {
			arModReceiptDetails.setRctPosDcAmount(Double.parseDouble(receipt.substring(start, start + length)));
		} catch (Exception ex) {

		}

		// on account
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosOnAccount((byte) Integer.parseInt(receipt.substring(start, start + length)));

		// end separator
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		String lineSeparator = "~";
		// begin lineSeparator
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(lineSeparator, start);
		length = nextIndex - start;

		ArrayList invIliList = new ArrayList();

		while (true) {

			ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = new ArModInvoiceLineItemDetails();

			// begin separator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;

			// line number
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliLine(Short.parseShort(receipt.substring(start, start + length)));

			// item name
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliIiName(receipt.substring(start, start + length));
			System.out.println(arModInvoiceLineItemDetails.getIliIiName());
			// quantity
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliQuantity(Double.parseDouble(receipt.substring(start, start + length)));

			// price
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliUnitPrice(Double.parseDouble(receipt.substring(start, start + length)));

			// total
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliAmount(Double.parseDouble(receipt.substring(start, start + length)));

			// uom
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliUomName(receipt.substring(start, start + length));

			// location
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliLocName(receipt.substring(start, start + length));

			// location
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliTaxCode(receipt.substring(start, start + length));

			invIliList.add(arModInvoiceLineItemDetails);

			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(lineSeparator, start);
			length = nextIndex - start;

			int tempStart = nextIndex + 1;
			if (receipt.indexOf(separator, tempStart) == -1)
				break;

		}

		arModReceiptDetails.setInvIliList(invIliList);

		return arModReceiptDetails;

	}

	private ArModReceiptDetails receiptDecodeWithExpiryDate(String receipt) throws Exception {

		Debug.print("ArMiscReceiptSyncControllerBean receiptDecodeWithExpiryDate");

		/// USED METHOD ENCODE
		String separator = "$";
		ArModReceiptDetails arModReceiptDetails = new ArModReceiptDetails();

		// Remove first $ character
		receipt = receipt.substring(1);

		// customer code
		int start = 0;
		int nextIndex = receipt.indexOf(separator, start);
		int length = nextIndex - start;

		arModReceiptDetails.setRctCstCustomerCode(receipt.substring(start, start + length));
		System.out.println("CUSTOMER CODE:" + receipt.substring(start, start + length));

		// payment type
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// or number
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctNumber(receipt.substring(start, start + length));
		System.out.println("or-" + receipt.substring(start, start + length));

		// pos name
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPOSName(receipt.substring(start, start + length));
		System.out.println("POS NAME" + receipt.substring(start, start + length));

		// receipt source
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		System.out.println("SOURCE:" + receipt.substring(start, start + length));
		length = nextIndex - start;

		// currency
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CRRNCY:" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctFcName(receipt.substring(start, start + length));

		// currency rate
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CRRNY RT:" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctConversionRate(Double.parseDouble(receipt.substring(start, start + length)));

		// cash amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CSH AMNT:" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctPosCashAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// check amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CHECK AMNT:" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctPosCheckAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// card amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CARD AMNT:" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctPosCardAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// total amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("TOTAL AMNT:" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctPosTotalAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// discount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("DISC:" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctPosDiscount(Double.parseDouble(receipt.substring(start, start + length)));

		// void
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// void reason
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// void amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosVoidAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// date
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		sdf.setLenient(false);
		try {
			arModReceiptDetails.setRctDate(sdf.parse(receipt.substring(start, start + length)));
			System.out.println("DAte-" + arModReceiptDetails.getRctDate());
		} catch (Exception ex) {

			throw ex;
		}

		// location
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);

		length = nextIndex - start;
		System.out.println("Location:" + receipt.substring(start, start + length));

		// payment method
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);

		length = nextIndex - start;
		System.out.println("payment method:" + receipt.substring(start, start + length));

		// sc amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);

		length = nextIndex - start;
		System.out.println("sc amount:" + receipt.substring(start, start + length));
		try {
			arModReceiptDetails.setRctPosScAmount(Double.parseDouble(receipt.substring(start, start + length)));
		} catch (Exception ex) {

		}

		// dc amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);

		length = nextIndex - start;
		System.out.println("dc amnt:" + receipt.substring(start, start + length));
		try {
			arModReceiptDetails.setRctPosDcAmount(Double.parseDouble(receipt.substring(start, start + length)));
		} catch (Exception ex) {

		}

		// on account
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("on account:" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctPosOnAccount(
				(byte) Integer.parseInt(receipt.substring(start, start + length) == "True" ? "1" : "0"));

		// vat code name
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("on account:" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctTcName(receipt.substring(start, start + length));

		// CUSTOMER NAME / NOTE
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CUSTOMER NAME" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctCstName(receipt.substring(start, start + length));

		// SALESPERSON
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("SALESPERSON" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctSlpSalespersonCode(receipt.substring(start, start + length));

		// CASH AMOUNT
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CASH AMOUNT" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctAmountCash(Double.parseDouble(receipt.substring(start, start + length)));

		// CARD NUMBER 1
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CARD NO 1" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctCardNumber1(receipt.substring(start, start + length));

		// CARD TYPE 1
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CARD TYPE 1" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctCardType1(receipt.substring(start, start + length));

		// CARD AMOUNT 1
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CARD AMOUNT 1" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctAmountCard1(Double.parseDouble(receipt.substring(start, start + length)));

		// CARD NUMBER 2
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CARD NO 2" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctCardNumber2(receipt.substring(start, start + length));

		// CARD TYPE
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CARD TYPE 2" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctCardType2(receipt.substring(start, start + length));

		// CARD AMOUNT 2
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CARD AMOUNT 2" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctAmountCard2(Double.parseDouble(receipt.substring(start, start + length)));

		// CARD NUMBER 3
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CARD NO 3 " + receipt.substring(start, start + length));
		arModReceiptDetails.setRctCardNumber3(receipt.substring(start, start + length));

		// CARD TYPE 3
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CARD TYPE 3" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctCardType3(receipt.substring(start, start + length));

		// CARD AMOUNT 3
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CARD AMOUNT 3" + receipt.substring(start, start + length));
		arModReceiptDetails.setRctAmountCard3(Double.parseDouble(receipt.substring(start, start + length)));

		// CHEQUE NUMBER
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CHEQUE NO  " + receipt.substring(start, start + length));
		arModReceiptDetails.setRctChequeNumber(receipt.substring(start, start + length));

		// CHEQUE AMOUNT
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("CHEQUE AMOUNT " + receipt.substring(start, start + length));
		arModReceiptDetails.setRctAmountCheque(Double.parseDouble(receipt.substring(start, start + length)));

		// VOUCHER NUMBER
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("VOUCHER NO " + receipt.substring(start, start + length));
		arModReceiptDetails.setRctVoucherNumber(receipt.substring(start, start + length));

		// VOUCHER AMOUNT
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		System.out.println("VOUCHER AMOUNT " + receipt.substring(start, start + length));
		arModReceiptDetails.setRctAmountVoucher(Double.parseDouble(receipt.substring(start, start + length)));

		// end separator
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		String lineSeparator = "~";
		// begin lineSeparator
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(lineSeparator, start);
		length = nextIndex - start;

		ArrayList invIliList = new ArrayList();

		while (true) {
			String asdf = "";
			String asdf2 = "";
			ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = new ArModInvoiceLineItemDetails();

			// begin separator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;

			// line number
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;

			arModInvoiceLineItemDetails.setIliLine(Short.parseShort(receipt.substring(start, start + length)));

			// item code

			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliCode(Integer.parseInt(receipt.substring(start, start + length)));
			System.out
					.println("receiptDecodeWithExpiryfate getIliIiName()" + arModInvoiceLineItemDetails.getIliIiName());
			// item name
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliIiName(receipt.substring(start, start + length));
			System.out.println(arModInvoiceLineItemDetails.getIliIiName());
			// quantity
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliQuantity(Double.parseDouble(receipt.substring(start, start + length)));

			// price
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliUnitPrice(Double.parseDouble(receipt.substring(start, start + length)));

			// total
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliAmount(Double.parseDouble(receipt.substring(start, start + length)));

			// total Discount
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails
					.setIliTotalDiscount(Double.parseDouble(receipt.substring(start, start + length)));

			// uom
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliUomName(receipt.substring(start, start + length));

			// EMEI no use
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setILI_IMEI(receipt.substring(start, start + length));

			// location
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliLocName(receipt.substring(start, start + length));

//			Expiry Date / IMEI
			start = nextIndex + 1;
			nextIndex = receipt.indexOf("$~", start);
			length = nextIndex - start;

			asdf = receipt.substring(start);
			// MessageBox.Show("asdf: " + asdf);
			if (asdf != "~~") {
				asdf = asdf.substring(1, asdf.length() - 2);

				// OLD CODE
				asdf2 = expiryDates2("$" + asdf + "fin$");
				System.out.println("KABOOM!    " + asdf2);

				// arModInvoiceLineItemDetails.setIliMisc(asdf2);
				arModInvoiceLineItemDetails.setILI_IMEI(asdf2);
				System.out.println("asdf2: " + asdf2);
				//

				// NEW CODE
				System.out.println("asdf2: " + asdf);
				ArrayList tagList = this.convertMiscToInvModTagList(asdf);
				arModInvoiceLineItemDetails.setIliTagList(tagList);
			}
			invIliList.add(arModInvoiceLineItemDetails);

			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(lineSeparator, start);
			length = nextIndex - start;

			int tempStart = nextIndex + 1;
			if (receipt.indexOf(separator, tempStart) == -1)
				break;

		}

		arModReceiptDetails.setInvIliList(invIliList);

		return arModReceiptDetails;

	}

	private ArModReceiptDetails receiptDecodeWithExpiryDateAndEntries(String receipt) throws Exception {

		Debug.print("ArMiscReceiptSyncControllerBean receiptDecodeWithExpiryDate");

		String separator = "$";
		ArModReceiptDetails arModReceiptDetails = new ArModReceiptDetails();

		// Remove first $ character
		receipt = receipt.substring(1);

		// customer code
		int start = 0;
		int nextIndex = receipt.indexOf(separator, start);
		int length = nextIndex - start;
		arModReceiptDetails.setRctCstCustomerCode(receipt.substring(start, start + length));

		// payment type
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// or number
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctNumber(receipt.substring(start, start + length));
		System.out.println("or-" + receipt.substring(start, start + length));

		// receipt source
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// currency
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctFcName(receipt.substring(start, start + length));

		// currency rate
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctConversionRate(Double.parseDouble(receipt.substring(start, start + length)));

		// cash amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCashAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// check amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCheckAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// card amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosCardAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// total amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosTotalAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// discount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosDiscount(Double.parseDouble(receipt.substring(start, start + length)));

		// void
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// void reason
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// void amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosVoidAmount(Double.parseDouble(receipt.substring(start, start + length)));

		// date
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		sdf.setLenient(false);
		try {
			arModReceiptDetails.setRctDate(sdf.parse(receipt.substring(start, start + length)));
			System.out.println("DAte-" + arModReceiptDetails.getRctDate());
		} catch (Exception ex) {

			throw ex;
		}

		// location
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// payment method
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		// sc amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		try {
			arModReceiptDetails.setRctPosScAmount(Double.parseDouble(receipt.substring(start, start + length)));
		} catch (Exception ex) {

		}

		// dc amount
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		try {
			arModReceiptDetails.setRctPosDcAmount(Double.parseDouble(receipt.substring(start, start + length)));
		} catch (Exception ex) {

		}

		// on account
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;
		arModReceiptDetails.setRctPosOnAccount((byte) Integer.parseInt(receipt.substring(start, start + length)));

		// end separator
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(separator, start);
		length = nextIndex - start;

		String lineSeparator = "~";
		// begin lineSeparator
		start = nextIndex + 1;
		nextIndex = receipt.indexOf(lineSeparator, start);
		length = nextIndex - start;

		ArrayList invIliList = new ArrayList();

		while (true) {
			String asdf = "";

			ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = new ArModInvoiceLineItemDetails();

			// begin separator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;

			// line number
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliLine(Short.parseShort(receipt.substring(start, start + length)));

			// item name
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliIiName(receipt.substring(start, start + length));
			System.out.println(arModInvoiceLineItemDetails.getIliIiName());
			// quantity
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliQuantity(Double.parseDouble(receipt.substring(start, start + length)));

			// price
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliUnitPrice(Double.parseDouble(receipt.substring(start, start + length)));

			// total
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliAmount(Double.parseDouble(receipt.substring(start, start + length)));

			// uom
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliUomName(receipt.substring(start, start + length));

			// location
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliLocName(receipt.substring(start, start + length));

//			Expiry Date
			start = nextIndex + 1;
			nextIndex = receipt.indexOf("$~", start);
			length = nextIndex - start;

			asdf = receipt.substring(start);
			// MessageBox.Show("asdf: " + asdf);
			System.out.println("asdf");
			if (asdf != "~~") {
				asdf = asdf.substring(1, asdf.length() - 2);

				arModInvoiceLineItemDetails.setIliMisc(asdf);
				System.out.println("asdf: " + asdf);
			}
			invIliList.add(arModInvoiceLineItemDetails);

			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(lineSeparator, start);
			length = nextIndex - start;

			int tempStart = nextIndex + 1;
			if (receipt.indexOf(separator, tempStart) == -1)
				break;

		}

		arModReceiptDetails.setInvIliList(invIliList);

		ArrayList jrnlIliList = new ArrayList();

		while (true) {
			ArModInvoiceLineItemDetails arModInvoiceLineItemDetails = new ArModInvoiceLineItemDetails();

			// begin separator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;

			// line number
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(separator, start);
			length = nextIndex - start;
			arModInvoiceLineItemDetails.setIliLine(Short.parseShort(receipt.substring(start, start + length)));
			/*
			 * // Debit cash start = nextIndex + 1; nextIndex = receipt.indexOf(separator,
			 * start); length = nextIndex - start;
			 * arModInvoiceLineItemDetails.setDebitCash(Double.parseDouble(receipt.substring
			 * (start, start + length)));
			 * System.out.println(arModInvoiceLineItemDetails.getDebitCash());
			 * 
			 * // Credit Revenue start = nextIndex + 1; nextIndex =
			 * receipt.indexOf(separator, start); length = nextIndex - start;
			 * arModInvoiceLineItemDetails.setCreditCash(Double.parseDouble(receipt.
			 * substring(start, start + length)));
			 * 
			 * // Debit Cost of Sales start = nextIndex + 1; nextIndex =
			 * receipt.indexOf(separator, start); length = nextIndex - start;
			 * arModInvoiceLineItemDetails.setDebitCostofSales(Double.parseDouble(receipt.
			 * substring(start, start + length)));
			 * 
			 * // Credit inventory start = nextIndex + 1; nextIndex =
			 * receipt.indexOf(separator, start); length = nextIndex - start;
			 * arModInvoiceLineItemDetails.setCreditInventory(Double.parseDouble(receipt.
			 * substring(start, start + length)));
			 * 
			 * // Debit expenses per account start = nextIndex + 1; nextIndex =
			 * receipt.indexOf(separator, start); length = nextIndex - start;
			 * arModInvoiceLineItemDetails.setDebitExpensesPerAccount(Double.parseDouble(
			 * receipt.substring(start, start + length)));
			 * 
			 * // Credit Cash start = nextIndex + 1; nextIndex = receipt.indexOf(separator,
			 * start); length = nextIndex - start;
			 * arModInvoiceLineItemDetails.setCreditCash(Double.parseDouble(receipt.
			 * substring(start, start + length)));
			 * 
			 * jrnlIliList.add(arModInvoiceLineItemDetails);
			 */
			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = receipt.indexOf(lineSeparator, start);
			length = nextIndex - start;

			break;
		}

		// arModReceiptDetails.setJrnlIliList(jrnlIliList);
		return arModReceiptDetails;

	}

	private int expiryDates(String misc) throws Exception {
		Debug.print("ApReceivingItemControllerBean getExpiryDates");
		System.out.println("misc: " + misc);
		String separator = "$";

		// Remove first $ character
		misc = misc.substring(1);

		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;

		ArrayList miscList = new ArrayList();
		String checker = "";
		int qty = 0;
		try {
			while (!checker.equals("fin")) {

				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;

				checker = misc.substring(start, start + length);
				System.out.println("checker: " + checker);
				if (checker.length() != 0 || checker.equals("~")) {
					break;
				}
				if (checker.equals("fin")) {
					break;
				}
				if (checker.length() != 0 || checker != "null") {
					miscList.add(checker);
					qty++;
				} else {
					miscList.add("null");
				}
			}
		} catch (Exception e) {
			qty = qty - 1;
		}

		System.out.println("qty :" + qty);
		return qty;
	}

	private ArrayList convertMiscToInvModTagList(String misc) {
		ArrayList list = new ArrayList();

		String[] imei_array = misc.split("\\$");

		System.out.println("to be split: " + misc);

		for (int x = 0; x < imei_array.length; x++) {
			if (imei_array[x].trim().equals("")) {
				continue;
			}
			InvModTagListDetails details = new InvModTagListDetails();
			System.out.println("tokenized: " + imei_array[x]);
			details.setTgPropertyCode("");
			details.setTgSpecs("");
			details.setTgTransactionDate(new Date());
			details.setTgSerialNumber(imei_array[x]);

			list.add(details);
		}

		return list;

	}

	private String expiryDates2(String misc) throws Exception {
		Debug.print("ApReceivingItemControllerBean getExpiryDates");
		System.out.println("misc: " + misc);
		String separator = "$";

		// Remove first $ character
		misc = misc.substring(1);

		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;

		ArrayList miscList = new ArrayList();
		String checker = "";
		String checker2 = "";
		int qty = 0;
		try {
			while (!checker.equals("fin")) {

				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;

				checker = misc.substring(start, start + length);
				System.out.println("checker: " + checker);
				if (checker.length() == 0 || checker.equals("~")) {
					break;
				}
				if (checker.equals("fin")) {
					break;
				}
				if (checker.length() != 0 || checker != "null") {
					miscList.add(checker);
					checker2 = checker2 + "$" + checker;
					qty++;
				} else {
					miscList.add("null");
				}
			}
		} catch (Exception e) {
			qty = qty - 1;
		}
		String retExp = "";
		if (qty != 0) {
			retExp = "$" + qty + checker2 + "$";
		}

		// System.out.println("qty :" + qty);
		return retExp;
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ArInvoiceEntryControllerBean getGlFcPrecisionUnit");

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private String generateCmFundTransfer(ArModReceiptDetails arModReceiptDetails, String fundTransferNumber,
			String CASHIER, Integer AD_BRNCH, Integer AD_CMPNY) throws Exception {

		Debug.print("ArMiscReceiptSyncControllerBean generateCmFundTransfer");

		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;

		try {

			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory.lookUpLocalHome(
					LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME,
							LocalAdBranchDocumentSequenceAssignmentHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome) EJBHomeFactory
					.lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// generate fund transfer number

			String generatedFTNumber = null;

			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
			LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

			try {

				adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("CM FUND TRANSFER",
						AD_CMPNY);

			} catch (FinderException ex) {

			}

			try {

				adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome
						.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

			} catch (FinderException ex) {

			}

			while (true) {

				if (adBranchDocumentSequenceAssignment == null
						|| adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

					try {

						cmFundTransferHome.findByFtDocumentNumberAndBrCode(
								adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
						adDocumentSequenceAssignment.setDsaNextSequence(
								EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

					} catch (FinderException ex) {

						generatedFTNumber = adDocumentSequenceAssignment.getDsaNextSequence();
						adDocumentSequenceAssignment.setDsaNextSequence(
								EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
						break;

					}

				} else {

					try {

						cmFundTransferHome.findByFtDocumentNumberAndBrCode(
								adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
						adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon
								.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

					} catch (FinderException ex) {

						generatedFTNumber = adBranchDocumentSequenceAssignment.getBdsNextSequence();
						adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon
								.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
						break;

					}

				}

			}

			if (fundTransferNumber == null) {

				fundTransferNumber = generatedFTNumber;

			}

			return fundTransferNumber;

		} catch (Exception ex) {

			throw ex;

		}

	}

	private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem,
			double ADJST_QTY, Integer AD_CMPNY) {

		Debug.print("ArMiscReceiptSyncControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome
					.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
			LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
					.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(),
							AD_CMPNY);

			return EJBCommon.roundIt(
					ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor()
							/ invUnitOfMeasureConversion.getUmcConversionFactor(),
					adPreference.getPrfInvQuantityPrecisionUnit());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addArDrIliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE,
			LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY)
			throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArMiscReceiptEntryControllerBean addArDrIliEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory
					.lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(COA_CODE);

			// create distribution record

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
					EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), EJBCommon.FALSE,
					EJBCommon.FALSE, AD_CMPNY);

			// arReceipt.addArDistributionRecord(arDistributionRecord);
			arDistributionRecord.setArReceipt(arReceipt);

			// glChartOfAccount.addArDistributionRecord(arDistributionRecord);
			arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

		} catch (FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addArDrIliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE,
			LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
			throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArMiscReceiptEntryControllerBean addArDrIliEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory
					.lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(COA_CODE);

			// create distribution record

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
					EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), EJBCommon.FALSE,
					EJBCommon.FALSE, AD_CMPNY);

			// arInvoice.addArDistributionRecord(arDistributionRecord);
			arDistributionRecord.setArInvoice(arInvoice);
			// glChartOfAccount.addArDistributionRecord(arDistributionRecord);
			arDistributionRecord.setGlChartOfAccount(glChartOfAccount);
		} catch (FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addArDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, byte DR_RVRSD,
			LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY)
			throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArMiscReceipSyncControllerBean addArDrEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdBranchHome adBranchHome = null;

		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory
					.lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);
			adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
					LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(COA_CODE);
			System.out.println("COA_CODE: " + COA_CODE + "    COA_DESC: " + glChartOfAccount.getCoaAccountNumber());
			if (DR_CLSS.equals("DISCOUNT") || DR_CLSS.equals("FOREX")) {

				LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(AD_BRNCH);
				System.out.println("adBranch.getBrCoaSegment() = " + adBranch.getBrCoaSegment());
				String newCoa = adBranch.getBrCoaSegment();

				StringTokenizer st = new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), "-");
				int ctr = 0;
				while (st.hasMoreTokens()) {
					ctr++;
					if (ctr >= 3) {
						newCoa = newCoa + "-" + st.nextToken();
					} else {
						st.nextToken();
					}
				}
				System.out.println("MIRACLE: " + newCoa);
				System.out.println("glChartOfAccountHome.findByCoaAccountNumber(" + newCoa + ", " + AD_CMPNY + ")");

				glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(newCoa, AD_CMPNY);

			}

			// create distribution record
			System.out.println("Create Dstrbtn Rcrd");
			System.out.println("DR_CLSS: " + DR_CLSS);
			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
					EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), EJBCommon.FALSE,
					DR_RVRSD, AD_CMPNY);

			// arReceipt.addArDistributionRecord(arDistributionRecord);
			arDistributionRecord.setArReceipt(arReceipt);
			// glChartOfAccount.addArDistributionRecord(arDistributionRecord);
			arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

		} catch (FinderException ex) {
			ex.printStackTrace();
			throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, boolean isAdjustFifo,
			Integer AD_BRNCH, Integer AD_CMPNY) {

		LocalInvCostingHome invCostingHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;

		// Initialize EJB Home

		try {

			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			Collection invFifoCostings = invCostingHome
					.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH,
							AD_CMPNY);

			if (invFifoCostings.size() > 0) {

				Iterator x = invFifoCostings.iterator();

				if (isAdjustFifo) {

					// executed during POST transaction

					double totalCost = 0d;
					double cost;

					if (CST_QTY < 0) {

						// for negative quantities
						double neededQty = -(CST_QTY);

						while (x.hasNext() && neededQty != 0) {

							LocalInvCosting invFifoCosting = (LocalInvCosting) x.next();

							if (invFifoCosting.getApPurchaseOrderLine() != null
									|| invFifoCosting.getApVoucherLineItem() != null) {
								cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
							} else if (invFifoCosting.getArInvoiceLineItem() != null) {
								cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
							} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
								cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
							} else {
								cost = invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity();
							}

							if (neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

								invFifoCosting.setCstRemainingLifoQuantity(
										invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
								totalCost += (neededQty * cost);
								neededQty = 0d;
							} else {

								neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
								totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
								invFifoCosting.setCstRemainingLifoQuantity(0);
							}
						}

						// if needed qty is not yet satisfied but no more quantities to fetch, get the
						// default cost
						if (neededQty != 0) {

							LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
							totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
						}

						cost = totalCost / -CST_QTY;
					}

					else {

						// for positive quantities
						cost = CST_COST;
					}
					return cost;
				}

				else {

					// executed during ENTRY transaction

					LocalInvCosting invFifoCosting = (LocalInvCosting) x.next();

					if (invFifoCosting.getApPurchaseOrderLine() != null
							|| invFifoCosting.getApVoucherLineItem() != null) {
						return EJBCommon.roundIt(
								invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(),
								this.getGlFcPrecisionUnit(AD_CMPNY));
					} else if (invFifoCosting.getArInvoiceLineItem() != null) {
						return EJBCommon.roundIt(
								invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(),
								this.getGlFcPrecisionUnit(AD_CMPNY));
					} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
						return EJBCommon.roundIt(
								invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(),
								this.getGlFcPrecisionUnit(AD_CMPNY));
					} else {
						return EJBCommon.roundIt(
								invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity(),
								this.getGlFcPrecisionUnit(AD_CMPNY));
					}
				}
			} else {

				// most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}

		} catch (Exception ex) {
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
		}
	}

	private void addArDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, byte DR_RVRSD,
			LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
			throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArMiscReceipSyncControllerBean addArDrEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdBranchHome adBranchHome = null;

		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory
					.lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);
			adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
					LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(COA_CODE);

			if (DR_CLSS.equals("DISCOUNT") || DR_CLSS.equals("FOREX")) {
				System.out.println("DR_CLSS: " + DR_CLSS);
				LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(AD_BRNCH);
				String newCoa = adBranch.getBrCoaSegment();
				StringTokenizer st = new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), "-");
				int ctr = 0;
				while (st.hasMoreTokens()) {
					ctr++;
					if (ctr >= 3) {
						newCoa = newCoa + "-" + st.nextToken();
					} else {
						st.nextToken();
					}
				}

				glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(newCoa, AD_CMPNY);

			}

			// create distribution record

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
					EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), EJBCommon.FALSE,
					DR_RVRSD, AD_CMPNY);

			// arInvoice.addArDistributionRecord(arDistributionRecord);
			arDistributionRecord.setArInvoice(arInvoice);
			// glChartOfAccount.addArDistributionRecord(arDistributionRecord);
			arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

		} catch (FinderException ex) {
			ex.printStackTrace();
			throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method
	 */
	public String modifyInvoiceJournal(String invoiceRefNum, Integer AD_CMPNY) {
		String results = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		try {
			arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory.lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME,
					LocalArInvoiceHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory
					.lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}
		LocalArInvoice arInvoice = null;

		try {
			arInvoice = arInvoiceHome.findByReferenceNumberAndCompanyCode(invoiceRefNum, AD_CMPNY);
		} catch (Exception ex) {

		}

		HashMap arAccountMap = new HashMap();

		arAccountMap.put("Real Property Tax", 28);
		arAccountMap.put("Ajustment", 45);
		arAccountMap.put("Insurance", 26);
		arAccountMap.put("Electricity Consumption", 20);
		arAccountMap.put("Adjustment - Electricity", 39);
		arAccountMap.put("Parking Rental", 45);
		arAccountMap.put("Adjustmesnt - Inv#390", 15);
		arAccountMap.put("Adjustments", 32);
		arAccountMap.put("Real Estate Tax (Land)", 28);
		arAccountMap.put("Emergency Consumption", 39);
		arAccountMap.put("Adjustment -  Emergency Power", 40);
		arAccountMap.put("Storage Rental", 45);
		arAccountMap.put("Car Pass", 43);
		arAccountMap.put("Work Order - Aircon Cleaning/Installation", 42);
		arAccountMap.put("Work Order - replacement of bulb", 42);
		arAccountMap.put("Work Order - Grease Trap", 44);
		arAccountMap.put("Adjustment - ASD Increase", 16);
		arAccountMap.put("Adjustment - per OR#69018 21,695.40 (posted 24,261.95)", 16);
		arAccountMap.put("Work Order - Declogging", 42);
		arAccountMap.put("Association Dues", 16);
		arAccountMap.put("Space Rental", 32);
		arAccountMap.put("Aircon Extension", 24);
		arAccountMap.put("Work Order", 42);
		arAccountMap.put("Common Area Lipo", 18);
		arAccountMap.put("Rental", 45);
		arAccountMap.put("Water Consumption", 22);
		arAccountMap.put("Adjustment - EWT 2%", 38);
		arAccountMap.put("Adjustment - Common Area LIPO", 18);

		if (arInvoice != null) {
			Collection arInvoiceLines = arInvoice.getArInvoiceLines();
			Iterator p = arInvoiceLines.iterator();

			HashMap cstMap = new HashMap();
			while (p.hasNext()) {

				LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine) p.next();
				// arInvoiceLine.getArStandardMemoLine();

				System.out.println(arAccountMap.get(arInvoiceLine.getArStandardMemoLine().getSmlName()).toString());
				System.out.println("arInvoiceLine.getArStandardMemoLine().getSmlName(): "
						+ arInvoiceLine.getArStandardMemoLine().getSmlName());
				if (arAccountMap.containsKey(arInvoiceLine.getArStandardMemoLine().getSmlName())) {
					try {
						System.out.println("Debit: " + arInvoiceLine.getIlAmount());
						this.addArDrEntry(arInvoice.getArDrNextLine(), "OTHERS", EJBCommon.TRUE,
								arInvoiceLine.getIlAmount(), Integer.parseInt(arAccountMap
										.get(arInvoiceLine.getArStandardMemoLine().getSmlName()).toString()),
								EJBCommon.FALSE, arInvoice, 1, AD_CMPNY);
					} catch (GlobalBranchAccountNumberInvalidException ex) {
						results = "Branch Account Number Invalid";
					} catch (NumberFormatException ex) {
						results = "0";
					}

				}

			}

			LocalArDistributionRecord arDistributionRecords = null;

			try {
				arDistributionRecords = arDistributionRecordHome.findByDrClassAndInvCode("RECEIVABLE",
						arInvoice.getInvCode(), AD_CMPNY);
				try {
					System.out.println("Credit: " + arDistributionRecords.getDrAmount());
					this.addArDrEntry(arInvoice.getArDrNextLine(), "OTHERS", EJBCommon.FALSE,
							arDistributionRecords.getDrAmount(),
							arDistributionRecords.getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE, arInvoice, 1,
							AD_CMPNY);
				} catch (GlobalBranchAccountNumberInvalidException ex) {
					results = "Branch Account Number Invalid";
				} catch (NumberFormatException ex) {
					results = "0";
				}
			} catch (Exception ex) {
				results = "Cannot find receivabl account";
			}
			/*
			 * Collection arDistributionRecords = arInvoice.getArDistributionRecords();
			 * Iterator i = arDistributionRecords.iterator(); while (i.hasNext()) {
			 * LocalArDistributionRecord arDistributionRecord =
			 * (LocalArDistributionRecord)p.next(); System.out.println("DR CLASS: " +
			 * arDistributionRecord.getDrClass());
			 * if(arDistributionRecord.getDrClass()=="RECEIVABLE"){ try{
			 * System.out.println("Credit: " + arDistributionRecord.getDrAmount());
			 * this.addArDrEntry(arInvoice.getArDrNextLine(), "OTHERS", EJBCommon.FALSE,
			 * arDistributionRecord.getDrAmount(),
			 * arDistributionRecord.getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE,
			 * arInvoice, 1, AD_CMPNY); }catch(GlobalBranchAccountNumberInvalidException
			 * ex){ results="Branch Account Number Invalid"; }catch(NumberFormatException
			 * ex){ results="0"; } } }
			 */

		}
		if (results != "0" && results != "Branch Account Number Invalid") {
			results = "1";
		}
		try {
			results = arInvoice.getInvNumber();
		} catch (Exception e) {

		}

		System.out.println(results);
		return results;
	}

	/**
	 * @ejb:interface-method
	 */
	public String modifyReceiptJournal(String invoiceRefNum, String[] payment, double[] paymentAmount,
			Integer AD_CMPNY) {
		String results = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		int arrayLength = payment.length;
		try {
			arReceiptHome = (LocalArReceiptHome) EJBHomeFactory.lookUpLocalHome(LocalArReceiptHome.JNDI_NAME,
					LocalArReceiptHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory
					.lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}
		LocalArReceipt arReceipt = null;

		try {
			arReceipt = arReceiptHome.findByReferenceNumberAndCompanyCode(invoiceRefNum, AD_CMPNY);
		} catch (Exception ex) {

		}

		HashMap arAccountMap = new HashMap();

		arAccountMap.put("Real Property Tax", 28);
		arAccountMap.put("Ajustment", 45);
		arAccountMap.put("Insurance", 26);
		arAccountMap.put("Electricity Consumption", 20);
		arAccountMap.put("Adjustment - Electricity", 39);
		arAccountMap.put("Parking Rental", 45);
		arAccountMap.put("Adjustmesnt - Inv#390", 15);
		arAccountMap.put("Adjustments", 32);
		arAccountMap.put("Real Estate Tax (Land)", 28);
		arAccountMap.put("Emergency Consumption", 39);
		arAccountMap.put("Adjustment -  Emergency Power", 40);
		arAccountMap.put("Storage Rental", 45);
		arAccountMap.put("Car Pass", 43);
		arAccountMap.put("Work Order - Aircon Cleaning/Installation", 42);
		arAccountMap.put("Work Order - replacement of bulb", 42);
		arAccountMap.put("Work Order - Grease Trap", 44);
		arAccountMap.put("Adjustment - ASD Increase", 16);
		arAccountMap.put("Adjustment - per OR#69018 21,695.40 (posted 24,261.95)", 16);
		arAccountMap.put("Work Order - Declogging", 42);
		arAccountMap.put("Association Dues", 16);
		arAccountMap.put("Space Rental", 32);
		arAccountMap.put("Aircon Extension", 24);
		arAccountMap.put("Work Order", 42);
		arAccountMap.put("Common Area Lipo", 18);
		arAccountMap.put("Rental", 45);
		arAccountMap.put("Water Consumption", 22);
		arAccountMap.put("Adjustment - EWT 2%", 38);
		arAccountMap.put("Adjustment - Common Area LIPO", 18);

		if (arReceipt != null) {
			System.out.println("arReceipt.getRctType(): " + arReceipt.getRctType());
			if (arReceipt.getRctType() != "MISC") {

				Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();
				Iterator p = arAppliedInvoices.iterator();

				HashMap cstMap = new HashMap();
				/*
				 * while (p.hasNext()) {
				 * 
				 * LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)p.next();
				 * 
				 * Collection arInvoiceLines =
				 * arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().
				 * getArInvoiceLines(); Iterator a = arInvoiceLines.iterator();
				 * //proRate=arAppliedInvoice.getAiApplyAmount()*0.6;
				 * 
				 * while (a.hasNext()) { LocalArInvoiceLine arInvoiceLine =
				 * (LocalArInvoiceLine)a.next();
				 * 
				 * 
				 * } }
				 */

				for (int h = 0; h < arrayLength; h++) {
					if (arAccountMap.containsKey(payment[h])) {
						System.out.println(arAccountMap.get(payment[h]).toString());
						System.out.println("payment[h]: " + payment[h]);
						// if(arAccountMap.containsKey(arInvoiceLine.getArStandardMemoLine().getSmlName())){
						try {
							System.out.println("Credit: " + paymentAmount[h]);
							this.addArDrEntry(arReceipt.getArDrNextLine(), "OTHERS", EJBCommon.FALSE, paymentAmount[h],
									Integer.parseInt(arAccountMap.get(payment[h]).toString()), EJBCommon.FALSE,
									arReceipt, 1, AD_CMPNY);
						} catch (GlobalBranchAccountNumberInvalidException ex) {
							results = "Branch Account Number Invalid";
						} catch (NumberFormatException ex) {
							results = "0";
						}

						// }
					}
				}
				LocalArDistributionRecord arDistributionRecords = null;

				try {
					arDistributionRecords = arDistributionRecordHome.findByDrClassAndRctCode("RECEIVABLE",
							arReceipt.getRctCode(), AD_CMPNY);

					try {
						System.out.println("Debit: " + arDistributionRecords.getDrAmount());
						this.addArDrEntry(arReceipt.getArDrNextLine(), "OTHERS", EJBCommon.TRUE,
								arDistributionRecords.getDrAmount(),
								arDistributionRecords.getGlChartOfAccount().getCoaCode(), EJBCommon.FALSE, arReceipt, 1,
								AD_CMPNY);
					} catch (GlobalBranchAccountNumberInvalidException ex) {
						results = "Branch Account Number Invalid";
					} catch (NumberFormatException ex) {
						results = "0";
					}
				} catch (Exception ex) {
					results = "Cannot find receivabl account";
				}

			}

		}
		if (results != "0" && results != "Branch Account Number Invalid") {
			results = "1";
		}
		try {
			results = arReceipt.getRctNumber();
		} catch (Exception e) {

		}

		System.out.println(results);
		return results;
	}

	/**
	 * @ejb:interface-method
	 */
	public String[] getArIpsByCstCustomerCode(String CST_CSTMR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ArReceiptEntryControllerBean getArIpsByCstCustomerCode");

		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalAdDiscountHome adDiscountHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;

		ArrayList list = new ArrayList();

		String[] results = null;
		// Initialize EJB Home

		try {

			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome) EJBHomeFactory.lookUpLocalHome(
					LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			adDiscountHome = (LocalAdDiscountHome) EJBHomeFactory.lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME,
					LocalAdDiscountHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arInvoicePaymentSchedules = arInvoicePaymentScheduleHome
					.findOpenIpsByIpsLockAndCstCustomerCodeAndBrCode(EJBCommon.FALSE, CST_CSTMR_CODE, AD_BRNCH,
							AD_CMPNY);

			if (arInvoicePaymentSchedules != null) {

				short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

				Iterator i = arInvoicePaymentSchedules.iterator();

				int ctr = 0;
				char separator = EJBCommon.SEPARATOR;
				int lengthOriginal = arInvoicePaymentSchedules.size();
				System.out.println("lengthOriginal: " + lengthOriginal);
				results = new String[lengthOriginal];
				while (i.hasNext()) {

					LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule) i.next();

					// verification if ips is already closed
					if (EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue(), precisionUnit) == EJBCommon
							.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid(), precisionUnit))
						continue;

					ArModInvoicePaymentScheduleDetails mdetails = new ArModInvoicePaymentScheduleDetails();

					StringBuffer encodedResult = new StringBuffer();
					// Start Separator
					encodedResult.append(separator);
					// ctr++;
					System.out.println(ctr);
					// ctr
					encodedResult.append(ctr);
					encodedResult.append(separator);

					System.out
							.println("arInvoicePaymentSchedule.getIpsCode(): " + arInvoicePaymentSchedule.getIpsCode());
					// IpsCode
					encodedResult.append(arInvoicePaymentSchedule.getIpsCode());
					encodedResult.append(separator);

					System.out.println("getInvReferenceNumber(): "
							+ arInvoicePaymentSchedule.getArInvoice().getInvReferenceNumber());
					// refNumber
					encodedResult.append(arInvoicePaymentSchedule.getArInvoice().getInvReferenceNumber());
					encodedResult.append(separator);

					System.out.println(
							"getInvReferenceNumber(): " + arInvoicePaymentSchedule.getArInvoice().getInvNumber());
					// getInvNumber
					encodedResult.append(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
					encodedResult.append(separator);

					System.out.println(ctr + " CHECKPOINT D: " + encodedResult.toString());
					results[ctr++] = encodedResult.toString();

				}

			}
			System.out.println(results);
			return results;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method
	 */
	public String getCheckInsufficientStocksReceipt(String invoiceDateFrom, String invoiceDateTo, String location,
			String user, String invAdjAccount, String transactionType, String AD_BRNCH, Integer AD_CMPNY) {

		LocalAdBranchItemLocationHome invStockOnHandHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalArReceiptHome arReceiptHome = null;
		// LocalArInvoice arInvoice = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalInvAdjustmentHome invAdjustmentHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		// Initialize EJB Home

		try {
			adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
					LocalAdBranchHome.class);
			invStockOnHandHome = (LocalAdBranchItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			arInvoiceHome = (LocalArInvoiceHome) EJBHomeFactory.lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME,
					LocalArInvoiceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			invAdjustmentHome = (LocalInvAdjustmentHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			arReceiptHome = (LocalArReceiptHome) EJBHomeFactory.lookUpLocalHome(LocalArReceiptHome.JNDI_NAME,
					LocalArReceiptHome.class);
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		int count = 0;
		double insufficientFix = 0;
		LocalAdCompany adCompany = null;

		int branch = 0;
		try {
			System.out.println("Check A: " + AD_BRNCH);
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(AD_BRNCH, AD_CMPNY);
			System.out.println("Check B");
			branch = adBranch.getBrCode();
			System.out.println("AD_BRNCH: " + AD_BRNCH);
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}

		Debug.print("InvItemSyncControllerBean getCheckInsufficientStocks");

		String[] insufficentItemsToFix = null;
		String results = null;
		// arInvoice = arInvoiceHome.findByPrimaryKey(details.getInvCode());
		try {

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date dateFrom = null;
			try {
				dateFrom = sdf.parse(invoiceDateFrom);
				System.out.println("dateFrom = " + sdf.format(dateFrom));
			} catch (Exception e) {
				e.printStackTrace();
			}

			Date dateTo = null;
			try {
				dateTo = sdf.parse(invoiceDateTo);
				System.out.println("dateTransact = " + sdf.format(dateTo));
			} catch (Exception e) {
				e.printStackTrace();
			}

			System.out.println("transactionType: " + transactionType);

			Collection arReceipts = arReceiptHome.findUnpostedRctByRctDateRange(dateFrom, dateTo, AD_CMPNY);

			Iterator i = arReceipts.iterator();
			while (i.hasNext()) {
				LocalArReceipt arReceipt = (LocalArReceipt) i.next();

				System.out
						.println("*************************CHECKING INSUFFICIENT STOCKS******************************");

				boolean hasInsufficientItems = false;
				String insufficientItems = "";

				Collection arReceiptLineItems = arReceipt.getArInvoiceLineItems();

				Iterator p = arReceiptLineItems.iterator();
				int a = 0;
				ArrayList insufficient = new ArrayList();
				ArrayList insufficientQty = new ArrayList();
				ArrayList insufficientUOM = new ArrayList();
				ArrayList insufficientUnitCost = new ArrayList();
				double totalAmount = 0;
				HashMap cstMap = new HashMap();
				while (p.hasNext()) {

					LocalArInvoiceLineItem arReceiptLineItem = (LocalArInvoiceLineItem) p.next();

					if (arReceipt.getRctType().equals("MISC")) {
						if (arReceiptLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arReceiptLineItem
								.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

							Collection invBillOfMaterials = arReceiptLineItem.getInvItemLocation().getInvItem()
									.getInvBillOfMaterials();

							Iterator j = invBillOfMaterials.iterator();

							while (j.hasNext()) {

								LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

								LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(
										invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

								double ILI_QTY = this.convertByUomFromAndItemAndQuantity(
										arReceiptLineItem.getInvUnitOfMeasure(),
										arReceiptLineItem.getInvItemLocation().getInvItem(),
										Math.abs(arReceiptLineItem.getIliQuantity()), AD_CMPNY);
								Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L01");

								LocalInvItem bomItm = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(),
										AD_CMPNY);

								double NEEDED_QTY = this.convertByUomFromAndItemAndQuantity(
										invBillOfMaterial.getInvUnitOfMeasure(), bomItm,
										invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY);

								double CURR_QTY = 0;

								boolean isIlFound = false;

								if (cstMap.containsKey(invItemLocation.getIlCode().toString())) {

									isIlFound = true;
									CURR_QTY = ((Double) cstMap.get(invItemLocation.getIlCode().toString()))
											.doubleValue();

								} else {

									try {

										LocalInvCosting invBomCosting = invCostingHome
												.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIlCode(
														arReceipt.getRctDate(), invItemLocation.getIlCode(), branch,
														AD_CMPNY);

										if (invBomCosting != null) {

											CURR_QTY = this.convertByUomAndQuantity(
													invBomCosting.getInvItemLocation().getInvItem()
															.getInvUnitOfMeasure(),
													invBomCosting.getInvItemLocation().getInvItem(),
													invBomCosting.getCstRemainingQuantity(), AD_CMPNY);
											// System.out.println("invBomCosting: " + invBomCosting.getCstCode());
											Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04");
										}

										Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L02");

									} catch (FinderException ex) {

									}

								}

								if (CURR_QTY == 0 || CURR_QTY < (NEEDED_QTY * ILI_QTY)) {

									hasInsufficientItems = true;

									insufficientItems += arReceiptLineItem.getInvItemLocation().getInvItem().getIiName()
											+ "-" + invBillOfMaterial.getBomIiName() + ", ";

									a++;
									System.out.println("Doc Number: " + arReceipt.getRctNumber());
									System.out.println(
											"Item: " + arReceiptLineItem.getInvItemLocation().getInvItem().getIiName());
									System.out.println("Adjust Qty: " + arReceiptLineItem.getIliQuantity());
									double remQty = 0;
									double unitCost = 0;

									remQty = CURR_QTY;

									System.out.println("Remaining Qty: " + remQty);

									insufficientFix = arReceiptLineItem.getIliQuantity() - remQty;
									insufficient.add(arReceiptLineItem.getInvItemLocation().getInvItem().getIiName());
									insufficientQty.add(insufficientFix);
									insufficientUOM.add(arReceiptLineItem.getInvItemLocation().getInvItem()
											.getInvUnitOfMeasure().getUomName());
									insufficientUnitCost.add(arReceiptLineItem.getIliUnitPrice());
									totalAmount = totalAmount + (arReceiptLineItem.getIliQuantity()
											* arReceiptLineItem.getIliUnitPrice());
									System.out.println("insufficientFix: " + insufficientFix);

								}

								CURR_QTY -= (NEEDED_QTY * ILI_QTY);

								if (!isIlFound) {
									cstMap.remove(invItemLocation.getIlCode().toString());
								}

								cstMap.put(invItemLocation.getIlCode().toString(), new Double(CURR_QTY));

								Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L03");

							}
						} else {

							LocalInvCosting invCosting = null;

							double ILI_QTY = this.convertByUomAndQuantity(arReceiptLineItem.getInvUnitOfMeasure(),
									arReceiptLineItem.getInvItemLocation().getInvItem(),
									Math.abs(arReceiptLineItem.getIliQuantity()), AD_CMPNY);

							try {

								/*
								 * invCosting = invCostingHome.
								 * getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(
								 * arInvoice.getInvDate(),
								 * arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(),
								 * arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(),
								 * AD_BRNCH, AD_CMPNY);
								 */

								invCosting = invCostingHome
										.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
												arReceipt.getRctDate(),
												arReceiptLineItem.getInvItemLocation().getInvItem().getIiName(),
												arReceiptLineItem.getInvItemLocation().getInvLocation().getLocName(),
												branch, AD_CMPNY);
								Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N04");
							} catch (FinderException ex) {

							}

							double LOWEST_QTY = this.convertByUomAndQuantity(arReceiptLineItem.getInvUnitOfMeasure(),
									arReceiptLineItem.getInvItemLocation().getInvItem(), 1, AD_CMPNY);

							Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N05");

							if (invCosting == null || invCosting.getCstRemainingQuantity() == 0
									|| invCosting.getCstRemainingQuantity() - ILI_QTY <= -LOWEST_QTY) {
								a++;
								hasInsufficientItems = true;
								System.out.println("Doc Number: " + arReceipt.getRctNumber());
								System.out.println(
										"Item: " + arReceiptLineItem.getInvItemLocation().getInvItem().getIiName());
								System.out.println("Adjust Qty: " + arReceiptLineItem.getIliQuantity());
								double remQty = 0;
								double unitCost = 0;
								try {
									remQty = invCosting.getCstRemainingQuantity();
								} catch (Exception e) {
									remQty = 0;
								}
								System.out.println("Remaining Qty: " + remQty);
								insufficientItems += arReceiptLineItem.getInvItemLocation().getInvItem().getIiName()
										+ " ";
								insufficientFix = arReceiptLineItem.getIliQuantity() - remQty;
								insufficient.add(arReceiptLineItem.getInvItemLocation().getInvItem().getIiName());
								insufficientQty.add(insufficientFix);
								insufficientUOM.add(arReceiptLineItem.getInvItemLocation().getInvItem()
										.getInvUnitOfMeasure().getUomName());
								insufficientUnitCost.add(arReceiptLineItem.getIliUnitPrice());
								totalAmount = totalAmount
										+ (arReceiptLineItem.getIliQuantity() * arReceiptLineItem.getIliUnitPrice());
								System.out.println("insufficientFix: " + insufficientFix);

							}
						}
					}

				}

				String[] insuItem;
				insuItem = new String[a];

				Double[] insuQty;
				insuQty = new Double[a];

				String[] insuUOM;
				insuUOM = new String[a];

				Double[] insuCost;
				insuCost = new Double[a];

				if (hasInsufficientItems) {
					results = "hasInsufficientItems";
					System.out.println("insufficientItems: " + insufficientItems);
					System.out.println(insufficient.size());

					Iterator mi = insufficient.iterator();
					int b = 0;
					while (mi.hasNext()) {

						String miscStr = (String) mi.next();
						System.out.println("Items: " + miscStr);
						insuItem[b] = miscStr;
						System.out.println("insuTest[b]: " + insuItem[b]);
						b++;
					}

					Iterator mi2 = insufficientQty.iterator();
					b = 0;
					while (mi2.hasNext()) {

						Double miscStr = (Double) mi2.next();
						insuQty[b] = miscStr;
						System.out.println("insuTest[b]: " + insuQty[b]);
						b++;
					}

					Iterator mi3 = insufficientUOM.iterator();
					b = 0;
					while (mi3.hasNext()) {

						String miscStr = (String) mi3.next();
						insuUOM[b] = miscStr;
						System.out.println("insuTest[b]: " + insuUOM[b]);
						b++;
					}

					Iterator mi4 = insufficientUnitCost.iterator();
					b = 0;
					while (mi4.hasNext()) {

						Double miscStr = (Double) mi4.next();
						insuCost[b] = miscStr;
						System.out.println("insuCost[b]: " + insuCost[b]);
						b++;
					}

					String[] adjEncode = new String[a];
					int ctr = 0;
					for (int x = 0; x < a; x++) {
						adjEncode[ctr++] = rctRowEncode(arReceipt.getRctDate(), user, location, invAdjAccount, insuItem,
								insuQty, insuUOM, insuCost, a, arReceipt.getRctNumber(),
								arReceipt.getArCustomer().getCstCustomerCode(), arReceipt.getRctPaymentMethod(),
								totalAmount);
					}
					int Result = 0;
					try {
						Result = setArMiscReceiptAllNewAndVoid(adjEncode, null, AD_BRNCH, AD_CMPNY, null);
						count++;
					} catch (Exception ex) {
						ex.printStackTrace();
						throw new EJBException(ex.getMessage());
					}

					System.out.println("Result: " + Result);
					results = Integer.toString(Result);

					// throw new GlobalRecordInvalidException(insufficientItems.substring(0,
					// insufficientItems.lastIndexOf(",")));
				}

			}

		} catch (Exception ex) {

			ex.printStackTrace();
			throw new EJBException(ex.getMessage());

		}

		System.out.println("*************************INSUFFICIENT STOCKS Fixed******************************");
		System.out.println("Number of Insufficiency: " + count);
		return results;
	}

	private String rctRowEncode(Date invAdjDate, String User, String location, String adjustmentAccount,
			String[] insuItem, Double[] insuQty, String[] insuUOM, Double[] insuCost, int arrayCount,
			String documentNumber, String customerCode, String paymentType, Double totalAmount) {

		Debug.print("InvItemSyncControllerBean rctRowEncode");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer encodedResult = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_OUTPUT);

		// Start separator heading
		encodedResult.append(separator);
		// MessageBox.Show(mscRct.customerCode);
		// CUSTOMER CODE
		encodedResult.append(customerCode);
		encodedResult.append(separator);

		// PAYMENT TYPE
		encodedResult.append("CASH");
		encodedResult.append(separator);

		// OR NUMBER
		encodedResult.append("");
		encodedResult.append(separator);

		// RECEIPT SOURCE
		encodedResult.append("NONE");
		encodedResult.append(separator);

		// CURRENCY
		encodedResult.append("PHP");
		encodedResult.append(separator);

		// CURRENCY RATE
		encodedResult.append("1");
		encodedResult.append(separator);

		// CASH AMOUNT
		encodedResult.append(totalAmount);
		encodedResult.append(separator);

		// CHECK AMOUNT
		encodedResult.append(totalAmount);
		encodedResult.append(separator);

		// CARD AMT
		encodedResult.append(totalAmount);
		encodedResult.append(separator);

		// TOTAL AMT
		encodedResult.append(totalAmount);
		encodedResult.append(separator);

		// DISCOUNT
		encodedResult.append("0");
		encodedResult.append(separator);

		// VOID
		encodedResult.append("NONE");
		encodedResult.append(separator);

		// VOID REASON
		encodedResult.append("NONE");
		encodedResult.append(separator);

		// VOID AMOUNT

		encodedResult.append("0");
		encodedResult.append(separator);

		// DATE PURCHASED
		encodedResult.append(sdf.format(invAdjDate));
		encodedResult.append(separator);

		// LOCATION
		encodedResult.append(location);
		encodedResult.append(separator);

		// PAYMENT METHOD
		if (false) {
			encodedResult.append("CASH");
			encodedResult.append(separator);
		} else {
			encodedResult.append(separator);
		}

		// SC
		if (false) {
			// encodedResult.append(receipts["SC_AMOUNT"].ToString());
			encodedResult.append(separator);
		} else {
			encodedResult.append(separator);
		}

		// DC
		if (false) {
			// encodedResult.append(receipts["DC_AMOUNT"].ToString());
			encodedResult.append(separator);
		} else {
			encodedResult.append(separator);
		}

		// ON ACCOUNT
		encodedResult.append("0");
		encodedResult.append(separator);

		// End Separator heading
		encodedResult.append(separator);

		String lineSeparator = "~";

		// begin lineSeparator
		encodedResult.append(lineSeparator);

		for (int x = 0; x < arrayCount; x++) {

			// Start separator
			encodedResult.append(separator);

			// LINE_NUM
			encodedResult.append("1");
			encodedResult.append(separator);

			// ITEM CODE
			// MessageBox.Show(lines.itemCode.ToString());
			encodedResult.append(insuItem[x]);
			encodedResult.append(separator);

			// QUANTITY
			encodedResult.append(insuQty[x]);
			encodedResult.append(separator);

			// PRICE
			encodedResult.append(insuCost[x]);
			encodedResult.append(separator);

			// TOTAL
			encodedResult.append(insuQty[x] * insuCost[x]);
			encodedResult.append(separator);

			// UOM
			encodedResult.append(insuUOM[x]);
			encodedResult.append(separator);

			// MessageBox.Show("Tryr" + drReceiptLines["SUB_LOCATION"].ToString());
			// SUB LOCATION

			String subLocation = location;

			encodedResult.append(subLocation);
			encodedResult.append(separator);

			encodedResult.append(lineSeparator);

		}

		return encodedResult.toString();

	}

	private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem,
			double ADJST_QTY, Integer AD_CMPNY) {

		Debug.print("InvBranchStockTransferOutEntryControllerBean convertByUomFromAndUomToAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome
					.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
			LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
					.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(),
							AD_CMPNY);

			return EJBCommon.roundIt(
					ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor()
							/ invUnitOfMeasureConversion.getUmcConversionFactor(),
					adPreference.getPrfInvQuantityPrecisionUnit());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, Date CONVERSION_DATE,
			double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("ArMiscReceiptEntryControllerBean convertForeignToFunctionalCurrency");

		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
					LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0) {

			AMOUNT = AMOUNT * CONVERSION_RATE;

		} else if (CONVERSION_DATE != null) {

			try {

				// Get functional currency rate

				LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;

				if (!FC_NM.equals("USD")) {

					glReceiptFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE,
							CONVERSION_DATE, AD_CMPNY);

					AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();

				}

				// Get set of book functional currency rate if necessary

				if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

					LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = glFunctionalCurrencyRateHome
							.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().getFcCode(), CONVERSION_DATE,
									AD_CMPNY);

					AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();

				}

			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}

		}

		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

	private void executeArRctPost(Integer RCT_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY)
			throws GlobalRecordAlreadyDeletedException, GlobalTransactionAlreadyPostedException,
			GlobalTransactionAlreadyVoidPostedException, GlJREffectiveDateNoPeriodExistException,
			GlJREffectiveDatePeriodClosedException, GlobalJournalNotBalanceException,
			AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {

		Debug.print("ArMiscReceiptEntryControllerBean executeArRctPost");

		InvItemEntryControllerHome homeII = null;
		InvItemEntryController ejbII = null;

		LocalArReceiptHome arReceiptHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGlJournalHome glJournalHome = null;
		LocalGlJournalBatchHome glJournalBatchHome = null;
		LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalGlJournalSourceHome glJournalSourceHome = null;
		LocalGlJournalCategoryHome glJournalCategoryHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalInvItemHome invItemHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
		LocalGlForexLedgerHome glForexLedgerHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

		LocalArReceipt arReceipt = null;

		// Initialize EJB Home

		try {

			homeII = (InvItemEntryControllerHome) com.util.EJBHomeFactory.lookUpHome("ejb/InvItemEntryControllerEJB",
					InvItemEntryControllerHome.class);

			arReceiptHome = (LocalArReceiptHome) EJBHomeFactory.lookUpLocalHome(LocalArReceiptHome.JNDI_NAME,
					LocalArReceiptHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory.lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME,
					LocalGlSetOfBookHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome) EJBHomeFactory.lookUpLocalHome(
					LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			glJournalHome = (LocalGlJournalHome) EJBHomeFactory.lookUpLocalHome(LocalGlJournalHome.JNDI_NAME,
					LocalGlJournalHome.class);
			glJournalBatchHome = (LocalGlJournalBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
			glSuspenseAccountHome = (LocalGlSuspenseAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
			glJournalLineHome = (LocalGlJournalLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			glJournalSourceHome = (LocalGlJournalSourceHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
			glJournalCategoryHome = (LocalGlJournalCategoryHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory
					.lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adBankAccountHome = (LocalAdBankAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
			glForexLedgerHome = (LocalGlForexLedgerHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
					LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			ejbII = homeII.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			// validate if receipt is already deleted

			try {

				arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if receipt is already posted

			if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() == EJBCommon.TRUE) {

				throw new GlobalTransactionAlreadyPostedException();

				// validate if receipt void is already posted

			} else if (arReceipt.getRctVoid() == EJBCommon.TRUE && arReceipt.getRctVoidPosted() == EJBCommon.TRUE) {

				throw new GlobalTransactionAlreadyVoidPostedException();

			}

			/*
			if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctType().equals("MISC")
					&& !arReceipt.getArInvoiceLineItems().isEmpty()) {

				// regenerate inventory dr

				this.regenerateInventoryDr(arReceipt, AD_BRNCH, AD_CMPNY);

			}
			*/
			// post receipt

			if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() == EJBCommon.FALSE) {

				if (arReceipt.getRctType().equals("MISC") && !arReceipt.getArInvoiceLineItems().isEmpty()) {

					Iterator c = arReceipt.getArInvoiceLineItems().iterator();

					while (c.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

						String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
						String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

						double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
								arInvoiceLineItem.getInvUnitOfMeasure(),
								arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
								AD_CMPNY);

						LocalInvCosting invCosting = null;

						try {

							System.out.println("Sync RCT date is : " + arReceipt.getRctDate());

							 invCosting = invCostingHome
									.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arReceipt.getRctDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {
							System.out.println("Sync RCT date not found");
						}

						double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

						if (invCosting == null) {

							this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), QTY_SLD, COST * QTY_SLD, -QTY_SLD,
									-COST * QTY_SLD, 0d, null, AD_BRNCH, AD_CMPNY);

						} else {

							if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {
			
								double avgCost =  ejbII.getInvIiUnitCostByIiNameAndUomName(II_NM, arInvoiceLineItem.getInvUnitOfMeasure().getUomName(), arReceipt.getRctDate(), AD_CMPNY);

								this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), QTY_SLD, avgCost * QTY_SLD,
										invCosting.getCstRemainingQuantity() - QTY_SLD,
										invCosting.getCstRemainingValue() - (avgCost * QTY_SLD), 0d, null, AD_BRNCH,
										AD_CMPNY);

							} else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

								double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST
										: this.getInvFifoCost(invCosting.getCstDate(),
												invCosting.getInvItemLocation().getIlCode(), QTY_SLD,
												arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

								this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), QTY_SLD, fifoCost * QTY_SLD,
										invCosting.getCstRemainingQuantity() - QTY_SLD,
										invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD), 0d, null, AD_BRNCH,
										AD_CMPNY);

							} else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod()
									.equals("Standard")) {

								double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

								this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(), QTY_SLD,
										standardCost * QTY_SLD, invCosting.getCstRemainingQuantity() - QTY_SLD,
										invCosting.getCstRemainingValue() - (standardCost * QTY_SLD), 0d, null,
										AD_BRNCH, AD_CMPNY);
							}

						}

					}

				}

				// increase bank balance CASH
				if (arReceipt.getRctAmountCash() > 0) {

					LocalAdBankAccount adBankAccount = adBankAccountHome
							.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

					try {

						// find bankaccount balance before or equal receipt date

						Collection adBankAccountBalances = adBankAccountBalanceHome
								.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
										arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

						if (!adBankAccountBalances.isEmpty()) {

							// get last check

							ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) adBankAccountBalanceList
									.get(adBankAccountBalanceList.size() - 1);

							if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

								// create new balance

								LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
										arReceipt.getRctDate(),
										adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash(), "BOOK",
										AD_CMPNY);

								// adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
								adNewBankAccountBalance.setAdBankAccount(adBankAccount);
							} else { // equals to check date

								adBankAccountBalance.setBabBalance(
										adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash());

							}

						} else {

							// create new balance
							System.out.println("arReceipt.getRctAmountCash()=" + arReceipt.getRctAmountCash());
							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome
									.create(arReceipt.getRctDate(), (arReceipt.getRctAmountCash()), "BOOK", AD_CMPNY);

							// adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);

						}

						// propagate to subsequent balances if necessary

						adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(
								arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

						Iterator i = adBankAccountBalances.iterator();

						while (i.hasNext()) {

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

							adBankAccountBalance
									.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash());

						}

					} catch (Exception ex) {

						ex.printStackTrace();

					}
				}

				// increase bank balance CARD 1
				if (arReceipt.getRctAmountCard1() > 0) {

					LocalAdBankAccount adBankAccount = adBankAccountHome
							.findByPrimaryKey(arReceipt.getAdBankAccountCard1().getBaCode());

					try {

						// find bankaccount balance before or equal receipt date

						Collection adBankAccountBalances = adBankAccountBalanceHome
								.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
										arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

						if (!adBankAccountBalances.isEmpty()) {

							// get last check

							ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) adBankAccountBalanceList
									.get(adBankAccountBalanceList.size() - 1);

							if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

								// create new balance

								LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
										arReceipt.getRctDate(),
										adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1(), "BOOK",
										AD_CMPNY);

								// adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
								adNewBankAccountBalance.setAdBankAccount(adBankAccount);
							} else { // equals to check date

								adBankAccountBalance.setBabBalance(
										adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1());

							}

						} else {

							// create new balance
							System.out.println("arReceipt.getRctAmountCard1()=" + arReceipt.getRctAmountCard1());
							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome
									.create(arReceipt.getRctDate(), (arReceipt.getRctAmountCard1()), "BOOK", AD_CMPNY);

							// adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);

						}

						// propagate to subsequent balances if necessary

						adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(
								arReceipt.getRctDate(), arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK",
								AD_CMPNY);

						Iterator i = adBankAccountBalances.iterator();

						while (i.hasNext()) {

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

							adBankAccountBalance.setBabBalance(
									adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1());

						}

					} catch (Exception ex) {

						ex.printStackTrace();

					}

				}

//increase bank balance CARD 2
				if (arReceipt.getRctAmountCard2() > 0) {
					LocalAdBankAccount adBankAccount = adBankAccountHome
							.findByPrimaryKey(arReceipt.getAdBankAccountCard2().getBaCode());

					try {

						// find bankaccount balance before or equal receipt date

						Collection adBankAccountBalances = adBankAccountBalanceHome
								.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
										arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

						if (!adBankAccountBalances.isEmpty()) {

							// get last check

							ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) adBankAccountBalanceList
									.get(adBankAccountBalanceList.size() - 1);

							if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

								// create new balance

								LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
										arReceipt.getRctDate(),
										adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2(), "BOOK",
										AD_CMPNY);

								// adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
								adNewBankAccountBalance.setAdBankAccount(adBankAccount);
							} else { // equals to check date

								adBankAccountBalance.setBabBalance(
										adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2());

							}

						} else {

							// create new balance
							System.out.println("arReceipt.getRctAmountCard2()=" + arReceipt.getRctAmountCard2());
							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome
									.create(arReceipt.getRctDate(), (arReceipt.getRctAmountCard2()), "BOOK", AD_CMPNY);

							// adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);

						}

						// propagate to subsequent balances if necessary

						adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(
								arReceipt.getRctDate(), arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK",
								AD_CMPNY);

						Iterator i = adBankAccountBalances.iterator();

						while (i.hasNext()) {

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

							adBankAccountBalance.setBabBalance(
									adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2());

						}

					} catch (Exception ex) {

						ex.printStackTrace();

					}
				}

				// increase bank balance CARD 3
				if (arReceipt.getRctAmountCard3() > 0) {
					LocalAdBankAccount adBankAccount = adBankAccountHome
							.findByPrimaryKey(arReceipt.getAdBankAccountCard3().getBaCode());

					try {

						// find bankaccount balance before or equal receipt date

						Collection adBankAccountBalances = adBankAccountBalanceHome
								.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
										arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

						if (!adBankAccountBalances.isEmpty()) {

							// get last check

							ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) adBankAccountBalanceList
									.get(adBankAccountBalanceList.size() - 1);

							if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

								// create new balance

								LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
										arReceipt.getRctDate(),
										adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3(), "BOOK",
										AD_CMPNY);

								// adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
								adNewBankAccountBalance.setAdBankAccount(adBankAccount);
							} else { // equals to check date

								adBankAccountBalance.setBabBalance(
										adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3());

							}

						} else {

							// create new balance
							System.out.println("arReceipt.getRctAmountCard3()=" + arReceipt.getRctAmountCard3());
							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome
									.create(arReceipt.getRctDate(), (arReceipt.getRctAmountCard3()), "BOOK", AD_CMPNY);

							// adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);

						}

						// propagate to subsequent balances if necessary

						adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(
								arReceipt.getRctDate(), arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK",
								AD_CMPNY);

						Iterator i = adBankAccountBalances.iterator();

						while (i.hasNext()) {

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

							adBankAccountBalance.setBabBalance(
									adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3());

						}

					} catch (Exception ex) {

						ex.printStackTrace();

					}
				}
				// increase bank balance CHEQUE
				if (arReceipt.getRctAmountCheque() > 0) {
					LocalAdBankAccount adBankAccount = adBankAccountHome
							.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

					try {

						// find bankaccount balance before or equal receipt date

						Collection adBankAccountBalances = adBankAccountBalanceHome
								.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(),
										arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

						if (!adBankAccountBalances.isEmpty()) {

							// get last check

							ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) adBankAccountBalanceList
									.get(adBankAccountBalanceList.size() - 1);

							if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

								// create new balance

								LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
										arReceipt.getRctDate(),
										adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque(), "BOOK",
										AD_CMPNY);

								// adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
								adNewBankAccountBalance.setAdBankAccount(adBankAccount);
							} else { // equals to check date

								adBankAccountBalance.setBabBalance(
										adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque());

							}

						} else {

							// create new balance
							System.out.println("arReceipt.getRctAmountCheque()=" + arReceipt.getRctAmountCheque());
							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome
									.create(arReceipt.getRctDate(), (arReceipt.getRctAmountCheque()), "BOOK", AD_CMPNY);

							// adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);

						}

						// propagate to subsequent balances if necessary

						adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(
								arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

						Iterator i = adBankAccountBalances.iterator();

						while (i.hasNext()) {

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

							adBankAccountBalance.setBabBalance(
									adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque());

						}

					} catch (Exception ex) {

						ex.printStackTrace();

					}
				}

				// set receipt post status

				arReceipt.setRctPosted(EJBCommon.TRUE);
				arReceipt.setRctPostedBy(USR_NM);
				arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

			}

			// post to gl if necessary

			if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
				System.out.println(arReceipt.getRctReferenceNumber() + "  Reference Number");
				// validate if date has no period and period is closed

				LocalGlSetOfBook glJournalSetOfBook = null;

				try {

					glJournalSetOfBook = glSetOfBookHome.findByDate(arReceipt.getRctDate(), AD_CMPNY);

				} catch (FinderException ex) {

					throw new GlJREffectiveDateNoPeriodExistException();

				}

				LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome
						.findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								arReceipt.getRctDate(), AD_CMPNY);

				if (glAccountingCalendarValue.getAcvStatus() == 'N' || glAccountingCalendarValue.getAcvStatus() == 'C'
						|| glAccountingCalendarValue.getAcvStatus() == 'P') {

					throw new GlJREffectiveDatePeriodClosedException();

				}

				// check if invoice is balance if not check suspense posting

				LocalGlJournalLine glOffsetJournalLine = null;

				Collection arDistributionRecords = null;

				if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

					arDistributionRecords = arDistributionRecordHome
							.findImportableDrByDrReversedAndRctCode(EJBCommon.FALSE, arReceipt.getRctCode(), AD_CMPNY);

				} else {

					arDistributionRecords = arDistributionRecordHome
							.findImportableDrByDrReversedAndRctCode(EJBCommon.TRUE, arReceipt.getRctCode(), AD_CMPNY);

				}

				Iterator j = arDistributionRecords.iterator();

				double TOTAL_DEBIT = 0d;
				double TOTAL_CREDIT = 0d;

				while (j.hasNext()) {

					LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) j.next();

					double DR_AMNT = 0d;

					if (arDistributionRecord.getArAppliedInvoice() != null) {

						LocalArInvoice arInvoice = arDistributionRecord.getArAppliedInvoice()
								.getArInvoicePaymentSchedule().getArInvoice();

						DR_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);

					} else {

						DR_AMNT = this.convertForeignToFunctionalCurrency(
								arReceipt.getGlFunctionalCurrency().getFcCode(),
								arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);

					}

					if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
						System.out.println("TOTAL DEBIT to add : " + DR_AMNT);
						TOTAL_DEBIT += DR_AMNT;

					} else {
						System.out.println("TOTAL Credit to add : " + DR_AMNT);
						TOTAL_CREDIT += DR_AMNT;

					}

				}

				TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
				TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

				if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE && TOTAL_DEBIT != TOTAL_CREDIT) {

					LocalGlSuspenseAccount glSuspenseAccount = null;

					try {

						glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES",
								"SALES RECEIPTS", AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalJournalNotBalanceException();

					}

					if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

						glOffsetJournalLine = glJournalLineHome.create((short) (arDistributionRecords.size() + 1),
								EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

					} else {

						glOffsetJournalLine = glJournalLineHome.create((short) (arDistributionRecords.size() + 1),
								EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

					}

					LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
					// glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
					glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);

				} else if (TOTAL_DEBIT > 9999999999d || TOTAL_CREDIT > 9999999999d
						|| (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE
								&& TOTAL_DEBIT != TOTAL_CREDIT)) {
					throw new GlobalJournalNotBalanceException();

				}

				// create journal batch if necessary

				LocalGlJournalBatch glJournalBatch = null;
				java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

				try {

					if (adPreference.getPrfEnableArReceiptBatch() == EJBCommon.TRUE) {

						glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT "
								+ formatter.format(new Date()) + " " + arReceipt.getArReceiptBatch().getRbName(),
								AD_BRNCH, AD_CMPNY);

					} else {

						glJournalBatch = glJournalBatchHome.findByJbName(
								"JOURNAL IMPORT " + formatter.format(new Date()) + " SALES RECEIPTS", AD_BRNCH,
								AD_CMPNY);

					}

				} catch (FinderException ex) {
				}

				if (adPreference.getPrfEnableGlJournalBatch() == EJBCommon.TRUE && glJournalBatch == null) {

					if (adPreference.getPrfEnableArReceiptBatch() == EJBCommon.TRUE) {

						glJournalBatch = glJournalBatchHome.create(
								"JOURNAL IMPORT " + formatter.format(new Date()) + " "
										+ arReceipt.getArReceiptBatch().getRbName(),
								"JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM,
								AD_BRNCH, AD_CMPNY);

					} else {

						glJournalBatch = glJournalBatchHome.create(
								"JOURNAL IMPORT " + formatter.format(new Date()) + " SALES RECEIPTS", "JOURNAL IMPORT",
								"CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

					}

				}

				// create journal entry
				String customerName = arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName()
						: arReceipt.getRctCustomerName();

				LocalGlJournal glJournal = glJournalHome.create(arReceipt.getRctReferenceNumber(),
						arReceipt.getRctDescription(), arReceipt.getRctDate(), 0.0d, null, arReceipt.getRctNumber(),
						null, 1d, "N/A", null, 'N', EJBCommon.TRUE, EJBCommon.FALSE, USR_NM, new Date(), USR_NM,
						new Date(), null, null, USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						arReceipt.getArCustomer().getCstTin(), customerName, EJBCommon.FALSE, null, AD_BRNCH, AD_CMPNY);

				LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES",
						AD_CMPNY);
				glJournal.setGlJournalSource(glJournalSource);

				LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
						.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
				glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

				LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("SALES RECEIPTS",
						AD_CMPNY);
				glJournal.setGlJournalCategory(glJournalCategory);

				if (glJournalBatch != null) {

					glJournal.setGlJournalBatch(glJournalBatch);

				}

				// create journal lines

				j = arDistributionRecords.iterator();

				while (j.hasNext()) {

					LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) j.next();

					double DR_AMNT = 0d;

					LocalArInvoice arInvoice = null;

					if (arDistributionRecord.getArAppliedInvoice() != null) {

						arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule()
								.getArInvoice();

						DR_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);

					} else {

						DR_AMNT = this.convertForeignToFunctionalCurrency(
								arReceipt.getGlFunctionalCurrency().getFcCode(),
								arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(), arDistributionRecord.getDrAmount(), AD_CMPNY);

					}

					LocalGlJournalLine glJournalLine = glJournalLineHome.create(arDistributionRecord.getDrLine(),
							arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

					glJournalLine.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());

					glJournalLine.setGlJournal(glJournal);

					arDistributionRecord.setDrImported(EJBCommon.TRUE);

					// for FOREX revaluation

					int FC_CODE = arDistributionRecord.getArAppliedInvoice() != null
							? arInvoice.getGlFunctionalCurrency().getFcCode().intValue()
							: arReceipt.getGlFunctionalCurrency().getFcCode().intValue();

					if ((FC_CODE != adCompany.getGlFunctionalCurrency().getFcCode().intValue())
							&& glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null
							&& (FC_CODE == glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode()
									.intValue())) {

						double CONVERSION_RATE = arDistributionRecord.getArAppliedInvoice() != null
								? arInvoice.getInvConversionRate()
								: arReceipt.getRctConversionRate();

						Date DATE = arDistributionRecord.getArAppliedInvoice() != null
								? arInvoice.getInvConversionDate()
								: arReceipt.getRctConversionDate();

						if (DATE != null && (CONVERSION_RATE == 0 || CONVERSION_RATE == 1)) {

							CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
									glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
									glJournal.getJrConversionDate(), AD_CMPNY);

						} else if (CONVERSION_RATE == 0) {

							CONVERSION_RATE = 1;

						}

						Collection glForexLedgers = null;

						DATE = arDistributionRecord.getArAppliedInvoice() != null ? arInvoice.getInvDate()
								: arReceipt.getRctDate();

						try {

							glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(DATE,
									glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

						} catch (FinderException ex) {

						}

						LocalGlForexLedger glForexLedger = (glForexLedgers.isEmpty() || glForexLedgers == null) ? null
								: (LocalGlForexLedger) glForexLedgers.iterator().next();

						int FRL_LN = (glForexLedger != null && glForexLedger.getFrlDate().compareTo(DATE) == 0)
								? glForexLedger.getFrlLine().intValue() + 1
								: 1;

						// compute balance
						double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
						double FRL_AMNT = arDistributionRecord.getDrAmount();

						if (glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
							FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (-1 * FRL_AMNT));
						else
							FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (-1 * FRL_AMNT) : FRL_AMNT);

						COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

						glForexLedger = glForexLedgerHome.create(DATE, new Integer(FRL_LN), "OR", FRL_AMNT,
								CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

						// glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
						glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());

						// propagate balances
						try {

							glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
									glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
									glForexLedger.getFrlAdCompany());

						} catch (FinderException ex) {

						}

						Iterator itrFrl = glForexLedgers.iterator();

						while (itrFrl.hasNext()) {

							glForexLedger = (LocalGlForexLedger) itrFrl.next();
							FRL_AMNT = arDistributionRecord.getDrAmount();

							if (glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
								FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (-1 * FRL_AMNT));
							else
								FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (-1 * FRL_AMNT) : FRL_AMNT);

							glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

						}

					}

				}

				if (glOffsetJournalLine != null) {

					// glJournal.addGlJournalLine(glOffsetJournalLine);
					glOffsetJournalLine.setGlJournal(glJournal);
				}

				// post journal to gl

				Collection glJournalLines = glJournal.getGlJournalLines();

				Iterator i = glJournalLines.iterator();

				while (i.hasNext()) {

					LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

					// post current to current acv

					this.postToGl(glAccountingCalendarValue, glJournalLine.getGlChartOfAccount(), true,
							glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

					// post to subsequent acvs (propagate)

					Collection glSubsequentAccountingCalendarValues = glAccountingCalendarValueHome
							.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
									glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
									glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

					Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

					while (acvsIter.hasNext()) {

						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = (LocalGlAccountingCalendarValue) acvsIter
								.next();

						this.postToGl(glSubsequentAccountingCalendarValue, glJournalLine.getGlChartOfAccount(), false,
								glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

					}

					// post to subsequent years if necessary

					Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
							glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

					if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

						adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
						LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome
								.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

						Iterator sobIter = glSubsequentSetOfBooks.iterator();

						while (sobIter.hasNext()) {

							LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook) sobIter.next();

							String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

							// post to subsequent acvs of subsequent set of book(propagate)

							Collection glAccountingCalendarValues = glAccountingCalendarValueHome.findByAcCode(
									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

							Iterator acvIter = glAccountingCalendarValues.iterator();

							while (acvIter.hasNext()) {

								LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = (LocalGlAccountingCalendarValue) acvIter
										.next();

								if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY")
										|| ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

									this.postToGl(glSubsequentAccountingCalendarValue,
											glJournalLine.getGlChartOfAccount(), false, glJournalLine.getJlDebit(),
											glJournalLine.getJlAmount(), AD_CMPNY);

								} else { // revenue & expense

									this.postToGl(glSubsequentAccountingCalendarValue, glRetainedEarningsAccount, false,
											glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

								}

							}

							if (glSubsequentSetOfBook.getSobYearEndClosed() == 0)
								break;

						}

					}

				}

			}

		} catch (GlJREffectiveDateNoPeriodExistException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlJREffectiveDatePeriodClosedException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyDeletedException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidPostedException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

			throw ex;

		} catch (GlobalExpiryDateNotFoundException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void regenerateInventoryDr(LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY)
			throws GlobalInventoryDateException, GlobalBranchAccountNumberInvalidException {

		Debug.print("ArMiscReceiptEntryControllerBean regenerateInventoryDr");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalInvItemHome invItemHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome) EJBHomeFactory
					.lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			// regenerate inventory distribution records

			Collection arDistributionRecords = null;

			if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

				arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.FALSE,
						arReceipt.getRctCode(), AD_CMPNY);

			} else {

				arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.TRUE,
						arReceipt.getRctCode(), AD_CMPNY);

			}

			Iterator i = arDistributionRecords.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				if (arDistributionRecord.getDrClass().equals("COGS")
						|| arDistributionRecord.getDrClass().equals("INVENTORY")) {

					i.remove();
					arDistributionRecord.remove();

				}

			}

			Collection arInvoiceLineItems = arReceipt.getArInvoiceLineItems();

			if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

				i = arInvoiceLineItems.iterator();

				while (i.hasNext()) {

					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
					LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
					if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						if (!invNegTxnCosting.isEmpty())
							throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
					}

					// add cost of sales distribution and inventory

					double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

					try {

						LocalInvCosting invCosting = invCostingHome
								.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
										arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(),
										invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

						if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

							COST = invCosting.getCstRemainingQuantity() <= 0 ? COST
									: Math.abs(
											invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

						if (COST <= 0) {
							COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
						}

						else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

							COST = invCosting.getCstRemainingQuantity() == 0 ? COST
									: Math.abs(this.getInvFifoCost(invCosting.getCstDate(),
											invCosting.getInvItemLocation().getIlCode(),
											arInvoiceLineItem.getIliQuantity(), arInvoiceLineItem.getIliUnitPrice(),
											false, AD_BRNCH, AD_CMPNY));

						else if (invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))

							COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

					} catch (FinderException ex) {
					}

					double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
							arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
							AD_CMPNY);

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try {

						adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
								arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE && arInvoiceLineItem
							.getInvItemLocation().getInvItem().getIiNonInventoriable() == EJBCommon.FALSE) {

						if (adBranchItemLocation != null) {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE, COST * QTY_SLD,
									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt, AD_BRNCH,
									AD_CMPNY);

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
									COST * QTY_SLD, adBranchItemLocation.getBilCoaGlInventoryAccount(), arReceipt,
									AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "COGS", EJBCommon.TRUE, COST * QTY_SLD,
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arReceipt,
									AD_BRNCH, AD_CMPNY);

							this.addArDrIliEntry(arReceipt.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE,
									COST * QTY_SLD, arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(),
									arReceipt, AD_BRNCH, AD_CMPNY);

						}

					}

				}
			}

		} catch (GlobalInventoryDateException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
			throws GlobalConversionDateNotExistException {

		Debug.print("ArMiscReceiptEntryControllerBean getFrRateByFrNameAndFrDate");

		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory.lookUpLocalHome(
					LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

			double CONVERSION_RATE = 1;

			// Get functional currency rate

			if (!FC_NM.equals("USD")) {

				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
						.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(), CONVERSION_DATE, AD_CMPNY);

				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

			}

			// Get set of book functional currency rate if necessary

			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = glFunctionalCurrencyRateHome
						.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().getFcCode(), CONVERSION_DATE,
								AD_CMPNY);

				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

			}

			return CONVERSION_RATE;

		} catch (FinderException ex) {

			// getSessionContext().setRollbackOnly();
			throw new GlobalConversionDateNotExistException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ArMiscReceiptEntryControllerBean getInvGpQuantityPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvQuantityPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault,
			Integer AD_CMPNY) {

		Debug.print("ArInvoiceEntryControllerBean convertCostByUom");

		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome
					.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
			LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
					.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

			if (isFromDefault) {

				return unitCost * invDefaultUomConversion.getUmcConversionFactor()
						/ invUnitOfMeasureConversion.getUmcConversionFactor();

			} else {

				return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor()
						/ invDefaultUomConversion.getUmcConversionFactor();

			}

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void postToBua(LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_ASSMBLY_QTY,
			double CST_ASSMBLY_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, String II_NM, String LOC_NM,
			double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY)
			throws AdPRFCoaGlVarianceAccountNotFoundException {

		Debug.print("ArMiscReceiptEntryControllerBean postToBua");

		LocalInvCostingHome invCostingHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

		// Initialize EJB Home

		try {

			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);
			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);
			int CST_LN_NMBR = 0;

			CST_ASSMBLY_QTY = EJBCommon.roundIt(CST_ASSMBLY_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
			CST_ASSMBLY_CST = EJBCommon.roundIt(CST_ASSMBLY_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
			CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
			CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

			if ((CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 0) || (CST_ASSMBLY_QTY < 0
					&& arInvoiceLineItem.getIliEnableAutoBuild() == 1
					&& !arInvoiceLineItem.getInvItemLocation().getInvItem().equals(invItemLocation.getInvItem()))) {

				invItemLocation
						.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ASSMBLY_QTY));

			}

			try {

				// generate line number
				LocalInvCosting invCurrentCosting = invCostingHome
						.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(),
								invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
								AD_BRNCH, AD_CMPNY);
				CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

			} catch (FinderException ex) {

				CST_LN_NMBR = 1;

			}

			// void subsequent cost variance adjustments
			Collection invAdjustmentLines = invAdjustmentLineHome
					.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(CST_DT,
							invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
			Iterator i = invAdjustmentLines.iterator();

			while (i.hasNext()) {

				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
				this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

			}

			// create costing
			LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d,
					CST_ASSMBLY_QTY, CST_ASSMBLY_CST, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d,
					CST_ASSMBLY_QTY > 0 ? CST_ASSMBLY_QTY : 0, AD_BRNCH, AD_CMPNY);
			// invItemLocation.addInvCosting(invCosting);
			invCosting.setInvItemLocation(invItemLocation);

			// if cost variance is not 0, generate cost variance for the transaction
			if (CST_VRNC_VL != 0) {

				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"ARMR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
						arInvoiceLineItem.getArReceipt().getRctDescription(),
						arInvoiceLineItem.getArReceipt().getRctDate(), USR_NM, AD_BRNCH, AD_CMPNY);

			}

			// propagate balance if necessary

			Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
					invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH,
					AD_CMPNY);

			i = invCostings.iterator();

			while (i.hasNext()) {

				LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

				invPropagatedCosting
						.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ASSMBLY_QTY);
				invPropagatedCosting
						.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ASSMBLY_CST);

			}

			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
				// regenerate cost variance
				this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
			}

		} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
			LocalGlChartOfAccount glChartOfAccount, boolean isCurrentAcv, byte isDebit, double JL_AMNT,
			Integer AD_CMPNY) {

		Debug.print("ArMiscReceiptEntryControllerBean postToGl 1");

		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome) EJBHomeFactory.lookUpLocalHome(
					LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccountBalance glChartOfAccountBalance = glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
					glAccountingCalendarValue.getAcvCode(), glChartOfAccount.getCoaCode(), AD_CMPNY);

			String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
			short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();

			if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) && isDebit == EJBCommon.TRUE)
					|| (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE")
							&& isDebit == EJBCommon.FALSE)) {

				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				if (!isCurrentAcv) {

					glChartOfAccountBalance.setCoabBeginningBalance(EJBCommon
							.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				}

			} else {

				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				if (!isCurrentAcv) {

					glChartOfAccountBalance.setCoabBeginningBalance(EJBCommon
							.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				}

			}

			if (isCurrentAcv) {

				if (isDebit == EJBCommon.TRUE) {

					glChartOfAccountBalance.setCoabTotalDebit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

				} else {

					glChartOfAccountBalance.setCoabTotalCredit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
				}

			}

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private void post(Date RCT_DT, double RCT_AMNT, LocalArCustomer arCustomer, Integer AD_CMPNY) {

		Debug.print("ArMiscReceiptEntryControllerBean post");

		LocalArCustomerBalanceHome arCustomerBalanceHome = null;

		// Initialize EJB Home

		try {

			arCustomerBalanceHome = (LocalArCustomerBalanceHome) EJBHomeFactory
					.lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);

		} catch (NamingException ex) {

			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

		try {

			// find customer balance before or equal invoice date

			Collection arCustomerBalances = arCustomerBalanceHome.findByBeforeOrEqualInvDateAndCstCustomerCode(RCT_DT,
					arCustomer.getCstCustomerCode(), AD_CMPNY);

			if (!arCustomerBalances.isEmpty()) {

				// get last invoice

				ArrayList arCustomerBalanceList = new ArrayList(arCustomerBalances);

				LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance) arCustomerBalanceList
						.get(arCustomerBalanceList.size() - 1);

				if (arCustomerBalance.getCbDate().before(RCT_DT)) {

					// create new balance

					LocalArCustomerBalance arNewCustomerBalance = arCustomerBalanceHome.create(RCT_DT,
							arCustomerBalance.getCbBalance() + RCT_AMNT, AD_CMPNY);

					// arCustomer.addArCustomerBalance(arNewCustomerBalance);
					arNewCustomerBalance.setArCustomer(arCustomer);

				} else { // equals to invoice date

					arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + RCT_AMNT);

				}

			} else {

				// create new balance

				LocalArCustomerBalance arNewCustomerBalance = arCustomerBalanceHome.create(RCT_DT, RCT_AMNT, AD_CMPNY);

				// arCustomer.addArCustomerBalance(arNewCustomerBalance);
				arNewCustomerBalance.setArCustomer(arCustomer);

			}

			// propagate to subsequent balances if necessary

			arCustomerBalances = arCustomerBalanceHome.findByAfterInvDateAndCstCustomerCode(RCT_DT,
					arCustomer.getCstCustomerCode(), AD_CMPNY);

			Iterator i = arCustomerBalances.iterator();

			while (i.hasNext()) {

				LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance) i.next();

				arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + RCT_AMNT);

			}

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY)
			throws GlobalRecordAlreadyDeletedException, GlobalTransactionAlreadyPostedException,
			GlJREffectiveDateNoPeriodExistException, GlJREffectiveDatePeriodClosedException,
			GlobalJournalNotBalanceException, GlobalBranchAccountNumberInvalidException {

		Debug.print("ArMiscReceiptEntryControllerBean executeInvAdjPost");

		LocalInvAdjustmentHome invAdjustmentHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGlJournalHome glJournalHome = null;
		LocalGlJournalBatchHome glJournalBatchHome = null;
		LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalGlJournalSourceHome glJournalSourceHome = null;
		LocalGlJournalCategoryHome glJournalCategoryHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

		// Initialize EJB Home

		try {

			invAdjustmentHome = (LocalInvAdjustmentHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory.lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME,
					LocalGlSetOfBookHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome) EJBHomeFactory.lookUpLocalHome(
					LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			glJournalHome = (LocalGlJournalHome) EJBHomeFactory.lookUpLocalHome(LocalGlJournalHome.JNDI_NAME,
					LocalGlJournalHome.class);
			glJournalBatchHome = (LocalGlJournalBatchHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
			glSuspenseAccountHome = (LocalGlSuspenseAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
			glJournalLineHome = (LocalGlJournalLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			glJournalSourceHome = (LocalGlJournalSourceHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
			glJournalCategoryHome = (LocalGlJournalCategoryHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			invDistributionRecordHome = (LocalInvDistributionRecordHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if adjustment is already deleted

			LocalInvAdjustment invAdjustment = null;

			try {

				invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if adjustment is already posted or void

			if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

				if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
					throw new GlobalTransactionAlreadyPostedException();

			}

			Collection invAdjustmentLines = null;

			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE)
				invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE,
						invAdjustment.getAdjCode(), AD_CMPNY);
			else
				invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE,
						invAdjustment.getAdjCode(), AD_CMPNY);

			Iterator i = invAdjustmentLines.iterator();

			while (i.hasNext()) {

				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

				LocalInvCosting invCosting = invCostingHome
						.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
								invAdjustment.getAdjDate(),
								invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
								invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH,
								AD_CMPNY);

				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);

			}

			// post to gl if necessary

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			// validate if date has no period and period is closed

			LocalGlSetOfBook glJournalSetOfBook = null;

			try {

				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

			} catch (FinderException ex) {

				throw new GlJREffectiveDateNoPeriodExistException();

			}

			LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome
					.findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
							invAdjustment.getAdjDate(), AD_CMPNY);

			if (glAccountingCalendarValue.getAcvStatus() == 'N' || glAccountingCalendarValue.getAcvStatus() == 'C'
					|| glAccountingCalendarValue.getAcvStatus() == 'P') {

				throw new GlJREffectiveDatePeriodClosedException();

			}

			// check if invoice is balance if not check suspense posting

			LocalGlJournalLine glOffsetJournalLine = null;

			Collection invDistributionRecords = null;

			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

				invDistributionRecords = invDistributionRecordHome
						.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);

			} else {

				invDistributionRecords = invDistributionRecordHome
						.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

			}

			Iterator j = invDistributionRecords.iterator();

			double TOTAL_DEBIT = 0d;
			double TOTAL_CREDIT = 0d;

			while (j.hasNext()) {

				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) j.next();

				double DR_AMNT = 0d;

				DR_AMNT = invDistributionRecord.getDrAmount();

				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

					TOTAL_DEBIT += DR_AMNT;

				} else {

					TOTAL_CREDIT += DR_AMNT;

				}

			}

			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE && TOTAL_DEBIT != TOTAL_CREDIT) {

				LocalGlSuspenseAccount glSuspenseAccount = null;

				try {

					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY",
							"INVENTORY ADJUSTMENTS", AD_CMPNY);

				} catch (FinderException ex) {

					throw new GlobalJournalNotBalanceException();

				}

				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

					glOffsetJournalLine = glJournalLineHome.create((short) (invDistributionRecords.size() + 1),
							EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

				} else {

					glOffsetJournalLine = glJournalLineHome.create((short) (invDistributionRecords.size() + 1),
							EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

				}

				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
				// glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
				glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);

			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE && TOTAL_DEBIT != TOTAL_CREDIT) {

				throw new GlobalJournalNotBalanceException();

			}

			// create journal batch if necessary

			LocalGlJournalBatch glJournalBatch = null;
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

			try {

				glJournalBatch = glJournalBatchHome.findByJbName(
						"JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", AD_BRNCH,
						AD_CMPNY);

			} catch (FinderException ex) {

			}

			if (glJournalBatch == null) {

				glJournalBatch = glJournalBatchHome.create(
						"JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", "JOURNAL IMPORT",
						"CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

			}

			// create journal entry

			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(), 0.0d, null,
					invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null, 'N', EJBCommon.TRUE, EJBCommon.FALSE,
					USR_NM, new Date(), USR_NM, new Date(), null, null, USR_NM,
					EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, EJBCommon.FALSE, null,

					AD_BRNCH, AD_CMPNY);

			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
			glJournal.setGlJournalSource(glJournalSource);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
					.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS",
					AD_CMPNY);
			glJournal.setGlJournalCategory(glJournalCategory);

			if (glJournalBatch != null) {

				glJournal.setGlJournalBatch(glJournalBatch);
			}

			// create journal lines

			j = invDistributionRecords.iterator();

			while (j.hasNext()) {

				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) j.next();

				double DR_AMNT = 0d;

				DR_AMNT = invDistributionRecord.getDrAmount();

				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),
						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

				glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());

				glJournalLine.setGlJournal(glJournal);

				invDistributionRecord.setDrImported(EJBCommon.TRUE);

			}

			if (glOffsetJournalLine != null) {

				glOffsetJournalLine.setGlJournal(glJournal);

			}

			// post journal to gl

			Collection glJournalLines = glJournal.getGlJournalLines();

			i = glJournalLines.iterator();

			while (i.hasNext()) {

				LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

				// post current to current acv

				this.postToGl(glAccountingCalendarValue, glJournalLine.getGlChartOfAccount(), true,
						glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

				// post to subsequent acvs (propagate)

				Collection glSubsequentAccountingCalendarValues = glAccountingCalendarValueHome
						.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
								glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

				while (acvsIter.hasNext()) {

					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = (LocalGlAccountingCalendarValue) acvsIter
							.next();

					this.postToGl(glSubsequentAccountingCalendarValue, glJournalLine.getGlChartOfAccount(), false,
							glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

				}

				// post to subsequent years if necessary

				Collection glSubsequentSetOfBooks = glSetOfBookHome
						.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
					LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome
							.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH,
									AD_CMPNY);

					Iterator sobIter = glSubsequentSetOfBooks.iterator();

					while (sobIter.hasNext()) {

						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook) sobIter.next();

						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

						// post to subsequent acvs of subsequent set of book(propagate)

						Collection glAccountingCalendarValues = glAccountingCalendarValueHome
								.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

						Iterator acvIter = glAccountingCalendarValues.iterator();

						while (acvIter.hasNext()) {

							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = (LocalGlAccountingCalendarValue) acvIter
									.next();

							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY")
									|| ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

								this.postToGl(glSubsequentAccountingCalendarValue, glJournalLine.getGlChartOfAccount(),
										false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

							} else { // revenue & expense

								this.postToGl(glSubsequentAccountingCalendarValue, glRetainedEarningsAccount, false,
										glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

							}

						}

						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0)
							break;

					}

				}

			}

			invAdjustment.setAdjPosted(EJBCommon.TRUE);

		} catch (GlJREffectiveDateNoPeriodExistException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlJREffectiveDatePeriodClosedException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyDeletedException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			// getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL,
			Integer COA_CODE, LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)

			throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArMiscReceiptEntryControllerBean addInvDrEntry");

		LocalAdCompanyHome adCompanyHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);
			invDistributionRecordHome = (LocalInvDistributionRecordHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			// validate coa

			LocalGlChartOfAccount glChartOfAccount = null;

			try {

				glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

			} catch (FinderException ex) {

				throw new GlobalBranchAccountNumberInvalidException();

			}

			// create distribution record

			LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
					EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL,
					EJBCommon.FALSE, AD_CMPNY);

			// invAdjustment.addInvDistributionRecord(invDistributionRecord);
			invDistributionRecord.setInvAdjustment(invAdjustment);
			// glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
			invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			throw new GlobalBranchAccountNumberInvalidException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ArMiscReceiptEntryControllerBean voidInvAdjustment");

		try {

			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
			ArrayList list = new ArrayList();

			Iterator i = invDistributionRecords.iterator();

			while (i.hasNext()) {

				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) i.next();

				list.add(invDistributionRecord);

			}

			i = list.iterator();

			while (i.hasNext()) {

				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) i.next();

				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
						invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
						invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

			}

			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
			i = invAdjustmentLines.iterator();
			list.clear();

			while (i.hasNext()) {

				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

				list.add(invAdjustmentLine);

			}

			i = list.iterator();

			while (i.hasNext()) {

				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(), invAdjustment,
						(invAdjustmentLine.getAlUnitCost()) * -1, EJBCommon.TRUE, AD_CMPNY);

			}

			invAdjustment.setAdjVoid(EJBCommon.TRUE);

			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
					AD_CMPNY);

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
			double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {

		Debug.print("ArMiscReceiptEntryControllerBean addInvAlEntry");

		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

		// Initialize EJB Home

		try {

			invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// create dr entry
			LocalInvAdjustmentLine invAdjustmentLine = null;
			invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL, null, null, 0, 0, AL_VD, AD_CMPNY);

			// map adjustment, unit of measure, item location
			// invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
			invAdjustmentLine.setInvAdjustment(invAdjustment);
			// invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
			invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
			// invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
			invAdjustmentLine.setInvItemLocation(invItemLocation);

			return invAdjustmentLine;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT,
			double CST_ADJST_QTY, double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,
			Integer AD_CMPNY) {

		Debug.print("ArMiscReceiptEntryControllerBean postInvAdjustmentToInventory");

		LocalInvCostingHome invCostingHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
			int CST_LN_NMBR = 0;

			CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
			CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
			CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
			CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

			if (CST_ADJST_QTY < 0) {

				invItemLocation
						.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

			}

			// create costing

			try {

				// generate line number

				LocalInvCosting invCurrentCosting = invCostingHome
						.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(),
								invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
								AD_BRNCH, AD_CMPNY);
				CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

			} catch (FinderException ex) {

				CST_LN_NMBR = 1;

			}

			LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
					CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d,
					CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
			// invItemLocation.addInvCosting(invCosting);
			invCosting.setInvItemLocation(invItemLocation);
			invCosting.setInvAdjustmentLine(invAdjustmentLine);

			// propagate balance if necessary

			Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
					invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH,
					AD_CMPNY);

			Iterator i = invCostings.iterator();

			while (i.hasNext()) {

				LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

				invPropagatedCosting
						.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
				invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);

			}

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			// getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void postToInv(LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_QTY_SLD,
			double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM,
			Integer AD_BRNCH, Integer AD_CMPNY)
			throws AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {

		Debug.print("ArMiscReceiptEntryControllerBean postToInv");

		LocalInvCostingHome invCostingHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

		// Initialize EJB Home

		try {

			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
			int CST_LN_NMBR = 0;

			CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
			CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
			CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
			CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

			if (CST_QTY_SLD > 0 && arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

				invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - CST_QTY_SLD);

			}

			try {

				// generate line number
				LocalInvCosting invCurrentCosting = invCostingHome
						.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(),
								invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
								AD_BRNCH, AD_CMPNY);
				CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

			} catch (FinderException ex) {

				CST_LN_NMBR = 1;

			}

			// void subsequent cost variance adjustments
			Collection invAdjustmentLines = invAdjustmentLineHome
					.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(CST_DT,
							invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
			Iterator i = invAdjustmentLines.iterator();

			while (i.hasNext()) {

				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
				this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

			}

			String prevExpiryDates = "";
			String miscListPrpgt = "";
			double qtyPrpgt = 0;
			try {
				LocalInvCosting prevCst = invCostingHome
						.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
								CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
				Debug.print("ArReceiptPostControllerBean postToInv B");
				prevExpiryDates = prevCst.getCstExpiryDate();
				qtyPrpgt = prevCst.getCstRemainingQuantity();

				if (prevExpiryDates == null) {
					prevExpiryDates = "";
				}

			} catch (Exception ex) {
				System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
				prevExpiryDates = "";
			}

			// create costing
			LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
					0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
			// invItemLocation.addInvCosting(invCosting);
			invCosting.setInvItemLocation(invItemLocation);
			invCosting.setArInvoiceLineItem(arInvoiceLineItem);

			// Get Latest Expiry Dates
			if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc() != 0) {
				if (prevExpiryDates != null && prevExpiryDates != "" && prevExpiryDates.length() != 0) {
					if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
							&& arInvoiceLineItem.getIliMisc().length() != 0) {
						int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));

						String miscList2Prpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt,
								"False");
						ArrayList miscList = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt);
						String propagateMiscPrpgt = "";
						String ret = "";
						String exp = "";
						String Checker = "";

						Iterator mi = miscList.iterator();

						propagateMiscPrpgt = prevExpiryDates;
						ret = propagateMiscPrpgt;
						while (mi.hasNext()) {
							String miscStr = (String) mi.next();

							Integer qTest = this.checkExpiryDates(ret + "fin$");
							ArrayList miscList2 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

							// ArrayList miscList2 = this.expiryDates("$" + ret, qtyPrpgt);
							Iterator m2 = miscList2.iterator();
							ret = "";
							String ret2 = "false";
							int a = 0;
							while (m2.hasNext()) {
								String miscStr2 = (String) m2.next();

								if (ret2 == "1st") {
									ret2 = "false";
								}
								System.out.println("miscStr: " + miscStr);
								System.out.println("miscStr2: " + miscStr2);

								if (miscStr2.equals(miscStr)) {
									if (a == 0) {
										a = 1;
										ret2 = "1st";
										Checker = "true";
									} else {
										a = a + 1;
										ret2 = "true";
									}
								}
								System.out.println("Checker: " + Checker);
								System.out.println("ret2: " + ret2);
								if (!miscStr2.equals(miscStr) || a > 1) {
									if ((ret2 != "1st") && ((ret2 == "false") || (ret2 == "true"))) {
										if (miscStr2 != "") {
											miscStr2 = "$" + miscStr2;
											ret = ret + miscStr2;
											System.out.println("ret " + ret);
											ret2 = "false";
										}
									}
								}

							}
							if (ret != "") {
								ret = ret + "$";
							}
							System.out.println("ret una: " + ret);
							exp = exp + ret;
							qtyPrpgt = qtyPrpgt - 1;
						}
						System.out.println("ret fin " + ret);
						System.out.println("exp fin " + exp);
						// propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
						propagateMiscPrpgt = ret;
						// propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a,
						// "True");
						if (Checker == "true") {
							// invCosting.setCstExpiryDate(propagateMiscPrpgt);
						} else {
							System.out.println("Exp Not Found");
							throw new GlobalExpiryDateNotFoundException(
									arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
						}
						invCosting.setCstExpiryDate(propagateMiscPrpgt);
					} else {
						invCosting.setCstExpiryDate(prevExpiryDates);
					}

				} else {
					if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
							&& arInvoiceLineItem.getIliMisc().length() != 0) {
						int initialQty = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
						String initialPrpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), initialQty,
								"False");

						invCosting.setCstExpiryDate(initialPrpgt);
					} else {
						invCosting.setCstExpiryDate(prevExpiryDates);
					}

				}
			}

			// if cost variance is not 0, generate cost variance for the transaction
			if (CST_VRNC_VL != 0) {

				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"APMR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
						arInvoiceLineItem.getArReceipt().getRctDescription(),
						arInvoiceLineItem.getArReceipt().getRctDate(), USR_NM, AD_BRNCH, AD_CMPNY);

			}

			// propagate balance if necessary
			Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT,
					invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH,
					AD_CMPNY);
			String miscList = "";
			ArrayList miscList2 = null;
			i = invCostings.iterator();
			String propagateMisc = "";
			String ret = "";
			while (i.hasNext()) {
				String Checker = "";
				String Checker2 = "";

				LocalInvCosting invPropagatedCosting = (LocalInvCosting) i.next();

				invPropagatedCosting
						.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() - CST_QTY_SLD);
				invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() - CST_CST_OF_SLS);

				if (arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc() != 0) {
					if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
							&& arInvoiceLineItem.getIliMisc().length() != 0) {
						double qty = Double.parseDouble(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
						// invPropagatedCosting.getInvAdjustmentLine().getAlMisc();
						miscList = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty, "False");
						miscList2 = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty);
						System.out.println("arInvoiceLineItem.getIliMisc(): " + arInvoiceLineItem.getIliMisc());
						System.out.println("getAlAdjustQuantity(): " + arInvoiceLineItem.getIliQuantity());

						if (arInvoiceLineItem.getIliQuantity() < 0) {
							Iterator mi = miscList2.iterator();

							propagateMisc = invPropagatedCosting.getCstExpiryDate();
							ret = invPropagatedCosting.getCstExpiryDate();
							while (mi.hasNext()) {
								String miscStr = (String) mi.next();

								Integer qTest = this.checkExpiryDates(ret + "fin$");
								ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

								// ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
								System.out.println("ret: " + ret);
								Iterator m2 = miscList3.iterator();
								ret = "";
								String ret2 = "false";
								int a = 0;
								while (m2.hasNext()) {
									String miscStr2 = (String) m2.next();

									if (ret2 == "1st") {
										ret2 = "false";
									}
									System.out.println("2 miscStr: " + miscStr);
									System.out.println("2 miscStr2: " + miscStr2);
									if (miscStr2.equals(miscStr)) {
										if (a == 0) {
											a = 1;
											ret2 = "1st";
											Checker2 = "true";
										} else {
											a = a + 1;
											ret2 = "true";
										}
									}
									System.out.println("Checker: " + Checker2);
									if (!miscStr2.equals(miscStr) || a > 1) {
										if ((ret2 != "1st") || (ret2 == "false") || (ret2 == "true")) {
											if (miscStr2 != "") {
												miscStr2 = "$" + miscStr2;
												ret = ret + miscStr2;
												ret2 = "false";
											}
										}
									}

								}
								if (Checker2 != "true") {
									throw new GlobalExpiryDateNotFoundException(
											arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
								}

								ret = ret + "$";
								qtyPrpgt = qtyPrpgt - 1;
							}
						}

					}

					if (arInvoiceLineItem.getIliMisc() != null && arInvoiceLineItem.getIliMisc() != ""
							&& arInvoiceLineItem.getIliMisc().length() != 0) {
						if (prevExpiryDates != null && prevExpiryDates != "" && prevExpiryDates.length() != 0) {

							Iterator mi = miscList2.iterator();

							propagateMisc = invPropagatedCosting.getCstExpiryDate();
							ret = propagateMisc;
							while (mi.hasNext()) {
								String miscStr = (String) mi.next();

								Integer qTest = this.checkExpiryDates(ret + "fin$");
								ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

								// ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
								System.out.println("ret: " + ret);
								Iterator m2 = miscList3.iterator();
								ret = "";
								String ret2 = "false";
								int a = 0;
								while (m2.hasNext()) {
									String miscStr2 = (String) m2.next();

									if (ret2 == "1st") {
										ret2 = "false";
									}
									System.out.println("2 miscStr: " + miscStr);
									System.out.println("2 miscStr2: " + miscStr2);
									if (miscStr2.equals(miscStr)) {
										if (a == 0) {
											a = 1;
											ret2 = "1st";
											Checker = "true";
										} else {
											a = a + 1;
											ret2 = "true";
										}
									}
									System.out.println("Checker: " + Checker);
									if (!miscStr2.equals(miscStr) || a > 1) {
										if ((ret2 != "1st") || (ret2 == "false") || (ret2 == "true")) {
											if (miscStr2 != "") {
												miscStr2 = "$" + miscStr2;
												ret = ret + miscStr2;
												ret2 = "false";
											}
										}
									}

								}
								ret = ret + "$";
								qtyPrpgt = qtyPrpgt - 1;
							}
							propagateMisc = ret;
						}

					} else {
						try {
							propagateMisc = miscList + invPropagatedCosting.getCstExpiryDate().substring(1,
									invPropagatedCosting.getCstExpiryDate().length());
						} catch (Exception e) {
							propagateMisc = miscList;
						}
					}
					invPropagatedCosting.setCstExpiryDate(propagateMisc);
				}

			}

			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
				// regenerate cost variance
				this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
			}

		} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

			throw ex;

		} catch (GlobalExpiryDateNotFoundException ex) {
			throw ex;
		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
			String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY)
			throws AdPRFCoaGlVarianceAccountNotFoundException {
		/*
		 * Debug.print("ArMiscReceiptEntryControllerBean generateCostVariance");
		 * 
		 * LocalAdPreferenceHome adPreferenceHome = null; LocalGlChartOfAccountHome
		 * glChartOfAccountHome = null; LocalAdBranchItemLocationHome
		 * adBranchItemLocationHome = null;
		 * 
		 * // Initialize EJB Home
		 * 
		 * try {
		 * 
		 * adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		 * lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
		 * LocalAdPreferenceHome.class); glChartOfAccountHome =
		 * (LocalGlChartOfAccountHome)EJBHomeFactory.
		 * lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME,
		 * LocalGlChartOfAccountHome.class); adBranchItemLocationHome =
		 * (LocalAdBranchItemLocationHome)EJBHomeFactory.
		 * lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME,
		 * LocalAdBranchItemLocationHome.class);
		 * 
		 * 
		 * } catch (NamingException ex) {
		 * 
		 * throw new EJBException(ex.getMessage());
		 * 
		 * }
		 * 
		 * try{
		 * 
		 * LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR,
		 * ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH, AD_CMPNY); LocalAdPreference
		 * adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
		 * LocalGlChartOfAccount glCoaVarianceAccount = null;
		 * 
		 * if(adPreference.getPrfInvGlCoaVarianceAccount() == null) throw new
		 * AdPRFCoaGlVarianceAccountNotFoundException();
		 * 
		 * try{
		 * 
		 * glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.
		 * getPrfInvGlCoaVarianceAccount());
		 * //glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
		 * newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
		 * 
		 * } catch (FinderException ex) {
		 * 
		 * throw new AdPRFCoaGlVarianceAccountNotFoundException();
		 * 
		 * }
		 * 
		 * LocalInvAdjustmentLine invAdjustmentLine =
		 * this.addInvAlEntry(invItemLocation, newInvAdjustment, CST_VRNC_VL,
		 * EJBCommon.FALSE, AD_CMPNY);
		 * 
		 * // check for branch mapping
		 * 
		 * LocalAdBranchItemLocation adBranchItemLocation = null;
		 * 
		 * try{
		 * 
		 * adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
		 * invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
		 * 
		 * } catch (FinderException ex) {
		 * 
		 * }
		 * 
		 * LocalGlChartOfAccount glInventoryChartOfAccount = null;
		 * 
		 * if (adBranchItemLocation == null) {
		 * 
		 * glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
		 * invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount()); } else
		 * {
		 * 
		 * glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
		 * adBranchItemLocation.getBilCoaGlInventoryAccount());
		 * 
		 * }
		 * 
		 * 
		 * boolean isDebit = CST_VRNC_VL < 0 ? false : true;
		 * 
		 * //inventory dr this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),
		 * "INVENTORY", isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE,
		 * Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
		 * glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH,
		 * AD_CMPNY);
		 * 
		 * //variance dr
		 * this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", !isDebit
		 * == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL),
		 * EJBCommon.FALSE, glCoaVarianceAccount.getCoaCode(), newInvAdjustment,
		 * AD_BRNCH, AD_CMPNY);
		 * 
		 * this.executeInvAdjPost(newInvAdjustment.getAdjCode(),
		 * newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);
		 * 
		 * } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
		 * 
		 * getSessionContext().setRollbackOnly(); throw ex;
		 * 
		 * } catch (Exception ex) {
		 * 
		 * Debug.printStackTrace(ex); getSessionContext().setRollbackOnly(); throw new
		 * EJBException(ex.getMessage());
		 * 
		 * }
		 */
	}

	private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH,
			Integer AD_CMPNY) throws AdPRFCoaGlVarianceAccountNotFoundException {
		/*
		 * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance");
		 * 
		 * try {
		 * 
		 * Iterator i = invCostings.iterator(); LocalInvCosting prevInvCosting =
		 * invCosting;
		 * 
		 * while (i.hasNext()) {
		 * 
		 * LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
		 * 
		 * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance A");
		 * if(prevInvCosting.getCstRemainingQuantity() < 0) {
		 * 
		 * double TTL_CST = 0; double QNTY = 0; String ADJ_RFRNC_NMBR = ""; String
		 * ADJ_DSCRPTN = ""; String ADJ_CRTD_BY = "";
		 * 
		 * // get unit cost adjusment, document number and unit of measure if
		 * (invPropagatedCosting.getApPurchaseOrderLine() != null) {
		 * 
		 * TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount(); QNTY =
		 * this.convertByUomFromAndItemAndQuantity(
		 * invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
		 * invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem
		 * (), invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
		 * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance B");
		 * ADJ_DSCRPTN =
		 * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().
		 * getPoDescription(); ADJ_CRTD_BY =
		 * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().
		 * getPoPostedBy(); ADJ_RFRNC_NMBR = "APRI" +
		 * invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().
		 * getPoDocumentNumber();
		 * 
		 * } else if (invPropagatedCosting.getApVoucherLineItem() != null){
		 * 
		 * TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount(); QNTY =
		 * this.convertByUomFromAndItemAndQuantity(
		 * invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
		 * invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem()
		 * , invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
		 * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance C"); if
		 * (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
		 * 
		 * ADJ_DSCRPTN =
		 * invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription(
		 * ); ADJ_CRTD_BY =
		 * invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
		 * ADJ_RFRNC_NMBR = "APVOU" +
		 * invPropagatedCosting.getApVoucherLineItem().getApVoucher().
		 * getVouDocumentNumber();
		 * 
		 * } else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null)
		 * {
		 * 
		 * ADJ_DSCRPTN =
		 * invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
		 * ADJ_CRTD_BY =
		 * invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
		 * ADJ_RFRNC_NMBR = "APCHK" +
		 * invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber
		 * ();
		 * 
		 * }
		 * 
		 * } else if (invPropagatedCosting.getArInvoiceLineItem() != null){
		 * 
		 * QNTY = this.convertByUomFromAndItemAndQuantity(
		 * invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
		 * invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem()
		 * , invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
		 * TTL_CST = prevInvCosting.getCstRemainingValue() -
		 * invPropagatedCosting.getCstRemainingValue();
		 * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance D");
		 * if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
		 * 
		 * ADJ_DSCRPTN =
		 * invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription(
		 * ); ADJ_CRTD_BY =
		 * invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
		 * ADJ_RFRNC_NMBR = "ARCM" +
		 * invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
		 * 
		 * } else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() !=
		 * null){
		 * 
		 * ADJ_DSCRPTN =
		 * invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription(
		 * ); ADJ_CRTD_BY =
		 * invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
		 * ADJ_RFRNC_NMBR = "ARMR" +
		 * invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();
		 * 
		 * }
		 * 
		 * } else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){
		 * 
		 * TTL_CST = prevInvCosting.getCstRemainingValue() -
		 * invPropagatedCosting.getCstRemainingValue(); QNTY =
		 * this.convertByUomFromAndItemAndQuantity(
		 * invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().
		 * getInvUnitOfMeasure(),
		 * invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().
		 * getInvItemLocation().getInvItem(),
		 * invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(),
		 * AD_CMPNY);
		 * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance E");
		 * ADJ_DSCRPTN =
		 * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().
		 * getInvDescription(); ADJ_CRTD_BY =
		 * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().
		 * getInvPostedBy(); ADJ_RFRNC_NMBR = "ARCM" +
		 * invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber
		 * ();
		 * 
		 * } else if (invPropagatedCosting.getInvAdjustmentLine() != null){
		 * 
		 * ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().
		 * getAdjDescription(); ADJ_CRTD_BY =
		 * invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy
		 * (); ADJ_RFRNC_NMBR = "INVADJ" +
		 * invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().
		 * getAdjDocumentNumber();
		 * 
		 * if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
		 * 
		 * TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
		 * invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity()); QNTY =
		 * this.convertByUomFromAndItemAndQuantity(
		 * invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
		 * invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem()
		 * , invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(),
		 * AD_CMPNY);
		 * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance F"); }
		 * 
		 * } else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){
		 * 
		 * QNTY =
		 * invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
		 * TTL_CST =
		 * invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();
		 * 
		 * ADJ_DSCRPTN =
		 * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().
		 * getAtrDescription(); ADJ_CRTD_BY =
		 * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().
		 * getAtrPostedBy(); ADJ_RFRNC_NMBR = "INVAT" +
		 * invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().
		 * getAtrDocumentNumber();
		 * 
		 * } else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){
		 * 
		 * if(invPropagatedCosting.getInvBranchStockTransferLine().
		 * getInvBranchStockTransfer().getBstTransferOutNumber() != null) {
		 * 
		 * TTL_CST =
		 * invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount(); QNTY =
		 * this.convertByUomFromAndItemAndQuantity(
		 * invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
		 * invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().
		 * getInvItem(),
		 * invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived()
		 * , AD_CMPNY);
		 * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance G"); }
		 * else {
		 * 
		 * TTL_CST =
		 * invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount(); QNTY =
		 * this.convertByUomFromAndItemAndQuantity(
		 * invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
		 * invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().
		 * getInvItem(),
		 * invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(),
		 * AD_CMPNY);
		 * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance H"); }
		 * 
		 * ADJ_DSCRPTN = invPropagatedCosting.getInvBranchStockTransferLine().
		 * getInvBranchStockTransfer().getBstDescription(); ADJ_CRTD_BY =
		 * invPropagatedCosting.getInvBranchStockTransferLine().
		 * getInvBranchStockTransfer().getBstPostedBy(); ADJ_RFRNC_NMBR = "INVBST" +
		 * invPropagatedCosting.getInvBranchStockTransferLine().
		 * getInvBranchStockTransfer().getBstNumber();
		 * 
		 * } else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){
		 * 
		 * TTL_CST = prevInvCosting.getCstRemainingValue() -
		 * invPropagatedCosting.getCstRemainingValue(); QNTY =
		 * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
		 * ADJ_DSCRPTN = invPropagatedCosting.getInvBuildUnbuildAssemblyLine().
		 * getInvBuildUnbuildAssembly().getBuaDescription(); ADJ_CRTD_BY =
		 * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().
		 * getInvBuildUnbuildAssembly().getBuaPostedBy(); ADJ_RFRNC_NMBR = "INVBUA" +
		 * invPropagatedCosting.getInvBuildUnbuildAssemblyLine().
		 * getInvBuildUnbuildAssembly().getBuaDocumentNumber();
		 * 
		 * } else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){
		 * 
		 * TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
		 * QNTY = this.convertByUomFromAndItemAndQuantity(
		 * invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
		 * invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().
		 * getInvItem(),
		 * invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(),
		 * AD_CMPNY);
		 * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance I");
		 * ADJ_DSCRPTN =
		 * invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().
		 * getSiDescription(); ADJ_CRTD_BY =
		 * invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().
		 * getSiPostedBy(); ADJ_RFRNC_NMBR = "INVSI" +
		 * invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().
		 * getSiDocumentNumber();
		 * 
		 * } else if (invPropagatedCosting.getInvStockTransferLine()!= null) {
		 * 
		 * TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount(); QNTY
		 * = this.convertByUomFromAndItemAndQuantity(
		 * invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
		 * invPropagatedCosting.getInvStockTransferLine().getInvItem(),
		 * invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(),
		 * AD_CMPNY);
		 * Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance J");
		 * ADJ_DSCRPTN =
		 * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().
		 * getStDescription(); ADJ_CRTD_BY =
		 * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().
		 * getStPostedBy(); ADJ_RFRNC_NMBR = "INVST" +
		 * invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().
		 * getStDocumentNumber();
		 * 
		 * } else {
		 * 
		 * prevInvCosting = invPropagatedCosting; continue;
		 * 
		 * }
		 * 
		 * // if quantity is equal 0, no variance. if(QNTY == 0) continue;
		 * 
		 * // compute new cost variance double UNT_CST = TTL_CST/QNTY; double
		 * CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
		 * invPropagatedCosting.getCstRemainingValue());
		 * 
		 * if(CST_VRNC_VL != 0)
		 * this.generateCostVariance(invPropagatedCosting.getInvItemLocation(),
		 * CST_VRNC_VL, ADJ_RFRNC_NMBR, ADJ_DSCRPTN, invPropagatedCosting.getCstDate(),
		 * ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
		 * 
		 * }
		 * 
		 * // set previous costing prevInvCosting = invPropagatedCosting;
		 * 
		 * }
		 * 
		 * } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
		 * 
		 * throw ex;
		 * 
		 * } catch (Exception ex) {
		 * 
		 * Debug.printStackTrace(ex); //getSessionContext().setRollbackOnly(); throw new
		 * EJBException(ex.getMessage());
		 * 
		 * }
		 */
	}

	public String propagateExpiryDates(String misc, double qty, String reverse) throws Exception {
		// ActionErrors errors = new ActionErrors();
		Debug.print("ApReceivingItemControllerBean getExpiryDates");
		System.out.println("misc: " + misc);

		String separator = "";
		if (reverse == "False") {
			separator = "$";
		} else {
			separator = " ";
		}

		// Remove first $ character
		misc = misc.substring(1);
		System.out.println("misc: " + misc);
		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;

		String miscList = new String();

		for (int x = 0; x < qty; x++) {

			// Date
			start = nextIndex + 1;
			nextIndex = misc.indexOf(separator, start);
			length = nextIndex - start;
			String g = misc.substring(start, start + length);
			System.out.println("g: " + g);
			System.out.println("g length: " + g.length());
			if (g.length() != 0) {
				miscList = miscList + "$" + g;
				System.out.println("miscList G: " + miscList);
			}
		}

		miscList = miscList + "$";
		System.out.println("miscList :" + miscList);
		return (miscList);
	}

	public String getQuantityExpiryDates(String qntty) {
		String separator = "$";
		String y = "";
		try {
//     		Remove first $ character
			qntty = qntty.substring(1);

			// Counter
			int start = 0;
			int nextIndex = qntty.indexOf(separator, start);
			int length = nextIndex - start;

			y = (qntty.substring(start, start + length));
			System.out.println("Y " + y);
		} catch (Exception e) {
			y = "0";
		}

		return y;
	}

	private ArrayList expiryDates(String misc, double qty) throws Exception {
		Debug.print("ApReceivingItemControllerBean getExpiryDates");
		System.out.println("misc: " + misc);
		String separator = "$";

		// Remove first $ character
		misc = misc.substring(1);

		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;

		System.out.println("qty" + qty);
		ArrayList miscList = new ArrayList();

		for (int x = 0; x < qty; x++) {

			// Date
			start = nextIndex + 1;
			nextIndex = misc.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println("x" + x);
			String checker = misc.substring(start, start + length);
			System.out.println("checker" + checker);
			if (checker.length() != 0 || checker != "null") {
				miscList.add(checker);
			} else {
				miscList.add("null");
			}
		}

		System.out.println("miscList :" + miscList);
		return miscList;
	}

	public static int checkExpiryDates(String misc) throws Exception {

		String separator = "$";

		// Remove first $ character
		misc = misc.substring(1);
		// System.out.println("misc: " + misc);
		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;
		int numberExpry = 0;
		String miscList = new String();
		String miscList2 = "";
		String g = "";
		try {
			while (g != "fin") {
				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				g = misc.substring(start, start + length);
				if (g.length() != 0) {
					if (g != null || g != "" || g != "null") {
						if (g.contains("null")) {
							miscList2 = "Error";
						} else {
							miscList = miscList + "$" + g;
							numberExpry++;
						}
					} else {
						miscList2 = "Error";
					}

				} else {
					miscList2 = "Error";
				}
			}
		} catch (Exception e) {

		}

		if (miscList2 == "") {
			miscList = miscList + "$";
		} else {
			miscList = miscList2;
		}

		return (numberExpry);
	}

	public void ejbCreate() throws CreateException {
		Debug.print("ArMiscReceiptSyncControllerBean ejbCreate");
	}

	public void ejbRemove() {
	};

	public void ejbActivate() {
	}

	public void ejbPassivate() {
	}

	public void setSessionContext(SessionContext ctx) {
	}

}
