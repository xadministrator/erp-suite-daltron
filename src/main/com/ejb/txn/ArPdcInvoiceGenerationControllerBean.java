
/*
 * ArPdcInvoiceGenerationControllerBean.java
 *
 * Created on July 1, 2005, 5:00 PM
 *
 * @author  Jolly T. Martin
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdBranchCustomer;
import com.ejb.ad.LocalAdBranchCustomerHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDiscount;
import com.ejb.ad.LocalAdDiscountHome;
import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentScheduleHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArAutoAccountingSegment;
import com.ejb.ar.LocalArAutoAccountingSegmentHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArPdc;
import com.ejb.ar.LocalArPdcHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.AdPRFCoaGlCustomerDepositAccountNotFoundException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.genfld.LocalGenField;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.ArModAppliedInvoiceDetails;
import com.util.ArModPdcDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArPdcInvoiceInvoiceGenerationControllerEJB"
 *           display-name="Used for generating invoices for post dated checks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArPdcInvoiceGenerationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArPdcInvoiceGenerationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArPdcInvoiceGenerationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 */

public class ArPdcInvoiceGenerationControllerBean extends AbstractSessionBean {


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ArPdcInvoiceGenerationControllerBean getArCstAll");

		LocalArCustomerHome arCustomerHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

			Iterator i = arCustomers.iterator();

			while (i.hasNext()) {

				LocalArCustomer arCustomer = (LocalArCustomer)i.next();

				list.add(arCustomer.getCstCustomerCode());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArPdcByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ArPdcInvoiceGenerationControllerBean getArPdcByCriteria");

		LocalArPdcHome arPdcHome = null;

		// Initialize EJB Home

		try {

			arPdcHome = (LocalArPdcHome)EJBHomeFactory.
			lookUpLocalHome(LocalArPdcHome.JNDI_NAME, LocalArPdcHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		ArrayList pdcList = new ArrayList();

		StringBuffer jbossQl = new StringBuffer();
		jbossQl.append("SELECT OBJECT(pdc) FROM ArPdc pdc ");

		boolean firstArgument = true;
		short ctr = 0;
		int criteriaSize = criteria.size();
		Object obj[];

		// Allocate the size of the object parameter

		if (criteria.containsKey("customerCode")) {

			criteriaSize--;

		}

		obj = new Object[criteriaSize];

		if (criteria.containsKey("customerCode")) {

			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			jbossQl.append("pdc.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

		}

		if (criteria.containsKey("maturityDateFrom")) {

			if (!firstArgument) {
				jbossQl.append("AND ");
			} else {
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			jbossQl.append("pdc.pdcMaturityDate>=?" + (ctr+1) + " ");
			obj[ctr] = (Date)criteria.get("maturityDateFrom");
			ctr++;
		}

		if (criteria.containsKey("maturityDateTo")) {

			if (!firstArgument) {
				jbossQl.append("AND ");
			} else {
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			jbossQl.append("pdc.pdcMaturityDate<=?" + (ctr+1) + " ");
			obj[ctr] = (Date)criteria.get("maturityDateTo");
			ctr++;

		}

		if (!firstArgument) {
			jbossQl.append("AND ");
		} else {
			firstArgument = false;
			jbossQl.append("WHERE ");
		}

		jbossQl.append("pdc.pdcStatus='MATURED' ");

		if (!firstArgument) {
			jbossQl.append("AND ");
		} else {
			firstArgument = false;
			jbossQl.append("WHERE ");
		}

		jbossQl.append("pdc.pdcPosted=" + EJBCommon.TRUE +" ");

		if (!firstArgument) {
			jbossQl.append("AND ");
		} else {
			firstArgument = false;
			jbossQl.append("WHERE ");
		}

		jbossQl.append("pdc.pdcAdBranch=" + AD_BRNCH + " AND pdc.pdcAdCompany=" + AD_CMPNY + " ");

		jbossQl.append("OFFSET " + OFFSET + " ");

		jbossQl.append("LIMIT " + LIMIT);

		Collection arPdcs = null;

		try {

			arPdcs = arPdcHome.getPdcByCriteria(jbossQl.toString(), obj);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

		if (arPdcs.isEmpty())
			throw new GlobalNoRecordFoundException();

		Iterator i = arPdcs.iterator();

		while (i.hasNext()) {

			LocalArPdc arPdc = (LocalArPdc) i.next();

			ArModPdcDetails mdetails = new ArModPdcDetails();
			mdetails.setPdcCode(arPdc.getPdcCode());
			mdetails.setPdcCstCustomerCode(arPdc.getArCustomer().getCstCustomerCode());
			mdetails.setPdcReferenceNumber(arPdc.getPdcReferenceNumber());
			mdetails.setPdcCheckNumber(arPdc.getPdcCheckNumber());
			mdetails.setPdcDateReceived(arPdc.getPdcDateReceived());
			mdetails.setPdcMaturityDate(arPdc.getPdcMaturityDate());
			mdetails.setPdcAmount(arPdc.getPdcAmount());

			if(!arPdc.getArInvoiceLines().isEmpty()) {

				ArrayList list = new ArrayList();
				Collection arInvLines = arPdc.getArInvoiceLines();

				Iterator itrIl = arInvLines.iterator();
				while(itrIl.hasNext()){

					LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)itrIl.next();
					list.add(arInvoiceLine);

				}

				mdetails.setPdcIlList(list);

			} else {

				ArrayList list = new ArrayList();
				Collection arInvLineItems = arPdc.getArInvoiceLineItems();

				Iterator itrIli = arInvLineItems.iterator();
				while(itrIli.hasNext()) {

					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)itrIli.next();
					list.add(arInvoiceLineItem);

				}

				mdetails.setPdcIliList(list);

			}

			pdcList.add(mdetails);

		}

		return pdcList;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void executeArPdcInvoiceGeneration(Integer PDC_CODE,
			String INV_NMBR, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalRecordAlreadyDeletedException,
			GlobalDocumentNumberNotUniqueException,
			GlobalBranchAccountNumberInvalidException {

		Debug.print("ArPdcInvoiceControllerBean executeArPdcInvoiceGeneration");

		LocalArPdcHome arPdcHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalArInvoiceBatchHome arInvoiceBatchHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalArReceiptBatchHome arReceiptBatchHome = null;
		LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;


		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");

		// Initialize EJB Home

		try {

			arPdcHome = (LocalArPdcHome)EJBHomeFactory.
			lookUpLocalHome(LocalArPdcHome.JNDI_NAME, LocalArPdcHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);



		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get post dated check
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalArPdc arPdc = null;

			try {

				arPdc = arPdcHome.findByPrimaryKey(PDC_CODE);

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}


			// if statement (invoice (ITEMS/MEMO LINE)
			if(arPdc.getArTaxCode() != null && arPdc.getArWithholdingTaxCode() != null && arPdc.getAdPaymentTerm() != null)
			{

				// validate if invoice number is unique

				try {

					LocalArInvoice arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(INV_NMBR, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

					throw new GlobalDocumentNumberNotUniqueException();

				} catch (FinderException ex) {

				}

				// generate document number if necessary

				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment =
					adDocumentSequenceAssignmentHome.findByDcName("AR INVOICE", AD_CMPNY);

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
						(INV_NMBR == null || INV_NMBR.trim().length() == 0)) {

					while (true) {

						try {

							arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
							adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

						} catch (FinderException ex) {

							INV_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
							adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
							break;

						}

					}

				}

				// generate approval status

				LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

				String INV_APPRVL_STATUS = null;

				// generate invoice

				LocalArInvoice arInvoice = arInvoiceHome.create("ITEMS",EJBCommon.FALSE,
						arPdc.getPdcDescription(), arPdc.getPdcMaturityDate(), INV_NMBR, arPdc.getPdcReferenceNumber(), null,null, null,
						arPdc.getPdcAmount(), 0d,0d, 0d,0d, 0d, arPdc.getPdcConversionDate(), arPdc.getPdcConversionRate(), null,
						0d, 0d, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
						arPdc.getPdcLvFreight(), INV_APPRVL_STATUS, null,
						EJBCommon.FALSE,
						null, EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE, null, 0d, null, null, null, null,
						USR_NM, new Date(), USR_NM, new Date(), null, null, null, null,
						EJBCommon.FALSE, arPdc.getPdcLvShift(), null, null, EJBCommon.FALSE, EJBCommon.FALSE,
						null, arPdc.getPdcEffectivityDate(), AD_BRNCH, AD_CMPNY);

				// payment term
				arPdc.getAdPaymentTerm().addArInvoice(arInvoice);
				// functional currency
				adCompany.getGlFunctionalCurrency().addArInvoice(arInvoice);
				// tax code
				arPdc.getArTaxCode().addArInvoice(arInvoice);
				// withholding tax
				arPdc.getArWithholdingTaxCode().addArInvoice(arInvoice);
				// customer
				arPdc.getArCustomer().addArInvoice(arInvoice);

				// generate invoice batch if necessary

				LocalArInvoiceBatch arInvoiceBatch = null;

				if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

					arInvoiceBatch = arInvoiceBatchHome.create("Post Dated Checks " + formatter.format(new java.util.Date()),
							"POST DATED CHECKS", "OPEN", "INVOICE", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

				}

				if (arInvoiceBatch != null) {

					arInvoiceBatch.addArInvoice(arInvoice);

				}

				// add invoice lines & distribution records

				if(!arPdc.getArInvoiceLines().isEmpty()) {

					this.addLinesDrEntry(arPdc.getArInvoiceLines(), false, arInvoice, arPdc, adPreference, AD_BRNCH, AD_CMPNY);

				} else{

					this.addLinesDrEntry(arPdc.getArInvoiceLineItems(), true, arInvoice, arPdc, adPreference, AD_BRNCH, AD_CMPNY);

				}

				// create invoice payment schedule

				short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
				double TOTAL_PAYMENT_SCHEDULE =  0d;

				LocalAdPaymentTerm adPaymentTerm = arPdc.getAdPaymentTerm();

				GregorianCalendar gcPrevDateDue = new GregorianCalendar();
	            GregorianCalendar gcDateDue = new GregorianCalendar();
	            gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

				Collection adPaymentSchedules = adPaymentTerm.getAdPaymentSchedules();

				Iterator i = adPaymentSchedules.iterator();

				while (i.hasNext()) {

					LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)i.next();

					// get date due

					if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")){

	                	gcDateDue.setTime(arInvoice.getInvEffectivityDate());
	                	gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

	                } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")){

	                	gcDateDue = gcPrevDateDue;
	                	gcDateDue.add(Calendar.MONTH, 1);
	                	gcPrevDateDue = gcDateDue;

	                } else if(arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

	                	gcDateDue = gcPrevDateDue;

	                	if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
	                		if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31 && gcPrevDateDue.get(Calendar.DATE) > 15 && gcPrevDateDue.get(Calendar.DATE) < 31){
	                			gcDateDue.add(Calendar.DATE, 16);
	                		} else {
	                    		gcDateDue.add(Calendar.DATE, 15);
	                    	}
	                	} else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
	                		if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28 && gcPrevDateDue.get(Calendar.DATE) == 14) {
	                			gcDateDue.add(Calendar.DATE, 14);
	                		} else if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28 && gcPrevDateDue.get(Calendar.DATE) >= 15 && gcPrevDateDue.get(Calendar.DATE) < 28) {
	                			gcDateDue.add(Calendar.DATE, 13);
	                		} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29 && gcPrevDateDue.get(Calendar.DATE) >= 15 && gcPrevDateDue.get(Calendar.DATE) < 29) {
	                			gcDateDue.add(Calendar.DATE, 14);
	                		} else {
	                			gcDateDue.add(Calendar.DATE, 15);
	                		}
	                	}

	                	gcPrevDateDue = gcDateDue;

	            	}

					// create a payment schedule

					double PAYMENT_SCHEDULE_AMOUNT = 0;

					// if last payment schedule subtract to avoid rounding difference error

					if (i.hasNext()) {

						PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount() / adPaymentTerm.getPytBaseAmount()) * arInvoice.getInvAmountDue(), precisionUnit);

					} else {

						PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_PAYMENT_SCHEDULE;

					}

					LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
						arInvoicePaymentScheduleHome.create(gcDateDue.getTime(),
								adPaymentSchedule.getPsLineNumber(), PAYMENT_SCHEDULE_AMOUNT,
								0d, EJBCommon.FALSE,
								(short)0, gcDateDue.getTime(), 0d, 0d,
								AD_CMPNY);

					arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);

					TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;

				}



			} else if(arPdc.getAdBankAccount() != null) {


				// check if receipt is existing
				// validate if receipt number is unique
				String RCT_NMBR = INV_NMBR;

				try {

					LocalArReceipt arExistingReceipt = arReceiptHome.findByRctNumberAndBrCode(RCT_NMBR, AD_BRNCH, AD_CMPNY);

					throw new GlobalDocumentNumberNotUniqueException();

				} catch (FinderException ex) {

				}

				// generate AR RECEIPT number
				// generate receipt number if necessary




				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment =
					adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT", AD_CMPNY);

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
						(RCT_NMBR == null || RCT_NMBR.trim().length() == 0)) {

					while (true) {

						try {

							arReceiptHome.findByRctNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
							adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

						} catch (FinderException ex) {

							RCT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
							adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
							break;

						}

					}

				}

				// generate approval status

				LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);


				// generate receipt

				LocalArReceipt arReceipt = arReceiptHome.create("COLLECTION", arPdc.getPdcDescription(), arPdc.getPdcMaturityDate(), RCT_NMBR,
						arPdc.getPdcReferenceNumber(), arPdc.getPdcCheckNumber(), null,
						null,null,null,null,null,
						arPdc.getPdcAmount(), arPdc.getPdcAmount(), 0d, 0d, 0d,  0d, 0d,
						arPdc.getPdcConversionDate(), arPdc.getPdcConversionRate(),
						null, null, EJBCommon.FALSE, 0d, null, null, EJBCommon.FALSE,
						null, EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
					    null, null, null, null, null,

						arPdc.getPdcCreatedBy(),
						arPdc.getPdcDateCreated(), arPdc.getPdcLastModifiedBy(), arPdc.getPdcDateLastModified(), null, null, null, null, EJBCommon.FALSE,
						arPdc.getPdcLvShift(), EJBCommon.FALSE, EJBCommon.FALSE,
						arPdc.getArCustomer().getCstName(), null,
						EJBCommon.FALSE, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);


				// functional currency
				adCompany.getGlFunctionalCurrency().addArReceipt(arReceipt);
				// customer
				arPdc.getArCustomer().addArReceipt(arReceipt);
				//bank account
				arPdc.getAdBankAccount().addArReceipt(arReceipt);

				// generate receipt batch if necessary

				LocalArReceiptBatch arReceiptBatch = null;

				if (adPreference.getPrfEnableArReceiptBatch() == EJBCommon.TRUE) {

					arReceiptBatch = arReceiptBatchHome.create("PDC " + formatter.format(new java.util.Date()),
							"POST DATED CHECKS", "OPEN", "INVOICE", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

				}

				if (arReceiptBatch != null) {

					arReceiptBatch.addArReceipt(arReceipt);
				}

				Collection arAppliedInvoices = arPdc.getArAppliedInvoices();

				Iterator i = arAppliedInvoices.iterator();
				// create applied invoice and distribution record (refer from ar receipt)

				while (i.hasNext()) {
					LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();
					LocalArAppliedInvoice arNewAppliedInvoice = arAppliedInvoiceHome.create(arAppliedInvoice.getAiApplyAmount(), arAppliedInvoice.getAiPenaltyApplyAmount(),
							arAppliedInvoice.getAiCreditableWTax(), arAppliedInvoice.getAiDiscountAmount(), arAppliedInvoice.getAiRebate(),
							arAppliedInvoice.getAiAppliedDeposit(), arAppliedInvoice.getAiAllocatedPaymentAmount(),
							arAppliedInvoice.getAiForexGainLoss(),
							EJBCommon.FALSE,
							AD_CMPNY);

					arReceipt.addArAppliedInvoice(arNewAppliedInvoice);
					arAppliedInvoice.getArInvoicePaymentSchedule().addArAppliedInvoice(arNewAppliedInvoice);
				}



				// create cash distribution record

		  	    if(arReceipt.getRctAmount() != 0)
		  	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE,
		  	    		arReceipt.getRctAmount(), EJBCommon.FALSE, arReceipt.getAdBankAccount().getBaCoaGlCashAccount(),
						arReceipt, null, AD_BRNCH, AD_CMPNY);

		  	    // add new applied vouchers and distribution record
		  	    arAppliedInvoices = arReceipt.getArAppliedInvoices();
				i = arAppliedInvoices.iterator();
	      	    while (i.hasNext()) {

		      	  	    LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

			      	     // create cred. withholding tax distribution record if necessary

		      	  	    if (arAppliedInvoice.getAiCreditableWTax() > 0) {

			      	  	    Integer WTC_COA_CODE = null;

			      	  	    if (arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArWithholdingTaxCode().getGlChartOfAccount() != null) {

			      	  	    	WTC_COA_CODE = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArWithholdingTaxCode().getGlChartOfAccount().getCoaCode();

			      	  	    } else {


			      	  	    	WTC_COA_CODE = adPreference.getArWithholdingTaxCode().getGlChartOfAccount().getCoaCode();

			      	  	    }

		      	  	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "W-TAX", EJBCommon.TRUE,
			      	        arAppliedInvoice.getAiCreditableWTax(), EJBCommon.FALSE,
			      	        WTC_COA_CODE, arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);

		      	  	    }

		      	  	    // create discount distribution records if necessary

		      	  	    if (arAppliedInvoice.getAiDiscountAmount() != 0) {

		      	  	    	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

		      	  	    	// get discount percent

				        	double DISCOUNT_PERCENT = EJBCommon.roundIt(arAppliedInvoice.getAiDiscountAmount() / (arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiAppliedDeposit()), (short)6);
				        	DISCOUNT_PERCENT = EJBCommon.roundIt(DISCOUNT_PERCENT * ((arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiAppliedDeposit()) / arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvAmountDue()), (short)6);

				        	Collection arDiscountDistributionRecords = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArDistributionRecords();

				        	// get total debit and credit for rounding difference calculation

				        	double TOTAL_DEBIT = 0d;
				        	double TOTAL_CREDIT = 0d;
				        	boolean isRoundingDifferenceCalculated = false;

				        	Iterator j = arDiscountDistributionRecords.iterator();

				        	while (j.hasNext()) {

				        		LocalArDistributionRecord arDiscountDistributionRecord = (LocalArDistributionRecord)j.next();

				        		if (arDiscountDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

				        			TOTAL_DEBIT += EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);

				        		} else {

				        			TOTAL_CREDIT += EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);

				        		}

				        	}


				        	j = arDiscountDistributionRecords.iterator();

				        	while (j.hasNext()) {

				        		LocalArDistributionRecord arDiscountDistributionRecord = (LocalArDistributionRecord)j.next();

				        		if (arDiscountDistributionRecord.getDrClass().equals("RECEIVABLE")) continue;

				        		double DR_AMNT = EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);

				        		// calculate rounding difference if necessary

				        		if (arDiscountDistributionRecord.getDrDebit() == EJBCommon.FALSE &&
				        		    TOTAL_DEBIT != TOTAL_CREDIT && !isRoundingDifferenceCalculated) {

				        		    DR_AMNT = DR_AMNT +  TOTAL_DEBIT - TOTAL_CREDIT;

				        		    isRoundingDifferenceCalculated = true;

				        		}

				        		if (arDiscountDistributionRecord.getDrClass().equals("REVENUE")) {

					        		this.addArDrEntry(arReceipt.getArDrNextLine(), "SALES DISCOUNT",
					        		    EJBCommon.TRUE,
					        		    DR_AMNT,
					        		    EJBCommon.FALSE,
					        		    arReceipt.getAdBankAccount().getBaCoaGlSalesDiscount(), arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);

				        		} else {


				        			this.addArDrEntry(arReceipt.getArDrNextLine(), arDiscountDistributionRecord.getDrClass(),
					        		    arDiscountDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
					        		    DR_AMNT,
					        		    EJBCommon.FALSE,
					        		    arDiscountDistributionRecord.getGlChartOfAccount().getCoaCode(), arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);

				        		}

				        	}
		      	  	    }

		      	  	    // create applied deposit distribution records if necessary

		      	  	    if (arAppliedInvoice.getAiAppliedDeposit() != 0) {


		      	  	    	if(adPreference.getPrfArGlCoaCustomerDepositAccount() == null)
		      	  	    		throw new AdPRFCoaGlCustomerDepositAccountNotFoundException();

		      	  	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "APPLIED DEPOSIT",
		      	  	    			EJBCommon.TRUE,
									arAppliedInvoice.getAiAppliedDeposit(),
									EJBCommon.FALSE,
									adPreference.getPrfArGlCoaCustomerDepositAccount(), arReceipt,
									arAppliedInvoice, AD_BRNCH, AD_CMPNY);

		      	  	    }

		      	  	    // get receivable account

			  	  	    LocalArDistributionRecord arDistributionRecord =
			  	  	        arDistributionRecordHome.findByDrClassAndInvCode("RECEIVABLE", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);

			  	  	    this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE", EJBCommon.FALSE,
			  	  	        arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiAppliedDeposit(),
			  	  	        EJBCommon.FALSE,
			  	  	        arDistributionRecord.getGlChartOfAccount().getCoaCode(),
			  	  	        arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);


			  	  	    // reverse deferred tax if necessary

			  	  	    LocalArTaxCode arTaxCode = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArTaxCode();

			  	  	    if (!arTaxCode.getTcType().equals("NONE") &&
			  	  	        !arTaxCode.getTcType().equals("EXEMPT") &&
							arTaxCode.getTcInterimAccount() != null) {

			  	  	    	try {

			  	  	    		LocalArDistributionRecord arDeferredDistributionRecord =
			  	  	    			arDistributionRecordHome.findByDrClassAndInvCode("DEFERRED TAX", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);

			  	  	    		double DR_AMNT =
			  	  	    			EJBCommon.roundIt((arDeferredDistributionRecord.getDrAmount() / arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvAmountDue()) *
			  	  	    					(arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax()), this.getGlFcPrecisionUnit(AD_CMPNY));

			  	  	    		this.addArDrEntry(arReceipt.getArDrNextLine(), "DEFERRED TAX", EJBCommon.TRUE,
			  	  	    				DR_AMNT, EJBCommon.FALSE,
										arDeferredDistributionRecord.getGlChartOfAccount().getCoaCode(),
										arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);



			  	  	    		this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE,
			  	  	    				DR_AMNT, EJBCommon.FALSE,
										arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArTaxCode().getGlChartOfAccount().getCoaCode(),
										arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);



			  	  	    	} catch (FinderException ex) {

			  	  	    	}

			  	  	    }

		      	    }


			}

			arPdc.setPdcStatus("CLEARED");

		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ArPdcInvoiceGenerationControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ArPdcInvoiceGenerationControllerBean getInvGpQuantityPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvQuantityPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {

    	Debug.print("ArPdcInvoiceGenerationControllerBean getAdPrfArUseCustomerPulldown");

    	LocalAdPreferenceHome adPreferenceHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}


    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

    		return adPreference.getPrfArUseCustomerPulldown();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

	// private method

	private void addLinesDrEntry(Collection arPdcLines, boolean isLineItems,  LocalArInvoice arInvoice, LocalArPdc arPdc,
			LocalAdPreference adPreference, Integer AD_BRNCH, Integer AD_CMPNY)
			throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArInvoiceGenerationControllerBean addIlDrEntry");

		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
		LocalAdDiscountHome adDiscountHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalInvItemHome invItemHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdBranchCustomerHome adBranchCustomerHome = null;

		// Initialize EJB Home

		try {

			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
			adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adBranchCustomerHome = (LocalAdBranchCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			double TOTAL_TAX = 0d;
			double TOTAL_LINE = 0d;

			// invoice line items

			if(isLineItems) {

				Iterator i = arPdcLines.iterator();

				while (i.hasNext()) {

					LocalArInvoiceLineItem arPdcInvoiceLineItem = (LocalArInvoiceLineItem) i.next();

					LocalInvItemLocation invItemLocation = arPdcInvoiceLineItem.getInvItemLocation();

					// line

					LocalArInvoiceLineItem arNewInvoiceLineItem = this.addArIliEntry(arPdcInvoiceLineItem, arInvoice, invItemLocation, AD_CMPNY);

					// add cost of sales distribution and inventory

					double COST = 0d;

					try {

						LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
								arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

						COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

					} catch (FinderException ex) {

						COST = arNewInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

					}

					double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arNewInvoiceLineItem.getInvUnitOfMeasure(),
							arNewInvoiceLineItem.getInvItemLocation().getInvItem(), arNewInvoiceLineItem.getIliQuantity(), AD_CMPNY);

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try {

						adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arNewInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

					} catch(FinderException ex) {

					}

					if (arNewInvoiceLineItem.getIliEnableAutoBuild() == 0 || arNewInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

						if(adBranchItemLocation != null) {

							// Use AdBranchItemLocation Coa
							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
									"COGS", EJBCommon.TRUE, COST * QTY_SLD,
									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
									"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
									adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

						} else {

							// Use default Coa
							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
									"COGS", EJBCommon.TRUE, COST * QTY_SLD,
									arNewInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
									"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
									arNewInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

						}
						// add quantity to item location committed quantity

						double convertedQuantity = this.convertByUomFromAndItemAndQuantity(arNewInvoiceLineItem.getInvUnitOfMeasure(),
								arNewInvoiceLineItem.getInvItemLocation().getInvItem(),
								arNewInvoiceLineItem.getIliQuantity(), AD_CMPNY);
						invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

					}

					// add inventory sale distributions
					if(adBranchItemLocation != null) {

						// Use AdBranchItemLocation Coa
						this.addArDrIliEntry(arInvoice.getArDrNextLine(),
								"REVENUE", EJBCommon.FALSE, arNewInvoiceLineItem.getIliAmount(),
								adBranchItemLocation.getBilCoaGlSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

					} else {

						// Use default
						this.addArDrIliEntry(arInvoice.getArDrNextLine(),
								"REVENUE", EJBCommon.FALSE, arNewInvoiceLineItem.getIliAmount(),
								arNewInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

					}

					TOTAL_LINE += arNewInvoiceLineItem.getIliAmount();
					TOTAL_TAX += arNewInvoiceLineItem.getIliTaxAmount();

					// if auto build is enable

					if (arNewInvoiceLineItem.getIliEnableAutoBuild() == 1 && arNewInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

						byte DEBIT = EJBCommon.TRUE;

						double TOTAL_AMOUNT = 0;
						double ABS_TOTAL_AMOUNT = 0;

						LocalGlChartOfAccount glInventoryChartOfAccount = null;

						if(adBranchItemLocation != null) {

							// Use AdBranchItemLocation Coa
							glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
									adBranchItemLocation.getBilCoaGlInventoryAccount());

						} else {

							// Use default
							glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
									arNewInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount());

						}
						Collection invBillOfMaterials = arNewInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

						Iterator j = invBillOfMaterials.iterator();

						while (j.hasNext()) {

							LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

							// add bill of material quantity needed to item location

							// get raw material
							invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

							// bom conversion
                            double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                                    invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
                                    EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * QTY_SLD,
                                    this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);

							invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

							LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

							double COSTING = 0d;

							try {

								LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(invItem.getIiName(),
										arNewInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

								COSTING = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

							} catch (FinderException ex) {

								COSTING = invItem.getIiUnitCost();

							}

							// bom conversion
							COSTING = this.convertCostByUom(invItemLocation.getInvItem().getIiName(),
                                    invBillOfMaterial.getInvUnitOfMeasure().getUomName(), COSTING, true, AD_CMPNY);

							double BOM_AMOUNT = EJBCommon.roundIt(
									(QTY_SLD * (invBillOfMaterial.getBomQuantityNeeded() * COSTING)),
									this.getGlFcPrecisionUnit(AD_CMPNY));

							TOTAL_AMOUNT += BOM_AMOUNT;
							ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

							// add inventory account

							this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(BOM_AMOUNT),
									glInventoryChartOfAccount.getCoaCode(), arInvoice, AD_BRNCH, AD_CMPNY);

						}

						// add cost of sales account

						if(adBranchItemLocation != null) {

							// Use AdBranchItemLocation Account
							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
									"COGS", EJBCommon.TRUE, Math.abs(TOTAL_AMOUNT),
									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

						} else {

							// Use default account
							this.addArDrIliEntry(arInvoice.getArDrNextLine(),
									"COGS", EJBCommon.TRUE, Math.abs(TOTAL_AMOUNT),
									arNewInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

						}

					}

				}

				// add tax distribution if necessary

				if (!arPdc.getArTaxCode().getTcType().equals("NONE") &&
						!arPdc.getArTaxCode().getTcType().equals("EXEMPT")) {

					if (arPdc.getArTaxCode().getTcInterimAccount() == null) {

						this.addArDrEntry(arInvoice.getArDrNextLine(),
								"TAX", EJBCommon.FALSE, TOTAL_TAX, arPdc.getArTaxCode().getGlChartOfAccount().getCoaCode(),
								arInvoice, AD_BRNCH, AD_CMPNY);

					} else {

						this.addArDrEntry(arInvoice.getArDrNextLine(),
								"DEFERRED TAX", EJBCommon.FALSE, TOTAL_TAX, arPdc.getArTaxCode().getTcInterimAccount(),
								arInvoice, AD_BRNCH, AD_CMPNY);

					}

				}

				// add wtax distribution if necessary

				double W_TAX_AMOUNT = 0d;

				if (arPdc.getArWithholdingTaxCode().getWtcRate() != 0 && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

					W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (arPdc.getArWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

					this.addArDrEntry(arInvoice.getArDrNextLine(), "W-TAX",
							EJBCommon.TRUE, W_TAX_AMOUNT, arPdc.getArWithholdingTaxCode().getGlChartOfAccount().getCoaCode(),
							arInvoice, AD_BRNCH, AD_CMPNY);

				}

				// add payment discount if necessary

				double DISCOUNT_AMT = 0d;

				if (arPdc.getAdPaymentTerm().getPytDiscountOnInvoice() == EJBCommon.TRUE) {

					Collection adPaymentSchedules = adPaymentScheduleHome.findByPytCode(arPdc.getAdPaymentTerm().getPytCode(),AD_CMPNY);
					ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
					LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)adPaymentScheduleList.get(0);

					Collection adDiscounts = adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
					ArrayList adDiscountList = new ArrayList(adDiscounts);
					LocalAdDiscount adDiscount = (LocalAdDiscount)adDiscountList.get(0);

					double rate = adDiscount.getDscDiscountPercent();
					DISCOUNT_AMT = (TOTAL_LINE + TOTAL_TAX) * (rate / 100d);

					this.addArDrEntry(arInvoice.getArDrNextLine(), "DISCOUNT",
							EJBCommon.TRUE, DISCOUNT_AMT,
							arPdc.getAdPaymentTerm().getGlChartOfAccount().getCoaCode(),
							arInvoice, AD_BRNCH, AD_CMPNY);

				}

				// add receivable distribution

				LocalAdBranchCustomer adBranchCustomer = null;

				try {

					adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

				} catch(FinderException ex) {

				}

				if (adBranchCustomer != null) {

					// Use AdBranchCustomer Account
					this.addArDrIliEntry(arInvoice.getArDrNextLine(), "RECEIVABLE",
							EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT,
							adBranchCustomer.getBcstGlCoaReceivableAccount(),
							arInvoice, AD_BRNCH, AD_CMPNY);

				} else {

					// Use default Account
					this.addArDrIliEntry(arInvoice.getArDrNextLine(), "RECEIVABLE",
							EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT,
							arInvoice.getArCustomer().getCstGlCoaReceivableAccount(),
							arInvoice, AD_BRNCH, AD_CMPNY);

				}

				// set invoice amount due

				arInvoice.setInvAmountDue(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT);

				// invoice lines

			} else {

				Iterator i = arPdcLines.iterator();

				while (i.hasNext()) {

					LocalArInvoiceLine arPdcInvoiceLine = (LocalArInvoiceLine) i.next();

					// line

					LocalArInvoiceLine arNewInvoiceLine = this.addArIlEntry(arPdcInvoiceLine, arInvoice, AD_CMPNY);

					// add revenue/credit distributions

					this.addArDrEntry(arInvoice.getArDrNextLine(),
							"REVENUE", EJBCommon.FALSE, arNewInvoiceLine.getIlAmount(),
							this.getArGlCoaRevenueAccount(arNewInvoiceLine, AD_BRNCH, AD_CMPNY), arInvoice, AD_BRNCH, AD_CMPNY);

					TOTAL_LINE += arNewInvoiceLine.getIlAmount();
					TOTAL_TAX += arNewInvoiceLine.getIlTaxAmount();

				}


				// add tax distribution if necessary

				if (!arPdc.getArTaxCode().getTcType().equals("NONE") &&
						!arPdc.getArTaxCode().getTcType().equals("EXEMPT")) {

					if (arPdc.getArTaxCode().getTcInterimAccount() == null) {

						this.addArDrEntry(arInvoice.getArDrNextLine(),
								"TAX", EJBCommon.FALSE, TOTAL_TAX, arPdc.getArTaxCode().getGlChartOfAccount().getCoaCode(),
								arInvoice, AD_BRNCH, AD_CMPNY);

					} else {

						this.addArDrEntry(arInvoice.getArDrNextLine(),
								"DEFERRED TAX", EJBCommon.FALSE, TOTAL_TAX, arPdc.getArTaxCode().getTcInterimAccount(),
								arInvoice, AD_BRNCH, AD_CMPNY);

					}

				}

				// add wtax distribution if necessary

				double W_TAX_AMOUNT = 0d;

				if (arPdc.getArWithholdingTaxCode().getWtcRate() != 0 && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

					W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (arPdc.getArWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

					this.addArDrEntry(arInvoice.getArDrNextLine(), "W-TAX",
							EJBCommon.TRUE, W_TAX_AMOUNT, arPdc.getArWithholdingTaxCode().getGlChartOfAccount().getCoaCode(),
							arInvoice, AD_BRNCH, AD_CMPNY);

				}

				// add payment discount if necessary

				double DISCOUNT_AMT = 0d;

				if (arPdc.getAdPaymentTerm().getPytDiscountOnInvoice() == EJBCommon.TRUE) {

					Collection adPaymentSchedules = adPaymentScheduleHome.findByPytCode(arPdc.getAdPaymentTerm().getPytCode(),AD_CMPNY);
					ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
					LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)adPaymentScheduleList.get(0);

					Collection adDiscounts = adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
					ArrayList adDiscountList = new ArrayList(adDiscounts);
					LocalAdDiscount adDiscount = (LocalAdDiscount)adDiscountList.get(0);


					double rate = adDiscount.getDscDiscountPercent();
					DISCOUNT_AMT = (TOTAL_LINE + TOTAL_TAX) * (rate / 100d);

					this.addArDrIliEntry(arInvoice.getArDrNextLine(), "DISCOUNT",
							EJBCommon.TRUE, DISCOUNT_AMT,
							arPdc.getAdPaymentTerm().getGlChartOfAccount().getCoaCode(),
							arInvoice, AD_BRNCH, AD_CMPNY);

				}

				// add receivable distribution

				LocalAdBranchCustomer adBranchCustomer = null;

				try {

					adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

				} catch(FinderException ex) {

				}

				if (adBranchCustomer != null) {

					// Use AdBranchCustomer Account
					this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE",
							EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT,
							adBranchCustomer.getBcstGlCoaReceivableAccount(),
							arInvoice, AD_BRNCH, AD_CMPNY);

				} else {

					// Use default account
					this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE",
							EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT,
							arInvoice.getArCustomer().getCstGlCoaReceivableAccount(),
							arInvoice, AD_BRNCH, AD_CMPNY);

				}

				// set invoice amount due

				arInvoice.setInvAmountDue(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT);

			}

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private LocalArInvoiceLine addArIlEntry(LocalArInvoiceLine arPdcInvoiceLine, LocalArInvoice arInvoice, Integer AD_CMPNY) {

		Debug.print("ArInvoiceGenerationControllerBean addArIlEntry");

		LocalArInvoiceLineHome arInvoiceLineHome = null;
		LocalArStandardMemoLineHome arStandardMemoLineHome = null;


		// Initialize EJB Home

		try {

			arInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
			arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArInvoiceLine arNewInvoiceLine = arInvoiceLineHome.create(
					arPdcInvoiceLine.getIlDescription(), arPdcInvoiceLine.getIlQuantity(),
					arPdcInvoiceLine.getIlUnitPrice(), arPdcInvoiceLine.getIlAmount(),
					arPdcInvoiceLine.getIlTaxAmount(), arPdcInvoiceLine.getIlTax(), AD_CMPNY);

			arInvoice.addArInvoiceLine(arNewInvoiceLine);

			arPdcInvoiceLine.getArStandardMemoLine().addArInvoiceLine(arNewInvoiceLine);

			return arNewInvoiceLine;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}


	private void addArDrEntry(short DR_LN, String DR_CLSS,
		    byte DR_DBT, double DR_AMNT, byte DR_RVRSD, Integer COA_CODE, LocalArReceipt arReceipt,
		    LocalArAppliedInvoice arAppliedInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
			throws GlobalBranchAccountNumberInvalidException {

			Debug.print("ArReceiptEntryControllerBean addArDrEntry");

			LocalArDistributionRecordHome arDistributionRecordHome = null;
			LocalGlChartOfAccountHome glChartOfAccountHome = null;
			LocalAdCompanyHome adCompanyHome = null;


	        // Initialize EJB Home

	        try {

	            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
	                lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
	            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


	        } catch (NamingException ex) {

	            throw new EJBException(ex.getMessage());

	        }

	        try {

	        	// get company

	        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	        	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

			    // create distribution record

			    LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
				    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
				    EJBCommon.FALSE, DR_RVRSD, AD_CMPNY);

				arReceipt.addArDistributionRecord(arDistributionRecord);
				glChartOfAccount.addArDistributionRecord(arDistributionRecord);

				// to be used by gl journal interface for cross currency receipts
				if (arAppliedInvoice != null) {

					arAppliedInvoice.addArDistributionRecord(arDistributionRecord);

				}

	        } catch (FinderException ex) {

	    		throw new GlobalBranchAccountNumberInvalidException();

	    	} catch (Exception ex) {

	        	Debug.printStackTrace(ex);
	        	getSessionContext().setRollbackOnly();
	        	throw new EJBException(ex.getMessage());

	        }

		}


	private void addArDrEntry(short DR_LN, String DR_CLSS,
			byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
			throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArInvoiceGenerationControllerBean addArDrEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

			// create distribution record

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			arInvoice.addArDistributionRecord(arDistributionRecord);
			glChartOfAccount.addArDistributionRecord(arDistributionRecord);

		} catch (FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private LocalArInvoiceLineItem addArIliEntry(LocalArInvoiceLineItem arPdcInvoiceLineItem, LocalArInvoice arInvoice, LocalInvItemLocation invItemLocation, Integer AD_CMPNY) {

		Debug.print("ArInvoiceGenerationControllerBean addArIliEntry");

		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

		// Initialize EJB Home

		try {

			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArInvoiceLineItem arNewInvoiceLineItem = arInvoiceLineItemHome.create(
					arPdcInvoiceLineItem.getIliLine(), arPdcInvoiceLineItem.getIliQuantity(), arPdcInvoiceLineItem.getIliUnitPrice(),
					arPdcInvoiceLineItem.getIliAmount(), arPdcInvoiceLineItem.getIliTaxAmount(), arPdcInvoiceLineItem.getIliEnableAutoBuild(),
					arPdcInvoiceLineItem.getIliDiscount1(), arPdcInvoiceLineItem.getIliDiscount2(), arPdcInvoiceLineItem.getIliDiscount3(),
					arPdcInvoiceLineItem.getIliDiscount4(), arPdcInvoiceLineItem.getIliTotalDiscount(), arPdcInvoiceLineItem.getIliTax(), AD_CMPNY);

			arInvoice.addArInvoiceLineItem(arNewInvoiceLineItem);

			invItemLocation.addArInvoiceLineItem(arNewInvoiceLineItem);

			arPdcInvoiceLineItem.getInvUnitOfMeasure().addArInvoiceLineItem(arNewInvoiceLineItem);

			return arNewInvoiceLineItem;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addArDrIliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
		throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArInvoiceGenerationControllerBean addArDrIliEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

			// create distribution record

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			arInvoice.addArDistributionRecord(arDistributionRecord);
			glChartOfAccount.addArDistributionRecord(arDistributionRecord);

		} catch (FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private Integer getArGlCoaRevenueAccount(LocalArInvoiceLine arInvoiceLine, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ArPdcInvoiceGenerationControllerBean getArGlCoaRevenueAccount");

		LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdBranchCustomerHome adBranchCustomerHome = null;

		// Initialize EJB Home

		try {

			arAutoAccountingSegmentHome = (LocalArAutoAccountingSegmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adBranchCustomerHome = (LocalAdBranchCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		// generate revenue account

		try {

			String GL_COA_ACCNT = "";

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGenField genField = adCompany.getGenField();

			String FL_SGMNT_SPRTR = String.valueOf(genField.getFlSegmentSeparator());


			Collection arAutoAccountingSegments = arAutoAccountingSegmentHome.findByAaAccountType("REVENUE", AD_CMPNY);

			Iterator i = arAutoAccountingSegments.iterator();

			while (i.hasNext()) {

				LocalArAutoAccountingSegment arAutoAccountingSegment =
					(LocalArAutoAccountingSegment) i.next();

				LocalGlChartOfAccount glChartOfAccount = null;

				if (arAutoAccountingSegment.getAasClassType().equals("AR CUSTOMER")) {

					LocalAdBranchCustomer adBranchCustomer = null;

					try {

						adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(arInvoiceLine.getArInvoice().getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

					} catch(FinderException ex) {

					}

					if (adBranchCustomer != null) {

						// Use AdBranchCustomer Receivable Account
						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
								adBranchCustomer.getBcstGlCoaRevenueAccount());

					} else {

						// Use default Receivable Account
						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
								arInvoiceLine.getArInvoice().getArCustomer().getCstGlCoaRevenueAccount());

					}
					StringTokenizer st = new StringTokenizer(
							glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

					int ctr = 0;
					while (st.hasMoreTokens()) {

						++ctr;

						if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

							GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR +
							st.nextToken();

							break;

						} else {

							st.nextToken();

						}
					}


				} else if (arAutoAccountingSegment.getAasClassType().equals("AR STANDARD MEMO LINE")) {

					glChartOfAccount = arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount();

					StringTokenizer st = new StringTokenizer(
							glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

					int ctr = 0;
					while (st.hasMoreTokens()) {

						++ctr;

						if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

							GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR +
							st.nextToken();

							break;

						} else {

							st.nextToken();

						}
					}

				}
			}

			GL_COA_ACCNT = GL_COA_ACCNT.substring(1, GL_COA_ACCNT.length());

			try {

				LocalGlChartOfAccount glGeneratedChartOfAccount =
					glChartOfAccountHome.findByCoaAccountNumber(GL_COA_ACCNT, AD_CMPNY);

				return glGeneratedChartOfAccount.getCoaCode();

			} catch (FinderException ex) {

				return arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount().getCoaCode();

			}


		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_SLD, Integer AD_CMPNY) {

		Debug.print("ArPdcInvoiceGenerationControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			    lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(QTY_SLD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean convertCostByUom");

        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            if (isFromDefault) {

                return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();

            } else {

                return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();

            }

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ArPdcInvoiceGenerationControllerBean ejbCreate");

	}

}
