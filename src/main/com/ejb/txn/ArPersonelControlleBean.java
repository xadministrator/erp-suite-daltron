   
/*
 * ArPersonelConrollerBea.java
 *
 * Created on December 19, 2018, 10:40 PM
 *
 * @author  Ruben P. Lamberte
 */


package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchSalespersonHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArPersonel;
import com.ejb.ar.LocalArPersonelHome;
import com.ejb.ar.LocalArPersonelType;
import com.ejb.ar.LocalArPersonelTypeHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import com.util.AbstractSessionBean;
import com.util.ArModPersonelDetails;
import com.util.ArPersonelDetails;
import com.util.ArPersonelTypeDetails;
import com.util.ArSalespersonDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArPersonelControllerEJB"
 *           display-name="Used for entering personel"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArPersonelControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArPersonelController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArPersonelControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
*/
public class ArPersonelControlleBean extends AbstractSessionBean{

	
	 /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArModPersonelDetails getArPeByPeCode(Integer PE_CODE)
        throws GlobalNoRecordFoundException {
    	
    	 Debug.print("ArPersonelControlleBean getArPrlAll");

         LocalArPersonelHome arPersonelHome = null;
         LocalArPersonel arPersonel = null;


         // Initialize EJB Home

         try {

         	arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }
    	
    	try {
    		
    		try {
    			arPersonel = arPersonelHome.findByPrimaryKey(PE_CODE);
    		}catch(FinderException ex) {
    			throw new GlobalNoRecordFoundException();
    		}
    		
    	
    		
    		
    		ArModPersonelDetails details = new ArModPersonelDetails();
    		
    		
    		details.setPeCode(arPersonel.getPeCode());
    		details.setPeIdNumber(arPersonel.getPeIdNumber());
    		details.setPeName(arPersonel.getPeName());
    		details.setPeDescription(arPersonel.getPeDescription());
    		details.setPeAddress(arPersonel.getPeAddress());
    		details.setPePtName(arPersonel.getArPersonelType().getPtName());
    		details.setPePtShortName(arPersonel.getArPersonelType().getPtShortName());
    		details.setPeRate(arPersonel.getPeRate());
    		
    		
    		return details;
    		
    	}  catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }
    	
    	
    	
    }
	
	
	

	 /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArPeAll(Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArPersonelControlleBean getArPrlAll");

        LocalArPersonelHome arPersonelHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	
	        Collection arSalespersons = arPersonelHome.findPeAll(AD_CMPNY);

	        if (arSalespersons.isEmpty()) {

	            throw new GlobalNoRecordFoundException();

	        }

	        Iterator i = arSalespersons.iterator();

	        while (i.hasNext()) {

	        	LocalArPersonel arPersonel = (LocalArPersonel)i.next();

	        	ArModPersonelDetails details = new ArModPersonelDetails();
	        	
	        	details.setPeCode(arPersonel.getPeCode());
	        	details.setPeIdNumber(arPersonel.getPeIdNumber());
	        	
	        	details.setPeName(arPersonel.getPeName());
	        	details.setPeDescription(arPersonel.getPeDescription());
	        	details.setPeAddress(arPersonel.getPeAddress());
	        	details.setPePtName(arPersonel.getArPersonelType().getPtName());
	        	details.setPeAdCompany(AD_CMPNY);
	        	details.setPeRate(arPersonel.getPeRate());

	        		list.add(details);

	        }

	        return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @throws GlobalRecordAlreadyExistException 
     * @ejb:interface-method view-type="remote"
     **/    
    public void addArPeEntry(com.util.ArModPersonelDetails details, Integer AD_CMPNY) throws GlobalRecordAlreadyExistException {
    	
    	 Debug.print("ArPersonelController addArPeEntry");
    	
    	 LocalArPersonelHome arPersonelHome = null;
    	 LocalArPersonelTypeHome arPersonelTypeHome = null;
    	 ArrayList list = new ArrayList();
    	 
    	 try {
             
    		 arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);
    		 
    		 
    		 arPersonelTypeHome = (LocalArPersonelTypeHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArPersonelTypeHome.JNDI_NAME, LocalArPersonelTypeHome.class);
    		
                                          
         } catch (NamingException ex) {
             
             throw new EJBException(ex.getMessage());
             
         }
    	 
    	 
    	 try {
    		 
    		 LocalArPersonel arPersonel = null;
    		 
    		 try {
    			 
    			 arPersonel = arPersonelHome.findByPeIdNumber(details.getPeIdNumber(), AD_CMPNY);
    			 throw new GlobalRecordAlreadyExistException();
    			 
    		 } catch(FinderException ex) {
    			  
    		 }
    		 
    		 
    		 try {
    			 
    			 arPersonel = arPersonelHome.findByPeName(details.getPeName(), AD_CMPNY);
    			 throw new GlobalRecordAlreadyExistException();
    			 
    		 } catch(FinderException ex) {
    			  
    		 }
    		 
    		 
    		 arPersonel = arPersonelHome.create(details.getPeIdNumber(), details.getPeName(), details.getPeDescription(), details.getPeAddress(), details.getPeRate(), AD_CMPNY);
    		 
    		 LocalArPersonelType arPersonelType = arPersonelTypeHome.findByPtName(details.getPePtName(), AD_CMPNY);
			 
     		
    		 
    		 
			 arPersonel.setArPersonelType(arPersonelType);
    		 
    		 
    		 
    	 } catch (GlobalRecordAlreadyExistException ex) {
         	
         	getSessionContext().setRollbackOnly();
         	throw ex;
         	
         } catch (Exception ex) {
         	
         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());
         	
         }
             
         
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateArPeEntry(com.util.ArModPersonelDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
    	
    	Debug.print("ArPersonelController updateArPeEntry");
    	
    	LocalArPersonelHome arPersonelHome = null;
     	LocalArPersonelTypeHome arPersonelTypeHome = null;
      	
    	try {
            
    		 arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);
          
    		 arPersonelTypeHome = (LocalArPersonelTypeHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArPersonelTypeHome.JNDI_NAME, LocalArPersonelTypeHome.class);
    		                 
         } catch (NamingException ex) {
             
             throw new EJBException(ex.getMessage());
             
         }
    	System.out.println("pasok 1");
    	
    	 try {
    		 LocalArPersonel arPersonel = null;
    		 
    		 try {
    		      	
	 
    			arPersonel = arPersonelHome.findByPrimaryKey(details.getPeCode());
    			 
	
 	        } catch (FinderException ex) {
 	        	  throw new GlobalRecordAlreadyExistException();
 	        
 	        	
 	        }
    		 System.out.println("pasok 2");
    		 
    		 try {
 		      	
    			 LocalArPersonel arExistingPersonel = arPersonelHome.findByPeIdNumber(details.getPeIdNumber(), AD_CMPNY);
    			 
    			 if(!arExistingPersonel.getPeCode().equals(arPersonel.getPeCode())) {
    				 
    				 
    			      throw new GlobalRecordAlreadyExistException(details.getPeName());
    				 
    				 
    			 }
    			 
    			 
    			 arExistingPersonel = arPersonelHome.findByPeName(details.getPeName(), AD_CMPNY);
    			 
    			 if(!arExistingPersonel.getPeCode().equals(arPersonel.getPeCode())) {
    				 
    				 
   			      	throw new GlobalRecordAlreadyExistException(details.getPeName());
   				 
   				 
    			 }
    			 
    			 
    		
 	        } catch (FinderException ex) {
 	        	
 	        
 	        	
 	        }
    		 
    		 
    		 System.out.println("pasok 3");
    		 
    		 
    		 
    		 
    		
    		 
    		 arPersonel = arPersonelHome.findByPrimaryKey(details.getPeCode());
    		 arPersonel.setPeIdNumber(details.getPeIdNumber());
    		 arPersonel.setPeName(details.getPeName());
    		 arPersonel.setPeDescription(details.getPeDescription());
    		 arPersonel.setPeAddress(details.getPeAddress());
    		 arPersonel.setPeRate(details.getPeRate());
    		 System.out.println("pasok 4");
    		 LocalArPersonelType arPersonelType = arPersonelTypeHome.findByPtName(details.getPePtName(), AD_CMPNY);
			 
    		 System.out.println("pasok 1");
			 arPersonel.setArPersonelType(arPersonelType);

    		 
    	 } catch (GlobalRecordAlreadyExistException ex) {
          	
          	getSessionContext().setRollbackOnly();
          	throw ex;
          	
         } catch (Exception ex) {
          	
          	Debug.printStackTrace(ex);
          	throw new EJBException(ex.getMessage());
          	
         }
    	
    	
    	
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
     public void deleteArPeEntry(Integer PRL_CODE, Integer AD_CMPNY) 
         throws GlobalRecordAlreadyDeletedException,
 			   GlobalRecordAlreadyAssignedException {
    	 
    	  Debug.print("ArPersonelController deleteArPeEntry");
    	 
    	  LocalArPersonelHome arPersonelHome = null;
     	 
     	
     	  try {
              
     		 arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
                  lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);
           
                                           
          } catch (NamingException ex) {
              
              throw new EJBException(ex.getMessage());
              
          }
     	  
     	  
     	  try {
    		 
    		 LocalArPersonel arPersonel = null;
    		 
    		 try {
    		      	
    			 arPersonel = arPersonelHome.findByPrimaryKey(PRL_CODE);
 	        	
 	        } catch (FinderException ex) {
 	        	
 	        	throw new GlobalRecordAlreadyDeletedException();
 	        	
 	        }
 	        
    		 //Assigned Exception not in ready
    		 /*
 	        if (!arSalesperson.getArInvoices().isEmpty() || 
 	                !arSalesperson.getArReceipts().isEmpty()||
 	        		  !arSalesperson.getArCustomers().isEmpty()||
 	        		  !arSalesperson.getArSalesOrders().isEmpty()) {
 	        	
 	        	throw new GlobalRecordAlreadyAssignedException();
 	        	
 	        }
 	        */
    		 arPersonel.remove();
    		 
    		 
    		 
    		 
     	 } catch (GlobalRecordAlreadyDeletedException ex) {
         	
         	getSessionContext().setRollbackOnly();
         	throw ex;
         
      //   } catch (GlobalRecordAlreadyAssignedException ex) {
         	
      //   	getSessionContext().setRollbackOnly();
     //    	throw ex;
         	
         } catch (Exception ex) {
         	
         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());
         	
         }
     }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("ArPersonelController getGlFcPrecisionUnit");

       
        LocalAdCompanyHome adCompanyHome = null;
       
      
        // Initialize EJB Home
         
        try {
             
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
             
        } catch (NamingException ex) {
             
            throw new EJBException(ex.getMessage());
             
        }

        try {
        	
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             
          return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                      
        } catch (Exception ex) {
        	 
        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
          
        }

     } 
     
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public double getPersonelTypeRateByPersonelTypeName(String PT_NM, Integer AD_CMPNY)
         throws GlobalNoRecordFoundException {

         Debug.print("ArPersonelControlleBean getPersonelTypeRateByPersonelTypeName");

         LocalArPersonelTypeHome arPersonelTypeHome = null;

         ArrayList list = new ArrayList();

         // Initialize EJB Home

         try {

         	arPersonelTypeHome = (LocalArPersonelTypeHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArPersonelTypeHome.JNDI_NAME, LocalArPersonelTypeHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {
         	
 	        LocalArPersonelType arPersonelType = arPersonelTypeHome.findByPtName(PT_NM, AD_CMPNY);

 	      
 	        if(arPersonelType!=null) {
 	        	
 	        	return arPersonelType.getPtRate();
 	        	
 	        }else {
 	        	return 0;
 	        }

         } catch (FinderException ex) {

         	return 0;

         } catch (Exception ex) {

         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());

         }

     }
     
     
     
     
     
     
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArrayList getArPtAll(Integer AD_CMPNY)
         throws GlobalNoRecordFoundException {

         Debug.print("ArPersonelControlleBean getArPtAll");

         LocalArPersonelTypeHome arPersonelTypeHome = null;

         ArrayList list = new ArrayList();

         // Initialize EJB Home

         try {

         	arPersonelTypeHome = (LocalArPersonelTypeHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArPersonelTypeHome.JNDI_NAME, LocalArPersonelTypeHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {
         	
 	        Collection arPersonelTypes = arPersonelTypeHome.findPtAll(AD_CMPNY);

 	        if (arPersonelTypes.isEmpty()) {

 	            throw new GlobalNoRecordFoundException();

 	        }

 	        Iterator i = arPersonelTypes.iterator();

 	        while (i.hasNext()) {

 	        	LocalArPersonelType arPersonelType = (LocalArPersonelType)i.next();

 	        	ArPersonelTypeDetails details = new ArPersonelTypeDetails(arPersonelType.getPtCode(),
 	        			arPersonelType.getPtShortName(), arPersonelType.getPtName(),
 	        			arPersonelType.getPtDescription(), arPersonelType.getPtRate(),
 					     AD_CMPNY);

        		list.add(details);

 	        }

 	        return list;

         } catch (GlobalNoRecordFoundException ex) {

         	throw ex;

         } catch (Exception ex) {

         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());

         }

     }



}
