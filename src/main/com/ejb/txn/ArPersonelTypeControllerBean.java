package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArPersonelType;
import com.ejb.ar.LocalArPersonelTypeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.ArModCustomerTypeDetails;
import com.util.ArModPersonelTypeDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArPersonelTypeControllerEJB"
 *           display-name="Used for entering customer types"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArPersonelTypeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArPersonelTypeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArPersonelTypeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArPersonelTypeControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArPtAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArPersonelTypeControllerBean getArCtAll");

        LocalArPersonelTypeHome ArPersonelTypeHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {

            ArPersonelTypeHome = (LocalArPersonelTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPersonelTypeHome.JNDI_NAME, LocalArPersonelTypeHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);                

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	        	
	        Collection ArPersonelTypes = ArPersonelTypeHome.findPtAll(AD_CMPNY);
	
	        if (ArPersonelTypes.isEmpty()) {
	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	            
	        Iterator i = ArPersonelTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArPersonelType arPersonelType = (LocalArPersonelType)i.next();
	        	
	        	ArModPersonelTypeDetails mdetails = new ArModPersonelTypeDetails();
	        	
	        	
	        	mdetails.setPtCode(arPersonelType.getPtCode());
	        	mdetails.setPtShortName(arPersonelType.getPtShortName());
	        	mdetails.setPtName(arPersonelType.getPtName());
	        	mdetails.setPtDescription(arPersonelType.getPtDescription());
	        	mdetails.setPtRate(arPersonelType.getPtRate());
	        	mdetails.setPtAdCompany(arPersonelType.getPtAdCompany());
	        	                       
	                	                	                
        		list.add(mdetails);
	        		
	        }              
	                                                        		        		        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
   
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addArPtEntry(com.util.ArModPersonelTypeDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("ArPersonelTypeControllerBean addArCtEntry");
        
        LocalArPersonelTypeHome ArPersonelTypeHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
            
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            ArPersonelTypeHome = (LocalArPersonelTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPersonelTypeHome.JNDI_NAME, LocalArPersonelTypeHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);                

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
            LocalArPersonelType ArPersonelType = null;
			LocalAdBankAccount adBankAccount = null;
        
			
			try { 
	            
	            ArPersonelType = ArPersonelTypeHome.findByPtShortName(details.getPtShortName(), AD_CMPNY);
	            
	            throw new GlobalRecordAlreadyExistException(details.getPtShortName());
	            
	        } catch (FinderException ex) {
	         	
	        	 	
	        }
			
			
			
	        try { 
	            
	            ArPersonelType = ArPersonelTypeHome.findByPtName(details.getPtName(), AD_CMPNY);
	            
	            throw new GlobalRecordAlreadyExistException(details.getPtName());
	            
	        } catch (FinderException ex) {
	         	
	        	 	
	        }
	         
	    	// create new supplier class
	    				    	
	        
	        
	        
	    	ArPersonelType = ArPersonelTypeHome.create(details.getPtShortName(),details.getPtName(),
	    	        details.getPtDescription(),details.getPtRate(), AD_CMPNY);

	
	        
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateArPtEntry(com.util.ArModPersonelTypeDetails details,Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("ArPersonelTypeControllerBean updateArPtEntry");
        
        LocalArPersonelTypeHome arPersonelTypeHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
        
        LocalArPersonelType arPersonelType = null;
        LocalAdBankAccount adBankAccount = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            arPersonelTypeHome = (LocalArPersonelTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPersonelTypeHome.JNDI_NAME, LocalArPersonelTypeHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);                
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	try {
        	                   
	            LocalArPersonelType apExistingPersonelType = arPersonelTypeHome.findByPtName(details.getPtName(), AD_CMPNY);
	            
	            if (!apExistingPersonelType.getPtCode().equals(details.getPtCode())) {
	            
	                 throw new GlobalRecordAlreadyExistException();
	                 
	            }
	            
	        } catch (FinderException ex) {
	        	
	        }
        	
        	
        	try {
                
	            LocalArPersonelType apExistingPersonelType = arPersonelTypeHome.findByPtShortName(details.getPtShortName(), AD_CMPNY);
	            
	            if (!apExistingPersonelType.getPtCode().equals(details.getPtCode())) {
	            
	                 throw new GlobalRecordAlreadyExistException();
	                 
	            }
	            
	        } catch (FinderException ex) {
	        	
	        }
            
			// find and update supplier class

        	arPersonelType = arPersonelTypeHome.findByPrimaryKey(details.getPtCode());

        	arPersonelType.setPtShortName(details.getPtShortName());
        	arPersonelType.setPtName(details.getPtName());
        	arPersonelType.setPtDescription(details.getPtDescription());
        	arPersonelType.setPtRate(details.getPtRate());
		    		
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteArPtEntry(Integer PT_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("ArPersonelTypeControllerBean deleteArPtEntry");
      
        LocalArPersonelTypeHome ArPersonelTypeHome = null;

        // Initialize EJB Home
        
        try {

            ArPersonelTypeHome = (LocalArPersonelTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPersonelTypeHome.JNDI_NAME, LocalArPersonelTypeHome.class);           
      
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
      
        try {
      	
      	    LocalArPersonelType ArPersonelType = null;                
      
	        try {
	      	
	            ArPersonelType = ArPersonelTypeHome.findByPrimaryKey(PT_CODE);
	         
	        } catch (FinderException ex) {
	      	
	            throw new GlobalRecordAlreadyDeletedException();
	            
	        }
	         
	        if (!ArPersonelType.getArPersonels().isEmpty()) {
	  
	            throw new GlobalRecordAlreadyAssignedException();
	         
	        }
	                            	
		    ArPersonelType.remove();
	    
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }         
   
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("ArPersonelTypeControllerBean getGlFcPrecisionUnit");

       
        LocalAdCompanyHome adCompanyHome = null;
       
      
        // Initialize EJB Home
         
        try {
             
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
             
        } catch (NamingException ex) {
             
            throw new EJBException(ex.getMessage());
             
        }

        try {
        	
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             
          return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                      
        } catch (Exception ex) {
        	 
        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
          
        }

     } 
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {
 
        Debug.print("ArPersonelTypeControllerBean ejbCreate");
      
    }
    
}

