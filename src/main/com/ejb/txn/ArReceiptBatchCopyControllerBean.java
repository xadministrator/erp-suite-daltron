
/*
 * ArReceiptBatchCopyControllerBean.java
 *
 * Created on May 27, 2004, 8:27 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.exception.GlobalBatchCopyInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.util.AbstractSessionBean;
import com.util.ArModReceiptDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArReceiptBatchCopyControllerEJB"
 *           display-name="Used for copying receipt batch"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArReceiptBatchCopyControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArReceiptBatchCopyController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArReceiptBatchCopyControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArReceiptBatchCopyControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArOpenRbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArReceiptBatchCopyControllerBean getArOpenRbAll");
        
        LocalArReceiptBatchHome arReceiptBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection arReceiptBatches = arReceiptBatchHome.findOpenRbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = arReceiptBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArReceiptBatch arReceiptBatch = (LocalArReceiptBatch)i.next();
        		
        		list.add(arReceiptBatch.getRbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeArRctBatchCopy(ArrayList list, String RB_NM_TO, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalTransactionBatchCloseException,
		GlobalBatchCopyInvalidException {
                    
        Debug.print("ArReceiptBatchCopyControllerBean executeArRctBatchCopy");
        
        LocalArReceiptHome arReceiptHome = null;
        LocalArReceiptBatchHome arReceiptBatchHome = null; 

        // Initialize EJB Home
        
        try {
            
        	arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			    lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
        	arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {

        	// find receipt batch to
        	
        	LocalArReceiptBatch arReceiptBatchTo = arReceiptBatchHome.findByRbName(RB_NM_TO, AD_BRNCH, AD_CMPNY);

        	// validate if batch to is closed
        	
        	if(arReceiptBatchTo.getRbStatus().equals("CLOSED")) {        		
        		throw new GlobalTransactionBatchCloseException();
        		
        	}
        	    			
        	Iterator i = list.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey((Integer)i.next());
        		
        		if (!arReceipt.getArReceiptBatch().getRbType().equals(arReceiptBatchTo.getRbType())) {
        			
        			throw new GlobalBatchCopyInvalidException();
        			
        		}
        		
        		arReceipt.getArReceiptBatch().dropArReceipt(arReceipt);
        		arReceiptBatchTo.addArReceipt(arReceipt);

        	}

        } catch (GlobalTransactionBatchCloseException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	
        } catch (GlobalBatchCopyInvalidException ex) {
        	   
    	   getSessionContext().setRollbackOnly();    	
    	   throw ex;	
                                    
        } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }

    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public ArrayList getArRctByRbName(String RB_NM, Integer AD_CMPNY)
  	  throws GlobalNoRecordFoundException {
  	
  	  Debug.print("ArReceiptBatchCopyControllerBean getArRctByRbName");
  	  
  	  LocalArReceiptHome arReceiptHome = null;
  	  
  	  // Initialize EJB Home
  	    
  	  try {
  	  	
  	      arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
  	          lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);          
  	        
  	  } catch (NamingException ex) {
  	        
  	      throw new EJBException(ex.getMessage());
  	        
  	  }
  	  
  	  try { 
  	      
  	      ArrayList rctList = new ArrayList();
  	        	        	
  	      Collection arReceipts = null;
  	      
  	      try {
  	      	
  	         arReceipts = arReceiptHome.findByRbName(RB_NM, AD_CMPNY);
  	         
  	      } catch (Exception ex) {
  	      	
  	      	 throw new EJBException(ex.getMessage());
  	      	 
  	      }
  	      
  	      if (arReceipts.isEmpty())
  	         throw new GlobalNoRecordFoundException();
  	         
  	      Iterator i = arReceipts.iterator();
  	      while (i.hasNext()) {
  	
  	         LocalArReceipt arReceipt = (LocalArReceipt) i.next();
  	
  		     ArModReceiptDetails mdetails = new ArModReceiptDetails();
  		     mdetails.setRctCode(arReceipt.getRctCode());
               mdetails.setRctType(arReceipt.getRctType());
  		     mdetails.setRctDate(arReceipt.getRctDate());
  		     mdetails.setRctNumber(arReceipt.getRctNumber());
  		     mdetails.setRctAmount(arReceipt.getRctAmount());
  		     mdetails.setRctCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode());
  		     mdetails.setRctBaName(arReceipt.getAdBankAccount().getBaName());
  		           	  
  	      	 rctList.add(mdetails);
  	      	
  	      }
  	         
  	      return rctList;
    
  	  } catch (GlobalNoRecordFoundException ex) {
  	  	 
  	  	  throw ex;
  	  	
  	  } catch (Exception ex) {
  	  	

  	  	  ex.printStackTrace();
  	  	  throw new EJBException(ex.getMessage());
  	  	
  	  }
          
     }
     
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

         Debug.print("ArReceiptBatchCopyControllerBean getGlFcPrecisionUnit");

        
         LocalAdCompanyHome adCompanyHome = null;
        
       
         // Initialize EJB Home
          
         try {
              
             adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
              
         } catch (NamingException ex) {
              
             throw new EJBException(ex.getMessage());
              
         }

         try {
         	
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
              
           return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                       
         } catch (Exception ex) {
         	 
         	 Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
           
         }

      }


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArReceiptBatchCopyControllerBean ejbCreate");
      
    }
}
