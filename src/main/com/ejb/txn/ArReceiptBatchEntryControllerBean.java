
/*
 * ArReceiptBatchEntryControllerBean.java
 *
 * Created on May 20, 2004, 3:52 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.util.AbstractSessionBean;
import com.util.ArReceiptBatchDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArReceiptBatchEntryControllerEJB"
 *           display-name="used for entering receipt batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArReceiptBatchEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArReceiptBatchEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArReceiptBatchEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArReceiptBatchEntryControllerBean extends AbstractSessionBean {

    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.ArReceiptBatchDetails getArRbByRbCode(Integer RB_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArReceiptBatchEntryControllerBean getArRbByRbCode");
        
        LocalArReceiptBatchHome arReceiptBatchHome = null;        
                
        // Initialize EJB Home
        
        try {
            
            arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalArReceiptBatch arReceiptBatch = null;
        	
        	
        	try {
        		
        		arReceiptBatch = arReceiptBatchHome.findByPrimaryKey(RB_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArReceiptBatchDetails details = new ArReceiptBatchDetails();
        	details.setRbCode(arReceiptBatch.getRbCode());
        	details.setRbName(arReceiptBatch.getRbName());
        	details.setRbDescription(arReceiptBatch.getRbDescription());
        	details.setRbStatus(arReceiptBatch.getRbStatus());
        	details.setRbType(arReceiptBatch.getRbType());
        	details.setRbDateCreated(arReceiptBatch.getRbDateCreated());
        	details.setRbCreatedBy(arReceiptBatch.getRbCreatedBy());
        	
        	return details;
        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveArRbEntry(com.util.ArReceiptBatchDetails details, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyExistException,
        GlobalRecordAlreadyDeletedException,
		GlobalTransactionBatchCloseException,
		GlobalRecordAlreadyAssignedException {
                    
        Debug.print("ArReceiptBatchEntryControllerBean saveArIbEntry");
        
        LocalArReceiptBatchHome arReceiptBatchHome = null;  
        LocalArReceiptHome arReceiptHome = null;       
        
        LocalArReceiptBatch arReceiptBatch = null;
         
                
        // Initialize EJB Home
        
        try {
            
            arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);     
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);       
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        
        try {
 
                		
			// validate if receipt batch is already deleted
			
			try {
				
				if (details.getRbCode() != null) {
					
					arReceiptBatch = arReceiptBatchHome.findByPrimaryKey(details.getRbCode());
					
				}
				
		 	} catch (FinderException ex) {
		 		
		 		throw new GlobalRecordAlreadyDeletedException();	 		
				
			} 
	        
	        // validate if receipt batch exists
	        		
			try {
				
			    LocalArReceiptBatch arExistingReceiptBatch = arReceiptBatchHome.findByRbName(details.getRbName(), AD_BRNCH, AD_CMPNY);
			
			    if (details.getRbCode() == null ||
			        details.getRbCode() != null && !arExistingReceiptBatch.getRbCode().equals(details.getRbCode())) {
			    	
			        throw new GlobalRecordAlreadyExistException();
			        
			    }
			 
			} catch (GlobalRecordAlreadyExistException ex) {
				
				throw ex;
			
			} catch (FinderException ex) {
			    
			} 	
			
			// validate if receipt batch closing
			
			if (details.getRbStatus().equals("CLOSED")) {
				
				Collection arReceipts = arReceiptHome.findByRctPostedAndRctVoidAndRbName(EJBCommon.FALSE, EJBCommon.FALSE, details.getRbName(), AD_CMPNY);
				
				if (!arReceipts.isEmpty()) {
					
					throw new GlobalTransactionBatchCloseException();
					
				}
				
			}
			
			// validate if receipt already assigned
			
			if (details.getRbCode() != null) {
				
				Collection arReceipts = arReceiptBatch.getArReceipts();
				
				if (!arReceiptBatch.getRbType().equals(details.getRbType()) &&
				    !arReceipts.isEmpty()) {
				    	
				    throw new GlobalRecordAlreadyAssignedException();
				    	
				}
				
			}											
			
			
			if (details.getRbCode() == null) {
				
				arReceiptBatch = arReceiptBatchHome.create(details.getRbName(),
				   details.getRbDescription(), details.getRbStatus(), details.getRbType(),
				   details.getRbDateCreated(), details.getRbCreatedBy(), AD_BRNCH, AD_CMPNY);
				
			} else {
				
				arReceiptBatch.setRbName(details.getRbName());
				arReceiptBatch.setRbDescription(details.getRbDescription());
				arReceiptBatch.setRbStatus(details.getRbStatus());
				arReceiptBatch.setRbType(details.getRbType());
				
			}
			
			return arReceiptBatch.getRbCode();				
		      	            	
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	 
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
        
        } catch (GlobalRecordAlreadyExistException ex) {	 	
        
            getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	    
      	} catch (GlobalTransactionBatchCloseException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
                	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }     
            
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteArRbEntry(Integer RB_CODE, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException,
        GlobalRecordAlreadyAssignedException {
                    
        Debug.print("ArReceiptBatchEntryControllerBean deleteArRbEntry");
        
        LocalArReceiptBatchHome arReceiptBatchHome = null;  
        LocalArReceiptHome arReceiptHome = null;               
                
        // Initialize EJB Home
        
        try {
            
            arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);     
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);       
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalArReceiptBatch arReceiptBatch = arReceiptBatchHome.findByPrimaryKey(RB_CODE);
        	
        	Collection arReceipts = arReceiptBatch.getArReceipts();
        	
        	if (!arReceipts.isEmpty()) {
        		
        		throw new GlobalRecordAlreadyAssignedException();
        		
        	}
        	
        	arReceiptBatch.remove();
        	
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw ex; 
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    
              
	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArReceiptBatchEntryControllerBean ejbCreate");
      
    }
    
   // private methods

   

}