
/*
 * ArReceiptBatchPrintControllerBean.java
 *
 * Created on May 17, 2004, 3:12 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.ArModReceiptDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArReceiptBatchPrintControllerEJB"
 *           display-name="Used for print batch receipts"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArReceiptBatchPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArReceiptBatchPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArReceiptBatchPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArReceiptBatchPrintControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArReceiptBatchPrintControllerBean getArCstAll");
        
        LocalArCustomerHome arCustomerHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);        	
        	        	
        	Iterator i = arCustomers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArCustomer arCustomer = (LocalArCustomer)i.next();
        		
        		list.add(arCustomer.getCstCustomerCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } 


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArReceiptBatchPrintControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = adBankAccounts.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
        		
        		list.add(adBankAccount.getBaName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArOpenRbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArReceiptBatchPrintControllerBean getArOpenRbAll");
        
        LocalArReceiptBatchHome arReceiptBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection arReceiptBatches = arReceiptBatchHome.findOpenRbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = arReceiptBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalArReceiptBatch arReceiptBatch = (LocalArReceiptBatch)i.next();
        		
        		list.add(arReceiptBatch.getRbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableArReceiptBatch(Integer AD_CMPNY) {

        Debug.print("ArReceiptBatchPrintControllerBean getAdPrfEnableArReceiptBatch");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableArReceiptBatch();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
    
   
  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getArRctByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
	  throws GlobalNoRecordFoundException {
	
	  Debug.print("ArReceiptBatchPrintControllerBean getArRctByCriteria");
	  
	  LocalArReceiptHome arReceiptHome = null;

	  ArrayList list = new ArrayList();

	  // Initialize EJB Home
	    
	  try {
	  	
	      arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
	          lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	  
	  try { 
	      
	      StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
	      int criteriaSize = criteria.size() + 2;
		  Object obj[];
		  
		  if (criteria.containsKey("approvalStatus")) {
	      	
	      	 String approvalStatus = (String)criteria.get("approvalStatus");
	      	
	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
	      	 	
	      	 	 criteriaSize--;
	      	 	
	      	 }
	      	
	      }
		  
		  if (criteria.containsKey("miscType")) {
		  	
		  	criteriaSize--;
		  	
		  }
		  
		  obj = new Object[criteriaSize];
		  	      
		  if (criteria.containsKey("bankAccount")) {
		  	
		  	 firstArgument = false;
		  	 
		  	 jbossQl.append("WHERE rct.adBankAccount.baName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("bankAccount");
		  	 ctr++;
		  	 
		  } 
		  
		  if (criteria.containsKey("batchName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.arReceiptBatch.rbName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("batchName");
		  	 ctr++;
		  }	
		   
		  if (criteria.containsKey("receiptType")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctType=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("receiptType");
		  	 ctr++;
		  }	
		  
		  if (criteria.containsKey("receiptVoid")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctVoid=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Byte)criteria.get("receiptVoid");
		  	 ctr++;
		  }		   
		      
		  if (criteria.containsKey("customerCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("customerCode");
		  	 ctr++;
		  }      
		      
		  if (criteria.containsKey("dateFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		      
		  if (criteria.containsKey("receiptNumberFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("receiptNumberFrom");
		  	 ctr++;
		  	 
		  }  
		      
		  if (criteria.containsKey("receiptNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("rct.rctNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("receiptNumberTo");
		  	 ctr++;
		  	 
		  } 	      
 
		  if (criteria.containsKey("approvalStatus")) {
		   	
		   	  if (!firstArgument) {
		   	  	
		   	     jbossQl.append("AND ");
		   	     
		   	  } else {
		   	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 
		   	  }
		   	  
		   	  String approvalStatus = (String)criteria.get("approvalStatus");
       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("rct.rctApprovalStatus IS NULL ");
	       	  	
	       	  } else if (approvalStatus.equals("REJECTED")) {
	       	  	
		       	  jbossQl.append("rct.rctReasonForRejection IS NOT NULL ");
	       	  	
	      	  } else {
	      	  	
		      	  jbossQl.append("rct.rctApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
		   	  
		  }
		  
		  if (criteria.containsKey("posted")) {
		   	
		   	  if (!firstArgument) {
		   	  	
		   	     jbossQl.append("AND ");
		   	     
		   	  } else {
		   	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 
		   	  }
		   	  
		   	  jbossQl.append("rct.rctPosted=?" + (ctr+1) + " ");
		   	  
		   	  String posted = (String)criteria.get("posted");
		   	  
		   	  if (posted.equals("YES")) {
		   	  	
		   	  	obj[ctr] = new Byte(EJBCommon.TRUE);
		   	  	
		   	  } else {
		   	  	
		   	  	obj[ctr] = new Byte(EJBCommon.FALSE);
		   	  	
		   	  }       	  
		   	 
		   	  ctr++;
		   	  
		  }	  	   
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("rct.rctAdBranch=" + AD_BRNCH + " AND rct.rctAdCompany=" + AD_CMPNY + " ");
	             	     
	      String orderBy = null;
		      
	      if (ORDER_BY.equals("BANK ACCOUNT")) {
	          
	          orderBy = "rct.adBankAccount.baName";
	          	
	      } else if (ORDER_BY.equals("CUSTOMER CODE")) {
	      	 
	      	  orderBy = "rct.arCustomer.cstCustomerCode";

	      } else if (ORDER_BY.equals("RECEIPT NUMBER")) {
	      	
	      	  orderBy = "rct.rctNumber";
	      	
	      }
	      
		  
		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy + ", rct.rctDate");
		  	
		  }  else {
		  	
		  	jbossQl.append("ORDER BY rct.rctDate");
		  	
		  }
		  
	               
	      jbossQl.append(" OFFSET ?" + (ctr + 1));
	      obj[ctr] = OFFSET;
	      ctr++;
	      
	      jbossQl.append(" LIMIT ?" + (ctr + 1));
	      obj[ctr] = LIMIT;

	      Collection arReceipts = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);

	      if (arReceipts.isEmpty())
	         throw new GlobalNoRecordFoundException();	        
	               
	      Iterator i = arReceipts.iterator();
	      while (i.hasNext()) {
	
	         LocalArReceipt arReceipt = (LocalArReceipt) i.next();	         	         

	         if (arReceipt.getRctType().equals("MISC"))  {
	         	
	         	String miscType = (String)criteria.get("miscType");
	         	
	         	if ((miscType.equals("ITEMS") && arReceipt.getArInvoiceLineItems().isEmpty()) ||
	         			(miscType.equals("MEMO LINES") && arReceipt.getArInvoiceLines().isEmpty()))
	         		continue;
	         	
	         }

		     ArModReceiptDetails mdetails = new ArModReceiptDetails();
		     mdetails.setRctCode(arReceipt.getRctCode());
             mdetails.setRctType(arReceipt.getRctType());
		     mdetails.setRctDate(arReceipt.getRctDate());
		     mdetails.setRctNumber(arReceipt.getRctNumber());
		     mdetails.setRctAmount(arReceipt.getRctAmount());
		     mdetails.setRctCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode());
		     mdetails.setRctBaName(arReceipt.getAdBankAccount().getBaName());
		           	  
	      	 list.add(mdetails);
	      	
	      }
	       
	      if (list.isEmpty())
	      	throw new GlobalNoRecordFoundException();
	      
	      return list;
  
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
   }
   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArReceiptBatchPrintControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {
    	
    	Debug.print("ArReceiptBatchPrintControllerBean getAdPrfArUseCustomerPulldown");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;         
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		return adPreference.getPrfArUseCustomerPulldown();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("ArReceiptBatchPrintControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
