package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchArTaxCodeHome;
import com.ejb.ad.LocalAdBranchBankAccount;
import com.ejb.ad.LocalAdBranchBankAccountHome;
import com.ejb.ad.LocalAdBranchCustomer;
import com.ejb.ad.LocalAdBranchCustomerHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdDiscount;
import com.ejb.ad.LocalAdDiscountHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ar.LocalArAppliedCredit;
import com.ejb.ar.LocalArAppliedCreditHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalance;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.exception.AdPRFCoaGlCustomerDepositAccountNotFoundException;
import com.ejb.exception.ArINVOverCreditBalancePaidapplicationNotAllowedException;
import com.ejb.exception.ArINVOverapplicationNotAllowedException;
import com.ejb.exception.ArRCTInvoiceHasNoWTaxCodeException;
import com.ejb.exception.ArREDuplicatePayfileReferenceNumberException;
import com.ejb.exception.ArReceiptEntryValidationException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalDuplicateEmployeeIdException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.hr.LocalHrDeployedBranch;
import com.ejb.hr.LocalHrDeployedBranchHome;
import com.ejb.hr.LocalHrPayrollPeriod;
import com.ejb.hr.LocalHrPayrollPeriodHome;
import com.util.AbstractSessionBean;
import com.util.ArModAppliedInvoiceDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoicePaymentScheduleDetails;
import com.util.ArModReceiptDetails;
import com.util.ArReceiptDetails;
import com.util.CmAdjustmentDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;

/**
 * @ejb:bean name="ArReceiptEntryControllerEJB"
 *           display-name="used for entering receipt"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArReceiptEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArReceiptEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArReceiptEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
*/

public class ArReceiptEntryControllerBean extends AbstractSessionBean {



	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getHrOpenPpAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArReceiptEntryControllerBean getHrOpenPpAll");

        LocalHrPayrollPeriodHome hrPayrollPeriodHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	hrPayrollPeriodHome = (LocalHrPayrollPeriodHome)EJBHomeFactory.
            lookUpLocalHome(LocalHrPayrollPeriodHome.JNDI_NAME, LocalHrPayrollPeriodHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection hrPayrollPeriods = hrPayrollPeriodHome.findPpAll( AD_CMPNY);

            Iterator i = hrPayrollPeriods.iterator();

            while (i.hasNext()) {

                LocalHrPayrollPeriod hrPayrollPeriod = (LocalHrPayrollPeriod)i.next();

                list.add(hrPayrollPeriod.getPpName());

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {
    	
        Debug.print("ArReceiptEntryControllerBean getGlFcAllWithDefault");

        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        Collection glFunctionalCurrencies = null;

        LocalGlFunctionalCurrency glFunctionalCurrency = null;
        LocalAdCompany adCompany = null;


        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
            	EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (glFunctionalCurrencies.isEmpty()) {

        	return null;

        }

        Iterator i = glFunctionalCurrencies.iterator();

        while (i.hasNext()) {

        	glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

        	          GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
        	    glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
        	    adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
        	       EJBCommon.TRUE : EJBCommon.FALSE);

        	list.add(mdetails);

        }

        return list;

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArReceiptEntryControllerBean getAdBaAll");

        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

        	Iterator i = adBankAccounts.iterator();

        	while (i.hasNext()) {

        		LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();

        		list.add(adBankAccount.getBaName());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

      /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public int getArCstAdBrnchByEmplyId(String EMP_ID, Integer AD_CMPNY) throws  GlobalNoRecordFoundException,GlobalDuplicateEmployeeIdException {
       Debug.print("ArReceiptImportControllerBean getArCstAdBrnchByEmplyId");
        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

                arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

        } catch (NamingException ex) {

                throw new EJBException(ex.getMessage());

        }

        try {



                Collection arCustomers = arCustomerHome.findManyByCstEmployeeID(EMP_ID, AD_CMPNY);

                if(arCustomers.size() > 1){
                        throw new GlobalDuplicateEmployeeIdException();
                }


                LocalArCustomer arCustomer = arCustomerHome.findByCstEmployeeID(EMP_ID, AD_CMPNY);

                return 	arCustomer.getCstAdBranch();

        } catch (FinderException ex) {

             //   throw new GlobalNoRecordFoundException();

                return 0;

        }catch (GlobalDuplicateEmployeeIdException ex){
                System.out.println("2 emp id");
                throw new GlobalDuplicateEmployeeIdException();



        }catch (Exception ex) {

                Debug.printStackTrace(ex);
                throw new EJBException(ex.getMessage());

        }
   }





    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public String getArCstCstmrCodeByEmplyId(String EMP_ID, Integer AD_CMPNY) throws  GlobalNoRecordFoundException,GlobalDuplicateEmployeeIdException {

   //	findByCstEmployeeID

           Debug.print("ArReceiptImportControllerBean getArCstCstmrCodeByEmplyId");

           LocalArCustomerHome arCustomerHome = null;

           ArrayList list = new ArrayList();

           // Initialize EJB Home

           try {

                   arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                   lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

           } catch (NamingException ex) {

                   throw new EJBException(ex.getMessage());

           }

           try {



                   Collection arCustomers = arCustomerHome.findManyByCstEmployeeID(EMP_ID, AD_CMPNY);

                   if(arCustomers.size() > 1){
                           throw new GlobalDuplicateEmployeeIdException();
                   }


                   LocalArCustomer arCustomer = arCustomerHome.findByCstEmployeeID(EMP_ID, AD_CMPNY);

                   return 	arCustomer.getCstCustomerCode();

           } catch (FinderException ex) {

                   throw new GlobalNoRecordFoundException();

           }catch (GlobalDuplicateEmployeeIdException ex){
                   System.out.println("2 emp id");
                   throw new GlobalDuplicateEmployeeIdException();



           }catch (Exception ex) {

                   Debug.printStackTrace(ex);
                   throw new EJBException(ex.getMessage());

           }




   }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArCreditMemoEntryControllerBean getArCstAll");

        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

        	Iterator i = arCustomers.iterator();

        	while (i.hasNext()) {

        		LocalArCustomer arCustomer = (LocalArCustomer)i.next();

        		list.add(arCustomer.getCstCustomerCode());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArModReceiptDetails getArRctByRctNum(String RCT_NMBR, Integer AD_BRNCH, Integer AD_CMPNY) throws  GlobalNoRecordFoundException {
Debug.print("ArReceiptEntryControllerBean getArRctByRctNum");

        LocalArReceiptHome arReceiptHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        // Initialize EJB Home


        try {

            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }



        try {

        	LocalArReceipt arReceipt = null;


        	try {

        		arReceipt = arReceiptHome.findByRctNumberAndBrCode(RCT_NMBR, AD_BRNCH, AD_CMPNY);

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}

        	ArrayList rctAiList = new ArrayList();

        	// get applied invoices

        	Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();

        	Iterator i = arAppliedInvoices.iterator();

        	while (i.hasNext()) {

        		LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

        		ArModAppliedInvoiceDetails mdetails = new ArModAppliedInvoiceDetails();

        		mdetails.setAiApplyAmount(arAppliedInvoice.getAiApplyAmount());
        		mdetails.setAiPenaltyApplyAmount(arAppliedInvoice.getAiPenaltyApplyAmount());
        		mdetails.setAiCreditableWTax(arAppliedInvoice.getAiCreditableWTax());
        		mdetails.setAiCreditBalancePaid(arAppliedInvoice.getAiCreditBalancePaid());
        		mdetails.setAiDiscountAmount(arAppliedInvoice.getAiDiscountAmount());
        		mdetails.setAiRebate(arAppliedInvoice.getAiRebate());
        		mdetails.setAiAppliedDeposit(arAppliedInvoice.getAiAppliedDeposit());
        		mdetails.setAiAllocatedPaymentAmount(arAppliedInvoice.getAiAllocatedPaymentAmount());
        		mdetails.setAiIpsCode(arAppliedInvoice.getArInvoicePaymentSchedule().getIpsCode());
        		mdetails.setAiIpsNumber(arAppliedInvoice.getArInvoicePaymentSchedule().getIpsNumber());
        		mdetails.setAiIpsDueDate(arAppliedInvoice.getArInvoicePaymentSchedule().getIpsDueDate());
        		mdetails.setAiIpsAmountDue(arAppliedInvoice.getArInvoicePaymentSchedule().getIpsAmountDue() - arAppliedInvoice.getArInvoicePaymentSchedule().getIpsAmountPaid());
        		mdetails.setAiIpsPenaltyDue(arAppliedInvoice.getArInvoicePaymentSchedule().getIpsPenaltyDue() - arAppliedInvoice.getArInvoicePaymentSchedule().getIpsPenaltyPaid());

        		mdetails.setAiIpsInvNumber(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvNumber());
        		mdetails.setAiIpsInvReferenceNumber(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvReferenceNumber());
        		mdetails.setAiIpsInvFcName(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcName());

        		// add apply deposit
        		mdetails.setAiAppliedDeposit(arAppliedInvoice.getAiAppliedDeposit());

        		rctAiList.add(mdetails);

        	}

        	ArModReceiptDetails mRctDetails = new ArModReceiptDetails();

        	mRctDetails.setRctCode(arReceipt.getRctCode());
        	mRctDetails.setRctDocumentType(arReceipt.getRctDocumentType());
        	mRctDetails.setRctDate(arReceipt.getRctDate());
        	mRctDetails.setRctNumber(arReceipt.getRctNumber());
        	mRctDetails.setRctReferenceNumber(arReceipt.getRctReferenceNumber());
        	mRctDetails.setRctCheckNo(arReceipt.getRctCheckNo());
        	mRctDetails.setRctAmount(arReceipt.getRctAmount());
        	mRctDetails.setRctEnableAdvancePayment(arReceipt.getRctEnableAdvancePayment());
        	mRctDetails.setRctExcessAmount(arReceipt.getRctExcessAmount());
        	mRctDetails.setRctVoid(arReceipt.getRctVoid());
        	mRctDetails.setRctDescription(arReceipt.getRctDescription());
        	mRctDetails.setRctConversionDate(arReceipt.getRctConversionDate());
        	mRctDetails.setRctConversionRate(arReceipt.getRctConversionRate());
        	mRctDetails.setRctPaymentMethod(arReceipt.getRctPaymentMethod());
        	mRctDetails.setRctApprovalStatus(arReceipt.getRctApprovalStatus());
        	mRctDetails.setRctPosted(arReceipt.getRctPosted());
        	mRctDetails.setRctVoid(arReceipt.getRctVoid());
        	mRctDetails.setRctVoidApprovalStatus(arReceipt.getRctVoidApprovalStatus());
        	mRctDetails.setRctVoidPosted(arReceipt.getRctVoidPosted());
        	mRctDetails.setRctReasonForRejection(arReceipt.getRctReasonForRejection());
        	mRctDetails.setRctCreatedBy(arReceipt.getRctCreatedBy());
        	mRctDetails.setRctDateCreated(arReceipt.getRctDateCreated());
        	mRctDetails.setRctLastModifiedBy(arReceipt.getRctLastModifiedBy());
        	mRctDetails.setRctDateLastModified(arReceipt.getRctDateLastModified());
        	mRctDetails.setRctApprovedRejectedBy(arReceipt.getRctApprovedRejectedBy());
        	mRctDetails.setRctDateApprovedRejected(arReceipt.getRctDateApprovedRejected());
        	mRctDetails.setRctPostedBy(arReceipt.getRctPostedBy());
        	mRctDetails.setRctDatePosted(arReceipt.getRctDatePosted());
        	mRctDetails.setRctFcName(arReceipt.getGlFunctionalCurrency().getFcName());
        	mRctDetails.setRctCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode());
        	mRctDetails.setRctBaName(arReceipt.getAdBankAccount().getBaName());
        	mRctDetails.setRctRbName(arReceipt.getArReceiptBatch() != null ? arReceipt.getArReceiptBatch().getRbName() : null);
        	mRctDetails.setRctCstName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());

        	mRctDetails.setRctAiList(rctAiList);

        	mRctDetails.setReportParameter(arReceipt.getReportParameter());

        	return mRctDetails;

        } catch (GlobalNoRecordFoundException ex) {
        	System.out.println("====================NOT FOUND :" + RCT_NMBR);
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }
    }

      /**
    * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArModReceiptDetails getArRctByRctCode(Integer RCT_CODE, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArReceiptEntryControllerBean getArRctByRctCode");

        LocalArReceiptHome arReceiptHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        // Initialize EJB Home

        try {

            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalArReceipt arReceipt = null;


        	try {

        		arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}

        	ArrayList rctAiList = new ArrayList();

        	// get applied invoices

        	Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();

        	Iterator i = arAppliedInvoices.iterator();
        	double totalAmount =0d;
        	while (i.hasNext()) {

        		LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

        		ArModAppliedInvoiceDetails mdetails = new ArModAppliedInvoiceDetails();
        		totalAmount += (arAppliedInvoice.getAiApplyAmount()+arAppliedInvoice.getAiPenaltyApplyAmount()
		    			 + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiCreditBalancePaid());

        		mdetails.setAiApplyAmount(arAppliedInvoice.getAiApplyAmount());
        		mdetails.setAiPenaltyApplyAmount(arAppliedInvoice.getAiPenaltyApplyAmount());
        		mdetails.setAiCreditableWTax(arAppliedInvoice.getAiCreditableWTax());
        		mdetails.setAiDiscountAmount(arAppliedInvoice.getAiDiscountAmount());

        		mdetails.setAiAppliedDeposit(arAppliedInvoice.getAiAppliedDeposit());
        		mdetails.setAiAllocatedPaymentAmount(arAppliedInvoice.getAiAllocatedPaymentAmount());
        		mdetails.setAiIpsCode(arAppliedInvoice.getArInvoicePaymentSchedule().getIpsCode());
        		mdetails.setAiIpsNumber(arAppliedInvoice.getArInvoicePaymentSchedule().getIpsNumber());
        		mdetails.setAiIpsInvDate(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvDate());
        		mdetails.setAiIpsDueDate(arAppliedInvoice.getArInvoicePaymentSchedule().getIpsDueDate());
        		mdetails.setAiIpsAmountDue(arAppliedInvoice.getArInvoicePaymentSchedule().getIpsAmountDue());
        		mdetails.setAiIpsPenaltyDue(arAppliedInvoice.getArInvoicePaymentSchedule().getIpsPenaltyDue());
        		mdetails.setAiIpsInvNumber(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvNumber());
        		mdetails.setAiIpsInvReferenceNumber(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvReferenceNumber());
        		mdetails.setAiIpsInvFcName(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcName());
        		mdetails.setAiCreditBalancePaid(arAppliedInvoice.getAiCreditBalancePaid());
        		// add apply deposit
        		mdetails.setAiAppliedDeposit(arAppliedInvoice.getAiAppliedDeposit());

        		mdetails.setAiRebate(arAppliedInvoice.getAiRebate());
        		mdetails.setAiApplyRebate(arAppliedInvoice.getAiApplyRebate());

        		rctAiList.add(mdetails);

        	}

        	ArModReceiptDetails mRctDetails = new ArModReceiptDetails();

        	mRctDetails.setRctCode(arReceipt.getRctCode());
        	mRctDetails.setRctDocumentType(arReceipt.getRctDocumentType());
        	mRctDetails.setRctDate(arReceipt.getRctDate());
        	mRctDetails.setRctNumber(arReceipt.getRctNumber());
        	mRctDetails.setRctReferenceNumber(arReceipt.getRctReferenceNumber());
        	mRctDetails.setRctCheckNo(arReceipt.getRctCheckNo());
        	mRctDetails.setRctPayfileReferenceNumber(arReceipt.getRctPayfileReferenceNumber());
        	mRctDetails.setRctAmount(totalAmount);

        	mRctDetails.setRctEnableAdvancePayment(arReceipt.getRctEnableAdvancePayment());
        	mRctDetails.setRctExcessAmount(arReceipt.getRctExcessAmount());
        	mRctDetails.setRctVoid(arReceipt.getRctVoid());
        	mRctDetails.setRctDescription(arReceipt.getRctDescription());
        	mRctDetails.setRctConversionDate(arReceipt.getRctConversionDate());
        	mRctDetails.setRctConversionRate(arReceipt.getRctConversionRate());
        	mRctDetails.setRctPaymentMethod(arReceipt.getRctPaymentMethod());
        	mRctDetails.setRctApprovalStatus(arReceipt.getRctApprovalStatus());
        	mRctDetails.setRctPosted(arReceipt.getRctPosted());
        	mRctDetails.setRctVoid(arReceipt.getRctVoid());
        	mRctDetails.setRctVoidApprovalStatus(arReceipt.getRctVoidApprovalStatus());
        	mRctDetails.setRctVoidPosted(arReceipt.getRctVoidPosted());
        	mRctDetails.setRctReasonForRejection(arReceipt.getRctReasonForRejection());
        	mRctDetails.setRctCreatedBy(arReceipt.getRctCreatedBy());
        	mRctDetails.setRctDateCreated(arReceipt.getRctDateCreated());
        	mRctDetails.setRctLastModifiedBy(arReceipt.getRctLastModifiedBy());
        	mRctDetails.setRctDateLastModified(arReceipt.getRctDateLastModified());
        	mRctDetails.setRctApprovedRejectedBy(arReceipt.getRctApprovedRejectedBy());
        	mRctDetails.setRctDateApprovedRejected(arReceipt.getRctDateApprovedRejected());
        	mRctDetails.setRctPostedBy(arReceipt.getRctPostedBy());
        	mRctDetails.setRctDatePosted(arReceipt.getRctDatePosted());
        	mRctDetails.setRctFcName(arReceipt.getGlFunctionalCurrency().getFcName());
        	mRctDetails.setRctCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode());
        	mRctDetails.setRctBaName(arReceipt.getAdBankAccount().getBaName());
        	mRctDetails.setRctRbName(arReceipt.getArReceiptBatch() != null ? arReceipt.getArReceiptBatch().getRbName() : null);
        	mRctDetails.setRctCstName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());

        	//HRIS
        	mRctDetails.setRctHrPayrollPeriodName(arReceipt.getHrPayrollPeriod() != null ? arReceipt.getHrPayrollPeriod().getPpName() : null);
        	mRctDetails.setReportParameter(arReceipt.getReportParameter());
        	mRctDetails.setRctAiList(rctAiList);

        	return mRctDetails;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArModCustomerDetails getArCstByCstCustomerCode(String CST_CSTMR_CODE, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArReceiptEntryControllerBean getArCstByCstCustomerCode");

        LocalArCustomerHome arCustomerHome = null;

        // Initialize EJB Home

        try {

            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalArCustomer arCustomer = null;


        	try {
        		System.out.println("pasok");
        		arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

        		System.out.println("found : " + arCustomer.getCstCustomerCode());
        	} catch (FinderException ex) {

        		throw new GlobalNoRecordFoundException();

        	}

        	ArModCustomerDetails mdetails = new ArModCustomerDetails();


        	mdetails.setCstCustomerCode(arCustomer.getCstCustomerCode());
        	mdetails.setCstPaymentMethod(arCustomer.getCstPaymentMethod());
        	mdetails.setCstCtBaName(arCustomer.getAdBankAccount().getBaName());
        	mdetails.setCstName(arCustomer.getCstName());

        	return mdetails;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

     /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArIpsByInvcNmbr(String AR_INVC_NMBR, Date RCT_DT, boolean ENBL_RBT, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

         Debug.print("ArReceiptEntryControllerBean getArIpsByCstCustomerCode");

        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalAdDiscountHome adDiscountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;

        ArrayList list = new ArrayList();


        // Initialize EJB Home

        try {

            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);



        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try{

            //if getting all invoice in all branch
        	Collection arInvoicePaymentSchedules;


                System.out.println("invoice : " + AR_INVC_NMBR);
        	if(AD_BRNCH <= 0 ){
                    System.out.println("Get all INvoice in all branch");

                    arInvoicePaymentSchedules =  arInvoicePaymentScheduleHome.findOpenIpsByIpsLockAndInvNumber(EJBCommon.FALSE, AR_INVC_NMBR, AD_CMPNY);

        	} else {
                    arInvoicePaymentSchedules =  arInvoicePaymentScheduleHome.findOpenIpsByIpsLockAndInvNumberAdBranchCompny(EJBCommon.FALSE, AR_INVC_NMBR, AD_BRNCH, AD_CMPNY);

        	}


        	if (arInvoicePaymentSchedules.isEmpty()) {
        		System.out.println("empty");
        		throw new GlobalNoRecordFoundException();

        	}

        	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

        	Iterator i = arInvoicePaymentSchedules.iterator();

        	while (i.hasNext()) {

        		LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        		   (LocalArInvoicePaymentSchedule)i.next();

                // verification if ips is already closed
        		if (EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue(), precisionUnit) == EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid(), precisionUnit)) continue;

        		ArModInvoicePaymentScheduleDetails mdetails = new ArModInvoicePaymentScheduleDetails();

        		Collection arInvoiceLines = arInvoicePaymentSchedule.getArInvoice().getArInvoiceLines();
        		Iterator x = arInvoiceLines.iterator();
        		String reference = "";
        		while(x.hasNext()){
        			LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)x.next();
        			reference = arInvoiceLine.getArStandardMemoLine().getSmlName().replace("AR - ", "");
        			reference.replace("Association Dues", "ASD");
        			reference.replace("Parking Dues", "PD");
        			reference.replace("Water", "WTR");
        		}

        		mdetails.setIpsCode(arInvoicePaymentSchedule.getIpsCode());
        		mdetails.setIpsNumber(arInvoicePaymentSchedule.getIpsNumber());
                        mdetails.setIpsArCustomerCode(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode());
        		mdetails.setIpsInvDate(arInvoicePaymentSchedule.getArInvoice().getInvDate());

        		mdetails.setIpsDueDate(arInvoicePaymentSchedule.getIpsDueDate());
        		mdetails.setIpsAmountDue(arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid());
        		mdetails.setIpsPenaltyDue(arInvoicePaymentSchedule.getIpsPenaltyDue() - arInvoicePaymentSchedule.getIpsPenaltyPaid());

        		double interestDue =
        				arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEnableRebate() == EJBCommon.TRUE ? arInvoicePaymentSchedule.getIpsInterestDue() : 0d;
        		mdetails.setIpsInterestDue(interestDue);

        		mdetails.setIpsInvNumber(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
        		mdetails.setIpsInvReferenceNumber(reference + " "+ arInvoicePaymentSchedule.getArInvoice().getInvReferenceNumber());
        		mdetails.setIpsInvFcName(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName());
        		mdetails.setIpsAdBranch(arInvoicePaymentSchedule.getArInvoice().getInvAdBranch());
        		// calculate default discount
        		System.out.println( "1 pasok" + RCT_DT);
        		System.out.println( "2 pasok" +  arInvoicePaymentSchedule.getIpsDueDate().getTime());

        		short INVOICE_AGE = (short)((RCT_DT.getTime() -
	                    arInvoicePaymentSchedule.getIpsDueDate().getTime()) / (1000 * 60 * 60 * 24));
        		System.out.println("arInvoicePaymentSchedule.getIpsDueDate()="+arInvoicePaymentSchedule.getIpsDueDate());
        		System.out.println("INVOICE_AGE="+INVOICE_AGE);

	            double IPS_DSCNT_AMNT = 0d;

	            if (arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getPytDiscountOnInvoice() == EJBCommon.FALSE) {

			        Collection adDiscounts = adDiscountHome.findByPsLineNumberAndPytName(
			                	arInvoicePaymentSchedule.getIpsNumber(),
			                	arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getPytName(), AD_CMPNY);

		            Iterator j = adDiscounts.iterator();

		            while (j.hasNext()) {

		    	        LocalAdDiscount adDiscount = (LocalAdDiscount)j.next();

		    	        System.out.println("adDiscount.getDscPaidWithinDay() ="+adDiscount.getDscPaidWithinDay() );
		    	        if (adDiscount.getDscPaidWithinDay() >= INVOICE_AGE) {

		    		        IPS_DSCNT_AMNT = EJBCommon.roundIt(mdetails.getIpsAmountDue() * adDiscount.getDscDiscountPercent() / 100, this.getGlFcPrecisionUnit(AD_CMPNY));
		    		        System.out.println("IPS_DSCNT_AMNT="+IPS_DSCNT_AMNT);
		    		        break;

		    	        }

		            }

	            }


	            double IPS_REBATE = 0d;

	            if (arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getPytDiscountOnInvoice() == EJBCommon.FALSE
	            		&& arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEnableRebate() == EJBCommon.TRUE && ENBL_RBT) {

			        	if (0 >= INVOICE_AGE) {

			        		IPS_REBATE = EJBCommon.roundIt(mdetails.getIpsInterestDue(), this.getGlFcPrecisionUnit(AD_CMPNY));
		    		        System.out.println("IPS_REBATE="+IPS_REBATE);

		    	        }



	            }

	             // calculate default tax withheld

	            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

	            double APPLY_AMOUNT = mdetails.getIpsAmountDue() - IPS_DSCNT_AMNT;
	            double PENALTY_APPLY_AMOUNT = mdetails.getIpsPenaltyDue();

	            double W_TAX_AMOUNT = 0d;

	            if (arInvoicePaymentSchedule.getArInvoice().getArWithholdingTaxCode().getWtcRate() != 0	&&
	                adPreference.getPrfArWTaxRealization().equals("COLLECTION")) {

	                LocalArTaxCode arTaxCode = arInvoicePaymentSchedule.getArInvoice().getArTaxCode();

	                double NET_AMOUNT = 0d;

	                if (arTaxCode.getTcType().equals("INCLUSIVE") || arTaxCode.getTcType().equals("EXCLUSIVE")) {

		    			NET_AMOUNT = EJBCommon.roundIt(APPLY_AMOUNT / (1 + (arTaxCode.getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));

		            } else {

		            	NET_AMOUNT = APPLY_AMOUNT;

		            }

	                W_TAX_AMOUNT = EJBCommon.roundIt(NET_AMOUNT * (arInvoicePaymentSchedule.getArInvoice().getArWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

	                APPLY_AMOUNT -= W_TAX_AMOUNT;



	            }

         		mdetails.setIpsAiApplyAmount(APPLY_AMOUNT);
         		mdetails.setIpsAiPenaltyApplyAmount(PENALTY_APPLY_AMOUNT);
         		mdetails.setIpsAiCreditableWTax(W_TAX_AMOUNT);
        		mdetails.setIpsAiDiscountAmount(IPS_DSCNT_AMNT);
        		mdetails.setIpsAiRebate(IPS_REBATE);
        		System.out.println("ADD : + ");
        		list.add(mdetails);

        	}

        	if (list.isEmpty()) {

                    throw new GlobalNoRecordFoundException();

        	}

        	return list;



        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }



    }







    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArIpsByCstCustomerCode(String CST_CSTMR_CODE, Date RCT_DT, boolean ENBL_RBT, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArReceiptEntryControllerBean getArIpsByCstCustomerCode");

        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalAdDiscountHome adDiscountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;

        ArrayList list = new ArrayList();


        // Initialize EJB Home

        try {

            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	//if getting all invoice in all branch
        	Collection arInvoicePaymentSchedules;
        	if(AD_BRNCH <= 0 ){
                    System.out.println("Get all INvoice in all branch");

                    arInvoicePaymentSchedules =  arInvoicePaymentScheduleHome.findOpenIpsByIpsLockAndCstCustomerCode(EJBCommon.FALSE, CST_CSTMR_CODE, AD_CMPNY);

        	} else {
        		 arInvoicePaymentSchedules =
                arInvoicePaymentScheduleHome.findOpenIpsByIpsLockAndCstCustomerCodeAndBrCode(EJBCommon.FALSE, CST_CSTMR_CODE, AD_BRNCH, AD_CMPNY);

        	}


        	if (arInvoicePaymentSchedules.isEmpty()) {

        		throw new GlobalNoRecordFoundException();

        	}

        	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

        	Iterator i = arInvoicePaymentSchedules.iterator();

        	while (i.hasNext()) {

        		LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        		   (LocalArInvoicePaymentSchedule)i.next();

                // verification if ips is already closed
        		if (EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue(), precisionUnit) == EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid(), precisionUnit)) continue;

        		ArModInvoicePaymentScheduleDetails mdetails = new ArModInvoicePaymentScheduleDetails();

        		Collection arInvoiceLines = arInvoicePaymentSchedule.getArInvoice().getArInvoiceLines();
        		Iterator x = arInvoiceLines.iterator();
        		String reference = "";
        		while(x.hasNext()){
        			LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)x.next();
        			reference = arInvoiceLine.getArStandardMemoLine().getSmlName().replace("AR - ", "");
        			reference.replace("Association Dues", "ASD");
        			reference.replace("Parking Dues", "PD");
        			reference.replace("Water", "WTR");
        		}

        		mdetails.setIpsCode(arInvoicePaymentSchedule.getIpsCode());
        		mdetails.setIpsNumber(arInvoicePaymentSchedule.getIpsNumber());
        		mdetails.setIpsInvDate(arInvoicePaymentSchedule.getArInvoice().getInvDate());
        		mdetails.setIpsArCustomerCode(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode());
        		mdetails.setIpsDueDate(arInvoicePaymentSchedule.getIpsDueDate());
        		mdetails.setIpsAmountDue(arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid());
        		mdetails.setIpsPenaltyDue(arInvoicePaymentSchedule.getIpsPenaltyDue() - arInvoicePaymentSchedule.getIpsPenaltyPaid());

        		double interestDue =
        				arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEnableRebate() == EJBCommon.TRUE ? arInvoicePaymentSchedule.getIpsInterestDue() : 0d;
        		mdetails.setIpsInterestDue(interestDue);

        		mdetails.setIpsInvNumber(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
        		mdetails.setIpsInvReferenceNumber(reference + " "+ arInvoicePaymentSchedule.getArInvoice().getInvReferenceNumber());
        		mdetails.setIpsInvFcName(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName());
        		mdetails.setIpsAdBranch(arInvoicePaymentSchedule.getArInvoice().getInvAdBranch());
        		// calculate default discount
        		System.out.println( "1 pasok" + RCT_DT);
        		System.out.println( "2 pasok" +  arInvoicePaymentSchedule.getIpsDueDate().getTime());

        		short INVOICE_AGE = (short)((RCT_DT.getTime() -
	                    arInvoicePaymentSchedule.getIpsDueDate().getTime()) / (1000 * 60 * 60 * 24));
        		System.out.println("arInvoicePaymentSchedule.getIpsDueDate()="+arInvoicePaymentSchedule.getIpsDueDate());
        		System.out.println("INVOICE_AGE="+INVOICE_AGE);

	            double IPS_DSCNT_AMNT = 0d;

	            if (arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getPytDiscountOnInvoice() == EJBCommon.FALSE) {

			        Collection adDiscounts = adDiscountHome.findByPsLineNumberAndPytName(
			                	arInvoicePaymentSchedule.getIpsNumber(),
			                	arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getPytName(), AD_CMPNY);

		            Iterator j = adDiscounts.iterator();

		            while (j.hasNext()) {

		    	        LocalAdDiscount adDiscount = (LocalAdDiscount)j.next();

		    	        System.out.println("adDiscount.getDscPaidWithinDay() ="+adDiscount.getDscPaidWithinDay() );
		    	        if (adDiscount.getDscPaidWithinDay() >= INVOICE_AGE) {

		    		        IPS_DSCNT_AMNT = EJBCommon.roundIt(mdetails.getIpsAmountDue() * adDiscount.getDscDiscountPercent() / 100, this.getGlFcPrecisionUnit(AD_CMPNY));
		    		        System.out.println("IPS_DSCNT_AMNT="+IPS_DSCNT_AMNT);
		    		        break;

		    	        }

		            }

	            }


	            double IPS_REBATE = 0d;

	            if (arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getPytDiscountOnInvoice() == EJBCommon.FALSE
	            		&& arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEnableRebate() == EJBCommon.TRUE && ENBL_RBT) {

			        	if (0 >= INVOICE_AGE) {

			        		IPS_REBATE = EJBCommon.roundIt(mdetails.getIpsInterestDue(), this.getGlFcPrecisionUnit(AD_CMPNY));
		    		        System.out.println("IPS_REBATE="+IPS_REBATE);

		    	        }



	            }

	             // calculate default tax withheld

	            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

	            double APPLY_AMOUNT = mdetails.getIpsAmountDue() - IPS_DSCNT_AMNT;
	            double PENALTY_APPLY_AMOUNT = mdetails.getIpsPenaltyDue();

	            double W_TAX_AMOUNT = 0d;

	            if (arInvoicePaymentSchedule.getArInvoice().getArWithholdingTaxCode().getWtcRate() != 0	&&
	                adPreference.getPrfArWTaxRealization().equals("COLLECTION")) {

	                LocalArTaxCode arTaxCode = arInvoicePaymentSchedule.getArInvoice().getArTaxCode();

	                double NET_AMOUNT = 0d;

	                if (arTaxCode.getTcType().equals("INCLUSIVE") || arTaxCode.getTcType().equals("EXCLUSIVE")) {

		    			NET_AMOUNT = EJBCommon.roundIt(APPLY_AMOUNT / (1 + (arTaxCode.getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));

		            } else {

		            	NET_AMOUNT = APPLY_AMOUNT;

		            }

	                W_TAX_AMOUNT = EJBCommon.roundIt(NET_AMOUNT * (arInvoicePaymentSchedule.getArInvoice().getArWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

	                APPLY_AMOUNT -= W_TAX_AMOUNT;



	            }

         		mdetails.setIpsAiApplyAmount(APPLY_AMOUNT);
         		mdetails.setIpsAiPenaltyApplyAmount(PENALTY_APPLY_AMOUNT);
         		mdetails.setIpsAiCreditableWTax(W_TAX_AMOUNT);
        		mdetails.setIpsAiDiscountAmount(IPS_DSCNT_AMNT);
        		mdetails.setIpsAiRebate(IPS_REBATE);

        		list.add(mdetails);

        	}

        	if (list.isEmpty()) {

        		throw new GlobalNoRecordFoundException();

        	}

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }





    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArIpsByIpsCode(Integer IPS_CODE, boolean ENBL_RBT, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArReceiptEntryControllerBean getArIpsByIpsCode");

        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalAdDiscountHome adDiscountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;

        ArrayList list = new ArrayList();


        // Initialize EJB Home

        try {

            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	System.out.println("IPS_CODE="+IPS_CODE);
        	Collection arInvoicePaymentSchedules =  arInvoicePaymentScheduleHome.findOpenIpsByIpsLockAndIpsCode(EJBCommon.FALSE, IPS_CODE, AD_CMPNY);

        	System.out.println("arInvoicePaymentSchedules.size()"+arInvoicePaymentSchedules.size());



        	if (arInvoicePaymentSchedules.isEmpty()) {

        		throw new GlobalNoRecordFoundException();

        	}

        	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

        	Iterator i = arInvoicePaymentSchedules.iterator();

        	while (i.hasNext()) {

        		LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        		   (LocalArInvoicePaymentSchedule)i.next();

                // verification if ips is already closed
        		if (EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue(), precisionUnit) == EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid(), precisionUnit)) continue;

        		ArModInvoicePaymentScheduleDetails mdetails = new ArModInvoicePaymentScheduleDetails();

        		Collection arInvoiceLines = arInvoicePaymentSchedule.getArInvoice().getArInvoiceLines();
        		Iterator x = arInvoiceLines.iterator();
        		String reference = "";
        		while(x.hasNext()){
        			LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)x.next();
        			reference = arInvoiceLine.getArStandardMemoLine().getSmlName().replace("AR - ", "");
        			reference.replace("Association Dues", "ASD");
        			reference.replace("Parking Dues", "PD");
        			reference.replace("Water", "WTR");
        		}

        		mdetails.setIpsCode(arInvoicePaymentSchedule.getIpsCode());
        		mdetails.setIpsNumber(arInvoicePaymentSchedule.getIpsNumber());
        		mdetails.setIpsInvDate(arInvoicePaymentSchedule.getArInvoice().getInvDate());
        		mdetails.setIpsArCustomerCode(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode());
        		mdetails.setIpsDueDate(arInvoicePaymentSchedule.getIpsDueDate());
        		mdetails.setIpsAmountDue(arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid());
        		mdetails.setIpsPenaltyDue(arInvoicePaymentSchedule.getIpsPenaltyDue() - arInvoicePaymentSchedule.getIpsPenaltyPaid());

        		double interestDue =
        				arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEnableRebate() == EJBCommon.TRUE ? arInvoicePaymentSchedule.getIpsInterestDue() : 0d;
        		mdetails.setIpsInterestDue(interestDue);

        		mdetails.setIpsInvNumber(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
        		mdetails.setIpsInvReferenceNumber(reference + " "+ arInvoicePaymentSchedule.getArInvoice().getInvReferenceNumber());
        		mdetails.setIpsInvFcName(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName());
        		mdetails.setIpsAdBranch(arInvoicePaymentSchedule.getArInvoice().getInvAdBranch());

	             // calculate default tax withheld

	            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

	            double APPLY_AMOUNT = mdetails.getIpsAmountDue();
	            double PENALTY_APPLY_AMOUNT = mdetails.getIpsPenaltyDue();

	            double W_TAX_AMOUNT = 0d;

	            if (arInvoicePaymentSchedule.getArInvoice().getArWithholdingTaxCode().getWtcRate() != 0	&&
	                adPreference.getPrfArWTaxRealization().equals("COLLECTION")) {

	                LocalArTaxCode arTaxCode = arInvoicePaymentSchedule.getArInvoice().getArTaxCode();

	                double NET_AMOUNT = 0d;

	                if (arTaxCode.getTcType().equals("INCLUSIVE") || arTaxCode.getTcType().equals("EXCLUSIVE")) {

		    			NET_AMOUNT = EJBCommon.roundIt(APPLY_AMOUNT / (1 + (arTaxCode.getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));

		            } else {

		            	NET_AMOUNT = APPLY_AMOUNT;

		            }

	                W_TAX_AMOUNT = EJBCommon.roundIt(NET_AMOUNT * (arInvoicePaymentSchedule.getArInvoice().getArWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

	                APPLY_AMOUNT -= W_TAX_AMOUNT;



	            }

         		mdetails.setIpsAiApplyAmount(APPLY_AMOUNT);
         		mdetails.setIpsAiPenaltyApplyAmount(PENALTY_APPLY_AMOUNT);
         		mdetails.setIpsAiCreditableWTax(W_TAX_AMOUNT);
        		mdetails.setIpsAiDiscountAmount(0d);
        		mdetails.setIpsAiRebate(0d);

        		list.add(mdetails);

        	}

        	if (list.isEmpty()) {

        		throw new GlobalNoRecordFoundException();

        	}

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveArRctEntry(com.util.ArReceiptDetails details, boolean recalculateJournal, String BA_NM, String FC_NM, String CST_CSTMR_CODE,
    	String RB_NM, ArrayList aiList, boolean isDraft, boolean isValidating, String PP_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
    	ArReceiptEntryValidationException,
    	GlobalRecordAlreadyDeletedException,
		GlobalDocumentNumberNotUniqueException,
		GlobalConversionDateNotExistException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		ArINVOverCreditBalancePaidapplicationNotAllowedException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		ArINVOverapplicationNotAllowedException,
		GlobalTransactionAlreadyLockedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		ArRCTInvoiceHasNoWTaxCodeException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalBranchAccountNumberInvalidException,
		GlobalRecordAlreadyAssignedException,
		AdPRFCoaGlCustomerDepositAccountNotFoundException,
		ArREDuplicatePayfileReferenceNumberException{

        Debug.print("ArReceiptEntryControllerBean saveArRctEntry");

        LocalArReceiptHome arReceiptHome = null;
        LocalArReceiptBatchHome arReceiptBatchHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdBranchCustomerHome adBranchCustomerHome = null;
        LocalAdBranchBankAccountHome adBranchBankAccountHome = null;

        LocalHrPayrollPeriodHome hrPayrollPeriodHome = null;

        LocalArReceipt arReceipt = null;

        // Initialize EJB Home

        try {

        	hrPayrollPeriodHome = (LocalHrPayrollPeriodHome)EJBHomeFactory.
                    lookUpLocalHome(LocalHrPayrollPeriodHome.JNDI_NAME, LocalHrPayrollPeriodHome.class);

            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adBranchCustomerHome = (LocalAdBranchCustomerHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
            adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {


        	// validate if receipt is already deleted

        	try {

        		if (details.getRctCode() != null) {

        			arReceipt = arReceiptHome.findByPrimaryKey(details.getRctCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if receipt is already posted, void, approved or pending

	        if (details.getRctCode() != null && details.getRctVoid() == EJBCommon.FALSE) {

        		if (arReceipt.getRctApprovalStatus() != null) {

	        		if (arReceipt.getRctApprovalStatus().equals("APPROVED") ||
	        		    arReceipt.getRctApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (arReceipt.getRctApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (arReceipt.getRctPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (arReceipt.getRctVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}

        	// check void

	    	if (details.getRctCode() != null && details.getRctVoid() == EJBCommon.TRUE) {

	    		if (arReceipt.getRctVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

    			// check  if receipt is already deposited

	        	if (!arReceipt.getCmFundTransferReceipts().isEmpty()){

	        		throw new GlobalRecordAlreadyAssignedException ();

	        	}

        		if (arReceipt.getRctPosted() == EJBCommon.TRUE) {

		    	    // generate approval status

		    		String RCT_APPRVL_STATUS = null;




				    if (!isDraft) {

		        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

		        		// check if ar receipt approval is enabled

		        		if (adApproval.getAprEnableArReceipt() == EJBCommon.FALSE) {

		        			RCT_APPRVL_STATUS = "N/A";

		        		} else {

		        			// check if receipt is self approved

		        			LocalAdAmountLimit adAmountLimit = null;

		        			try {

		        				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR RECEIPT", "REQUESTER", details.getRctLastModifiedBy(), AD_CMPNY);

		        			} catch (FinderException ex) {

		        				throw new GlobalNoApprovalRequesterFoundException();

		        			}

		        			if (arReceipt.getRctAmount() <= adAmountLimit.getCalAmountLimit()) {

		        				RCT_APPRVL_STATUS = "N/A";

		        			} else {

		        				// for approval, create approval queue

		        				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AR RECEIPT", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

		        				 if (adAmountLimits.isEmpty()) {

		        				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

		        				 	if (adApprovalUsers.isEmpty()) {

		        				 		throw new GlobalNoApprovalApproverFoundException();

		        				 	}

		        				 	Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
		        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

		        				 } else {

		        				 	boolean isApprovalUsersFound = false;

		        				 	Iterator i = adAmountLimits.iterator();

		        				 	while (i.hasNext()) {

		        				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

		        				 		if (arReceipt.getRctAmount() <= adNextAmountLimit.getCalAmountLimit()) {

		        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

		        				 			Iterator j = adApprovalUsers.iterator();

				        				 	while (j.hasNext()) {

				        				 		isApprovalUsersFound = true;

				        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

				        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
				        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

				        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

				        				 	}

			    				 			break;

		        				 		} else if (!i.hasNext()) {

		        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

		        				 			Iterator j = adApprovalUsers.iterator();

				        				 	while (j.hasNext()) {

				        				 		isApprovalUsersFound = true;

				        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

				        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
				        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

				        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

				        				 	}

			    				 			break;

		        				 		}

		        				 		adAmountLimit = adNextAmountLimit;

		        				 	}

		        				 	if (!isApprovalUsersFound) {

		        				 		throw new GlobalNoApprovalApproverFoundException();

		        				 	}

		        			    }

		        			    RCT_APPRVL_STATUS = "PENDING";
		        			}
		        		}
		        	}



		    		// reverse distribution records

		    		Collection arDistributionRecords = arReceipt.getArDistributionRecords();
		    		ArrayList list = new ArrayList();

		    		Iterator i = arDistributionRecords.iterator();

		    		while (i.hasNext()) {

		    			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

		    			list.add(arDistributionRecord);

		    		}

		    		i = list.iterator();

		    		while (i.hasNext()) {

		    			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();
		    			System.out.println("Check Point A");
		    			this.addArDrEntry(arReceipt.getArDrNextLine(), arDistributionRecord.getDrClass(),
		    			    arDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
		    			    arDistributionRecord.getDrAmount(), EJBCommon.TRUE, arDistributionRecord.getGlChartOfAccount().getCoaCode(),
		    			    arReceipt, arDistributionRecord.getArAppliedInvoice(), AD_BRNCH, AD_CMPNY);

		    		}

		    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

		        	if (RCT_APPRVL_STATUS != null && RCT_APPRVL_STATUS.equals("N/A") && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

		        		arReceipt.setRctVoid(EJBCommon.TRUE);
		        		this.executeArRctPost(arReceipt.getRctCode(), details.getRctLastModifiedBy(), AD_BRNCH, AD_CMPNY);

		        	}

		        	// set void approval status

		    	    arReceipt.setRctVoidApprovalStatus(RCT_APPRVL_STATUS);

		        } else {

		        	// release invoice lock

		        	Collection arLockedAppliedInvoices = arReceipt.getArAppliedInvoices();

		        	Iterator aiIter = arLockedAppliedInvoices.iterator();

		        	while (aiIter.hasNext()) {

		        		LocalArAppliedInvoice arAppliedInvoice =
		        		    (LocalArAppliedInvoice) aiIter.next();

		        	    arAppliedInvoice.getArInvoicePaymentSchedule().setIpsLock(EJBCommon.FALSE);

		        	}

		        }

		        arReceipt.setRctVoid(EJBCommon.TRUE);
		        arReceipt.setRctLastModifiedBy(details.getRctLastModifiedBy());
        		arReceipt.setRctDateLastModified(details.getRctDateLastModified());

	    	    return arReceipt.getRctCode();

	    	}

        	// validate if document number is unique document number is automatic then set next sequence

	    	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

        	if (details.getRctCode() == null) {

        		String documentType = details.getRctDocumentType();
				
				try {
					if(documentType != null) {
						adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName(documentType , AD_CMPNY);
					}else {
						documentType = "AR RECEIPT";
					}
				} catch(FinderException ex) {
					documentType = "AR RECEIPT";
				}
				
            	
        		
        		
        		
        		try {

 					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);

 				} catch (FinderException ex) {

 				}

 				try {

 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

 				} catch (FinderException ex) {

 				}

	    		LocalArReceipt arExistingReceipt = null;

	    		try {
	    		System.out.println("rct num?" + details.getRctNumber());
	    		    arExistingReceipt = arReceiptHome.findByRctNumberAndBrCode(
	        		    details.getRctNumber(), AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	            if (arExistingReceipt != null) {
	            	System.out.println("erro rct num?" + details.getRctNumber());
	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

		        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
		            (details.getRctNumber() == null || details.getRctNumber().trim().length() == 0)) {

		            while (true) {

		            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

		            		try {

			            		arReceiptHome.findByRctNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

			            	} catch (FinderException ex) {

			            		details.setRctNumber(adDocumentSequenceAssignment.getDsaNextSequence());
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
					            break;

			            	}

		            	} else {

		            		try {

			            		arReceiptHome.findByRctNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

			            	} catch (FinderException ex) {

			            		details.setRctNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
					            break;

			            	}

		            	}

		            }

		        }

		    } else {

		    	LocalArReceipt arExistingReceipt = null;

		    	try {
	    		System.out.println("rct number? " + details.getRctNumber());

	    		    arExistingReceipt = arReceiptHome.findByRctNumberAndBrCode(
	        		    details.getRctNumber(), AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	        	if (arExistingReceipt != null &&
	                !arExistingReceipt.getRctCode().equals(details.getRctCode())) {
	        		System.out.println("error in " + details.getRctNumber());
	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

	            if (arReceipt.getRctNumber() != details.getRctNumber() &&
	                (details.getRctNumber() == null || details.getRctNumber().trim().length() == 0)) {

	                details.setRctNumber(arReceipt.getRctNumber());

	         	}

		    }


        	// validate if payfile Reference Number exist


        	if(details.getRctPayfileReferenceNumber()!=null){

        		if( !details.getRctPayfileReferenceNumber().equals("")){
        			try {

                		arReceiptHome.findByPayfileReferenceNumberAndCompanyCode(details.getRctPayfileReferenceNumber(), AD_CMPNY);

                		// if payfile referenceNumberFound
                		System.out.println("found duplicate payfile Ref Numb");
                		throw new ArREDuplicatePayfileReferenceNumberException();

                	} catch (FinderException ex) {



                	}
        		}




        	}



		    // validate if conversion date exists

	        try {

	      	    if (details.getRctConversionDate() != null) {


		        	  LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
		        	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		        	  if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

		        	  	LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
		        	  		glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
		        	  				details.getRctConversionDate(), AD_CMPNY);

		        	  } else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

		        	  	LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
		        	  		glFunctionalCurrencyRateHome.findByFcCodeAndDate(
		        	  				adCompany.getGlFunctionalCurrency().getFcCode(), details.getRctConversionDate(), AD_CMPNY);

		        	  }

		        }

	        } catch (FinderException ex) {

	        	 throw new GlobalConversionDateNotExistException();

	        }
	        System.out.println("pass 1");
        	//get remaining credit balance of customer and check if exceeds credibalance paid
	        double creditBalanceRemaining = this.getArRctCreditBalanceByCstCustomerCode(CST_CSTMR_CODE,AD_BRNCH , AD_CMPNY);

	        System.out.println("pass 2");
        	// used in checking if receipt should re-generate distribution records and re-calculate taxes
	        boolean isRecalculate = true;

	        // create new receipt
	        boolean newReceipt = false;
	        
	        
        	// create receipt
        	System.out.println("insert line herer");
        	if (details.getRctCode() == null) {

				arReceipt = arReceiptHome.create("COLLECTION", details.getRctDescription(),
				    details.getRctDate(), details.getRctNumber(),
				    details.getRctReferenceNumber(), details.getRctCheckNo(),details.getRctPayfileReferenceNumber(),
				    null,null,null,null,null,
				    details.getRctAmount(), details.getRctAmount(),0d,0d,0d, 0d, 0d,
				    details.getRctConversionDate(), details.getRctConversionRate(),
				    null, details.getRctPaymentMethod(), EJBCommon.FALSE, 0d, null, null, EJBCommon.FALSE,
				    null, EJBCommon.FALSE, EJBCommon.FALSE,
				    EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
				    null, null, null, null, null,
				    details.getRctCreatedBy(),details.getRctDateCreated(), details.getRctLastModifiedBy(),
				    details.getRctDateLastModified(), null, null, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE,
					EJBCommon.FALSE, null, null,
					EJBCommon.FALSE, EJBCommon.FALSE, null,
					AD_BRNCH, AD_CMPNY);


					arReceipt.setRctEnableAdvancePayment(details.getRctEnableAdvancePayment());
					arReceipt.setRctExcessAmount(details.getRctExcessAmount());



					recalculateJournal = true;
					newReceipt = true;

        	} else {

        		// check if critical fields are changed

        		if (!arReceipt.getAdBankAccount().getBaName().equals(BA_NM) ||
					!arReceipt.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE) ||
					aiList.size() != arReceipt.getArAppliedInvoices().size()) {
        			System.out.println("recalculate");
        			isRecalculate = true;

        		} else if (aiList.size() == arReceipt.getArAppliedInvoices().size()) {
        			System.out.println("loop get list");
        			Iterator aiIter = arReceipt.getArAppliedInvoices().iterator();
        			Iterator aiListIter = aiList.iterator();

        			while (aiIter.hasNext()) {
        				System.out.println("0.1");
        				LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)aiIter.next();
        				System.out.println("1");
        				ArModAppliedInvoiceDetails mdetails = (ArModAppliedInvoiceDetails)aiListIter.next();
        				System.out.println("1.1");
        				if (!arAppliedInvoice.getArInvoicePaymentSchedule().getIpsCode().equals(mdetails.getAiIpsCode()) ||
        				    arAppliedInvoice.getAiApplyAmount() != mdetails.getAiApplyAmount() ||
        				    arAppliedInvoice.getAiPenaltyApplyAmount() != mdetails.getAiPenaltyApplyAmount() ||
        				    arAppliedInvoice.getAiCreditableWTax() != mdetails.getAiCreditableWTax() ||
        				    arAppliedInvoice.getAiDiscountAmount() != mdetails.getAiDiscountAmount() ||
        				    arAppliedInvoice.getAiRebate() != mdetails.getAiRebate() ||
							arAppliedInvoice.getAiAppliedDeposit() != mdetails.getAiAppliedDeposit() ||
							arAppliedInvoice.getAiCreditBalancePaid() != mdetails.getAiCreditBalancePaid() ||
							arAppliedInvoice.getAiAllocatedPaymentAmount() != mdetails.getAiAllocatedPaymentAmount() )
        					{

        					isRecalculate = true;
        					break;

        				}

        				isRecalculate = false;

        			}
        			System.out.println("loop get list end");
        		} else {

        			isRecalculate = false;

        		}

        		arReceipt.setRctDescription(details.getRctDescription());
        		arReceipt.setRctDocumentType(details.getRctDocumentType());
        		arReceipt.setRctDate(details.getRctDate());
        		arReceipt.setRctNumber(details.getRctNumber());
        		arReceipt.setRctReferenceNumber(details.getRctReferenceNumber());
        		arReceipt.setRctCheckNo(details.getRctCheckNo());
        		arReceipt.setRctAmount(details.getRctAmount());
        		arReceipt.setRctAmountCash(details.getRctAmount());
        		arReceipt.setRctConversionDate(details.getRctConversionDate());
        		arReceipt.setRctConversionRate(details.getRctConversionRate());
        	    arReceipt.setRctPaymentMethod(details.getRctPaymentMethod());
        		arReceipt.setRctLastModifiedBy(details.getRctLastModifiedBy());
        		arReceipt.setRctDateLastModified(details.getRctDateLastModified());
        		arReceipt.setRctReasonForRejection(null);
        		arReceipt.setRctCustomerName(null);
        		arReceipt.setRctCustomerAddress(null);
        		arReceipt.setRctEnableAdvancePayment(details.getRctEnableAdvancePayment());
				arReceipt.setRctExcessAmount(details.getRctExcessAmount());
        	}

        	arReceipt.setRctDocumentType(details.getRctDocumentType());
        	arReceipt.setReportParameter(details.getReportParameter());
        	System.out.println("c1");
        	LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
        	//adBankAccount.addArReceipt(arReceipt);
        	arReceipt.setAdBankAccount(adBankAccount);

        	System.out.println("c2");
        	LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
        	System.out.println("c3");
        	//glFunctionalCurrency.addArReceipt(arReceipt);
        	arReceipt.setGlFunctionalCurrency(glFunctionalCurrency);

        	try {

                LocalHrPayrollPeriod hrPayrollPeriod = hrPayrollPeriodHome.findByPpName(PP_NM, AD_CMPNY);

                arReceipt.setHrPayrollPeriod(hrPayrollPeriod);

            } catch (FinderException ex) {

            }



        	System.out.println("LocalGlFunctionalCurrency glFunctionalCurrency="+FC_NM);
        	System.out.println("c4");
        	LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
        	arReceipt.setArCustomer(arCustomer);
        	//arCustomer.addArReceipt(arReceipt);

        	System.out.println("enter");
        	System.out.println(details.getRctCustomerName() + "this");
        	if(details.getRctCustomerName().length() > 0 && !arCustomer.getCstName().equals(details.getRctCustomerName())) {
				arReceipt.setRctCustomerName(details.getRctCustomerName());
			}

        	try {

        		LocalArReceiptBatch arReceiptBatch = arReceiptBatchHome.findByRbName(RB_NM, AD_BRNCH, AD_CMPNY);
        		arReceipt.setArReceiptBatch(arReceiptBatch);
        		//arReceiptBatch.addArReceipt(arReceipt);

        	} catch (FinderException ex) {

        	}

        	if (isRecalculate) {

	        	// remove all distribution records

		  	    Collection arDistributionRecords = arReceipt.getArDistributionRecords();

		  	    Iterator i = arDistributionRecords.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

		  	   	  if(recalculateJournal) {
			  	   	   i.remove();
	
			  	  	    arDistributionRecord.remove();
		  	   	    }

		  	    }




	        	// release ips locks and remove all applied invoices

		  	    Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();

		  	    i = arAppliedInvoices.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

		  	   	    arAppliedInvoice.getArInvoicePaymentSchedule().setIpsLock(EJBCommon.FALSE);



		  	   	    // remove all applied credits
			      	try {

			      		Collection arAppliedCredits = arAppliedInvoice.getArAppliedCredits();

				  	    Iterator x = arAppliedCredits.iterator();

				  	    while (x.hasNext()) {

				  	   	    LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)x.next();

				  	  	    x.remove();

				  	  	    arAppliedCredit.remove();

				  	    }

			      	}catch(Exception ex){

			      	}



			  	    i.remove();

		  	   	    arAppliedInvoice.remove();



		  	    }




		  	    double totalScAmount = 0;
		  	    double totalAdvancePayment = 0;
		  	    double totalCreditBalancePaid = 0;
		  	    double totalApplyAmount = 0;
		  	    double RCT_FRX_GN_LSS = 0d;

		  	    double INVC_CONVERSION_RT = 0d;
		  	    double RCT_CONVERSION_RT = 0d;
		  	  LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
		  	    LocalArAppliedInvoice arAppliedInvoice = null;
		  	    // add new applied vouchers and distribution record

	      	    i = aiList.iterator();

	      	    while (i.hasNext()) {
	      	    	
	      	    	
	      	    	
	      	  	    ArModAppliedInvoiceDetails mAiDetails = (ArModAppliedInvoiceDetails) i.next();
	      	  	    totalApplyAmount +=  mAiDetails.getAiApplyAmount() + mAiDetails.getAiPenaltyApplyAmount();
///test run

	      	  	    arAppliedInvoice = this.addArAiEntry(mAiDetails, arReceipt, AD_CMPNY);
	      	  	    
	      	  	    LocalGlFunctionalCurrency invcFC = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency();
	      	  	    
	      	  	    LocalGlFunctionalCurrency rctFC = arReceipt.getGlFunctionalCurrency();
		      	  	INVC_CONVERSION_RT = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate();
		      	    RCT_CONVERSION_RT = arReceipt.getRctConversionRate();

		      	    
		      	  RCT_FRX_GN_LSS += arAppliedInvoice.getAiForexGainLoss();
		      	     // create cred. withholding tax distribution record if necessary

	      	  	    if (mAiDetails.getAiCreditableWTax() > 0) {

		      	  	    Integer WTC_COA_CODE = null;
		      	 
		      	  	   double CREDITABLE_WTAX = this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
		      	  			invcFC.getFcName(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
		                    arAppliedInvoice.getAiCreditableWTax(), AD_CMPNY);

		      	  	    if (arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArWithholdingTaxCode().getGlChartOfAccount() != null) {

		      	  	    	WTC_COA_CODE = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArWithholdingTaxCode().getGlChartOfAccount().getCoaCode();

		      	  	    } else {

		      	  	    	adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
		      	  	    	WTC_COA_CODE = adPreference.getArWithholdingTaxCode().getGlChartOfAccount().getCoaCode();

		      	  	    }
		      	  	    System.out.println("Check Point B");
		      	  	    
		      	  	    if(recalculateJournal) {
		      	  	    this.addArDrEntry(arReceipt.getArDrNextLine(), "W-TAX", EJBCommon.TRUE,
		      	  	    		CREDITABLE_WTAX, EJBCommon.FALSE,
				      	        WTC_COA_CODE, arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
		      	  	    }

	      	  	    	

	      	  	    }

	      	  	   /* // create discount distribution records if necessary

	      	  	    if (arAppliedInvoice.getAiDiscountAmount() != 0) {

	      	  	    	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

	      	  	    	// get discount percent

			        	double DISCOUNT_PERCENT = EJBCommon.roundIt(arAppliedInvoice.getAiDiscountAmount() / (arAppliedInvoice.getAiApplyAmount()+ arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiAppliedDeposit()), (short)6);

			        	System.out.println("arAppliedInvoice.getAiDiscountAmount()="+arAppliedInvoice.getAiDiscountAmount());
			        	System.out.println("arAppliedInvoice.getAiApplyAmount()="+arAppliedInvoice.getAiApplyAmount());
			        	System.out.println("1DISCOUNT_PERCENT"+DISCOUNT_PERCENT);
			        	DISCOUNT_PERCENT = EJBCommon.roundIt(DISCOUNT_PERCENT * ((arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiAppliedDeposit()) / arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvAmountDue()), (short)6);
			        	System.out.println("arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvAmountDue()="+arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvAmountDue());
			        	System.out.println("2DISCOUNT_PERCENT"+DISCOUNT_PERCENT);
			        	Collection arDiscountDistributionRecords = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArDistributionRecords();

			        	// get total debit and credit for rounding difference calculation

			        	double TOTAL_DEBIT = 0d;
			        	double TOTAL_CREDIT = 0d;
			        	boolean isRoundingDifferenceCalculated = false;

			        	Iterator j = arDiscountDistributionRecords.iterator();

			        	while (j.hasNext()) {

			        		LocalArDistributionRecord arDiscountDistributionRecord = (LocalArDistributionRecord)j.next();

			        		if (arDiscountDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

			        			TOTAL_DEBIT += EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);

			        		} else {

			        			TOTAL_CREDIT += EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);

			        		}

			        	}


			        	j = arDiscountDistributionRecords.iterator();

			        	while (j.hasNext()) {

			        		LocalArDistributionRecord arDiscountDistributionRecord = (LocalArDistributionRecord)j.next();

			        		if (arDiscountDistributionRecord.getDrClass().equals("RECEIVABLE")) continue;

			        		double DR_AMNT = EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);
			        		System.out.println("DR_AMNT"+DR_AMNT);
			        		// calculate rounding difference if necessary

			        		if (arDiscountDistributionRecord.getDrDebit() == EJBCommon.FALSE &&
			        		    TOTAL_DEBIT != TOTAL_CREDIT && !isRoundingDifferenceCalculated) {

			        		    DR_AMNT = DR_AMNT +  TOTAL_DEBIT - TOTAL_CREDIT;

			        		    isRoundingDifferenceCalculated = true;

			        		}


			        		if (arDiscountDistributionRecord.getDrClass().equals("REVENUE")) {
			        			// add branch bank account
			      	      	  LocalAdBranchBankAccount adBranchBankAccount = null;

			                      try {
			                          adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(arReceipt.getAdBankAccount().getBaCode(), AD_BRNCH, AD_CMPNY);

			                      } catch(FinderException ex) {

			                      }

			        			System.out.println("Check Point C");
				        		this.addArDrEntry(arReceipt.getArDrNextLine(), "SALES DISCOUNT",
				        		    EJBCommon.FALSE,
				        		    DR_AMNT,
				        		    EJBCommon.FALSE,
				        		    adBranchBankAccount.getBbaGlCoaSalesDiscountAccount(), arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);

				        		System.out.println("Check Point D");
			        			this.addArDrEntry(arReceipt.getArDrNextLine(), arDiscountDistributionRecord.getDrClass(),
				        		    EJBCommon.TRUE,
				        		    DR_AMNT,
				        		    EJBCommon.FALSE,
				        		    arDiscountDistributionRecord.getGlChartOfAccount().getCoaCode(), arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);

			        		} else {

			        			System.out.println("Check Point D");
			        			this.addArDrEntry(arReceipt.getArDrNextLine(), arDiscountDistributionRecord.getDrClass(),
				        		    arDiscountDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
				        		    DR_AMNT,
				        		    EJBCommon.FALSE,
				        		    arDiscountDistributionRecord.getGlChartOfAccount().getCoaCode(), arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);

			        		}

			        	}
	      	  	    }*/






	      	  	    // create discount distribution records if necessary

	      	  	    if (arAppliedInvoice.getAiDiscountAmount() != 0) {

	      	  	   
	      	  	    	
	      	  	    
	      	  	 double DISCOUNT_AMOUNT = this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
		      	  			invcFC.getFcName(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
		                    arAppliedInvoice.getAiDiscountAmount(), AD_CMPNY);
			      	  	    LocalAdBranchBankAccount adBranchBankAccount = null;

		                    try {
		                        adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(arReceipt.getAdBankAccount().getBaCode(), AD_BRNCH, AD_CMPNY);

		                    } catch(FinderException ex) {

		                    }

		      			System.out.println("Check Point C");
		      			
		      			if(recalculateJournal) {
			        		this.addArDrEntry(arReceipt.getArDrNextLine(), "SALES DISCOUNT",
			        		    EJBCommon.TRUE,
			        		    DISCOUNT_AMOUNT,
			        		    EJBCommon.FALSE,
			        		    adBranchBankAccount.getBbaGlCoaSalesDiscountAccount(), arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);

		      			}
	      	  	    }




// create applied deposit distribution records if necessary

	      	  	    if (arAppliedInvoice.getAiCreditBalancePaid() != 0) {

	      	  	 // add branch bank account
	      	      	  LocalAdBranchBankAccount adBranchBankAccount = null;

	      	      	  
	      	      	  
	      	        double CREDIT_BALANCE_PAID = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
		      	  			arReceipt.getGlFunctionalCurrency().getFcName(),
		      	  			arReceipt.getRctConversionDate(),
		      	  			arReceipt.getRctConversionRate(),
		      	  		 arAppliedInvoice.getAiCreditBalancePaid(), AD_CMPNY);
	      	        
	      	        
	      	        
	      	      
		      	  	
	      	        
	                      try {
	                          adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(arReceipt.getAdBankAccount().getBaCode(), AD_BRNCH, AD_CMPNY);

	                      } catch(FinderException ex) {

	                      }
	                      if(recalculateJournal) {
	      	  	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "CREDITED BALANCE",
	      	  	    			EJBCommon.TRUE,
	      	  	    		CREDIT_BALANCE_PAID,
								EJBCommon.FALSE,
								adBranchBankAccount.getBbaGlCoaAdvanceAccount(), arReceipt,
								arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	                      }

	      	  	    }

	      	  	    // create applied deposit distribution records if necessary

	      	  	    if (arAppliedInvoice.getAiAppliedDeposit() != 0) {

	      	  	    	
	      	  	  	  
		      	     
		      	        
		      	      double APPLIED_DEPOSIT =  this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
			      	  			invcFC.getFcName(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
			                    arAppliedInvoice.getAiAppliedDeposit(), AD_CMPNY);
	      	  	    	
	      	  	    	adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

	      	  	    	if(adPreference.getPrfArGlCoaCustomerDepositAccount() == null)
	      	  	    		throw new AdPRFCoaGlCustomerDepositAccountNotFoundException();
	      	  	    System.out.println("Check Point E");
	      	  	    
	      	  	    if(recalculateJournal) {
	      	  	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "APPLIED DEPOSIT",
	      	  	    			EJBCommon.TRUE,
	      	  	    		APPLIED_DEPOSIT,
								EJBCommon.FALSE,
								adPreference.getPrfArGlCoaCustomerDepositAccount(), arReceipt,
								arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	      	  	    }

	      	  	    }

	      	  	    // Get Service Charge Records
			  	    Collection arScDistributionRecords =
		  	  	        arDistributionRecordHome.findDrsByDrClassAndInvCode("SC", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);

			  	    Iterator scIter = arScDistributionRecords.iterator();

			  	    
			  	
			  	    double applyAmount = arAppliedInvoice.getAiApplyAmount()+ arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiAppliedDeposit();
		  	    	double dueAmount = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvAmountDue();

		  	    
		  	    	
		  	    	applyAmount=  this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
			      	  			invcFC.getFcName(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
			      	  		applyAmount, AD_CMPNY);
		  	    	
		  	    	
		  	    	
		  	    	
		  	    	dueAmount=  this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
		      	  			invcFC.getFcName(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
		      	  		dueAmount, AD_CMPNY);
			  	    
		  	    	
		  	    	
		  	    	double scAmount = 0;

			  	    while(scIter.hasNext()){

			  	    	LocalArDistributionRecord arScDistributionRecord = (LocalArDistributionRecord)scIter.next();
			  	    	
			  	    	
			  	    	
			  	    	
			  	    	double scDrAmount=  this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
			      	  			invcFC.getFcName(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
			      	  		arScDistributionRecord.getDrAmount(), AD_CMPNY);
			  	    	
			  	    	//scAmount += arScDistributionRecord.getDrAmount() * (applyAmount/dueAmount);
			  	    	
			  	    	scAmount += scDrAmount* (applyAmount/dueAmount);
			  	    	
			  	    	System.out.println("Check Point F");
			  	    	if(recalculateJournal) {
				  	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE",
				  	    			EJBCommon.FALSE,scAmount,EJBCommon.FALSE,
				  	    			arScDistributionRecord.getDrScAccount(), arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
				  	    	System.out.println("Check Point G");
				  	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "SC",
				  	    			EJBCommon.TRUE, scAmount, EJBCommon.FALSE,
				  	    			arScDistributionRecord.getGlChartOfAccount().getCoaCode(), arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
			  	    	}

			  	    }

			  	    totalScAmount += scAmount;


			  	    // create rebate distribution records if necessary

	      	  	    if (arAppliedInvoice.getAiRebate() != 0 ) {

	      	  	    LocalArDistributionRecord arDistributionRecord =
			  	  	        arDistributionRecordHome.findByDrClassAndInvCode("UNINTEREST", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);

	      	  	    LocalArDistributionRecord arDistributionRecordReceivable =
	      	  	    		arDistributionRecordHome.findByDrClassAndInvCode("RECEIVABLE INTEREST", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);


	      	  	    	if(recalculateJournal) {
	      	  	    		
	      	  	  
	      	  	    		
	      	  	    double REBATE_AMOUNT=  this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
		      	  			invcFC.getFcName(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
		      	  		 arAppliedInvoice.getAiRebate(), AD_CMPNY);	
	      	  	    		
	      	  	    		
	      	  	    		
	      	  	    		System.out.println("Check Point C");
			        		this.addArDrEntry(arReceipt.getArDrNextLine(), "REBATE",
			        		    EJBCommon.TRUE,
			        		    REBATE_AMOUNT,
			        		    EJBCommon.FALSE,
			        		    arDistributionRecord.getGlChartOfAccount().getCoaCode(), arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);


			        		this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE REBATE",
			        				EJBCommon.FALSE,
			        				REBATE_AMOUNT ,
					  	  	        EJBCommon.FALSE,
					  	  	        arDistributionRecordReceivable.getGlChartOfAccount().getCoaCode(),
					  	  	        arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	      	  	    	}

	      	  	    } else {

	      	  	    // earned interest
				  	    try {


				  	    	LocalAdBranchBankAccount adBranchBankAccount = null;

			                try {
			                    adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(arReceipt.getAdBankAccount().getBaCode(), AD_BRNCH, AD_CMPNY);

			                } catch(FinderException ex) {

			                }
					  	  	 LocalArDistributionRecord arDistributionRecord =
					  	  	        arDistributionRecordHome.findByDrClassAndInvCode("RECEIVABLE INTEREST", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);
					  	  	System.out.println("Check Point H");

					  	  	
					  	  	
					  	 
		      	  	    		
					  	  	
					  	  double dueInterest = arAppliedInvoice.getArInvoicePaymentSchedule().getIpsInterestDue();
					  	double dueAmountMonthly = arAppliedInvoice.getArInvoicePaymentSchedule().getIpsAmountDue();
					  	
					 
					  	
					  	dueInterest =  this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
			      	  			invcFC.getFcName(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
			      	  		dueInterest, AD_CMPNY);	
					  	
					  	
					  

					  	
					  	dueAmountMonthly =  this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
			      	  			invcFC.getFcName(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
			      	  		dueAmountMonthly, AD_CMPNY);
					  	
					  	
					  	  System.out.println("dueInterest="+dueInterest);
					  		LocalAdBranchCustomer adBranchCustomer =
		                			adBranchCustomerHome.findBcstByCstCodeAndBrCode(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

					  		
					  		if(recalculateJournal) {
					  		
					  	  	    this.addArDrEntry(arReceipt.getArDrNextLine(), "UNINTEREST", EJBCommon.TRUE,
					  	  	    		(applyAmount/dueAmountMonthly) * dueInterest, EJBCommon.FALSE, adBranchCustomer.getBcstGlCoaUnEarnedInterestAccount(),
					  	  	        	arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	
						  	  	this.addArDrEntry(arReceipt.getArDrNextLine(), "EARNED INTEREST", EJBCommon.FALSE,
						  	  			(applyAmount/dueAmountMonthly) * dueInterest, EJBCommon.FALSE, adBranchCustomer.getBcstGlCoaEarnedInterestAccount(),
										arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	
	
						  	  	this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE INTEREST", EJBCommon.FALSE,
				        				(applyAmount/dueAmountMonthly) * dueInterest,EJBCommon.FALSE,
						  	  	        arDistributionRecord.getGlChartOfAccount().getCoaCode(),
						  	  	        arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	
						  	  	this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE,
						  	  			(applyAmount/dueAmountMonthly) * dueInterest, EJBCommon.FALSE, adBranchBankAccount.getBbaGlCoaCashAccount(),
						  	  			arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
					  		}
				  	    } catch (FinderException ex) {


				  	    }

	      	  	    }


	      	  	// earned penalty
	      	  	    System.out.println("arAppliedInvoice.getAiPenaltyApplyAmount()="+arAppliedInvoice.getAiPenaltyApplyAmount());
	      	  	if (arAppliedInvoice.getAiPenaltyApplyAmount() != 0 ) {

	      	  	try {



			  	  		double applyPenaltyDue = arAppliedInvoice.getAiPenaltyApplyAmount();
			  	  		
			  	
			  	  	
			  	  applyPenaltyDue =  this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
		      	  			invcFC.getFcName(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
		      	  		applyPenaltyDue, AD_CMPNY);
			  	  		

				  		LocalAdBranchCustomer adBranchCustomer =
	               			adBranchCustomerHome.findBcstByCstCodeAndBrCode(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

				  		if(recalculateJournal) {
				  		
				  	  	    this.addArDrEntry(arReceipt.getArDrNextLine(), "UNPENALTY", EJBCommon.TRUE,
				  	  	    		applyPenaltyDue,EJBCommon.FALSE, adBranchCustomer.getBcstGlCoaUnEarnedPenaltyAccount(),
				  	  	        arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	
					  	  	this.addArDrEntry(arReceipt.getArDrNextLine(), "EARNED PENALTY", EJBCommon.FALSE,
					  	  		applyPenaltyDue, EJBCommon.FALSE, adBranchCustomer.getBcstGlCoaEarnedPenaltyAccount(),
									arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	 
					  	  	this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE PENALTY", EJBCommon.FALSE,
					  	  	    	arAppliedInvoice.getAiPenaltyApplyAmount(),EJBCommon.FALSE, adBranchCustomer.getBcstGlCoaReceivableAccount(),
					  	  	        arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
				  		}

			  	    } catch (FinderException ex) {


			  	    }

	      	  	}



	      	  	    // get receivable account TODO: ALWAYS GETTING ERROR BELOW
	      	  	    try{

	      	  	    	System.out.println("first inv:" + arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode());
	      	  	    LocalArDistributionRecord arDistributionRecord =
			  	  	        arDistributionRecordHome.findByDrClassAndInvCode("RECEIVABLE", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);
			  	  	System.out.println("Check Point H");

			  	
			  	  	
			  	  double amount_crd =  this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
		      	  			invcFC.getFcName(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
		      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
		      	  		arAppliedInvoice.getAiApplyAmount()+ arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiCreditableWTax() +
		  	  	        arAppliedInvoice.getAiAppliedDeposit() + arAppliedInvoice.getAiDiscountAmount(), AD_CMPNY);
			  	  	
			  	  	
			  	  	
				  	  if(recalculateJournal) {
				  	  	    this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE", EJBCommon.FALSE,
				  	  	    	amount_crd,EJBCommon.FALSE,
				  	  	        arDistributionRecord.getGlChartOfAccount().getCoaCode(),
				  	  	        arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	
				  	  }
	      	  	    } catch (Exception ex){/*
	      	  	    	System.out.println("error:" + ex.toString());
	      	  	    	System.out.println("MULTIPLE RECEIVABLE");

		      	  	    Collection arDrReceivables = arDistributionRecordHome.findDrsByDrClassAndInvCode("RECEIVABLE", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);
		      	  	    Iterator x = arDrReceivables.iterator();
		      	  	    double reamingAppliedAmount = arAppliedInvoice.getAiApplyAmount();

		      	  	    double amountRemaining = 0d;
		      	  	    double totalInvAmount = 0d;
		      	  	    double totalPaid = 0d;
		      	  	    double amountDue = 0d;
		      	  	    double distributionAmount = 0d;
		      	  	    System.out.println("reamingAppliedAmount="+reamingAppliedAmount);

		      	  	    //get total invoice amount
			      	  	while(x.hasNext()){
			      	  		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)x.next();
			      	  		totalInvAmount += EJBCommon.roundIt(arDistributionRecord.getDrAmount(), this.getGlFcPrecisionUnit(AD_CMPNY));
			      	  	}
			      	   totalInvAmount = EJBCommon.roundIt(totalInvAmount, this.getGlFcPrecisionUnit(AD_CMPNY));

			      	  	System.out.println("tsad");
		      	  	    //get total paid
			      	  	amountDue =  mAiDetails.getAiIpsAmountDue();
			      	  	totalPaid =   EJBCommon.roundIt(totalInvAmount -  amountDue, this.getGlFcPrecisionUnit(AD_CMPNY));


			      	  	amountRemaining = reamingAppliedAmount;
			      	  	x = arDrReceivables.iterator();

		      	  	    while(x.hasNext()){
		      	  	    	LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)x.next();
		      	  	    	System.out.println("-------->arDistributionRecord.getDrAmount()="+arDistributionRecord.getDrAmount());
		      	  	    	System.out.println("-------->reamingAppliedAmount="+reamingAppliedAmount);
		      	  	    	System.out.println("-------->amount remaining=" + amountRemaining);
		      	  	    	System.out.println(" the amount due is : " + amountDue);
		      	  	    	System.out.println(" the total paid is : " + totalPaid);

		      	  	    	if(amountRemaining <= 0 ){
		      	  	    		System.out.println("break");
		      	  	    		break;
		      	  	    	}

		      	  	        distributionAmount =  EJBCommon.roundIt(arDistributionRecord.getDrAmount()- totalPaid, this.getGlFcPrecisionUnit(AD_CMPNY));
		      	  	        if(distributionAmount <= 0){
		      	  	        	totalPaid -=arDistributionRecord.getDrAmount();
		      	  	        	continue;
		      	  	        }
		      	  	    	if(amountRemaining >= distributionAmount){
			      	  	    	System.out.println("Still Remain");

			      	  	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE", EJBCommon.FALSE,
			      	  	    		    distributionAmount ,
						  	  	        EJBCommon.FALSE,
						  	  	        arDistributionRecord.getGlChartOfAccount().getCoaCode(),
						  	  	        arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);


			      	  	    	System.out.println("amount remain:" + amountRemaining);

		      	  	    	}else{
		      	  	    		System.out.println("zero remaining");
		      	  	    		System.out.println("dr amount: " + arDistributionRecord.getDrAmount() + "  and amount rem: "+ amountRemaining);
			      	  	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE", EJBCommon.FALSE,
			      	  	    					amountRemaining ,
								  	  	        EJBCommon.FALSE,
								  	  	        arDistributionRecord.getGlChartOfAccount().getCoaCode(),
								  	  	        	arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
			      	  	    }
		      	  	    System.out.println("finished");

		      	  	    	System.out.println("the total paid is: " + totalPaid);
		      	  	    	System.out.println("the distribute : " + distributionAmount);
		      	  	        System.out.println("the amount remaining : " +amountRemaining);

		      	  	    	amountRemaining -= distributionAmount	;
		      	  	        System.out.println("the amount remaining now  : " +amountRemaining);
		      	  	    	totalPaid = 0;

		      	  	    }




		      	  	    	*/


	      	  	    }



		  	  	    // reverse deferred tax if necessary

		  	  	    LocalArTaxCode arTaxCode = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArTaxCode();

		  	  	    if (!arTaxCode.getTcType().equals("NONE") &&
		  	  	        !arTaxCode.getTcType().equals("EXEMPT") &&
						arTaxCode.getTcInterimAccount() != null) {

		  	  	    	try {

		  	  	    		LocalArDistributionRecord arDeferredDistributionRecord =
		  	  	    			arDistributionRecordHome.findByDrClassAndInvCode("DEFERRED TAX", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);

		  	  	    		double DR_AMNT =
		  	  	    			EJBCommon.roundIt((arDeferredDistributionRecord.getDrAmount() / arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvAmountDue()) *
		  	  	    					(arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiAppliedDeposit()), this.getGlFcPrecisionUnit(AD_CMPNY));
		  	  	    	
		  	  	    		
		  	  	    		
		  	  	    	DR_AMNT =  this.convertForeignToFunctionalCurrency(invcFC.getFcCode(),
			      	  			invcFC.getFcName(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
			      	  			arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
			      	  		DR_AMNT, AD_CMPNY);
				  	  	
		  	  	    		
		  	  	    		
		  	  	    		if(recalculateJournal) {
			  	  	    		System.out.println("Check Point I");
			  	  	    		this.addArDrEntry(arReceipt.getArDrNextLine(), "DEFERRED TAX", EJBCommon.TRUE,
			  	  	    				DR_AMNT, EJBCommon.FALSE,
										arDeferredDistributionRecord.getGlChartOfAccount().getCoaCode(),
										arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
	
	
				  	  	    	System.out.println("Check Point J");
				  	  	    		this.addArDrEntry(arReceipt.getArDrNextLine(), "TAX", EJBCommon.FALSE,
				  	  	    				DR_AMNT, EJBCommon.FALSE,
											arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArTaxCode().getGlChartOfAccount().getCoaCode(),
											arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
			  	  	    	}


		  	  	    	} catch (FinderException ex) {

		  	  	    	}

		  	  	    }


	      	    }
	      	    
	      	    
	      	    
	      	  if(RCT_CONVERSION_RT > INVC_CONVERSION_RT) {

	            	this.addArDrEntry(arReceipt.getArDrNextLine(), "FOREX GAIN", EJBCommon.TRUE,
	            			Math.abs(RCT_FRX_GN_LSS), EJBCommon.FALSE, adPreference.getPrfMiscPosGiftCertificateAccount(),
	            			arReceipt, null, AD_BRNCH, AD_CMPNY);
	            }

	            if(INVC_CONVERSION_RT > RCT_CONVERSION_RT) {

	            	this.addArDrEntry(arReceipt.getArDrNextLine(), "FOREX LOSS", EJBCommon.FALSE,
	            			Math.abs(RCT_FRX_GN_LSS), EJBCommon.FALSE, adPreference.getPrfMiscPosGiftCertificateAccount(),
	            			arReceipt, null, AD_BRNCH, AD_CMPNY);
	            }


	      	    
	      	    
	      	    
	      	    
	      	    
	      	    
	      	    
	      	    
	      	    // create cash distribution record



	      	// add branch bank account
	      	  LocalAdBranchBankAccount adBranchBankAccount = null;

                try {
                    adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(adBankAccount.getBaCode(), AD_BRNCH, AD_CMPNY);

                } catch(FinderException ex) {

                }

                if(recalculateJournal) {
		      	    System.out.println("-------->totalApplyAmount()="+totalApplyAmount);
			  	    if(totalApplyAmount!= 0){
			  	    	
			  	    	
			  			
			  	    	totalApplyAmount =  this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
		  	  	    		arReceipt.getGlFunctionalCurrency().getFcName(),
		  	  	    		arReceipt.getRctConversionDate(),
		  	  	    		arReceipt.getRctConversionRate(),
		  	  	    	totalApplyAmount, AD_CMPNY);
			  	    	
			  	    	
			  	    	
			  	    	System.out.println("Check Point K");
			  	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE,
			  	    			totalApplyAmount, EJBCommon.FALSE, adBranchBankAccount.getBbaGlCoaCashAccount(),
							arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);
			  	    }
                }
      	    }


      	    // generate approval status

            String RCT_APPRVL_STATUS = null;

        	if (!isDraft) {

        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        		// check if ar receipt approval is enabled

        		if (adApproval.getAprEnableArReceipt() == EJBCommon.FALSE) {

        			RCT_APPRVL_STATUS = "N/A";

        			//create cm adjustment for inserting advance payment
        			  if(details.getRctEnableAdvancePayment()!=0){

        		        this.postAdvncPymntByCmAdj(arReceipt ,BA_NM, AD_BRNCH, AD_CMPNY);


        			  }


        		} else {

        			// check if receipt is self approved

        			LocalAdAmountLimit adAmountLimit = null;

        			try {

        				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR RECEIPT", "REQUESTER", details.getRctLastModifiedBy(), AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalNoApprovalRequesterFoundException();

        			}

        			if (arReceipt.getRctAmount() <= adAmountLimit.getCalAmountLimit()) {

        				RCT_APPRVL_STATUS = "N/A";

        			} else {

        				// for approval, create approval queue

        				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AR RECEIPT", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

        				 if (adAmountLimits.isEmpty()) {

        				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 	if (adApprovalUsers.isEmpty()) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        				 	Iterator j = adApprovalUsers.iterator();

        				 	while (j.hasNext()) {

        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

        				 	}

        				 } else {

        				 	boolean isApprovalUsersFound = false;

        				 	Iterator i = adAmountLimits.iterator();

        				 	while (i.hasNext()) {

        				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

        				 		if (arReceipt.getRctAmount() <= adNextAmountLimit.getCalAmountLimit()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
		        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		} else if (!i.hasNext()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
		        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		}

        				 		adAmountLimit = adNextAmountLimit;

        				 	}

        				 	if (!isApprovalUsersFound) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        			    }

        			    RCT_APPRVL_STATUS = "PENDING";
        			}
        		}
        	}

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (RCT_APPRVL_STATUS != null && RCT_APPRVL_STATUS.equals("N/A") && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		this.executeArRctPost(arReceipt.getRctCode(), arReceipt.getRctLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}

        	// set receipt approval status

        	arReceipt.setRctApprovalStatus(RCT_APPRVL_STATUS);

        	 System.out.println("enter xx");
	      	if(isValidating){
	      		  throw new ArReceiptEntryValidationException();
                 }



       	      return arReceipt.getRctCode();

        } catch (ArReceiptEntryValidationException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ArREDuplicatePayfileReferenceNumberException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalConversionDateNotExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ArINVOverapplicationNotAllowedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ArINVOverCreditBalancePaidapplicationNotAllowedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;
        } catch (ArRCTInvoiceHasNoWTaxCodeException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyLockedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

         } catch (GlobalNoApprovalRequesterFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

		} catch (GlobalRecordAlreadyAssignedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (AdPRFCoaGlCustomerDepositAccountNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }


    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteArRctEntry(Integer RCT_CODE, String AD_USR, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException {

        Debug.print("ArReceiptEntryControllerBean deleteArRctEntry");

        LocalArReceiptHome arReceiptHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home

        try {

            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);
			adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

        	if (arReceipt.getRctApprovalStatus() != null && arReceipt.getRctApprovalStatus().equals("PENDING")) {

        		Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR RECEIPT", arReceipt.getRctCode(), AD_CMPNY);

        		Iterator i = adApprovalQueues.iterator();

        		while(i.hasNext()) {

        			LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

        			adApprovalQueue.remove();

        		}

        	}

        	// release lock and remove applied invoice

      	    Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();

  	   		Iterator i = arAppliedInvoices.iterator();

  	   		while (i.hasNext()) {

      	   	   LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

    	   	   arAppliedInvoice.getArInvoicePaymentSchedule().setIpsLock(EJBCommon.FALSE);

    	   	   // remove all applied credits

    	   	   try {

    	   		Collection arAppliedCredits = arAppliedInvoice.getArAppliedCredits();

    	   		System.out.println("arAppliedCredits="+arAppliedCredits.size());
		  	    Iterator x = arAppliedCredits.iterator();

		  	    while (x.hasNext()) {

		  	   	    LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)x.next();

		  	  	    x.remove();

		  	  	    arAppliedCredit.remove();

		  	    }

    	   	   }catch(Exception ex){


    	   	   }


    	   	   i.remove();

    	   	   arAppliedInvoice.remove();

  	        }


  	   		adDeleteAuditTrailHome.create("GL_JOURNAL", arReceipt.getRctDate(), arReceipt.getRctNumber(), arReceipt.getRctReferenceNumber(),
  	   	 					arReceipt.getRctAmount(), AD_USR, new Date(), AD_CMPNY);

        	arReceipt.remove();

        } catch (FinderException ex) {

            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArReceiptEntryControllerBean getGlFcPrecisionUnit");


       LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         return  adCompany.getGlFunctionalCurrency().getFcPrecision();

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }

    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList getAdApprovalNotifiedUsersByRctCode(Integer RCT_CODE, Integer AD_CMPNY) {

       Debug.print("ArReceiptEntryControllerBean getAdApprovalNotifiedUsersByRctCode");


       LocalAdApprovalQueueHome adApprovalQueueHome = null;
       LocalArReceiptHome arReceiptHome = null;

       ArrayList list = new ArrayList();


       // Initialize EJB Home

       try {

           adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
           arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
              lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

         if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() == EJBCommon.TRUE) {

         	list.add("DOCUMENT POSTED");
         	return list;

         } else if (arReceipt.getRctVoid() == EJBCommon.TRUE && arReceipt.getRctVoidPosted() == EJBCommon.TRUE) {

         	list.add("DOCUMENT POSTED");
         	return list;

         }

         Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR RECEIPT", RCT_CODE, AD_CMPNY);

         Iterator i = adApprovalQueues.iterator();

         while(i.hasNext()) {

         	LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

         	list.add(adApprovalQueue.getAdUser().getUsrDescription());

         }

         return list;

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableArReceiptBatch(Integer AD_CMPNY) {

        Debug.print("ArReceiptEntryControllerBean getAdPrfEnableArReceiptBatch");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }


        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

           return adPreference.getPrfEnableArReceiptBatch();

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArrayList getArOpenRbAll(Integer AD_BRNCH, Integer AD_CMPNY) {

         Debug.print("ArReceiptEntryControllerBean getArOpenRbAll");

         LocalArReceiptBatchHome arReceiptBatchHome = null;

         ArrayList list = new ArrayList();

         // Initialize EJB Home

         try {

         	arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         try {

         	Collection arReceiptBatches = arReceiptBatchHome.findOpenRbByRbType("COLLECTION", AD_BRNCH, AD_CMPNY);

         	Iterator i = arReceiptBatches.iterator();

         	while (i.hasNext()) {

         		LocalArReceiptBatch arReceiptBatch = (LocalArReceiptBatch)i.next();

         		list.add(arReceiptBatch.getRbName());

         	}

         	return list;

         } catch (Exception ex) {

         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());

         }

     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {

     	Debug.print("ArReceiptEntryControllerBean getAdPrfArUseCustomerPulldown");

     	LocalAdPreferenceHome adPreferenceHome = null;

     	// Initialize EJB Home

     	try {

     		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

     	} catch (NamingException ex) {

     		throw new EJBException(ex.getMessage());

     	}


     	try {

     		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

     		return adPreference.getPrfArUseCustomerPulldown();

     	} catch (Exception ex) {

     		Debug.printStackTrace(ex);
     		throw new EJBException(ex.getMessage());

     	}

     }


     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public double getAiCreditBalancePaidByRctCodeByInvNum(Integer IPS_CODE , Integer RCT_CODE, Integer AD_CMPNY)
     throws GlobalNoRecordFoundException {

    	 Debug.print("ArReceiptEntryControllerBean getAiCreditBalancePaidByRctCodeByInvNum");

    	 LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
    	 try {

 			arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
 					lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);


         } catch (NamingException ex) {

      		throw new EJBException(ex.getMessage());

       	 }

		 double creditBalancePaid = 0d;

    	 try {
    		 Collection arAppliedInvoices = null;
    		 try{
    			 System.out.println(IPS_CODE + "  " + RCT_CODE + "  " + AD_CMPNY);
    			 arAppliedInvoices = arAppliedInvoiceHome.findByIpsCodeAndRctCode(IPS_CODE,RCT_CODE,AD_CMPNY);


    			 System.out.println(arAppliedInvoices.size() + " the size");
      			Iterator i = arAppliedInvoices.iterator();

          		while(i.hasNext()){

          			LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

          			creditBalancePaid += arAppliedInvoice.getAiCreditBalancePaid();

          		}





    		 }catch(FinderException fx){
    			 System.out.println("finder error");
    			 creditBalancePaid = 0d;
    		 }





    	 }catch(Exception ex){
    		 Debug.printStackTrace(ex);
      		throw new EJBException(ex.getMessage());
    	 }


    	 return creditBalancePaid;



     }





 /**
  * @ejb:interface-method view-type="remote"
  * @jboss:method-attributes read-only="true"
  **/
 public double getArRctCreditBalanceByCstCustomerCode(String CST_CSTMR_CODE, Integer AD_BRANCH, Integer AD_CMPNY)
 throws GlobalNoRecordFoundException {

	 Debug.print("ArReceiptEntryControllerBean getArRctCreditBalanceByCstCustomerCode");

	 LocalCmAdjustmentHome  cmAdjustmentHome = null;

	try {

		cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);



 	} catch (NamingException ex) {

 		throw new EJBException(ex.getMessage());

 	}

	try {


 		double creditbalanceRemaining = 0d;

 		Collection cmAdjustments = null;
 		Collection arAppliedInvoices = null;
 		try {

 			cmAdjustments = cmAdjustmentHome.findPostedAdjByCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

 			Iterator i = cmAdjustments.iterator();

     		while(i.hasNext()){

     			LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
     			Collection arAppliedCredits = cmAdjustment.getArAppliedCredits();

     			Iterator x = arAppliedCredits.iterator();
     			double totalAppliedCredit = 0d;
     			while(x.hasNext()){
     				LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)x.next();

     				totalAppliedCredit += arAppliedCredit.getAcApplyCredit();

     			}
				
				System.out.println("cm doc: " + cmAdjustment.getAdjDocumentNumber());
				System.out.println("cm amount: " + cmAdjustment.getAdjAmount());
				System.out.println("cm applied: " + totalAppliedCredit);
				System.out.println("cm refund: " + cmAdjustment.getAdjRefundAmount());
				
     			creditbalanceRemaining += cmAdjustment.getAdjAmount() - totalAppliedCredit - cmAdjustment.getAdjRefundAmount();
				System.out.println("cred bal remaining: " + creditbalanceRemaining);
     		}

 		} catch (FinderException ex) {


 		}

 		return EJBCommon.roundIt(creditbalanceRemaining,this.getGlFcPrecisionUnit(AD_CMPNY));



 	} catch (Exception ex) {
 		Debug.printStackTrace(ex);
 		throw new EJBException(ex.getMessage());

 	}

 }


	 /**
	  * @ejb:interface-method view-type="remote"
	  * @jboss:method-attributes read-only="true"
	  **/
	 public boolean checkIfExistArRctReferenceNumber(String RCT_RFRNC_NMBR, Integer AD_CMPNY){
		 Debug.print("ArReceiptEntryControllerBean checkIfExistArRctReferenceNumber");

		 LocalArReceiptHome  arReceiptHome = null;
		 LocalAdBranchHome adBranchHome = null;

		try {

			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
					lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);


	 	} catch (NamingException ex) {

	 		throw new EJBException(ex.getMessage());

	 	}
		boolean isFound = false;
		try{

			LocalArReceipt arReceipt= null;

			Collection branchList = adBranchHome.findBrAll(AD_CMPNY);

			Iterator i = branchList.iterator();

			while(i.hasNext()){
				try{

	     			LocalAdBranch adBranch = (LocalAdBranch)i.next();

	     			System.out.println(RCT_RFRNC_NMBR + " " + adBranch.getBrCode().toString() + "<-- payfile ref numb");
	     			arReceipt = arReceiptHome.findByPayfileReferenceNumberAndCompanyCode(RCT_RFRNC_NMBR + " " + adBranch.getBrCode().toString(),AD_CMPNY);

	     			System.out.println("return true");
	     			 return true;

			    } catch (FinderException ex) {
			    	System.out.println("not found");
			    	 isFound = false;

				}
			}
		}catch (FinderException ex) {
			return false;
		}

		System.out.println("not found exactly");
		return isFound;

	 }



     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public double getArRctDepositAmountByCstCustomerCode(String CST_CSTMR_CODE,Integer AD_BRANCH , Integer AD_CMPNY)
     throws GlobalNoRecordFoundException {

     	Debug.print("ArReceiptEntryControllerBean getArRctDepositAmountByCstCustomerCode");

     	LocalArReceiptHome arReceiptHome = null;
     	LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;

     	// Initialize EJB Home

     	try {

     		arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
     		arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);

     	} catch (NamingException ex) {

     		throw new EJBException(ex.getMessage());

     	}

     	try {

     		double depositAmount = 0d;
     		double draftAppliedDeposit = 0d;

     		Collection arReceipts = null;

     		try {

     			arReceipts = arReceiptHome.findOpenDepositEnabledPostedRctByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

     		} catch (FinderException ex) {

     			throw new GlobalNoRecordFoundException();

     		}

     		Iterator i = arReceipts.iterator();

     		while(i.hasNext()){

     			LocalArReceipt arReceipt = (LocalArReceipt)i.next();

     			depositAmount += arReceipt.getRctAmount() - arReceipt.getRctAppliedDeposit();

     		}

     		Collection arAppliedInvoices = null;

     		try {

     			arAppliedInvoices = arAppliedInvoiceHome.findUnpostedAiWithDepositByCstCustomerCode(CST_CSTMR_CODE,AD_BRANCH, AD_CMPNY);

     		} catch (FinderException ex) {

     			throw new GlobalNoRecordFoundException();

     		}

     		i = arAppliedInvoices.iterator();

     		while(i.hasNext()){

     			LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

     			draftAppliedDeposit += arAppliedInvoice.getAiAppliedDeposit();

     		}


     		return depositAmount - draftAppliedDeposit;

     	} catch (GlobalNoRecordFoundException ex) {

     		throw ex;

     	} catch (Exception ex) {

     		Debug.printStackTrace(ex);
     		throw new EJBException(ex.getMessage());

     	}

     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
     throws GlobalConversionDateNotExistException {

     	Debug.print("ArReceiptEntryControllerBean getFrRateByFrNameAndFrDate");

     	LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
     	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
     	LocalAdCompanyHome adCompanyHome = null;

     	// Initialize EJB Home

     	try {

     		glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
     		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
     		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

     	} catch (NamingException ex) {

     		throw new EJBException(ex.getMessage());

     	}

     	try {

     		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
     		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

     		double CONVERSION_RATE = 1;

     		// Get functional currency rate
     		System.out.println("------------->FC_NM="+FC_NM);
     		if (!FC_NM.equals("USD")) {

     			LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
     				glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
     						CONVERSION_DATE, AD_CMPNY);

     			CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

     		}

     		// Get set of book functional currency rate if necessary

     		if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

     			LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
     				glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
     						CONVERSION_DATE, AD_CMPNY);

     			CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

     		}

     		return CONVERSION_RATE;

     	} catch (FinderException ex) {

     		getSessionContext().setRollbackOnly();
     		throw new GlobalConversionDateNotExistException();

     	} catch (Exception ex) {

     		Debug.printStackTrace(ex);
     		throw new EJBException(ex.getMessage());

     	}

     }

    // private methods

    private LocalArAppliedInvoice addArAiEntry(ArModAppliedInvoiceDetails mdetails, LocalArReceipt arReceipt, Integer AD_CMPNY)
        throws ArINVOverapplicationNotAllowedException,
        GlobalTransactionAlreadyLockedException,
        ArRCTInvoiceHasNoWTaxCodeException,
        ArINVOverCreditBalancePaidapplicationNotAllowedException {

		Debug.print("ArReceiptEntryControllerBean addArAiEntry");

		LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalCmAdjustmentHome cmAdjustmentHome = null;
        LocalHrPayrollPeriodHome hrPayrollPeriodHome = null;


        // Initialize EJB Home

        try {

            arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);
            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalCmAdjustmentHome.class);
            hrPayrollPeriodHome = (LocalHrPayrollPeriodHome)EJBHomeFactory.
                	lookUpLocalHome(LocalHrPayrollPeriodHome.JNDI_NAME, LocalHrPayrollPeriodHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// get company

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	// get functional currency name

        	String FC_NM = adCompany.getGlFunctionalCurrency().getFcName();


        	// validate overapplication

        	LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        	    arInvoicePaymentScheduleHome.findByPrimaryKey(mdetails.getAiIpsCode());

        	double totalAmountDue = EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY));
    		double totalApplyAmount = EJBCommon.roundIt(mdetails.getAiApplyAmount()+ mdetails.getAiCreditBalancePaid() + mdetails.getAiCreditableWTax() + mdetails.getAiDiscountAmount() , this.getGlFcPrecisionUnit(AD_CMPNY));

    		System.out.println("mdetails.getAiApplyAmount()="+mdetails.getAiApplyAmount());
    		System.out.println("mdetails.getAiCreditBalancePaid()="+mdetails.getAiCreditBalancePaid());

    		//mdetails.setAiAppliedDeposit();
    		System.out.println("totalAmountDue="+totalAmountDue);
    		System.out.println("totalApplyAmount="+totalApplyAmount);
        	if (totalAmountDue < totalApplyAmount) {

        		throw new ArINVOverapplicationNotAllowedException(arInvoicePaymentSchedule.getArInvoice().getInvNumber() + "-" + arInvoicePaymentSchedule.getIpsNumber());

        	}


        	double remainingCreditBalance =  this.getArRctCreditBalanceByCstCustomerCode(
        				arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode(), arInvoicePaymentSchedule.getArInvoice().getInvAdBranch(), AD_CMPNY);

        	System.out.println("remainingCreditBalance="+remainingCreditBalance);
        	System.out.println("mdetails.getAiCreditBalancePaid()="+mdetails.getAiCreditBalancePaid());

	  	    if(remainingCreditBalance < mdetails.getAiCreditBalancePaid() ){

	  	    	System.out.println("--------------------------->");
	  	    	throw new ArINVOverCreditBalancePaidapplicationNotAllowedException(arInvoicePaymentSchedule.getArInvoice().getInvNumber() + "-" + arInvoicePaymentSchedule.getIpsNumber());

	  	    }

        	// validate if ips already locked

        	if (arInvoicePaymentSchedule.getIpsLock() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyLockedException(arInvoicePaymentSchedule.getArInvoice().getInvNumber() + "-" + arInvoicePaymentSchedule.getIpsNumber());

        	}

        	// validate invoice wtax code if necessary

        	if (mdetails.getAiCreditableWTax() != 0 &&
        	    arInvoicePaymentSchedule.getArInvoice().getArWithholdingTaxCode().getGlChartOfAccount() == null &&
        	    (adPreference.getArWithholdingTaxCode() == null ||
				 adPreference.getArWithholdingTaxCode().getGlChartOfAccount() == null)) {

        		throw new ArRCTInvoiceHasNoWTaxCodeException(arInvoicePaymentSchedule.getArInvoice().getInvNumber() + "-" + arInvoicePaymentSchedule.getIpsNumber());

        	}

        	double AI_FRX_GN_LSS = 0d;

        	if (!FC_NM.equals(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName()) ||
        	    !FC_NM.equals(arReceipt.getGlFunctionalCurrency().getFcName())) {

        	    double AI_ALLCTD_PYMNT_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
        	                                       arReceipt.getGlFunctionalCurrency().getFcName(),
        	                                       arReceipt.getRctConversionDate(),
        	                                       arReceipt.getRctConversionRate(),
        	                                       mdetails.getAiApplyAmount()+ mdetails.getAiCreditBalancePaid(), AD_CMPNY);

        	    double AI_APPLY_AMNT = this.convertForeignToFunctionalCurrency(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcCode(),
        	                                       arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName(),
        	                                       arInvoicePaymentSchedule.getArInvoice().getInvConversionDate(),
        	                                       arInvoicePaymentSchedule.getArInvoice().getInvConversionRate(),
        	                                       mdetails.getAiApplyAmount() + mdetails.getAiCreditBalancePaid(), AD_CMPNY);

        	    System.out.println("apply amount converted: " + AI_APPLY_AMNT);
        	    
        	    
        	    double AI_CRDTBL_W_TX = this.convertForeignToFunctionalCurrency(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcCode(),
        	                                       arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName(),
        	                                       arInvoicePaymentSchedule.getArInvoice().getInvConversionDate(),
        	                                       arInvoicePaymentSchedule.getArInvoice().getInvConversionRate(),
        	                                       mdetails.getAiCreditableWTax(), AD_CMPNY);

        	    double AI_DSCNT_AMNT = this.convertForeignToFunctionalCurrency(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcCode(),
        	                                       arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName(),
        	                                       arInvoicePaymentSchedule.getArInvoice().getInvConversionDate(),
        	                                       arInvoicePaymentSchedule.getArInvoice().getInvConversionRate(),
        	                                       mdetails.getAiDiscountAmount(), AD_CMPNY);

        	    double AI_APPLD_DPST = this.convertForeignToFunctionalCurrency(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcCode(),
                        arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName(),
                        arInvoicePaymentSchedule.getArInvoice().getInvConversionDate(),
                        arInvoicePaymentSchedule.getArInvoice().getInvConversionRate(),
                        mdetails.getAiAppliedDeposit(), AD_CMPNY);


        	    AI_FRX_GN_LSS = EJBCommon.roundIt((AI_ALLCTD_PYMNT_AMNT + AI_CRDTBL_W_TX + AI_DSCNT_AMNT + AI_APPLD_DPST) -
        	                    (AI_APPLY_AMNT + AI_CRDTBL_W_TX + AI_DSCNT_AMNT + AI_APPLD_DPST), this.getGlFcPrecisionUnit(AD_CMPNY));

        	    
        	    System.out.println("forex gain loss  " + AI_FRX_GN_LSS);
        	}

		    // create applied invoice

		    LocalArAppliedInvoice arAppliedInvoice = arAppliedInvoiceHome.create(mdetails.getAiApplyAmount(), mdetails.getAiPenaltyApplyAmount(), mdetails.getAiCreditableWTax(),
		        mdetails.getAiDiscountAmount(), mdetails.getAiRebate(), mdetails.getAiAppliedDeposit(), mdetails.getAiAllocatedPaymentAmount(), AI_FRX_GN_LSS,
		        mdetails.getAiApplyRebate(),
		        AD_CMPNY);

		    arAppliedInvoice.setAiCreditBalancePaid(mdetails.getAiCreditBalancePaid());

		    // update cm ajustment advance remaining balance and create applied credit


		    if(mdetails.getAiCreditBalancePaid() > 0){


		    	Collection cmAdjustments = cmAdjustmentHome.findPostedAdjByCustomerCode(
			    		arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode(),AD_CMPNY);
			    Iterator i = cmAdjustments.iterator();

		//	    double creditBalancePaid = mdetails.getAiCreditBalancePaid();
//
			    
			    double creditBalancePaid = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
                        arReceipt.getGlFunctionalCurrency().getFcName(),
                        arReceipt.getRctConversionDate(),
                        arReceipt.getRctConversionRate(),
                        mdetails.getAiCreditBalancePaid(), AD_CMPNY);
			    
			    
			    while(i.hasNext()){

			    	LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();

			    	//StringBuilder strB = cmAdjustment.getAdjAmountAppliedLog();
			    	Collection arAppliedCredits = cmAdjustment.getArAppliedCredits();

	     			Iterator x = arAppliedCredits.iterator();
	     			double totalAppliedCredit = 0d;
	     			while(x.hasNext()){
	     				LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)x.next();

	     				totalAppliedCredit += arAppliedCredit.getAcApplyCredit();

	     			}

			    	double advanceRemaining = EJBCommon.roundIt(cmAdjustment.getAdjAmount() - totalAppliedCredit - cmAdjustment.getAdjRefundAmount(), this.getGlFcPrecisionUnit(AD_CMPNY));
			    	if(advanceRemaining == 0) continue;
			    	if(creditBalancePaid <= advanceRemaining ){

			    		cmAdjustment.addArAppliedCredit(this.addArAcEntry(EJBCommon.roundIt(creditBalancePaid, this.getGlFcPrecisionUnit(AD_CMPNY)), arAppliedInvoice, AD_CMPNY));
				    	creditBalancePaid -= creditBalancePaid;

			    	} else {
			    		cmAdjustment.addArAppliedCredit(this.addArAcEntry(EJBCommon.roundIt(advanceRemaining, this.getGlFcPrecisionUnit(AD_CMPNY)), arAppliedInvoice, AD_CMPNY));
			    		creditBalancePaid -= advanceRemaining;

			    	}


			    	if(creditBalancePaid<=0)break;
			    }



		    }


		    arReceipt.addArAppliedInvoice(arAppliedInvoice);
		    arInvoicePaymentSchedule.addArAppliedInvoice(arAppliedInvoice);

		    // lock invoice

		   arInvoicePaymentSchedule.setIpsLock(EJBCommon.TRUE);

		    return arAppliedInvoice;

		} catch (ArINVOverapplicationNotAllowedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

		} catch (ArINVOverCreditBalancePaidapplicationNotAllowedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;


        } catch (GlobalTransactionAlreadyLockedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ArRCTInvoiceHasNoWTaxCodeException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

    private LocalArAppliedCredit addArAcEntry(double APPLY_AMNT,LocalArAppliedInvoice arAppliedInvoice, Integer AD_CMPNY)
    		throws GlobalBranchAccountNumberInvalidException {

    		Debug.print("ArReceiptEntryControllerBean addArAcEntry");


    		LocalArAppliedCreditHome arAppliedCreditHome = null;

    		LocalAdCompanyHome adCompanyHome = null;


            // Initialize EJB Home

            try {

            	arAppliedCreditHome = (LocalArAppliedCreditHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArAppliedCreditHome.JNDI_NAME, LocalArAppliedCreditHome.class);

                adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


            } catch (NamingException ex) {

                throw new EJBException(ex.getMessage());

            }

            try {

            	// get company

            	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		    // create distribution record


    		    LocalArAppliedCredit arAppliedCredit = arAppliedCreditHome.create(
    		    		EJBCommon.roundIt(APPLY_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), AD_CMPNY);


    			// to be used by gl journal interface for cross currency receipts
    			if (arAppliedInvoice != null) {

    				arAppliedInvoice.addArAppliedCredit(arAppliedCredit);
    			}

    			return arAppliedCredit;

            } catch (FinderException ex) {

        		throw new GlobalBranchAccountNumberInvalidException();

        	} catch (Exception ex) {

            	Debug.printStackTrace(ex);
            	getSessionContext().setRollbackOnly();
            	throw new EJBException(ex.getMessage());

            }

    	}

	private void addArDrEntry(short DR_LN, String DR_CLSS,
	    byte DR_DBT, double DR_AMNT, byte DR_RVRSD, Integer COA_CODE, LocalArReceipt arReceipt,
	    LocalArAppliedInvoice arAppliedInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
		throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArReceiptEntryControllerBean addArDrEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// get company

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	System.out.println("COA CODE IS: " + COA_CODE);
        	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

		    // create distribution record

		    LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
			    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    EJBCommon.FALSE, DR_RVRSD, AD_CMPNY);

			arReceipt.addArDistributionRecord(arDistributionRecord);
			glChartOfAccount.addArDistributionRecord(arDistributionRecord);

			// to be used by gl journal interface for cross currency receipts
			if (arAppliedInvoice != null) {

				arAppliedInvoice.addArDistributionRecord(arDistributionRecord);

			}

        } catch (FinderException ex) {
    		System.out.println("DR_CLSS: " + DR_CLSS);
    		throw new GlobalBranchAccountNumberInvalidException();

    	} catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}


    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

	    Debug.print("ArReceiptEntryControllerBean convertForeignToFunctionalCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT / CONVERSION_RATE;

         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public String createAdvancePayment(ArReceiptDetails rctDetails,String customer_code, String BA_NM,String payfileReference, int AD_BRNCH, int AD_CMPNY)
    throws GlobalDocumentNumberNotUniqueException,GlobalConversionDateNotExistException,Exception{
    	Debug.print("ArReceiptEntryControllerBean saveCmAdjEntry");
        //CREATE CM ADJUSTMENT ADVANCE
        LocalCmAdjustmentHome cmAdjustmentHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBranchBankAccountHome adBranchBankAccountHome = null;

        //initialized EJB Home

        try {

       	 adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);

            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
 			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);

        } catch (NamingException ex) {

        	return ex.toString();
         //   throw new EJBException(ex.getMessage());

        }

        CmAdjustmentDetails details = new CmAdjustmentDetails();


        details.setAdjCode(null);
        details.setAdjType("ADVANCE");
        details.setAdjDate(rctDetails.getRctDate());
        details.setAdjDocumentNumber("");
        details.setAdjReferenceNumber(payfileReference);
        details.setAdjAmount(rctDetails.getRctAmount());
        details.setAdjConversionDate(rctDetails.getRctConversionDate());
        details.setAdjConversionRate(rctDetails.getRctConversionRate());
        details.setAdjVoid(rctDetails.getRctVoid());
        details.setAdjMemo(rctDetails.getRctDescription());
        details.setAdjCreatedBy(rctDetails.getRctCreatedBy());
        details.setAdjDateCreated(new java.util.Date());


        details.setAdjLastModifiedBy(rctDetails.getRctLastModifiedBy());
        details.setAdjDateLastModified(new java.util.Date());


        //validate reference Number

        if(!payfileReference.equals("")){


        	try{

        		cmAdjustmentHome.findAdjByReferenceNumber(payfileReference, AD_CMPNY);

        		//this code if there's existing reference number
        		return "Already Posted/Created ADVANCE";
        	} catch (FinderException ex) {
				//no dubplicate
			}

        }




   	 ///validate document number is unique

        LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

		try {

			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("CM ADJUSTMENT", AD_CMPNY);

		} catch (FinderException ex) {

		}

			try {

				adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

			} catch (FinderException ex) {

			}

		LocalCmAdjustment cmExistingAdjustment = null;

		try {

		    cmExistingAdjustment = cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);

    	} catch (FinderException ex) {
    	}

        if (cmExistingAdjustment != null) {

        	return "Document Number not Unique";
        //	throw new GlobalDocumentNumberNotUniqueException();

        }

	        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
	            (details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {

	            while (true) {

	            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

		            	try {

		            		cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

		            	} catch (FinderException ex) {

		            		details.setAdjDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
				            break;

		            	}

	            	} else {

	            		try {

		            		cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

		            	} catch (FinderException ex) {

		            		details.setAdjDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
				            break;

		            	}

	            	}

	            }

	        }


	     // validate if conversion date exists

	        try {

	      	    if (details.getAdjConversionDate() != null) {

	      	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	      	    	LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
	      	    			adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);

	 				if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

	 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
	 								details.getAdjConversionDate(), AD_CMPNY);

	 				} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

	 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(
	 							adCompany.getGlFunctionalCurrency().getFcCode(), details.getAdjConversionDate(), AD_CMPNY);

	 				}

		        }

	        } catch (FinderException ex) {

	        	return "Conversion Date Not Exist";
	        //	 throw new GlobalConversionDateNotExistException();

	        }


	        LocalCmAdjustment cmAdjustment = null;
	        try{

	        	 cmAdjustment = cmAdjustmentHome.create(
		            	 details.getAdjType(), details.getAdjDate(),
		            	 details.getAdjDocumentNumber(), details.getAdjReferenceNumber(), details.getAdjCheckNumber(), details.getAdjAmount(), 0d,
		            	 details.getAdjConversionDate(), details.getAdjConversionRate(),
		             	 details.getAdjMemo(),null, EJBCommon.FALSE, EJBCommon.FALSE,
		             	 EJBCommon.FALSE, 0d, null,
		             	 EJBCommon.FALSE, null,
		             	 null, EJBCommon.FALSE, details.getAdjCreatedBy(), details.getAdjDateCreated(),
	       	    	details.getAdjLastModifiedBy(), details.getAdjDateLastModified(), null, null, null, null, null, AD_BRNCH, AD_CMPNY);

	        	 cmAdjustment.setAdjApprovalStatus("N/A");

		        LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
		        adBankAccount.addCmAdjustment(cmAdjustment);


		        // remove all distribution records
	        	System.out.println("flag 3");
		  	    Collection cmDistributionRecords = cmAdjustment.getCmDistributionRecords();

		  	    Iterator i = cmDistributionRecords.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)i.next();

		  	  	    i.remove();

		  	  	    cmDistributionRecord.remove();

		  	    }

		  	   LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);


		  	   // add branch bank account
		      	  LocalAdBranchBankAccount adBranchBankAccount = null;

	                try {
	                    adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(adBankAccount.getBaCode(), AD_BRNCH, AD_CMPNY);

	                } catch(FinderException ex) {

	                }

		    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
	    	  	    "CASH", EJBCommon.TRUE, cmAdjustment.getAdjAmount(),
	    	  	  adBranchBankAccount.getBbaGlCoaCashAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

		    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
	    	  	    "ADVANCE", EJBCommon.FALSE, cmAdjustment.getAdjAmount(),
	    	  	  adBranchBankAccount.getBbaGlCoaAdvanceAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

		    	LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(customer_code,AD_CMPNY);

	  	    	arCustomer.addCmAdjustment(cmAdjustment);



	  	    	this.executeCmAdjPost(cmAdjustment.getAdjCode(), cmAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);
	        }catch(Exception ex){
	        	throw ex;
	        }


	        return "Success Creating ADVANCE";

    }



    private void postAdvncPymntByCmAdj(LocalArReceipt arReceipt, String BA_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	    Exception{

    	 Debug.print("ArReceiptEntryControllerBean saveCmAdjEntry");
         //CREATE CM ADJUSTMENT ADVANCE
         LocalCmAdjustmentHome cmAdjustmentHome = null;
         LocalAdBankAccountHome adBankAccountHome = null;
         LocalArCustomerHome arCustomerHome = null;
         LocalAdApprovalHome adApprovalHome = null;
         LocalAdAmountLimitHome adAmountLimitHome = null;
         LocalAdApprovalUserHome adApprovalUserHome = null;
         LocalAdApprovalQueueHome adApprovalQueueHome = null;
         LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
         LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
         LocalAdPreferenceHome adPreferenceHome = null;
         LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
         LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
         LocalAdCompanyHome adCompanyHome = null;
         LocalAdBranchBankAccountHome adBranchBankAccountHome = null;

         //initialized EJB Home

         try {

        	 adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
                     lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);

             cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                 lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
             adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
             arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                     lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
             adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
             adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
             adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
             adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
             glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                 lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
             glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
                 lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
             adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
             adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
             	lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
  			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
 				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
  			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
 				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         CmAdjustmentDetails details = new CmAdjustmentDetails();


         details.setAdjCode(null);
         details.setAdjType("ADVANCE");
         details.setAdjDate(arReceipt.getRctDate());
         details.setAdjDocumentNumber("");
         details.setAdjReferenceNumber(arReceipt.getRctNumber());
         details.setAdjAmount(arReceipt.getRctExcessAmount());
         details.setAdjConversionDate(arReceipt.getRctConversionDate());
         details.setAdjConversionRate(arReceipt.getRctConversionRate());
         details.setAdjMemo("");
         details.setAdjVoid(arReceipt.getRctVoid());

         details.setAdjCreatedBy(arReceipt.getRctCreatedBy());
         details.setAdjDateCreated(new java.util.Date());


         details.setAdjLastModifiedBy(arReceipt.getRctLastModifiedBy());
         details.setAdjDateLastModified(new java.util.Date());



    	 ///validate document number is unique

         LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
 		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

 		try {

 			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("CM ADJUSTMENT", AD_CMPNY);

 		} catch (FinderException ex) {

 		}

			try {

				adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

			} catch (FinderException ex) {

			}

 		LocalCmAdjustment cmExistingAdjustment = null;

 		try {

 		    cmExistingAdjustment = cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);

     	} catch (FinderException ex) {
     	}

         if (cmExistingAdjustment != null) {

         	throw new GlobalDocumentNumberNotUniqueException();

         }

	        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
	            (details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {

	            while (true) {

	            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

		            	try {

		            		cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

		            	} catch (FinderException ex) {

		            		details.setAdjDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
				            break;

		            	}

	            	} else {

	            		try {

		            		cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

		            	} catch (FinderException ex) {

		            		details.setAdjDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
				            break;

		            	}

	            	}

	            }

	        }


	     // validate if conversion date exists

	        try {

	      	    if (details.getAdjConversionDate() != null) {

	      	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	      	    	LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
	      	    			adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);

	 				if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

	 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
	 								details.getAdjConversionDate(), AD_CMPNY);

	 				} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

	 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(
	 							adCompany.getGlFunctionalCurrency().getFcCode(), details.getAdjConversionDate(), AD_CMPNY);

	 				}

		        }

	        } catch (FinderException ex) {

	        	 throw new GlobalConversionDateNotExistException();

	        }


	        LocalCmAdjustment cmAdjustment = null;
	        try{

	        	 cmAdjustment = cmAdjustmentHome.create(
		            	 details.getAdjType(), details.getAdjDate(),
		            	 details.getAdjDocumentNumber(), details.getAdjReferenceNumber(), details.getAdjCheckNumber(), details.getAdjAmount(), 0d,
		            	 details.getAdjConversionDate(), details.getAdjConversionRate(),
		             	 details.getAdjMemo(),null, EJBCommon.FALSE, EJBCommon.FALSE,
		             	 EJBCommon.FALSE, 0d, null,
		             	 EJBCommon.FALSE, null,
		             	 null, EJBCommon.FALSE, details.getAdjCreatedBy(), details.getAdjDateCreated(),
	       	    	details.getAdjLastModifiedBy(), details.getAdjDateLastModified(), null, null, null, null, null, AD_BRNCH, AD_CMPNY);

	        	 cmAdjustment.setAdjApprovalStatus("N/A");

		        LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
		        adBankAccount.addCmAdjustment(cmAdjustment);


		        // remove all distribution records
	        	System.out.println("flag 3");
		  	    Collection cmDistributionRecords = cmAdjustment.getCmDistributionRecords();

		  	    Iterator i = cmDistributionRecords.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)i.next();

		  	  	    i.remove();

		  	  	    cmDistributionRecord.remove();

		  	    }

		  	   LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);


		  	   // add branch bank account
		      	  LocalAdBranchBankAccount adBranchBankAccount = null;

	                try {
	                    adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(adBankAccount.getBaCode(), AD_BRNCH, AD_CMPNY);

	                } catch(FinderException ex) {

	                }

		    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
	    	  	    "CASH", EJBCommon.TRUE, cmAdjustment.getAdjAmount(),
	    	  	  adBranchBankAccount.getBbaGlCoaCashAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

		    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
	    	  	    "ADVANCE", EJBCommon.FALSE, cmAdjustment.getAdjAmount(),
	    	  	  adBranchBankAccount.getBbaGlCoaAdvanceAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

		    	LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode(),AD_CMPNY);

	  	    	arCustomer.addCmAdjustment(cmAdjustment);



	  	    	this.executeCmAdjPost(cmAdjustment.getAdjCode(), cmAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);
	        }catch(Exception ex){
	        	throw  ex;
	        }
    }



    private void executeCmAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	    GlobalRecordAlreadyDeletedException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException {

	    Debug.print("ArReceiptEntryControllerBean executeCmAdjPost");

	    LocalCmAdjustmentHome cmAdjustmentHome = null;
	    LocalAdCompanyHome adCompanyHome = null;
	    LocalAdPreferenceHome adPreferenceHome = null;
	    LocalGlSetOfBookHome glSetOfBookHome = null;
	    LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
	    LocalGlJournalHome glJournalHome = null;
	    LocalGlJournalBatchHome glJournalBatchHome = null;
	    LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
	    LocalGlJournalLineHome glJournalLineHome = null;
	    LocalGlJournalSourceHome glJournalSourceHome = null;
	    LocalGlJournalCategoryHome glJournalCategoryHome = null;
	    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
	    LocalCmDistributionRecordHome cmDistributionRecordHome = null;
	    LocalGlChartOfAccountHome glChartOfAccountHome = null;
	    LocalAdBankAccountHome adBankAccountHome = null;
	    LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
	    LocalGlForexLedgerHome glForexLedgerHome = null;
			LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

	    LocalCmAdjustment cmAdjustment = null;

	    // Initialize EJB Home

	    try {

	        cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
	            lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
	        glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
	        glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
	        glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
	        glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
	        glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
	        glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
	        glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
	        glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
	        glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
	        cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
	        glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	        	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	        adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
	        adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
	        glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
				glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	    	// validate if adjustment is already deleted

	    	try {

	    		cmAdjustment = cmAdjustmentHome.findByPrimaryKey(ADJ_CODE);

	    	} catch (FinderException ex) {

	    		throw new GlobalRecordAlreadyDeletedException();

	    	}

	    	// validate if adjustment is already posted or void

	    	if (cmAdjustment.getAdjPosted() == EJBCommon.TRUE) {

	    		throw new GlobalTransactionAlreadyPostedException();

	    	} else if (cmAdjustment.getAdjVoid() == EJBCommon.TRUE) {

	    		throw new GlobalTransactionAlreadyVoidException();
	    	}

	    	// post adjustment

	    	if (cmAdjustment.getAdjVoid() == EJBCommon.FALSE && cmAdjustment.getAdjPosted() == EJBCommon.FALSE) {

	    		if(cmAdjustment.getAdjType().equals("INTEREST") || cmAdjustment.getAdjType().equals("DEBIT MEMO")) {

	    			// increase bank account balances

					LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());

					try {

						// find bankaccount balance before or equal receipt date

						Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

						if (!adBankAccountBalances.isEmpty()) {

							// get last check

							ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

							if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {

								// create new balance

								LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
										cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);

								adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

							} else { // equals to check date

								adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());

							}

						} else {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									cmAdjustment.getAdjDate(), (cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);

							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

						}

						// propagate to subsequent balances if necessary

						adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

						Iterator i = adBankAccountBalances.iterator();

						while (i.hasNext()) {

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());

						}

					} catch (Exception ex) {

						ex.printStackTrace();

					}

	    		} else {

	    			// decrease bank account balances

					LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());

					try {

						// find bankaccount balance before or equal receipt date

						Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

						if (!adBankAccountBalances.isEmpty()) {

							// get last check

							ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

							if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {

								// create new balance

								LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
										cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);

								adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

							} else { // equals to check date

								adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());

							}

						} else {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									cmAdjustment.getAdjDate(), (0 - cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);

							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

						}

						// propagate to subsequent balances if necessary

						adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

						Iterator i = adBankAccountBalances.iterator();

						while (i.hasNext()) {

							LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());

						}

					} catch (Exception ex) {

						ex.printStackTrace();

					}

	    		}

	    	}

	    	// set adjcher post status

	    	cmAdjustment.setAdjPosted(EJBCommon.TRUE);
	    	cmAdjustment.setAdjPostedBy(USR_NM);
	    	cmAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


	    	// post to gl if necessary

	    	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	    	if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

	    		// validate if date has no period and period is closed

	    		LocalGlSetOfBook glJournalSetOfBook = null;

	    		try {

	    			glJournalSetOfBook = glSetOfBookHome.findByDate(cmAdjustment.getAdjDate(), AD_CMPNY);

	    		} catch (FinderException ex) {

	    			throw new GlJREffectiveDateNoPeriodExistException();

	    		}

	    		LocalGlAccountingCalendarValue glAccountingCalendarValue =
	    			glAccountingCalendarValueHome.findByAcCodeAndDate(
	    					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), cmAdjustment.getAdjDate(), AD_CMPNY);


	    		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
	    				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

	    			throw new GlJREffectiveDatePeriodClosedException();

	    		}

	    		// check if invoice is balance if not check suspense posting

	    		LocalGlJournalLine glOffsetJournalLine = null;

	    		Collection cmDistributionRecords = cmDistributionRecordHome.findByDrReversalAndDrImportedAndAdjCode(EJBCommon.FALSE, EJBCommon.FALSE, cmAdjustment.getAdjCode(), AD_CMPNY);

	    		Iterator j = cmDistributionRecords.iterator();

	    		double TOTAL_DEBIT = 0d;
	    		double TOTAL_CREDIT = 0d;

	    		while (j.hasNext()) {

	    			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

	    			double DR_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
	    					cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(),
							cmAdjustment.getAdjConversionDate(),
							cmAdjustment.getAdjConversionRate(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);

	    			if (cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

	    				TOTAL_DEBIT += DR_AMNT;

	    			} else {

	    				TOTAL_CREDIT += DR_AMNT;

	    			}

	    		}

	    		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
	    		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	    		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
	    				TOTAL_DEBIT != TOTAL_CREDIT) {

	    			LocalGlSuspenseAccount glSuspenseAccount = null;

	    			try {

	    				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("CASH MANAGEMENT", "BANK ADJUSTMENTS", AD_CMPNY);

	    			} catch (FinderException ex) {

	    				throw new GlobalJournalNotBalanceException();

	    			}

	    			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

	    				glOffsetJournalLine = glJournalLineHome.create(
	    						(short)(cmDistributionRecords.size() + 1),
								EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

	    			} else {

	    				glOffsetJournalLine = glJournalLineHome.create(
	    						(short)(cmDistributionRecords.size() + 1),
								EJBCommon.FALSE,
								TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

	    			}

	    			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
	    			glChartOfAccount.addGlJournalLine(glOffsetJournalLine);


	    		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
	    				TOTAL_DEBIT != TOTAL_CREDIT) {

	    			throw new GlobalJournalNotBalanceException();

	    		}

	    		// create journal batch if necessary

	    		LocalGlJournalBatch glJournalBatch = null;
	    		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

	    		try {

	    			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " BANK ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

	    		} catch (FinderException ex) {
	    		}

	    		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
	    				glJournalBatch == null) {

	    			glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " BANK ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

	    		}

	    		// create journal entry

	    		LocalGlJournal glJournal = glJournalHome.create(cmAdjustment.getAdjReferenceNumber(),
	    				cmAdjustment.getAdjMemo(), cmAdjustment.getAdjDate(),
						0.0d, null, cmAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						null, null, EJBCommon.FALSE, null,

						AD_BRNCH, AD_CMPNY);

	    		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("CASH MANAGEMENT", AD_CMPNY);
	    		glJournal.setGlJournalSource(glJournalSource);

	    		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
	    		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

	    		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("BANK ADJUSTMENTS", AD_CMPNY);
	    		glJournal.setGlJournalCategory(glJournalCategory);

	    		if (glJournalBatch != null) {

	    			glJournal.setGlJournalBatch(glJournalBatch);

	    		}


	    		// create journal lines

	    		j = cmDistributionRecords.iterator();

	    		while (j.hasNext()) {

	    			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

	    			double DR_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
	    					cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(),
							cmAdjustment.getAdjConversionDate(),
							cmAdjustment.getAdjConversionRate(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);

	    			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
	    					cmDistributionRecord.getDrLine(),
							cmDistributionRecord.getDrDebit(),
							DR_AMNT, "", AD_CMPNY);

	    			cmDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);

	    			glJournal.addGlJournalLine(glJournalLine);

	    			cmDistributionRecord.setDrImported(EJBCommon.TRUE);

			  	  	// for FOREX revaluation
			  	  	if((cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode() !=
			  	  			adCompany.getGlFunctionalCurrency().getFcCode()) &&
						glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
			  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
			  	  			cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode()))){

			  	  		double CONVERSION_RATE = 1;

			  	  		if (cmAdjustment.getAdjConversionRate() != 0 && cmAdjustment.getAdjConversionRate() != 1) {

			  	  			CONVERSION_RATE = cmAdjustment.getAdjConversionRate();

			  	  		} else if (cmAdjustment.getAdjConversionDate() != null){

			  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
				  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
									glJournal.getJrConversionDate(), AD_CMPNY);

			  	  		}

			  	  		Collection glForexLedgers = null;

			  	  		try {

			  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
			  	  				cmAdjustment.getAdjDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

			  	  		} catch(FinderException ex) {

			  	  		}

			  	  		LocalGlForexLedger glForexLedger =
			  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
			  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();

			  	  		int FRL_LN = (glForexLedger != null &&
			  	  				glForexLedger.getFrlDate().compareTo(cmAdjustment.getAdjDate()) == 0) ?
			  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;

			  	  		// compute balance
			  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
			  	  		double FRL_AMNT = cmDistributionRecord.getDrAmount();

			  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
			  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));
			  	  		else
			  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);

			  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

			  	  		glForexLedger = glForexLedgerHome.create(cmAdjustment.getAdjDate(), new Integer (FRL_LN),
			  	  			"OTH", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

			  	  		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);

			  	  		// propagate balances
			  	  		try{

			  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
			  	  				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
								glForexLedger.getFrlAdCompany());

			  	  		} catch (FinderException ex) {

			  	  		}

			  	  		Iterator itrFrl = glForexLedgers.iterator();

			  	  		while (itrFrl.hasNext()) {

			  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
			  	  			FRL_AMNT = cmDistributionRecord.getDrAmount();

			  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
			  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT :
			  	  					(- 1 * FRL_AMNT));
			  	  			else
			  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) :
			  	  					FRL_AMNT);

			  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

			  	  		}

			  	  	}

	    		}

	    		if (glOffsetJournalLine != null) {

	    			glJournal.addGlJournalLine(glOffsetJournalLine);

	    		}

	    		// post journal to gl

	    		Collection glJournalLines = glJournal.getGlJournalLines();

	    		Iterator i = glJournalLines.iterator();

	    		while (i.hasNext()) {

	    			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

	    			// post current to current acv

	    			this.postToGl(glAccountingCalendarValue,
	    					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


	    			// post to subsequent acvs (propagate)

	    			Collection glSubsequentAccountingCalendarValues =
	    				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
	    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

	    			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

	    			while (acvsIter.hasNext()) {

	    				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
	    					(LocalGlAccountingCalendarValue)acvsIter.next();

	    				this.postToGl(glSubsequentAccountingCalendarValue,
	    						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

	    			}

	    			// post to subsequent years if necessary

	    			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

	    			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

	    				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	    				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

	    				Iterator sobIter = glSubsequentSetOfBooks.iterator();

	    				while (sobIter.hasNext()) {

	    					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

	    					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

	    					// post to subsequent acvs of subsequent set of book(propagate)

	    					Collection glAccountingCalendarValues =
	    						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

	    					Iterator acvIter = glAccountingCalendarValues.iterator();

	    					while (acvIter.hasNext()) {

	    						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
	    							(LocalGlAccountingCalendarValue)acvIter.next();

	    						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
	    								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

	    							this.postToGl(glSubsequentAccountingCalendarValue,
	    									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

	    						} else { // revenue & expense

	    							this.postToGl(glSubsequentAccountingCalendarValue,
	    									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

	    						}

	    					}

	    					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

	    				}

	    			}

	    		}

	    	}

	    } catch (GlJREffectiveDateNoPeriodExistException ex) {

	    	getSessionContext().setRollbackOnly();
	    	throw ex;

	    } catch (GlJREffectiveDatePeriodClosedException ex) {

	    	getSessionContext().setRollbackOnly();
	    	throw ex;

	    } catch (GlobalJournalNotBalanceException ex) {

	    	getSessionContext().setRollbackOnly();
	    	throw ex;

	    } catch (GlobalRecordAlreadyDeletedException ex) {

	    	getSessionContext().setRollbackOnly();
	    	throw ex;

	    } catch (GlobalTransactionAlreadyPostedException ex) {

	    	getSessionContext().setRollbackOnly();
	    	throw ex;

	    } catch (GlobalTransactionAlreadyVoidException ex) {

	    	getSessionContext().setRollbackOnly();
	    	throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}





    private void addCmDrEntry(short DR_LN, String DR_CLSS,
    	    byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalCmAdjustment cmAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
        throws GlobalBranchAccountNumberInvalidException {

    		Debug.print("ArInvoiceEntryControllerBean addCmDrEntry");

    		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
    		LocalGlChartOfAccountHome glChartOfAccountHome = null;
    		LocalAdCompanyHome adCompanyHome = null;


            // Initialize EJB Home

            try {

                cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
                    lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
                glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                    lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
                adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


            } catch (NamingException ex) {

                throw new EJBException(ex.getMessage());

            }

            try {

            	// get company

            	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		    // create distribution record

    		    LocalCmDistributionRecord cmDistributionRecord = cmDistributionRecordHome.create(
    			    DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_DBT,
    			    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

    			cmAdjustment.addCmDistributionRecord(cmDistributionRecord);
    			glChartOfAccount.addCmDistributionRecord(cmDistributionRecord);
            } catch (FinderException ex) {

            	throw new GlobalBranchAccountNumberInvalidException();

            } catch (Exception ex) {

            	Debug.printStackTrace(ex);
            	getSessionContext().setRollbackOnly();
            	throw new EJBException(ex.getMessage());

            }

    	}


    private void executeArRctPost(Integer RCT_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException {

        Debug.print("ArReceiptEntryControllerBean executeArRctPost");

        LocalArReceiptHome arReceiptHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalCmAdjustmentHome cmAdjustmentHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;


        LocalArReceipt arReceipt = null;

        // Initialize EJB Home

        try {

            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                	lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// validate if receipt is already deleted

        	try {

        		arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if receipt is already posted

        	if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyPostedException();

        		// validate if receipt void is already posted

        	} else if (arReceipt.getRctVoid() == EJBCommon.TRUE && arReceipt.getRctVoidPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyVoidPostedException();

        	}

        	// post receipt

        	Collection arDepositReceipts = null;
        	LocalArReceipt arDepositReceipt = null;
        	LocalArCustomer arCustomer = arReceipt.getArCustomer();

            if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() == EJBCommon.FALSE) {


        		if (arReceipt.getRctType().equals("COLLECTION")) {

        			double RCT_CRDTS = 0d;
        			double RCT_AI_APPLD_DPSTS = 0d;
        			double RCT_EXCSS_AMNT = arReceipt.getRctExcessAmount();

        			//create adjustment for advance payment
        			if(arReceipt.getRctEnableAdvancePayment()!=0){

        			}

        			// increase amount paid in invoice payment schedules and invoice

        			Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();

        			Iterator i = arAppliedInvoices.iterator();

        			while (i.hasNext()) {

        				LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

        				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = arAppliedInvoice.getArInvoicePaymentSchedule();

        				double AMOUNT_PAID = arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiAppliedDeposit() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiRebate();

        				double PENALTY_PAID = arAppliedInvoice.getAiPenaltyApplyAmount();


        				RCT_CRDTS += this.convertForeignToFunctionalCurrency(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcCode(),
        						arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcName(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
								arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiCreditableWTax(), AD_CMPNY);

        				RCT_AI_APPLD_DPSTS += this.convertForeignToFunctionalCurrency(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcCode(),
        						arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcName(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
								arAppliedInvoice.getAiAppliedDeposit(), AD_CMPNY);

        				arInvoicePaymentSchedule.setIpsAmountPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid() + AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				arInvoicePaymentSchedule.setIpsPenaltyPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsPenaltyPaid() + PENALTY_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));

        				arInvoicePaymentSchedule.getArInvoice().setInvAmountPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getArInvoice().getInvAmountPaid() + AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));

        				arInvoicePaymentSchedule.getArInvoice().setInvPenaltyPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getArInvoice().getInvPenaltyPaid() + PENALTY_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));

        				/*// Remove penalty
        				if(arInvoicePaymentSchedule.getIpsAmountDue() ==  arInvoicePaymentSchedule.getIpsAmountPaid() ){
        					arInvoicePaymentSchedule.getArInvoice().setInvInterest(EJBCommon.TRUE);
        				}*/
        				// release invoice lock

        				arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

        			}



    		     	// decrease customer balance

        			double RCT_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
        					arReceipt.getGlFunctionalCurrency().getFcName(),
							arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
							arReceipt.getRctAmount(), AD_CMPNY);

        			this.post(arReceipt.getRctDate(), (RCT_AMNT + RCT_CRDTS + RCT_AI_APPLD_DPSTS) * -1, arReceipt.getArCustomer(), AD_CMPNY);

        		}

        		// increase bank balance

        		LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

        		try {

        			// find bankaccount balance before or equal receipt date

        			Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

        			if (!adBankAccountBalances.isEmpty()) {

        				// get last check

        				ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

        				LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

        				if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

        					// create new balance

        					LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
        							arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmount(), "BOOK", AD_CMPNY);

        					adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

        				} else { // equals to check date

        					adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmount());

        				}

        			} else {

        				// create new balance

        				LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
        						arReceipt.getRctDate(), (arReceipt.getRctAmount()), "BOOK", AD_CMPNY);

        				adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

        			}

        			// propagate to subsequent balances if necessary

        			adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

        			Iterator i = adBankAccountBalances.iterator();

        			while (i.hasNext()) {

        				LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

        				adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmount());

        			}

        		} catch (Exception ex) {

        			ex.printStackTrace();

        		}

        		// set receipt post status

        		arReceipt.setRctPosted(EJBCommon.TRUE);
        		arReceipt.setRctPostedBy(USR_NM);
        		arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


        	} else if (arReceipt.getRctVoid() == EJBCommon.TRUE && arReceipt.getRctVoidPosted() == EJBCommon.FALSE) {// void receipt


        		if (arReceipt.getRctType().equals("COLLECTION")) {

        			double RCT_CRDTS = 0d;
        			double RCT_AI_APPLD_DPSTS = 0d;

        			// decrease amount paid in invoice payment schedules and invoice

        			Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();

        			Iterator i = arAppliedInvoices.iterator();

        			while (i.hasNext()) {

        				LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

        				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = arAppliedInvoice.getArInvoicePaymentSchedule();

        				double AMOUNT_PAID = arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiAppliedDeposit() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiRebate();

        				double PENALTY_PAID = arAppliedInvoice.getAiPenaltyApplyAmount();


        				RCT_CRDTS += this.convertForeignToFunctionalCurrency(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcCode(),
        						arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcName(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
								arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiCreditableWTax(), AD_CMPNY);

        				RCT_AI_APPLD_DPSTS += this.convertForeignToFunctionalCurrency(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcCode(),
        						arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getGlFunctionalCurrency().getFcName(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionDate(),
								arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvConversionRate(),
								arAppliedInvoice.getAiAppliedDeposit(), AD_CMPNY);



        				try {

        	    	   		Collection arAppliedCredits = arAppliedInvoice.getArAppliedCredits();

        	    	   		System.out.println("arAppliedCredits="+arAppliedCredits.size());
        			  	    Iterator x = arAppliedCredits.iterator();

        			  	    while (x.hasNext()) {

        			  	   	    LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)x.next();

        			  	  	    x.remove();

        			  	  	    arAppliedCredit.remove();

        			  	    }

        	    	   	   }catch(Exception ex){


        	    	   	   }

        				arInvoicePaymentSchedule.setIpsAmountPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid() - AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				arInvoicePaymentSchedule.setIpsPenaltyPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsPenaltyPaid() - PENALTY_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));

        				/*arInvoicePaymentSchedule.getArInvoice().setInvInterest(EJBCommon.TRUE);*/
        				arInvoicePaymentSchedule.getArInvoice().setInvAmountPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getArInvoice().getInvAmountPaid() - AMOUNT_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));
        				arInvoicePaymentSchedule.getArInvoice().setInvPenaltyPaid(EJBCommon.roundIt(arInvoicePaymentSchedule.getArInvoice().getInvPenaltyPaid() - PENALTY_PAID, this.getGlFcPrecisionUnit(AD_CMPNY)));


        			}


        			// increase customer balance

        			double RCT_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
        					arReceipt.getGlFunctionalCurrency().getFcName(),
							arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
							arReceipt.getRctAmount(), AD_CMPNY);

        			this.post(arReceipt.getRctDate(), RCT_AMNT + RCT_CRDTS + RCT_AI_APPLD_DPSTS, arReceipt.getArCustomer(), AD_CMPNY);

        		}

        		// decrease bank balance

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

				try {

					// find bankaccount balance before or equal receipt date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() - arReceipt.getRctAmount(), "BOOK", AD_CMPNY);

							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmount());

						}

					} else {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), (0 - arReceipt.getRctAmount()), "BOOK", AD_CMPNY);

						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmount());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}

        		// set receipt post status

	           arReceipt.setRctVoidPosted(EJBCommon.TRUE);
	           arReceipt.setRctPostedBy(USR_NM);
	           arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

        	}

        	// post to gl if necessary

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		// validate if date has no period and period is closed

        		LocalGlSetOfBook glJournalSetOfBook = null;

        		try {

        			glJournalSetOfBook = glSetOfBookHome.findByDate(arReceipt.getRctDate(), AD_CMPNY);

        		} catch (FinderException ex) {

        			throw new GlJREffectiveDateNoPeriodExistException();

        		}

        		LocalGlAccountingCalendarValue glAccountingCalendarValue =
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), arReceipt.getRctDate(), AD_CMPNY);


        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

        			throw new GlJREffectiveDatePeriodClosedException();

        		}

        		// check if invoice is balance if not check suspense posting

        		LocalGlJournalLine glOffsetJournalLine = null;

        		Collection arDistributionRecords = null;

        		System.out.println("receiptCode is : " + arReceipt.getRctCode());
        		if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

        			arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.FALSE, arReceipt.getRctCode(), AD_CMPNY);

        		} else {

        			arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.TRUE, arReceipt.getRctCode(), AD_CMPNY);

        		}


        		Iterator j = arDistributionRecords.iterator();

        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;

        		while (j.hasNext()) {

        			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

        			double DR_AMNT = 0d;

        			if (arDistributionRecord.getArAppliedInvoice() != null) {

        				LocalArInvoice arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice();
        				/*
        				DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
        						arInvoice.getGlFunctionalCurrency().getFcName(),
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);
						*/
        				DR_AMNT = arDistributionRecord.getDrAmount();
        				
        			} else {
        				
        				DR_AMNT = arDistributionRecord.getDrAmount();
        				
        				/*
        				DR_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
        						arReceipt.getGlFunctionalCurrency().getFcName(),
								arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);
        				*/
        			}

        			if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

        				TOTAL_DEBIT += DR_AMNT;

        			} else {

        				TOTAL_CREDIT += DR_AMNT;

        			}

        		}
        		
        		System.out.println("J DEBIT: " + TOTAL_DEBIT);
        		System.out.println("J CREDIT: " + TOTAL_CREDIT);
        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			LocalGlSuspenseAccount glSuspenseAccount = null;

        			try {

        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES", "SALES RECEIPTS", AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalJournalNotBalanceException();

        			}

        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

        				glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
								EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        			} else {

        				glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
								EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        			}

        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			glChartOfAccount.addGlJournalLine(glOffsetJournalLine);


        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			System.out.println("TOTAL_DEBIT="+TOTAL_DEBIT);
        			System.out.println("TOTAL_CREDIT="+TOTAL_CREDIT);
        			System.out.println("GlobalJournalNotBalanceException()");
        			throw new GlobalJournalNotBalanceException();

        		}


        		// create journal batch if necessary

        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        		try {

        			if (adPreference.getPrfEnableArReceiptBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arReceipt.getArReceiptBatch().getRbName(), AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES RECEIPTS", AD_BRNCH, AD_CMPNY);

        			}


        		} catch (FinderException ex) {
        		}

        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
        				glJournalBatch == null) {

        			if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arReceipt.getArReceiptBatch().getRbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			} else {

        				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES RECEIPTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        			}


        		}

        		// create journal entry
        		String customerName = arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() :
        			arReceipt.getRctCustomerName();

        		LocalGlJournal glJournal = glJournalHome.create(arReceipt.getRctReferenceNumber(),
        				arReceipt.getRctDescription(), arReceipt.getRctDate(),
						0.0d, null, arReceipt.getRctNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						arReceipt.getArCustomer().getCstTin(),
						customerName, EJBCommon.FALSE,
						null,
						AD_BRNCH, AD_CMPNY);

        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);

        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("SALES RECEIPTS", AD_CMPNY);
        		glJournal.setGlJournalCategory(glJournalCategory);


        		if (glJournalBatch != null) {

        			glJournal.setGlJournalBatch(glJournalBatch);

        		}


        		// create journal lines

        		j = arDistributionRecords.iterator();
        		boolean firstFlag = true;

        		while (j.hasNext()) {

        			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

        			double DR_AMNT = 0d;
        			LocalArInvoice arInvoice = null;

        			if (arDistributionRecord.getArAppliedInvoice() != null) {

        				arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice();

        				/*
        				DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
        						arInvoice.getGlFunctionalCurrency().getFcName(),
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);
						*/
        				DR_AMNT = arDistributionRecord.getDrAmount();
        			} else {
        				/*
        				DR_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
        						arReceipt.getGlFunctionalCurrency().getFcName(),
								arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);
        				*/
        				DR_AMNT = arDistributionRecord.getDrAmount();
        			}

        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(arDistributionRecord.getDrLine(),
							arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

        			arDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);

        			glJournal.addGlJournalLine(glJournalLine);

        			arDistributionRecord.setDrImported(EJBCommon.TRUE);

        			// for FOREX revaluation

            	    int FC_CODE = arDistributionRecord.getArAppliedInvoice() != null ?
            	    	arInvoice.getGlFunctionalCurrency().getFcCode().intValue() :
            	    	arReceipt.getGlFunctionalCurrency().getFcCode().intValue();

    		       	if((FC_CODE != adCompany.getGlFunctionalCurrency().getFcCode().intValue()) &&
    					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null && (FC_CODE ==
    						glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().intValue())){

    		       		double CONVERSION_RATE = arDistributionRecord.getArAppliedInvoice() != null ?
    		            	    arInvoice.getInvConversionRate() : arReceipt.getRctConversionRate();

    		            Date DATE = arDistributionRecord.getArAppliedInvoice() != null ?
    	    		    	arInvoice.getInvConversionDate() : arReceipt.getRctConversionDate();

    	    		    System.out.println("CONVERSION_RATE="+CONVERSION_RATE);

    		            if (DATE != null && (CONVERSION_RATE == 0 || CONVERSION_RATE == 1)){

    		            	CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    		            			glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
									glJournal.getJrConversionDate(), AD_CMPNY);

    		            } else if (CONVERSION_RATE == 0) {

    		            	CONVERSION_RATE = 1;

    		       		}

    		       		Collection glForexLedgers = null;

    		       		DATE = arDistributionRecord.getArAppliedInvoice() != null ?
        	    		    arInvoice.getInvDate() : arReceipt.getRctDate();

    		       		try {

    		       			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		       					DATE, glJournalLine.getGlChartOfAccount().getCoaCode(),
    								AD_CMPNY);

    		       		} catch(FinderException ex) {

    		       		}

    		       		LocalGlForexLedger glForexLedger =
    		       			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		       				(LocalGlForexLedger) glForexLedgers.iterator().next();

    		       		int FRL_LN = (glForexLedger != null &&
    		       			glForexLedger.getFrlDate().compareTo(DATE) == 0) ?
    		       				glForexLedger.getFrlLine().intValue() + 1 : 1;

    		       		// compute balance
    		       		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		       		double FRL_AMNT = arDistributionRecord.getDrAmount();

    		       		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));
    		       		else
    		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);

    		       		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

    		       		double FRX_GN_LSS = 0d;

    		       		if (glOffsetJournalLine != null && firstFlag) {

    		       			if(glOffsetJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))

    		       				FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ?
    		       						glOffsetJournalLine.getJlAmount() : (- 1 * glOffsetJournalLine.getJlAmount()));

    		       			else

    		       				FRX_GN_LSS = (glOffsetJournalLine.getJlDebit() == EJBCommon.TRUE ?
    		       						(- 1 * glOffsetJournalLine.getJlAmount()) : glOffsetJournalLine.getJlAmount());

    		       			firstFlag = false;

    		       		}

    		       		glForexLedger = glForexLedgerHome.create(DATE, new Integer (FRL_LN), "OR", FRL_AMNT,
    		       				CONVERSION_RATE, COA_FRX_BLNC, FRX_GN_LSS, AD_CMPNY);

    		       		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);

    		       		// propagate balances
    		       		try{

    		       			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		       					glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
    								glForexLedger.getFrlAdCompany());

    		       		} catch (FinderException ex) {

    		       		}

    		       		Iterator itrFrl = glForexLedgers.iterator();

    		       		while (itrFrl.hasNext()) {

    		       			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		       			FRL_AMNT = arDistributionRecord.getDrAmount();

    		       			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT :
    		       					(- 1 * FRL_AMNT));
    		       			else
    		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) :
    		       					FRL_AMNT);

    		       			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

    		       		}

    		       	}
        		}

        		if (glOffsetJournalLine != null) {

        			glJournal.addGlJournalLine(glOffsetJournalLine);

        		}

        		// post journal to gl

        		Collection glJournalLines = glJournal.getGlJournalLines();

        		Iterator i = glJournalLines.iterator();

        		while (i.hasNext()) {

        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

        			// post current to current acv

        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


        			// post to subsequent acvs (propagate)

        			Collection glSubsequentAccountingCalendarValues =
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

        			while (acvsIter.hasNext()) {

        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        					(LocalGlAccountingCalendarValue)acvsIter.next();

        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        			}

        			// post to subsequent years if necessary

        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

        				Iterator sobIter = glSubsequentSetOfBooks.iterator();

        				while (sobIter.hasNext()) {

        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

        					// post to subsequent acvs of subsequent set of book(propagate)

        					Collection glAccountingCalendarValues =
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

        					Iterator acvIter = glAccountingCalendarValues.iterator();

        					while (acvIter.hasNext()) {

        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        							(LocalGlAccountingCalendarValue)acvIter.next();

        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						} else { // revenue & expense

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						}

        					}

        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

        				}

        			}

        		}




        		if(arReceipt.getArCustomer().getApSupplier()!=null){

    		    	// Post Investors Account balance
    			       byte scLedger = arReceipt.getArCustomer().getApSupplier().getApSupplierClass().getScLedger();


    				    // post current to current acv
    				if(scLedger == EJBCommon.TRUE){

                        this.postToGlInvestor(glAccountingCalendarValue,
                        			arReceipt.getArCustomer().getApSupplier(),
    								true,
    								arReceipt.getRctInvtrBeginningBalance(),
    								arReceipt.getRctVoid() == EJBCommon.FALSE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								arReceipt.getRctVoid() == EJBCommon.FALSE ? arReceipt.getRctAmount() : -arReceipt.getRctAmount(),
    								AD_CMPNY);


    				       	// post to subsequent acvs (propagate)

    				       	Collection glSubsequentAccountingCalendarValues =
    				       		glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
    				       				glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
    									glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

    				       	Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

    				       	while (acvsIter.hasNext()) {

    				       		LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    				       			(LocalGlAccountingCalendarValue)acvsIter.next();


    				       		this.postToGlInvestor(glSubsequentAccountingCalendarValue,
    					       			arReceipt.getArCustomer().getApSupplier(),
    					       			false,
    					       			EJBCommon.FALSE,
    					       			arReceipt.getRctVoid() == EJBCommon.FALSE ? EJBCommon.FALSE : EJBCommon.TRUE,
    					       			arReceipt.getRctVoid() == EJBCommon.FALSE ? arReceipt.getRctAmount() : -arReceipt.getRctAmount(),
    					       			AD_CMPNY);

    				       	}


    				       	// post to subsequent years if necessary

    				       	Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

    				       	if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

    				       		adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    				       		Iterator sobIter = glSubsequentSetOfBooks.iterator();

    				       		while (sobIter.hasNext()) {

    				       			LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

    				       			// post to subsequent acvs of subsequent set of book(propagate)

    				       			Collection glAccountingCalendarValues =
    				       				glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

    				       			Iterator acvIter = glAccountingCalendarValues.iterator();

    				       			while (acvIter.hasNext()) {

    				       				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    				       					(LocalGlAccountingCalendarValue)acvIter.next();

    				       				this.postToGlInvestor(glSubsequentAccountingCalendarValue,
    							       			arReceipt.getArCustomer().getApSupplier(),
    							       			false,
    							       			arReceipt.getRctInvtrBeginningBalance(),
    							       			arReceipt.getRctVoid() == EJBCommon.FALSE ? EJBCommon.FALSE : EJBCommon.TRUE,
    							       					arReceipt.getRctVoid() == EJBCommon.FALSE ? arReceipt.getRctAmount() : -arReceipt.getRctAmount(),
    							       			AD_CMPNY);

    				       			}

    				       			if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

    				       		}

    				       	}
                    }




    		      }

        	}

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void post(Date RCT_DT, double RCT_AMNT, LocalArCustomer arCustomer, Integer AD_CMPNY) {

       Debug.print("ArReceiptEntryControllerBean post");

       LocalArCustomerBalanceHome arCustomerBalanceHome = null;

       // Initialize EJB Home

       try {

           arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
               lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);

       } catch (NamingException ex) {

           getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

       }

       try {

	       // find customer balance before or equal invoice date

	       Collection arCustomerBalances = arCustomerBalanceHome.findByBeforeOrEqualInvDateAndCstCustomerCode(RCT_DT, arCustomer.getCstCustomerCode(), AD_CMPNY);

	       if (!arCustomerBalances.isEmpty()) {

	    	   // get last invoice

	    	   ArrayList arCustomerBalanceList = new ArrayList(arCustomerBalances);

	    	   LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance)arCustomerBalanceList.get(arCustomerBalanceList.size() - 1);

	    	   if (arCustomerBalance.getCbDate().before(RCT_DT)) {

	    	       // create new balance

	    	       LocalArCustomerBalance arNewCustomerBalance = arCustomerBalanceHome.create(
		    	       RCT_DT, arCustomerBalance.getCbBalance() + RCT_AMNT, AD_CMPNY);

		           arCustomer.addArCustomerBalance(arNewCustomerBalance);

	    	   } else { // equals to invoice date

	    	       arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + RCT_AMNT);

	    	   }

	    	} else {

	    	    // create new balance

		    	LocalArCustomerBalance arNewCustomerBalance = arCustomerBalanceHome.create(
		    		RCT_DT, RCT_AMNT, AD_CMPNY);

		        arCustomer.addArCustomerBalance(arNewCustomerBalance);

	     	}

	     	// propagate to subsequent balances if necessary

	     	arCustomerBalances = arCustomerBalanceHome.findByAfterInvDateAndCstCustomerCode(RCT_DT, arCustomer.getCstCustomerCode(), AD_CMPNY);

	     	Iterator i = arCustomerBalances.iterator();

	     	while (i.hasNext()) {

	     		LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance)i.next();

	     		arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + RCT_AMNT);

	     	}

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

	}

   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
      LocalGlChartOfAccount glChartOfAccount,
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

      Debug.print("ArReceiptEntryControllerBean postToGl");

      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance =
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);

	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {

 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

					}


			  } else {

				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

					}

		 	  }

		 	  if (isCurrentAcv) {

			 	 if (isDebit == EJBCommon.TRUE) {

		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

		 		 } else {

		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
		 		 }

		 	}

       } catch (Exception ex) {

       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());

       }


   }


   private void postToGlInvestor(LocalGlAccountingCalendarValue glAccountingCalendarValue,
		      LocalApSupplier apSupplier,
		      boolean isCurrentAcv, byte isBeginningBalance, byte isDebit, double JL_AMNT,Integer AD_CMPNY) {

		      Debug.print("ArReceiptEntryControllerBean postToGlInvestor");

		      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		      LocalAdCompanyHome adCompanyHome = null;

		      LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;


		       // Initialize EJB Home

		       try {

		           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
		              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
		           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
		           glInvestorAccountBalanceHome = (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
				              lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);




		       } catch (NamingException ex) {

		           throw new EJBException(ex.getMessage());

		       }

		       try {

		               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);


			       	   LocalGlInvestorAccountBalance glInvestorAccountBalance =
			       			glInvestorAccountBalanceHome.findByAcvCodeAndSplCode(
			       					glAccountingCalendarValue.getAcvCode(),
					               	  apSupplier.getSplCode(), AD_CMPNY);


			           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();


			           glInvestorAccountBalance.setIrabEndingBalance(
						       EJBCommon.roundIt(glInvestorAccountBalance.getIrabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));


                             if (!isCurrentAcv) {

                                 glInvestorAccountBalance.setIrabBeginningBalance(
                                         EJBCommon.roundIt(glInvestorAccountBalance.getIrabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

                             }

                               if (isCurrentAcv) {

                                      glInvestorAccountBalance.setIrabTotalCredit(
                                                             EJBCommon.roundIt(glInvestorAccountBalance.getIrabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));

                             }


                               if(isBeginningBalance!=0){
                                    glInvestorAccountBalance.setIrabBonus(EJBCommon.TRUE);
                                    glInvestorAccountBalance.setIrabInterest(EJBCommon.TRUE);
                               }else{
                                   glInvestorAccountBalance.setIrabBonus(EJBCommon.FALSE);
                                    glInvestorAccountBalance.setIrabInterest(EJBCommon.FALSE);
                               }

		       } catch (Exception ex) {

		       	   Debug.printStackTrace(ex);
		       	   throw new EJBException(ex.getMessage());

		       }

		   }



   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public ArrayList getArReceiptReportParameters(Integer AD_CMPNY) {

       Debug.print("ArReceiptEntryControllerBean getArReceiptReportParameters");

       ArrayList list = new ArrayList();
       LocalAdLookUpValueHome adLookUpValueHome = null;


       // Initialize EJB Home

       try {

       	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }


       try {

       	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR PRINT RECEIPT PARAMETER", AD_CMPNY);

       	if(adLookUpValues.size() <=0 ) {
       		return list;
       	}

       	Iterator i = adLookUpValues.iterator();

       	while(i.hasNext()) {

       		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
       		System.out.println(adLookUpValue.getLvName());
       		list.add(adLookUpValue.getLvName());
       	}


       	return list;



       } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

       }

   }

   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public ArrayList getDocumentTypeList(String DCMNT_TYP, Integer AD_CMPNY) {

   	Debug.print("ArReceiptEntryControllerBean getDocumentTypeList");

       ArrayList list = new ArrayList();
       LocalAdLookUpValueHome adLookUpValueHome = null;


       // Initialize EJB Home

       try {

       	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }


       try {

       	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR RECEIPT DOCUMENT TYPE", AD_CMPNY);

       	if(adLookUpValues.size() <=0 ) {
       		return list;
       	}

       	Iterator i = adLookUpValues.iterator();

       	while(i.hasNext()) {

       		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
       		System.out.println(adLookUpValue.getLvName());
       		list.add(adLookUpValue.getLvName());
       	}


       	return list;



       } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

       }

   }


	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArReceiptEntryControllerBean ejbCreate");

    }

}