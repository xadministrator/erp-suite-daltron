/*
 * ArReceiptImportControllerBean.java
 *
 * Created on January 13, 2006, 9:00 AM
 *
 * @author:  Aliza D.J. Anos
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchStandardMemoLine;
import com.ejb.ad.LocalAdBranchStandardMemoLineHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArAutoAccountingSegment;
import com.ejb.ar.LocalArAutoAccountingSegmentHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArReceiptImportPreference;
import com.ejb.ar.LocalArReceiptImportPreferenceHome;
import com.ejb.ar.LocalArReceiptImportPreferenceLine;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.ArINVInvoiceDoesNotExist;
import com.ejb.exception.ArINVOverapplicationNotAllowedException;
import com.ejb.exception.ArRICustomerRequiredException;
import com.ejb.exception.GlobalAmountInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalNotAllTransactionsArePostedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordDisabledException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalRecordInvalidForCurrentBranchException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.genfld.LocalGenField;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.ArModAppliedInvoiceDetails;
import com.util.ArModDistributionRecordDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.ArModReceiptDetails;
import com.util.ArModReceiptImportPreferenceDetails;
import com.util.ArReceiptImportPreferenceLineDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArReceiptImportControllerEJB"
 *           display-name="used for importing receipts"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArReceiptImportControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArReceiptImportController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArReceiptImportControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 */

public class ArReceiptImportControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public ArModReceiptImportPreferenceDetails getArRipByRipType(String RIP_TYPE, Integer AD_CMPNY) throws
		GlobalNoRecordFoundException {

		Debug.print("ArReceiptImportControllerBean getArRipByRipType");

		LocalArReceiptImportPreferenceHome arReceiptImportPreferenceHome = null;

		try {

			arReceiptImportPreferenceHome = (LocalArReceiptImportPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptImportPreferenceHome.JNDI_NAME, LocalArReceiptImportPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArReceiptImportPreference arReceiptImportPreference = null;

			try {

				arReceiptImportPreference = arReceiptImportPreferenceHome.findByRipType(RIP_TYPE, AD_CMPNY);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ArModReceiptImportPreferenceDetails details = new ArModReceiptImportPreferenceDetails();

			details.setRipType(arReceiptImportPreference.getRipType());
			details.setRipIsSummarized(arReceiptImportPreference.getRipIsSummarized());
			details.setRipCustomerColumn(arReceiptImportPreference.getRipCustomerColumn());
			details.setRipCustomerFile(arReceiptImportPreference.getRipCustomerFile());
			details.setRipDateColumn(arReceiptImportPreference.getRipDateColumn());
			details.setRipDateFile(arReceiptImportPreference.getRipDateFile());
			details.setRipReceiptNumberColumn(arReceiptImportPreference.getRipReceiptNumberColumn());
			details.setRipReceiptNumberFile(arReceiptImportPreference.getRipReceiptNumberFile());
			details.setRipReceiptNumberLineColumn(arReceiptImportPreference.getRipReceiptNumberLineColumn());
			details.setRipReceiptNumberLineFile(arReceiptImportPreference.getRipReceiptNumberLineFile());
			details.setRipReceiptAmountColumn(arReceiptImportPreference.getRipReceiptAmountColumn());
			details.setRipReceiptAmountFile(arReceiptImportPreference.getRipReceiptAmountFile());
			details.setRipTcColumn(arReceiptImportPreference.getRipTcColumn());
			details.setRipTcFile(arReceiptImportPreference.getRipTcFile());
			details.setRipWtcColumn(arReceiptImportPreference.getRipWtcColumn());
			details.setRipWtcFile(arReceiptImportPreference.getRipWtcFile());
			details.setRipItemNameColumn(arReceiptImportPreference.getRipItemNameColumn());
			details.setRipItemNameFile(arReceiptImportPreference.getRipItemNameFile());
			details.setRipLocationNameColumn(arReceiptImportPreference.getRipLocationNameColumn());
			details.setRipLocationNameFile(arReceiptImportPreference.getRipLocationNameFile());
			details.setRipUomNameColumn(arReceiptImportPreference.getRipUomNameColumn());
			details.setRipUomNameFile(arReceiptImportPreference.getRipUomNameFile());
			details.setRipQuantityColumn(arReceiptImportPreference.getRipQuantityColumn());
			details.setRipQuantityFile(arReceiptImportPreference.getRipQuantityFile());
			details.setRipUnitPriceColumn(arReceiptImportPreference.getRipUnitPriceColumn());
			details.setRipUnitPriceFile(arReceiptImportPreference.getRipUnitPriceFile());
			details.setRipTotalColumn(arReceiptImportPreference.getRipTotalColumn());
			details.setRipTotalFile(arReceiptImportPreference.getRipTotalFile());
			details.setRipMemoLineColumn(arReceiptImportPreference.getRipMemoLineColumn());
			details.setRipMemoLineFile(arReceiptImportPreference.getRipMemoLineFile());
			details.setRipInvoiceNumberColumn(arReceiptImportPreference.getRipInvoiceNumberColumn());
			details.setRipInvoiceNumberFile(arReceiptImportPreference.getRipInvoiceNumberFile());
			details.setRipApplyAmountColumn(arReceiptImportPreference.getRipApplyAmountColumn());
			details.setRipApplyAmountFile(arReceiptImportPreference.getRipApplyAmountFile());
			details.setRipApplyDiscountColumn(arReceiptImportPreference.getRipApplyDiscountColumn());
			details.setRipApplyDiscountFile(arReceiptImportPreference.getRipApplyDiscountFile());

			Collection arReceiptImportPreferenceLines = arReceiptImportPreference.getArReceiptImportPreferenceLines();
			Iterator i = arReceiptImportPreferenceLines.iterator();

			while(i.hasNext()) {

				LocalArReceiptImportPreferenceLine arReceiptImportPreferenceLine = (LocalArReceiptImportPreferenceLine)i.next();

				ArReceiptImportPreferenceLineDetails rilDetails = new ArReceiptImportPreferenceLineDetails();

				rilDetails.setRilType(arReceiptImportPreferenceLine.getRilType());
				rilDetails.setRilName(arReceiptImportPreferenceLine.getRilName());
				rilDetails.setRilGlAccountNumber(arReceiptImportPreferenceLine.getRilGlAccountNumber());
				rilDetails.setRilBankAccountName(arReceiptImportPreferenceLine.getRilBankAccountName());
				rilDetails.setRilColumn(arReceiptImportPreferenceLine.getRilColumn());
				rilDetails.setRilFile(arReceiptImportPreferenceLine.getRilFile());
				rilDetails.setRilAmountColumn(arReceiptImportPreferenceLine.getRilAmountColumn());

				details.saveRipRilList(rilDetails);

			}

			return details;

		} catch (GlobalNoRecordFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ArReceiptImportControllerBean getGlFcPrecisionUnit");

		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}




	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ArReceiptImportControllerBean getArCstAll");

		LocalArCustomerHome arCustomerHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

			Iterator i = arCustomers.iterator();

			while (i.hasNext()) {

				LocalArCustomer arCustomer = (LocalArCustomer)i.next();

				list.add(arCustomer.getCstName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArTaxCodeAll(Integer AD_CMPNY) {

		Debug.print("ArReceiptImportControllerBean getArTaxCodeAll");

		LocalArTaxCodeHome arTaxCodeHome = null;

		// Initialize EJB Home

		try {

			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		ArrayList list = new ArrayList();

		try {

			Collection arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);

			Iterator i = arTaxCodes.iterator();

			while(i.hasNext()) {

				LocalArTaxCode arTaxCode = (LocalArTaxCode)i.next();

				list.add(arTaxCode.getTcName());

			}

		} catch(Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

		return list;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void importRctAi(ArrayList rctList, boolean isSummarized, String RB_NM, String TC_NM, String CRTD_BY, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalDocumentNumberNotUniqueException,
		GlobalTransactionAlreadyLockedException,
		GlobalNotAllTransactionsArePostedException,
		ArINVInvoiceDoesNotExist,
		GlobalNoRecordFoundException,
		ArINVOverapplicationNotAllowedException,
		GlobalRecordInvalidException,
		GlobalRecordInvalidForCurrentBranchException,
		GlobalRecordDisabledException,
		GlobalJournalNotBalanceException,
		GlobalAmountInvalidException,
		ArRICustomerRequiredException {

		Debug.print("ArReceiptImportControllerBean importRctAi");

		LocalArReceiptHome arReceiptHome = null;
		LocalArReceiptBatchHome arReceiptBatchHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;

		LocalArReceipt arReceipt = null;

		// Initialize EJB Home

		try {

			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArReceiptBatch arReceiptBatch = arReceiptBatchHome.create(RB_NM, null, "OPEN", "COLLECTION",
					new java.util.Date(), CRTD_BY, AD_BRNCH, AD_CMPNY);

			Iterator rctIter = rctList.iterator();

			while(rctIter.hasNext()) {

				ArModReceiptDetails details = (ArModReceiptDetails) rctIter.next();

				if(details.getRctAmount() < 0) {

					throw new GlobalAmountInvalidException("total amount for receipt " + details.getRctNumber());

				}

				Date RCPT_DATE = null;
				Date CURR_DATE = EJBCommon.getGcCurrentDateWoTime().getTime();

				if(isSummarized) {

					RCPT_DATE = CURR_DATE;

				} else {

					RCPT_DATE = details.getRctDate();

				}

				// validate if document number is unique document number is automatic then set next sequence

				String RCPT_NMBR = null;

		    	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	        	if (isSummarized) {
		    		//generate document number
	        		try {

	 					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT", AD_CMPNY);

	 				} catch (FinderException ex) {

	 				}

	 				try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) {

	 				}

			        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A') {

			            while (true) {

			            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

			            		try {

				            		arReceiptHome.findByRctNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

				            	} catch (FinderException ex) {

				            		RCPT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
						            break;

				            	}

			            	} else {

			            		try {

				            		arReceiptHome.findByRctNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

				            	} catch (FinderException ex) {

				            		RCPT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
						            break;

				            	}

			            	}

			            }

			        }

			    } else {

			    	LocalArReceipt arExistingReceipt = null;

			    	try {

		    		    arExistingReceipt = arReceiptHome.findByRctNumberAndBrCode(
		        		    details.getRctNumber(), AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {
		        	}

		        	if (arExistingReceipt != null &&
		                !arExistingReceipt.getRctCode().equals(details.getRctCode())) {

		            	throw new GlobalDocumentNumberNotUniqueException(details.getRctNumber());

		            }

		            RCPT_NMBR = details.getRctNumber();

			    }

				// create receipt

				arReceipt = arReceiptHome.create("COLLECTION", null, RCPT_DATE, RCPT_NMBR, null, null,null,
						null,null,null,null,null,
						0d, 0d, 0d, 0d, 0d,  0d, 0d,
						null, details.getRctConversionRate(),
						null, null, EJBCommon.FALSE, 0d, null, null, EJBCommon.FALSE,
						null, EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
					    null, null, null, null, null,
						CRTD_BY, CURR_DATE, CRTD_BY, CURR_DATE,
						null, null, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null, null,
						EJBCommon.FALSE, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);

				//add customer
				LocalArCustomer arCustomer = null;

				if(details.getRctCstName() == null || details.getRctCstName().equals("")) {

					throw new ArRICustomerRequiredException(arReceipt.getRctNumber());

				}

				try {

					arCustomer = arCustomerHome.findByCstName(details.getRctCstName(), AD_BRNCH, AD_CMPNY);

					if(arCustomer.getCstEnable() == EJBCommon.TRUE){

						arCustomer.addArReceipt(arReceipt);

					} else {

						throw new GlobalRecordDisabledException("Customer: " + details.getRctCstName());

					}

				} catch (FinderException ex) {

					throw new GlobalRecordInvalidForCurrentBranchException("Customer: " + details.getRctCstName());

				}

				LocalAdBankAccount adBankAccount = arCustomer.getAdBankAccount();
				adBankAccount.addArReceipt(arReceipt);

				//add withholding tax

				try {

					LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName("NONE", AD_CMPNY);

					if(arWithholdingTaxCode.getWtcEnable() == EJBCommon.TRUE) {

						arWithholdingTaxCode.addArReceipt(arReceipt);

					} else {

						throw new GlobalRecordDisabledException("Withholding Tax: NONE");

					}

				} catch(FinderException ex) {

					throw new GlobalNoRecordFoundException("Withholding Tax: NONE");

				}

				//add tax
				LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("NONE", AD_CMPNY);
				arTaxCode.addArReceipt(arReceipt);

				//add receipt batch
				arReceiptBatch.addArReceipt(arReceipt);

				//add functional currency
				LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(details.getRctFcName(), AD_CMPNY);

				if(glFunctionalCurrency.getFcEnable() == EJBCommon.TRUE) {

					glFunctionalCurrency.addArReceipt(arReceipt);

				} else {

					throw new GlobalRecordDisabledException("Currency: " + details.getRctFcName());

				}

				// add new applied invoices and distribution record

				double TOTAL_AMOUNT = 0d;
				double TOTAL_LINE = 0d;
				double TOTAL_DISC = 0d;

				for(int i=0; i<details.getRctAiListSize(); i++) {

					ArModAppliedInvoiceDetails mAiDetails = details.getRctAiListByIndex(i);

					if(mAiDetails.getAiApplyAmount() < 0) {

						throw new GlobalAmountInvalidException("apply amount for " + mAiDetails.getAiIpsInvNumber() + " of receipt " + arReceipt.getRctNumber());

					}

					if(mAiDetails.getAiDiscountAmount() < 0) {

						throw new GlobalAmountInvalidException("discount amount for " + mAiDetails.getAiIpsInvNumber() + " of receipt " + arReceipt.getRctNumber());

					}

					TOTAL_AMOUNT += mAiDetails.getAiApplyAmount();

					LocalArInvoice arInvoice = null;

					try {

						arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(mAiDetails.getAiIpsInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
						if(arInvoice.getInvPosted() == EJBCommon.FALSE) {

							throw new GlobalNotAllTransactionsArePostedException(mAiDetails.getAiIpsInvNumber());

						}

					} catch(FinderException ex) {

						throw new ArINVInvoiceDoesNotExist(mAiDetails.getAiIpsInvNumber());

					}

					//check if receipt's customer matches invoice's customer

					if(!arCustomer.getCstCustomerCode().equals(arInvoice.getArCustomer().getCstCustomerCode())) {

						throw new GlobalRecordInvalidException(arInvoice.getInvNumber() + ": Customer " + arCustomer.getCstName() + " - " + arInvoice.getArCustomer().getCstName());

					}

					Collection arInvoicePaymentSchedules = arInvoicePaymentScheduleHome.findOpenIpsByInvNumber(mAiDetails.getAiIpsInvNumber(), AD_BRNCH, AD_CMPNY);

					Iterator ipsIter = arInvoicePaymentSchedules.iterator();

					while(ipsIter.hasNext()) {

						LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)ipsIter.next();

						ArModAppliedInvoiceDetails mDetails = new ArModAppliedInvoiceDetails();

						mDetails.setAiIpsCode(arInvoicePaymentSchedule.getIpsCode());
						mDetails.setAiReceiptNumber(mAiDetails.getAiReceiptNumber());
						mDetails.setAiIpsInvNumber(mAiDetails.getAiIpsInvNumber());
						mDetails.setAiIpsInvReferenceNumber(mAiDetails.getAiIpsInvReferenceNumber());

						if(mAiDetails.getAiApplyAmount() == 0) {

							break;

						}

						Collection arAppliedInvoices = arAppliedInvoiceHome.findByIpsCode(arInvoicePaymentSchedule.getIpsCode(), AD_CMPNY);

						Iterator aiIter = arAppliedInvoices.iterator();

						double TOTAL_PAID = 0d;

						while(aiIter.hasNext()) {

							LocalArAppliedInvoice existingArAppliedInvoice = (LocalArAppliedInvoice)aiIter.next();

							TOTAL_PAID += existingArAppliedInvoice.getAiApplyAmount();

						}

						if(TOTAL_PAID == arInvoicePaymentSchedule.getIpsAmountDue() && arInvoicePaymentSchedule.getIpsLock() == EJBCommon.TRUE) {

							if(ipsIter.hasNext())
								continue;
							else
								throw new ArINVOverapplicationNotAllowedException(arInvoicePaymentSchedule.getArInvoice().getInvNumber());

						}

						Collection arReceipts = arReceiptBatch.getArReceipts();

						double TOTAL_APPLY_AMOUNT = 0d;

						Iterator rIter = arReceipts.iterator();

						boolean found = true;

						while(rIter.hasNext()) {

							LocalArReceipt arRcpt = (LocalArReceipt)rIter.next();

							Collection arRcptAppliedInvoices = arRcpt.getArAppliedInvoices();

							Iterator rcptAiIter = arRcptAppliedInvoices.iterator();

							while(rcptAiIter.hasNext()) {

								LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)rcptAiIter.next();

								if(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvNumber().equals(mAiDetails.getAiIpsInvNumber())) {
									TOTAL_APPLY_AMOUNT += arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiDiscountAmount();
									found = true;
								}

							}

						}

						double BALANCE_AMOUNT = 0d;

						if(found == true) {

							BALANCE_AMOUNT = arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid() - TOTAL_APPLY_AMOUNT;

						} else {

							BALANCE_AMOUNT = arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid();

						}

						if(ipsIter.hasNext() && mAiDetails.getAiApplyAmount() > BALANCE_AMOUNT) {

							mDetails.setAiApplyAmount(BALANCE_AMOUNT);
							mAiDetails.setAiApplyAmount(mAiDetails.getAiApplyAmount() - BALANCE_AMOUNT);

						} else {

							mDetails.setAiApplyAmount(mAiDetails.getAiApplyAmount());
			        		mDetails.setAiDiscountAmount(mAiDetails.getAiDiscountAmount());
			        		mAiDetails.setAiApplyAmount(0);

						}

						LocalArAppliedInvoice arAppliedInvoice = this.addArAiEntry(mDetails, arReceipt, arReceiptBatch, AD_CMPNY);

						TOTAL_LINE += arAppliedInvoice.getAiApplyAmount();
						TOTAL_DISC += arAppliedInvoice.getAiDiscountAmount();

						// create discount distribution records if necessary

						if (arAppliedInvoice.getAiDiscountAmount() != 0) {

							short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

							// get discount percent

							double DISCOUNT_PERCENT = EJBCommon.roundIt(arAppliedInvoice.getAiDiscountAmount() / (arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount()), (short)6);
							DISCOUNT_PERCENT = EJBCommon.roundIt(DISCOUNT_PERCENT * ((arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount()) / arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvAmountDue()), (short)6);

							Collection arDiscountDistributionRecords = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArDistributionRecords();

							// get total debit and credit for rounding difference calculation

							double TOTAL_DEBIT = 0d;
							double TOTAL_CREDIT = 0d;
							boolean isRoundingDifferenceCalculated = false;

							Iterator j = arDiscountDistributionRecords.iterator();

							while (j.hasNext()) {

								LocalArDistributionRecord arDiscountDistributionRecord = (LocalArDistributionRecord)j.next();

								if (arDiscountDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

									TOTAL_DEBIT += EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);

								} else {

									TOTAL_CREDIT += EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);

								}

							}


							j = arDiscountDistributionRecords.iterator();

							while (j.hasNext()) {

								LocalArDistributionRecord arDiscountDistributionRecord = (LocalArDistributionRecord)j.next();

								if (arDiscountDistributionRecord.getDrClass().equals("RECEIVABLE")) continue;

								double DR_AMNT = EJBCommon.roundIt(arDiscountDistributionRecord.getDrAmount() * DISCOUNT_PERCENT, precisionUnit);

								// calculate rounding difference if necessary

								if (arDiscountDistributionRecord.getDrDebit() == EJBCommon.FALSE &&
										TOTAL_DEBIT != TOTAL_CREDIT && !isRoundingDifferenceCalculated) {

									DR_AMNT = DR_AMNT +  TOTAL_DEBIT - TOTAL_CREDIT;

									isRoundingDifferenceCalculated = true;

								}

								if (arDiscountDistributionRecord.getDrClass().equals("REVENUE")) {

									this.addArDrEntry(arReceipt.getArDrNextLine(), "SALES DISCOUNT",
											EJBCommon.TRUE,	DR_AMNT, EJBCommon.FALSE,
											arReceipt.getAdBankAccount().getBaCoaGlSalesDiscount(), arReceipt,
											arAppliedInvoice, AD_BRNCH, AD_CMPNY);

								} else {


									this.addArDrEntry(arReceipt.getArDrNextLine(), arDiscountDistributionRecord.getDrClass(),
											arDiscountDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
											DR_AMNT, EJBCommon.FALSE, arDiscountDistributionRecord.getGlChartOfAccount().getCoaCode(),
											arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);

								}

							}
						}

						// get receivable account

						LocalArDistributionRecord arDistributionRecord =
							arDistributionRecordHome.findByDrClassAndInvCode("RECEIVABLE", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);

						this.addArDrEntry(arReceipt.getArDrNextLine(), "RECEIVABLE", EJBCommon.FALSE,
								arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount(),
								EJBCommon.FALSE, arDistributionRecord.getGlChartOfAccount().getCoaCode(),
								arReceipt, arAppliedInvoice, AD_BRNCH, AD_CMPNY);

					}

				}

				if(details.getRctAmount() != TOTAL_LINE) {

					throw new GlobalJournalNotBalanceException(arReceipt.getRctNumber());

				}

				// create cash distribution record

				this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH", EJBCommon.TRUE,
						TOTAL_AMOUNT, EJBCommon.FALSE, arReceipt.getAdBankAccount().getBaCoaGlCashAccount(),
						arReceipt, null, AD_BRNCH, AD_CMPNY);

				arReceipt.setRctAmount(TOTAL_AMOUNT);

			}

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyLockedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNotAllTransactionsArePostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (ArINVInvoiceDoesNotExist ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoRecordFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (ArINVOverapplicationNotAllowedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordInvalidForCurrentBranchException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordDisabledException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalAmountInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (ArRICustomerRequiredException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}


	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void importRctIli(ArrayList miscRctList, boolean isSummarized, String RB_NM, String TC_NM, boolean taxableServiceCharge,
			boolean taxableDiscount, String CRTD_BY, Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalInvItemLocationNotFoundException,
			GlobalBranchAccountNumberInvalidException,
			GlobalNoRecordFoundException,
			GlobalDocumentNumberNotUniqueException,
			GlobalRecordInvalidForCurrentBranchException,
			GlobalRecordInvalidException,
			GlobalRecordDisabledException,
			GlobalJournalNotBalanceException,
			GlobalAmountInvalidException,
			GlobalRecordAlreadyExistException {

		Debug.print("ArReceiptImportControllerBean importRctIli");

		LocalAdCompanyHome adCompanyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalArReceiptBatchHome arReceiptBatchHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvLocationHome invLocationHome = null;

		LocalArWithholdingTaxCode arWithholdingTaxCode = null;
		LocalArTaxCode arTaxCode = null;
		LocalArReceipt arReceipt = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Date CURR_DATE = EJBCommon.getGcCurrentDateWoTime().getTime();

			//check if POS data already exists

			ArModReceiptDetails rctDetails = (ArModReceiptDetails)miscRctList.get(0);

			Collection arReceipts = arReceiptHome.findByRctDateAndBaName(rctDetails.getRctDate(), "POS%", AD_CMPNY);

			if(!arReceipts.isEmpty()) {

				throw new GlobalRecordAlreadyExistException(EJBCommon.convertSQLDateToString(rctDetails.getRctDate()));

			}

			LocalArReceiptBatch arReceiptBatch = arReceiptBatchHome.create(RB_NM, null, "OPEN", "MISC", CURR_DATE, CRTD_BY, AD_BRNCH, AD_CMPNY);


			Iterator mrIter = miscRctList.iterator();

			while(mrIter.hasNext()) {

				ArModReceiptDetails details = (ArModReceiptDetails)mrIter.next();

				if(details.getRctAmount() < 0) {

					throw new GlobalAmountInvalidException("total amount for receipt " + details.getRctNumber());

				}

				// validate if receipt number is unique receipt number is automatic then set next sequence

				String RCPT_NMBR = null;

		    	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	        	if (isSummarized) {

	        		try {

	 					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT", AD_CMPNY);

	 				} catch (FinderException ex) {

	 				}

	 				try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) {

	 				}

			        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A') {

			            while (true) {

			            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

			            		try {

				            		arReceiptHome.findByRctNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

				            	} catch (FinderException ex) {

				            		RCPT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
						            break;

				            	}

			            	} else {

			            		try {

				            		arReceiptHome.findByRctNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

				            	} catch (FinderException ex) {

				            		RCPT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
						            break;

				            	}

			            	}

			            }

			        }

			    } else {

			    	LocalArReceipt arExistingReceipt = null;

			    	try {

		    		    arExistingReceipt = arReceiptHome.findByRctNumberAndBrCode(details.getRctNumber(), AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {
		        	}

		        	if (arExistingReceipt != null &&
		                !arExistingReceipt.getRctCode().equals(details.getRctCode())) {

		            	throw new GlobalDocumentNumberNotUniqueException(details.getRctNumber());

		            }

		            RCPT_NMBR = details.getRctNumber();

			    }

				//create misc receipt

				arReceipt = arReceiptHome.create("MISC", null, details.getRctDate(), RCPT_NMBR, null, null, null,
						null,null,null,null,null,
						0d, 0d, 0d, 0d, 0d,  0d, 0d,
						null, details.getRctConversionRate(), null, null, EJBCommon.FALSE, 0d, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
					    null, null, null, null, null,
						CRTD_BY, CURR_DATE, CRTD_BY, CURR_DATE, null, null, null, null,
						EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null, null,
						EJBCommon.FALSE, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);

				//add bank account
				LocalAdBankAccount adBankAccount = null;

				try {

					adBankAccount = adBankAccountHome.findByBaNameAndBrCode(details.getRctBaName(), AD_BRNCH, AD_CMPNY);

					if(adBankAccount.getBaEnable() == EJBCommon.TRUE) {

						adBankAccount.addArReceipt(arReceipt);

					} else {

						throw new GlobalRecordDisabledException("Bank Account: " + details.getRctBaName());

					}

				} catch(FinderException ex) {

					throw new GlobalRecordInvalidForCurrentBranchException("Bank Account: " + details.getRctBaName());

				}

				//add customer
				LocalArCustomer arCustomer = null;

				if(details.getRctCstName() == null || details.getRctCstName().equals("")) {

					throw new GlobalRecordInvalidException();

				}

				try {

					arCustomer = arCustomerHome.findByCstName(details.getRctCstName(), AD_BRNCH, AD_CMPNY);

					if(arCustomer.getCstEnable() == EJBCommon.TRUE) {

						arCustomer.addArReceipt(arReceipt);

					} else {

						throw new GlobalRecordDisabledException("Customer: " + details.getRctCstName());

					}

				} catch(FinderException ex) {

					throw new GlobalRecordInvalidForCurrentBranchException("Customer: " + details.getRctCstName());

				}

				try {

					arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName("NONE", AD_CMPNY);
					if(arWithholdingTaxCode.getWtcEnable() == EJBCommon.TRUE) {

						arWithholdingTaxCode.addArReceipt(arReceipt);

					} else {

						throw new GlobalRecordDisabledException("Withholding Tax: NONE");

					}

				} catch(FinderException ex) {

					throw new GlobalNoRecordFoundException("Withholding Tax: NONE");

				}

				try {

					arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
					arTaxCode.addArReceipt(arReceipt);

				} catch(FinderException ex) {

					throw new GlobalNoRecordFoundException("Tax: " + TC_NM);

				}

				arReceiptBatch.addArReceipt(arReceipt);

				LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(details.getRctFcName(), AD_CMPNY);

				if(glFunctionalCurrency.getFcEnable() == EJBCommon.TRUE) {

					glFunctionalCurrency.addArReceipt(arReceipt);

				} else {

					throw new GlobalRecordDisabledException("Currency: " + details.getRctFcName());

				}

				// add new invoice line items and distribution record

				double TOTAL_LINE = 0d;
				double TOTAL_TAX = 0d;
				short ILI_LINE = 1;

				ArrayList iliList = details.getInvIliList();

				Iterator i = iliList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails) i.next();

					if(mIliDetails.getIliUnitPrice() < 0) {

						throw new GlobalAmountInvalidException("unit price for item " + mIliDetails.getIliIiName() + " of receipt " + arReceipt.getRctNumber());

					}

					if(mIliDetails.getIliQuantity() <= 0) {

						throw new GlobalAmountInvalidException("quantity for item " + mIliDetails.getIliIiName() + " of receipt " + arReceipt.getRctNumber());

					}

					try {

						LocalInvLocation invLocation = invLocationHome.findByLocName(mIliDetails.getIliLocName(), AD_CMPNY);

					} catch(FinderException ex) {

						throw new GlobalNoRecordFoundException("Location: " + mIliDetails.getIliLocName());

					}

					try {

						LocalInvItem invItem = invItemHome.findByIiName(mIliDetails.getIliIiName(), AD_CMPNY);

						if(invItem.getIiEnable() == EJBCommon.FALSE) {

							throw new GlobalRecordDisabledException("Item: " + mIliDetails.getIliIiName());

						}

					} catch(FinderException ex) {

						throw new GlobalNoRecordFoundException("Item: " + mIliDetails.getIliIiName());

					}

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(mIliDetails.getIliLocName(), mIliDetails.getIliIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(mIliDetails.getIliIiName() + "-" + mIliDetails.getIliLocName());

					}

					if(mIliDetails.getIliUomName().equals("")) {

						mIliDetails.setIliUomName(invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName());

					}

					mIliDetails.setIliLine(ILI_LINE);
					ILI_LINE++;

					mIliDetails.setIliAmount(mIliDetails.getIliUnitPrice() * mIliDetails.getIliQuantity());

					LocalArInvoiceLineItem arInvoiceLineItem = this.addArIliEntry(mIliDetails, arReceipt, invItemLocation, AD_CMPNY);

					// add cost of sales distribution and inventory

					double COST = 0d;

					try {

						LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
								arReceipt.getRctDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

						COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

					} catch (FinderException ex) {

						COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

					}

					double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
							arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try {

						adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

					} catch(FinderException ex) {

					}

					if (arInvoiceLineItem.getIliEnableAutoBuild() == 0 || arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Stock")) {

						if(adBranchItemLocation != null) {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(),
									"COGS", EJBCommon.TRUE, COST * QTY_SLD,
									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

							this.addArDrIliEntry(arReceipt.getArDrNextLine(),
									"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
									adBranchItemLocation.getBilCoaGlInventoryAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

						} else {

							this.addArDrIliEntry(arReceipt.getArDrNextLine(),
									"COGS", EJBCommon.TRUE, COST * QTY_SLD,
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

							this.addArDrIliEntry(arReceipt.getArDrNextLine(),
									"INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
									arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

						}

						// add quantity to item location committed quantity

						double convertedQuantity = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(), arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
						invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

					}

					// add inventory sale distributions

					if(adBranchItemLocation != null) {

						this.addArDrIliEntry(arReceipt.getArDrNextLine(),
								"REVENUE", EJBCommon.FALSE, arInvoiceLineItem.getIliAmount(),
								adBranchItemLocation.getBilCoaGlSalesAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

					} else {

						this.addArDrIliEntry(arReceipt.getArDrNextLine(),
								"REVENUE", EJBCommon.FALSE, arInvoiceLineItem.getIliAmount(),
								arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount(), arReceipt, AD_BRNCH, AD_CMPNY);

					}

					TOTAL_LINE += arInvoiceLineItem.getIliAmount();
					TOTAL_TAX += arInvoiceLineItem.getIliTaxAmount();

				}

				double SERVICE_CHARGE_AMOUNT = 0d;
				double DISCOUNT_AMOUNT = 0d;
				double GIFT_CHECK_AMOUNT = 0d;
				double SERVICE_CHARGE_TAX = 0d;
				double DISCOUNT_TAX = 0d;

				short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

				ArrayList drList = details.getRctDrList();

				Iterator drIter = drList.iterator();

				while(drIter.hasNext()) {

					ArModDistributionRecordDetails drDetails = (ArModDistributionRecordDetails)drIter.next();

					if(drDetails.getDrClass().equals("SERVICE CHARGE") && drDetails.getDrAmount() > 0) {

						LocalGlChartOfAccount glServiceChargeAccount = null;

						try {

							glServiceChargeAccount = glChartOfAccountHome.findByCoaAccountNumber(drDetails.getDrCoaAccountNumber(), AD_CMPNY);

						} catch(FinderException ex) {

							throw new GlobalNoRecordFoundException("Service Charge Account " + drDetails.getDrCoaAccountNumber());

						}

						Collection arDistributionRecords = null;

						try {

							arDistributionRecords = arDistributionRecordHome.findDrsByDrClassAndRctCode("SERVICE", arReceipt.getRctCode(), AD_CMPNY);

						} catch(FinderException ex) {

						}

						if (taxableServiceCharge && !arTaxCode.getTcType().equals("NONE") &&
								!arTaxCode.getTcType().equals("EXEMPT")) {

							SERVICE_CHARGE_TAX = this.getTaxAmount(arTaxCode, drDetails.getDrAmount(), precisionUnit);

							if(arTaxCode.getTcType().equals("INCLUSIVE")) {

								SERVICE_CHARGE_AMOUNT = drDetails.getDrAmount() - SERVICE_CHARGE_TAX;

							} else {

								SERVICE_CHARGE_AMOUNT = drDetails.getDrAmount();

							}

						} else {

							SERVICE_CHARGE_AMOUNT = drDetails.getDrAmount();

						}

						if(arDistributionRecords.isEmpty()) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "SERVICE", EJBCommon.FALSE,
									SERVICE_CHARGE_AMOUNT, EJBCommon.FALSE, glServiceChargeAccount.getCoaCode(),
									arReceipt, null, AD_BRNCH, AD_CMPNY);

						} else {

							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) arDistributionRecords.iterator().next();

							arDistributionRecord.setDrAmount(arDistributionRecord.getDrAmount() + SERVICE_CHARGE_AMOUNT);

							SERVICE_CHARGE_AMOUNT = arDistributionRecord.getDrAmount();

						}

					} else if(drDetails.getDrClass().equals("DISCOUNT") && drDetails.getDrAmount() > 0) {

						LocalGlChartOfAccount glDiscountAccount = null;

						try {

							glDiscountAccount = glChartOfAccountHome.findByCoaAccountNumber(drDetails.getDrCoaAccountNumber(), AD_CMPNY);

						} catch(FinderException ex) {

							throw new GlobalNoRecordFoundException("Discount Account " + drDetails.getDrCoaAccountNumber());

						}

						Collection arDistributionRecords = null;

						try {

							arDistributionRecords = arDistributionRecordHome.findDrsByDrClassAndRctCode("DISCOUNT", arReceipt.getRctCode(), AD_CMPNY);

						} catch(FinderException ex) {

						}

						if (taxableDiscount && !arTaxCode.getTcType().equals("NONE") &&
								!arTaxCode.getTcType().equals("EXEMPT")) {

							DISCOUNT_TAX = this.getTaxAmount(arTaxCode, drDetails.getDrAmount(), precisionUnit);

							if(arTaxCode.getTcType().equals("INCLUSIVE")) {

								DISCOUNT_AMOUNT = drDetails.getDrAmount() - DISCOUNT_TAX;

							} else {

								DISCOUNT_AMOUNT = drDetails.getDrAmount();

							}

						} else {

							DISCOUNT_AMOUNT = drDetails.getDrAmount();

						}

						if(arDistributionRecords.isEmpty()) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
									DISCOUNT_AMOUNT, EJBCommon.FALSE, glDiscountAccount.getCoaCode(),
									arReceipt, null, AD_BRNCH, AD_CMPNY);

						} else {

							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) arDistributionRecords.iterator().next();

							arDistributionRecord.setDrAmount(arDistributionRecord.getDrAmount() + DISCOUNT_AMOUNT);

							DISCOUNT_AMOUNT = arDistributionRecord.getDrAmount();

						}

					} else if(drDetails.getDrClass().equals("GIFT CHECK") && drDetails.getDrAmount() > 0) {

						LocalGlChartOfAccount glGiftCheckAccount = null;

						try {

							glGiftCheckAccount = glChartOfAccountHome.findByCoaAccountNumber(drDetails.getDrCoaAccountNumber(), AD_CMPNY);

						} catch(FinderException ex) {

							throw new GlobalNoRecordFoundException("Gift Check Account " + drDetails.getDrCoaAccountNumber());

						}

						Collection arDistributionRecords = null;

						try {

							arDistributionRecords = arDistributionRecordHome.findDrsByDrClassAndRctCode("GIFT CHECK", arReceipt.getRctCode(), AD_CMPNY);

						} catch(FinderException ex) {

						}

						if(arDistributionRecords.isEmpty()) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "GIFT CHECK", EJBCommon.TRUE,
									drDetails.getDrAmount(), EJBCommon.FALSE, glGiftCheckAccount.getCoaCode(),
									arReceipt, null, AD_BRNCH, AD_CMPNY);

							GIFT_CHECK_AMOUNT = drDetails.getDrAmount();

						} else {

							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) arDistributionRecords.iterator().next();

							arDistributionRecord.setDrAmount(arDistributionRecord.getDrAmount() + drDetails.getDrAmount());

							GIFT_CHECK_AMOUNT = arDistributionRecord.getDrAmount();

						}

					}

				}

				//add tax distribution if necessary

				TOTAL_TAX = TOTAL_TAX + SERVICE_CHARGE_TAX - DISCOUNT_TAX;

	      	    if (!arTaxCode.getTcType().equals("NONE") &&
	        	    !arTaxCode.getTcType().equals("EXEMPT")) {

		      	    this.addArDrEntry(arReceipt.getArDrNextLine(),
		      	        "TAX", EJBCommon.FALSE, TOTAL_TAX, EJBCommon.FALSE,
		      	         arTaxCode.getGlChartOfAccount().getCoaCode(),arReceipt, null, AD_BRNCH, AD_CMPNY);

		        }

	      	    // add wtax distribution if necessary

	      	    double W_TAX_AMOUNT = 0d;

	      	    if (arWithholdingTaxCode.getWtcRate() != 0) {

	      	    	W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (arWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

	      	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "W-TAX",
	      	    	    EJBCommon.TRUE, W_TAX_AMOUNT, EJBCommon.FALSE,
	      	    	    arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(), arReceipt, null, AD_BRNCH, AD_CMPNY);

	      	    }

	      	    double RCPT_AMNT = 0d;

	      	    LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	      	    if(adCompany.getCmpShortName().equals("obi") && arReceipt.getAdBankAccount().getBaName().equals("POS MC")) {

	      	    	RCPT_AMNT = details.getRctAmount() + GIFT_CHECK_AMOUNT + SERVICE_CHARGE_AMOUNT + SERVICE_CHARGE_TAX;

	      	    } else {

	      	    	RCPT_AMNT = details.getRctAmount() + GIFT_CHECK_AMOUNT;

	      	    }

	      	    if(EJBCommon.roundIt(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT + SERVICE_CHARGE_AMOUNT - DISCOUNT_AMOUNT, this.getGlFcPrecisionUnit(AD_CMPNY)) != EJBCommon.roundIt(RCPT_AMNT, this.getGlFcPrecisionUnit(AD_CMPNY))) {

	      	    	throw new GlobalJournalNotBalanceException(arReceipt.getRctNumber());

	      	    }

	      	    if(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT + SERVICE_CHARGE_AMOUNT - DISCOUNT_AMOUNT - GIFT_CHECK_AMOUNT > 0) {

	      	    	// add cash distribution

	      	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH",
	      	    			EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT + SERVICE_CHARGE_AMOUNT - DISCOUNT_AMOUNT - GIFT_CHECK_AMOUNT,
							EJBCommon.FALSE, arReceipt.getAdBankAccount().getBaCoaGlCashAccount(),
							arReceipt, null, AD_BRNCH, AD_CMPNY);

	      	    }

				// set receipt amount

				arReceipt.setRctAmount(EJBCommon.roundIt(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT + SERVICE_CHARGE_AMOUNT - DISCOUNT_AMOUNT, this.getGlFcPrecisionUnit(AD_CMPNY)));

			}

		} catch (GlobalInvItemLocationNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoRecordFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordInvalidForCurrentBranchException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordDisabledException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalAmountInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}


	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/

	public void importRctIl(ArrayList miscRctList, boolean isSummarized, String RB_NM, String TC_NM, boolean taxableServiceCharge,
			boolean taxableDiscount, String CRTD_BY,
			Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalDocumentNumberNotUniqueException,
			GlobalNoRecordFoundException,
			GlobalRecordInvalidForCurrentBranchException,
			GlobalRecordDisabledException,
			GlobalRecordInvalidException,
			GlobalBranchAccountNumberInvalidException,
			GlobalJournalNotBalanceException,
			GlobalAmountInvalidException,
			GlobalRecordAlreadyExistException {

		Debug.print("ArReceiptImportControllerBean importRctIl");

		LocalArReceiptBatchHome arReceiptBatchHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalArStandardMemoLineHome arStandardMemoLineHome = null;

		LocalArReceipt arReceipt = null;
		LocalArWithholdingTaxCode arWithholdingTaxCode = null;
		LocalArTaxCode arTaxCode = null;

		try {

			arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Date CURR_DATE = EJBCommon.getGcCurrentDateWoTime().getTime();

			LocalArReceiptBatch arReceiptBatch = arReceiptBatchHome.create(RB_NM, null, "OPEN", "MISC", CURR_DATE, CRTD_BY, AD_BRNCH, AD_CMPNY);

			//check if POS data already exists

			ArModReceiptDetails rctDetails = (ArModReceiptDetails)miscRctList.get(0);

			Collection arReceipts = arReceiptHome.findByRctDateAndBaName(rctDetails.getRctDate(), "POS%", AD_CMPNY);

			if(!arReceipts.isEmpty()) {

				throw new GlobalRecordAlreadyExistException(EJBCommon.convertSQLDateToString(rctDetails.getRctDate()));

			}

			Iterator mrIter = miscRctList.iterator();

			while(mrIter.hasNext()) {

				ArModReceiptDetails details = (ArModReceiptDetails)mrIter.next();

				if(details.getRctAmount() < 0) {

					throw new GlobalAmountInvalidException("total amount for receipt " + details.getRctNumber());

				}

				String RCPT_NMBR = null;

				//validate if receipt number is unique receipt number is automatic then set next sequence

		    	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	        	if (isSummarized) {

	        		try {

	 					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT", AD_CMPNY);

	 				} catch (FinderException ex) {

	 				}

	 				try {

	 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	 				} catch (FinderException ex) {

	 				}

			        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A') {

			            while (true) {

			            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

			            		try {

				            		arReceiptHome.findByRctNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

				            	} catch (FinderException ex) {

				            		RCPT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
				            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
						            break;

				            	}

			            	} else {

			            		try {

				            		arReceiptHome.findByRctNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

				            	} catch (FinderException ex) {

				            		RCPT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
				            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
						            break;

				            	}

			            	}

			            }

			        }

			    } else {

			    	LocalArReceipt arExistingReceipt = null;

			    	try {

		    		    arExistingReceipt = arReceiptHome.findByRctNumberAndBrCode(details.getRctNumber(), AD_BRNCH, AD_CMPNY);

		        	} catch (FinderException ex) {
		        	}

		        	if (arExistingReceipt != null &&
		                !arExistingReceipt.getRctCode().equals(details.getRctCode())) {

		            	throw new GlobalDocumentNumberNotUniqueException(details.getRctNumber());

		            }

		        	RCPT_NMBR = details.getRctNumber();

			    }

				//create misc receipt

				arReceipt = arReceiptHome.create("MISC", null, details.getRctDate(), RCPT_NMBR, null, null, null,
						null,null,null,null,null,
						details.getRctAmount(), 0d, 0d, 0d, 0d,  0d, 0d,
						null, details.getRctConversionRate(),
						null, null, EJBCommon.FALSE, 0d, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
					    null, null, null, null, null,
						CRTD_BY, CURR_DATE, CRTD_BY, CURR_DATE, null, null, null, null,
						EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE, null, null,
						EJBCommon.FALSE, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);

				LocalAdBankAccount adBankAccount = null;

				try {

					adBankAccount = adBankAccountHome.findByBaNameAndBrCode(details.getRctBaName(), AD_BRNCH, AD_CMPNY);

					if(adBankAccount.getBaEnable() == EJBCommon.TRUE) {

						adBankAccount.addArReceipt(arReceipt);

					} else {

						throw new GlobalRecordDisabledException("Bank Account: " + details.getRctBaName());

					}

				} catch(FinderException ex) {

					throw new GlobalRecordInvalidForCurrentBranchException("Bank Account: " + details.getRctBaName());

				}

				//add customer
				LocalArCustomer arCustomer = null;

				if(details.getRctCstName() == null || details.getRctCstName().equals("")) {

					throw new GlobalRecordInvalidException();

				}

				try {

					arCustomer = arCustomerHome.findByCstName(details.getRctCstName(), AD_BRNCH, AD_CMPNY);

					if(arCustomer.getCstEnable() == EJBCommon.TRUE) {

						arCustomer.addArReceipt(arReceipt);

					} else {

						throw new GlobalRecordDisabledException("Customer: " + details.getRctCstName());

					}

				} catch(FinderException ex) {

					throw new GlobalRecordInvalidForCurrentBranchException("Customer: " + details.getRctCstName());

				}

				try {

					arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName("NONE", AD_CMPNY);
					if(arWithholdingTaxCode.getWtcEnable() == EJBCommon.TRUE) {

						arWithholdingTaxCode.addArReceipt(arReceipt);

					} else {

						throw new GlobalRecordDisabledException("Withholding Tax: NONE");

					}

				} catch(FinderException ex) {

					throw new GlobalNoRecordFoundException("Withholding Tax: NONE");

				}

				try {

					arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
					arTaxCode.addArReceipt(arReceipt);

				} catch(FinderException ex) {

					throw new GlobalNoRecordFoundException("Tax: " + TC_NM);

				}

				LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(details.getRctFcName(), AD_CMPNY);

				if(glFunctionalCurrency.getFcEnable() == EJBCommon.TRUE) {

					glFunctionalCurrency.addArReceipt(arReceipt);

				} else {

					throw new GlobalRecordDisabledException("Currency: " + details.getRctFcName());

				}

				arReceiptBatch.addArReceipt(arReceipt);


				//add new invoice lines

				double TOTAL_TAX = 0d;
				double TOTAL_LINE = 0d;

				ArrayList ilList = details.getInvIlList();

				Iterator i = ilList.iterator();

				while (i.hasNext()) {

					ArModInvoiceLineDetails mIlDetails = (ArModInvoiceLineDetails) i.next();

					if(mIlDetails.getIlUnitPrice() < 0) {

						throw new GlobalAmountInvalidException("unit price for memo line " + mIlDetails.getIlSmlName() + " of receipt " + arReceipt.getRctNumber());

					}

					if(mIlDetails.getIlQuantity() <= 0) {

						throw new GlobalAmountInvalidException("quantity for memo line " + mIlDetails.getIlSmlName() + " of receipt " + arReceipt.getRctNumber());

					}

					LocalArStandardMemoLine arStandardMemoLine = null;

					try {

						arStandardMemoLine = arStandardMemoLineHome.findBySmlNameAndBrCode(mIlDetails.getIlSmlName(), AD_BRNCH, AD_CMPNY);

						if(arStandardMemoLine.getSmlEnable() == EJBCommon.FALSE) {

							throw new GlobalRecordDisabledException("Standard Memo Line: " + mIlDetails.getIlSmlName());

						}

					} catch(FinderException ex) {

						throw new GlobalRecordInvalidForCurrentBranchException("Standard Memo Line: " + mIlDetails.getIlSmlName());

					}

					mIlDetails.setIlTax(arStandardMemoLine.getSmlTax());
					mIlDetails.setIlAmount(mIlDetails.getIlUnitPrice() * mIlDetails.getIlQuantity());

					LocalArInvoiceLine arInvoiceLine = this.addArIlEntry(mIlDetails, arReceipt, AD_CMPNY);

					// add revenue/credit distributions

					this.addArDrEntry(arReceipt.getArDrNextLine(),
							"REVENUE", EJBCommon.FALSE, arInvoiceLine.getIlAmount(), EJBCommon.FALSE,
							this.getArGlCoaRevenueAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), arReceipt, null, AD_BRNCH, AD_CMPNY);

					TOTAL_TAX += arInvoiceLine.getIlTaxAmount();
					TOTAL_LINE += arInvoiceLine.getIlAmount();

				}

				double SERVICE_CHARGE_AMOUNT = 0d;
				double DISCOUNT_AMOUNT = 0d;
				double GIFT_CHECK_AMOUNT = 0d;
				double DISCOUNT_TAX = 0d;
				double SERVICE_CHARGE_TAX = 0d;

				short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

				ArrayList drList = details.getRctDrList();

				Iterator drIter = drList.iterator();

				while(drIter.hasNext()) {

					ArModDistributionRecordDetails drDetails = (ArModDistributionRecordDetails)drIter.next();

					if(drDetails.getDrClass().equals("SERVICE CHARGE") && drDetails.getDrAmount() > 0) {

						LocalGlChartOfAccount glServiceChargeAccount = null;

						try {

							glServiceChargeAccount = glChartOfAccountHome.findByCoaAccountNumber(drDetails.getDrCoaAccountNumber(), AD_CMPNY);

						} catch(FinderException ex) {

							throw new GlobalNoRecordFoundException("Service Charge Account " + drDetails.getDrCoaAccountNumber());

						}

						Collection arDistributionRecords = null;

						try {

							arDistributionRecords = arDistributionRecordHome.findDrsByDrClassAndRctCode("SERVICE", arReceipt.getRctCode(), AD_CMPNY);

						} catch(FinderException ex) {

						}

						if (taxableServiceCharge && !arTaxCode.getTcType().equals("NONE") &&
								!arTaxCode.getTcType().equals("EXEMPT")) {

							SERVICE_CHARGE_TAX = this.getTaxAmount(arTaxCode, drDetails.getDrAmount(), precisionUnit);

							if(arTaxCode.getTcType().equals("INCLUSIVE")) {

								SERVICE_CHARGE_AMOUNT = drDetails.getDrAmount() - SERVICE_CHARGE_TAX;

							} else {

								SERVICE_CHARGE_AMOUNT = drDetails.getDrAmount();

							}

						} else {

							SERVICE_CHARGE_AMOUNT = drDetails.getDrAmount();

						}

						if(arDistributionRecords.isEmpty()) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "SERVICE", EJBCommon.FALSE,
									SERVICE_CHARGE_AMOUNT, EJBCommon.FALSE, glServiceChargeAccount.getCoaCode(),
									arReceipt, null, AD_BRNCH, AD_CMPNY);

						} else {

							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) arDistributionRecords.iterator().next();

							arDistributionRecord.setDrAmount(arDistributionRecord.getDrAmount() + SERVICE_CHARGE_AMOUNT);

							SERVICE_CHARGE_AMOUNT = arDistributionRecord.getDrAmount();

						}

					} else if(drDetails.getDrClass().equals("DISCOUNT") && drDetails.getDrAmount() > 0) {

						LocalGlChartOfAccount glDiscountAccount = null;

						try {

							glDiscountAccount = glChartOfAccountHome.findByCoaAccountNumber(drDetails.getDrCoaAccountNumber(), AD_CMPNY);

						} catch(FinderException ex) {

							throw new GlobalNoRecordFoundException("Discount Account " + drDetails.getDrCoaAccountNumber());

						}

						Collection arDistributionRecords = null;

						try {

							arDistributionRecords = arDistributionRecordHome.findDrsByDrClassAndRctCode("DISCOUNT", arReceipt.getRctCode(), AD_CMPNY);

						} catch(FinderException ex) {

						}

						if (taxableDiscount && !arTaxCode.getTcType().equals("NONE") &&
								!arTaxCode.getTcType().equals("EXEMPT")) {

							DISCOUNT_TAX = this.getTaxAmount(arTaxCode, drDetails.getDrAmount(), precisionUnit);

							if(arTaxCode.getTcType().equals("INCLUSIVE")) {

								DISCOUNT_AMOUNT = drDetails.getDrAmount() - DISCOUNT_TAX;

							} else {

								DISCOUNT_AMOUNT = drDetails.getDrAmount();

							}

						} else {

							DISCOUNT_AMOUNT = drDetails.getDrAmount();

						}

						if(arDistributionRecords.isEmpty()) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "DISCOUNT", EJBCommon.TRUE,
									DISCOUNT_AMOUNT, EJBCommon.FALSE, glDiscountAccount.getCoaCode(),
									arReceipt, null, AD_BRNCH, AD_CMPNY);

						} else {

							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) arDistributionRecords.iterator().next();

							arDistributionRecord.setDrAmount(arDistributionRecord.getDrAmount() + DISCOUNT_AMOUNT);

							DISCOUNT_AMOUNT = arDistributionRecord.getDrAmount();

						}

					} else if(drDetails.getDrClass().equals("GIFT CHECK") && drDetails.getDrAmount() > 0) {

						LocalGlChartOfAccount glGiftCheckAccount = null;

						try {

							glGiftCheckAccount = glChartOfAccountHome.findByCoaAccountNumber(drDetails.getDrCoaAccountNumber(), AD_CMPNY);

						} catch(FinderException ex) {

							throw new GlobalNoRecordFoundException("Gift Check Account " + drDetails.getDrCoaAccountNumber());

						}

						Collection arDistributionRecords = null;

						try {

							arDistributionRecords = arDistributionRecordHome.findDrsByDrClassAndRctCode("GIFT CHECK", arReceipt.getRctCode(), AD_CMPNY);

						} catch(FinderException ex) {

						}

						if(arDistributionRecords.isEmpty()) {

							this.addArDrEntry(arReceipt.getArDrNextLine(), "GIFT CHECK", EJBCommon.TRUE,
									drDetails.getDrAmount(), EJBCommon.FALSE, glGiftCheckAccount.getCoaCode(),
									arReceipt, null, AD_BRNCH, AD_CMPNY);

							GIFT_CHECK_AMOUNT = drDetails.getDrAmount();

						} else {

							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) arDistributionRecords.iterator().next();

							arDistributionRecord.setDrAmount(arDistributionRecord.getDrAmount() + drDetails.getDrAmount());

							GIFT_CHECK_AMOUNT = arDistributionRecord.getDrAmount();

						}

					}

				}

				// add tax distribution if necessary

				TOTAL_TAX = TOTAL_TAX + SERVICE_CHARGE_TAX - DISCOUNT_TAX;

	      	    if (!arTaxCode.getTcType().equals("NONE") &&
	        	    !arTaxCode.getTcType().equals("EXEMPT")) {

		      	    this.addArDrEntry(arReceipt.getArDrNextLine(),
		      	        "TAX", EJBCommon.FALSE, TOTAL_TAX, EJBCommon.FALSE,
		      	         arTaxCode.getGlChartOfAccount().getCoaCode(),arReceipt, null, AD_BRNCH, AD_CMPNY);

		        }

	      	    // add wtax distribution if necessary

	      	    double W_TAX_AMOUNT = 0d;

	      	    if (arWithholdingTaxCode.getWtcRate() != 0) {

	      	    	W_TAX_AMOUNT = EJBCommon.roundIt(TOTAL_LINE * (arWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

	      	    	this.addArDrEntry(arReceipt.getArDrNextLine(), "W-TAX",
	      	    	    EJBCommon.TRUE, W_TAX_AMOUNT, EJBCommon.FALSE,
	      	    	    arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(), arReceipt, null, AD_BRNCH, AD_CMPNY);

	      	    }

	      	    double RCPT_AMNT = details.getRctAmount() + GIFT_CHECK_AMOUNT ;

	      	    if(RCPT_AMNT != EJBCommon.roundIt(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT + SERVICE_CHARGE_AMOUNT - DISCOUNT_AMOUNT, this.getGlFcPrecisionUnit(AD_CMPNY))) {

	      	    	throw new GlobalJournalNotBalanceException(arReceipt.getRctNumber());

	      	    }

				// add cash distribution

				this.addArDrEntry(arReceipt.getArDrNextLine(), "CASH",
						EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT + SERVICE_CHARGE_AMOUNT - DISCOUNT_AMOUNT - GIFT_CHECK_AMOUNT,
						EJBCommon.FALSE, arReceipt.getAdBankAccount().getBaCoaGlCashAccount(),
						arReceipt, null,AD_BRNCH, AD_CMPNY);

				// set receipt amount

				arReceipt.setRctAmount(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT + SERVICE_CHARGE_AMOUNT - DISCOUNT_AMOUNT);

			}

		} catch (GlobalNoRecordFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordInvalidForCurrentBranchException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordDisabledException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalAmountInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void rollback() {

		getSessionContext().setRollbackOnly();

	}

	// private methods

	private LocalArAppliedInvoice addArAiEntry(ArModAppliedInvoiceDetails mdetails, LocalArReceipt arReceipt, LocalArReceiptBatch arReceiptBatch, Integer AD_CMPNY)
	throws GlobalTransactionAlreadyLockedException,
	ArINVOverapplicationNotAllowedException {

		Debug.print("ArReceiptImportControllerBean addArAiEntry");

		LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			// get functional currency name

			String FC_NM = adCompany.getGlFunctionalCurrency().getFcName();

			//validate overapplication

        	LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
        	    arInvoicePaymentScheduleHome.findByPrimaryKey(mdetails.getAiIpsCode());

        	if (EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid(), this.getGlFcPrecisionUnit(AD_CMPNY)) <
        	    EJBCommon.roundIt(mdetails.getAiApplyAmount() + mdetails.getAiCreditableWTax() + mdetails.getAiDiscountAmount(), this.getGlFcPrecisionUnit(AD_CMPNY))) {

        		throw new ArINVOverapplicationNotAllowedException(arInvoicePaymentSchedule.getArInvoice().getInvNumber());

        	}

        	Collection arAppliedInvoices = null;
        	LocalArAppliedInvoice arAppliedInvoice = null;

        	arAppliedInvoices = arInvoicePaymentSchedule.getArAppliedInvoices();

        	Iterator appInvcIter = arAppliedInvoices.iterator();

        	double TOTAL_AMOUNT = 0d;

        	while(appInvcIter.hasNext()) {

        		arAppliedInvoice = (LocalArAppliedInvoice)appInvcIter.next();

        		if(arAppliedInvoice.getArReceipt().getRctVoidPosted() == EJBCommon.FALSE) {

        			TOTAL_AMOUNT += arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiDiscountAmount();

        		}

        	}

        	if(TOTAL_AMOUNT + mdetails.getAiApplyAmount() + mdetails.getAiDiscountAmount() > arInvoicePaymentSchedule.getIpsAmountDue()) {

        		throw new ArINVOverapplicationNotAllowedException(arInvoicePaymentSchedule.getArInvoice().getInvNumber());

        	}

        	// validate if ips already locked

			if (arInvoicePaymentSchedule.getIpsLock() == EJBCommon.TRUE) {

				Collection arReceipts = arReceiptBatch.getArReceipts();

				Iterator i = arReceipts.iterator();

				boolean found = false;
				while(i.hasNext()) {

					LocalArReceipt existingArReceipt = (LocalArReceipt)i.next();

					arAppliedInvoices = existingArReceipt.getArAppliedInvoices();

					Iterator j = arAppliedInvoices.iterator();

					while(j.hasNext()) {

						arAppliedInvoice = (LocalArAppliedInvoice)j.next();

						if(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvNumber().equals(mdetails.getAiIpsInvNumber()))
							found = true;

					}

				}

				if(found == false) {

					throw new GlobalTransactionAlreadyLockedException(arInvoicePaymentSchedule.getArInvoice().getInvNumber());

				}

			}

			double AI_FRX_GN_LSS = 0d;

			if (!FC_NM.equals(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName()) ||
					!FC_NM.equals(arReceipt.getGlFunctionalCurrency().getFcName())) {

				double AI_ALLCTD_PYMNT_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
						arReceipt.getGlFunctionalCurrency().getFcName(),
						arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						mdetails.getAiAllocatedPaymentAmount(), AD_CMPNY);

				double AI_APPLY_AMNT = this.convertForeignToFunctionalCurrency(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcCode(),
						arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName(),
						arInvoicePaymentSchedule.getArInvoice().getInvConversionDate(),
						arInvoicePaymentSchedule.getArInvoice().getInvConversionRate(),
						mdetails.getAiApplyAmount(), AD_CMPNY);

				double AI_CRDTBL_W_TX = this.convertForeignToFunctionalCurrency(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcCode(),
						arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName(),
						arInvoicePaymentSchedule.getArInvoice().getInvConversionDate(),
						arInvoicePaymentSchedule.getArInvoice().getInvConversionRate(),
						mdetails.getAiCreditableWTax(), AD_CMPNY);

				double AI_DSCNT_AMNT = this.convertForeignToFunctionalCurrency(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcCode(),
						arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName(),
						arInvoicePaymentSchedule.getArInvoice().getInvConversionDate(),
						arInvoicePaymentSchedule.getArInvoice().getInvConversionRate(),
						mdetails.getAiDiscountAmount(), AD_CMPNY);

				AI_FRX_GN_LSS = EJBCommon.roundIt((AI_ALLCTD_PYMNT_AMNT + AI_CRDTBL_W_TX + AI_DSCNT_AMNT) -
						(AI_APPLY_AMNT + AI_CRDTBL_W_TX + AI_DSCNT_AMNT), this.getGlFcPrecisionUnit(AD_CMPNY));

			}

			// create applied invoice

			arAppliedInvoice = arAppliedInvoiceHome.create(mdetails.getAiApplyAmount(),mdetails.getAiPenaltyApplyAmount(), mdetails.getAiCreditableWTax(),
					mdetails.getAiDiscountAmount(),mdetails.getAiRebate(), 0d, mdetails.getAiAllocatedPaymentAmount(), AI_FRX_GN_LSS,
					EJBCommon.FALSE,
					AD_CMPNY);

			arReceipt.addArAppliedInvoice(arAppliedInvoice);
			arInvoicePaymentSchedule.addArAppliedInvoice(arAppliedInvoice);

			// lock invoice

			arInvoicePaymentSchedule.setIpsLock(EJBCommon.TRUE);

			return arAppliedInvoice;

		} catch (GlobalTransactionAlreadyLockedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (ArINVOverapplicationNotAllowedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addArDrEntry(short DR_LN, String DR_CLSS,
			byte DR_DBT, double DR_AMNT, byte DR_RVRSD, Integer COA_CODE, LocalArReceipt arReceipt,
			LocalArAppliedInvoice arAppliedInvoice, Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalBranchAccountNumberInvalidException {

		Debug.print("ArReceiptImportControllerBean addArDrEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = null;

			try {

				glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);;

			} catch(FinderException ex) {

				throw new GlobalBranchAccountNumberInvalidException();

			}

			// create distribution record

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, DR_RVRSD, AD_CMPNY);

			arReceipt.addArDistributionRecord(arDistributionRecord);
			glChartOfAccount.addArDistributionRecord(arDistributionRecord);

			// to be used by gl journal interface for cross currency receipts
			if (arAppliedInvoice != null) {

				arAppliedInvoice.addArDistributionRecord(arDistributionRecord);

			}

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}


	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("ArReceiptImportControllerBean convertForeignToFunctionalCurrency");


		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}


		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

			AMOUNT = AMOUNT / CONVERSION_RATE;

		}
		
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

	private LocalArInvoiceLineItem addArIliEntry(ArModInvoiceLineItemDetails mdetails, LocalArReceipt arReceipt, LocalInvItemLocation invItemLocation, Integer AD_CMPNY)
		throws GlobalRecordDisabledException {

		Debug.print("ArReceiptImportControllerBean addArIliEntry");

		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

		// Initialize EJB Home

		try {

			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

			double ILI_AMNT = 0d;
			double ILI_TAX_AMNT = 0d;

			// calculate net amount

			LocalArTaxCode arTaxCode = arReceipt.getArTaxCode();

			if (arTaxCode.getTcType().equals("INCLUSIVE")) {

				ILI_AMNT = EJBCommon.roundIt(mdetails.getIliAmount() / (1 + (arTaxCode.getTcRate() / 100)), precisionUnit);

			} else {

				// tax exclusive, none, zero rated or exempt

				ILI_AMNT = mdetails.getIliAmount();

			}

			// calculate tax

			if (!arTaxCode.getTcType().equals("NONE") &&
					!arTaxCode.getTcType().equals("EXEMPT")) {


				if (arTaxCode.getTcType().equals("INCLUSIVE")) {

					ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getIliAmount() - ILI_AMNT, precisionUnit);

				} else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

					ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getIliAmount() * arTaxCode.getTcRate() / 100, precisionUnit);

				} else {

					// tax none zero-rated or exempt

				}

			}

			LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
					mdetails.getIliLine(), mdetails.getIliQuantity(), mdetails.getIliUnitPrice(),
					ILI_AMNT, ILI_TAX_AMNT, EJBCommon.FALSE, mdetails.getIliDiscount1(),
					mdetails.getIliDiscount2(),mdetails.getIliDiscount3(), mdetails.getIliDiscount4(),
					mdetails.getIliTotalDiscount(), EJBCommon.TRUE, AD_CMPNY);

			arReceipt.addArInvoiceLineItem(arInvoiceLineItem);

			invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);

			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getIliUomName(), AD_CMPNY);

			if(invUnitOfMeasure.getUomEnable() == EJBCommon.FALSE) {

				throw new GlobalRecordDisabledException("Unit of measure: " + invUnitOfMeasure.getUomName());

			}

			invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);

			return arInvoiceLineItem;

		} catch (GlobalRecordDisabledException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addArDrIliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArReceiptImportControllerBean addArDrIliEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

			// create distribution record

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			arReceipt.addArDistributionRecord(arDistributionRecord);
			glChartOfAccount.addArDistributionRecord(arDistributionRecord);

		} catch (FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private LocalArInvoiceLine addArIlEntry(ArModInvoiceLineDetails mdetails, LocalArReceipt arReceipt, Integer AD_CMPNY) {

		Debug.print("ArReceiptImportControllerBean addArIlEntry");

		LocalArInvoiceLineHome arInvoiceLineHome = null;
		LocalArStandardMemoLineHome arStandardMemoLineHome = null;


		// Initialize EJB Home

		try {

			arInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
			arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

			double IL_AMNT = 0d;
			double IL_TAX_AMNT = 0d;

			if (mdetails.getIlTax() == EJBCommon.TRUE) {

				// calculate net amount

				LocalArTaxCode arTaxCode = arReceipt.getArTaxCode();

				if (arTaxCode.getTcType().equals("INCLUSIVE")) {

					IL_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() / (1 + (arTaxCode.getTcRate() / 100)), precisionUnit);

				} else {

					// tax exclusive, none, zero rated or exempt

					IL_AMNT = mdetails.getIlAmount();

				}

				// calculate tax

				if (!arTaxCode.getTcType().equals("NONE") &&
						!arTaxCode.getTcType().equals("EXEMPT")) {


					if (arTaxCode.getTcType().equals("INCLUSIVE")) {

						IL_TAX_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() - IL_AMNT, precisionUnit);

					} else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

						IL_TAX_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() * arTaxCode.getTcRate() / 100, precisionUnit);

					} else {

						// tax none zero-rated or exempt

					}

				}

			} else {

				IL_AMNT = mdetails.getIlAmount();

			}

			LocalArInvoiceLine arInvoiceLine = arInvoiceLineHome.create(
					mdetails.getIlDescription(), mdetails.getIlQuantity(),
					mdetails.getIlUnitPrice(), IL_AMNT,
					IL_TAX_AMNT, mdetails.getIlTax(), AD_CMPNY);

			arReceipt.addArInvoiceLine(arInvoiceLine);

			LocalArStandardMemoLine arStandardMemoLine = arStandardMemoLineHome.findBySmlName(mdetails.getIlSmlName(), AD_CMPNY);
			arStandardMemoLine.addArInvoiceLine(arInvoiceLine);

			return arInvoiceLine;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private Integer getArGlCoaRevenueAccount(LocalArInvoiceLine arInvoiceLine, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ArReceiptImportControllerBean getArGlCoaRevenueAccount");


		LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;

		// Initialize EJB Home

		try {

			arAutoAccountingSegmentHome = (LocalArAutoAccountingSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adBranchStandardMemoLineHome = (LocalAdBranchStandardMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		// generate revenue account

		try {

			String GL_COA_ACCNT = "";

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGenField genField = adCompany.getGenField();

			String FL_SGMNT_SPRTR = String.valueOf(genField.getFlSegmentSeparator());

			LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;

			try {

				adBranchStandardMemoLine = adBranchStandardMemoLineHome.findBSMLBySMLCodeAndBrCode(arInvoiceLine.getArStandardMemoLine().getSmlCode(), AD_BRNCH, AD_CMPNY);

			} catch(FinderException ex) {

			}


			Collection arAutoAccountingSegments = arAutoAccountingSegmentHome.findByAaAccountType("REVENUE", AD_CMPNY);

			Iterator i = arAutoAccountingSegments.iterator();

			while (i.hasNext()) {

				LocalArAutoAccountingSegment arAutoAccountingSegment =
					(LocalArAutoAccountingSegment) i.next();

				LocalGlChartOfAccount glChartOfAccount = null;

				if (arAutoAccountingSegment.getAasClassType().equals("AR CUSTOMER")) {

					glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
							arInvoiceLine.getArReceipt().getArCustomer().getCstGlCoaRevenueAccount());

					StringTokenizer st = new StringTokenizer(
							glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

					int ctr = 0;
					while (st.hasMoreTokens()) {

						++ctr;

						if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

							GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR +
							st.nextToken();

							break;

						} else {

							st.nextToken();

						}
					}


				} else if (arAutoAccountingSegment.getAasClassType().equals("AR STANDARD MEMO LINE")) {

					if(adBranchStandardMemoLine != null) {

						try {

							glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlAccount());

						} catch (FinderException ex) {

						}

					} else {

						glChartOfAccount = arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount();

					}

					StringTokenizer st = new StringTokenizer(
							glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

					int ctr = 0;
					while (st.hasMoreTokens()) {

						++ctr;

						if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

							GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR +
							st.nextToken();

							break;

						} else {

							st.nextToken();

						}
					}

				}
			}

			GL_COA_ACCNT = GL_COA_ACCNT.substring(1, GL_COA_ACCNT.length());

			try {

				LocalGlChartOfAccount glGeneratedChartOfAccount =
					glChartOfAccountHome.findByCoaAccountNumber(GL_COA_ACCNT, AD_CMPNY);

				return glGeneratedChartOfAccount.getCoaCode();

			} catch (FinderException ex) {

				if(adBranchStandardMemoLine != null) {

					LocalGlChartOfAccount glChartOfAccount = null;

					try {

						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlAccount());;

					} catch(FinderException e) {

					}

					return glChartOfAccount.getCoaCode();

				} else {

					return arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount().getCoaCode();

				}

			}


		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

		Debug.print("ArReceiptImportControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        	lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private double getTaxAmount(LocalArTaxCode arTaxCode, double TAXABLE_AMOUNT, short precisionUnit) {

		double AMNT = 0d;
		double TAX_AMNT = 0d;

		if (arTaxCode.getTcType().equals("INCLUSIVE")) {

			AMNT = EJBCommon.roundIt(TAXABLE_AMOUNT / (1 + (arTaxCode.getTcRate() / 100)), precisionUnit);

		} else {

			// tax exclusive

			AMNT = TAXABLE_AMOUNT;

		}

		// calculate tax

		if (arTaxCode.getTcType().equals("INCLUSIVE")) {

			TAX_AMNT = EJBCommon.roundIt(TAXABLE_AMOUNT - AMNT, precisionUnit);

		} else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

			TAX_AMNT = EJBCommon.roundIt(TAXABLE_AMOUNT * arTaxCode.getTcRate() / 100, precisionUnit);

		}

		return TAX_AMNT;

	}

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ArReceiptEntryControllerBean ejbCreate");

	}

}