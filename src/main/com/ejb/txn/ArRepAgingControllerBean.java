
/*
 * ArRepAgingControllerBean.java
 *
 * Created on March 12, 2004, 01:49 PM
 *
 * @author  Dennis Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArAppliedCredit;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdPreferenceDetails;
import com.util.ArRepAgingDetails;
import com.util.ArRepAgingSummaryDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepAgingControllerEJB"
 *           dicstay-name="Used for reporting aged receivables"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepAgingControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepAgingController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepAgingControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class ArRepAgingControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerBatchAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepAgingControllerBean getAdLvCustomerBatchAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArRepAgingControllerBean getArCcAll");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalArCustomerClass arCustomerClass = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);
	        
	        Iterator i = arCustomerClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerClass = (LocalArCustomerClass)i.next();
	        	
	        	list.add(arCustomerClass.getCcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCtAll(Integer AD_CMPNY) {
                    
        Debug.print("ArRepAgingControllerBean getArCtAll");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalArCustomerType arCustomerType = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

	        Iterator i = arCustomerTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerType = (LocalArCustomerType)i.next();
	        	
	        	list.add(arCustomerType.getCtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArRepAgingControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepAging(HashMap criteria, ArrayList branchList, String AGNG_BY, String ORDER_BY, String GROUP_BY, String currency,
    									Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepAgingControllerBean executeArRepAging");
        
        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalCmAdjustmentHome cmAdjustmentHome = null;
        ArrayList list = new ArrayList();
        Date agingDate = null;
        
        //initialized EJB Home
        
        try {
        	cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                    lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
                
            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
		
		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		  short precisionUnit = adCompany.getGlFunctionalCurrency().getFcPrecision();
		  
		  LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(adCompany.getCmpCode());
		  int agingBucket1 = adPreference.getPrfArAgingBucket();
		  int agingBucket2 = adPreference.getPrfArAgingBucket() * 2;
		  int agingBucket3 = adPreference.getPrfArAgingBucket() * 3;
		  int agingBucket4 = adPreference.getPrfArAgingBucket() * 4;
		   
		  boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();
		  
		  StringBuffer jbossQl = new StringBuffer();
		  StringBuffer jbossQlAdv = new StringBuffer();
		  StringBuffer jbossQlAdvPaid = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE (");
		  jbossQlAdv.append("SELECT OBJECT(adj) FROM CmAdjustment adj WHERE (");
		  jbossQlAdvPaid.append("SELECT OBJECT(ai) FROM ArAppliedInvoice ai WHERE (");
		  
		  
		  if (branchList.isEmpty())
		  	throw new GlobalNoRecordFoundException();
		  
		  Iterator brIter = branchList.iterator();
		  
		  AdBranchDetails details = (AdBranchDetails)brIter.next();
		  jbossQl.append("ips.arInvoice.invAdBranch=" + details.getBrCode());
		  jbossQlAdv.append("adj.adjAdBranch=" + details.getBrCode());
		  jbossQlAdvPaid.append("ai.arReceipt.rctAdBranch=" + details.getBrCode());
		  
		  while(brIter.hasNext()) {
		  	
		  		details = (AdBranchDetails)brIter.next();
		  		
		  		jbossQl.append(" OR ips.arInvoice.invAdBranch=" + details.getBrCode());
		  		jbossQlAdv.append(" OR adj.adjAdBranch=" + details.getBrCode());
		  		jbossQlAdvPaid.append(" OR ai.arReceipt.rctAdBranch=" + details.getBrCode());
			  	
		  }
		  
		  jbossQl.append(") ");
		  jbossQlAdv.append(") ");
		  jbossQlAdvPaid.append(") ");
		  firstArgument = false;
		  
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("customerCode")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
          if (criteria.containsKey("includeUnpostedTransaction")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
          
          if (criteria.containsKey("includeAdvance")) {
    	      	
  	      	 criteriaSize--;
  	      	 
  	      }
          
          if (criteria.containsKey("includeAdvanceOnly")) {
  	      	
   	      	 criteriaSize--;
   	      	 
   	      }
	      
	      if (criteria.containsKey("includePaid")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("customerCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  		jbossQl.append("AND ");
		  	    jbossQlAdv.append("AND ");
		  	    jbossQlAdvPaid.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	jbossQlAdv.append("WHERE ");
		     	jbossQlAdvPaid.append("WHERE ");
		     	
		     }
		     
		  	jbossQl.append("ips.arInvoice.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
		  	 jbossQlAdv.append("adj.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
		  	 jbossQlAdvPaid.append("ai.arReceipt.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("customerName")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   		  jbossQl.append("AND ");
		   		  jbossQlAdv.append("AND ");
		   		  jbossQlAdvPaid.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 jbossQlAdv.append("WHERE ");
		   	  	 jbossQlAdvPaid.append("WHERE ");		       	  	 
		   	  }

		   	  jbossQl.append("ips.arInvoice.arCustomer.arCustomerName.ctName=?" + (ctr+1) + " ");
		   	  jbossQlAdv.append("adj.arCustomer.arCustomerName.ctName=?" + (ctr+1) + " ");
		   	  jbossQlAdvPaid.append("ai.arReceipt.arCustomer.arCustomerName.ctName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerName");
		   	  ctr++;
	   	  
	      }	

		  if (criteria.containsKey("customerType")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   		jbossQl.append("AND ");
		   	     jbossQlAdv.append("AND ");
		   	     jbossQlAdvPaid.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 jbossQlAdv.append("WHERE ");
		   	  	 jbossQlAdvPaid.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("ips.arInvoice.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  jbossQlAdv.append("adj.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  jbossQlAdvPaid.append("ai.arReceipt.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerType");
		   	  ctr++;
	   	  
	      }
		  
		  if (criteria.containsKey("customerBatch")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   		  jbossQl.append("AND ");
		   	     jbossQlAdv.append("AND ");
		   	     jbossQlAdvPaid.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 jbossQlAdv.append("WHERE ");
		   	  	 jbossQlAdvPaid.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("ips.arInvoice.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
		   	  jbossQlAdv.append("adj.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
		   	  jbossQlAdvPaid.append("ai.arReceipt.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerBatch");
		   	  ctr++;
	   	  
	      }
			
		  if (criteria.containsKey("customerClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   		jbossQl.append("AND ");
		   	     jbossQlAdv.append("AND ");
		   	     jbossQlAdvPaid.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 jbossQlAdv.append("WHERE ");
		   	  	 jbossQlAdvPaid.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("ips.arInvoice.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  jbossQlAdv.append("adj.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  jbossQlAdvPaid.append("ai.arReceipt.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerClass");
		   	  ctr++;
	   	  
	      }		          
          			
	      if (criteria.containsKey("date")) {
		      	
		  	 if (!firstArgument) {
		  		jbossQl.append("AND ");
		  	 	jbossQlAdv.append("AND ");
		  	 	jbossQlAdvPaid.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 	jbossQlAdv.append("WHERE ");
		  	 	jbossQlAdvPaid.append("WHERE ");
		  	 }
		  	jbossQl.append("ips.arInvoice.invDate<=?" + (ctr+1) + " ");
		  	 jbossQlAdv.append("adj.adjDate<=?" + (ctr+1) + " ");
		  	jbossQlAdvPaid.append("ai.arReceipt.rctDate<=?" + (ctr+1) + " ");
		  	
		  	 obj[ctr] = (Date)criteria.get("date");
		  	 agingDate = (Date)criteria.get("date");
		  	 ctr++;
		  }  		      		  
		  		  
	      if (criteria.containsKey("includeUnpostedTransaction")) {
	          
	          String unposted = (String)criteria.get("includeUnpostedTransaction");
	          
	          if (!firstArgument) {
		       	  	
	        	  jbossQl.append("AND ");
		       	  jbossQlAdv.append("AND ");
		       	  jbossQlAdvPaid.append("AND ");
		       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 jbossQlAdv.append("WHERE ");
	       	  	 jbossQlAdvPaid.append("WHERE ");
	       	  	 
	       	  }	       	  
	                  	
	       	  if (unposted.equals("NO")) {
	       			       	  		       	  
	       		  jbossQl.append("ips.arInvoice.invPosted = 1 ");
		       	  jbossQlAdv.append("adj.adjPosted = 1 ");
		       	  jbossQlAdvPaid.append("ai.arReceipt.rctPosted = 1 ");
		       	  
	       	  } else {
	       	  	
	       		  jbossQl.append("ips.arInvoice.invVoid = 0 "); 
	       	      jbossQlAdv.append("adj.adjVoid = 0 "); 
	       	      jbossQlAdvPaid.append("ai.arReceipt.rctVoid = 0 "); 
	       	  	
	       	  }   	 
	       	  	       	  
	      }	
	      
	      if (!firstArgument) {
	       	  	
		   	     jbossQl.append("AND ");
		   	     jbossQlAdv.append("AND ");
		   	     jbossQlAdvPaid.append("AND ");
		   	     
		   	  } else {
		   	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 jbossQlAdv.append("WHERE ");
		   	  	 jbossQlAdvPaid.append("WHERE ");
		  }	       	  	
	   	  	   	  	   	  
	      jbossQl.append("ips.arInvoice.invCreditMemo = 0 AND ips.ipsAdCompany=" + AD_CMPNY);
	      jbossQlAdv.append("adj.adjType = 'ADVANCE' AND adj.adjAdCompany=" + AD_CMPNY);  
   	  	  jbossQlAdvPaid.append("ai.aiCreditBalancePaid > 0 AND ai.arReceipt.rctAdCompany=" + AD_CMPNY);  
 	  	    			  
   	  	  String includePaid = (String)criteria.get("includePaid");  	 
		  
   	  	  System.out.println(jbossQl.toString());
   	  	  
   	  	  String includeAdvance = (String)criteria.get("includeAdvance");
   	  	  String includeAdvanceOnly = (String)criteria.get("includeAdvanceOnly");
	      
	      if(includeAdvance.contains("YES") || includeAdvanceOnly.contains("YES")){

	    	  Collection advancePayments = cmAdjustmentHome.getAdjByCriteria(jbossQlAdv.toString(), obj);	         
		      

		      Iterator x = advancePayments.iterator();

		      while (x.hasNext()) {
		    	  
		    	  LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)x.next();
		    	  
		    	  //System.out.println("cmAdjustment="+cmAdjustment.getAdjDocumentNumber());
		    	  ArRepAgingDetails mdetails = new ArRepAgingDetails();		  	  
		    	  mdetails.setAgTransactionDate(cmAdjustment.getAdjDate());
		    	  mdetails.setAgInvoiceNumber(cmAdjustment.getAdjDocumentNumber());
		    	  mdetails.setAgCustomerCode(cmAdjustment.getArCustomer().getCstCustomerCode());
	  	  	      mdetails.setAgCustomerName(cmAdjustment.getArCustomer().getCstName());
	  	  	      mdetails.setAgCustomerClass(cmAdjustment.getArCustomer().getArCustomerClass().getCcName());	
	  	  	      mdetails.setAgDescription("ADVANCE");
	  	  	      if(cmAdjustment.getArCustomer().getArCustomerType() == null) {
		  	  		
		  	  		mdetails.setAgCustomerType("UNDEFINE");
		  	  		
	  	  	      } else {
		  	  	
		  	  		mdetails.setAgCustomerType(cmAdjustment.getArCustomer().getArCustomerType().getCtName());
					
	  	  	      }
	  	  	      mdetails.setGroupBy(GROUP_BY);
	  	  	      
	  	  	      double ADV_AMNT = 0d;
	  	  	     
	  	  	      if (cmAdjustment.getAdjDate().before(agingDate) || cmAdjustment.getAdjDate().equals(agingDate)) {
	 
	  	  	    	 if(currency.equals("USD") && cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName().equals("USD")){

	  	  	    		  //CM_AMNT=(arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiCreditableWTax())*-1;

	  	  	    	  }else{
	  	  	    		  
	  	  	    		Collection arAppliedCredits = cmAdjustment.getArAppliedCredits();
		  	              
		  	              Iterator y = arAppliedCredits.iterator();
		  	              
		  	              double totalAppliedCredit = 0d;
		  	              
		  	              while(y.hasNext()){
		  	              	
		  	              	LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)y.next();
		  	              	
		  	              	totalAppliedCredit += arAppliedCredit.getAcApplyCredit();
		  	              	
		  	              }
	  	  	    		ADV_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
	  	  	    			cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(),
	  	  	    			cmAdjustment.getAdjConversionDate(),
	  	  	    			cmAdjustment.getAdjConversionRate(),
	  	  	    		cmAdjustment.getAdjAmount() - totalAppliedCredit - cmAdjustment.getAdjRefundAmount(), AD_CMPNY) * -1;
		  	  	      	
	  	  	    	  }
	  	  	      	
	  	  	      	mdetails.setAgAmount(ADV_AMNT);
	  	  	      	
	  	  	      	int INVOICE_AGE = 0;
			  	  
			  	  if (AGNG_BY.equals("DUE DATE")) {
			  	  	
			  	  	  INVOICE_AGE = (short)((agingDate.getTime() -
			  	  			cmAdjustment.getAdjDate().getTime()) / (1000 * 60 * 60 * 24));
			  	  	
			  	  }	else if (AGNG_BY.equals("INVOICE DATE")){
			  	  	  
			  	  	  INVOICE_AGE = (short)((agingDate.getTime() -
			  	  			cmAdjustment.getAdjDate().getTime()) / (1000 * 60 * 60 * 24));       				        				
			  	  	
			  	  } else if (AGNG_BY.equals("RECEIVED DATE")){
			  	  	  
			  	  	  INVOICE_AGE = (short)((agingDate.getTime() -
			  	  			cmAdjustment.getAdjDate().getTime()) / (1000 * 60 * 60 * 24));       				        				
			  	  	
			  	  }else {
			  	      
			  	      GregorianCalendar calendar = new GregorianCalendar();
			  	      calendar.setTime(cmAdjustment.getAdjDate());
			  	      calendar.add(GregorianCalendar.DATE, cmAdjustment.getArCustomer().getCstEffectivityDays());
			  	      Date effectivityDate = calendar.getTime();
			  	      
			  	      INVOICE_AGE = (short)((agingDate.getTime() -
			  	            effectivityDate.getTime()) / (1000 * 60 * 60 * 24));
			  	      
			  	  }
	  	  	      	if (INVOICE_AGE <= 0) {
	  	  	      		
	  	  	      		mdetails.setAgBucket0(ADV_AMNT);
	  	  	      		
	  	  	      	} else if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {
	  	  	      		
	  	  	      		mdetails.setAgBucket1(ADV_AMNT);
	  	  	      		
	  	  	      	} else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {
	  	  	      		
	  	  	      		mdetails.setAgBucket2(ADV_AMNT);
	  	  	      		
	  	  	      	} else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {
	  	  	      		
	  	  	      		mdetails.setAgBucket3(ADV_AMNT);
	  	  	      		
	  	  	      	} else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {
	  	  	      		
	  	  	      		mdetails.setAgBucket4(ADV_AMNT);
	  	  	      		
	  	  	      	} else if (INVOICE_AGE > agingBucket4) {
	  	  	      		
	  	  	      		mdetails.setAgBucket5(ADV_AMNT);
	  	  	      		
	  	  	      	}
	  	  	      	mdetails.setAgVouFcSymbol(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcSymbol());
	  	  	      	list.add(mdetails);
	  	  	      	
	  	  	      }
			  	  

		    	  
		      }
		      
	    	  
	      }
	      
	      if(includeAdvanceOnly.contains("NO")){
	    	  
	    	  Date LATEST_OR_DATE = null;
	    	  boolean isFirst = true;
	    	  String CST_CODE = "";
	      
	      Collection arInvoicePaymentSchedules = arInvoicePaymentScheduleHome.getIpsByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arInvoicePaymentSchedules.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arInvoicePaymentSchedules.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();   	  
		  	  		  	
		  	  LocalArInvoice arInvoice = arInvoicePaymentSchedule.getArInvoice();
		  	  if (!CST_CODE.equals(arInvoice.getArCustomer().getCstCustomerCode())){
		  		  LATEST_OR_DATE = null;
		  		  isFirst = true;
		  	  }
		  	
		  	  ArRepAgingDetails mdetails = new ArRepAgingDetails();
		  	  mdetails.setAgLastOrDate(LATEST_OR_DATE == null ? arInvoice.getInvDate() : LATEST_OR_DATE);
		  	  int DATE_DIFF = Days.daysBetween(new LocalDate(LATEST_OR_DATE == null ? arInvoice.getInvDate() : LATEST_OR_DATE), new LocalDate(agingDate)).getDays();
		  	  CST_CODE = arInvoice.getArCustomer().getCstCustomerCode();
		  	  mdetails.setAgAgedInMonth(DATE_DIFF);
		  	  
		  	  mdetails.setAgCustomerName(arInvoice.getArCustomer().getCstName());
		  	  mdetails.setAgCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
		  	  mdetails.setAgInvoiceNumber(arInvoice.getInvNumber());
		  	  mdetails.setAgReferenceNumber(arInvoice.getInvReferenceNumber());
		  	  mdetails.setAgInstallmentNumber(arInvoicePaymentSchedule.getIpsNumber());
		  	  mdetails.setAgInstallmentAmount(arInvoicePaymentSchedule.getIpsAmountDue());
		  	  mdetails.setAgTransactionDate(arInvoice.getInvDate());
		  	  mdetails.setAgIpsDueDate(arInvoicePaymentSchedule.getIpsDueDate());
		  	  mdetails.setAgCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());	
		  	  mdetails.setAgDescription(arInvoice.getInvDescription());
		  	  try{
		  		mdetails.setAgSalesPerson(arInvoice.getArSalesperson().getSlpSalespersonCode());
		  	  }catch(Exception e){
		  		mdetails.setAgSalesPerson("");
		  	  }
		  	  
		  	  if(arInvoice.getArCustomer().getArCustomerType() == null) {
		  	  		
		  	  		mdetails.setAgCustomerType("UNDEFINE");
		  	  		
		  	  } else {
		  	  	
		  	  		mdetails.setAgCustomerType(arInvoice.getArCustomer().getArCustomerType().getCtName());
					
		  	  }
		  	  mdetails.setOrderBy(ORDER_BY);
		  	  mdetails.setGroupBy(GROUP_BY);
		  	  
		  	  double AMNT_DUE = 0d;
		  	  
		  	  if (includePaid.equals("NO")) {
		  	  
		  	       // get future cm
		  	  	
		  	  	   double CM_AMNT = 0d;
		  	  	
		  	  	   Collection arCreditMemos = arInvoiceHome.findByInvCreditMemoAndInvCmInvoiceNumberAndInvVoidAndInvPosted(EJBCommon.TRUE, arInvoice.getInvNumber(), EJBCommon.FALSE, EJBCommon.TRUE, AD_CMPNY);
		  	  	    
		  	  	   Iterator arCmIter = arCreditMemos.iterator();
		  	  	    
		  	  	   while (arCmIter.hasNext()) {
		  	  	    	
		  	  	    	LocalArInvoice arCreditMemo = (LocalArInvoice) arCmIter.next();

		  	  	    	if (arCreditMemo.getInvDate().after(agingDate)) {
		  	  	    		
		  	  	        	CM_AMNT += EJBCommon.roundIt(arCreditMemo.getInvAmountDue() * (arInvoicePaymentSchedule.getIpsAmountDue() / arInvoice.getInvAmountDue()), precisionUnit);
		  	  	        	
		  	  	        } 
		  	  	    	
		  	  	    }
		  	  	   		  	  	    
		  	  	    // get future receipts
		  	  	   
		  	  	   	
		  	  	   	
		  	  	   
		  	  	    double RCPT_AMNT = 0d;
		  	  	    
		  	  	    Collection arAppliedInvoices = arAppliedInvoiceHome.findPostedAiByIpsCode(arInvoicePaymentSchedule.getIpsCode(), AD_CMPNY);
		  	  	    
		  	  	    Iterator arAiIter = arAppliedInvoices.iterator();
					
		  	  	    while (arAiIter.hasNext()) {
		  	  	    	
		  	  	    	LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice) arAiIter.next();
		  	  	    	
		  	  	    	if(isFirst){
		  	  	    		
		  	  	    		LATEST_OR_DATE = arAppliedInvoice.getArReceipt().getRctDate();
		  	  	    	
		  	  	    		isFirst = false;
		  	  	    	} else {

		  	  	    		LATEST_OR_DATE = 
		  	  	    			arAppliedInvoice.getArReceipt().getRctDate().after(LATEST_OR_DATE) ? arAppliedInvoice.getArReceipt().getRctDate(): LATEST_OR_DATE;
		  	  	    		
		  	  	    	}
		  	  	    	
		  	  	    	System.out.println("LATEST_OR_DATE="+LATEST_OR_DATE);
		  	  	    	mdetails.setAgLastOrDate(LATEST_OR_DATE);
		  	  	    	
		  	  	    	
		  	  	    	DATE_DIFF = Days.daysBetween(new LocalDate(LATEST_OR_DATE), new LocalDate(agingDate)).getDays();

						mdetails.setAgAgedInMonth(DATE_DIFF);

		  	  	    	if (arAppliedInvoice.getArReceipt().getRctDate().after(agingDate)) {
		  	  	    	
		  	  	    		RCPT_AMNT += arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiCreditableWTax();
		  	  	    		
		  	  	    	}
		  	  	    	
		  	  	    }

		  	  	    if(currency.equals("USD") && arInvoice.getGlFunctionalCurrency().getFcName().equals("USD")){

		  	  	    	AMNT_DUE=arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid() + CM_AMNT + RCPT_AMNT;

		  	  	    }else{
		  	  	    	AMNT_DUE = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
		  	  	    			arInvoice.getGlFunctionalCurrency().getFcName(),
		  	  	    			arInvoice.getInvConversionDate(),
		  	  	    			arInvoice.getInvConversionRate(),
		  	  	    			arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid() + CM_AMNT + RCPT_AMNT, AD_CMPNY);

		  	  	    }
		  	  		
		  	  		if (AMNT_DUE == 0) continue;
			  	      
		  	  } else {

		  		  if(currency.equals("USD") && arInvoice.getGlFunctionalCurrency().getFcName().equals("USD")){

		  			  AMNT_DUE=arInvoicePaymentSchedule.getIpsAmountDue();

		  		  }else{
		  			AMNT_DUE = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
			  				  arInvoice.getGlFunctionalCurrency().getFcName(),
			  				  arInvoice.getInvConversionDate(),
			  				  arInvoice.getInvConversionRate(),
			  				  arInvoicePaymentSchedule.getIpsAmountDue(), AD_CMPNY);	
		  		  }
		  		  	  	  	
		  	  }
		  	  
		  	  
		  	      
		  	  mdetails.setAgAmount(AMNT_DUE);
		  	  
		  	  
		  	  int INVOICE_AGE = 0;
		  	  
		  	  System.out.println("AGING IS: " + AGNG_BY);
		  	  
		  	  if (AGNG_BY.equals("DUE DATE")) {
		  	  	
		  	  	  INVOICE_AGE = (short)((agingDate.getTime() -
			            arInvoicePaymentSchedule.getIpsDueDate().getTime()) / (1000 * 60 * 60 * 24));
		  	  	
		  	  }	else if (AGNG_BY.equals("INVOICE DATE")){
		  	  	  
		  	  	  INVOICE_AGE = (short)((agingDate.getTime() -
			            arInvoice.getInvDate().getTime()) / (1000 * 60 * 60 * 24));       				        				
		  	  	
		  	  } else if (AGNG_BY.equals("RECEIVED DATE")){
		  	  	  
		  	  	  INVOICE_AGE = (short)((agingDate.getTime() -
			            arInvoice.getInvRecieveDate().getTime()) / (1000 * 60 * 60 * 24));       				        				
		  	  	
		  	  } else {
		  	  	  
		  	      GregorianCalendar calendar = new GregorianCalendar();
		  	      calendar.setTime(arInvoice.getInvDate());
		  	      calendar.add(GregorianCalendar.DATE, arInvoice.getArCustomer().getCstEffectivityDays());
		  	      Date effectivityDate = calendar.getTime();
		  	      
		  	      INVOICE_AGE = (short)((agingDate.getTime() -
		  	            effectivityDate.getTime()) / (1000 * 60 * 60 * 24));
		  	  	
		  	  }
	
		  	  if (INVOICE_AGE <= 0) {
		  	  		
		  	  	   mdetails.setAgBucket0(AMNT_DUE);
		  	  	
		  	  }else if (INVOICE_AGE >=1 && INVOICE_AGE <= agingBucket1) {
		  	  	
		  	  	   mdetails.setAgBucket1(AMNT_DUE);
		  	  	
		  	  } else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {
		  	  	
		  	  	   mdetails.setAgBucket2(AMNT_DUE);
		  	  	
		  	  } else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {
		  	  	
		  	  	   mdetails.setAgBucket3(AMNT_DUE);
		  	  	
		  	  } else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {
		  	  	
		  	  	   mdetails.setAgBucket4(AMNT_DUE);
		  	  	
		  	  } else if (INVOICE_AGE > agingBucket4) {
		  	  	
		  	  	   mdetails.setAgBucket5(AMNT_DUE);
		  	  	   
		  	  }
		  	  
		  	  mdetails.setAgInvoiceAge(INVOICE_AGE);
		  	  			 
		  	  mdetails.setAgVouFcSymbol(arInvoice.getGlFunctionalCurrency().getFcSymbol());
			  list.add(mdetails);
			  
			  if (includePaid.equals("YES") && arInvoicePaymentSchedule.getIpsAmountPaid() != 0) {
			  	  
			  	 // get cm on or after aging date
		  	  	
		  	  	 double CM_AMNT = 0d;
		  	  	
		  	  	 Collection arCreditMemos = arInvoiceHome.findByInvCreditMemoAndInvCmInvoiceNumberAndInvVoidAndInvPosted(EJBCommon.TRUE, arInvoice.getInvNumber(), EJBCommon.FALSE, EJBCommon.TRUE, AD_CMPNY);
		  	  	    
		  	  	 Iterator arCmIter = arCreditMemos.iterator();
		  	  	    
		  	  	 while (arCmIter.hasNext()) {
		  	  	    	
		  	  	      LocalArInvoice arCreditMemo = (LocalArInvoice) arCmIter.next();
		  	  	      
		  	  	      mdetails = new ArRepAgingDetails();
		  	  	      
		  	  	      mdetails.setAgCustomerName(arInvoice.getArCustomer().getCstName());
		  	  	      mdetails.setAgCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
		  	  	      mdetails.setAgInvoiceNumber(arCreditMemo.getInvNumber());
		  	  	      mdetails.setAgReferenceNumber(arCreditMemo.getInvCmInvoiceNumber());
		  	  	      mdetails.setAgInstallmentNumber((short)0);
		  	  	      mdetails.setAgTransactionDate(arCreditMemo.getInvDate());
		  	  	      mdetails.setAgIpsDueDate(arInvoicePaymentSchedule.getIpsDueDate());
		  	  	      mdetails.setAgCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());
		  	  	      mdetails.setAgDescription(arInvoice.getInvDescription());
		  	  	      try{
		  	  	    	  mdetails.setAgSalesPerson(arInvoice.getArSalesperson().getSlpSalespersonCode());
		  	  	      }catch(Exception e){
		  	  	    	  mdetails.setAgSalesPerson("");
		  	  	      }
		  	  	      if(arInvoice.getArCustomer().getArCustomerType() == null) {

		  	  	      	mdetails.setAgCustomerType("UNDEFINE");
			  	  		
		  	  	      } else {
			  	  	
			  	  		mdetails.setAgCustomerType(arInvoice.getArCustomer().getArCustomerType().getCtName());
						
		  	  	      }
			  	  	  mdetails.setOrderBy(ORDER_BY);
				  	  mdetails.setGroupBy(GROUP_BY);
		  	  	      
		  	  	      if (arCreditMemo.getInvDate().before(agingDate) || arCreditMemo.getInvDate().equals(agingDate)) {
		  	  	    	  if(currency.equals("USD") && arInvoice.getGlFunctionalCurrency().getFcName().equals("USD")){

		  	  	    		  CM_AMNT=(EJBCommon.roundIt(arCreditMemo.getInvAmountDue() * (arInvoicePaymentSchedule.getIpsAmountDue() / arInvoice.getInvAmountDue()), precisionUnit))*-1;

		  	  	    	  }else{
		  	  	    		  CM_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
		  	  	    				  arInvoice.getGlFunctionalCurrency().getFcName(),
		  	  	    				  arInvoice.getInvConversionDate(),
		  	  	    				  arInvoice.getInvConversionRate(),
		  	  	    				  EJBCommon.roundIt(arCreditMemo.getInvAmountDue() * (arInvoicePaymentSchedule.getIpsAmountDue() / arInvoice.getInvAmountDue()), precisionUnit), AD_CMPNY) * -1;

		  	  	    	  }
		  	  	      	
		  	  	      	mdetails.setAgAmount(CM_AMNT);
		  	  	      	
		  	  	      	if (INVOICE_AGE <= 0) {
		  	  	      		
		  	  	      		mdetails.setAgBucket0(CM_AMNT);
		  	  	      		
		  	  	      	}if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {
		  	  	      		
		  	  	      		mdetails.setAgBucket1(CM_AMNT);
		  	  	      		
		  	  	      	} else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {
		  	  	      		
		  	  	      		mdetails.setAgBucket2(CM_AMNT);
		  	  	      		
		  	  	      	} else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {
		  	  	      		
		  	  	      		mdetails.setAgBucket3(CM_AMNT);
		  	  	      		
		  	  	      	} else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {
		  	  	      		
		  	  	      		mdetails.setAgBucket4(CM_AMNT);
		  	  	      		
		  	  	      	} else if (INVOICE_AGE > agingBucket4) {
		  	  	      		
		  	  	      		mdetails.setAgBucket5(CM_AMNT);
		  	  	      		
		  	  	      	}
		  	  	    mdetails.setAgVouFcSymbol(arInvoice.getGlFunctionalCurrency().getFcSymbol());
		  	  	      	list.add(mdetails);
		  	  	      	
		  	  	      } 	
		  	  	      
		  	  	  }
		  	  	    
		  	  	  // get future receipts
		  	  	   
		  	  	  double RCPT_AMNT = 0d;
		  	  	    
		  	  	  Collection arAppliedInvoices = arAppliedInvoiceHome.findPostedAiByIpsCode(arInvoicePaymentSchedule.getIpsCode(), AD_CMPNY);
		  	  	    
		  	  	  Iterator arAiIter = arAppliedInvoices.iterator();
		  	  	  
		  	  	  
		  	  	  while (arAiIter.hasNext()) {
		  	  	    	
		  	  	      LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice) arAiIter.next();
		  	  	      
		  	  	      mdetails = new ArRepAgingDetails();
		  	  	      
		  	  	  if(isFirst){
	  	  	    		
	  	  	    		LATEST_OR_DATE = arAppliedInvoice.getArReceipt().getRctDate();
	  	  	    		isFirst = false;
	  	  	    	} else {

	  	  	    		LATEST_OR_DATE = 
	  	  	    			arAppliedInvoice.getArReceipt().getRctDate().after(LATEST_OR_DATE) ? arAppliedInvoice.getArReceipt().getRctDate(): LATEST_OR_DATE;
	  	  	    	}
	  	  	    	
	  	  	    	
	  	  	    	mdetails.setAgLastOrDate(LATEST_OR_DATE);
		  	  	      
	  	  	    		DATE_DIFF = Days.daysBetween(new LocalDate(LATEST_OR_DATE), new LocalDate(agingDate)).getDays();
		  	  	      
		  	  	      mdetails.setAgAgedInMonth(DATE_DIFF);
		  	  	      
		  	  	      mdetails.setAgCustomerName(arInvoice.getArCustomer().getCstName());
		  	  	      mdetails.setAgCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
		  	  	      mdetails.setAgInvoiceNumber(arAppliedInvoice.getArReceipt().getRctNumber());
		  	  	      mdetails.setAgReferenceNumber(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvNumber());
		  	  	      mdetails.setAgInstallmentNumber((short)0);
		  	  	      mdetails.setAgTransactionDate(arAppliedInvoice.getArReceipt().getRctDate());
		  	  	      mdetails.setAgIpsDueDate(arInvoicePaymentSchedule.getIpsDueDate());
		  	  	      mdetails.setAgCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());	
		  	  	      mdetails.setAgDescription(arInvoice.getInvDescription());
		  	  	      try{
		  	  	    	  mdetails.setAgSalesPerson(arInvoice.getArSalesperson().getSlpSalespersonCode());
		  	  	      }catch(Exception e){
		  	  	    	  mdetails.setAgSalesPerson("");
		  	  	      }
		  	  	      if(arInvoice.getArCustomer().getArCustomerType() == null) {

		  	  	    	  mdetails.setAgCustomerType("UNDEFINE");

		  	  	      } else {

			  	  		mdetails.setAgCustomerType(arInvoice.getArCustomer().getArCustomerType().getCtName());
						
		  	  	      }		  	  	      
			  	  	  mdetails.setOrderBy(ORDER_BY);
				  	  mdetails.setGroupBy(GROUP_BY);
		  	  	    	
		  	  	      if (arAppliedInvoice.getArReceipt().getRctDate().before(agingDate) || arAppliedInvoice.getArReceipt().getRctDate().equals(agingDate)) {
		 
		  	  	    	  if(currency.equals("USD") && arInvoice.getGlFunctionalCurrency().getFcName().equals("USD")){

		  	  	    		  RCPT_AMNT=(arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditBalancePaid()  + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiCreditableWTax())*-1;

		  	  	    	  }else{
		  	  	    		  RCPT_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
		  	  	    				  arInvoice.getGlFunctionalCurrency().getFcName(),
		  	  	    				  arInvoice.getInvConversionDate(),
		  	  	    				  arInvoice.getInvConversionRate(),
		  	  	    				  arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditBalancePaid()  + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiCreditableWTax(), AD_CMPNY) * -1;

		  	  	    	  }
		  	  	      	
		  	  	      	mdetails.setAgAmount(RCPT_AMNT);
		  	  	      	
		  	  	      	if (INVOICE_AGE <= 0) {
		  	  	      		
		  	  	      		mdetails.setAgBucket0(RCPT_AMNT);
		  	  	      		
		  	  	      	} else if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {
		  	  	      		
		  	  	      		mdetails.setAgBucket1(RCPT_AMNT);
		  	  	      		
		  	  	      	} else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {
		  	  	      		
		  	  	      		mdetails.setAgBucket2(RCPT_AMNT);
		  	  	      		
		  	  	      	} else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {
		  	  	      		
		  	  	      		mdetails.setAgBucket3(RCPT_AMNT);
		  	  	      		
		  	  	      	} else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {
		  	  	      		
		  	  	      		mdetails.setAgBucket4(RCPT_AMNT);
		  	  	      		
		  	  	      	} else if (INVOICE_AGE > agingBucket4) {
		  	  	      		
		  	  	      		mdetails.setAgBucket5(RCPT_AMNT);
		  	  	      		
		  	  	      	}
		  	  	      	
		  	  	      	mdetails.setAgVouFcSymbol(arInvoice.getGlFunctionalCurrency().getFcSymbol());
		  	  	      	
		  	  	      	list.add(mdetails);
		  	  	      	
		  	  	      }
		  	  	      	
		  	  	  }		  	  

			  }
			         
		  }
		  
		  if (list.isEmpty()) {
		  	
		  	  throw new GlobalNoRecordFoundException();
		  	
		  }
		  
	      }
			     
		  if(GROUP_BY.equals("CUSTOMER CODE")) {
		  	
		  		Collections.sort(list, ArRepAgingDetails.CustomerCodeComparator);
		  		
		  }else if(GROUP_BY.equals("CUSTOMER NAME")) {
		  		
		  		Collections.sort(list, ArRepAgingDetails.CustomerNameComparator);
		  		
		  }else if(GROUP_BY.equals("CUSTOMER TYPE")) {
		  		
		  		Collections.sort(list, ArRepAgingDetails.CustomerTypeComparator);
		  		
		  }else if(GROUP_BY.equals("CUSTOMER CLASS")) {
		  	
		  		Collections.sort(list, ArRepAgingDetails.CustomerClassComparator);
		  		
		  }else if(GROUP_BY.equals("SAR")) {
			  	
		  		Collections.sort(list, ArRepAgingDetails.SARComparator);
		  		
		  }else {
		  	
	  			Collections.sort(list, ArRepAgingDetails.NoClassComparator);
	  		
		  }
		  
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	
	  	  Debug.printStackTrace(ex);
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepAgingControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdPreferenceDetails getAdPreference(Integer AD_CMPNY) {
	
	  Debug.print("ArRepAgingControllerBean getAdPreference");      
	  
	  LocalAdPreferenceHome adPreferenceHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
		  adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	     
	     AdPreferenceDetails details = new AdPreferenceDetails();
	     details.setPrfApAgingBucket(adPreference.getPrfApAgingBucket());
	     details.setPrfArAgingBucket(adPreference.getPrfArAgingBucket());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	} 
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    //Debug.print("ArRepAgingControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepAgingControllerBean ejbCreate");
      
    }
}
