
/*
 * ArRepCreditMemoPrintControllerBean.java
 *
 * Created on March 11, 2004, 1:27 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArRepInvoicePrintDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepCreditMemoPrintControllerEJB"
 *           display-name="Used for printing credit memo transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepCreditMemoPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepCreditMemoPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepCreditMemoPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
 */

public class ArRepCreditMemoPrintControllerBean extends AbstractSessionBean {


	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public ArrayList executeArRepInvoicePrint(ArrayList invCodeList, Integer AD_BRNCH, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {

		Debug.print("ArRepCreditMemoPrintControllerBean executeArRepInvoicePrint");

		LocalArInvoiceHome arInvoiceHome = null;    
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
		LocalAdUserHome adUserHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalArInvoiceLineHome arInvoiceLineHome = null;
	
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			arInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
			

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}        

		try {

			Iterator i = invCodeList.iterator();

			while (i.hasNext()) {

				Integer INV_CODE = (Integer) i.next();

				LocalArInvoice arInvoice = null;
				LocalArInvoice arCreditedInvoice = null;


				try {

					arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);
					arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);		
					
					
					

				} catch (FinderException ex) {

					continue;

				}

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);	        		
				LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("AR CREDIT MEMO", AD_CMPNY);	        	

				if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {

					if (arInvoice.getInvApprovalStatus() == null || 
							arInvoice.getInvApprovalStatus().equals("PENDING")) {

						continue;

					} 


				} else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {

					if (arInvoice.getInvApprovalStatus() != null && 
							(arInvoice.getInvApprovalStatus().equals("N/A") || 
									arInvoice.getInvApprovalStatus().equals("APPROVED"))) {

						continue;

					}

				}

				if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE && 
						arInvoice.getInvPrinted() == EJBCommon.TRUE){

					continue;	

				}

				// show duplicate

				boolean showDuplicate = false;

				if (adApprovalDocument.getAdcTrackDuplicate() == EJBCommon.TRUE &&
						arInvoice.getInvPrinted() == EJBCommon.TRUE) {

					showDuplicate = true;

				}

				// set printed


				Collection arInvoiceLineItems = null;
			    Collection arInvoiceLines = null;
			    
			    
			    
			    try {

			    	if(arInvoice.getInvCmInvoiceNumber()!=null) {
			    		
			    		
			    		
			    		arInvoiceLines = arInvoiceLineHome.findInvoiceLineByInvCodeAndAdCompany(arCreditedInvoice.getInvCode(), AD_CMPNY);
			    	}
			    	

				} catch(FinderException ex) {
					System.out.println("no invoice lines");
				}
			    
			    

				try {

					arInvoiceLineItems = arInvoiceLineItemHome.findByInvCode(arInvoice.getInvCode(), AD_CMPNY);

				} catch(FinderException ex) {
					System.out.println("no invoice line items");
				}

				
				double TOTAL_TAX_AMNT = 0;
				double TOTAL_WTAX_AMNT = 0;
				Collection arDistributionRecords = null;


					arDistributionRecords = arInvoice.getArDistributionRecords();
					System.out.println("size dr: "+ arDistributionRecords.size());
					Iterator iDr = arDistributionRecords.iterator();
					
					while(iDr.hasNext()) {
						
						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)iDr.next();
						System.out.println("dr class:"+ arDistributionRecord.getDrClass());
						if(arDistributionRecord.getDrClass().equals("TAX")) {
							TOTAL_TAX_AMNT += arDistributionRecord.getDrAmount();
						}
						if(arDistributionRecord.getDrClass().equals("W-TAX")) {
							TOTAL_WTAX_AMNT += arDistributionRecord.getDrAmount();
						}
	
						
					}
					
					
					
					
					
					
					


				
				
				if(arInvoiceLineItems.size()>0) {
					Iterator ilIter = arInvoiceLineItems.iterator();

					while (ilIter.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)ilIter.next();



						arInvoice.setInvPrinted(EJBCommon.TRUE);

						ArRepInvoicePrintDetails details = new ArRepInvoicePrintDetails();
						details.setIpInvType(arInvoice.getInvType());
						details.setIpInvDate(arInvoice.getInvDate());
						details.setIpInvNumber(arInvoice.getInvNumber());
						details.setIpInvCmReferenceNumber(arInvoice.getInvCmReferenceNumber());
						System.out.println("arInvoice.getInvCmReferenceNumber()="+arInvoice.getInvCmReferenceNumber());
						
						details.setIpInvAmount(arInvoice.getInvAmountDue());
						details.setIpInvCustomerName(arInvoice.getArCustomer().getCstName());
						details.setIpInvCustomerAddress(arInvoice.getArCustomer().getCstAddress());
						details.setIpInvCreatedBy(arInvoice.getInvCreatedBy());
						details.setIpInvApprovedRejectedBy(arInvoice.getInvApprovedRejectedBy());
						details.setIpIlAmount(arInvoice.getInvAmountDue());
						details.setIpInvDescription(arInvoice.getInvDescription());  	        
						details.setIpInvCurrencySymbol(arCreditedInvoice.getGlFunctionalCurrency().getFcSymbol());
						details.setIpInvCurrencyDescription(arCreditedInvoice.getGlFunctionalCurrency().getFcDescription());
						//details.setIpShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);
						details.setIpInvReferenceNumber(arInvoice.getInvCmInvoiceNumber());
						details.setIpInvCustomerCity(arInvoice.getArCustomer().getCstCity());

						if(arCreditedInvoice.getArSalesperson() != null) {

							details.setIpSlpSalespersonCode(arCreditedInvoice.getArSalesperson().getSlpSalespersonCode());
							details.setIpSlpName(arCreditedInvoice.getArSalesperson().getSlpName());

						}
						details.setIpInvCmInvoiceNumber(arCreditedInvoice.getInvNumber());
						details.setIpInvTotalTax(TOTAL_TAX_AMNT);
						details.setIpInvWithholdingTaxAmount(TOTAL_WTAX_AMNT);
						details.setIpInvCmInvoiceDate(EJBCommon.convertSQLDateToString(arCreditedInvoice.getInvDate()));
						details.setIpInvApprovalStatus(arInvoice.getInvApprovalStatus());
						System.out.println(arInvoice.getInvApprovalStatus());
						// get user name description

						System.out.println("apVoucher.getChkCreatedBy(): "+arInvoice.getInvCreatedBy());

						try{
							LocalAdUser adUser = adUserHome.findByUsrName(arInvoice.getInvCreatedBy(), AD_CMPNY);
							details.setIpInvCreatedByDescription(adUser.getUsrDescription());
						}catch(Exception e){
							details.setIpInvCreatedByDescription("");
						}

						System.out.println("apVoucher.getChkCreatedBy(): "+details.getIpInvCreatedByDescription());

						/*try{
							System.out.println("adPreference.getPrfApDefaultChecker(): "+adPreference.getPrfApDefaultChecker());
		    				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
		    				details.setIpInvCheckByDescription(adUser2.getUsrDescription());
						}catch(Exception e){
							details.setIpInvCheckByDescription("");
						}*/

						try{
							System.out.println("apVoucher.getVouApprovedRejectedBy(): "+arInvoice.getInvApprovedRejectedBy());
							LocalAdUser adUser3 = adUserHome.findByUsrName(arInvoice.getInvApprovedRejectedBy(), AD_CMPNY);
							details.setIpInvApprovedRejectedByDescription(adUser3.getUsrDescription());

						}catch(Exception e){
							details.setIpInvApprovedRejectedByDescription("");
						}
						
						
						
						
						
						System.out.println("DpVouApprovedRejectedByDescription(): "+details.getIpInvApprovedRejectedByDescription());

						details.setIpIlName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
						details.setIpIlDescription(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiDescription());
						details.setIpIlQuantity(arInvoiceLineItem.getIliQuantity());
						details.setIpInvIlSmlUnitOfMeasure(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
						details.setIpIlUnitPrice(arInvoiceLineItem.getIliUnitPrice());
						details.setIpIlAmount(arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount());
						
						System.out.println("amount line is: "+ details.getIpIlAmount());
						details.setIpIliDiscount1(arInvoiceLineItem.getIliDiscount1());
						details.setIpIliTotalDiscount(arInvoiceLineItem.getIliTotalDiscount());
						
						System.out.println("arInvoiceLine.getIlDescription(): " + arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
						System.out.println("arInvoiceLine.getIlQuantity(): " + arInvoiceLineItem.getIliQuantity());
						list.add(details);
					}
				}else {
					
					Iterator ilter = arInvoiceLines.iterator();

					while (ilter.hasNext()) {
						
						
						LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)ilter.next();

						arInvoice.setInvPrinted(EJBCommon.TRUE);

						ArRepInvoicePrintDetails details = new ArRepInvoicePrintDetails();

						details.setIpInvType(arInvoice.getInvType());
						details.setIpInvDate(arInvoice.getInvDate());
						details.setIpInvNumber(arInvoice.getInvNumber());
						details.setIpInvCmReferenceNumber(arInvoice.getInvCmReferenceNumber());
						System.out.println("arInvoice.getInvCmReferenceNumber()="+arInvoice.getInvCmReferenceNumber());
						
						details.setIpInvAmount(arInvoice.getInvAmountDue());
						details.setIpInvCustomerName(arInvoice.getArCustomer().getCstName());
						details.setIpInvCustomerAddress(arInvoice.getArCustomer().getCstAddress());
						details.setIpInvCreatedBy(arInvoice.getInvCreatedBy());
						details.setIpInvApprovedRejectedBy(arInvoice.getInvApprovedRejectedBy());
						details.setIpIlAmount(arInvoice.getInvAmountDue());
						details.setIpInvDescription(arInvoice.getInvDescription());  	        
						details.setIpInvCurrencySymbol(arCreditedInvoice.getGlFunctionalCurrency().getFcSymbol());
						details.setIpInvCurrencyDescription(arCreditedInvoice.getGlFunctionalCurrency().getFcDescription());
						//details.setIpShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);
						details.setIpInvReferenceNumber(arInvoice.getInvCmInvoiceNumber());
						details.setIpInvCustomerCity(arInvoice.getArCustomer().getCstCity());

						if(arCreditedInvoice.getArSalesperson() != null) {

							details.setIpSlpSalespersonCode(arCreditedInvoice.getArSalesperson().getSlpSalespersonCode());
							details.setIpSlpName(arCreditedInvoice.getArSalesperson().getSlpName());

						}
						details.setIpInvCmInvoiceNumber(arCreditedInvoice.getInvNumber());
						
						details.setIpInvCmInvoiceDate(EJBCommon.convertSQLDateToString(arCreditedInvoice.getInvDate()));
						details.setIpInvApprovalStatus(arInvoice.getInvApprovalStatus());
						System.out.println(arInvoice.getInvApprovalStatus());
						// get user name description

						System.out.println("apVoucher.getChkCreatedBy(): "+arInvoice.getInvCreatedBy());

						try{
							LocalAdUser adUser = adUserHome.findByUsrName(arInvoice.getInvCreatedBy(), AD_CMPNY);
							details.setIpInvCreatedByDescription(adUser.getUsrDescription());
						}catch(Exception e){
							details.setIpInvCreatedByDescription("");
						}

						System.out.println("apVoucher.getChkCreatedBy(): "+details.getIpInvCreatedByDescription());

						/*try{
							System.out.println("adPreference.getPrfApDefaultChecker(): "+adPreference.getPrfApDefaultChecker());
		    				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
		    				details.setIpInvCheckByDescription(adUser2.getUsrDescription());
						}catch(Exception e){
							details.setIpInvCheckByDescription("");
						}*/

						try{
							System.out.println("apVoucher.getVouApprovedRejectedBy(): "+arInvoice.getInvApprovedRejectedBy());
							LocalAdUser adUser3 = adUserHome.findByUsrName(arInvoice.getInvApprovedRejectedBy(), AD_CMPNY);
							details.setIpInvApprovedRejectedByDescription(adUser3.getUsrDescription());

						}catch(Exception e){
							details.setIpInvApprovedRejectedByDescription("");
						}
						System.out.println("DpVouApprovedRejectedByDescription(): "+details.getIpInvApprovedRejectedByDescription());

						//details.setIpIlName(arInvoiceLine.getInvItemLocation().getInvItem().getIiName());
						details.setIpIlDescription(arInvoiceLine.getIlDescription());
						details.setIpIlQuantity(arInvoiceLine.getIlQuantity());
					//	details.setIpInvIlSmlUnitOfMeasure(arInvoiceLine.getInvUnitOfMeasure().getUomName());
						details.setIpIlUnitPrice(arInvoiceLine.getIlUnitPrice());
						details.setIpIlAmount(arInvoiceLine.getIlAmount() );
			//			details.setIpIliDiscount1(arInvoiceLine.getIliDiscount1());
				//		details.setIpIliTotalDiscount(arInvoiceLine.getIliTotalDiscount());
						details.setIpInvTotalTax(TOTAL_TAX_AMNT);
					//	System.out.println("arInvoiceLine.getIlDescription(): " + arInvoiceLine.arInvoiceLine());
					//	System.out.println("arInvoiceLine.getIlQuantity(): " + arInvoiceLineItem.getIliQuantity());
						list.add(details);
					}
					
				}
				
				
				
			



			}
			
			
			
			

			if (list.isEmpty()) {
				
					throw new GlobalNoRecordFoundException();
				
			}        	       	

			return list;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

		Debug.print("ArRepCreditMemoPrintControllerBean getAdCompany");      

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());
			details.setCmpAddress(adCompany.getCmpAddress());
			details.setCmpCity(adCompany.getCmpCity());
			details.setCmpCountry(adCompany.getCmpCountry());

			return details;  	

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}    

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeArRepInvoicePrintSub(ArrayList invCodeList, Integer AD_CMPNY) {

		Debug.print("ArRepCreditMemoPrintControllerBean executeArRepInvoicePrintSub");

		LocalArInvoiceHome arInvoiceHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}        

		try {

			Iterator i = invCodeList.iterator();

			while (i.hasNext()) {

				Integer INV_CODE = (Integer) i.next();

				LocalArInvoice arInvoice = null;

				try {

					arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

				} catch (FinderException ex) {

					continue;

				}	        	

				// set printed

				arInvoice.setInvPrinted(EJBCommon.TRUE);

				//get invoice distribution records

				Collection arDistributionRecords = arDistributionRecordHome.findByInvCode(arInvoice.getInvCode(), AD_CMPNY);

				Iterator drIter = arDistributionRecords.iterator();

				while(drIter.hasNext()) {

					LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drIter.next();

					ArRepInvoicePrintDetails details = new ArRepInvoicePrintDetails();

					details.setIpDrCoaAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
					details.setIpDrCoaAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
					details.setIpDrAmount(arDistributionRecord.getDrAmount());
					details.setIpDrDebit(arDistributionRecord.getDrDebit());

					list.add(details);

				}

			}

			if (list.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}        	       	

			return list;    

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ArRepCreditMemoPrintControllerBean ejbCreate");

	}
}
