
/*
 * ArRepCreditableWithholdingTaxControllerBean.java
 *
 * Created on March 26, 2004, 6:51 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlTaxInterface;
import com.ejb.gl.LocalGlTaxInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepCreditableWithholdingTaxDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepCreditableWithholdingTaxControllerEJB"
 *           dicstay-name="Used for viewing creditable withholding tax summary"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepCreditableWithholdingTaxControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepCreditableWithholdingTaxController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepCreditableWithholdingTaxControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepCreditableWithholdingTaxControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArRepCreditableWithholdingTaxControllerBean getArCcAll");
        
        LocalArCustomerClassHome arCustomerClassHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);

	        Iterator i = arCustomerClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArCustomerClass arCustomerClass = (LocalArCustomerClass)i.next();
	        	
	        	list.add(arCustomerClass.getCcName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCtAll(Integer AD_CMPNY) {
                    
        Debug.print("ArRepCreditableWithholdingTaxControllerBean getArCtAll");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

	        Iterator i = arCustomerTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArCustomerType arCustomerType = (LocalArCustomerType)i.next();
	        	
	        	list.add(arCustomerType.getCtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArRepCreditableWithholdingTaxControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepCreditableWithholdingTax(HashMap criteria, ArrayList branchList, String ORDER_BY, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
    	
    	Debug.print("ArRepCreditableWithholdingTaxControllerBean executeArRepCreditableWithholdingTax");
    	
    	LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
        LocalGlTaxInterfaceHome glTaxInterfaceHome = null;        
        LocalGlJournalLineHome glJournalHome = null;
        
        ArrayList list = new ArrayList();
        
        try {
	        
		    arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
		    arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME,LocalArWithholdingTaxCodeHome.class);
		    glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);
		    glJournalHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,LocalGlJournalLineHome.class);                        
		                           
		} catch (NamingException ex) 	{
		    
		    throw new EJBException(ex.getMessage());
		    
		}
		
		try { 
        	//get all available records
        	
			boolean firstArgument = true;
  	        short ctr = 0;
  		    int criteriaSize = criteria.size();
  		    Date dateFrom = null;
  		    Date dateTo = null;
  		    int SEQUENCE_NUMBER = 0;
  		    
  		    StringBuffer jbossQl = new StringBuffer();
  		    
  		    jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti WHERE(");
  		    
  		    if (branchList.isEmpty())
  		    	throw new GlobalNoRecordFoundException();
  		    
  		    Iterator brIter = branchList.iterator();
  		    
  		    AdBranchDetails brDetails = (AdBranchDetails)brIter.next();
  		    jbossQl.append("ti.tiAdBranch=" + brDetails.getBrCode());
        	
        	while(brIter.hasNext()) {
        		
        		brDetails = (AdBranchDetails)brIter.next();
        		
        		jbossQl.append(" OR ti.tiAdBranch=" + brDetails.getBrCode());
        		
        	}
        	
        	jbossQl.append(") ");
        	firstArgument = false;
  		  
  	        Object obj[];	      
    		
  		    //  Allocate the size of the object parameter
  		    
		    if (criteria.containsKey("customerCode")) {
	      	
	      	   criteriaSize--;
	      	 
	        }
		    
	        obj = new Object[criteriaSize];
		       	      
		    if (criteria.containsKey("customerCode")) {
		  	
		  	   if (!firstArgument) {
		  	 
		  	     jbossQl.append("AND ");	
		  	 	
		       } else {
		     	
		     	 firstArgument = false;
		     	 jbossQl.append("WHERE ");
		     	
		       }
		  	   
		  	   jbossQl.append("ti.tiSlSubledgerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
		    }
		    
		    if (criteria.containsKey("dateFrom")) {
		      	
		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate>=?" + (ctr+1) + " ");
		  	   obj[ctr] = (Date)criteria.get("dateFrom");
		  	   dateFrom = (Date)criteria.get("dateFrom");
		  	   ctr++;
		    }  
		      
		    if (criteria.containsKey("dateTo")) {
		  	
		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate<=?" + (ctr+1) + " ");
		  	   obj[ctr] = (Date)criteria.get("dateTo");
		  	   dateTo = (Date)criteria.get("dateTo");
		  	   ctr++;
		  	 
		    }    		  		  		    	  
		  
		    if (!firstArgument) {
		       	  	
	   	       jbossQl.append("AND ");
	   	     
	   	    } else {
	   	  	
	   	  	   firstArgument = false;
	   	  	   jbossQl.append("WHERE ");
	   	  	 
	   	    }

		    jbossQl.append("(ti.tiDocumentType='AR INVOICE' OR ti.tiDocumentType='AR CREDIT MEMO' OR " +
		    				"ti.tiDocumentType='AR RECEIPT' OR ti.tiDocumentType='GL JOURNAL') " + 
							"AND ti.tiIsArDocument=1 AND ti.tiTcCode IS NULL " + 
							"AND ti.tiWtcCode IS NOT NULL AND ti.tiAdCompany=" + AD_CMPNY + 
							" ORDER BY ti.tiSlSubledgerCode");
		    
		    short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);
		    
			Collection glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);
			
			ArrayList arCustomerList = new ArrayList();		    		 
				      		        		        	
			Iterator i = glTaxInterfaces.iterator();
			
			while(i.hasNext()) {
				
				//	get available record per customer
				
				LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();
				
				if (arCustomerList.contains(glTaxInterface.getTiSlSubledgerCode())) {
        			
        			continue;
        			
        		}         		
				
				arCustomerList.add(glTaxInterface.getTiSlSubledgerCode());
				
				jbossQl = new StringBuffer();
        		
        		jbossQl.append("SELECT DISTINCT OBJECT(ti) FROM GlTaxInterface ti " + 
        					   "WHERE (ti.tiDocumentType='AR INVOICE' OR ti.tiDocumentType='AR CREDIT MEMO' " +
        					   "OR ti.tiDocumentType='AR RECEIPT' OR " +
        					   "ti.tiDocumentType='GL JOURNAL') ");
        		
        		ctr = 0;        		        		
        		criteriaSize = 0;
        		String sbldgrCode = glTaxInterface.getTiSlSubledgerCode(); 
        		
        		if(dateFrom != null ) { 
        			criteriaSize++;        			
        		}
        		
        		if(dateTo != null) {
        			criteriaSize++;        		
        		}
        		
        		if(sbldgrCode != null) {
        			criteriaSize++;        		
        		}
        		
        		obj = new Object[criteriaSize];
        		
        		if(sbldgrCode != null) {
	       		     
        			jbossQl.append("AND ti.tiSlSubledgerCode = ?"  + (ctr+1) + " ");
     			   	obj[ctr] = sbldgrCode;
     			   	ctr++;
				
				} else {
				
					jbossQl.append("AND ti.tiSlSubledgerCode IS NULL ");
				
				}
        		
        		if (dateFrom != null) {
	  	    		    
					jbossQl.append("AND ti.tiTxnDate >=?" + (ctr+1) + " ");
					obj[ctr] = dateFrom;
					ctr++;
				
				}
				
				if(dateTo != null) {
					  
					jbossQl.append("AND ti.tiTxnDate <=?" + (ctr+1) + " ");
					obj[ctr] = dateTo;
					ctr++;
					
				}        		        				    		    	   	    
				
				jbossQl.append("AND ti.tiIsArDocument=1 AND ti.tiTcCode IS NULL AND ti.tiWtcCode IS NOT NULL AND " + 
				"ti.tiAdCompany=" + AD_CMPNY +  " ORDER BY ti.tiTxnDate"); 
				
				Collection glTaxInterfaceDocs = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(),obj);
        		
				Iterator j = glTaxInterfaceDocs.iterator();  
				
				ArrayList apWtcList = new ArrayList();
				
				while(j.hasNext()) {	
					
					//	get available record per wtc code
					
					LocalGlTaxInterface glTaxInterfaceDoc = (LocalGlTaxInterface) j.next();
        			
        			if (apWtcList.contains(glTaxInterfaceDoc.getTiWtcCode())) {
            			
            			continue;
            			
            		}   
        			
        			apWtcList.add(glTaxInterfaceDoc.getTiWtcCode());
        			
        			jbossQl = new StringBuffer();
            		
            		jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti " + 
            					   "WHERE (ti.tiDocumentType='AR INVOICE' OR ti.tiDocumentType='AR CREDIT MEMO' " +
            					   "OR ti.tiDocumentType='AR RECEIPT' OR " +
            					   "ti.tiDocumentType='GL JOURNAL') ");
            		
            		ctr = 0;        		        		
            		criteriaSize = 0;            		
            		Integer wtcCode = glTaxInterfaceDoc.getTiWtcCode();
            		            		
            		if(dateFrom != null ) { 
            			criteriaSize++;        			
            		}
            		
            		if(dateTo != null) {
            			criteriaSize++;        		
            		}
            		
            		if(wtcCode != null) {
            			criteriaSize++;
            		}
            		
            		if(sbldgrCode != null) {
            			criteriaSize++;        		
            		}
            		
            		obj = new Object[criteriaSize];
            		
            		if(sbldgrCode != null) {
            			    		       		     
        			   jbossQl.append("AND ti.tiSlSubledgerCode = ?"  + (ctr+1) + " ");
        			   obj[ctr] = sbldgrCode;
    				   ctr++;
        			   
            		} else {
            			
            			jbossQl.append("AND ti.tiSlSubledgerCode IS NULL ");
            			
            		}
            		
            		if (dateFrom != null) {
            		  	    		  	    		    
        		  	   jbossQl.append("AND ti.tiTxnDate >=?" + (ctr+1) + " ");
            		   obj[ctr] = dateFrom;
            		   ctr++;
            		   
        		    }
            		
            		if(dateTo != null) {
            			    			  
           		  	   jbossQl.append("AND ti.tiTxnDate <=?" + (ctr+1) + " ");
               		   obj[ctr] = dateTo;
               		   ctr++;
               		   
            		}
            		
            		if(wtcCode != null) {
            			
        			   jbossQl.append("AND ti.tiWtcCode =?" + (ctr+1) + " ");
               		   obj[ctr] = (Integer) wtcCode;
               		   ctr++;
               		   
            		}
        	   	    
        		    jbossQl.append("AND ti.tiIsArDocument=1 AND ti.tiAdCompany=" + AD_CMPNY +  " ORDER BY ti.tiTxnDate");
        		    
        		    Collection glTaxInterfaceWtcs = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(),obj);        			    			        			        			        			   
    			    
    			    Iterator k = glTaxInterfaceWtcs.iterator();    			        			    
    			    
    				double WITHHOLDING_TAX = 0d;		    
    			    double TAX_BASE = 0d;
    			    
    			    while(k.hasNext()) {
    			    	
    			    	LocalGlTaxInterface glTaxInterfaceWtc = (LocalGlTaxInterface) k.next();
    			    	
    			    	if(glTaxInterfaceWtc.getTiDocumentType().equals("GL JOURNAL")) {
            				
            				LocalGlJournalLine glJournal = glJournalHome.findByPrimaryKey( 
            									glTaxInterfaceWtc.getTiTxlCode());
            				
            				if(glJournal.getJlDebit() == EJBCommon.TRUE) {
            						
            					WITHHOLDING_TAX += EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT);        					
            					TAX_BASE += EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT);
            					
            				} else {

            					WITHHOLDING_TAX -= EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT);        					
            					TAX_BASE -= EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT);
            					
            				}
            				
            			} else {
            				
    						LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.findByPrimaryKey(
    											glTaxInterfaceWtc.getTiTxlCode());
    						
    						if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    							WITHHOLDING_TAX += EJBCommon.roundIt(arDistributionRecord.getDrAmount(), PRECISION_UNIT);							
    							TAX_BASE += EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT);							
    						
    						} else {
    							
    							WITHHOLDING_TAX -= EJBCommon.roundIt(arDistributionRecord.getDrAmount(), PRECISION_UNIT);							
    							TAX_BASE -= EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT);							
    						
    						}												
    						
            			}
    			    	    			    	
    			    }
    			    
    			    LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.findByPrimaryKey(glTaxInterfaceDoc.getTiWtcCode());
			    	
			    	
			    	ArRepCreditableWithholdingTaxDetails details = new ArRepCreditableWithholdingTaxDetails();
				    details.setCwtSequenceNumber(++SEQUENCE_NUMBER);
				    details.setCwtCstTinNumber(glTaxInterfaceDoc.getTiSlTin());
				    details.setCwtCstCustomerName(glTaxInterfaceDoc.getTiSlName());
				    details.setCwtNatureOfIncomePayment(arWithholdingTaxCode.getWtcDescription());
				    details.setCwtAtcCode(arWithholdingTaxCode.getWtcName());					  
				    details.setCwtTaxBase(TAX_BASE);
				    details.setCwtRate(arWithholdingTaxCode.getWtcRate());
				    details.setCwtTaxWithheld(WITHHOLDING_TAX);
				    
				    list.add(details);
    			    
				}
									      			
			} 
			
			
			if (list.isEmpty()) {
    		  	 
    		  	  throw new GlobalNoRecordFoundException();
    		  	
    		}
		} catch (GlobalNoRecordFoundException ex) {
	   	  	 
        	Debug.printStackTrace(ex);
        	throw ex;
   	  	
        } catch (Exception ex) {
   	  	
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
   	  	
   	  	}
	        
	        return list;   
			
    }
           
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepCreditableWithholdingTaxControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	// private
   
   public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArInvoiceEntryControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }  
    
    
    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	  
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	} 

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepCreditableWithholdingTaxControllerBean ejbCreate");
      
    }
}
