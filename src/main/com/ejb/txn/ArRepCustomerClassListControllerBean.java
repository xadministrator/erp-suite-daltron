
/*
 * ArRepCustomerClassListControllerBean.java
 *
 * Created on March 02, 2005, 10:07 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArRepCustomerClassListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepCustomerClassListControllerEJB"
 *           display-name="Used for viewing customer class lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepCustomerClassListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepCustomerClassListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepCustomerClassListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepCustomerClassListControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepCustomerClassList(HashMap criteria, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepCustomerClassListControllerBean executeArRepCustomerClassList");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
         
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(cc) FROM ArCustomerClass cc ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("customerClassName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("customerClassName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("cc.ccName LIKE '%" + (String)criteria.get("customerClassName") + "%' ");
		  	 
		  }
		  
	      if (!firstArgument) {		       	  	
	   	     jbossQl.append("AND ");		       	     
	   	  } else {		       	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");		       	  	 
	   	  }	     
	   	  
	   	  jbossQl.append("cc.ccAdCompany=" + AD_CMPNY + " ");

		  String orderBy = null;
          
          if (ORDER_BY.equals("CUSTOMER CLASS NAME")) {
	      	 
	      	  orderBy = "cc.ccName";
	      	  
	      } 

		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }     
			  	      	
	      Collection arCustomerClasses = arCustomerClassHome.getCcByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arCustomerClasses.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arCustomerClasses.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalArCustomerClass arCustomerClass = (LocalArCustomerClass)i.next();   	  
		  	  
		  	  ArRepCustomerClassListDetails details = new ArRepCustomerClassListDetails();
		  	  details.setCclCcName(arCustomerClass.getCcName());
		  	  details.setCclCcDescription(arCustomerClass.getCcDescription());
		  	  details.setCclTaxName(arCustomerClass.getArTaxCode().getTcName());
		  	  details.setCclWithholdingTaxName(arCustomerClass.getArWithholdingTaxCode().getWtcName());
		  	  
		  	  LocalGlChartOfAccount glReceivableAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaReceivableAccount());
		  	  LocalGlChartOfAccount glRevenueAccount = null;
		  	  
		  	  if (arCustomerClass.getCcGlCoaRevenueAccount() != null) {
		  	  	
		  	      glRevenueAccount = glChartOfAccountHome.findByPrimaryKey(arCustomerClass.getCcGlCoaRevenueAccount());
		  	  	
		  	  }
		  	  
		  	  details.setCclReceivableAccountNumber(glReceivableAccount.getCoaAccountNumber());
		  	  details.setCclRevenueAccountNumber(glRevenueAccount != null ? glRevenueAccount.getCoaAccountNumber() : null);  
		  	  details.setCclReceivableAccountDescription(glReceivableAccount.getCoaAccountDescription());		
		  	  
		  	  if (glRevenueAccount != null) {
		  	  	
		  	  	details.setCclRevenueAccountDescription(glRevenueAccount.getCoaAccountDescription());
		  	  	
		  	  }
		  	  
		  	  details.setCclEnable(arCustomerClass.getCcEnable());

			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepCustomerClassListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepCustomerClassListControllerBean ejbCreate");
      
    }
}
