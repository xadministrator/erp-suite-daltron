
/*
 * ArRepCustomerListControllerBean.java
 *
 * Created on March 03, 2004, 9:29 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.ArDistributionRecordBean;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalance;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepCustomerListDetails;
import com.util.ArRepOrRegisterDetails;
import com.util.ArRepSalesRegisterDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepCustomerListControllerEJB"
 *           dicstay-name="Used for viewing customer lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepCustomerListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepCustomerListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepCustomerListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
 */

public class ArRepCustomerListControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArCcAll(Integer AD_CMPNY) {

		Debug.print("ArRepCustomerListControllerBean getArCcAll");

		LocalArCustomerClassHome arCustomerClassHome = null;
		LocalArCustomerClass arCustomerClass = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);

			Iterator i = arCustomerClasses.iterator();

			while (i.hasNext()) {

				arCustomerClass = (LocalArCustomerClass)i.next();

				list.add(arCustomerClass.getCcName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArCtAll(Integer AD_CMPNY) {

		Debug.print("ArRepCustomerListControllerBean getArCtAll");

		LocalArCustomerTypeHome arCustomerTypeHome = null;
		LocalArCustomerType arCustomerType = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

			Iterator i = arCustomerTypes.iterator();

			while (i.hasNext()) {

				arCustomerType = (LocalArCustomerType)i.next();

				list.add(arCustomerType.getCtName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/

	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{

		Debug.print("ArRepCustomerListControllerBean getAdBrResAll");

		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;

		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;

		Collection adBranchResponsibilities = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (adBranchResponsibilities.isEmpty()) {

			throw new GlobalNoRecordFoundException();

		}

		try {

			Iterator i = adBranchResponsibilities.iterator();

			while(i.hasNext()) {

				adBranchResponsibility = (LocalAdBranchResponsibility)i.next();

				adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());

				AdBranchDetails details = new AdBranchDetails();
				details.setBrCode(adBranch.getBrCode());
				details.setBrBranchCode(adBranch.getBrBranchCode());
				details.setBrName(adBranch.getBrName());
				details.setBrHeadQuarter(adBranch.getBrHeadQuarter());

				list.add(details);

			}

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		return list;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeArRepCustomerList(HashMap criteria, ArrayList branchList, String date, String salesPerson, String ORDER_BY, String GROUP_BY, boolean splitBySalesperson, boolean includedNegativeBalances, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {

		Debug.print("ArRepCustomerListControllerBean executeArRepCustomerList");

		LocalArCustomerHome arCustomerHome = null;
		LocalArCustomerBalanceHome arCustomerBalanceHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdCompany adCompany = null;

		ArrayList list = new ArrayList();

		//initialized EJB Home

		try {

			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
			arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) 	{

			throw new EJBException(ex.getMessage());

		}

		try { 

			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      

			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT DISTINCT OBJECT(cst) FROM ArCustomer cst, IN(cst.adBranchCustomers)bcst WHERE (");

			if (branchList.isEmpty())
				throw new GlobalNoRecordFoundException();

			Iterator brIter = branchList.iterator();

			AdBranchDetails brDetails = (AdBranchDetails)brIter.next();
			jbossQl.append("bcst.adBranch.brCode=" + brDetails.getBrCode());

			while(brIter.hasNext()) {

				brDetails = (AdBranchDetails)brIter.next();

				jbossQl.append(" OR bcst.adBranch.brCode=" + brDetails.getBrCode());

			}

			jbossQl.append(") ");
			firstArgument = false;

			Object obj[];	      

			// Allocate the size of the object parameter

			if (criteria.containsKey("customerCode")) {

				criteriaSize--;

			}

			if (criteria.containsKey("date")) {

				criteriaSize--;

			}

			if (criteria.containsKey("includeZeroes")) {

				criteriaSize--;

			}

			obj = new Object[criteriaSize];

			if (criteria.containsKey("customerCode")) {

				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

			}

			if (criteria.containsKey("customerType")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("cst.arCustomerType.ctName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("customerType");
				ctr++;

			}	

			if (criteria.containsKey("customerClass")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("cst.arCustomerClass.ccName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("customerClass");
				ctr++;

			}		

			/*if (criteria.containsKey("salesPerson")) {

		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }

		   	  jbossQl.append("cst.ArSalesperson=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerRegion");
		   	  ctr++;

	      }*/

			if (criteria.containsKey("customerRegion")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("cst.cstAdLvRegion=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("customerRegion");
				ctr++;

			}	


			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			jbossQl.append("cst.cstAdCompany=" + AD_CMPNY + " ");

			String orderBy = null;

			if (ORDER_BY.equals("CUSTOMER CODE")) {

				orderBy = "cst.cstCustomerCode";
			} else if (ORDER_BY.equals("CUSTOMER NAME")) {

				orderBy = "cst.cstName";

			} else if (ORDER_BY.equals("CUSTOMER TYPE")) {

				orderBy = "cst.arCustomerType.ctName";

			} else if (ORDER_BY.equals("CUSTOMER CLASS")) {

				orderBy = "cst.arCustomerClass.ccName";

			}	else if (ORDER_BY.equals("CUSTOMER REGION")) {

				orderBy = "cst.cstAdLvRegion";

			}

			if (GROUP_BY.equals("CUSTOMER CODE")) {

				orderBy = "cst.cstCustomerCode";
			}
			
			if (GROUP_BY.equals("SALESPERSON")) {

				orderBy = "cst.cstCustomerCode";
			}
			

			if (orderBy != null) {

				jbossQl.append("ORDER BY " + orderBy);

			}

			System.out.println(jbossQl.toString());
			
			  try {	
			 adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		
			  } catch (Exception ex) {
         	
            throw new EJBException(ex.getMessage());
        	
			  }	 

			Collection arCustomers = arCustomerHome.getCstByCriteria(jbossQl.toString(), obj);	         

			if (arCustomers.size() == 0)
				throw new GlobalNoRecordFoundException();

			Iterator i = arCustomers.iterator();
			int a=0;
		
			while (i.hasNext()) {

				LocalArCustomer arCustomer = (LocalArCustomer)i.next();   	  


				if (splitBySalesperson) {
					
					ArrayList salesPersonList = new ArrayList();
					HashMap hmSalesPerson = new HashMap();
					HashMap hmSalesPerson1 = new HashMap();
					
					
					

			Collection arInvoice2 = arInvoiceHome.findByInvNumberAndInvCreditMemoAndInvDateAndCustomerCodeOrderBySlp((byte) 0, (Date)criteria.get("date"), arCustomer.getCstCustomerCode().toString().trim(), AD_CMPNY);

					
					if(!arInvoice2.isEmpty()) {
						Iterator j = arInvoice2.iterator();
						
						while (j.hasNext()) {
							double balance = 0d;
							LocalArInvoice arInvoice = (LocalArInvoice)j.next();
							Date date1 = (Date)criteria.get("date");
							Date invDate = (Date)arInvoice.getInvDate();
							String invNum = arInvoice.getInvNumber().toString().trim();
							double miscAm = 0d;
							
							int results = date1.compareTo(invDate);
							ArRepCustomerListDetails details = new ArRepCustomerListDetails();
							if(results>=0){	
							
							
						
							if(arInvoice.getInvCreditMemo() == 0){
								
								double invoiceAmount = 0d;
								double cmAmount = 0d;
								double aiAmount = 0d;
								double aiAmountMisc = 0d;
								invoiceAmount = (double)arInvoice.getInvAmountDue();
																
										try{
												if (arInvoice.getArSalesperson().getSlpSalespersonCode().equals(null)) {
													details.setSalesPerson(null);
													}else {

													details.setSalesPerson(arInvoice.getArSalesperson().getSlpSalespersonCode());
													details.setSalesPersonCrt(salesPerson);
									
													}
											}catch(Exception e){

												details.setSalesPerson(null);			  					
											}
								
									//get CM

											Collection arInvoices1 = arCustomer.getArInvoices();
											Iterator j1 = arInvoices1.iterator();
											while (j1.hasNext()) {
												LocalArInvoice arInvoice1 = (LocalArInvoice)j1.next();
												Date invDate1 = (Date)arInvoice1.getInvDate();
												int results1 = date1.compareTo(invDate1);
												if(results1>=0){	
														if (arInvoice1.getInvCreditMemo() == 1){
													
														
															if(arInvoice1.getInvCmInvoiceNumber().toString().trim().equalsIgnoreCase(invNum)){
															cmAmount += (double)arInvoice1.getInvAmountDue();

															}
													}
												}
										}
											
											
											
								//get OR/Collection
											
										
										Collection arReceipts = arReceiptHome.findByInvCodeAndRctVoid(arInvoice.getInvCode(), EJBCommon.FALSE, AD_CMPNY);
										Iterator jj = arReceipts.iterator();
										while (jj.hasNext()) {
											LocalArReceipt arReceipt= (LocalArReceipt)jj.next();
											
											Date recDate = (Date)arReceipt.getRctDate();
											int results2 = date1.compareTo(recDate);
											if(results2>=0){	
													if(arReceipt.getRctType().equals("COLLECTION")){
														
														String arRctPk = arReceipt.getRctCode().toString().trim();
														
														Iterator iterAI = arReceipt.getArAppliedInvoices().iterator();
														String ipsInvCode = "";
														
														while(iterAI.hasNext()){
															LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)iterAI.next();
															ipsInvCode = (String)arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode().toString().trim();
															
															if(arInvoice.getInvCode().toString().trim().equals(ipsInvCode.toString().trim())){
																
															aiAmount += (double)arAppliedInvoice.getAiApplyAmount() + (double)arAppliedInvoice.getAiCreditableWTax() + (double)arAppliedInvoice.getAiDiscountAmount();

															}
															
														}
														
													
	
													}
												}
											
										}

										//
										//get Misc
										Collection arReceiptsMisc = arReceiptHome.findPostedRctByRctDateToAndCstCode((Date)criteria.get("date"), arCustomer.getCstCustomerCode(), AD_CMPNY);
										Iterator jjMisc = arReceiptsMisc.iterator();
										aiAmountMisc = 0;
										while (jjMisc.hasNext()) {
											
											LocalArReceipt arReceiptMisc= (LocalArReceipt)jjMisc.next();
											
											Date recDate1 = (Date)arReceiptMisc.getRctDate();
											int results3 = date1.compareTo(recDate1);
											if(results3>=0){
											if(arReceiptMisc.getRctType().equals("MISC")){
												try{
											
													if(arReceiptMisc.getArCustomer().getCstCustomerCode().toString().trim().equals(arCustomer.getCstCustomerCode().toString().trim()) && 
															arReceiptMisc.getArSalesperson().toString().trim().equals(arInvoice.getArSalesperson().toString().trim())){
																
															
															aiAmountMisc += (double)arReceiptMisc.getRctAmount();
														
													}
											}catch(Exception e){
												
											}
																
											}
											}
										}

											 invoiceAmount -= cmAmount;

											
											if(includedNegativeBalances){
												double balance1 = invoiceAmount - aiAmount;
												balance = EJBCommon.roundIt(balance1, adCompany.getGlFunctionalCurrency().getFcPrecision());
											    miscAm = aiAmountMisc;
											}else{
												double balance1 = invoiceAmount - aiAmount;
												balance = EJBCommon.roundIt(balance1, adCompany.getGlFunctionalCurrency().getFcPrecision());
											}							
										
								}
	
					}
							if(includedNegativeBalances){
		
									details.setClCstBalance(balance);
									details.setClCstCustomerCode(arCustomer.getCstCustomerCode());
									details.setClCstName(arCustomer.getCstName());		  	 		  	  
									details.setClCstContact(arCustomer.getCstContact());
									details.setClCstPhone(arCustomer.getCstPhone());
									details.setClCstTin(arCustomer.getCstTin());
									details.setClCstCreditLimit(arCustomer.getCstCreditLimit());
									details.setClCstAddress(arCustomer.getCstAddress());
									details.setClCstFax(arCustomer.getCstFax());
									details.setClCstPaymentTerm(arCustomer.getAdPaymentTerm().getPytName());
									details.setClCstType(arCustomer.getArCustomerType() == null ? "" : arCustomer.getArCustomerType().getCtName());
									details.setCustomerRegion(arCustomer.getCstAdLvRegion());
									details.setClCstDealPrice(arCustomer.getCstDealPrice());
									details.setIncludedNegativeBalances(true);
									details.setClRcptBalance(miscAm);
									details.setSalesPersonCrt(salesPerson);
								
								
								list.add(details); 
								balance = 0d;
								miscAm = 0d;
									
								
						}else{

								details.setClCstBalance(balance);
								details.setClCstCustomerCode(arCustomer.getCstCustomerCode());
								details.setClCstName(arCustomer.getCstName());		  	 		  	  
								details.setClCstContact(arCustomer.getCstContact());
								details.setClCstPhone(arCustomer.getCstPhone());
								details.setClCstTin(arCustomer.getCstTin());
								details.setClCstCreditLimit(arCustomer.getCstCreditLimit());
								details.setClCstAddress(arCustomer.getCstAddress());
								details.setClCstFax(arCustomer.getCstFax());
								details.setClCstPaymentTerm(arCustomer.getAdPaymentTerm().getPytName());
								details.setClCstType(arCustomer.getArCustomerType() == null ? "" : arCustomer.getArCustomerType().getCtName());
								details.setCustomerRegion(arCustomer.getCstAdLvRegion());
								details.setClCstDealPrice(arCustomer.getCstDealPrice());							
								details.setClRcptBalance(0);
								details.setSalesPersonCrt(salesPerson);
								
								
								list.add(details); 
								balance = 0d;
								miscAm = 0d;	
							
						}

						}
						
						//customer deposit
						
						Collection customerDep = arReceiptHome.findOpenDepositEnabledPostedRctByCstCustomerCodeOrderBySlp((Date)criteria.get("date"), arCustomer.getCstCustomerCode(), AD_CMPNY);
						
						Iterator jjCD = customerDep.iterator();
						
						double cstmrDpst = 0d;
					
					
						while (jjCD.hasNext()) {
							
							LocalArReceipt cstDep= (LocalArReceipt)jjCD.next();
							Collection arInvoice3 = arInvoiceHome.findByInvNumberAndInvCreditMemoAndInvDateAndCustomerCodeSlpCodeOrderBySlp((byte) 0, (Date)criteria.get("date"), arCustomer.getCstCustomerCode().toString().trim(), cstDep.getArSalesperson().getSlpSalespersonCode().toString().trim(), AD_CMPNY);
							
							if(arInvoice3.isEmpty()){
								if(includedNegativeBalances){
								ArRepCustomerListDetails details = new ArRepCustomerListDetails();
								
								details.setClCstBalance(0);
								details.setClCstCustomerCode(arCustomer.getCstCustomerCode());
								details.setClCstName(arCustomer.getCstName());		  	 		  	  
								details.setClCstContact(arCustomer.getCstContact());
								details.setClCstPhone(arCustomer.getCstPhone());
								details.setClCstTin(arCustomer.getCstTin());
								details.setClCstCreditLimit(arCustomer.getCstCreditLimit());
								details.setClCstAddress(arCustomer.getCstAddress());
								details.setClCstFax(arCustomer.getCstFax());
								details.setClCstPaymentTerm(arCustomer.getAdPaymentTerm().getPytName());
								details.setClCstType(arCustomer.getArCustomerType() == null ? "" : arCustomer.getArCustomerType().getCtName());
								details.setCustomerRegion(arCustomer.getCstAdLvRegion());
								details.setClCstDealPrice(arCustomer.getCstDealPrice());							
								details.setClRcptBalance(cstDep.getRctAmount());
								details.setSalesPerson(cstDep.getArSalesperson().getSlpSalespersonCode().toString().trim());
								details.setSalesPersonCrt(salesPerson);
								
								list.add(details); 
								
								}
								
							}
						}
						
						//end customer deposit
					}//if customr has no invoice
					else{
						if(includedNegativeBalances){
						//get Misc
						Collection arReceiptsMisc = arReceiptHome.findPostedRctByRctDateToAndCstCode((Date)criteria.get("date"), arCustomer.getCstCustomerCode(), AD_CMPNY);
						Iterator jjMisc = arReceiptsMisc.iterator();
						double aiAmountMisc1 = 0d;
						String salespersonMisc = ""; 
						while (jjMisc.hasNext()) {
							
							LocalArReceipt arReceiptMisc= (LocalArReceipt)jjMisc.next();
						
						
							if(arReceiptMisc.getRctType().equals("MISC")){
								try{
									salespersonMisc = arReceiptMisc.getArSalesperson().getSlpSalespersonCode().toString().trim();
									if(arReceiptMisc.getArCustomer().getCstCustomerCode().toString().trim().equals(arCustomer.getCstCustomerCode().toString().trim())){

										aiAmountMisc1 += (double)arReceiptMisc.getRctAmount();
										
									}
							}catch(Exception e){
								
							}
												
							}
							
						}
						ArRepCustomerListDetails details = new ArRepCustomerListDetails();
						
						details.setSalesPerson(salespersonMisc);	
						details.setClCstBalance(0);
						details.setClCstCustomerCode(arCustomer.getCstCustomerCode());
						details.setClCstName(arCustomer.getCstName());		  	 		  	  
						details.setClCstContact(arCustomer.getCstContact());
						details.setClCstPhone(arCustomer.getCstPhone());
						details.setClCstTin(arCustomer.getCstTin());
						details.setClCstCreditLimit(arCustomer.getCstCreditLimit());
						details.setClCstAddress(arCustomer.getCstAddress());
						details.setClCstFax(arCustomer.getCstFax());
						details.setClCstPaymentTerm(arCustomer.getAdPaymentTerm().getPytName());
						details.setClCstType(arCustomer.getArCustomerType() == null ? "" : arCustomer.getArCustomerType().getCtName());
						details.setCustomerRegion(arCustomer.getCstAdLvRegion());
						details.setClCstDealPrice(arCustomer.getCstDealPrice());							
						details.setClRcptBalance(aiAmountMisc1);
						details.setIncludedNegativeBalances(true);
						details.setSalesPersonCrt(salesPerson);
						
						list.add(details); 
						
					}
					}
					
				}  else {
					
					
					ArrayList salesPersonList = new ArrayList();
					HashMap hmSalesPerson = new HashMap();
					HashMap hmSalesPerson1 = new HashMap();
					
					//Collection  arInvoiceNull = arCustomer.getArInvoices();
					

			Collection arInvoice2 = arInvoiceHome.findByInvNumberAndInvCreditMemoAndInvDateAndCustomerCode((byte) 0, (Date)criteria.get("date"), arCustomer.getCstCustomerCode().toString().trim(), AD_CMPNY);
					
					if(!arInvoice2.isEmpty()) {
						Iterator j = arInvoice2.iterator();
						ArRepCustomerListDetails details = new ArRepCustomerListDetails();
						double balance = 0d;
						double miscAm = 0d;
						double invoiceAmount = 0d;
						double cmAmount = 0d;
						double aiAmount = 0d;
						double aiAmountMisc = 0d;
						while (j.hasNext()) {
							
							LocalArInvoice arInvoice = (LocalArInvoice)j.next();
							
							Date date1 = (Date)criteria.get("date");
							Date invDate = (Date)arInvoice.getInvDate();
							String invNum = arInvoice.getInvNumber().toString().trim();
							
							
							int results = date1.compareTo(invDate);
							
							if(results>=0){	

							if(arInvoice.getInvCreditMemo() == 0){
								
							
								invoiceAmount += (double)arInvoice.getInvAmountDue();
							
					
									
										try{
												if (arInvoice.getArSalesperson().getSlpSalespersonCode().equals(null)) {
													details.setSalesPerson(null);
													}else {

													details.setSalesPerson(arInvoice.getArSalesperson().getSlpSalespersonCode());
									
													}
											}catch(Exception e){

												details.setSalesPerson(null);			  					
											}
								
									//get CM

											Collection arInvoices1 = arCustomer.getArInvoices();
											Iterator j1 = arInvoices1.iterator();
											while (j1.hasNext()) {
												LocalArInvoice arInvoice1 = (LocalArInvoice)j1.next();
												Date invDate1 = (Date)arInvoice1.getInvDate();
												int results1 = date1.compareTo(invDate1);
												if(results1>=0){	
														if (arInvoice1.getInvCreditMemo() == 1){

															if(arInvoice1.getInvCmInvoiceNumber().toString().trim().equalsIgnoreCase(invNum)){
															cmAmount += (double)arInvoice1.getInvAmountDue();

															}
													}
												}
										}

								//get OR/Collection

										Collection arReceipts = arReceiptHome.findByInvCodeAndRctVoid(arInvoice.getInvCode(), EJBCommon.FALSE, AD_CMPNY);
										Iterator jj = arReceipts.iterator();
										while (jj.hasNext()) {
											LocalArReceipt arReceipt= (LocalArReceipt)jj.next();
											
											Date recDate = (Date)arReceipt.getRctDate();
											int results2 = date1.compareTo(recDate);
											if(results2>=0){	
													if(arReceipt.getRctType().equals("COLLECTION")){
														
														String arRctPk = arReceipt.getRctCode().toString().trim();
														
														Iterator iterAI = arReceipt.getArAppliedInvoices().iterator();
														String ipsInvCode = "";
														
														while(iterAI.hasNext()){
															LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)iterAI.next();
															ipsInvCode = (String)arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode().toString().trim();
															
															if(arInvoice.getInvCode().toString().trim().equals(ipsInvCode.toString().trim())){
																
															aiAmount += (double)arAppliedInvoice.getAiApplyAmount() + (double)arAppliedInvoice.getAiCreditableWTax() + (double)arAppliedInvoice.getAiDiscountAmount();

															}

														}

													}
												}
											
										}

										//
										//get Misc
										Collection arReceiptsMisc = arReceiptHome.findPostedRctByRctDateToAndCstCode((Date)criteria.get("date"), arCustomer.getCstCustomerCode(), AD_CMPNY);
										Iterator jjMisc = arReceiptsMisc.iterator();
										aiAmountMisc = 0d;
										while (jjMisc.hasNext()) {
											
											LocalArReceipt arReceiptMisc= (LocalArReceipt)jjMisc.next();
											
											Date recDate1 = (Date)arReceiptMisc.getRctDate();
											int results3 = date1.compareTo(recDate1);
											if(results3>=0){
											if(arReceiptMisc.getRctType().equals("MISC")){
												try{

															aiAmountMisc += (double)arReceiptMisc.getRctAmount();

											}catch(Exception e){
												
											}
																
											}
											}
										}

								

									}//endwhile all inv customer
						
												
							
					}

		}
						
						
						 invoiceAmount -= cmAmount;

							
							if(includedNegativeBalances){
								
								double balance1 = invoiceAmount - aiAmount;
								balance = EJBCommon.roundIt(balance1, adCompany.getGlFunctionalCurrency().getFcPrecision());
							    miscAm = aiAmountMisc;
							   
							}else{
								double balance1 = invoiceAmount - aiAmount;
								balance = EJBCommon.roundIt(balance1, adCompany.getGlFunctionalCurrency().getFcPrecision());
							}		
							
				if(includedNegativeBalances){

							details.setClCstBalance(balance);
							details.setClCstCustomerCode(arCustomer.getCstCustomerCode());
							details.setClCstName(arCustomer.getCstName());		  	 		  	  
							details.setClCstContact(arCustomer.getCstContact());
							details.setClCstPhone(arCustomer.getCstPhone());
							details.setClCstTin(arCustomer.getCstTin());
							details.setClCstCreditLimit(arCustomer.getCstCreditLimit());
							details.setClCstAddress(arCustomer.getCstAddress());
							details.setClCstFax(arCustomer.getCstFax());
							details.setClCstPaymentTerm(arCustomer.getAdPaymentTerm().getPytName());
							details.setClCstType(arCustomer.getArCustomerType() == null ? "" : arCustomer.getArCustomerType().getCtName());
							details.setCustomerRegion(arCustomer.getCstAdLvRegion());
							details.setClCstDealPrice(arCustomer.getCstDealPrice());
							details.setIncludedNegativeBalances(true);
							details.setClRcptBalance(miscAm);
							details.setSalesPersonCrt(salesPerson);
						
						
						list.add(details); 
						balance = 0d;
						miscAm = 0d;
				
			}else{
			
				
					details.setClCstBalance(balance);
					details.setClCstCustomerCode(arCustomer.getCstCustomerCode());
					details.setClCstName(arCustomer.getCstName());		  	 		  	  
					details.setClCstContact(arCustomer.getCstContact());
					details.setClCstPhone(arCustomer.getCstPhone());
					details.setClCstTin(arCustomer.getCstTin());
					details.setClCstCreditLimit(arCustomer.getCstCreditLimit());
					details.setClCstAddress(arCustomer.getCstAddress());
					details.setClCstFax(arCustomer.getCstFax());
					details.setClCstPaymentTerm(arCustomer.getAdPaymentTerm().getPytName());
					details.setClCstType(arCustomer.getArCustomerType() == null ? "" : arCustomer.getArCustomerType().getCtName());
					details.setCustomerRegion(arCustomer.getCstAdLvRegion());
					details.setClCstDealPrice(arCustomer.getCstDealPrice());								
					details.setClRcptBalance(0);
					details.setSalesPersonCrt(salesPerson);
					
					list.add(details); 
					balance = 0d;	
				
			}
		}//no invoice
		else{
			if(includedNegativeBalances){
			//get Misc
			Collection arReceiptsMisc = arReceiptHome.findPostedRctByRctDateToAndCstCode((Date)criteria.get("date"), arCustomer.getCstCustomerCode(), AD_CMPNY);
			Iterator jjMisc = arReceiptsMisc.iterator();
			double aiAmountMisc1 = 0d;
			String salespersonMisc = ""; 
			while (jjMisc.hasNext()) {
				
				LocalArReceipt arReceiptMisc= (LocalArReceipt)jjMisc.next();
				
			
				if(arReceiptMisc.getRctType().equals("MISC")){
					try{
						salespersonMisc = arReceiptMisc.getArSalesperson().getSlpSalespersonCode().toString().trim();
						if(arReceiptMisc.getArCustomer().getCstCustomerCode().toString().trim().equals(arCustomer.getCstCustomerCode().toString().trim())){
									
								
							aiAmountMisc1 += (double)arReceiptMisc.getRctAmount();
							
						}
				}catch(Exception e){
					
				}
									
				}
				
			}
			ArRepCustomerListDetails details = new ArRepCustomerListDetails();
			
			details.setSalesPerson(salespersonMisc);	
			details.setClCstBalance(0);
			details.setClCstCustomerCode(arCustomer.getCstCustomerCode());
			details.setClCstName(arCustomer.getCstName());		  	 		  	  
			details.setClCstContact(arCustomer.getCstContact());
			details.setClCstPhone(arCustomer.getCstPhone());
			details.setClCstTin(arCustomer.getCstTin());
			details.setClCstCreditLimit(arCustomer.getCstCreditLimit());
			details.setClCstAddress(arCustomer.getCstAddress());
			details.setClCstFax(arCustomer.getCstFax());
			details.setClCstPaymentTerm(arCustomer.getAdPaymentTerm().getPytName());
			details.setClCstType(arCustomer.getArCustomerType() == null ? "" : arCustomer.getArCustomerType().getCtName());
			details.setCustomerRegion(arCustomer.getCstAdLvRegion());
			details.setClCstDealPrice(arCustomer.getCstDealPrice());							
			details.setClRcptBalance(aiAmountMisc1);
			details.setIncludedNegativeBalances(true);
			details.setSalesPersonCrt(salesPerson);
			
			list.add(details); 
			
		
		}
			
		}		
				
	}
			
}
		
			if(GROUP_BY.equals("SALESPERSON")) {

				Collections.sort(list, ArRepCustomerListDetails.CustomerCodeComparator);

			} 
			//if(GROUP_BY.equals("SALESPERSON")) {
			
			//	Collections.sort(list, ArRepCustomerListDetails.CustomerCodeComparator);
			//	}
				//else {

				//Collections.sort(list, ArRepCustomerListDetails.NoGroupComparator);

			//}
			System.out.println(list.isEmpty()); 
			
			
			return list;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		}


		catch (Exception ex) {


			ex.printStackTrace();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

		Debug.print("ArRepCustomerListControllerBean getAdCompany");      

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());

			return details;  	

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}    

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ArRepCustomerListControllerBean ejbCreate");

	}
}
