
/*
 * ArRepCustomerTypeListControllerBean.java
 *
 * Created on March 03, 2005, 04:44 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArRepCustomerTypeListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepCustomerTypeListControllerEJB"
 *           display-name="Used for viewing customer type lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepCustomerTypeListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepCustomerTypeListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepCustomerTypeListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepCustomerTypeListControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepCustomerTypeList(HashMap criteria, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepCustomerTypeListControllerBean executeArRepCustomerTypeList");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(ct) FROM ArCustomerType ct ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("customerTypeName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("customerTypeName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("ct.ctName LIKE '%" + (String)criteria.get("customerTypeName") + "%' ");
		  	 
		  }
		  
	      if (!firstArgument) {		       	  	
	   	     jbossQl.append("AND ");		       	     
	   	  } else {		       	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");		       	  	 
	   	  }	     
	   	  
	   	  jbossQl.append("ct.ctAdCompany=" + AD_CMPNY + " ");

		  String orderBy = null;
          
          if (ORDER_BY.equals("CUSTOMER TYPE NAME")) {
	      	 
	      	  orderBy = "ct.ctName";
	      	  
	      } 

		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }     
			  	      	
	      Collection arCustomerTypes = arCustomerTypeHome.getCtByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arCustomerTypes.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arCustomerTypes.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalArCustomerType arCustomerType = (LocalArCustomerType)i.next();   	  
		  	  
		  	  ArRepCustomerTypeListDetails details = new ArRepCustomerTypeListDetails();
		  	  details.setCtlCtName(arCustomerType.getCtName());
		  	  details.setCtlCtDescription(arCustomerType.getCtDescription());
		  	  details.setCtlBankAccount(arCustomerType.getAdBankAccount().getBaName());
		  	  details.setCtlEnable(arCustomerType.getCtEnable());

			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepCustomerTypeListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepCustomerTypeListControllerBean ejbCreate");
      
    }
}
