
/*
 * ArDeliveryPrintControllerBean.java
 *
 * Created on June 7, 2019, 5:10 P
 *
 * @author  Ruben P. Lamberte
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApAppliedVoucherHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ar.LocalArDelivery;
import com.ejb.ar.LocalArDeliveryHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArJobOrderInvoiceLine;
import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvTag;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArModSalesOrderDetails;
import com.util.ArRepDeliveryPrintDetails;
import com.util.ArRepDeliveryPrintDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepDeliveryPrintControllerEJB"
 *           display-name="Used for printing delivery transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepDeliveryPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepDeliveryPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepDeliveryPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
*/

public class ArRepDeliveryPrintControllerBean extends AbstractSessionBean {


	 /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeArRepDeliveryPrintAll(Integer SO_CODE, Integer AD_CMPNY) {

    	 Debug.print("ArDeliveryPrintControllerBean executeArDeliveryPrint");

    	 LocalArSalesOrderHome arSalesOrderHone = null;
    	 LocalArDeliveryHome arDeliveryHome = null;
    	

         ArrayList list = new ArrayList();

      // Initialize EJB Home

         try {

        	 arSalesOrderHone = (LocalArSalesOrderHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
        	 arDeliveryHome = (LocalArDeliveryHome)EJBHomeFactory.
               lookUpLocalHome(LocalArDeliveryHome.JNDI_NAME, LocalArDeliveryHome.class);
            

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }



         try {

        
            Collection arDeliveries = arDeliveryHome.findBySoCode(SO_CODE);
        	 
        	 
		    Iterator i = arDeliveries.iterator();


    		while(i.hasNext()) {

        		 
    			LocalArDelivery arDelivery = (LocalArDelivery)i.next();
    			
	        	ArRepDeliveryPrintDetails details = new ArRepDeliveryPrintDetails();

	        	
	        	
				details.setDpDvCode(arDelivery.getDvCode().toString());
				details.setDpDvContainer(arDelivery.getDvContainer());
				details.setDpDvDeliveryNumber(arDelivery.getDvDeliveryNumber());
				details.setDpDvBookingTime(arDelivery.getDvBookingTime());
				details.setDpDvTabsFeePullOut(arDelivery.getDvTabsFeePullOut());
				details.setDpDvReleasedDate(arDelivery.getDvReleasedDate());
				details.setDpDvDeliveredDate(arDelivery.getDvDeliveredDate());
				details.setDpDvDeliveredTo(arDelivery.getDvDeliveredTo());
				details.setDpDvPlateNo(arDelivery.getDvPlateNo());
				details.setDpDvDriverName(arDelivery.getDvDriverName());
				details.setDpDvEmptyReturnDate(arDelivery.getDvEmptyReturnDate());
				details.setDpDvEmptyReturnTo(arDelivery.getDvEmptyReturnTo());
				details.setDpDvTabsFeeReturn(arDelivery.getDvTabsFeeReturn());
				details.setDpDvStatus(arDelivery.getDvStatus());
				details.setDpDvRemarks(arDelivery.getDvRemarks());
				
				details.setDpCstCustomerCode(arDelivery.getArSalesOrder().getArCustomer().getCstCustomerCode());
				details.setDpSoDocumentNumber(arDelivery.getArSalesOrder().getSoDocumentNumber());
				details.setDpSoReferenceNumber(arDelivery.getArSalesOrder().getSoReferenceNumber());
				
	        	
	        	list.add(details);
	        	
		        
    		}

        		return list;
         } catch (Exception ex) {

         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());

         }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeArRepDeliveryByDvCode(Integer DV_CODE, Integer AD_CMPNY) {

    	 Debug.print("ArDeliveryPrintControllerBean executeArDeliveryPrint");

    	 LocalArSalesOrderHome arSalesOrderHone = null;
    	 LocalArDeliveryHome arDeliveryHome = null;
    	

         ArrayList list = new ArrayList();

      // Initialize EJB Home

         try {

        	 arSalesOrderHone = (LocalArSalesOrderHome)EJBHomeFactory.
                 lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
        	 arDeliveryHome = (LocalArDeliveryHome)EJBHomeFactory.
               lookUpLocalHome(LocalArDeliveryHome.JNDI_NAME, LocalArDeliveryHome.class);
            

         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }



         try {

        
        	 LocalArDelivery arDelivery = arDeliveryHome.findByPrimaryKey(DV_CODE);
        	 
        	 
        	 ArRepDeliveryPrintDetails details = new ArRepDeliveryPrintDetails();

	        	
	        	
				details.setDpDvCode(arDelivery.getDvCode().toString());
				details.setDpDvContainer(arDelivery.getDvContainer());
				details.setDpDvDeliveryNumber(arDelivery.getDvDeliveryNumber());
				details.setDpDvBookingTime(arDelivery.getDvBookingTime());
				details.setDpDvTabsFeePullOut(arDelivery.getDvTabsFeePullOut());
				details.setDpDvReleasedDate(arDelivery.getDvReleasedDate());
				details.setDpDvDeliveredDate(arDelivery.getDvDeliveredDate());
				details.setDpDvDeliveredTo(arDelivery.getDvDeliveredTo());
				details.setDpDvPlateNo(arDelivery.getDvPlateNo());
				details.setDpDvDriverName(arDelivery.getDvDriverName());
				details.setDpDvEmptyReturnDate(arDelivery.getDvEmptyReturnDate());
				details.setDpDvEmptyReturnTo(arDelivery.getDvEmptyReturnTo());
				details.setDpDvTabsFeeReturn(arDelivery.getDvTabsFeeReturn());
				details.setDpDvStatus(arDelivery.getDvStatus());
				details.setDpDvRemarks(arDelivery.getDvRemarks());
				
				details.setDpCstCustomerCode(arDelivery.getArSalesOrder().getArCustomer().getCstCustomerCode());
				details.setDpSoDocumentNumber(arDelivery.getArSalesOrder().getSoDocumentNumber());
				details.setDpSoReferenceNumber(arDelivery.getArSalesOrder().getSoReferenceNumber());
				
				list.add(details);
        		return list;
         } catch (Exception ex) {

         	Debug.printStackTrace(ex);
         	throw new EJBException(ex.getMessage());

         }

    }

    
    /**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.ArModSalesOrderDetails getArSalesOrderBySoCode(Integer SO_CODE) {

	  Debug.print("ArDeliveryPrintControllerBean getArSalesOrderBySoCode");

	  LocalArSalesOrderHome arSalesOrderHome = null;

	  // Initialize EJB Home

	  try {

		  arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
	           lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

	  try {

	     LocalArSalesOrder arSalesOrder = arSalesOrderHome.findByPrimaryKey(SO_CODE);

	     ArModSalesOrderDetails details = new ArModSalesOrderDetails();
	     System.out.println("doc num:" + arSalesOrder.getSoDocumentNumber());
	     details.setSoCode(arSalesOrder.getSoCode());
	     details.setSoDocumentNumber(arSalesOrder.getSoDocumentNumber());
	     details.setSoReferenceNumber(arSalesOrder.getSoReferenceNumber());
	     details.setSoDate(arSalesOrder.getSoDate());
	     details.setSoTransactionStatus(arSalesOrder.getSoTransactionStatus());
	     details.setSoCstCustomerCode(arSalesOrder.getArCustomer().getCstCustomerCode());
	     
	     return details;

	  } catch (Exception ex) {

	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());

	  }

	}
	
	
    /**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

	  Debug.print("ArDeliveryPrintControllerBean getAdCompany");

	  LocalAdCompanyHome adCompanyHome = null;

	  // Initialize EJB Home

	  try {

	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

	  try {

	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpTin(adCompany.getCmpTin());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     details.setCmpFax(adCompany.getCmpFax());


	     return details;

	  } catch (Exception ex) {

	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());

	  }

	}
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArDeliveryPrintControllerBean ejbCreate");

    }
}