package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArSalesOrderLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepDeliveryReceiptDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;


/**
 * @ejb:bean name="ArRepDeliveryReceiptControllerEJB"
 *           display-name="Used for finding ar delivery receipt"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepDeliveryReceiptControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepDeliveryReceiptController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepDeliveryReceiptControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepDeliveryReceiptControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeArRepDeliveryReceipt(HashMap criteria, String ORDER_BY, String GROUP_BY, boolean SHOW_LN_ITEMS, ArrayList adBrnchList, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("ArRepDeliveryReceiptControllerBean executeArRepDeliveryReceipt");
		
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalArSalesOrderHome arSalesOrderHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
		LocalArSalesOrderLineHome arSalesOrderLineHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			arSalesOrderLineHome = (LocalArSalesOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			String tempDocNum = null;
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("includedMiscReceipts")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(ili) FROM ArInvoiceLineItem ili ");
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("ili.arInvoice.invDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("ili.arInvoice.invDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}    
			
			
			if (((String)criteria.get("includedUnposted")).equals("NO")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				jbossQl.append("ili.arInvoice.invPosted = 1 ");
				
			} 
			
			if(adBrnchList.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			} else {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	      
				
				jbossQl.append("ili.arInvoice.invAdBranch in (");
				
				boolean firstLoop = true;
				
				Iterator i = adBrnchList.iterator();
				
				while(i.hasNext()) {
					
					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}
					
					AdBranchDetails mdetails = (AdBranchDetails) i.next();
					
					jbossQl.append(mdetails.getBrCode());
					
				}
				
				jbossQl.append(") ");
				
				firstArgument = false;
				
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("ili.arInvoice.invAdCompany = " + AD_CMPNY + " ");
			
			String orderBy = null;
			
			if (ORDER_BY.equals("DATE")) {
				
				orderBy = "ili.arInvoice.invDate";
				
			} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
				
				orderBy = "ili.arInvoice.invNumber";
				
			} 
			
			if (orderBy != null) {
				
				jbossQl.append("ORDER BY " + orderBy);
				
			} 
			
			Collection arInvoiceLineItems = null;
			
			try {
				
				arInvoiceLineItems = arInvoiceLineItemHome.getIliByCriteria(jbossQl.toString(), obj);
				
			} catch (FinderException ex)	{
				
			} 	
			
			Iterator i = arInvoiceLineItems.iterator();
			
			while (i.hasNext()) {
				
				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();   
				
				if (SHOW_LN_ITEMS) {
					
					double AMOUNT = 0d;
					
					ArRepDeliveryReceiptDetails details = new ArRepDeliveryReceiptDetails();
					
					details.setDrCustomerName(arInvoiceLineItem.getArInvoice().getArCustomer().getCstName());
					details.setDrCustomerCode(arInvoiceLineItem.getArInvoice().getArCustomer().getCstCustomerCode());
					details.setDrDate(arInvoiceLineItem.getArInvoice().getInvDate());
					details.setDrDocumentNumber(arInvoiceLineItem.getArInvoice().getInvNumber());
					details.setDrReferenceNumber(arInvoiceLineItem.getArInvoice().getInvReferenceNumber());
					details.setDrDescription(arInvoiceLineItem.getArInvoice().getInvDescription());
					
					details.setDrItemName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
					details.setDrLocation(arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName());
					details.setDrQuantity(arInvoiceLineItem.getIliQuantity());
					details.setDrUnit(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
					details.setDrUnitPrice(arInvoiceLineItem.getIliUnitPrice());
					
					AMOUNT = EJBCommon.roundIt(arInvoiceLineItem.getIliQuantity() * arInvoiceLineItem.getIliUnitPrice(),  this.getGlFcPrecisionUnit(AD_CMPNY));
					
					details.setDrAmount(AMOUNT);
					
					details.setOrderBy(ORDER_BY);
					
					list.add(details);
					
				} else {
					
					if(tempDocNum == null || !tempDocNum.equals(arInvoiceLineItem.getArInvoice().getInvNumber())) {
						
						tempDocNum = arInvoiceLineItem.getArInvoice().getInvNumber();
						
						ArRepDeliveryReceiptDetails details = new ArRepDeliveryReceiptDetails();
						
						details.setDrCustomerName(arInvoiceLineItem.getArInvoice().getArCustomer().getCstName());
						details.setDrCustomerCode(arInvoiceLineItem.getArInvoice().getArCustomer().getCstCustomerCode());
						details.setDrDate(arInvoiceLineItem.getArInvoice().getInvDate());
						details.setDrDocumentNumber(arInvoiceLineItem.getArInvoice().getInvNumber());
						details.setDrReferenceNumber(arInvoiceLineItem.getArInvoice().getInvReferenceNumber());
						details.setDrDescription(arInvoiceLineItem.getArInvoice().getInvDescription());
						details.setDrAmount(arInvoiceLineItem.getArInvoice().getInvAmountDue());
						details.setOrderBy(ORDER_BY);
						
						list.add(details);
						
					}
				}
			}
			
			// Sales Order
			
			jbossQl = new StringBuffer();
			
			jbossQl.append("SELECT OBJECT(sil) FROM ArSalesOrderInvoiceLine sil ");
			
			firstArgument = true;
			ctr = 0;
			
			// Allocate the size of the object parameter
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("sil.arSalesOrderLine.arSalesOrder.soDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("sil.arSalesOrderLine.arSalesOrder.soDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}    
			
			if (((String)criteria.get("includedUnposted")).equals("NO")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				jbossQl.append("sil.arSalesOrderLine.arSalesOrder.soPosted = 1 ");
				
			} 
			
			if(adBrnchList.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			} else {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	      
				
				jbossQl.append("sil.arSalesOrderLine.arSalesOrder.soAdBranch in (");
				
				boolean firstLoop = true;
				
				Iterator j = adBrnchList.iterator();
				
				while(j.hasNext()) {
					
					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}
					
					AdBranchDetails mdetails = (AdBranchDetails) j.next();
					
					jbossQl.append(mdetails.getBrCode());
					
				}
				
				jbossQl.append(") ");
				
				firstArgument = false;
				
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("sil.silAdCompany = " + AD_CMPNY + " ");
			
			orderBy = null;
			
			if (ORDER_BY.equals("DATE")) {
				
				orderBy = "sil.arSalesOrderLine.arSalesOrder.soDate";
				
			} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
				
				orderBy = "sil.arSalesOrderLine.arSalesOrder.soDocumentNumber";
				
			} 
			
			if (orderBy != null) {
				
				jbossQl.append("ORDER BY " + orderBy);
				
			} 
			
			Collection arSalesOrderInvoiceLines = null;
			
			try {
				
				arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome.getSalesOrderInvoiceLineByCriteria(jbossQl.toString(), obj);
				
			} catch (FinderException ex)	{
				
			} 
			
			i = arSalesOrderInvoiceLines.iterator();
			
			while (i.hasNext()) {
				
				LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)i.next();
				
				if (SHOW_LN_ITEMS) {
					
					double AMOUNT = 0d;
					
					ArRepDeliveryReceiptDetails details = new ArRepDeliveryReceiptDetails();
					
					details.setDrCustomerName(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getArCustomer().getCstName());
					details.setDrCustomerCode(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getArCustomer().getCstCustomerCode());
					details.setDrDate(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoDate());
					details.setDrDocumentNumber(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoDocumentNumber());
					details.setDrReferenceNumber(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoReferenceNumber());
					details.setDrDescription(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoDescription());
					
					details.setDrItemName(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiName());
					details.setDrLocation(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvLocation().getLocName());
					details.setDrQuantity(arSalesOrderInvoiceLine.getSilQuantityDelivered());
					details.setDrUnit(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
					details.setDrUnitPrice(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice());
					
					AMOUNT = EJBCommon.roundIt(arSalesOrderInvoiceLine.getSilQuantityDelivered() * arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice(),  this.getGlFcPrecisionUnit(AD_CMPNY));
					
					details.setDrAmount(AMOUNT);
					details.setOrderBy(ORDER_BY);
					
					list.add(details);
					
				} else {
					
					if(tempDocNum == null || !tempDocNum.equals(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoDocumentNumber())) {
						
						tempDocNum = arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoDocumentNumber();
						
						ArRepDeliveryReceiptDetails details = new ArRepDeliveryReceiptDetails();
						
						details.setDrCustomerName(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getArCustomer().getCstName());
						details.setDrCustomerCode(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getArCustomer().getCstCustomerCode());
						details.setDrDate(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoDate());
						details.setDrDocumentNumber(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoDocumentNumber());
						details.setDrReferenceNumber(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoReferenceNumber());
						details.setDrDescription(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoDescription());
						details.setDrAmount(arSalesOrderInvoiceLine.getArInvoice().getInvAmountDue());
						details.setOrderBy(ORDER_BY);
						
						list.add(details);
						
					}
					
				}
			}
			
			if(((String)criteria.get("includedMiscReceipts")).equals("YES")) {
				
				// misc receipt
				
				jbossQl = new StringBuffer();
				firstArgument = true;
				ctr = 0;
				
				obj = new Object[criteriaSize];
				
				jbossQl.append("SELECT OBJECT(ili) FROM ArInvoiceLineItem ili ");
				
				if (criteria.containsKey("dateFrom")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("ili.arReceipt.rctDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
				}  
				
				if (criteria.containsKey("dateTo")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("ili.arReceipt.rctDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;
					
				}    
				
				
				if (((String)criteria.get("includedUnposted")).equals("NO")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}	       	  
					
					jbossQl.append("ili.arReceipt.rctPosted = 1 ");
					
				} 
				
				if(adBrnchList.isEmpty()) {
					
					throw new GlobalNoRecordFoundException();
					
				} else {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}	      
					
					jbossQl.append("ili.arReceipt.rctAdBranch in (");
					
					boolean firstLoop = true;
					
					i = adBrnchList.iterator();
					
					while(i.hasNext()) {
						
						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}
						
						AdBranchDetails mdetails = (AdBranchDetails) i.next();
						
						jbossQl.append(mdetails.getBrCode());
						
					}
					
					jbossQl.append(") ");
					
					firstArgument = false;
					
				}
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("ili.arReceipt.rctVoid = 0 AND ili.arReceipt.rctAdCompany = " + AD_CMPNY + " ");
				
				orderBy = null;
				
				if (ORDER_BY.equals("DATE")) {
					
					orderBy = "ili.arReceipt.rctDate";
					
				} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
					
					orderBy = "ili.arReceipt.rctNumber";
					
				} 
				
				if (orderBy != null) {
					
					jbossQl.append("ORDER BY " + orderBy);
					
				} 
				
				arInvoiceLineItems = null;
				
				try {
					
					arInvoiceLineItems = arInvoiceLineItemHome.getIliByCriteria(jbossQl.toString(), obj);
					
				} catch (FinderException ex)	{
					
				} 	
				
				i = arInvoiceLineItems.iterator();
				
				
				
				while (i.hasNext()) {
					
					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();   
					
					
					if (SHOW_LN_ITEMS) {
						
						double AMOUNT = 0d;
						
						ArRepDeliveryReceiptDetails details = new ArRepDeliveryReceiptDetails();
						
						details.setDrCustomerName(arInvoiceLineItem.getArReceipt().getArCustomer().getCstName());
						details.setDrCustomerCode(arInvoiceLineItem.getArReceipt().getArCustomer().getCstCustomerCode());
						details.setDrDate(arInvoiceLineItem.getArReceipt().getRctDate());
						details.setDrDocumentNumber(arInvoiceLineItem.getArReceipt().getRctNumber());
						details.setDrReferenceNumber(arInvoiceLineItem.getArReceipt().getRctReferenceNumber());
						details.setDrDescription(arInvoiceLineItem.getArReceipt().getRctDescription());
						
						details.setDrItemName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
						details.setDrLocation(arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName());
						details.setDrQuantity(arInvoiceLineItem.getIliQuantity());
						details.setDrUnit(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
						details.setDrUnitPrice(arInvoiceLineItem.getIliUnitPrice());
						
						AMOUNT = EJBCommon.roundIt(arInvoiceLineItem.getIliQuantity() * arInvoiceLineItem.getIliUnitPrice(),  this.getGlFcPrecisionUnit(AD_CMPNY));
						
						details.setDrAmount(AMOUNT);
						details.setOrderBy(ORDER_BY);
						
						list.add(details);
						
					} else {
						
						if(tempDocNum == null || !tempDocNum.equals(arInvoiceLineItem.getArReceipt().getRctNumber())) {
							
							tempDocNum = arInvoiceLineItem.getArReceipt().getRctNumber();
							
							ArRepDeliveryReceiptDetails details = new ArRepDeliveryReceiptDetails();
							
							details.setDrCustomerName(arInvoiceLineItem.getArReceipt().getArCustomer().getCstName());
							details.setDrCustomerCode(arInvoiceLineItem.getArReceipt().getArCustomer().getCstCustomerCode());
							details.setDrDate(arInvoiceLineItem.getArReceipt().getRctDate());
							details.setDrDocumentNumber(arInvoiceLineItem.getArReceipt().getRctNumber());
							details.setDrReferenceNumber(arInvoiceLineItem.getArReceipt().getRctReferenceNumber());
							details.setDrDescription(arInvoiceLineItem.getArReceipt().getRctDescription());
							details.setDrAmount(arInvoiceLineItem.getIliAmount());
							details.setOrderBy(ORDER_BY);
							
							list.add(details);
							
						}
						
					}
					
				}
				
			}
			
			if(list.isEmpty())
				throw new GlobalNoRecordFoundException();
			
			// sort
			
			if (GROUP_BY.equals("ITEM NAME")) {
				
				Collections.sort(list, ArRepDeliveryReceiptDetails.ItemNameComparator);
				
			} else if (GROUP_BY.equals("CUSTOMER")) {
				
				Collections.sort(list, ArRepDeliveryReceiptDetails.CustomerComparator);
				
			} else {
				
				Collections.sort(list, ArRepDeliveryReceiptDetails.NoGroupComparator);
				
			}
			
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
		
		Debug.print("ArRepDeliveryReceiptControllerBean getAdCompany");      
		
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());
			
			return details;  	
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}  
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{
		
		Debug.print("ArRepDeliveryReceiptControllerBean getAdBrResAll");
		
		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;
		
		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;
		
		Collection adBranchResponsibilities = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (adBranchResponsibilities.isEmpty()) {
			
			throw new GlobalNoRecordFoundException();
			
		}
		
		try {
			
			Iterator i = adBranchResponsibilities.iterator();
			
			while(i.hasNext()) {
				
				adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
				
				adBranch = adBranchResponsibility.getAdBranch();
				
				AdBranchDetails details = new AdBranchDetails();
				
				details.setBrCode(adBranch.getBrCode());
				details.setBrBranchCode(adBranch.getBrBranchCode());
				details.setBrName(adBranch.getBrName());
				details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
				
				list.add(details);
				
			}	               
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		return list;
		
	}    
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ArRepDeliveryReceiptControllerBean getGlFcPrecisionUnit");
		
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
		
		Debug.print("ArRepDeliveryReceiptControllerBean convertForeignToFunctionalCurrency");
		
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		LocalAdCompany adCompany = null;
		
		// Initialize EJB Homes
		
		try {
			
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		// get company and extended precision
		
		try {
			
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}	     
		
		// Convert to functional currency if necessary
		
		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
			
			AMOUNT = AMOUNT / CONVERSION_RATE;
			
		}
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
		
	}
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("ArRepDeliveryReceiptControllerBean ejbCreate");
		
	}
}
