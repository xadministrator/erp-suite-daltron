
/*
 * ArRepInvoiceEditListControllerBean.java
 *
 * Created on June 25, 2004, 9:36 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArRepInvoiceEditListDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepInvoiceEditListControllerEJB"
 *           display-name="Used for printing invoice list transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepInvoiceEditListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepInvoiceEditListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepInvoiceEditListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class ArRepInvoiceEditListControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepInvoiceEditList(ArrayList invCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepInvoicePrintControllerBean executeArRepInvoiceEditList");
        
        LocalArInvoiceHome arInvoiceHome = null;        

        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
        	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = invCodeList.iterator();
        	
        	while (i.hasNext()) {
        	
        	    Integer INV_CODE = (Integer) i.next();
        	
	        	LocalArInvoice arInvoice = null;

	        	try {
	        		
	        		arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

	        	} catch (FinderException ex) {
	        		
	        		continue;
	        		
	        	}
	        		        	
	        	// get invoice distribution records
	        	
	        	Collection arDistributionRecords = arInvoice.getArDistributionRecords();        	            
	        	
	        	Iterator drIter = arDistributionRecords.iterator();
	        	
	        	while (drIter.hasNext()) {
	        		
	        		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drIter.next();
	        		
	        		ArRepInvoiceEditListDetails details = new ArRepInvoiceEditListDetails();
	        			        			        		
	        		if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
	        			
	        			details.setIelInvType("INV");
						details.setIelInvAmountDue(arInvoice.getInvAmountDue());
	        			
	        		} else {
	        			
	        			details.setIelInvType("CM");
						details.setIelInvAmountDue(arInvoice.getInvAmountDue());
	        			
	        		}
	        		
	        		details.setIelInvDateCreated(arInvoice.getInvDateCreated());
	        		details.setIelInvCreatedBy(arInvoice.getInvCreatedBy());
	        		details.setIelInvDocumentNumber(arInvoice.getInvNumber());
	        		details.setIelInvDate(arInvoice.getInvDate());
	        		details.setIelCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
	        		details.setIelCstCustomerName(arInvoice.getArCustomer().getCstName());
	        		details.setIelInvBatchName(arInvoice.getArInvoiceBatch() != null ? arInvoice.getArInvoiceBatch().getIbName() : "N/A");
	        		details.setIelInvBatchDescription(arInvoice.getArInvoiceBatch() != null ? arInvoice.getArInvoiceBatch().getIbDescription() : "N/A");
	        		details.setIelInvTransactionTotal(arInvoice.getArInvoiceBatch() != null ? arInvoice.getArInvoiceBatch().getArInvoices().size() : 0);
	        		details.setIelDrAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
	        		details.setIelDrAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
	        		details.setIelDrClass(arDistributionRecord.getDrClass());
	        		details.setIelDrDebit(arDistributionRecord.getDrDebit());
	        		details.setIelDrAmount(arDistributionRecord.getDrAmount());	   
	        		details.setIelInvDescription(arInvoice.getInvDescription());
	        		
	        		try{
	        			details.setIelSalesPersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
	        			System.out.println("arInvoice.getArSalesperson().getSlpSalespersonCode(): "+ arInvoice.getArSalesperson().getSlpSalespersonCode());
	        		}catch(Exception e){
	        			
	        		}
	        		
	        		
	        		list.add(details);

	        	}
	        	
	        }
	        
	        if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	        }        	       	
        	
        	return list;        	
        	        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepVoucherPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepVoucherPrintControllerBean ejbCreate");
      
    }
}
