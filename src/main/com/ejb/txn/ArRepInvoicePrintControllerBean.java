
/*
 * ArRepInvoicePrintControllerBean.java
 *
 * Created on March 11, 2004, 9:38 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArJobOrderAssignment;
import com.ejb.ar.LocalArJobOrderInvoiceLine;
import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvTag;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArRepInvoicePrintDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepInvoicePrintControllerEJB"
 *           display-name="Used for printing invoice transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepInvoicePrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepInvoicePrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepInvoicePrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
*/

public class ArRepInvoicePrintControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeArRepInvoicePrint(ArrayList invCodeList, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArRepInvoicePrintControllerBean executeArRepInvoicePrint");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdUserHome adUserHome = null;
        LocalAdBranchHome adBranchHome = null;


        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
              lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
        	    lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
    	        lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	Iterator i = invCodeList.iterator();

        	while (i.hasNext()) {

        	    Integer INV_CODE = (Integer) i.next();

	        	LocalArInvoice arInvoice = null;


	        	try {

	        		arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

	        	} catch (FinderException ex) {

	        		continue;

	        	}

	        	LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
				LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("AR INVOICE", AD_CMPNY);

				if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {

	        		if (arInvoice.getInvApprovalStatus() == null ||
	        			arInvoice.getInvApprovalStatus().equals("PENDING")) {

	        		    continue;

	        		}


	        	} else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {

	        		if (arInvoice.getInvApprovalStatus() != null &&
	        			(arInvoice.getInvApprovalStatus().equals("N/A") ||
	        			 arInvoice.getInvApprovalStatus().equals("APPROVED"))) {

	        			continue;

	        		}

	        	}

	        	if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE &&
	        		arInvoice.getInvPrinted() == EJBCommon.TRUE){

	        		continue;

	        	}

	        	// show duplicate

	        	boolean showDuplicate = false;

	        	if (adApprovalDocument.getAdcTrackDuplicate() == EJBCommon.TRUE &&
	        		arInvoice.getInvPrinted() == EJBCommon.TRUE) {

	        		showDuplicate = true;

	        	}

	        	// set printed

	        	arInvoice.setInvPrinted(EJBCommon.TRUE);

	        	// get total vat of all invoices

	        	double TOTAL_TAX = 0d;

	        	Collection arDistributionRecords = arDistributionRecordHome.findDrsByDrClassAndInvCode("TAX", arInvoice.getInvCode(), AD_CMPNY);

	        	Iterator j = arDistributionRecords.iterator();

	        	while(j.hasNext()) {

	        		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

	        		TOTAL_TAX += arDistributionRecord.getDrAmount();

	        	}

                        arDistributionRecords = arDistributionRecordHome.findDrsByDrClassAndInvCode("DEFERRED TAX", arInvoice.getInvCode(), AD_CMPNY);

	        	j = arDistributionRecords.iterator();

	        	while(j.hasNext()) {

	        		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

	        		TOTAL_TAX += arDistributionRecord.getDrAmount();

	        	}

	        	// get invoice lines

	        	if (adPreference.getPrfArSalesInvoiceDataSource().equals("AR DISTRIBUTION RECORD")) {

	        		// ar distribution record data source

	        		if (!arInvoice.getArInvoiceLines().isEmpty()) {

	        			Collection arInvoiceLines = arInvoice.getArInvoiceLines();

	        			Iterator ilIter = arInvoiceLines.iterator();

	        			while (ilIter.hasNext()) {

	        				LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)ilIter.next();

	        				ArRepInvoicePrintDetails details = new ArRepInvoicePrintDetails();

	        				details.setIpInvType(arInvoice.getInvType());
	        				details.setIpInvDescription(arInvoice.getInvDescription());
	        				details.setIpInvDate(arInvoice.getInvDate());
	        				details.setIpInvNumber(arInvoice.getInvNumber());

	        				try {
	        					details.setIpInvBatch(arInvoice.getArInvoiceBatch().getIbName());

	        				} catch (Exception ex) {
	        					details.setIpInvBatch("");
	        				}
	        				details.setIpInvCustomerName(arInvoice.getArCustomer().getCstName());
	        				details.setIpInvCustomerAddress(arInvoice.getArCustomer().getCstAddress());
	        				details.setIpInvCustomerCity(arInvoice.getArCustomer().getCstCity());
	        				details.setIpInvCustomerDealPrice(arInvoice.getArCustomer().getCstDealPrice());
	        				details.setIpInvCustomerCountry(arInvoice.getArCustomer().getCstCountry());
	        				details.setIpIlDescription(arInvoiceLine.getIlDescription());
	        				details.setIpIlQuantity(arInvoiceLine.getIlQuantity());

	        				details.setIpIlUnitPrice(arInvoiceLine.getIlUnitPrice());
	        				details.setIpIlAmount(arInvoiceLine.getIlAmount() + arInvoiceLine.getIlTaxAmount());


	        				details.setIpInvCurrency(arInvoice.getGlFunctionalCurrency().getFcName());
	        				details.setIpInvCurrencySymbol(arInvoice.getGlFunctionalCurrency().getFcSymbol());
	        				details.setIpInvCurrencyDescription(arInvoice.getGlFunctionalCurrency().getFcDescription());
	        				details.setIpInvReferenceNumber(arInvoice.getInvReferenceNumber());
	        				details.setIpInvCreatedBy(arInvoice.getInvCreatedBy());
	        				details.setIpInvApprovalStatus(arInvoice.getInvApprovalStatus());
	        				details.setIpReportParameter(arInvoice.getReportParameter());
	        				
	        				details.setIpInvIsDraft(arInvoice.getInvPosted()==EJBCommon.FALSE?true:false);
	        				try {
	        					LocalAdUser adUser = adUserHome.findByUsrName(arInvoice.getInvCreatedBy(), AD_CMPNY);
	        					details.setIpInvCreatedByDesc(adUser.getUsrDescription());
	        					System.out.print("adUser.getUsrDescription(): "+adUser.getUsrDescription());
	        				} catch (FinderException ex) {}

            				try{
            					System.out.println("adPreference.getPrfApDefaultChecker()(): "+adPreference.getPrfApDefaultChecker());
                				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
                				details.setIpInvCheckByDescription(adUser2.getUsrDescription());
                				System.out.print("adUser2.getUsrDescription(): "+adUser2.getUsrDescription());
            				}catch(Exception e){
            					details.setIpInvCheckByDescription("");
            				}

            				try{
            					System.out.println("apCheck.getChkApprovedRejectedBy(): "+arInvoice.getInvApprovedRejectedBy());
                				LocalAdUser adUser3 = adUserHome.findByUsrName(arInvoice.getInvApprovedRejectedBy(), AD_CMPNY);
                				details.setIpInvApprovedRejectedByDescription(adUser3.getUsrDescription());
                				System.out.print("adUser3.getUsrDescription(): "+adUser3.getUsrDescription());
            				}catch(Exception e){
            					details.setIpInvApprovedRejectedByDescription("");
            				}

	        				details.setIpInvApprovedRejectedBy(arInvoice.getInvApprovedRejectedBy());
	        				details.setIpInvAmount(arInvoice.getInvAmountDue());
	        				details.setIpInvAmountUnearnedInterest(arInvoice.getInvAmountUnearnedInterest());
	        				details.setIpShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);

	        				// added fields

	        				details.setIpInvBillingHeader(arInvoice.getInvBillingHeader());
	        				details.setIpInvBillingFooter(arInvoice.getInvBillingFooter());
	        				details.setIpInvBillingHeader2(arInvoice.getInvBillingHeader2());
	        				details.setIpInvBillingFooter2(arInvoice.getInvBillingFooter2());
	        				details.setIpInvBillingHeader3(arInvoice.getInvBillingHeader3());
	        				details.setIpInvBillingFooter3(arInvoice.getInvBillingFooter3());
	        				details.setIpInvBillingSignatory(arInvoice.getInvBillingSignatory());
	        				details.setIpInvSignatoryTitle(arInvoice.getInvSignatoryTitle());
	        				details.setIpInvBillToAddress(arInvoice.getInvBillToAddress());
	        				details.setIpInvBillToContact(arInvoice.getInvBillToContact());
	        				details.setIpInvBillToAltContact(arInvoice.getInvBillToAltContact());
	        				details.setIpInvBillToPhone(arInvoice.getInvBillToPhone());
	        				details.setIpInvShipToAddress(arInvoice.getInvShipToAddress());
	        				details.setIpInvShipToContact(arInvoice.getInvShipToContact());
	        				details.setIpInvShipToAltContact(arInvoice.getInvShipToAltContact());
	        				details.setIpInvShipToPhone(arInvoice.getInvShipToPhone());
	        				details.setIpInvTotalTax(TOTAL_TAX);
	        				details.setIpIlAmountWoVat(arInvoiceLine.getIlAmount());
	        				details.setIpInvCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
	        				details.setIpIlName(arInvoiceLine.getArStandardMemoLine().getSmlName());
	        				details.setIpInvCustomerDescription(arInvoice.getArCustomer().getCstDescription());
	        				details.setIpInvCustomerTin(arInvoice.getArCustomer().getCstTin());
	        				details.setIpInvCustomerContactPerson(arInvoice.getArCustomer().getCstContact());
	        				details.setIpInvCustomerPhoneNumber(arInvoice.getArCustomer().getCstPhone());
	        				details.setIpInvCustomerMobileNumber(arInvoice.getArCustomer().getCstMobilePhone());
	        				details.setIpInvCustomerFax(arInvoice.getArCustomer().getCstFax());
	        				details.setIpInvCustomerEmail(arInvoice.getArCustomer().getCstEmail());
	        				details.setIpInvIsDraft(arInvoice.getInvPosted()==EJBCommon.FALSE?true:false);
	        				
	        				list.add(details);
	        				// get unit price w/o VAT

	        				short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

	        				double UNT_PRC = this.calculateIlNetUnitPrice(details, arInvoice.getArTaxCode().getTcRate(),
	        						arInvoice.getArTaxCode().getTcType(), precisionUnit);

	        				System.out.println();
	        				details.setIpIlUnitPriceWoVat(UNT_PRC);

	        				// add fields discount amount, discount desc, unit of measure, payment term, term desc

	        				LocalArDistributionRecord arDistributionRecord = null;
	        				try {

	        					arDistributionRecord = arDistributionRecordHome.findByDrClassAndInvCode("DISCOUNT", arInvoice.getInvCode(), AD_CMPNY);

	        					details.setIpInvDiscountAmount(arDistributionRecord.getDrAmount());
	        					details.setIpInvDiscountDescription(arInvoice.getAdPaymentTerm().getPytDiscountDescription());

	        				} catch(FinderException ex) {

	        				}

	        				details.setIpInvIlSmlUnitOfMeasure(arInvoiceLine.getArStandardMemoLine().getSmlUnitOfMeasure());

	        				details.setIpInvPytName(arInvoice.getAdPaymentTerm().getPytName());
	        				details.setIpInvPytDescription(arInvoice.getAdPaymentTerm().getPytDescription());

	        				Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();
	        				ArrayList arInvoicePaymentScheduleList = new ArrayList(arInvoicePaymentSchedules);

	        				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
	        					(LocalArInvoicePaymentSchedule)arInvoicePaymentScheduleList.get(arInvoicePaymentScheduleList.size() - 1);

	        				details.setIpInvDueDate(arInvoicePaymentSchedule.getIpsDueDate());
	        				details.setIpInvEffectivityDate(arInvoice.getInvEffectivityDate());


	        				if(arInvoice.getArSalesperson() != null) {

	        					details.setIpSlpSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
	        					details.setIpSlpName(arInvoice.getArSalesperson().getSlpName());

	        				}

	        				details.setIpInvTcRate(arInvoice.getArTaxCode().getTcRate());
	        				details.setIpInvTaxCode(arInvoice.getArTaxCode().getTcName());
	        				details.setIpInvWithholdingTaxCode(arInvoice.getArWithholdingTaxCode().getWtcName());

	        				// get withholding tax amount
	        				double netAmount = EJBCommon.roundIt(arInvoice.getInvAmountDue() / (1 + (arInvoice.getArTaxCode().getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));
        					details.setIpInvWithholdingTaxAmount(EJBCommon.roundIt(netAmount * (arInvoice.getArWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY)));



	        			}

	        		} else if (!arInvoice.getArInvoiceLineItems().isEmpty()) {

	        			Collection arInvoiceLineItems = null;

	        			try {

	        				arInvoiceLineItems = arInvoiceLineItemHome.findByInvCode(arInvoice.getInvCode(), AD_CMPNY);

	        			} catch(FinderException ex) {

	        			}

	        			Iterator ilIter = arInvoiceLineItems.iterator();

	        			while (ilIter.hasNext()) {

	        				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)ilIter.next();

	        				ArRepInvoicePrintDetails details = new ArRepInvoicePrintDetails();

	        				details.setIpInvType(arInvoice.getInvType());
	        				details.setIpInvDescription(arInvoice.getInvDescription());
	        				details.setIpInvDate(arInvoice.getInvDate());
	        				details.setIpInvNumber(arInvoice.getInvNumber());
	        				try {
	        					details.setIpInvBatch(arInvoice.getArInvoiceBatch().getIbName());

	        				} catch (Exception ex) {
	        					details.setIpInvBatch("");
	        				}

	        				details.setIpInvCustomerName(arInvoice.getArCustomer().getCstName());
	        				details.setIpInvCustomerAddress(arInvoice.getArCustomer().getCstAddress());
	        				details.setIpInvCustomerCity(arInvoice.getArCustomer().getCstCity());
	        				details.setIpInvCustomerCountry(arInvoice.getArCustomer().getCstCountry());
	        				details.setIpInvCustomerDealPrice(arInvoice.getArCustomer().getCstDealPrice());
	        				details.setIpIlDescription(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiDescription());
	        				details.setIpIlQuantity(arInvoiceLineItem.getIliQuantity());
	        				details.setIpInvIlSmlUnitOfMeasure(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());

	        				details.setIpIlUnitPrice(arInvoiceLineItem.getIliUnitPrice());
	        				details.setIpIlAmount(arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount());
	        				details.setIpIlIsTax(arInvoiceLineItem.getIliTax()==EJBCommon.TRUE?"Y":"N");
	        				details.setIpInvCurrency(arInvoice.getGlFunctionalCurrency().getFcName());
	        				details.setIpInvCurrencySymbol(arInvoice.getGlFunctionalCurrency().getFcSymbol());
	        				details.setIpInvCurrencyDescription(arInvoice.getGlFunctionalCurrency().getFcDescription());
	        				details.setIpInvReferenceNumber(arInvoice.getInvReferenceNumber());
	        				details.setIpInvCreatedBy(arInvoice.getInvCreatedBy());
	        				details.setIpInvApprovalStatus(arInvoice.getInvApprovalStatus());
	        				details.setIpReportParameter(arInvoice.getReportParameter());
	        				
	        				details.setIpInvIsDraft(arInvoice.getInvPosted()==EJBCommon.FALSE?true:false);

	        				try {
	        					LocalAdUser adUser = adUserHome.findByUsrName(arInvoice.getInvCreatedBy(), AD_CMPNY);
	        					details.setIpInvCreatedByDesc(adUser.getUsrDescription());
	        				} catch (FinderException ex) {}

	        				try{
            					System.out.println("adPreference.getPrfApDefaultChecker()(2): "+adPreference.getPrfApDefaultChecker());
                				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
                				details.setIpInvCheckByDescription(adUser2.getUsrDescription());
            				}catch(Exception e){
            					details.setIpInvCheckByDescription("");
            				}

            				try{
            					System.out.println("apCheck.getChkApprovedRejectedBy(2): "+arInvoice.getInvApprovedRejectedBy());
                				LocalAdUser adUser3 = adUserHome.findByUsrName(arInvoice.getInvApprovedRejectedBy(), AD_CMPNY);
                				details.setIpInvApprovedRejectedByDescription(adUser3.getUsrDescription());
            				}catch(Exception e){
            					details.setIpInvApprovedRejectedByDescription("");
            				}

	        				details.setIpInvApprovedRejectedBy(arInvoice.getInvApprovedRejectedBy());
	        				details.setIpInvAmount(arInvoice.getInvAmountDue());
	        				details.setIpInvAmountUnearnedInterest(arInvoice.getInvAmountUnearnedInterest());
	        				details.setIpShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);

	        				// added fields
	        				details.setIpInvBillingHeader(arInvoice.getInvBillingHeader());
	        				details.setIpInvBillingFooter(arInvoice.getInvBillingFooter());
	        				details.setIpInvBillingHeader2(arInvoice.getInvBillingHeader2());
	        				details.setIpInvBillingFooter2(arInvoice.getInvBillingFooter2());
	        				details.setIpInvBillingHeader3(arInvoice.getInvBillingHeader3());
	        				details.setIpInvBillingFooter3(arInvoice.getInvBillingFooter3());
	        				details.setIpInvBillingSignatory(arInvoice.getInvBillingSignatory());
	        				details.setIpInvSignatoryTitle(arInvoice.getInvSignatoryTitle());
	        				details.setIpInvBillToAddress(arInvoice.getInvBillToAddress());
	        				details.setIpInvBillToContact(arInvoice.getInvBillToContact());
	        				details.setIpInvBillToAltContact(arInvoice.getInvBillToAltContact());
	        				details.setIpInvBillToPhone(arInvoice.getInvBillToPhone());
	        				details.setIpInvShipToAddress(arInvoice.getInvShipToAddress());
	        				details.setIpInvShipToContact(arInvoice.getInvShipToContact());
	        				details.setIpInvShipToAltContact(arInvoice.getInvShipToAltContact());
	        				details.setIpInvShipToPhone(arInvoice.getInvShipToPhone());
	        				details.setIpIliClientPo(arInvoice.getInvClientPO());
	        				details.setIpInvTotalTax(TOTAL_TAX);

	        				details.setIpIlAmountWoVat(arInvoice.getArTaxCode().getTcName().contains("EXCLUSIVE") ? arInvoiceLineItem.getIliAmount() : arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount());
	        				details.setIpIlCategory(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiAdLvCategory());
	        				details.setIpInvCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
	        				details.setIpIlName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
	        				details.setIpIlUomShortName(arInvoiceLineItem.getInvUnitOfMeasure().getUomShortName());

	        				details.setIpInvCustomerDescription(arInvoice.getArCustomer().getCstDescription());
	        				details.setIpInvCustomerTin(arInvoice.getArCustomer().getCstTin());

	        				details.setIpIliDiscount1(arInvoiceLineItem.getIliDiscount1());
	        				details.setIpIliDiscount2(arInvoiceLineItem.getIliDiscount2());
	        				details.setIpIliDiscount3(arInvoiceLineItem.getIliDiscount3());
	        				details.setIpIliDiscount4(arInvoiceLineItem.getIliDiscount4());

	        				details.setIpInvIsDraft(arInvoice.getInvPosted()==EJBCommon.FALSE?true:false);
	        				// get unit price w/o VAT
	        				short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

	        				double UNT_PRC = this.calculateIlNetUnitPrice(details, arInvoice.getArTaxCode().getTcRate(),
	        						arInvoice.getArTaxCode().getTcType(), precisionUnit);

	        				details.setIpIlUnitPriceWoVat(UNT_PRC);

	        				// add fields discount amount, discount desc, unit of measure, payment term, term desc

	        				LocalArDistributionRecord arDistributionRecord = null;

	        				try {

	        					arDistributionRecord = arDistributionRecordHome.findByDrClassAndInvCode("DISCOUNT", arInvoice.getInvCode(), AD_CMPNY);

	        					details.setIpInvDiscountAmount(arDistributionRecord.getDrAmount());
	        					details.setIpInvDiscountDescription(arInvoice.getAdPaymentTerm().getPytDiscountDescription());

	        				} catch (FinderException ex) {

	        				}

	        				details.setIpInvPytName(arInvoice.getAdPaymentTerm().getPytName());
	        				details.setIpInvPytDescription(arInvoice.getAdPaymentTerm().getPytDescription());

	        				Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();
	        				ArrayList arInvoicePaymentScheduleList = new ArrayList(arInvoicePaymentSchedules);

	        				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
	        					(LocalArInvoicePaymentSchedule)arInvoicePaymentScheduleList.get(arInvoicePaymentScheduleList.size() - 1);

	        				details.setIpInvDueDate(arInvoicePaymentSchedule.getIpsDueDate());
	        				details.setIpInvEffectivityDate(arInvoice.getInvEffectivityDate());


	        				if(arInvoice.getArSalesperson() != null) {

	        					details.setIpSlpSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
	        					details.setIpSlpName(arInvoice.getArSalesperson().getSlpName());

	        				}

	        				details.setIpInvTcRate(arInvoice.getArTaxCode().getTcRate());
	        				details.setIpInvTaxCode(arInvoice.getArTaxCode().getTcName());
	        				details.setIpInvWithholdingTaxCode(arInvoice.getArWithholdingTaxCode().getWtcName());

	        				// get withholding tax amount
	        				double netAmount = EJBCommon.roundIt(arInvoice.getInvAmountDue() / (1 + (arInvoice.getArTaxCode().getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));
        					details.setIpInvWithholdingTaxAmount(EJBCommon.roundIt(netAmount * (arInvoice.getArWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY)));
        					details.setIpIliTotalDiscount(arInvoiceLineItem.getIliTotalDiscount());

//                			Include Branch
            				LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(arInvoice.getInvAdBranch());
            				details.setIpInvBranchCode(adBranch.getBrBranchCode());
            				details.setIpInvBranchName(adBranch.getBrName());

            				details.setIpInvPartNumber(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiPartNumber());
        					System.out.println("Pasok1");


        					//trace misc
                			if(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()==EJBCommon.TRUE) {

                				if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getInvTags().size() <= 0) {
                					System.out.println("old code");
                					ArrayList miscArray = convertMiscToArrayList(arInvoiceLineItem.getIliMisc());
                					//property code
                					details.setIpIlItemPropertyCode(miscArray.get(0).toString());
                					//serial number
                					details.setIpIlItemSerialNumber(miscArray.get(1).toString());
                					//specs
                					details.setIpIlItemSpecs(miscArray.get(2).toString());
                					//custodian
                					details.setIpIlItemCustodian(miscArray.get(3).toString());
                					//expiration date
                					details.setIpIlItemExpiryDate(miscArray.get(4).toString());
                				}

        						if(arInvoiceLineItem.getInvTags().size()>0) {
        							System.out.println("new code");
        							StringBuilder strBProperty = new StringBuilder();
        							StringBuilder strBSerial = new StringBuilder();
        							StringBuilder strBSpecs = new StringBuilder();
        							StringBuilder strBCustodian= new StringBuilder();
        							StringBuilder strBExpirationDate = new StringBuilder();
        							Iterator it = arInvoiceLineItem.getInvTags().iterator();


        							while(it.hasNext()) {

        								LocalInvTag invTag = (LocalInvTag)it.next();

        								//property code
        								if(!invTag.getTgPropertyCode().trim().equals("")) {
        									strBProperty.append(invTag.getTgPropertyCode());
        									strBProperty.append(System.getProperty("line.separator"));
        								}

        								//serial

        								if(!invTag.getTgSerialNumber().trim().equals("")) {
        									System.out.println("serial prob:" + invTag.getTgSerialNumber());
        									strBSerial.append(invTag.getTgSerialNumber());
        									strBSerial.append(System.getProperty("line.separator"));
        								}

        								//spec

        								if(!invTag.getTgSpecs().trim().equals("")) {
        									strBSpecs.append(invTag.getTgSpecs());
        									strBSpecs.append(System.getProperty("line.separator"));
        								}

        								//custodian

        								if(invTag.getAdUser()!= null) {
        									strBCustodian.append(invTag.getAdUser().getUsrName());
        									strBCustodian.append(System.getProperty("line.separator"));
        								}

        								//exp date

        								if(invTag.getTgExpiryDate()!=null) {
        									strBExpirationDate.append(invTag.getTgExpiryDate());
        									strBExpirationDate.append(System.getProperty("line.separator"));

        								}



        							}
        							//property code
                					details.setIpIlItemPropertyCode(strBProperty.toString().toString());
                					//serial number
                					details.setIpIlItemSerialNumber(strBSerial.toString().toString());
                					//specs
                					details.setIpIlItemSpecs(strBSpecs.toString().toString());
                					//custodian
                					details.setIpIlItemCustodian(strBCustodian.toString().toString());
                					//expiration date
                					details.setIpIlItemExpiryDate(strBExpirationDate.toString().toString());

                					System.out.println(strBSerial.toString());
                					System.out.println(strBSpecs.toString());

        						}


                			}





	        				list.add(details);

	        			}

	        		} else if(!arInvoice.getArSalesOrderInvoiceLines().isEmpty()) {

	        			Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

	        			Iterator soiIter = arSalesOrderInvoiceLines.iterator();

	        			while (soiIter.hasNext()) {

	        				LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)soiIter.next();
	        				LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();

	        				ArRepInvoicePrintDetails details = new ArRepInvoicePrintDetails();

	        				details.setIpInvType(arInvoice.getInvType());
	        				details.setIpInvDescription(arInvoice.getInvDescription());
	        				details.setIpInvDate(arInvoice.getInvDate());
	        				details.setIpInvNumber(arInvoice.getInvNumber());
	        				try {
	        					details.setIpInvBatch(arInvoice.getArInvoiceBatch().getIbName());

	        				} catch (Exception ex) {
	        					details.setIpInvBatch("");
	        				}
	        				details.setIpInvCustomerName(arInvoice.getArCustomer().getCstName());
	        				details.setIpInvCustomerAddress(arInvoice.getArCustomer().getCstAddress());
	        				details.setIpInvCustomerCity(arInvoice.getArCustomer().getCstCity());
	        				details.setIpIlCategory(arSalesOrderLine.getInvItemLocation().getInvItem().getIiAdLvCategory());
	        				details.setIpInvCustomerCountry(arInvoice.getArCustomer().getCstCountry());
	        				details.setIpInvCustomerDealPrice(arInvoice.getArCustomer().getCstDealPrice());
	        				details.setIpIlDescription(arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription());
	        				details.setIpIlQuantity(arSalesOrderInvoiceLine.getSilQuantityDelivered());

	        				details.setIpInvIlSmlUnitOfMeasure(arSalesOrderLine.getInvUnitOfMeasure().getUomName());
	        				details.setIpIlUnitPrice(arSalesOrderLine.getSolUnitPrice());
	        				details.setIpIlAmount(arSalesOrderInvoiceLine.getSilAmount() + arSalesOrderInvoiceLine.getSilTaxAmount());

	        				details.setIpIlIsTax(arSalesOrderInvoiceLine.getSilTax()==EJBCommon.TRUE?"Y":"N");
	        				details.setIpInvCurrency(arInvoice.getGlFunctionalCurrency().getFcName());
	        				details.setIpInvCurrencySymbol(arInvoice.getGlFunctionalCurrency().getFcSymbol());
	        				details.setIpInvCurrencyDescription(arInvoice.getGlFunctionalCurrency().getFcDescription());
	        				details.setIpInvReferenceNumber(arInvoice.getInvReferenceNumber());
	        				details.setIpInvCreatedBy(arInvoice.getInvCreatedBy());
	        				details.setIpInvApprovalStatus(arInvoice.getInvApprovalStatus());
	        				details.setIpReportParameter(arInvoice.getReportParameter());
	        				
	        				details.setIpInvIsDraft(arInvoice.getInvPosted()==EJBCommon.FALSE?true:false);
	        				try {
	        					LocalAdUser adUser = adUserHome.findByUsrName(arInvoice.getInvCreatedBy(), AD_CMPNY);
	        					details.setIpInvCreatedByDesc(adUser.getUsrDescription());
	        				} catch (FinderException ex) {}
	        				try{
            					System.out.println("adPreference.getPrfApDefaultChecker()(3): "+adPreference.getPrfApDefaultChecker());
                				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
                				details.setIpInvCheckByDescription(adUser2.getUsrDescription());
            				}catch(Exception e){
            					details.setIpInvCheckByDescription("");
            				}

            				try{
            					System.out.println("apCheck.getChkApprovedRejectedBy(3): "+arInvoice.getInvApprovedRejectedBy());
                				LocalAdUser adUser3 = adUserHome.findByUsrName(arInvoice.getInvApprovedRejectedBy(), AD_CMPNY);
                				details.setIpInvApprovedRejectedByDescription(adUser3.getUsrDescription());
            				}catch(Exception e){
            					details.setIpInvApprovedRejectedByDescription("");
            				}

            				details.setIpInvApprovedRejectedBy(arInvoice.getInvApprovedRejectedBy());
	        				details.setIpInvAmount(arInvoice.getInvAmountDue());
	        				details.setIpInvAmountUnearnedInterest(arInvoice.getInvAmountUnearnedInterest());
	        				details.setIpShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);

	        				// added fields
	        				details.setIpInvBillingHeader(arInvoice.getInvBillingHeader());
	        				details.setIpInvBillingFooter(arInvoice.getInvBillingFooter());
	        				details.setIpInvBillingHeader2(arInvoice.getInvBillingHeader2());
	        				details.setIpInvBillingFooter2(arInvoice.getInvBillingFooter2());
	        				details.setIpInvBillingSignatory(arInvoice.getInvBillingSignatory());
	        				details.setIpInvSignatoryTitle(arInvoice.getInvSignatoryTitle());
	        				details.setIpInvBillToAddress(arInvoice.getInvBillToAddress());
	        				details.setIpInvBillToContact(arInvoice.getInvBillToContact());
	        				details.setIpInvBillToAltContact(arInvoice.getInvBillToAltContact());
	        				details.setIpInvBillToPhone(arInvoice.getInvBillToPhone());
	        				details.setIpInvShipToAddress(arInvoice.getInvShipToAddress());
	        				details.setIpInvShipToContact(arInvoice.getInvShipToContact());
	        				details.setIpInvShipToAltContact(arInvoice.getInvShipToAltContact());
	        				details.setIpInvShipToPhone(arInvoice.getInvShipToPhone());
	        				details.setIpIliClientPo(arInvoice.getInvClientPO());
	        				details.setIpInvTotalTax(TOTAL_TAX);

	        				details.setIpIlAmountWoVat(arInvoice.getArTaxCode().getTcName().contains("EXCLUSIVE") ? arSalesOrderInvoiceLine.getSilAmount() : arSalesOrderInvoiceLine.getSilAmount() + arSalesOrderInvoiceLine.getSilTaxAmount());


	        				details.setIpInvCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
	        				details.setIpIlName(arSalesOrderLine.getInvItemLocation().getInvItem().getIiName());
	        				details.setIpIlUomShortName(arSalesOrderLine.getInvUnitOfMeasure().getUomShortName());

	        				details.setIpInvCustomerDescription(arInvoice.getArCustomer().getCstDescription());
	        				details.setIpInvCustomerTin(arInvoice.getArCustomer().getCstTin());

	        				// add fields discount amount, discount desc, unit of measure, payment term, term desc

	        				LocalArDistributionRecord arDistributionRecord = null;

	        				try {

	        					arDistributionRecord = arDistributionRecordHome.findByDrClassAndInvCode("DISCOUNT", arInvoice.getInvCode(), AD_CMPNY);

	        					details.setIpInvDiscountAmount(arDistributionRecord.getDrAmount());
	        					details.setIpInvDiscountDescription(arInvoice.getAdPaymentTerm().getPytDiscountDescription());

	        				} catch (FinderException ex) {

	        				}

	        				details.setIpInvPytName(arInvoice.getAdPaymentTerm().getPytName());
	        				details.setIpInvPytDescription(arInvoice.getAdPaymentTerm().getPytDescription());

	        				Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();
	        				ArrayList arInvoicePaymentScheduleList = new ArrayList(arInvoicePaymentSchedules);

	        				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
	        					(LocalArInvoicePaymentSchedule)arInvoicePaymentScheduleList.get(arInvoicePaymentScheduleList.size() - 1);

	        				details.setIpInvDueDate(arInvoicePaymentSchedule.getIpsDueDate());
	        				details.setIpInvEffectivityDate(arInvoice.getInvEffectivityDate());

	        				if(arInvoice.getArSalesperson() != null) {

	        					details.setIpSlpSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
	        					details.setIpSlpName(arInvoice.getArSalesperson().getSlpName());

	        				}

	        				details.setIpInvSoNumber(arInvoice.getInvSoNumber());
	        				details.setIpInvJoNumber(arInvoice.getInvJoNumber());

	        				// get unit price w/o VAT

	        				short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

	        				double UNT_PRC = this.calculateIlNetUnitPrice(details, arInvoice.getArTaxCode().getTcRate(),
	        						arInvoice.getArTaxCode().getTcType(), precisionUnit);

	        				details.setIpIlUnitPriceWoVat(UNT_PRC);

	        				details.setIpIliDiscount1(arSalesOrderInvoiceLine.getSilDiscount1());
	        				details.setIpIliDiscount2(arSalesOrderInvoiceLine.getSilDiscount2());
	        				details.setIpIliDiscount3(arSalesOrderInvoiceLine.getSilDiscount3());
	        				details.setIpIliDiscount4(arSalesOrderInvoiceLine.getSilDiscount4());

	        				details.setIpInvTcRate(arInvoice.getArTaxCode().getTcRate());
	        				details.setIpInvTaxCode(arInvoice.getArTaxCode().getTcName());
	        				details.setIpInvWithholdingTaxCode(arInvoice.getArWithholdingTaxCode().getWtcName());

	        				// get withholding tax amount
	        				double netAmount = EJBCommon.roundIt(arInvoice.getInvAmountDue() / (1 + (arInvoice.getArTaxCode().getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));
        					details.setIpInvWithholdingTaxAmount(EJBCommon.roundIt(netAmount * (arInvoice.getArWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY)));

        					details.setIpIliTotalDiscount(arSalesOrderInvoiceLine.getSilTotalDiscount());

//                			Include Branch
            				LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(arInvoice.getInvAdBranch());
            				details.setIpInvBranchCode(adBranch.getBrBranchCode());
            				details.setIpInvBranchName(adBranch.getBrName());

        					System.out.println("Pasok2");
        					details.setIpInvPartNumber(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiPartNumber());

        					details.setIpInvSoReferenceNumber(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoReferenceNumber());
        					details.setIpInvIsDraft(arInvoice.getInvPosted()==EJBCommon.FALSE?true:false);


        					//trace misc
                			if(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiTraceMisc()==EJBCommon.TRUE) {



        						if(arSalesOrderInvoiceLine.getInvTags().size()>0) {
        							System.out.println("new code");
        							StringBuilder strBProperty = new StringBuilder();
        							StringBuilder strBSerial = new StringBuilder();
        							StringBuilder strBSpecs = new StringBuilder();
        							StringBuilder strBCustodian= new StringBuilder();
        							StringBuilder strBExpirationDate = new StringBuilder();
        							Iterator it = arSalesOrderInvoiceLine.getInvTags().iterator();


        							while(it.hasNext()) {

        								LocalInvTag invTag = (LocalInvTag)it.next();

        								//property code
        								if(!invTag.getTgPropertyCode().trim().equals("")) {
        									strBProperty.append(invTag.getTgPropertyCode());
        									strBProperty.append(System.getProperty("line.separator"));
        								}

        								//serial

        								if(!invTag.getTgSerialNumber().trim().equals("")) {
        									strBSerial.append(invTag.getTgSerialNumber());
        									strBSerial.append(System.getProperty("line.separator"));
        								}

        								//spec

        								if(!invTag.getTgSpecs().trim().equals("")) {
        									strBSpecs.append(invTag.getTgSpecs());
        									strBSpecs.append(System.getProperty("line.separator"));
        								}

        								//custodian

        								if(invTag.getAdUser()!= null) {
        									strBCustodian.append(invTag.getAdUser().getUsrName());
        									strBCustodian.append(System.getProperty("line.separator"));
        								}

        								//exp date

        								if(invTag.getTgExpiryDate()!=null) {
        									strBExpirationDate.append(invTag.getTgExpiryDate());
        									strBExpirationDate.append(System.getProperty("line.separator"));

        								}



        							}
        							//property code
                					details.setIpIlItemPropertyCode(strBProperty.toString());
                					//serial number
                					details.setIpIlItemSerialNumber(strBSerial.toString());
                					//specs
                					details.setIpIlItemSpecs(strBSpecs.toString());
                					//custodian
                					details.setIpIlItemCustodian(strBCustodian.toString());
                					//expiration date
                					details.setIpIlItemExpiryDate(strBExpirationDate.toString());



        						}


                			}










	        				list.add(details);

	        			}

	        		}else if(!arInvoice.getArJobOrderInvoiceLines().isEmpty()) {

	        			Collection arJobOrderInvoiceLines = arInvoice.getArJobOrderInvoiceLines();

	        			Iterator joiIter = arJobOrderInvoiceLines.iterator();

	        			while (joiIter.hasNext()) {

	        				
	        				LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine)joiIter.next();
	        				LocalArJobOrderLine arJobOrderLine = arJobOrderInvoiceLine.getArJobOrderLine();

	        				ArRepInvoicePrintDetails details = new ArRepInvoicePrintDetails();

	        				details.setIpInvType(arInvoice.getInvType());
	        				details.setIpInvDescription(arInvoice.getInvDescription());
	        				details.setIpInvDate(arInvoice.getInvDate());
	        				details.setIpInvNumber(arInvoice.getInvNumber());
	        				try {
	        					details.setIpInvBatch(arInvoice.getArInvoiceBatch().getIbName());

	        				} catch (Exception ex) {
	        					details.setIpInvBatch("");
	        				}
	        				details.setIpInvCustomerName(arInvoice.getArCustomer().getCstName());
	        				details.setIpInvCustomerAddress(arInvoice.getArCustomer().getCstAddress());
	        				details.setIpInvCustomerCity(arInvoice.getArCustomer().getCstCity());
	        				details.setIpIlCategory(arJobOrderLine.getInvItemLocation().getInvItem().getIiAdLvCategory());
	        				details.setIpInvCustomerCountry(arInvoice.getArCustomer().getCstCountry());
	        				details.setIpInvCustomerDealPrice(arInvoice.getArCustomer().getCstDealPrice());
	        				details.setIpIlDescription(arJobOrderLine.getInvItemLocation().getInvItem().getIiDescription());
	        				details.setIpIlQuantity(arJobOrderInvoiceLine.getJilQuantityDelivered());

	        				details.setIpInvIlSmlUnitOfMeasure(arJobOrderLine.getInvUnitOfMeasure().getUomName());
	        				details.setIpIlUnitPrice(arJobOrderLine.getJolUnitPrice());
	        				details.setIpIlAmount(arJobOrderInvoiceLine.getJilAmount() + arJobOrderInvoiceLine.getJilTaxAmount());
	        				

	        				details.setIpIlIsTax(arJobOrderInvoiceLine.getJilTax()==EJBCommon.TRUE?"Y":"N");
	        				details.setIpInvCurrency(arInvoice.getGlFunctionalCurrency().getFcName());
	        				details.setIpInvCurrencySymbol(arInvoice.getGlFunctionalCurrency().getFcSymbol());
	        				details.setIpInvCurrencyDescription(arInvoice.getGlFunctionalCurrency().getFcDescription());
	        				details.setIpInvReferenceNumber(arInvoice.getInvReferenceNumber());
	        				details.setIpInvCreatedBy(arInvoice.getInvCreatedBy());
	        				details.setIpInvApprovalStatus(arInvoice.getInvApprovalStatus());
	        				details.setIpReportParameter(arInvoice.getReportParameter());
	        				
	        				details.setIpInvIsDraft(arInvoice.getInvPosted()==EJBCommon.FALSE?true:false);
	        				try {
	        					LocalAdUser adUser = adUserHome.findByUsrName(arInvoice.getInvCreatedBy(), AD_CMPNY);
	        					details.setIpInvCreatedByDesc(adUser.getUsrDescription());
	        				} catch (FinderException ex) {}
	        				try{
            					System.out.println("adPreference.getPrfApDefaultChecker()(3): "+adPreference.getPrfApDefaultChecker());
                				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
                				details.setIpInvCheckByDescription(adUser2.getUsrDescription());
            				}catch(Exception e){
            					details.setIpInvCheckByDescription("");
            				}

            				try{
            					System.out.println("apCheck.getChkApprovedRejectedBy(3): "+arInvoice.getInvApprovedRejectedBy());
                				LocalAdUser adUser3 = adUserHome.findByUsrName(arInvoice.getInvApprovedRejectedBy(), AD_CMPNY);
                				details.setIpInvApprovedRejectedByDescription(adUser3.getUsrDescription());
            				}catch(Exception e){
            					details.setIpInvApprovedRejectedByDescription("");
            				}

            				details.setIpInvApprovedRejectedBy(arInvoice.getInvApprovedRejectedBy());
	        				details.setIpInvAmount(arInvoice.getInvAmountDue());
	        				details.setIpInvAmountUnearnedInterest(arInvoice.getInvAmountUnearnedInterest());
	        				details.setIpShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);

	        				// added fields
	        				details.setIpInvBillingHeader(arInvoice.getInvBillingHeader());
	        				details.setIpInvBillingFooter(arInvoice.getInvBillingFooter());
	        				details.setIpInvBillingHeader2(arInvoice.getInvBillingHeader2());
	        				details.setIpInvBillingFooter2(arInvoice.getInvBillingFooter2());
	        				details.setIpInvBillingSignatory(arInvoice.getInvBillingSignatory());
	        				details.setIpInvSignatoryTitle(arInvoice.getInvSignatoryTitle());
	        				details.setIpInvBillToAddress(arInvoice.getInvBillToAddress());
	        				details.setIpInvBillToContact(arInvoice.getInvBillToContact());
	        				details.setIpInvBillToAltContact(arInvoice.getInvBillToAltContact());
	        				details.setIpInvBillToPhone(arInvoice.getInvBillToPhone());
	        				details.setIpInvShipToAddress(arInvoice.getInvShipToAddress());
	        				details.setIpInvShipToContact(arInvoice.getInvShipToContact());
	        				details.setIpInvShipToAltContact(arInvoice.getInvShipToAltContact());
	        				details.setIpInvShipToPhone(arInvoice.getInvShipToPhone());
	        				details.setIpIliClientPo(arInvoice.getInvClientPO());
	        				details.setIpInvTotalTax(TOTAL_TAX);

	        				details.setIpIlAmountWoVat(arInvoice.getArTaxCode().getTcName().contains("EXCLUSIVE") ? arJobOrderInvoiceLine.getJilAmount() : arJobOrderInvoiceLine.getJilAmount() + arJobOrderInvoiceLine.getJilTaxAmount());


	        				details.setIpInvCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
	        				details.setIpIlName(arJobOrderLine.getInvItemLocation().getInvItem().getIiName());
	        				details.setIpIlUomShortName(arJobOrderLine.getInvUnitOfMeasure().getUomShortName());

	        				details.setIpInvCustomerDescription(arInvoice.getArCustomer().getCstDescription());
	        				details.setIpInvCustomerTin(arInvoice.getArCustomer().getCstTin());

	        				// add fields discount amount, discount desc, unit of measure, payment term, term desc

	        				LocalArDistributionRecord arDistributionRecord = null;

	        				try {

	        					arDistributionRecord = arDistributionRecordHome.findByDrClassAndInvCode("DISCOUNT", arInvoice.getInvCode(), AD_CMPNY);

	        					details.setIpInvDiscountAmount(arDistributionRecord.getDrAmount());
	        					details.setIpInvDiscountDescription(arInvoice.getAdPaymentTerm().getPytDiscountDescription());

	        				} catch (FinderException ex) {

	        				}

	        				details.setIpInvPytName(arInvoice.getAdPaymentTerm().getPytName());
	        				details.setIpInvPytDescription(arInvoice.getAdPaymentTerm().getPytDescription());

	        				Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();
	        				ArrayList arInvoicePaymentScheduleList = new ArrayList(arInvoicePaymentSchedules);

	        				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
	        					(LocalArInvoicePaymentSchedule)arInvoicePaymentScheduleList.get(arInvoicePaymentScheduleList.size() - 1);

	        				details.setIpInvDueDate(arInvoicePaymentSchedule.getIpsDueDate());
	        				details.setIpInvEffectivityDate(arInvoice.getInvEffectivityDate());

	        				if(arInvoice.getArSalesperson() != null) {

	        					details.setIpSlpSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
	        					details.setIpSlpName(arInvoice.getArSalesperson().getSlpName());

	        				}

	        				details.setIpInvSoNumber(arInvoice.getInvSoNumber());
	        				details.setIpInvJoNumber(arInvoice.getInvJoNumber());

	        				// get unit price w/o VAT

	        				short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

	        				double UNT_PRC = this.calculateIlNetUnitPrice(details, arInvoice.getArTaxCode().getTcRate(),
	        						arInvoice.getArTaxCode().getTcType(), precisionUnit);

	        				details.setIpIlUnitPriceWoVat(UNT_PRC);

	        				details.setIpIliDiscount1(arJobOrderInvoiceLine.getJilDiscount1());
	        				details.setIpIliDiscount2(arJobOrderInvoiceLine.getJilDiscount2());
	        				details.setIpIliDiscount3(arJobOrderInvoiceLine.getJilDiscount3());
	        				details.setIpIliDiscount4(arJobOrderInvoiceLine.getJilDiscount4());

	        				details.setIpInvTcRate(arInvoice.getArTaxCode().getTcRate());
	        				details.setIpInvTaxCode(arInvoice.getArTaxCode().getTcName());
	        				details.setIpInvWithholdingTaxCode(arInvoice.getArWithholdingTaxCode().getWtcName());

	        				// get withholding tax amount
	        				double netAmount = EJBCommon.roundIt(arInvoice.getInvAmountDue() / (1 + (arInvoice.getArTaxCode().getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));
        					details.setIpInvWithholdingTaxAmount(EJBCommon.roundIt(netAmount * (arInvoice.getArWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY)));

        					details.setIpIliTotalDiscount(arJobOrderInvoiceLine.getJilTotalDiscount());

//                			Include Branch
            				LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(arInvoice.getInvAdBranch());
            				details.setIpInvBranchCode(adBranch.getBrBranchCode());
            				details.setIpInvBranchName(adBranch.getBrName());

        					System.out.println("Pasok2");
        					details.setIpInvPartNumber(arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem().getIiPartNumber());

        					details.setIpInvSoReferenceNumber(arJobOrderInvoiceLine.getArJobOrderLine().getArJobOrder().getJoReferenceNumber());

        					details.setIpInvIsDraft(arInvoice.getInvPosted()==EJBCommon.FALSE?true:false);

        					details.setIpJaQuantity(0);
        					
        					
        					
        					
        					//trace misc
                			if(arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem().getIiTraceMisc()==EJBCommon.TRUE) {



        						if(arJobOrderInvoiceLine.getInvTags().size()>0) {
        							System.out.println("new code");
        							StringBuilder strBProperty = new StringBuilder();
        							StringBuilder strBSerial = new StringBuilder();
        							StringBuilder strBSpecs = new StringBuilder();
        							StringBuilder strBCustodian= new StringBuilder();
        							StringBuilder strBExpirationDate = new StringBuilder();
        							Iterator it = arJobOrderInvoiceLine.getInvTags().iterator();


        							while(it.hasNext()) {

        								LocalInvTag invTag = (LocalInvTag)it.next();

        								//property code
        								if(!invTag.getTgPropertyCode().trim().equals("")) {
        									strBProperty.append(invTag.getTgPropertyCode());
        									strBProperty.append(System.getProperty("line.separator"));
        								}

        								//serial

        								if(!invTag.getTgSerialNumber().trim().equals("")) {
        									strBSerial.append(invTag.getTgSerialNumber());
        									strBSerial.append(System.getProperty("line.separator"));
        								}

        								//spec

        								if(!invTag.getTgSpecs().trim().equals("")) {
        									strBSpecs.append(invTag.getTgSpecs());
        									strBSpecs.append(System.getProperty("line.separator"));
        								}

        								//custodian

        								if(invTag.getAdUser()!= null) {
        									strBCustodian.append(invTag.getAdUser().getUsrName());
        									strBCustodian.append(System.getProperty("line.separator"));
        								}

        								//exp date

        								if(invTag.getTgExpiryDate()!=null) {
        									strBExpirationDate.append(invTag.getTgExpiryDate());
        									strBExpirationDate.append(System.getProperty("line.separator"));

        								}



        							}
        							//property code
                					details.setIpIlItemPropertyCode(strBProperty.toString());
                					//serial number
                					details.setIpIlItemSerialNumber(strBSerial.toString());
                					//specs
                					details.setIpIlItemSpecs(strBSpecs.toString());
                					//custodian
                					details.setIpIlItemCustodian(strBCustodian.toString());
                					//expiration date
                					details.setIpIlItemExpiryDate(strBExpirationDate.toString());



        						}


                			}

                			
                			
                			
                			
                			
                			details.setIpIiJobServices(arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem().getIiJobServices());

                			if(arJobOrderInvoiceLine.getArJobOrderLine().getArJobOrderAssignments().size() > 0) {
        						
                
        						Iterator joa = arJobOrderInvoiceLine.getArJobOrderLine().getArJobOrderAssignments().iterator();
        						
        						while(joa.hasNext()) {
        							
        							
        							LocalArJobOrderAssignment jobOrderAssignment = (LocalArJobOrderAssignment)joa.next();
        							
        							ArRepInvoicePrintDetails details2 = (ArRepInvoicePrintDetails)details.clone();
        							
        							if(jobOrderAssignment.getJaSo()==EJBCommon.FALSE) {
        								
        								continue;
        							}
        							
        				
        							
        							details2.setIpJaLine(jobOrderAssignment.getArJobOrderLine().toString());
        							details2.setIpJaIdNumber(jobOrderAssignment.getArPersonel().getPeIdNumber());
        							details2.setIpJaPersonelName(jobOrderAssignment.getArPersonel().getPeName());
        							details2.setIpJaQuantity(jobOrderAssignment.getJaQuantity());
        							details2.setIpJaUnitCost(jobOrderAssignment.getJaUnitCost());
        							details2.setIpJaAmount(jobOrderAssignment.getJaAmount());
        							details2.setIpJaRemarks(jobOrderAssignment.getJaRemarks());
        							details2.setIpJaSo(jobOrderAssignment.getJaSo());
        						
        							
        							list.add(details2);
        							
        						}
        
        						
        						
        				
        					}
                			else {
                				
                			
                				list.add(details);
                			}






	        			}

	        		}

	        	} else {

	        		// ar payment schedule data source

	        		if (!arInvoice.getArInvoicePaymentSchedules().isEmpty()) {

	        			Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();

	        			Iterator ipsIter = arInvoicePaymentSchedules.iterator();

	        			while(ipsIter.hasNext()) {

	        				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)ipsIter.next();
	        				LocalAdUser adUser = adUserHome.findByUsrName(arInvoice.getInvCreatedBy(), AD_CMPNY);

	        				ArRepInvoicePrintDetails details = new ArRepInvoicePrintDetails();

	        				details.setIpInvCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());
	        				details.setIpInvNumber(arInvoice.getInvNumber());
	        				try {
	        					details.setIpInvBatch(arInvoice.getArInvoiceBatch().getIbName());

	        				} catch (Exception ex) {
	        					details.setIpInvBatch("");
	        				}
	        				details.setIpInvCustomerAddress(arInvoice.getArCustomer().getCstAddress());
	        				details.setIpInvCustomerCity(arInvoice.getArCustomer().getCstCity());
	        				details.setIpInvBillToContact(arInvoice.getInvBillToContact());
	        				details.setIpInvBillToAddress(arInvoice.getInvBillToAddress());
	        				details.setIpInvPytName(arInvoice.getAdPaymentTerm().getPytName());
	        				details.setIpInvCreatedBy(adUser.getUsrDescription());
	        				
	        				details.setIpInvIsDraft(arInvoice.getInvPosted()==EJBCommon.FALSE?true:false);
	        				
	        				
	        				
	        				try {
	        					LocalAdUser adUser2 = adUserHome.findByUsrName(arInvoice.getInvCreatedBy(), AD_CMPNY);
	        					details.setIpInvCreatedByDesc(adUser2.getUsrDescription());
	        				} catch (FinderException ex) {}
	        				try{
            					System.out.println("adPreference.getPrfApDefaultChecker()(4): "+adPreference.getPrfApDefaultChecker());
                				LocalAdUser adUser2 = adUserHome.findByUsrName(adPreference.getPrfApDefaultChecker(), AD_CMPNY);
                				details.setIpInvCheckByDescription(adUser2.getUsrDescription());
            				}catch(Exception e){
            					details.setIpInvCheckByDescription("");
            				}

            				try{
            					System.out.println("apCheck.getChkApprovedRejectedBy(4): "+arInvoice.getInvApprovedRejectedBy());
                				LocalAdUser adUser3 = adUserHome.findByUsrName(arInvoice.getInvApprovedRejectedBy(), AD_CMPNY);
                				details.setIpInvApprovedRejectedByDescription(adUser3.getUsrDescription());
            				}catch(Exception e){
            					details.setIpInvApprovedRejectedByDescription("");
            				}
	        				details.setIpInvAmount(arInvoice.getInvAmountPaid());
	        				details.setIpIpsDueDate(arInvoicePaymentSchedule.getIpsDueDate());
	        				details.setIpIpsAmountDue(arInvoicePaymentSchedule.getIpsAmountDue());

	        				//System.out.println("Pasok3");
	        				list.add(details);
	        			}

	        		}

	        	}




	        }

	        if (list.isEmpty()) {
	        	System.out.println("throw new GlobalNoRecordFoundException()");

	        	throw new GlobalNoRecordFoundException();

	        }

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getInvDebitMemo (ArrayList invCodeList, Integer AD_CMPNY){
    	Debug.print("ArRepInvoicePrintControllerBean getInvDebitMemo");

    	LocalArInvoiceHome arInvoiceHome = null;
    	byte debitMemo=0;

    	try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
    	  } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

    	  Iterator i = invCodeList.iterator();

      	while (i.hasNext()) {

      	    Integer INV_CODE = (Integer) i.next();

      	    LocalArInvoice arInvoice = null;


      	    try {

      	    	arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

      	    } catch (FinderException ex) {

      	    	continue;

      	    }



      	  debitMemo = arInvoice.getInvDebitMemo();
      	  break;


      	}

      	return debitMemo;
    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepInvoicePrintSub(ArrayList invCodeList, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArRepInvoicePrintControllerBean executeArRepInvoicePrintSub");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
        LocalAdLookUpValueHome adLookUpValueHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
            adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Iterator i = invCodeList.iterator();

        	while (i.hasNext()) {

        	    Integer INV_CODE = (Integer) i.next();

	        	LocalArInvoice arInvoice = null;

	        	System.out.println("INV_CODE="+INV_CODE);
	        	try {

	        		arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

	        	} catch (FinderException ex) {

	        		continue;

	        	}

	        	LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
				LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("AR INVOICE", AD_CMPNY);

				if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {

	        		if (arInvoice.getInvApprovalStatus() == null ||
	        			arInvoice.getInvApprovalStatus().equals("PENDING")) {

	        		    continue;

	        		}


	        	} else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {

	        		if (arInvoice.getInvApprovalStatus() != null &&
	        			(arInvoice.getInvApprovalStatus().equals("N/A") ||
	        			 arInvoice.getInvApprovalStatus().equals("APPROVED"))) {

	        			continue;

	        		}

	        	}

	        	if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE &&
	        		arInvoice.getInvPrinted() == EJBCommon.TRUE){

	        		continue;

	        	}

	        	// set printed

	        	arInvoice.setInvPrinted(EJBCommon.TRUE);

	        	// get invoice lines

	        	if (!arInvoice.getArInvoiceLineItems().isEmpty()) {

	        		Collection adLookupValues = null;

	        		try {

	        			adLookupValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);

	        		} catch(FinderException ex) {

	        		}

	        		Iterator lookUpIter = adLookupValues.iterator();

	        		while(lookUpIter.hasNext()) {

	        			LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) lookUpIter.next();
	        			String CTGRY_NM = adLookUpValue.getLvName();

	        			Collection arInvoiceLineItems = null;

	        			try {

	        				arInvoiceLineItems = arInvoiceLineItemHome.findByInvCodeAndIlIiLvCtgry(arInvoice.getInvCode(), CTGRY_NM, AD_CMPNY);

	        			} catch (FinderException ex) {

	        			}

	        			//calculate total amount per item category

	        			double TOTAL_PER_CATEGORY = 0d;
	        			Iterator arIliIter = arInvoiceLineItems.iterator();

	        			while(arIliIter.hasNext()) {

	        				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) arIliIter.next();

	        				TOTAL_PER_CATEGORY += arInvoiceLineItem.getIliAmount();

	        			}

	        			ArRepInvoicePrintDetails details = new ArRepInvoicePrintDetails();

	        			details.setIpIlCategory(CTGRY_NM);
	        			details.setIpIlAmount(TOTAL_PER_CATEGORY);
	        			details.setIpInvDate(arInvoice.getInvDate());
	        			list.add(details);

	        		}

	        	}

	        }

	        if (list.isEmpty()) {

	        	System.out.println("executeArRepInvoicePrintSub throw new GlobalNoRecordFoundException()");
	        	throw new GlobalNoRecordFoundException();

	        }

        	return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepInvoicePrintSubDr(ArrayList invCodeList, Integer AD_CMPNY) {

        Debug.print("ArRepInvoicePrintControllerBean executeArRepInvoicePrintSubDr");

        LocalArInvoiceHome arInvoiceHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Iterator i = invCodeList.iterator();

        	while (i.hasNext()) {

        	    Integer INV_CODE = (Integer) i.next();

	        	LocalArInvoice arInvoice = null;

	        	try {

	        		arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

	        	} catch (FinderException ex) {

	        		continue;

	        	}

				// set printed

	        	arInvoice.setInvPrinted(EJBCommon.TRUE);

	        	//get invoice distribution records

	        	Collection arDistributionRecords = arInvoice.getArDistributionRecords();

	        	Iterator drIter = arDistributionRecords.iterator();

	        	while(drIter.hasNext()) {

	        		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drIter.next();

	        		ArRepInvoicePrintDetails details = new ArRepInvoicePrintDetails();

	        		details.setIpDrCoaAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
	        		details.setIpDrAmount(arDistributionRecord.getDrAmount());
	        		details.setIpDrDebit(arDistributionRecord.getDrDebit());

	        		list.add(details);

	        	}

	        }

	        if (list.isEmpty()) {

	        	throw new GlobalNoRecordFoundException();

	        }

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

	  Debug.print("ArRepInvoicePrintControllerBean getAdCompany");

	  LocalAdCompanyHome adCompanyHome = null;

	  // Initialize EJB Home

	  try {

	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

	  try {

	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpTin(adCompany.getCmpTin());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     details.setCmpFax(adCompany.getCmpFax());


	     return details;

	  } catch (Exception ex) {

	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());

	  }

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public String getPrfArSalesInvoiceDataSource(Integer AD_CMPNY) {

		Debug.print("ArRepInvoicePrintControllerBean getArSalesInvoiceDataSource");

		LocalAdPreferenceHome adPreferenceHome = null;

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return  adPreference.getPrfArSalesInvoiceDataSource();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}


	}

	// private methods

    private double calculateIlNetUnitPrice(ArRepInvoicePrintDetails mdetails, double tcRate, String tcType, short precisionUnit) {

		double amount = mdetails.getIpIlUnitPrice();

		if (tcType.equals("INCLUSIVE")) {

			amount = EJBCommon.roundIt(mdetails.getIpIlUnitPrice() / (1 + (tcRate / 100)), precisionUnit);

	    }

		return amount;

	}

    private short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

          return  adCompany.getGlFunctionalCurrency().getFcPrecision();

        } catch (Exception ex) {

        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());

        }

     }



	// private methods
		private ArrayList convertMiscToArrayList(String misc) {


			Debug.print("ArRepSalesOrderControllerBean convertMiscToArrayList");

	        ArrayList list = new ArrayList();

	        if(misc==null) {
	        	return list;
	        }
			//String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
			String propertyCode = "";
			String serialNumber = "";
			String specs = "";
			String custodian = "";
			String expiryDate = "";
			String tgDocumentNumber = "";

			StringBuilder strB = new StringBuilder();

			//quantity
		    String[] arrayMisc = misc.split("_");


			//property code
			misc = arrayMisc[1];
			arrayMisc = misc.split("@");
	  	   	String[] valueList = arrayMisc[0].split(",");
	  	    strB = new StringBuilder();
	  	   	for(int x=0;x<valueList.length;x++) {
	  	   		if(!valueList[x].trim().equals("")) {
		  	   		strB.append(valueList[x]);
		  	   		strB.append(System.getProperty("line.separator"));
	  	   		}

	  	   	}
	  	   	propertyCode = strB.toString();
	  	   	list.add(propertyCode);

	  	   	//serialNumber
	  	   	misc = arrayMisc[1];
	  	   	arrayMisc = misc.split("<");
	  	    valueList = arrayMisc[0].split(",");
	  	    strB = new StringBuilder();
	  	   	for(int x=0;x<valueList.length;x++) {
		  	   	if(!valueList[x].trim().equals("")) {
			  	   	strB.append(valueList[x]);
		  	   		strB.append(System.getProperty("line.separator"));

		  	   	}

	  	   	}
	  	    serialNumber= strB.toString();
	  	    System.out.println("serial no: " + serialNumber);
	  	  list.add(serialNumber);

	  	    //specs
	  	   	misc = arrayMisc[1];
	  	   	arrayMisc = misc.split(">");
	  	   	valueList = arrayMisc[0].split(",");
	  	   	strB = new StringBuilder();
		   	for(int x=0;x<valueList.length;x++) {
		   	 	if(!valueList[x].trim().equals("")) {
			  	   	strB.append(valueList[x]);
		  	   		strB.append(System.getProperty("line.separator"));

		  	   	}
		   	}
		    specs= strB.toString();

		    list.add(specs);

		    //custodian
	  	   	misc = arrayMisc[1];
	  	   	arrayMisc = misc.split(";");
	  	    valueList = arrayMisc[0].split(",");
		  	strB = new StringBuilder();
		   	for(int x=0;x<valueList.length;x++) {
		   	 	if(!valueList[x].trim().equals("")) {
			  	   	strB.append(valueList[x]);
		  	   		strB.append(System.getProperty("line.separator"));

		  	   	}
		   	}
		    custodian= strB.toString();
		    list.add(custodian);


	  	   	//expiryDate

	  	   	misc = arrayMisc[1];
	  	   	arrayMisc = misc.split("%");
	  	    valueList = arrayMisc[0].split(",");
		  	strB = new StringBuilder();
		   	for(int x=0;x<valueList.length;x++) {
		   	 	if(!valueList[x].trim().equals("")) {
			  	   	strB.append(valueList[x]);
		  	   		strB.append(System.getProperty("line.separator"));

		  	   	}
		   	}
		    expiryDate= strB.toString();
		    list.add(expiryDate);





			return list;




		}


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepInvoicePrintControllerBean ejbCreate");

    }
}