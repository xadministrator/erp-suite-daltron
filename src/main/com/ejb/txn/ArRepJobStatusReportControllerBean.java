
/*
 * ArRepJobStatusControllerBean.java
 *
 * Created on April 23, 2007, 5:52 PM
 *
 * @author  Ruben P. Lamberte
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArJobOrder;
import com.ejb.ar.LocalArJobOrderAssignment;
import com.ejb.ar.LocalArJobOrderHome;
import com.ejb.ar.LocalArJobOrderInvoiceLine;
import com.ejb.ar.LocalArJobOrderInvoiceLineHome;
import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArJobOrderLineHome;
import com.ejb.ar.LocalArJobOrderType;
import com.ejb.ar.LocalArJobOrderTypeHome;
import com.ejb.ar.LocalArPersonel;
import com.ejb.ar.LocalArPersonelHome;
import com.ejb.ar.LocalArPersonelType;
import com.ejb.ar.LocalArPersonelTypeHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArPersonelTypeDetails;
import com.util.ArRepJobOrderPrintDetails;
import com.util.ArRepJobStatusReportDetails;

import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModTagListDetails;

/**
 * @ejb:bean name="ArRepJobStatusReportControllerEJB"
 *           display-name="Used for viewing sales orders"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepJobStatusReportControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepJobStatusReportController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepJobStatusReportControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 */

public class ArRepJobStatusReportControllerBean extends AbstractSessionBean {


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerBatchAll(Integer AD_CMPNY) {

		Debug.print("ArRepJobStatusReportControllerBean getAdLvCustomerBatchAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArCcAll(Integer AD_CMPNY) {

		Debug.print("ArRepJobStatusReportControllerBean getArCcAll");

		LocalArCustomerClassHome arCustomerClassHome = null;
		LocalArCustomerClass arCustomerClass = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);

			Iterator i = arCustomerClasses.iterator();

			while (i.hasNext()) {

				arCustomerClass = (LocalArCustomerClass)i.next();

				list.add(arCustomerClass.getCcName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArCtAll(Integer AD_CMPNY) {

		Debug.print("ArRepJobStatusReportControllerBean getArCtAll");

		LocalArCustomerTypeHome arCustomerTypeHome = null;
		LocalArCustomerType arCustomerType = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

			Iterator i = arCustomerTypes.iterator();

			while (i.hasNext()) {

				arCustomerType = (LocalArCustomerType)i.next();

				list.add(arCustomerType.getCtName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/

	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException{

		Debug.print("ArRepJobStatusReportControllerBean getAdBrResAll");

		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;

		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;

		Collection adBranchResponsibilities = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (adBranchResponsibilities.isEmpty()) {

			throw new GlobalNoRecordFoundException();

		}

		try {

			Iterator i = adBranchResponsibilities.iterator();

			while(i.hasNext()) {

				adBranchResponsibility = (LocalAdBranchResponsibility)i.next();

				adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());

				AdBranchDetails details = new AdBranchDetails();
				details.setBrCode(adBranch.getBrCode());
				details.setBrBranchCode(adBranch.getBrBranchCode());
				details.setBrName(adBranch.getBrName());
				details.setBrHeadQuarter(adBranch.getBrHeadQuarter());

				list.add(details);

			}

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		return list;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeArRepJobStatusReport(HashMap criteria, ArrayList branchList, String personelName, String invoiceStatus, String ORDER_BY, String GROUP_BY, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ArRepJobStatusReportControllerBean executeArRepJobStatusReport");

		
		LocalArJobOrderHome arJobOrderHome = null;
		LocalArJobOrderLineHome arJobOrderLineHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalArJobOrderInvoiceLineHome arJobOrderInvoiceLineHome = null;

		ArrayList list = new ArrayList();

		//initialized EJB Home

		try {

			arJobOrderHome = (LocalArJobOrderHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);
			arJobOrderLineHome = (LocalArJobOrderLineHome)EJBHomeFactory.
					lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arJobOrderInvoiceLineHome = (LocalArJobOrderInvoiceLineHome)EJBHomeFactory.
					lookUpLocalHome(LocalArJobOrderInvoiceLineHome.JNDI_NAME, LocalArJobOrderInvoiceLineHome.class);

		} catch (NamingException ex) 	{

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size() ;

			StringBuffer jbossQl = new StringBuffer();
			Iterator brIter = null;
			AdBranchDetails details = null;
			Object obj[];
			Collection arJobOrderLines = null;
			 
			
			if (criteria.containsKey("jobOrderType")) {

				criteriaSize--;

			}
			
			
			if (criteria.containsKey("jobOrderStatus")) {

				criteriaSize--;

			}
			
			if (criteria.containsKey("personelType")) {

				criteriaSize--;

			}

			if (criteria.containsKey("customerCode")) {

				criteriaSize--;

			}

			if (((String)criteria.get("approvalStatus")).equals("")) {

				criteriaSize--;

			}

			//if (((String)criteria.get("orderStatus")).equals("")) {

			criteriaSize--;
			criteriaSize--;

			//}



			jbossQl.append("SELECT OBJECT(jol) FROM ArJobOrderLine jol ");
		

	
	

			// Allocate the size of the object parameter
			
			obj = new Object[criteriaSize];

			if (criteria.containsKey("jobOrderType")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("jol.arJobOrder.arJobOrderType.jotName LIKE '%" + (String)criteria.get("jobOrderType") + "%' ");

			}
			
			
			
			obj = new Object[criteriaSize];

			if (criteria.containsKey("jobOrderStatus")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("jol.arJobOrder.joJobOrderStatus = '" + (String)criteria.get("jobOrderStatus") + "' ");

			}
			
			
			

	

			if (criteria.containsKey("customerCode")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("jol.arJobOrder.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

			}

			if (criteria.containsKey("customerType")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("jol.arJobOrder.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("customerType");
				ctr++;

			}

			if (criteria.containsKey("customerBatch")) {

			   	  if (!firstArgument) {
			   	     jbossQl.append("AND ");
			   	  } else {
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");
			   	  }

			   	  jbossQl.append("jol.arJobOrder.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("customerBatch");
			   	  ctr++;

		      }

			if (criteria.containsKey("customerClass")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("jol.arJobOrder.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("customerClass");
				ctr++;

			}

			if (criteria.containsKey("dateFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jol.arJobOrder.joDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;

			}

			if (criteria.containsKey("dateTo")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jol.arJobOrder.joDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;

			}

			if (criteria.containsKey("documentNumberFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jol.arJobOrder.joDocumentNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberFrom");
				ctr++;
			}


			if (criteria.containsKey("documentNumberTo")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jol.arJobOrder.joDocumentNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberTo");
				ctr++;

			}


			if (criteria.containsKey("referenceNumberFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jol.arJobOrder.joReferenceNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("referenceNumberFrom");
				ctr++;
			}


			if (criteria.containsKey("referenceNumberTo")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jol.arJobOrder.joReferenceNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("referenceNumberTo");
				ctr++;

			}

			if (criteria.containsKey("approvalStatus")) {

				if (((String)criteria.get("approvalStatus")).equals("N/A") ||
						((String)criteria.get("approvalStatus")).equals("PENDING")||
						((String)criteria.get("approvalStatus")).equals("APPROVED")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}

					jbossQl.append("jol.arJobOrder.joApprovalStatus=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("approvalStatus");
					ctr++;
				}

			}
			
			
			
			
			
			

            if (criteria.containsKey("includedUnposted")) {

		  		String unposted = (String)criteria.get("includedUnposted");

		  		if (!firstArgument) {

		  			jbossQl.append("AND ");

		  		} else {

		  			firstArgument = false;
		  			jbossQl.append("WHERE ");

		  		}

		  		if (unposted.equals("NO")) {

		  			jbossQl.append("jol.arJobOrder.joPosted = 1 ");

		  		} else {

		  			jbossQl.append("jol.arJobOrder.joVoid = 0 ");

		  		}

		  	}

		

			if (criteria.containsKey("orderStatus")) {
				System.out.println("ORDER STATUS : " + (String)criteria.get("orderStatus"));

				if (!((String)criteria.get("orderStatus")).equals("")){
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					/*
					System.out.println("ORDER STATUS : " + (String)criteria.get("orderStatus"));
					jbossQl.append("so.soOrderStatus LIKE ? 'BAD%' " + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("orderStatus");
					ctr++;*/
					if (criteria.get("orderStatus").equals("Good")){
						jbossQl.append("jol.arJobOrder.joOrderStatus = 'Good' ");
					}else{
						//System.out.println("ORDER STATUS : " + (String)criteria.get("orderStatus"));
						jbossQl.append("jol.arJobOrder.joOrderStatus LIKE 'Bad%' ");

					}

				}

			}

			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			jbossQl.append("jol.arJobOrder.joAdCompany=" + AD_CMPNY + " ");

			System.out.println("jbossQl-" + jbossQl.toString());
			System.out.println("obj size :" + obj.length);
			arJobOrderLines = arJobOrderHome.getJOByCriteria(jbossQl.toString(), obj);
	
			double ORDER_AMOUNT = 0d;
			double ORDER_TAX_AMOUNT = 0d;
			double ORDER_QTY = 0d;
			double INVOICE_QTY = 0d;
			Integer JO_CODE = 0;
			
			if(!arJobOrderLines.isEmpty()) {

				Iterator i = arJobOrderLines.iterator();

				while (i.hasNext()) {
					
					
					

					LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine)i.next();
					
				
					
					
					ArRepJobStatusReportDetails mdetails = new ArRepJobStatusReportDetails();
			
					
					LocalArJobOrder arJobOrder = arJobOrderLine.getArJobOrder();
					
					
					
					if(JO_CODE!=arJobOrder.getJoCode()) {
					
						ORDER_AMOUNT = 0d;
						ORDER_TAX_AMOUNT = 0d;
						ORDER_QTY = 0d;
						INVOICE_QTY = 0d;
						
						JO_CODE = arJobOrder.getJoCode();
					}
					
					
					mdetails.setJoDate(arJobOrder.getJoDate());
					mdetails.setJoType(arJobOrder.getArJobOrderType()==null?null:arJobOrder.getArJobOrderType().getJotName());
					mdetails.setJoDocumentNumber(arJobOrder.getJoDocumentNumber());
					mdetails.setJoReferenceNumber(arJobOrder.getJoReferenceNumber());
					mdetails.setJoDescription(arJobOrder.getJoDescription());
					mdetails.setJoCstCustomerCode(arJobOrder.getArCustomer().getCstCustomerCode() + "-" + arJobOrder.getArCustomer().getCstName());
					mdetails.setJoCstCustomerClass(arJobOrder.getArCustomer().getArCustomerClass().getCcName());
					mdetails.setJoCstCustomerCode2(arJobOrder.getArCustomer().getCstCustomerCode());
				    mdetails.setJoJobOrderStatus(arJobOrder.getJoJobOrderStatus());
					mdetails.setJoSlsSalespersonCode(arJobOrder.getArSalesperson() != null ? arJobOrder.getArSalesperson().getSlpSalespersonCode() : null);

					mdetails.setJoSlsName(arJobOrder.getArSalesperson() != null ? arJobOrder.getArSalesperson().getSlpName() : null);
	

                    // select customer type
					if (arJobOrder.getArCustomer().getArCustomerType() == null) {

						mdetails.setJoCstCustomerType("UNDEFINED");

					} else {

						mdetails.setJoCstCustomerType(arJobOrder.getArCustomer().getArCustomerType().getCtName());

					}

					
					
					ORDER_QTY += arJobOrderLine.getJolQuantity();
  					
  					if (arJobOrderLine.getArJobOrder().getArTaxCode().getTcType()
  							.equals("INCLUSIVE")) {

  						ORDER_TAX_AMOUNT += arJobOrderLine.getJolAmount() - EJBCommon.roundIt(arJobOrderLine.getJolAmount() / (1 + (arJobOrderLine.getArJobOrder().getArTaxCode().getTcRate() / 100)), adCompany.getGlFunctionalCurrency().getFcPrecision());
  						ORDER_AMOUNT += arJobOrderLine.getJolAmount();

  			        } else if (arJobOrderLine.getArJobOrder().getArTaxCode().getTcType()
  							.equals("arJobOrderLine")){

  			            // tax exclusive, none, zero rated or exempt

  			            ORDER_TAX_AMOUNT += EJBCommon.roundIt(arJobOrderLine.getJolAmount() * arJobOrderLine.getArJobOrder().getArTaxCode().getTcRate() / 100, adCompany.getGlFunctionalCurrency().getFcPrecision());
  			            ORDER_AMOUNT += arJobOrderLine.getJolAmount() + ORDER_TAX_AMOUNT;

  			        } else {

  			        	ORDER_AMOUNT += arJobOrderLine.getJolAmount();

  			        }
					
					mdetails.setJoOrderQty(ORDER_QTY);
					mdetails.setJoAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
  							arJobOrder.getGlFunctionalCurrency().getFcCode(),
  							arJobOrder.getGlFunctionalCurrency().getFcName(),
  							arJobOrder.getJoConversionDate(),
  							arJobOrder.getJoConversionRate(),
							ORDER_AMOUNT, AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));

					mdetails.setJoTaxAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
  							arJobOrder.getGlFunctionalCurrency().getFcCode(),
  							arJobOrder.getGlFunctionalCurrency().getFcName(),
  							arJobOrder.getJoConversionDate(),
  							arJobOrder.getJoConversionRate(),
							ORDER_TAX_AMOUNT, AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					mdetails.setOrderBy(ORDER_BY);
					mdetails.setJoOrderStatus(arJobOrder.getJoOrderStatus());
					mdetails.setJoApprovalStatus(arJobOrder.getJoApprovalStatus());
					mdetails.setJoApprovedRejectedBy(arJobOrder.getJoApprovedRejectedBy());
					
					mdetails.setJolIIName(arJobOrderLine.getInvItemLocation().getInvItem().getIiName());
					mdetails.setJolDescription(arJobOrderLine.getInvItemLocation().getInvItem().getIiDescription());
					mdetails.setJolQuantity(arJobOrderLine.getJolQuantity());
					mdetails.setJolUnitPrice(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
  							arJobOrder.getGlFunctionalCurrency().getFcCode(),
  							arJobOrder.getGlFunctionalCurrency().getFcName(),
  							arJobOrder.getJoConversionDate(),
  							arJobOrder.getJoConversionRate(),
							arJobOrderLine.getJolUnitPrice(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					mdetails.setJolAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
  							arJobOrder.getGlFunctionalCurrency().getFcCode(),
  							arJobOrder.getGlFunctionalCurrency().getFcName(),
  							arJobOrder.getJoConversionDate(),
  							arJobOrder.getJoConversionRate(),
							arJobOrderLine.getJolAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					mdetails.setJolUnit(arJobOrderLine.getInvUnitOfMeasure().getUomName());
					
					
				
					if(arJobOrderLine.getArJobOrderAssignments().size() > 0) {
						Collection arJobOrderAssignments = arJobOrderLine.getArJobOrderAssignments();
    					
    					
    					Iterator jai = arJobOrderAssignments.iterator();
    					
    					boolean isFirst = true;
    					while(jai.hasNext()) {
    						
    						LocalArJobOrderAssignment arJobOrderAssignment = (LocalArJobOrderAssignment)jai.next();
    						
    		
    						ArRepJobStatusReportDetails mdetails2 =(ArRepJobStatusReportDetails) mdetails.clone();
    						isFirst =false;
    						if(!isFirst) {
    							mdetails.setJolIIName(null);
    							mdetails.setJolDescription(null);
    							mdetails.setJolQuantity(0);
    							mdetails.setJolUnitPrice(0);
    							mdetails.setJolAmount(0);
    							mdetails.setJolUnit(null);
    							
    						}
    						
    						
    						
    						if(arJobOrderAssignment.getJaSo()==EJBCommon.FALSE) {
    							continue;
    						}
    						
    						if(!personelName.equals("") && !personelName.equals(arJobOrderAssignment.getArPersonel().getPeName())) {
    							continue;
    						}
    						mdetails2.setJaPersonelCode(arJobOrderAssignment.getArPersonel().getPeIdNumber());
    						mdetails2.setJaPersonelName(arJobOrderAssignment.getArPersonel().getPeName());
    						mdetails2.setJaQuantity(arJobOrderAssignment.getJaQuantity());
    						mdetails2.setJaUnitCost(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    	  							arJobOrder.getGlFunctionalCurrency().getFcCode(),
    	  							arJobOrder.getGlFunctionalCurrency().getFcName(),
    	  							arJobOrder.getJoConversionDate(),
    	  							arJobOrder.getJoConversionRate(),
    	  							arJobOrderAssignment.getJaUnitCost(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
    						mdetails2.setJaAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    	  							arJobOrder.getGlFunctionalCurrency().getFcCode(),
    	  							arJobOrder.getGlFunctionalCurrency().getFcName(),
    	  							arJobOrder.getJoConversionDate(),
    	  							arJobOrder.getJoConversionRate(),
    	  							arJobOrderAssignment.getJaAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
    						mdetails2.setJaRemarks(arJobOrderAssignment.getJaRemarks());
    					
    						
    						
                			list.add(mdetails2);
    						
    					}
    					
						
						
					}else {

						if(!personelName.equals("")) {
							continue;
						}
						mdetails.setJaPersonelCode(null);
						mdetails.setJaPersonelName(null);
						mdetails.setJaQuantity(0);
						mdetails.setJaUnitCost(0);
						mdetails.setJaAmount(0);
						mdetails.setJaRemarks(null);
						list.add(mdetails);
						
					}
					
					
					
					
					
					
					

					/*System.out.print("ORDER_AMOUNT-" + ORDER_AMOUNT);
					System.out.print("ORDER_TAX_AMOUNT-" + ORDER_TAX_AMOUNT);

					String invoiceNumbers = "";
					if(true){
						Object invObj[]= new Object[2];
						invObj[0] = arSalesOrder.getSoDocumentNumber();
						invObj[1] = AD_CMPNY;

						String invJbossQl = "SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invSoNumber=?1 AND inv.invAdCompany=?2 ";
						Iterator invIter = arInvoiceHome.getInvByCriteria(invJbossQl.toString(), invObj).iterator();

						while(invIter.hasNext()){

							LocalArInvoice arInvoice = (LocalArInvoice)invIter.next();

							invoiceNumbers += invoiceNumbers + "/" + arInvoice.getInvNumber() ;

						}
					}

					// Eliminate Trailing Slash Char
					if(!invoiceNumbers.equals("")){

						invoiceNumbers = invoiceNumbers.substring(1);

					}

					if(invoiceStatus.equals("SERVED") && invoiceNumbers.equals("")){
						continue;
					} else if(invoiceStatus.equals("UNSERVED") && !invoiceNumbers.equals("")){
						continue;
					}

					Collection arSoLines = arSalesOrder.getArSalesOrderLines();

					Iterator solIter = arSoLines.iterator();

					String Approver = "";

					if(arSalesOrder.getSoApprovalStatus()!=null && arSalesOrder.getSoApprovalStatus().equals("PENDING")){
						try {

							Collection adApprovalQueueList = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR SALES ORDER", arSalesOrder.getSoCode(), AD_CMPNY);

							if(adApprovalQueueList.iterator().hasNext()){
								LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue) adApprovalQueueList.iterator().next();
								Approver = adApprovalQueue.getAdUser().getUsrName();
							}



						} catch (FinderException ex) {

							throw new GlobalRecordAlreadyDeletedException();

						}
					}

					while(solIter.hasNext()){

						LocalArSalesOrderLine soLine = (LocalArSalesOrderLine)solIter.next();

						ArRepSalesOrderDetails tmpdetails = new ArRepSalesOrderDetails();
						tmpdetails.setSoDate(mdetails.getSoDate());
						tmpdetails.setSoDocumentNumber(mdetails.getSoDocumentNumber());
						tmpdetails.setSoReferenceNumber(mdetails.getSoReferenceNumber());
						tmpdetails.setSoDescription(mdetails.getSoReferenceNumber());
						tmpdetails.setSoCstCustomerCode(mdetails.getSoCstCustomerCode());
						tmpdetails.setSoCstCustomerClass(mdetails.getSoCstCustomerClass());
						tmpdetails.setSoCstCustomerCode2(mdetails.getSoCstCustomerCode2());
						tmpdetails.setSoSlsSalespersonCode(mdetails.getSoSlsSalespersonCode());

						tmpdetails.setSoSlsName(mdetails.getSoSlsName());
						tmpdetails.setSoCstCustomerType(mdetails.getSoCstCustomerType());
						tmpdetails.setSoAmount(mdetails.getSoAmount());
						tmpdetails.setSoTaxAmount(mdetails.getSoTaxAmount());
						tmpdetails.setOrderBy(mdetails.getOrderBy());
						tmpdetails.setSolIIName(soLine.getInvItemLocation().getInvItem().getIiName());
						tmpdetails.setSolQty(soLine.getSolQuantity());
						tmpdetails.setSoOrderStatus(soLine.getArSalesOrder().getSoOrderStatus());
						tmpdetails.setSoApprovalStatus(soLine.getArSalesOrder().getSoApprovalStatus());
						tmpdetails.setSoApprovedRejectedBy(soLine.getArSalesOrder().getSoApprovedRejectedBy());


						if(!Approver.equals(""))
							tmpdetails.setSoApprovedRejectedBy(Approver);

						tmpdetails.setSoInvoiceNumbers(invoiceNumbers);

						list.add(tmpdetails);
					}*/



				}

			}

			if(list.isEmpty() || list.size() == 0) {

				throw new GlobalNoRecordFoundException();

			}

			// sort
			

			return list;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {


			ex.printStackTrace();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

		Debug.print("ArRepJobStatusReportControllerBean getAdCompany");

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());

			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArJotAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepJobStatusReportControllerBean getArJotAll");

		
		LocalArJobOrderTypeHome arJobOrderTypeHome = null;
		
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arJobOrderTypeHome = (LocalArJobOrderTypeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArJobOrderTypeHome.JNDI_NAME, LocalArJobOrderTypeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arJobOrderTypes = arJobOrderTypeHome.findJotAll((AD_CMPNY));

			Iterator i = arJobOrderTypes.iterator();

			while (i.hasNext()) {

				LocalArJobOrderType jobOrderType = (LocalArJobOrderType)i.next();

				list.add(jobOrderType.getJotName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
		
	}

	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArPrsnlAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepJobStatusReportControllerBean getArPrsnlAll");
		
		LocalArPersonelHome	arPersonelHome = null;
		
		
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
			lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arPersonels = arPersonelHome.findPeAll(AD_CMPNY);

			Iterator i = arPersonels.iterator();

			while (i.hasNext()) {

				LocalArPersonel arPersonel = (LocalArPersonel)i.next();

				list.add(arPersonel.getPeName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
		
	}

	
	

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArSlpAll(Integer AD_CMPNY) {

		Debug.print("ArRepJobStatusReportControllerBean getArSlpAll");

		LocalArSalespersonHome arSalespersonHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arSalespersons = arSalespersonHome.findSlpAll(AD_CMPNY);

			Iterator i = arSalespersons.iterator();

			while (i.hasNext()) {

				LocalArSalesperson arSalesperson = (LocalArSalesperson)i.next();

				list.add(arSalesperson.getSlpName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}











	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("ArRepJobStatusReportControllerBean convertForeignToFunctionalCurrency");


		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}


		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

			AMOUNT = AMOUNT / CONVERSION_RATE;

		}
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public ArrayList getAllJobOrderTypeName(Integer AD_CMPNY) {

		Debug.print("ArRepPersonelSummaryControllerBean getAllJobOrderType");

		LocalArJobOrderTypeHome arJobOrderTypeHome = null;

		//Initialize EJB Home

		try {

			arJobOrderTypeHome = (LocalArJobOrderTypeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderTypeHome.JNDI_NAME, LocalArJobOrderTypeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arJobOrderTypes = arJobOrderTypeHome.findJotAll(AD_CMPNY);
			ArrayList list = new ArrayList();


			Iterator i = arJobOrderTypes.iterator();

			while(i.hasNext()) {

				LocalArJobOrderType arJobOrderType = (LocalArJobOrderType)i.next();

				list.add(arJobOrderType.getJotName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArPeAll(Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("ArRepPersonelSummaryControllerBean getApSplAll");
		
		LocalArPersonelHome arPersonelHome = null;
		

		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
				lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection arPersonels = arPersonelHome.findPeAll( AD_CMPNY);
			
			Iterator i = arPersonels.iterator();
			
			while (i.hasNext()) {
				
				LocalArPersonel arPersonel = (LocalArPersonel)i.next();
				
				list.add(arPersonel.getPeName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	 /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvJobOrderStatus(Integer AD_CMPNY) {

		Debug.print("ArRepJobStatusReportControllerBean getAdLvJobOrderStatus");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			String lookUpName = "AR JOB ORDER STATUS";



			Collection adLookUpValues = adLookUpValueHome.findByLuName(lookUpName, AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	
	  
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArPtAll(Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArRepJobStatusReportControllerBean getArPtAll");

        LocalArPersonelTypeHome arPersonelTypeHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	arPersonelTypeHome = (LocalArPersonelTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPersonelTypeHome.JNDI_NAME, LocalArPersonelTypeHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	
	        Collection arPersonelTypes = arPersonelTypeHome.findPtAll(AD_CMPNY);

	        if (arPersonelTypes.isEmpty()) {

	            throw new GlobalNoRecordFoundException();

	        }

	        Iterator i = arPersonelTypes.iterator();

	        while (i.hasNext()) {

	        	LocalArPersonelType arPersonelType = (LocalArPersonelType)i.next();

	        	
       		list.add(arPersonelType.getPtName());

	        }

	        return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ArRepJobStatusReportControllerBean ejbCreate");

	}
}
