
/*
 * ArRepOrRegisterControllerBean.java
 *
 * Created on March 12, 2004, 2:27 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArPdc;
import com.ejb.ar.LocalArPdcHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepOrRegisterDetails;
import com.util.ArRepSalesRegisterDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepOrRegisterControllerEJB"
 *           display-name="Used for viewing offial receipts"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepOrRegisterControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepOrRegisterController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepOrRegisterControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepOrRegisterControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerBatchAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepOrRegisterControllerBean getAdLvCustomerBatchAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArRepOrRegisterControllerBean getArCcAll");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalArCustomerClass arCustomerClass = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);
	        
	        Iterator i = arCustomerClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerClass = (LocalArCustomerClass)i.next();
	        	
	        	list.add(arCustomerClass.getCcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCtAll(Integer AD_CMPNY) {
                    
        Debug.print("ArRepOrRegisterControllerBean getArCtAll");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalArCustomerType arCustomerType = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

	        Iterator i = arCustomerTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerType = (LocalArCustomerType)i.next();
	        	
	        	list.add(arCustomerType.getCtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("ArRepOrRegisterControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccount adBankAccount = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = adBankAccounts.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	adBankAccount = (LocalAdBankAccount)i.next();
	        	
	        	list.add(adBankAccount.getBaName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArRepOrRegisterControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepOrRegister(HashMap criteria, ArrayList branchList, String ORDER_BY, String GROUP_BY, 
    		boolean detailedReport, boolean SHOW_ENTRIES, boolean SUMMARIZE, boolean INCLUDE_PR, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepOrRegisterControllerBean executeArRepOrRegister");
        
        LocalArReceiptHome arReceiptHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalArPdcHome arPdcHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            arPdcHome = (LocalArPdcHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArPdcHome.JNDI_NAME, LocalArPdcHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
		
		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           
		  boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct WHERE (");
		  
		  if (branchList.isEmpty())
		  	throw new GlobalNoRecordFoundException();

		  Iterator brIter = branchList.iterator();
		  
		  AdBranchDetails details = (AdBranchDetails)brIter.next();
		  jbossQl.append("rct.rctAdBranch=" + details.getBrCode());
		  
		  while(brIter.hasNext()) {
		  	
		  		details = (AdBranchDetails)brIter.next();
		  		
		  		jbossQl.append(" OR rct.rctAdBranch=" + details.getBrCode());
		  	
		  }
		  
		  jbossQl.append(") ");
		  firstArgument = false;
		  
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("customerCode")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	     
	      if (criteria.containsKey("unpostedOnly")) {

	    	  criteriaSize--;

	      }

	      if (criteria.containsKey("includedUnposted")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
          
          if (criteria.containsKey("includedMiscReceipts")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
          
          if (criteria.containsKey("detailedReport")) {
  	      	
 	      	 criteriaSize--;
 	      	 
 	      }
          
          obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("customerCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("rct.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("receiptBatchName")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("rct.arReceiptBatch.rbName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("receiptBatchName");
				ctr++;
				
			}

		  if (criteria.containsKey("bankAccount")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("rct.adBankAccount.baName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("bankAccount");
		   	  ctr++;
	   	  
	      }

		  if (criteria.containsKey("customerBatch")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("rct.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerBatch");
		   	  ctr++;
	   	  
	      }
		  
    	  if (criteria.containsKey("customerType")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("rct.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerType");
		   	  ctr++;
	   	  
	      }	
    	  
			
		  if (criteria.containsKey("customerClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("rct.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerClass");
		   	  ctr++;
	   	  
	      }		
		 
			if (criteria.containsKey("salesperson")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("rct.arSalesperson.slpName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("salesperson");
				ctr++;

			}
          			
	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }
		  
		  if (criteria.containsKey("receiptType")) {
		  	
		  	if (!firstArgument) {
		  		jbossQl.append("AND ");
		  	} else {
		  		firstArgument = false;
		  		jbossQl.append("WHERE ");
		  	}
		  	jbossQl.append("rct.rctType>=?" + (ctr+1) + " ");
		  	obj[ctr] = (String)criteria.get("receiptType");
		  	ctr++;
		  }
		  
		  if (criteria.containsKey("receiptNumberFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("rct.rctNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("receiptNumberFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("receiptNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("rct.rctNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("receiptNumberTo");
		  	 ctr++;
		  	 
		  }

		  System.out.println("unposted Only: " + criteria.containsKey("unpostedOnly"));

		  boolean unpostedOnly=false;
		  if (criteria.containsKey("unpostedOnly")) {

			  String unposted = (String)criteria.get("unpostedOnly");

			  if (unposted.equals("YES")) {
				  if (!firstArgument) {

					  jbossQl.append("AND ");

				  } else {

					  firstArgument = false;
					  jbossQl.append("WHERE ");

				  }	       	  
				  unpostedOnly=true;
	       			       	  		       	  
				  jbossQl.append("((rct.rctPosted = 0 AND rct.rctVoid = 0)) ");
		       	  
	       	  

			  } 
		  }


	      if (criteria.containsKey("includedUnposted")  && unpostedOnly==false) {
	          
	          String unposted = (String)criteria.get("includedUnposted");
	          
	          if (!firstArgument) {
		       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }	       	  
	                  	
	       	  if (unposted.equals("NO")) {
	       			       	  		       	  
				  jbossQl.append("((rct.rctPosted = 1 AND rct.rctVoid = 0) OR (rct.rctPosted = 1 AND rct.rctVoid = 1 AND rct.rctVoidPosted = 0)) ");
		       	  
	       	  } else {
	       	  	
	       	      jbossQl.append("rct.rctVoid = 0 ");  	  
	       	  	
	       	  }   	 
	       	  	       	  
	      }	
	      
	      if (criteria.containsKey("includedMiscReceipts")) {
	          
	          String includeMiscReceipts = (String)criteria.get("includedMiscReceipts");
	          
	          if(includeMiscReceipts.equals("NO")) {
	          	
	          	if (!firstArgument) {
	          		
	          		jbossQl.append("AND ");
	          		
	          	} else {
	          		
	          		firstArgument = false;
	          		jbossQl.append("WHERE ");
	          		
	          	}	       	  
	          	
	          	jbossQl.append("rct.rctType = 'COLLECTION' ");  	   
	          	
	          }
	          	       	  
	      }	
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("rct.rctAdCompany=" + AD_CMPNY + " ");        
	         
       	  if (GROUP_BY.equals("SALESPERSON")) {
       		jbossQl.append(" ORDER BY rct.arCustomer.cstCustomerCode ");
       	  }
	    	
       	  Collection arReceipts = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arReceipts.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  if(SHOW_ENTRIES) {
		  	
		  	Iterator i = arReceipts.iterator();
		  	
		  	while (i.hasNext()) {
		  		
		  		LocalArReceipt arReceipt = (LocalArReceipt)i.next();   	  
		  		
		  		
		  		boolean first = true;
		  		String appliedInvoiceNumbers = null;
		  		String batchName = null;
		  		double total_vat = 0d;
	  			
		  
		  		Collection arDistributionRecords = arReceipt.getArDistributionRecords();
		  		
		  		Iterator j = arDistributionRecords.iterator();
		  		
		  		while(j.hasNext()) {
		  			
		  			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();
		  			
		  			ArRepOrRegisterDetails mdetails = new ArRepOrRegisterDetails();
		  			
		  			//arDistributionRecord.getArInvoice().getInvCode();
		  		
			  		mdetails.setOrrDate(arReceipt.getRctDate());
			  		mdetails.setOrrReceiptNumber(arReceipt.getRctNumber());
			  		mdetails.setOrrReferenceNumber(arReceipt.getRctReferenceNumber());
			  		mdetails.setOrrDescription(arReceipt.getRctDescription());		  	  
			  		mdetails.setOrrCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode());	
			  		mdetails.setOrrCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
			  		mdetails.setOrrCheckNumber(arReceipt.getRctCheckNo());
			  		// type
			  		if(arReceipt.getArCustomer().getArCustomerType() == null) {
			  			
			  			mdetails.setOrrCstCustomerType("UNDEFINE");
			  			
			  		} else {
			  			
			  			mdetails.setOrrCstCustomerType(arReceipt.getArCustomer().getArCustomerType().getCtName());
			  			
			  		}
			  		
			  		if(first) {
			  			
			  			mdetails.setOrrAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
				  				arReceipt.getGlFunctionalCurrency().getFcCode(),
								arReceipt.getGlFunctionalCurrency().getFcName(),
								arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(),
								arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));

			  			// get list of applied invoices (invoice number)
			  			if(!arReceipt.getArAppliedInvoices().isEmpty()) {
			  				Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();
				  			Iterator k = arAppliedInvoices.iterator();
				  			while(k.hasNext()) {
				  				LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)k.next();
				  				
				  				
				  				
				  				System.out.println("computing vat");
				  				Collection arDistRcrds = arDistributionRecordHome.findDrsByDrClassAndInvCode("TAX", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);

					        	Iterator jVt = arDistRcrds.iterator();
					        	
					        	while(jVt.hasNext()) {
					        		
					        		LocalArDistributionRecord arDistRcrd = (LocalArDistributionRecord)jVt.next();
					        		
					        		total_vat += arDistRcrd.getDrAmount();
					        		
					        	}

				                arDistributionRecords = arDistributionRecordHome.findDrsByDrClassAndInvCode("DEFERRED TAX",  arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);

				                jVt = arDistRcrds.iterator();
					        	
					        	while(jVt.hasNext()) {
					        		
					        		LocalArDistributionRecord arDistRcrd = (LocalArDistributionRecord)jVt.next();
					        		
					        		total_vat += arDistRcrd.getDrAmount();
					        		
					        	}
				  				
				  				
				  				
				  				
				  				
				  				if(appliedInvoiceNumbers == null){
				  					
				  					System.out.println("computing vat is null");
				  					
				  					try{
				  						
				  						batchName = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArInvoiceBatch().getIbName();
				  						
						  				
				  					}catch(Exception nz){
				  						System.out.println("batch null");
				  						batchName = "";
				  					}
				  				
				  				} else if(appliedInvoiceNumbers != null && !appliedInvoiceNumbers.contains(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvNumber())) {
				  					appliedInvoiceNumbers += ";" + arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvNumber();
				  					batchName += ";" + arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArInvoiceBatch().getIbName();
				  					
				  					
				  					
				  					
				  				}
				  			}
				  		} 
			  			
			  			first = false;
			  			
			  		}
			  		
			  		mdetails.setOrrReceiptAppliedInvoices(appliedInvoiceNumbers);
			  	
		  			mdetails.setOrrBatchName(batchName);
		  			mdetails.setOrrPaymentMethod(arReceipt.getRctPaymentMethod());
			  		mdetails.setOrrBankAccount(arReceipt.getAdBankAccount().getBaName());
			  		mdetails.setOrderBy(ORDER_BY);
			  		mdetails.setOrrCstName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
			  		mdetails.setOrrTotalVat(total_vat);
			  		if(arReceipt.getArSalesperson() != null) {
			  			
			  			mdetails.setOrrSlsSalespersonCode(arReceipt.getArSalesperson().getSlpSalespersonCode());
			  			mdetails.setOrrSlsName(arReceipt.getArSalesperson().getSlpName());
			  			
			  		} else {
			  			mdetails.setOrrSlsSalespersonCode("");
			  			mdetails.setOrrSlsName("");
			  		}
			  		
			  		//distribution record details
			  		
			  		mdetails.setOrrDrCoaAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
			  		mdetails.setOrrDrCoaAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
			  		
			  		if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
			  			
			  			mdetails.setOrrDrDebitAmount(arDistributionRecord.getDrAmount());
			  			
			  		} else {
			  			
			  			mdetails.setOrrDrCreditAmount(arDistributionRecord.getDrAmount());
			  			
			  		}
			  		
			  		mdetails.setOrrPosted(arReceipt.getRctPosted()==1 ? "YES" : "NO" );
			  		
			  		list.add(mdetails);
		  			
		  		}
		  		
		  	}
		  	
		  } else {
			  
			  Iterator i = arReceipts.iterator();
		  	
		  		while (i.hasNext()) {
			  	LocalArReceipt arReceipt = (LocalArReceipt)i.next();
			  	
			  	ArRepOrRegisterDetails mdetails = new ArRepOrRegisterDetails();
		  		mdetails.setOrrDate(arReceipt.getRctDate());
		  		mdetails.setOrrReceiptNumber(arReceipt.getRctNumber());
		  		mdetails.setOrrPaymentMethod(arReceipt.getRctPaymentMethod());
		  		mdetails.setOrrReferenceNumber(arReceipt.getRctReferenceNumber());
		  		mdetails.setOrrDescription(arReceipt.getRctDescription());		  	  
		  		mdetails.setOrrCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode());	
		  		mdetails.setOrrCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
		  		
				Collection arDistributionRecords = arReceipt.getArDistributionRecords();
				
				Iterator xx = arDistributionRecords.iterator();
				
				while (xx.hasNext()) {
					
					LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) xx.next();
					
					if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")) {
						if (arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service") || (arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sale") && !(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales Parts")))){
						mdetails.setOrrDrCoaAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
					}
					}	
				}

	  		
		  		if (INCLUDE_PR) {
		  			//set pr details, pr number-or ref nu, pr date-findByReferenceNumber, chk-dt, chknum, si

					System.out.println("HELLO!!!!! INCLUDE_PR SELECTED");
					
	  				if (arReceipt.getRctReferenceNumber().length() > 0) {
					
						String invoiceNumber = "";
			  			
			  			mdetails.setOrrPrNumber(arReceipt.getRctReferenceNumber());
			  			
			  			try {
			  			
				  			LocalArPdc arPdc = arPdcHome.findPdcByReferenceNumber(arReceipt.getRctReferenceNumber(), AD_CMPNY);
				  			
				  			mdetails.setOrrPrDate(arPdc.getPdcDateReceived());
				  			mdetails.setOrrCheckNumber(arPdc.getPdcCheckNumber());
				  			mdetails.setOrrCheckDate(arPdc.getPdcMaturityDate());
	
				  			
				  			Collection arAppliedInvoices = arPdc.getArAppliedInvoices();
		    				Iterator x = arAppliedInvoices.iterator();
		    				
		    				System.out.println("SIZE : " + arAppliedInvoices.size());
		    				
		    				while (x.hasNext()) {
		    					
		    					System.out.println("HELLO!!!!! PASOK!");
		    					
		    					LocalArAppliedInvoice arAppliedInvoiceNumber = (LocalArAppliedInvoice)x.next();
		    					
		    					if (x.hasNext())
		    						invoiceNumber += arAppliedInvoiceNumber.getArInvoicePaymentSchedule().getArInvoice().getInvNumber() + ", ";
		    					
		    					else
		    						invoiceNumber += arAppliedInvoiceNumber.getArInvoicePaymentSchedule().getArInvoice().getInvNumber();
		    					
		    					if(arAppliedInvoiceNumber.getArInvoicePaymentSchedule().getArInvoice().getArSalesperson()!=null){
				  					
				  					LocalArSalesperson arSalesperson =  arAppliedInvoiceNumber.getArInvoicePaymentSchedule().getArInvoice().getArSalesperson();
				  					mdetails.setOrrSlsSalespersonCode(arSalesperson.getSlpSalespersonCode());
						  			mdetails.setOrrSlsName(arSalesperson.getSlpName());
				  					
				  				} else {
				  					LocalArSalesperson arSalesperson =  arAppliedInvoiceNumber.getArInvoicePaymentSchedule().getArInvoice().getArSalesperson();
				  					mdetails.setOrrSlsSalespersonCode(" ");
						  			mdetails.setOrrSlsName(" ");
				  				}
		    					
		    					mdetails.setOrrInvoiceReferenceNumber(arAppliedInvoiceNumber.getArInvoicePaymentSchedule().getArInvoice().getInvReferenceNumber());
		    					mdetails.setOrrBatchName(arAppliedInvoiceNumber.getArInvoicePaymentSchedule().getArInvoice().getArInvoiceBatch().getIbName());
		    					
		    				}
		    				
				  			mdetails.setOrrInvoiceNumber(invoiceNumber);

				  			System.out.println("PDC CODE : " + arPdc.getPdcCode());
				  			System.out.println("Reference Number : " + mdetails.getOrrPrNumber());
				  			System.out.println("PR DATE : " + mdetails.getOrrPrDate() + "\t" + arPdc.getPdcDateReceived());
				  			System.out.println("CHECK NUMBER : " + mdetails.getOrrCheckNumber() + "\t" + arPdc.getPdcCheckNumber());
				  			System.out.println("CHECK DATE : " + mdetails.getOrrCheckDate() + "\t" + arPdc.getPdcMaturityDate());
				  			System.out.println("INVOICE NUMBERS : " + mdetails.getOrrInvoiceNumber());

				  			
			  			} catch(Exception ex) {
			  				
				  			mdetails.setOrrPrDate(null);
				  			mdetails.setOrrCheckNumber("");
				  			mdetails.setOrrCheckDate(null);
				  			mdetails.setOrrInvoiceNumber("");
				  			mdetails.setOrrBatchName("");
			  				
			  			}

	  				}

		  		}
		  		
		  		// type
		  		if(arReceipt.getArCustomer().getArCustomerType() == null) {
		  			
		  			mdetails.setOrrCstCustomerType("UNDEFINE");
		  			
		  		} else {
		  			
		  			mdetails.setOrrCstCustomerType(arReceipt.getArCustomer().getArCustomerType().getCtName());
		  			
		  		}
		  		
		  		mdetails.setOrrAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
		  				arReceipt.getGlFunctionalCurrency().getFcCode(),
						arReceipt.getGlFunctionalCurrency().getFcName(),
						arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
		  		mdetails.setOrrBankAccount(arReceipt.getAdBankAccount().getBaName());
		  		mdetails.setOrderBy(ORDER_BY);
		  		mdetails.setOrrCstName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
		  		
		  		if(arReceipt.getArSalesperson() != null) {
		  			
		  			mdetails.setOrrSlsSalespersonCode(arReceipt.getArSalesperson().getSlpSalespersonCode());
		  			mdetails.setOrrSlsName(arReceipt.getArSalesperson().getSlpName());
		  			
		  		} else {
		  			mdetails.setOrrSlsSalespersonCode(" ");
		  			mdetails.setOrrSlsName(" ");
		  		}
		  		
		  		double TOTAL_NET_AMNT = 0d;
		  		double TOTAL_TAX_AMNT = 0d;
		  		
		  		if(!arReceipt.getArInvoiceLineItems().isEmpty()) {
		  			
		  			Iterator j = arReceipt.getArInvoiceLineItems().iterator();
		  			
		  			while(j.hasNext()) {
		  				
		  				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)j.next();
		  				
		  				TOTAL_NET_AMNT += arInvoiceLineItem.getIliAmount();
		  				TOTAL_TAX_AMNT += arInvoiceLineItem.getIliTaxAmount();
		  				
		  			}
		  			
		  		} else if(!arReceipt.getArInvoiceLines().isEmpty()) {
		  			
		  			Iterator j = arReceipt.getArInvoiceLines().iterator();
		  			
		  			while(j.hasNext()) {
		  				
		  				LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)j.next();
		  				
		  				TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
		  				TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();
		  				
		  			}
		  			
		  		} else if(!arReceipt.getArAppliedInvoices().isEmpty()) {
		  			
		  			Debug.print("ArRepOrRegisterControllerBean getAppliedInvoices A");
		  			
		  			Iterator j = arReceipt.getArAppliedInvoices().iterator();
		  			
		  			while(j.hasNext()) {
		  				
		  				LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)j.next();
		  				
		  				TOTAL_NET_AMNT += arAppliedInvoice.getAiApplyAmount();
		  				
		  				if(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArSalesperson()!=null){
		  					
		  					LocalArSalesperson arSalesperson =  arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArSalesperson();
		  					mdetails.setOrrSlsSalespersonCode(arSalesperson.getSlpSalespersonCode());
				  			mdetails.setOrrSlsName(arSalesperson.getSlpName());
		  					
		  				} else {
		  					LocalArSalesperson arSalesperson =  arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArSalesperson();
		  					mdetails.setOrrSlsSalespersonCode(" ");
				  			mdetails.setOrrSlsName(" ");
		  				}
		  				
		  				
		  				
		  				System.out.println("computing vat");
		  				Collection arDistRcrds = arDistributionRecordHome.findDrsByDrClassAndInvCode("TAX", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);

			        	Iterator jVt = arDistributionRecords.iterator();
			        	double total_vat = 0d;
			        	while(jVt.hasNext()) {
			        		
			        		LocalArDistributionRecord arDistRcrd = (LocalArDistributionRecord)jVt.next();
			        		
			        		total_vat += arDistRcrd.getDrAmount();
			        		
			        	}

		                        arDistributionRecords = arDistributionRecordHome.findDrsByDrClassAndInvCode("DEFERRED TAX", arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvCode(), AD_CMPNY);

		                        jVt = arDistributionRecords.iterator();
			        	
			        	while(jVt.hasNext()) {
			        		
			        		LocalArDistributionRecord arDistRcrd = (LocalArDistributionRecord)jVt.next();
			        		
			        		total_vat += arDistRcrd.getDrAmount();
			        		
			        	}
		  				
		  				
		  				mdetails.setOrrTotalVat(total_vat);
		  				
		  				
		  				mdetails.setOrrInvoiceReferenceNumber(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvReferenceNumber());	
		  				try{
		  					mdetails.setOrrBatchName(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getArInvoiceBatch().getIbName());
		    				
		  				}catch(Exception ex){
		  					mdetails.setOrrBatchName("");
		    				
		  				}
		  				
		  			}
		  			
		  			Debug.print("ArRepOrRegisterControllerBean getAppliedInvoices B");
		  			
		  		}
		  		
		  		mdetails.setOrrReceiptNetAmount(TOTAL_NET_AMNT);
		  		mdetails.setOrrReceiptTaxAmount(TOTAL_TAX_AMNT);
		  		
		  		mdetails.setOrrPosted(arReceipt.getRctPosted()==1 ? "YES" : "NO" );
		  		
		  		list.add(mdetails);
		  		}
		  	}	
		  		
		  
	      
		  
		  
		  // sort
		  
		  if(SHOW_ENTRIES) {
			  
			  Collections.sort(list, ArRepOrRegisterDetails.CoaAccountNumberComparator);
			  
		  }
		  
		  if (GROUP_BY.equals("CUSTOMER CODE")) {
		  	
		  	Collections.sort(list, ArRepOrRegisterDetails.CustomerCodeComparator);
		  	
		  } else if (GROUP_BY.equals("CUSTOMER TYPE")) {
		  	
		  	Collections.sort(list, ArRepOrRegisterDetails.CustomerTypeComparator);
		  	
		  } else if (GROUP_BY.equals("CUSTOMER CLASS")) {
		  	
		  	Collections.sort(list, ArRepOrRegisterDetails.CustomerClassComparator);
		  	
		  } else if (GROUP_BY.equals("SALESPERSON")) {
			  	
			  	Collections.sort(list, ArRepOrRegisterDetails.SalespersonComparator);
			  	
		  } else {
		  	
		  	Collections.sort(list, ArRepOrRegisterDetails.NoGroupComparator);
		  	
		  }
		  
		  if (SUMMARIZE) {
		  	
		  	Collections.sort(list, ArRepOrRegisterDetails.CoaAccountNumberComparator);
		  	
		  }
       	  
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepOrRegisterControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
public ArrayList getArRbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("ArRepORRegisterControllerBean getArRbAll");
		
		LocalArReceiptBatchHome arReceiptBatchHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection arReceiptBatches = arReceiptBatchHome.findOpenRbAll(AD_BRNCH, AD_CMPNY);
			
			Iterator i = arReceiptBatches.iterator();
			
			while (i.hasNext()) {
				
				LocalArReceiptBatch arReceiptBatch = (LocalArReceiptBatch)i.next();
				
				list.add(arReceiptBatch.getRbName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ArRepOrRegisterControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepOrRegisterControllerBean ejbCreate");
      
    }
}
