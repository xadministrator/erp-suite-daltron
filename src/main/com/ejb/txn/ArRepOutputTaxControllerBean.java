
/*
 * ArRepOutputTaxControllerBean.java
 *
 * Created on March 25, 2004, 8:56 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlTaxInterface;
import com.ejb.gl.LocalGlTaxInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepOutputTaxDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepOutputTaxControllerEJB"
 *           diinvay-name="Used for viewing output tax summary"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepOutputTaxControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepOutputTaxController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepOutputTaxControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepOutputTaxControllerBean extends AbstractSessionBean {
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArRepOutputTaxControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
        	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public ArrayList executeArRepOutputTax(HashMap criteria, ArrayList branchList, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
                
		Debug.print("ArRepOutputTaxControllerBean executeArRepOutputTax");
		
		LocalArDistributionRecordHome arDistributionRecordHome = null;                
        LocalGlTaxInterfaceHome glTaxInterfaceHome = null;        
        LocalGlJournalLineHome glJournalHome = null;                        
        
        ArrayList list = new ArrayList();
        
        try {
        	        
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);                        
            glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);
            glJournalHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,LocalGlJournalLineHome.class);                        
                                   
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }                        
        
        try { 
        	
        	//get all available records
        	
        	boolean firstArgument = true;
  	        short ctr = 0;
  		    int criteriaSize = criteria.size();
  		    Date dateFrom = null;
		    Date dateTo = null;
        	
        	StringBuffer jbossQl = new StringBuffer();
  		    
        	jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti WHERE (");
        	
        	if (branchList.isEmpty())
        		throw new GlobalNoRecordFoundException();
        	
        	Iterator brIter = branchList.iterator();
  		  
  		  	AdBranchDetails brDetails = (AdBranchDetails)brIter.next();
  		  	jbossQl.append("ti.tiAdBranch=" + brDetails.getBrCode());
  		  
  		  	while(brIter.hasNext()) {
  		  	
  		  		brDetails = (AdBranchDetails)brIter.next();
  		  		
  		  		jbossQl.append(" OR ti.tiAdBranch=" + brDetails.getBrCode());
  		  	
  		  	}
  		  
  		  	jbossQl.append(") ");
  		  	firstArgument = false;
  		    
  	        Object obj[];	      
  		
  		    // Allocate the size of the object parameter
  		  
  		    if (criteria.containsKey("customerCode")) {
  	      	
  	      	   criteriaSize--;
  	      	 
  	        }  		    		    
  		    
  		    obj = new Object[criteriaSize];
        	
  		    if (criteria.containsKey("customerCode")) {
		  	
		  	   if (!firstArgument) {
		  	 
		  	      jbossQl.append("AND ");	
		  	 	
		       } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		       }
		     
		  	   jbossQl.append("ti.tiSlSubledgerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
		  	 
		    }
        	        		        	
        	if (criteria.containsKey("dateFrom")) {
		      	
		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate>=?" + (ctr+1) + " ");
		  	   obj[ctr] = (Date)criteria.get("dateFrom");
		  	   dateFrom = (Date)criteria.get("dateFrom");
		  	   ctr++;
		    }  
		      
		    if (criteria.containsKey("dateTo")) {
		  	
		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate<=?" + (ctr+1) + " ");
		  	   obj[ctr] = (Date)criteria.get("dateTo");
		  	   dateTo = (Date)criteria.get("dateTo");
		  	   ctr++;
		  	 
		    }    		  		  		    	  
		  
		    if (!firstArgument) {
		       	  	
	   	       jbossQl.append("AND ");
	   	     
	   	    } else {
	   	  	
	   	  	   firstArgument = false;
	   	  	   jbossQl.append("WHERE ");
	   	  	 
	   	    }

		    jbossQl.append("(ti.tiDocumentType='AR INVOICE' OR ti.tiDocumentType='AR CREDIT MEMO' OR " +
		    				"ti.tiDocumentType='AR RECEIPT' OR ti.tiDocumentType='GL JOURNAL') " +
		    				"AND ti.tiIsArDocument=1 AND ti.tiTcCode IS NOT NULL AND ti.tiWtcCode IS NULL " +
							"AND ti.tiAdCompany=" + AD_CMPNY +	" ORDER BY ti.tiSlSubledgerCode");
		    		   	    
		    short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);
		
		    Collection glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);
	
		    ArrayList arCustomerList = new ArrayList();		    		  
		    		            			        		        		        	
        	Iterator i = glTaxInterfaces.iterator();	        		        	
        	
        	while(i.hasNext()) {
        	
        		//get available records per customer
        		
        		LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();
        		
        		if (arCustomerList.contains(glTaxInterface.getTiSlSubledgerCode())) {
        			
        			continue;
        			
        		}         		        		
        			        		
    			arCustomerList.add(glTaxInterface.getTiSlSubledgerCode());        			
        		        		
        		jbossQl = new StringBuffer();
        		
        		jbossQl.append("SELECT DISTINCT OBJECT(ti) FROM GlTaxInterface ti " + 
        					   "WHERE (ti.tiDocumentType='AR INVOICE' OR ti.tiDocumentType='AR CREDIT MEMO' " +
        					   "OR ti.tiDocumentType='AR RECEIPT' OR ti.tiDocumentType='GL JOURNAL') ");        					   
        		
        		ctr = 0;        		        		
        		criteriaSize = 0;
        		String sbldgrCode = glTaxInterface.getTiSlSubledgerCode(); 
        		
        		if(dateFrom != null ) { 
        			criteriaSize++;        			
        		}
        		
        		if(dateTo != null) {
        			criteriaSize++;        		
        		}
        		
        		if(sbldgrCode != null) {
        			criteriaSize++;        		
        		}
        		
        		obj = new Object[criteriaSize];
        		
        		if(sbldgrCode != null) {
        			    		       		     
    			   jbossQl.append("AND ti.tiSlSubledgerCode = ?"  + (ctr+1) + " ");
    			   obj[ctr] = sbldgrCode;
				   ctr++;
    			   
        		} else {
        			
        			jbossQl.append("AND ti.tiSlSubledgerCode IS NULL ");
        			
        		}
        		
        		if (dateFrom != null) {
        		  	    		  	    		    
    		  	   jbossQl.append("AND ti.tiTxnDate >=?" + (ctr+1) + " ");
        		   obj[ctr] = dateFrom;
        		   ctr++;
        		   
    		    }
        		
        		if(dateTo != null) {
        			    			  
       		  	   jbossQl.append("AND ti.tiTxnDate <=?" + (ctr+1) + " ");
           		   obj[ctr] = dateTo;
           		   ctr++;
           		   
        		}        		        				    		    	   	    
    	   	    
    		    jbossQl.append("AND ti.tiIsArDocument=1 AND ti.tiTcCode IS NOT NULL AND ti.tiWtcCode IS NULL AND " + 
    		    				"ti.tiAdCompany=" + AD_CMPNY +  " ORDER BY ti.tiTxnDate");
    		    
    			double OUTPUT_TAX = 0d;		    
			    double NET_AMOUNT = 0d;
			    String DOC_NUM = null;
        		        		
			    Collection glTaxInterfaceTcs = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(),obj);
			            		
        		Iterator j = glTaxInterfaceTcs.iterator();
        	
        		while(j.hasNext()) {
        			    			
        		
        			LocalGlTaxInterface glTaxInterfaceTc = (LocalGlTaxInterface) j.next();         		        			        			
        			 System.out.println("intr code: " + glTaxInterfaceTc.getTiCode());			
        			if(glTaxInterfaceTc.getTiDocumentType().equals("GL JOURNAL")) {
        			
        				LocalGlJournalLine glJournal = glJournalHome.findByPrimaryKey( 
        									glTaxInterfaceTc.getTiTxlCode());
        				
        				if(glJournal.getJlDebit() == EJBCommon.TRUE) {

        					OUTPUT_TAX -= EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT);        					
        					NET_AMOUNT -= EJBCommon.roundIt(glTaxInterfaceTc.getTiNetAmount(), PRECISION_UNIT);
        					
        				} else {

        					OUTPUT_TAX += EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT);        					
        					NET_AMOUNT += EJBCommon.roundIt(glTaxInterfaceTc.getTiNetAmount(), PRECISION_UNIT);
        				
        				}
        				
        				DOC_NUM = glJournal.getGlJournal().getJrDocumentNumber();
        				
        			} else {
        				 System.out.println("enter loop 1: " + glTaxInterfaceTc.getTiTxnCode());
        				 
        				 	
        			//	 LocalArDistributionRecord arDistributionRecord = null;
        				 
        				
        				 if(glTaxInterfaceTc.getTiDocumentType().equals("AR INVOICE")) {
        					 
        					 
        					 System.out.println("its invoice" + glTaxInterfaceTc.getTiTxnCode());
        					 try {
        					//	 arDistributionRecord = arDistributionRecordHome.findByDrClassAndInvCode("RECEIVABLE",glTaxInterfaceTc.getTiTxnCode(), AD_CMPNY);
            					 String invNumber = "";
        						 Collection drlist = arDistributionRecordHome.findDrsByDrClassAndInvCode("RECEIVABLE",glTaxInterfaceTc.getTiTxnCode(), AD_CMPNY);
        						 Iterator arI = drlist.iterator();
            					 
            					 while(arI.hasNext()) {
            						 
            						 
            						 LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)arI.next();
            						 System.out.println("enter loop 1.1");
            						 
            						 invNumber = arDistributionRecord.getArInvoice().getInvNumber();
              						if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
              							 System.out.println("enter loop 1.2");
              							// OUTPUT_TAX -= EJBCommon.roundIt(glTaxInterfaceTc.getTiSalesAmount(), PRECISION_UNIT);			
              							 
              							OUTPUT_TAX -= EJBCommon.roundIt(glTaxInterfaceTc.getTiTaxAmount(), PRECISION_UNIT);		
              							 //OUTPUT_TAX -= EJBCommon.roundIt(arDistributionRecord.getDrAmount(), PRECISION_UNIT);							
              							NET_AMOUNT -= EJBCommon.roundIt(glTaxInterfaceTc.getTiNetAmount(), PRECISION_UNIT);							
              							
              						} else {
              							 System.out.println("enter loop 1.3");
              							OUTPUT_TAX += EJBCommon.roundIt(arDistributionRecord.getDrAmount(), PRECISION_UNIT);	
              						//	 OUTPUT_TAX -= EJBCommon.roundIt(glTaxInterfaceTc.getTiSalesAmount(), PRECISION_UNIT);	
              							NET_AMOUNT += EJBCommon.roundIt(glTaxInterfaceTc.getTiNetAmount(), PRECISION_UNIT);							
              							
              						}
            					 }
        						 
            					 
            				
     							if(DOC_NUM == null) {
     							
     								DOC_NUM = invNumber;
     							}
     								
     							else {
     								
     								DOC_NUM = DOC_NUM + "; " + invNumber;
     							}
            					
        					 }catch(FinderException ex) {
        						 
        					 }
        					
        					 
        				 }
        				 
        				 
        				 if(glTaxInterfaceTc.getTiDocumentType().equals("AR RECEIPT")) {
        					 
        					 System.out.println("its receiptL " + glTaxInterfaceTc.getTiTxnCode());
        					 try {
        						 //arDistributionRecord =  arDistributionRecordHome.findByDrClassAndRctCode("CASH",glTaxInterfaceTc.getTiTxnCode(), AD_CMPNY);
        						 String rctNumber = "";
        						 
            					 Collection drlist = arDistributionRecordHome.findDrsByDrClassAndRctCode("CASH",glTaxInterfaceTc.getTiTxnCode(), AD_CMPNY);
            					 
            					 Iterator arI = drlist.iterator();
            					 
            					 while(arI.hasNext()) {
            						 LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)arI.next();
            						 
            						 rctNumber = arDistributionRecord.getArReceipt().getRctNumber();
            						 System.out.println("enter loop 1.1");
              						if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
              							 System.out.println("enter loop 1.2");
              							// OUTPUT_TAX -= EJBCommon.roundIt(glTaxInterfaceTc.getTiSalesAmount(), PRECISION_UNIT);			
              							OUTPUT_TAX -= EJBCommon.roundIt(glTaxInterfaceTc.getTiTaxAmount(), PRECISION_UNIT);	
              					//		 OUTPUT_TAX -= EJBCommon.roundIt(arDistributionRecord.getDrAmount(), PRECISION_UNIT);							
              							NET_AMOUNT -= EJBCommon.roundIt(glTaxInterfaceTc.getTiNetAmount(), PRECISION_UNIT);							
              							
              						} else {
              							 System.out.println("enter loop 1.3");
              							//OUTPUT_TAX += EJBCommon.roundIt(arDistributionRecord.getDrAmount(), PRECISION_UNIT);	
              							OUTPUT_TAX += EJBCommon.roundIt(glTaxInterfaceTc.getTiTaxAmount(), PRECISION_UNIT);	
              						//	 OUTPUT_TAX -= EJBCommon.roundIt(glTaxInterfaceTc.getTiSalesAmount(), PRECISION_UNIT);	
              							NET_AMOUNT += EJBCommon.roundIt(glTaxInterfaceTc.getTiNetAmount(), PRECISION_UNIT);							
              							
              						}
            						 
            						 
            						 
            					 }
            					 
            					 System.out.println("enter loop 1.4");
      							if(DOC_NUM == null) {
      						
      								DOC_NUM = rctNumber;
      							}
      								
      							else {
      							
      								DOC_NUM = DOC_NUM + "; " + rctNumber;
      							}
            					 
            					
        					 }catch(FinderException ex) {
        						 System.out.println("cash not found");
        					 }
        					
        					 /*
        					 try {
        					
         						
         					
         						 Collection drlist = arDistributionRecordHome.findDrsByDrClassAndRctCode("RECEIVABLE",glTaxInterfaceTc.getTiTxnCode(), AD_CMPNY);
         						 Iterator arI = drlist.iterator();
            					 
            					 while(arI.hasNext()) {
            						 LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)arI.next();
            						 System.out.println("enter loop 1.1");
             						if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
             							 System.out.println("enter loop 1.2");
             							// OUTPUT_TAX -= EJBCommon.roundIt(glTaxInterfaceTc.getTiSalesAmount(), PRECISION_UNIT);			
             							 
             							 OUTPUT_TAX -= EJBCommon.roundIt(arDistributionRecord.getDrAmount(), PRECISION_UNIT);							
             							NET_AMOUNT -= EJBCommon.roundIt(glTaxInterfaceTc.getTiNetAmount(), PRECISION_UNIT);							
             							
             						} else {
             							 System.out.println("enter loop 1.3");
             							OUTPUT_TAX += EJBCommon.roundIt(arDistributionRecord.getDrAmount(), PRECISION_UNIT);	
             						//	 OUTPUT_TAX -= EJBCommon.roundIt(glTaxInterfaceTc.getTiSalesAmount(), PRECISION_UNIT);	
             							NET_AMOUNT += EJBCommon.roundIt(glTaxInterfaceTc.getTiNetAmount(), PRECISION_UNIT);							
             							
             						}
            					 }
           					 
           					     
         						
         						
        					 }catch(FinderException ex) {
        						 System.out.println("receivable not found");
        					 }
        					
     						
     						*/
     						
     						
     						
        				 }
        				 
        				 
        				 
        				 
        				 
        				 
        				 
					//	LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.findByPrimaryKey(
										//	glTaxInterfaceTc.getTiTxlCode());
					//				glTaxInterfaceTc.getTiTxnCode());
					
						
						
					
					
						 System.out.println("enter loop 1 end");
						
        			}
	        		            		
        		}   
        		
        		ArRepOutputTaxDetails details = new ArRepOutputTaxDetails();        		
        		details.setOtTinOfCustomer(glTaxInterface.getTiSlTin());
        		details.setOtRegisteredName(glTaxInterface.getTiSlName());
        		details.setOtLastName(null);
        		details.setOtFirstName(null);
        		details.setOtMiddleName(null);
        		details.setOtAddress1(glTaxInterface.getTiSlAddress());
        		details.setOtAddress2(null);
        		details.setOtNetAmount(NET_AMOUNT);
        		details.setOtOutputTax(OUTPUT_TAX);
        		details.setOtTransaction(DOC_NUM);
        		
        		list.add(details);
        		
        	}
	                	        	
        	if (list.isEmpty()) {
    		  	
    		  	  throw new GlobalNoRecordFoundException();
    		  	
    		}	      
        
        } catch (GlobalNoRecordFoundException ex) {
   	  	 
        	Debug.printStackTrace(ex);
        	throw ex;
   	  	
        } catch (Exception ex) {
   	  	
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
   	  	
   	  	}
        
        return list;        
       
	}
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepOutputTaxControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
	
	private short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArRepOutputTaxControllerBean getGlFcPrecisionUnit");
      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

	} 
	
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	  
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepOutputTaxControllerBean ejbCreate");
      
    }
}
