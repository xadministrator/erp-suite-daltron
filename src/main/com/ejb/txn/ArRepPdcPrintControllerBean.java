
/*
 * ArRepPdcPrintControllerBean.java
 *
 * Created on July 17, 2008, 6:26 PM
 *
 * @author  Reginald Cris Pasco, Ariel Joseph De Guzman
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArPdc;
import com.ejb.ar.LocalArPdcHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArModReceiptDetails;
import com.util.ArModPdcDetails;
import com.util.ArRepSalesDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepPdcPrintControllerEJB"
 *           display-name="Used for printing PDC PR transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepPdcPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepPdcPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepPdcPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class ArRepPdcPrintControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeArRepPdcPrint(ArrayList pdcCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepPdcPrintControllerBean executeArRepPdcPrint");
        
        LocalArPdcHome arPdcHome = null;    
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;
        
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
        	arPdcHome = (LocalArPdcHome)EJBHomeFactory.
                lookUpLocalHome(LocalArPdcHome.JNDI_NAME, LocalArPdcHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
        		lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = pdcCodeList.iterator();

        	while (i.hasNext()) {

        		Integer PDC_CODE = (Integer) i.next();

        		LocalArPdc arPdc = null;

        		try {

        			arPdc = arPdcHome.findByPrimaryKey(PDC_CODE);

        		} catch (FinderException ex) {

        			continue;

        		}

        		Collection arPdcAppliedInvoices = arPdc.getArAppliedInvoices();

        		if(!arPdcAppliedInvoices.isEmpty()) {
        			
        			Iterator j = arPdcAppliedInvoices.iterator();

        			while(j.hasNext()) {
        				
        				String invoiceNumber = "";

        				LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)j.next();

        				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
        				LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("AR RECEIPT", AD_CMPNY);	       		

        				if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {

        					if (arPdc.getPdcApprovalStatus() == null || 
        							arPdc.getPdcApprovalStatus().equals("PENDING")) {

        						continue;

        					} 

        				} else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {

        					if (arPdc.getPdcApprovalStatus() != null && 
        							(arPdc.getPdcApprovalStatus().equals("N/A") || 
        									arPdc.getPdcApprovalStatus().equals("APPROVED"))) {

        						continue;

        					}

        				}

        				if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE && 
        						arPdc.getPdcPrinted() == EJBCommon.TRUE){

        					continue;	

        				}

        				// show duplicate

        				boolean showDuplicate = false;

        				if (adApprovalDocument.getAdcTrackDuplicate() == EJBCommon.TRUE &&
        						arPdc.getPdcPrinted() == EJBCommon.TRUE) {

        					showDuplicate = true;

        				}

        				// set printed

        				arPdc.setPdcPrinted(EJBCommon.TRUE);

        				ArModPdcDetails mdetails = new ArModPdcDetails();

        				
        	        	mdetails.setPdcCode(arPdc.getPdcCode());
        	        	mdetails.setPdcCheckNumber(arPdc.getPdcCheckNumber());
	    	        	mdetails.setPdcReferenceNumber(arPdc.getPdcReferenceNumber());
	    	        	mdetails.setPdcDateReceived(arPdc.getPdcDateReceived());
        	        	mdetails.setPdcMaturityDate(arPdc.getPdcMaturityDate());        	        	        	
	    	        	mdetails.setPdcDescription(arPdc.getPdcDescription());
	    	            mdetails.setPdcAmount(arPdc.getPdcAmount());
	        	        mdetails.setPdcCstCustomerCode(arPdc.getArCustomer().getCstCustomerCode());
	        	        mdetails.setPdcCstName(arPdc.getArCustomer().getCstName());
	        			mdetails.setPdcCstTin(arPdc.getArCustomer().getCstTin());
	        			mdetails.setPdcCstAddress(arPdc.getArCustomer().getCstAddress());
	        			mdetails.setRctBaName(arPdc.getAdBankAccount().getBaName());
	        			mdetails.setPdcBankName(arPdc.getAdBankAccount().getAdBank().getBnkName() + " - " + 
	        					arPdc.getAdBankAccount().getAdBank().getBnkBranch());
	        			
        				
        				Collection arAppliedInvoices = arPdc.getArAppliedInvoices();
        				Iterator x = arAppliedInvoices.iterator();
        				
        				while (x.hasNext()) {
        					
        					LocalArAppliedInvoice arAppliedInvoiceNumber = (LocalArAppliedInvoice)x.next();
        					
        					if (x.hasNext())
        						invoiceNumber += arAppliedInvoiceNumber.getArInvoicePaymentSchedule().getArInvoice().getInvNumber() + ", ";
        					
        					else
        						invoiceNumber += arAppliedInvoiceNumber.getArInvoicePaymentSchedule().getArInvoice().getInvNumber();
        				}
        				
        				mdetails.setPdcInvoiceNumbers(invoiceNumber);

        				list.add(mdetails);

        			}

        		}

        	}

        	if (list.isEmpty()) {

        		throw new GlobalNoRecordFoundException();

        	}        	       	

        	return list; 

        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepPdcPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

	// private methods
	
	private short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepPdcPrintControllerBean ejbCreate");
      
    }
}
