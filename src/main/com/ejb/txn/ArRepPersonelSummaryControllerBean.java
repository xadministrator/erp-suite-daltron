
/*
 * ArRepPersonelSummaryControllerBean.java
 *
 * Created on January 23, 2019, 10:52 AM
 *
 * @author  Ruben P. Lamberte
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdBranchStandardMemoLine;
import com.ejb.ad.LocalAdBranchStandardMemoLineHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArJobOrder;
import com.ejb.ar.LocalArJobOrderAssignment;
import com.ejb.ar.LocalArJobOrderAssignmentHome;
import com.ejb.ar.LocalArJobOrderHome;
import com.ejb.ar.LocalArJobOrderInvoiceLineHome;
import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArJobOrderLineHome;
import com.ejb.ar.LocalArJobOrderType;
import com.ejb.ar.LocalArJobOrderTypeHome;
import com.ejb.ar.LocalArPersonel;
import com.ejb.ar.LocalArPersonelHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepJobOrderPrintDetails;
import com.util.ArRepPersonelSummaryDetails;
import com.util.ArRepSalespersonDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepPersonelSummaryControllerEJB"
 *           display-name="Used for viewing personel"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepPersonelSummaryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepPersonelSummaryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepPersonelSummaryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepPersonelSummaryControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArRepPersonelSummaryControllerBean getArCcAll");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalArCustomerClass arCustomerClass = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);
	        
	        Iterator i = arCustomerClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerClass = (LocalArCustomerClass)i.next();
	        	
	        	list.add(arCustomerClass.getCcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCtAll(Integer AD_CMPNY) {
                    
        Debug.print("ArRepPersonelSummaryControllerBean getArCtAll");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalArCustomerType arCustomerType = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

	        Iterator i = arCustomerTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerType = (LocalArCustomerType)i.next();
	        	
	        	list.add(arCustomerType.getCtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArRepPersonelSummaryControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepPersonelSummary(HashMap criteria, ArrayList branchList, String personelName, String GROUP_BY, boolean includeUnposted, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepPersonelSummaryControllerBean executeArRepSalesperson");
        
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArReceiptHome arReceiptHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalArJobOrderHome arJobOrderHome = null;
        LocalArJobOrderLineHome arJobOrderLineHome = null;
        LocalArJobOrderAssignmentHome arJobOrderAssignmentHome = null;
		LocalArJobOrderInvoiceLineHome arJobOrderInvoiceLineHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            arJobOrderHome = (LocalArJobOrderHome)EJBHomeFactory.
    				lookUpLocalHome(LocalArJobOrderHome.JNDI_NAME, LocalArJobOrderHome.class);
            arJobOrderLineHome = (LocalArJobOrderLineHome)EJBHomeFactory.
    				lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);
            arJobOrderAssignmentHome = (LocalArJobOrderAssignmentHome)EJBHomeFactory.
    				lookUpLocalHome(LocalArJobOrderAssignmentHome.JNDI_NAME, LocalArJobOrderAssignmentHome.class);
            arJobOrderInvoiceLineHome = (LocalArJobOrderInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderInvoiceLineHome.JNDI_NAME, LocalArJobOrderInvoiceLineHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 

		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           
		  boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	  
		  String PYMNT_STTS = null;
		  
	      StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(jo) FROM ArJobOrder jo WHERE (");
		  
		  if (branchList.isEmpty())
		  	throw new GlobalNoRecordFoundException();

		  Iterator brIter = branchList.iterator();
		  
		  AdBranchDetails details = (AdBranchDetails)brIter.next();
		  jbossQl.append(" jo.joAdBranch=" + details.getBrCode());
		  
		  while(brIter.hasNext()) {
		  	
		  		details = (AdBranchDetails)brIter.next();
		  		
		  		jbossQl.append(" OR jo.joAdBranch=" + details.getBrCode());
		  	
		  }
		  
		//  jbossQl.append(") AND inv.arSalesperson.slpSalespersonCode IS NOT NULL ");
		  jbossQl.append(") ");
		
		  firstArgument = false;
		  
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		

	      if (criteria.containsKey("jobOrderType")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      
	      
	      if (criteria.containsKey("customerCode")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

    
          
          if (criteria.containsKey("paymentStatus")) {
	      	
	      	 criteriaSize--;
	      	 PYMNT_STTS = (String)criteria.get("paymentStatus");
	      	 
	      }
	      
	      obj = new Object[criteriaSize];
		       	  
	      
	      /*
	      
		  if (criteria.containsKey("salespersonCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("inv.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salespersonCode") + "%' ");
		  	 
		  }
	       
	      */
	      
		  if (criteria.containsKey("jobOrderType")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("jo.arJobOrderType.jotName = '" + (String)criteria.get("jobOrderType") + "' ");
		  	 
		  }

	      
		  if (criteria.containsKey("customerCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("jo.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
		  	 
		  }

		  if (criteria.containsKey("customerClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("jo.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerClass");
		   	  ctr++;
	   	  
	      }		
		  
		  if (criteria.containsKey("customerType")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("jo.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerType");
		   	  ctr++;
	   	  
	      }	
		  
	
          			
	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("jo.joDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("jo.joDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		  
		  if (criteria.containsKey("documentNumberFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("jo.joDocumentNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("documentNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("jo.joDocumentNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberTo");
		  	 ctr++;
		  	 
		  }
	      
	     
		  
		  
		  if (!includeUnposted) {
			  if (!firstArgument) {
				  	 
			  	    jbossQl.append("AND ");	
			  	 	
			     } else {
			     	
			     	firstArgument = false;
			     	jbossQl.append("WHERE ");
			     	
			     }
			     
			  	 jbossQl.append(" jo.joPosted = 1 ");
		  }
		  
		  
		  
		
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append(" jo.joVoid = 0 AND jo.joAdCompany=" + AD_CMPNY + " ORDER BY jo.joDocumentNumber");
       	  
       	  System.out.println(jbossQl.toString());
       	  Collection arJobOrders = arJobOrderHome.getJOByCriteria(jbossQl.toString(), obj);
	      
       	  double JO_AMOUNT = 0;
       	  double JO_QTY = 0;
       	  double JA_AMOUNT = 0;
       	  double JA_QTY = 0;
       	  Integer JO_CODE = 0;
       	  
       	  if(!arJobOrders.isEmpty()) {
       	  	
       	  	Iterator i = arJobOrders.iterator();
       	  	
       	  	while (i.hasNext()) {
       	  		
       	  	    LocalArJobOrder arJobOrder = (LocalArJobOrder)i.next();   	  
       	  		
       	  		ArRepPersonelSummaryDetails mdetails = new ArRepPersonelSummaryDetails();
       	  		
       	  	
       	  		
   	  		    JO_AMOUNT = 0;
         	    JO_QTY = 0;
         	    	  		
       	  		
       	  		mdetails.setJoCode(arJobOrder.getJoCode());
       	  		mdetails.setJoDate(arJobOrder.getJoDate());
       	  		mdetails.setJoType(arJobOrder.getArJobOrderType().getJotName());
       	  		mdetails.setJoDocumentNumber(arJobOrder.getJoDocumentNumber());
       	  		mdetails.setJoCstCustomerCode(arJobOrder.getArCustomer().getCstCustomerCode());
       	  		mdetails.setJoCstCustomerCode(arJobOrder.getArCustomer().getCstName());
       	  		mdetails.setJoCstCustomerClass(arJobOrder.getArCustomer().getArCustomerClass()!=null?arJobOrder.getArCustomer().getArCustomerClass().getCcName():null);
       	  		mdetails.setJoCstCustomerClass(arJobOrder.getArCustomer().getArCustomerType()!=null?arJobOrder.getArCustomer().getArCustomerType().getCtName():null);
       	  		//amount
       	  		
       	  		Iterator ijol = arJobOrder.getArJobOrderLines().iterator();
       	  		
       	  	
       	  		while (ijol.hasNext()) {
       	  			LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine)ijol.next();
       	  			
       	  			if(arJobOrderLine.getArJobOrderAssignments().size() <= 0) {
       	  				continue;
       	  			}
       	  			
	       	 

	       	  		JO_QTY += arJobOrderLine.getJolQuantity();
	       	  		JO_AMOUNT += arJobOrderLine.getJolAmount();
	       	  		
	       	  		Iterator jai = arJobOrderLine.getArJobOrderAssignments().iterator();
		       	  	JA_AMOUNT = 0;
	         	    JA_QTY = 0;
		       	  	while(jai.hasNext()) {
		       	  		LocalArJobOrderAssignment arJobOrderAssignment = (LocalArJobOrderAssignment)jai.next();
		       	  		
		       	  		System.out.println("personel name: " + personelName);
		       	  		if(!personelName.equals("") && !personelName.equals(arJobOrderAssignment.getArPersonel().getPeName())) {
		       	  			continue;
		       	  		}
		       	  		
		       	  		ArRepPersonelSummaryDetails mdetails2 = (ArRepPersonelSummaryDetails)mdetails.clone();
	       	  		
			       	  	mdetails2.setJaCode(arJobOrderAssignment.getJaCode());
				       	mdetails2.setJaPersonelCode(arJobOrderAssignment.getArPersonel().getPeIdNumber());
				       	mdetails2.setJaPersonelName(arJobOrderAssignment.getArPersonel().getPeName());
				       	mdetails2.setJaQuantity(arJobOrderAssignment.getJaQuantity());
				       	mdetails2.setJaUnitCost(arJobOrderAssignment.getJaUnitCost());
				       	mdetails2.setJaAmount(arJobOrderAssignment.getJaAmount());
			       	
						
				       	mdetails2.setJoAmount(JO_AMOUNT);
				       	mdetails2.setJoQuantity(JO_QTY);
		       	  		
		       	  		list.add(mdetails2);
		       	  	}
	       	  	
	       	  		
       	  		}
       	 
       	  		
       	  		
       	  		
       	  		
       	  	
   	  	
       	  		
       	  	}
       	  	
       	  }
       	  
       	 
    
       	  System.out.println("size is: "+list.size());
       	  return list;
       	  
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepSalespersonControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ArRepSalespersonControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
    
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public ArrayList getAllJobOrderTypeName(Integer AD_CMPNY) {

		Debug.print("ArRepPersonelSummaryControllerBean getAllJobOrderType");

		LocalArJobOrderTypeHome arJobOrderTypeHome = null;

		//Initialize EJB Home

		try {

			arJobOrderTypeHome = (LocalArJobOrderTypeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderTypeHome.JNDI_NAME, LocalArJobOrderTypeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arJobOrderTypes = arJobOrderTypeHome.findJotAll(AD_CMPNY);
			ArrayList list = new ArrayList();


			Iterator i = arJobOrderTypes.iterator();

			while(i.hasNext()) {

				LocalArJobOrderType arJobOrderType = (LocalArJobOrderType)i.next();

				list.add(arJobOrderType.getJotName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArPeAll(Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("ArRepPersonelSummaryControllerBean getApSplAll");
		
		LocalArPersonelHome arPersonelHome = null;
		

		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			arPersonelHome = (LocalArPersonelHome)EJBHomeFactory.
				lookUpLocalHome(LocalArPersonelHome.JNDI_NAME, LocalArPersonelHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection arPersonels = arPersonelHome.findPeAll( AD_CMPNY);
			
			Iterator i = arPersonels.iterator();
			
			while (i.hasNext()) {
				
				LocalArPersonel arPersonel = (LocalArPersonel)i.next();
				
				list.add(arPersonel.getPeName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepPersonelSummaryControllerBean ejbCreate");
      
    }
}
