
/*
 * ArRepPostDatedCheckControllerBean.java
 *
 * Created on July 1, 2005, 02:40 PM
 *
 * @author  Arnel Masikip
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArPdc;
import com.ejb.ar.LocalArPdcHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepPostDatedCheckDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;


/**
 * @ejb:bean name="ArRepPostDatedCheckControllerEJB"
 *           dicstay-name="Used for reporting post dated checks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepPostDatedCheckControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepPostDatedCheckController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepPostDatedCheckControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/
public class ArRepPostDatedCheckControllerBean extends AbstractSessionBean {
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArRepPostDatedCheckControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeArRepPostDatedCheckList(HashMap criteria, ArrayList branchList, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{
		
		Debug.print("ArRepPostDatedCheckControllerBean executeArRepPostDatedCheckList");
		
		LocalArPdcHome arPdcHome = null;		
		ArrayList list = new ArrayList();
		
		try {
			
			arPdcHome = (LocalArPdcHome) EJBHomeFactory.lookUpLocalHome(
					 	 LocalArPdcHome.JNDI_NAME, LocalArPdcHome.class);			
			
		} catch(NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {					
			
			boolean firstArgument = true;
		    short ctr = 0;
			int criteriaSize = criteria.size();	
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(pdc) FROM ArPdc pdc WHERE (");
			
			if (branchList.isEmpty())
				throw new GlobalNoRecordFoundException();
			
			Iterator brIter = branchList.iterator();
			
			AdBranchDetails brDetails = (AdBranchDetails)brIter.next();
		    jbossQl.append("pdc.pdcAdBranch=" + brDetails.getBrCode());
		  
		    while(brIter.hasNext()) {
		  	
		    	brDetails = (AdBranchDetails)brIter.next();
		  		
		  		jbossQl.append(" OR pdc.pdcAdBranch=" + brDetails.getBrCode());
		  	
		    }
		  
		    jbossQl.append(") ");
		    firstArgument = false;
			
			Object obj[];

			// Allocate the size of the object parameter
									
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("maturityDateFrom")) {
				
			  	 if (!firstArgument) {
			  	 	jbossQl.append("AND ");
			  	 } else {
			  	 	firstArgument = false;
			  	 	jbossQl.append("WHERE ");
			  	 }
			  	 jbossQl.append("pdc.pdcMaturityDate>=?" + (ctr+1) + " ");
			  	 obj[ctr] = (Date)criteria.get("maturityDateFrom");
			  	 ctr++;
			  	 
			}  
			
			if (criteria.containsKey("maturityDateTo")) {
				
			  	 if (!firstArgument) {
			  	 	jbossQl.append("AND ");
			  	 } else {
			  	 	firstArgument = false;
			  	 	jbossQl.append("WHERE ");
			  	 }
			  	 jbossQl.append("pdc.pdcMaturityDate<=?" + (ctr+1) + " ");
			  	 obj[ctr] = (Date)criteria.get("maturityDateTo");
			  	 ctr++;
			  	 
			}  
			
			
			if (criteria.containsKey("status")) {
				 
			  	 if (!firstArgument) {
			  	 	jbossQl.append("AND ");
			  	 } else {
			  	 	firstArgument = false;
			  	 	jbossQl.append("WHERE ");
			  	 }
			  	 			  	 			  	 			  	 			  	 			  	 
			  	 jbossQl.append("pdc.pdcStatus=?" + (ctr+1) + " ");
			  	 obj[ctr] = (String) criteria.get("status");
			  	 ctr++;
			  	 
			}  
			
			
			if (criteria.containsKey("isCancelled")) {
				
				if (!firstArgument) {
			  	 	jbossQl.append("AND ");
			  	} else {
			  	 	firstArgument = false;
			  	 	jbossQl.append("WHERE ");
			  	}
				
				jbossQl.append("pdc.pdcCancelled=?" + (ctr+1) + " ");
				
			  	obj[ctr] = (Byte) criteria.get("isCancelled");
			  	ctr++;
			}
									
		    if (!firstArgument) {
	       	  	
				jbossQl.append("AND ");
   	     
	   	    } else {
	   	  	
		   	  	firstArgument = false;
		   	  	jbossQl.append("WHERE ");
	   	  	 
	   	    }	       	  	
			
			jbossQl.append("pdc.pdcAdCompany = " + AD_CMPNY + " ORDER BY pdc.arCustomer.cstCode");
			
			Collection arPostDatedChecks = arPdcHome.getPdcByCriteria(jbossQl.toString(), obj);
			
			if(arPostDatedChecks.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			Iterator i = arPostDatedChecks.iterator();
			
			while(i.hasNext()) {
				
				LocalArPdc arPostDatedCheck = (LocalArPdc) i.next();
				
				ArRepPostDatedCheckDetails details = new ArRepPostDatedCheckDetails();
				
				details.setPdcCustomerCode(arPostDatedCheck.getArCustomer().getCstCustomerCode());
				details.setPdcCustomerName(arPostDatedCheck.getArCustomer().getCstName());
				details.setPdcDescription(arPostDatedCheck.getPdcDescription());				
				details.setPdcDateReceived(arPostDatedCheck.getPdcDateReceived());
				details.setPdcMaturityDate(arPostDatedCheck.getPdcMaturityDate());
				details.setPdcCheckNumber(arPostDatedCheck.getPdcCheckNumber());
				details.setPdcAmount(arPostDatedCheck.getPdcAmount());
				list.add(details);
				
			}
			
			if (list.isEmpty()) {
			  	
			  	  throw new GlobalNoRecordFoundException();
			  	
			}
				     
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
		  	 
			throw ex;
		  	
		} catch (Exception ex) {
		  	
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
		  	
		}		
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepAgingControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	 // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepAgingControllerBean ejbCreate");
      
    }

}
