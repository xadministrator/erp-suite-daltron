
/*
 * ArRepReceiptEditListControllerBean.java
 *
 * Created on June 25, 2004, 1:30 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArRepReceiptEditListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepReceiptEditListControllerEJB"
 *           display-name="Used for printing receipt list transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepReceiptEditListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepReceiptEditListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepReceiptEditListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class ArRepReceiptEditListControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepReceiptEditList(ArrayList rctCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepReceiptPrintControllerBean executeArRepReceiptEditList");
        
        LocalArReceiptHome arReceiptHome = null;        

        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
        	arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = rctCodeList.iterator();
        	
        	while (i.hasNext()) {
        	
        	    Integer RCT_CODE = (Integer) i.next();
        	
	        	LocalArReceipt arReceipt = null;

	        	try {
	        		
	        		arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

	        	} catch (FinderException ex) {
	        		
	        		continue;
	        		
	        	}
	        		        	
	        	// get receipt distribution records
	        	
	        	Collection arDistributionRecords = arReceipt.getArDistributionRecords();        	            
	        	
	        	Iterator drIter = arDistributionRecords.iterator();
	        	
	        	while (drIter.hasNext()) {
	        		
	        		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drIter.next();
	        		
	        		ArRepReceiptEditListDetails details = new ArRepReceiptEditListDetails();

	        		details.setRelRctBatchName(arReceipt.getArReceiptBatch() != null ? arReceipt.getArReceiptBatch().getRbName() : "N/A");
	        		details.setRelRctBatchDescription(arReceipt.getArReceiptBatch() != null ? arReceipt.getArReceiptBatch().getRbDescription() : "N/A");
	        		details.setRelRctTransactionTotal(arReceipt.getArReceiptBatch() != null ? arReceipt.getArReceiptBatch().getArReceipts().size() : 0);
	        		details.setRelRctDateCreated(arReceipt.getRctDateCreated());
	        		details.setRelRctCreatedBy(arReceipt.getRctCreatedBy());
	        		details.setRelRctNumber(arReceipt.getRctNumber());
	        		details.setRelRctDate(arReceipt.getRctDate());
	        		details.setRelCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode());
	        		details.setRelCstCustomerName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
	        		details.setRelRctReferenceNumber(arReceipt.getRctReferenceNumber());
	        		details.setRelRctDescription(arReceipt.getRctDescription());
	        		details.setRelRctAmount(arReceipt.getRctAmount());        		
	        		details.setRelDrAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
	        		details.setRelDrAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
	        		details.setRelDrClass(arDistributionRecord.getDrClass());
	        		details.setRelDrDebit(arDistributionRecord.getDrDebit());
	        		details.setRelDrAmount(arDistributionRecord.getDrAmount());	        		
				    
	        		list.add(details);

	        	}
	        	
	        }
	        
	        if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	        }        	       	
        	
        	return list;        	
        	        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepReceiptEditListSub(ArrayList rctCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepReceiptPrintControllerBean executeArRepReceiptEditListSub");
        
        LocalArReceiptHome arReceiptHome = null;        

        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
        	arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = rctCodeList.iterator();
        	
        	while (i.hasNext()) {
        	
        	    Integer RCT_CODE = (Integer) i.next();
        	
	        	LocalArReceipt arReceipt = null;

	        	try {
	        		
	        		arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

	        	} catch (FinderException ex) {
	        		
	        		continue;
	        		
	        	}
	        		        	
	        	// get receipt distribution records
	        	
	        	Collection arAppliedInvoices = arReceipt.getArAppliedInvoices();        	            
	        	
	        	Iterator invIter = arAppliedInvoices.iterator();
	        	
	        	while (invIter.hasNext()) {
	        		
	        		LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)invIter.next();
	        		
	        		ArRepReceiptEditListDetails details = new ArRepReceiptEditListDetails();

	        		details.setRelRctNumber(arReceipt.getRctNumber());
	        		details.setRelInvNumber(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvNumber());
	        		details.setRelInvAmountApplied(arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditableWTax() + arAppliedInvoice.getAiDiscountAmount());
	        		
	        		list.add(details);
	        		
	        	}
	        	
	        }
	        
	        if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	        }        	       	
        	
        	return list;        	
        	        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ApRepVoucherPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ApRepVoucherPrintControllerBean ejbCreate");
      
    }
}
