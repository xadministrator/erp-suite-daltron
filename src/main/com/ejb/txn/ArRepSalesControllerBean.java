
/*
 * ArRepSalesControllerBean.java
 *
 * Created on February 02, 2005, 1:12 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.ar.LocalArSalesOrderLineHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPhysicalInventoryHome;
import com.ejb.inv.LocalInvPhysicalInventoryLineHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepSalesDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
* @ejb:bean name="ArRepSalesControllerEJB"
*           display-name="Used for generation of sales reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/ArRepSalesControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.ArRepSalesController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.ArRepSalesControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="aruser"
*                        role-link="aruserlink"
*
* @ejb:permission role-name="aruser"
* 
*/

public class ArRepSalesControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerBatchAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesControllerBean getAdLvCustomerBatchAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesControllerBean getAdLvReportTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR REPORT TYPE - ITEM SALES", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArRepSalesControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeArRepSales(HashMap criteria, ArrayList branchList, String GROUP_BY, String ORDER_BY, boolean INCLUDECM, boolean INCLUDEINVOICE, boolean INCLUDEMISC, boolean INCLUDECOLLECTION, boolean SHOW_ENTRIES,  Integer AD_CMPNY)
		throws GlobalNoRecordFoundException {
		
		Debug.print("ArRepSalesControllerBean executeArRepSales");
		
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
		LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;     
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
		LocalArSalesOrderLineHome arSalesOrderLineHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
			invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			arSalesOrderLineHome = (LocalArSalesOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
					lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
					lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
	
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			String customerCode = null;
			String customerBatch = null;
			String salesperson = null;
			String typeStatus = null;
			String region = null;
			
			if (criteria.containsKey("customerCode")) {
				
				customerCode = (String)criteria.get("customerCode");
				
			}
			
			if (criteria.containsKey("customerBatch")) {
				
				customerBatch = (String)criteria.get("customerBatch");
				
			}

			if (criteria.containsKey("salesperson")) {

				salesperson = (String)criteria.get("salesperson");

			}
			
			if (criteria.containsKey("typeStatus")) {

				typeStatus = (String)criteria.get("typeStatus");

			}

			if (criteria.containsKey("region")) {

				region = (String)criteria.get("region");

			}

			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(cst) FROM InvCosting cst ");

			boolean firstArgument = true;
			short ctr = 0;	      
			int criteriaSize = 0;

			Object obj[] = null;

			if (branchList.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			} else {

				jbossQl.append(" WHERE cst.cstAdBranch in (");

				boolean firstLoop = true;

				Iterator j = branchList.iterator();

				while(j.hasNext()) {

					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}

					AdBranchDetails mdetails = (AdBranchDetails) j.next();

					jbossQl.append(mdetails.getBrCode());

				}

				jbossQl.append(") ");

				firstArgument = false;

			}                    
			
			

			// Allocate the size of the object parameter

			if (criteria.containsKey("category")) {

				criteriaSize++;

			}

			if (criteria.containsKey("location")) {

				criteriaSize++;

			}

			if (criteria.containsKey("dateFrom")) {

				criteriaSize++;

			}

			if (criteria.containsKey("dateTo")) {

				criteriaSize++;

			}
			
			if (criteria.containsKey("numberFrom")) {

				criteriaSize++;

			}

			if (criteria.containsKey("numberTo")) {

				criteriaSize++;

			}

			if (criteria.containsKey("itemClass")) {

				criteriaSize++;

			}
			
			if (criteria.containsKey("itemName")) {

				criteriaSize++;

			}
			
			  		

			
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("cst.invItemLocation.invItem.iiName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemName");
				ctr++;

			}	

			if (criteria.containsKey("category")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("cst.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;

			}	

			if (criteria.containsKey("location")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("cst.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;

			}

			if (criteria.containsKey("dateFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.cstDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;

			}  

			if (criteria.containsKey("dateTo")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.cstDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;

			}
			
			if (criteria.containsKey("numberFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.arInvoiceLineItem.arInvoice.invNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("numberFrom");
				System.out.println("Number From: "+(String)criteria.get("numberFrom"));
				ctr++;

			}  

			if (criteria.containsKey("numberTo")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.arInvoiceLineItem.arInvoice.invNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("numberTo");
				System.out.println("Number To: "+(String)criteria.get("numberTo"));
				ctr++;

			}

			if (criteria.containsKey("itemClass")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("cst.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemClass");
				ctr++;

			}
			
			if (criteria.containsKey("includeZeroes")) {
				
				if(criteria.get("includeZeroes").equals("NO"))
					jbossQl.append(" AND cst.cstQuantitySold <> 0 ");

			}
			
			

			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			jbossQl.append(" cst.cstAdCompany=" + AD_CMPNY + " ");

			jbossQl.append("ORDER BY cst.invItemLocation.invItem.iiName, cst.cstDate, cst.cstLineNumber");
			System.out.println("jbossQl.toString(): " + jbossQl.toString());
			Collection invCostings = invCostingHome.getCstByCriteria(jbossQl.toString(), obj);
			
			System.out.println("invCostingsSize-" + invCostings.size());
			System.out.println("INCLUDECM-----"+INCLUDECM);
			System.out.println("INCLUDEINVOICE-----"+INCLUDEINVOICE);
			System.out.println("INCLUDECOLLECTION-----"+INCLUDECOLLECTION);
			System.out.println("INCLUDEMISC-----"+INCLUDEMISC);
			System.out.println("SHOW_ENTRIES-----"+SHOW_ENTRIES);
			

			Iterator i = invCostings.iterator();
			

								
			
			while (i.hasNext()) {

				LocalInvCosting invCosting = (LocalInvCosting) i.next();
				
				if (invCosting.getArInvoiceLineItem() != null && invCosting.getArInvoiceLineItem().getArInvoice() != null && invCosting.getArInvoiceLineItem().getArInvoice().getInvVoid() == EJBCommon.FALSE ||
				   invCosting.getArInvoiceLineItem() != null && invCosting.getArInvoiceLineItem().getArReceipt() != null && invCosting.getArInvoiceLineItem().getArReceipt().getRctVoid() == EJBCommon.FALSE ||
				   invCosting.getArSalesOrderInvoiceLine() != null && invCosting.getArSalesOrderInvoiceLine().getArInvoice() != null && invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvVoid() == EJBCommon.FALSE) {
					System.out.println("DATE--"+invCosting.getCstDate());
					System.out.println("IICODE---"+invCosting.getCstCode());
					
					
					if(INCLUDEINVOICE){

						System.out.println("INCLUDEINVOICE");
						
					//	if(invCosting.getArInvoiceLineItem().getArInvoice() != null && invCosting.getArInvoiceLineItem().getArInvoice().getInvCreditMemo() == 0){
							System.out.println("1");
							ArRepSalesDetails details = new ArRepSalesDetails();
							
							details.setSlsItemName(invCosting.getInvItemLocation().getInvItem().getIiName());
							details.setSlsItemDescription(invCosting.getInvItemLocation().getInvItem().getIiDescription());
							details.setSlsDate(invCosting.getCstDate());
							details.setSlsShowEntries(SHOW_ENTRIES);
							System.out.println("1BRANCH="+invCosting.getCstAdBranch());
							details.setSlsAdBranch(invCosting.getCstAdBranch());
							details.setSlsSource("INV");
							details.setSlsItemAdLvCategory(invCosting.getInvItemLocation().getInvItem().getIiAdLvCategory());
							
							System.out.println("2");
							
							System.out.println("ar inv line item is: " + invCosting.getArInvoiceLineItem());
							if (invCosting.getArInvoiceLineItem() != null ) {
								if(invCosting.getArInvoiceLineItem().getArInvoice()!=null){
										
										
									if( invCosting.getArInvoiceLineItem().getArInvoice().getInvCreditMemo() == 0){
										System.out.println("invCosting.getArInvoiceLineItem()--------------");
										
										
										System.out.println("invCosting.getArInvoiceLineItem().getIliQuantity()--"+invCosting.getArInvoiceLineItem().getIliQuantity());
										details.setSlsQuantitySold(invCosting.getArInvoiceLineItem().getIliQuantity());
										details.setSlsUnitPrice(EJBCommon.roundIt((invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
										System.out.println("unit: " + invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
										details.setSlsUnit(invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
										details.setSlsAmount(EJBCommon.roundIt(invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
										details.setSlsOutputVat(invCosting.getArInvoiceLineItem().getIliTaxAmount());
										details.setSlsGrossUnitPrice(invCosting.getArInvoiceLineItem().getIliUnitPrice());
										details.setSlsDiscount(invCosting.getArInvoiceLineItem().getIliTotalDiscount());
										details.setSlsDefaultUnitPrice(invCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem().getIiUnitCost());
										try{						
											String slsRefNum = (invCosting.getArInvoiceLineItem().getArInvoice().getInvReferenceNumber());
											details.setSlsReferenceNumber(slsRefNum == null ? "" : slsRefNum);
										}catch(Exception ex){
											details.setSlsReferenceNumber("");
										}
										//System.out.println("1 : "+details.getSlsReferenceNumber());
										if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {
											System.out.println("invCosting.getArInvoiceLineItem().getArInvoice() != null");
											details.setSlsCustomerCode(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstCustomerCode());
											details.setSlsCustomerBatch(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstCustomerBatch());
											
											details.setSlsCustomerName(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstName());
											details.setSlsDocumentNumber(invCosting.getArInvoiceLineItem().getArInvoice().getInvNumber());
											details.setSlsCustomerAddress(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstAddress());							
											details.setSlsCustomerStateProvince(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstStateProvince());
											System.out.println("REGION"+invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getArCustomerClass().getCcName());
											details.setSlsCustomerClass(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getArCustomerClass().getCcName());
											
											
											Collection arInvoicePaymentSchedules = arInvoicePaymentScheduleHome.findByIpsLockAndInvCode((byte)(0), invCosting.getArInvoiceLineItem().getArInvoice().getInvCode(), invCosting.getArInvoiceLineItem().getArInvoice().getInvAdCompany());
											
											Iterator xyz = arInvoicePaymentSchedules.iterator();
											Date dueDate = null;
											while(xyz.hasNext()){
												LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)xyz.next();
												dueDate = arInvoicePaymentSchedule.getIpsDueDate();
											}
											System.out.println("DUE DATE REAL-----"+dueDate);
											
											details.setSlsEffectivityDate(dueDate);
											
											
										try{
											if(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson() != null){
												details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpSalespersonCode());
												details.setSlsSalespersonName(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpName());								
											}
										}catch(Exception ex){
											details.setSlsSalespersonCode("");
											details.setSlsSalespersonName("");
										}
										
											
											details.setSlsCustomerRegion(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstAdLvRegion());
										
										}
										if (invCosting.getArInvoiceLineItem().getArReceipt() != null) {
											System.out.println("invCosting.getArInvoiceLineItem().getArReceipt() != null");
											
											details.setSlsCustomerCode(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstCustomerCode());
											details.setSlsCustomerBatch(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstCustomerBatch());
											
											details.setSlsCustomerName(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstName());
											details.setSlsDocumentNumber(invCosting.getArInvoiceLineItem().getArReceipt().getRctNumber());
											details.setSlsCustomerAddress(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstAddress());
											
											try{
												if(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson() != null){								
													details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpSalespersonCode());
													details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpName());								
												}
											}catch(Exception ex){
												details.setSlsSalespersonCode("");
												details.setSlsSalespersonName("");
											}
											
											try{						
												String slsRefNum = (invCosting.getArInvoiceLineItem().getArReceipt().getRctReferenceNumber());
												details.setSlsReferenceNumber(slsRefNum == null ? "" : slsRefNum);
											}catch(Exception ex){
												details.setSlsReferenceNumber("");
											}
											
											details.setSlsCustomerRegion(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstAdLvRegion());							
											details.setSlsCustomerStateProvince(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstStateProvince());
										
										}
										
										if (invCosting.getArInvoiceLineItem().getArInvoice() != null && invCosting.getArInvoiceLineItem().getArInvoice().getInvCreditMemo() == 1) {
											
											details.setSlsQuantitySold(invCosting.getArInvoiceLineItem().getIliQuantity() * -1);
											details.setSlsAmount(EJBCommon.roundIt(invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)) * -1);
											details.setSlsOutputVat(invCosting.getArInvoiceLineItem().getIliTaxAmount() * -1);
											details.setSlsDiscount(invCosting.getArInvoiceLineItem().getIliTotalDiscount() * -1);
										
										}
										
										System.out.println("3");					
										details.setOrderBy(ORDER_BY);
										
										if(customerCode != null && !details.getSlsCustomerCode().contains(customerCode)) continue;
										if(customerBatch != null && !details.getSlsCustomerBatch().contains(customerBatch)) continue;
										if(salesperson != null && (details.getSlsSalespersonCode() == null || 
												(details.getSlsSalespersonCode() != null && !details.getSlsSalespersonCode().contains(salesperson)))) continue;
										if(region != null && (details.getSlsCustomerRegion() == null || 
												(details.getSlsCustomerRegion() != null && !details.getSlsCustomerRegion().equals(region)))) continue;
										System.out.println("pass");
										list.add(details);
									}
								}
			
						} else if (invCosting.getArSalesOrderInvoiceLine() != null) {
								System.out.println("invCosting.getArSalesOrderInvoiceLine()");
								details.setSlsUnit(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
								details.setSlsQuantitySold(invCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered());
								details.setSlsUnitPrice(EJBCommon.roundIt((invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsUnit(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
								System.out.println("unit: " + invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
								details.setSlsAmount(EJBCommon.roundIt(invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsOutputVat(invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount());
								details.setSlsGrossUnitPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolUnitPrice());
								details.setSlsDiscount(invCosting.getArSalesOrderInvoiceLine().getSilTotalDiscount());
								details.setSlsDefaultUnitPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem().getIiUnitCost());
								System.out.println(" ---- " + invCosting.getArSalesOrderInvoiceLine().getArInvoice());
								if (invCosting.getArSalesOrderInvoiceLine().getArInvoice() != null) {
									
									details.setSlsCustomerCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstCustomerCode());
									details.setSlsCustomerBatch(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstCustomerBatch());
									
									details.setSlsCustomerName(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstName());
									details.setSlsDocumentNumber(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber());
									details.setSlsCustomerAddress(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstAddress());

									try{
										if(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson() != null){
											details.setSlsSalespersonCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpSalespersonCode());
											details.setSlsSalespersonCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpName());
											//details.setSlsSalespersonName(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpName());
										}
									}catch(Exception ex){
										details.setSlsSalespersonCode("");
										details.setSlsSalespersonName("");
									}
									
									details.setSlsCustomerRegion(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstAdLvRegion());
									details.setSlsCustomerStateProvince(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstStateProvince());							
									
									details.setSlsSalesOrderNumber(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoDocumentNumber());
									details.setSlsSalesOrderDate(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoDate());
									details.setSlsSalesOrderQuantity(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolQuantity());
									details.setSlsSalesOrderAmount(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolAmount());
									details.setSlsSalesOrderSalesPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolUnitPrice());
									
									try{
										details.setSlsReferenceNumber(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoReferenceNumber());
									}catch(Exception ex){
										details.setSlsReferenceNumber("");
									}
									//System.out.println("2 : "+details.getSlsReferenceNumber());
									
									System.out.println("3");					
									details.setOrderBy(ORDER_BY);
									
									if(customerCode != null && !details.getSlsCustomerCode().contains(customerCode)) continue;
									if(customerBatch != null && !details.getSlsCustomerBatch().contains(customerBatch)) continue;
									if(salesperson != null && (details.getSlsSalespersonCode() == null || 
											(details.getSlsSalespersonCode() != null && !details.getSlsSalespersonCode().contains(salesperson)))) continue;
									if(region != null && (details.getSlsCustomerRegion() == null || 
											(details.getSlsCustomerRegion() != null && !details.getSlsCustomerRegion().equals(region)))) continue;
									System.out.println("pass");
									list.add(details);
									
								}
								
								
			
							}
							
							
							
							
							
						
					//	}
						
					}
					
					
					

					
					if(INCLUDECM){

						System.out.println("INCLUDECM");
						
						if(invCosting.getArInvoiceLineItem().getArInvoice() != null && invCosting.getArInvoiceLineItem().getArInvoice().getInvCreditMemo() == 1){
							System.out.println("1");
							ArRepSalesDetails details = new ArRepSalesDetails();
							
							details.setSlsItemName(invCosting.getInvItemLocation().getInvItem().getIiName());
							details.setSlsItemDescription(invCosting.getInvItemLocation().getInvItem().getIiDescription());
							details.setSlsDate(invCosting.getCstDate());
							details.setSlsSource("CM");
							details.setSlsShowEntries(SHOW_ENTRIES);
							System.out.println("1BRANCH="+invCosting.getCstAdBranch());
							details.setSlsAdBranch(invCosting.getCstAdBranch());
							details.setSlsItemAdLvCategory(invCosting.getInvItemLocation().getInvItem().getIiAdLvCategory());
							System.out.println("2");
							if (invCosting.getArInvoiceLineItem() != null) {
								System.out.println("invCosting.getArInvoiceLineItem()--------------");
			
								details.setSlsUnit(invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
								System.out.println("invCosting.getArInvoiceLineItem().getIliQuantity()--"+invCosting.getArInvoiceLineItem().getIliQuantity());
								details.setSlsQuantitySold(invCosting.getArInvoiceLineItem().getIliQuantity());
								details.setSlsUnitPrice(EJBCommon.roundIt((invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsUnit(invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
								details.setSlsAmount(EJBCommon.roundIt(invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsOutputVat(invCosting.getArInvoiceLineItem().getIliTaxAmount());
								details.setSlsGrossUnitPrice(invCosting.getArInvoiceLineItem().getIliUnitPrice());
								details.setSlsDiscount(invCosting.getArInvoiceLineItem().getIliTotalDiscount());
								details.setSlsDefaultUnitPrice(invCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem().getIiUnitCost());
								try{						
									String slsRefNum = (invCosting.getArInvoiceLineItem().getArInvoice().getInvReferenceNumber());
									details.setSlsReferenceNumber(slsRefNum == null ? "" : slsRefNum);
								}catch(Exception ex){
									details.setSlsReferenceNumber("");
								}
								//System.out.println("1 : "+details.getSlsReferenceNumber());
								if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {
									System.out.println("invCosting.getArInvoiceLineItem().getArInvoice() != null");
									details.setSlsCustomerCode(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstCustomerCode());
									details.setSlsCustomerBatch(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstCustomerBatch());
									
									details.setSlsCustomerName(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstName());
									details.setSlsDocumentNumber(invCosting.getArInvoiceLineItem().getArInvoice().getInvNumber());
									details.setSlsCustomerAddress(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstAddress());							
									details.setSlsCustomerStateProvince(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstStateProvince());
									System.out.println("description"+invCosting.getArInvoiceLineItem().getArInvoice().getInvDescription());
									details.setSlsDescription(invCosting.getArInvoiceLineItem().getArInvoice().getInvDescription());
									
									 
									
									
									
								try{
									if(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson() != null){
										details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpSalespersonCode());
										details.setSlsSalespersonName(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpName());								
									}else{
										System.out.println("invoice number"+invCosting.getArInvoiceLineItem().getArInvoice().getInvCmInvoiceNumber());
										LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(invCosting.getArInvoiceLineItem().getArInvoice().getInvCmInvoiceNumber(), (byte)(0), invCosting.getArInvoiceLineItem().getArInvoice().getInvAdBranch(), invCosting.getArInvoiceLineItem().getArInvoice().getInvAdCompany());
										System.out.println("getSlpSalespersonCode()"+arInvoice.getArSalesperson().getSlpSalespersonCode());
										details.setSlsCmInvoiceNumber(invCosting.getArInvoiceLineItem().getArInvoice().getInvCmInvoiceNumber());
										details.setSlsSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
										details.setSlsSalespersonName(arInvoice.getArSalesperson().getSlpName());
										
									}
								}catch(Exception ex){
									details.setSlsSalespersonCode("");
									details.setSlsSalespersonName("");
								}
								
									
									details.setSlsCustomerRegion(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstAdLvRegion());
								
								}
								if (invCosting.getArInvoiceLineItem().getArReceipt() != null) {
									System.out.println("invCosting.getArInvoiceLineItem().getArReceipt() != null");
									
									details.setSlsCustomerCode(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstCustomerCode());
									details.setSlsCustomerBatch(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstCustomerBatch());
									
									details.setSlsCustomerName(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstName());
									details.setSlsDocumentNumber(invCosting.getArInvoiceLineItem().getArReceipt().getRctNumber());
									details.setSlsCustomerAddress(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstAddress());
									
									try{
										if(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson() != null){								
											details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpSalespersonCode());
											details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpName());								
										}
									}catch(Exception ex){
										details.setSlsSalespersonCode("");
										details.setSlsSalespersonName("");
									}
									
									try{						
										String slsRefNum = (invCosting.getArInvoiceLineItem().getArReceipt().getRctReferenceNumber());
										details.setSlsReferenceNumber(slsRefNum == null ? "" : slsRefNum);
									}catch(Exception ex){
										details.setSlsReferenceNumber("");
									}
									
									details.setSlsCustomerRegion(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstAdLvRegion());							
									details.setSlsCustomerStateProvince(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstStateProvince());
								
								}
								
								if (invCosting.getArInvoiceLineItem().getArInvoice() != null && invCosting.getArInvoiceLineItem().getArInvoice().getInvCreditMemo() == 1) {
									
									details.setSlsQuantitySold(invCosting.getArInvoiceLineItem().getIliQuantity() * -1);
									details.setSlsAmount(EJBCommon.roundIt(invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)) * -1);
									details.setSlsOutputVat(invCosting.getArInvoiceLineItem().getIliTaxAmount() * -1);
									details.setSlsDiscount(invCosting.getArInvoiceLineItem().getIliTotalDiscount() * -1);
								
								}
			
							} else if (invCosting.getArSalesOrderInvoiceLine() != null) {
								System.out.println("invCosting.getArSalesOrderInvoiceLine()");
								details.setSlsUnit(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
								details.setSlsQuantitySold(invCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered());
								details.setSlsUnitPrice(EJBCommon.roundIt((invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsUnit(invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
								details.setSlsAmount(EJBCommon.roundIt(invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsOutputVat(invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount());
								details.setSlsGrossUnitPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolUnitPrice());
								details.setSlsDiscount(invCosting.getArSalesOrderInvoiceLine().getSilTotalDiscount());
								details.setSlsDefaultUnitPrice(invCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem().getIiUnitCost());
								if (invCosting.getArSalesOrderInvoiceLine().getArInvoice() != null) {
									
									details.setSlsCustomerCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstCustomerCode());
									details.setSlsCustomerBatch(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstCustomerBatch());
									
									details.setSlsCustomerName(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstName());
									details.setSlsDocumentNumber(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber());
									details.setSlsCustomerAddress(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstAddress());

									try{
										if(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson() != null){
											details.setSlsSalespersonCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpSalespersonCode());
											details.setSlsSalespersonCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpName());
											//details.setSlsSalespersonName(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpName());
										}
									}catch(Exception ex){
										details.setSlsSalespersonCode("");
										details.setSlsSalespersonName("");
									}
									
									details.setSlsCustomerRegion(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstAdLvRegion());
									details.setSlsCustomerStateProvince(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstStateProvince());							
									
									details.setSlsSalesOrderNumber(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoDocumentNumber());
									details.setSlsSalesOrderDate(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoDate());
									details.setSlsSalesOrderQuantity(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolQuantity());
									details.setSlsSalesOrderAmount(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolAmount());
									details.setSlsSalesOrderSalesPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolUnitPrice());
									
									try{
										details.setSlsReferenceNumber(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoReferenceNumber());
									}catch(Exception ex){
										details.setSlsReferenceNumber("");
									}
									//System.out.println("2 : "+details.getSlsReferenceNumber());
								}
			
							}
							System.out.println("3");					
							details.setOrderBy(ORDER_BY);
							
							if(customerCode != null && !details.getSlsCustomerCode().contains(customerCode)) continue;
							if(customerBatch != null && !details.getSlsCustomerBatch().contains(customerBatch)) continue;
							System.out.println("salesperson-----"+salesperson);
							System.out.println("getSlsSalespersonCode----"+details.getSlsSalespersonCode());
							
							if(salesperson != null && (details.getSlsSalespersonCode() == null || 
									(details.getSlsSalespersonCode() != null && !details.getSlsSalespersonCode().contains(salesperson)))) continue;
							if(typeStatus != null && !details.getSlsDescription().contains(typeStatus)) continue;
							if(region != null && (details.getSlsCustomerRegion() == null || 
									(details.getSlsCustomerRegion() != null && !details.getSlsCustomerRegion().equals(region)))) continue;
							
							list.add(details);
							
							
							
						
						}
						
					}
					
					
					
					
					
					
					if(INCLUDEMISC){
						System.out.println("INCLUDEMISC");
						
						System.out.println("ili :" + invCosting.getArInvoiceLineItem());
					//	System.out.println("rct :" + invCosting.getArInvoiceLineItem().getArReceipt());
					//	if(invCosting.getArInvoiceLineItem().getArReceipt() != null && invCosting.getArInvoiceLineItem().getArReceipt().getRctType().equals("MISC")){
						
						

							System.out.println("1");
							ArRepSalesDetails details = new ArRepSalesDetails();
							
							details.setSlsItemName(invCosting.getInvItemLocation().getInvItem().getIiName());
							details.setSlsItemDescription(invCosting.getInvItemLocation().getInvItem().getIiDescription());
							details.setSlsDate(invCosting.getCstDate());
							details.setSlsSource("MISC");
							details.setSlsShowEntries(SHOW_ENTRIES);
							System.out.println("1BRANCH="+invCosting.getCstAdBranch());
							details.setSlsAdBranch(invCosting.getCstAdBranch());
							details.setSlsItemAdLvCategory(invCosting.getInvItemLocation().getInvItem().getIiAdLvCategory());
							System.out.println("2");
							if (invCosting.getArInvoiceLineItem() != null) {
								System.out.println("invCosting.getArInvoiceLineItem()--------------");
			
								details.setSlsUnit(invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
								System.out.println("invCosting.getArInvoiceLineItem().getIliQuantity()--"+invCosting.getArInvoiceLineItem().getIliQuantity());
								details.setSlsQuantitySold(invCosting.getArInvoiceLineItem().getIliQuantity());
								details.setSlsUnitPrice(EJBCommon.roundIt((invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsUnit(invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
								details.setSlsAmount(EJBCommon.roundIt(invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsOutputVat(invCosting.getArInvoiceLineItem().getIliTaxAmount());
								details.setSlsGrossUnitPrice(invCosting.getArInvoiceLineItem().getIliUnitPrice());
								details.setSlsDiscount(invCosting.getArInvoiceLineItem().getIliTotalDiscount());
								details.setSlsDefaultUnitPrice(invCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem().getIiUnitCost());
								try{						
									String slsRefNum = (invCosting.getArInvoiceLineItem().getArInvoice().getInvReferenceNumber());
									details.setSlsReferenceNumber(slsRefNum == null ? "" : slsRefNum);
								}catch(Exception ex){
									details.setSlsReferenceNumber("");
								}
								//System.out.println("1 : "+details.getSlsReferenceNumber());
								if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {
									System.out.println("invCosting.getArInvoiceLineItem().getArInvoice() != null");
									details.setSlsCustomerCode(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstCustomerCode());
									details.setSlsCustomerBatch(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstCustomerBatch());
									
									details.setSlsCustomerName(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstName());
									details.setSlsDocumentNumber(invCosting.getArInvoiceLineItem().getArInvoice().getInvNumber());
									System.out.println("BRANCH="+invCosting.getArInvoiceLineItem().getArInvoice().getInvAdBranch());
									details.setSlsAdBranch(invCosting.getArInvoiceLineItem().getArInvoice().getInvAdBranch());
									details.setSlsCustomerAddress(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstAddress());							
									details.setSlsCustomerStateProvince(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstStateProvince());
									
								try{
									
										System.out.println("MISC SalesPersonCode="+invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpSalespersonCode());
										details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpSalespersonCode());
										details.setSlsSalespersonName(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpName());								
									
								}catch(Exception ex){
									details.setSlsSalespersonCode("DEF");
									details.setSlsSalespersonName("DEF");
								}
								
									
									details.setSlsCustomerRegion(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstAdLvRegion());
								
								}
								if (invCosting.getArInvoiceLineItem().getArReceipt() != null) {
									System.out.println("invCosting.getArInvoiceLineItem().getArReceipt() != null");
									
									if(invCosting.getArInvoiceLineItem().getArReceipt().getRctType().equals("MISC")) {
										details.setSlsCustomerCode(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstCustomerCode());
										details.setSlsCustomerBatch(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstCustomerBatch());
										details.setSlsCustomerName(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstName());
										details.setSlsDocumentNumber(invCosting.getArInvoiceLineItem().getArReceipt().getRctNumber());
										
										
										details.setSlsCustomerAddress(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstAddress());
										
										try{
											System.out.println("MISC SALES PERSON");
											if(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson() != null){		
												System.out.println("1 MISC SALES PERSON"+invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpSalespersonCode());
												details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpSalespersonCode());
												details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpName());								
											}
										}catch(Exception ex){
											details.setSlsSalespersonCode("");
											details.setSlsSalespersonName("");
										}
										
										try{						
											String slsRefNum = (invCosting.getArInvoiceLineItem().getArReceipt().getRctReferenceNumber());
											details.setSlsReferenceNumber(slsRefNum == null ? "" : slsRefNum);
										}catch(Exception ex){
											details.setSlsReferenceNumber("");
										}
										
										details.setSlsCustomerRegion(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstAdLvRegion());							
										details.setSlsCustomerStateProvince(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstStateProvince());
									
									}
									
									
								}
								
								if (invCosting.getArInvoiceLineItem().getArInvoice() != null && invCosting.getArInvoiceLineItem().getArInvoice().getInvCreditMemo() == 1) {
									
									details.setSlsQuantitySold(invCosting.getArInvoiceLineItem().getIliQuantity() * -1);
									details.setSlsAmount(EJBCommon.roundIt(invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)) * -1);
									details.setSlsOutputVat(invCosting.getArInvoiceLineItem().getIliTaxAmount() * -1);
									details.setSlsDiscount(invCosting.getArInvoiceLineItem().getIliTotalDiscount() * -1);
								
								}
			
							} else if (invCosting.getArSalesOrderInvoiceLine() != null) {
								System.out.println("invCosting.getArSalesOrderInvoiceLine()");
								details.setSlsUnit(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
								details.setSlsQuantitySold(invCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered());
								details.setSlsUnitPrice(EJBCommon.roundIt((invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsAmount(EJBCommon.roundIt(invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsOutputVat(invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount());
								details.setSlsGrossUnitPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolUnitPrice());
								details.setSlsDiscount(invCosting.getArSalesOrderInvoiceLine().getSilTotalDiscount());
								details.setSlsDefaultUnitPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem().getIiUnitCost());
								if (invCosting.getArSalesOrderInvoiceLine().getArInvoice() != null) {
									
									details.setSlsCustomerCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstCustomerCode());
									details.setSlsCustomerBatch(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstCustomerBatch());
									
									details.setSlsCustomerName(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstName());
									details.setSlsDocumentNumber(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber());
									details.setSlsCustomerAddress(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstAddress());

									try{
										if(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson() != null){
											details.setSlsSalespersonCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpSalespersonCode());
											details.setSlsSalespersonCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpName());
											//details.setSlsSalespersonName(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpName());
										}
									}catch(Exception ex){
										details.setSlsSalespersonCode("");
										details.setSlsSalespersonName("");
									}
									
									details.setSlsCustomerRegion(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstAdLvRegion());
									details.setSlsCustomerStateProvince(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstStateProvince());							
									
									details.setSlsSalesOrderNumber(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoDocumentNumber());
									details.setSlsSalesOrderDate(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoDate());
									details.setSlsSalesOrderQuantity(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolQuantity());
									details.setSlsSalesOrderAmount(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolAmount());
									details.setSlsSalesOrderSalesPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolUnitPrice());
									
									try{
										details.setSlsReferenceNumber(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoReferenceNumber());
									}catch(Exception ex){
										details.setSlsReferenceNumber("");
									}
									//System.out.println("2 : "+details.getSlsReferenceNumber());
								}
			
							}
							System.out.println("3");					
							details.setOrderBy(ORDER_BY);
							
							if(customerCode != null && !details.getSlsCustomerCode().contains(customerCode)) continue;
							if(customerBatch != null && !details.getSlsCustomerBatch().contains(customerBatch)) continue;
							if(salesperson != null && (details.getSlsSalespersonCode() == null || 
									(details.getSlsSalespersonCode() != null && !details.getSlsSalespersonCode().contains(salesperson)))) continue;
							if(region != null && (details.getSlsCustomerRegion() == null || 
									(details.getSlsCustomerRegion() != null && !details.getSlsCustomerRegion().equals(region)))) continue;
							
							list.add(details);
							
						
						}
					
					
					
					

					
					if(INCLUDECOLLECTION){
						
						System.out.println("INCLUDECOLLECTION");
						System.out.println("cosint in ili: " + invCosting.getArInvoiceLineItem());
						System.out.println("code costing: " + invCosting.getCstCode());
					//	System.out.println("cosint in rct: " + invCosting.getArInvoiceLineItem().getArReceipt());
		
						
						
					//	if(invCosting.getArInvoiceLineItem().getArReceipt() != null && invCosting.getArInvoiceLineItem().getArReceipt().getRctType().equals("COLLECTION")){
			       // if(invCosting.getArInvoiceLineItem().getArReceipt() != null && invCosting.getArInvoiceLineItem().getArReceipt().getRctType().equals("COLLECTION")){
								


							System.out.println("1");
							ArRepSalesDetails details = new ArRepSalesDetails();
							
							details.setSlsItemName(invCosting.getInvItemLocation().getInvItem().getIiName());
							details.setSlsItemDescription(invCosting.getInvItemLocation().getInvItem().getIiDescription());
							details.setSlsDate(invCosting.getCstDate());
							details.setSlsSource("CTN");
							details.setSlsShowEntries(SHOW_ENTRIES);
							System.out.println("1BRANCH="+invCosting.getCstAdBranch());
							details.setSlsAdBranch(invCosting.getCstAdBranch());
							details.setSlsItemAdLvCategory(invCosting.getInvItemLocation().getInvItem().getIiAdLvCategory());
							System.out.println("2");
							
							if (invCosting.getArInvoiceLineItem() != null) {
								System.out.println("invCosting.getArInvoiceLineItem()--------------");
			
								details.setSlsUnit(invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
								System.out.println("invCosting.getArInvoiceLineItem().getIliQuantity()--"+invCosting.getArInvoiceLineItem().getIliQuantity());
								details.setSlsQuantitySold(invCosting.getArInvoiceLineItem().getIliQuantity());
								details.setSlsUnitPrice(EJBCommon.roundIt((invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsUnit(invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
								details.setSlsAmount(EJBCommon.roundIt(invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsOutputVat(invCosting.getArInvoiceLineItem().getIliTaxAmount());
								details.setSlsGrossUnitPrice(invCosting.getArInvoiceLineItem().getIliUnitPrice());
								details.setSlsDiscount(invCosting.getArInvoiceLineItem().getIliTotalDiscount());
								details.setSlsDefaultUnitPrice(invCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem().getIiUnitCost());
								try{						
									String slsRefNum = (invCosting.getArInvoiceLineItem().getArInvoice().getInvReferenceNumber());
									details.setSlsReferenceNumber(slsRefNum == null ? "" : slsRefNum);
								}catch(Exception ex){
									details.setSlsReferenceNumber("");
								}
								//System.out.println("1 : "+details.getSlsReferenceNumber());
								if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {
									System.out.println("invCosting.getArInvoiceLineItem().getArInvoice() != null");
									details.setSlsCustomerCode(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstCustomerCode());
									details.setSlsCustomerBatch(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstCustomerBatch());
									
									details.setSlsCustomerName(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstName());
									details.setSlsDocumentNumber(invCosting.getArInvoiceLineItem().getArInvoice().getInvNumber());
									details.setSlsUnit(invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
									details.setSlsCustomerAddress(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstAddress());							
									details.setSlsCustomerStateProvince(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstStateProvince());
									
								try{
									if(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson() != null){
										details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpSalespersonCode());
										details.setSlsSalespersonName(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpName());								
									}
								}catch(Exception ex){
									details.setSlsSalespersonCode("");
									details.setSlsSalespersonName("");
								}
								
									
									details.setSlsCustomerRegion(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstAdLvRegion());
								
								}
								if (invCosting.getArInvoiceLineItem().getArReceipt() != null) {
									System.out.println("invCosting.getArInvoiceLineItem().getArReceipt() != null");
									
									
									if(invCosting.getArInvoiceLineItem().getArReceipt().getRctType().equals("COLLECTION")) {
										details.setSlsCustomerCode(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstCustomerCode());
										details.setSlsCustomerBatch(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstCustomerBatch());
										
										details.setSlsCustomerName(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstName());
										details.setSlsDocumentNumber(invCosting.getArInvoiceLineItem().getArReceipt().getRctNumber());
										details.setSlsCustomerAddress(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstAddress());
										details.setSlsUnit(invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
										try{
											if(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson() != null){								
												details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpSalespersonCode());
												details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpName());								
											}
										}catch(Exception ex){
											details.setSlsSalespersonCode("");
											details.setSlsSalespersonName("");
										}
										
										try{						
											String slsRefNum = (invCosting.getArInvoiceLineItem().getArReceipt().getRctReferenceNumber());
											details.setSlsReferenceNumber(slsRefNum == null ? "" : slsRefNum);
										}catch(Exception ex){
											details.setSlsReferenceNumber("");
										}
										
										details.setSlsCustomerRegion(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstAdLvRegion());							
										details.setSlsCustomerStateProvince(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstStateProvince());
									
									}
									
									
								}
								
								if (invCosting.getArInvoiceLineItem().getArInvoice() != null && invCosting.getArInvoiceLineItem().getArInvoice().getInvCreditMemo() == 1) {
									
									details.setSlsQuantitySold(invCosting.getArInvoiceLineItem().getIliQuantity() * -1);
									details.setSlsAmount(EJBCommon.roundIt(invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)) * -1);
									details.setSlsOutputVat(invCosting.getArInvoiceLineItem().getIliTaxAmount() * -1);
									details.setSlsDiscount(invCosting.getArInvoiceLineItem().getIliTotalDiscount() * -1);
								
								}
			
							} else if (invCosting.getArSalesOrderInvoiceLine() != null) {
								System.out.println("invCosting.getArSalesOrderInvoiceLine()");
								details.setSlsUnit(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
								details.setSlsQuantitySold(invCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered());
								details.setSlsUnitPrice(EJBCommon.roundIt((invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsAmount(EJBCommon.roundIt(invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
								details.setSlsOutputVat(invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount());
								details.setSlsGrossUnitPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolUnitPrice());
								details.setSlsUnit(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
								details.setSlsDiscount(invCosting.getArSalesOrderInvoiceLine().getSilTotalDiscount());
								details.setSlsDefaultUnitPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem().getIiUnitCost());
								if (invCosting.getArSalesOrderInvoiceLine().getArInvoice() != null) {
									
									details.setSlsCustomerCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstCustomerCode());
									details.setSlsCustomerBatch(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstCustomerBatch());
									
									details.setSlsCustomerName(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstName());
									details.setSlsDocumentNumber(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber());
									details.setSlsCustomerAddress(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstAddress());

									try{
										if(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson() != null){
											details.setSlsSalespersonCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpSalespersonCode());
											details.setSlsSalespersonCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpName());
											//details.setSlsSalespersonName(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpName());
										}
									}catch(Exception ex){
										details.setSlsSalespersonCode("");
										details.setSlsSalespersonName("");
									}
									
									details.setSlsCustomerRegion(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstAdLvRegion());
									details.setSlsCustomerStateProvince(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstStateProvince());							
									
									details.setSlsSalesOrderNumber(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoDocumentNumber());
									details.setSlsSalesOrderDate(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoDate());
									details.setSlsSalesOrderQuantity(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolQuantity());
									details.setSlsSalesOrderAmount(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolAmount());
									details.setSlsSalesOrderSalesPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolUnitPrice());
									
									try{
										details.setSlsReferenceNumber(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getArSalesOrder().getSoReferenceNumber());
									}catch(Exception ex){
										details.setSlsReferenceNumber("");
									}
									//System.out.println("2 : "+details.getSlsReferenceNumber());
								}
			
							}
							System.out.println("3");					
							details.setOrderBy(ORDER_BY);
							
							if(customerCode != null && !details.getSlsCustomerCode().contains(customerCode)) continue;
							if(customerBatch != null && !details.getSlsCustomerBatch().contains(customerBatch)) continue;
							if(salesperson != null && (details.getSlsSalespersonCode() == null || 
									(details.getSlsSalespersonCode() != null && !details.getSlsSalespersonCode().contains(salesperson)))) continue;
							if(region != null && (details.getSlsCustomerRegion() == null || 
									(details.getSlsCustomerRegion() != null && !details.getSlsCustomerRegion().equals(region)))) continue;
							
							list.add(details);
							
							
							
						
							
						
					}
					
					
					
					
					
					
					
					
					
				} 

			}
			
			if (criteria.containsKey("region")) {

				criteriaSize++;

			}
			
			/*if (criteria.containsKey("orderStatus")) {

				criteriaSize++;

			}
			
			if (((String)criteria.get("orderStatus")).equals("")) {

				criteriaSize--;

			}*/
						
			// if include unposted ---------------------
			
			if(((String)criteria.get("includeUnposted")).equals("YES")) {


				obj = new Object[criteriaSize];
				String orderBy = null;
				String orderStatus = null;
				
				// get unposted invoices
				
				jbossQl = new StringBuffer();
				firstArgument = true;
				ctr = 0;

				jbossQl.append("SELECT OBJECT(ili) FROM ArInvoiceLineItem ili ");

				if (branchList.isEmpty()) {

					throw new GlobalNoRecordFoundException();

				} else {

					jbossQl.append(" WHERE ili.arInvoice.invAdBranch in (");

					boolean firstLoop = true;

					Iterator j = branchList.iterator();

					while(j.hasNext()) {

						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}

						AdBranchDetails mdetails = (AdBranchDetails) j.next();

						jbossQl.append(mdetails.getBrCode());

					}

					jbossQl.append(") ");

					firstArgument = false;

				}  
				
				if (criteria.containsKey("itemName")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemName");
					ctr++;

				}	

				if (criteria.containsKey("category")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("category");
					ctr++;

				}	

				if (criteria.containsKey("location")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("location");
					ctr++;

				}

				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {
						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arInvoice.invDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;

				}  

				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arInvoice.invDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}

				if (criteria.containsKey("itemClass")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemClass");
					ctr++;

				}
				
				if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("ili.arInvoice.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

    			}
				
				if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("ili.arInvoice.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}
				
				if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("ili.arInvoice.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salesperson") + "%' ");

    			}
				
				if (criteria.containsKey("typeStatus")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("ili.arInvoice.invDescription LIKE '%" + (String)criteria.get("typeStatus") + "%' ");

    			}
				
				if (criteria.containsKey("region")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("ili.arInvoice.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("region");
					ctr++;
					
    			}
				

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("ili.arInvoice.invPosted = 0 AND ili.arInvoice.invVoid = 0 AND ili.iliAdCompany=" + AD_CMPNY + " ");
				
				orderBy = null;
				
				if (ORDER_BY.equals("DATE")) {
					
					orderBy = "ili.arInvoice.invDate";
					
				} else if (ORDER_BY.equals("ITEM NAME")) {
					
					orderBy = "ili.invItemLocation.invItem.iiName";
					
				} 
				
				if (orderBy != null) {
					
					jbossQl.append("ORDER BY " + orderBy);
					
				} 
				

				Collection arInvoices = arInvoiceLineItemHome.getIliByCriteria(jbossQl.toString(), obj);

				i = arInvoices.iterator();	

				while (i.hasNext()) {

					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
					
					
					ArRepSalesDetails details = new ArRepSalesDetails();

					details.setSlsItemName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
					details.setSlsItemDescription(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiDescription());
					details.setSlsItemAdLvCategory(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiAdLvCategory());
					details.setSlsUnit(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
					details.setSlsUnitPrice(EJBCommon.roundIt((arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
					details.setSlsGrossUnitPrice(arInvoiceLineItem.getIliUnitPrice());
					details.setSlsDefaultUnitPrice(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost());
					System.out.println("------------"+arInvoiceLineItem.getArInvoice().getInvCreditMemo());
					details.setSlsQuantitySold(arInvoiceLineItem.getArInvoice().getInvCreditMemo() == 0 ? 
							arInvoiceLineItem.getIliQuantity() : arInvoiceLineItem.getIliQuantity() * 0);
					details.setSlsAmount(arInvoiceLineItem.getArInvoice().getInvCreditMemo() == 0 ? 
							EJBCommon.roundIt(arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)) : 
							EJBCommon.roundIt(arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)) * -1);
					details.setSlsOutputVat(arInvoiceLineItem.getArInvoice().getInvCreditMemo() == 0 ? 
							arInvoiceLineItem.getIliTaxAmount() : arInvoiceLineItem.getIliTaxAmount() * -1);
					details.setSlsDiscount(arInvoiceLineItem.getArInvoice().getInvCreditMemo() == 0 ? 
							arInvoiceLineItem.getIliTotalDiscount() : arInvoiceLineItem.getIliTotalDiscount() * -1);

					details.setSlsDate(arInvoiceLineItem.getArInvoice().getInvDate());
					details.setSlsCustomerCode(arInvoiceLineItem.getArInvoice().getArCustomer().getCstCustomerCode());
					details.setSlsCustomerBatch(arInvoiceLineItem.getArInvoice().getArCustomer().getCstCustomerBatch());
					details.setSlsCustomerName(arInvoiceLineItem.getArInvoice().getArCustomer().getCstName());
					details.setSlsDocumentNumber(arInvoiceLineItem.getArInvoice().getInvNumber());
					details.setSlsCustomerAddress(arInvoiceLineItem.getArInvoice().getArCustomer().getCstAddress());
					
					if(arInvoiceLineItem.getArInvoice().getArSalesperson() != null){						
						details.setSlsSalespersonCode(arInvoiceLineItem.getArInvoice().getArSalesperson().getSlpSalespersonCode());
						details.setSlsSalespersonName(arInvoiceLineItem.getArInvoice().getArSalesperson().getSlpName());
					}
					details.setSlsCustomerRegion(arInvoiceLineItem.getArInvoice().getArCustomer().getCstAdLvRegion());
					details.setSlsCustomerStateProvince(arInvoiceLineItem.getArInvoice().getArCustomer().getCstStateProvince());
					try{
						details.setSlsReferenceNumber(arInvoiceLineItem.getArInvoice().getInvReferenceNumber());
					}catch(Exception ex){
						details.setSlsReferenceNumber("");
					}
					//System.out.println("3 : "+details.getSlsReferenceNumber());
					details.setOrderBy(ORDER_BY);
					
					list.add(details);
					
				} 
				
				// get unposted misc receipts
				
				jbossQl = new StringBuffer();
				firstArgument = true;
				ctr = 0;

				jbossQl.append("SELECT OBJECT(ili) FROM ArInvoiceLineItem ili ");

				if (branchList.isEmpty()) {

					throw new GlobalNoRecordFoundException();

				} else {

					jbossQl.append(" WHERE ili.arReceipt.rctAdBranch in (");

					boolean firstLoop = true;

					Iterator j = branchList.iterator();

					while(j.hasNext()) {

						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}

						AdBranchDetails mdetails = (AdBranchDetails) j.next();

						jbossQl.append(mdetails.getBrCode());

					}

					jbossQl.append(") ");

					firstArgument = false;

				}  
				
				if (criteria.containsKey("itemName")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemName");
					ctr++;

				}	

				if (criteria.containsKey("category")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("category");
					ctr++;

				}	

				if (criteria.containsKey("location")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("location");
					ctr++;

				}

				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {
						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arReceipt.rctDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;

				}  

				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arReceipt.rctDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}

				if (criteria.containsKey("itemClass")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemClass");
					ctr++;

				}
				
				if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("ili.arReceipt.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

    			}
				
				if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("ili.arReceipt.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}
				
				if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("ili.arReceipt.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salesperson") + "%' ");

    			}
				
				
				
				if (criteria.containsKey("region")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("ili.arReceipt.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("region");
					ctr++;
					
    			}

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("ili.arReceipt.rctPosted = 0 AND ili.arReceipt.rctVoid = 0 AND ili.iliAdCompany=" + AD_CMPNY + " ");
				
				orderBy = null;
				
				if (ORDER_BY.equals("DATE")) {
					
					orderBy = "ili.arReceipt.rctDate";
					
				} else if (ORDER_BY.equals("ITEM NAME")) {
					
					orderBy = "ili.invItemLocation.invItem.iiName";
					
				} 
				
				if (orderBy != null) {
					
					jbossQl.append("ORDER BY " + orderBy);
					
				} 
				System.out.println("Receipts jbossQl.toString()"+jbossQl.toString());
				Collection arReceipts = arInvoiceLineItemHome.getIliByCriteria(jbossQl.toString(), obj);

				i = arReceipts.iterator();	

				while (i.hasNext()) {

					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
					
					ArRepSalesDetails details = new ArRepSalesDetails();
					
					details.setSlsItemName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
					details.setSlsItemDescription(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiDescription());
					details.setSlsItemAdLvCategory(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiAdLvCategory());
					details.setSlsUnit(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
					details.setSlsQuantitySold(arInvoiceLineItem.getIliQuantity());
					
					
					details.setSlsUnitPrice(EJBCommon.roundIt((arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
					details.setSlsAmount(EJBCommon.roundIt(arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
					details.setSlsOutputVat(arInvoiceLineItem.getIliTaxAmount());
					details.setSlsGrossUnitPrice(arInvoiceLineItem.getIliUnitPrice());
					details.setSlsDiscount(arInvoiceLineItem.getIliTotalDiscount());
					details.setSlsDefaultUnitPrice(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost());
					
					details.setSlsDate(arInvoiceLineItem.getArReceipt().getRctDate());
					details.setSlsCustomerCode(arInvoiceLineItem.getArReceipt().getArCustomer().getCstCustomerCode());
					details.setSlsCustomerBatch(arInvoiceLineItem.getArReceipt().getArCustomer().getCstCustomerBatch());
					details.setSlsCustomerName(arInvoiceLineItem.getArReceipt().getArCustomer().getCstName());
					details.setSlsDocumentNumber(arInvoiceLineItem.getArReceipt().getRctNumber());
					details.setSlsCustomerAddress(arInvoiceLineItem.getArReceipt().getArCustomer().getCstAddress());
					System.out.println("unit: " + arInvoiceLineItem.getInvUnitOfMeasure().getUomName() );
					if(arInvoiceLineItem.getArReceipt().getArSalesperson() != null){
						details.setSlsSalespersonCode(arInvoiceLineItem.getArReceipt().getArSalesperson().getSlpSalespersonCode());
						details.setSlsSalespersonName(arInvoiceLineItem.getArReceipt().getArSalesperson().getSlpName());
					}
					
					try{						
						String slsRefNum = (arInvoiceLineItem.getArReceipt().getRctReferenceNumber());
						details.setSlsReferenceNumber(slsRefNum == null ? "" : slsRefNum);
					}catch(Exception ex){
						details.setSlsReferenceNumber("");
					}
					details.setSlsCustomerRegion(arInvoiceLineItem.getArReceipt().getArCustomer().getCstAdLvRegion());
					details.setSlsCustomerStateProvince(arInvoiceLineItem.getArReceipt().getArCustomer().getCstStateProvince());
					try{
						details.setSlsReferenceNumber(arInvoiceLineItem.getArInvoice().getInvReferenceNumber());
					}catch(Exception ex){
						details.setSlsReferenceNumber("");
					}
					//System.out.println("4 : "+details.getSlsReferenceNumber());
					details.setOrderBy(ORDER_BY);
					
					list.add(details);
					
				} 
				
				// get unposted sales order invoices
				
				jbossQl = new StringBuffer();
				firstArgument = true;
				ctr = 0;

				jbossQl.append("SELECT OBJECT(sil) FROM ArSalesOrderInvoiceLine sil ");

				if (branchList.isEmpty()) {

					throw new GlobalNoRecordFoundException();

				} else {

					jbossQl.append(" WHERE sil.arInvoice.invAdBranch in (");

					boolean firstLoop = true;

					Iterator j = branchList.iterator();

					while(j.hasNext()) {

						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}

						AdBranchDetails mdetails = (AdBranchDetails) j.next();

						jbossQl.append(mdetails.getBrCode());

					}

					jbossQl.append(") ");

					firstArgument = false;

				}  


				if (criteria.containsKey("itemName")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sil.arSalesOrderLine.invItemLocation.invItem.iiName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemName");
					ctr++;

				}	

				if (criteria.containsKey("category")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sil.arSalesOrderLine.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("category");
					ctr++;

				}	

				if (criteria.containsKey("location")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sil.arSalesOrderLine.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("location");
					ctr++;

				}

				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {
						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sil.arInvoice.invDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;

				}  

				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sil.arInvoice.invDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}

				if (criteria.containsKey("itemClass")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sil.arSalesOrderLine.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemClass");
					ctr++;

				}
				
				if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sil.arInvoice.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

    			}
				
				if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sil.arInvoice.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}
				
				if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sil.arInvoice.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salesperson") + "%' ");

    			}
				
				
				if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("ili.arReceipt.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salesperson") + "%' ");

    			}
				
				
				if (criteria.containsKey("region")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sil.arInvoice.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("region");
					ctr++;
					
    			}
				

				
				if (criteria.containsKey("orderStatus")) {
					
					
					if (!((String)criteria.get("orderStatus")).equals("")){
						if (!firstArgument) {
							jbossQl.append("AND ");
						} else {
							firstArgument = false;
							jbossQl.append("WHERE ");
						}
						orderStatus = (String)criteria.get("orderStatus");

						
						jbossQl.append("sil.arSalesOrderLine.arSalesOrder.soOrderStatus='" + orderStatus + "' ");
						
					}
				} 
				
				
				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("sil.arInvoice.invPosted = 0 AND sil.arInvoice.invVoid = 0 AND sil.silAdCompany=" + AD_CMPNY + " ");
				
				orderBy = null;
				
				if (ORDER_BY.equals("DATE")) {
					
					orderBy = "sil.arInvoice.invDate";
					
				} else if (ORDER_BY.equals("ITEM NAME")) {
					
					orderBy = "sil.arSalesOrderLine.invItemLocation.invItem.iiName";
					
				} 
				
				if (orderBy != null) {
				
				jbossQl.append("ORDER BY " + orderBy);
				
				} 				

				Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome.getSalesOrderInvoiceLineByCriteria(jbossQl.toString(), obj);

				i = arSalesOrderInvoiceLines.iterator();	
				
				while (i.hasNext()) {

					LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine) i.next();
					
					if(orderStatus!=null && !arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoOrderStatus().equals(orderStatus))
						continue;
					
					ArRepSalesDetails details = new ArRepSalesDetails();
					
					details.setSlsItemName(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiName());
					details.setSlsItemDescription(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiDescription());
					details.setSlsItemAdLvCategory(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiAdLvCategory());
					details.setSlsUnit(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
					details.setSlsQuantitySold(arSalesOrderInvoiceLine.getSilQuantityDelivered());
					details.setSlsUnitPrice(EJBCommon.roundIt((arSalesOrderInvoiceLine.getSilAmount() + arSalesOrderInvoiceLine.getSilTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
					details.setSlsAmount(EJBCommon.roundIt(arSalesOrderInvoiceLine.getSilAmount() + arSalesOrderInvoiceLine.getSilTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
					details.setSlsOutputVat(arSalesOrderInvoiceLine.getSilTaxAmount());
					details.setSlsGrossUnitPrice(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice	());
					details.setSlsDiscount(arSalesOrderInvoiceLine.getSilTotalDiscount());
					details.setSlsDefaultUnitPrice(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiUnitCost());
					
					details.setSlsDate(arSalesOrderInvoiceLine.getArInvoice().getInvDate());
					details.setSlsCustomerCode(arSalesOrderInvoiceLine.getArInvoice().getArCustomer().getCstCustomerCode());
					details.setSlsCustomerBatch(arSalesOrderInvoiceLine.getArInvoice().getArCustomer().getCstCustomerBatch());
					details.setSlsCustomerName(arSalesOrderInvoiceLine.getArInvoice().getArCustomer().getCstName());
					details.setSlsDocumentNumber(arSalesOrderInvoiceLine.getArInvoice().getInvNumber());
					details.setSlsCustomerAddress(arSalesOrderInvoiceLine.getArInvoice().getArCustomer().getCstAddress());
					
					if(arSalesOrderInvoiceLine.getArInvoice().getArSalesperson() != null){
						details.setSlsSalespersonCode(arSalesOrderInvoiceLine.getArInvoice().getArSalesperson().getSlpSalespersonCode());
						details.setSlsSalespersonName(arSalesOrderInvoiceLine.getArInvoice().getArSalesperson().getSlpName());
					}
					
					details.setSlsCustomerRegion(arSalesOrderInvoiceLine.getArInvoice().getArCustomer().getCstAdLvRegion());
					details.setSlsCustomerStateProvince(arSalesOrderInvoiceLine.getArInvoice().getArCustomer().getCstStateProvince());
					
					details.setSlsSalesOrderNumber(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoDocumentNumber());
					details.setSlsSalesOrderDate(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoDate());
					details.setSlsSalesOrderQuantity(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolQuantity());
					details.setSlsSalesOrderAmount(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolAmount());
					details.setSlsSalesOrderSalesPrice(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice());
					
					try{					
						details.setSlsReferenceNumber(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoDocumentNumber());
					}catch(Exception ex){
						details.setSlsReferenceNumber("");
					}
											details.setSlsOrderStatus(arSalesOrderInvoiceLine.getArSalesOrderLine().getArSalesOrder().getSoOrderStatus());
					details.setOrderBy(ORDER_BY);
					//System.out.println("5 : "+details.getSlsReferenceNumber());
					list.add(details);
					
				}
				
			}
			
			// if include unserved sales orders
			
			if(((String)criteria.get("includeUnservedSO")).equals("YES")) {
				
				/*if (criteria.containsKey("dateFrom")) {

					criteriaSize--;

				}*/
				/*if (((String)criteria.get("orderStatus")).equals("")) {
					
					criteriaSize--;
					
				}
				
				if (criteria.containsKey("orderStatus")) {

					criteriaSize++;

				}*/
				String orderStatus = null;
				obj = new Object[criteriaSize];
				String orderBy = null;
				
				jbossQl = new StringBuffer();
				firstArgument = true;
				ctr = 0;

				jbossQl.append("SELECT OBJECT(sol) FROM ArSalesOrderLine sol");


				if (branchList.isEmpty()) {

					throw new GlobalNoRecordFoundException();

				} else {

					jbossQl.append(" WHERE sol.arSalesOrder.soAdBranch in (");

					boolean firstLoop = true;

					Iterator j = branchList.iterator();

					while(j.hasNext()) {

						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}

						AdBranchDetails mdetails = (AdBranchDetails) j.next();

						jbossQl.append(mdetails.getBrCode());

					}

					jbossQl.append(") ");
					
					firstArgument = false;

				}  

				if (criteria.containsKey("itemName")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sol.invItemLocation.invItem.iiName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemName");
					ctr++;

				}	

				if (criteria.containsKey("category")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sol.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("category");
					ctr++;

				}	

				if (criteria.containsKey("location")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sol.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("location");
					ctr++;

				}
				
				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sol.arSalesOrder.soDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;

				}

				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sol.arSalesOrder.soDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}
				
				if (criteria.containsKey("itemClass")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sol.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemClass");
					ctr++;

				}
				
				if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sol.arSalesOrder.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

    			}
				
				if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sol.arSalesOrder.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}
				
				if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sol.arSalesOrder.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salesperson") + "%' ");

    			}
				
				if (criteria.containsKey("region")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sol.arSalesOrder.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("region");
					ctr++;
					
    			}
				
				
				if (criteria.containsKey("orderStatus")) {
					String oS = null;
					oS = (String)criteria.get("orderStatus");
					
					if (!((String)criteria.get("orderStatus")).equals("")){
						if (!firstArgument) {
							jbossQl.append("AND ");
						} else {
							firstArgument = false;
							jbossQl.append("WHERE ");
						}
						orderStatus = (String)criteria.get("orderStatus");					
						jbossQl.append("sol.arSalesOrder.soOrderStatus='" + orderStatus + "' ");
						
						
					}
					
				
				} 

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}
				
				
	
				
				if(((String)criteria.get("includeUnposted")).equals("YES")) {
				
					jbossQl.append("sol.arSalesOrder.soVoid = 0 AND sol.solAdCompany=" + AD_CMPNY + " ");
				
				} else {

					jbossQl.append("sol.arSalesOrder.soPosted = 1 AND sol.arSalesOrder.soVoid = 0 AND sol.solAdCompany=" + AD_CMPNY + " ");
				}
				
				
				
				orderBy = null;
				
				if (ORDER_BY.equals("DATE")) {
					
					orderBy = "sol.arSalesOrder.soDate";
					
				} else if (ORDER_BY.equals("ITEM NAME")) {
					
					orderBy = "sol.invItemLocation.invItem.iiName";
					
				} 
				
				if (orderBy != null) {
					
					jbossQl.append("ORDER BY " + orderBy + ", sol.arSalesOrder.soDocumentNumber");
					
				} 

				Collection arSalesOrderLines = arSalesOrderLineHome.getSalesOrderLineByCriteria(jbossQl.toString(), obj);

				System.out.println(arSalesOrderLines.size() + " the size");
				i = arSalesOrderLines.iterator();	

				while (i.hasNext()) {

					LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) i.next();

					if (!arSalesOrderLine.getArSalesOrderInvoiceLines().isEmpty()) continue;

					if(orderStatus!=null && !arSalesOrderLine.getArSalesOrder().getSoOrderStatus().equals(orderStatus))
						continue;
	
					ArRepSalesDetails details = new ArRepSalesDetails();
					
					details.setSlsItemName(arSalesOrderLine.getInvItemLocation().getInvItem().getIiName());
					details.setSlsItemDescription(arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription());
					details.setSlsItemAdLvCategory(arSalesOrderLine.getInvItemLocation().getInvItem().getIiAdLvCategory());
					details.setSlsUnit(arSalesOrderLine.getInvUnitOfMeasure().getUomName());
					details.setSlsQuantitySold(0);
					details.setSlsUnitPrice(arSalesOrderLine.getSolUnitPrice());
					details.setSlsDefaultUnitPrice(arSalesOrderLine.getInvItemLocation().getInvItem().getIiUnitCost());
					
					details.setSlsDate(arSalesOrderLine.getArSalesOrder().getSoDate());
					details.setSlsCustomerCode(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstCustomerCode());
					details.setSlsCustomerBatch(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstCustomerBatch());
					
					details.setSlsCustomerName(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstName());
					details.setSlsCustomerAddress(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstAddress());
					
					if(arSalesOrderLine.getArSalesOrder().getArSalesperson() != null){
						details.setSlsSalespersonCode(arSalesOrderLine.getArSalesOrder().getArSalesperson().getSlpSalespersonCode());
						details.setSlsSalespersonName(arSalesOrderLine.getArSalesOrder().getArSalesperson().getSlpName());
					}
					
					details.setSlsCustomerRegion(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstAdLvRegion());
					details.setSlsCustomerStateProvince(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstStateProvince());

					details.setSlsSalesOrderNumber(arSalesOrderLine.getArSalesOrder().getSoDocumentNumber());
					details.setSlsSalesOrderDate(arSalesOrderLine.getArSalesOrder().getSoDate());
					details.setSlsSalesOrderQuantity(arSalesOrderLine.getSolQuantity());
					details.setSlsSalesOrderAmount(arSalesOrderLine.getSolAmount());
					details.setSlsSalesOrderSalesPrice(arSalesOrderLine.getSolUnitPrice());
					try{
						details.setSlsReferenceNumber(arSalesOrderLine.getArSalesOrder().getSoDocumentNumber());
					}catch(Exception ex){
						details.setSlsReferenceNumber("");
					}
					details.setSlsOrderStatus(arSalesOrderLine.getArSalesOrder().getSoOrderStatus());
					details.setOrderBy(ORDER_BY);
					//System.out.println("6 : "+details.getSlsReferenceNumber());
					System.out.println("unit : " + arSalesOrderLine.getInvUnitOfMeasure().getUomName());
					list.add(details);

				}
				
			}

			if (list.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}		
			
			
			
			if (GROUP_BY.equalsIgnoreCase("CUSTOMER")) {

				Collections.sort(list, ArRepSalesDetails.CustomerComparator);
				
			} else if (GROUP_BY.equalsIgnoreCase("CATEGORY")) {

				Collections.sort(list, ArRepSalesDetails.ItemCategoryComparator);

			} else if (GROUP_BY.equalsIgnoreCase("ITEM")) {
				
				Collections.sort(list, ArRepSalesDetails.ItemComparator);
				
			} else if (GROUP_BY.equalsIgnoreCase("SALES ORDER")) {
				
				Collections.sort(list, ArRepSalesDetails.SalesOrderNumberComparator);

			} else if (GROUP_BY.equalsIgnoreCase("USR-SI")) {
				
				Collections.sort(list, ArRepSalesDetails.USRSIComparator);	
				
			} else if (GROUP_BY.equalsIgnoreCase("USR-CS")) {
				
				Collections.sort(list, ArRepSalesDetails.USRCSComparator);	
				
			} else if (GROUP_BY.equalsIgnoreCase("SIS")) {
				
				Collections.sort(list, ArRepSalesDetails.SISComparator);
				
			} else if (GROUP_BY.equalsIgnoreCase("SBS")) {
				
				Collections.sort(list, ArRepSalesDetails.SBSComparator);	
				
			} else if (GROUP_BY.equalsIgnoreCase("CMS")) {
				
				Collections.sort(list, ArRepSalesDetails.CMSComparator);	
				
			} else if (GROUP_BY.equalsIgnoreCase("CSS")) {
				
				Collections.sort(list, ArRepSalesDetails.CSSComparator);	
				
			} else if (GROUP_BY.equalsIgnoreCase("DSS")) {
				
				Collections.sort(list, ArRepSalesDetails.DSSComparator);

			} else if (GROUP_BY.equalsIgnoreCase("DSS2")) {
				
				Collections.sort(list, ArRepSalesDetails.DSS2Comparator);
				
			} else if (GROUP_BY.equalsIgnoreCase("DSS3")) {
				
				Collections.sort(list, ArRepSalesDetails.DSS3Comparator);
			}

			return list; 	  

		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeArRepSalesSub(HashMap criteria, ArrayList branchList, Integer AD_CMPNY)
		throws GlobalNoRecordFoundException {
		
		Debug.print("ArRepSalesControllerBean executeArRepSalesSub");
		
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
		LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;     
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
		LocalArSalesOrderLineHome arSalesOrderLineHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
			invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			arSalesOrderLineHome = (LocalArSalesOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
	
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {

			String customerCode = null;
			String customerBatch = null;
			String salesperson = null;
			String region = null;
			
			if (criteria.containsKey("customerCode")) {
				
				customerCode = (String)criteria.get("customerCode");
				
			}
			
			
			if (criteria.containsKey("customerBatch")) {
				
				customerBatch = (String)criteria.get("customerBatch");
				
			}

			if (criteria.containsKey("salesperson")) {

				salesperson = (String)criteria.get("salesperson");

			}

			if (criteria.containsKey("region")) {

				region = (String)criteria.get("region");

			}
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(cst) FROM InvCosting cst ");

			boolean firstArgument = true;
			short ctr = 0;	      
			int criteriaSize = 0;

			Object obj[] = null;

			if (branchList.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			} else {

				jbossQl.append(" WHERE cst.cstAdBranch in (");

				boolean firstLoop = true;

				Iterator j = branchList.iterator();

				while(j.hasNext()) {

					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}

					AdBranchDetails mdetails = (AdBranchDetails) j.next();

					jbossQl.append(mdetails.getBrCode());

				}

				jbossQl.append(") ");

				firstArgument = false;

			}                    

			// Allocate the size of the object parameter

			if (criteria.containsKey("category")) {

				criteriaSize++;

			}

			if (criteria.containsKey("location")) {

				criteriaSize++;

			}

			if (criteria.containsKey("dateFrom")) {

				criteriaSize++;

			}

			if (criteria.containsKey("dateTo")) {

				criteriaSize++;

			}

			if (criteria.containsKey("itemClass")) {

				criteriaSize++;

			}
			
			if (criteria.containsKey("itemName")) {

				criteriaSize++;

			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("cst.invItemLocation.invItem.iiName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemName");
				ctr++;

			}	

			if (criteria.containsKey("category")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("cst.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;

			}	

			if (criteria.containsKey("location")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("cst.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;

			}

			if (criteria.containsKey("dateFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.cstDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;

			}  

			if (criteria.containsKey("dateTo")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.cstDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;

			}

			if (criteria.containsKey("itemClass")) {

				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}

				jbossQl.append("cst.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemClass");
				ctr++;

			}

			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			jbossQl.append(" cst.cstQuantitySold <> 0 AND cst.cstAdCompany=" + AD_CMPNY + " ");

			jbossQl.append("ORDER BY cst.invItemLocation.invItem.iiName, cst.cstDate, cst.cstLineNumber");
			
			Collection invCostings = invCostingHome.getCstByCriteria(jbossQl.toString(), obj);

			Iterator i = invCostings.iterator();	

			while (i.hasNext()) {

				LocalInvCosting invCosting = (LocalInvCosting) i.next();
				
				if (invCosting.getArInvoiceLineItem() != null && invCosting.getArInvoiceLineItem().getArInvoice() != null && invCosting.getArInvoiceLineItem().getArInvoice().getInvVoid() == EJBCommon.FALSE ||
				   invCosting.getArInvoiceLineItem() != null && invCosting.getArInvoiceLineItem().getArReceipt() != null && invCosting.getArInvoiceLineItem().getArReceipt().getRctVoid() == EJBCommon.FALSE ||
				   invCosting.getArSalesOrderInvoiceLine() != null && invCosting.getArSalesOrderInvoiceLine().getArInvoice() != null && invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvVoid() == EJBCommon.FALSE) {
				
					ArRepSalesDetails details = new ArRepSalesDetails();
	
					details.setSlsItemName(invCosting.getInvItemLocation().getInvItem().getIiName());
					details.setSlsItemDescription(invCosting.getInvItemLocation().getInvItem().getIiDescription());
					details.setSlsDate(invCosting.getCstDate());
					details.setSlsItemAdLvCategory(invCosting.getInvItemLocation().getInvItem().getIiAdLvCategory());
	
					if (invCosting.getArInvoiceLineItem() != null) {
	
						details.setSlsUnit(invCosting.getArInvoiceLineItem().getInvUnitOfMeasure().getUomName());
						details.setSlsQuantitySold(invCosting.getArInvoiceLineItem().getIliQuantity());
						details.setSlsUnitPrice(EJBCommon.roundIt((invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
						details.setSlsAmount(EJBCommon.roundIt(invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
						details.setSlsOutputVat(invCosting.getArInvoiceLineItem().getIliTaxAmount());
						details.setSlsGrossUnitPrice(invCosting.getArInvoiceLineItem().getIliUnitPrice());
						details.setSlsDiscount(invCosting.getArInvoiceLineItem().getIliTotalDiscount());
						details.setSlsDefaultUnitPrice(invCosting.getInvItemLocation().getInvItem().getIiUnitCost());
						
						if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {
							
							details.setSlsCustomerCode(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstCustomerCode());
							details.setSlsCustomerBatch(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstCustomerBatch());
							
							details.setSlsCustomerName(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstName());
							details.setSlsDocumentNumber(invCosting.getArInvoiceLineItem().getArInvoice().getInvNumber());
							
							if(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson() != null){
								details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpSalespersonCode());
								details.setSlsSalespersonName(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpName());
							}
							
							details.setSlsCustomerRegion(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstAdLvRegion());
							details.setSlsCustomerStateProvince(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstStateProvince());
							
						}
						if (invCosting.getArInvoiceLineItem().getArReceipt() != null) {
							
							details.setSlsCustomerCode(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstCustomerCode());
							details.setSlsCustomerBatch(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstCustomerBatch());
							
							details.setSlsCustomerName(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstName());
							details.setSlsDocumentNumber(invCosting.getArInvoiceLineItem().getArReceipt().getRctNumber());
							
							if(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson() != null){								
								details.setSlsSalespersonCode(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpSalespersonCode());
								details.setSlsSalespersonName(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpName());								
							}
							
							try{						
								String slsRefNum = (invCosting.getArInvoiceLineItem().getArReceipt().getRctReferenceNumber());
								details.setSlsReferenceNumber(slsRefNum == null ? "" : slsRefNum);
							}catch(Exception ex){
								details.setSlsReferenceNumber("");
							}
							details.setSlsCustomerRegion(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstAdLvRegion());
							details.setSlsCustomerStateProvince(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstStateProvince());

						}
						
						if (invCosting.getArInvoiceLineItem().getArInvoice() != null && invCosting.getArInvoiceLineItem().getArInvoice().getInvCreditMemo() == 1) {
							
							details.setSlsQuantitySold(invCosting.getArInvoiceLineItem().getIliQuantity() * -1);
							details.setSlsAmount(EJBCommon.roundIt(invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)) * -1);
							details.setSlsOutputVat(invCosting.getArInvoiceLineItem().getIliTaxAmount() * -1);
							details.setSlsDiscount(invCosting.getArInvoiceLineItem().getIliTotalDiscount() * -1);
							
						}
	
					} else if (invCosting.getArSalesOrderInvoiceLine() != null) {
	
						details.setSlsUnit(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
						details.setSlsQuantitySold(invCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered());
						details.setSlsUnitPrice(EJBCommon.roundIt((invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
						details.setSlsAmount(EJBCommon.roundIt(invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
						details.setSlsOutputVat(invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount());
						details.setSlsGrossUnitPrice(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getSolUnitPrice());
						details.setSlsDiscount(invCosting.getArSalesOrderInvoiceLine().getSilTotalDiscount());
						details.setSlsDefaultUnitPrice(invCosting.getInvItemLocation().getInvItem().getIiUnitCost());
						
						if (invCosting.getArSalesOrderInvoiceLine().getArInvoice() != null) {
							
							details.setSlsCustomerCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstCustomerCode());
							details.setSlsCustomerBatch(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstCustomerBatch());
							
							details.setSlsCustomerName(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstName());
							details.setSlsDocumentNumber(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber());
							
							if(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson() != null){
								details.setSlsSalespersonCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpSalespersonCode());
								details.setSlsSalespersonName(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpName());
							}
							
							details.setSlsCustomerRegion(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstAdLvRegion());
							details.setSlsCustomerStateProvince(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstStateProvince());

						}
	
					}
					
					if(customerCode != null && !details.getSlsCustomerCode().contains(customerCode)) continue;
					if(customerBatch != null && !details.getSlsCustomerBatch().contains(customerBatch)) continue;
					if(salesperson != null && (details.getSlsSalespersonCode() == null || 
							(details.getSlsSalespersonCode() != null && !details.getSlsSalespersonCode().contains(salesperson)))) continue;
					if(region != null && (details.getSlsCustomerRegion() == null || 
							(details.getSlsCustomerRegion() != null && !details.getSlsCustomerRegion().equals(region)))) continue;
					
					list.add(details);
				} 

			}
			
			if (criteria.containsKey("region")) {

				criteriaSize++;

			}
			
			// if include unposted
			
			if(((String)criteria.get("includeUnposted")).equals("YES")) {
				
				obj = new Object[criteriaSize];
				String orderBy = null;
				
				// get unposted invoices
				
				jbossQl = new StringBuffer();
				firstArgument = true;
				ctr = 0;

				jbossQl.append("SELECT OBJECT(ili) FROM ArInvoiceLineItem ili ");

				if (branchList.isEmpty()) {

					throw new GlobalNoRecordFoundException();

				} else {

					jbossQl.append(" WHERE ili.arInvoice.invAdBranch in (");

					boolean firstLoop = true;

					Iterator j = branchList.iterator();

					while(j.hasNext()) {

						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}

						AdBranchDetails mdetails = (AdBranchDetails) j.next();

						jbossQl.append(mdetails.getBrCode());

					}

					jbossQl.append(") ");

					firstArgument = false;

				}  
				
				if (criteria.containsKey("itemName")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemName");
					ctr++;

				}	

				if (criteria.containsKey("category")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("category");
					ctr++;

				}	

				if (criteria.containsKey("location")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("location");
					ctr++;

				}

				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {
						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arInvoice.invDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;

				}  

				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arInvoice.invDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}

				if (criteria.containsKey("itemClass")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemClass");
					ctr++;

				}

				if (criteria.containsKey("customerCode")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arInvoice.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

				}
				
				if (criteria.containsKey("customerBatch")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arInvoice.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

				}

				if (criteria.containsKey("salesperson")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arInvoice.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salesperson") + "%' ");

				}

				if (criteria.containsKey("region")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arInvoice.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("region");
					ctr++;

				}
				
				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("ili.arInvoice.invPosted = 0 AND ili.arInvoice.invVoid = 0 AND ili.iliAdCompany=" + AD_CMPNY + " ");
				
				Collection arInvoices = arInvoiceLineItemHome.getIliByCriteria(jbossQl.toString(), obj);

				i = arInvoices.iterator();	

				while (i.hasNext()) {

					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
					
					ArRepSalesDetails details = new ArRepSalesDetails();
					
					details.setSlsItemName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
					details.setSlsItemDescription(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiDescription());
					details.setSlsItemAdLvCategory(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiAdLvCategory());
					details.setSlsUnit(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
					details.setSlsUnitPrice(EJBCommon.roundIt((arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
					details.setSlsGrossUnitPrice(arInvoiceLineItem.getIliUnitPrice());
					details.setSlsDefaultUnitPrice(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost());
					details.setSlsQuantitySold(arInvoiceLineItem.getArInvoice().getInvCreditMemo() == 0 ? 
							arInvoiceLineItem.getIliQuantity() : arInvoiceLineItem.getIliQuantity() * 0);
					details.setSlsAmount(arInvoiceLineItem.getArInvoice().getInvCreditMemo() == 0 ?
							EJBCommon.roundIt(arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)) :
							EJBCommon.roundIt(arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)) * -1);
					details.setSlsOutputVat(arInvoiceLineItem.getArInvoice().getInvCreditMemo() == 0 ?
							arInvoiceLineItem.getIliTaxAmount() : arInvoiceLineItem.getIliTaxAmount() * -1);
					details.setSlsDiscount(arInvoiceLineItem.getArInvoice().getInvCreditMemo() == 0 ?
							arInvoiceLineItem.getIliTotalDiscount() : arInvoiceLineItem.getIliTotalDiscount() * -1);

					details.setSlsDate(arInvoiceLineItem.getArInvoice().getInvDate());
					details.setSlsCustomerCode(arInvoiceLineItem.getArInvoice().getArCustomer().getCstCustomerCode());
					details.setSlsCustomerBatch(arInvoiceLineItem.getArInvoice().getArCustomer().getCstCustomerBatch());
					
					details.setSlsCustomerName(arInvoiceLineItem.getArInvoice().getArCustomer().getCstName());
					details.setSlsDocumentNumber(arInvoiceLineItem.getArInvoice().getInvNumber());

					list.add(details);
					
				} 
				
				// get unposted misc receipts
				
				jbossQl = new StringBuffer();
				firstArgument = true;
				ctr = 0;

				jbossQl.append("SELECT OBJECT(ili) FROM ArInvoiceLineItem ili ");

				if (branchList.isEmpty()) {

					throw new GlobalNoRecordFoundException();

				} else {

					jbossQl.append(" WHERE ili.arReceipt.rctAdBranch in (");

					boolean firstLoop = true;

					Iterator j = branchList.iterator();

					while(j.hasNext()) {

						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}

						AdBranchDetails mdetails = (AdBranchDetails) j.next();

						jbossQl.append(mdetails.getBrCode());

					}

					jbossQl.append(") ");

					firstArgument = false;

				}  
				
				if (criteria.containsKey("itemName")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemName");
					ctr++;

				}	

				if (criteria.containsKey("category")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("category");
					ctr++;

				}	

				if (criteria.containsKey("location")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("location");
					ctr++;

				}

				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {
						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arReceipt.rctDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;

				}  

				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arReceipt.rctDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}

				if (criteria.containsKey("itemClass")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("ili.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemClass");
					ctr++;

				}

				if (criteria.containsKey("customerCode")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arReceipt.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

				}
				
				if (criteria.containsKey("customerBatch")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arReceipt.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

				}

				if (criteria.containsKey("salesperson")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arReceipt.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salesperson") + "%' ");

				}

				if (criteria.containsKey("region")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("ili.arReceipt.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("region");
					ctr++;

				}
				
				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("ili.arReceipt.rctPosted = 0 AND ili.arReceipt.rctVoid = 0 AND ili.iliAdCompany=" + AD_CMPNY + " ");
				
				Collection arReceipts = arInvoiceLineItemHome.getIliByCriteria(jbossQl.toString(), obj);

				i = arReceipts.iterator();	

				while (i.hasNext()) {

					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
					
					ArRepSalesDetails details = new ArRepSalesDetails();
					
					details.setSlsItemName(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
					details.setSlsItemDescription(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiDescription());
					details.setSlsItemAdLvCategory(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiAdLvCategory());
					details.setSlsUnit(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
					details.setSlsQuantitySold(arInvoiceLineItem.getIliQuantity());
					details.setSlsUnitPrice(EJBCommon.roundIt((arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
					details.setSlsAmount(EJBCommon.roundIt(arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
					details.setSlsOutputVat(arInvoiceLineItem.getIliTaxAmount());
					details.setSlsGrossUnitPrice(arInvoiceLineItem.getIliUnitPrice());
					details.setSlsDiscount(arInvoiceLineItem.getIliTotalDiscount());
					details.setSlsDefaultUnitPrice(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost());
					
					details.setSlsDate(arInvoiceLineItem.getArReceipt().getRctDate());
					details.setSlsCustomerCode(arInvoiceLineItem.getArReceipt().getArCustomer().getCstCustomerCode());
					details.setSlsCustomerBatch(arInvoiceLineItem.getArReceipt().getArCustomer().getCstCustomerBatch());
					details.setSlsCustomerName(arInvoiceLineItem.getArReceipt().getArCustomer().getCstName());
					details.setSlsDocumentNumber(arInvoiceLineItem.getArReceipt().getRctNumber());

					try{						
						String slsRefNum = (arInvoiceLineItem.getArReceipt().getRctReferenceNumber());
						details.setSlsReferenceNumber(slsRefNum == null ? "" : slsRefNum);
					}catch(Exception ex){
						details.setSlsReferenceNumber("");
					}
					
					list.add(details);
					
				} 
				
				// get unposted sales order invoices
				
				jbossQl = new StringBuffer();
				firstArgument = true;
				ctr = 0;

				jbossQl.append("SELECT OBJECT(sil) FROM ArSalesOrderInvoiceLine sil ");

				if (branchList.isEmpty()) {

					throw new GlobalNoRecordFoundException();

				} else {

					jbossQl.append(" WHERE sil.arInvoice.invAdBranch in (");

					boolean firstLoop = true;

					Iterator j = branchList.iterator();

					while(j.hasNext()) {

						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}

						AdBranchDetails mdetails = (AdBranchDetails) j.next();

						jbossQl.append(mdetails.getBrCode());

					}

					jbossQl.append(") ");

					firstArgument = false;

				}  
				
				if (criteria.containsKey("itemName")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sil.arSalesOrderLine.invItemLocation.invItem.iiName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemName");
					ctr++;

				}	

				if (criteria.containsKey("category")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sil.arSalesOrderLine.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("category");
					ctr++;

				}	

				if (criteria.containsKey("location")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sil.arSalesOrderLine.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("location");
					ctr++;

				}

				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {
						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sil.arInvoice.invDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;

				}  

				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sil.arInvoice.invDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}

				if (criteria.containsKey("itemClass")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sil.arSalesOrderLine.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemClass");
					ctr++;

				}

				if (criteria.containsKey("customerCode")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sil.arInvoice.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

				}
				
				if (criteria.containsKey("customerBatch")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sil.arInvoice.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

				}

				if (criteria.containsKey("salesperson")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sil.arInvoice.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salesperson") + "%' ");

				}

				if (criteria.containsKey("region")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sil.arInvoice.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("region");
					ctr++;

				}
				
				
				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("sil.arInvoice.invPosted = 0 AND sil.arInvoice.invVoid = 0 AND sil.silAdCompany=" + AD_CMPNY + " ");
				
				Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome.getSalesOrderInvoiceLineByCriteria(jbossQl.toString(), obj);

				i = arSalesOrderInvoiceLines.iterator();	

				while (i.hasNext()) {

					LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine) i.next();
					
					ArRepSalesDetails details = new ArRepSalesDetails();
					
					details.setSlsItemName(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiName());
					details.setSlsItemDescription(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiDescription());
					details.setSlsItemAdLvCategory(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiAdLvCategory());
					details.setSlsUnit(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
					details.setSlsQuantitySold(arSalesOrderInvoiceLine.getSilQuantityDelivered());
					details.setSlsUnitPrice(EJBCommon.roundIt((arSalesOrderInvoiceLine.getSilAmount() + arSalesOrderInvoiceLine.getSilTaxAmount()) / details.getSlsQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
					details.setSlsAmount(EJBCommon.roundIt(arSalesOrderInvoiceLine.getSilAmount() + arSalesOrderInvoiceLine.getSilTaxAmount(), this.getGlFcPrecisionUnit(AD_CMPNY)));
					details.setSlsOutputVat(arSalesOrderInvoiceLine.getSilTaxAmount());
					details.setSlsGrossUnitPrice(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice());
					details.setSlsDiscount(arSalesOrderInvoiceLine.getSilTotalDiscount());
					details.setSlsDefaultUnitPrice(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiUnitCost());
					
					details.setSlsDate(arSalesOrderInvoiceLine.getArInvoice().getInvDate());
					details.setSlsCustomerCode(arSalesOrderInvoiceLine.getArInvoice().getArCustomer().getCstCustomerCode());
					details.setSlsCustomerBatch(arSalesOrderInvoiceLine.getArInvoice().getArCustomer().getCstCustomerBatch());
					details.setSlsCustomerName(arSalesOrderInvoiceLine.getArInvoice().getArCustomer().getCstName());
					details.setSlsDocumentNumber(arSalesOrderInvoiceLine.getArInvoice().getInvNumber());

					list.add(details);
					
				}
				
			}
			
			// if include unserved sales orders
			
			if(((String)criteria.get("includeUnservedSO")).equals("YES")) {
				
				/*if (criteria.containsKey("dateFrom")) {

					criteriaSize--;

				}*/

				obj = new Object[criteriaSize];
				
				jbossQl = new StringBuffer();
				firstArgument = true;
				ctr = 0;

				jbossQl.append("SELECT OBJECT(sol) FROM ArSalesOrderLine sol");


				if (branchList.isEmpty()) {

					throw new GlobalNoRecordFoundException();

				} else {

					jbossQl.append(" WHERE sol.arSalesOrder.soAdBranch in (");

					boolean firstLoop = true;

					Iterator j = branchList.iterator();

					while(j.hasNext()) {

						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}

						AdBranchDetails mdetails = (AdBranchDetails) j.next();

						jbossQl.append(mdetails.getBrCode());

					}

					jbossQl.append(") ");
					
					firstArgument = false;

				}  
				
				if (criteria.containsKey("itemName")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sol.invItemLocation.invItem.iiName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemName");
					ctr++;

				}	

				if (criteria.containsKey("category")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sol.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("category");
					ctr++;

				}	

				if (criteria.containsKey("location")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sol.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("location");
					ctr++;

				}
				
				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sol.arSalesOrder.soDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;

				}

				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("sol.arSalesOrder.soDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}
				
				if (criteria.containsKey("itemClass")) {

					if (!firstArgument) {		       	  	
						jbossQl.append("AND ");		       	     
					} else {		       	  	
						firstArgument = false;
						jbossQl.append("WHERE ");		       	  	 
					}

					jbossQl.append("sol.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemClass");
					ctr++;

				}
				
				if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sol.arSalesOrder.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

    			}
				
				if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sol.arSalesOrder.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}
				
				if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sol.arSalesOrder.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salesperson") + "%' ");

    			}
				
				if (criteria.containsKey("region")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("sol.arSalesOrder.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("region");
					ctr++;
					
    			}

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				if(((String)criteria.get("includeUnposted")).equals("YES")) {
					
					jbossQl.append("sol.arSalesOrder.soVoid = 0 AND sol.solAdCompany=" + AD_CMPNY + " ");
				
				} else {

					jbossQl.append("sol.arSalesOrder.soPosted = 1 AND sol.arSalesOrder.soVoid = 0 AND sol.solAdCompany=" + AD_CMPNY + " ");
				}
				
				Collection arSalesOrderLines = arSalesOrderLineHome.getSalesOrderLineByCriteria(jbossQl.toString(), obj);

				i = arSalesOrderLines.iterator();	

				while (i.hasNext()) {

					LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) i.next();

					if (!arSalesOrderLine.getArSalesOrderInvoiceLines().isEmpty()) continue;

					ArRepSalesDetails details = new ArRepSalesDetails();

					details.setSlsItemName(arSalesOrderLine.getInvItemLocation().getInvItem().getIiName());
					details.setSlsItemDescription(arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription());
					details.setSlsItemAdLvCategory(arSalesOrderLine.getInvItemLocation().getInvItem().getIiAdLvCategory());
					details.setSlsUnit(arSalesOrderLine.getInvUnitOfMeasure().getUomName());
					details.setSlsQuantitySold(0);
					details.setSlsUnitPrice(arSalesOrderLine.getSolUnitPrice());
					details.setSlsDefaultUnitPrice(arSalesOrderLine.getInvItemLocation().getInvItem().getIiUnitCost());
					
					details.setSlsDate(arSalesOrderLine.getArSalesOrder().getSoDate());
					details.setSlsCustomerCode(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstCustomerCode());
					details.setSlsCustomerBatch(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstCustomerBatch());
					details.setSlsCustomerName(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstName());
					details.setSlsCustomerAddress(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstAddress());
					
					if(arSalesOrderLine.getArSalesOrder().getArSalesperson() != null){
						details.setSlsSalespersonCode(arSalesOrderLine.getArSalesOrder().getArSalesperson().getSlpSalespersonCode());
						details.setSlsSalespersonName(arSalesOrderLine.getArSalesOrder().getArSalesperson().getSlpName());
					}
					details.setSlsCustomerRegion(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstAdLvRegion());
					details.setSlsCustomerStateProvince(arSalesOrderLine.getArSalesOrder().getArCustomer().getCstStateProvince());

					details.setSlsSalesOrderNumber(arSalesOrderLine.getArSalesOrder().getSoDocumentNumber());
					details.setSlsSalesOrderDate(arSalesOrderLine.getArSalesOrder().getSoDate());
					details.setSlsSalesOrderQuantity(arSalesOrderLine.getSolQuantity());
					details.setSlsSalesOrderAmount(arSalesOrderLine.getSolAmount());
					details.setSlsSalesOrderSalesPrice(arSalesOrderLine.getSolUnitPrice());
					try{
						details.setSlsReferenceNumber(arSalesOrderLine.getArSalesOrder().getSoReferenceNumber());
					}catch(Exception ex){
						details.setSlsReferenceNumber("");
					}
					//System.out.println("7 : "+details.getSlsReferenceNumber());
					list.add(details);

				}
				
			}

			if (list.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}		
			
			Collections.sort(list, ArRepSalesDetails.SubComparator);
			
			return list; 	  

		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvArRegionAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesControllerBean getAdLvArRegionAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR REGION", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArSlpAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesControllerBean getArSlpAll");
		
		LocalArSalespersonHome arSalespersonHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection arSalespersons = arSalespersonHome.findSlpAll(AD_CMPNY);
			
			Iterator i = arSalespersons.iterator();
			
			while (i.hasNext()) {
				
				LocalArSalesperson arSalesperson = (LocalArSalesperson)i.next();
				
				list.add(arSalesperson.getSlpSalespersonCode());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepSalesControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		//Debug.print("ArRepSalesControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}			
	
	// SessionBean methods
	
	/**
* @ejb:create-method view-type="remote"
**/
	public void ejbCreate() throws CreateException {
		
		Debug.print("ArRepSalesControllerBean ejbCreate");
		
	}
	
}

