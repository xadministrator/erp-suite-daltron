
/*
 * ArRepSalesOrderControllerBean.java
 *
 * Created on April 23, 2007, 5:52 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepSalesOrderDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModTagListDetails;

/**
 * @ejb:bean name="ArRepSalesOrderControllerEJB"
 *           display-name="Used for viewing sales orders"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepSalesOrderControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepSalesOrderController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepSalesOrderControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
 */

public class ArRepSalesOrderControllerBean extends AbstractSessionBean {


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerBatchAll(Integer AD_CMPNY) {

		Debug.print("ArRepSalesOrderControllerBean getAdLvCustomerBatchAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArCcAll(Integer AD_CMPNY) {

		Debug.print("ArRepSalesOrderControllerBean getArCcAll");

		LocalArCustomerClassHome arCustomerClassHome = null;
		LocalArCustomerClass arCustomerClass = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);

			Iterator i = arCustomerClasses.iterator();

			while (i.hasNext()) {

				arCustomerClass = (LocalArCustomerClass)i.next();

				list.add(arCustomerClass.getCcName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArCtAll(Integer AD_CMPNY) {

		Debug.print("ArRepSalesOrderControllerBean getArCtAll");

		LocalArCustomerTypeHome arCustomerTypeHome = null;
		LocalArCustomerType arCustomerType = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

			Iterator i = arCustomerTypes.iterator();

			while (i.hasNext()) {

				arCustomerType = (LocalArCustomerType)i.next();

				list.add(arCustomerType.getCtName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/

	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException{

		Debug.print("ArRepSalesOrderControllerBean getAdBrResAll");

		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;

		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;

		Collection adBranchResponsibilities = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (adBranchResponsibilities.isEmpty()) {

			throw new GlobalNoRecordFoundException();

		}

		try {

			Iterator i = adBranchResponsibilities.iterator();

			while(i.hasNext()) {

				adBranchResponsibility = (LocalAdBranchResponsibility)i.next();

				adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());

				AdBranchDetails details = new AdBranchDetails();
				details.setBrCode(adBranch.getBrCode());
				details.setBrBranchCode(adBranch.getBrBranchCode());
				details.setBrName(adBranch.getBrName());
				details.setBrHeadQuarter(adBranch.getBrHeadQuarter());

				list.add(details);

			}

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		return list;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeArRepSalesOrder(HashMap criteria, ArrayList branchList, String invoiceStatus, String ORDER_BY, String GROUP_BY, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ArRepSalesOrderControllerBean executeArRepSalesOrder");

		LocalArSalesOrderHome arSalesOrderHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;

		ArrayList list = new ArrayList();

		//initialized EJB Home

		try {

			arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
					lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

		} catch (NamingException ex) 	{

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size() - 1;

			StringBuffer jbossQl = new StringBuffer();
			Iterator brIter = null;
			AdBranchDetails details = null;
			Object obj[];
			Collection arSalesOrders = null;

			if (criteria.containsKey("customerCode")) {

				criteriaSize--;

			}

			if (((String)criteria.get("approvalStatus")).equals("")) {

				criteriaSize--;

			}

			//if (((String)criteria.get("orderStatus")).equals("")) {



			//}

			criteriaSize--;


			jbossQl.append("SELECT OBJECT(so) FROM ArSalesOrder so WHERE (");

			if (branchList.isEmpty())
				throw new GlobalNoRecordFoundException();

			brIter = branchList.iterator();

			details = (AdBranchDetails)brIter.next();
			jbossQl.append("so.soAdBranch=" + details.getBrCode());

			while(brIter.hasNext()) {

				details = (AdBranchDetails)brIter.next();

				jbossQl.append(" OR so.soAdBranch=" + details.getBrCode());

			}

			jbossQl.append(") ");
			firstArgument = false;

			// Allocate the size of the object parameter

			obj = new Object[criteriaSize];

			if (criteria.containsKey("customerCode")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("so.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");

			}

			if (criteria.containsKey("customerType")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("so.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("customerType");
				ctr++;

			}

			if (criteria.containsKey("customerBatch")) {

			   	  if (!firstArgument) {
			   	     jbossQl.append("AND ");
			   	  } else {
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");
			   	  }

			   	  jbossQl.append("so.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("customerBatch");
			   	  ctr++;

		      }

			if (criteria.containsKey("customerClass")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("so.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("customerClass");
				ctr++;

			}

			if (criteria.containsKey("dateFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("so.soDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;

			}

			if (criteria.containsKey("dateTo")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("so.soDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;

			}

			if (criteria.containsKey("documentNumberFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("so.soDocumentNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberFrom");
				ctr++;
			}


			if (criteria.containsKey("documentNumberTo")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("so.soDocumentNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberTo");
				ctr++;

			}


			if (criteria.containsKey("referenceNumberFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("so.soReferenceNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("referenceNumberFrom");
				ctr++;
			}


			if (criteria.containsKey("referenceNumberTo")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("so.soReferenceNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("referenceNumberTo");
				ctr++;

			}

			if (criteria.containsKey("approvalStatus")) {

				if (((String)criteria.get("approvalStatus")).equals("N/A") ||
						((String)criteria.get("approvalStatus")).equals("PENDING")||
						((String)criteria.get("approvalStatus")).equals("APPROVED")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}

					jbossQl.append("so.soApprovalStatus=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("approvalStatus");
					ctr++;
				}

			}

            if (criteria.containsKey("includedUnposted")) {

		  		String unposted = (String)criteria.get("includedUnposted");

		  		if (!firstArgument) {

		  			jbossQl.append("AND ");

		  		} else {

		  			firstArgument = false;
		  			jbossQl.append("WHERE ");

		  		}

		  		if (unposted.equals("NO")) {

		  			jbossQl.append("so.soPosted = 1 ");

		  		} else {

		  			jbossQl.append("so.soVoid = 0 ");

		  		}

		  	}

			if (criteria.containsKey("salesperson")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("so.arSalesperson.slpName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("salesperson");
				ctr++;

			}

			if (criteria.containsKey("orderStatus")) {
				System.out.println("ORDER STATUS : " + (String)criteria.get("orderStatus"));

				if (!((String)criteria.get("orderStatus")).equals("")){
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					/*
					System.out.println("ORDER STATUS : " + (String)criteria.get("orderStatus"));
					jbossQl.append("so.soOrderStatus LIKE ? 'BAD%' " + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("orderStatus");
					ctr++;*/
					if (criteria.get("orderStatus").equals("Good")){
						jbossQl.append("so.soOrderStatus = 'Good' ");
					}else{
						//System.out.println("ORDER STATUS : " + (String)criteria.get("orderStatus"));
						jbossQl.append("so.soOrderStatus LIKE 'Bad%' ");

					}

				}

			}

			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			jbossQl.append("so.soAdCompany=" + AD_CMPNY + " ");

			System.out.println("jbossQl-" + jbossQl.toString());

			arSalesOrders = arSalesOrderHome.getSOByCriteria(jbossQl.toString(), obj);

			if(!arSalesOrders.isEmpty()) {

				Iterator i = arSalesOrders.iterator();

				while (i.hasNext()) {

					LocalArSalesOrder arSalesOrder = (LocalArSalesOrder)i.next();

					ArRepSalesOrderDetails mdetails = new ArRepSalesOrderDetails();
					mdetails.setSoDate(arSalesOrder.getSoDate());
					mdetails.setSoDocumentNumber(arSalesOrder.getSoDocumentNumber());
					mdetails.setSoReferenceNumber(arSalesOrder.getSoReferenceNumber());
					mdetails.setSoDescription(arSalesOrder.getSoDescription());
					mdetails.setSoCstCustomerCode(arSalesOrder.getArCustomer().getCstCustomerCode() + "-" + arSalesOrder.getArCustomer().getCstName());
					mdetails.setSoCstCustomerClass(arSalesOrder.getArCustomer().getArCustomerClass().getCcName());
					mdetails.setSoCstCustomerCode2(arSalesOrder.getArCustomer().getCstCustomerCode());
					mdetails.setSoSlsSalespersonCode(arSalesOrder.getArSalesperson() != null ? arSalesOrder.getArSalesperson().getSlpSalespersonCode() : null);

					mdetails.setSoSlsName(arSalesOrder.getArSalesperson() != null ? arSalesOrder.getArSalesperson().getSlpName() : null);

                    // select customer type
					if (arSalesOrder.getArCustomer().getArCustomerType() == null) {

						mdetails.setSoCstCustomerType("UNDEFINED");

					} else {

						mdetails.setSoCstCustomerType(arSalesOrder.getArCustomer().getArCustomerType().getCtName());

					}

					double ORDER_AMOUNT = 0d;
					double ORDER_TAX_AMOUNT = 0d;
					double ORDER_QTY = 0d;

					if (!arSalesOrder.getArSalesOrderLines().isEmpty()) {

						Iterator j = arSalesOrder.getArSalesOrderLines().iterator();

						while (j.hasNext()) {

		  					LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine)j.next();

		  					ORDER_QTY += arSalesOrderLine.getSolQuantity();
		  					
		  					if (arSalesOrderLine.getArSalesOrder().getArTaxCode().getTcType()
		  							.equals("INCLUSIVE")) {

		  						ORDER_TAX_AMOUNT += arSalesOrderLine.getSolAmount() - EJBCommon.roundIt(arSalesOrderLine.getSolAmount() / (1 + (arSalesOrderLine.getArSalesOrder().getArTaxCode().getTcRate() / 100)), adCompany.getGlFunctionalCurrency().getFcPrecision());
		  						ORDER_AMOUNT += arSalesOrderLine.getSolAmount();

		  			        } else if (arSalesOrderLine.getArSalesOrder().getArTaxCode().getTcType()
		  							.equals("EXCLUSIVE")){

		  			            // tax exclusive, none, zero rated or exempt

		  			            ORDER_TAX_AMOUNT += EJBCommon.roundIt(arSalesOrderLine.getSolAmount() * arSalesOrderLine.getArSalesOrder().getArTaxCode().getTcRate() / 100, adCompany.getGlFunctionalCurrency().getFcPrecision());
		  			            ORDER_AMOUNT += arSalesOrderLine.getSolAmount() + ORDER_TAX_AMOUNT;

		  			        } else {

		  			        	ORDER_AMOUNT += arSalesOrderLine.getSolAmount();

		  			        }

						}

					}

					mdetails.setSoOrderQty(ORDER_QTY);
					mdetails.setSoAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
  							arSalesOrder.getGlFunctionalCurrency().getFcCode(),
  							arSalesOrder.getGlFunctionalCurrency().getFcName(),
  							arSalesOrder.getSoConversionDate(),
  							arSalesOrder.getSoConversionRate(),
							ORDER_AMOUNT, AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));

					mdetails.setSoTaxAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
  							arSalesOrder.getGlFunctionalCurrency().getFcCode(),
  							arSalesOrder.getGlFunctionalCurrency().getFcName(),
  							arSalesOrder.getSoConversionDate(),
  							arSalesOrder.getSoConversionRate(),
							ORDER_TAX_AMOUNT, AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					mdetails.setOrderBy(ORDER_BY);
					mdetails.setSoOrderStatus(arSalesOrder.getSoOrderStatus());
					mdetails.setSoApprovalStatus(arSalesOrder.getSoApprovalStatus());
					mdetails.setSoApprovedRejectedBy(arSalesOrder.getSoApprovedRejectedBy());
					
					
					double INVOICE_QTY = 0d;
					
					Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome.findSolBySoCodeAndInvAdBranch(arSalesOrder.getSoCode(), AD_BRNCH, AD_CMPNY);
					
					Iterator x = arSalesOrderInvoiceLines.iterator();
					
					while(x.hasNext()) {
						
						LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)x.next();
						
						INVOICE_QTY += arSalesOrderInvoiceLine.getSilQuantityDelivered();
						System.out.println("arSalesOrderInvoiceLine.getSilQuantityDelivered()="+arSalesOrderInvoiceLine.getSilQuantityDelivered());

					}
					
					mdetails.setSoInvoiceQty(INVOICE_QTY);
					
					list.add(mdetails);

					/*System.out.print("ORDER_AMOUNT-" + ORDER_AMOUNT);
					System.out.print("ORDER_TAX_AMOUNT-" + ORDER_TAX_AMOUNT);

					String invoiceNumbers = "";
					if(true){
						Object invObj[]= new Object[2];
						invObj[0] = arSalesOrder.getSoDocumentNumber();
						invObj[1] = AD_CMPNY;

						String invJbossQl = "SELECT OBJECT(inv) FROM ArInvoice inv WHERE inv.invSoNumber=?1 AND inv.invAdCompany=?2 ";
						Iterator invIter = arInvoiceHome.getInvByCriteria(invJbossQl.toString(), invObj).iterator();

						while(invIter.hasNext()){

							LocalArInvoice arInvoice = (LocalArInvoice)invIter.next();

							invoiceNumbers += invoiceNumbers + "/" + arInvoice.getInvNumber() ;

						}
					}

					// Eliminate Trailing Slash Char
					if(!invoiceNumbers.equals("")){

						invoiceNumbers = invoiceNumbers.substring(1);

					}

					if(invoiceStatus.equals("SERVED") && invoiceNumbers.equals("")){
						continue;
					} else if(invoiceStatus.equals("UNSERVED") && !invoiceNumbers.equals("")){
						continue;
					}

					Collection arSoLines = arSalesOrder.getArSalesOrderLines();

					Iterator solIter = arSoLines.iterator();

					String Approver = "";

					if(arSalesOrder.getSoApprovalStatus()!=null && arSalesOrder.getSoApprovalStatus().equals("PENDING")){
						try {

							Collection adApprovalQueueList = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR SALES ORDER", arSalesOrder.getSoCode(), AD_CMPNY);

							if(adApprovalQueueList.iterator().hasNext()){
								LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue) adApprovalQueueList.iterator().next();
								Approver = adApprovalQueue.getAdUser().getUsrName();
							}



						} catch (FinderException ex) {

							throw new GlobalRecordAlreadyDeletedException();

						}
					}

					while(solIter.hasNext()){

						LocalArSalesOrderLine soLine = (LocalArSalesOrderLine)solIter.next();

						ArRepSalesOrderDetails tmpdetails = new ArRepSalesOrderDetails();
						tmpdetails.setSoDate(mdetails.getSoDate());
						tmpdetails.setSoDocumentNumber(mdetails.getSoDocumentNumber());
						tmpdetails.setSoReferenceNumber(mdetails.getSoReferenceNumber());
						tmpdetails.setSoDescription(mdetails.getSoReferenceNumber());
						tmpdetails.setSoCstCustomerCode(mdetails.getSoCstCustomerCode());
						tmpdetails.setSoCstCustomerClass(mdetails.getSoCstCustomerClass());
						tmpdetails.setSoCstCustomerCode2(mdetails.getSoCstCustomerCode2());
						tmpdetails.setSoSlsSalespersonCode(mdetails.getSoSlsSalespersonCode());

						tmpdetails.setSoSlsName(mdetails.getSoSlsName());
						tmpdetails.setSoCstCustomerType(mdetails.getSoCstCustomerType());
						tmpdetails.setSoAmount(mdetails.getSoAmount());
						tmpdetails.setSoTaxAmount(mdetails.getSoTaxAmount());
						tmpdetails.setOrderBy(mdetails.getOrderBy());
						tmpdetails.setSolIIName(soLine.getInvItemLocation().getInvItem().getIiName());
						tmpdetails.setSolQty(soLine.getSolQuantity());
						tmpdetails.setSoOrderStatus(soLine.getArSalesOrder().getSoOrderStatus());
						tmpdetails.setSoApprovalStatus(soLine.getArSalesOrder().getSoApprovalStatus());
						tmpdetails.setSoApprovedRejectedBy(soLine.getArSalesOrder().getSoApprovedRejectedBy());


						if(!Approver.equals(""))
							tmpdetails.setSoApprovedRejectedBy(Approver);

						tmpdetails.setSoInvoiceNumbers(invoiceNumbers);

						list.add(tmpdetails);
					}*/



				}

			}

			if(list.isEmpty() || list.size() == 0) {

				throw new GlobalNoRecordFoundException();

			}

			// sort

			if(GROUP_BY.equals("CUSTOMER CODE")) {

				Collections.sort(list, ArRepSalesOrderDetails.CustomerCodeComparator);

			} else if(GROUP_BY.equals("CUSTOMER TYPE")) {

				Collections.sort(list, ArRepSalesOrderDetails.CustomerTypeComparator);

			} else if(GROUP_BY.equals("CUSTOMER CLASS")) {

				Collections.sort(list, ArRepSalesOrderDetails.CustomerClassComparator);

			} else if(GROUP_BY.equals("SO NUMBER")) {

				Collections.sort(list, ArRepSalesOrderDetails.SONumberComparator);

			} else {

				Collections.sort(list, ArRepSalesOrderDetails.NoGroupComparator);

			}

			return list;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {


			ex.printStackTrace();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

		Debug.print("ArRepSalesOrderControllerBean getAdCompany");

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());

			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}





	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArSlpAll(Integer AD_CMPNY) {

		Debug.print("ArRepSalesOrderControllerBean getArSlpAll");

		LocalArSalespersonHome arSalespersonHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arSalespersons = arSalespersonHome.findSlpAll(AD_CMPNY);

			Iterator i = arSalespersons.iterator();

			while (i.hasNext()) {

				LocalArSalesperson arSalesperson = (LocalArSalesperson)i.next();

				list.add(arSalesperson.getSlpName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}











	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("ArRepSalesOrderControllerBean convertForeignToFunctionalCurrency");


		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}


		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

			AMOUNT = AMOUNT / CONVERSION_RATE;

		}
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ArRepSalesOrderControllerBean ejbCreate");

	}
}
