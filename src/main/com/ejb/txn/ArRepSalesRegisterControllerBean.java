
/*
 * ArRepSalesRegisterControllerBean.java
 *
 * Created on March 12, 2004, 9:34 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArJobOrderInvoiceLine;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepOrRegisterDetails;
import com.util.ArRepSalesRegisterDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepSalesRegisterControllerEJB"
 *           display-name="Used for viewing sales"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepSalesRegisterControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepSalesRegisterController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepSalesRegisterControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepSalesRegisterControllerBean extends AbstractSessionBean {
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerBatchAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesRegisterControllerBean getAdLvCustomerBatchAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesRegisterControllerBean getAdLvReportTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR REPORT TYPE - SALES REGISTER", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getArCcAll");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalArCustomerClass arCustomerClass = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);
	        
	        Iterator i = arCustomerClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerClass = (LocalArCustomerClass)i.next();
	        	
	        	list.add(arCustomerClass.getCcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCtAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getArCtAll");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalArCustomerType arCustomerType = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

	        Iterator i = arCustomerTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerType = (LocalArCustomerType)i.next();
	        	
	        	list.add(arCustomerType.getCtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArCustomerEntryControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepSalesRegisterMemoLine(HashMap criteria, ArrayList branchList, String ORDER_BY, String GROUP_BY,
    		boolean PRINT_SALES_RELATED_ONLY, boolean SHOW_ENTRIES, boolean SUMMARIZE, boolean summary, boolean detailedSales, Integer AD_BRNCH, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {

    	Debug.print("ArRepSalesRegisterControllerBean executeArRepSalesRegisterMemoLine");

    	LocalArInvoiceHome arInvoiceHome = null;
    	LocalArReceiptHome arReceiptHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome= null;
    	LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;

    	ArrayList list = new ArrayList();

    	//initialized EJB Home

    	try {

    		arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
    		arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
    		lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
    		lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
    		arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
    		lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
    		arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);

    	} catch (NamingException ex) 	{

    		throw new EJBException(ex.getMessage());

    	}

    	try { 

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		boolean firstArgument = true;
    		short ctr = 0;
    		int criteriaSize = criteria.size();	  
    		
    		StringBuffer jbossQl = new StringBuffer();
    		Iterator brIter = null;
    		AdBranchDetails details = null;
    		Object obj[];
    		Collection arInvoices = null;

    		double GRAND_TOTAL_NET_AMNT = 0d;
    		double GRAND_TOTAL_TAX_AMNT = 0d;
    		double GRAND_TOTAL_AMNT = 0d;

    		if (criteria.containsKey("customerCode")) {

    			criteriaSize--;

    		}
    		
    		if (criteria.containsKey("customerBatch")) {

    			criteriaSize--;

    		}
    		
    		if (criteria.containsKey("unpostedOnly")) {

    			criteriaSize--;

    		}
    		
    		if (criteria.containsKey("includedUnposted")) {

    			criteriaSize--;

    		}
    		

    		if (criteria.containsKey("includedInvoices")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("includedMiscReceipts")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("includedCreditMemos")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("includedDebitMemos")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("paymentStatus")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("includedPriorDues")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("includedCollections")) {

    			criteriaSize--;

    		}
    		
    		if (criteria.containsKey("posted")) {

    			criteriaSize--;

    		}
    		
    		if (criteria.containsKey("orderStatus")) {

    			criteriaSize--;

    		}
    		
    		if (criteria.containsKey("summary")) {

    			criteriaSize--;

    		}
    		if (criteria.containsKey("detailedSales")) {

    			criteriaSize--;

    		}
    		
    		
    		if(((String)criteria.get("includedInvoices")).equals("YES") || ((String)criteria.get("includedCreditMemos")).equals("YES") || 
    				((String)criteria.get("includedDebitMemos")).equals("YES")) {

    			jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv WHERE (");

    			if (branchList.isEmpty())
    				throw new GlobalNoRecordFoundException();

    			brIter = branchList.iterator();

    			details = (AdBranchDetails)brIter.next();
    			jbossQl.append("inv.invAdBranch=" + details.getBrCode());

    			while(brIter.hasNext()) {

    				details = (AdBranchDetails)brIter.next();

    				jbossQl.append(" OR inv.invAdBranch=" + details.getBrCode());

    			}

    			jbossQl.append(") ");
    			firstArgument = false;

    			// Allocate the size of the object parameter

    			obj = new Object[criteriaSize];
    			
    			if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("inv.arCustomer.cstCustomerCode = '" + (String)criteria.get("customerCode") + "' ");

    			}
    			
    			if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("inv.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}
    			
    			if (criteria.containsKey("invoiceBatchName")) {
    				
    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.arInvoiceBatch.ibName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("invoiceBatchName");
    				ctr++;
    				
    			}

    			if (criteria.containsKey("customerType")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerType");
    				ctr++;

    			}	

    			if (criteria.containsKey("customerClass")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerClass");
    				ctr++;

    			}
    			
    			if (criteria.containsKey("dateFrom")) {

    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.invDate>=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateFrom");
    				ctr++;

    			}  	

    			if (criteria.containsKey("dateTo")) {

    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.invDate<=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateTo");
    				ctr++;

    			}    


    			if (criteria.containsKey("invoiceNumberFrom")) {

    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.invNumber>=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("invoiceNumberFrom");
    				ctr++;
    			}  


    			if (criteria.containsKey("invoiceNumberTo")) {

    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.invNumber<=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("invoiceNumberTo");
    				ctr++;

    			}

    			System.out.println("unposted Only: " + criteria.containsKey("unpostedOnly"));
    			
    			boolean unpostedOnly=false;
    			if (criteria.containsKey("unpostedOnly")) {

    				String unposted = (String)criteria.get("unpostedOnly");

    				if (unposted.equals("YES")) {
    					if (!firstArgument) {

    						jbossQl.append("AND ");

    					} else {

    						firstArgument = false;
    						jbossQl.append("WHERE ");

    					}	       	  
    					unpostedOnly=true;
    					jbossQl.append("inv.invPosted = 0 AND inv.invVoid = 0 ");

    				} 
    			}
    			
    			if (criteria.containsKey("includedUnposted") && unpostedOnly==false) {

    				String unposted = (String)criteria.get("includedUnposted");

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}	       	  
    				    			
    				if (unposted == null) unposted = "NO";
    				if (unposted.equals("NO")) {

    					jbossQl.append("inv.invPosted = 1 ");

    				} else {

    					jbossQl.append("inv.invVoid = 0 ");  	  

    				}   	 

    			}

    			if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arSalesperson.slpName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("salesperson");
    				ctr++;

    			}
    			
    			if (criteria.containsKey("paymentStatus")) {

    				String paymentStatus = (String)criteria.get("paymentStatus");

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				if (paymentStatus.equals("PAID")) {	      	 

    					jbossQl.append("inv.invAmountDue = inv.invAmountPaid ");

    				} else if (paymentStatus.equals("UNPAID")) {	         

    					jbossQl.append("inv.invAmountDue <> inv.invAmountPaid ");

    				}

    			}

    			if (criteria.containsKey("region")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("region");
    				ctr++;

    			}
    			

    			if (!firstArgument) {

    				jbossQl.append("AND ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}

    			boolean firstSource = true;

    			if(((String)criteria.get("includedCreditMemos")).equals("YES")) {

    				jbossQl.append("(inv.invCreditMemo = 1 ");
    				firstSource = false;

    			}

    			if(((String)criteria.get("includedDebitMemos")).equals("YES")) {

    				if (!firstSource) {

    					jbossQl.append("OR ");

    				} else {

    					jbossQl.append("(");

    				}

    				jbossQl.append("inv.invDebitMemo = 1 ");
    				firstSource = false;

    			}

    			if(((String)criteria.get("includedInvoices")).equals("YES")) {

    				if (!firstSource) {

    					jbossQl.append("OR ");

    				} else {

    					jbossQl.append("(");

    				}

    				jbossQl.append("(inv.invCreditMemo = 0 AND inv.invDebitMemo = 0)");
    				firstSource = false;

    			}
    			
    			
/*    			if(((String)criteria.get("Posted")).equals("YES")) {

    				if (!firstSource) {

    					jbossQl.append("OR ");

    				} else {

    					jbossQl.append("(");

    				}

    				jbossQl.append("(inv.posted = 0)");
    				firstSource = false;

    			}*/


    			jbossQl.append(") ");



    			if (!firstArgument) {

    				jbossQl.append("AND ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}
    			jbossQl.append("inv.invAdCompany=" + AD_CMPNY + " ");
    			
    			
    			
    	       	  String orderBy = null;
    	          
    	          if (GROUP_BY.equals("SALESPERSON") || GROUP_BY.equals("PRODUCT")) {
    	  	      	 
    	  	      	  orderBy = "inv.arCustomer.cstCustomerCode";

    	          } 

    	  		  if (orderBy != null) {
    	  		  
    	  			jbossQl.append("ORDER BY " + orderBy);
    	  		  }  
    			
    			    			
    			System.out.println("jbossQl.toString(): "+jbossQl.toString());
    			arInvoices = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);
    			

    			String refNum="";
    			if(!arInvoices.isEmpty()) {

    				Iterator i = arInvoices.iterator();

    				while (i.hasNext()) {

    					LocalArInvoice arInvoice = (LocalArInvoice)i.next();   	

    					/* if (PRINT_SALES_RELATED_ONLY) {
    					 * Collection arDistributionRecords = arInvoice.getArDistributionRecords();
    					 * boolean isSalesRelated = false;
    					 * while (){
    					 * 	   LocalArDistribut
    					 *    if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")
    					 *    isSalesRelated = true;
    					 *    break;
    					 * 
    					 * }
    					 * 
    					 * if (!isSalesRelated) continue;
    					 }
    				
						*/
    					
    					double REVENUE_AMOUNT = 0d;
    					
    					
    					Collection arDistributionRecords = arInvoice.getArDistributionRecords();
						
						Iterator iDr = arDistributionRecords.iterator();
						
						while (iDr.hasNext()) {
							
							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) iDr.next();
							
							
								if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
									
									if(arDistributionRecord.getDrDebit()==EJBCommon.TRUE) {
										REVENUE_AMOUNT -= arDistributionRecord.getDrAmount();
									}else {
										REVENUE_AMOUNT += arDistributionRecord.getDrAmount();
									}
								
								
							}
								
						}
						
						
						if (PRINT_SALES_RELATED_ONLY) {
							
							boolean isSalesRelated = false;
							
						//	Collection arDistributionRecords = arInvoice.getArDistributionRecords();
							
							Iterator x = arDistributionRecords.iterator();
							
							while (x.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
								
								
									if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
										
										isSalesRelated = true;
										break;
									
								}
									
							}
							
							if (!isSalesRelated)
									
								continue;
						}

    					if(SHOW_ENTRIES) {

    						boolean first = true;

    					//	Collection arDistributionRecords = arInvoice.getArDistributionRecords();

    						Iterator k = arDistributionRecords.iterator();

    						while(k.hasNext()) {

    							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)k.next();

    							ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    							mdetails.setSrDate(arInvoice.getInvDate());
    							mdetails.setSrInvoiceNumber(arInvoice.getInvNumber());
    							mdetails.setSrReferenceNumber(arInvoice.getInvReferenceNumber());
    							refNum=(arInvoice.getInvReferenceNumber());
    							mdetails.setSrDescription(arInvoice.getInvDescription());		  	  
    							mdetails.setSrCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode() + "-" + arInvoice.getArCustomer().getCstName());		
    							mdetails.setSrCstCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());
    							mdetails.setSrCstName(arInvoice.getArCustomer().getCstName());
    							mdetails.setSrCstCustomerCode2(arInvoice.getArCustomer().getCstCustomerCode());
    							mdetails.setPosted(arInvoice.getInvPosted());    							
    							
    							// type
    							if(arInvoice.getArCustomer().getArCustomerType() == null) {

    								mdetails.setSrCstCustomerType("UNDEFINE");
	    							mdetails.setPosted(arInvoice.getInvPosted());
	    							
    							} else {

    								mdetails.setSrCstCustomerType(arInvoice.getArCustomer().getArCustomerType().getCtName());
	    							mdetails.setPosted(arInvoice.getInvPosted());
	    							
    							}

    							if(first) {

    								double AMNT_DUE = 0d;

    								if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    									AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    											arInvoice.getGlFunctionalCurrency().getFcCode(),
    											arInvoice.getGlFunctionalCurrency().getFcName(),
    											arInvoice.getInvConversionDate(),
    											arInvoice.getInvConversionRate(),
    											arInvoice.getInvAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    								} else {

    									LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    									AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    											arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
    											arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
    											arCreditedInvoice.getInvConversionDate(),
    											arCreditedInvoice.getInvConversionRate(),
    											arInvoice.getInvAmountDue(), AD_CMPNY) * -1, adCompany.getGlFunctionalCurrency().getFcPrecision());

    								}

    								mdetails.setSrAmount(AMNT_DUE);
	    							mdetails.setPosted(arInvoice.getInvPosted());

    							}
    							
    							
    							
    							
    							
    							
    							
    							
    							
    							

    							mdetails.setOrderBy(ORDER_BY);
    							mdetails.setPosted(arInvoice.getInvPosted());
    							
    							double TOTAL_NET_AMNT = 0d;
    							double TOTAL_TAX_AMNT = 0d;
    							double TOTAL_DISC_AMNT = 0d;

    							//compute net amount, tax amount and discount amount
    							if(!arInvoice.getArInvoiceLines().isEmpty()) {

    								Iterator j = arInvoice.getArInvoiceLines().iterator();

    								while(j.hasNext()) {

    									LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)j.next();

    									TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
    									TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();
    									
    									mdetails.setSrMemoName(arInvoiceLine.getArStandardMemoLine().getSmlName());
    									
    									System.out.println(arInvoiceLine.getArStandardMemoLine().getSmlName() + "   ***********  " + refNum);
        								if(arInvoiceLine.getArStandardMemoLine().getSmlName().trim().equals("CPF 7%") && refNum==arInvoiceLine.getArInvoice().getInvReferenceNumber()){
        									mdetails.setSrCpf(arInvoiceLine.getIlAmount());
        									System.out.println("ALMOST 1: "+arInvoiceLine.getIlAmount());
        								}
    	    							if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {

    	    								if(TOTAL_NET_AMNT != 0) {

    	    									TOTAL_NET_AMNT = TOTAL_NET_AMNT * -1;

    	    								}

    	    								if(TOTAL_TAX_AMNT != 0) {

    	    									TOTAL_TAX_AMNT = TOTAL_TAX_AMNT * -1;

    	    								}

    	    							}

    	    							mdetails.setSrNetAmount(TOTAL_NET_AMNT);
    	    							mdetails.setSrTaxAmount(TOTAL_TAX_AMNT);
    	    							mdetails.setPosted(arInvoice.getInvPosted());
    	    							
    	    							if(first) {

    	    								mdetails.setSrDiscountAmount(TOTAL_DISC_AMNT);
    	    								first = false;

    	    							}

    	    							if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    	    								if(arInvoice.getArSalesperson() != null) {

    	    									mdetails.setSrSlsSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
    	    									mdetails.setSrSlsName(arInvoice.getArSalesperson().getSlpName());
    	    	    							mdetails.setPosted(arInvoice.getInvPosted());
    	    								} else {
    	    									mdetails.setSrSlsSalespersonCode("");
    	    									mdetails.setSrSlsName("");
    	    									mdetails.setPosted(arInvoice.getInvPosted());
    	    								}

    	    							} else {

    	    								try {

    	    									LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    	    									if(arCreditedInvoice.getArSalesperson() != null) {

    	    										mdetails.setSrSlsSalespersonCode(arCreditedInvoice.getArSalesperson().getSlpSalespersonCode());
    	    										mdetails.setSrSlsName(arCreditedInvoice.getArSalesperson().getSlpName());
    	    		    							mdetails.setPosted(arInvoice.getInvPosted());
    	    									} else {
    	    										mdetails.setSrSlsSalespersonCode("");
    	    										mdetails.setSrSlsName("");
    	    										mdetails.setPosted(arInvoice.getInvPosted());
    	    									}

    	    								} catch(FinderException ex) {

    	    								}

    	    							}

    	    							//set distribution record details
    	    							mdetails.setSrDrGlAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
    	    							mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
    	    							mdetails.setPosted(arInvoice.getInvPosted());
    	    							if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    	    								mdetails.setSrDrDebitAmount(arDistributionRecord.getDrAmount());
    		    							mdetails.setPosted(arInvoice.getInvPosted());
    	    							} else {
    	    								mdetails.setSrDrCreditAmount(arDistributionRecord.getDrAmount());
    		    							mdetails.setPosted(arInvoice.getInvPosted());
    	    							}

    	    							
    	    							mdetails.setSrRevenueAmount(REVENUE_AMOUNT);
    	    							
    	    							list.add(mdetails);
    									
    								}

    							}


    						}

    					} else {
    													
							String customerType ="";
							byte pstd =0;
    						// type
    						if(arInvoice.getArCustomer().getArCustomerType() == null) {
    							customerType="UNDEFINE";
    							pstd = arInvoice.getInvPosted();
    						} else {
    							customerType=arInvoice.getArCustomer().getArCustomerType().getCtName();
    							pstd = arInvoice.getInvPosted();
    						}

    						double TOTAL_NET_AMNT = 0d;
    						double TOTAL_TAX_AMNT = 0d;
    						double TOTAL_DISC_AMNT = 0d;
    						double TOTAL_QTY= 0d;
    						double AMNT= 0d;
    						double TAX_AMNT= 0d;
    						double TOTAL_AMNT= 0d;

    						double AMNT_DUE = 0d;

    						if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    							AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    									arInvoice.getGlFunctionalCurrency().getFcCode(),
    									arInvoice.getGlFunctionalCurrency().getFcName(),
    									arInvoice.getInvConversionDate(),
    									arInvoice.getInvConversionRate(),
    									arInvoice.getInvAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    						} else {
    							
    							LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    							AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    									arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
    									arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
    									arCreditedInvoice.getInvConversionDate(),
    									arCreditedInvoice.getInvConversionRate(),
    									arInvoice.getInvAmountDue(), AD_CMPNY) * -1, adCompany.getGlFunctionalCurrency().getFcPrecision());

    						}
    						
    						//compute net amount, tax amount and discount amount
    						if(!arInvoice.getArInvoiceLines().isEmpty()) {

    							Iterator j = arInvoice.getArInvoiceLines().iterator();
    							String customerCode = "";
    							double AMOUNT = 0;

    							while(j.hasNext()) {
    	    						ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    								mdetails.setSrDate(arInvoice.getInvDate());
    	    						mdetails.setSrInvoiceNumber(arInvoice.getInvNumber());
    	    						mdetails.setSrReferenceNumber(arInvoice.getInvReferenceNumber());
    	    						mdetails.setSrDescription(arInvoice.getInvDescription());		
    	    						customerCode = arInvoice.getArCustomer().getCstCustomerCode();
    	    						mdetails.setSrCstCustomerCode(customerCode + "-" + arInvoice.getArCustomer().getCstName());
    	    						mdetails.setSrCstCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());
    	    						mdetails.setSrCstName(arInvoice.getArCustomer().getCstName());
    	    						mdetails.setSrCstCustomerCode2(arInvoice.getArCustomer().getCstCustomerCode());
    								mdetails.setPosted(arInvoice.getInvPosted());
						
    								/*Collection arDistributionRecords = arInvoice.getArDistributionRecords();
    								
    								Iterator x = arDistributionRecords.iterator();
    								
    								while (x.hasNext()) {
    									
    									LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
    									if (AD_CMPNY==15) {
    										if (arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
        										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
        									}
    									} else {
    										if ((arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") && (arDistributionRecord.getGlChartOfAccount().getCoaCode()!=5971 && arDistributionRecord.getGlChartOfAccount().getCoaCode()!=6259)) || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
        										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
        									}
    									}
    									
    										
    								}*/
    								mdetails.setSrCstCustomerType(customerType);
        							mdetails.setPosted(pstd);
        							mdetails.setSrAmount(AMNT_DUE);
        							System.out.print("eto ang amount, try lang: " + AMOUNT);
            						mdetails.setOrderBy(ORDER_BY);
        							mdetails.setPosted(arInvoice.getInvPosted());

    								LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)j.next();
    								TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
    								TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();
    								
    								System.out.println("arInvoiceLine.getIlAmount(): "+arInvoiceLine.getIlAmount());
    								
    								AMNT = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
        									arInvoice.getGlFunctionalCurrency().getFcCode(),
        									arInvoice.getGlFunctionalCurrency().getFcName(),
        									arInvoice.getInvConversionDate(),
        									arInvoice.getInvConversionRate(),
        									arInvoiceLine.getIlAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    								System.out.println("AMNT: "+AMNT);
    								
    								TAX_AMNT = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
        									arInvoice.getGlFunctionalCurrency().getFcCode(),
        									arInvoice.getGlFunctionalCurrency().getFcName(),
        									arInvoice.getInvConversionDate(),
        									arInvoice.getInvConversionRate(),
        									arInvoiceLine.getIlTaxAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    								System.out.println("TAX_AMNT: "+TAX_AMNT);
    								
    								TOTAL_AMNT = AMNT+TAX_AMNT;
    								System.out.println("TOTAL_AMNT: "+TOTAL_AMNT);
    								
    								System.out.println("TOTAL_NET_AMNT: "+TOTAL_NET_AMNT);
    								System.out.println("TOTAL_TAX_AMNT: "+TOTAL_TAX_AMNT);
    								System.out.println("arInvoiceLine.getIlTaxAmount(): "+arInvoiceLine.getIlTaxAmount());
    								mdetails.setSrLineSAmount(AMNT);
    								mdetails.setSrLineTaxAmount(TAX_AMNT);
    								mdetails.setSrLineAmount(TOTAL_AMNT);
    								
    								mdetails.setSrMemoName(arInvoiceLine.getArStandardMemoLine().getSmlName());
    								System.out.println(arInvoiceLine.getArStandardMemoLine().getSmlName() + "   ***********  " + refNum);
    								if(arInvoiceLine.getArStandardMemoLine().getSmlName().trim().equals("CPF 7%") && refNum==arInvoiceLine.getArInvoice().getInvReferenceNumber()){
    									mdetails.setSrCpf(arInvoiceLine.getIlAmount());
    									System.out.println("ALMOST 2: "+arInvoiceLine.getIlAmount());
    								}
    								System.out.println("arInvoiceLine.getArStandardMemoLine().getSmlName(): " + arInvoiceLine.getArStandardMemoLine().getSmlName());
    								System.out.println("arInvoiceLine.getIlQuantity()1: " + arInvoiceLine.getIlQuantity() + " " + arInvoiceLine.getIlDescription());
    								TOTAL_QTY= arInvoiceLine.getIlQuantity();
    								
    								mdetails.setSrQuantity(TOTAL_QTY);

    	    						if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {
    	    							mdetails.setSrInvCreditMemo(true);
    	    							mdetails.setPosted(arInvoice.getInvPosted());
    	    							mdetails.setCmInvoiceNumber(arInvoice.getInvCmInvoiceNumber());
    	    						} else {
    	    							mdetails.setSrInvCreditMemo(false);
    	    							mdetails.setPosted(arInvoice.getInvPosted());
    	    						}

    	    						mdetails.setSrNetAmount(TOTAL_NET_AMNT);
    	    						mdetails.setSrTaxAmount(TOTAL_TAX_AMNT);
    	    						mdetails.setSrDiscountAmount(TOTAL_DISC_AMNT);
    	    						
    								mdetails.setSrServiceAmount(0d);

    	    						mdetails.setPosted(arInvoice.getInvPosted());

    	    						if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    	    							if(arInvoice.getArSalesperson() != null) {

    	    								mdetails.setSrSlsSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
    	    								mdetails.setSrSlsName(arInvoice.getArSalesperson().getSlpName());
    	    								mdetails.setPosted(arInvoice.getInvPosted());
    	    							} else {
    	    								mdetails.setSrSlsSalespersonCode("");
    	    								mdetails.setSrSlsName("");
    	    								mdetails.setPosted(arInvoice.getInvPosted());
    	    							}

    	    						} else {

    	    							try {

    	    								LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    	    								if(arCreditedInvoice.getArSalesperson() != null) {

    	    									mdetails.setSrSlsSalespersonCode(arCreditedInvoice.getArSalesperson().getSlpSalespersonCode());
    	    									mdetails.setSrSlsName(arCreditedInvoice.getArSalesperson().getSlpName());
    	    									mdetails.setPosted(arInvoice.getInvPosted());
    	    								} else {
    	    									mdetails.setSrSlsSalespersonCode("");
    	    									mdetails.setSrSlsName("");
    	    									mdetails.setPosted(arInvoice.getInvPosted());
    	    								}

    	    							} catch(FinderException ex) {

    	    							}

    	    						}

    	    						if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {

    	    							GRAND_TOTAL_NET_AMNT -= TOTAL_NET_AMNT;
    	    							GRAND_TOTAL_TAX_AMNT -= TOTAL_TAX_AMNT;
    	    							GRAND_TOTAL_AMNT -= (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);

    	    						} else {

    	    							GRAND_TOTAL_NET_AMNT += TOTAL_NET_AMNT;
    	    							GRAND_TOTAL_TAX_AMNT += TOTAL_TAX_AMNT;
    	    							GRAND_TOTAL_AMNT += (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);

    	    						}

    	    						mdetails.setSrRctType("INVOICE RELEASES");
    	    						mdetails.setPosted(arInvoice.getInvPosted());
    	    						// get receipt info
    	    						
    	    						String invoiceReceiptNumbers = null;
    	    						String invoiceReceiptDates = null;
    	    						
    	    						Collection arInvoiceReceipts = arReceiptHome.findByInvCodeAndRctVoid(arInvoice.getInvCode(), EJBCommon.FALSE, AD_CMPNY);
    	    						
    	    						Iterator r = arInvoiceReceipts.iterator();
    	    						
    	    						while (r.hasNext()) {
    	    							
    	    							LocalArReceipt arReceipt = (LocalArReceipt)r.next();
    	    							
    	    							if (invoiceReceiptNumbers == null) {
    	    								invoiceReceiptNumbers = arReceipt.getRctNumber();
    	    							} else if (invoiceReceiptNumbers != null && !invoiceReceiptNumbers.contains(arReceipt.getRctNumber())){
    	    								invoiceReceiptNumbers += "/" + arReceipt.getRctNumber();
    	    							}
    	    							
    	    							if (invoiceReceiptDates == null) {
    	    								invoiceReceiptDates = EJBCommon.convertSQLDateToString(arReceipt.getRctDate());
    	    							} else if (invoiceReceiptDates != null && !invoiceReceiptDates.contains(EJBCommon.convertSQLDateToString(arReceipt.getRctDate()))){
    	    								invoiceReceiptDates += "/" + EJBCommon.convertSQLDateToString(arReceipt.getRctDate());
    	    							}
    	    							
    	    						}
    	    						
    	    						mdetails.setSrInvReceiptNumbers(invoiceReceiptNumbers);
    	    						mdetails.setSrInvReceiptDates(invoiceReceiptDates);
    	    						mdetails.setPosted(arInvoice.getInvPosted());
    	    						mdetails.setSrRevenueAmount(REVENUE_AMOUNT);
    	    						list.add(mdetails);
    							}
							
    						
								
    						}

    					}

    				}

    			}

    		}


    		// Prior Dues

    		if(((String)criteria.get("includedPriorDues")).equals("YES")) {

    			jbossQl = new StringBuffer();
    			jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv WHERE (");

    			if (branchList.isEmpty())
    				throw new GlobalNoRecordFoundException();

    			brIter = branchList.iterator();

    			details = (AdBranchDetails)brIter.next();
    			jbossQl.append("inv.invAdBranch=" + details.getBrCode());

    			while(brIter.hasNext()) {

    				details = (AdBranchDetails)brIter.next();

    				jbossQl.append(" OR inv.invAdBranch=" + details.getBrCode());

    			}

    			jbossQl.append(") ");
    			firstArgument = false;

    			ctr = 0;
    			// Allocate the size of the object parameter

    			obj = new Object[criteriaSize]; 

    			if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("inv.arCustomer.cstCustomerCode = '" + (String)criteria.get("customerCode") + "' ");

    			}
    			
    			if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("inv.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}
    			
/*    			if (criteria.containsKey("invoiceBatchName")) {
    				
    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.arInvoiceBatch.ibName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("invoiceBatchName");
    				ctr++;
    				
    			}
*/
    			if (criteria.containsKey("customerType")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerType");
    				ctr++;

    			}	

    			if (criteria.containsKey("customerClass")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerClass");
    				ctr++;

    			}			     

    			if (criteria.containsKey("dateFrom")) {

    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.invDate<?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateFrom");
    				ctr++;

    			}  	

    			if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arSalesperson.slpName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("salesperson");
    				ctr++;

    			}
   			
    			if (criteria.containsKey("dateTo")) {

    				obj[ctr] = (Date)criteria.get("dateTo");
    				ctr++;

    			}    

    			if (criteria.containsKey("invoiceNumberFrom")) {

    				obj[ctr] = (String)criteria.get("invoiceNumberFrom");
    				ctr++;
    			}  

    			if (criteria.containsKey("invoiceNumberTo")) {

    				obj[ctr] = (String)criteria.get("invoiceNumberTo");
    				ctr++;

    			}
    			
    			boolean unpostedOnly=false;
    			if (criteria.containsKey("unpostedOnly")) {

    				String unposted = (String)criteria.get("unpostedOnly");

    				if (unposted.equals("YES")) {
    					if (!firstArgument) {

    						jbossQl.append("AND ");

    					} else {

    						firstArgument = false;
    						jbossQl.append("WHERE ");

    					}	       	  

    					unpostedOnly=true;
    					jbossQl.append("inv.invPosted = 0 AND inv.invVoid = 0 ");

    				} 
    			}

    			if (criteria.containsKey("includedUnPosted") && unpostedOnly==false) {

    				String unPosted = (String)criteria.get("includedUnPosted");

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}	       	  
    				if (unPosted == null) unPosted = "NO";
    				if (unPosted.equals("NO")) {

    					jbossQl.append("inv.invPosted = 1");
    				
    				}  else {

    					jbossQl.append("inv.invVoid = 0 ");  	  

    				}   	 

    			}

    			if (!firstArgument) {

    				jbossQl.append("AND ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}

    			boolean firstSource = true;

    			if(((String)criteria.get("includedCreditMemos")).equals("YES")) {

    				jbossQl.append("(inv.invCreditMemo = 1 ");
    				firstSource = false;

    			}

    			if(((String)criteria.get("includedDebitMemos")).equals("YES")) {

    				if (!firstSource) {

    					jbossQl.append("OR ");

    				} else {

    					jbossQl.append("(");

    				}

    				jbossQl.append("inv.invDebitMemo = 1 ");
    				firstSource = false;

    			}

    			if(((String)criteria.get("includedInvoices")).equals("YES")) {

    				if (!firstSource) {

    					jbossQl.append("OR ");

    				} else {

    					jbossQl.append("(");

    				}

    				jbossQl.append("(inv.invCreditMemo = 0 AND inv.invDebitMemo = 0)");
    				firstSource = false;

    			}
    			
    			jbossQl.append(") ");



    			if (!firstArgument) {

    				jbossQl.append("AND ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}

    			jbossQl.append("inv.invAdCompany=" + AD_CMPNY + " ");
    			
  	       	  String orderBy = null;
	          
	          if (GROUP_BY.equals("SALESPERSON") || GROUP_BY.equals("PRODUCT")) {
	  	      	 
	  	      	  orderBy = "inv.arCustomer.cstCustomerCode";

	          } 

	  		  if (orderBy != null) {
	  		  
	  		  	jbossQl.append("ORDER BY " + orderBy);
	  		  	
	  		  }  
    			
    			String refNum="";
    			
    			System.out.println("jbossQl.toString()2: "+jbossQl.toString());
    			System.out.println("unposted Only2: " + criteria.containsKey("unpostedOnly"));
    			arInvoices = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);
    			
    			if(!arInvoices.isEmpty()) {

    				Iterator i = arInvoices.iterator();

    				while (i.hasNext()) {

    					LocalArInvoice arInvoice = (LocalArInvoice)i.next(); 
    					
    					
    					double REVENUE_AMOUNT = 0d;
    					
    					
    					Collection arDistributionRecords = arInvoice.getArDistributionRecords();
						
						Iterator x = arDistributionRecords.iterator();
						
						while (x.hasNext()) {
							
							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
							
							if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
								
								if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
									REVENUE_AMOUNT -= arDistributionRecord.getDrAmount();
								}else {
									REVENUE_AMOUNT += arDistributionRecord.getDrAmount();
								}
							
							}
						}
    					
    					
    					
						if (PRINT_SALES_RELATED_ONLY) {
							
							boolean isSalesRelated = false;
							
							arDistributionRecords = arInvoice.getArDistributionRecords();
							
							Iterator iDr = arDistributionRecords.iterator();
							
							while (iDr.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) iDr.next();
								
								if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
									
									isSalesRelated = true;
									break;
								
							}
									
							}
							
							if (!isSalesRelated)
									
								continue;
						}   					
    					
    					if(SHOW_ENTRIES) {

    						boolean first = true;

    						arDistributionRecords = arInvoice.getArDistributionRecords();

    						Iterator k = arDistributionRecords.iterator();

    						while(k.hasNext()) {

    							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)k.next();

    							ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    							mdetails.setSrDate(arInvoice.getInvDate());
    							mdetails.setSrInvoiceNumber(arInvoice.getInvNumber());
    							mdetails.setSrReferenceNumber(arInvoice.getInvReferenceNumber());
    							refNum=arInvoice.getInvReferenceNumber();
    							mdetails.setSrDescription(arInvoice.getInvDescription());		  	  
    							mdetails.setSrCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode() + "-" + arInvoice.getArCustomer().getCstName());		
    							mdetails.setSrCstCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());
    							mdetails.setSrCstName(arInvoice.getArCustomer().getCstName());
    							mdetails.setSrCstCustomerCode2(arInvoice.getArCustomer().getCstCustomerCode());
    							mdetails.setPosted(arInvoice.getInvPosted());
    							
    							// type
    							if(arInvoice.getArCustomer().getArCustomerType() == null) {

    								mdetails.setSrCstCustomerType("UNDEFINE");

    							} else {

    								mdetails.setSrCstCustomerType(arInvoice.getArCustomer().getArCustomerType().getCtName());

    							}

    							if(first) {

    								double AMNT_DUE = 0d;

    								if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    									AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    											arInvoice.getGlFunctionalCurrency().getFcCode(),
    											arInvoice.getGlFunctionalCurrency().getFcName(),
    											arInvoice.getInvConversionDate(),
    											arInvoice.getInvConversionRate(),
    											arInvoice.getInvAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    								} else {

    									LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    									AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    											arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
    											arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
    											arCreditedInvoice.getInvConversionDate(),
    											arCreditedInvoice.getInvConversionRate(),
    											arInvoice.getInvAmountDue(), AD_CMPNY) * -1, adCompany.getGlFunctionalCurrency().getFcPrecision());

    								}

    								mdetails.setSrAmount(AMNT_DUE);
	    							mdetails.setPosted(arInvoice.getInvPosted());
    								// first = false;

    							}

    							mdetails.setOrderBy(ORDER_BY);
    							mdetails.setPosted(arInvoice.getInvPosted());
    							double TOTAL_NET_AMNT = 0d;
    							double TOTAL_TAX_AMNT = 0d;
    							double TOTAL_DISC_AMNT = 0d;
    							
    							//compute net amount, tax amount and discount amount
    							if(!arInvoice.getArInvoiceLines().isEmpty()) {

    								Iterator j = arInvoice.getArInvoiceLines().iterator();

    								while(j.hasNext()) {

    									LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)j.next();
    									arInvoiceLine.getArStandardMemoLine().getSmlName();
    									TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
    									TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();

    									mdetails.setSrMemoName(arInvoiceLine.getArStandardMemoLine().getSmlName());
    									System.out.println(arInvoiceLine.getArStandardMemoLine().getSmlName() + "   ***********  " + refNum);
        								if(arInvoiceLine.getArStandardMemoLine().getSmlName().trim().equals("CPF 7%") && refNum==arInvoiceLine.getArInvoice().getInvReferenceNumber()){
        									mdetails.setSrCpf(arInvoiceLine.getIlAmount());
        									System.out.println("ALMOST 3: "+arInvoiceLine.getIlAmount());
        								}
    									System.out.println("arInvoiceLine.getArStandardMemoLine().getSmlName()2: " + arInvoiceLine.getArStandardMemoLine().getSmlName());
    									mdetails.setSrQuantity(arInvoiceLine.getIlQuantity());
    									System.out.println("arInvoiceLine.getIlQuantity()2: " + arInvoiceLine.getIlQuantity());
    	    							if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {

    	    								if(TOTAL_NET_AMNT != 0) {

    	    									TOTAL_NET_AMNT = TOTAL_NET_AMNT * -1;

    	    								}

    	    								if(TOTAL_TAX_AMNT != 0) {

    	    									TOTAL_TAX_AMNT = TOTAL_TAX_AMNT * -1;

    	    								}

    	    							}

    	    							mdetails.setSrNetAmount(TOTAL_NET_AMNT);
    	    							mdetails.setSrTaxAmount(TOTAL_TAX_AMNT);
    	    							mdetails.setPosted(arInvoice.getInvPosted());
    	    							
    	    							if(first) {

    	    								mdetails.setSrDiscountAmount(TOTAL_DISC_AMNT);
    	    								first = false;

    	    							}

    	    							if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    	    								if(arInvoice.getArSalesperson() != null) {

    	    									mdetails.setSrSlsSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
    	    									mdetails.setSrSlsName(arInvoice.getArSalesperson().getSlpName());
    	    	    							mdetails.setPosted(arInvoice.getInvPosted());

    	    								} else {
       	    									mdetails.setSrSlsSalespersonCode("");
    	    									mdetails.setSrSlsName("");
    	    									mdetails.setPosted(arInvoice.getInvPosted());
    	    								}

    	    							} else {

    	    								try {

    	    									LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    	    									if(arCreditedInvoice.getArSalesperson() != null) {

    	    										mdetails.setSrSlsSalespersonCode(arCreditedInvoice.getArSalesperson().getSlpSalespersonCode());
    	    										mdetails.setSrSlsName(arCreditedInvoice.getArSalesperson().getSlpName());
    	    		    							mdetails.setPosted(arInvoice.getInvPosted());

    	    									} else {
    	    										mdetails.setSrSlsSalespersonCode("");
    	    										mdetails.setSrSlsName("");
    	    										mdetails.setPosted(arInvoice.getInvPosted());
    	    									}

    	    								} catch(FinderException ex) {

    	    								}

    	    							}

    	    							//set distribution record details
    	    							mdetails.setSrDrGlAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
    	    							mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

    	    							if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    	    								mdetails.setSrDrDebitAmount(arDistributionRecord.getDrAmount());
    		    							mdetails.setPosted(arInvoice.getInvPosted());

    	    							} else {

    	    								mdetails.setSrDrCreditAmount(arDistributionRecord.getDrAmount());
    		    							mdetails.setPosted(arInvoice.getInvPosted());

    	    							}

    	    							mdetails.setSrRevenueAmount(REVENUE_AMOUNT);
    	    							list.add(mdetails);
    								}

    							}

    						}

    					} else {

    						ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    						mdetails.setSrDate(arInvoice.getInvDate());
    						mdetails.setSrInvoiceNumber(arInvoice.getInvNumber());
    						mdetails.setSrReferenceNumber(arInvoice.getInvReferenceNumber());
    						refNum=arInvoice.getInvReferenceNumber();
    						mdetails.setSrDescription(arInvoice.getInvDescription());		  	  
    						mdetails.setSrCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode() + "-" + arInvoice.getArCustomer().getCstName());		
    						mdetails.setSrCstCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());
    						mdetails.setSrCstName(arInvoice.getArCustomer().getCstName());
    						mdetails.setSrCstCustomerCode2(arInvoice.getArCustomer().getCstCustomerCode());
							mdetails.setPosted(arInvoice.getInvPosted());
    						
							arDistributionRecords = arInvoice.getArDistributionRecords();
							
							x = arDistributionRecords.iterator();
							
							while (x.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
								
								if (AD_CMPNY==15) {
									if (arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
									}
								} else {
									if ((arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") && (arDistributionRecord.getGlChartOfAccount().getCoaCode()!=5971 && arDistributionRecord.getGlChartOfAccount().getCoaCode()!=6259)) || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
									}
								}
									
							}
							
    						// type
    						if(arInvoice.getArCustomer().getArCustomerType() == null) {

    							mdetails.setSrCstCustomerType("UNDEFINE");
    							mdetails.setPosted(arInvoice.getInvPosted());

    						} else {

    							mdetails.setSrCstCustomerType(arInvoice.getArCustomer().getArCustomerType().getCtName());
    							mdetails.setPosted(arInvoice.getInvPosted());

    						}

    						double AMNT_DUE = 0d;

    						if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    							AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    									arInvoice.getGlFunctionalCurrency().getFcCode(),
    									arInvoice.getGlFunctionalCurrency().getFcName(),
    									arInvoice.getInvConversionDate(),
    									arInvoice.getInvConversionRate(),
    									arInvoice.getInvAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    						} else {

    							LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    							AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    									arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
    									arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
    									arCreditedInvoice.getInvConversionDate(),
    									arCreditedInvoice.getInvConversionRate(),
    									arInvoice.getInvAmountDue(), AD_CMPNY) * -1, adCompany.getGlFunctionalCurrency().getFcPrecision());

    						}

    						mdetails.setSrAmount(AMNT_DUE);
    						mdetails.setOrderBy(ORDER_BY);
							mdetails.setPosted(arInvoice.getInvPosted());
							
    						double TOTAL_NET_AMNT = 0d;
    						double TOTAL_TAX_AMNT = 0d;
    						double TOTAL_DISC_AMNT = 0d;

    						//compute net amount, tax amount and discount amount
    						if(!arInvoice.getArInvoiceLines().isEmpty()) {

    							Iterator j = arInvoice.getArInvoiceLines().iterator();

    							while(j.hasNext()) {

    								LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)j.next();

    								TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
    								TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();

    								mdetails.setSrMemoName(arInvoiceLine.getArStandardMemoLine().getSmlName());
    								System.out.println(arInvoiceLine.getArStandardMemoLine().getSmlName() + "   ***********  " + refNum);
    								if(arInvoiceLine.getArStandardMemoLine().getSmlName().trim().equals("CPF 7%") && refNum==arInvoiceLine.getArInvoice().getInvReferenceNumber()){
    									mdetails.setSrCpf(arInvoiceLine.getIlAmount());
    									System.out.println("ALMOST 4: "+arInvoiceLine.getIlAmount());
    								}
    								System.out.println("arInvoiceLine.getArStandardMemoLine().getSmlName()3: " + arInvoiceLine.getArStandardMemoLine().getSmlName());
    								mdetails.setSrQuantity(arInvoiceLine.getIlQuantity());
    								System.out.println("arInvoiceLine.getIlQuantity()3: " + arInvoiceLine.getIlQuantity());
    	    						if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {

    	    							mdetails.setSrInvCreditMemo(true);
    	    							mdetails.setPosted(arInvoice.getInvPosted());

    	    						} else {

    	    							mdetails.setSrInvCreditMemo(false);
    	    							mdetails.setPosted(arInvoice.getInvPosted());

    	    						}

    	    						mdetails.setSrNetAmount(TOTAL_NET_AMNT);
    	    						mdetails.setSrTaxAmount(TOTAL_TAX_AMNT);
    	    						mdetails.setSrDiscountAmount(TOTAL_DISC_AMNT);
    								mdetails.setPosted(arInvoice.getInvPosted());
    								
    	    						if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    	    							if(arInvoice.getArSalesperson() != null) {

    	    								mdetails.setSrSlsSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
    	    								mdetails.setSrSlsName(arInvoice.getArSalesperson().getSlpName());
    		    							mdetails.setPosted(arInvoice.getInvPosted());

    	    							} else {
     	    								mdetails.setSrSlsSalespersonCode("");
    	    								mdetails.setSrSlsName("");
    	    								mdetails.setPosted(arInvoice.getInvPosted());
    	    							}

    	    						} else {

    	    							try {

    	    								LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    	    								if(arCreditedInvoice.getArSalesperson() != null) {

    	    									mdetails.setSrSlsSalespersonCode(arCreditedInvoice.getArSalesperson().getSlpSalespersonCode());
    	    									mdetails.setSrSlsName(arCreditedInvoice.getArSalesperson().getSlpName());
    	    	    							mdetails.setPosted(arCreditedInvoice.getInvPosted());

    	    								} else {
    	    									mdetails.setSrSlsSalespersonCode("");
    	    									mdetails.setSrSlsName("");
    	    									mdetails.setPosted(arCreditedInvoice.getInvPosted());
    	    								}

    	    							} catch(FinderException ex) {

    	    							}

    	    						}

    	    						if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {
    	    							mdetails.setCmInvoiceNumber(arInvoice.getInvCmInvoiceNumber());	
    	    							GRAND_TOTAL_NET_AMNT -= TOTAL_NET_AMNT;
    	    							GRAND_TOTAL_TAX_AMNT -= TOTAL_TAX_AMNT;
    	    							GRAND_TOTAL_AMNT -= (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);

    	    						} else {

    	    							GRAND_TOTAL_NET_AMNT += TOTAL_NET_AMNT;
    	    							GRAND_TOTAL_TAX_AMNT += TOTAL_TAX_AMNT;
    	    							GRAND_TOTAL_AMNT += (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);

    	    						}

    	    						mdetails.setSrRctType("INVOICE");
    	    						
    	    						// get receipt info
    	    						
    	    						String invoiceReceiptNumbers = null;
    	    						String invoiceReceiptDates = null;
    	    						
    	    						Collection arInvoiceReceipts = arReceiptHome.findByInvCodeAndRctVoid(arInvoice.getInvCode(), EJBCommon.FALSE, AD_CMPNY);
    	    						
    	    						Iterator r = arInvoiceReceipts.iterator();
    	    						
    	    						while (r.hasNext()) {
    	    							
    	    							LocalArReceipt arReceipt = (LocalArReceipt)r.next();
    	    							
    	    							if (invoiceReceiptNumbers == null) {
    	    								invoiceReceiptNumbers = arReceipt.getRctNumber();
    	    							} else if (invoiceReceiptNumbers != null && !invoiceReceiptNumbers.contains(arReceipt.getRctNumber())){
    	    								invoiceReceiptNumbers += "/" + arReceipt.getRctNumber();
    	    							}
    	    							
    	    							if (invoiceReceiptDates == null) {
    	    								invoiceReceiptDates = EJBCommon.convertSQLDateToString(arReceipt.getRctDate());
    	    							} else if (invoiceReceiptDates != null && !invoiceReceiptDates.contains(EJBCommon.convertSQLDateToString(arReceipt.getRctDate()))){
    	    								invoiceReceiptDates += "/" + EJBCommon.convertSQLDateToString(arReceipt.getRctDate());
    	    							}
    	    							
    	    						}
    	    						
    	    						mdetails.setSrInvReceiptNumbers(invoiceReceiptNumbers);
    	    						mdetails.setSrInvReceiptDates(invoiceReceiptDates);
    	    						mdetails.setSrRevenueAmount(REVENUE_AMOUNT);
    	    						list = getPriorDues(arInvoice, mdetails, criteria, list);

    							}

    						} 

    					}
    				}
    			}
    		}

    		Collection arReceipts = null;
    		String miscReceipts = null;

    		if(((String)criteria.get("includedMiscReceipts")).equals("YES")) {
    			System.out.println("includedMiscReceipts");
    			// misc receipt

    			jbossQl = new StringBuffer();
    			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct WHERE (");

    			brIter = branchList.iterator();

    			details = (AdBranchDetails)brIter.next();
    			jbossQl.append("rct.rctAdBranch=" + details.getBrCode());

    			while(brIter.hasNext()) {

    				details = (AdBranchDetails)brIter.next();

    				jbossQl.append(" OR rct.rctAdBranch=" + details.getBrCode());

    			}

    			jbossQl.append(") ");
    			firstArgument = false;

    			ctr = 0;

    			if (criteria.containsKey("invoiceNumberFrom")) {

    				criteriaSize--;

    			}

    			if (criteria.containsKey("invoiceNumberTo")) {

    				criteriaSize--;

    			}
    			
    			if(criteria.containsKey("invoiceBatchName"))
    				obj = new Object[criteriaSize-1];
    			else
    				obj = new Object[criteriaSize];

    			if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.cstCustomerCode = '" + (String)criteria.get("customerCode") + "' ");

    			}
    			
    			if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}

/*    			if (criteria.containsKey("receiptBatchName")) {
    				
    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("rct.arReceiptBatch.rbName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("receiptBatchName");
    				ctr++;
    				
    			}
*/
    			if (criteria.containsKey("customerType")) {

    				if (!firstArgument) {		

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerType");
    				ctr++;

    			}	

    			if (criteria.containsKey("customerClass")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerClass");
    				ctr++;

    			}		
    			
    			if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("rct.arSalesperson.slpName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("salesperson");
    				ctr++;

    			}

    			if (criteria.containsKey("dateFrom")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}
    				jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateFrom");
    				ctr++;
    			}  

    			if (criteria.containsKey("dateTo")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}
    				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateTo");
    				ctr++;

    			}

    			boolean unpostedOnly=false;
    			if (criteria.containsKey("unpostedOnly")) {

    				String unposted = (String)criteria.get("unpostedOnly");

    				if (unposted.equals("YES")) {
    					if (!firstArgument) {

    						jbossQl.append("AND ");

    					} else {

    						firstArgument = false;
    						jbossQl.append("WHERE ");

    					}	       	  

    					unpostedOnly = true;
    					jbossQl.append("rct.rctPosted = 0 AND rct.rctVoid = 0 ");

    				} 
    			}
    			
    			if (criteria.containsKey("includedUnposted") && unpostedOnly==false) {

    				String unposted = (String)criteria.get("includedUnposted");

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}	       	  
    				if (unposted == null) unposted = "NO";
    				if (unposted.equals("NO")) {

    					jbossQl.append("rct.rctPosted = 1 ");

    				} else {

    					jbossQl.append("rct.rctVoid = 0 ");  	  

    				}   	 

    			}

    			if (criteria.containsKey("region")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("rct.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("region");
    				ctr++;

    			}

    			if (!firstArgument) {

    				jbossQl.append("AND ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}

    			jbossQl.append("rct.rctType='MISC' AND rct.rctAdCompany=" + AD_CMPNY + " ");
    			
    	       	  String orderBy = null;
    	          
    	          if (GROUP_BY.equals("SALESPERSON") || GROUP_BY.equals("PRODUCT")) {
    	  	      	 
    	  	      	  orderBy = "rct.arCustomer.cstCustomerCode";

    	          } 

    	  		  if (orderBy != null) {
    	  		  
    	  		  	jbossQl.append("ORDER BY " + orderBy);
    	  		  	
    	  		  }  
    			

    			arReceipts = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);
    			
    			if(!arReceipts.isEmpty()) {

    				Iterator i = arReceipts.iterator();

    				while(i.hasNext()) {
    					
    					
    					LocalArReceipt arReceipt = (LocalArReceipt)i.next();
    					
    					Collection arDistributionRecords = arReceipt.getArDistributionRecords();
    					
    					double REVENUE_AMOUNT = 0d;
    					Iterator iDr = arDistributionRecords.iterator();
    					
    					while(iDr.hasNext()) {
    						
    						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)iDr.next();
    						
    						
    						if(arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")) {
    							
    							if(arDistributionRecord.getDrDebit()==EJBCommon.TRUE) {
    								REVENUE_AMOUNT -= arDistributionRecord.getDrAmount();
    							}else {
    								REVENUE_AMOUNT += arDistributionRecord.getDrAmount();
    							}
    						}
    						
    						
    					}
    					
    					
    					
    					Iterator k = arReceipt.getArInvoiceLines().iterator();

						while(k.hasNext()) {

							LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)k.next();
							
							ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
							mdetails.setSrDate(arReceipt.getRctDate());
							mdetails.setSrInvoiceNumber(arReceipt.getRctNumber());
							mdetails.setSrMemoName(arInvoiceLine.getArStandardMemoLine().getSmlName());
							mdetails.setSrAmount(arInvoiceLine.getIlAmount());
							mdetails.setSrReferenceNumber(arReceipt.getRctReferenceNumber());
							mdetails.setSrDescription(arReceipt.getRctDescription());		  	  
							mdetails.setSrCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode() + "-" + 
									(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName()));		
							mdetails.setSrCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
							mdetails.setSrCstName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
							mdetails.setSrCstCustomerCode2(arReceipt.getArCustomer().getCstCustomerCode());
							mdetails.setSrRctType("MISC");
							mdetails.setSrRevenueAmount(REVENUE_AMOUNT);
							list.add(mdetails);
						}
    					
    					
    					
    					
    					
    					
    					
    					
    					
    					
    					
    					
    					
    					
    					
    					
    					
    					
    					/*
    					LocalArReceipt arReceipt = (LocalArReceipt)i.next();   	  

						if (PRINT_SALES_RELATED_ONLY) {
							
							boolean isSalesRelated = false;
							
							Collection arDistributionRecords = arReceipt.getArDistributionRecords();
							
							Iterator x = arDistributionRecords.iterator();
							
							while (x.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
								
								if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
									
									isSalesRelated = true;
									break;
								
							}
									
							}
							
							if (!isSalesRelated)
									
								continue;
						}
						
    					if(SHOW_ENTRIES) {

    						boolean first = true;

    						Collection arDistributionRecords = arReceipt.getArDistributionRecords();

    						Iterator j = arDistributionRecords.iterator();

    						while(j.hasNext()) {

    							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

    							ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    							mdetails.setSrDate(arReceipt.getRctDate());
    							mdetails.setSrInvoiceNumber(arReceipt.getRctNumber());
    							mdetails.setSrReferenceNumber(arReceipt.getRctReferenceNumber());
    							mdetails.setSrDescription(arReceipt.getRctDescription());		  	  
    							mdetails.setSrCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode() + "-" + 
    									(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName()));		
    							mdetails.setSrCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
    							mdetails.setSrCstName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
    							mdetails.setSrCstCustomerCode2(arReceipt.getArCustomer().getCstCustomerCode());
    							mdetails.setPosted(arReceipt.getRctPosted());
        						// type
    							if(arReceipt.getArCustomer().getArCustomerType() == null) {

    								mdetails.setSrCstCustomerType("UNDEFINE");
        							mdetails.setPosted(arReceipt.getRctPosted());

    							} else {

    								mdetails.setSrCstCustomerType(arReceipt.getArCustomer().getArCustomerType().getCtName());
        							mdetails.setPosted(arReceipt.getRctPosted());
        							

    							}

    							if(first) {

    								double AMNT_DUE = 0d;

    								AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    										arReceipt.getGlFunctionalCurrency().getFcCode(),
    										arReceipt.getGlFunctionalCurrency().getFcName(),
    										arReceipt.getRctConversionDate(),
    										arReceipt.getRctConversionRate(),
    										arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    								mdetails.setSrAmount(AMNT_DUE);

    								first = false;

    							}

    							mdetails.setOrderBy(ORDER_BY);
    							mdetails.setPosted(arReceipt.getRctPosted());
    							//set distribution record details
    							mdetails.setSrDrGlAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
    							//mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

    							if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    								mdetails.setSrDrDebitAmount(arDistributionRecord.getDrAmount());
        							mdetails.setPosted(arReceipt.getRctPosted());
    							} else {

    								mdetails.setSrDrCreditAmount(arDistributionRecord.getDrAmount());
        							mdetails.setPosted(arReceipt.getRctPosted());
    							}

    							list.add(mdetails);

    						}

    					} else {
    						
    						System.out.println("ELSE--------------->");

    						ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    						mdetails.setSrDate(arReceipt.getRctDate());
    						mdetails.setSrInvoiceNumber(arReceipt.getRctNumber());
    						mdetails.setSrReferenceNumber(arReceipt.getRctReferenceNumber());
    						mdetails.setSrDescription(arReceipt.getRctDescription());		  	  
    						mdetails.setSrCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode() + "-" + 
    								(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName()));		
    						mdetails.setSrCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
    						mdetails.setSrCstName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
    						mdetails.setSrCstCustomerCode2(arReceipt.getArCustomer().getCstCustomerCode());
							mdetails.setPosted(arReceipt.getRctPosted());
							
							
    						// type
    						if(arReceipt.getArCustomer().getArCustomerType() == null) {

    							mdetails.setSrCstCustomerType("UNDEFINE");
    							mdetails.setPosted(arReceipt.getRctPosted());
    						} else {

    							mdetails.setSrCstCustomerType(arReceipt.getArCustomer().getArCustomerType().getCtName());
    							mdetails.setPosted(arReceipt.getRctPosted());
    						}

    						boolean isCustDeposit = (arReceipt.getRctCustomerDeposit() == 1 ? true : false);
    						
    						double AMNT_DUE = 0d;

    						AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    								arReceipt.getGlFunctionalCurrency().getFcCode(),
    								arReceipt.getGlFunctionalCurrency().getFcName(),
    								arReceipt.getRctConversionDate(),
    								arReceipt.getRctConversionRate(),
    								arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    						//mdetails.setSrAmount(AMNT_DUE * -1);
    						mdetails.setOrderBy(ORDER_BY);
							mdetails.setPosted(arReceipt.getRctPosted());
    						double TOTAL_NET_AMNT = 0d;
    						double TOTAL_TAX_AMNT = 0d;

    						if(!arReceipt.getArInvoiceLines().isEmpty()) {

    							Iterator k = arReceipt.getArInvoiceLines().iterator();

    							while(k.hasNext()) {

    								LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)k.next();

    								TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
    								TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();
    								System.out.println("MEMO NAME="+arInvoiceLine.getArStandardMemoLine().getSmlName());
    								mdetails.setSrMemoName(arInvoiceLine.getArStandardMemoLine().getSmlName());
    								mdetails.setSrQuantity(arInvoiceLine.getIlQuantity());
    								mdetails.setSrAmount(arInvoiceLine.getIlAmount());
    								mdetails.setSrNetAmount(isCustDeposit ? (TOTAL_NET_AMNT * -1): TOTAL_NET_AMNT);
    	    						mdetails.setSrTaxAmount(isCustDeposit ? (TOTAL_TAX_AMNT * -1): TOTAL_TAX_AMNT);
    								mdetails.setPosted(arReceipt.getRctPosted());
    								
    	    						if(arReceipt.getArSalesperson() != null) {

    	    							mdetails.setSrSlsSalespersonCode(arReceipt.getArSalesperson().getSlpSalespersonCode());
    	    							mdetails.setSrSlsName(arReceipt.getArSalesperson().getSlpName());
    	    							mdetails.setPosted(arReceipt.getRctPosted());
    	    						} else {
    	    							mdetails.setSrSlsSalespersonCode("");
    	    							mdetails.setSrSlsName("");
    	    							mdetails.setPosted(arReceipt.getRctPosted());
    	    						}

    	    						if(isCustDeposit){
    	    							GRAND_TOTAL_NET_AMNT -= TOTAL_NET_AMNT;
    	    							GRAND_TOTAL_TAX_AMNT -= TOTAL_TAX_AMNT;
    	    							GRAND_TOTAL_AMNT -= (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);
    	    						}else{
    	    							GRAND_TOTAL_NET_AMNT += TOTAL_NET_AMNT;
    	    							GRAND_TOTAL_TAX_AMNT += TOTAL_TAX_AMNT;
    	    							GRAND_TOTAL_AMNT += (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);
    	    						}

    	    						mdetails.setSrRctType("MISC");
    								mdetails.setPosted(arReceipt.getRctPosted());
    	    						list.add(mdetails);

    							}

    						}

    					}*/


    				}

    			}

    		}


    		if(((String)criteria.get("includedCollections")).equals("YES")) {

    			// collections

    			jbossQl = new StringBuffer();
    			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct WHERE (");

    			brIter = branchList.iterator();

    			details = (AdBranchDetails)brIter.next();
    			jbossQl.append("rct.rctAdBranch=" + details.getBrCode());

    			while(brIter.hasNext()) {

    				details = (AdBranchDetails)brIter.next();

    				jbossQl.append(" OR rct.rctAdBranch=" + details.getBrCode());

    			}

    			jbossQl.append(") ");
    			firstArgument = false;

    			ctr = 0;

    			if (criteria.containsKey("invoiceNumberFrom")) {

    				criteriaSize--;

    			}

    			if (criteria.containsKey("invoiceNumberTo")) {

    				criteriaSize--;

    			}
    			
    			if(criteria.containsKey("invoiceBatchName"))
    				obj = new Object[criteriaSize-1];
    			else
    				obj = new Object[criteriaSize];
    			
    			if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.cstCustomerCode  '=" + (String)criteria.get("customerCode") + "' ");

    			}
    			
    			if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}
    			
/*    			if (criteria.containsKey("receiptBatchName")) {
    				
    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("rct.arReceiptBatch.rbName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("receiptBatchName");
    				ctr++;
    				
    			}
*/

    			if (criteria.containsKey("customerType")) {

    				if (!firstArgument) {		

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerType");
    				ctr++;

    			}	

    			if (criteria.containsKey("customerClass")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerClass");
    				ctr++;

    			}			     

    			if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("rct.arSalesperson.slpName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("salesperson");
    				ctr++;

    			}
    			   			
    			if (criteria.containsKey("dateFrom")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}
    				jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateFrom");
    				ctr++;
    			}  

    			if (criteria.containsKey("dateTo")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}
    				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateTo");
    				ctr++;

    			}

    			boolean unpostedOnly=false;
    			if (criteria.containsKey("unpostedOnly")) {

    				String unposted = (String)criteria.get("unpostedOnly");

    				if (unposted.equals("YES")) {
    					if (!firstArgument) {

    						jbossQl.append("AND ");

    					} else {

    						firstArgument = false;
    						jbossQl.append("WHERE ");

    					}	       	  

    					unpostedOnly = true;
    					jbossQl.append("rct.rctPosted = 0 AND rct.rctVoid = 0 ");

    				} 
    			}
    			
    			if (criteria.containsKey("includedUnposted") && unpostedOnly==false) {

    				String unposted = (String)criteria.get("includedUnposted");

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}	       	  
    				if (unposted == null) unposted = "NO";
    				if (unposted.equals("NO")) {

    					jbossQl.append("rct.rctPosted = 1 ");

    				}else {

    					jbossQl.append("rct.rctVoid = 0 ");  	  

    				}

    			}

    			if (!firstArgument) {

    				jbossQl.append("AND ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}

    			jbossQl.append("rct.rctType='COLLECTION' AND rct.rctAdCompany=" + AD_CMPNY + " ");
    			
    	       	  String orderBy = null;
    	          
    	          if (GROUP_BY.equals("SALESPERSON") || GROUP_BY.equals("PRODUCT")) {
    	  	      	 
    	  	      	  orderBy = "rct.arCustomer.cstCustomerCode";

    	          } 

    	  		  if (orderBy != null) {
    	  		  
    	  		  	jbossQl.append("ORDER BY " + orderBy);
    	  		  	
    	  		  } 
    			
    			System.out.println("jbossQl.toString()4: "+jbossQl.toString());
    			System.out.println("unposted Only: 4" + criteria.containsKey("unpostedOnly"));
    			arReceipts = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);
    			
    			if(!arReceipts.isEmpty()) {

    				Iterator i = arReceipts.iterator();

    				while(i.hasNext()) {

    					LocalArReceipt arReceipt = (LocalArReceipt)i.next();  
    					
    					
						if (PRINT_SALES_RELATED_ONLY) {
							
							boolean isSalesRelated = false;
							
							Collection arDistributionRecords = arReceipt.getArDistributionRecords();
							
							Iterator x = arDistributionRecords.iterator();
							
							while (x.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
								
								if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
									
									isSalesRelated = true;
									break;
								
							}
									
							}
							
							if (!isSalesRelated)
									
								continue;
						}
						
    					if(SHOW_ENTRIES) {

    						boolean first = true;

    						Collection arDistributionRecords = arReceipt.getArDistributionRecords();

    						Iterator j = arDistributionRecords.iterator();

    						while(j.hasNext()) {

    							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

    							ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    							mdetails.setSrDate(arReceipt.getRctDate());
    							mdetails.setSrInvoiceNumber(arReceipt.getRctNumber());
    							mdetails.setSrReferenceNumber(arReceipt.getRctReferenceNumber());
    							mdetails.setSrDescription(arReceipt.getRctDescription());		  	  
    							mdetails.setSrCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode() + "-" + 
    									(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName()));		
    							mdetails.setSrCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
    							mdetails.setSrCstName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
    							mdetails.setSrCstCustomerCode2(arReceipt.getArCustomer().getCstCustomerCode());
    							mdetails.setPosted(arReceipt.getRctPosted());
    							// type
    							if(arReceipt.getArCustomer().getArCustomerType() == null) {

    								mdetails.setSrCstCustomerType("UNDEFINE");
        							mdetails.setPosted(arReceipt.getRctPosted());

    							} else {

    								mdetails.setSrCstCustomerType(arReceipt.getArCustomer().getArCustomerType().getCtName());
        							mdetails.setPosted(arReceipt.getRctPosted());
    							}

    							/*if(first) {

   	   	  						double AMNT_DUE = 0d;

   	   	  						AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
   	   	  								arReceipt.getGlFunctionalCurrency().getFcCode(),
										arReceipt.getGlFunctionalCurrency().getFcName(),
										arReceipt.getRctConversionDate(),
										arReceipt.getRctConversionRate(),
										arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

   	   	  						mdetails.setSrAmount(AMNT_DUE);

   	   	  						first = false;

   	   	  					}*/

    							double AMNT_DUE = 0d;

    							AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    									arReceipt.getGlFunctionalCurrency().getFcCode(),
    									arReceipt.getGlFunctionalCurrency().getFcName(),
    									arReceipt.getRctConversionDate(),
    									arReceipt.getRctConversionRate(),
    									arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    							mdetails.setSrAmount(AMNT_DUE);

    							mdetails.setOrderBy(ORDER_BY);

    							mdetails.setPosted(arReceipt.getRctPosted());
    							//set distribution record details
    							mdetails.setSrDrGlAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
    							mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
    							

    							if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    								mdetails.setSrDrDebitAmount(arDistributionRecord.getDrAmount());
        							mdetails.setPosted(arReceipt.getRctPosted());

    							} else {

    								mdetails.setSrDrCreditAmount(arDistributionRecord.getDrAmount());
        							mdetails.setPosted(arReceipt.getRctPosted());

    							}

    						}

    					} else {

    						ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    						mdetails.setSrDate(arReceipt.getRctDate());
    						mdetails.setSrInvoiceNumber(arReceipt.getRctNumber());
    						mdetails.setSrReferenceNumber(arReceipt.getRctReferenceNumber());
    						mdetails.setSrDescription(arReceipt.getRctDescription());		  	  
    						mdetails.setSrCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode() + "-" + 
    								(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName()));		
    						mdetails.setSrCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
    						mdetails.setSrCstName(arReceipt.getArCustomer().getCstName());
    						mdetails.setSrCstCustomerCode2(arReceipt.getArCustomer().getCstCustomerCode());
							mdetails.setPosted(arReceipt.getRctPosted());
							
							Collection arDistributionRecords = arReceipt.getArDistributionRecords();
							
							Iterator x = arDistributionRecords.iterator();
							
							while (x.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
								
								if (AD_CMPNY==15) {
									if (arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
									}
								} else {
									if ((arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") && (arDistributionRecord.getGlChartOfAccount().getCoaCode()!=5971 && arDistributionRecord.getGlChartOfAccount().getCoaCode()!=6259)) || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
									}
								}
									
							}
							
    						// type
    						if(arReceipt.getArCustomer().getArCustomerType() == null) {

    							mdetails.setSrCstCustomerType("UNDEFINE");

    						} else {

    							mdetails.setSrCstCustomerType(arReceipt.getArCustomer().getArCustomerType().getCtName());
    							mdetails.setPosted(arReceipt.getRctPosted());

    						}

    						double AMNT_DUE = 0d;

    						AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    								arReceipt.getGlFunctionalCurrency().getFcCode(),
    								arReceipt.getGlFunctionalCurrency().getFcName(),
    								arReceipt.getRctConversionDate(),
    								arReceipt.getRctConversionRate(),
    								arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    						mdetails.setSrAmount(AMNT_DUE * -1);
    				
    						mdetails.setOrderBy(ORDER_BY);
							mdetails.setPosted(arReceipt.getRctPosted());
							
    						double TOTAL_NET_AMNT = 0d;
    						double TOTAL_TAX_AMNT = 0d;

    						if(!arReceipt.getArInvoiceLines().isEmpty()) {

    							Iterator k = arReceipt.getArInvoiceLines().iterator();

    							while(k.hasNext()) {

    								LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)k.next();

    								ArRepSalesRegisterDetails newdetails = mdetails;
    								
    								TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
    								TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();

    								newdetails.setSrMemoName(arInvoiceLine.getArStandardMemoLine().getSmlName());
    								
    								System.out.println("arInvoiceLine.getArStandardMemoLine().getSmlName()5: " + arInvoiceLine.getArStandardMemoLine().getSmlName());
    								newdetails.setSrQuantity(arInvoiceLine.getIlQuantity());
    								System.out.println("arInvoiceLine.getIlQuantity()6: " + arInvoiceLine.getIlQuantity());
    	    						newdetails.setSrNetAmount(TOTAL_NET_AMNT * -1);
    	    						newdetails.setSrTaxAmount(TOTAL_TAX_AMNT * -1);
    								newdetails.setPosted(arReceipt.getRctPosted());
    								
    	    						if(arReceipt.getArSalesperson() != null) {

    	    							newdetails.setSrSlsSalespersonCode(arReceipt.getArSalesperson().getSlpSalespersonCode());
    	    							newdetails.setSrSlsName(arReceipt.getArSalesperson().getSlpName());
    	    							newdetails.setPosted(arReceipt.getRctPosted());
    	    						} else {
    	    							newdetails.setSrSlsSalespersonCode("");
    	    							newdetails.setSrSlsName("");
    	    							newdetails.setPosted(arReceipt.getRctPosted());
    	    						}

    	    						GRAND_TOTAL_NET_AMNT -= TOTAL_NET_AMNT;
    	    						GRAND_TOTAL_TAX_AMNT -= TOTAL_TAX_AMNT;
    	    						GRAND_TOTAL_AMNT -= (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);

    	    						newdetails.setSrRctType("COLLECTION");
    	    						
    	    						double SR_AI_APPLY_AMNT = 0;
    	    						double SR_AI_CRDTBL_W_TXAMNT = 0;
    	    						double SR_AI_DSCNT_AMNT = 0;
    	    						double SR_AI_APPLD_DPST = 0;
    	    						
    	    						Iterator iterAI = arReceipt.getArAppliedInvoices().iterator();
    								
    								while(iterAI.hasNext()){
    									
    									LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)iterAI.next();
    									SR_AI_APPLY_AMNT += arAppliedInvoice.getAiApplyAmount();
    									SR_AI_CRDTBL_W_TXAMNT += arAppliedInvoice.getAiCreditableWTax();
    									SR_AI_DSCNT_AMNT += arAppliedInvoice.getAiDiscountAmount();
    									SR_AI_APPLD_DPST += arAppliedInvoice.getAiAppliedDeposit();
    									
    								}							
    								
    	    						// Set Apllied Invoice 
    	    						newdetails.setSrAiApplyAmount(SR_AI_APPLY_AMNT);
    	    						newdetails.setSrAiCreditableWTax(SR_AI_CRDTBL_W_TXAMNT);
    	    						newdetails.setSrAiDiscountAmount(SR_AI_DSCNT_AMNT);
    	    						newdetails.setSrAiAppliedDeposit(SR_AI_APPLD_DPST);

    	    						list.add(newdetails);
    							}

    						}


    					}


    				}

    			}

    		}



    		if(list.isEmpty() || list.size() == 0) {

    			throw new GlobalNoRecordFoundException();

    		}


    		// sort

    		if(SHOW_ENTRIES) {

    			Collections.sort(list, ArRepSalesRegisterDetails.CoaAccountNumberComparator);

    		}

    		if(GROUP_BY.equals("CUSTOMER CODE")) {

    			Collections.sort(list, ArRepSalesRegisterDetails.CustomerCodeComparator);

    		} else if(GROUP_BY.equals("CUSTOMER TYPE")) {

    			Collections.sort(list, ArRepSalesRegisterDetails.CustomerTypeComparator);

    		} else if(GROUP_BY.equals("CUSTOMER CLASS")) {

    			Collections.sort(list, ArRepSalesRegisterDetails.CustomerClassComparator);

    		} else if(GROUP_BY.equals("MEMO LINE")){
				
    			Collections.sort(list, ArRepSalesRegisterDetails.MemoLineComparator);
				
		   } else if (GROUP_BY.equals("SALESPERSON")) {
			   
			   Collections.sort(list, ArRepSalesRegisterDetails.SalespersonComparator);
			   
		   } else if (GROUP_BY.equals("PRODUCT")) {
			   
			   Collections.sort(list, ArRepSalesRegisterDetails.ProductComparator);
			   
		   }else {

    			Collections.sort(list, ArRepSalesRegisterDetails.NoGroupComparator);

    		}
    		
    		if (SUMMARIZE) {

    			Collections.sort(list, ArRepSalesRegisterDetails.CoaAccountNumberComparator);

    		}

    		ArRepSalesRegisterDetails mdetails = (ArRepSalesRegisterDetails)list.get(list.size() - 1);

    		mdetails.setSrInvTotalNetAmount(GRAND_TOTAL_NET_AMNT);
    		mdetails.setSrInvTotalTaxAmount(GRAND_TOTAL_TAX_AMNT);
    		mdetails.setSrInvTotalAmount(GRAND_TOTAL_AMNT);
    		
    		return list;

    	} catch (GlobalNoRecordFoundException ex) {

    		throw ex;

    	} catch (Exception ex) {


    		ex.printStackTrace();
    		throw new EJBException(ex.getMessage());

    	}

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepSalesRegister(HashMap criteria, ArrayList branchList, String ORDER_BY, String GROUP_BY,boolean PRINT_SALES_RELATED_ONLY, boolean SHOW_ENTRIES, boolean SUMMARIZE, boolean detailedSales, boolean summary, Integer AD_BRNCH, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {

    	Debug.print("ArRepSalesRegisterControllerBean executeArRepSalesRegister");

    	LocalArInvoiceHome arInvoiceHome = null;
    	LocalArReceiptHome arReceiptHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome= null;
    	LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;

    	ArrayList list = new ArrayList();

    	//initialized EJB Home

    	try {

    		arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
    		arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
    		lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
    		lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
    		arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
    		lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
    		arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);

    	} catch (NamingException ex) 	{

    		throw new EJBException(ex.getMessage());

    	}

    	try { 

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		boolean firstArgument = true;
    		short ctr = 0;
    		int criteriaSize = criteria.size();	  
    		
    		StringBuffer jbossQl = new StringBuffer();
    		Iterator brIter = null;
    		AdBranchDetails details = null;
    		Object obj[];
    		Collection arInvoices = null;

    		double GRAND_TOTAL_NET_AMNT = 0d;
    		double GRAND_TOTAL_TAX_AMNT = 0d;
    		double GRAND_TOTAL_AMNT = 0d;
    		double GRAND_TOTAL_GROSS_AMNT = 0d;

    		if (criteria.containsKey("customerCode")) {

    			criteriaSize--;

    		}
    		
    		if (criteria.containsKey("customerBatch")) {

    			criteriaSize--;

    		}
    		
    		
    		
    		if (criteria.containsKey("unpostedOnly")) {

    			criteriaSize--;

    		}
    		
    		if (criteria.containsKey("includedUnposted")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("includedInvoices")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("includedMiscReceipts")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("includedCreditMemos")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("includedDebitMemos")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("paymentStatus")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("includedPriorDues")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("includedCollections")) {

    			criteriaSize--;

    		}
    		
    		if (criteria.containsKey("posted")) {

    			criteriaSize--;

    		}
    		
    		if (criteria.containsKey("orderStatus")) {

    			criteriaSize--;

    		}

    		if (criteria.containsKey("summary")) {

    			criteriaSize--;

    		}
    		if (criteria.containsKey("detailedSales")) {

    			criteriaSize--;

    		}
    		

			if (criteria.containsKey("salesperson")) {
				
				
				criteriaSize--;
			}
    		
    		if(((String)criteria.get("includedInvoices")).equals("YES") || ((String)criteria.get("includedCreditMemos")).equals("YES") || 
    				((String)criteria.get("includedDebitMemos")).equals("YES")) {

    			jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv WHERE (");

    			if (branchList.isEmpty())
    				throw new GlobalNoRecordFoundException();

    			brIter = branchList.iterator();

    			details = (AdBranchDetails)brIter.next();
    			jbossQl.append("inv.invAdBranch=" + details.getBrCode());

    			while(brIter.hasNext()) {

    				details = (AdBranchDetails)brIter.next();

    				jbossQl.append(" OR inv.invAdBranch=" + details.getBrCode());

    			}

    			jbossQl.append(") ");
    			firstArgument = false;

    			// Allocate the size of the object parameter

    			obj = new Object[criteriaSize];
    			
    			if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("inv.arCustomer.cstCustomerCode = '" + (String)criteria.get("customerCode") + "' ");

    			}
    			
    			if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("inv.arCustomer.cstCustomerBatch = '" + (String)criteria.get("customerBatch") + "' ");

    			}
    			
    			if (criteria.containsKey("invoiceBatchName")) {
    				
    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.arInvoiceBatch.ibName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("invoiceBatchName");
    				ctr++;
    				
    			}

    			if (criteria.containsKey("customerType")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerType");
    				ctr++;

    			}	

    			if (criteria.containsKey("customerClass")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerClass");
    				ctr++;

    			}			     

    			if (criteria.containsKey("dateFrom")) {

    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.invDate>=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateFrom");
    				ctr++;

    			}  	

    			if (criteria.containsKey("dateTo")) {

    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.invDate<=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateTo");
    				ctr++;

    			}    


    			if (criteria.containsKey("invoiceNumberFrom")) {

    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.invNumber>=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("invoiceNumberFrom");
    				ctr++;
    			}  


    			if (criteria.containsKey("invoiceNumberTo")) {

    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.invNumber<=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("invoiceNumberTo");
    				ctr++;

    			}

    			boolean unpostedOnly=false;
    			if (criteria.containsKey("unpostedOnly")) {

    				String unposted = (String)criteria.get("unpostedOnly");

    				if (unposted.equals("YES")) {
    					if (!firstArgument) {

    						jbossQl.append("AND ");

    					} else {

    						firstArgument = false;
    						jbossQl.append("WHERE ");

    					}	       	  

    					unpostedOnly = true;
    					jbossQl.append("inv.invPosted = 0 AND inv.invVoid = 0 ");

    				} 
    			}
    			
    			if (criteria.containsKey("includedUnposted") && unpostedOnly==false) {

    				String unposted = (String)criteria.get("includedUnposted");

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}	       	  
    				if (unposted == null) unposted = "NO";
    				if (unposted.equals("NO")) {

    					jbossQl.append("inv.invPosted = 1 ");

    				} else {

    					jbossQl.append("inv.invVoid = 0 ");  	  

    				}   	


    			}

    		
    			/*
    			if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arSalesperson.slpName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("salesperson");
    				ctr++;

    			}
    			*/
    				if (criteria.containsKey("paymentStatus")) {

    				String paymentStatus = (String)criteria.get("paymentStatus");

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				if (paymentStatus.equals("PAID")) {	      	 

    					jbossQl.append("inv.invAmountDue = inv.invAmountPaid ");

    				} else if (paymentStatus.equals("UNPAID")) {	         

    					jbossQl.append("inv.invAmountDue <> inv.invAmountPaid ");

    				}

    			}

    			if (criteria.containsKey("region")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("region");
    				ctr++;

    			}
    			

    			if (!firstArgument) {

    				jbossQl.append(" AND ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}

    			boolean firstSource = true;
    			
    			boolean includeCM = ((String)criteria.get("includedCreditMemos")).equals("YES");
    			boolean includeDM = ((String)criteria.get("includedDebitMemos")).equals("YES");
    			boolean includeINV = ((String)criteria.get("includedInvoices")).equals("YES");
    			

    			/*
    			
    			if(includeINV) {
    				
    				
    				if(includeCM) {
    					jbossQl.append("(inv.invCreditMemo = 1 OR inv.invCreditMemo = 0 ");
        				firstSource = false;
    					
    				}else {
    					
    					
    					jbossQl.append("(inv.invCreditMemo = 0 ");
        				firstSource = false;
    				}
    				
    				if(includeDM) {
    					
    					if (!firstSource) {

        					jbossQl.append("OR ");

        				} else {

        					jbossQl.append("(");

        				}

        				jbossQl.append("inv.invDebitMemo = 1 OR inv.invDebitMemo = 0 ");
        				firstSource = false;
    					
    				}else {
    					
    					
    					if (!firstSource) {

        					jbossQl.append("OR ");

        				} else {

        					jbossQl.append("(");

        				}

    					jbossQl.append("inv.invDebitMemo = 0 ");
        				firstSource = false;
    				}
    			}
    			
    			

    		
    			
    			if(((String)criteria.get("includedCreditMemos")).equals("YES")) {

    				jbossQl.append("(inv.invCreditMemo = 1 ");
    				firstSource = false;

    			}

    			if(((String)criteria.get("includedDebitMemos")).equals("YES")) {

    				if (!firstSource) {

    					jbossQl.append("OR ");

    				} else {

    					jbossQl.append("(");

    				}

    				jbossQl.append("inv.invDebitMemo = 1 ");
    				firstSource = false;

    			}

    			if(((String)criteria.get("includedInvoices")).equals("YES")) {


    				
    				if (!firstSource) {

    					jbossQl.append("OR ");

    				} else {

    					jbossQl.append("(");

    				}
    				if (((String)criteria.get("includedCollections")).equals("YES") && ((String)criteria.get("includedMiscReceipts")).equals("YES")) {
    				jbossQl.append("(inv.invCreditMemo = 0 AND inv.invDebitMemo = 0)) AND");
    				firstSource = false;
    				} else if (((String)criteria.get("includedMiscReceipts")).equals("YES")) {
    					jbossQl.append("(inv.invCreditMemo = 0 AND inv.invDebitMemo = 0)) AND");
        				firstSource = false;
    				}
    				else {
    					jbossQl.append("(inv.invCreditMemo = 0 AND inv.invDebitMemo = 0)");
        				firstSource = false;
    				}

    			}
    			*/
    			
    			
/*    			if(((String)criteria.get("Posted")).equals("YES")) {

    				if (!firstSource) {

    					jbossQl.append("OR ");

    				} else {

    					jbossQl.append("(");

    				}

    				jbossQl.append("(inv.posted = 0)");
    				firstSource = false;

    			}
        		if(((String)criteria.get("includedMiscReceipts")).equals("YES")) {
    			jbossQl.append(" ");



    			if (!firstArgument) {

    				jbossQl.append(" ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}
        		}
        		else {
        			jbossQl.append(") ");



        			if (!firstArgument) {

        				jbossQl.append(" AND ");

        			} else {

        				firstArgument = false;
        				jbossQl.append("WHERE ");

        			}
        			
        		}
        		
        		*/
    			jbossQl.append("inv.invAdCompany=" + AD_CMPNY + " ");
    			
  	       	  String orderBy = null;
	          
	          if (GROUP_BY.equals("SALESPERSON") || GROUP_BY.equals("PRODUCT")) {
	  	      	 
	  	      	  orderBy = "inv.arCustomer.cstCustomerCode";
	          } 

                        if (orderBy != null) {

                              jbossQl.append("ORDER BY " + orderBy);

                        }  

      			System.out.println("jbossQl.toString()5: "+jbossQl.toString());
      			System.out.println("AD_CMPNY: " + AD_CMPNY);
    			System.out.println("unposted Only:5 " + criteria.containsKey("unpostedOnly"));
    			arInvoices = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);
    			String refNum="";
    			String cst = "";
    			
    			if(!arInvoices.isEmpty()) {

    				Iterator i = arInvoices.iterator();

    				while (i.hasNext()) {

    					LocalArInvoice arInvoice = (LocalArInvoice)i.next();   	

    					
    					if(includeINV) {
    						
    						
    						if(!includeCM && arInvoice.getInvCreditMemo()==EJBCommon.TRUE) {
        						continue;
        						
        					}
        					
        					if(!includeDM && arInvoice.getInvDebitMemo()==EJBCommon.TRUE) {
        						continue;
        						
        					}
    					}
    					
    					
    					if(arInvoice.getInvCreditMemo()==EJBCommon.TRUE) {
    						try {
								LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

								
							}catch(FinderException ex) {
								
								continue;
							}
    					}
    					
    					
    					/* if (PRINT_SALES_RELATED_ONLY) {
    					 * Collection arDistributionRecords = arInvoice.getArDistributionRecords();
    					 * boolean isSalesRelated = false;
    					 * while (){
    					 * 	   LocalArDistribut
    					 *    if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")
    					 *    isSalesRelated = true;
    					 *    break;
    					 * 
    					 * }
    					 * 
    					 * if (!isSalesRelated) continue;
    					 }
    				
						*/
    					double REVENUE_AMOUNT = 0d;
    					Collection arDistributionRecords = arInvoice.getArDistributionRecords();
						
						Iterator iDr = arDistributionRecords.iterator();
						
							while (iDr.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) iDr.next();
								if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
									
									
									if(arDistributionRecord.getDrDebit()==EJBCommon.TRUE) {
										REVENUE_AMOUNT -= arDistributionRecord.getDrAmount();
									}else {
										REVENUE_AMOUNT += arDistributionRecord.getDrAmount();
									}
									
								
								
							}
								
						}
    					
                        if (PRINT_SALES_RELATED_ONLY) {
							
							boolean isSalesRelated = false;
							
							arDistributionRecords = arInvoice.getArDistributionRecords();
							
							Iterator x = arDistributionRecords.iterator();
							
								while (x.hasNext()) {
									
									LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
									if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
										
										isSalesRelated = true;
										break;
									
								}
									
							}
							
							if (!isSalesRelated)
									
								continue;
						}

    					if(SHOW_ENTRIES) {

    						boolean first = true;

    						arDistributionRecords = arInvoice.getArDistributionRecords();

    						Iterator k = arDistributionRecords.iterator();

    						while(k.hasNext()) {

    							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)k.next();

    							ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    							mdetails.setSrDate(arInvoice.getInvDate());
    							mdetails.setSrInvoiceNumber(arInvoice.getInvNumber());
    							mdetails.setSrReferenceNumber(arInvoice.getInvReferenceNumber());
    							refNum=arInvoice.getInvReferenceNumber();
    							mdetails.setSrDescription(arInvoice.getInvDescription());		  	  
    							mdetails.setSrCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode() + "-" + arInvoice.getArCustomer().getCstName());		
    							mdetails.setSrCstCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());
    							mdetails.setSrCstName(arInvoice.getArCustomer().getCstName());
    							mdetails.setSrCstCustomerCode2(arInvoice.getArCustomer().getCstCustomerCode());
    							mdetails.setPosted(arInvoice.getInvPosted());    							
    							
    							// type
    							if(arInvoice.getArCustomer().getArCustomerType() == null) {

    								mdetails.setSrCstCustomerType("UNDEFINE");
	    							mdetails.setPosted(arInvoice.getInvPosted());
	    							
    							} else {

    								mdetails.setSrCstCustomerType(arInvoice.getArCustomer().getArCustomerType().getCtName());
	    							mdetails.setPosted(arInvoice.getInvPosted());
	    							
    							}

    							if(first) {

    								double AMNT_DUE = 0d;

    								if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    									AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    											arInvoice.getGlFunctionalCurrency().getFcCode(),
    											arInvoice.getGlFunctionalCurrency().getFcName(),
    											arInvoice.getInvConversionDate(),
    											arInvoice.getInvConversionRate(),
    											arInvoice.getInvAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    								} else {
    									
    									LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    									AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    											arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
    											arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
    											arCreditedInvoice.getInvConversionDate(),
    											arCreditedInvoice.getInvConversionRate(),
    											arInvoice.getInvAmountDue(), AD_CMPNY) * -1, adCompany.getGlFunctionalCurrency().getFcPrecision());

    								}

    								mdetails.setSrAmount(AMNT_DUE);
	    							mdetails.setPosted(arInvoice.getInvPosted());

    							}

    							mdetails.setOrderBy(ORDER_BY);
    							mdetails.setPosted(arInvoice.getInvPosted());
    							
    							double TOTAL_NET_AMNT = 0d;
    							double TOTAL_TAX_AMNT = 0d;
    							double TOTAL_DISC_AMNT = 0d;
    							double TOTAL_GROSS_AMNT = 0d;

    							//compute net amount, tax amount and discount amount
    							if(!arInvoice.getArInvoiceLineItems().isEmpty()) {

    								Iterator j = arInvoice.getArInvoiceLineItems().iterator();

    								while(j.hasNext()) {

    									LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)j.next();

    									TOTAL_GROSS_AMNT += arInvoiceLineItem.getIliAmount() - arInvoiceLineItem.getIliTaxAmount();
    									TOTAL_NET_AMNT += arInvoiceLineItem.getIliAmount();
    									TOTAL_TAX_AMNT += arInvoiceLineItem.getIliTaxAmount();
    									TOTAL_DISC_AMNT += arInvoiceLineItem.getIliTotalDiscount();

    								}

    							} else if(!arInvoice.getArInvoiceLines().isEmpty()) {

    								Iterator j = arInvoice.getArInvoiceLines().iterator();

    								while(j.hasNext()) {

    									LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)j.next();

    									TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
    									TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();
    									TOTAL_GROSS_AMNT += arInvoiceLine.getIlAmount() - arInvoiceLine.getIlTaxAmount();
    								}

    							} else if(!arInvoice.getArSalesOrderInvoiceLines().isEmpty()) {

    								Iterator j = arInvoice.getArSalesOrderInvoiceLines().iterator();

    								while(j.hasNext()) {

    									LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)j.next();

    									TOTAL_NET_AMNT += arSalesOrderInvoiceLine.getSilAmount();
    									TOTAL_TAX_AMNT += arSalesOrderInvoiceLine.getSilTaxAmount();
    									TOTAL_DISC_AMNT += arSalesOrderInvoiceLine.getSilTotalDiscount();
    									TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;
    									
    								}

    							} else if(!arInvoice.getArJobOrderInvoiceLines().isEmpty()) {

    								Iterator j = arInvoice.getArJobOrderInvoiceLines().iterator();

    								while(j.hasNext()) {

    									
    									LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine)j.next();
    									
    									TOTAL_NET_AMNT += arJobOrderInvoiceLine.getJilAmount();
    									TOTAL_TAX_AMNT += arJobOrderInvoiceLine.getJilTaxAmount();
    									TOTAL_DISC_AMNT += arJobOrderInvoiceLine.getJilTotalDiscount();
    									TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;
    									
    								}

    							} else if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE && arInvoice.getArInvoiceLineItems().isEmpty()) {

    								try {

    									arDistributionRecord = arDistributionRecordHome.findByDrClassAndInvCode("TAX", arInvoice.getInvCode(), AD_CMPNY);	
    									  TOTAL_TAX_AMNT = arDistributionRecord.getDrAmount();
                                          TOTAL_NET_AMNT = arInvoice.getInvAmountDue() - TOTAL_TAX_AMNT;
    									
    								//	TOTAL_TAX_AMNT = arDistributionRecord.getDrAmount();
    								//	TOTAL_NET_AMNT = arInvoice.getInvAmountDue();
    								//	TOTAL_GROSS_AMNT = arInvoice.getInvAmountDue() - TOTAL_TAX_AMNT;

    								} catch(FinderException ex) {

    									TOTAL_TAX_AMNT = 0;
    									TOTAL_NET_AMNT = arInvoice.getInvAmountDue();

    								}

    							}

    							if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {

    								if(TOTAL_NET_AMNT != 0) {

    									TOTAL_NET_AMNT = TOTAL_NET_AMNT * -1;

    								}

    								if(TOTAL_TAX_AMNT != 0) {

    									TOTAL_TAX_AMNT = TOTAL_TAX_AMNT * -1;

    								}

    							}
    							mdetails.setSrGrossAmount(TOTAL_GROSS_AMNT);
    							mdetails.setSrNetAmount(TOTAL_NET_AMNT);
    							mdetails.setSrTaxAmount(TOTAL_TAX_AMNT);
    							mdetails.setPosted(arInvoice.getInvPosted());
    							
    							if(first) {

    								mdetails.setSrDiscountAmount(TOTAL_DISC_AMNT);
    								first = false;

    							}

    							if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    								if(arInvoice.getArSalesperson() != null) {

    									

				    					if (criteria.containsKey("salesperson") ) {

				    						String slsprsn = (String)criteria.get("salesperson");
				    						
				    						if(!slsprsn.equals(arInvoice.getArSalesperson().getSlpName())) {
				    							continue;
				    						}
				    						
				    	    			

				    	    			}
    									
    									mdetails.setSrSlsSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
    									mdetails.setSrSlsName(arInvoice.getArSalesperson().getSlpName());
    	    							mdetails.setPosted(arInvoice.getInvPosted());
    								} else {
    									
    									if (criteria.containsKey("salesperson") ) {
    										
    										continue;
    									}
    									mdetails.setSrSlsSalespersonCode("");
    									mdetails.setSrSlsName("");
    									mdetails.setPosted(arInvoice.getInvPosted());
    								}

    							} else {

    								try {

    									LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    									if(arCreditedInvoice.getArSalesperson() != null) {

    										
    				    					if (criteria.containsKey("salesperson") ) {

    				    						String slsprsn = (String)criteria.get("salesperson");
    				    						
    				    						if(!slsprsn.equals(arCreditedInvoice.getArSalesperson().getSlpName())) {
    				    							continue;
    				    						}
    				    						
    				    	    			

    				    	    			}
    				    				
    										
    										
    										
    										mdetails.setSrSlsSalespersonCode(arCreditedInvoice.getArSalesperson().getSlpSalespersonCode());
    										mdetails.setSrSlsName(arCreditedInvoice.getArSalesperson().getSlpName());
    		    							mdetails.setPosted(arInvoice.getInvPosted());
    									} else {
    										
    										if (criteria.containsKey("salesperson") ) {
    											
    											continue;
    										}
    										mdetails.setSrSlsSalespersonCode("");
    										mdetails.setSrSlsName("");
    										mdetails.setPosted(arInvoice.getInvPosted());
    									}

    								} catch(FinderException ex) {

    								}

    							}

    							//set distribution record details
    							mdetails.setSrDrGlAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
    							mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
    							mdetails.setPosted(arInvoice.getInvPosted());
    							if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    								mdetails.setSrDrDebitAmount(arDistributionRecord.getDrAmount());
	    							mdetails.setPosted(arInvoice.getInvPosted());
    							} else {
    								mdetails.setSrDrCreditAmount(arDistributionRecord.getDrAmount());
	    							mdetails.setPosted(arInvoice.getInvPosted());
    							}

    							mdetails.setSrRevenueAmount(REVENUE_AMOUNT);
    							list.add(mdetails);

    						}

    					} else {
				
                                ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();

                                mdetails.setSrDate(arInvoice.getInvDate());
                                mdetails.setSrInvoiceNumber(arInvoice.getInvNumber());
                                mdetails.setSrReferenceNumber(arInvoice.getInvReferenceNumber());
                                refNum=arInvoice.getInvReferenceNumber();
                                mdetails.setSrDescription(arInvoice.getInvDescription());
                                mdetails.setSrCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode() +	 "-" + arInvoice.getArCustomer().getCstName());		
                                mdetails.setSrCstCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());
                                mdetails.setSrCstName(arInvoice.getArCustomer().getCstName());
                                mdetails.setSrCstCustomerCode2(arInvoice.getArCustomer().getCstCustomerCode());
                                mdetails.setPosted(arInvoice.getInvPosted());
		    						
								/*Collection arDistributionRecords = arInvoice.getArDistributionRecords();
								
								Iterator x = arDistributionRecords.iterator();
								
								while (x.hasNext()) {
									
									LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
									
									if (AD_CMPNY==15) {
										if (arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
    										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
    									}
									} else {
										if ((arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") && (arDistributionRecord.getGlChartOfAccount().getCoaCode()!=5971 && arDistributionRecord.getGlChartOfAccount().getCoaCode()!=6259)) || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
    										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
    									}
									}
										
								}
*/
							
                                        // type
                                        if(arInvoice.getArCustomer().getArCustomerType() == null) {

                                                mdetails.setSrCstCustomerType("UNDEFINE");
                                                mdetails.setPosted(arInvoice.getInvPosted());
                                        } else {

                                                mdetails.setSrCstCustomerType(arInvoice.getArCustomer().getArCustomerType().getCtName());
                                                mdetails.setPosted(arInvoice.getInvPosted());
                                        }

                                        double AMNT_DUE = 0d;
                                        double AMOUNT = 0d;

                                        if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                                                AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
                                                                arInvoice.getGlFunctionalCurrency().getFcCode(),
                                                                arInvoice.getGlFunctionalCurrency().getFcName(),
                                                                arInvoice.getInvConversionDate(),
                                                                arInvoice.getInvConversionRate(),
                                                                arInvoice.getInvAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

                                        } else {

                                                System.out.println("arInvoice.getInvCode(="+arInvoice.getInvCode());
                                                System.out.println("arInvoice.getInvCmInvoiceNumber(="+arInvoice.getInvCmInvoiceNumber());
                                                try {
                                                	
                                                	 LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

                                                     AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
                                                                     arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
                                                                     arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
                                                                     arCreditedInvoice.getInvConversionDate(),
                                                                     arCreditedInvoice.getInvConversionRate(),
                                                                     arInvoice.getInvAmountDue(), AD_CMPNY) * -1, adCompany.getGlFunctionalCurrency().getFcPrecision());

                                                	
                                                }catch(FinderException ex) {
                                                	 System.out.println("arInvoice.getInvCmInvoiceNumber(="+arInvoice.getInvCmInvoiceNumber() + " => not found");
                                                }
                                               
                                        }

                                        mdetails.setSrAmount(AMNT_DUE);
                                        mdetails.setOrderBy(ORDER_BY);
                                                mdetails.setPosted(arInvoice.getInvPosted());

                                        double TOTAL_NET_AMNT = 0d;
                                        double TOTAL_GROSS_AMNT =0d;
                                        double TOTAL_TAX_AMNT = 0d;
                                        double TOTAL_DISC_AMNT = 0d;

                                            //compute net amount, tax amount and discount amount
                                            if(!arInvoice.getArInvoiceLineItems().isEmpty()) {

                                                    Iterator j = arInvoice.getArInvoiceLineItems().iterator();

                                                    while(j.hasNext()) {

                                                            LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)j.next();
                                                            TOTAL_GROSS_AMNT += arInvoiceLineItem.getIliAmount() + arInvoiceLineItem.getIliTaxAmount() + arInvoiceLineItem.getIliTotalDiscount();

                                                            TOTAL_NET_AMNT += arInvoiceLineItem.getIliAmount();
                                                            TOTAL_TAX_AMNT += arInvoiceLineItem.getIliTaxAmount();
                                                            TOTAL_DISC_AMNT += arInvoiceLineItem.getIliTotalDiscount();

                                                    }

                                            } else if(!arInvoice.getArInvoiceLines().isEmpty()) {

                                                    Iterator j = arInvoice.getArInvoiceLines().iterator();

                                                    while(j.hasNext()) {

                                                            LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)j.next();
                                                            mdetails.setSrQuantity(arInvoiceLine.getIlQuantity());
                                                            TOTAL_GROSS_AMNT += arInvoiceLine.getIlAmount() + arInvoiceLine.getIlTaxAmount();

                                                            TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
                                                            TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();


                                                            //System.out.println("arInvoiceLine.getIlQuantity()7: " + arInvoiceLine.getIlQuantity());
                                                    }

                                            } else if(!arInvoice.getArSalesOrderInvoiceLines().isEmpty()) {

                                                    Iterator j = arInvoice.getArSalesOrderInvoiceLines().iterator();

                                                    while(j.hasNext()) {

                                                            LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)j.next();
                                                            TOTAL_GROSS_AMNT += arSalesOrderInvoiceLine.getSilAmount() - arSalesOrderInvoiceLine.getSilTaxAmount();
                                                            TOTAL_NET_AMNT += arSalesOrderInvoiceLine.getSilAmount();
                                                            TOTAL_TAX_AMNT += arSalesOrderInvoiceLine.getSilTaxAmount();
                                                            TOTAL_DISC_AMNT += arSalesOrderInvoiceLine.getSilTotalDiscount();

                                                    }

                                            } else if(!arInvoice.getArJobOrderInvoiceLines().isEmpty()) {

                                                Iterator j = arInvoice.getArJobOrderInvoiceLines().iterator();

                                                while(j.hasNext()) {
                                                		LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine)j.next();
                                                      
                                                        TOTAL_GROSS_AMNT += arJobOrderInvoiceLine.getJilAmount() - arJobOrderInvoiceLine.getJilTaxAmount();
                                                        TOTAL_NET_AMNT += arJobOrderInvoiceLine.getJilAmount();
                                                        TOTAL_TAX_AMNT += arJobOrderInvoiceLine.getJilTaxAmount();
                                                        TOTAL_DISC_AMNT += arJobOrderInvoiceLine.getJilTotalDiscount();

                                                }

                                        } else if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE && arInvoice.getArInvoiceLineItems().isEmpty()) {

                                                    try {

                                                            LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.findByDrClassAndInvCode("TAX", arInvoice.getInvCode(), AD_CMPNY);	

                                                            TOTAL_TAX_AMNT = arDistributionRecord.getDrAmount();
                                                            TOTAL_NET_AMNT = arInvoice.getInvAmountDue() - TOTAL_TAX_AMNT;
                                                       //     TOTAL_TAX_AMNT = arDistributionRecord.getDrAmount();
                        								//	TOTAL_NET_AMNT = arInvoice.getInvAmountDue();
                        								//	TOTAL_GROSS_AMNT = arInvoice.getInvAmountDue() - TOTAL_TAX_AMNT;
                                                    } catch(FinderException ex) {

                                                            TOTAL_TAX_AMNT = 0;
                                                            TOTAL_NET_AMNT = arInvoice.getInvAmountDue();

                                                    }

                                            }

                                            if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {
                                                    mdetails.setSrInvCreditMemo(true);
                                                    mdetails.setPosted(arInvoice.getInvPosted());
                                                    mdetails.setCmInvoiceNumber(arInvoice.getInvCmInvoiceNumber());
                                            } else {
                                                    mdetails.setSrInvCreditMemo(false);
                                                    mdetails.setPosted(arInvoice.getInvPosted());
                                            }

                                            mdetails.setSrServiceAmount(0d);
                                            System.out.println("TOTAL_GROSS_AMNT="+TOTAL_GROSS_AMNT);
                                            mdetails.setSrGrossAmount(TOTAL_GROSS_AMNT);
                                            mdetails.setSrNetAmount(TOTAL_NET_AMNT);
                                            mdetails.setSrTaxAmount(TOTAL_TAX_AMNT);
                                            mdetails.setSrDiscountAmount(TOTAL_DISC_AMNT);
                                                    mdetails.setPosted(arInvoice.getInvPosted());

                                            if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                                                    if(arInvoice.getArSalesperson() != null) {

                                                    	if (criteria.containsKey("salesperson") ) {

                				    						String slsprsn = (String)criteria.get("salesperson");
                				    						
                				    						if(!slsprsn.equals(arInvoice.getArSalesperson().getSlpName())) {
                				    							continue;
                				    						}
                				    						
                				    	    			

                				    	    			}
                                                            mdetails.setSrSlsSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
                                                            mdetails.setSrSlsName(arInvoice.getArSalesperson().getSlpName());
                                                            mdetails.setPosted(arInvoice.getInvPosted());
                                                    } else {
                                                    	if (criteria.containsKey("salesperson") ) {
                                                    		continue;
                                                    	}
                                                    	
                                                            mdetails.setSrSlsSalespersonCode("");
                                                            mdetails.setSrSlsName("");
                                                            mdetails.setPosted(arInvoice.getInvPosted());
                                                    }


                                            } else {

                                                    try {

                                                            LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

                                                            if(arCreditedInvoice.getArSalesperson() != null) {
                                                            	
                                                            	
                                                            	if (criteria.containsKey("salesperson") ) {

                        				    						String slsprsn = (String)criteria.get("salesperson");
                        				    						
                        				    						if(!slsprsn.equals(arCreditedInvoice.getArSalesperson().getSlpName())) {
                        				    							continue;
                        				    						}
                        				    						
                        				    	    			

                        				    	    			}
                                                            	
                                                            	
                                                            	
                                                                    mdetails.setSrSlsSalespersonCode(arCreditedInvoice.getArSalesperson().getSlpSalespersonCode());
                                                                    mdetails.setSrSlsName(arCreditedInvoice.getArSalesperson().getSlpName());
                                                            mdetails.setPosted(arInvoice.getInvPosted());
                                                            } else {
                                                            	
                                                            	if (criteria.containsKey("salesperson") ) {
                                                            		continue;
                                                            	}
                                                                    mdetails.setSrSlsSalespersonCode("");
                                                                    mdetails.setSrSlsName("");
                                                                    mdetails.setPosted(arInvoice.getInvPosted());
                                                            }

                                                    } catch(FinderException ex) {

                                                    }

                                            }

                                            if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {

                                            		GRAND_TOTAL_GROSS_AMNT -= TOTAL_GROSS_AMNT;
                                                    GRAND_TOTAL_NET_AMNT -= TOTAL_NET_AMNT;
                                                    GRAND_TOTAL_TAX_AMNT -= TOTAL_TAX_AMNT;
                                                    GRAND_TOTAL_AMNT -= (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);

                                            } else {

                                            		GRAND_TOTAL_GROSS_AMNT += TOTAL_GROSS_AMNT;
                                                    GRAND_TOTAL_NET_AMNT += TOTAL_NET_AMNT;
                                                    GRAND_TOTAL_TAX_AMNT += TOTAL_TAX_AMNT;
                                                    GRAND_TOTAL_AMNT += (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);

                                            }

                                            mdetails.setSrRctType("INVOICE RELEASES");
                                                    mdetails.setPosted(arInvoice.getInvPosted());
                                            // get receipt info

                                            String invoiceReceiptNumbers = null;
                                            String invoiceReceiptDates = null;
                                            double invoiceReceiptAmount = 0.0d;
                                            Collection arInvoiceReceipts = arReceiptHome.findByInvCodeAndRctVoid(arInvoice.getInvCode(), EJBCommon.FALSE, AD_CMPNY);

                                            Iterator j = arInvoiceReceipts.iterator();

                                            while (j.hasNext()) {

                                                LocalArReceipt arReceipt = (LocalArReceipt)j.next();

                                                if (invoiceReceiptNumbers == null) {
                                                        invoiceReceiptNumbers = arReceipt.getRctNumber();
                                                       invoiceReceiptAmount += arReceipt.getRctAmount();
                                                } else if (invoiceReceiptNumbers != null && !invoiceReceiptNumbers.contains(arReceipt.getRctNumber())){
                                                        invoiceReceiptNumbers += "/" + arReceipt.getRctNumber();
                                                      invoiceReceiptAmount += arReceipt.getRctAmount();
                                                }

                                                if (invoiceReceiptDates == null) {
                                                        invoiceReceiptDates = EJBCommon.convertSQLDateToString(arReceipt.getRctDate());
                                                } else if (invoiceReceiptDates != null && !invoiceReceiptDates.contains(EJBCommon.convertSQLDateToString(arReceipt.getRctDate()))){
                                                        invoiceReceiptDates += "/" + EJBCommon.convertSQLDateToString(arReceipt.getRctDate());
                                                }

                                            }

                                            
                                            mdetails.setSrInvReceiptNumbers(invoiceReceiptNumbers==null?"":invoiceReceiptNumbers);
                                            mdetails.setSrInvReceiptDates(invoiceReceiptDates);
                                            mdetails.setPosted(arInvoice.getInvPosted());
                                            mdetails.setSrOrAmount(invoiceReceiptAmount);
                                            mdetails.setSrRevenueAmount(REVENUE_AMOUNT);
                                            list.add(mdetails);

    					}

							
    					

    				}

    			}

    		}


    		// Prior Dues

    		if(((String)criteria.get("includedPriorDues")).equals("YES")) {

    			jbossQl = new StringBuffer();
    			jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv WHERE (");

    			if (branchList.isEmpty())
    				throw new GlobalNoRecordFoundException();

    			brIter = branchList.iterator();

    			details = (AdBranchDetails)brIter.next();
    			jbossQl.append("inv.invAdBranch=" + details.getBrCode());

    			while(brIter.hasNext()) {

    				details = (AdBranchDetails)brIter.next();

    				jbossQl.append(" OR inv.invAdBranch=" + details.getBrCode());

    			}

    			jbossQl.append(") ");
    			firstArgument = false;

    			ctr = 0;
    			// Allocate the size of the object parameter
    			System.out.print("criteriaSize to: " + criteriaSize);
    			obj = new Object[criteriaSize]; 
    			System.out.print("object to: " + obj);

    			if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("inv.arCustomer.cstCustomerCode = '" + (String)criteria.get("customerCode") + "' ");

    			}
    			
    			if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("inv.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}
    			
    			if (criteria.containsKey("invoiceBatchName")) {
    				
    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.arInvoiceBatch.ibName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("invoiceBatchName");
    				ctr++;
    				
    			}

    			if (criteria.containsKey("customerType")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerType");
    				ctr++;

    			}	

    			if (criteria.containsKey("customerClass")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerClass");
    				ctr++;

    			}			     

    			if (criteria.containsKey("dateFrom")) {

    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("inv.invDate<?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateFrom");
    				ctr++;

    			}  	

    			/*
    			if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("inv.arSalesperson.slpName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("salesperson");
    				ctr++;

    			}
    			*/
    			if (criteria.containsKey("dateTo")) {

    				obj[ctr] = (Date)criteria.get("dateTo");
    				ctr++;

    			}    

    			if (criteria.containsKey("invoiceNumberFrom")) {

    				obj[ctr] = (String)criteria.get("invoiceNumberFrom");
    				ctr++;
    			}  

    			if (criteria.containsKey("invoiceNumberTo")) {

    				obj[ctr] = (String)criteria.get("invoiceNumberTo");
    				ctr++;

    			}

    			boolean unpostedOnly=false;
    			if (criteria.containsKey("unpostedOnly")) {

    				String unposted = (String)criteria.get("unpostedOnly");

    				if (unposted.equals("YES")) {
    					if (!firstArgument) {

    						jbossQl.append("AND ");

    					} else {

    						firstArgument = false;
    						jbossQl.append("WHERE ");

    					}	       	  

    					unpostedOnly = true;
    					jbossQl.append("inv.invPosted = 0 AND inv.invVoid = 0 ");

    				} 
    			}
    			
    			if (criteria.containsKey("includedUnposted") && unpostedOnly==false) {
    				String unPosted = (String)criteria.get("includedUnPosted");

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}	       	  
    				if (unPosted == null) unPosted = "NO";
    				if (unPosted.equals("NO")) {

    					jbossQl.append("inv.invPosted = 1 ");
    					
    				} else {

    					jbossQl.append("inv.invVoid = 0 ");  	  

    				}
    				
    				
    			}
    			System.out.print("includedUnposted: " + criteria.get("includedUnposted"));

    			if (!firstArgument) {

    				jbossQl.append("AND ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}

    			boolean firstSource = true;

    			if(((String)criteria.get("includedCreditMemos")).equals("YES")) {

    				jbossQl.append("(inv.invCreditMemo = 1 ");
    				firstSource = false;

    			}

    			if(((String)criteria.get("includedDebitMemos")).equals("YES")) {

    				if (!firstSource) {

    					jbossQl.append("OR ");

    				} else {

    					jbossQl.append("(");

    				}

    				jbossQl.append("inv.invDebitMemo = 1 ");
    				firstSource = false;

    			}

    			if(((String)criteria.get("includedInvoices")).equals("YES")) {

    				if (!firstSource) {

    					jbossQl.append("OR ");

    				} else {

    					jbossQl.append("(");

    				}

    				jbossQl.append("(inv.invCreditMemo = 0 AND inv.invDebitMemo = 0)");
    				firstSource = false;

    			}
    			
    			if(((String)criteria.get("includedMiscReceipts")).equals("YES")) {
    			jbossQl.append(" ");



    			if (!firstArgument) {

    				jbossQl.append(" ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}
    			} else { 
    				jbossQl.append(") ");



        			if (!firstArgument) {

        				jbossQl.append(" AND ");

        			} else {

        				firstArgument = false;
        				jbossQl.append("WHERE ");

        			}
    			}
    			String refNum="";
    			jbossQl.append("inv.invAdCompany=" + AD_CMPNY + " ");
    			
    	       	  String orderBy = null;
    	          
    	          if (GROUP_BY.equals("SALESPERSON")) {
    	  	      	 
    	  	      	  orderBy = "inv.arCustomer.cstCustomerCode";

    	          } 

    	  		  if (orderBy != null) {
    	  		  
    	  		  	jbossQl.append("ORDER BY " + orderBy);
    	  		  	
    	  		  }
    			
    			
    			System.out.println("jbossQl.toString(): 6"+jbossQl.toString());
    			System.out.println("unposted Only: 6" + criteria.containsKey("unpostedOnly"));
    			arInvoices = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);
    			
    			if(!arInvoices.isEmpty()) {

    				Iterator i = arInvoices.iterator();

    				while (i.hasNext()) {

    					LocalArInvoice arInvoice = (LocalArInvoice)i.next(); 
    					
    					double REVENUE_AMOUNT = 0d;
    					Collection arDistributionRecords = arInvoice.getArDistributionRecords();
						
						Iterator iDr = arDistributionRecords.iterator();
						
						while (iDr.hasNext()) {
							
							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) iDr.next();
							
							if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
								
								if(arDistributionRecord.getDrDebit()==EJBCommon.TRUE){
									REVENUE_AMOUNT -= arDistributionRecord.getDrAmount();
								}else {
									REVENUE_AMOUNT += arDistributionRecord.getDrAmount();
								}
							
							}
								
						}
    					
    					if(arInvoice.getInvCreditMemo()==EJBCommon.TRUE) {
    						try {
								LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

								
							}catch(FinderException ex) {
								
								continue;
							}
    					}
    					
    					
						if (PRINT_SALES_RELATED_ONLY) {
							
							boolean isSalesRelated = false;
							
							arDistributionRecords = arInvoice.getArDistributionRecords();
							
							Iterator x = arDistributionRecords.iterator();
							
							while (x.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
								
								if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
									
									isSalesRelated = true;
									break;
								
							}
									
							}
							
							if (!isSalesRelated)
									
								continue;
						}    					
    					
    					if(SHOW_ENTRIES) {

    						boolean first = true;

    						arDistributionRecords = arInvoice.getArDistributionRecords();

    						Iterator k = arDistributionRecords.iterator();

    						while(k.hasNext()) {

    							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)k.next();

    							ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    							mdetails.setSrDate(arInvoice.getInvDate());
    							mdetails.setSrInvoiceNumber(arInvoice.getInvNumber());
    							mdetails.setSrReferenceNumber(arInvoice.getInvReferenceNumber());
    							refNum=arInvoice.getInvReferenceNumber();
    							mdetails.setSrDescription(arInvoice.getInvDescription());		  	  
    							mdetails.setSrCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode() + "-" + arInvoice.getArCustomer().getCstName());		
    							mdetails.setSrCstCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());
    							mdetails.setSrCstName(arInvoice.getArCustomer().getCstName());
    							mdetails.setSrCstCustomerCode2(arInvoice.getArCustomer().getCstCustomerCode());
    							mdetails.setPosted(arInvoice.getInvPosted());
    							
    							// type
    							if(arInvoice.getArCustomer().getArCustomerType() == null) {

    								mdetails.setSrCstCustomerType("UNDEFINE");

    							} else {

    								mdetails.setSrCstCustomerType(arInvoice.getArCustomer().getArCustomerType().getCtName());

    							}

    							if(first) {

    								double AMNT_DUE = 0d;

    								if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    									AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    											arInvoice.getGlFunctionalCurrency().getFcCode(),
    											arInvoice.getGlFunctionalCurrency().getFcName(),
    											arInvoice.getInvConversionDate(),
    											arInvoice.getInvConversionRate(),
    											arInvoice.getInvAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    								} else {

    									LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    									AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    											arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
    											arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
    											arCreditedInvoice.getInvConversionDate(),
    											arCreditedInvoice.getInvConversionRate(),
    											arInvoice.getInvAmountDue(), AD_CMPNY) * -1, adCompany.getGlFunctionalCurrency().getFcPrecision());

    								}

    								mdetails.setSrAmount(AMNT_DUE);
	    							mdetails.setPosted(arInvoice.getInvPosted());
    								// first = false;

    							}

    							mdetails.setOrderBy(ORDER_BY);
    							mdetails.setPosted(arInvoice.getInvPosted());
    							double TOTAL_GROSS_AMNT = 0d;
    							double TOTAL_NET_AMNT = 0d;
    							double TOTAL_TAX_AMNT = 0d;
    							double TOTAL_DISC_AMNT = 0d;

    							//compute net amount, tax amount and discount amount
    							if(!arInvoice.getArInvoiceLineItems().isEmpty()) {

    								Iterator j = arInvoice.getArInvoiceLineItems().iterator();

    								while(j.hasNext()) {

    									LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)j.next();

    									
    									TOTAL_NET_AMNT += arInvoiceLineItem.getIliAmount();
    									TOTAL_TAX_AMNT += arInvoiceLineItem.getIliTaxAmount();
    									TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;
    									TOTAL_DISC_AMNT += arInvoiceLineItem.getIliTotalDiscount();

    								}

    							} else if(!arInvoice.getArInvoiceLines().isEmpty()) {

    								Iterator j = arInvoice.getArInvoiceLines().iterator();

    								while(j.hasNext()) {

    									LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)j.next();
    									//arInvoiceLine.getArStandardMemoLine().getSmlName();
    									mdetails.setSrQuantity(arInvoiceLine.getIlQuantity());
    									TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
    									TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();
    									TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;
    									System.out.println("arInvoiceLine.getIlQuantity()8: " + arInvoiceLine.getIlQuantity());
    								}

    							} else if(!arInvoice.getArSalesOrderInvoiceLines().isEmpty()) {

    								Iterator j = arInvoice.getArSalesOrderInvoiceLines().iterator();

    								while(j.hasNext()) {

    									LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)j.next();
    									String cst = arInvoice.getArCustomer().getCstCustomerCode();
    									TOTAL_NET_AMNT += arSalesOrderInvoiceLine.getSilAmount();
    									TOTAL_TAX_AMNT += arSalesOrderInvoiceLine.getSilTaxAmount();
    									TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;
    									TOTAL_DISC_AMNT += arSalesOrderInvoiceLine.getSilTotalDiscount();
    								}

    							} else if(!arInvoice.getArJobOrderInvoiceLines().isEmpty()) {

    								Iterator j = arInvoice.getArJobOrderInvoiceLines().iterator();

    								while(j.hasNext()) {

    									LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine)j.next();
    									String cst = arInvoice.getArCustomer().getCstCustomerCode();
    									TOTAL_NET_AMNT += arJobOrderInvoiceLine.getJilAmount();
    									TOTAL_TAX_AMNT += arJobOrderInvoiceLine.getJilTaxAmount();
    									TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;
    									TOTAL_DISC_AMNT += arJobOrderInvoiceLine.getJilTotalDiscount();
    								}

    							} else if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE && arInvoice.getArInvoiceLineItems().isEmpty()) {

    								try {

    									arDistributionRecord = arDistributionRecordHome.findByDrClassAndInvCode("TAX", arInvoice.getInvCode(), AD_CMPNY);	

    									TOTAL_TAX_AMNT = arDistributionRecord.getDrAmount();
    									TOTAL_NET_AMNT = arInvoice.getInvAmountDue() - TOTAL_TAX_AMNT;
    									
    									
    								} catch(FinderException ex) {

    									TOTAL_TAX_AMNT = 0;
    									TOTAL_NET_AMNT = arInvoice.getInvAmountDue();

    								}

    							}

    							if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {

    								if(TOTAL_NET_AMNT != 0) {

    									TOTAL_NET_AMNT = TOTAL_NET_AMNT * -1;

    								}

    								if(TOTAL_TAX_AMNT != 0) {

    									TOTAL_TAX_AMNT = TOTAL_TAX_AMNT * -1;

    								}

    							}

    							mdetails.setSrGrossAmount(TOTAL_GROSS_AMNT);
    							mdetails.setSrNetAmount(TOTAL_NET_AMNT);
    							mdetails.setSrTaxAmount(TOTAL_TAX_AMNT);
    							mdetails.setPosted(arInvoice.getInvPosted());
    							
    							if(first) {

    								mdetails.setSrDiscountAmount(TOTAL_DISC_AMNT);
    								first = false;

    							}

    							if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    								if(arInvoice.getArSalesperson() != null) {
    									
    									
    									
                                    	if (criteria.containsKey("salesperson") ) {

				    						String slsprsn = (String)criteria.get("salesperson");
				    						
				    						if(!slsprsn.equals(arInvoice.getArSalesperson().getSlpName())) {
				    							continue;
				    						}
				    						
				    	    			

				    	    			}
                                    	
    									mdetails.setSrSlsSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
    									mdetails.setSrSlsName(arInvoice.getArSalesperson().getSlpName());
    	    							mdetails.setPosted(arInvoice.getInvPosted());

    								} else {
    									
    									if (criteria.containsKey("salesperson") ) {
    										continue;
    									}
    									
    									mdetails.setSrSlsSalespersonCode("");
    									mdetails.setSrSlsName("");
    									mdetails.setPosted(arInvoice.getInvPosted());
    								}

    							} else {

    								try {

    									LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    									if(arCreditedInvoice.getArSalesperson() != null) {

    										if (criteria.containsKey("salesperson") ) {

    				    						String slsprsn = (String)criteria.get("salesperson");
    				    						
    				    						if(!slsprsn.equals(arCreditedInvoice.getArSalesperson().getSlpName())) {
    				    							continue;
    				    						}
    				    						
    				    	    			

    				    	    			}
                                        	
    										
    										mdetails.setSrSlsSalespersonCode(arCreditedInvoice.getArSalesperson().getSlpSalespersonCode());
    										mdetails.setSrSlsName(arCreditedInvoice.getArSalesperson().getSlpName());
    		    							mdetails.setPosted(arInvoice.getInvPosted());

    									} else {
    										
    										if (criteria.containsKey("salesperson") ) {
    											continue;
    										}
    										mdetails.setSrSlsSalespersonCode("");
    										mdetails.setSrSlsName("");
    										mdetails.setPosted(arInvoice.getInvPosted());
    									}

    								} catch(FinderException ex) {

    								}

    							}

    							//set distribution record details
    							mdetails.setSrDrGlAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
    							mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

    							if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    								mdetails.setSrDrDebitAmount(arDistributionRecord.getDrAmount());
	    							mdetails.setPosted(arInvoice.getInvPosted());

    							} else {

    								mdetails.setSrDrCreditAmount(arDistributionRecord.getDrAmount());
	    							mdetails.setPosted(arInvoice.getInvPosted());

    							}

    							mdetails.setSrRevenueAmount(REVENUE_AMOUNT);
    							list.add(mdetails);

    						}

    					} else {

    						ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    						mdetails.setSrDate(arInvoice.getInvDate());
    						mdetails.setSrInvoiceNumber(arInvoice.getInvNumber());
    						mdetails.setSrReferenceNumber(arInvoice.getInvReferenceNumber());
    						refNum=arInvoice.getInvReferenceNumber();
    						mdetails.setSrDescription(arInvoice.getInvDescription());		  	  
    						mdetails.setSrCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode() + "-" + arInvoice.getArCustomer().getCstName());		
    						mdetails.setSrCstCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());
    						mdetails.setSrCstName(arInvoice.getArCustomer().getCstName());
    						mdetails.setSrCstCustomerCode2(arInvoice.getArCustomer().getCstCustomerCode());
							mdetails.setPosted(arInvoice.getInvPosted());
							arDistributionRecords = arInvoice.getArDistributionRecords();
							
							Iterator x = arDistributionRecords.iterator();
							
							while (x.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
								
								if (AD_CMPNY==15) {
									if (arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
									}
								} else {
									if ((arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") && (arDistributionRecord.getGlChartOfAccount().getCoaCode()!=5971 && arDistributionRecord.getGlChartOfAccount().getCoaCode()!=6259)) || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
									}
								}
							}
    						// type
    						if(arInvoice.getArCustomer().getArCustomerType() == null) {

    							mdetails.setSrCstCustomerType("UNDEFINE");
    							mdetails.setPosted(arInvoice.getInvPosted());

    						} else {

    							mdetails.setSrCstCustomerType(arInvoice.getArCustomer().getArCustomerType().getCtName());
    							mdetails.setPosted(arInvoice.getInvPosted());

    						}

    						double AMNT_DUE = 0d;

    						if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    							AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    									arInvoice.getGlFunctionalCurrency().getFcCode(),
    									arInvoice.getGlFunctionalCurrency().getFcName(),
    									arInvoice.getInvConversionDate(),
    									arInvoice.getInvConversionRate(),
    									arInvoice.getInvAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    						} else {

    							LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    							AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    									arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
    									arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
    									arCreditedInvoice.getInvConversionDate(),
    									arCreditedInvoice.getInvConversionRate(),
    									arInvoice.getInvAmountDue(), AD_CMPNY) * -1, adCompany.getGlFunctionalCurrency().getFcPrecision());

    						}

    						mdetails.setSrAmount(AMNT_DUE);
    						mdetails.setOrderBy(ORDER_BY);
							mdetails.setPosted(arInvoice.getInvPosted());
							
							
							double TOTAL_GROSS_AMNT = 0d;
    						double TOTAL_NET_AMNT = 0d;
    						double TOTAL_TAX_AMNT = 0d;
    						double TOTAL_DISC_AMNT = 0d;

    						//compute net amount, tax amount and discount amount
    						if(!arInvoice.getArInvoiceLineItems().isEmpty()) {

    							Iterator j = arInvoice.getArInvoiceLineItems().iterator();

    							while(j.hasNext()) {

    								LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)j.next();

    								TOTAL_NET_AMNT += arInvoiceLineItem.getIliAmount();
    								TOTAL_TAX_AMNT += arInvoiceLineItem.getIliTaxAmount();
    								TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;
    								TOTAL_DISC_AMNT += arInvoiceLineItem.getIliTotalDiscount();

    							}

    						} else if(!arInvoice.getArInvoiceLines().isEmpty()) {

    							Iterator j = arInvoice.getArInvoiceLines().iterator();

    							while(j.hasNext()) {

    								LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)j.next();
    								mdetails.setSrQuantity(arInvoiceLine.getIlQuantity());
    								TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
    								TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();
    								TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;
    								System.out.println("arInvoiceLine.getIlQuantity()9 " + arInvoiceLine.getIlQuantity());
    							}

    						} else if(!arInvoice.getArSalesOrderInvoiceLines().isEmpty()) {

    							Iterator j = arInvoice.getArSalesOrderInvoiceLines().iterator();

    							while(j.hasNext()) {

    								LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)j.next();

    								TOTAL_NET_AMNT += arSalesOrderInvoiceLine.getSilAmount();
    								TOTAL_TAX_AMNT += arSalesOrderInvoiceLine.getSilTaxAmount();
    								TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;
    								TOTAL_DISC_AMNT += arSalesOrderInvoiceLine.getSilTotalDiscount();
    								
    							}

    						}  else if(!arInvoice.getArJobOrderInvoiceLines().isEmpty()) {

    							Iterator j = arInvoice.getArJobOrderInvoiceLines().iterator();

    							while(j.hasNext()) {

    								LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine)j.next();

    								TOTAL_NET_AMNT += arJobOrderInvoiceLine.getJilAmount();
    								TOTAL_TAX_AMNT += arJobOrderInvoiceLine.getJilTaxAmount();
    								TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;
    								TOTAL_DISC_AMNT += arJobOrderInvoiceLine.getJilTotalDiscount();
    								
    							}

    						} else if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE && arInvoice.getArInvoiceLineItems().isEmpty()) {

    							try {

    								LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.findByDrClassAndInvCode("TAX", arInvoice.getInvCode(), AD_CMPNY);	

    								TOTAL_TAX_AMNT = arDistributionRecord.getDrAmount();
    								TOTAL_NET_AMNT = arInvoice.getInvAmountDue() - TOTAL_TAX_AMNT;
    								

    							} catch(FinderException ex) {

    								TOTAL_TAX_AMNT = 0;
    								TOTAL_NET_AMNT = arInvoice.getInvAmountDue();

    							}

    						}

    						if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {

    							mdetails.setSrInvCreditMemo(true);
    							mdetails.setPosted(arInvoice.getInvPosted());

    						} else {

    							mdetails.setSrInvCreditMemo(false);
    							mdetails.setPosted(arInvoice.getInvPosted());

    						}

    						mdetails.setSrGrossAmount(TOTAL_GROSS_AMNT);
    						mdetails.setSrNetAmount(TOTAL_NET_AMNT);
    						mdetails.setSrTaxAmount(TOTAL_TAX_AMNT);
    						mdetails.setSrDiscountAmount(TOTAL_DISC_AMNT);
							mdetails.setPosted(arInvoice.getInvPosted());
							
    						if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

    							if(arInvoice.getArSalesperson() != null) {

    								
    								if (criteria.containsKey("salesperson") ) {

			    						String slsprsn = (String)criteria.get("salesperson");
			    						
			    						if(!slsprsn.equals(arInvoice.getArSalesperson().getSlpName())) {
			    							continue;
			    						}
			    						
			    	    			

			    	    			}
                                	
    								
    								
    								mdetails.setSrSlsSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
    								mdetails.setSrSlsName(arInvoice.getArSalesperson().getSlpName());
	    							mdetails.setPosted(arInvoice.getInvPosted());

    							} else {
    								
    								if (criteria.containsKey("salesperson") ) {
    									continue;
    								}
    								
    								mdetails.setSrSlsSalespersonCode("");
    								mdetails.setSrSlsName("");
    								mdetails.setPosted(arInvoice.getInvPosted());
    							}

    						} else {

    							try {

    								LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

    								if(arCreditedInvoice.getArSalesperson() != null) {

    									if (criteria.containsKey("salesperson") ) {

    			    						String slsprsn = (String)criteria.get("salesperson");
    			    						
    			    						if(!slsprsn.equals(arCreditedInvoice.getArSalesperson().getSlpName())) {
    			    							continue;
    			    						}
    			    						
    			    	    			

    			    	    			}
                                    	
    									
    									
    									mdetails.setSrSlsSalespersonCode(arCreditedInvoice.getArSalesperson().getSlpSalespersonCode());
    									mdetails.setSrSlsName(arCreditedInvoice.getArSalesperson().getSlpName());
    	    							mdetails.setPosted(arCreditedInvoice.getInvPosted());

    								} else {
    									
    									if (criteria.containsKey("salesperson") ) {
    										continue;
    									}
    									mdetails.setSrSlsSalespersonCode("");
    									mdetails.setSrSlsName("");
    									mdetails.setPosted(arCreditedInvoice.getInvPosted());
    								}

    							} catch(FinderException ex) {

    							}

    						}

    						if(arInvoice.getInvCreditMemo() == EJBCommon.TRUE) {
    							mdetails.setCmInvoiceNumber(arInvoice.getInvCmInvoiceNumber());	
    							GRAND_TOTAL_NET_AMNT -= TOTAL_NET_AMNT;
    							GRAND_TOTAL_TAX_AMNT -= TOTAL_TAX_AMNT;
    							GRAND_TOTAL_AMNT -= (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);
    							GRAND_TOTAL_GROSS_AMNT -= TOTAL_GROSS_AMNT;

    						} else {

    							GRAND_TOTAL_NET_AMNT += TOTAL_NET_AMNT;
    							GRAND_TOTAL_TAX_AMNT += TOTAL_TAX_AMNT;
    							GRAND_TOTAL_AMNT += (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);
    							GRAND_TOTAL_GROSS_AMNT += TOTAL_GROSS_AMNT;
    						}

    						mdetails.setSrRctType("INVOICE");
    						
    						// get receipt info
    						
    						String invoiceReceiptNumbers = null;
    						String invoiceReceiptDates = null;
    						
    						Collection arInvoiceReceipts = arReceiptHome.findByInvCodeAndRctVoid(arInvoice.getInvCode(), EJBCommon.FALSE, AD_CMPNY);
    						
    						Iterator j = arInvoiceReceipts.iterator();
    						
    						while (j.hasNext()) {
    							
    							LocalArReceipt arReceipt = (LocalArReceipt)j.next();
    							
    							if (invoiceReceiptNumbers == null) {
    								invoiceReceiptNumbers = arReceipt.getRctNumber();
    							} else if (invoiceReceiptNumbers != null && !invoiceReceiptNumbers.contains(arReceipt.getRctNumber())){
    								invoiceReceiptNumbers += "/" + arReceipt.getRctNumber();
    							}
    							
    							if (invoiceReceiptDates == null) {
    								invoiceReceiptDates = EJBCommon.convertSQLDateToString(arReceipt.getRctDate());
    							} else if (invoiceReceiptDates != null && !invoiceReceiptDates.contains(EJBCommon.convertSQLDateToString(arReceipt.getRctDate()))){
    								invoiceReceiptDates += "/" + EJBCommon.convertSQLDateToString(arReceipt.getRctDate());
    							}
    							
    						}
    						
    						mdetails.setSrInvReceiptNumbers(invoiceReceiptNumbers);
    						mdetails.setSrInvReceiptDates(invoiceReceiptDates);
    						mdetails.setSrRevenueAmount(REVENUE_AMOUNT);
    						list = getPriorDues(arInvoice, mdetails, criteria, list);

    					}
    				}
    			}
    		}

    		Collection arReceipts = null;
    		String miscReceipts = null;

    		if(((String)criteria.get("includedMiscReceipts")).equals("YES")) {

    			// misc receipt

    			jbossQl = new StringBuffer();
    			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct WHERE (");

    			brIter = branchList.iterator();

    			details = (AdBranchDetails)brIter.next();
    			jbossQl.append("rct.rctAdBranch=" + details.getBrCode());

    			while(brIter.hasNext()) {

    				details = (AdBranchDetails)brIter.next();

    				jbossQl.append(" OR rct.rctAdBranch=" + details.getBrCode());

    			}

    			jbossQl.append(") ");
    			firstArgument = false;

    			ctr = 0;

    			if (criteria.containsKey("invoiceNumberFrom")) {

    				criteriaSize--;

    			}

    			if (criteria.containsKey("invoiceNumberTo")) {

    				criteriaSize--;

    			}
    			/*
    			if (criteria.containsKey("salesperson")) {

    				criteriaSize--;

    			}
    			*/
    			if(criteria.containsKey("invoiceBatchName"))
    				obj = new Object[criteriaSize-1];
    			else
    				obj = new Object[criteriaSize];

    			if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.cstCustomerCode = '" + (String)criteria.get("customerCode") + "' ");

    			}
    			
    			if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}

/*    			if (criteria.containsKey("receiptBatchName")) {
    				
    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("rct.arReceiptBatch.rbName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("receiptBatchName");
    				ctr++;
    				
    			}
*/
    			if (criteria.containsKey("customerType")) {

    				if (!firstArgument) {		

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerType");
    				ctr++;

    			}	

    			if (criteria.containsKey("customerClass")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerClass");
    				ctr++;

    			}		
    			
    			/*
    			
    			if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("rct.arSalesperson.slpName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("salesperson");
    				ctr++;

    			}
    			
    			*/
    			
    			if (criteria.containsKey("dateFrom")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}
    				jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateFrom");
    				ctr++;
    			}  

    			if (criteria.containsKey("dateTo")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}
    				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateTo");
    				ctr++;

    			}
    			
    			boolean unpostedOnly=false;
    			if (criteria.containsKey("unpostedOnly")) {

    				String unposted = (String)criteria.get("unpostedOnly");

    				if (unposted.equals("YES")) {
    					if (!firstArgument) {

    						jbossQl.append("AND ");

    					} else {

    						firstArgument = false;
    						jbossQl.append("WHERE ");

    					}	       	  

    					unpostedOnly = true;
    					jbossQl.append("rct.rctPosted = 0 AND rct.rctVoid = 0 ");

    				} 
    			}
    			
    			if (criteria.containsKey("includedUnposted") && unpostedOnly==false) {

    				String unposted = (String)criteria.get("includedUnposted");

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}	       	  
    				if (unposted == null) unposted = "NO";
    				if (unposted.equals("NO")) {

    					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
    				
    				} else {

    					jbossQl.append("rct.rctVoid = 0 ");  	  

    				}   	


    			}

    			if (criteria.containsKey("region")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("rct.arCustomer.cstAdLvRegion=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("region");
    				ctr++;

    			}

    			if (!firstArgument) {

    				jbossQl.append("AND ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}

    			jbossQl.append("rct.rctType='MISC' AND rct.rctAdCompany=" + AD_CMPNY + " ");
    			
  	       	  String orderBy = null;
	          
	          if (GROUP_BY.equals("SALESPERSON") || GROUP_BY.equals("PRODUCT")) {
	  	      	 
	  	      	  orderBy = "rct.arCustomer.cstCustomerCode";

	          } 

	  		  if (orderBy != null) {
	  		  
	  		  	jbossQl.append("ORDER BY " + orderBy);
	  		  	
	  		  }  
    			

    		      
    			System.out.println("jbossQl.toString():7 "+jbossQl.toString());
    			System.out.println("unposted Only: 7" + criteria.containsKey("unpostedOnly"));
    			arReceipts = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);
    			
    			if(!arReceipts.isEmpty()) {

    				Iterator i = arReceipts.iterator();

    				while(i.hasNext()) {

    					LocalArReceipt arReceipt = (LocalArReceipt)i.next();   	  

    					double REVENU_AMOUNT = 0d;
    					
    					Collection arDistributionRecords = arReceipt.getArDistributionRecords();
						
						Iterator iDr = arDistributionRecords.iterator();
						
							while (iDr.hasNext()) {
							
							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) iDr.next();
							
							if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
								
								if(arDistributionRecord.getDrDebit()==EJBCommon.TRUE) {
									REVENU_AMOUNT -= arDistributionRecord.getDrAmount();
								}else {
									REVENU_AMOUNT += arDistributionRecord.getDrAmount();
								}
								
							
							
							}
								
						}
    					
						if (PRINT_SALES_RELATED_ONLY) {
							
							boolean isSalesRelated = false;
							
							arDistributionRecords = arReceipt.getArDistributionRecords();
							
							Iterator x = arDistributionRecords.iterator();
							
							while (x.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
								
								if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
									
									isSalesRelated = true;
									break;
								
							}
									
							}
							
							if (!isSalesRelated)
									
								continue;
						}
						
    					if(SHOW_ENTRIES) {

    						boolean first = true;

    						arDistributionRecords = arReceipt.getArDistributionRecords();

    						Iterator j = arDistributionRecords.iterator();

    						while(j.hasNext()) {

    							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

    							ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    							mdetails.setSrDate(arReceipt.getRctDate());
    							mdetails.setSrInvoiceNumber(arReceipt.getRctNumber());
    							mdetails.setSrReferenceNumber(arReceipt.getRctReferenceNumber());
    							mdetails.setSrDescription(arReceipt.getRctDescription());		  	  
    							mdetails.setSrCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode() + "-" + 
    									(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName()));		
    							mdetails.setSrCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
    							mdetails.setSrCstName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
    							mdetails.setSrCstCustomerCode2(arReceipt.getArCustomer().getCstCustomerCode());
    							mdetails.setPosted(arReceipt.getRctPosted());
        						// type
    							if(arReceipt.getArCustomer().getArCustomerType() == null) {

    								mdetails.setSrCstCustomerType("UNDEFINE");
        							mdetails.setPosted(arReceipt.getRctPosted());

    							} else {

    								mdetails.setSrCstCustomerType(arReceipt.getArCustomer().getArCustomerType().getCtName());
        							mdetails.setPosted(arReceipt.getRctPosted());
        							

    							}

    							if(first) {

    								double AMNT_DUE = 0d;

    								AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    										arReceipt.getGlFunctionalCurrency().getFcCode(),
    										arReceipt.getGlFunctionalCurrency().getFcName(),
    										arReceipt.getRctConversionDate(),
    										arReceipt.getRctConversionRate(),
    										arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    								mdetails.setSrAmount(AMNT_DUE);

    								first = false;

    							}

    							mdetails.setOrderBy(ORDER_BY);
    							mdetails.setPosted(arReceipt.getRctPosted());
    							//set distribution record details
    							mdetails.setSrDrGlAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
    							mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());

    							if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    								mdetails.setSrDrDebitAmount(arDistributionRecord.getDrAmount());
        							mdetails.setPosted(arReceipt.getRctPosted());
    							} else {

    								mdetails.setSrDrCreditAmount(arDistributionRecord.getDrAmount());
        							mdetails.setPosted(arReceipt.getRctPosted());
    							}

    							mdetails.setSrRevenueAmount(REVENU_AMOUNT);
    							list.add(mdetails);

    						}

    					} else {

    						ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    						mdetails.setSrDate(arReceipt.getRctDate());
    						mdetails.setSrInvoiceNumber(arReceipt.getRctNumber());
    						mdetails.setSrReferenceNumber(arReceipt.getRctReferenceNumber());
    						mdetails.setSrDescription(arReceipt.getRctDescription());		  	  
    						mdetails.setSrCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode() + "-" + 
    								(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName()));		
    						mdetails.setSrCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
    						mdetails.setSrCstName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
    						mdetails.setSrCstCustomerCode2(arReceipt.getArCustomer().getCstCustomerCode());
							mdetails.setPosted(arReceipt.getRctPosted());
							
							arDistributionRecords = arReceipt.getArDistributionRecords();
							
							Iterator x = arDistributionRecords.iterator();
							
							while (x.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
								
								if (AD_CMPNY==15) {
									if (arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
									}
								} else {
									if ((arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") && (arDistributionRecord.getGlChartOfAccount().getCoaCode()!=5971 && arDistributionRecord.getGlChartOfAccount().getCoaCode()!=6259)) || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
										mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
									}
								}
							}
    						// type
    						if(arReceipt.getArCustomer().getArCustomerType() == null) {

    							mdetails.setSrCstCustomerType("UNDEFINE");
    							mdetails.setPosted(arReceipt.getRctPosted());
    						} else {

    							mdetails.setSrCstCustomerType(arReceipt.getArCustomer().getArCustomerType().getCtName());
    							mdetails.setPosted(arReceipt.getRctPosted());
    						}

    						boolean isCustDeposit = (arReceipt.getRctCustomerDeposit() == 1 ? true : false);
    						
    						double AMNT_DUE = 0d;

    						AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    								arReceipt.getGlFunctionalCurrency().getFcCode(),
    								arReceipt.getGlFunctionalCurrency().getFcName(),
    								arReceipt.getRctConversionDate(),
    								arReceipt.getRctConversionRate(),
    								arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    						
    						//added to remove negative if negative
    						
    						if((AMNT_DUE * -1)< 0 ) {
    							mdetails.setSrAmount(AMNT_DUE );
    						}else {
    							mdetails.setSrAmount(AMNT_DUE * -1 );
    						}
    						
    				
    						mdetails.setOrderBy(ORDER_BY);
							mdetails.setPosted(arReceipt.getRctPosted());
    						double TOTAL_NET_AMNT = 0d;
    						double TOTAL_TAX_AMNT = 0d;
    						double TOTAL_GROSS_AMNT = 0d;
    						String smlName = null;

    						if(!arReceipt.getArInvoiceLineItems().isEmpty()) {

    							Iterator k = arReceipt.getArInvoiceLineItems().iterator();

    							while(k.hasNext()) {

    								LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)k.next();

    								TOTAL_NET_AMNT += arInvoiceLineItem.getIliAmount();
    								TOTAL_TAX_AMNT += arInvoiceLineItem.getIliTaxAmount();
    								TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;

    							}

    						} else if(!arReceipt.getArInvoiceLines().isEmpty()) {

    							Iterator k = arReceipt.getArInvoiceLines().iterator();

    							while(k.hasNext()) {

    								LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)k.next();
    								mdetails.setSrQuantity(arInvoiceLine.getIlQuantity());
    								TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
    								TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();
    								TOTAL_GROSS_AMNT += TOTAL_NET_AMNT - TOTAL_TAX_AMNT;
    								System.out.println("arInvoiceLine.getIlQuantity()10: " + arInvoiceLine.getIlQuantity());
    								System.out.println("SML NAME="+arInvoiceLine.getArStandardMemoLine().getSmlName());
    								smlName = arInvoiceLine.getArStandardMemoLine().getSmlName();
    							}

    						}

    						mdetails.setSrMemoName(smlName);
    						mdetails.setSrGrossAmount(TOTAL_GROSS_AMNT);
    						mdetails.setSrNetAmount(isCustDeposit ? (TOTAL_NET_AMNT * -1): TOTAL_NET_AMNT);
    						mdetails.setSrTaxAmount(isCustDeposit ? (TOTAL_TAX_AMNT * -1): TOTAL_TAX_AMNT);
							mdetails.setPosted(arReceipt.getRctPosted());
    						if(arReceipt.getArSalesperson() != null) {

								if (criteria.containsKey("salesperson") ) {
								
									String slsprsn = (String)criteria.get("salesperson");
									
									if(!slsprsn.equals(arReceipt.getArSalesperson().getSlpName())) {
										continue;
									}
									
								
								
								}


    							mdetails.setSrSlsSalespersonCode(arReceipt.getArSalesperson().getSlpSalespersonCode());
    							mdetails.setSrSlsName(arReceipt.getArSalesperson().getSlpName());
    							mdetails.setPosted(arReceipt.getRctPosted());
    						} else {
    							
    							if (criteria.containsKey("salesperson") ) {
    								continue;
    							}
    							mdetails.setSrSlsSalespersonCode("");
    							mdetails.setSrSlsName("");
    							mdetails.setPosted(arReceipt.getRctPosted());
    						}

    						if(isCustDeposit){
    							GRAND_TOTAL_NET_AMNT -= TOTAL_NET_AMNT;
    							GRAND_TOTAL_TAX_AMNT -= TOTAL_TAX_AMNT;
    							GRAND_TOTAL_AMNT -= (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);
    							GRAND_TOTAL_GROSS_AMNT -= TOTAL_GROSS_AMNT;
    						}else{
    							GRAND_TOTAL_NET_AMNT += TOTAL_NET_AMNT;
    							GRAND_TOTAL_TAX_AMNT += TOTAL_TAX_AMNT;
    							GRAND_TOTAL_AMNT += (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);
    							GRAND_TOTAL_GROSS_AMNT += TOTAL_GROSS_AMNT;
    						}

    						mdetails.setSrRctType("MISC");
							mdetails.setPosted(arReceipt.getRctPosted());
							mdetails.setSrRevenueAmount(REVENU_AMOUNT);
    						list.add(mdetails);

    					}


    				}

    			}

    		}


    		if(((String)criteria.get("includedCollections")).equals("YES")) {

    			// collections

    			jbossQl = new StringBuffer();
    			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct WHERE (");

    			brIter = branchList.iterator();

    			details = (AdBranchDetails)brIter.next();
    			jbossQl.append("rct.rctAdBranch=" + details.getBrCode());

    			while(brIter.hasNext()) {

    				details = (AdBranchDetails)brIter.next();

    				jbossQl.append(" OR rct.rctAdBranch=" + details.getBrCode());

    			}

    			jbossQl.append(") ");
    			firstArgument = false;

    			ctr = 0;

    			if (criteria.containsKey("invoiceNumberFrom")) {

    				criteriaSize--;

    			}

    			if (criteria.containsKey("invoiceNumberTo")) {

    				criteriaSize--;

    			}
    			/*
    			if (criteria.containsKey("salesperson")) {

    				criteriaSize--;

    			}
    			*/
    			if(criteria.containsKey("invoiceBatchName"))
    				obj = new Object[criteriaSize-1];
    			else
    				obj = new Object[criteriaSize];
    			
    			if (criteria.containsKey("customerCode")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.cstCustomerCode = '" + (String)criteria.get("customerCode") + "' ");

    			}
    			
    			if (criteria.containsKey("customerBatch")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");	

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.cstCustomerBatch LIKE '%" + (String)criteria.get("customerBatch") + "%' ");

    			}
    			
/*    			if (criteria.containsKey("receiptBatchName")) {
    				
    				if (!firstArgument) {
    					jbossQl.append("AND ");
    				} else {
    					firstArgument = false;
    					jbossQl.append("WHERE ");
    				}
    				jbossQl.append("rct.arReceiptBatch.rbName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("receiptBatchName");
    				ctr++;
    				
    			}
*/

    			if (criteria.containsKey("customerType")) {

    				if (!firstArgument) {		

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerType");
    				ctr++;

    			}	

    			if (criteria.containsKey("customerClass")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}

    				jbossQl.append("rct.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("customerClass");
    				ctr++;

    			}			     
    			/*
    			if (criteria.containsKey("salesperson")) {

    				if (!firstArgument) {		       	  	
    					jbossQl.append("AND ");		       	     
    				} else {		       	  	
    					firstArgument = false;
    					jbossQl.append("WHERE ");		       	  	 
    				}

    				jbossQl.append("rct.arSalesperson.slpName=?" + (ctr+1) + " ");
    				obj[ctr] = (String)criteria.get("salesperson");
    				ctr++;

    			}
    			  	*/		
    			if (criteria.containsKey("dateFrom")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}
    				jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateFrom");
    				ctr++;
    			}  

    			if (criteria.containsKey("dateTo")) {

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}
    				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
    				obj[ctr] = (Date)criteria.get("dateTo");
    				ctr++;

    			}

    			boolean unpostedOnly=false;
    			if (criteria.containsKey("unpostedOnly")) {

    				String unposted = (String)criteria.get("unpostedOnly");

    				if (unposted.equals("YES")) {
    					if (!firstArgument) {

    						jbossQl.append("AND ");

    					} else {

    						firstArgument = false;
    						jbossQl.append("WHERE ");

    					}	       	  

    					unpostedOnly = true;
    					jbossQl.append("rct.rctPosted = 0 AND rct.rctVoid = 0 ");

    				} 
    			}
    			
    			if (criteria.containsKey("includedUnposted") && unpostedOnly==false) {

    				String unposted = (String)criteria.get("includedUnposted");

    				if (!firstArgument) {

    					jbossQl.append("AND ");

    				} else {

    					firstArgument = false;
    					jbossQl.append("WHERE ");

    				}	       	  
    				if (unposted == null) unposted = "NO";
    				if (unposted.equals("NO")) {

    					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
    					
    				} else {

    					jbossQl.append("rct.rctVoid = 0 ");  	  

    				}  


    			}

    			if (!firstArgument) {

    				jbossQl.append("AND ");

    			} else {

    				firstArgument = false;
    				jbossQl.append("WHERE ");

    			}

    			jbossQl.append("rct.rctType='COLLECTION' AND rct.rctAdCompany=" + AD_CMPNY + " ");
    			
    	       	  String orderBy = null;
    	          
    	          if (GROUP_BY.equals("SALESPERSON") || GROUP_BY.equals("PRODUCT")) {
    	  	      	 
    	  	      	  orderBy = "rct.arCustomer.cstCustomerCode";

    	          } 

    	  		  if (orderBy != null) {
    	  		  
    	  		  	jbossQl.append("ORDER BY " + orderBy);
    	  		  	
    	  		  }  
    			
  		     	System.out.println("jbossQl.toString()8: "+jbossQl.toString());
    			System.out.println("unposted Only: 8" + criteria.containsKey("unpostedOnly"));
    			arReceipts = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);
    			
    			if(!arReceipts.isEmpty()) {

    				Iterator i = arReceipts.iterator();

    				while(i.hasNext()) {

    					LocalArReceipt arReceipt = (LocalArReceipt)i.next();  
    					
    					double REVENUE_AMOUNT = 0d;
    					
    					Collection arDistributionRecords = arReceipt.getArDistributionRecords();
						
						Iterator iDr = arDistributionRecords.iterator();
						
						while (iDr.hasNext()) {
							
							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) iDr.next();
							
							if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
								
								if(arDistributionRecord.getDrDebit()==EJBCommon.TRUE) {
									REVENUE_AMOUNT -= arDistributionRecord.getDrAmount();
								}else {
									REVENUE_AMOUNT += arDistributionRecord.getDrAmount();
								}
								
							
							}
								
						}
    					

						if (PRINT_SALES_RELATED_ONLY) {
							
							boolean isSalesRelated = false;
							
							arDistributionRecords = arReceipt.getArDistributionRecords();
							
							Iterator x = arDistributionRecords.iterator();
							
							while (x.hasNext()) {
								
								LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();
								
								if (arDistributionRecord.getGlChartOfAccount().getCoaAccountType().equals("REVENUE")){
									
									isSalesRelated = true;
									break;
								
							}
									
							}
							
							if (!isSalesRelated)
									
								continue;
						}
						
    					if(SHOW_ENTRIES) {

    						boolean first = true;

    						arDistributionRecords = arReceipt.getArDistributionRecords();

    						Iterator j = arDistributionRecords.iterator();

    						while(j.hasNext()) {

    							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

    							ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    							mdetails.setSrDate(arReceipt.getRctDate());
    							mdetails.setSrInvoiceNumber(arReceipt.getRctNumber());
    							mdetails.setSrReferenceNumber(arReceipt.getRctReferenceNumber());
    							mdetails.setSrDescription(arReceipt.getRctDescription());		  	  
    							mdetails.setSrCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode() + "-" + 
    									(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName()));		
    							mdetails.setSrCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
    							mdetails.setSrCstName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
    							mdetails.setSrCstCustomerCode2(arReceipt.getArCustomer().getCstCustomerCode());
    							mdetails.setPosted(arReceipt.getRctPosted());
    							// type
    							if(arReceipt.getArCustomer().getArCustomerType() == null) {

    								mdetails.setSrCstCustomerType("UNDEFINE");
        							mdetails.setPosted(arReceipt.getRctPosted());

    							} else {

    								mdetails.setSrCstCustomerType(arReceipt.getArCustomer().getArCustomerType().getCtName());
        							mdetails.setPosted(arReceipt.getRctPosted());
    							}

    							/*if(first) {

   	   	  						double AMNT_DUE = 0d;

   	   	  						AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
   	   	  								arReceipt.getGlFunctionalCurrency().getFcCode(),
										arReceipt.getGlFunctionalCurrency().getFcName(),
										arReceipt.getRctConversionDate(),
										arReceipt.getRctConversionRate(),
										arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

   	   	  						mdetails.setSrAmount(AMNT_DUE);

   	   	  						first = false;

   	   	  					}*/

    							double AMNT_DUE = 0d;

    							AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    									arReceipt.getGlFunctionalCurrency().getFcCode(),
    									arReceipt.getGlFunctionalCurrency().getFcName(),
    									arReceipt.getRctConversionDate(),
    									arReceipt.getRctConversionRate(),
    									arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    							mdetails.setSrAmount(AMNT_DUE);

    							mdetails.setOrderBy(ORDER_BY);

    							mdetails.setPosted(arReceipt.getRctPosted());
    							//set distribution record details
    							mdetails.setSrDrGlAccountNumber(arDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
    							mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
    							

    							if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    								mdetails.setSrDrDebitAmount(arDistributionRecord.getDrAmount());
        							mdetails.setPosted(arReceipt.getRctPosted());

    							} else {

    								mdetails.setSrDrCreditAmount(arDistributionRecord.getDrAmount());
        							mdetails.setPosted(arReceipt.getRctPosted());

    							}

    						}

    					} else {

    						ArRepSalesRegisterDetails mdetails = new ArRepSalesRegisterDetails();
    						mdetails.setSrDate(arReceipt.getRctDate());
    						mdetails.setSrInvoiceNumber(arReceipt.getRctReferenceNumber());
                                                System.out.println("nv num of receipt: " + arReceipt.getRctReferenceNumber());
                                                mdetails.setSrOrNumber(arReceipt==null?"":arReceipt.getRctNumber());
                                             
    						mdetails.setSrReferenceNumber(arReceipt.getRctReferenceNumber());
    						mdetails.setSrDescription(arReceipt.getRctDescription());		  	  
    						mdetails.setSrCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode() + "-" + 
    								(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName()));		
    						mdetails.setSrCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
    						mdetails.setSrCstName(arReceipt.getArCustomer().getCstName());
    						mdetails.setSrCstCustomerCode2(arReceipt.getArCustomer().getCstCustomerCode());
                                                mdetails.setPosted(arReceipt.getRctPosted());

                                                arDistributionRecords = arReceipt.getArDistributionRecords();

                                                Iterator x = arDistributionRecords.iterator();

                                                while (x.hasNext()) {

                                                    LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) x.next();

                                                    if (AD_CMPNY==15) {
                                                            if (arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
                                                                    mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
                                                            }
                                                    } else {
                                                            if ((arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Sales") && (arDistributionRecord.getGlChartOfAccount().getCoaCode()!=5971 && arDistributionRecord.getGlChartOfAccount().getCoaCode()!=6259)) || arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription().contains("Service")) {
                                                                    mdetails.setSrDrGlAccountDescription(arDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
                                                            }
                                                    }
                                                }
							
							
    						// type
    						if(arReceipt.getArCustomer().getArCustomerType() == null) {

    							mdetails.setSrCstCustomerType("UNDEFINE");

    						} else {

    							mdetails.setSrCstCustomerType(arReceipt.getArCustomer().getArCustomerType().getCtName());
    							mdetails.setPosted(arReceipt.getRctPosted());

    						}

    						double AMNT_DUE = 0d;

    						AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
    								arReceipt.getGlFunctionalCurrency().getFcCode(),
    								arReceipt.getGlFunctionalCurrency().getFcName(),
    								arReceipt.getRctConversionDate(),
    								arReceipt.getRctConversionRate(),
    								arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());

    						mdetails.setSrAmount(AMNT_DUE * -1);
                                                
                                                mdetails.setSrOrAmount(AMNT_DUE * -1);
    						mdetails.setOrderBy(ORDER_BY);
							mdetails.setPosted(arReceipt.getRctPosted());
							
    						double TOTAL_NET_AMNT = 0d;
    						double TOTAL_TAX_AMNT = 0d;

    						if(!arReceipt.getArInvoiceLineItems().isEmpty()) {

                                                    Iterator k = arReceipt.getArInvoiceLineItems().iterator();

                                                    while(k.hasNext()) {

                                                            LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)k.next();

                                                            TOTAL_NET_AMNT += arInvoiceLineItem.getIliAmount();
                                                            TOTAL_TAX_AMNT += arInvoiceLineItem.getIliTaxAmount();

                                                    }

    						} else if(!arReceipt.getArInvoiceLines().isEmpty()) {

    							Iterator k = arReceipt.getArInvoiceLines().iterator();

    							while(k.hasNext()) {

    								LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)k.next();
    								mdetails.setSrQuantity(arInvoiceLine.getIlQuantity());
    								TOTAL_NET_AMNT += arInvoiceLine.getIlAmount();
    								TOTAL_TAX_AMNT += arInvoiceLine.getIlTaxAmount();
    								System.out.println("arInvoiceLine.getIlQuantity()11: " + arInvoiceLine.getIlQuantity());
    							}

    						}
                                      
                                              //  mdetails.setSrInvoiceNumber("");    
    						
    						mdetails.setSrNetAmount(TOTAL_NET_AMNT * -1);
    						mdetails.setSrTaxAmount(TOTAL_TAX_AMNT * -1);
    						
    						mdetails.setSrGrossAmount((TOTAL_NET_AMNT * -1) - (TOTAL_TAX_AMNT * -1));
							mdetails.setPosted(arReceipt.getRctPosted());
							
    						if(arReceipt.getArSalesperson() != null) {

    							if (criteria.containsKey("salesperson") ) {
    								
									String slsprsn = (String)criteria.get("salesperson");
									
									if(!slsprsn.equals(arReceipt.getArSalesperson().getSlpName())) {
										continue;
									}
									
								
								
								}
    							
    							
    							mdetails.setSrSlsSalespersonCode(arReceipt.getArSalesperson().getSlpSalespersonCode());
    							mdetails.setSrSlsName(arReceipt.getArSalesperson().getSlpName());
    							mdetails.setPosted(arReceipt.getRctPosted());
    						} else {
    							
    							if (criteria.containsKey("salesperson") ) {
    								continue;
    							}
    							mdetails.setSrSlsSalespersonCode("");
    							mdetails.setSrSlsName("");
    							mdetails.setPosted(arReceipt.getRctPosted());
    						}

    						GRAND_TOTAL_NET_AMNT -= TOTAL_NET_AMNT;
    						GRAND_TOTAL_TAX_AMNT -= TOTAL_TAX_AMNT;
    						GRAND_TOTAL_AMNT -= (TOTAL_NET_AMNT + TOTAL_TAX_AMNT);
    						GRAND_TOTAL_GROSS_AMNT -= TOTAL_NET_AMNT - TOTAL_TAX_AMNT;

    						mdetails.setSrRctType("COLLECTION");
    						
    						double SR_AI_APPLY_AMNT = 0;
    						double SR_AI_CRDTBL_W_TXAMNT = 0;
    						double SR_AI_DSCNT_AMNT = 0;
    						double SR_AI_APPLD_DPST = 0;
    						
    						Iterator iterAI = arReceipt.getArAppliedInvoices().iterator();
							
							while(iterAI.hasNext()){
								
								LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)iterAI.next();
								SR_AI_APPLY_AMNT += arAppliedInvoice.getAiApplyAmount();
								SR_AI_CRDTBL_W_TXAMNT += arAppliedInvoice.getAiCreditableWTax();
								SR_AI_DSCNT_AMNT += arAppliedInvoice.getAiDiscountAmount();
								SR_AI_APPLD_DPST += arAppliedInvoice.getAiAppliedDeposit();
								
							}							
							
    						// Set Apllied Invoice 
    						mdetails.setSrAiApplyAmount(SR_AI_APPLY_AMNT);
    						mdetails.setSrAiCreditableWTax(SR_AI_CRDTBL_W_TXAMNT);
    						mdetails.setSrAiDiscountAmount(SR_AI_DSCNT_AMNT);
    						mdetails.setSrAiAppliedDeposit(SR_AI_APPLD_DPST);
    						mdetails.setSrRevenueAmount(REVENUE_AMOUNT);
    						list.add(mdetails);

    					}


    				}

    			}

    		}



    		if(list.isEmpty() || list.size() == 0) {

    			throw new GlobalNoRecordFoundException();

    		}


    		// sort

    		if(SHOW_ENTRIES) {

    			Collections.sort(list, ArRepSalesRegisterDetails.CoaAccountNumberComparator);

    		}

    		if(GROUP_BY.equals("CUSTOMER CODE")) {

    			Collections.sort(list, ArRepSalesRegisterDetails.CustomerCodeComparator);

    		} else if(GROUP_BY.equals("CUSTOMER TYPE")) {

    			Collections.sort(list, ArRepSalesRegisterDetails.CustomerTypeComparator);

    		} else if(GROUP_BY.equals("CUSTOMER CLASS")) {

    			Collections.sort(list, ArRepSalesRegisterDetails.CustomerClassComparator);

    		} else if(GROUP_BY.equals("SALESPERSON")) {

    			Collections.sort(list, ArRepSalesRegisterDetails.SalespersonComparator);

    		} else if(GROUP_BY.equals("PRODUCT")) {

    			Collections.sort(list, ArRepSalesRegisterDetails.ProductComparator);

    		} else {

    			Collections.sort(list, ArRepSalesRegisterDetails.NoGroupComparator);

    		}
    		
    		if (SUMMARIZE) {

    			Collections.sort(list, ArRepSalesRegisterDetails.CoaAccountNumberComparator);

    		}

    		ArRepSalesRegisterDetails mdetails = (ArRepSalesRegisterDetails)list.get(list.size() - 1);

    		mdetails.setSrInvTotalNetAmount(GRAND_TOTAL_NET_AMNT);
    		mdetails.setSrInvTotalTaxAmount(GRAND_TOTAL_TAX_AMNT);
    		mdetails.setSrInvTotalAmount(GRAND_TOTAL_AMNT);
    		
    		return list;

    	} catch (GlobalNoRecordFoundException ex) {

    		throw ex;

    	} catch (Exception ex) {


    		ex.printStackTrace();
    		throw new EJBException(ex.getMessage());

    	}

    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepSalesRegisterControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}   
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvArRegionAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesRegisterControllerBean getAdLvArRegionAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR REGION", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArSlpAll(Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesRegisterControllerBean getArSlpAll");
		
		LocalArSalespersonHome arSalespersonHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection arSalespersons = arSalespersonHome.findSlpAll(AD_CMPNY);
			
			Iterator i = arSalespersons.iterator();
			
			while (i.hasNext()) {
				
				LocalArSalesperson arSalesperson = (LocalArSalesperson)i.next();
				
				list.add(arSalesperson.getSlpName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArIbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesRegisterControllerBean getArIbAll");
		
		LocalArInvoiceBatchHome arInvoiceBatchHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection arInvoiceBatches = arInvoiceBatchHome.findOpenIbAll(AD_BRNCH, AD_CMPNY);
			
			Iterator i = arInvoiceBatches.iterator();
			
			while (i.hasNext()) {
				
				LocalArInvoiceBatch arInvoiceBatch = (LocalArInvoiceBatch)i.next();
				
				list.add(arInvoiceBatch.getIbName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArRbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("ArRepSalesRegisterControllerBean getArRbAll");
		
		LocalArReceiptBatchHome arReceiptBatchHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection arReceiptBatches = arReceiptBatchHome.findOpenRbAll(AD_BRNCH, AD_CMPNY);
			
			Iterator i = arReceiptBatches.iterator();
			
			while (i.hasNext()) {
				
				LocalArReceiptBatch arReceiptBatch = (LocalArReceiptBatch)i.next();
				
				list.add(arReceiptBatch.getRbName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ArRepSalesRegisterControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
	
	private ArrayList getPriorDues(LocalArInvoice arInvoice, ArRepSalesRegisterDetails mdetails, HashMap criteria, ArrayList list) {
		ArrayList newList = new ArrayList();
		
		if (arInvoice.getInvAmountDue() == arInvoice.getInvAmountPaid()) {
				
				Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();
				
				if(!arInvoicePaymentSchedules.isEmpty()) {
				    
				    	Iterator j = arInvoicePaymentSchedules.iterator();
			  		
			  		while (j.hasNext()) {
			  			
			  			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)j.next();
			  			Collection arAppliedInvoices = arInvoicePaymentSchedule.getArAppliedInvoices();
			  			
			  			if(!arAppliedInvoices.isEmpty()) {
  			  		
			  				Iterator k = arAppliedInvoices.iterator();
  			  		
			  				while (k.hasNext()) {
			  					
			  					LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)k.next();
			  					
			  					if (!(arAppliedInvoice.getArReceipt().getRctDate().before((Date)criteria.get("dateFrom")))) {
			  							  					    	
			  						list.add(mdetails);
			  						
			  					}
			  				}
			  			} 
			  		}
				}	
			} else {
			    	
				double newInvoiceAmount = arInvoice.getInvAmountDue() - arInvoice.getInvAmountPaid();
				
				Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();
				
				if(!arInvoicePaymentSchedules.isEmpty()) {
			  		
			  		Iterator j = arInvoicePaymentSchedules.iterator();
			  		
			  		while (j.hasNext()) {
			  			
			  			LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)j.next(); 
			  			Collection arAppliedInvoices = arInvoicePaymentSchedule.getArAppliedInvoices();
			  			
			  			if(!arAppliedInvoices.isEmpty()) {
  			  		
			  				Iterator k = arAppliedInvoices.iterator();
  			  		
			  				while (k.hasNext()) {
			  					
			  					LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)k.next();
			  					
			  					if (!(arAppliedInvoice.getArReceipt().getRctDate().before((Date)criteria.get("dateFrom")))) {
			  							
			  						newInvoiceAmount += arAppliedInvoice.getArReceipt().getRctAmount();
			  						mdetails.setSrAmount(newInvoiceAmount);
			  						list.add(mdetails);
			  						
			  					} else {
			  						
			  						mdetails.setSrAmount(newInvoiceAmount);
			  						list.add(mdetails);
			  						
			  					}
			  				}
			  			} else {
			  				mdetails.setSrAmount(newInvoiceAmount);
			  				list.add(mdetails);
			  			}
			  		}
				}	
			}
		
		newList = list;
		
		return newList;
	}
    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepSalesRegisterControllerBean ejbCreate");
      
    }
}