
/*
 * ArRepSalespersonControllerBean.java
 *
 * Created on February 6, 2006, 4:34 PM
 *
 * @author  Franco Antonio R. Roig
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdBranchStandardMemoLine;
import com.ejb.ad.LocalAdBranchStandardMemoLineHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepSalespersonDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepSalespersonControllerEJB"
 *           display-name="Used for viewing salespersons"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepSalespersonControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepSalespersonController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepSalespersonControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepSalespersonControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCcAll(Integer AD_CMPNY) {
                    
        Debug.print("ArRepSalespersonControllerBean getArCcAll");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalArCustomerClass arCustomerClass = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);
	        
	        Iterator i = arCustomerClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerClass = (LocalArCustomerClass)i.next();
	        	
	        	list.add(arCustomerClass.getCcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCtAll(Integer AD_CMPNY) {
                    
        Debug.print("ArCustomerEntryControllerBean getArCtAll");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalArCustomerType arCustomerType = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

	        Iterator i = arCustomerTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerType = (LocalArCustomerType)i.next();
	        	
	        	list.add(arCustomerType.getCtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArCustomerEntryControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepSalesperson(HashMap criteria, ArrayList branchList, String GROUP_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepSalespersonControllerBean executeArRepSalesperson");
        
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArReceiptHome arReceiptHome = null;
        LocalAdCompanyHome adCompanyHome = null;
		LocalArInvoiceLineHome arInvoiceLineHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			arInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 

		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           
		  boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	  
		  String PYMNT_STTS = null;
		  
	      StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv WHERE (");
		  
		  if (branchList.isEmpty())
		  	throw new GlobalNoRecordFoundException();

		  Iterator brIter = branchList.iterator();
		  
		  AdBranchDetails details = (AdBranchDetails)brIter.next();
		  jbossQl.append("inv.invAdBranch=" + details.getBrCode());
		  
		  while(brIter.hasNext()) {
		  	
		  		details = (AdBranchDetails)brIter.next();
		  		
		  		jbossQl.append(" OR inv.invAdBranch=" + details.getBrCode());
		  	
		  }
		  
		  jbossQl.append(") AND inv.arSalesperson.slpSalespersonCode IS NOT NULL ");
		  firstArgument = false;
		  
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("salespersonCode")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      if (criteria.containsKey("customerCode")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

          if (criteria.containsKey("includedMiscReceipts")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
          
          if (criteria.containsKey("paymentStatus")) {
	      	
	      	 criteriaSize--;
	      	 PYMNT_STTS = (String)criteria.get("paymentStatus");
	      	 
	      }
	      
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("salespersonCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("inv.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salespersonCode") + "%' ");
		  	 
		  }

		  if (criteria.containsKey("customerCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("inv.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
		  	 
		  }

		  if (criteria.containsKey("customerClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("inv.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerClass");
		   	  ctr++;
	   	  
	      }		
		  
		  if (criteria.containsKey("customerType")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("inv.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerType");
		   	  ctr++;
	   	  
	      }	
          			
	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		  
		  if (criteria.containsKey("invoiceNumberFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("invoiceNumberFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("invoiceNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("inv.invNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("invoiceNumberTo");
		  	 ctr++;
		  	 
		  }
	      
	      if (criteria.containsKey("paymentStatus")) {
	      	
	      	String paymentStatus = (String)criteria.get("paymentStatus");
	      	
	      	if (!firstArgument) {
	      		
	      		jbossQl.append("AND ");
	      		
	      	} else {
	      		
	      		firstArgument = false;
	      		jbossQl.append("WHERE ");
	      		
	      	}
	      	
	      	if (paymentStatus.equals("PAID")) {	      	 
	      		
	      		jbossQl.append("inv.invAmountDue = inv.invAmountPaid ");
	      		
	      	} else if (paymentStatus.equals("UNPAID")) {	         
	      		
	      		jbossQl.append("inv.invAmountDue <> inv.invAmountPaid ");
	      		
	      	}
	      	
	      }
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("inv.invPosted = 1 AND inv.invVoid = 0 AND inv.invAdCompany=" + AD_CMPNY + " ");
       	  
       	  Collection arInvoices = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);
	      
       	  if(!arInvoices.isEmpty()) {
       	  	
       	  	Iterator i = arInvoices.iterator();
       	  	
       	  	while (i.hasNext()) {
       	  		
       	  		LocalArInvoice arInvoice = (LocalArInvoice)i.next();   	  
       	  		
       	  		ArRepSalespersonDetails mdetails = new ArRepSalespersonDetails();
       	  		mdetails.setSlpDate(arInvoice.getInvDate());
       	  		mdetails.setSlpInvoiceNumber(arInvoice.getInvNumber());
       	  		mdetails.setSlpCstCustomerCode(arInvoice.getArCustomer().getCstCustomerCode());	
       	  		mdetails.setSlpCstCustomerName(arInvoice.getArCustomer().getCstName());	
       	  		mdetails.setSlpCstCustomerClass(arInvoice.getArCustomer().getArCustomerClass().getCcName());
       	  		mdetails.setSlpCstCustomerType(arInvoice.getArCustomer().getArCustomerType() != null ? arInvoice.getArCustomer().getArCustomerType().getCtName() : "N/A");
       	  		mdetails.setSlpSubjectToCommission(arInvoice.getInvSubjectToCommission());
       	  		mdetails.setSlpType("INVOICE");
       	  		mdetails.setSlpPrimaryKey(arInvoice.getInvCode());
   	  			mdetails.setSlpSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode());
   	  			mdetails.setSlpSalespersonName(arInvoice.getArSalesperson().getSlpName());
   	  			mdetails.setSlpAdBranch(arInvoice.getInvAdBranch());

   	  			Iterator ilItr = arInvoice.getArInvoiceLines().iterator();
   	  			
   	  			while (ilItr.hasNext()) {
   	  				
   	  				LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)ilItr.next();
   	  				mdetails.saveSlpArInvoiceLines(arInvoiceLine);
   	  				
   	  			}
   	  			
   	  			Iterator iliItr = arInvoice.getArInvoiceLineItems().iterator();
   	  			
   	  			while (iliItr.hasNext()) {
   	  				
   	  				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)iliItr.next();
   	  				mdetails.saveSlpArInvoiceLineItems(arInvoiceLineItem);
   	  				
   	  			}
   	  			
   	  			Iterator soilItr = arInvoice.getArSalesOrderInvoiceLines().iterator();
   	  			
   	  			while (soilItr.hasNext()) {
   	  				
   	  				LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)soilItr.next();
   	  				mdetails.saveSlpArSalesOrderInvoiceLines(arSalesOrderInvoiceLine);
   	  				
   	  			}

       	  		double AMNT_DUE = 0d;
       	  		double AMNT_PD = 0d;
       	  		
       	  		if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
       	  			
       	  			AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
       	  					arInvoice.getGlFunctionalCurrency().getFcCode(),
       	  					arInvoice.getGlFunctionalCurrency().getFcName(),
       	  					arInvoice.getInvConversionDate(),
       	  					arInvoice.getInvConversionRate(),
       	  					arInvoice.getInvAmountDue(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());
       	  			
       	  			AMNT_PD = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
       	  					arInvoice.getGlFunctionalCurrency().getFcCode(),
       	  					arInvoice.getGlFunctionalCurrency().getFcName(),
       	  					arInvoice.getInvConversionDate(),
       	  					arInvoice.getInvConversionRate(),
       	  					arInvoice.getInvAmountPaid(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());
       	  			
       	  		} else {
       	  			
       	  			
       	  			LocalArInvoice arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
       	  			
       	  			AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
       	  					arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
       	  					arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
       	  					arCreditedInvoice.getInvConversionDate(),
       	  					arCreditedInvoice.getInvConversionRate(),
       	  					arInvoice.getInvAmountDue(), AD_CMPNY) * -1, adCompany.getGlFunctionalCurrency().getFcPrecision());
       	  			
       	  			AMNT_PD = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
       	  					arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
       	  					arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
       	  					arCreditedInvoice.getInvConversionDate(),
       	  					arCreditedInvoice.getInvConversionRate(),
       	  					arInvoice.getInvAmountPaid(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());
       	  			
       	  		}	
       	  		
       	  	
       	  		mdetails.setSlpAmount(AMNT_DUE);
       	  		mdetails.setSlpInvcAmountPd(AMNT_PD);
       	  		
       	  		if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
       	  			if (arInvoice.getInvAmountDue() == arInvoice.getInvAmountPaid()) {
       	  				mdetails.setSlpPaymentStatus("PAID");
       	  				Iterator ipsIter = arInvoice.getArInvoicePaymentSchedules().iterator();
       	  				while (ipsIter.hasNext()) {
       	  					
       	  					LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
       	  						(LocalArInvoicePaymentSchedule) ipsIter.next();
       	  					Iterator aiIter = arInvoicePaymentSchedule.getArAppliedInvoices().iterator();
       	  					while (aiIter.hasNext()) {       	  					
	       	  					LocalArAppliedInvoice arAppliedInvoice =
	       	  						(LocalArAppliedInvoice) aiIter.next();	       	  					
	       	  					
	       	  					mdetails.setSlpCollectionDate(arAppliedInvoice.getArReceipt()!=null ? arAppliedInvoice.getArReceipt().getRctDate() :
	       	  						arAppliedInvoice.getArPdc().getPdcDateReceived());
       	  					}
       	  					
       	  				}
       	  				
       	  			} else {
       	  				mdetails.setSlpPaymentStatus("UNPAID");
       	  			}
       	  		} else {
       	  			mdetails.setSlpPaymentStatus("PAID");
       	  		}
       	  			
       	  		
       	  		list.add(mdetails);
       	  		
       	  	}
       	  	
       	  }
       	  
       	  Collection arReceipts = null;
       	  String miscReceipts = null;
       	  
       	  if(criteria.containsKey("includedMiscReceipts")) {
       	  	
       	  	miscReceipts = (String)criteria.get("includedMiscReceipts");
       	  	
       	  	if (miscReceipts.equals("YES")) {
       	  		
       	  		// misc receipt
       	  		
       	  		jbossQl = new StringBuffer();
       	  		jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct WHERE (");
       	  		
       	  		brIter = branchList.iterator();
       	  		
       	  		details = (AdBranchDetails)brIter.next();
       	  		jbossQl.append("rct.rctAdBranch=" + details.getBrCode());
       	  		
       	  		while(brIter.hasNext()) {
       	  			
       	  			details = (AdBranchDetails)brIter.next();
       	  			
       	  			jbossQl.append(" OR rct.rctAdBranch=" + details.getBrCode());
       	  			
       	  		}
       	  		
       	  		jbossQl.append(") AND rct.arSalesperson.slpSalespersonCode IS NOT NULL ");
       	  		firstArgument = false;
       	  		
       	  		ctr = 0;
       	  		
       	  		if (criteria.containsKey("invoiceNumberFrom")) {
       	  			
       	  			criteriaSize--;
       	  			
       	  		}
       	  		
       	  		if (criteria.containsKey("invoiceNumberTo")) {
       	  			
       	  			criteriaSize--;
       	  			
       	  		}
       	  		
       	  		obj = new Object[criteriaSize];
       	  		
       	  		if (criteria.containsKey("customerCode")) {
       	  			
       	  			if (!firstArgument) {
       	  				
       	  				jbossQl.append("AND ");	
       	  				
       	  			} else {
       	  				
       	  				firstArgument = false;
       	  				jbossQl.append("WHERE ");
       	  				
       	  			}
       	  			
       	  			jbossQl.append("rct.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
       	  			
       	  		}
       	  		
       	  		if (criteria.containsKey("salespersonCode")) {
       	  			
       	  			if (!firstArgument) {
       	  				
       	  				jbossQl.append("AND ");	
       	  				
       	  			} else {
       	  				
       	  				firstArgument = false;
       	  				jbossQl.append("WHERE ");
       	  				
       	  			}
       	  			
       	  			jbossQl.append("rct.arSalesperson.slpSalespersonCode LIKE '%" + (String)criteria.get("salespersonCode") + "%' ");
       	  			
       	  		}

       	  		if (criteria.containsKey("customerClass")) {
       	  			
       	  			if (!firstArgument) {
       	  				
       	  				jbossQl.append("AND ");
       	  				
       	  			} else {
       	  				
       	  				firstArgument = false;
       	  				jbossQl.append("WHERE ");
       	  				
       	  			}
       	  			
       	  			jbossQl.append("rct.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
       	  			obj[ctr] = (String)criteria.get("customerClass");
       	  			ctr++;
       	  			
       	  		}			  
       	  		
       	  		if (criteria.containsKey("customerType")) {

       	  			if (!firstArgument) {

       	  				jbossQl.append("AND ");

       	  			} else {

       	  				firstArgument = false;
       	  				jbossQl.append("WHERE ");

       	  			}

       	  			jbossQl.append("rct.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
       	  			obj[ctr] = (String)criteria.get("customerType");
       	  			ctr++;

       	  		}
       	  		
       	  		if (criteria.containsKey("dateFrom")) {
       	  			
       	  			if (!firstArgument) {
       	  				
       	  				jbossQl.append("AND ");
       	  				
       	  			} else {
       	  				
       	  				firstArgument = false;
       	  				jbossQl.append("WHERE ");
       	  				
       	  			}
       	  			jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
       	  			obj[ctr] = (Date)criteria.get("dateFrom");
       	  			ctr++;
       	  		}  
       	  		
       	  		if (criteria.containsKey("dateTo")) {
       	  			
       	  			if (!firstArgument) {
       	  				
       	  				jbossQl.append("AND ");
       	  				
       	  			} else {
       	  				
       	  				firstArgument = false;
       	  				jbossQl.append("WHERE ");
       	  				
       	  			}
       	  			jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
       	  			obj[ctr] = (Date)criteria.get("dateTo");
       	  			ctr++;
       	  			
       	  		}
       	  		
       	  		if (criteria.containsKey("paymentStatus")) {
       	  			
       	  			String paymentStatus = (String)criteria.get("paymentStatus");
       	  			
       	  			if (!firstArgument) {
       	  				
       	  				jbossQl.append("AND ");
       	  				
       	  			} else {
       	  				
       	  				firstArgument = false;
       	  				jbossQl.append("WHERE ");
       	  				
       	  			}
       	  			
       	  			if (paymentStatus.equals("PAID")) {	      	 
       	  				
       	  				jbossQl.append("rct.rctPosted = 1 ");
       	  				
       	  			} else if (paymentStatus.equals("UNPAID")) {	         
       	  				
       	  				jbossQl.append("rct.rctPosted = 0 ");
       	  				
       	  			}
       	  			
       	  		}
       	  		
//       	  		if (criteria.containsKey("paymentStatus")) {
//       	  			
//       	  			String paymentStatus = (String)criteria.get("paymentStatus");
//       	  			
//       	  			
//       	  			if (paymentStatus.equals("PAID")) {
//       	  				
//       	  				if (!firstArgument) {
//       	  					
//       	  					jbossQl.append("AND ");
//       	  					
//       	  				} else {
//       	  					
//       	  					firstArgument = false;
//       	  					jbossQl.append("WHERE ");
//       	  					
//       	  				}
//       	  				
//       	  				jbossQl.append("(rct.rctApprovalStatus = 'N/A' OR rct.rctApprovalStatus = 'APPROVED') ");
//       	  				
//       	  			}
//       	  			
//       	  		}
       	  		
       	  		if (!firstArgument) {
       	  			
       	  			jbossQl.append("AND ");
       	  			
       	  		} else {
       	  			
       	  			firstArgument = false;
       	  			jbossQl.append("WHERE ");
       	  			
       	  		}
       	  		
       	  		jbossQl.append("rct.rctVoid = 0 AND rct.rctType='MISC' AND rct.rctAdCompany=" + AD_CMPNY + " ");

       	  		arReceipts = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);
       	  		
       	  		if(!arReceipts.isEmpty()) {
       	  			
       	  			Iterator i = arReceipts.iterator();
       	  			
       	  			while(i.hasNext()) {
       	  				
       	  				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
       	  				
       	  				ArRepSalespersonDetails mdetails = new ArRepSalespersonDetails();
       	  				mdetails.setSlpDate(arReceipt.getRctDate());
       	  				mdetails.setSlpInvoiceNumber(arReceipt.getRctNumber());
       	  				mdetails.setSlpCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode());		
       	  				mdetails.setSlpCstCustomerName(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
       	  				mdetails.setSlpCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
       	  				mdetails.setSlpCstCustomerType(arReceipt.getArCustomer().getArCustomerType() != null ? arReceipt.getArCustomer().getArCustomerType().getCtName() : "N/A");
       	  				mdetails.setSlpSubjectToCommission(arReceipt.getRctSubjectToCommission());
       	  				mdetails.setSlpType("RECEIPT");
       	  				mdetails.setSlpPrimaryKey(arReceipt.getRctCode());
   	  					mdetails.setSlpSalespersonCode(arReceipt.getArSalesperson().getSlpSalespersonCode());
   	  					mdetails.setSlpSalespersonName(arReceipt.getArSalesperson().getSlpName());
   	  					mdetails.setSlpAdBranch(arReceipt.getRctAdBranch());
   	  					mdetails.setSlpRctCustomerDeposit(new Boolean(arReceipt.getRctCustomerDeposit()==1 ? true : false));
   	  					
   	    	  			Iterator ilItr = arReceipt.getArInvoiceLines().iterator();
   	    	  			while (ilItr.hasNext()) {
   	    	  				
   	    	  				LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)ilItr.next();
   	    	  				mdetails.saveSlpArInvoiceLines(arInvoiceLine);
   	    	  				
   	    	  			}
   	    	  			
   	    	  			Iterator iliItr = arReceipt.getArInvoiceLineItems().iterator();
   	    	  			
   	    	  			
   	    	  			while (iliItr.hasNext()) {
   	    	  				
   	    	  				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)iliItr.next();
   	    	  				mdetails.saveSlpArInvoiceLineItems(arInvoiceLineItem);
   	    	  				
   	    	  			}
   	    	  			
       	  				double AMNT_DUE = 0d;
       	  				AMNT_DUE = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
       	  						arReceipt.getGlFunctionalCurrency().getFcCode(),
								arReceipt.getGlFunctionalCurrency().getFcName(),
								arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(),
								arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());
       	  				mdetails.setSlpAmount(AMNT_DUE);
       	  				mdetails.setSlpPaymentStatus("PAID");
       	  				mdetails.setSlpCollectionDate(arReceipt.getRctDate());
       	  				list.add(mdetails);
       	  				
       	  			}
       	  			
       	  		}
       	  		
       	  	}
       	  	
       	  }
       	 
       	  if(arInvoices.size() == 0) {
       	  	
       	  	if(arReceipts != null && arReceipts.size() == 0)
       	  		throw new GlobalNoRecordFoundException();
       	  	
       	  	else if(miscReceipts.equals("NO"))
       	  		throw new GlobalNoRecordFoundException();
       	  	
       	  }
       	  
       	  ArrayList finalList = new ArrayList();
       	  
       	  finalList = findAndComputeCommission(list, GROUP_BY, criteria, branchList, AD_CMPNY);
       	  
       	  return finalList;
       	  
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepSalespersonControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("ArRepSalespersonControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
    
	
	// Private Methods
	
	private ArrayList findAndComputeCommission(ArrayList tempList, String GROUP_BY, HashMap criteria, ArrayList branchList, Integer AD_CMPNY) {
		
		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoiceLineHome arInvoiceLineHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
		LocalArStandardMemoLineHome arStandardMemoLineHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		
		try {
			
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			adBranchStandardMemoLineHome = (LocalAdBranchStandardMemoLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		ArrayList list = new ArrayList();
		ArrayList existingCmList = new ArrayList();
		Iterator i = tempList.iterator();
		double grandTotalSales = 0;
		
		while (i.hasNext()) {
			
			ArRepSalespersonDetails details = (ArRepSalespersonDetails)i.next();
			
			// this is an invoice
			
			if (!details.getSlpArInvoiceLines().isEmpty()) {	// Invoice is MEMO LINE

				Iterator j = details.getSlpArInvoiceLines().iterator();
				
				while (j.hasNext()) {
					
					LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)j.next();
					LocalArStandardMemoLine arStandardMemoLine = null;
					LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;
					
					try {

						arStandardMemoLine = arStandardMemoLineHome.findByPrimaryKey(arInvoiceLine.getArStandardMemoLine().getSmlCode());
						
					} catch (FinderException ex) {

					}
					
					if (arStandardMemoLine.getSmlSubjectToCommission() == EJBCommon.TRUE) {	// SML is subject to commission

						details.setSlpCommissionAmount(details.getSlpCommissionAmount() + arInvoiceLine.getIlAmount());
						
					} else {	// branch sml might be subject to commission
						
						try {

							adBranchStandardMemoLine = adBranchStandardMemoLineHome.findBSMLBySMLCodeAndBrCode(arInvoiceLine.getArStandardMemoLine().getSmlCode(), details.getSlpAdBranch(), AD_CMPNY);
							
						} catch (FinderException ex) {
							
						}

						if (adBranchStandardMemoLine.getBsmlSubjectToCommission() == EJBCommon.TRUE) {	// branch sml is subject to commission

							details.setSlpCommissionAmount(details.getSlpCommissionAmount() + arInvoiceLine.getIlAmount());
							
						} else {	// branch sml is not subject to commission

							details.setSlpNonCommissionAmount(details.getSlpNonCommissionAmount() + arInvoiceLine.getIlAmount());
							
						}
						
					}
					
				}
				
			} else if (!details.getSlpArInvoiceLineItems().isEmpty()) {	// Invoice is ITEMS

				Iterator j = details.getSlpArInvoiceLineItems().iterator();
				
				while (j.hasNext()) {
					
					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)j.next();
					LocalInvItemLocation invItemLocation = null;
					LocalAdBranchItemLocation adBranchItemLocation = null;
					
					try {
						invItemLocation = invItemLocationHome.findByPrimaryKey(arInvoiceLineItem.getInvItemLocation().getIlCode());
						
					} catch (FinderException ex) {

					}
					
					if (invItemLocation.getIlSubjectToCommission() == EJBCommon.TRUE) {	// IL is subject to commission

						details.setSlpCommissionAmount(details.getSlpCommissionAmount() + arInvoiceLineItem.getIliAmount());
						
					} else {	// branch IL might be subject to commission
						
						try {
							
							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arInvoiceLineItem.getInvItemLocation().getIlCode(), details.getSlpAdBranch(), AD_CMPNY);
							if (adBranchItemLocation.getBilSubjectToCommission() == EJBCommon.TRUE) {	// branch IL is subject to commission

							details.setSlpCommissionAmount(details.getSlpCommissionAmount() + arInvoiceLineItem.getIliAmount());
								
							} else {	// branch IL is not subject to commission
	
								details.setSlpNonCommissionAmount(details.getSlpNonCommissionAmount() + arInvoiceLineItem.getIliAmount());
								
							}
						} catch (FinderException ex) {
							
						}
						
						
						
					}
					
				}
				
			} else if (!details.getSlpArSalesOrderInvoiceLines().isEmpty()) {	// Invoice is SO MATCHED

				Iterator j = details.getSlpArSalesOrderInvoiceLines().iterator();
				
				while (j.hasNext()) {
					
					LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)j.next();
					LocalInvItemLocation invItemLocation = null;
					LocalAdBranchItemLocation adBranchItemLocation = null;
					
					try {

						invItemLocation = invItemLocationHome.findByPrimaryKey(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlCode());
						
					} catch (FinderException ex) {
						
					}
					
					if (invItemLocation.getIlSubjectToCommission() == EJBCommon.TRUE) {	// SO is subject to commission

						details.setSlpCommissionAmount(details.getSlpCommissionAmount() + arSalesOrderInvoiceLine.getSilAmount());
						
					} else {	// branch IL might be subject to commission
						
						try {
							
							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlCode(), details.getSlpAdBranch(), AD_CMPNY);
							
							if (adBranchItemLocation.getBilSubjectToCommission() == EJBCommon.TRUE) {	// branch SO is subject to commission

								details.setSlpCommissionAmount(details.getSlpCommissionAmount() + arSalesOrderInvoiceLine.getSilAmount());
								
							} else {	// branch IL is not subject to commission

								details.setSlpNonCommissionAmount(details.getSlpNonCommissionAmount() + arSalesOrderInvoiceLine.getSilAmount());
								
							}
						} catch (FinderException ex) {
							System.out.println(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlCode());
						}
						
					
						
					}
					
				}
				
			}
			
			boolean hasCreditMemo = false;
			double originalNonCommAmount = details.getSlpNonCommissionAmount();
			double difference = 0d;
			
			if (details.getSlpType().equalsIgnoreCase("INVOICE")) {
				
				Collection arCreditMemos = null;
				
				try {
					
					arCreditMemos = arInvoiceHome.findByInvCreditMemoAndInvCmInvoiceNumberAndInvVoidAndInvPosted((byte)1, details.getSlpInvoiceNumber(), (byte)0, (byte)1, AD_CMPNY);
					
				} catch (FinderException ex) {
					
				}	
				
				// find credit memos with previous invoices					
				
				if (criteria.containsKey("dateFrom") && !existingCmList.contains(details.getSlpCstCustomerCode())) {
					existingCmList.add(details.getSlpCstCustomerCode());
					StringBuffer jbossQl = new StringBuffer();
					  jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv WHERE (");
					  
					  Iterator brIter = branchList.iterator();
					  
					  AdBranchDetails brDetails = (AdBranchDetails)brIter.next();
					  jbossQl.append("inv.invAdBranch=" + brDetails.getBrCode());
					  
					  while(brIter.hasNext()) {
					  	
					  		brDetails = (AdBranchDetails)brIter.next();
					  		jbossQl.append(" OR inv.invAdBranch=" + brDetails.getBrCode());					  	
					  }					  
					  
					  int criteriaSize = 1;
					  int ctr = 0;
				      Object obj[];	
				      
				      if (criteria.containsKey("dateTo")) {					      	
					     criteriaSize++;					      	 
					  }					      
					  obj = new Object[criteriaSize];
					  
				  	 jbossQl.append(") AND inv.invDate>=?" + (ctr+1) + " ");
				  	 obj[ctr] = (Date)criteria.get("dateFrom");
				  	 ctr++;					 
					      
					  if (criteria.containsKey("dateTo")) {
					  	
					  	 jbossQl.append("AND inv.invDate<=?" + (ctr+1) + " ");
					  	 obj[ctr] = (Date)criteria.get("dateTo");					  	 					  	 
					  }   
					  
					  jbossQl.append("AND inv.arCustomer.cstCustomerCode='" + details.getSlpCstCustomerCode() + "' AND inv.invCreditMemo=1 AND inv.invPosted = 1 AND inv.invVoid = 0 AND inv.invAdCompany=" + AD_CMPNY + " ");
			       	  			       	  
			       	  try {
			       		  Collection arPreviousCreditMemos = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);
			       		  
			       		  Iterator prevCmIter = arPreviousCreditMemos.iterator();
			       		  while (prevCmIter.hasNext()) {
			       			  LocalArInvoice arPrevCreditMemo = (LocalArInvoice)prevCmIter.next();
			       			  LocalArInvoice arPrevInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arPrevCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, arPrevCreditMemo.getInvAdBranch(), AD_CMPNY);
			       			  if (arPrevInvoice.getInvDate().before((Date)criteria.get("dateFrom"))) {
			       				  arCreditMemos.add(arPrevCreditMemo);
			       			  }
			       			  
			       		  }
			       		  
			       	  } catch (FinderException ex) {
			       		  
			       	  }
					
				}
				
				Iterator j = arCreditMemos.iterator();
				
				while (j.hasNext()) {

					hasCreditMemo = true;
					
					double creditMemoAmount = 0;
					String creditMemoType = "";

					LocalArInvoice arCreditMemo = (LocalArInvoice)j.next();					
					
					if (arCreditMemo.getInvPosted() == EJBCommon.TRUE) {

						// get total credit memo amount
						
						if (arCreditMemo.getArInvoiceLines().isEmpty() && arCreditMemo.getArInvoiceLineItems().isEmpty()) {	// MEMO LINES
							
							creditMemoType = "MEMO LINES";
							creditMemoAmount = creditMemoAmount + arCreditMemo.getInvAmountDue();

						} else if (!arCreditMemo.getArInvoiceLineItems().isEmpty() && arCreditMemo.getInvSubjectToCommission() == EJBCommon.TRUE) {	// ITEMS

							Iterator k = arCreditMemo.getArInvoiceLineItems().iterator();
							
							while (k.hasNext()) {
								
								LocalArInvoiceLineItem arCreditMemoLineItem = (LocalArInvoiceLineItem)k.next();
								creditMemoAmount = creditMemoAmount + arCreditMemoLineItem.getIliAmount();
								
							}

						}
						
						if (arCreditMemo.getInvSubjectToCommission() == EJBCommon.TRUE) {	// MEMO LINE + SUBJECT TO COMMISSION
							
							details.setSlpCommissionAmount(details.getSlpCommissionAmount() - creditMemoAmount); 
							
						} else if (creditMemoType.equalsIgnoreCase("MEMO LINES")) {	// MEMO LINE + NOT SUBJECT TO COMMISSION
							
							details.setSlpNonCommissionAmount(details.getSlpNonCommissionAmount() - creditMemoAmount);
							
						} else {	// ITEMS + NOT SUBJECT TO COMMISSION
							
							Iterator k = arCreditMemo.getArInvoiceLineItems().iterator();
							
							while (k.hasNext()) {
								
								LocalArInvoiceLineItem arCreditMemoLineItem = (LocalArInvoiceLineItem)k.next();
								LocalInvItemLocation invItemLocation = null;
								LocalAdBranchItemLocation adBranchItemLocation = null;
								
								try {
									
									invItemLocation = invItemLocationHome.findByPrimaryKey(arCreditMemoLineItem.getInvItemLocation().getIlCode());
									
								} catch (FinderException ex) {

								}
								
								if (invItemLocation.getIlSubjectToCommission() == EJBCommon.TRUE) {	// IL is subject to commission

									details.setSlpCommissionAmount(details.getSlpCommissionAmount() - arCreditMemoLineItem.getIliAmount());
									
									
								} else {	// branch IL might be subject to commission
									
									try {
										
										adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arCreditMemoLineItem.getInvItemLocation().getIlCode(), details.getSlpAdBranch(), AD_CMPNY);
											if (adBranchItemLocation.getBilSubjectToCommission() == EJBCommon.TRUE) {	// branch IL is subject to commission

										details.setSlpCommissionAmount(details.getSlpCommissionAmount() - arCreditMemoLineItem.getIliAmount());
										
									} else {	// branch IL is not subject to commission

										details.setSlpNonCommissionAmount(details.getSlpNonCommissionAmount() - arCreditMemoLineItem.getIliAmount());
										
									}
									} catch (FinderException ex) {
										
									}
									
								
									
								}
								
							}

							
						}
						
					} else {
						
						break;
						
					}
					
				}
				
			}
			
			
			grandTotalSales = grandTotalSales + details.getSlpCommissionAmount() + details.getSlpNonCommissionAmount();
			
			if (details.getSlpSubjectToCommission() == EJBCommon.TRUE) {

				if (hasCreditMemo == true)
					difference = originalNonCommAmount - details.getSlpNonCommissionAmount();

				details.setSlpCommissionAmount(details.getSlpCommissionAmount() + details.getSlpNonCommissionAmount());
				details.setSlpNonCommissionAmount(0d);
				
				details.setSlpCommissionAmount(details.getSlpCommissionAmount() + difference);
				details.setSlpNonCommissionAmount(details.getSlpNonCommissionAmount() - difference);
				
			}
			
			list.add(details);

		}

		Iterator j = tempList.iterator();
		
		while (j.hasNext()) {
		
			ArRepSalespersonDetails arRepSlpdetails = (ArRepSalespersonDetails)j.next();
			arRepSlpdetails.setSlpGrandTotalSales(grandTotalSales);
			
		}

		Collections.sort(list, ArRepSalespersonDetails.sortByCustomerCode);
		Collections.sort(list, ArRepSalespersonDetails.sortBySalespersonCode);
	
		if (GROUP_BY.equalsIgnoreCase("CUSTOMER CODE")) {

			Collections.sort(list, ArRepSalespersonDetails.sortByCustomerCode);
			
		} else if (GROUP_BY.equalsIgnoreCase("CUSTOMER TYPE")) {
			
			Collections.sort(list, ArRepSalespersonDetails.sortByCustomerType);
			
		}
		
		return list;
		
	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepSalespersonControllerBean ejbCreate");
      
    }
}
