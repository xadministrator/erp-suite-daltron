
/*
 * ArRepStandardMemoLineListControllerBean.java
 *
 * Created on March 02, 2005, 04:06 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepStandardMemoLineListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepStandardMemoLineListControllerEJB"
 *           display-name="Used for viewing standard memo line lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepStandardMemoLineListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepStandardMemoLineListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepStandardMemoLineListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepStandardMemoLineListControllerBean extends AbstractSessionBean {

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArRepStandardMemoLineListControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepStandardMemoLineList(HashMap criteria, ArrayList branchList, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepStandardMemoLineListControllerBean executeArRepStandardMemoLineList");
        
        LocalArStandardMemoLineHome arStandardMemoLineHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
        	
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  boolean firstArgument = true;
		  short ctr = 0;
		  int criteriaSize = criteria.size();	      
		
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT DISTINCT OBJECT(sml) FROM ArStandardMemoLine sml, IN(sml.adBranchStandardMemoLines)bsml WHERE(");
		  
		  if (branchList.isEmpty())
		  	throw new GlobalNoRecordFoundException();

		  Iterator brIter = branchList.iterator();
		  
		  AdBranchDetails brDetails = (AdBranchDetails)brIter.next();
		  jbossQl.append("bsml.adBranch.brCode=" + brDetails.getBrCode());
		  
		  while(brIter.hasNext()) {
		  	
		  		brDetails = (AdBranchDetails)brIter.next();
		  		
		  		jbossQl.append(" OR bsml.adBranch.brCode=" + brDetails.getBrCode());
		  	
		  }
		  
		  jbossQl.append(") ");
		  firstArgument = false;
		  
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("standardMemoLineName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("standardMemoLineName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("sml.smlName LIKE '%" + (String)criteria.get("standardMemoLineName") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("type")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("sml.smlType=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("type");
		   	  ctr++;
	   	  
	      }	
	      
	      if (!firstArgument) {		       	  	
	   	     jbossQl.append("AND ");		       	     
	   	  } else {		       	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");		       	  	 
	   	  }	     
	   	  
	   	  jbossQl.append("sml.smlAdCompany=" + AD_CMPNY + " ");

		  String orderBy = null;
          
          if (ORDER_BY.equals("MEMO LINE NAME")) {
	      	 
	      	  orderBy = "sml.smlName";
	      	  
	      } else if (ORDER_BY.equals("MEMO LINE TYPE")) {
	      	
	      	  orderBy = "sml.smlType";
	      	
	      }

		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }     
			  	      	
		  Collection arStandardMemoLines = arStandardMemoLineHome.getSmlByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arStandardMemoLines.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arStandardMemoLines.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalArStandardMemoLine arStandardMemoLine = (LocalArStandardMemoLine)i.next();   	  
		  	  
		  	  ArRepStandardMemoLineListDetails details = new ArRepStandardMemoLineListDetails();
		  	  details.setSllSmlName(arStandardMemoLine.getSmlName());
		  	  details.setSllSmlDescription(arStandardMemoLine.getSmlDescription());
		  	  details.setSllSmlType(arStandardMemoLine.getSmlType());
		  	  details.setSllSmlUnitPrice(arStandardMemoLine.getSmlUnitPrice());
		  	  
		  	  if (arStandardMemoLine.getGlChartOfAccount() != null) {
		  	  	
		  	  	 details.setSllCoaGlAccountNumber(arStandardMemoLine.getGlChartOfAccount().getCoaAccountNumber());	                		                	    
		  	  	 details.setSllCoaGlAccountDescription(arStandardMemoLine.getGlChartOfAccount().getCoaAccountDescription());
		  	  	
		  	  }
		  	  
		  	  details.setSllTaxable(arStandardMemoLine.getSmlTax());
		  	  details.setSllEnable(arStandardMemoLine.getSmlEnable());

			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepStandardMemoLineListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepStandardMemoLineListControllerBean ejbCreate");
      
    }
}
