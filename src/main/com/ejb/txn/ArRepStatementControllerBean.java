
/*
 * ArRepStatementControllerBean.java
 *
 * Created on March 11, 2004, 02:49 PM
 *
 * @author  Dennis Hilario
 */

package com.ejb.txn;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentScheduleHome;
import com.ejb.ar.LocalArAppliedCredit;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;

import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.AdPreferenceDetails;
import com.util.ArCustomerDetails;
import com.util.ArInvoiceDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoiceDetails;
import com.util.ArRepStatementDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import java.sql.ResultSet;


/**
 * @ejb:bean name="ArRepStatementControllerEJB"
 *           display-name="Used for generating customer statements"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepStatementControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepStatementController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepStatementControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
*/

public class ArRepStatementControllerBean extends AbstractSessionBean {
	

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {

		Debug.print("ArRepStatementControllerBean getAdLvReportTypeAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR REPORT TYPE - STATEMENT OF ACCOUNT", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCcAll(Integer AD_CMPNY) {

        Debug.print("ArRepStatementControllerBean getArCcAll");

        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalArCustomerClass arCustomerClass = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);

	        Iterator i = arCustomerClasses.iterator();

	        while (i.hasNext()) {

	        	arCustomerClass = (LocalArCustomerClass)i.next();

	        	list.add(arCustomerClass.getCcName());

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCtAll(Integer AD_CMPNY) {

        Debug.print("ArRepStatementControllerBean getArCtAll");

        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalArCustomerType arCustomerType = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

	        Iterator i = arCustomerTypes.iterator();

	        while (i.hasNext()) {

	        	arCustomerType = (LocalArCustomerType)i.next();

	        	list.add(arCustomerType.getCtName());

	        }

	        return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerBatchAll(Integer AD_CMPNY) {

		Debug.print("ArRepStatementControllerBean getAdLvCustomerBatchAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerDepartmentAll(Integer AD_CMPNY) {

		Debug.print("ArRepStatementControllerBean getAdLvCustomerDepartmentAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER DEPARTMENT - SOA", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(ArrayList customerBatchList,Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArRepStatementControllerBean getArCstAll");

        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

        	Iterator i = arCustomers.iterator();

        	while (i.hasNext()) {
        		LocalArCustomer arCustomer = (LocalArCustomer)i.next();

        		if(customerBatchList.size()>0){

        			for (int x=0; x < customerBatchList.size(); x++) {


        				if(arCustomer.getCstCustomerBatch().equals(customerBatchList.get(x).toString())){
        					list.add(arCustomer.getCstCustomerCode());
        					break;
        				}



	          		}


        		}else{
        			list.add(arCustomer.getCstCustomerCode());
        		}





        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAllbyCustomerBatch(String CST_BTCH, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArRepStatementControllerBean getArCstAllbyCustomerBatch");

        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection arCustomers = arCustomerHome.findEnabledCstAllbyCustomerBatch(CST_BTCH, AD_BRNCH, AD_CMPNY);

        	Iterator i = arCustomers.iterator();

        	while (i.hasNext()) {

        		LocalArCustomer arCustomer = (LocalArCustomer)i.next();

        		list.add(arCustomer.getCstCustomerCode());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


   /**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

	  Debug.print("ArRepStatementControllerBean getAdCompany");

	  LocalAdCompanyHome adCompanyHome = null;

	  // Initialize EJB Home

	  try {

	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

	  try {

	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpZip(adCompany.getCmpZip());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     details.setCmpFax(adCompany.getCmpFax());
	     details.setCmpEmail(adCompany.getCmpEmail());
	     details.setCmpTin(adCompany.getCmpTin());

	     return details;

	  } catch (Exception ex) {

	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());

	  }

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/

    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException{

    	Debug.print("ArRepStatementControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;

    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adBranchResponsibilities.isEmpty()) {

            throw new GlobalNoRecordFoundException();

        }

        try {

	        Iterator i = adBranchResponsibilities.iterator();

	        while(i.hasNext()) {

	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();

		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());

		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());

		    	list.add(details);

	        }

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        return list;

    }


    /**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdPreferenceDetails getAdPreference(Integer AD_CMPNY) {

	  Debug.print("ArRepStatementControllerBean getAdPreference");

	  LocalAdPreferenceHome adPreferenceHome = null;

	  // Initialize EJB Home

	  try {

		  adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

	  try {

	     LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

	     AdPreferenceDetails details = new AdPreferenceDetails();
	     details.setPrfApAgingBucket(adPreference.getPrfApAgingBucket());
	     details.setPrfArAgingBucket(adPreference.getPrfArAgingBucket());

	     return details;

	  } catch (Exception ex) {

	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());

	  }

	}



	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArSmlAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArRepStatementControllerBean getArSmlAll");

        LocalArStandardMemoLineHome arStandardMemoLineHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection arStandardMemoLines = arStandardMemoLineHome.findEnabledSmlAll(AD_BRNCH, AD_CMPNY);

            Iterator i = arStandardMemoLines.iterator();

            while (i.hasNext()) {

                LocalArStandardMemoLine arStandardMemoLine = (LocalArStandardMemoLine)i.next();
                list.add(arStandardMemoLine.getSmlName());

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepStatement2(HashMap criteria, String SML_NAME, String customerCode, boolean includeUnpostedTransaction, boolean showUnearnedInterestTransactionOnly, boolean includeAdvance, boolean includeAdvanceOnly, boolean showAll, boolean includeZero, boolean includeCollection, boolean includeUnpostedInvoice, String ORDER_BY, String GROUP_BY, String REPORT_TYP, ArrayList customerBatchList, ArrayList customerDepartmentList, ArrayList branchList, Integer AD_CMPNY)
            throws GlobalNoRecordFoundException {
    	Debug.print("ArRepStatementControllerBean executeArRepStatement2");

    	  Debug.print("ArRepStatementControllerBean executeArRepStatement");


          LocalArCustomerBalanceHome arCustomerBalanceHome = null;
          LocalAdCompanyHome adCompanyHome = null;
          LocalArCustomerHome arCustomerHome = null;
          LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
          LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
          LocalArInvoiceLineHome arSalesInvoiceLineHome = null;
          LocalAdPreferenceHome adPreferenceHome = null;
          LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
          LocalArInvoiceHome arInvoiceHome = null;
          LocalCmAdjustmentHome cmAdjustmentHome = null;
          LocalArReceiptHome arReceiptHome = null;
          /*
          LocalPmProjectHome pmProjectHome = null;
          LocalPmContractHome pmContractHome = null;
          LocalPmContractTermHome pmContractTermHome = null;
          LocalPmProjectTypeHome pmProjectTypeHome = null;
          LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;
          LocalPmProjectPhaseHome pmProjectPhaseHome = null;

	*/
          ArrayList list = new ArrayList();
          Date agingDate = null;

          //initialized EJB Home

          try {
/*
          	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                  	lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
          	pmContractHome = (LocalPmContractHome)EJBHomeFactory.
                  	lookUpLocalHome(LocalPmContractHome.JNDI_NAME, LocalPmContractHome.class);
          	pmContractTermHome = (LocalPmContractTermHome)EJBHomeFactory.
                  	lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);
          	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
                  	lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
          	pmProjectPhaseHome = (LocalPmProjectPhaseHome)EJBHomeFactory.
                  	lookUpLocalHome(LocalPmProjectPhaseHome.JNDI_NAME, LocalPmProjectPhaseHome.class);
          	pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
                  	lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
*/


              arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
                  lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
              adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                  lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
              arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
              lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

              arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
                      lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);

              arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
                      lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);

              arSalesInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
                      lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);

              adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                      lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

              arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
                      lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);

              arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                      lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

              arReceiptHome= (LocalArReceiptHome)EJBHomeFactory.
                      lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);

              cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                      lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);


          } catch (NamingException ex) 	{

              throw new EJBException(ex.getMessage());

          }




          try {

        	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		  short precisionUnit = adCompany.getGlFunctionalCurrency().getFcPrecision();


    		  LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(adCompany.getCmpCode());
    		  int agingBucket1 = adPreference.getPrfArAgingBucket();
    		  int agingBucket2 = adPreference.getPrfArAgingBucket() * 2;
    		  int agingBucket3 = adPreference.getPrfArAgingBucket() * 3;
    		  int agingBucket4 = adPreference.getPrfArAgingBucket() * 4;

    		  boolean firstArgument = true;
    	      short ctr = 0;
    		  int criteriaSize = criteria.size();


              StringBuffer arInvoiceSQL = new StringBuffer();
    		  StringBuffer cmAdjustmentSQL = new StringBuffer();
    		  StringBuffer arReceiptSQL = new StringBuffer();

    		  arInvoiceSQL.append("SELECT OBJECT(inv) FROM ArInvoice inv ");
    		  cmAdjustmentSQL.append("SELECT OBJECT(adj) FROM CmAdjustment adj ");
    		  arReceiptSQL.append("SELECT OBJECT(rct) FROM ArReceipt rct ");




    		  //customer Code
    		  if (!customerCode.equals("")) {

    	 	  	 if (!firstArgument) {

    	 	  		arInvoiceSQL.append("AND ");
    	 	  		cmAdjustmentSQL.append("AND ");
    	 	  		arReceiptSQL.append("AND ");

    	 	     } else {

    	 	     	firstArgument = false;
    	 	     	arInvoiceSQL.append("WHERE ");
    	 	     	cmAdjustmentSQL.append("WHERE ");
    	 	     	arReceiptSQL.append("WHERE ");

    	 	     }

    		 	  	arInvoiceSQL.append("inv.arCustomer.cstCustomerCode LIKE '%" + customerCode + "%' ");
    		 	  	cmAdjustmentSQL.append("adj.arCustomer.cstCustomerCode LIKE '%" + customerCode + "%' ");
    		 	  	arReceiptSQL.append("rct.arCustomer.cstCustomerCode LIKE '%" + customerCode + "%' ");

    	 	  }

    		  //include unposted transaction
    		//  String unposted = (String)criteria.get("includeUnpostedTransaction");

              if (!firstArgument) {

            	  arInvoiceSQL.append("AND ");
            	  cmAdjustmentSQL.append("AND ");
            	  arReceiptSQL.append("AND ");

           	  } else {

           	  	 firstArgument = false;
           	  	 arInvoiceSQL.append("WHERE ");
    	       	 cmAdjustmentSQL.append("WHERE ");
    	       	 arReceiptSQL.append("WHERE ");

           	  }

           	  if (includeUnpostedTransaction) {

           		arInvoiceSQL.append("inv.invPosted = 1 ");
           		cmAdjustmentSQL.append("adj.adjPosted = 1 ");
           		arReceiptSQL.append("rct.rctPosted = 1 ");

           	  } else {

           		arInvoiceSQL.append("inv.invVoid = 0 ");
           		cmAdjustmentSQL.append("adj.adjVoid = 0 ");
           		arReceiptSQL.append("rct.rctVoid = 0 ");

           	  }

           	 if (showUnearnedInterestTransactionOnly) {


           		 if (!firstArgument) {
					 arInvoiceSQL.append("AND ");
				  } else {
			   	  	 firstArgument = false;
			   	  	 arInvoiceSQL.append("WHERE ");
				  }

			  	 arInvoiceSQL.append("inv.invAmountUnearnedInterest > 0 ");

           	 }



           	  if (branchList.isEmpty()) throw new GlobalNoRecordFoundException();

           	  if (!firstArgument) {

	           	  arInvoiceSQL.append("AND (");
	           	  cmAdjustmentSQL.append("AND (");
	           	  arReceiptSQL.append("AND ");

          	  } else {

          	  	 firstArgument = false;
          	  	 arInvoiceSQL.append("WHERE (");
          	  	 cmAdjustmentSQL.append("WHERE (");
          	  	 arReceiptSQL.append("WHERE (");

          	  }


           	  //branch list
    		  Iterator brIter = branchList.iterator();

    		  AdBranchDetails details = (AdBranchDetails)brIter.next();
    		  arInvoiceSQL.append("inv.invAdBranch=" + details.getBrCode());
    		  cmAdjustmentSQL.append("adj.adjAdBranch=" + details.getBrCode());
    		  arReceiptSQL.append("rct.rctAdBranch=" + details.getBrCode());

    		  while(brIter.hasNext()) {

		  			details = (AdBranchDetails)brIter.next();

		  			arInvoiceSQL.append(" OR inv.invAdBranch=" + details.getBrCode());
		  			cmAdjustmentSQL.append(" OR adj.adjAdBranch=" + details.getBrCode());
		  			arReceiptSQL.append(" OR rct.rctAdBranch=" + details.getBrCode());

    		  }

    		  arInvoiceSQL.append(") ");
    		  cmAdjustmentSQL.append(") ");
    		  arReceiptSQL.append(") ");


           	  //customer batch list
    		  if (!customerBatchList.isEmpty()) {

    			  if (!firstArgument) {

    	           	  arInvoiceSQL.append("AND ");
    	           	  cmAdjustmentSQL.append("AND ");
    	           	  arReceiptSQL.append("AND ");

              	  } else {

              	  	 firstArgument = false;
              	  	 arInvoiceSQL.append("WHERE ");
              	  	 cmAdjustmentSQL.append("WHERE ");
              	  	 arReceiptSQL.append("WHERE ");

              	  }

    			  Iterator j = customerBatchList.iterator();


    			  arInvoiceSQL.append("inv.arCustomer.cstCustomerBatch in (");
    			  cmAdjustmentSQL.append("adj.arCustomer.cstCustomerBatch in (");
    			  arReceiptSQL.append("rct.arCustomer.cstCustomerBatch in (");


    		 	  boolean firstLoop = true;


    		 	  while(j.hasNext()) {

    		  		if(firstLoop == false) {
    		  			arInvoiceSQL.append(", ");
    		  		}
    		  		else {
    		  			firstLoop = false;
    		  		}


    		  		String CSTMR_BTCH = (String) j.next();

    		  		arInvoiceSQL.append("'"+CSTMR_BTCH+"'");
    		  		cmAdjustmentSQL.append("'"+CSTMR_BTCH+"'");
    		  		arReceiptSQL.append("'"+CSTMR_BTCH+"'");


    			  	}

    		 	  arInvoiceSQL.append(") ");
    		 	  cmAdjustmentSQL.append(") ");
    		 	  arReceiptSQL.append(") ");



    		  }

    		  //customer department list

    		  if (!customerDepartmentList.isEmpty()) {

    			  if (!firstArgument) {

    	           	  arInvoiceSQL.append("AND ");
    	           	  cmAdjustmentSQL.append("AND ");
    	           	  arReceiptSQL.append("AND ");

              	  } else {

              	  	 firstArgument = false;
              	  	 arInvoiceSQL.append("WHERE ");
              	  	 cmAdjustmentSQL.append("WHERE ");
              	  	 arReceiptSQL.append("WHERE ");

              	  }

    			  Iterator j = customerDepartmentList.iterator();


    			  arInvoiceSQL.append("AND inv.arCustomer.cstCustomerDepartment in (");
    			  cmAdjustmentSQL.append("AND adj.arCustomer.cstCustomerDepartment in (");
    			  arReceiptSQL.append("AND rct.arCustomer.cstCustomerDepartment in (");


    		 	  boolean firstLoop = true;


    		 	  while(j.hasNext()) {

    		  		if(firstLoop == false) {
    		  			arInvoiceSQL.append(", ");
    		  		}
    		  		else {
    		  			firstLoop = false;
    		  		}


    		  		String CSTMR_DEP = (String) j.next();

    		  		arInvoiceSQL.append("'"+CSTMR_DEP+"'");
    		  		cmAdjustmentSQL.append("'"+CSTMR_DEP+"'");
    		  		arReceiptSQL.append("'"+CSTMR_DEP+"'");


    			  	}

    		 	 arInvoiceSQL.append(") ");
    		 	 cmAdjustmentSQL.append(") ");
    		 	 arReceiptSQL.append(") ");



    		  }

           	Object obj[] = new Object[criteriaSize];

           	if (criteria.containsKey("customerType")) {

    		   	  if (!firstArgument) {
    		   		  arInvoiceSQL.append("AND ");
    		   		  cmAdjustmentSQL.append("AND ");
    		   		  arReceiptSQL.append("AND ");
    		   	  } else {
    		   	  	  firstArgument = false;
    		   	  	  arInvoiceSQL.append("WHERE ");
    		   	  	  cmAdjustmentSQL.append("WHERE ");
    	   	  	 	  arReceiptSQL.append("WHERE ");
    		   	  }

    			   	arInvoiceSQL.append("inv.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
    			   	cmAdjustmentSQL.append("adj.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
    			   	arReceiptSQL.append("rct.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
    		   	  obj[ctr] = (String)criteria.get("customerType");
    		   	  ctr++;

    	      }

    	      if (criteria.containsKey("customerClass")) {

    		   	  if (!firstArgument) {
    		   		arInvoiceSQL.append("AND ");
    		   		cmAdjustmentSQL.append("AND ");
    		   		arReceiptSQL.append("AND ");
    		   	  } else {
    		   	  	firstArgument = false;
    		   	    arInvoiceSQL.append("WHERE ");
    			   	cmAdjustmentSQL.append("WHERE ");
    			   	arReceiptSQL.append("WHERE ");
    		   	  }

    		   	  arInvoiceSQL.append("inv.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
    		   	  cmAdjustmentSQL.append("adj.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
    		   	  arReceiptSQL.append("rct.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
    		   	  obj[ctr] = (String)criteria.get("customerClass");
    		   	  ctr++;

    	      }

    	      if (criteria.containsKey("dateFrom")) {

    			 if (!firstArgument) {
    				 arInvoiceSQL.append("AND ");
    				 cmAdjustmentSQL.append("AND ");
    				 arReceiptSQL.append("AND ");
    			 } else {
    			 	 firstArgument = false;
    			 	 arInvoiceSQL.append("WHERE ");
    			 	 cmAdjustmentSQL.append("WHERE ");
    			 	 arReceiptSQL.append("WHERE ");
    			 }
    			 arInvoiceSQL.append("inv.invDate>=?" + (ctr+1) + " ");
    			 cmAdjustmentSQL.append("adj.adjDate>=?" + (ctr+1) + " ");
    			 arReceiptSQL.append("rct.rctDate>=?" + (ctr+1) + " ");

    			 obj[ctr] = (Date)criteria.get("dateFrom");

    			 ctr++;
    		  }




    	      if (criteria.containsKey("dateTo")) {

    		 	 if (!firstArgument) {
    		 		arInvoiceSQL.append("AND ");
    		 		cmAdjustmentSQL.append("AND ");
    		 		arReceiptSQL.append("AND ");
    		 	 } else {
    		 	 	firstArgument = false;
    		 	 	arInvoiceSQL.append("WHERE ");
    		 	 	cmAdjustmentSQL.append("WHERE ");
    		 	 	arReceiptSQL.append("WHERE ");
    		 	 }
    		 	 arInvoiceSQL.append("inv.invDate<=?" + (ctr+1) + " ");
    		 	 cmAdjustmentSQL.append("adj.adjDate<=?" + (ctr+1) + " ");
    		 	 arReceiptSQL.append("rct.rctDate<=?" + (ctr+1) + " ");

    		 	 obj[ctr] = (Date)criteria.get("dateTo");
    		 	 agingDate = (Date)criteria.get("dateTo");
    		 	 ctr++;
    		  }

    		  if (criteria.containsKey("invoiceNumberFrom")) {

    			  if (!firstArgument) {
    				  arInvoiceSQL.append("AND ");
    			  } else {
    				  firstArgument = false;
    				  arInvoiceSQL.append("WHERE ");
    			  }
    			  arInvoiceSQL.append("inv.invNumber>=?" + (ctr+1) + " ");
    			  obj[ctr] = (String)criteria.get("invoiceNumberFrom");
    			  ctr++;

    		  }

    		  if (criteria.containsKey("invoiceNumberTo")) {

    			  if (!firstArgument) {
    				  arInvoiceSQL.append("AND ");
    			  } else {
    				  firstArgument = false;
    				  arInvoiceSQL.append("WHERE ");
    			  }
    			  arInvoiceSQL.append("inv.invNumber<=?" + (ctr+1) + " ");
    			  obj[ctr] = (String)criteria.get("invoiceNumberTo");
    			  ctr++;

    		  }

    		  if (criteria.containsKey("invoiceBatchName")) {

    		   	  if (!firstArgument) {
    		   		arInvoiceSQL.append("AND ");
    		   	  } else {
    		   	  	 firstArgument = false;
    		   	  arInvoiceSQL.append("WHERE ");
    		   	  }

    		   	arInvoiceSQL.append("inv.arInvoiceBatch.ibName=?" + (ctr) + " ");
    		   	  obj[ctr] = (String)criteria.get("invoiceBatchName");
    		   	  ctr++;

    	      }




    		  if (!firstArgument) {

    			 arInvoiceSQL.append("AND ");
    	   	     cmAdjustmentSQL.append("AND ");
    	   	  } else {

    	   	  	 firstArgument = false;
    	   	  	 arInvoiceSQL.append("WHERE ");
    	   		 cmAdjustmentSQL.append("WHERE ");

    	   	  }

    		  arInvoiceSQL.append("inv.invVoid=0  AND inv.invAdCompany=" + AD_CMPNY + " ORDER BY inv.arCustomer.cstName, inv.invDate");
    		  cmAdjustmentSQL.append("adj.adjVoid = 0 AND adj.adjType = 'ADVANCE' AND adj.adjAdCompany=" + AD_CMPNY);


    	      if (showAll){

    	    	  arInvoiceSQL.append("inv.invVoid=0 AND inv.invCreditMemo = 0 AND inv.invAdCompany=" + AD_CMPNY + " ORDER BY inv.arCustomer.cstName, inv.invDate");
    	    	  cmAdjustmentSQL.append("adj.adjVoid = 0 AND adj.adjType = 'ADVANCE' AND adj.adjAdCompany=" + AD_CMPNY);

    	     /* Needs to create independent block of code
    	      }else if(includeZero){

    	    	  arInvoiceSQL.append("inv.invVoid=0 AND (ips.ipsAmountDue <= ips.ipsAmountPaid OR ips.ipsAmountDue >= ips.ipsAmountPaid) AND ips.arInvoice.invCreditMemo = 0 AND ips.ipsAdCompany=" + AD_CMPNY + " ORDER BY ips.arInvoice.arCustomer.cstName, ips.arInvoice.invDate, ips.ipsNumber");
    	    	  cmAdjustmentSQL.append("adj.adjVoid = 0 AND adj.adjType = 'ADVANCE' AND adj.adjAdCompany=" + AD_CMPNY);

    	      }else {

    	    	  arInvoiceSQL.append("ips.arInvoice.invVoid=0 AND (ips.ipsAmountDue < ips.ipsAmountPaid OR ips.ipsAmountDue > ips.ipsAmountPaid) AND ips.arInvoice.invCreditMemo = 0 AND ips.ipsAdCompany=" + AD_CMPNY + " ORDER BY ips.arInvoice.arCustomer.cstName, ips.arInvoice.invDate, ips.ipsNumber");
    	    	  cmAdjustmentSQL.append("adj.adjVoid = 0 AND adj.adjType = 'ADVANCE' AND adj.adjAdCompany=" + AD_CMPNY);

    	      */
    	      }


    	      System.out.println("sql invoice is : " + arInvoiceSQL);

    	      System.out.println("sql adv is : " + cmAdjustmentSQL);

    	      System.out.println("size obj is : " + obj.length);
    	      if(includeAdvance	|| includeAdvanceOnly) {
    	    	  Collection advancePayments = cmAdjustmentHome.getAdjByCriteria(cmAdjustmentSQL.toString(), obj);

    	    	  Iterator x = advancePayments.iterator();

    	    	  while (x.hasNext()) {

    		    	  LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)x.next();


    		    	  //System.out.println("cmAdjustment="+cmAdjustment.getAdjDocumentNumber());
    		    	  ArRepStatementDetails mdetails = new ArRepStatementDetails();
    		    	  mdetails.setSmtCstCustomerID(cmAdjustment.getArCustomer().getCstWordPressCustomerID());
    		    	  mdetails.setSmtCstEmployeeID(cmAdjustment.getArCustomer().getCstEmployeeID());
    		    	  mdetails.setSmtCstAccountNumber(cmAdjustment.getArCustomer().getCstAccountNumber());
    		    	  mdetails.setSmtCstSquareMeter(cmAdjustment.getArCustomer().getCstSquareMeter());
    		    	  mdetails.setSmtCustomerEntryDate(cmAdjustment.getArCustomer().getCstEntryDate());
    		    	  mdetails.setSmtCstParkingID(cmAdjustment.getArCustomer().getCstParkingID());
    		    	  mdetails.setSmtCustomerBatch(cmAdjustment.getArCustomer().getCstCustomerBatch());
    		    	  mdetails.setSmtCustomerDepartment(cmAdjustment.getArCustomer().getCstCustomerDepartment());

    		    	  mdetails.setSmtCustomerCode(cmAdjustment.getArCustomer().getCstCustomerCode());

    	  	  	      mdetails.setSmtCustomerName(cmAdjustment.getArCustomer().getCstName());
    	  	  	      mdetails.setSmtCstEML(cmAdjustment.getArCustomer().getCstEmail());
    	  	  	      mdetails.setSmtDate(cmAdjustment.getAdjDate());
    	  	  	      mdetails.setSmtDueDate(cmAdjustment.getAdjDate());
    	  	  	      mdetails.setSmtTransactionType("ADVANCE");

    	  	  	      mdetails.setSmtTransaction(cmAdjustment.getAdjDocumentNumber());
    	  	  	      mdetails.setSmtPosted(cmAdjustment.getAdjPosted());


    	  	  	      // get applied credit

    	              Collection arAppliedCredits = cmAdjustment.getArAppliedCredits();

    	              Iterator y = arAppliedCredits.iterator();

    	              double totalAppliedCredit = 0d;

    	              while(y.hasNext()){

    	              	LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)y.next();

    	              	totalAppliedCredit += arAppliedCredit.getAcApplyCredit();

    	              }


    	  	  	      double ADV_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
    	  	  	    			cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(),
    	  	  	    			cmAdjustment.getAdjConversionDate(),
    	  	  	    			cmAdjustment.getAdjConversionRate(),
    	  	  	    		cmAdjustment.getAdjAmount() - totalAppliedCredit- cmAdjustment.getAdjRefundAmount(), AD_CMPNY) ;

    	  	  	      if ( !includeZero && (!showAll && ADV_AMNT == 0d)) continue;
    	  	  	      if (includeCollection && ADV_AMNT == 0d) continue;

    	  	  	      try{
    	  	  	    	  SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MMMMM yyyy");
    		  	  	      String date = DATE_FORMAT.format(cmAdjustment.getAdjDate());
    		  	  	      mdetails.setSmtBatchName(date);
    	  	  	      }catch(Exception ex){
    	  	  	    	mdetails.setSmtBatchName("");
    	  	  	      }

    	  	  	      mdetails.setSmtOrNumber(cmAdjustment.getAdjDocumentNumber());

    	  	  	      
    	  	  	      
    	  	  	      mdetails.setSmtDescription(cmAdjustment.getAdjMemo().equals("")?"ADVANCE PAYMENT / CUSTOMER DEPOSIT":cmAdjustment.getAdjMemo());
    	  	  	      mdetails.setSmtAmountPaid(ADV_AMNT );
    	  	  	      mdetails.setSmtItemDescription("ADVANCE");
    	  	  	      mdetails.setSmtAmount(0d);
    	  	  	      mdetails.setSmtAmountDue(ADV_AMNT * -1 );
    	  	  	      mdetails.setSmtIpsAmountDue(cmAdjustment.getAdjAmount()* -1);
    	  	  	      mdetails.setSmtDebit(0d);
    	  	  	      mdetails.setSmtCredit(ADV_AMNT);
    	  	  	      mdetails.setSmtCreditAmount(0d);
    	  	  	      mdetails.setSmtTaxAmount(0d);

    	  	  	      mdetails.setSmtAmountDetails(ADV_AMNT * -1 );
      	  	      	  int INVOICE_AGE = (short)((agingDate.getTime() -
      	  	    	  cmAdjustment.getAdjDate().getTime()) / (1000 * 60 * 60 * 24));


      	  	      	  if (INVOICE_AGE <= 0) {

      	  	      		  mdetails.setSmtBucketADV0(ADV_AMNT * -1);

      	  	      	  } else if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {

      	  	      		  mdetails.setSmtBucketADV1(ADV_AMNT * -1);

      	  	      	  } else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {

      	  	      		  mdetails.setSmtBucketADV2(ADV_AMNT * -1);

      	  	      	  } else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {

      	  	      		  mdetails.setSmtBucketADV3(ADV_AMNT * -1);

      	  	      	  } else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {

      	  	      		  mdetails.setSmtBucketADV4(ADV_AMNT * -1);

      	  	      	  } else if (INVOICE_AGE > agingBucket4) {

      	  	      		  mdetails.setSmtBucketADV5(ADV_AMNT * -1);

      	  	      	  }
      	  	      	  list.add(mdetails);

      	      	}

    	      }

    	      Collection arInvoices = arInvoiceHome.getInvByCriteria(arInvoiceSQL.toString(), obj);


    	      System.out.println("jbossQl.toString()="+arInvoiceSQL.toString());
    	      if(!includeAdvanceOnly){

    	    	  String SMT_CSTMR_CODE = "";
    			  String SMT_CSTMR_NM = null;
    			  String SMT_CSTMR_ADDRSS = null;
    			  String SMT_CST_BLLNG_FTR = null;
    			  String SMT_CST_BLLNG_HDR = null;
    			  String SMT_CST_BLLNG_SGNTRY = null;
    			  String SMT_CST_SGNTRY_TTL = null;
    			  String SMT_CST_CNTCT = null;
    			  String SMT_CST_TIN = null;
    			  String SMT_EML = null;
    			  String SMT_TRM = null;
    			  String SMT_CST_WP_CSTMR_ID = null;

    			  arInvoices = arInvoiceHome.getInvByCriteria(arInvoiceSQL.toString(), obj);





    			  System.out.println(" COA arInvoices.size()="+arInvoices.size());
    			  Date lastDueDate = null;

    			  //its nonesense but try
    			  if(!arInvoices.isEmpty()) {

  	            	LocalArInvoicePaymentSchedule arInvoicePaymentSchedule;

  	            	lastDueDate = null;

    			  }

    			  Iterator i  = arInvoices.iterator();

    			  while(i.hasNext()) {

    				  LocalArInvoice arInvoice = (LocalArInvoice)i.next();
    				  LocalArCustomer arCustomer = arInvoice.getArCustomer();

    				  Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();



				  	 double creditAmount=0d;
				  	 try{

				  		System.out.println("CREDIT MEMO INVOICE="+ arInvoice.getInvNumber());

				  		Collection creditMemos = arInvoiceHome.findByCmInvoiceNumberCreditMemoAndCompanyCode(arInvoice.getInvNumber(),arInvoice.getInvAdBranch(), AD_CMPNY);

				  		Iterator y = creditMemos.iterator();

				  		while(y.hasNext()){

				  			LocalArInvoice arInvoiceCreditMemo = (LocalArInvoice)y.next();
				  			System.out.println("CREDIT MEMO EXIST="+arInvoiceCreditMemo.getInvNumber());
				  			creditAmount += arInvoiceCreditMemo.getInvAmountDue();

				  		}
				  		System.out.println("CREDIT AMOUNT="+creditAmount);
				  		if(creditMemos.size() > 0 && EJBCommon.roundIt(creditAmount, precisionUnit)  == EJBCommon.roundIt(arInvoice.getInvAmountDue(), precisionUnit)){
				  			
				  			System.out.println("skip invoice due to cm and overdue are equal :" + arInvoice.getInvNumber());
				  			continue;
				  		}

				  	 }catch(Exception ex){

				  		System.out.println("ERROR");
				  	 }



    				  Iterator i2 = arInvoicePaymentSchedules.iterator();


    				  while(i2.hasNext()) {

    					  LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i2.next();


    					  double ipsAmountDue = arInvoicePaymentSchedule.getIpsAmountDue();
    					  double ipsAmountPaid = arInvoicePaymentSchedule.getIpsAmountPaid();



    					  if(!includeZero && !showAll){

    						  if(ipsAmountPaid== ipsAmountDue) {
    							  continue;
    						  }
    					  }



    					  Collection appliedInvoices = arInvoicePaymentSchedule.getArAppliedInvoices();

    					  System.out.println("includeZero="+includeZero);
					  	  System.out.println("arInvoicePaymentSchedule.getIpsAmountDue()="+EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue(), precisionUnit));
					  	  System.out.println("arInvoicePaymentSchedule.getIpsAmountPaid()="+EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid(), precisionUnit));

					  	  if (!includeZero && (!showAll &&
					  			EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue(), precisionUnit) ==
					  			EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid(), precisionUnit))
					  			) {System.out.println("continue");continue; }

					  	  //SALES ORDER INVOICE LINES





					  	int INVOICE_AGE = (short)((agingDate.getTime() -
					            arInvoicePaymentSchedule.getIpsDueDate().getTime()) / (1000 * 60 * 60 * 24));
					  	System.out.println(INVOICE_AGE+"=INVOICE_AGE="+arInvoicePaymentSchedule.getIpsDueDate()) ;
					  	System.out.println(INVOICE_AGE+"=INVOICE_AGE="+arInvoicePaymentSchedule.getIpsDueDate()) ;


				  	    int soSize = arInvoice.getArSalesOrderInvoiceLines().size();
				  	    int invoiceItemSize = arInvoice.getArInvoiceLineItems().size();
				  	    int joSize = arInvoice.getArJobOrderInvoiceLines().size();
				  	 // int joSize = 0;

					  	double SO_CM_AMNT = 0d;

					  	//invoice line items and invoice sales order lines
					  	  if(soSize!=0 || invoiceItemSize !=0 || joSize != 0){
					  		 ArRepStatementDetails so_mdetails = new ArRepStatementDetails();


					  	     if(arInvoice.getArInvoiceBatch() != null) {
					  		   so_mdetails.setSmtInvoiceBatchName(arInvoice.getArInvoiceBatch().getIbName());
					  	     }
					  	 	 so_mdetails.setSmtTransactionType("INVOICE");
					  	 	 so_mdetails.setSmtLastDueDate(lastDueDate);

						  	 so_mdetails.setSmtDueDate(arInvoicePaymentSchedule.getIpsDueDate());
						  	 double amountDue = arInvoicePaymentSchedule.getIpsAmountDue() + arInvoicePaymentSchedule.getIpsPenaltyDue() ;
							 double mo_ttlAmount = arInvoicePaymentSchedule.getIpsAmountDue() ;

							 so_mdetails.setSmtNumber(arInvoicePaymentSchedule.getIpsNumber());
							 so_mdetails.setSmtAmount(mo_ttlAmount);
							 so_mdetails.setSmtPenalty(arInvoicePaymentSchedule.getIpsPenaltyDue());
							 so_mdetails.setSmtAmountDue(amountDue);
							 so_mdetails.setSmtIpsAmountDue(arInvoicePaymentSchedule.getIpsAmountDue());
							 so_mdetails.setSmtInterestDue(arInvoicePaymentSchedule.getIpsInterestDue());

							 //Applied Invoices
							 Iterator i3 = appliedInvoices.iterator();

							 Double appliedAmount = 0d;
						  	 String OR_NUM = "";

						  	 while(i3.hasNext()){

					  		    LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i3.next();

						  		int y = arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate);

						  		System.out.println("RCT DATE="+arAppliedInvoice.getArReceipt().getRctDate());
						  		System.out.println("AGING DATE="+agingDate);
						  		System.out.println("Y===="+y);

						  		if(arAppliedInvoice.getArReceipt().getRctVoid() == 1
						  				|| (arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate) > 0) ) continue;
						  		System.out.println("---------------------->");
						  		System.out.println(arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate) <= 0);
						  		  OR_NUM += arAppliedInvoice.getArReceipt().getRctNumber() + ":";
						  		 appliedAmount += arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiPenaltyApplyAmount()
						  				+ arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiRebate();

						  		so_mdetails.setSmtLastOrDate(arAppliedInvoice.getArReceipt().getRctDate());
						  	 }
						  	so_mdetails.setSmtOrNumber(OR_NUM);
						  	so_mdetails.setSmtAmountPaid(appliedAmount);


						  	 //invoice info
						  	 so_mdetails.setSmtInvoiceNumber(arInvoice.getInvNumber());
						  	 so_mdetails.setSmtPosted(arInvoice.getInvPosted());
						  	 so_mdetails.setSmtPaymentTerm(arInvoice.getAdPaymentTerm().getAdPaymentSchedules().size());
						  	 so_mdetails.setSmtPaymentTermName(arInvoice.getAdPaymentTerm().getPytName());
						  	 so_mdetails.setSmtTransaction(arInvoice.getInvNumber());
						   	 so_mdetails.setSmtReferenceNumber(arInvoice.getInvReferenceNumber());
						  	 so_mdetails.setSmtDescription(arInvoice.getInvDescription());
						  	 so_mdetails.setSmtInvoiceAmountDue(arInvoice.getInvAmountDue());
						  	 so_mdetails.setSmtInvoiceDownPayment(arInvoice.getInvDownPayment());
						  	 so_mdetails.setSmtInvoiceUnearnedInterest(arInvoice.getInvAmountUnearnedInterest());

						  	 so_mdetails.setSmtInvoicePenaltyDue(arInvoice.getInvPenaltyDue());
							 try {
								 so_mdetails.setSmtBatchName(arInvoice.getArInvoiceBatch().getIbName());

							 } catch (Exception ex){

								 so_mdetails.setSmtBatchName("");
							 }
							 so_mdetails.setSmtDate(arInvoice.getInvDate());
							 so_mdetails.setSmtDescription(arInvoice.getInvDescription());
							 so_mdetails.setSmtCurrency(arInvoice.getGlFunctionalCurrency().getFcName());
							 so_mdetails.setSmtPaymentTerm(arInvoice.getAdPaymentTerm().getAdPaymentSchedules().size());
							 so_mdetails.setSmtPaymentTermName(arInvoice.getAdPaymentTerm().getPytName());


							 so_mdetails.setSmtCreditAmount(creditAmount);
							 so_mdetails.setSmtTaxCode(arInvoicePaymentSchedule.getArInvoice().getArTaxCode().getTcName());
							 so_mdetails.setSmtRemainingBalance(amountDue - appliedAmount);
							 so_mdetails.setSmtDebit(arInvoicePaymentSchedule.getIpsAmountDue());
							 so_mdetails.setSmtCredit(creditAmount);

							  SO_CM_AMNT = amountDue - creditAmount - appliedAmount;



					  	    //customer info
							 so_mdetails.setSmtCustomerCode(arCustomer.getCstCustomerCode());
							 so_mdetails.setSmtCstCustomerID(arCustomer.getCstWordPressCustomerID());
					  		 so_mdetails.setSmtCstEmployeeID(arCustomer.getCstEmployeeID());
					  		 so_mdetails.setSmtCstAccountNumber(arCustomer.getCstAccountNumber());
					  		 so_mdetails.setSmtCustomerBatch(arCustomer.getCstCustomerBatch());
					  		 so_mdetails.setSmtCustomerEntryDate(arCustomer.getCstEntryDate());
					  		 so_mdetails.setSmtCustomerName(arCustomer.getCstName());
					  		 so_mdetails.setSmtCustomerAddress(arCustomer.getCstAddress());
					  		 so_mdetails.setSmtCstContact(arCustomer.getCstContact());
					  		 so_mdetails.setSmtCstTin(arCustomer.getCstTin());
					  		 so_mdetails.setSmtCstSquareMeter(arCustomer.getCstSquareMeter());
					  		 so_mdetails.setSmtCstParkingID(arCustomer.getCstParkingID());
						  	 so_mdetails.setSmtCstEML(arCustomer.getCstEmail());
						  	 so_mdetails.setSmtCstCustomerID(arCustomer.getCstWordPressCustomerID());
						  	 so_mdetails.setSmtCstEmployeeID(arCustomer.getCstEmployeeID());
						  	 so_mdetails.setSmtCstAccountNumber(arCustomer.getCstAccountNumber());


							 if (INVOICE_AGE <= 0) {

								 so_mdetails.setSmtBucket0(SO_CM_AMNT);

				  	    	  }else if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {

				  	    		so_mdetails.setSmtBucket1(SO_CM_AMNT);



				  	    	  } else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {

				  	    		so_mdetails.setSmtBucket2(SO_CM_AMNT);


				  	    	  } else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {

				  	    		so_mdetails.setSmtBucket3(SO_CM_AMNT);


				  	    	  } else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {

				  	    		so_mdetails.setSmtBucket4(SO_CM_AMNT);


				  	    	  } else if (INVOICE_AGE > agingBucket4) {

				  	    		so_mdetails.setSmtBucket5(SO_CM_AMNT);


				  	    	  }

							 list.add(so_mdetails);


					  	  }



					  	  //invoice memo line
						  	double CM_AMNT = 0d;
						  	double CM_AMNT_ASD = 0d;
						  	double CM_AMNT_RPT = 0d;
						  	double CM_AMNT_PD = 0d;
						  	double CM_AMNT_WTR = 0d;
						  	double CM_AMNT_MISC = 0d;


						  	Collection arSalesInvoiceLines = arInvoice.getArInvoiceLines();
						 	Collection arJobOrderInvoiceLines = arInvoice.getArJobOrderInvoiceLines();
						  	System.out.println("arSalesInvoiceLines.size()="+arSalesInvoiceLines.size());
						  	System.out.println("arJobOrderInvoiceLines.size()="+arJobOrderInvoiceLines.size());
						  	
						  	
						  	if(+arSalesInvoiceLines.size() > 0){



							  	ArrayList arSalesInvoiceLineList = new ArrayList(arSalesInvoiceLines);
							  	LocalArInvoiceLine arSalesInvoiceLine = (LocalArInvoiceLine)arSalesInvoiceLineList.get(0);


							  	String smlName = arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription();

							  	if (!SML_NAME.equals("") && !SML_NAME.equals(smlName)){

							  		continue;

							  	}
								ArRepStatementDetails mdetails = new ArRepStatementDetails();

								if(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch() != null) {
									mdetails.setSmtInvoiceBatchName(arInvoice.getArInvoiceBatch().getIbName());
							  	}

								//PMA
								/*
								if(arInvoicePaymentSchedule.getArInvoice().getPmProject()!=null) {
									mdetails.setSmtPmProjectCode(arInvoice.getPmProject().getPrjProjectCode());
									mdetails.setSmtPmProjectType(arInvoice.getPmProjectTypeType().getPmProjectType().getPtValue());
									mdetails.setSmtPmProjectName(arInvoice.getPmProject().getPrjName());
									mdetails.setSmtPmContractPrice(arInvoice.getPmContractTerm().getPmContract().getCtrPrice());
									mdetails.setSmtPmContractValue(arInvoice.getPmContractTerm().getCtValue());
									mdetails.setSmtPmContractTermName(arInvoice.getPmContractTerm().getCtTermDescription());


								}
								*/

								mdetails.setSmtCustomerCode(arCustomer.getCstCustomerCode());
								mdetails.setSmtCstCustomerID(arCustomer.getCstWordPressCustomerID());
								mdetails.setSmtCstEmployeeID(arCustomer.getCstEmployeeID());
								mdetails.setSmtCstAccountNumber(arCustomer.getCstAccountNumber());
								mdetails.setSmtCustomerBatch(arCustomer.getCstCustomerBatch());
								mdetails.setSmtCustomerDepartment(arCustomer.getCstCustomerDepartment());
								mdetails.setSmtCustomerEntryDate(arCustomer.getCstEntryDate());
								mdetails.setSmtCustomerName(arCustomer.getCstName());
								mdetails.setSmtCustomerAddress(arCustomer.getCstAddress());
								mdetails.setSmtCstContact(arCustomer.getCstContact());
								mdetails.setSmtCstTin(arCustomer.getCstTin());
								mdetails.setSmtCstSquareMeter(arCustomer.getCstSquareMeter());
								mdetails.setSmtCstParkingID(arCustomer.getCstParkingID());
								mdetails.setSmtCstEML(arCustomer.getCstEmail());
								mdetails.setSmtCstCustomerID(arCustomer.getCstWordPressCustomerID());
								mdetails.setSmtCstEmployeeID(arCustomer.getCstEmployeeID());
								mdetails.setSmtCstAccountNumber(arCustomer.getCstAccountNumber());


								mdetails.setSmtTransactionType("INVOICE");
								mdetails.setSmtPosted(arInvoice.getInvPosted());
								mdetails.setSmtTransaction(arInvoice.getInvNumber());
								mdetails.setSmtReferenceNumber(arInvoice.getInvReferenceNumber());


								mdetails.setSmtLastDueDate(lastDueDate);
								mdetails.setSmtDueDate(arInvoicePaymentSchedule.getIpsDueDate());
								mdetails.setSmtInvoiceAmountDue(arInvoice.getInvAmountDue());
								mdetails.setSmtInvoiceDownPayment(arInvoice.getInvDownPayment());
								mdetails.setSmtInvoiceUnearnedInterest(arInvoice.getInvAmountUnearnedInterest());


								mdetails.setSmtPaymentTerm(arInvoice.getAdPaymentTerm().getAdPaymentSchedules().size());
								mdetails.setSmtPaymentTermName(arInvoice.getAdPaymentTerm().getPytName());

								mdetails.setSmtDate(arInvoice.getInvDate());
								mdetails.setSmtDescription(arInvoice.getInvDescription());
								mdetails.setSmtTaxCode(arInvoice.getArTaxCode().getTcName());

								double netAmount = EJBCommon.roundIt((arInvoice.getInvAmountDue()) / (1 + (arInvoice.getArTaxCode().getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));
								double wtaxAmount = EJBCommon.roundIt(netAmount * (arInvoice.getArWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));
								mdetails.setSmtWithholdingTaxAmount(wtaxAmount);

								try {
									mdetails.setSmtBatchName(arInvoice.getArInvoiceBatch().getIbName());
								} catch (Exception ex){ mdetails.setSmtBatchName("");}

								mdetails.setSmtItemDescription(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription());
								mdetails.setSmtSmlWpProductID(arSalesInvoiceLine.getArStandardMemoLine().getSmlWordPressProductID());
								mdetails.setSmtPreviousReading(arSalesInvoiceLine.getArInvoice().getInvPreviousReading());
								mdetails.setSmtPresentReading(arSalesInvoiceLine.getArInvoice().getInvPresentReading());
								mdetails.setSmtQuantity(arSalesInvoiceLine.getIlQuantity());
								mdetails.setSmtCurrency(arSalesInvoiceLine.getArInvoice().getGlFunctionalCurrency().getFcName());
								mdetails.setSmtUnitPrice(arSalesInvoiceLine.getIlUnitPrice());


								double amountDue = arInvoicePaymentSchedule.getIpsAmountDue() + arInvoicePaymentSchedule.getIpsPenaltyDue() ;
								double mo_ttlAmount = arInvoicePaymentSchedule.getIpsAmountDue() ;

								mdetails.setSmtNumber(arInvoicePaymentSchedule.getIpsNumber());
								mdetails.setSmtAmount(mo_ttlAmount);
								mdetails.setSmtPenalty(arInvoicePaymentSchedule.getIpsPenaltyDue());
								mdetails.setSmtAmountDue(amountDue);
								mdetails.setSmtIpsAmountDue(arInvoicePaymentSchedule.getIpsAmountDue());


								Collection arAppliedInvoices = arInvoicePaymentSchedule.getArAppliedInvoices();
								System.out.println("arAppliedInvoices="+arAppliedInvoices.size());
								Iterator x = arAppliedInvoices.iterator();

								double appliedAmount = 0d;
								String OR_NUM = "";

							  	 while(x.hasNext()){

							  		 LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)x.next();

							  		 if(arAppliedInvoice.getArReceipt() == null) continue;

							  		 int y = arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate);

							  		 if(arAppliedInvoice.getArReceipt().getRctVoid() == 1
							  				|| (arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate) > 0) ) continue;

							  		 OR_NUM += arAppliedInvoice.getArReceipt().getRctNumber() + ":";
							  		 appliedAmount += arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiRebate();
							  		 mdetails.setSmtLastOrDate(arAppliedInvoice.getArReceipt().getRctDate());

							  	 }

								 mdetails.setSmtOrNumber(OR_NUM);
								 mdetails.setSmtInvoiceNumber(arInvoice.getInvNumber());
							  	 mdetails.setSmtAmountPaid(appliedAmount);
							  	 mdetails.setSmtInterestDue(arInvoicePaymentSchedule.getIpsInterestDue());

								 mdetails.setSmtCreditAmount(creditAmount);
								 mdetails.setSmtRemainingBalance(amountDue - appliedAmount);
								 mdetails.setSmtDebit(arInvoicePaymentSchedule.getIpsAmountDue());
								 mdetails.setSmtCredit(creditAmount);

								 CM_AMNT = amountDue - creditAmount - appliedAmount;

								 if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Association Dues") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("ASD")){

									 CM_AMNT_ASD = CM_AMNT;

								 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("RPT")){

									 CM_AMNT_RPT = CM_AMNT;

								 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Parking Dues") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("PD")){

									 CM_AMNT_PD = CM_AMNT;

								 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Water") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("WTR")){

									 CM_AMNT_WTR = CM_AMNT;

								 } else {

									 CM_AMNT_MISC = CM_AMNT;

								 }


								 if (INVOICE_AGE <= 0) {

									 mdetails.setSmtBucket0(CM_AMNT);
									 mdetails.setSmtBucketASD0(CM_AMNT_ASD);
									 mdetails.setSmtBucketRPT0(CM_AMNT_RPT);
									 mdetails.setSmtBucketPD0(CM_AMNT_PD);
									 mdetails.setSmtBucketWTR0(CM_AMNT_WTR);
									 mdetails.setSmtBucketMISC0(CM_AMNT_MISC);

								  } else if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {

									mdetails.setSmtBucket1(CM_AMNT);
									 mdetails.setSmtBucketASD1(CM_AMNT_ASD);
									 mdetails.setSmtBucketRPT1(CM_AMNT_RPT);
									 mdetails.setSmtBucketPD1(CM_AMNT_PD);
									 mdetails.setSmtBucketWTR1(CM_AMNT_WTR);
									 mdetails.setSmtBucketMISC1(CM_AMNT_MISC);


								  } else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {

									mdetails.setSmtBucket2(CM_AMNT);
									 mdetails.setSmtBucketASD2(CM_AMNT_ASD);
									 mdetails.setSmtBucketRPT2(CM_AMNT_RPT);
									 mdetails.setSmtBucketPD2(CM_AMNT_PD);
									 mdetails.setSmtBucketWTR2(CM_AMNT_WTR);
									 mdetails.setSmtBucketMISC2(CM_AMNT_MISC);

								  } else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {

									mdetails.setSmtBucket3(CM_AMNT);
									 mdetails.setSmtBucketASD3(CM_AMNT_ASD);
									 mdetails.setSmtBucketRPT3(CM_AMNT_RPT);
									 mdetails.setSmtBucketPD3(CM_AMNT_PD);
									 mdetails.setSmtBucketWTR3(CM_AMNT_WTR);
									 mdetails.setSmtBucketMISC3(CM_AMNT_MISC);

								  } else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {

									mdetails.setSmtBucket4(CM_AMNT);
									 mdetails.setSmtBucketASD4(CM_AMNT_ASD);
									 mdetails.setSmtBucketRPT4(CM_AMNT_RPT);
									 mdetails.setSmtBucketPD4(CM_AMNT_PD);
									 mdetails.setSmtBucketWTR4(CM_AMNT_WTR);
									 mdetails.setSmtBucketMISC4(CM_AMNT_MISC);

								  } else if (INVOICE_AGE > agingBucket4) {

									mdetails.setSmtBucket5(CM_AMNT);
									 mdetails.setSmtBucketASD5(CM_AMNT_ASD);
									 mdetails.setSmtBucketRPT5(CM_AMNT_RPT);
									 mdetails.setSmtBucketPD5(CM_AMNT_PD);
									 mdetails.setSmtBucketWTR5(CM_AMNT_WTR);
									 mdetails.setSmtBucketMISC5(CM_AMNT_MISC);

								  }



								 list.add(mdetails);
						  	}
						  	
						  	


						  	//collections
						    if (includeCollection){

						    	//  Collection arAppliedInvoices = arInvoicePaymentSchedule.getArAppliedInvoices();


						    	  System.out.println("arAppliedInvoices.size()="+appliedInvoices.size());


								  Iterator j = appliedInvoices.iterator();

								  while (j.hasNext()) {

									  LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice) j.next();

									  ArRepStatementDetails mdetails = new ArRepStatementDetails();

									  if(arAppliedInvoice.getArReceipt().getRctVoid() == 1
								  				|| (arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate) > 0) ) continue;
								  		System.out.println("---------------------->");

									  mdetails.setSmtCustomerBatch(arInvoice.getArCustomer().getCstCustomerBatch());
									  mdetails.setSmtCustomerDepartment(arInvoice.getArCustomer().getCstCustomerDepartment());


									  if(arInvoice.getArInvoiceBatch() != null) {
										  mdetails.setSmtInvoiceBatchName(arInvoice.getArInvoiceBatch().getIbName());
									  }

									  mdetails.setSmtInvoiceNumber(arInvoice.getInvNumber());
									  mdetails.setSmtCustomerCode(arCustomer.getCstCustomerCode());

									  mdetails.setSmtCstCustomerID(arCustomer.getCstWordPressCustomerID());
									  mdetails.setSmtCstEmployeeID(arCustomer.getCstEmployeeID());
									  mdetails.setSmtCstAccountNumber(arCustomer.getCstAccountNumber());
									  mdetails.setSmtCustomerBatch(arCustomer.getCstCustomerBatch());
									  mdetails.setSmtCustomerDepartment(arCustomer.getCstCustomerDepartment());
									  mdetails.setSmtCustomerEntryDate(arCustomer.getCstEntryDate());
									  mdetails.setSmtCustomerName(arCustomer.getCstName());
									  mdetails.setSmtCustomerAddress(arCustomer.getCstAddress());
									  mdetails.setSmtTransaction(arAppliedInvoice.getArReceipt().getRctNumber());
									  mdetails.setSmtPosted(arAppliedInvoice.getArReceipt().getRctPosted());
									  mdetails.setSmtTransactionType("RECEIPT");
									  mdetails.setSmtDate(arAppliedInvoice.getArReceipt().getRctDate());
									  mdetails.setSmtDescription(arAppliedInvoice.getArReceipt().getRctDescription());
									  mdetails.setSmtDebit(0d);

									  double appliedAmount = arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiRebate();


									  mdetails.setSmtCredit(appliedAmount);

									  list.add(mdetails);

								  }


			    	        }


    				  }








			  	 }


        		  if (list.isEmpty()) {
        			  System.out.println("no record found");
        		  	  throw new GlobalNoRecordFoundException();

        		  }
    	      }

    	      if (REPORT_TYP.equals("CUSTOMER LEDGER")){
    			  Collections.sort(list, ArRepStatementDetails.CustomerLedgerComparator);

    		  } else if (REPORT_TYP.equals("ACCOUNT SUMMARY")){
    			  Collections.sort(list, ArRepStatementDetails.AccountSummaryComparator);


    		  } else if (ORDER_BY.contains("DUE DATE")){
    			  Collections.sort(list, ArRepStatementDetails.DueDateComparator);

    		  } else {
    			  System.out.println("ArRepStatementDetails.DateComparator");
    			  Collections.sort(list, ArRepStatementDetails.DateComparator);
    		  }


    		  return list;




	      } catch (GlobalNoRecordFoundException ex) {

	    	  throw ex;


	      } catch (Exception ex) {

			    Debug.printStackTrace(ex);
			    throw new EJBException(ex.getMessage());

	      }






    }





    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepStatement(HashMap criteria, String SML_NAME, String customerCode, boolean includeUnpostedTransaction, boolean showUnearnedInterestTransactionOnly, boolean includeAdvance, boolean includeAdvanceOnly, boolean showAll, boolean includeZero, boolean includeCollection, boolean includeUnpostedInvoice, boolean includeCreditMemo, String ORDER_BY, String GROUP_BY, String REPORT_TYP, ArrayList customerBatchList, ArrayList customerDepartmentList, ArrayList branchList, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArRepStatementControllerBean executeArRepStatement");

        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalArCustomerBalanceHome arCustomerBalanceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
        LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
        LocalArInvoiceLineHome arSalesInvoiceLineHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalCmAdjustmentHome cmAdjustmentHome = null;

        /*
        LocalPmProjectHome pmProjectHome = null;
        LocalPmContractHome pmContractHome = null;
        LocalPmContractTermHome pmContractTermHome = null;
        LocalPmProjectTypeHome pmProjectTypeHome = null;
        LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;
        LocalPmProjectPhaseHome pmProjectPhaseHome = null;
			*/
        ArrayList list = new ArrayList();
        Date agingDate = null;

        //initialized EJB Home

        try {
        	/*
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
        	pmContractHome = (LocalPmContractHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmContractHome.JNDI_NAME, LocalPmContractHome.class);
        	pmContractTermHome = (LocalPmContractTermHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);
        	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
        	pmProjectPhaseHome = (LocalPmProjectPhaseHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectPhaseHome.JNDI_NAME, LocalPmProjectPhaseHome.class);
        	pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
	*/

            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

            arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);

            arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);

            arSalesInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

            arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                    lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);


        } catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

		try {

		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		  short precisionUnit = adCompany.getGlFunctionalCurrency().getFcPrecision();


		  LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(adCompany.getCmpCode());
		  int agingBucket1 = adPreference.getPrfArAgingBucket();
		  int agingBucket2 = adPreference.getPrfArAgingBucket() * 2;
		  int agingBucket3 = adPreference.getPrfArAgingBucket() * 3;
		  int agingBucket4 = adPreference.getPrfArAgingBucket() * 4;

		  boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();

		  StringBuffer jbossQl = new StringBuffer();
		  StringBuffer jbossQlAdv = new StringBuffer();
		  StringBuffer jbossQlAdvPaid = new StringBuffer();

		  jbossQl.append("SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips ");
		  jbossQlAdv.append("SELECT OBJECT(adj) FROM CmAdjustment adj ");
		  jbossQlAdvPaid.append("SELECT OBJECT(ai) FROM ArAppliedInvoice ai ");

		  if (branchList.isEmpty())
		  	throw new GlobalNoRecordFoundException();


		  if (!firstArgument) {

 	  	    jbossQl.append("AND (");
 	  	    jbossQlAdv.append("AND (");
 	  	   jbossQlAdvPaid.append("AND (");

 	     } else {

 	     	firstArgument = false;
 	     	jbossQl.append("WHERE (");
 	     	jbossQlAdv.append("WHERE (");
 	     	jbossQlAdvPaid.append("WHERE (");

 	     }


		  Iterator brIter = branchList.iterator();

		  AdBranchDetails details = (AdBranchDetails)brIter.next();
		  jbossQl.append("ips.arInvoice.invAdBranch=" + details.getBrCode());
		  jbossQlAdv.append("adj.adjAdBranch=" + details.getBrCode());
		  jbossQlAdvPaid.append("ai.arReceipt.rctAdBranch=" + details.getBrCode());

		  while(brIter.hasNext()) {

		  		details = (AdBranchDetails)brIter.next();

		  		jbossQl.append(" OR ips.arInvoice.invAdBranch=" + details.getBrCode());
		  		jbossQlAdv.append(" OR adj.adjAdBranch=" + details.getBrCode());
		  		jbossQlAdvPaid.append(" OR ai.arReceipt.rctAdBranch=" + details.getBrCode());

		  }

		  jbossQl.append(") ");
		  jbossQlAdv.append(") ");
		  jbossQlAdvPaid.append(") ");




		  if (!customerBatchList.isEmpty()) {


			  if (!firstArgument) {

		 	  	    jbossQl.append("AND ");
		 	  	    jbossQlAdv.append("AND ");
		 	  	   jbossQlAdvPaid.append("AND ");

		 	     } else {

		 	     	firstArgument = false;
		 	     	jbossQl.append("WHERE ");
		 	     	jbossQlAdv.append("WHERE ");
		 	     	jbossQlAdvPaid.append("WHERE ");

		 	     }


			  Iterator j = customerBatchList.iterator();


			  jbossQl.append("ips.arInvoice.arCustomer.cstCustomerBatch in (");
		 	  jbossQlAdv.append("adj.arCustomer.cstCustomerBatch in (");
		 	  jbossQlAdvPaid.append("ai.arReceipt.arCustomer.cstCustomerBatch in (");


		 	  boolean firstLoop = true;


		 	  while(j.hasNext()) {

		  		if(firstLoop == false) {
		  			jbossQl.append(", ");
		  		}
		  		else {
		  			firstLoop = false;
		  		}


		  		String CSTMR_BTCH = (String) j.next();

		  		jbossQl.append("'"+CSTMR_BTCH+"'");
		  		jbossQlAdv.append("'"+CSTMR_BTCH+"'");
		  		jbossQlAdvPaid.append("'"+CSTMR_BTCH+"'");


			  	}

			  	jbossQl.append(") ");
			  	jbossQlAdv.append(") ");
			  	jbossQlAdvPaid.append(") ");



		  }

		  if (!customerDepartmentList.isEmpty()) {


			  if (!firstArgument) {

		 	  	    jbossQl.append("AND ");
		 	  	    jbossQlAdv.append("AND ");
		 	  	   jbossQlAdvPaid.append("AND ");

		 	     } else {

		 	     	firstArgument = false;
		 	     	jbossQl.append("WHERE ");
		 	     	jbossQlAdv.append("WHERE ");
		 	     	jbossQlAdvPaid.append("WHERE ");

		 	     }

			  Iterator j = customerDepartmentList.iterator();


			  jbossQl.append("ips.arInvoice.arCustomer.cstCustomerDepartment in (");
		 	  jbossQlAdv.append("adj.arCustomer.cstCustomerDepartment in (");
		 	  jbossQlAdvPaid.append("ai.arReceipt.arCustomer.cstCustomerDepartment in (");


		 	  boolean firstLoop = true;


		 	  while(j.hasNext()) {

		  		if(firstLoop == false) {
		  			jbossQl.append(", ");
		  		}
		  		else {
		  			firstLoop = false;
		  		}


		  		String CSTMR_DEP = (String) j.next();

		  		jbossQl.append("'"+CSTMR_DEP+"'");
		  		jbossQlAdv.append("'"+CSTMR_DEP+"'");
		  		jbossQlAdvPaid.append("'"+CSTMR_DEP+"'");


			  	}

			  	jbossQl.append(") ");
			  	jbossQlAdv.append(") ");
			  	jbossQlAdvPaid.append(") ");



		  }


		  if (!customerCode.equals("")) {

		 	  	 if (!firstArgument) {

		 	  	    jbossQl.append("AND ");
		 	  	    jbossQlAdv.append("AND ");
		 	  	   jbossQlAdvPaid.append("AND ");

		 	     } else {

		 	     	firstArgument = false;
		 	     	jbossQl.append("WHERE ");
		 	     	jbossQlAdv.append("WHERE ");
		 	     	jbossQlAdvPaid.append("WHERE ");

		 	     }

		 	  	 jbossQl.append("ips.arInvoice.arCustomer.cstCustomerCode = '" + customerCode + "' ");
		 	  	jbossQlAdv.append("adj.arCustomer.cstCustomerCode = '" + customerCode + "' ");
		 	  	jbossQlAdvPaid.append("ai.arReceipt.arCustomer.cstCustomerCode = '" + customerCode + "' ");

	 	  }

		  //include unposted

		  if (!firstArgument) {

 	  	    jbossQl.append("AND ");
 	  	    jbossQlAdv.append("AND ");
 	  	   jbossQlAdvPaid.append("AND ");

 	     } else {

 	     	firstArgument = false;
 	     	jbossQl.append("WHERE ");
 	     	jbossQlAdv.append("WHERE ");
 	     	jbossQlAdvPaid.append("WHERE ");

 	     }

		  if (includeUnpostedTransaction) {

			  jbossQl.append("ips.arInvoice.invVoid = 0 ");
	       	  jbossQlAdv.append("adj.adjPosted = 1 ");
	       	  jbossQlAdvPaid.append("ai.arReceipt.rctPosted = 1 ");



	      }else {
	   	      jbossQl.append("");
	    	  jbossQl.append("ips.arInvoice.invVoid = 0 AND ips.arInvoice.invPosted = 1 ");
       	      jbossQlAdv.append("adj.adjVoid = 0 ");
                    jbossQlAdvPaid.append("ai.arReceipt.rctVoid = 0 ");
	      }


		  //showUnearnedInterestTransactionOnly


		  if (showUnearnedInterestTransactionOnly) {

			  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  }
			  jbossQl.append("ips.arInvoice.invAmountUnearnedInterest > 0 ");



	      }










		  firstArgument = false;

	      // Allocate the size of the object parameter



	      Object obj[] = new Object[criteriaSize];



	      if (criteria.containsKey("customerType")) {

		   	  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	     jbossQlAdv.append("AND ");
		   	     jbossQlAdvPaid.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 jbossQlAdv.append("WHERE ");
		   	  	 jbossQlAdvPaid.append("WHERE ");
		   	  }

		   	  jbossQl.append("ips.arInvoice.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  jbossQlAdv.append("adj.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  jbossQlAdvPaid.append("ai.arReceipt.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerType");
		   	  ctr++;

	      }

	      if (criteria.containsKey("customerClass")) {

		   	  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	     jbossQlAdv.append("AND ");
		   	     jbossQlAdvPaid.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  	 jbossQlAdv.append("WHERE ");
		   	  jbossQlAdvPaid.append("WHERE ");
		   	  }

		   	  jbossQl.append("ips.arInvoice.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  jbossQlAdv.append("adj.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  jbossQlAdvPaid.append("ai.arReceipt.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerClass");
		   	  ctr++;

	      }

	      if (criteria.containsKey("dateFrom")) {

				 if (!firstArgument) {
				 	jbossQl.append("AND ");
				 	jbossQlAdv.append("AND ");
				 	jbossQlAdvPaid.append("AND ");
				 } else {
				 	firstArgument = false;
				 	jbossQl.append("WHERE ");
				 	jbossQlAdv.append("WHERE ");
				 	jbossQlAdvPaid.append("WHERE ");
				 }
				 jbossQl.append("ips.arInvoice.invDate>=?" + (ctr+1) + " ");
				 jbossQlAdv.append("adj.adjDate>=?" + (ctr+1) + " ");
				jbossQlAdvPaid.append("ai.arReceipt.rctDate>=?" + (ctr+1) + " ");

				 obj[ctr] = (Date)criteria.get("dateFrom");

				 ctr++;
			}




	      if (criteria.containsKey("dateTo")) {
	    	  System.out.println("DATE IS :" + criteria.get("dateTo"));
			 	 if (!firstArgument) {
			 	 	jbossQl.append("AND ");
			 	 	jbossQlAdv.append("AND ");
			 	 	jbossQlAdvPaid.append("AND ");
			 	 } else {
			 	 	firstArgument = false;
			 	 	jbossQl.append("WHERE ");
			 	 	jbossQlAdv.append("WHERE ");
			 	 	jbossQlAdvPaid.append("WHERE ");
			 	 }
			 	 jbossQl.append("ips.arInvoice.invDate<=?" + (ctr+1) + " ");
			 	 jbossQlAdv.append("adj.adjDate<=?" + (ctr+1) + " ");
			 	jbossQlAdvPaid.append("ai.arReceipt.rctDate<=?" + (ctr+1) + " ");

			 	 obj[ctr] = (Date)criteria.get("dateTo");
			 	 agingDate = (Date)criteria.get("dateTo");
			 	 ctr++;
			 }

		  if (criteria.containsKey("invoiceNumberFrom")) {

			  if (!firstArgument) {
				  jbossQl.append("AND ");
			  } else {
				  firstArgument = false;
				  jbossQl.append("WHERE ");
			  }
			  jbossQl.append("ips.arInvoice.invNumber>=?" + (ctr+1) + " ");
			  obj[ctr] = (String)criteria.get("invoiceNumberFrom");
			  ctr++;

		  }

		  if (criteria.containsKey("invoiceNumberTo")) {

			  if (!firstArgument) {
				  jbossQl.append("AND ");
			  } else {
				  firstArgument = false;
				  jbossQl.append("WHERE ");
			  }
			  jbossQl.append("ips.arInvoice.invNumber<=?" + (ctr+1) + " ");
			  obj[ctr] = (String)criteria.get("invoiceNumberTo");
			  ctr++;

		  }

		  if (criteria.containsKey("invoiceBatchName")) {

		   	  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  }

		   	  jbossQl.append("ips.arInvoice.arInvoiceBatch.ibName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("invoiceBatchName");
		   	  ctr++;

	      }



		  if (criteria.containsKey("customerBatch")) {

		 	  if (!firstArgument) {
		 	     jbossQl.append("AND ");
		 	     jbossQlAdv.append("AND ");
		 	     jbossQlAdvPaid.append("AND ");
		 	  } else {
		 	  	 firstArgument = false;
		 	  	 jbossQl.append("WHERE ");
		 	  	 jbossQlAdv.append("WHERE ");
		 	  	 jbossQlAdvPaid.append("WHERE ");
		 	  }

		 	  jbossQl.append("ips.arInvoice.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
		 	  jbossQlAdv.append("adj.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
		 	  jbossQlAdvPaid.append("ai.arReceipt.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
		 	  obj[ctr] = (String)criteria.get("customerBatch");
		 	  ctr++;

		}






	      if (!firstArgument) {

	   	     jbossQl.append("AND ");
	   	     jbossQlAdv.append("AND ");
	   	  } else {

	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");
	   	  	 jbossQlAdv.append("WHERE ");

	   	  }

	      if (showAll){

	    	  jbossQl.append("ips.arInvoice.invVoid=0 AND ips.arInvoice.invCreditMemo = 0 AND ips.ipsAdCompany=" + AD_CMPNY + " ORDER BY ips.arInvoice.arCustomer.cstName, ips.arInvoice.invDate, ips.ipsNumber");
	    	  jbossQlAdv.append("adj.adjVoid = 0 AND adj.adjType = 'ADVANCE' AND adj.adjAdCompany=" + AD_CMPNY);

	      }else if(includeZero){

	    	  jbossQl.append("ips.arInvoice.invVoid=0 AND (ips.ipsAmountDue <= ips.ipsAmountPaid OR ips.ipsAmountDue >= ips.ipsAmountPaid) AND ips.arInvoice.invCreditMemo = 0 AND ips.ipsAdCompany=" + AD_CMPNY + " ORDER BY ips.arInvoice.arCustomer.cstName, ips.arInvoice.invDate, ips.ipsNumber");
	    	  jbossQlAdv.append("adj.adjVoid = 0 AND adj.adjType = 'ADVANCE' AND adj.adjAdCompany=" + AD_CMPNY);

	      }else {

	    	  jbossQl.append("ips.arInvoice.invVoid=0 AND (ips.ipsAmountDue < ips.ipsAmountPaid OR ips.ipsAmountDue > ips.ipsAmountPaid) AND ips.arInvoice.invCreditMemo = 0 AND ips.ipsAdCompany=" + AD_CMPNY + " ORDER BY ips.arInvoice.arCustomer.cstName, ips.arInvoice.invDate, ips.ipsNumber");
	    	  jbossQlAdv.append("adj.adjVoid = 0 AND adj.adjType = 'ADVANCE' AND adj.adjAdCompany=" + AD_CMPNY);
	      }





	 //   System.out.println("-----size i----" + arInvoicePaymentSchedules.size());
	     // String includeAdvance = (String)criteria.get("includeAdvance");
	    //  String includeAdvanceOnly = (String)criteria.get("includeAdvanceOnly");

	      if(includeAdvance ||includeAdvanceOnly){

	    	  Collection advancePayments = cmAdjustmentHome.getAdjByCriteria(jbossQlAdv.toString(), obj);


		      Iterator x = advancePayments.iterator();

		      while (x.hasNext()) {

		    	  LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)x.next();

		    	  //System.out.println("cmAdjustment="+cmAdjustment.getAdjDocumentNumber());
		    	  ArRepStatementDetails mdetails = new ArRepStatementDetails();
		    	  mdetails.setSmtCstCustomerID(cmAdjustment.getArCustomer().getCstWordPressCustomerID());
		    	  mdetails.setSmtCstEmployeeID(cmAdjustment.getArCustomer().getCstEmployeeID());
		    	  mdetails.setSmtCstAccountNumber(cmAdjustment.getArCustomer().getCstAccountNumber());
		    	  mdetails.setSmtCstSquareMeter(cmAdjustment.getArCustomer().getCstSquareMeter());
		    	  mdetails.setSmtCustomerEntryDate(cmAdjustment.getArCustomer().getCstEntryDate());
		    	  mdetails.setSmtCstParkingID(cmAdjustment.getArCustomer().getCstParkingID());
		    	  mdetails.setSmtCustomerBatch(cmAdjustment.getArCustomer().getCstCustomerBatch());
		    	  mdetails.setSmtCustomerDepartment(cmAdjustment.getArCustomer().getCstCustomerDepartment());

		    	  mdetails.setSmtCustomerCode(cmAdjustment.getArCustomer().getCstCustomerCode());

	  	  	      mdetails.setSmtCustomerName(cmAdjustment.getArCustomer().getCstName());
	  	  	      mdetails.setSmtCstEML(cmAdjustment.getArCustomer().getCstEmail());
	  	  	      mdetails.setSmtDate(cmAdjustment.getAdjDate());
	  	  	      mdetails.setSmtDueDate(cmAdjustment.getAdjDate());
	  	  	      mdetails.setSmtTransactionType("ADVANCE");

	  	  	      mdetails.setSmtTransaction(cmAdjustment.getAdjDocumentNumber());
	  	  	      mdetails.setSmtPosted(cmAdjustment.getAdjPosted());

	  	  	      
	  	  	      
	  	  	   
	  	  	      // get applied credit

	              Collection arAppliedCredits = cmAdjustment.getArAppliedCredits();

	              Iterator y = arAppliedCredits.iterator();

	              double totalAppliedCredit = 0d;

	              while(y.hasNext()){

	              	LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)y.next();

	              	totalAppliedCredit += arAppliedCredit.getAcApplyCredit();

	              }


	  	  	      double ADV_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
	  	  	    			cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(),
	  	  	    			cmAdjustment.getAdjConversionDate(),
	  	  	    			cmAdjustment.getAdjConversionRate(),
	  	  	    		cmAdjustment.getAdjAmount() - totalAppliedCredit- cmAdjustment.getAdjRefundAmount(), AD_CMPNY) ;

	  	  	      if ( !includeZero && (!showAll && ADV_AMNT == 0d)) continue;
	  	  	 //     if (includeCollection && ADV_AMNT == 0d) continue;

	  	  	      
	  	  	  

	  	  	   
	  	  	      
	  	  	      
	  	  	      
	  	  	      
	  	  	      try{
	  	  	    	  SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MMMMM yyyy");
		  	  	      String date = DATE_FORMAT.format(cmAdjustment.getAdjDate());
		  	  	      mdetails.setSmtBatchName(date);
	  	  	      }catch(Exception ex){
	  	  	    	mdetails.setSmtBatchName("");
	  	  	      }

	  	  	      mdetails.setSmtOrNumber(cmAdjustment.getAdjDocumentNumber());
	  	  	      mdetails.setSmtRemainingBalance(ADV_AMNT * -1);
	  	  	      
	  	  	      mdetails.setSmtDescription(cmAdjustment.getAdjMemo().equals("")?"ADVANCE PAYMENT / CUSTOMER DEPOSIT":cmAdjustment.getAdjMemo());
	  	  	     // mdetails.setSmtDescription("ADVANCE PAYMENT / CUSTOMER DEPOSIT");
	  	  	      mdetails.setSmtAmountPaid(ADV_AMNT );
	  	  	      mdetails.setSmtItemDescription("ADVANCE");
	  	  	      mdetails.setSmtAmount(0d);
	  	  	      mdetails.setSmtAmountDue(ADV_AMNT * -1 );
	  	  	      mdetails.setSmtIpsAmountDue(cmAdjustment.getAdjAmount()* -1);
	  	  	      mdetails.setSmtDebit(0d);
	  	  	      mdetails.setSmtCredit(cmAdjustment.getAdjAmount());
	  	  	      mdetails.setSmtCreditAmount(0d);
	  	  	      mdetails.setSmtTaxAmount(0d);
	  	  	      mdetails.setSmtAmountDetails(ADV_AMNT * -1);
	  	  	  System.out.println("ADJUSMENT AMOUNT: " + cmAdjustment.getAdjAmount());
              System.out.println("TOTAL APPLIED CREDIT: " + totalAppliedCredit);
              System.out.println("REFUND AMOUNT: " + cmAdjustment.getAdjRefundAmount());
              


	  	  	      	int INVOICE_AGE = (short)((agingDate.getTime() -
	  	  	    	cmAdjustment.getAdjDate().getTime()) / (1000 * 60 * 60 * 24));


	  	  	      	if (INVOICE_AGE <= 0) {

	  	  	      		mdetails.setSmtBucketADV0(ADV_AMNT * -1);

	  	  	      	} else if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {

	  	  	      		mdetails.setSmtBucketADV1(ADV_AMNT * -1);

	  	  	      	} else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {

	  	  	      		mdetails.setSmtBucketADV2(ADV_AMNT * -1);

	  	  	      	} else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {

	  	  	      		mdetails.setSmtBucketADV3(ADV_AMNT * -1);

	  	  	      	} else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {

	  	  	      		mdetails.setSmtBucketADV4(ADV_AMNT * -1);

	  	  	      	} else if (INVOICE_AGE > agingBucket4) {

	  	  	      		mdetails.setSmtBucketADV5(ADV_AMNT * -1);

	  	  	      	}
	  	  	      	
	  	  	      	
	  	  	      
	  	  	
	  	  	      	double TOTAL_REFUND = 0d;
	  	  	      	double TOTAL_ADV_PAID = 0d;
	  	  	      	
	  	  	      	//Credit Balance Refund
	  	  	      	
	  	  	      	if(cmAdjustment.getAdjRefundAmount()>0) {
	  	  	      		ArRepStatementDetails mdetailsAR = (ArRepStatementDetails)mdetails.clone();
					              	
					    TOTAL_REFUND +=   cmAdjustment.getAdjRefundAmount();        
	              	
		              	mdetailsAR.setSmtOrNumber(cmAdjustment.getAdjDocumentNumber());
		              	mdetailsAR.setSmtRemainingBalance(ADV_AMNT * -1);
		  	  	      
		  	  	  
		              	mdetailsAR.setSmtDescription(cmAdjustment.getAdjMemo().equals("")?"ADVANCE PAYMENT / CUSTOMER DEPOSIT":cmAdjustment.getAdjMemo());
		              	mdetailsAR.setSmtAmountPaid(ADV_AMNT );
		              	mdetailsAR.setSmtItemDescription("ADV. REFUND");
		              	mdetailsAR.setSmtAmount(0d);
		              	mdetailsAR.setSmtAmountDue(ADV_AMNT * -1 );
		              	mdetailsAR.setSmtIpsAmountDue(0);
		              	mdetailsAR.setSmtDebit(cmAdjustment.getAdjRefundAmount());
		              	mdetailsAR.setSmtCredit(0);
		              	mdetailsAR.setSmtCreditAmount(0d);
		              	mdetailsAR.setSmtTaxAmount(0d);
		              	mdetailsAR.setSmtAmountDetails(ADV_AMNT * -1);
		              	mdetailsAR.setSmtTransactionType("ADV.REFUND");
		              	mdetailsAR.setSmtTransaction(cmAdjustment.getAdjDocumentNumber() + "- Refund");
		             // 	list.add(mdetailsAR);
	  	  	      	}
		  	  	  
	  	  	      	
	  	  	      	
	  	  	      	//Credit balance amount applied 
		  	  	      


			  	  	  y = arAppliedCredits.iterator();
			  	  	  Integer numberIndex = 1;
		              while(y.hasNext()){
  
		              	LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)y.next();
	
		              	if(arAppliedCredit.getArAppliedInvoice()==null) {
		              	   

		              		continue;
		              	}
		              	ArRepStatementDetails mdetailsAP = (ArRepStatementDetails)mdetails.clone();
		             // 	System.out.println("ac = " + arAppliedCredit.getAcCode());
		            //  	System.out.println("ips = " + arAppliedCredit.getArAppliedInvoice().getArInvoicePaymentSchedule().getIpsCode());
		            //  	System.out.println("inv = " + arAppliedCredit.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice().getInvCode() );
		              	
		             	String INVOICE_NMBR = arAppliedCredit.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice().getInvNumber();
		             
		             	String RECEIPT_NMBR = arAppliedCredit.getArAppliedInvoice().getArReceipt().getRctNumber();
		              	
		           
		              	String desc =  INVOICE_NMBR + " - " + RECEIPT_NMBR;
		              	
		              	TOTAL_ADV_PAID += arAppliedCredit.getAcApplyCredit();
		              	mdetailsAP.setSmtOrNumber(cmAdjustment.getAdjDocumentNumber());
		              	mdetailsAP.setSmtRemainingBalance(ADV_AMNT * -1);
		  	  	      
		  	  	  
		              	mdetailsAP.setSmtDescription(desc);
		              	mdetailsAP.setSmtAmountPaid(ADV_AMNT );
		              	mdetailsAP.setSmtItemDescription("ADV. PAID");
		              	mdetailsAP.setSmtTransactionType("ADV. PAID");
		              	mdetailsAP.setSmtAmount(0d);
		              	mdetailsAP.setSmtAmountDue(ADV_AMNT * -1 );
		              	mdetailsAP.setSmtIpsAmountDue(cmAdjustment.getAdjAmount()* -1);
		              	mdetailsAP.setSmtDebit(arAppliedCredit.getAcApplyCredit());
		              	mdetailsAP.setSmtCredit(0);
		              	mdetailsAP.setSmtCreditAmount(0d);
		              	mdetailsAP.setSmtTaxAmount(0d);
		              	mdetailsAP.setSmtAmountDetails(ADV_AMNT * -1);
		              	mdetailsAP.setSmtTransaction(cmAdjustment.getAdjDocumentNumber() + "-" + numberIndex);
		              	
		              //list.add(mdetailsAP);
		              	numberIndex++;
		              }
		              
		              mdetails.setSmtCredit(cmAdjustment.getAdjAmount() - TOTAL_REFUND - TOTAL_ADV_PAID);
		              
		              list.add(mdetails); 
	  	  	      }

		      }

	      System.out.println("jbossQl.toString()="+jbossQl.toString());
	      /* if (arInvoicePaymentSchedules.size() == 0)
		     throw new GlobalNoRecordFoundException();
		  */
	      if(!includeAdvanceOnly){
	    	  System.out.println("includeAdvanceOnly="+includeAdvanceOnly);

			  String SMT_CSTMR_CODE = "";
			  String SMT_CSTMR_NM = null;
			  String SMT_CSTMR_ADDRSS = null;
			  String SMT_CST_BLLNG_FTR = null;
			  String SMT_CST_BLLNG_HDR = null;
			  String SMT_CST_BLLNG_SGNTRY = null;
			  String SMT_CST_SGNTRY_TTL = null;
			  String SMT_CST_CNTCT = null;
			  String SMT_CST_TIN = null;
			  String SMT_EML = null;
			  String SMT_TRM = null;
			  String SMT_CST_WP_CSTMR_ID = null;

			  Collection arInvoicePaymentSchedules = arInvoicePaymentScheduleHome.getIpsByCriteria(jbossQl.toString(), obj);

			  System.out.println("invoice arInvoicePaymentSchedules.size()="+arInvoicePaymentSchedules.size());
			  Date lastDueDate = null;

			  if(!arInvoicePaymentSchedules.isEmpty()) {

	            	ArrayList arInvoicePaymentScheduleList = new ArrayList(arInvoicePaymentSchedules);

	            	LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)arInvoicePaymentScheduleList.get(arInvoicePaymentScheduleList.size() - 1);

	            	lastDueDate = arInvoicePaymentSchedule.getIpsDueDate();

			  }





			  Iterator i = arInvoicePaymentSchedules.iterator();


			  while (i.hasNext()) {

			  	  LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();

			  	System.out.println("includeZero="+includeZero);
			  	System.out.println("arInvoicePaymentSchedule.getIpsAmountDue()="+EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue(), precisionUnit));
			  	System.out.println("arInvoicePaymentSchedule.getIpsAmountPaid()="+EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid(), precisionUnit));

			  	//if(EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue(), precisionUnit) == EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid(), precisionUnit)) continue;
			  	if (!includeZero && (!showAll &&
			  			EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue(), precisionUnit) ==
			  			EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountPaid(), precisionUnit))
			  			) {System.out.println("skip invoice : " + arInvoicePaymentSchedule.getArInvoice().getInvNumber() + " continue");continue; }


			  	double creditAmount=0d;
			  	try{
			  		System.out.println("CREDIT MEMO INVOICE="+arInvoicePaymentSchedule .getArInvoice().getInvNumber());

			  		Collection creditMemos = arInvoiceHome.findByCmInvoiceNumberCreditMemoAndCompanyCode(arInvoicePaymentSchedule.getArInvoice().getInvNumber(),arInvoicePaymentSchedule.getArInvoice().getInvAdBranch(), AD_CMPNY);

			  		
			  		System.out.println("cm size : "+ creditMemos.size());
			  		Iterator y = creditMemos.iterator();

			  		while(y.hasNext()){

			  			LocalArInvoice arInvoiceCreditMemo = (LocalArInvoice)y.next();
			  			System.out.println("CREDIT MEMO EXIST="+arInvoiceCreditMemo.getInvNumber());
			  			creditAmount += arInvoiceCreditMemo.getInvAmountDue();

			  		}
			  		System.out.println("CREDIT AMOUNT="+creditAmount);
			  		if(creditMemos.size() > 0 && EJBCommon.roundIt(creditAmount, precisionUnit)  == EJBCommon.roundIt(arInvoicePaymentSchedule.getArInvoice().getInvAmountDue(), precisionUnit) && !includeCreditMemo){
			  		
			  			System.out.println("pass because its fully credit memo");
			  			continue;
			  		}

			  	 }catch(Exception ex){

			  		System.out.println("ERROR");
			  	 }



			  	int INVOICE_AGE = (short)((agingDate.getTime() -
			            arInvoicePaymentSchedule.getIpsDueDate().getTime()) / (1000 * 60 * 60 * 24));
			  	System.out.println(INVOICE_AGE+"=INVOICE_AGE="+arInvoicePaymentSchedule.getIpsDueDate()) ;
			  	System.out.println(INVOICE_AGE+"=INVOICE_AGE="+arInvoicePaymentSchedule.getIpsDueDate()) ;
			  	

			  	//SALES ORDER INVOICE LINES

			  	int soSize = arInvoicePaymentSchedule.getArInvoice().getArSalesOrderInvoiceLines().size();
			  	int invoiceItemSize = arInvoicePaymentSchedule.getArInvoice().getArInvoiceLineItems().size();
			 	int joSize = arInvoicePaymentSchedule.getArInvoice().getArJobOrderInvoiceLines().size();
			// 	int joSize = 0;
			  	double SO_CM_AMNT = 0d;

			  	if(soSize!=0 || invoiceItemSize !=0 || joSize!=0){

			  	ArRepStatementDetails so_mdetails = new ArRepStatementDetails();
			  	

			  	if(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch() != null) {
			  		so_mdetails.setSmtInvoiceBatchName(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch().getIbName());
			  	}


		  		so_mdetails.setSmtCustomerCode(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode());

		  		so_mdetails.setSmtCstCustomerID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstWordPressCustomerID());
		  		so_mdetails.setSmtCstEmployeeID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEmployeeID());
		  		so_mdetails.setSmtCstAccountNumber(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstAccountNumber());
		  		so_mdetails.setSmtCustomerBatch(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerBatch());
		  		so_mdetails.setSmtCustomerDepartment(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerDepartment());

		  		so_mdetails.setSmtCustomerEntryDate(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEntryDate());
		  		so_mdetails.setSmtCustomerName(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstName());
		  		so_mdetails.setSmtCustomerAddress(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstAddress());
		  		so_mdetails.setSmtCstContact(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstContact());
		  		so_mdetails.setSmtCstTin(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstTin());
		  		so_mdetails.setSmtCstSquareMeter(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstSquareMeter());
		  		so_mdetails.setSmtCstParkingID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstParkingID());
			  	so_mdetails.setSmtCstEML(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEmail());
			  	
			  	so_mdetails.setSmtPosted(arInvoicePaymentSchedule.getArInvoice().getInvPosted());
			  	so_mdetails.setSmtPaymentTerm(arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getAdPaymentSchedules().size());
			  	so_mdetails.setSmtPaymentTermName(arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getPytName());

			  	so_mdetails.setSmtTransactionType("INVOICE");
			  	so_mdetails.setSmtTransaction(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
			  	so_mdetails.setSmtReferenceNumber(arInvoicePaymentSchedule.getArInvoice().getInvReferenceNumber());
			  	so_mdetails.setSmtCstCustomerID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstWordPressCustomerID());
			  	so_mdetails.setSmtCstEmployeeID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEmployeeID());
			  	so_mdetails.setSmtCstAccountNumber(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstAccountNumber());
			  	so_mdetails.setSmtDescription(arInvoicePaymentSchedule.getArInvoice().getInvDescription());
			  	so_mdetails.setSmtLastDueDate(lastDueDate);
			  	so_mdetails.setSmtDueDate(arInvoicePaymentSchedule.getIpsDueDate());
			  	so_mdetails.setSmtInvoiceAmountDue(arInvoicePaymentSchedule.getArInvoice().getInvAmountDue());
			  	so_mdetails.setSmtInvoiceDownPayment(arInvoicePaymentSchedule.getArInvoice().getInvDownPayment());
			  	so_mdetails.setSmtInvoiceUnearnedInterest(arInvoicePaymentSchedule.getArInvoice().getInvAmountUnearnedInterest());

			  	so_mdetails.setSmtInvoicePenaltyDue(arInvoicePaymentSchedule.getArInvoice().getInvPenaltyDue());
				 try {
					 so_mdetails.setSmtBatchName(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch().getIbName());

				 } catch (Exception ex){

					 so_mdetails.setSmtBatchName("");
				 }

				 so_mdetails.setSmtDate(arInvoicePaymentSchedule.getArInvoice().getInvDate());
				 so_mdetails.setSmtDescription(arInvoicePaymentSchedule.getArInvoice().getInvDescription());
				 so_mdetails.setSmtCurrency(arInvoicePaymentSchedule.getArInvoice().getGlFunctionalCurrency().getFcName());
				 so_mdetails.setSmtPaymentTerm(arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getAdPaymentSchedules().size());
				 so_mdetails.setSmtPaymentTermName(arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getPytName());

				 double amountDue = arInvoicePaymentSchedule.getIpsAmountDue() + arInvoicePaymentSchedule.getIpsPenaltyDue() ;
				 double mo_ttlAmount = arInvoicePaymentSchedule.getIpsAmountDue() ;

				 so_mdetails.setSmtNumber(arInvoicePaymentSchedule.getIpsNumber());
				 so_mdetails.setSmtAmount(mo_ttlAmount);
				 so_mdetails.setSmtPenalty(arInvoicePaymentSchedule.getIpsPenaltyDue());
				 so_mdetails.setSmtAmountDue(amountDue);
				 so_mdetails.setSmtIpsAmountDue(arInvoicePaymentSchedule.getIpsAmountDue());
				 Collection arAppliedInvoices = arInvoicePaymentSchedule.getArAppliedInvoices();

			  	 Iterator xx = arAppliedInvoices.iterator();

			  	 Double appliedAmount = 0d;
			  	String OR_NUM = "";

			  	 while(xx.hasNext()){

			  		 LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)xx.next();

			  		int y = arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate);

			  		System.out.println("RCT DATE="+arAppliedInvoice.getArReceipt().getRctDate());
			  		System.out.println("AGING DATE="+agingDate);
			  		System.out.println("Y===="+y);



			  		if(arAppliedInvoice.getArReceipt().getRctVoid() == 1
			  				|| (arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate) > 0) ) {
			  			continue;
			  		}
			  		System.out.println("---------------------->");
			  		System.out.println(arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate) <= 0);
			  		  OR_NUM += arAppliedInvoice.getArReceipt().getRctNumber() + ":";
			  		 appliedAmount += arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiPenaltyApplyAmount()
			  				+ arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiRebate();

			  		so_mdetails.setSmtLastOrDate(arAppliedInvoice.getArReceipt().getRctDate());
			  	 }

			  	so_mdetails.setSmtOrNumber(OR_NUM);
			  	so_mdetails.setSmtInvoiceNumber(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
			  	so_mdetails.setSmtAmountPaid(appliedAmount);
			  	so_mdetails.setSmtInterestDue(arInvoicePaymentSchedule.getIpsInterestDue());

			  

				  

				  SO_CM_AMNT = amountDue - creditAmount - appliedAmount;



				 if (INVOICE_AGE <= 0) {

					 so_mdetails.setSmtBucket0(SO_CM_AMNT);

	  	    	  }else if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {

	  	    		so_mdetails.setSmtBucket1(SO_CM_AMNT);



	  	    	  } else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {

	  	    		so_mdetails.setSmtBucket2(SO_CM_AMNT);


	  	    	  } else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {

	  	    		so_mdetails.setSmtBucket3(SO_CM_AMNT);


	  	    	  } else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {

	  	    		so_mdetails.setSmtBucket4(SO_CM_AMNT);


	  	    	  } else if (INVOICE_AGE > agingBucket4) {

	  	    		so_mdetails.setSmtBucket5(SO_CM_AMNT);


	  	    	  }
				 
				  so_mdetails.setSmtCreditAmount(0);
				  so_mdetails.setSmtTaxCode(arInvoicePaymentSchedule.getArInvoice().getArTaxCode().getTcName());
				  so_mdetails.setSmtRemainingBalance(amountDue - appliedAmount);
				  so_mdetails.setSmtDebit(arInvoicePaymentSchedule.getIpsAmountDue());
				  so_mdetails.setSmtCredit(0);
				  
				
				  
				  
				  System.out.println("invoice added : " + so_mdetails.getSmtInvoiceNumber());
				  
				  list.add(so_mdetails);
				  /*
				 if(includeCreditMemo) {

					  	try{
					  		System.out.println("CREDIT MEMO INVOICE="+arInvoicePaymentSchedule .getArInvoice().getInvNumber());

					  		Collection creditMemos = arInvoiceHome.findByCmInvoiceNumberCreditMemoAndCompanyCode(arInvoicePaymentSchedule.getArInvoice().getInvNumber(),arInvoicePaymentSchedule.getArInvoice().getInvAdBranch(), AD_CMPNY);
					  		System.out.println("pasok 1");
					  		Iterator y = creditMemos.iterator();

					  		double creditMemoAmount =0;
					  		while(y.hasNext()){

					  		
					  			LocalArInvoice arInvoiceCreditMemo = (LocalArInvoice)y.next();
					  			
					  			System.out.println("ENTER CREDIT MEMO EXIST="+arInvoiceCreditMemo.getInvNumber());
					  		
					  			
					  			ArRepStatementDetails so_cm_mdetails = (ArRepStatementDetails)so_mdetails.clone();
					  			
					  			
					  		
					  			so_cm_mdetails.setSmtTransactionType("CREDIT MEMO");
					  			so_cm_mdetails.setSmtTransaction(arInvoiceCreditMemo.getInvNumber());
					  		
							  	so_cm_mdetails.setSmtInvoiceAmountDue(0);
							  	so_cm_mdetails.setSmtInvoiceDownPayment(0);
							  	so_cm_mdetails.setSmtInvoiceUnearnedInterest(0);
						
							  	so_cm_mdetails.setSmtInvoicePenaltyDue(0);
								 try {
									 so_cm_mdetails.setSmtBatchName(arInvoiceCreditMemo.getArInvoiceBatch().getIbName());

								 } catch (Exception ex){

									 so_cm_mdetails.setSmtBatchName("");
								 }
					
								 so_cm_mdetails.setSmtDate(arInvoiceCreditMemo.getInvDate());
								 so_cm_mdetails.setSmtDescription(arInvoiceCreditMemo.getInvDescription());
						
								 so_cm_mdetails.setSmtAmount(arInvoiceCreditMemo.getInvAmountDue());
						
								 so_cm_mdetails.setSmtAmountDue(arInvoiceCreditMemo.getInvAmountDue());
								 so_cm_mdetails.setSmtIpsAmountDue(0);
									System.out.println("pasok 8");
								 so_cm_mdetails.setSmtInvoiceNumber(arInvoiceCreditMemo.getInvNumber());
								 so_cm_mdetails.setSmtAmountPaid(0);
								 so_cm_mdetails.setSmtInterestDue(0);
									System.out.println("pasok 9");
								 so_cm_mdetails.setSmtCreditAmount(creditAmount);
								 so_cm_mdetails.setSmtTaxCode(arInvoicePaymentSchedule.getArInvoice().getArTaxCode().getTcName());
								 so_cm_mdetails.setSmtRemainingBalance(0);
								 so_cm_mdetails.setSmtDebit(0);
								 so_cm_mdetails.setSmtCredit(arInvoiceCreditMemo.getInvAmountDue());
					
									System.out.println("cm amount due : " + arInvoiceCreditMemo.getInvAmountDue());
									System.out.println("cm amount due 2: " + creditAmount);               
									creditMemoAmount+=arInvoiceCreditMemo.getInvAmountDue();
								 list.add(so_cm_mdetails); 
					  		}
					  		

					  		
					 
							
							  
							  System.out.println("amount due:" + amountDue) ;
							  System.out.println("applied amount:" + appliedAmount) ;
							  System.out.println("cm amount" + creditMemoAmount) ;
							  so_mdetails.setSmtCreditAmount(0);
							  so_mdetails.setSmtTaxCode(arInvoicePaymentSchedule.getArInvoice().getArTaxCode().getTcName());
							  so_mdetails.setSmtRemainingBalance(amountDue );
							  so_mdetails.setSmtDebit(arInvoicePaymentSchedule.getIpsAmountDue());
							  so_mdetails.setSmtCredit(0);
							  
							  
							  list.add(so_mdetails);

					  	 }catch(Exception ex){

					  		System.out.println("ERROR IN CM INCLUDED");
					  		System.out.println(ex.toString());
					  	 }
					
					 
				 }else {
					 
					 
					 so_mdetails.setSmtCreditAmount(creditAmount);
					  so_mdetails.setSmtTaxCode(arInvoicePaymentSchedule.getArInvoice().getArTaxCode().getTcName());
					  so_mdetails.setSmtRemainingBalance(amountDue - appliedAmount);
					  so_mdetails.setSmtDebit(arInvoicePaymentSchedule.getIpsAmountDue());
					  so_mdetails.setSmtCredit(creditAmount);
					 list.add(so_mdetails);
				 }

			*/

		  	}


			


		  	//INVOICE MEMO LINES

		  	double CM_AMNT = 0d;
		  	double CM_AMNT_ASD = 0d;
		  	double CM_AMNT_RPT = 0d;
		  	double CM_AMNT_PD = 0d;
		  	double CM_AMNT_WTR = 0d;
		  	double CM_AMNT_MISC = 0d;


		  	Collection arSalesInvoiceLines = arInvoicePaymentSchedule.getArInvoice().getArInvoiceLines();
		  	System.out.println("arSalesInvoiceLines.size()="+arSalesInvoiceLines.size());

		  	if(+arSalesInvoiceLines.size() > 0){



		  	ArrayList arSalesInvoiceLineList = new ArrayList(arSalesInvoiceLines);
		  	LocalArInvoiceLine arSalesInvoiceLine = (LocalArInvoiceLine)arSalesInvoiceLineList.get(0);


		  	String smlName = arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription();

		  	if (!SML_NAME.equals("") && !SML_NAME.equals(smlName)){

		  		continue;

		  	}
			ArRepStatementDetails mdetails = new ArRepStatementDetails();

			if(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch() != null) {
				mdetails.setSmtInvoiceBatchName(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch().getIbName());
		  	}

			//PMA
			/*
			if(arInvoicePaymentSchedule.getArInvoice().getPmProject()!=null) {
				mdetails.setSmtPmProjectCode(arInvoicePaymentSchedule.getArInvoice().getPmProject().getPrjProjectCode());
				mdetails.setSmtPmProjectType(arInvoicePaymentSchedule.getArInvoice().getPmProjectTypeType().getPmProjectType().getPtValue());
				mdetails.setSmtPmProjectName(arInvoicePaymentSchedule.getArInvoice().getPmProject().getPrjName());
				mdetails.setSmtPmContractPrice(arInvoicePaymentSchedule.getArInvoice().getPmContractTerm().getPmContract().getCtrPrice());
				mdetails.setSmtPmContractValue(arInvoicePaymentSchedule.getArInvoice().getPmContractTerm().getCtValue());
				mdetails.setSmtPmContractTermName(arInvoicePaymentSchedule.getArInvoice().getPmContractTerm().getCtTermDescription());


			}

			*/
			mdetails.setSmtCustomerCode(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode());
			mdetails.setSmtCstCustomerID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstWordPressCustomerID());
			mdetails.setSmtCstEmployeeID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEmployeeID());
			mdetails.setSmtCstAccountNumber(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstAccountNumber());
			mdetails.setSmtCustomerBatch(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerBatch());
			mdetails.setSmtCustomerDepartment(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerDepartment());
			mdetails.setSmtCustomerEntryDate(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEntryDate());
			mdetails.setSmtCustomerName(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstName());
			mdetails.setSmtCustomerAddress(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstAddress());
			mdetails.setSmtCstContact(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstContact());
			mdetails.setSmtCstTin(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstTin());
			mdetails.setSmtCstSquareMeter(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstSquareMeter());
			mdetails.setSmtCstParkingID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstParkingID());
			mdetails.setSmtCstEML(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEmail());
			mdetails.setSmtTransactionType("INVOICE");
			mdetails.setSmtPosted(arInvoicePaymentSchedule.getArInvoice().getInvPosted());
			mdetails.setSmtTransaction(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
			mdetails.setSmtReferenceNumber(arInvoicePaymentSchedule.getArInvoice().getInvReferenceNumber());
			mdetails.setSmtCstCustomerID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstWordPressCustomerID());
			mdetails.setSmtCstEmployeeID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEmployeeID());
			mdetails.setSmtCstAccountNumber(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstAccountNumber());

			mdetails.setSmtLastDueDate(lastDueDate);
			mdetails.setSmtDueDate(arInvoicePaymentSchedule.getIpsDueDate());
			mdetails.setSmtInvoiceAmountDue(arInvoicePaymentSchedule.getArInvoice().getInvAmountDue());
			mdetails.setSmtInvoiceDownPayment(arInvoicePaymentSchedule.getArInvoice().getInvDownPayment());
			mdetails.setSmtInvoiceUnearnedInterest(arInvoicePaymentSchedule.getArInvoice().getInvAmountUnearnedInterest());


			mdetails.setSmtPaymentTerm(arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getAdPaymentSchedules().size());
			mdetails.setSmtPaymentTermName(arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getPytName());

			mdetails.setSmtDate(arInvoicePaymentSchedule.getArInvoice().getInvDate());
			mdetails.setSmtDescription(arInvoicePaymentSchedule.getArInvoice().getInvDescription());
			mdetails.setSmtTaxCode(arInvoicePaymentSchedule.getArInvoice().getArTaxCode().getTcName());

			double netAmount = EJBCommon.roundIt((arInvoicePaymentSchedule.getArInvoice().getInvAmountDue()) / (1 + (arInvoicePaymentSchedule.getArInvoice().getArTaxCode().getTcRate() / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));
			double wtaxAmount = EJBCommon.roundIt(netAmount * (arInvoicePaymentSchedule.getArInvoice().getArWithholdingTaxCode().getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));
			mdetails.setSmtWithholdingTaxAmount(wtaxAmount);

			try {
				mdetails.setSmtBatchName(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch().getIbName());
			} catch (Exception ex){ mdetails.setSmtBatchName("");}

			mdetails.setSmtItemDescription(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription());
			mdetails.setSmtSmlWpProductID(arSalesInvoiceLine.getArStandardMemoLine().getSmlWordPressProductID());
			mdetails.setSmtPreviousReading(arSalesInvoiceLine.getArInvoice().getInvPreviousReading());
			mdetails.setSmtPresentReading(arSalesInvoiceLine.getArInvoice().getInvPresentReading());
			mdetails.setSmtQuantity(arSalesInvoiceLine.getIlQuantity());
			mdetails.setSmtCurrency(arSalesInvoiceLine.getArInvoice().getGlFunctionalCurrency().getFcName());
			mdetails.setSmtUnitPrice(arSalesInvoiceLine.getIlUnitPrice());


			double amountDue = arInvoicePaymentSchedule.getIpsAmountDue() + arInvoicePaymentSchedule.getIpsPenaltyDue() ;
			double mo_ttlAmount = arInvoicePaymentSchedule.getIpsAmountDue() ;

			mdetails.setSmtNumber(arInvoicePaymentSchedule.getIpsNumber());
			mdetails.setSmtAmount(mo_ttlAmount);
			mdetails.setSmtPenalty(arInvoicePaymentSchedule.getIpsPenaltyDue());
			mdetails.setSmtAmountDue(amountDue);
			mdetails.setSmtIpsAmountDue(arInvoicePaymentSchedule.getIpsAmountDue());


			Collection arAppliedInvoices = arInvoicePaymentSchedule.getArAppliedInvoices();
			System.out.println("arAppliedInvoices="+arAppliedInvoices.size());
			Iterator x = arAppliedInvoices.iterator();

			double appliedAmount = 0d;
			String OR_NUM = "";

		  	 while(x.hasNext()){

		  		 LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)x.next();

		  		 if(arAppliedInvoice.getArReceipt() == null) continue;

		  		 int y = arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate);

		  		 if(arAppliedInvoice.getArReceipt().getRctVoid() == 1
		  				|| (arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate) > 0) ) continue;

		  		 OR_NUM += arAppliedInvoice.getArReceipt().getRctNumber() + ":";
		  		 appliedAmount += arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiRebate();
		  		 mdetails.setSmtLastOrDate(arAppliedInvoice.getArReceipt().getRctDate());

		  	 }

			 mdetails.setSmtOrNumber(OR_NUM);
			 mdetails.setSmtInvoiceNumber(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
		  	 mdetails.setSmtAmountPaid(appliedAmount);
		  	 mdetails.setSmtInterestDue(arInvoicePaymentSchedule.getIpsInterestDue());

			 mdetails.setSmtCreditAmount(0);
			 mdetails.setSmtRemainingBalance(amountDue - appliedAmount);
			 mdetails.setSmtDebit(arInvoicePaymentSchedule.getIpsAmountDue());
			 mdetails.setSmtCredit(0);

			 CM_AMNT = amountDue - creditAmount - appliedAmount;

			 if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Association Dues") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("ASD")){

				 CM_AMNT_ASD = CM_AMNT;

			 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("RPT")){

				 CM_AMNT_RPT = CM_AMNT;

			 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Parking Dues") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("PD")){

				 CM_AMNT_PD = CM_AMNT;

			 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Water") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("WTR")){

				 CM_AMNT_WTR = CM_AMNT;

			 } else {

				 CM_AMNT_MISC = CM_AMNT;

			 }


			 if (INVOICE_AGE <= 0) {

				 mdetails.setSmtBucket0(CM_AMNT);
				 mdetails.setSmtBucketASD0(CM_AMNT_ASD);
				 mdetails.setSmtBucketRPT0(CM_AMNT_RPT);
				 mdetails.setSmtBucketPD0(CM_AMNT_PD);
				 mdetails.setSmtBucketWTR0(CM_AMNT_WTR);
				 mdetails.setSmtBucketMISC0(CM_AMNT_MISC);

			  } else if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {

				mdetails.setSmtBucket1(CM_AMNT);
				 mdetails.setSmtBucketASD1(CM_AMNT_ASD);
				 mdetails.setSmtBucketRPT1(CM_AMNT_RPT);
				 mdetails.setSmtBucketPD1(CM_AMNT_PD);
				 mdetails.setSmtBucketWTR1(CM_AMNT_WTR);
				 mdetails.setSmtBucketMISC1(CM_AMNT_MISC);


			  } else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {

				mdetails.setSmtBucket2(CM_AMNT);
				 mdetails.setSmtBucketASD2(CM_AMNT_ASD);
				 mdetails.setSmtBucketRPT2(CM_AMNT_RPT);
				 mdetails.setSmtBucketPD2(CM_AMNT_PD);
				 mdetails.setSmtBucketWTR2(CM_AMNT_WTR);
				 mdetails.setSmtBucketMISC2(CM_AMNT_MISC);

			  } else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {

				mdetails.setSmtBucket3(CM_AMNT);
				 mdetails.setSmtBucketASD3(CM_AMNT_ASD);
				 mdetails.setSmtBucketRPT3(CM_AMNT_RPT);
				 mdetails.setSmtBucketPD3(CM_AMNT_PD);
				 mdetails.setSmtBucketWTR3(CM_AMNT_WTR);
				 mdetails.setSmtBucketMISC3(CM_AMNT_MISC);

			  } else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {

				mdetails.setSmtBucket4(CM_AMNT);
				 mdetails.setSmtBucketASD4(CM_AMNT_ASD);
				 mdetails.setSmtBucketRPT4(CM_AMNT_RPT);
				 mdetails.setSmtBucketPD4(CM_AMNT_PD);
				 mdetails.setSmtBucketWTR4(CM_AMNT_WTR);
				 mdetails.setSmtBucketMISC4(CM_AMNT_MISC);

			  } else if (INVOICE_AGE > agingBucket4) {

				mdetails.setSmtBucket5(CM_AMNT);
				 mdetails.setSmtBucketASD5(CM_AMNT_ASD);
				 mdetails.setSmtBucketRPT5(CM_AMNT_RPT);
				 mdetails.setSmtBucketPD5(CM_AMNT_PD);
				 mdetails.setSmtBucketWTR5(CM_AMNT_WTR);
				 mdetails.setSmtBucketMISC5(CM_AMNT_MISC);

			  }



			 list.add(mdetails);
		  	}

		  }



			  //CREDIT MEMO
			 Collection arInvoicePaymentSchedulerCreditMemo  = arInvoicePaymentScheduleHome.getIpsByCriteria(jbossQl.toString(), obj);

			 Iterator iCm = arInvoicePaymentSchedulerCreditMemo.iterator();
			 
			 HashMap INV_NMBRS = new HashMap();
			 
		
			 
			 while(iCm.hasNext()) {
				 
				 
				  LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)iCm.next();
				  
				  
				  arInvoicePaymentSchedule.getArInvoice().getInvNumber();
				  
				  if(INV_NMBRS.size()>0) {
					  
					  if(INV_NMBRS.containsKey(  arInvoicePaymentSchedule.getArInvoice().getInvNumber())) {
						  continue;
					  }
				  }
				  
				  INV_NMBRS.put(arInvoicePaymentSchedule.getArInvoice().getInvNumber(), 0d);
				  
				  
				  ArRepStatementDetails mdetails = new ArRepStatementDetails();
				  
				  
				  
				  if(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch() != null) {
					  mdetails.setSmtInvoiceBatchName(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch().getIbName());
				  }

				  mdetails.setSmtInvoiceNumber(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
				  mdetails.setSmtCustomerCode(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode());

				  mdetails.setSmtCstCustomerID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstWordPressCustomerID());
				  mdetails.setSmtCstEmployeeID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEmployeeID());
				  mdetails.setSmtCstAccountNumber(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstAccountNumber());
				  mdetails.setSmtCustomerBatch(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerBatch());
				  mdetails.setSmtCustomerDepartment(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerDepartment());
				  mdetails.setSmtCustomerEntryDate(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEntryDate());
				  mdetails.setSmtCustomerName(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstName());
				  mdetails.setSmtCustomerAddress(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstAddress());
				  
				  mdetails.setSmtPosted(arInvoicePaymentSchedule.getArInvoice().getInvPosted());
				  mdetails.setSmtTransactionType("CREDIT MEMO");
				  mdetails.setSmtTransaction(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
				  
				  mdetails.setSmtDate(arInvoicePaymentSchedule.getArInvoice().getInvDate());
				  mdetails.setSmtDescription(arInvoicePaymentSchedule.getArInvoice().getInvDescription());
				  mdetails.setSmtDebit(0d);
				  
				  
				  
				  
				 double creditAmount=0d;
			  	try{
			  		System.out.println("CREDIT MEMO INVOICE="+arInvoicePaymentSchedule .getArInvoice().getInvNumber());

			  		Collection creditMemos = arInvoiceHome.findByCmInvoiceNumberCreditMemoAndCompanyCode(arInvoicePaymentSchedule.getArInvoice().getInvNumber(),arInvoicePaymentSchedule.getArInvoice().getInvAdBranch(), AD_CMPNY);

			  		
			  		System.out.println("cm size : "+ creditMemos.size());
			  		Iterator y = creditMemos.iterator();

			  		while(y.hasNext()){
			  			LocalArInvoice arInvoiceCreditMemo = (LocalArInvoice)y.next();
			  			if(includeCreditMemo) {
			  				ArRepStatementDetails mdetailsCm = (ArRepStatementDetails)mdetails.clone();
			  				mdetailsCm.setSmtPosted(arInvoiceCreditMemo.getInvPosted());
			  				mdetailsCm.setSmtTransaction(arInvoiceCreditMemo.getInvNumber());
			  				mdetailsCm.setSmtInvoiceNumber(arInvoiceCreditMemo.getInvNumber());
			  				mdetailsCm.setSmtTransactionType("CREDIT MEMO");
			  				mdetailsCm.setSmtDate(arInvoiceCreditMemo.getInvDate());
			  				mdetailsCm.setSmtDescription("Credit Memo from Invoice : " + arInvoiceCreditMemo.getInvCmInvoiceNumber());
			  				mdetailsCm.setSmtDebit(0);
			  				mdetailsCm.setSmtCredit(arInvoiceCreditMemo.getInvAmountDue());
			  				list.add(mdetailsCm);
			  				
			  			}else {
			  				creditAmount += arInvoiceCreditMemo.getInvAmountDue();
			  			}
			  			
			  		

			  		}
			  		

			  	 }catch(Exception ex){

			  	 }
			  	
			  	
			  	if(!includeCreditMemo && creditAmount > 0 &&  EJBCommon.roundIt(creditAmount, precisionUnit)  != EJBCommon.roundIt(arInvoicePaymentSchedule.getArInvoice().getInvAmountDue(), precisionUnit)) {
			  		
			  		
			  		
			  		 mdetails.setSmtTransactionType("INVOICE");
			  		 mdetails.setSmtDate(arInvoicePaymentSchedule.getArInvoice().getInvDate());
			  		 mdetails.setSmtTransaction(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
			  		mdetails.setSmtInvoiceNumber(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
			  		mdetails.setSmtAmountDue(creditAmount);
			  		mdetails.setSmtCredit(0);
			  		mdetails.setSmtDebit(creditAmount * -1);
			  		mdetails.setSmtPosted(arInvoicePaymentSchedule.getArInvoice().getInvPosted());
			  		list.add(mdetails);
			  	}
			  	

				 
			 }






			  
			  

			  if (includeCollection){



				  Collection arInvoicePaymentSchedulerReceipts = arInvoicePaymentScheduleHome.getIpsByCriteria(jbossQl.toString(), obj);



				  Iterator y = arInvoicePaymentSchedules.iterator();


				  while (y.hasNext()) {

				  	  LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)y.next();

				  	  Collection arAppliedInvoices = arInvoicePaymentSchedule.getArAppliedInvoices();


				  	  System.out.println("arAppliedInvoices.size()="+arAppliedInvoices.size());


					  Iterator j = arAppliedInvoices.iterator();

					  while (j.hasNext()) {

						  LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice) j.next();

						  ArRepStatementDetails mdetails = new ArRepStatementDetails();

						  if(arAppliedInvoice.getArReceipt().getRctVoid() == 1
					  				|| (arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate) > 0) ) continue;
					  		System.out.println("---------------------->");

						  mdetails.setSmtCustomerBatch(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerBatch());
						  mdetails.setSmtCustomerDepartment(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerDepartment());


						  if(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch() != null) {
							  mdetails.setSmtInvoiceBatchName(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch().getIbName());
						  }

						  mdetails.setSmtInvoiceNumber(arInvoicePaymentSchedule.getArInvoice().getInvNumber());
						  mdetails.setSmtCustomerCode(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode());

						  mdetails.setSmtCstCustomerID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstWordPressCustomerID());
						  mdetails.setSmtCstEmployeeID(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEmployeeID());
						  mdetails.setSmtCstAccountNumber(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstAccountNumber());
						  mdetails.setSmtCustomerBatch(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerBatch());
						  mdetails.setSmtCustomerDepartment(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerDepartment());
						  mdetails.setSmtCustomerEntryDate(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstEntryDate());
						  mdetails.setSmtCustomerName(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstName());
						  mdetails.setSmtCustomerAddress(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstAddress());
						  mdetails.setSmtTransaction(arAppliedInvoice.getArReceipt().getRctNumber());
						  mdetails.setSmtPosted(arAppliedInvoice.getArReceipt().getRctPosted());
						  mdetails.setSmtTransactionType("RECEIPT");
						  mdetails.setSmtDate(arAppliedInvoice.getArReceipt().getRctDate());
						  mdetails.setSmtDescription(arAppliedInvoice.getArReceipt().getRctDescription());
						  mdetails.setSmtDebit(0d);

						  double appliedAmount = arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiRebate();


						  mdetails.setSmtCredit(appliedAmount);

						  list.add(mdetails);

					  }

				  }


			  }










			  System.out.println("totla number of lines is : "+ list.size());

		  if (list.isEmpty()) {

		  	  throw new GlobalNoRecordFoundException();

		  }

	      }

		  if (REPORT_TYP.equals("CUSTOMER LEDGER")){
			  Collections.sort(list, ArRepStatementDetails.CustomerLedgerComparator);

		  } else if (REPORT_TYP.equals("ACCOUNT SUMMARY")){
			  Collections.sort(list, ArRepStatementDetails.AccountSummaryComparator);


		  } else if (ORDER_BY.contains("DUE DATE")){
			  Collections.sort(list, ArRepStatementDetails.DueDateComparator);

		  } else {
			  System.out.println("ArRepStatementDetails.DateComparator");
			  Collections.sort(list, ArRepStatementDetails.DateComparator);
		  }


		  return list;

	  } catch (GlobalNoRecordFoundException ex) {

	  	  throw ex;

	  } catch (Exception ex) {

	  	  Debug.printStackTrace(ex);
	  	  throw new EJBException(ex.getMessage());

	  }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeSpArRepStatement(ResultSet rs, Date agingDate, String SML_NAME, boolean showAll, boolean includeZero, boolean includeCollection, String ORDER_BY, String GROUP_BY, String REPORT_TYP, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException{
        Debug.print("ArRepStatementControllerBean executeSpArRepStatement");
        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalArCustomerBalanceHome arCustomerBalanceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
        LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
        LocalArInvoiceLineHome arSalesInvoiceLineHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalCmAdjustmentHome cmAdjustmentHome = null;
        LocalAdPaymentScheduleHome adPaymentScheduleHome = null;

        ArrayList list = new ArrayList();


        //initialized EJB Home

        try {

            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

            arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);

            arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);

            arSalesInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

            arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                    lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);

            adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);


        } catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

        try {
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            short precisionUnit = adCompany.getGlFunctionalCurrency().getFcPrecision();


            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(adCompany.getCmpCode());
            int agingBucket1 = adPreference.getPrfArAgingBucket();
            int agingBucket2 = adPreference.getPrfArAgingBucket() * 2;
            int agingBucket3 = adPreference.getPrfArAgingBucket() * 3;
            int agingBucket4 = adPreference.getPrfArAgingBucket() * 4;

            while(rs.next()){


                String ROW_TYPE = rs.getString("TYPE_ROW");
                System.out.println("row type: " + ROW_TYPE);
                if(ROW_TYPE.equals("ADVANCE")){

                    //System.out.println("cmAdjustment="+cmAdjustment.getAdjDocumentNumber());
                    ArRepStatementDetails mdetails = new ArRepStatementDetails();
                    mdetails.setSmtCstCustomerID(rs.getString("CST_WP_CSTMR_ID"));
                    mdetails.setSmtCstEmployeeID(rs.getString("CST_EMP_ID"));
                    mdetails.setSmtCstAccountNumber(rs.getString("CST_ACCNT_NO"));
                    mdetails.setSmtCstSquareMeter(rs.getDouble("CST_SQ_MTR"));
                    mdetails.setSmtCustomerEntryDate(rs.getDate("CST_ENTRY_DT"));
                    mdetails.setSmtCstParkingID(rs.getString("CST_PRKNG_ID"));
                    mdetails.setSmtCustomerBatch(rs.getString("CST_CSTMR_BTCH"));
                    mdetails.setSmtCustomerDepartment(rs.getString("CST_CSTMR_DPRTMNT"));

                    mdetails.setSmtCustomerCode(rs.getString("CST_CSTMR_CODE"));
                    mdetails.setSmtCustomerName(rs.getString("CST_NM"));
                    mdetails.setSmtCstEML(rs.getString("CST_EML"));
                    mdetails.setSmtDate(rs.getDate("DATE_TRANSACTION"));
                    mdetails.setSmtTransactionType("ADVANCE");

                    mdetails.setSmtTransaction(rs.getString("TRANSCATION_NUMBER"));
                    mdetails.setSmtPosted(rs.getByte("POSTED"));

                    // get applied credit



                    int FC_CODE = rs.getInt("CURRENCY_CODE");
                    String FC_NM = rs.getString("CURRENCY");
                    Date CONVERTION_DATE = rs.getDate("CONVERTION_DATE");
                    double CONVERTION_RATE = rs.getDouble("CONVERTION_RATE");

                    double ADV_AMNT = this.convertForeignToFunctionalCurrency(FC_CODE,
                                      FC_NM,
                                      CONVERTION_DATE,
                                      CONVERTION_RATE,
                                      rs.getDouble("AMOUNT_PAID"), AD_CMPNY) ;

                    if ( !includeZero && (!showAll && ADV_AMNT == 0d)) continue;
                    if (includeCollection && ADV_AMNT == 0d) continue;

                    try{
                        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MMMMM yyyy");
                            String date = DATE_FORMAT.format(rs.getDate("DATE_TRANSACTION"));
                            mdetails.setSmtBatchName(date);
                    }catch(Exception ex){
                      mdetails.setSmtBatchName("");
                    }

                    mdetails.setSmtOrNumber(rs.getString("TRANSCATION_NUMBER"));
                    
                
                    mdetails.setSmtDescription("ADVANCE PAYMENT / CUSTOMER DEPOSIT");
                    mdetails.setSmtAmountPaid(ADV_AMNT );
                    mdetails.setSmtItemDescription("ADVANCE");
                    mdetails.setSmtAmount(0d);
                    mdetails.setSmtAmountDue(ADV_AMNT * -1 );
                    mdetails.setSmtIpsAmountDue(rs.getDouble("IPS_AMOUNT_DUE")* -1);
                    mdetails.setSmtDebit(0d);
                    mdetails.setSmtCredit(ADV_AMNT);
                    mdetails.setSmtCreditAmount(0d);
                    mdetails.setSmtTaxAmount(0d);

                    int INVOICE_AGE = (short)((agingDate.getTime() -
                    rs.getDate("DATE_TRANSACTION").getTime()) / (1000 * 60 * 60 * 24));


                    if (INVOICE_AGE <= 0) {

                            mdetails.setSmtBucketADV0(ADV_AMNT * -1);

                    } else if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {

                            mdetails.setSmtBucketADV1(ADV_AMNT * -1);

                    } else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {

                            mdetails.setSmtBucketADV2(ADV_AMNT * -1);

                    } else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {

                            mdetails.setSmtBucketADV3(ADV_AMNT * -1);

                    } else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {

                            mdetails.setSmtBucketADV4(ADV_AMNT * -1);

                    } else if (INVOICE_AGE > agingBucket4) {

                            mdetails.setSmtBucketADV5(ADV_AMNT * -1);

                    }
                    list.add(mdetails);


                }else if(ROW_TYPE.equals("INVOICE ITEM")){

                    //SALES ORDER INVOICE LINES
                    int INVOICE_AGE = (short)((agingDate.getTime() -
                    rs.getDate("DUE_DATE").getTime()) / (1000 * 60 * 60 * 24));
                    System.out.println(INVOICE_AGE+"=INVOICE_AGE="+rs.getDate("DUE_DATE").getTime()) ;

                    if(rs.getLong("SALES_INVOICE_LINE_SIZE")<=0 ||rs.getLong("INVOICE_LINE_ITEM_SIZE")<=0){
                        continue;
                    }

                    double SO_CM_AMNT = 0d;

                    ArRepStatementDetails so_mdetails = new ArRepStatementDetails();

                    so_mdetails.setSmtCustomerCode(rs.getString("CST_CSTMR_CODE"));

                    so_mdetails.setSmtCstCustomerID(rs.getString("CST_WP_CSTMR_ID"));
                    so_mdetails.setSmtCstEmployeeID(rs.getString("CST_EMP_ID"));
                    so_mdetails.setSmtCstAccountNumber(rs.getString("CST_ACCNT_NO"));
                    so_mdetails.setSmtCustomerBatch(rs.getString("CST_CSTMR_BTCH"));
                    so_mdetails.setSmtCustomerDepartment(rs.getString("CST_CSTMR_DPRTMNT"));

                    so_mdetails.setSmtCustomerEntryDate(rs.getDate("CST_ENTRY_DT"));
                    so_mdetails.setSmtCustomerName(rs.getString("CST_NM"));
                    so_mdetails.setSmtCustomerAddress(rs.getString("CST_ADDRSS"));
                    so_mdetails.setSmtCstContact(rs.getString("CST_CNTCT"));
                    so_mdetails.setSmtCstTin(rs.getString("CST_TIN"));
                    so_mdetails.setSmtCstSquareMeter(rs.getDouble("CST_SQ_MTR"));
                    so_mdetails.setSmtCstParkingID(rs.getString("CST_PRKNG_ID"));
                    so_mdetails.setSmtCstEML(rs.getString("CST_EML"));
                    so_mdetails.setSmtTransactionType("INVOICE");
                    so_mdetails.setSmtPosted(rs.getByte("POSTED"));
                    so_mdetails.setSmtPaymentTerm((int)rs.getLong("PAYMENT_TERM_SIZE"));
                    so_mdetails.setSmtPaymentTermName(rs.getString("PAYMENT_TERM"));

                    so_mdetails.setSmtTransaction(rs.getString("TRANSCATION_NUMBER"));
                    so_mdetails.setSmtReferenceNumber(rs.getString("REFERENCE_NUMBER"));
                    so_mdetails.setSmtDescription(rs.getString("DESCRIPTION"));

                    so_mdetails.setSmtDueDate(rs.getDate("DUE_DATE"));
                    so_mdetails.setSmtInvoiceAmountDue(rs.getDouble("INVOICE_AMOUNT_DUE"));
                    so_mdetails.setSmtInvoiceDownPayment(rs.getDouble("INVOICE_DOWN_PAYMENT"));
                    so_mdetails.setSmtInvoiceUnearnedInterest(rs.getDouble("INVOICE_UNEARNED_INT"));
                    so_mdetails.setSmtDebit(rs.getDouble("INVOICE_AMOUNT_DUE"));
                    so_mdetails.setSmtCredit(0d);
                    so_mdetails.setSmtInvoicePenaltyDue(rs.getDouble("PENALTY_DUE"));
                    so_mdetails.setSmtBatchName(rs.getString("BATCH_NAME"));

                    so_mdetails.setSmtDate(rs.getDate("DATE_TRANSACTION"));
                    so_mdetails.setSmtDescription(rs.getString("DESCRIPTION"));
                    so_mdetails.setSmtCurrency(rs.getString("CURRENCY"));
                    so_mdetails.setSmtPaymentTerm((int)rs.getLong("PAYMENT_TERM_SIZE"));
                    so_mdetails.setSmtPaymentTermName(rs.getString("PAYMENT_TERM"));


                    so_mdetails.setSmtNumber(rs.getDouble("IPS_NUMBER"));
                    so_mdetails.setSmtAmount(rs.getDouble("IPS_AMOUNT_DUE"));
                    so_mdetails.setSmtPenalty(rs.getDouble("PENALTY_DUE"));
                    so_mdetails.setSmtAmountDue(rs.getDouble("AMOUNT_DUE"));
                    so_mdetails.setSmtIpsAmountDue(rs.getDouble("IPS_NUMBER"));

                    Collection arAppliedInvoices = arAppliedInvoiceHome.findByIpsCode(rs.getInt("IPS_CODE"), AD_CMPNY);


                    Iterator xx = arAppliedInvoices.iterator();

                    Double appliedAmount = 0d;
                    String OR_NUM = "";

                    while(xx.hasNext()){

                        LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)xx.next();

                        int y = arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate);

                        System.out.println("RCT DATE="+arAppliedInvoice.getArReceipt().getRctDate());
                        System.out.println("AGING DATE="+agingDate);
                        System.out.println("Y===="+y);



                        if(arAppliedInvoice.getArReceipt().getRctVoid() == 1
                                        || (arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate) > 0) ) continue;
                        System.out.println("---------------------->");
                        System.out.println(arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate) <= 0);
                        OR_NUM += arAppliedInvoice.getArReceipt().getRctNumber() + ":";
                        appliedAmount += arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiPenaltyApplyAmount()
                                           + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiRebate();

                        so_mdetails.setSmtLastOrDate(arAppliedInvoice.getArReceipt().getRctDate());
                    }

                    so_mdetails.setSmtOrNumber(OR_NUM);
                    so_mdetails.setSmtAmountPaid(appliedAmount);
                    so_mdetails.setSmtInterestDue(rs.getDouble("INTEREST_DUE"));
                    double creditMemoAmountDue = 0d;


                    Collection arCreditMemoes = arInvoiceHome.findByCmInvoiceNumberCreditMemoAndCompanyCode(rs.getString("TRANSCATION_NUMBER"),rs.getInt("AD_BRANCH"), AD_CMPNY);

                    Iterator jd = arCreditMemoes.iterator();

                    while (jd.hasNext()) {

                            LocalArInvoice arCreditMemo = (LocalArInvoice) jd.next();

                            creditMemoAmountDue += arCreditMemo.getInvAmountDue();


                    }


                    so_mdetails.setSmtCreditAmount(creditMemoAmountDue);
                    so_mdetails.setSmtTaxCode(rs.getString("TAX"));
                    so_mdetails.setSmtRemainingBalance(rs.getDouble("AMOUNT_DUE") - appliedAmount);

                    SO_CM_AMNT = rs.getDouble("AMOUNT_DUE") - creditMemoAmountDue - appliedAmount;



                    if (INVOICE_AGE <= 0) {

                        so_mdetails.setSmtBucket0(SO_CM_AMNT);

                    }else if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {

                        so_mdetails.setSmtBucket1(SO_CM_AMNT);

                    } else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {

                        so_mdetails.setSmtBucket2(SO_CM_AMNT);


                    } else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {

                        so_mdetails.setSmtBucket3(SO_CM_AMNT);


                    } else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {

                        so_mdetails.setSmtBucket4(SO_CM_AMNT);


                    } else if (INVOICE_AGE > agingBucket4) {

                        so_mdetails.setSmtBucket5(SO_CM_AMNT);


                    }

                    list.add(so_mdetails);


                }else if(ROW_TYPE.equals("INVOICE MEMO LINE")){
                    double CM_AMNT = 0d;
                    double CM_AMNT_ASD = 0d;
                    double CM_AMNT_RPT = 0d;
                    double CM_AMNT_PD = 0d;
                    double CM_AMNT_WTR = 0d;
                    double CM_AMNT_MISC = 0d;

                    if (!SML_NAME.equals("") && !SML_NAME.equals(rs.getString("ITEM_DESCRIPTION"))){

                        continue;

                    }
                    ArRepStatementDetails mdetails = new ArRepStatementDetails();

                    mdetails.setSmtCustomerCode(rs.getString("CST_CSTMR_CODE"));
                    mdetails.setSmtCstCustomerID(rs.getString("CST_WP_CSTMR_ID"));
                    mdetails.setSmtCstEmployeeID(rs.getString("CST_EMP_ID"));
                    mdetails.setSmtCstAccountNumber(rs.getString("CST_ACCNT_NO"));
                    mdetails.setSmtCustomerBatch(rs.getString("CST_CSTMR_BTCH"));
                    mdetails.setSmtCustomerDepartment(rs.getString("CST_CSTMR_DPRTMNT"));
                    mdetails.setSmtCustomerEntryDate(rs.getDate("CST_ENTRY_DT"));
                    mdetails.setSmtCustomerName(rs.getString("CST_NM"));
                    mdetails.setSmtCustomerAddress(rs.getString("CST_ADDRSS"));
                    mdetails.setSmtCstContact(rs.getString("CST_CNTCT"));
                    mdetails.setSmtCstTin(rs.getString("CST_TN"));
                    mdetails.setSmtCstSquareMeter(rs.getDouble("CST_SQ_MTR"));
                    mdetails.setSmtCstParkingID(rs.getString("CST_PRKNG_ID"));
                    mdetails.setSmtCstEML(rs.getString("CST_EML"));
                    mdetails.setSmtTransactionType("INVOICE");
                    mdetails.setSmtPosted(rs.getByte("POSTED"));
                    mdetails.setSmtTransaction(rs.getString("TRANSCATION_NUMBER"));
                    mdetails.setSmtReferenceNumber(rs.getString("REFERENCE_NUMBER"));
                    mdetails.setSmtDueDate(rs.getDate("DUE_DATE"));
                    mdetails.setSmtInvoiceAmountDue(rs.getDouble("INVOICE_AMOUNT_DUE"));
                    mdetails.setSmtInvoiceDownPayment(rs.getDouble("INVOICE_DOWN_PAYMENT"));
                    mdetails.setSmtInvoiceUnearnedInterest(rs.getDouble("INVOICE_UNEARNED_INT"));

                    mdetails.setSmtDebit(rs.getDouble("IPS_AMOUNT_DUE"));
                    mdetails.setSmtCredit(0d);
                    mdetails.setSmtPaymentTerm((int)rs.getLong("PAYMENT_TERM_SIZE"));
                    mdetails.setSmtPaymentTermName(rs.getString("PAYMENT_TERM"));

                    mdetails.setSmtDate(rs.getDate("DATE_TRANSACTION"));
                    mdetails.setSmtDescription(rs.getString("DESCRIPTION"));
                    mdetails.setSmtTaxCode(rs.getString("TAX"));

                    double netAmount = EJBCommon.roundIt((rs.getDouble("INVOICE_AMOUNT_DUE")) / (1 + (rs.getDouble("TAX_RATE") / 100)), this.getGlFcPrecisionUnit(AD_CMPNY));
                    double wtaxAmount = EJBCommon.roundIt(netAmount * (rs.getDouble("WITHHOLDING_TAX_RATE") / 100), this.getGlFcPrecisionUnit(AD_CMPNY));
                    mdetails.setSmtWithholdingTaxAmount(wtaxAmount);

                    double amountDue = rs.getDouble("AMOUNT_DUE");
                    mdetails.setSmtAmountDue(amountDue);
                    mdetails.setSmtIpsAmountDue(rs.getDouble("IPS_AMOUNT_DUE"));
                    mdetails.setSmtBatchName(rs.getString("BATCH_NAME"));


                    mdetails.setSmtItemDescription(rs.getString("ITEM_DESCRIPTION"));
                    mdetails.setSmtSmlWpProductID(rs.getString("PRODUCT_ID"));
                    mdetails.setSmtPreviousReading(rs.getDouble("PREVIOUS_READING"));
                    mdetails.setSmtPresentReading(rs.getDouble("PRESENT_READING"));
                    mdetails.setSmtQuantity(rs.getDouble("QUANTITY"));
                    mdetails.setSmtCurrency(rs.getString("CURRENCY"));
                    mdetails.setSmtUnitPrice(rs.getDouble("UNIT_PRICE"));
                    mdetails.setSmtTaxAmount(rs.getDouble("TAX_AMOUNT"));
                    mdetails.setSmtAmount(rs.getDouble("UNIT_AMOUNT"));

                    Collection arAppliedInvoices = arAppliedInvoiceHome.findByIpsCode(rs.getInt("IPS_CODE"), AD_CMPNY);

                    System.out.println("arAppliedInvoices="+arAppliedInvoices.size());
                    Iterator x = arAppliedInvoices.iterator();

                    double appliedAmount = 0d;
                    String OR_NUM = "";

                    while(x.hasNext()){

                        LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)x.next();

                        if(arAppliedInvoice.getArReceipt() == null) continue;

                        int y = arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate);

                        if(arAppliedInvoice.getArReceipt().getRctVoid() == 1
                                       || (arAppliedInvoice.getArReceipt().getRctDate().compareTo(agingDate) > 0) ) continue;

                        OR_NUM += arAppliedInvoice.getArReceipt().getRctNumber() + ":";
                        appliedAmount += arAppliedInvoice.getAiApplyAmount() + arAppliedInvoice.getAiCreditBalancePaid() + arAppliedInvoice.getAiDiscountAmount() + arAppliedInvoice.getAiRebate();
                        mdetails.setSmtLastOrDate(arAppliedInvoice.getArReceipt().getRctDate());

                    }
                    mdetails.setSmtOrNumber(OR_NUM);
                    mdetails.setSmtAmountPaid(appliedAmount);
                    mdetails.setSmtInterestDue(rs.getInt("INTEREST_DUE"));
                    double  creditMemoAmountDue = 0d;

                    Collection arCreditMemos = arInvoiceHome.findByCmInvoiceNumberCreditMemoAndCompanyCode(rs.getString("TRANSCATION_NUMBER"),rs.getInt("AD_BRANCH"), AD_CMPNY);

                    Iterator j = arCreditMemos.iterator();

                    while (j.hasNext()) {

                        LocalArInvoice arCreditMemo = (LocalArInvoice) j.next();

                        creditMemoAmountDue += arCreditMemo.getInvAmountDue();


                    }
                    mdetails.setSmtCreditAmount(creditMemoAmountDue);
                    mdetails.setSmtRemainingBalance(amountDue - appliedAmount);

                    CM_AMNT = amountDue;
                    String SML_DESC = rs.getString("ITEM_DESCRIPTION");

                    if(SML_DESC.contains("Association Dues") ||SML_DESC.contains("ASD")){

                        CM_AMNT_ASD = amountDue;

                    } else if(SML_DESC.contains("RPT")){

                        CM_AMNT_RPT = amountDue;

                    } else if(SML_DESC.contains("Parking Dues") || SML_DESC.contains("PD")){

                        CM_AMNT_PD = amountDue;

                    } else if(SML_DESC.contains("Water") || SML_DESC.contains("WTR")){

                        CM_AMNT_WTR = amountDue;

                    } else {

                        CM_AMNT_MISC = amountDue;

                    }

                    int INVOICE_AGE = (short)((agingDate.getTime() -
                    rs.getDate("DUE_DATE").getTime()) / (1000 * 60 * 60 * 24));
                    System.out.println(INVOICE_AGE+"=INVOICE_AGE="+rs.getDate("DUE_DATE")) ;


                    if (INVOICE_AGE <= 0) {

                        mdetails.setSmtBucket0(CM_AMNT);
                        mdetails.setSmtBucketASD0(CM_AMNT_ASD);
                        mdetails.setSmtBucketRPT0(CM_AMNT_RPT);
                        mdetails.setSmtBucketPD0(CM_AMNT_PD);
                        mdetails.setSmtBucketWTR0(CM_AMNT_WTR);
                        mdetails.setSmtBucketMISC0(CM_AMNT_MISC);

                    } else if (INVOICE_AGE >= 1 && INVOICE_AGE <= agingBucket1) {

                        mdetails.setSmtBucket1(CM_AMNT);
                        mdetails.setSmtBucketASD1(CM_AMNT_ASD);
                        mdetails.setSmtBucketRPT1(CM_AMNT_RPT);
                        mdetails.setSmtBucketPD1(CM_AMNT_PD);
                        mdetails.setSmtBucketWTR1(CM_AMNT_WTR);
                        mdetails.setSmtBucketMISC1(CM_AMNT_MISC);


                    } else if (INVOICE_AGE >= (agingBucket1 + 1) && INVOICE_AGE <= agingBucket2) {

                        mdetails.setSmtBucket2(CM_AMNT);
                        mdetails.setSmtBucketASD2(CM_AMNT_ASD);
                        mdetails.setSmtBucketRPT2(CM_AMNT_RPT);
                        mdetails.setSmtBucketPD2(CM_AMNT_PD);
                        mdetails.setSmtBucketWTR2(CM_AMNT_WTR);
                        mdetails.setSmtBucketMISC2(CM_AMNT_MISC);

                    } else if (INVOICE_AGE >= (agingBucket2 + 1) && INVOICE_AGE <= agingBucket3) {

                        mdetails.setSmtBucket3(CM_AMNT);
                        mdetails.setSmtBucketASD3(CM_AMNT_ASD);
                        mdetails.setSmtBucketRPT3(CM_AMNT_RPT);
                        mdetails.setSmtBucketPD3(CM_AMNT_PD);
                        mdetails.setSmtBucketWTR3(CM_AMNT_WTR);
                        mdetails.setSmtBucketMISC3(CM_AMNT_MISC);

                    } else if (INVOICE_AGE >= (agingBucket3 + 1) && INVOICE_AGE <= agingBucket4) {

                        mdetails.setSmtBucket4(CM_AMNT);
                        mdetails.setSmtBucketASD4(CM_AMNT_ASD);
                        mdetails.setSmtBucketRPT4(CM_AMNT_RPT);
                        mdetails.setSmtBucketPD4(CM_AMNT_PD);
                        mdetails.setSmtBucketWTR4(CM_AMNT_WTR);
                        mdetails.setSmtBucketMISC4(CM_AMNT_MISC);

                    } else if (INVOICE_AGE > agingBucket4) {

                        mdetails.setSmtBucket5(CM_AMNT);
                        mdetails.setSmtBucketASD5(CM_AMNT_ASD);
                        mdetails.setSmtBucketRPT5(CM_AMNT_RPT);
                        mdetails.setSmtBucketPD5(CM_AMNT_PD);
                        mdetails.setSmtBucketWTR5(CM_AMNT_WTR);
                        mdetails.setSmtBucketMISC5(CM_AMNT_MISC);

                    }

                    list.add(mdetails);

                }else if(ROW_TYPE.equals("COLLECTION")){

                    ArRepStatementDetails mdetails = new ArRepStatementDetails();

                    mdetails.setSmtCustomerBatch(rs.getString("CST_CSTMR_BTCH"));
                    mdetails.setSmtCustomerDepartment(rs.getString("CST_CSTMR_DPRTMNT"));

                    mdetails.setSmtCustomerCode(rs.getString("CST_CSTMR_CODE"));

                    mdetails.setSmtCstCustomerID(rs.getString("CST_WP_CSTMR_ID"));
                    mdetails.setSmtCstEmployeeID(rs.getString("CST_EMP_ID"));
                    mdetails.setSmtCstAccountNumber(rs.getString("CST_ACCNT_NO"));
                    mdetails.setSmtCustomerBatch(rs.getString("CST_CSTMR_BTCH"));
                    mdetails.setSmtCustomerDepartment(rs.getString("CST_CSTMR_DPRTMNT"));
                    mdetails.setSmtCustomerEntryDate(rs.getDate("CST_ENTRY_DT"));
                    mdetails.setSmtCustomerName(rs.getString("CST_NM"));
                    mdetails.setSmtCustomerAddress(rs.getString("CST_ADDRSS"));
                    mdetails.setSmtTransaction(rs.getString("TRANSCATION_NUMBER"));
                    mdetails.setSmtPosted(rs.getByte("POSTED"));
                    mdetails.setSmtTransactionType("RECEIPT");
                    mdetails.setSmtDate(rs.getDate("DATE_TRANSACTION"));
                    mdetails.setSmtDescription(rs.getString("DESCRIPTION"));
                    mdetails.setSmtDebit(0d);
                    mdetails.setSmtCredit(rs.getDouble("CREDIT"));

                    list.add(mdetails);


                }
            }

            if (list.isEmpty()) {

                throw new GlobalNoRecordFoundException();

            }



            if (REPORT_TYP.equals("CUSTOMER LEDGER")){
                    Collections.sort(list, ArRepStatementDetails.CustomerLedgerComparator);

            } else if (ORDER_BY.contains("DUE DATE")){
                    Collections.sort(list, ArRepStatementDetails.DueDateComparator);

            } else {
                    System.out.println("ArRepStatementDetails.DateComparator");
                    Collections.sort(list, ArRepStatementDetails.DateComparator);
            }

            return list;

  	} catch (GlobalNoRecordFoundException ex) {

            throw ex;


        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }





    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeSpArRepAging(ResultSet rs, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException{
        Debug.print("ArRepStatementControllerBean executeSpArRepAging");

        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;

        ArrayList list = new ArrayList();


        //initialized EJB Home

        try {


            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

        try {
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            short precisionUnit = adCompany.getGlFunctionalCurrency().getFcPrecision();


            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(adCompany.getCmpCode());


            while(rs.next()){

            	ArRepStatementDetails mdetails = new ArRepStatementDetails();
            	mdetails.setSmtCustomerCode(rs.getString("CST_CSTMR_CODE"));
            	mdetails.setSmtCustomerName(rs.getString("CST_NM"));
            	mdetails.setSmtCstAccountNumber(rs.getString("CST_ACCNT_NO"));
            	mdetails.setSmtCstEmployeeID(rs.getString("CST_EMP_ID"));
            	mdetails.setSmtCustomerDepartment(rs.getString("CST_CSTMR_DPRTMNT"));

            	mdetails.setSmtAmountDue(rs.getDouble("AMNT_DUE"));
                mdetails.setSmtAmountPaid(rs.getDouble("AMNT_PD"));
                mdetails.setSmtCreditAmount(rs.getDouble("TOT_ADJ_AMNT"));
                mdetails.setSmtExclusiveAdvance(rs.getDouble("TOT_BLNC_EXCADV"));
                mdetails.setSmtInclusiveAdvance(rs.getDouble("TOT_BLNC_INCADV"));


                mdetails.setSmtTransaction(rs.getString("INV_NMBR"));
                mdetails.setSmtDate(rs.getDate("LST_INV_DT"));
                mdetails.setSmtPaymentTermName(rs.getString("TERMS"));
                mdetails.setSmtLastOrDate(rs.getDate("LST_PYMT_DT"));
                mdetails.setSmtNumberDays(rs.getString("NDAYS"));


                list.add(mdetails);

            }

            if (list.isEmpty()) {

                throw new GlobalNoRecordFoundException();

            }

            return list;

  	} catch (GlobalNoRecordFoundException ex) {

            throw ex;


        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepStatementReceiptSub(HashMap criteria, String ORDER_BY, String GROUP_BY, String REPORT_TYP, ArrayList branchList, Integer AD_CMPNY) {

        Debug.print("ArRepStatementControllerBean executeArRepStatementReceiptSub");

        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArCustomerBalanceHome arCustomerBalanceHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        ArrayList list = new ArrayList();

        //initialized EJB Home

        try {

            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

		try {

		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		  short precisionUnit = adCompany.getGlFunctionalCurrency().getFcPrecision();

		  boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();

		  StringBuffer jbossQl = new StringBuffer();

		  jbossQl.append("SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE (");

		  if (branchList.isEmpty())
		  	throw new GlobalNoRecordFoundException();

		  Iterator brIter = branchList.iterator();

		  AdBranchDetails details = (AdBranchDetails)brIter.next();
		  jbossQl.append("ips.arInvoice.invAdBranch=" + details.getBrCode());

		  while(brIter.hasNext()) {

		  		details = (AdBranchDetails)brIter.next();

		  		jbossQl.append(" OR ips.arInvoice.invAdBranch=" + details.getBrCode());

		  }

		  jbossQl.append(") ");
		  firstArgument = false;

	      Object obj[];

		  // Allocate the size of the object parameter

	      obj = new Object[criteriaSize];

		  if (criteria.containsKey("customerCode")) {

		  	 if (!firstArgument) {

		  	    jbossQl.append("AND ");

		     } else {

		     	firstArgument = false;
		     	jbossQl.append("WHERE ");

		     }

		  	 jbossQl.append("ips.arInvoice.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("customerCode");
		   	 ctr++;

		  }

		  if (criteria.containsKey("customerType")) {

		   	  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  }

		   	  jbossQl.append("ips.arInvoice.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerType");
		   	  ctr++;

	      }

		  if (criteria.containsKey("customerClass")) {

		   	  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  }

		   	  jbossQl.append("ips.arInvoice.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerClass");
		   	  ctr++;

	      }

		  if (criteria.containsKey("dateFrom")) {

			  if (!firstArgument) {
				  jbossQl.append("AND ");
			  } else {
				  firstArgument = false;
				  jbossQl.append("WHERE ");
			  }
			  jbossQl.append("ips.arInvoice.invDate>=?" + (ctr+1) + " ");
			  obj[ctr] = (Date)criteria.get("dateFrom");
			  ctr++;

		  }

		  if (criteria.containsKey("dateTo")) {

			  if (!firstArgument) {
				  jbossQl.append("AND ");
			  } else {
				  firstArgument = false;
				  jbossQl.append("WHERE ");
			  }
			  jbossQl.append("ips.arInvoice.invDate<=?" + (ctr+1) + " ");
			  obj[ctr] = (Date)criteria.get("dateTo");
			  ctr++;

		  }

		  if (criteria.containsKey("invoiceNumberFrom")) {

			  if (!firstArgument) {
				  jbossQl.append("AND ");
			  } else {
				  firstArgument = false;
				  jbossQl.append("WHERE ");
			  }
			  jbossQl.append("ips.arInvoice.invNumber>=?" + (ctr+1) + " ");
			  obj[ctr] = (String)criteria.get("invoiceNumberFrom");
			  ctr++;

		  }

		  if (criteria.containsKey("invoiceNumberTo")) {

			  if (!firstArgument) {
				  jbossQl.append("AND ");
			  } else {
				  firstArgument = false;
				  jbossQl.append("WHERE ");
			  }
			  jbossQl.append("ips.arInvoice.invNumber<=?" + (ctr+1) + " ");
			  obj[ctr] = (String)criteria.get("invoiceNumberTo");
			  ctr++;

		  }

		  if (criteria.containsKey("customerBatch")) {

		   	  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  }

		   	  jbossQl.append("ips.arInvoice.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerBatch");
		   	  ctr++;

	      }

		  if (!firstArgument) {

	   	     jbossQl.append("AND ");

	   	  } else {

	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");

	   	  }

		  jbossQl.append("ips.arInvoice.invVoid = 0 AND ips.ipsAmountPaid > 0 AND ips.arInvoice.invCreditMemo = 0 AND ips.arInvoice.invPosted = 1 AND ips.ipsAdCompany=" + AD_CMPNY + " ORDER BY ips.arInvoice.arCustomer.cstName, ips.arInvoice.invDate, ips.arInvoice.invNumber");

		  Collection arInvoicePaymentSchedules = arInvoicePaymentScheduleHome.getIpsByCriteria(jbossQl.toString(), obj);

		  boolean first = true;

		  Iterator i = arInvoicePaymentSchedules.iterator();

		  while (i.hasNext()) {

			  LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();

			  LocalArInvoice arInvoice = arInvoicePaymentSchedule.getArInvoice();

			  Collection arAppliedInvoices = arInvoicePaymentSchedule.getArAppliedInvoices();

			  Iterator j = arAppliedInvoices.iterator();

			  //System.out.println("payment schedule code : " + arInvoicePaymentSchedule.getIpsCode());

			  while (j.hasNext()) {

				  LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice) j.next();

				  //System.out.println("applied invoice code : " + arAppliedInvoice.getAiCode());

				  //if(arAppliedInvoice.getArReceipt().getRctPosted() == EJBCommon.FALSE) continue;
				  if(arAppliedInvoice.getArReceipt() == null ||
						  arAppliedInvoice.getArReceipt().getRctPosted() == EJBCommon.FALSE)
					  continue;
				  // Exclude Void Receipt
				  if(arAppliedInvoice.getArReceipt().getRctVoid()==1)
					  continue;

				  ArRepStatementDetails mdetails = new ArRepStatementDetails();

				  mdetails.setSmtDate(arInvoice.getInvDate());
				  mdetails.setSmtDueDate(arInvoicePaymentSchedule.getIpsDueDate());
				  mdetails.setSmtAmount(arInvoicePaymentSchedule.getIpsAmountDue());
				  mdetails.setSmtTransaction("Invoice #" + arInvoice.getInvNumber());
				  mdetails.setSmtReferenceNumber("Reference #" + arInvoice.getInvReferenceNumber());

				  mdetails.setSmtReceiptNumber("Receipt #" + arAppliedInvoice.getArReceipt().getRctNumber());
				  mdetails.setSmtReceiptDate(arAppliedInvoice.getArReceipt().getRctDate());
				  mdetails.setSmtReceiptApplyAmount(arAppliedInvoice.getAiApplyAmount());

				  list.add(mdetails);

			  }

		  }

		  if (REPORT_TYP.equals("CUSTOMER LEDGER")){
			  Collections.sort(list, ArRepStatementDetails.CustomerLedgerComparator);

		  } else {
			  Collections.sort(list, ArRepStatementDetails.DateComparator);
		  }
		  return list;

	  } catch (Exception ex) {

	  	  Debug.printStackTrace(ex);
	  	  throw new EJBException(ex.getMessage());

	  }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepStatementCreditMemoSub(HashMap criteria, String orderBy, String groupBy, String REPORT_TYP, ArrayList branchList, Integer AD_CMPNY) {

        Debug.print("ArRepStatementControllerBean executeArRepStatementCreditMemoSub");

        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArCustomerBalanceHome arCustomerBalanceHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        ArrayList list = new ArrayList();

        //initialized EJB Home

        try {

            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

		try {

		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		  short precisionUnit = adCompany.getGlFunctionalCurrency().getFcPrecision();

		  boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();

		  StringBuffer jbossQl = new StringBuffer();

		  jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv WHERE (");

		  if (branchList.isEmpty())
		  	throw new GlobalNoRecordFoundException();

		  Iterator brIter = branchList.iterator();

		  AdBranchDetails details = (AdBranchDetails)brIter.next();
		  jbossQl.append("inv.invAdBranch=" + details.getBrCode());

		  while(brIter.hasNext()) {

		  		details = (AdBranchDetails)brIter.next();

		  		jbossQl.append(" OR inv.invAdBranch=" + details.getBrCode());

		  }

		  jbossQl.append(") ");
		  firstArgument = false;

		  // Allocate the size of the object parameter

		  Object obj[] = new Object[criteriaSize];

		  if (criteria.containsKey("customerCode")) {

		  	 if (!firstArgument) {

		  	    jbossQl.append("AND ");

		     } else {

		     	firstArgument = false;
		     	jbossQl.append("WHERE ");

		     }

		  	 jbossQl.append("inv.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("customerCode");
		   	 ctr++;

		  }

		  if (criteria.containsKey("customerType")) {

		   	  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  }

		   	  jbossQl.append("inv.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerType");
		   	  ctr++;

	      }

		  if (criteria.containsKey("customerClass")) {

		   	  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  }

		   	  jbossQl.append("inv.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerClass");
		   	  ctr++;

	      }

		  if (criteria.containsKey("dateFrom")) {

			  if (!firstArgument) {
				  jbossQl.append("AND ");
			  } else {
				  firstArgument = false;
				  jbossQl.append("WHERE ");
			  }
			  jbossQl.append("inv.invDate>=?" + (ctr+1) + " ");
			  obj[ctr] = (Date)criteria.get("dateFrom");
			  ctr++;

		  }

		  if (criteria.containsKey("dateTo")) {

			  if (!firstArgument) {
				  jbossQl.append("AND ");
			  } else {
				  firstArgument = false;
				  jbossQl.append("WHERE ");
			  }
			  jbossQl.append("inv.invDate<=?" + (ctr+1) + " ");
			  obj[ctr] = (Date)criteria.get("dateTo");
			  ctr++;

		  }

		  if (criteria.containsKey("invoiceNumberFrom")) {

			  if (!firstArgument) {
				  jbossQl.append("AND ");
			  } else {
				  firstArgument = false;
				  jbossQl.append("WHERE ");
			  }
			  jbossQl.append("inv.invNumber>=?" + (ctr+1) + " ");
			  obj[ctr] = (String)criteria.get("invoiceNumberFrom");
			  ctr++;

		  }

		  if (criteria.containsKey("invoiceNumberTo")) {

			  if (!firstArgument) {
				  jbossQl.append("AND ");
			  } else {
				  firstArgument = false;
				  jbossQl.append("WHERE ");
			  }
			  jbossQl.append("inv.invNumber<=?" + (ctr+1) + " ");
			  obj[ctr] = (String)criteria.get("invoiceNumberTo");
			  ctr++;

		  }

		  if (criteria.containsKey("customerBatch")) {

		   	  if (!firstArgument) {
		   	     jbossQl.append("AND ");
		   	  } else {
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");
		   	  }

		   	  jbossQl.append("inv.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerBatch");
		   	  ctr++;

	      }

		  if (!firstArgument) {

	   	     jbossQl.append("AND ");

	   	  } else {

	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");

	   	  }

		  jbossQl.append("inv.invVoid=0 AND inv.invCreditMemo = 0 AND inv.invPosted = 1 AND inv.invAdCompany=" + AD_CMPNY + " ORDER BY inv.arCustomer.cstName, inv.invDate, inv.invNumber");

		  Collection arInvoices = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);

		  Iterator i = arInvoices.iterator();

		  while (i.hasNext()) {

			  LocalArInvoice arInvoice = (LocalArInvoice)i.next();

			  Collection arCreditMemoes = arInvoiceHome.findByInvCreditMemoAndInvCmInvoiceNumberAndInvVoidAndInvPosted(
					  EJBCommon.TRUE, arInvoice.getInvNumber(), EJBCommon.FALSE, EJBCommon.TRUE, AD_CMPNY);

			  Iterator j = arCreditMemoes.iterator();

			  while (j.hasNext()) {

				  LocalArInvoice arCreditMemo = (LocalArInvoice) j.next();

				  ArRepStatementDetails mdetails = new ArRepStatementDetails();

				  mdetails.setSmtAmount(arInvoice.getInvAmountDue());
				  mdetails.setSmtDate(arInvoice.getInvDate());
				  mdetails.setSmtTransaction("Invoice #" + arInvoice.getInvNumber());
				  mdetails.setSmtReferenceNumber("Reference #" + arInvoice.getInvReferenceNumber());

				  mdetails.setSmtCreditMemoNumber("Credit Memo #" + arCreditMemo.getInvNumber());
				  mdetails.setSmtCreditMemoDate(arCreditMemo.getInvDate());
				  mdetails.setSmtCreditMemoAmount(arCreditMemo.getInvAmountDue());

				  list.add(mdetails);

			  }

		  }


		  if (REPORT_TYP.equals("CUSTOMER LEDGER")){
			  Collections.sort(list, ArRepStatementDetails.CustomerLedgerComparator);

		  } else {
			  Collections.sort(list, ArRepStatementDetails.DateComparator);
		  }

		  return list;

	  } catch (Exception ex) {

	  	  Debug.printStackTrace(ex);
	  	  throw new EJBException(ex.getMessage());

	  }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {

    	Debug.print("ArRepStatementControllerBean getAdPrfArUseCustomerPulldown");

    	LocalAdPreferenceHome adPreferenceHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}


    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

    		return adPreference.getPrfArUseCustomerPulldown();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

	// private methods


private byte getAdPrfArDetailedReceivable(Integer AD_CMPNY) {



		Debug.print("ArRepStatementControllerBean getAdPrfArDetailedReceivable");


        LocalAdPreferenceHome adPreferenceHome = null;

        // Initialize EJB Home

        try {

        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try{

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	return adPreference.getPrfArDetailedReceivable();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }



    }


    private short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

          return  adCompany.getGlFunctionalCurrency().getFcPrecision();

        } catch (Exception ex) {

        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());

        }

     }

	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

	    Debug.print("ArRepStatementControllerBean convertForeignToFunctionalCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT / CONVERSION_RATE;

         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepStatementControllerBean ejbCreate");

    }
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArOpenIbAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArRepStatementControllerBean getArOpenIbAll");

        LocalArInvoiceBatchHome arInvoiceBatchHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection arInvoiceBatches = arInvoiceBatchHome.findOpenIbAll(AD_BRNCH, AD_CMPNY);

        	Iterator i = arInvoiceBatches.iterator();

        	while (i.hasNext()) {

        		LocalArInvoiceBatch arInvoiceBatch = (LocalArInvoiceBatch)i.next();

        		list.add(arInvoiceBatch.getIbName());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }



}
