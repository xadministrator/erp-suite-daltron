
/*
 * ArRepTaxCodeListControllerBean.java
 *
 * Created on March 02, 2005, 04:06 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArRepTaxCodeListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepTaxCodeListControllerEJB"
 *           display-name="Used for viewing tax code lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepTaxCodeListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepTaxCodeListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepTaxCodeListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepTaxCodeListControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepTaxCodeList(HashMap criteria, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepTaxCodeListControllerBean executeArRepTaxCodeList");
        
        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
         
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  boolean firstArgument = true;
		  short ctr = 0;
		  int criteriaSize = criteria.size();	      
		
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT DISTINCT OBJECT(tc) FROM ArTaxCode tc ");
		  
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("taxName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("taxName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("tc.tcName LIKE '%" + (String)criteria.get("taxName") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("type")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("tc.tcType=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("type");
		   	  ctr++;
	   	  
	      }	
	      
	      if (!firstArgument) {		       	  	
	   	     jbossQl.append("AND ");		       	     
	   	  } else {		       	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");		       	  	 
	   	  }	     
	   	  
	   	  jbossQl.append("tc.tcAdCompany=" + AD_CMPNY + " ");

		  String orderBy = null;
          
          if (ORDER_BY.equals("TAX NAME")) {
	      	 
	      	  orderBy = "tc.tcName";
	      	  
	      } else if (ORDER_BY.equals("TAX TYPE")) {
	      	
	      	  orderBy = "tc.tcType";
	      	
	      }

		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }     
			  	      	
		  Collection arTaxCodes = arTaxCodeHome.getTcByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arTaxCodes.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arTaxCodes.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalArTaxCode arTaxCode = (LocalArTaxCode)i.next();   	  
		  	  
		  	  ArRepTaxCodeListDetails details = new ArRepTaxCodeListDetails();
		  	  details.setTclTaxName(arTaxCode.getTcName());
		  	  details.setTclTaxDescription(arTaxCode.getTcDescription());
		  	  details.setTclTaxType(arTaxCode.getTcType());
		  	  details.setTclTaxRate(arTaxCode.getTcRate());
		  	  
		  	  if (arTaxCode.getGlChartOfAccount() != null) {
		  	  	
		  	  	 details.setTclCoaGlTaxAccountNumber(arTaxCode.getGlChartOfAccount().getCoaAccountNumber());	                		                	    
		  	  	 details.setTclCoaGlTaxAccountDescription(arTaxCode.getGlChartOfAccount().getCoaAccountDescription());
		  	  	
		  	  }
		  	  
		  	  if (arTaxCode.getTcInterimAccount() != null) {
		  	  	
		  	  	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arTaxCode.getTcInterimAccount());
		  	  	
		  	  	details.setTclCoaGlInterimAccountNumber(glChartOfAccount.getCoaAccountNumber());	                		                    
		  	  	details.setTclCoaGlInterimAccountDescription(glChartOfAccount.getCoaAccountDescription());
		  	  	
		  	  }
		  	  
		  	  details.setTclEnable(arTaxCode.getTcEnable());

			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepTaxCodeListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepTaxCodeListControllerBean ejbCreate");
      
    }
}
