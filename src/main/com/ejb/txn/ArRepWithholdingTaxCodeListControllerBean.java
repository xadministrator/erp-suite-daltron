
/*
 * ArRepWithholdingTaxCodeListControllerBean.java
 *
 * Created on March 03, 2005, 02:53 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArRepWithholdingTaxCodeListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArRepWithholdingTaxCodeListControllerEJB"
 *           display-name="Used for viewing withholding tax code lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArRepWithholdingTaxCodeListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArRepWithholdingTaxCodeListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArRepWithholdingTaxCodeListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArRepWithholdingTaxCodeListControllerBean extends AbstractSessionBean {

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeArRepWithholdingTaxCodeList(HashMap criteria, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArRepWithholdingTaxCodeListControllerBean executeArRepWithholdingTaxCodeList");
        
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
         
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  boolean firstArgument = true;
		  short ctr = 0;
		  int criteriaSize = criteria.size();	      
		
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT DISTINCT OBJECT(wtc) FROM ArWithholdingTaxCode wtc ");
		  
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("taxName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("taxName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("wtc.wtcName LIKE '%" + (String)criteria.get("taxName") + "%' ");
		  	 
		  }
		  
	      if (!firstArgument) {		       	  	
	   	     jbossQl.append("AND ");		       	     
	   	  } else {		       	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");		       	  	 
	   	  }	     
	   	  
	   	  jbossQl.append("wtc.wtcAdCompany=" + AD_CMPNY + " ");

		  String orderBy = null;
          
          if (ORDER_BY.equals("TAX NAME")) {
	      	 
	      	  orderBy = "wtc.wtcName";
	      	  
	      } 

		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }     
			  	      	
		  Collection arWithholdingTaxCodes = arWithholdingTaxCodeHome.getWtcByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arWithholdingTaxCodes.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arWithholdingTaxCodes.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalArWithholdingTaxCode arWithholdingTaxCode = (LocalArWithholdingTaxCode)i.next();   	  
		  	  
		  	  ArRepWithholdingTaxCodeListDetails details = new ArRepWithholdingTaxCodeListDetails();
		  	  details.setWtlTaxName(arWithholdingTaxCode.getWtcName());
		  	  details.setWtlTaxDescription(arWithholdingTaxCode.getWtcDescription());
		  	  details.setWtlTaxRate(arWithholdingTaxCode.getWtcRate());
		  	  
		  	  if (arWithholdingTaxCode.getGlChartOfAccount() != null) {
		  	  	
		  	  	 details.setWtlCoaGlTaxAccountNumber(arWithholdingTaxCode.getGlChartOfAccount().getCoaAccountNumber());	                		                	    
		  	  	 details.setWtlCoaGlTaxAccountDescription(arWithholdingTaxCode.getGlChartOfAccount().getCoaAccountDescription());
		  	  	
		  	  }
		  	  
		  	  details.setWtlEnable(arWithholdingTaxCode.getWtcEnable());

			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepWithholdingTaxCodeListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("ArRepWithholdingTaxCodeListControllerBean ejbCreate");
      
    }
}
