package com.ejb.txn;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdLookUp;
import com.ejb.ad.LocalAdLookUpHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.ar.LocalArSalesOrderLineHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLineItem;
import com.ejb.inv.LocalInvLineItemTemplate;
import com.ejb.inv.LocalInvLineItemTemplateHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelDate;
import com.ejb.inv.LocalInvPriceLevelDateHome;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.ArModCustomerDetails;
import com.util.ArModSalesOrderDetails;
import com.util.ArModSalesOrderLineDetails;
import com.util.ArSalespersonDetails;
import com.util.ArTaxCodeDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="ArSalesOrderEntryControllerEJB"
 *           display-name="used for entering sales order"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArSalesOrderEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArSalesOrderEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArSalesOrderEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
*/

public class ArSalesOrderEntryControllerBean extends AbstractSessionBean {

    /**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer saveArSoEntry(com.util.ArModSalesOrderDetails details, String PYT_NM, String TC_NM, String FC_NM, String CST_CSTMR_CODE, ArrayList solList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalRecordAlreadyDeletedException,
		GlobalDocumentNumberNotUniqueException,
		GlobalConversionDateNotExistException,
		GlobalPaymentTermInvalidException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyVoidException,
		GlobalInvItemLocationNotFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalNoApprovalRequesterFoundException,
		GlobalRecordAlreadyAssignedException {

	    Debug.print("ArSalesOrderEntryControllerBean saveArSoEntry");

	    LocalArSalesOrderHome arSalesOrderHome = null;
	    LocalArSalesOrderLineHome arSalesOrderLineHome = null;
	    LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
	    LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
	    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
	    LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
	    LocalAdPaymentTermHome adPaymentTermHome = null;
	    LocalArCustomerHome arCustomerHome = null;
	    LocalArTaxCodeHome arTaxCodeHome = null;
	    LocalArSalespersonHome arSalespersonHome = null;
	    LocalInvItemLocationHome invItemLocationHome = null;
	    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
	    LocalAdPreferenceHome adPreferenceHome = null;
	    LocalAdApprovalHome adApprovalHome = null;
	    LocalAdAmountLimitHome adAmountLimitHome = null;
	    LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
	    LocalAdCompanyHome adCompanyHome = null;
	    LocalArInvoiceHome arInvoiceHome = null;
	    LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
	    LocalArSalesOrder arSalesOrder = null;

	    // Initialize EJB Home

	    try {

	        arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
	        arSalesOrderLineHome = (LocalArSalesOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
	        adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
	        adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
	        glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
	        glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
	        adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
	        arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
	        arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
	        arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
	        invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	        invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);

	    } catch(NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	        // validate if sales order is already deleted

	        try {

	            if(details.getSoCode() != null) {

	                arSalesOrder = arSalesOrderHome.findByPrimaryKey(details.getSoCode());

	            }

	        } catch(FinderException ex) {

	            throw new GlobalRecordAlreadyDeletedException();
	        }

	        // sales order void

			if (details.getSoCode() != null && details.getSoVoid() == EJBCommon.TRUE) {

				Collection arSalesOrderLines = arSalesOrder.getArSalesOrderLines();
				Iterator i = arSalesOrderLines.iterator();

				while (i.hasNext()) {

					LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) i.next();

					if (!arSalesOrderLine.getArSalesOrderInvoiceLines().isEmpty())
						throw new GlobalRecordAlreadyAssignedException();
				}

			    arSalesOrder.setSoVoid(EJBCommon.TRUE);
			    arSalesOrder.setSoLastModifiedBy(details.getSoLastModifiedBy());
			    arSalesOrder.setSoDateLastModified(details.getSoDateLastModified());

				return arSalesOrder.getSoCode();

			}

	        // validate if sales order is already posted, void, approved or pending

			if (details.getSoCode() != null) {
				//report Parameter saved
				arSalesOrder.setReportParameter(details.getReportParameter());
	        	if (arSalesOrder.getSoApprovalStatus() != null) {

	        		if (arSalesOrder.getSoApprovalStatus().equals("APPROVED") ||
	        		        arSalesOrder.getSoApprovalStatus().equals("N/A")) {

	        		    return arSalesOrder.getSoCode();

	        		} else if (arSalesOrder.getSoApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}
	        	}

        		if (arSalesOrder.getSoPosted() == EJBCommon.TRUE) {

        			return arSalesOrder.getSoCode();

        		} else if (arSalesOrder.getSoVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}

			// validate if document number is unique document number is automatic then set next sequence

			LocalArSalesOrder arExistingSalesOrder = null;

			try {

			    arExistingSalesOrder = arSalesOrderHome.findBySoDocumentNumber(
						details.getSoDocumentNumber(), AD_CMPNY);
				
			

			} catch (FinderException ex) {
			}

			LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

			if (details.getSoCode() == null) {


				String documentType = details.getSoDocumentType();

				try {
					if(documentType != null) {
						adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName(documentType , AD_CMPNY);
					}else {
						documentType = "AR SALES ORDER";
					}
				} catch(FinderException ex) {
					documentType = "AR SALES ORDER";
				}



				try {

					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName(documentType, AD_CMPNY);


 				} catch (FinderException ex) {

 				}

 				try {

 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

 				} catch (FinderException ex) {

 				}

				if (arExistingSalesOrder != null) {

					throw new GlobalDocumentNumberNotUniqueException();

				}


			} else {

				if (arExistingSalesOrder != null &&
						!arExistingSalesOrder.getSoCode().equals(details.getSoCode())) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (arSalesOrder.getSoDocumentNumber() != details.getSoDocumentNumber() &&
						(details.getSoDocumentNumber() == null || details.getSoDocumentNumber().trim().length() == 0)) {

					details.setSoDocumentNumber(arSalesOrder.getSoDocumentNumber());

				}

			}

			// validate if conversion date exists

			try {

				if (details.getSoConversionDate() != null) {

					LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
					LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

					if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
									details.getSoConversionDate(), AD_CMPNY);

					} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(
									adCompany.getGlFunctionalCurrency().getFcCode(), details.getSoConversionDate(), AD_CMPNY);

					}

				}

			} catch (FinderException ex) {

				throw new GlobalConversionDateNotExistException();

			}

			// validate if payment term has at least one payment schedule

			if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

				throw new GlobalPaymentTermInvalidException();

			}

			boolean isRecalculate = true;

			// create sales order
			System.out.println("1");
			if (details.getSoCode() == null) {

				arSalesOrder = arSalesOrderHome.create(details.getSoDate(), details.getSoDocumentNumber(),
				        details.getSoReferenceNumber(), details.getSoTransactionType(), details.getSoDescription(), details.getSoShippingLine(), details.getSoPort(), details.getSoBillTo(),
				        details.getSoShipTo(), details.getSoConversionDate(), details.getSoConversionRate(),
				        EJBCommon.FALSE, details.getSoMobile(),  null, EJBCommon.FALSE, null, details.getSoCreatedBy(),
				        details.getSoDateCreated(), details.getSoLastModifiedBy(), details.getSoDateLastModified(),
				        null, null, null, null, EJBCommon.FALSE, EJBCommon.FALSE, details.getSoMemo(), details.getSoTransactionStatus(), AD_BRNCH, AD_CMPNY);

			} else {

				// check if critical fields are changed
				System.out.println("2");
				if (!arSalesOrder.getArTaxCode().getTcName().equals(TC_NM) ||
						!arSalesOrder.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE) ||
						!arSalesOrder.getAdPaymentTerm().getPytName().equals(PYT_NM) ||
						solList.size() != arSalesOrder.getArSalesOrderLines().size()) {

					isRecalculate = true;
					System.out.println("3");
				} else if (solList.size() == arSalesOrder.getArSalesOrderLines().size()) {
					System.out.println("4");
					Iterator ilIter = arSalesOrder.getArSalesOrderLines().iterator();
					Iterator ilListIter = solList.iterator();

					while (ilIter.hasNext()) {

						LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine)ilIter.next();
						ArModSalesOrderLineDetails mdetails = (ArModSalesOrderLineDetails)ilListIter.next();
						System.out.println("4a");
						System.out.println(arSalesOrderLine.getInvItemLocation().getInvItem().getIiName()+"==="+mdetails.getSolIiName());
						System.out.println(arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription()+"==="+mdetails.getSolIiDescription());
						System.out.println(arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName()+"==="+mdetails.getSolLocName());
						System.out.println(arSalesOrderLine.getInvUnitOfMeasure().getUomName()+"==="+mdetails.getSolUomName());
						System.out.println(arSalesOrderLine.getSolQuantity()+"==="+mdetails.getSolQuantity());
						System.out.println(arSalesOrderLine.getSolUnitPrice()+"==="+mdetails.getSolUnitPrice());

						if (!arSalesOrderLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getSolIiName()) ||
								!arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription().equals(mdetails.getSolIiDescription()) ||
								!arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getSolLocName()) ||
								!arSalesOrderLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getSolUomName()) ||
								arSalesOrderLine.getSolQuantity() != mdetails.getSolQuantity() ||
								arSalesOrderLine.getSolUnitPrice() != mdetails.getSolUnitPrice() ||
								arSalesOrderLine.getSolTax() != mdetails.getSolTax() ||
								arSalesOrderLine.getSolMisc() != null && (!arSalesOrderLine.getSolMisc().equals(mdetails.getSolMisc()))
								) {

							isRecalculate = true;
							System.out.println("4b");
							break;

						}
						System.out.println("4c");
						isRecalculate = false;

					}
					System.out.println("4d");
				} else {
					System.out.println("4");
					isRecalculate = false;

				}

				arSalesOrder.setSoDate(details.getSoDate());
				arSalesOrder.setSoDocumentType(details.getSoDocumentType());
				arSalesOrder.setSoDocumentNumber(details.getSoDocumentNumber());
				arSalesOrder.setSoReferenceNumber(details.getSoReferenceNumber());
				arSalesOrder.setSoTransactionType(details.getSoTransactionType());
				arSalesOrder.setSoTransactionType(details.getSoTransactionType());
				arSalesOrder.setSoDescription(details.getSoDescription());
				arSalesOrder.setSoShippingLine(details.getSoShippingLine());
				arSalesOrder.setSoPort(details.getSoPort());
				arSalesOrder.setSoBillTo(details.getSoBillTo());
				arSalesOrder.setSoShipTo(details.getSoShipTo());
				arSalesOrder.setSoVoid(details.getSoVoid( ));
				arSalesOrder.setSoMobile(details.getSoMobile());
				arSalesOrder.setSoConversionDate(details.getSoConversionDate());
				arSalesOrder.setSoConversionRate(details.getSoConversionRate());
				arSalesOrder.setSoLastModifiedBy(details.getSoLastModifiedBy());
				arSalesOrder.setSoDateLastModified(details.getSoDateLastModified());
				arSalesOrder.setSoReasonForRejection(null);
				arSalesOrder.setSoMemo(details.getSoMemo());
				arSalesOrder.setSoTransactionStatus(details.getSoTransactionStatus());

			}

			arSalesOrder.setSoDocumentType(details.getSoDocumentType());

			arSalesOrder.setReportParameter(details.getReportParameter());

			LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
			arCustomer.addArSalesOrder(arSalesOrder);

			LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
			adPaymentTerm.addArSalesOrder(arSalesOrder);

			LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			arTaxCode.addArSalesOrder(arSalesOrder);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			glFunctionalCurrency.addArSalesOrder(arSalesOrder);

			LocalArSalesperson arSalesperson = details.getSoSlpSalespersonCode() == null ? null :
				arSalespersonHome.findBySlpSalespersonCode(details.getSoSlpSalespersonCode(), AD_CMPNY);

			if (arSalesperson != null)
				arSalesperson.addArSalesOrder(arSalesOrder);

			double ABS_TOTAL_AMOUNT = 0d;
			System.out.println("5");
			if(isRecalculate) {

			    // remove all sales order line items

				Collection arSalesOrderLines = arSalesOrder.getArSalesOrderLines();

				Iterator i = arSalesOrderLines.iterator();
				System.out.println("6");
				while (i.hasNext()) {

					LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine)i.next();

					Collection invTags = arSalesOrderLine.getInvTags();

		  	   	    Iterator x = invTags.iterator();

		  	   	    while (x.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();

		  	   	    	x.remove();

		  	   	    	invTag.remove();
		  	   	    }



					System.out.println("7");
					i.remove();

					arSalesOrderLine.remove();
					System.out.println("8");
				}

				// add new purchase order line item

				i = solList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ArModSalesOrderLineDetails mSolDetails = (ArModSalesOrderLineDetails) i.next();

					LocalArSalesOrderLine arSalesOrderLine = arSalesOrderLineHome.create(
					        mSolDetails.getSolLine(), mSolDetails.getSolLineIDesc(), mSolDetails.getSolQuantity(),
							mSolDetails.getSolUnitPrice(), mSolDetails.getSolAmount(), mSolDetails.getSolDiscount1(),
							mSolDetails.getSolDiscount2(),mSolDetails.getSolDiscount3(), mSolDetails.getSolDiscount4(),
							mSolDetails.getSolTotalDiscount(), 0d, mSolDetails.getSolMisc(), mSolDetails.getSolTax(), AD_CMPNY);










					arSalesOrder.addArSalesOrderLine(arSalesOrderLine);

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
						        mSolDetails.getSolLocName(), mSolDetails.getSolIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mSolDetails.getSolLine()));

					}

					ABS_TOTAL_AMOUNT += arSalesOrderLine.getSolAmount();

					invItemLocation.addArSalesOrderLine(arSalesOrderLine);

					LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
							mSolDetails.getSolUomName(), AD_CMPNY);

					invUnitOfMeasure.addArSalesOrderLine(arSalesOrderLine);

					if (arSalesOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc() == 1){
						this.createInvTagList(arSalesOrderLine,  mSolDetails.getSolTagList(), AD_CMPNY);

					}



				}

			}

			// ORDER STATUS

			Iterator invcIter = arCustomer.getArInvoices().iterator();
			//arSalesOrder.setSoOrderStatus("Good");
			String OrderStatus="";

			while(invcIter.hasNext()){

				LocalArInvoice arInvoice = (LocalArInvoice)invcIter.next();

				if(arInvoice.getInvPosted() == EJBCommon.FALSE)
					continue;

				Iterator ipsIter = arInvoice.getArInvoicePaymentSchedules().iterator();

				while(ipsIter.hasNext()){

					LocalArInvoicePaymentSchedule ips = (LocalArInvoicePaymentSchedule)ipsIter.next();

					try{
						if(ips.getIpsAmountPaid()==0 && OrderStatus.indexOf("Bad1")<0){
							OrderStatus="Bad1";
						}

						if(ips.getIpsAmountPaid()<ips.getIpsAmountDue() && OrderStatus.indexOf("Bad2")<0){

							if(OrderStatus!="")
								OrderStatus=OrderStatus+"/";

							OrderStatus=OrderStatus+"Bad2";

						}
						int result = 0;

						try{
							result = ips.getIpsDueDate().compareTo(((LocalArAppliedInvoice)ips.getArAppliedInvoices().toArray()[0]).getArReceipt().getRctDate());
							System.out.print("HERE IS: " +result);
						}catch(Exception ex){
							ex.printStackTrace();
						}


						if (result==-1 && OrderStatus.indexOf("Bad3")<0){
							if(OrderStatus!="")
								OrderStatus=OrderStatus+"/";

							OrderStatus=OrderStatus+"Bad3";
						}

					}catch(Exception ex){
						ex.printStackTrace();
						arSalesOrder.setSoOrderStatus(OrderStatus);
					}

				}
			}

			if(OrderStatus==""){
				OrderStatus="Good";
			}

			arSalesOrder.setSoOrderStatus(OrderStatus);


			// set sales order approval status

			String SO_APPRVL_STATUS = null;

			if(!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if ar sales order approval is enabled

				if (adApproval.getAprEnableArSalesOrder() == EJBCommon.FALSE) {

					SO_APPRVL_STATUS = "N/A";

				} else {

					// check if sales order is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR SALES ORDER", "REQUESTER", details.getSoLastModifiedBy(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit()) {

						SO_APPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AR SALES ORDER", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR SALES ORDER", arSalesOrder.getSoCode(),
										arSalesOrder.getSoDocumentNumber(), arSalesOrder.getSoDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

							}

						} else {

							boolean isApprovalUsersFound = false;

							Iterator i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR SALES ORDER", arSalesOrder.getSoCode(),
												arSalesOrder.getSoDocumentNumber(), arSalesOrder.getSoDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR SALES ORDER", arSalesOrder.getSoCode(),
												arSalesOrder.getSoDocumentNumber(), arSalesOrder.getSoDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						SO_APPRVL_STATUS = "PENDING";

					}

				}

				arSalesOrder.setSoApprovalStatus(SO_APPRVL_STATUS);





				// set post purchase order

				if(SO_APPRVL_STATUS.equals("N/A")) {

					arSalesOrder.setSoPosted(EJBCommon.TRUE);
					arSalesOrder.setSoPosted(EJBCommon.TRUE);
					arSalesOrder.setSoOrderStatus("Good");
					arSalesOrder.setSoPostedBy(arSalesOrder.getSoLastModifiedBy());
					arSalesOrder.setSoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

				}
				//arSalesOrder.setSoOrderStatus("Good");

			}

			//REFERENCE NUMBER GENERATOR
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			String newReferenceNumber = this.createCodedReferenceNumber(arSalesOrder,adCompany);

			arSalesOrder.setSoReferenceNumber(newReferenceNumber);

 	  	    return arSalesOrder.getSoCode();

	    } catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalConversionDateNotExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalPaymentTermInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyAssignedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteArSoEntry(Integer SO_CODE, String AD_USR, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException {

		Debug.print("ArSalesOrderEntryControllerBean deleteArSoEntry");

		LocalArSalesOrderHome arSalesOrderHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

		// Initialize EJB Home

		try {

		    arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
            			lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			System.out.println("SO_CODE="+SO_CODE);
			System.out.println("AD_USR="+AD_USR);


		    LocalArSalesOrder arSalesOrder = arSalesOrderHome.findByPrimaryKey(SO_CODE);
			System.out.println("1");
            if (arSalesOrder.getSoApprovalStatus() != null && arSalesOrder.getSoApprovalStatus().equals("PENDING")) {
            	System.out.println("2");
                Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR SALES ORDER",
                		arSalesOrder.getSoCode(), AD_CMPNY);

                Iterator i = adApprovalQueues.iterator();

                while(i.hasNext()) {

                    LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

                    adApprovalQueue.remove();

                }
                System.out.println("3");
            }

            adDeleteAuditTrailHome.create("AR SALES ORDER", arSalesOrder.getSoDate(), arSalesOrder.getSoDocumentNumber(), arSalesOrder.getSoReferenceNumber(),
 					0d, AD_USR, new Date(), AD_CMPNY);
            System.out.println("4"+arSalesOrder.getSoCode()+"SO Number"+arSalesOrder.getSoDocumentNumber());
		    arSalesOrder.remove();


		    System.out.println("5");
		} catch (FinderException ex) {

			getSessionContext().setRollbackOnly();
			throw new GlobalRecordAlreadyDeletedException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getApCstAll");

		LocalArCustomerHome arCustomerHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

		    arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

			Iterator i = arCustomers.iterator();

			while (i.hasNext()) {

				LocalArCustomer arCustomer = (LocalArCustomer)i.next();

				list.add(arCustomer.getCstCustomerCode());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdPytAll(Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getAdPytAll");

		LocalAdPaymentTermHome adPaymentTermHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adPaymentTerms = adPaymentTermHome.findEnabledPytAll(AD_CMPNY);

			Iterator i = adPaymentTerms.iterator();

			while (i.hasNext()) {

				LocalAdPaymentTerm adPaymentTerm = (LocalAdPaymentTerm)i.next();

				list.add(adPaymentTerm.getPytName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getAdUsrAll");

        LocalAdUserHome adUserHome = null;

        LocalAdUser adUser = null;

        Collection adUsers = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adUsers = adUserHome.findUsrAll(AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adUsers.isEmpty()) {

            return null;

        }

        Iterator i = adUsers.iterator();

        while (i.hasNext()) {

            adUser = (LocalAdUser)i.next();

            list.add(adUser.getUsrName());

        }

        return list;

    }


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArTcAll(Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getArTcAll");

		LocalArTaxCodeHome arTaxCodeHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);

			Iterator i = arTaxCodes.iterator();

			while (i.hasNext()) {

				LocalArTaxCode arTaxCode = (LocalArTaxCode)i.next();

        		list.add(arTaxCode.getTcName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArTaxCodeDetails getArTcByTcName(String TC_NM, Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getArTcByTcName");

		LocalArTaxCodeHome arTaxCodeHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);

			ArTaxCodeDetails details = new ArTaxCodeDetails();
			details.setTcType(arTaxCode.getTcType());
			details.setTcRate(arTaxCode.getTcRate());

			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getGlFcAllWithDefault");

		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		Collection glFunctionalCurrencies = null;

		LocalGlFunctionalCurrency glFunctionalCurrency = null;
		LocalAdCompany adCompany = null;


		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
					EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (glFunctionalCurrencies.isEmpty()) {

			return null;

		}

		Iterator i = glFunctionalCurrencies.iterator();

		while (i.hasNext()) {

			glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

			GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
					glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
					adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
							EJBCommon.TRUE : EJBCommon.FALSE);

			list.add(mdetails);

		}

		return list;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getInvLocAll");

		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			invLocations = invLocationHome.findLocAll(AD_CMPNY);

			if (invLocations.isEmpty()) {

				return null;

			}

			Iterator i = invLocations.iterator();

			while (i.hasNext()) {

				LocalInvLocation invLocation = (LocalInvLocation)i.next();
				String details = invLocation.getLocName();

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getInvGpQuantityPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvQuantityPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfArJournalLineNumber(Integer AD_CMPNY) {

		Debug.print("ArSalesEntryControllerBean getAdPrfArJournalLineNumber");

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfArInvoiceLineNumber();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArModSalesOrderDetails getArSoBySoCode(Integer SO_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ArSalesOrderEntryControllerBean getArSoBySoCode");

		LocalArSalesOrderHome arSalesOrderHome = null;

		// Initialize EJB Home

		try {

			arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
					lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArSalesOrder arSalesOrder = null;


			try {

				arSalesOrder = arSalesOrderHome.findByPrimaryKey(SO_CODE);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ArrayList list = new ArrayList();

			// get sales order lines if any

			Collection arSalesOrderLines = arSalesOrder.getArSalesOrderLines();

			Iterator i = arSalesOrderLines.iterator();

			while (i.hasNext()) {

				LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) i.next();

				ArModSalesOrderLineDetails solDetails = new ArModSalesOrderLineDetails();

				solDetails.setSolCode(arSalesOrderLine.getSolCode());
				solDetails.setSolLine(arSalesOrderLine.getSolLine());
				solDetails.setSolIiName(arSalesOrderLine.getInvItemLocation().getInvItem().getIiName());
				solDetails.setSolLocName(arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName());
				solDetails.setSolQuantity(arSalesOrderLine.getSolQuantity());
				solDetails.setSolUomName(arSalesOrderLine.getInvUnitOfMeasure().getUomName());
				solDetails.setSolUnitPrice(arSalesOrderLine.getSolUnitPrice());
				solDetails.setSolIiDescription(arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription());
				solDetails.setSolEDesc(arSalesOrderLine.getSolLineIDesc());
    			solDetails.setSolDiscount1(arSalesOrderLine.getSolDiscount1());
    			solDetails.setSolDiscount2(arSalesOrderLine.getSolDiscount2());
    			solDetails.setSolDiscount3(arSalesOrderLine.getSolDiscount3());
    			solDetails.setSolDiscount4(arSalesOrderLine.getSolDiscount4());
    			solDetails.setSolTotalDiscount(arSalesOrderLine.getSolTotalDiscount());
    			solDetails.setSolMisc(arSalesOrderLine.getSolMisc());
    			solDetails.setSolAmount(arSalesOrderLine.getSolAmount());
    			solDetails.setSolTax(arSalesOrderLine.getSolTax());

    			solDetails.setTraceMisc(arSalesOrderLine.getInvItemLocation().getInvItem().getIiTraceMisc());
	        	System.out.println(solDetails.getTraceMisc() + "<== getTraceMisc under getInvAdjByAdjCode controllerbean");
	        	if (solDetails.getTraceMisc() == 1){

	        		ArrayList tagList = new ArrayList();

	        		tagList = this.getInvTagList(arSalesOrderLine);
	        		solDetails.setSolTagList(tagList);
	        		solDetails.setTraceMisc(solDetails.getTraceMisc());


	        	}


				list.add(solDetails);

			}

			ArModSalesOrderDetails mSoDetails = new ArModSalesOrderDetails();

			mSoDetails.setSoCode(arSalesOrder.getSoCode());
			mSoDetails.setSoDate(arSalesOrder.getSoDate());
			mSoDetails.setSoDocumentType(arSalesOrder.getSoDocumentType());
			mSoDetails.setSoDocumentNumber(arSalesOrder.getSoDocumentNumber());
			mSoDetails.setSoReferenceNumber(arSalesOrder.getSoReferenceNumber());
			mSoDetails.setSoTransactionType(arSalesOrder.getSoTransactionType());
			mSoDetails.setSoDescription(arSalesOrder.getSoDescription());
			mSoDetails.setSoShippingLine(arSalesOrder.getSoShippingLine());
			mSoDetails.setSoPort(arSalesOrder.getSoPort());
			mSoDetails.setSoVoid(arSalesOrder.getSoVoid());
			mSoDetails.setSoMobile(arSalesOrder.getSoMobile());
			mSoDetails.setSoBillTo(arSalesOrder.getSoBillTo());
			mSoDetails.setSoShipTo(arSalesOrder.getSoShipTo());
			mSoDetails.setSoConversionDate(arSalesOrder.getSoConversionDate());
			mSoDetails.setSoConversionRate(arSalesOrder.getSoConversionRate());
			mSoDetails.setSoApprovalStatus(arSalesOrder.getSoApprovalStatus());
			mSoDetails.setSoPosted(arSalesOrder.getSoPosted());
			mSoDetails.setSoCreatedBy(arSalesOrder.getSoCreatedBy());
			mSoDetails.setSoDateCreated(arSalesOrder.getSoDateCreated());
			mSoDetails.setSoLastModifiedBy(arSalesOrder.getSoLastModifiedBy());
			mSoDetails.setSoDateLastModified(arSalesOrder.getSoDateLastModified());
			mSoDetails.setSoApprovedRejectedBy(arSalesOrder.getSoApprovedRejectedBy());
			mSoDetails.setSoDateApprovedRejected(arSalesOrder.getSoDateApprovedRejected());
			mSoDetails.setSoPostedBy(arSalesOrder.getSoPostedBy());
			mSoDetails.setSoDatePosted(arSalesOrder.getSoDatePosted());
			mSoDetails.setSoReasonForRejection(arSalesOrder.getSoReasonForRejection());
			mSoDetails.setSoCstCustomerCode(arSalesOrder.getArCustomer().getCstCustomerCode());
			mSoDetails.setSoCstName(arSalesOrder.getArCustomer().getCstName());
			mSoDetails.setSoPytName(arSalesOrder.getAdPaymentTerm().getPytName());
			mSoDetails.setSoTcName(arSalesOrder.getArTaxCode().getTcName());
			mSoDetails.setSoFcName(arSalesOrder.getGlFunctionalCurrency().getFcName());
        	mSoDetails.setSoTcRate(arSalesOrder.getArTaxCode().getTcRate());
        	mSoDetails.setSoTcType(arSalesOrder.getArTaxCode().getTcType());
        	mSoDetails.setSoApprovedRejectedBy(arSalesOrder.getSoApprovedRejectedBy());
        	mSoDetails.setSoDateApprovedRejected(arSalesOrder.getSoDateApprovedRejected());
        	mSoDetails.setSoMemo(arSalesOrder.getSoMemo());
        	mSoDetails.setSoTransactionStatus(arSalesOrder.getSoTransactionStatus());
        	mSoDetails.setSoOrderStatus(arSalesOrder.getSoOrderStatus());
        	mSoDetails.setReportParameter(arSalesOrder.getReportParameter());

			if (arSalesOrder.getArSalesperson() != null){

				mSoDetails.setSoSlpSalespersonCode(arSalesOrder.getArSalesperson().getSlpSalespersonCode());
				mSoDetails.setSoSlpName(arSalesOrder.getArSalesperson().getSlpName());

			}


			//get So Advance if have
			double SO_ADVNC_AMNT = 0d;


			Iterator i2 = arSalesOrder.getCmAdjustments().iterator();


			while(i2.hasNext()){

				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i2.next();

				if(cmAdjustment.getAdjVoid()==EJBCommon.FALSE &&
				   cmAdjustment.getAdjReconciled() == EJBCommon.FALSE) {
					SO_ADVNC_AMNT  +=  cmAdjustment.getAdjAmount();
				}



			}


			mSoDetails.setSoAdvanceAmount(SO_ADVNC_AMNT);

			mSoDetails.setSoSolList(list);

			return mSoDetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArModCustomerDetails getArCstByCstCustomerCode(String CST_CSTMR_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("ArSalesOrderEntryControllerBean getArCstByCstCustomerCode");

		LocalArCustomerHome arCustomerHome = null;
		LocalArSalespersonHome arSalespersonHome = null;
		// Initialize EJB Home

		try {
			arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
		    arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArCustomer arCustomer = null;


			try {

				arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ArModCustomerDetails mdetails = new ArModCustomerDetails();

			mdetails.setCstPytName(arCustomer.getAdPaymentTerm() != null ?
					arCustomer.getAdPaymentTerm().getPytName() : null);
			mdetails.setCstName(arCustomer.getCstName());

			if (arCustomer.getArSalesperson() != null && arCustomer.getCstArSalesperson2() == null) {

				mdetails.setCstSlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode());
				mdetails.setCstSlpName(arCustomer.getArSalesperson().getSlpName());

			} else if (arCustomer.getArSalesperson() == null && arCustomer.getCstArSalesperson2() != null) {

				LocalArSalesperson arSalesperson2 = null;
				arSalesperson2 = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());

				mdetails.setCstSlpSalespersonCode(arSalesperson2.getSlpSalespersonCode());
				mdetails.setCstSlpName(arSalesperson2.getSlpName());

			} if (arCustomer.getArSalesperson() != null && arCustomer.getCstArSalesperson2() != null) {

				mdetails.setCstSlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode());
				mdetails.setCstSlpName(arCustomer.getArSalesperson().getSlpName());
				LocalArSalesperson arSalesperson2 = null;
				arSalesperson2 = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());

				mdetails.setCstSlpSalespersonCode2(arSalesperson2.getSlpSalespersonCode());
				mdetails.setCstSlpName2(arSalesperson2.getSlpName());
			}

			if(arCustomer.getArCustomerClass().getArTaxCode() != null) {

				mdetails.setCstCcTcName(arCustomer.getArCustomerClass().getArTaxCode().getTcName());
				mdetails.setCstCcTcRate(arCustomer.getArCustomerClass().getArTaxCode().getTcRate());
				mdetails.setCstCcTcType(arCustomer.getArCustomerClass().getArTaxCode().getTcType());

			}

			if(arCustomer.getInvLineItemTemplate() != null) {
				mdetails.setCstLitName(arCustomer.getInvLineItemTemplate().getLitName());
			}

			return mdetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getInvUomByIiName");

		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemHome invItemHome = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			LocalInvItem invItem = null;
			LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

			invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
			invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

			Collection invUnitOfMeasures = null;
			Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
					invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
			while (i.hasNext()) {

				LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
				InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
				details.setUomName(invUnitOfMeasure.getUomName());

				if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

					details.setDefault(true);

				}

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public double getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(String CST_CSTMR_CODE, String II_NM, String UOM_NM, Integer AD_CMPNY) {

        Debug.print("ArSalesOrderEntryControllerBean getInvIiSalesPriceByIiNameAndUomName");

        LocalInvItemHome invItemHome = null;
        LocalInvPriceLevelHome invPriceLevelHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
         	invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
    			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

        	LocalArCustomer arCustomer = null;

        	try {

        		arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

        	} catch (FinderException ex) {

        		return 0d;

        	}

        	double unitPrice = 0d;

        	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        	LocalInvPriceLevel invPriceLevel = null;

        	try {

        		invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(II_NM, arCustomer.getCstDealPrice(), AD_CMPNY);

        		if (invPriceLevel.getPlAmount() == 0){

            		unitPrice = invItem.getIiSalesPrice();

            	} else {

            		unitPrice = invPriceLevel.getPlAmount();

            	}

        	} catch (FinderException ex) {

        		unitPrice = invItem.getIiSalesPrice();

        	}

        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(unitPrice * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }
     
     
     /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public double getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(String CST_CSTMR_CODE, String II_NM, Date SO_DT,  String UOM_NM, Integer AD_CMPNY) {

        Debug.print("ArSalesOrderEntryControllerBean getInvIiSalesPriceByIiNameAndUomName");

        LocalInvItemHome invItemHome = null;
        LocalInvPriceLevelHome invPriceLevelHome = null;
        LocalInvPriceLevelDateHome invPriceLevelDateHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
         	invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
            invPriceLevelDateHome = (LocalInvPriceLevelDateHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvPriceLevelDateHome.JNDI_NAME, LocalInvPriceLevelDateHome.class);	
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
    			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

        	LocalArCustomer arCustomer = null;

        	try {

        		arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

        	} catch (FinderException ex) {

        		return 0d;

        	}

        	double unitPrice = 0d;

        	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        	LocalInvPriceLevel invPriceLevel = null;

        	try {

        		invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(II_NM, arCustomer.getCstDealPrice(), AD_CMPNY);

				Collection invPriceLevelDates = invPriceLevelDateHome.findPdByIiCodeAndPdPriceLevel("ENABLE", invItem.getIiCode(),arCustomer.getCstDealPrice(), AD_CMPNY);
            
				unitPrice = invItem.getIiSalesPrice();
				
				
			
                if(invPriceLevelDates.size()<=0){
                
            	     if (invPriceLevel.getPlAmount() == 0){

	                    unitPrice = invItem.getIiSalesPrice();
	
	                } else {
	
	                    unitPrice = invPriceLevel.getPlAmount();
	
	                }
                
                }else{
                
                	boolean pricelLevelDateFound =false;
                	Iterator i = invPriceLevelDates.iterator();
                	
                	while(i.hasNext()){
                		System.out.println("searching price level date");

                		LocalInvPriceLevelDate invPriceLevelDate = (LocalInvPriceLevelDate)i.next();
                		            		
                		System.out.println("invoice date: " + SO_DT);
                		
                		System.out.println("deal date from date: " + invPriceLevelDate.getPdDateFrom());
                		
                		System.out.println("deal date to: " + invPriceLevelDate.getPdDateTo());
                		
						if(SO_DT.equals(invPriceLevelDate.getPdDateFrom()) || SO_DT.equals(invPriceLevelDate.getPdDateTo())){
							pricelLevelDateFound = true;
							
							System.out.println("priceleveldate found");
						    if (invPriceLevelDate.getPdAmount() == 0){

			                    unitPrice = invItem.getIiSalesPrice();
			
			                } else {
			
			                    unitPrice = invPriceLevelDate.getPdAmount();
			
			                }
			                
			                break;
	
						}


                		if(SO_DT.after(invPriceLevelDate.getPdDateFrom()) && SO_DT.before(invPriceLevelDate.getPdDateTo())){
                			
            				pricelLevelDateFound = true;
            				System.out.println("priceleveldate found");
						    if (invPriceLevelDate.getPdAmount() == 0){

			                    unitPrice = invItem.getIiSalesPrice();
			
			                } else {
			
			                    unitPrice = invPriceLevelDate.getPdAmount();
			
			                }
			                
			                break;

                		}
                			
                	
                	}
     
                
                }

        	} catch (FinderException ex) {

        		unitPrice = invItem.getIiSalesPrice();

        	}

        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(unitPrice * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getAdPrfArUseCustomerPulldown");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return  adPreference.getPrfArUseCustomerPulldown();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArSlpAll(Integer AD_BRNCH, Integer AD_CMPNY) {

	    Debug.print("ArSalesOrderEntryControllerBean getArSlpAll");

	    LocalArSalespersonHome arSalespersonHome = null;

	    ArrayList list = new ArrayList();

	    // Initialize EJB Home

	    try {

	        arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
	            lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	        Collection arSalespersons = arSalespersonHome.findSlpByBrCode(AD_BRNCH, AD_CMPNY);

	        Iterator i = arSalespersons.iterator();

	        while (i.hasNext()) {

	        	LocalArSalesperson arSalesperson = (LocalArSalesperson)i.next();

	        	ArSalespersonDetails details = new ArSalespersonDetails();

	        	details.setSlpSalespersonCode(arSalesperson.getSlpSalespersonCode());
	        	details.setSlpName(arSalesperson.getSlpName());

	        	list.add(details);

	        }

	        return list;

	    } catch (Exception ex) {

	    	Debug.printStackTrace(ex);
	    	throw new EJBException(ex.getMessage());

	    }

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalNotifiedUsersBySoCode(Integer SO_CODE, Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getAdApprovalNotifiedUsersBySoCode");

		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalArSalesOrderHome arSalesOrderHome = null;

		ArrayList list = new ArrayList();


		// Initialize EJB Home

		try {

			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArSalesOrder arSalesOrder = arSalesOrderHome.findByPrimaryKey(SO_CODE);

			if (arSalesOrder.getSoPosted() == EJBCommon.TRUE) {

				list.add("DOCUMENT POSTED");
				return list;

			}

			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("AR SALES ORDER", SO_CODE, AD_CMPNY);

			Iterator i = adApprovalQueues.iterator();

			while(i.hasNext()) {

				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

				list.add(adApprovalQueue.getAdUser().getUsrDescription());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
	throws GlobalConversionDateNotExistException {

		Debug.print("ArSalesOrderEntryControllerBean getFrRateByFrNameAndFrDate");

		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

			double CONVERSION_RATE = 1;

			// Get functional currency rate

			System.out.println("FC_NM="+FC_NM);
 			System.out.println("CONVERSION_DATE="+CONVERSION_DATE);
			
			
			if (!FC_NM.equals("USD")) {

				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
							CONVERSION_DATE, AD_CMPNY);

				System.out.println("pasok 1");
				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
				System.out.println("rate is : " + CONVERSION_RATE);
			}

			// Get set of book functional currency rate if necessary
			System.out.println("2nd curr: " + adCompany.getGlFunctionalCurrency().getFcName());
			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

				
				System.out.println("fcCode = " + adCompany.getGlFunctionalCurrency(). getFcCode());
				
				System.out.println("conversion date = " + CONVERSION_DATE);
				
				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
							CONVERSION_DATE, AD_CMPNY);
				System.out.println("pasok 2");
				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

			}
			System.out.println("pasok end");
			return CONVERSION_RATE;

		} catch (FinderException ex) {

			getSessionContext().setRollbackOnly();
			throw new GlobalConversionDateNotExistException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLitByCstLitName(String CST_LIT_NAME, Integer AD_CMPNY)
	    throws GlobalNoRecordFoundException {

	    Debug.print("ArSalesOrderEntryControllerBean getInvLitByCstLitName");

	    LocalInvLineItemTemplateHome invLineItemTemplateHome = null;

	    // Initialize EJB Home

	    try {

	    	invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	    	LocalInvLineItemTemplate invLineItemTemplate = null;


	    	try {

	    		invLineItemTemplate = invLineItemTemplateHome.findByLitName(CST_LIT_NAME, AD_CMPNY);

	    	} catch (FinderException ex) {

	    		throw new GlobalNoRecordFoundException();

	    	}

	    	ArrayList list = new ArrayList();

	    	// get line items if any

	    	Collection invLineItems = invLineItemTemplate.getInvLineItems();

	    	if (!invLineItems.isEmpty()) {

	    		Iterator i = invLineItems.iterator();

	    		while (i.hasNext()) {

	    			LocalInvLineItem invLineItem = (LocalInvLineItem) i.next();

	    			InvModLineItemDetails liDetails = new InvModLineItemDetails();

	    			liDetails.setLiCode(invLineItem.getLiCode());
	    			liDetails.setLiLine(invLineItem.getLiLine());
	    			liDetails.setLiIiName(invLineItem.getInvItemLocation().getInvItem().getIiName());
	    			liDetails.setLiLocName(invLineItem.getInvItemLocation().getInvLocation().getLocName());
	    			liDetails.setLiUomName(invLineItem.getInvUnitOfMeasure().getUomName());
	    			liDetails.setLiIiDescription(invLineItem.getInvItemLocation().getInvItem().getIiDescription());

	    			list.add(liDetails);

	    		}

	    	}

			return list;

	    } catch (GlobalNoRecordFoundException ex) {

	    	throw ex;

	    } catch (Exception ex) {

	    	Debug.printStackTrace(ex);
	    	throw new EJBException(ex.getMessage());

	    }

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public String getInvIiClassByIiName(String II_NM, Integer AD_CMPNY) {

        Debug.print("ArSalesOrderEntryControllerBean getInvIiClassByIiName");

        LocalInvItemHome invItemHome = null;

        // Initialize EJB Home

        try {

            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

            return invItem.getIiClass();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArEnablePaymentTerm(Integer AD_CMPNY) {

        Debug.print("ArSalesOrderEntryControllerBean getAdPrfArEnablePaymentTerm");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfArEnablePaymentTerm();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getSoTraceMisc(String II_NAME, Integer AD_CMPNY) {

        Debug.print("ArSalesOrderEntryControllerBean getSoTraceMisc");

        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isTraceMisc = false;
        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            if (invItem.getIiTraceMisc() == 1){
            	isTraceMisc = true;
            }
            return isTraceMisc;
    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArDisableSalesPrice(Integer AD_CMPNY) {

        Debug.print("ArSalesOrderEntryControllerBean getAdPrfArDisableSalesPrice");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfArDisableSalesPrice();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArSalesOrderReportParameters(Integer AD_CMPNY) {

        Debug.print("ArSalesOrderEntryControllerBean getArSalesOrderReportParameters");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR PRINT SALES ORDER PARAMETER", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getDocumentTypeList(String DCMNT_TYP, Integer AD_CMPNY) {

        Debug.print("ArSalesOrderEntryControllerBean getDocumentTypeList");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR SALES ORDER DOCUMENT TYPE", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArSoTransactionTypeList(Integer AD_CMPNY) {

        Debug.print("ArSalesOrderEntryControllerBean getArSoTransactionTypeList");

        ArrayList list = new ArrayList();
        LocalAdLookUpValueHome adLookUpValueHome = null;


        // Initialize EJB Home

        try {

        	adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

        	Collection adLookUpValues = adLookUpValueHome.findByLuName("AR SALES ORDER TRANSACTION TYPE", AD_CMPNY);

        	if(adLookUpValues.size() <=0 ) {
        		return list;
        	}

        	Iterator i = adLookUpValues.iterator();

        	while(i.hasNext()) {

        		LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
        		System.out.println(adLookUpValue.getLvName());
        		list.add(adLookUpValue.getLvName());
        	}


        	return list;



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    private String createCodedReferenceNumber(LocalArSalesOrder arSalesOrder, LocalAdCompany adCompany) {

    	System.out.println("create createCodedReferenceNumber" );
    	String newReferenceNumber = "";
    	if(!arSalesOrder.getSoReferenceNumber().equals("")) {
    		return arSalesOrder.getSoReferenceNumber();
    	}

    	//ICEI
    	if(adCompany.getCmpShortName().equals("ICEI") && arSalesOrder.getSoDocumentType() !=null) {

    		// "TransactionType" + "-" + "last two digit date year" + "-" + "sonumber"

    		//get trans
    		//String transTypeWord = arSalesOrder.getSoTransactionType().equals("")?"F":arSalesOrder.getSoTransactionType();

    		//get trans
    		if( arSalesOrder.getSoDocumentType().length() > 0) {
    			System.out.println("sad");
    			String transTypeWord = arSalesOrder.getSoDocumentType();

        		//get date year (2 digit)
        		DateFormat df = new SimpleDateFormat("yy"); // Just the year, with 2 digits
        		String formattedDate = df.format(arSalesOrder.getSoDate());

        		//get SO Number
        		String soNumber = arSalesOrder.getSoDocumentNumber();


        		newReferenceNumber = transTypeWord + "-" + formattedDate  + "-" + soNumber;
    		}



    	}





    	return newReferenceNumber;

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getInvTraceMisc(String II_NAME, Integer AD_CMPNY) {

        Debug.print("ArSalesOrderEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isTraceMisc = false;
        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            if (invItem.getIiTraceMisc() == 1){
            	isTraceMisc = true;
            }
            return isTraceMisc;
    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getAdLvReportTypeAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			String lookUpName = "AR SALES ORDER REPORT TYPE";



			Collection adLookUpValues = adLookUpValueHome.findByLuName(lookUpName, AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	private void createInvTagList(LocalArSalesOrderLine arSalesOrderLine, ArrayList list, Integer AD_CMPNY) throws Exception {

    	Debug.print("ArSalesOrderEntryControllerBean createInvTagList");

    	LocalAdUserHome adUserHome = null;
    	LocalInvTagHome invTagHome = null;

    	// Initialize EJB Home

    	try{
    		adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
    		invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}



    	try {
			System.out.println("aabot?");
	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
  	  	    Iterator t = list.iterator();

  	  	    LocalInvTag invTag  = null;
  	  	    System.out.println("umabot?");
  	  	    while (t.hasNext()){
  	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
  	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
  	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
  	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
  	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
  	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

  	  	    	if (tgLstDetails.getTgCode()==null){
  	  	    		System.out.println("ngcreate ba?");
      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
      	  	    			tgLstDetails.getTgType());

      	  	    	invTag.setArSalesOrderLine(arSalesOrderLine);
      	  	    	invTag.setInvItemLocation(arSalesOrderLine.getInvItemLocation());
      	  	    	LocalAdUser adUser = null;
      	  	    	try {
      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
      	  	    	}catch(FinderException ex){

      	  	    	}
      	  	    	invTag.setAdUser(adUser);
      	  	    	System.out.println("ngcreate ba?");
  	  	    	}

  	  	    }




		}catch(Exception ex) {
			throw ex;
		}
    }


    private ArrayList getInvTagList(LocalArSalesOrderLine arSalesOrderLine) {

    	ArrayList list = new ArrayList();

    	Collection invTags = arSalesOrderLine.getInvTags();
		Iterator x = invTags.iterator();
		while (x.hasNext()) {
			LocalInvTag invTag = (LocalInvTag) x.next();
			InvModTagListDetails tgLstDetails = new InvModTagListDetails();
			tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
			tgLstDetails.setTgSpecs(invTag.getTgSpecs());
			tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
			tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
			try{

				tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
			}
			catch(Exception ex){
				tgLstDetails.setTgCustodian("");
			}

			list.add(tgLstDetails);

			System.out.println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
			System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
			System.out.println(list+ "<== taglist inside controllerbean ");

		}

    	return list;

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvTransactionStatusAll(Integer AD_CMPNY) {

		Debug.print("ArSalesOrderEntryControllerBean getAdLvTransactionStatusAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			String lookUpName = "AR SALES ORDER TRANSACTION STATUS";



			Collection adLookUpValues = adLookUpValueHome.findByLuName(lookUpName, AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfSalesOrderSalespersonRequired(Integer AD_CMPNY) {

        Debug.print("ArSalesOrderEntryControllerBean getAdPrfSalesOrderSalespersonRequired");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfArSoSalespersonRequired();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


	// private methods

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("ArSalesOrderEntryControllerBean ejbCreate");

	}

}
