
/*
 * ArSalespersonControllerBean.java
 *
 * Created on January 31, 2006, 3:20 PM
 *
 * @author  Franco Antonio R. Roig
 */
 
package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchSalesperson;
import com.ejb.ad.LocalAdBranchSalespersonHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdResponsibilityDetails;
import com.util.ArSalespersonDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArSalespersonControllerEJB"
 *           display-name="Used for entering salespersons"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArSalespersonControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArSalespersonController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArSalespersonControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArSalespersonControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArSlpAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArSalespersonControllerBean getArSlpAll");

        LocalArSalespersonHome arSalespersonHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {

            arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
                lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	        	
	        Collection arSalespersons = arSalespersonHome.findSlpAll(AD_CMPNY);
	
	        if (arSalespersons.isEmpty()) {
	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	            
	        Iterator i = arSalespersons.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArSalesperson arSalesperson = (LocalArSalesperson)i.next();
     
	        	ArSalespersonDetails details = new ArSalespersonDetails(arSalesperson.getSlpCode(), 
	        			arSalesperson.getSlpSalespersonCode(), arSalesperson.getSlpName(), 
	        			arSalesperson.getSlpEntryDate(), arSalesperson.getSlpAddress(),
						arSalesperson.getSlpPhone(), arSalesperson.getSlpMobilePhone(), 
						arSalesperson.getSlpEmail(), AD_CMPNY);
	        		  	    
	        		list.add(details);
	        		
	        }              
	                                                        		        		        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrAll(Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArSalespersonControllerBean getAdBrAll");

    	LocalAdBranchHome adBranchHome = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranches = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranches = adBranchHome.findBrAll(AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranches.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = adBranches.iterator();
        
        while(i.hasNext()) {
        	
        	adBranch = (LocalAdBranch)i.next();
        	
        	AdBranchDetails details = new AdBranchDetails();

        	details.setBrBranchCode(adBranch.getBrBranchCode());
        	details.setBrCode(adBranch.getBrCode());
        	details.setBrName(adBranch.getBrName());
        	
        	list.add(details);
        	
        }
        
        return list;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrSlpAll(Integer BSLP_CODE, String RS_NM, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("ArSalespersonControllerBean getAdBrSlpAll");

    	LocalAdBranchSalespersonHome adBranchSalespersonHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchSalesperson adBranchSalesperson = null;
    	LocalAdBranch adBranch = null;
    	
    	Collection adBranchSalespersons = null;
    	
        ArrayList branchList = new ArrayList();
        
        // Initialize EJB Home

        try {
            
        	adBranchSalespersonHome = (LocalAdBranchSalespersonHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchSalespersonHome.JNDI_NAME, LocalAdBranchSalespersonHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchSalespersons = adBranchSalespersonHome.findBSLPBySLPCodeAndRsName(BSLP_CODE, RS_NM, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchSalespersons.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchSalespersons.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchSalesperson = (LocalAdBranchSalesperson)i.next();

	        	adBranch = adBranchHome.findByPrimaryKey(adBranchSalesperson.getAdBranch().getBrCode());
	        	
	        	AdBranchDetails details = new AdBranchDetails();
	        	details.setBrCode(adBranch.getBrCode());
	        	details.setBrName(adBranch.getBrName());
	        	
		    	branchList.add(details);
	        }
	        
        } catch (FinderException ex) {

        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return branchList;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public AdResponsibilityDetails getAdRsByRsCode(Integer RS_CODE) 
    	throws GlobalNoRecordFoundException {
    	
    	LocalAdResponsibilityHome adResHome = null;
    	LocalAdResponsibility adRes = null;
    	
    	try {
    		adResHome = (LocalAdResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
    	} catch (NamingException ex) {
    		
    	}
    	
    	try {
        	adRes = adResHome.findByPrimaryKey(RS_CODE);    		
    	} catch (FinderException ex) {
    		
    	}
    	
    	AdResponsibilityDetails details = new AdResponsibilityDetails();
    	details.setRsName(adRes.getRsName());
    	
    	return details;
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addArSlpEntry(com.util.ArSalespersonDetails details, 
    		ArrayList branchList, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("ArSalespersonControllerBean addArSlpEntry");
        
        LocalArSalespersonHome arSalespersonHome = null;
        LocalAdBranchHome adBranchHome = null;
        
    	LocalAdBranchSalespersonHome adBranchSalespersonHome = null;
    	LocalAdBranchSalesperson adBranchSalesperson = null;
    	LocalAdBranch adBranch = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
                lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
            adBranchSalespersonHome = (LocalAdBranchSalespersonHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchSalespersonHome.JNDI_NAME, LocalAdBranchSalespersonHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
                                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
            LocalArSalesperson arSalesperson = null;

	        try { 
	            
	            arSalesperson = arSalespersonHome.findBySlpSalespersonCode(details.getSlpSalespersonCode(), AD_CMPNY);
	            
	            throw new GlobalRecordAlreadyExistException();
	            
	        } catch (FinderException ex) {
	         	
	        	 	
	        }
	         
	    	// create new salesperson
	    				    	        	        	    	
	    	arSalesperson = arSalespersonHome.create(details.getSlpSalespersonCode(), details.getSlpName(),
	    			details.getSlpEntryDate(), details.getSlpAddress(), details.getSlpPhone(), details.getSlpMobilePhone(),
					details.getSlpEmail(), AD_CMPNY);
            
            // create new Branch Salesperson
        	Iterator i = branchList.iterator();
        	
        	while(i.hasNext()) {
        		
        		//AdModBranchStandardMemoLineDetails adBrSmlDetails = (AdModBranchStandardMemoLineDetails)i.next();
        		AdBranchDetails brDetails = (AdBranchDetails)i.next();
        		
        		adBranchSalesperson = adBranchSalespersonHome.create(AD_CMPNY);
        		
        		arSalesperson.addAdBranchSalesperson(adBranchSalesperson);
        		adBranch = adBranchHome.findByPrimaryKey(brDetails.getBrCode());
        		adBranch.addAdBranchSalesperson(adBranchSalesperson);

        	}

	    	        	    		        	    		    		    			        	        
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateArSlpEntry(com.util.ArSalespersonDetails details, String RS_NM, ArrayList branchList, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("ArSalespersonControllerBean updateArSlpEntry");
        
        LocalArSalespersonHome arSalespersonHome = null;
        LocalAdBranchHome adBranchHome = null;
        
    	LocalAdBranchSalespersonHome adBranchSalespersonHome = null;
    	LocalAdBranchSalesperson adBranchSalesperson = null;
    	LocalAdBranch adBranch = null;
    	                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
                lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
            adBranchSalespersonHome = (LocalAdBranchSalespersonHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchSalespersonHome.JNDI_NAME, LocalAdBranchSalespersonHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);


        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        
            LocalArSalesperson arSalesperson = null;
        	
        	try {
        	                   
	            LocalArSalesperson arExistingSalesperson = arSalespersonHome.findBySlpSalespersonCode(details.getSlpSalespersonCode(), AD_CMPNY);
	            
	            if (!arExistingSalesperson.getSlpCode().equals(details.getSlpCode())) {
	            
	                 throw new GlobalRecordAlreadyExistException();
	                 
	            }
	            
	        } catch (FinderException ex) {
	        	
	        }

			// find and update standard memo line

	        arSalesperson = arSalespersonHome.findByPrimaryKey(details.getSlpCode());
	         
	        arSalesperson.setSlpSalespersonCode(details.getSlpSalespersonCode());
	        arSalesperson.setSlpName(details.getSlpName());
	        arSalesperson.setSlpEntryDate(details.getSlpEntryDate());
	        arSalesperson.setSlpAddress(details.getSlpAddress());
	        arSalesperson.setSlpPhone(details.getSlpPhone());
	        arSalesperson.setSlpMobilePhone(details.getSlpMobilePhone());
	        arSalesperson.setSlpEmail(details.getSlpEmail());
	                   
            Collection adBranchSalespersons = adBranchSalespersonHome.findBSLPBySLPCodeAndRsName(arSalesperson.getSlpCode(), RS_NM, AD_CMPNY);
			
            Iterator j = adBranchSalespersons.iterator();
            
            // remove all adBranchSalesperson lines
            while(j.hasNext()) {
            	
            	adBranchSalesperson = (LocalAdBranchSalesperson)j.next();
            	
            	arSalesperson.dropAdBranchSalesperson(adBranchSalesperson);
            	
            	adBranch = adBranchHome.findByPrimaryKey(adBranchSalesperson.getAdBranch().getBrCode());
            	adBranch.dropAdBranchSalesperson(adBranchSalesperson);
            	adBranchSalesperson.remove();
            }
            
            // create new Branch Salesperson
        	Iterator i = branchList.iterator();
        	
        	while(i.hasNext()) {
        		
        		//AdModBranchStandardMemoLineDetails adBrSmlDetails = (AdModBranchStandardMemoLineDetails)i.next();
        		AdBranchDetails brDetails = (AdBranchDetails)i.next();
        		
        		adBranchSalesperson = adBranchSalespersonHome.create(AD_CMPNY);
        		
        		arSalesperson.addAdBranchSalesperson(adBranchSalesperson);
        		adBranch = adBranchHome.findByPrimaryKey(brDetails.getBrCode());
        		adBranch.addAdBranchSalesperson(adBranchSalesperson);

        	}
	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteArSlpEntry(Integer SLP_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
			   GlobalRecordAlreadyAssignedException {

        Debug.print("ArSalespersonControllerBean deleteArSlpEntry");
      
        LocalArSalespersonHome arSalespersonHome = null;

        // Initialize EJB Home
        
        try {

            arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
                lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);           
      
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
      
        try {
      	
      	    LocalArSalesperson arSalesperson = null;                
      
	        try {
	      	
	        	arSalesperson = arSalespersonHome.findByPrimaryKey(SLP_CODE);
	        	
	        } catch (FinderException ex) {
	        	
	        	throw new GlobalRecordAlreadyDeletedException();
	        	
	        }
	        
	        if (!arSalesperson.getArInvoices().isEmpty() || 
	                !arSalesperson.getArReceipts().isEmpty()||
	        		  !arSalesperson.getArCustomers().isEmpty()||
	        		  !arSalesperson.getArSalesOrders().isEmpty()) {
	        	
	        	throw new GlobalRecordAlreadyAssignedException();
	        	
	        }
	        
	        arSalesperson.remove();
	        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArStandardMemoLineControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    } 
    
    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ArStandardMemoLineControllerBean ejbCreate");
      
    }
    
}

