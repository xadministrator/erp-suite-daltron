
/*
 * ArStandardMemoLineClassControllerBean.java
 *
 * Created on April 27, 2018, 2:30 PM
 *
 * @author  Ruben P. Lamberte
 */
package com.ejb.txn;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineClass;
import com.ejb.ar.LocalArStandardMemoLineClassHome;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;

import com.util.AbstractSessionBean;
import com.util.ArCustomerDetails;
import com.util.ArModStandardMemoLineClassDetails;
import com.util.ArStandardMemoLineClassDetails;
import com.util.ArStandardMemoLineDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import org.joda.time.Days;
import org.joda.time.LocalDate;


/**
 * @ejb:bean name="ArStandardMemoLineClassControllerEJB"
 *           display-name="Used for assign unit price in standard memo lines based on customer class"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArStandardMemoLineClassControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArStandardMemoLineClassController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArStandardMemoLineClassControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/
public class ArStandardMemoLineClassControllerBean extends AbstractSessionBean{
    
    
    
    /**
    * @ejb:interface-method view-type="remote"
    **/
    public void saveArSmcEntry(ArrayList list, String USR_NM, String CC_NM, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException{

        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBranchHome adBranchHome = null;
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalArStandardMemoLineHome arStandardMemoLineHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalArStandardMemoLineClassHome arStandardMemoLineClassHome = null;
        
        Debug.print("ArStandardMemoLineClassControllerBean saveArSmcEntry");
        
        // Initialize EJB Home
       
        try {
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            arStandardMemoLineClassHome = (LocalArStandardMemoLineClassHome)EJBHomeFactory.
            lookUpLocalHome(LocalArStandardMemoLineClassHome.JNDI_NAME, LocalArStandardMemoLineClassHome.class);
                      
        }catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }   
           
        LocalAdCompany adCompany = null;
        
        try{
            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            Iterator i = list.iterator();
                    
            while(i.hasNext()) {
                
                ArModStandardMemoLineClassDetails  details = (ArModStandardMemoLineClassDetails) i.next();
                 
                String lineNumber = details.getSmcLine();
                
                LocalAdBranch adBranch = null;
                LocalArCustomerClass arCustomerClass = null;
                LocalArStandardMemoLine arStandardMemoLine = null;
                LocalArCustomer arCustomer = null;
                LocalArStandardMemoLineClass arStandardMemoLineClass = null;
              
                   
                try{
                    
                    System.out.println(CC_NM + " customer class name in controller");
                    arCustomerClass = arCustomerClassHome.findByCcName(CC_NM, AD_CMPNY);     
                }catch(FinderException ex){
                    //customer class not exist
                    System.out.println("Customer Class : " + CC_NM + " Not Exist in Line Number " + lineNumber);
                    throw ex;
                }
                
                try{
                    arStandardMemoLine = arStandardMemoLineHome.findBySmlName(details.getSmcStandardMemoLineName(), AD_CMPNY); 
                }catch(FinderException ex){
                    //standard memo line not exist
                    System.out.println("Standard Memo Line : " + details.getSmcStandardMemoLineName() + " Not Exist in Line Number " + lineNumber);
                    throw ex;
                }
                 
                try{
                    adBranch = adBranchHome.findByBrName(details.getSmcAdBranchName(), AD_CMPNY);
                }catch(FinderException ex){
                    //branch not exist
                    System.out.println("Branch : " + details.getSmcAdBranchName() + " Not Exist in Line Number " + lineNumber);
                    throw ex;
                }
             
                
                if(details.getSmcCode()!=null){
                    
                    //to update
                    try{
                        arStandardMemoLineClass = arStandardMemoLineClassHome.findByPrimaryKey(details.getSmcCode());
                    }catch(FinderException ex){
                        System.out.println("Standard Memo Line Class : " + details.getSmcAdBranchName() + " Not Exist for updating in Line Number " + lineNumber);
                        throw ex;
                    }
                    
                    
                    arStandardMemoLineClass.setSmcUnitPrice(details.getSmcUnitPrice()); 
                    arStandardMemoLineClass.setSmcStandardMemoLineDescription(details.getSmcStandardMemoLineDescription());
                    arStandardMemoLineClass.setSmcModifiedBy(USR_NM);
                    arStandardMemoLineClass.setSmcDateModified(new java.util.Date());
                    arStandardMemoLineClass.setSmcAdBranch(adBranch.getBrCode());

                    
                       
                    //ar customer if exist
                    if( details.getSmcCustomerCode()!=null && !details.getSmcCustomerCode().equals("")  ){
                        try{
                            arCustomer = arCustomerHome.findByCstCustomerCode(details.getSmcCustomerCode(), AD_CMPNY);
                    
                        }catch(FinderException ex){
                            System.out.println("Customer Code  : " + details.getSmcCustomerCode() + " Not Exist for updating in Line Number " + lineNumber);
                            throw ex;
                        }
                        arStandardMemoLineClass.setArCustomer(arCustomer);
                      
                    
                    }else{
                         arStandardMemoLineClass.setArCustomer(null);
                    }

                    
                    
                }else{
                    //to add      
                    
                   
                    arStandardMemoLineClass = arStandardMemoLineClassHome.create(details.getSmcUnitPrice(),details.getSmcStandardMemoLineDescription(), USR_NM,new java.util.Date(),USR_NM,new java.util.Date(),adBranch.getBrCode(),AD_CMPNY);
                    arStandardMemoLineClass.setArCustomerClass(arCustomerClass);
                    arStandardMemoLineClass.setArStandardMemoLine(arStandardMemoLine);
                    
                    
                    details.setSmcCustomerCode(arStandardMemoLineClass.getArCustomer()==null?"":arStandardMemoLineClass.getArCustomer().getCstCustomerCode());
                    details.setSmcCustomerName(arStandardMemoLineClass.getArCustomer()==null?"":arStandardMemoLineClass.getArCustomer().getCstName());
          

                   
                    //ar customer if exist
                    if( details.getSmcCustomerCode()!=null && !details.getSmcCustomerCode().equals("")  ){
                        try{
                            arCustomer = arCustomerHome.findByCstCustomerCode(details.getSmcCustomerCode(), AD_CMPNY);
                    
                        }catch(FinderException ex){
                            System.out.println("Customer Code  : " + details.getSmcCustomerCode() + " Not Exist for updating in Line Number " + lineNumber);
                            throw ex;
                        }
                        arStandardMemoLineClass.setArCustomer(arCustomer);
                      
                    
                    }
                }
               
            }
                 
        }catch(Exception ex){
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        }

    }
    
    
    /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteArSmcEntry(int SMC_CODE) 
        throws GlobalRecordAlreadyDeletedException{

        LocalArStandardMemoLineClassHome arStandardMemoLineClassHome = null;
        
        Debug.print("ArStandardMemoLineClassControllerBean deleteArSmcEntry");
        
        // Initialize EJB Home
       
        try {
       
            arStandardMemoLineClassHome = (LocalArStandardMemoLineClassHome)EJBHomeFactory.
            lookUpLocalHome(LocalArStandardMemoLineClassHome.JNDI_NAME, LocalArStandardMemoLineClassHome.class);
                      
        }catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }   
            
        try{
            
            LocalArStandardMemoLineClass arStandardMemoLineClass = null;
            arStandardMemoLineClass = arStandardMemoLineClassHome.findByPrimaryKey(SMC_CODE);

            if(arStandardMemoLineClass==null){
                throw new FinderException();
            }else{
                arStandardMemoLineClass.remove();
            }
        
        }catch(FinderException ex){
            //no standard memo line class exist or already deleted
            throw new GlobalRecordAlreadyDeletedException();
                   
        }catch(Exception ex){
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
        }

    }
    
    
    
    
    
    

    /**
    * @ejb:interface-method view-type="remote"
    **/
    public ArrayList getAllSmlNm(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException{
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalArStandardMemoLineHome arStandardMemoLineHome = null;
        
        Debug.print("ArStandardMemoLineClassControllerBean getAllSmlNm");
        
        // Initialize EJB Home
       
        try {
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
                    
        }catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        ArrayList list = new ArrayList();
          
        try{
            
            Collection smlList = arStandardMemoLineHome.findSmlAll(AD_CMPNY);
            
            Iterator i = smlList.iterator();
                    
            while(i.hasNext()) {
                LocalArStandardMemoLine arStandardMemoLine = (LocalArStandardMemoLine)i.next();
                
                list.add(arStandardMemoLine.getSmlName());

            }
            
        }catch(Exception ex){
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    }
    
    
    /**
    * @ejb:interface-method view-type="remote"
    **/
    public ArrayList getAllCcNm(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException{
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalArCustomerClassHome arCustomerClassHome = null;
        
        Debug.print("ArStandardMemoLineClassControllerBean getAllCcNm");
        
        // Initialize EJB Home
       
        try {
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
                    
        }catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        ArrayList list = new ArrayList();
          
        try{
            
            Collection ccList = arCustomerClassHome.findCcAll(AD_CMPNY);
            
            Iterator i = ccList.iterator();
                    
            while(i.hasNext()) {
                
                LocalArCustomerClass arCustomerClass = (LocalArCustomerClass)i.next();
                System.out.println(arCustomerClass.getCcName() + "  from controller");
                list.add(arCustomerClass.getCcName());

            }
            
        }catch(Exception ex){
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
        }
        
        return list;
            
    }
    
    
    /**
    * @ejb:interface-method view-type="remote"
    **/
    public com.util.ArModStandardMemoLineClassDetails getSmcByCcNmSmlNmCstCstmrCodeBrNm(String CC_NM, String SML_NM, String CST_CSTMR_CODE, String BR_NM, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException,FinderException{
        LocalArStandardMemoLineClassHome  arStandardMemoLineClassHome = null;

        LocalAdBranchHome adBranchHome = null;
        
        
        
        Debug.print("ArStandardMemoLineClassControllerBean getSmcByCcNmSmlNmCstCstmrCodeBrNm");
        
        // Initialize EJB Home
       
        try {
            
            arStandardMemoLineClassHome = (LocalArStandardMemoLineClassHome)EJBHomeFactory.
            lookUpLocalHome(LocalArStandardMemoLineClassHome.JNDI_NAME, LocalArStandardMemoLineClassHome.class);
          
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
                    
        }catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        ArrayList list = new ArrayList();
        ArModStandardMemoLineClassDetails details = null;  
        try{
            LocalAdBranch adBranch = adBranchHome.findByBrName(BR_NM,AD_CMPNY);
             
            LocalArStandardMemoLineClass arStandardMemoLineClass = arStandardMemoLineClassHome.findSmcByCcNameSmlNameCstCstmrCodeAdBrnch(CC_NM,SML_NM,CST_CSTMR_CODE, adBranch.getBrCode(),AD_CMPNY);
            
           
            details = new ArModStandardMemoLineClassDetails();
               
            
            details.setSmcCode(arStandardMemoLineClass.getSmcCode());
            details.setSmcAdBranchName(adBranch.getBrName());
            details.setSmcAdCompany(arStandardMemoLineClass.getSmcAdCompany());
            details.setSmcCustomerClassName(arStandardMemoLineClass.getArCustomerClass().getCcName());
            details.setSmcStandardMemoLineName(arStandardMemoLineClass.getArStandardMemoLine().getSmlName());
            details.setSmcStandardMemoLineDescription(arStandardMemoLineClass.getSmcStandardMemoLineDescription());
            details.setSmcUnitPrice(arStandardMemoLineClass.getSmcUnitPrice());
            details.setSmcCreatedBy(arStandardMemoLineClass.getSmcCreatedBy());
            details.setSmcDateCreated(arStandardMemoLineClass.getSmcDateCreated());
            details.setSmcModifiedBy(arStandardMemoLineClass.getSmcModifiedBy());
            details.setSmcDateModified(arStandardMemoLineClass.getSmcDateModified());

            
            if(arStandardMemoLineClass.getArCustomer()!=null){
                details.setSmcCustomerCode(arStandardMemoLineClass.getArCustomer().getCstCustomerCode());
                details.setSmcCustomerCode(arStandardMemoLineClass.getArCustomer().getCstName());
            
            }
            
        }catch(FinderException ex){
            throw ex;
        }catch(Exception ex){
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
        }
        
        return details;
            
    }
    
    
    /**
    * @ejb:interface-method view-type="remote"
    **/
    public ArrayList getAllCst(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException{
        
        LocalArCustomerHome arCustomerHome = null;
        
        Debug.print("ArStandardMemoLineClassControllerBean getAllCst");
        
        // Initialize EJB Home
       
        try {
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
          
                    
        }catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        ArrayList list = new ArrayList();
          
        try{
            
            Collection cstList = arCustomerHome.findEnabledCstAllOrderByCstName(AD_CMPNY);
            
            Iterator i = cstList.iterator();
                    
            while(i.hasNext()) {
                LocalArCustomer arCustomer = (LocalArCustomer)i.next();
                
                list.add(arCustomer.getCstCustomerCode());

            }
            
        }catch(Exception ex){
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
        } 
        
        
        return list;
        
        
    }
    
    
    
    /**
    * @ejb:interface-method view-type="remote"
    **/
    public ArrayList getAllBrNm(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException{
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBranchHome adBranchHome = null;
        
        Debug.print("ArStandardMemoLineClassControllerBean getAllBrNm");
        
        // Initialize EJB Home
       
        try {
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
                    
        }catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        ArrayList list = new ArrayList();
          
        try{
            
            Collection brList = adBranchHome.findBrAll(AD_CMPNY);
            
            Iterator i = brList.iterator();
                    
            while(i.hasNext()) {
                LocalAdBranch adBranch = (LocalAdBranch)i.next();
                
                list.add(adBranch.getBrName());

            }
            
        }catch(Exception ex){
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
        } 
        return list;
        
    }
    
    
    /**
    * @ejb:interface-method view-type="remote"
    **/
    public ArrayList getAllSmcByCcNm(String CC_NM, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException{
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBranchHome adBranchHome = null;
        LocalArStandardMemoLineClassHome arStandardMemoLineClassHome = null;
        
        Debug.print("ArStandardMemoLineClassControllerBean getAllCcNm");
        
        // Initialize EJB Home
       
        try {
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
            arStandardMemoLineClassHome = (LocalArStandardMemoLineClassHome)EJBHomeFactory.
            lookUpLocalHome(LocalArStandardMemoLineClassHome.JNDI_NAME, LocalArStandardMemoLineClassHome.class);
                    
        }catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        ArrayList list = new ArrayList();
          
        try{
            
            Collection smcList = arStandardMemoLineClassHome.findSmcByCcName(CC_NM, AD_CMPNY);
            
            Iterator i = smcList.iterator();
                    
            while(i.hasNext()) {
                
               
                
                LocalArStandardMemoLineClass arStandardMemoLineClass = (LocalArStandardMemoLineClass)i.next();
                System.out.println(arStandardMemoLineClass.getSmcCode()+ "  from controller");
                
                LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(arStandardMemoLineClass.getSmcAdBranch());
                
                ArModStandardMemoLineClassDetails details = new ArModStandardMemoLineClassDetails();
                
                details.setSmcCode(arStandardMemoLineClass.getSmcCode());
                details.setSmcAdBranchName(adBranch.getBrName());
                details.setSmcAdCompany(arStandardMemoLineClass.getSmcAdCompany());
                details.setSmcCustomerClassName(arStandardMemoLineClass.getArCustomerClass().getCcName());
                details.setSmcStandardMemoLineName(arStandardMemoLineClass.getArStandardMemoLine().getSmlName());
                details.setSmcStandardMemoLineDescription(arStandardMemoLineClass.getSmcStandardMemoLineDescription());
                details.setSmcUnitPrice(arStandardMemoLineClass.getSmcUnitPrice());
                details.setSmcCreatedBy(arStandardMemoLineClass.getSmcCreatedBy());
                details.setSmcDateCreated(arStandardMemoLineClass.getSmcDateCreated());
                details.setSmcModifiedBy(arStandardMemoLineClass.getSmcModifiedBy());
                details.setSmcDateModified(arStandardMemoLineClass.getSmcDateModified());
                details.setSmcCustomerCode(arStandardMemoLineClass.getArCustomer()==null?null:arStandardMemoLineClass.getArCustomer().getCstCustomerCode());
                details.setSmcCustomerName(arStandardMemoLineClass.getArCustomer()==null?null:arStandardMemoLineClass.getArCustomer().getCstName());

                list.add(details);

            }
            
        }catch (FinderException ex){
            
            return new ArrayList();
            
        }catch(Exception ex){
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
        }
        
        return list;
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public com.util.ArStandardMemoLineDetails getArSmlBySmlName(String SML_NM, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
        
        Debug.print("ArStandardMemoLineClassControllerBean getArSmlBySmlName");
        
        LocalArStandardMemoLineHome arStandardMemoLineHome = null;        
        
        // Initialize EJB Home
        
        try {
            
            arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            LocalArStandardMemoLine arStandardMemoLine = null;
            
            
            try {
                
                arStandardMemoLine = arStandardMemoLineHome.findBySmlName(SML_NM, AD_CMPNY);
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
                
            }
            
            ArStandardMemoLineDetails details = new ArStandardMemoLineDetails();
            details.setSmlCode(arStandardMemoLine.getSmlCode());
            details.setSmlName(arStandardMemoLine.getSmlName());
            details.setSmlDescription(arStandardMemoLine.getSmlDescription());
            details.setSmlUnitPrice(arStandardMemoLine.getSmlUnitPrice());
         
            
            return details;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
 
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public com.util.ArCustomerDetails getArCstByCstCstmrCode(String CST_CSTMR_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
        
        Debug.print("ArStandardMemoLineClassControllerBean getArCstByCstCstmrCode");
        
        LocalArCustomerHome arCustomerHome = null;        
        
        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            LocalArCustomer arCustomer = null;
            
            
            try {
                
                arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
                
            }
            
            ArCustomerDetails details = new ArCustomerDetails();
            details.setCstCustomerCode(arCustomer.getCstCustomerCode());
            details.setCstName(arCustomer.getCstName());
          
            return details;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
   
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ArStandardMemoLineClassControllerBean ejbCreate");
    } 
    
}
