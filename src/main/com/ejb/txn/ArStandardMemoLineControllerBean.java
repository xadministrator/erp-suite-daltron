
/*
 * ArStandardMemoLineControllerBean.java
 *
 * Created on March 08, 2004, 3:30 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchArTaxCode;
import com.ejb.ad.LocalAdBranchArTaxCodeHome;
import com.ejb.ad.LocalAdBranchBankAccount;
import com.ejb.ad.LocalAdBranchBankAccountHome;
import com.ejb.ad.LocalAdBranchCustomer;
import com.ejb.ad.LocalAdBranchCustomerHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchStandardMemoLine;
import com.ejb.ad.LocalAdBranchStandardMemoLineHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDiscount;
import com.ejb.ad.LocalAdDiscountHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentScheduleHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArAutoAccountingSegment;
import com.ejb.ar.LocalArAutoAccountingSegmentHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalance;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArPdc;
import com.ejb.ar.LocalArPdcHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptBatch;
import com.ejb.ar.LocalArReceiptBatchHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.ar.LocalArSalesOrderLineHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlReceivableAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlRevenueAccountNotFoundException;
import com.ejb.exception.ArINVAmountExceedsCreditLimitException;
import com.ejb.exception.ArTCInterimAccountInvalidException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.genfld.LocalGenField;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;

import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModBranchStandardMemoLineDetails;
import com.util.AdResponsibilityDetails;
import com.util.ArInvoiceDetails;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.ArModReceiptDetails;
import com.util.ArModSalesOrderLineDetails;
import com.util.ArModStandardMemoLineDetails;
import com.util.ArReceiptDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlInvestorAccountBalanceDetails;
import com.util.GlRepGeneralLedgerDetails;
import org.joda.time.DateTime;

/**
 * @ejb:bean name="ArStandardMemoLineControllerEJB"
 *           display-name="Used for entering standard memo lines"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArStandardMemoLineControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArStandardMemoLineController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArStandardMemoLineControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
*/

public class ArStandardMemoLineControllerBean extends AbstractSessionBean {


	/**
	    * @ejb:interface-method view-type="remote"
	    **/
        public String generateArInvestorBonusAndInterest(String SPL_SUPPLIER_CODE, boolean isRecalculate, String PERIOD_MONTH,int PERIOD_YEAR, String USER_NM, Integer AD_BRNCH, Integer AD_CMPNY)
	    		throws GlobalNoRecordFoundException,GlJREffectiveDatePeriodClosedException{

	       Debug.print("ArStandardMemoLineControllerBean generateArInvestorInterest");

	       LocalArInvoiceBatchHome arInvoiceBatchHome = null;
	       LocalApCheckHome apCheckHome = null;


	       LocalGlSetOfBookHome glSetOfBookHome = null;
	       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
	       LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;
	       LocalArCustomerHome arCustomerHome = null;
	       LocalArReceiptHome arReceiptHome = null;
	       // Initialize EJB Home

	       try {


	           arInvoiceBatchHome =  (LocalArInvoiceBatchHome)EJBHomeFactory.
	        	   lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);

	           apCheckHome =  (LocalApCheckHome)EJBHomeFactory.
		        	   lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);

	           glSetOfBookHome =  (LocalGlSetOfBookHome)EJBHomeFactory.
		        	   lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);

	           glAccountingCalendarValueHome =  (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
		        	   lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);


	           glInvestorAccountBalanceHome =  (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
		        	   lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);

	           arCustomerHome =  (LocalArCustomerHome)EJBHomeFactory.
		        	   lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

	           arReceiptHome =  (LocalArReceiptHome)EJBHomeFactory.
		        	   lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);




	       } catch (NamingException ex) {

	           throw new EJBException(ex.getMessage());

	       }



	       try {




                    int SUPPLIER_GENERATED = 0;
                    int SUPPLIER_ALREADY_GENERATED = 0;
                    int SUPPLIER_HAVE_DRAFT_AR = 0;
                    int SUPPLIER_HAVE_DRAFT_AP = 0;
                    int SUPPLIER_NO_DEPOST_WITHDRAW = 0;


                    short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);


	    	   LocalGlSetOfBook glJournalSetOfBook = null;
                   LocalGlAccountingCalendarValue glAccountingCalendarValue = null;
                    Date dateNow = DateTime.now().withTimeAtStartOfDay().toDate();

           	   try {

                        glJournalSetOfBook = glSetOfBookHome.findByDate(dateNow, AD_CMPNY);


                        Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(PERIOD_MONTH, EJBCommon.getIntendedDate(PERIOD_YEAR), AD_CMPNY);
			ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);
			LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);

			 glAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
						glSetOfBook.getGlAccountingCalendar().getAcCode(),
						PERIOD_MONTH, AD_CMPNY);

           	   } catch (FinderException ex) {

           	       throw new GlJREffectiveDateNoPeriodExistException();

           	   }


                    Collection glInvestorAccountBalancesSupplierList =  glInvestorAccountBalanceHome.findByAcvCode(
                                                 glAccountingCalendarValue.getAcvCode(), AD_CMPNY);



                    Iterator i = glInvestorAccountBalancesSupplierList.iterator();
                    //loop for multiple supplier investors
                    while(i.hasNext()){

                        LocalGlInvestorAccountBalance glInvestorAccountBalance = (LocalGlInvestorAccountBalance) i.next();

                        if(!SPL_SUPPLIER_CODE.equals("")){

                            if(!glInvestorAccountBalance.getApSupplier().getSplSupplierCode().equals(SPL_SUPPLIER_CODE)){
                                continue;
                            }


                        }


                        double THRES_HOLD = 500000d;
                       // double BONUS_PRCNT = 0.005d;
                      //  double INTEREST_PRCNT = 0.080083d;

                        double BONUS_PRCNT = glInvestorAccountBalance.getApSupplier().getApSupplierClass().getScInvestorBonusRate()/100;
                        double INTEREST_PRCNT = glInvestorAccountBalance.getApSupplier().getApSupplierClass().getScInvestorInterestRate()/100;

                        System.out.println("BONUS_PRCNT="+BONUS_PRCNT);
                        System.out.println("INTEREST_PRCNT="+INTEREST_PRCNT);

                        Date dateNowEndOfTheMonth = glInvestorAccountBalance.getGlAccountingCalendarValue().getAcvDateTo();


                        int supplierCode = glInvestorAccountBalance.getApSupplier().getSplCode();
                        System.out.println("Supplier:" + supplierCode);


                  //      LocalGlInvestorAccountBalance glInvestorAccountBalance = glInvestorAccountBalanceHome.findByAcvCodeAndSplCode(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),supplierCode , AD_CMPNY);

                        System.out.println(glInvestorAccountBalance.getIrabBonus() + " and " + glInvestorAccountBalance.getIrabInterest());
                        System.out.println("is Recalculate " +  isRecalculate);
                        double BEGGINING_BALANCE = glInvestorAccountBalance.getIrabBeginningBalance();
                        double RUN_BLNC_BONUS  = BEGGINING_BALANCE;
                        double RUN_BLNC_INTEREST = BEGGINING_BALANCE;


                        if( glInvestorAccountBalance.getIrabEndingBalance()==0){
                            System.out.println(supplierCode + " not generated");
                                SUPPLIER_NO_DEPOST_WITHDRAW++;
                                continue;
                        }

                        if(!isRecalculate){

                            if((glInvestorAccountBalance.getIrabBonus()!=0 && glInvestorAccountBalance.getIrabInterest()!=0) ){

                                System.out.println(supplierCode + " not generated");
                                SUPPLIER_ALREADY_GENERATED++;
                                continue;
                            }



                        }else{

                            //void existing receipt investor

                            Collection arReceipts = arReceiptHome.findInvestorReceiptByIrabCode(glInvestorAccountBalance.getIrabCode(),AD_CMPNY);
                            Iterator y = arReceipts.iterator();
                            //loop for multiple supplier investors

                            System.out.println("Entering voiding ");
                            while(y.hasNext()){

                                LocalArReceipt arReceipt = (LocalArReceipt)y.next();


                                ArrayList ilList = new ArrayList();

                                ArReceiptDetails  details = new ArReceiptDetails();

                                details.setRctCode(arReceipt.getRctCode());
                                details.setRctVoid(EJBCommon.TRUE);

                                System.out.println("rctCode to void"  + arReceipt.getRctCode());


                                this.saveArRctEntry(details, glInvestorAccountBalance,"INVESTOR FUND", "NONE", "NONE", "PGK", arReceipt.getArCustomer().getCstCustomerCode(), "", ilList, false, "", AD_BRNCH, AD_CMPNY);

                            }

                             BONUS_PRCNT = glInvestorAccountBalance.getIrabMonthlyBonusRate() /100;
                             INTEREST_PRCNT = glInvestorAccountBalance.getIrabMonthlyInterestRate() / 100;

                        }



                        Date acvDateFrom = glInvestorAccountBalance.getGlAccountingCalendarValue().getAcvDateFrom();
                        Date acvDateTo = glInvestorAccountBalance.getGlAccountingCalendarValue().getAcvDateTo();


                        Date RUN_DATE = acvDateFrom;
                        Date END_DATE = acvDateTo;


                        int CHECK_DATE_GAP = Days.daysBetween(new LocalDate(acvDateTo), new LocalDate(dateNowEndOfTheMonth)).getDays();

                        //This will check if lines is not exceeding in current date
                        if(CHECK_DATE_GAP < 0){
                            System.out.println("Date exceed. Loop will now exit");
                            break;
                        }


                        Collection arDraftRCTDrs = null;
                        Collection apDraftCHKDrs = null;
                        Collection arPostedRCTDrs = null;
                        Collection apPostedCHKDrs = null;

                        double RUN_BONUS = 0d;
                        double RUN_INTEREST = 0d;
                        System.out.println(glAccountingCalendarValue.getAcvDateFrom() +  " and  " + glAccountingCalendarValue.getAcvDateTo());
                        try {

                           //check if draft exist

                            arDraftRCTDrs = arReceiptHome.findUnPostedRctByRctDateRangeAndSplCode(
                                                acvDateFrom,acvDateTo,
                                                supplierCode, AD_BRNCH, AD_CMPNY);

                            if(arDraftRCTDrs.size()>0){
                                SUPPLIER_HAVE_DRAFT_AR++;

                            }

                            //get posted
                            arPostedRCTDrs = arReceiptHome.findPostedRctByRctDateRangeAndSplCode(
                                                acvDateFrom,acvDateTo,
                                                supplierCode, AD_BRNCH, AD_CMPNY);

                        } catch (Exception ex){}


                        try {

                            //check if draft exist

                            apDraftCHKDrs = apCheckHome.findUnPostedChkByChkDateRangeAndSplCode(
                                                     acvDateFrom,acvDateTo,
                                                     supplierCode, AD_BRNCH, AD_CMPNY);
                            if(apDraftCHKDrs.size()>0){
                                SUPPLIER_HAVE_DRAFT_AP++;

                            }


                            apPostedCHKDrs = apCheckHome.findPostedChkByChkDateRangeAndSplCode(
                                                     acvDateFrom,acvDateTo,
                                                     supplierCode, AD_BRNCH, AD_CMPNY);

                        } catch (Exception ex){}


                        if((SUPPLIER_HAVE_DRAFT_AR+SUPPLIER_HAVE_DRAFT_AP)>0){
                            continue;
                        }
                        System.out.println("RECEIPT: " + arPostedRCTDrs.size());
                        System.out.println("CHECK: " + apPostedCHKDrs.size());


                        ArrayList list = new ArrayList();

                        if(arPostedRCTDrs != null){

                                Iterator k = arPostedRCTDrs.iterator();

                                while(k.hasNext()){



                                    LocalArReceipt arReceipt = (LocalArReceipt) k.next();
                                    GlInvestorAccountBalanceDetails details = new GlInvestorAccountBalanceDetails();

                                    if(arReceipt.getGlInvestorAccountBalance()==null){
                                       details.setGlIrabCode(glInvestorAccountBalance.getIrabCode());
                                        details.setGlIrabDate(arReceipt.getRctDate());
                                        details.setGlIrabDocumentNumber(arReceipt.getRctNumber());
                                        details.setGlIrabType("ADD");
                                        details.setGlIrabAmount(arReceipt.getRctAmount());

                                    list.add(details);
                                    }


                                }

                        }


                        //
                        if(apPostedCHKDrs != null){

                                Iterator k = apPostedCHKDrs.iterator();

                                while(k.hasNext()){

                                        LocalApCheck apCheck = (LocalApCheck)k.next();
                                        GlInvestorAccountBalanceDetails details = new GlInvestorAccountBalanceDetails();

                                        details.setGlIrabCode(glInvestorAccountBalance.getIrabCode());
                                        details.setGlIrabDate(apCheck.getChkDate());
                                        details.setGlIrabDocumentNumber(apCheck.getChkDocumentNumber());
                                        details.setGlIrabType("SUB");
                                        details.setGlIrabAmount(apCheck.getChkAmount());

                                        list.add(details);
                                }

                        }



                        Collections.sort(list, GlInvestorAccountBalanceDetails.DateComparator);

                        Iterator x = list.iterator();



                        while(x.hasNext()){

                                GlInvestorAccountBalanceDetails details = (GlInvestorAccountBalanceDetails)x.next();
                                Date CURRENT_DATE = details.getGlIrabDate();

                                int DAYS_BETWEEN = Days.daysBetween(new LocalDate(RUN_DATE), new LocalDate(CURRENT_DATE)).getDays();

                                System.out.println(CURRENT_DATE+ " BETWEEN " + RUN_DATE + " = "+DAYS_BETWEEN);

                                RUN_BLNC_BONUS =  EJBCommon.roundIt(RUN_BLNC_BONUS,precisionUnit);



                                double CURRENT_BONUS = (RUN_BLNC_BONUS > THRES_HOLD) ? ( RUN_BLNC_BONUS - THRES_HOLD ) * (BONUS_PRCNT / 365) * DAYS_BETWEEN : 0d;
                                double CURRENT_INTEREST =RUN_BLNC_INTEREST * (INTEREST_PRCNT / 365) * DAYS_BETWEEN ;

                                System.out.println("CURRENT_BONUS="+CURRENT_BONUS);
                                System.out.println("DAYS BETWEEN=" + DAYS_BETWEEN);
                                System.out.println("RUN BAL BONUS="+ RUN_BLNC_BONUS);
                                System.out.println("CURRENT_INTEREST="+CURRENT_INTEREST);
                                System.out.println("RUN BAL INTEREST="+ RUN_BLNC_INTEREST);
                                System.out.println("AMOUNT="+details.getGlIrabAmount());

                                RUN_BLNC_BONUS += (details.getGlIrabType().equals("ADD") ? details.getGlIrabAmount() : details.getGlIrabAmount() * -1 );
                                RUN_BLNC_INTEREST += (details.getGlIrabType().equals("ADD") ? details.getGlIrabAmount() : details.getGlIrabAmount() * -1 );


                                System.out.println("------------------------------------------------------------------------");
                                RUN_DATE = CURRENT_DATE;
                                RUN_BONUS += CURRENT_BONUS;
                                RUN_INTEREST += CURRENT_INTEREST;


                        }



                      //  if(MONTHS_GENERATED>0){
                            int DAYS_BETWEEN = Days.daysBetween(new LocalDate(RUN_DATE), new LocalDate(END_DATE)).getDays() + 1 ;

                            double CURRENT_BONUS = (RUN_BLNC_BONUS > THRES_HOLD) ?( RUN_BLNC_BONUS - THRES_HOLD ) * (BONUS_PRCNT / 365) * DAYS_BETWEEN:0d;
                            double CURRENT_INTEREST = RUN_BLNC_INTEREST * (INTEREST_PRCNT / 365) * DAYS_BETWEEN ;

                            RUN_BONUS += CURRENT_BONUS;
                            RUN_INTEREST += CURRENT_INTEREST;
                            double[] myList = {RUN_BONUS,RUN_INTEREST};




                            System.out.println(END_DATE + " BETWEEN " + RUN_DATE + " = "+DAYS_BETWEEN);
                            System.out.println("CURRENT_BONUS="+CURRENT_BONUS);
                            System.out.println("CURRENT_INTEREST="+CURRENT_INTEREST);
                            System.out.println("RUN_BONUS="+RUN_BONUS);
                            System.out.println("RUN_INTEREST="+RUN_INTEREST);

                            glInvestorAccountBalance.setIrabTotalBonus(EJBCommon.roundIt(RUN_BONUS, precisionUnit));
                            glInvestorAccountBalance.setIrabTotalInterest(EJBCommon.roundIt(RUN_INTEREST, precisionUnit));


                            LocalArCustomer arCustomer = arCustomerHome.findByCstBySupplierCode(
                                            glInvestorAccountBalance.getApSupplier().getSplCode(), AD_CMPNY);

                             ArModInvoiceLineDetails mdetails;
                            ArReceiptDetails details;

                            double BONUS_PERCENT_PRINT = BONUS_PRCNT * 100;
                            double INTEREST_PERCENT_PRINT = INTEREST_PRCNT * 100;


                            ArrayList ilList = new ArrayList();

                            //save Bonus
                            if(RUN_BONUS!=0){
                                System.out.println("create bonus");
                                ilList = new ArrayList();
                                 mdetails = new ArModInvoiceLineDetails();


                                mdetails.setIlSmlName("Interest Income - Inscribed Stock");
                                mdetails.setIlDescription("Interest Income - Inscribed Stock");
                                mdetails.setIlQuantity(1);
                                mdetails.setIlUnitPrice(RUN_BONUS);
                                mdetails.setIlAmount(RUN_BONUS);
                                mdetails.setIlTax((byte)0);
                                ilList.add(mdetails);

                                details = new ArReceiptDetails();

                                details.setRctCode(null);
                                details.setRctDate(END_DATE);
                                details.setRctNumber(null);
                                details.setRctReferenceNumber(null);
                                details.setRctVoid(EJBCommon.FALSE);
                                details.setRctDescription("Bonus in " + PERIOD_MONTH + " @ " + Double.toString(BONUS_PERCENT_PRINT) + "%");
                                details.setRctConversionDate(EJBCommon.convertStringToSQLDate(null));
                                details.setRctConversionRate(1);
                                details.setRctSubjectToCommission(EJBCommon.FALSE);
                                details.setRctPaymentMethod("CASH");
                                details.setRctCustomerDeposit(EJBCommon.FALSE);
                                details.setRctCustomerName(arCustomer.getCstName());
                                details.setRctCustomerAddress(null);
                                details.setRctInvtrInvestorFund(EJBCommon.FALSE);
                                details.setRctInvtrNextRunDate(null);
                                details.setRctCreatedBy(USER_NM);
                                details.setRctDateCreated(new java.util.Date());
                                details.setRctLastModifiedBy(USER_NM);
                                details.setRctDateLastModified(new java.util.Date());


                                this.saveArRctEntry(details, glInvestorAccountBalance,"INVESTOR FUND", "NONE", "NONE", "PGK", arCustomer.getCstCustomerCode(), "", ilList, false, "", AD_BRNCH, AD_CMPNY);

                            }

                            //save Interest
                              if(RUN_INTEREST!=0){
                                System.out.println("create interest");
                                ilList = new ArrayList();
                                mdetails = new ArModInvoiceLineDetails();

                                mdetails.setIlSmlName("Interest Income - Inscribed Stock");
                                mdetails.setIlDescription("Interest Income - Inscribed Stock");
                                mdetails.setIlQuantity(1);
                                mdetails.setIlUnitPrice(RUN_INTEREST);
                                mdetails.setIlAmount(RUN_INTEREST);
                                mdetails.setIlTax((byte)0);
                                ilList.add(mdetails);

                                details = new ArReceiptDetails();

                                details.setRctCode(null);
                                details.setRctDate(END_DATE);
                                details.setRctNumber(null);
                                details.setRctReferenceNumber(null);
                                details.setRctVoid(EJBCommon.FALSE);
                                details.setRctDescription("Interest in " + PERIOD_MONTH + " @ " + Double.toString(INTEREST_PERCENT_PRINT) + "%");
                                details.setRctConversionDate(EJBCommon.convertStringToSQLDate(null));
                                details.setRctConversionRate(1);
                                details.setRctSubjectToCommission(EJBCommon.FALSE);
                                details.setRctPaymentMethod("CASH");
                                details.setRctCustomerDeposit(EJBCommon.FALSE);
                                details.setRctCustomerName(arCustomer.getCstName());
                                details.setRctCustomerAddress(null);
                                details.setRctInvtrInvestorFund(EJBCommon.FALSE);
                                details.setRctInvtrNextRunDate(null);
                                details.setRctCreatedBy(USER_NM);
                                details.setRctDateCreated(new java.util.Date());
                                details.setRctLastModifiedBy(USER_NM);
                                details.setRctDateLastModified(new java.util.Date());

                                this.saveArRctEntry(details, glInvestorAccountBalance,"INVESTOR FUND", "NONE", "NONE", "PGK", arCustomer.getCstCustomerCode(), "", ilList, false, "", AD_BRNCH, AD_CMPNY);



                              }
                        SUPPLIER_GENERATED++;

                        glInvestorAccountBalance.setIrabInterest(EJBCommon.TRUE);
                        glInvestorAccountBalance.setIrabBonus(EJBCommon.TRUE);
                        glInvestorAccountBalance.setIrabMonthlyBonusRate(BONUS_PRCNT*100);
                        glInvestorAccountBalance.setIrabMonthlyInterestRate(INTEREST_PRCNT*100);



                    }

                    //SUPPLIER_GENERATED
                   //SUPPLIER_ALREADY_GENERATED;
                   //SUPPLIER_HAVE_DRAFT_AR
                    //SUPPLIER_HAVE_DRAFT_AP


                    String status_result = "SUPPLIER GENERATE=" + SUPPLIER_GENERATED +  "  SUPPLIER ALREADY GENERATED=" + SUPPLIER_ALREADY_GENERATED + "  SUPPLIER HAVE DRAFT AR=" + SUPPLIER_HAVE_DRAFT_AR + "  SUPPLIER HAVE DRAFT AP=" + SUPPLIER_HAVE_DRAFT_AP + "  SUPPLIER NO DEPOST/WITHDRAW=" +SUPPLIER_NO_DEPOST_WITHDRAW;

                    return status_result;


	       }catch(GlJREffectiveDatePeriodClosedException gx){
	    	   throw new GlJREffectiveDatePeriodClosedException();


    	       } catch (Exception ex) {

	       	 Debug.printStackTrace(ex);
	          throw new EJBException(ex.getMessage());

	       }

	    }





        /**
	    * @ejb:interface-method view-type="remote"
	    **/
	    public ArrayList getSupplierInvestors(Integer AD_CMPNY)
	    		throws GlobalNoRecordFoundException,GlJREffectiveDatePeriodClosedException{

                 Debug.print("ArStandardMemoLineControllerBean generateArInvestorInterest");

	       LocalArInvoiceBatchHome arInvoiceBatchHome = null;
	       LocalApCheckHome apCheckHome = null;


	       LocalGlSetOfBookHome glSetOfBookHome = null;
	       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
	       LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;
	       LocalArCustomerHome arCustomerHome = null;
	       LocalArReceiptHome arReceiptHome = null;
	       // Initialize EJB Home

	       try {

	           glInvestorAccountBalanceHome =  (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
		        	   lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);


	       } catch (NamingException ex) {

	           throw new EJBException(ex.getMessage());

	       }


               ArrayList supplierList = new ArrayList();
                try{
                    Collection glInvestorAccountBalancesSupplierList =  glInvestorAccountBalanceHome.findByAcvCode(
                                                 1, AD_CMPNY);

                  Iterator i = glInvestorAccountBalancesSupplierList.iterator();
                    //loop for multiple supplier investors
                    while(i.hasNext()){

                        LocalGlInvestorAccountBalance glInvestorAccountBalance = (LocalGlInvestorAccountBalance) i.next();

                        if(glInvestorAccountBalance.getApSupplier().getApSupplierClass().getScName().contains("Investors")){
                              supplierList.add(glInvestorAccountBalance.getApSupplier().getSplSupplierCode());

                        }


                    }
                }catch(Exception ex){
                        	 Debug.printStackTrace(ex);
	          throw new EJBException(ex.getMessage());
                }

               return supplierList;
            }
	/**
	    * @ejb:interface-method view-type="remote"
	    **/
        public int generateArAccruedInterestIS(String USER_NM, Integer AD_BRNCH, Integer AD_CMPNY)
	    		throws GlobalNoRecordFoundException,GlJREffectiveDatePeriodClosedException{

	       Debug.print("ArStandardMemoLineControllerBean generateArAccruedInterestIS");
               boolean isError = false;
               int noOfSubmitted = 0;

	       LocalArInvoiceBatchHome arInvoiceBatchHome = null;
	       LocalApCheckHome apCheckHome = null;

	       // Initialize EJB Home

	       try {


	           arInvoiceBatchHome =  (LocalArInvoiceBatchHome)EJBHomeFactory.
	        	   lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
	           apCheckHome =  (LocalApCheckHome)EJBHomeFactory.
		        	   lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);


	       } catch (NamingException ex) {

	           throw new EJBException(ex.getMessage());

	       }



	       try {


	       	 Collection accruedInvoices = apCheckHome.findDirectChkForAccruedInterestISGeneration(AD_BRNCH, AD_CMPNY);

	       	 Iterator x = accruedInvoices.iterator();

	       	 while(x.hasNext()){

	       		LocalApCheck apCheck = (LocalApCheck)x.next();

                        Date maturityDate = apCheck.getChkInvtMaturityDate();
                        Date nextRunDate = apCheck.getChkInvtNextRunDate();

                        int CHECK_MATURITY_DATE_GAP = Days.daysBetween(new LocalDate(nextRunDate),new LocalDate(maturityDate)).getDays();

                           //This will check if lines is not exceeding in maturity date
                        if(CHECK_MATURITY_DATE_GAP < 0){
                            System.out.println("maturity date gap " + CHECK_MATURITY_DATE_GAP);
                            continue;
                        }

                        int CHECK_NEXTRUN_DATE_GAP = Days.daysBetween( new LocalDate(nextRunDate),new LocalDate(new Date())).getDays();

                           //This will check if lines is not exceeding in current date
                        if(CHECK_NEXTRUN_DATE_GAP < 0){
                              System.out.println("nextrundate date gap " + CHECK_NEXTRUN_DATE_GAP);
                            continue;
                        }

	       		 double interestAmount = EJBCommon.roundIt((apCheck.getChkInvtFaceValue() * (apCheck.getChkInvtCouponRate() / 100) ) / 12 , (short)2) ;
	       		 String customerCode = apCheck.getApSupplier().getSplSupplierCode();
	       		 String referenceNumber = apCheck.getChkDocumentNumber();

	       		 ArrayList ilList = new ArrayList();
		       	ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();



				 mdetails.setIlSmlName("Accrued Interest - IS");
				 mdetails.setIlDescription("Accrued Interest - IS");
				 mdetails.setIlQuantity(1);
				 mdetails.setIlUnitPrice(interestAmount);
				 mdetails.setIlAmount(interestAmount);
				 mdetails.setIlTax((byte)0);
				 ilList.add(mdetails);

				 ArInvoiceDetails details = new ArInvoiceDetails();
					Date d = new Date();
				    //SimpleDateFormat dtform = new SimpleDateFormat(Constants.DATE_FORMAT_INPUT);


					 details.setInvCode(null);
					 details.setInvType("MEMO LINES");
		             details.setInvDate(apCheck.getChkInvtNextRunDate());
		             details.setInvNumber(null);
		             details.setInvReferenceNumber(referenceNumber);
		             details.setInvVoid(EJBCommon.FALSE);
		             details.setInvDescription("SYSTEM GENERATED ACCRUED INTEREST FOR "+ referenceNumber);
		             details.setInvBillToAddress("");
		             details.setInvBillToContact("");
		             details.setInvBillToAltContact("");
		             details.setInvBillToPhone("");
		             details.setInvBillingHeader("");
		             details.setInvBillingFooter("");
		             details.setInvBillingHeader2("");
		             details.setInvBillingFooter2("");
		             details.setInvBillingHeader3(null);
		             details.setInvBillingFooter3(null);
		             details.setInvBillingSignatory("");
		             details.setInvSignatoryTitle("");
		             details.setInvShipToAddress("");
		             details.setInvShipToContact("");
		             details.setInvShipToAltContact("");
		             details.setInvShipToPhone("");
		             details.setInvLvFreight("");
		             details.setInvShipDate(EJBCommon.convertStringToSQLDate(null));
		             details.setInvConversionDate(EJBCommon.convertStringToSQLDate(null));
		             details.setInvConversionRate(1);
		             details.setInvDebitMemo((byte)0);
		             details.setInvSubjectToCommission((byte)0);
		             details.setInvClientPO("");
		             details.setInvEffectivityDate(apCheck.getChkInvtMaturityDate());
		             details.setInvRecieveDate(EJBCommon.convertStringToSQLDate(""));
		             details.setInvCreatedBy(USER_NM);
		             details.setInvDateCreated(new java.util.Date());
		             details.setInvLastModifiedBy(USER_NM);
		             details.setInvDateLastModified(new java.util.Date());

		             details.setInvDisableInterest(EJBCommon.TRUE);
		             details.setInvInterest(EJBCommon.TRUE);
		             details.setInvInterestReferenceNumber(referenceNumber);
		             details.setInvInterestAmount(interestAmount);
		             details.setInvInterestCreatedBy(USER_NM);
		             details.setInvInterestDateCreated(new java.util.Date());

		        //assign batch name , if not existed, create one




				 String INV_NUMBER = this.saveArInvEntry(details, "IMMEDIATE", "NONE", "NONE", "PGK", customerCode , null, ilList, true /*F if posted*/, null, AD_BRNCH, AD_CMPNY);

                                 //update the next Run Date



                                Calendar date = Calendar.getInstance();
                                date.setTime(nextRunDate);
                                date.add(Calendar.MONTH,+1);

                                 apCheck.setChkInvtNextRunDate(date.getTime());

				 /*Calendar calendar = new GregorianCalendar();
				//Add I month to InterestNextRunDate
		         calendar.setTime(arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate());
		         calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		         calendar.add(Calendar.DAY_OF_MONTH, 1);
		         System.out.println("OLD TIME IS : " + arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate().toString() + " -----------------------");

		         System.out.println("NEW TIME IS : " + calendar.getTime().toString() + " -----------------------");
				 arInvoicePaymentSchedule.getArInvoice().setInvInterestNextRunDate(calendar.getTime());*/
			noOfSubmitted ++;
	       	 }


	       	 System.out.println("accruedInvoices.size()="+noOfSubmitted);
	       	 return noOfSubmitted;
	       }catch(GlJREffectiveDatePeriodClosedException gx){
                   getSessionContext().setRollbackOnly();
	    	   throw new GlJREffectiveDatePeriodClosedException();


	       } catch (Exception ex) {
	       	getSessionContext().setRollbackOnly();
	       	 Debug.printStackTrace(ex);
	          throw new EJBException(ex.getMessage());

	       }

	    }


	    /**
        * @ejb:interface-method view-type="remote"
        **/
        public int generateArAccruedInterestTB(String USER_NM, Integer AD_BRNCH, Integer AD_CMPNY)
                    throws GlobalNoRecordFoundException,GlJREffectiveDatePeriodClosedException{

           Debug.print("ArStandardMemoLineControllerBean generateArAccruedInterestTB");

           LocalArInvoiceBatchHome arInvoiceBatchHome = null;
           LocalApCheckHome apCheckHome = null;
           int noOfSubmitted = 0;
           // Initialize EJB Home

           try {


               arInvoiceBatchHome =  (LocalArInvoiceBatchHome)EJBHomeFactory.
                       lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
               apCheckHome =  (LocalApCheckHome)EJBHomeFactory.
                               lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);


           } catch (NamingException ex) {

               throw new EJBException(ex.getMessage());

           }



           try {


             Collection accruedInvoices = apCheckHome.findDirectChkForAccruedInterestISGeneration(AD_BRNCH, AD_CMPNY);

             Iterator x = accruedInvoices.iterator();

             while(x.hasNext()){

                    LocalApCheck apCheck = (LocalApCheck)x.next();



                        Date maturityDate = apCheck.getChkInvtMaturityDate();
                        Date nextRunDate = apCheck.getChkInvtNextRunDate();

                        int CHECK_MATURITY_DATE_GAP = Days.daysBetween(new LocalDate(nextRunDate),new LocalDate(maturityDate)).getDays();

                           //This will check if lines is not exceeding in maturity date
                        if(CHECK_MATURITY_DATE_GAP < 0){
                            System.out.println("maturity date gap " + CHECK_MATURITY_DATE_GAP);
                            continue;
                        }

                        int CHECK_NEXTRUN_DATE_GAP = Days.daysBetween( new LocalDate(nextRunDate),new LocalDate(new Date())).getDays();

                           //This will check if lines is not exceeding in current date
                        if(CHECK_NEXTRUN_DATE_GAP < 0){
                              System.out.println("nextrundate date gap " + CHECK_NEXTRUN_DATE_GAP);
                            continue;
                        }
                     double interestAmount = EJBCommon.roundIt((apCheck.getChkInvtFaceValue() * (apCheck.getChkInvtCouponRate() / 100) ) / 12 , (short)2) ;
                     String customerCode = apCheck.getApSupplier().getSplSupplierCode();
                     String referenceNumber = apCheck.getChkDocumentNumber();

                     ArrayList ilList = new ArrayList();
                    ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();



                             mdetails.setIlSmlName("Accrued Interest - TB");
                             mdetails.setIlDescription("Accrued Interest - TB");
                             mdetails.setIlQuantity(1);
                             mdetails.setIlUnitPrice(interestAmount);
                             mdetails.setIlAmount(interestAmount);
                             mdetails.setIlTax((byte)0);
                             ilList.add(mdetails);

                             ArInvoiceDetails details = new ArInvoiceDetails();
                                    Date d = new Date();
                                //SimpleDateFormat dtform = new SimpleDateFormat(Constants.DATE_FORMAT_INPUT);


                                     details.setInvCode(null);
                                     details.setInvType("MEMO LINES");
                         details.setInvDate(apCheck.getChkInvtMaturityDate());
                         details.setInvNumber(null);
                         details.setInvReferenceNumber(referenceNumber);
                         details.setInvVoid(EJBCommon.FALSE);
                         details.setInvDescription("SYSTEM GENERATED ACCRUED INTEREST FOR "+ referenceNumber);
                         details.setInvBillToAddress("");
                         details.setInvBillToContact("");
                         details.setInvBillToAltContact("");
                         details.setInvBillToPhone("");
                         details.setInvBillingHeader("");
                         details.setInvBillingFooter("");
                         details.setInvBillingHeader2("");
                         details.setInvBillingFooter2("");
                         details.setInvBillingHeader3(null);
                         details.setInvBillingFooter3(null);
                         details.setInvBillingSignatory("");
                         details.setInvSignatoryTitle("");
                         details.setInvShipToAddress("");
                         details.setInvShipToContact("");
                         details.setInvShipToAltContact("");
                         details.setInvShipToPhone("");
                         details.setInvLvFreight("");
                         details.setInvShipDate(EJBCommon.convertStringToSQLDate(null));
                         details.setInvConversionDate(EJBCommon.convertStringToSQLDate(null));
                         details.setInvConversionRate(1);
                         details.setInvDebitMemo((byte)0);
                         details.setInvSubjectToCommission((byte)0);
                         details.setInvClientPO("");
                         details.setInvEffectivityDate(apCheck.getChkInvtMaturityDate());
                         details.setInvRecieveDate(EJBCommon.convertStringToSQLDate(""));
                         details.setInvCreatedBy(USER_NM);
                         details.setInvDateCreated(new java.util.Date());
                         details.setInvLastModifiedBy(USER_NM);
                         details.setInvDateLastModified(new java.util.Date());

                         details.setInvDisableInterest(EJBCommon.TRUE);
                         details.setInvInterest(EJBCommon.TRUE);
                         details.setInvInterestReferenceNumber(referenceNumber);
                         details.setInvInterestAmount(interestAmount);
                         details.setInvInterestCreatedBy(USER_NM);
                         details.setInvInterestDateCreated(new java.util.Date());

                    //assign batch name , if not existed, create one




                             String INV_NUMBER = this.saveArInvEntry(details, "IMMEDIATE", "NONE", "NONE", "PHP", customerCode , null, ilList, true /*F if posted*/, null, AD_BRNCH, AD_CMPNY);

                            //update the next Run Date



                                Calendar date = Calendar.getInstance();
                                date.setTime(nextRunDate);
                                date.add(Calendar.MONTH,+1);

                                 apCheck.setChkInvtNextRunDate(date.getTime());


                             /*Calendar calendar = new GregorianCalendar();
                            //Add I month to InterestNextRunDate
                     calendar.setTime(arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate());
                     calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
                     calendar.add(Calendar.DAY_OF_MONTH, 1);
                     System.out.println("OLD TIME IS : " + arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate().toString() + " -----------------------");

                     System.out.println("NEW TIME IS : " + calendar.getTime().toString() + " -----------------------");
                             arInvoicePaymentSchedule.getArInvoice().setInvInterestNextRunDate(calendar.getTime());*/

             }


             System.out.println("accruedInvoices.size()="+accruedInvoices.size());
             return accruedInvoices.size();
           }catch(GlJREffectiveDatePeriodClosedException gx){
               getSessionContext().setRollbackOnly();
               throw new GlJREffectiveDatePeriodClosedException();


           } catch (Exception ex) {
               getSessionContext().setRollbackOnly();
             Debug.printStackTrace(ex);
              throw new EJBException(ex.getMessage());

           }

        }

        /**
         * @ejb:interface-method view-type="remote"
         **/
        public ArrayList getOverdueIpsByNextRunDate(String USER_NM, Integer AD_BRNCH, Integer AD_CMPNY)
        		throws GlobalNoRecordFoundException,GlJREffectiveDatePeriodClosedException{

        	ArrayList list = new ArrayList();


    	 Debug.print("ArStandardMemoLineControllerBean generateArOverDueInvoices");

  	       LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
  	       LocalArInvoiceBatchHome arInvoiceBatchHome = null;


  	     try {

	    	   arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
	               lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
	           arInvoiceBatchHome =  (LocalArInvoiceBatchHome)EJBHomeFactory.
	        	   lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);


	       } catch (NamingException ex) {

	           throw new EJBException(ex.getMessage());

	       }


  	     	try {
	  	    	 Collection overDueInvoices = arInvoicePaymentScheduleHome.findOverdueIpsByNextRunDate(new Date(), AD_BRNCH, AD_CMPNY);

		       	 Iterator x = overDueInvoices.iterator();

		       	 while(x.hasNext()){
		       		 LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)x.next();
		       		 double balance = EJBCommon.roundIt((arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid()), (short)2) ;
		       		 double interestAmount = EJBCommon.roundIt((arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid()) * 0.02, (short)2) ;
		       		 String customerCode = arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode();
		       		 String referenceNumber = arInvoicePaymentSchedule.getArInvoice().getInvNumber();
		       		 ArModInvoiceDetails details = new ArModInvoiceDetails();

					Date d = new Date();
				    //SimpleDateFormat dtform = new SimpleDateFormat(Constants.DATE_FORMAT_INPUT);


					details.setInvCode(null);
					details.setInvType("MEMO LINES");
		             details.setInvDate(arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate());
		             details.setInvNumber(null);
		             details.setInvReferenceNumber(referenceNumber);
		             details.setInvVoid(EJBCommon.FALSE);
		             details.setInvDescription("SYSTEM GENERATED 2% INTEREST FOR "+ referenceNumber);
		             details.setInvBillToAddress("");
		             details.setInvBillToContact("");
		             details.setInvBillToAltContact("");
		             details.setInvBillToPhone("");
		             details.setInvBillingHeader("");
		             details.setInvBillingFooter("");
		             details.setInvBillingHeader2("");
		             details.setInvBillingFooter2("");
		             details.setInvBillingHeader3(null);
		             details.setInvBillingFooter3(null);
		             details.setInvBillingSignatory("");
		             details.setInvSignatoryTitle("");
		             details.setInvShipToAddress("");
		             details.setInvShipToContact("");
		             details.setInvShipToAltContact("");
		             details.setInvShipToPhone("");
		             details.setInvLvFreight("");
		             details.setInvShipDate(EJBCommon.convertStringToSQLDate(null));
		             details.setInvConversionDate(EJBCommon.convertStringToSQLDate(null));
		             details.setInvConversionRate(1);
		             details.setInvDebitMemo((byte)0);
		             details.setInvSubjectToCommission((byte)0);
		             details.setInvClientPO("");
		             details.setInvEffectivityDate(arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate());
		             details.setInvRecieveDate(EJBCommon.convertStringToSQLDate(""));
		             details.setInvCreatedBy(USER_NM);
		             details.setInvDateCreated(new java.util.Date());
		             details.setInvLastModifiedBy(USER_NM);
		             details.setInvDateLastModified(new java.util.Date());
		             details.setInvDisableInterest(EJBCommon.TRUE);
		             details.setInvInterest(EJBCommon.TRUE);
		             details.setInvInterestReferenceNumber(referenceNumber);
		             details.setInvInterestAmount(balance);
		             details.setInvInterestCreatedBy(USER_NM);
		             details.setInvInterestDateCreated(new java.util.Date());
		             details.setIpsCode(arInvoicePaymentSchedule.getIpsCode());

			        //assign batch name , if not existed, create one

			        String batchName = new SimpleDateFormat("MMMM yyyy").format(arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate());
				    System.out.println("Date to format: " + batchName + " -----------------------------");
				    try{

				    	System.out.println("Batch name detected: " + batchName);
				    	LocalArInvoiceBatch arInvBatch = arInvoiceBatchHome.findByIbName(batchName, AD_BRNCH, AD_CMPNY);


				    }catch(FinderException fx){

				    	System.out.println("New Batch name created: " + batchName);
				    	arInvoiceBatchHome.create(batchName, batchName, "OPEN", "INVOICE", new Date(), USER_NM, AD_BRNCH, AD_CMPNY);

				    }


				    details.setInvCstCustomerCode(customerCode);
				    details.setInvArBatchName(batchName);


			       	ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

			       	Collection arSalesInvoiceLines = arInvoicePaymentSchedule.getArInvoice().getArInvoiceLines();
				  	System.out.println("arSalesInvoiceLines.size()="+arSalesInvoiceLines.size());


				  	Iterator m = arSalesInvoiceLines.iterator();

				  	String memoLine = "";

				  	while (m.hasNext()) {

						 LocalArInvoiceLine arSalesInvoiceLine = (LocalArInvoiceLine)m.next();


						 if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Association Dues") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("ASD")){

							 memoLine = "AR - Interest ASD";

						 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("RPT")){

							 memoLine = "AR - Interest RPT";

						 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Parking Dues") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("PD")){

							 memoLine = "AR - Interest PD";

						 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Water") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("WTR")){

							 memoLine = "AR - Interest WTR";

						 } else {

							 memoLine = "AR - Interest MISC";
						 }


				  	}

					 mdetails.setIlSmlName(memoLine);
					 mdetails.setIlDescription(memoLine);
					 mdetails.setIlQuantity(1);
					 mdetails.setIlUnitPrice(interestAmount);
					 mdetails.setIlAmount(interestAmount);
					 mdetails.setIlTax((byte)0);

					 ArrayList mdetailsList = new ArrayList();
					 mdetailsList.add(mdetails);
				     details.setArModInvoiceLineDetailsList(mdetailsList);




				    list.add(details);


		       	 }



	       } catch (Exception ex) {

	       	 Debug.printStackTrace(ex);
	          throw new EJBException(ex.getMessage());

	       }

  	     return list;
        }






	/**
	    * @ejb:interface-method view-type="remote"
	    **/
	    public int generateArOverDueInvoices(String USER_NM, Integer AD_BRNCH, Integer AD_CMPNY)
	    		throws GlobalNoRecordFoundException,GlJREffectiveDatePeriodClosedException{

	       Debug.print("ArStandardMemoLineControllerBean generateArOverDueInvoices");

	       LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
	       LocalArInvoiceBatchHome arInvoiceBatchHome = null;

	       // Initialize EJB Home

	       try {

	    	   arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
	               lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
	           arInvoiceBatchHome =  (LocalArInvoiceBatchHome)EJBHomeFactory.
	        	   lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);


	       } catch (NamingException ex) {

	           throw new EJBException(ex.getMessage());

	       }


	       int LIMIT_TRANSACTION = 100;

	       int countTransaction = 0;
	       try {


	    	   String jbossSql = "SELECT OBJECT(ips) FROM ArInvoicePaymentSchedule ips WHERE ips.arInvoice.invPosted = 1 AND ips.arInvoice.arCustomer.cstAutoComputeInterest = 1 AND ips.ipsLock = 0 AND ips.arInvoice.invPosted = 1 AND ips.arInvoice.invInterest = 0 AND ips.arInvoice.invDisableInterest = 0 AND ips.arInvoice.invInterestNextRunDate IS NOT NULL AND ips.arInvoice.invInterestNextRunDate <= ?1 AND ips.ipsAmountDue > ips.ipsAmountPaid AND ips.arInvoice.invAdBranch = ?2 AND ips.ipsAdCompany = ?3";

	       //	 Collection overDueInvoices = arInvoicePaymentScheduleHome.findOverdueIpsByNextRunDate(new Date(), AD_BRNCH, AD_CMPNY);

	    	   Object obj[] = new Object[3];

	    	   obj[0] = EJBCommon.convertStringToSQLDate(  EJBCommon.convertSQLDateToString(new Date()));
	    	   obj[1] = AD_BRNCH;
	    	   obj[2] = AD_CMPNY;


	    	 Collection overDueInvoices = arInvoicePaymentScheduleHome.getIpsByCriteria(jbossSql, obj);

	       	 Iterator x = overDueInvoices.iterator();

	       	 while(x.hasNext() && countTransaction < LIMIT_TRANSACTION){
	       		 ArInvoiceDetails details = new ArModInvoiceDetails();

	       		 LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)x.next();

	       		 double balance = EJBCommon.roundIt((arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid()), (short)2) ;
	       		 double interestAmount = EJBCommon.roundIt((arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid()) * 0.02, (short)2) ;
	       		 String customerCode = arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode();
	       		 String referenceNumber = arInvoicePaymentSchedule.getArInvoice().getInvNumber();

	       		 ArrayList ilList = new ArrayList();
		       	ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

		       	Collection arSalesInvoiceLines = arInvoicePaymentSchedule.getArInvoice().getArInvoiceLines();
			  	System.out.println("arSalesInvoiceLines.size()="+arSalesInvoiceLines.size());


			  	Iterator m = arSalesInvoiceLines.iterator();

			  	String memoLine = "";

			  	while (m.hasNext()) {

					 LocalArInvoiceLine arSalesInvoiceLine = (LocalArInvoiceLine)m.next();


					 if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Association Dues") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("ASD")){

						 memoLine = "AR - Interest ASD";

					 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("RPT")){

						 memoLine = "AR - Interest RPT";

					 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Parking Dues") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("PD")){

						 memoLine = "AR - Interest PD";

					 } else if(arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("Water") || arSalesInvoiceLine.getArStandardMemoLine().getSmlDescription().contains("WTR")){

						 memoLine = "AR - Interest WTR";

					 } else {

						 memoLine = "AR - Interest MISC";
					 }


			  	}

				 mdetails.setIlSmlName(memoLine);
				 mdetails.setIlDescription(memoLine);
				 mdetails.setIlQuantity(1);
				 mdetails.setIlUnitPrice(interestAmount);
				 mdetails.setIlAmount(interestAmount);
				 mdetails.setIlTax((byte)0);

				 ArrayList mdetailsList = new ArrayList();
				 ilList.add(mdetails);


					Date d = new Date();
				    //SimpleDateFormat dtform = new SimpleDateFormat(Constants.DATE_FORMAT_INPUT);


					details.setInvCode(null);
					details.setInvType("MEMO LINES");
		             details.setInvDate(arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate());
		             details.setInvNumber(null);
		             details.setInvReferenceNumber(referenceNumber);
		             details.setInvVoid(EJBCommon.FALSE);
		             details.setInvDescription("SYSTEM GENERATED 2% INTEREST FOR "+ referenceNumber);
		             details.setInvBillToAddress("");
		             details.setInvBillToContact("");
		             details.setInvBillToAltContact("");
		             details.setInvBillToPhone("");
		             details.setInvBillingHeader("");
		             details.setInvBillingFooter("");
		             details.setInvBillingHeader2("");
		             details.setInvBillingFooter2("");
		             details.setInvBillingHeader3(null);
		             details.setInvBillingFooter3(null);
		             details.setInvBillingSignatory("");
		             details.setInvSignatoryTitle("");
		             details.setInvShipToAddress("");
		             details.setInvShipToContact("");
		             details.setInvShipToAltContact("");
		             details.setInvShipToPhone("");
		             details.setInvLvFreight("");
		             details.setInvShipDate(EJBCommon.convertStringToSQLDate(null));
		             details.setInvConversionDate(EJBCommon.convertStringToSQLDate(null));
		             details.setInvConversionRate(1);
		             details.setInvDebitMemo((byte)0);
		             details.setInvSubjectToCommission((byte)0);
		             details.setInvClientPO("");
		             details.setInvEffectivityDate(arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate());
		             details.setInvRecieveDate(EJBCommon.convertStringToSQLDate(""));
		             details.setInvCreatedBy(USER_NM);
		             details.setInvDateCreated(new java.util.Date());
		             details.setInvLastModifiedBy(USER_NM);
		             details.setInvDateLastModified(new java.util.Date());
		             details.setInvDisableInterest(EJBCommon.TRUE);
		             details.setInvInterest(EJBCommon.TRUE);
		             details.setInvInterestReferenceNumber(referenceNumber);
		             details.setInvInterestAmount(balance);
		             details.setInvInterestCreatedBy(USER_NM);
		             details.setInvInterestDateCreated(new java.util.Date());




		        //assign batch name , if not existed, create one

		        String batchName = new SimpleDateFormat("MMMM yyyy").format(arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate());
			    System.out.println("Date to format: " + batchName + " -----------------------------");
			    try{

			    	System.out.println("Batch name detected: " + batchName);
			    	LocalArInvoiceBatch arInvBatch = arInvoiceBatchHome.findByIbName(batchName, AD_BRNCH, AD_CMPNY);



			    }catch(FinderException fx){

			    	System.out.println("New Batch name created: " + batchName);
			    	arInvoiceBatchHome.create(batchName, batchName, "OPEN", "INVOICE", new Date(), USER_NM, AD_BRNCH, AD_CMPNY);

			    }






				 String INV_NUMBER = this.saveArInvEntry(details, "14 Days Net", "NONE", "NONE", "PHP", customerCode , batchName, ilList, false /*F if posted*/, null, AD_BRNCH, AD_CMPNY);






				 arInvoicePaymentSchedule.getArInvoice().setInvInterest(EJBCommon.FALSE);
				 arInvoicePaymentSchedule.getArInvoice().setInvInterestReferenceNumber(arInvoicePaymentSchedule.getArInvoice().getInvInterestReferenceNumber()+":"+INV_NUMBER);
				 arInvoicePaymentSchedule.getArInvoice().setInvInterestAmount(arInvoicePaymentSchedule.getArInvoice().getInvInterestAmount()+interestAmount);
				 arInvoicePaymentSchedule.getArInvoice().setInvInterestCreatedBy(USER_NM);
				 arInvoicePaymentSchedule.getArInvoice().setInvInterestDateCreated(new java.util.Date());
				 arInvoicePaymentSchedule.getArInvoice().setInvInterestLastRunDate(new java.util.Date());

				 Calendar calendar = new GregorianCalendar();
				//Add I month to InterestNextRunDate
		         calendar.setTime(arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate());
		         calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		         calendar.add(Calendar.DAY_OF_MONTH, 1);
		         System.out.println("OLD TIME IS : " + arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate().toString() + " -----------------------");

		         System.out.println("NEW TIME IS : " + calendar.getTime().toString() + " -----------------------");
				 arInvoicePaymentSchedule.getArInvoice().setInvInterestNextRunDate(calendar.getTime());
				 countTransaction++;
	       	 }


	       	 System.out.println("overDueInvoices.size()="+overDueInvoices.size());
	       	 return overDueInvoices.size();
	       }catch(GlJREffectiveDatePeriodClosedException gx){
	    	   throw new GlJREffectiveDatePeriodClosedException();


	       } catch (Exception ex) {

	       	 Debug.printStackTrace(ex);
	          throw new EJBException(ex.getMessage());

	       }

	    }

	    /**
	     * @ejb:interface-method view-type="remote"
	     **/
	     public void markInvoiceInterestAsDisable(int IPS_CODE, String INV_NMBR, String USER_NM, Integer AD_BRNCH, Integer AD_CMPNY)
	    throws GlobalNoRecordFoundException{
	    	 LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		       LocalArInvoiceBatchHome arInvoiceBatchHome = null;

		       // Initialize EJB Home

		       try {

		    	   arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
		               lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
		           arInvoiceBatchHome =  (LocalArInvoiceBatchHome)EJBHomeFactory.
		        	   lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);


		       } catch (NamingException ex) {

		           throw new EJBException(ex.getMessage());

		       }



		       try {


		    	   LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = arInvoicePaymentScheduleHome.findByPrimaryKey(IPS_CODE);


		    	   double balance = EJBCommon.roundIt((arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid()), (short)2) ;
		       		 double interestAmount = EJBCommon.roundIt((arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid()) * 0.02, (short)2) ;
		       		 String customerCode = arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCustomerCode();
		       		 String referenceNumber = arInvoicePaymentSchedule.getArInvoice().getInvNumber();



					 arInvoicePaymentSchedule.getArInvoice().setInvInterest(EJBCommon.FALSE);
					 arInvoicePaymentSchedule.getArInvoice().setInvInterestReferenceNumber(arInvoicePaymentSchedule.getArInvoice().getInvInterestReferenceNumber()+":"+INV_NMBR);
					 arInvoicePaymentSchedule.getArInvoice().setInvInterestAmount(arInvoicePaymentSchedule.getArInvoice().getInvInterestAmount()+interestAmount);
					 arInvoicePaymentSchedule.getArInvoice().setInvInterestCreatedBy(USER_NM);
					 arInvoicePaymentSchedule.getArInvoice().setInvInterestDateCreated(new java.util.Date());
					 arInvoicePaymentSchedule.getArInvoice().setInvInterestLastRunDate(new java.util.Date());

					 Calendar calendar = new GregorianCalendar();
					//Add I month to InterestNextRunDate
			         calendar.setTime(arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate());
			         calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
			         calendar.add(Calendar.DAY_OF_MONTH, 1);
			         System.out.println("OLD TIME IS : " + arInvoicePaymentSchedule.getArInvoice().getInvInterestNextRunDate().toString() + " -----------------------");

			         System.out.println("NEW TIME IS : " + calendar.getTime().toString() + " -----------------------");
					 arInvoicePaymentSchedule.getArInvoice().setInvInterestNextRunDate(calendar.getTime());

	    	 	} catch (Exception ex) {

		       	 Debug.printStackTrace(ex);
		          throw new EJBException(ex.getMessage());

		       }
	     }



/**
    * @ejb:interface-method view-type="remote"
    **/
    public int generateArPastDueInvoices(String USER_NM, Integer AD_BRNCH, Integer AD_CMPNY)
    		throws GlobalNoRecordFoundException{

       Debug.print("ArStandardMemoLineControllerBean generateArPastDueInvoices");

       LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
       LocalArInvoiceBatchHome arInvoiceBatchHome = null;
       LocalAdBranchCustomerHome adBranchCustomerHome = null;
       LocalArInvoiceHome arInvoiceHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       LocalAdPreferenceHome adPreferenceHome = null;
       LocalGlSetOfBookHome glSetOfBookHome = null;
       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
       LocalGlJournalHome glJournalHome = null;
       LocalGlJournalBatchHome glJournalBatchHome = null;
       LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
       LocalGlJournalLineHome glJournalLineHome = null;
       LocalGlJournalSourceHome glJournalSourceHome = null;
       LocalGlJournalCategoryHome glJournalCategoryHome = null;
       LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
       LocalArDistributionRecordHome arDistributionRecordHome = null;
       LocalInvCostingHome invCostingHome = null;
       LocalInvItemHome invItemHome = null;
       LocalGlChartOfAccountHome glChartOfAccountHome = null;
       LocalGlForexLedgerHome glForexLedgerHome = null;
       LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
       LocalInvItemLocationHome invItemLocationHome = null;
       // Initialize EJB Home

       try {

    	   arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
               lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
           arInvoiceBatchHome =  (LocalArInvoiceBatchHome)EJBHomeFactory.
        	   lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
           adBranchCustomerHome =  (LocalAdBranchCustomerHome)EJBHomeFactory.
            	   lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
           arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                   lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
                   adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                   lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
                   adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                   lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
                   glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
                   glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
                   glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
                   glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
                   glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
                   glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
                   glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
                   glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
                   glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
                   arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
                   lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
                   invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
                   lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
                   invItemHome = (LocalInvItemHome)EJBHomeFactory.
                   lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
                   glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
                   glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
                   glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
                   lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
                   invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
                   lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);


       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }



       try {


       	 Collection pastDueInvoices = arInvoicePaymentScheduleHome.findPastdueIpsByPenaltyDueDate(EJBCommon.convertStringToSQLDate(EJBCommon.convertSQLDateToString(new Date()) ), AD_BRNCH, AD_CMPNY);

       	 Iterator x = pastDueInvoices.iterator();

       	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

       	 while(x.hasNext()){

       		 LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)x.next();

       		double ipsAmountDue = arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid();
       		double monthlyPenaltyRate = EJBCommon.roundIt(ipsAmountDue * arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstMonthlyInterestRate() / 100, precisionUnit);
       		double ipsPenaltyDue = (arInvoicePaymentSchedule.getIpsPenaltyDue() + monthlyPenaltyRate);


       		int ctr = arInvoicePaymentSchedule.getIpsPenaltyCounter()+1;
       		GregorianCalendar gcDateDue = new GregorianCalendar();
       		gcDateDue.setTime(arInvoicePaymentSchedule.getIpsPenaltyDueDate());
       		gcDateDue.add(Calendar.MONTH, 1);

       		//Collection arInvoicePaymeSchedule = arInvoicePaymentScheduleHome.findByInvCodeAndAdCompny(INV_CODE, IPS_AD_CMPNY)

       		arInvoicePaymentSchedule.setIpsPenaltyCounter((short)ctr);
       		arInvoicePaymentSchedule.setIpsPenaltyDueDate(gcDateDue.getTime());
       		arInvoicePaymentSchedule.setIpsPenaltyDue(ipsPenaltyDue );
       		arInvoicePaymentSchedule.getArInvoice().setInvPenaltyDue(arInvoicePaymentSchedule.getArInvoice().getInvPenaltyDue() + monthlyPenaltyRate );

       		 // create distribution for unearned penalty
       		try{

        		LocalAdBranchCustomer adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);


                this.addArDrEntry(arInvoicePaymentSchedule.getArInvoice().getArDrNextLine(), "UNPENALTY",
                        EJBCommon.FALSE, monthlyPenaltyRate , adBranchCustomer.getBcstGlCoaUnEarnedPenaltyAccount(),
                        null, arInvoicePaymentSchedule.getArInvoice(), AD_BRNCH, AD_CMPNY);


                this.addArDrIliEntry(arInvoicePaymentSchedule.getArInvoice().getArDrNextLine(), "RECEIVABLE PENALTY",
                        EJBCommon.TRUE, monthlyPenaltyRate ,
                        adBranchCustomer.getBcstGlCoaReceivableAccount(),
                        arInvoicePaymentSchedule.getArInvoice(), AD_BRNCH, AD_CMPNY);



        	} catch (FinderException ex){

        	}



       	 LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

         LocalArInvoice arInvoice = arInvoicePaymentSchedule.getArInvoice();
         LocalArInvoice arCreditedInvoice = null;
         String USR_NM = arInvoice.getInvCreatedBy();
       		// post to gl if necessary

            if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

                // validate if date has no period and period is closed

                LocalGlSetOfBook glJournalSetOfBook = null;

                try {

                    glJournalSetOfBook = glSetOfBookHome.findByDate(arInvoice.getInvDate(), AD_CMPNY);

                } catch (FinderException ex) {

                    throw new GlJREffectiveDateNoPeriodExistException();

                }

                LocalGlAccountingCalendarValue glAccountingCalendarValue =
                    glAccountingCalendarValueHome.findByAcCodeAndDate(
                            glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), arInvoice.getInvDate(), AD_CMPNY);


                if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
                        glAccountingCalendarValue.getAcvStatus() == 'C' ||
                        glAccountingCalendarValue.getAcvStatus() == 'P') {

                    throw new GlJREffectiveDatePeriodClosedException();

                }

                // check if invoice is balance if not check suspense posting

                LocalGlJournalLine glOffsetJournalLine = null;

                Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);

                Iterator j = arDistributionRecords.iterator();

                double TOTAL_DEBIT = 0d;
                double TOTAL_CREDIT = 0d;

                while (j.hasNext()) {

                    LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

                    double DR_AMNT = 0d;

                    if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                        DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
                                arInvoice.getGlFunctionalCurrency().getFcName(),
                                arInvoice.getInvConversionDate(),
                                arInvoice.getInvConversionRate(),
                                arDistributionRecord.getDrAmount(), AD_CMPNY);

                    } else {

                        DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
                                arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
                                arCreditedInvoice.getInvConversionDate(),
                                arCreditedInvoice.getInvConversionRate(),
                                arDistributionRecord.getDrAmount(), AD_CMPNY);

                    }

                    if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

                        TOTAL_DEBIT += DR_AMNT;

                    } else {

                        TOTAL_CREDIT += DR_AMNT;

                    }

                }

                TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
                TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

                if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
                        TOTAL_DEBIT != TOTAL_CREDIT) {

                    LocalGlSuspenseAccount glSuspenseAccount = null;

                    try {

                        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES", arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS", AD_CMPNY);

                    } catch (FinderException ex) {

                        throw new GlobalJournalNotBalanceException();

                    }

                    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

                        glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1), EJBCommon.TRUE,
                                TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

                    } else {

                        glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1), EJBCommon.FALSE,
                                TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

                    }

                    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
                    //glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
                    glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);

                } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
                        TOTAL_DEBIT != TOTAL_CREDIT) {

                    throw new GlobalJournalNotBalanceException();

                }

                // create journal batch if necessary

                LocalGlJournalBatch glJournalBatch = null;
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

                try {

                    if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

                        glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arInvoice.getArInvoiceBatch().getIbName(), AD_BRNCH, AD_CMPNY);

                    } else {

                        glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", AD_BRNCH, AD_CMPNY);

                    }


                } catch (FinderException ex) {
                }

                if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
                        glJournalBatch == null) {

                    if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

                        glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arInvoice.getArInvoiceBatch().getIbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

                    } else {

                        glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

                    }


                }

                // create journal entry

                LocalGlJournal glJournal = glJournalHome.create("IPS Number-"+arInvoicePaymentSchedule.getIpsNumber(),
                        "AUTO GENERATE PENALTY", EJBCommon.convertStringToSQLDate(EJBCommon.convertSQLDateToString(new Date())) ,
                        0.0d, null, arInvoice.getInvNumber()+"-"+ctr, null, 1d, "N/A", null,
                        'N', EJBCommon.TRUE, EJBCommon.FALSE,
                        USR_NM, new Date(),
                        USR_NM, new Date(),
                        null, null,
                        USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
                        arInvoice.getArCustomer().getCstTin(),
                        arInvoice.getArCustomer().getCstName(), EJBCommon.FALSE,
                        null,
                        AD_BRNCH, AD_CMPNY);

                LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
                glJournal.setGlJournalSource(glJournalSource);

                LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
                glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

                LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName(arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS", AD_CMPNY);
                glJournal.setGlJournalCategory(glJournalCategory);

                if (glJournalBatch != null) {

                    glJournal.setGlJournalBatch(glJournalBatch);
                }


                // create journal lines

                j = arDistributionRecords.iterator();

                while (j.hasNext()) {

                    LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

                    double DR_AMNT = 0d;

                    if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                        DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
                                arInvoice.getGlFunctionalCurrency().getFcName(),
                                arInvoice.getInvConversionDate(),
                                arInvoice.getInvConversionRate(),
                                arDistributionRecord.getDrAmount(), AD_CMPNY);

                    } else {

                        DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
                                arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
                                arCreditedInvoice.getInvConversionDate(),
                                arCreditedInvoice.getInvConversionRate(),
                                arDistributionRecord.getDrAmount(), AD_CMPNY);

                    }

                    LocalGlJournalLine glJournalLine = glJournalLineHome.create(arDistributionRecord.getDrLine(),
                            arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

                    //arDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
                    glJournalLine.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());

                    //glJournal.addGlJournalLine(glJournalLine);
                    glJournalLine.setGlJournal(glJournal);
                    arDistributionRecord.setDrImported(EJBCommon.TRUE);

                    // for FOREX revaluation

                    LocalArInvoice arInvoiceTemp = arInvoice.getInvCreditMemo() == EJBCommon.FALSE ?
                            arInvoice: arCreditedInvoice;

                    if((arInvoiceTemp.getGlFunctionalCurrency().getFcCode() !=
                        adCompany.getGlFunctionalCurrency().getFcCode()) &&
                        glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
                        (glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
                                arInvoiceTemp.getGlFunctionalCurrency().getFcCode()))){

                        double CONVERSION_RATE = 1;

                        if (arInvoiceTemp.getInvConversionRate() != 0 && arInvoiceTemp.getInvConversionRate() != 1) {

                            CONVERSION_RATE = arInvoiceTemp.getInvConversionRate();

                        } else if (arInvoiceTemp.getInvConversionDate() != null){

                        	CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
                        			glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
                        			glJournal.getJrConversionDate(), AD_CMPNY);

                        }

                        Collection glForexLedgers = null;

                        try {

                            glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
                                    arInvoiceTemp.getInvDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

                        } catch(FinderException ex) {

                        }

                        LocalGlForexLedger glForexLedger =
                            (glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
                                (LocalGlForexLedger) glForexLedgers.iterator().next();

                        int FRL_LN = (glForexLedger != null &&
                                glForexLedger.getFrlDate().compareTo(arInvoiceTemp.getInvDate()) == 0) ?
                                        glForexLedger.getFrlLine().intValue() + 1 : 1;

                        // compute balance
                        double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
                        double FRL_AMNT = arDistributionRecord.getDrAmount();

                        if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
                            FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));
                        else
                            FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);

                        COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

                        glForexLedger = glForexLedgerHome.create(arInvoiceTemp.getInvDate(), new Integer (FRL_LN),
                                "SI", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

                        //glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
                        glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());

                        // propagate balances
                        try{

                            glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
                                    glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
                                    glForexLedger.getFrlAdCompany());

                        } catch (FinderException ex) {

                        }

                        Iterator itrFrl = glForexLedgers.iterator();

                        while (itrFrl.hasNext()) {

                            glForexLedger = (LocalGlForexLedger) itrFrl.next();
                            FRL_AMNT = arDistributionRecord.getDrAmount();

                            if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
                                FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT :
                                    (- 1 * FRL_AMNT));
                            else
                                FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) :
                                    FRL_AMNT);

                            glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

                        }

                    }

                }

                if (glOffsetJournalLine != null) {

                    //glJournal.addGlJournalLine(glOffsetJournalLine);
                    glOffsetJournalLine.setGlJournal(glJournal);

                }

                // post journal to gl

                Collection glJournalLines = glJournal.getGlJournalLines();

                Iterator i = glJournalLines.iterator();

                while (i.hasNext()) {

                    LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

                    // post current to current acv

                    this.postToGl(glAccountingCalendarValue,
                            glJournalLine.getGlChartOfAccount(),
                            true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


                    // post to subsequent acvs (propagate)

                    Collection glSubsequentAccountingCalendarValues =
                        glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                                glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                                glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

                    Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

                    while (acvsIter.hasNext()) {

                        LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                            (LocalGlAccountingCalendarValue)acvsIter.next();

                        this.postToGl(glSubsequentAccountingCalendarValue,
                                glJournalLine.getGlChartOfAccount(),
                                false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                    }

                    // post to subsequent years if necessary

                    Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

                    if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

                        adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
                        LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

                        Iterator sobIter = glSubsequentSetOfBooks.iterator();

                        while (sobIter.hasNext()) {

                            LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

                            String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

                            // post to subsequent acvs of subsequent set of book(propagate)

                            Collection glAccountingCalendarValues =
                                glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

                            Iterator acvIter = glAccountingCalendarValues.iterator();

                            while (acvIter.hasNext()) {

                                LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                                    (LocalGlAccountingCalendarValue)acvIter.next();

                                if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
                                        ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

                                    this.postToGl(glSubsequentAccountingCalendarValue,
                                            glJournalLine.getGlChartOfAccount(),
                                            false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                                } else { // revenue & expense

                                    this.postToGl(glSubsequentAccountingCalendarValue,
                                            glRetainedEarningsAccount,
                                            false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                                }

                            }

                            if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

                        }

                    }

                }

            }





       	 }


       	 System.out.println("pastDueInvoices.size()="+pastDueInvoices.size());
       	 return pastDueInvoices.size();
       }catch (Exception ex) {

       	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());

       }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArSmlAll(Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArStandardMemoLineControllerBean getArSmlAll");

        LocalArStandardMemoLineHome arStandardMemoLineHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalGlChartOfAccount glChartOfAccount = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

	        Collection arStandardMemoLines = arStandardMemoLineHome.findSmlAll(AD_CMPNY);

	        if (arStandardMemoLines.isEmpty()) {

	            throw new GlobalNoRecordFoundException();

	        }

	        Iterator i = arStandardMemoLines.iterator();

	        while (i.hasNext()) {

	        	LocalArStandardMemoLine arStandardMemoLine = (LocalArStandardMemoLine)i.next();

	        	ArModStandardMemoLineDetails mdetails = new ArModStandardMemoLineDetails();
	        		mdetails.setSmlCode(arStandardMemoLine.getSmlCode());
	        		mdetails.setSmlType(arStandardMemoLine.getSmlType());
	                mdetails.setSmlName(arStandardMemoLine.getSmlName());
	                mdetails.setSmlDescription(arStandardMemoLine.getSmlDescription());
	                mdetails.setSmlWordPressProductID(arStandardMemoLine.getSmlWordPressProductID());
	                mdetails.setSmlUnitPrice(arStandardMemoLine.getSmlUnitPrice());
	                mdetails.setSmlTax(arStandardMemoLine.getSmlTax());
	                mdetails.setSmlEnable(arStandardMemoLine.getSmlEnable());
	                mdetails.setSmlUnitOfMeasure(arStandardMemoLine.getSmlUnitOfMeasure());
	                mdetails.setSmlSubjectToCommission(arStandardMemoLine.getSmlSubjectToCommission());



	                if (arStandardMemoLine.getGlChartOfAccount() != null) {

	                	mdetails.setSmlGlCoaAccountNumber(arStandardMemoLine.getGlChartOfAccount().getCoaAccountNumber());
	                	mdetails.setSmlGlCoaAccountDescription(arStandardMemoLine.getGlChartOfAccount().getCoaAccountDescription());

	                }

	                if (arStandardMemoLine.getSmlGlCoaReceivableAccount()!= null) {
	                	try {

	    					glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arStandardMemoLine.getSmlGlCoaReceivableAccount());

	    				} catch (FinderException ex){

	    					throw new GlobalNoRecordFoundException();

	    				}
	                	mdetails.setSmlGlCoaReceivableAccountNumber(glChartOfAccount.getCoaAccountNumber());
	                	mdetails.setSmlGlCoaReceivableAccountDescription(glChartOfAccount.getCoaAccountDescription());

	                }

	                if (arStandardMemoLine.getSmlGlCoaRevenueAccount()!= null) {
	                	try {

	    					glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arStandardMemoLine.getSmlGlCoaRevenueAccount());

	    				} catch (FinderException ex){

	    					throw new GlobalNoRecordFoundException();

	    				}
	                	mdetails.setSmlGlCoaRevenueAccountNumber(glChartOfAccount.getCoaAccountNumber());
	                	mdetails.setSmlGlCoaRevenueAccountDescription(glChartOfAccount.getCoaAccountDescription());

	                }



	                if (arStandardMemoLine.getSmlInterimAccount() != null) {

	                    glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arStandardMemoLine.getSmlInterimAccount());

	                	mdetails.setSmlInterimAccountNumber(glChartOfAccount.getCoaAccountNumber());
					    mdetails.setSmlInterimAccountDescription(glChartOfAccount.getCoaAccountDescription());

                    }

	                list.add(mdetails);

	        }

	        return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrAll(Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException{

    	Debug.print("AdStandardMemoLineControllerBean getBrAll");

    	LocalAdBranchHome adBranchHome = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranches = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	adBranches = adBranchHome.findBrAll(AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adBranches.isEmpty()) {

            throw new GlobalNoRecordFoundException();

        }

        Iterator i = adBranches.iterator();

        while(i.hasNext()) {

        	adBranch = (LocalAdBranch)i.next();

        	AdBranchDetails details = new AdBranchDetails();

        	details.setBrBranchCode(adBranch.getBrBranchCode());
        	details.setBrCode(adBranch.getBrCode());
        	details.setBrName(adBranch.getBrName());

        	list.add(details);

        }

        return list;

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrSMLAll(Integer BSML_CODE, String RS_NM, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException{

    	Debug.print("ArStandardMemoLineControllerBean getAdBrSMLAll");

    	LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;

    	LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchStandardMemoLines = null;

        ArrayList branchList = new ArrayList();

        // Initialize EJB Home

        try {

        	adBranchStandardMemoLineHome = (LocalAdBranchStandardMemoLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	adBranchStandardMemoLines = adBranchStandardMemoLineHome.findBSMLBySMLCodeAndRsName(BSML_CODE, RS_NM, AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adBranchStandardMemoLines.isEmpty()) {

            throw new GlobalNoRecordFoundException();

        }

        try {

	        Iterator i = adBranchStandardMemoLines.iterator();

	        while(i.hasNext()) {

	        	adBranchStandardMemoLine = (LocalAdBranchStandardMemoLine)i.next();

	        	adBranch = adBranchHome.findByPrimaryKey(adBranchStandardMemoLine.getAdBranch().getBrCode());

	        	AdModBranchStandardMemoLineDetails mdetails = new AdModBranchStandardMemoLineDetails();
	        	mdetails.setBsmlBranchCode(adBranch.getBrCode());
	        	mdetails.setBsmlBranchName(adBranch.getBrName());

	        	LocalGlChartOfAccount glAccount = null;
		        LocalGlChartOfAccount glInterimChartOfAccount = null;
		        LocalGlChartOfAccount glReceivableChartOfAccount = null;
	       	 	LocalGlChartOfAccount glRevenueChartOfAccount = null;


	       	 	glAccount = glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlAccount());
	        	mdetails.setBsmlAccountNumber(glAccount.getCoaAccountNumber());
	        	mdetails.setBsmlAccountNumberDescription(glAccount.getCoaAccountDescription());

	        	glReceivableChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaReceivableAccount());
	        	mdetails.setBsmlReceivableAccountNumber(glReceivableChartOfAccount.getCoaAccountNumber());
	        	mdetails.setBsmlReceivableAccountNumberDescription(glReceivableChartOfAccount.getCoaAccountDescription());

	        	if(adBranchStandardMemoLine.getBsmlGlCoaRevenueAccount() != null) {

	        		glRevenueChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaRevenueAccount());

	        	}
	        	mdetails.setBsmlRevenueAccountNumber(glRevenueChartOfAccount != null ? glRevenueChartOfAccount.getCoaAccountNumber() : null);
	        	mdetails.setBsmlRevenueAccountNumberDescription(glRevenueChartOfAccount != null ? glRevenueChartOfAccount.getCoaAccountDescription() : null);



	        	mdetails.setBsmlSubjectToCommission(adBranchStandardMemoLine.getBsmlSubjectToCommission());

		    	branchList.add(mdetails);
	        }

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        return branchList;

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public AdResponsibilityDetails getAdRsByRsCode(Integer RS_CODE)
    	throws GlobalNoRecordFoundException {

    	LocalAdResponsibilityHome adResHome = null;
    	LocalAdResponsibility adRes = null;

    	try {
    		adResHome = (LocalAdResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
    	} catch (NamingException ex) {

    	}

    	try {
        	adRes = adResHome.findByPrimaryKey(RS_CODE);
    	} catch (FinderException ex) {

    	}

    	AdResponsibilityDetails details = new AdResponsibilityDetails();
    	details.setRsName(adRes.getRsName());

    	return details;
    }




    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void addArSmlEntry(com.util.ArModStandardMemoLineDetails mdetails,
        String SML_GL_COA_ACCNT_NMBR, String CST_GL_COA_RCVBL_ACCNT, String CST_GL_COA_RVNUE_ACCNT, String SML_INTRM_ACCNT_NMBR, ArrayList branchList, Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException,
               GlobalAccountNumberInvalidException {

        Debug.print("ArStandardMemoLineControllerBean addArSml");

        LocalArStandardMemoLineHome arStandardMemoLineHome = null;
        LocalGlChartOfAccountHome glCoaHome = null;
        LocalAdBranchHome adBranchHome = null;

    	LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;
    	LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;
    	LocalAdBranch adBranch = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
            glCoaHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBranchStandardMemoLineHome = (LocalAdBranchStandardMemoLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalArStandardMemoLine arStandardMemoLine = null;
	        LocalGlChartOfAccount glAccount = null;
	        LocalGlChartOfAccount glInterimChartOfAccount = null;
	        LocalGlChartOfAccount glReceivableChartOfAccount = null;
       	 	LocalGlChartOfAccount glRevenueChartOfAccount = null;

	        try {

	            arStandardMemoLine = arStandardMemoLineHome.findBySmlName(mdetails.getSmlName(), AD_CMPNY);

	            throw new GlobalRecordAlreadyExistException();

	        } catch (FinderException ex) {


	        }

	        // get account to validate accounts

	        try {

	            glAccount = glCoaHome.findByCoaAccountNumber(SML_GL_COA_ACCNT_NMBR, AD_CMPNY);

	        } catch (FinderException ex) {

	            if (this.getArSmlGlCoaAccountNumberEnable(AD_CMPNY)) {

	            	throw new GlobalAccountNumberInvalidException();

	            }
	        }

	        // get glChartOfAccount to validate accounts

	        try {

	        	if (SML_INTRM_ACCNT_NMBR != null && SML_INTRM_ACCNT_NMBR.length() > 0) {

		            glInterimChartOfAccount = glCoaHome.findByCoaAccountNumber(
		            		SML_INTRM_ACCNT_NMBR, AD_CMPNY);

                }

	        } catch (FinderException ex) {

	            throw new ArTCInterimAccountInvalidException();

	        }

	        try {

				glReceivableChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber( CST_GL_COA_RCVBL_ACCNT, AD_CMPNY);

			 } catch (FinderException ex) {

				throw new ArCCCoaGlReceivableAccountNotFoundException();

			 }


			 try {

				 if (CST_GL_COA_RVNUE_ACCNT != null && CST_GL_COA_RVNUE_ACCNT.length() > 0) {

					 glRevenueChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
							 CST_GL_COA_RVNUE_ACCNT, AD_CMPNY);


	                }


			 } catch (FinderException ex) {

				throw new ArCCCoaGlRevenueAccountNotFoundException();

			 }



	    	// create new standard memo line

	    	arStandardMemoLine = arStandardMemoLineHome.create(mdetails.getSmlType(),
	    	        mdetails.getSmlName(), mdetails.getSmlDescription(), mdetails.getSmlWordPressProductID(),
	    	        mdetails.getSmlUnitPrice(), mdetails.getSmlTax(), mdetails.getSmlEnable(),
	    	        mdetails.getSmlSubjectToCommission(), mdetails.getSmlUnitOfMeasure(), glInterimChartOfAccount != null ? glInterimChartOfAccount.getCoaCode() : null, glReceivableChartOfAccount.getCoaCode(), glRevenueChartOfAccount != null ? glRevenueChartOfAccount.getCoaCode() : null, AD_CMPNY);

            if (glAccount != null) {

	        	glAccount.addArStandardMemoLine(arStandardMemoLine);

	        }

            // create new Branch Standard Memo Line
        	LocalGlChartOfAccount glChartofAccount = null;
        	Iterator i = branchList.iterator();

        	while(i.hasNext()) {

        		AdModBranchStandardMemoLineDetails adBrSmlDetails = (AdModBranchStandardMemoLineDetails)i.next();

        		try {

            		glChartofAccount = glCoaHome.findByCoaAccountNumber( adBrSmlDetails.getBsmlAccountNumber(), AD_CMPNY);

            	} catch (FinderException ex) {

            		if (this.getArSmlGlCoaAccountNumberEnable(AD_CMPNY)) {
            			throw new GlobalAccountNumberInvalidException();
            		}
            	}


        		try {

					glReceivableChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
							adBrSmlDetails.getBsmlReceivableAccountNumber(), AD_CMPNY);

				} catch (FinderException ex) {

					throw new ArCCCoaGlReceivableAccountNotFoundException();

				}

				Integer glRevChrtOfAccntCode = null;

				if ( adBrSmlDetails.getBsmlRevenueAccountNumber()!= null &&
						adBrSmlDetails.getBsmlRevenueAccountNumber().length() > 0){

					try {

						glRevenueChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
								adBrSmlDetails.getBsmlRevenueAccountNumber(), AD_CMPNY);

						glRevChrtOfAccntCode = glRevenueChartOfAccount.getCoaCode();

					} catch (FinderException ex) {

						throw new ArCCCoaGlRevenueAccountNotFoundException();

					}

				}


        		adBranchStandardMemoLine = adBranchStandardMemoLineHome.create( glChartofAccount.getCoaCode(),glReceivableChartOfAccount.getCoaCode(), glRevChrtOfAccntCode, adBrSmlDetails.getBsmlSubjectToCommission(),'N', AD_CMPNY);
        		arStandardMemoLine.addAdBranchStandardMemoLine(adBranchStandardMemoLine);
        		adBranch = adBranchHome.findByPrimaryKey(adBrSmlDetails.getBsmlBranchCode());
        		adBranch.addAdBranchStandardMemoLine(adBranchStandardMemoLine);

        	}


        } catch (GlobalRecordAlreadyExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateArSmlEntry(com.util.ArModStandardMemoLineDetails mdetails, String SML_GL_COA_ACCNT_NMBR, String SML_GL_COA_RCVBL_ACCNT_NMBR, String SML_GL_COA_RVN_ACCNT_NMBR, String SML_INTRM_ACCNT_NMBR, String RS_NM, ArrayList branchList, Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException,
               GlobalAccountNumberInvalidException {

        Debug.print("ArStandardMemoLineControllerBean updateArSml");

        LocalArStandardMemoLineHome arStandardMemoLineHome = null;
        LocalGlChartOfAccountHome glCoaHome = null;
        LocalAdBranchHome adBranchHome = null;

    	LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;
    	LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;
    	LocalAdBranch adBranch = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
            glCoaHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBranchStandardMemoLineHome = (LocalAdBranchStandardMemoLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalArStandardMemoLine arStandardMemoLine = null;
	        LocalGlChartOfAccount glAccount = null;
	        LocalGlChartOfAccount glInterimChartOfAccount = null;
	        LocalGlChartOfAccount glReceivableChartOfAccount = null;
       	 	LocalGlChartOfAccount glRevenueChartOfAccount = null;

        	try {

	            LocalArStandardMemoLine arExistingStandardMemoLine = arStandardMemoLineHome.findBySmlName(mdetails.getSmlName(), AD_CMPNY);

	            if (!arExistingStandardMemoLine.getSmlCode().equals(mdetails.getSmlCode())) {

	                 throw new GlobalRecordAlreadyExistException();

	            }

	        } catch (FinderException ex) {

	        }

	        // get account to validate accounts

	        try {

	            glAccount = glCoaHome.findByCoaAccountNumber(SML_GL_COA_ACCNT_NMBR, AD_CMPNY);

	        } catch (FinderException ex) {

	            if (this.getArSmlGlCoaAccountNumberEnable(AD_CMPNY)) {

	            	throw new GlobalAccountNumberInvalidException();

	            }
	        }



	        // get glChartOfAccount to validate accounts

	        try {

	        	if (SML_INTRM_ACCNT_NMBR != null && SML_INTRM_ACCNT_NMBR.length() > 0) {

		            glInterimChartOfAccount = glCoaHome.findByCoaAccountNumber(
		            		SML_INTRM_ACCNT_NMBR, AD_CMPNY);

                }

	        } catch (FinderException ex) {

	            throw new ArTCInterimAccountInvalidException();

	        }



	        // get glChartOfAccount to validate accounts

	        try {

	        	if (SML_GL_COA_RCVBL_ACCNT_NMBR != null && SML_GL_COA_RCVBL_ACCNT_NMBR.length() > 0) {

		            glReceivableChartOfAccount = glCoaHome.findByCoaAccountNumber(
		            		SML_GL_COA_RCVBL_ACCNT_NMBR, AD_CMPNY);

                }

	        } catch (FinderException ex) {

	            throw new ArCCCoaGlReceivableAccountNotFoundException();

	        }


	        // get glChartOfAccount to validate accounts

	        try {

	        	if (SML_GL_COA_RVN_ACCNT_NMBR != null && SML_GL_COA_RVN_ACCNT_NMBR.length() > 0) {

		            glRevenueChartOfAccount = glCoaHome.findByCoaAccountNumber(
		            		SML_GL_COA_RVN_ACCNT_NMBR, AD_CMPNY);

                }

	        } catch (FinderException ex) {

	            throw new ArCCCoaGlRevenueAccountNotFoundException();

	        }


			// find and update standard memo line

			arStandardMemoLine = arStandardMemoLineHome.findByPrimaryKey(mdetails.getSmlCode());
                System.out.println("--c-- " + mdetails.getSmlWordPressProductID());
                arStandardMemoLine.setSmlType(mdetails.getSmlType());
				arStandardMemoLine.setSmlName(mdetails.getSmlName());
				arStandardMemoLine.setSmlDescription(mdetails.getSmlDescription());
				arStandardMemoLine.setSmlWordPressProductID(mdetails.getSmlWordPressProductID());
				arStandardMemoLine.setSmlUnitPrice(mdetails.getSmlUnitPrice());
				arStandardMemoLine.setSmlTax(mdetails.getSmlTax());
				arStandardMemoLine.setSmlEnable(mdetails.getSmlEnable());
				arStandardMemoLine.setSmlUnitOfMeasure(mdetails.getSmlUnitOfMeasure());
				arStandardMemoLine.setSmlSubjectToCommission(mdetails.getSmlSubjectToCommission());
				arStandardMemoLine.setSmlInterimAccount(glInterimChartOfAccount != null ? glInterimChartOfAccount.getCoaCode() : null);

				arStandardMemoLine.setSmlGlCoaReceivableAccount(glReceivableChartOfAccount != null ? glReceivableChartOfAccount.getCoaCode() : null);
				arStandardMemoLine.setSmlGlCoaRevenueAccount(glRevenueChartOfAccount != null ? glRevenueChartOfAccount.getCoaCode() : null);


				System.out.println("--c-- 2" + arStandardMemoLine.getSmlWordPressProductID());




            if (glAccount != null) {

	        	glAccount.addArStandardMemoLine(arStandardMemoLine);

	        }

            Collection adBranchStandardMemoLines = adBranchStandardMemoLineHome.findBSMLBySMLCodeAndRsName(arStandardMemoLine.getSmlCode(), RS_NM, AD_CMPNY);

            Iterator j = adBranchStandardMemoLines.iterator();

            // remove all adBranchStandardMemoLine lines
            while(j.hasNext()) {

            	adBranchStandardMemoLine = (LocalAdBranchStandardMemoLine)j.next();

            	arStandardMemoLine.dropAdBranchStandardMemoLine(adBranchStandardMemoLine);

            	adBranch = adBranchHome.findByPrimaryKey(adBranchStandardMemoLine.getAdBranch().getBrCode());
            	adBranch.dropAdBranchStandardMemoLine(adBranchStandardMemoLine);
            	adBranchStandardMemoLine.remove();
            }

            // create new Branch Standard Memo Line
        	LocalGlChartOfAccount glCoa = null;
        	Iterator i = branchList.iterator();

        	while(i.hasNext()) {

        		AdModBranchStandardMemoLineDetails adBrSmlDetails = (AdModBranchStandardMemoLineDetails)i.next();

        		try {
            		glCoa = glCoaHome.findByCoaAccountNumber( adBrSmlDetails.getBsmlAccountNumber(), AD_CMPNY);
            	} catch (FinderException ex) {
            		if (this.getArSmlGlCoaAccountNumberEnable(AD_CMPNY)) {
            			throw new GlobalAccountNumberInvalidException();
            		}
            	}


        		try {

        			System.out.println("adBrSmlDetails.getBsmlReceivableAccountNumber()="+adBrSmlDetails.getBsmlReceivableAccountNumber());

					glReceivableChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
							adBrSmlDetails.getBsmlReceivableAccountNumber(), AD_CMPNY);

				} catch (FinderException ex) {

					throw new ArCCCoaGlReceivableAccountNotFoundException();

				}



				try {
					System.out.println("adBrSmlDetails.getBsmlRevenueAccountNumber()="+adBrSmlDetails.getBsmlRevenueAccountNumber());

					glRevenueChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
							adBrSmlDetails.getBsmlRevenueAccountNumber(), AD_CMPNY);

				} catch (FinderException ex) {

					throw new ArCCCoaGlRevenueAccountNotFoundException();

				}


        		adBranchStandardMemoLine = adBranchStandardMemoLineHome.create( glCoa.getCoaCode(),glReceivableChartOfAccount.getCoaCode(), glRevenueChartOfAccount.getCoaCode(), adBrSmlDetails.getBsmlSubjectToCommission(),adBrSmlDetails.getBsmlStandardMemoLineDownloadStatus(), AD_CMPNY);

        		arStandardMemoLine.addAdBranchStandardMemoLine(adBranchStandardMemoLine);
        		adBranch = adBranchHome.findByPrimaryKey(adBrSmlDetails.getBsmlBranchCode());
        		adBranch.addAdBranchStandardMemoLine(adBranchStandardMemoLine);

        	}

        } catch (GlobalRecordAlreadyExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteArSmlEntry(Integer SML_CODE, Integer AD_CMPNY)
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("ArStandardMemoLineControllerBean deleteArSml");

        LocalArStandardMemoLineHome arStandardMemoLineHome = null;

        // Initialize EJB Home

        try {

            arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

      	    LocalArStandardMemoLine arStandardMemoLine = null;

	        try {

	            arStandardMemoLine = arStandardMemoLineHome.findByPrimaryKey(SML_CODE);

	        } catch (FinderException ex) {

	            throw new GlobalRecordAlreadyDeletedException();

	        }

	        if (!arStandardMemoLine.getArInvoiceLines().isEmpty()) {

	            throw new GlobalRecordAlreadyAssignedException();

	        }

		    arStandardMemoLine.remove();

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyAssignedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("ArStandardMemoLineControllerBean getGlFcPrecisionUnit");


       LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         return  adCompany.getGlFunctionalCurrency().getFcPrecision();

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getArSmlGlCoaAccountNumberEnable(Integer AD_CMPNY) {

        Debug.print("ArCustomerClassControllerBean getArSmlGlCoaAccountNumberEnable");

        LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;

        // Initialize EJB Home

        try {

            arAutoAccountingSegmentHome = (LocalArAutoAccountingSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection arAutoAccountingSegments =
        	   arAutoAccountingSegmentHome.findByAasClassTypeAndAaAccountType("AR STANDARD MEMO LINE", "REVENUE", AD_CMPNY);

            if (!arAutoAccountingSegments.isEmpty()) {

            	return true;

            } else {

            	return false;

            }


        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean getInvGpQuantityPrecisionUnit");

        LocalAdPreferenceHome adPreferenceHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfInvQuantityPrecisionUnit();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
	throws GlobalConversionDateNotExistException {

		Debug.print("ArInvoiceEntryControllerBean getFrRateByFrNameAndFrDate");

		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

			double CONVERSION_RATE = 1;

			// Get functional currency rate

			if (!FC_NM.equals("USD")) {

				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
							CONVERSION_DATE, AD_CMPNY);

				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

			}

			// Get set of book functional currency rate if necessary

			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
							CONVERSION_DATE, AD_CMPNY);

				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

			}

			return CONVERSION_RATE;

		} catch (FinderException ex) {

			getSessionContext().setRollbackOnly();
			throw new GlobalConversionDateNotExistException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public String saveArInvEntry(com.util.ArInvoiceDetails details, String PYT_NM, String TC_NM,
            String WTC_NM, String FC_NM, String CST_CSTMR_CODE, String IB_NM, ArrayList ilList, boolean isDraft, String SLP_SLSPRSN_CODE, Integer AD_BRNCH, Integer AD_CMPNY) throws
            GlobalRecordAlreadyDeletedException,
            GlobalDocumentNumberNotUniqueException,
            GlobalConversionDateNotExistException,
            GlobalPaymentTermInvalidException,
            ArINVAmountExceedsCreditLimitException,
            GlobalTransactionAlreadyApprovedException,
            GlobalTransactionAlreadyPendingException,
            GlobalTransactionAlreadyPostedException,
            GlobalTransactionAlreadyVoidException,
            GlobalNoApprovalRequesterFoundException,
            GlobalNoApprovalApproverFoundException,
            GlJREffectiveDateNoPeriodExistException,
            GlJREffectiveDatePeriodClosedException,
            GlobalJournalNotBalanceException,
            GlobalBranchAccountNumberInvalidException {

        Debug.print("ArStandardMemoLineControllerBean saveArInvEntry");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdPaymentTermHome adPaymentTermHome = null;
        LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
        LocalAdDiscountHome adDiscountHome = null;
        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalArCustomerBalanceHome arCustomerBalanceHome = null;
        LocalArReceiptHome arReceiptHome = null;
        LocalAdBranchCustomerHome adBranchCustomerHome = null;
        LocalArSalespersonHome arSalespersonHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;

        LocalArInvoice arInvoice = null;


        // Initialize EJB Home

        try {


            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
            lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
            lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
            adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
            adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
            lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
            lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
            lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
            lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            adBranchCustomerHome = (LocalAdBranchCustomerHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
            arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
            lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            // validate if invoice is already deleted

            try {

                if (details.getInvCode() != null) {

                    arInvoice = arInvoiceHome.findByPrimaryKey(details.getInvCode());

                }

            } catch (FinderException ex) {

                throw new GlobalRecordAlreadyDeletedException();

            }

            // validate if invoice is already posted, void, approved or pending

            if (details.getInvCode() != null) {

                if (arInvoice.getInvApprovalStatus() != null) {

                    if (arInvoice.getInvApprovalStatus().equals("APPROVED") ||
                            arInvoice.getInvApprovalStatus().equals("N/A")) {

                        throw new GlobalTransactionAlreadyApprovedException();


                    } else if (arInvoice.getInvApprovalStatus().equals("PENDING")) {

                        throw new GlobalTransactionAlreadyPendingException();

                    }

                }

                if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

                    throw new GlobalTransactionAlreadyPostedException();

                } else if (arInvoice.getInvVoid() == EJBCommon.TRUE) {

                    throw new GlobalTransactionAlreadyVoidException();

                }

            }

            // invoice void

            if (details.getInvCode() != null && details.getInvVoid() == EJBCommon.TRUE &&
                    arInvoice.getInvPosted() == EJBCommon.FALSE) {

                arInvoice.setInvVoid(EJBCommon.TRUE);
                arInvoice.setInvLastModifiedBy(details.getInvLastModifiedBy());
                arInvoice.setInvDateLastModified(details.getInvDateLastModified());

                return arInvoice.getInvNumber();

            }

            // validate if document number is unique document number is automatic then set next sequence

            LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
            LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
            Debug.print("ArInvoiceEntryControllerBean saveArInvEntry A");
            if (details.getInvCode() == null) {

                try {

                    adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR INVOICE", AD_CMPNY);

                } catch (FinderException ex) {

                }

                try {
                    System.out.println("adDocumentSequenceAssignment.getDsaCode()= "+ adDocumentSequenceAssignment.getDsaCode());
                    adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

                } catch (FinderException ex) {

                }


                System.out.println("trace 1");
                if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
                        (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {

                    while (true) {
                    	 System.out.println("trace  loop 1");
                        if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

                            try {
                            	 System.out.println("trace  loop 1 a");
                                arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

                            } catch (FinderException ex) {
                            	 System.out.println("trace  loop 1 b");
                                details.setInvNumber(adDocumentSequenceAssignment.getDsaNextSequence());
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
                                break;

                            }

                        } else {

                            try {
                            	 System.out.println("trace  loop 1 c");
                                arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

                            } catch (FinderException ex) {
                            	 System.out.println("trace  loop 1 d");
                                details.setInvNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
                                break;

                            }

                        }

                    }

                }

            } else {
            	 System.out.println("trace  loop 2");
                LocalArInvoice arExistingInvoice = null;

                try {

                    arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
                            details.getInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

                } catch (FinderException ex) {
                }

                if (arExistingInvoice != null &&
                        !arExistingInvoice.getInvCode().equals(details.getInvCode())) {

                    throw new GlobalDocumentNumberNotUniqueException();

                }

                if (arInvoice.getInvNumber() != details.getInvNumber() &&
                        (details.getInvNumber() == null || details.getInvNumber().trim().length() == 0)) {

                    details.setInvNumber(arInvoice.getInvNumber());

                }

            }
            Debug.print("ArInvoiceEntryControllerBean saveArInvEntry A Done");

            // validate if conversion date exists

            try {

                if (details.getInvConversionDate() != null) {

                    LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
                    LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

                    if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

                    	LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
                    		glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
                    				details.getInvConversionDate(), AD_CMPNY);

                    } else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

                    	LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
                    		glFunctionalCurrencyRateHome.findByFcCodeAndDate(
                    				adCompany.getGlFunctionalCurrency().getFcCode(), details.getInvConversionDate(), AD_CMPNY);

                    }

                }

            } catch (FinderException ex) {

                throw new GlobalConversionDateNotExistException();

            }

            // validate if payment term has at least one payment schedule

            if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

                throw new GlobalPaymentTermInvalidException();

            }





            // used in checking if invoice should re-generate distribution records and re-calculate taxes
            boolean isRecalculate = true;

            // create invoice
            Debug.print("ArInvoiceEntryControllerBean saveArInvEntry B");
            if (details.getInvCode() == null) {
            	System.out.println("CHECKING A");

            	arInvoice = arInvoiceHome.create(details.getInvType(), EJBCommon.FALSE,
            			details.getInvDescription(), details.getInvDate(),
            			details.getInvNumber(), details.getInvReferenceNumber(),details.getInvUploadNumber(), null, null,
            			0d,0d, 0d,0d,0d,0d, details.getInvConversionDate(), details.getInvConversionRate(),
            			details.getInvMemo(), details.getInvPreviousReading(), details.getInvPresentReading(),
            			details.getInvBillToAddress(), details.getInvBillToContact(), details.getInvBillToAltContact(),
            			details.getInvBillToPhone(), details.getInvBillingHeader(), details.getInvBillingFooter(),
            			details.getInvBillingHeader2(), details.getInvBillingFooter2(), details.getInvBillingHeader3(),
            			details.getInvBillingFooter3(), details.getInvBillingSignatory(), details.getInvSignatoryTitle(),
            			details.getInvShipToAddress(), details.getInvShipToContact(), details.getInvShipToAltContact(),
            			details.getInvShipToPhone(), details.getInvShipDate(), details.getInvLvFreight(),
            			null, null,
            			EJBCommon.FALSE,
            			null, EJBCommon.FALSE, EJBCommon.FALSE,
            			EJBCommon.FALSE, details.getInvDisableInterest(),
            			details.getInvInterest(), details.getInvInterestReferenceNumber(), details.getInvInterestAmount(), details.getInvInterestCreatedBy(), details.getInvInterestDateCreated(), null, null,
            			details.getInvCreatedBy(), details.getInvDateCreated(),
            			details.getInvLastModifiedBy(), details.getInvDateLastModified(),
            			null, null, null, null, EJBCommon.FALSE, null, null, null, details.getInvDebitMemo(),
            			details.getInvSubjectToCommission(), null, details.getInvEffectivityDate(), AD_BRNCH, AD_CMPNY);

            } else {

                // check if critical fields are changed

                if (!arInvoice.getArTaxCode().getTcName().equals(TC_NM) ||
                        !arInvoice.getArWithholdingTaxCode().getWtcName().equals(WTC_NM) ||
                        !arInvoice.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE) ||
                        !arInvoice.getAdPaymentTerm().getPytName().equals(PYT_NM) ||
                        !arInvoice.getInvDate().equals(details.getInvDate()) ||
                        !arInvoice.getInvEffectivityDate().equals(details.getInvEffectivityDate()) ||
                        ilList.size() != arInvoice.getArInvoiceLines().size()) {

                    isRecalculate = true;

                } else if (ilList.size() == arInvoice.getArInvoiceLines().size()) {
                    System.out.println("CHECK");
                    Iterator ilIter = arInvoice.getArInvoiceLines().iterator();
                    Iterator ilListIter = ilList.iterator();

                    while (ilIter.hasNext()) {

                        LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)ilIter.next();
                        ArModInvoiceLineDetails mdetails = (ArModInvoiceLineDetails)ilListIter.next();
                        if(!arInvoiceLine.getArStandardMemoLine().getSmlName().equals(mdetails.getIlSmlName())){
                        	System.out.println("SML");
                        }

                        if(arInvoiceLine.getIlQuantity() != mdetails.getIlQuantity()){
                        	System.out.println("QTY");
                        }

                        if(arInvoiceLine.getIlUnitPrice() != mdetails.getIlUnitPrice()){
                        	System.out.println("UPRICE");
                        }

                        if(arInvoiceLine.getIlTax() != mdetails.getIlTax()){
                        	System.out.println("TAX");
                        }

                        if (!arInvoiceLine.getArStandardMemoLine().getSmlName().equals(mdetails.getIlSmlName()) ||
                                arInvoiceLine.getIlQuantity() != mdetails.getIlQuantity() ||
                                arInvoiceLine.getIlUnitPrice() != mdetails.getIlUnitPrice() ||
                                arInvoiceLine.getIlTax() != mdetails.getIlTax()) {
                            System.out.println("NO!");
                            isRecalculate = true;
                            break;

                        }

                        arInvoiceLine.setIlDescription(mdetails.getIlDescription());

                        isRecalculate = false;

                    }

                } else {

                    isRecalculate = false;

                }
                Debug.print("ArInvoiceEntryControllerBean saveArInvEntry B Done");

                arInvoice.setInvDescription(details.getInvDescription());
                arInvoice.setInvDate(details.getInvDate());
                arInvoice.setInvNumber(details.getInvNumber());
                arInvoice.setInvReferenceNumber(details.getInvReferenceNumber());
                arInvoice.setInvMemo(details.getInvMemo());
                arInvoice.setInvConversionDate(details.getInvConversionDate());
                arInvoice.setInvConversionRate(details.getInvConversionRate());
                arInvoice.setInvPreviousReading(arInvoice.getInvPreviousReading());
                arInvoice.setInvPresentReading(arInvoice.getInvPresentReading());
                arInvoice.setInvBillToAddress(details.getInvBillToAddress());
                arInvoice.setInvBillToContact(details.getInvBillToContact());
                arInvoice.setInvBillToAltContact(details.getInvBillToAltContact());
                arInvoice.setInvBillToPhone(details.getInvBillToPhone());
                arInvoice.setInvBillingHeader(details.getInvBillingHeader());
                arInvoice.setInvBillingFooter(details.getInvBillingFooter());
                arInvoice.setInvBillingHeader2(details.getInvBillingHeader2());
                arInvoice.setInvBillingFooter2(details.getInvBillingFooter2());
                arInvoice.setInvBillingHeader3(details.getInvBillingHeader3());
                arInvoice.setInvBillingFooter3(details.getInvBillingFooter3());
                arInvoice.setInvBillingSignatory(details.getInvBillingSignatory());
                arInvoice.setInvSignatoryTitle(details.getInvSignatoryTitle());
                arInvoice.setInvShipToAddress(details.getInvShipToAddress());
                arInvoice.setInvShipToContact(details.getInvShipToContact());
                arInvoice.setInvShipToAltContact(details.getInvShipToAltContact());
                arInvoice.setInvShipToPhone(details.getInvShipToPhone());
                arInvoice.setInvShipDate(details.getInvShipDate());
                arInvoice.setInvLvFreight(details.getInvLvFreight());
                arInvoice.setInvLastModifiedBy(details.getInvLastModifiedBy());
                arInvoice.setInvDateLastModified(details.getInvDateLastModified());
                arInvoice.setInvReasonForRejection(null);
                arInvoice.setInvDebitMemo(details.getInvDebitMemo());
                arInvoice.setInvSubjectToCommission(details.getInvSubjectToCommission());
                arInvoice.setInvEffectivityDate(details.getInvEffectivityDate());
                arInvoice.setInvRecieveDate(details.getInvRecieveDate());

            }

            Debug.print("ArInvoiceEntryControllerBean saveArInvEntry C");

            LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
            //adPaymentTerm.addArInvoice(arInvoice);
            arInvoice.setAdPaymentTerm(adPaymentTerm);

            LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
            //glFunctionalCurrency.addArInvoice(arInvoice);
            arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

            LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
            //arTaxCode.addArInvoice(arInvoice);
            arInvoice.setArTaxCode(arTaxCode);

            System.out.println("WTC_NM: " + WTC_NM);
            LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
            //arWithholdingTaxCode.addArInvoice(arInvoice);
            arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);

            System.out.println("CST_CSTMR_CODE: " + CST_CSTMR_CODE);
            LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
            //arCustomer.addArInvoice(arInvoice);
            arInvoice.setArCustomer(arCustomer);

            LocalArSalesperson arSalesperson = null;

            if (SLP_SLSPRSN_CODE != null && SLP_SLSPRSN_CODE.length() > 0 && !SLP_SLSPRSN_CODE.equalsIgnoreCase("NO RECORD FOUND")) {

                // if he tagged a salesperson for this invoice
                arSalesperson = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE, AD_CMPNY);

                //arSalesperson.addArInvoice(arInvoice);
                arInvoice.setArSalesperson(arSalesperson);


            } else {

                // if he untagged a salesperson for this invoice
                if (arInvoice.getArSalesperson() != null) {

                    arSalesperson = arSalespersonHome.findBySlpSalespersonCode(arInvoice.getArSalesperson().getSlpSalespersonCode(), AD_CMPNY);
                    arSalesperson.dropArInvoice(arInvoice);

                }

                // if no salesperson is set, invoice should not be subject to commission
                arInvoice.setInvSubjectToCommission((byte)0);

            }

            try {

                LocalArInvoiceBatch arInvoiceBatch = arInvoiceBatchHome.findByIbName(IB_NM, AD_BRNCH, AD_CMPNY);
                //arInvoiceBatch.addArInvoice(arInvoice);
                arInvoice.setArInvoiceBatch(arInvoiceBatch);

            } catch (FinderException ex) {

            }
            Debug.print("ArInvoiceEntryControllerBean saveArInvEntry C Done");
            double amountDue = 0;

            Debug.print("ArInvoiceEntryControllerBean saveArInvEntry D");
            if (isRecalculate) {

                // remove all invoice line items

                Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

                Iterator i = arInvoiceLineItems.iterator();

                while (i.hasNext()) {

                    LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();

                    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(), arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
                    arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

                    i.remove();

                    arInvoiceLineItem.remove();

                }

                // remove all invoice lines

                Collection arInvoiceLines = arInvoice.getArInvoiceLines();

                i = arInvoiceLines.iterator();

                while (i.hasNext()) {

                    LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)i.next();

                    i.remove();

                    arInvoiceLine.remove();

                }

                // remove all sales order lines

                Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

                i = arSalesOrderInvoiceLines.iterator();

                while (i.hasNext()) {

                    LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)i.next();

                    i.remove();

                    arSalesOrderInvoiceLine.remove();

                }


                // remove all distribution records

                Collection arDistributionRecords = arInvoice.getArDistributionRecords();

                i = arDistributionRecords.iterator();

                while (i.hasNext()) {

                    LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

                    i.remove();

                    arDistributionRecord.remove();

                }

                LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

                // add new invoice lines and distribution record

                double TOTAL_TAX = 0d;
                double TOTAL_LINE = 0d;
                double TOTAL_UNTAXABLE = 0d;

                i = ilList.iterator();

                while (i.hasNext()) {

                    ArModInvoiceLineDetails mInvDetails = (ArModInvoiceLineDetails) i.next();

                    LocalArInvoiceLine arInvoiceLine = this.addArIlEntry(mInvDetails, arInvoice, AD_CMPNY);


                    // add receivable/debit distributions
                    double LINE = arInvoiceLine.getIlAmount();
                    double TAX = arInvoiceLine.getIlTaxAmount();
                    double UNTAXABLE = 0d;
                    double W_TAX = 0d;
                    double DISCOUNT =0d;



                    if(arInvoiceLine.getIlTax() == EJBCommon.FALSE)
                     	UNTAXABLE += arInvoiceLine.getIlAmount();


                    // compute w-tax per line


                    if (arWithholdingTaxCode.getWtcRate() != 0 && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

                    	W_TAX = EJBCommon.roundIt((LINE - UNTAXABLE) * (arWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

                    }

                    if (adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {

                        Collection adPaymentSchedules = adPaymentScheduleHome.findByPytCode(adPaymentTerm.getPytCode(),AD_CMPNY);
                        ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
                        LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)adPaymentScheduleList.get(0);

                        Collection adDiscounts = adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
                        ArrayList adDiscountList = new ArrayList(adDiscounts);
                        LocalAdDiscount adDiscount = (LocalAdDiscount)adDiscountList.get(0);


                        double rate = adDiscount.getDscDiscountPercent();
                        DISCOUNT = (LINE + TAX) * (rate / 100d);
                    }


                    // add revenue/credit distributions

                    // Check If Memo Line Type is Service Charge

                    if(arInvoiceLine.getArStandardMemoLine().getSmlType().equals("SC")){

                    	if(arInvoiceLine.getArStandardMemoLine().getSmlInterimAccount()==null)
                    		throw new GlobalBranchAccountNumberInvalidException();

                    	this.addArDrEntry(arInvoice.getArDrNextLine(),
                    			"SC", EJBCommon.FALSE, arInvoiceLine.getIlAmount(),
                    			arInvoiceLine.getArStandardMemoLine().getSmlInterimAccount(), this.getArGlCoaRevenueAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), arInvoice, AD_BRNCH, AD_CMPNY);
                    }else{

                    	this.addArDrEntry(arInvoice.getArDrNextLine(),
                    			"REVENUE", EJBCommon.FALSE, arInvoiceLine.getIlAmount(),
                    			this.getArGlCoaRevenueAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), null, arInvoice, AD_BRNCH, AD_CMPNY);

                    	this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE",
                                EJBCommon.TRUE, LINE + TAX - W_TAX - DISCOUNT,
                                this.getArGlCoaReceivableAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY),
                                null, arInvoice, AD_BRNCH, AD_CMPNY);


                    }


                    TOTAL_LINE += arInvoiceLine.getIlAmount();
                    TOTAL_TAX += arInvoiceLine.getIlTaxAmount();

                    if(arInvoiceLine.getIlTax() == EJBCommon.FALSE)
                    	TOTAL_UNTAXABLE += arInvoiceLine.getIlAmount();

                }


                // add tax distribution if necessary

                if (!arTaxCode.getTcType().equals("NONE") &&
                        !arTaxCode.getTcType().equals("EXEMPT")) {

                    if (arTaxCode.getTcInterimAccount() == null) {

                        this.addArDrEntry(arInvoice.getArDrNextLine(),
                                "TAX", EJBCommon.FALSE, TOTAL_TAX, arTaxCode.getGlChartOfAccount().getCoaCode(),
                                null, arInvoice, AD_BRNCH, AD_CMPNY);

                    } else {

                        this.addArDrEntry(arInvoice.getArDrNextLine(),
                                "DEFERRED TAX", EJBCommon.FALSE, TOTAL_TAX, arTaxCode.getTcInterimAccount(),
                                null, arInvoice, AD_BRNCH, AD_CMPNY);

                    }

                }

                // add wtax distribution if necessary

                adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

                double W_TAX_AMOUNT = 0d;

                if (arWithholdingTaxCode.getWtcRate() != 0 && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

                    W_TAX_AMOUNT = EJBCommon.roundIt((TOTAL_LINE - TOTAL_UNTAXABLE) * (arWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

                    this.addArDrEntry(arInvoice.getArDrNextLine(), "W-TAX",
                            EJBCommon.TRUE, W_TAX_AMOUNT, arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(),
                            null, arInvoice, AD_BRNCH, AD_CMPNY);

                }

                // add payment discount if necessary

                double DISCOUNT_AMT = 0d;

                if (adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {

                    Collection adPaymentSchedules = adPaymentScheduleHome.findByPytCode(adPaymentTerm.getPytCode(),AD_CMPNY);
                    ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
                    LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)adPaymentScheduleList.get(0);

                    Collection adDiscounts = adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
                    ArrayList adDiscountList = new ArrayList(adDiscounts);
                    LocalAdDiscount adDiscount = (LocalAdDiscount)adDiscountList.get(0);


                    double rate = adDiscount.getDscDiscountPercent();
                    DISCOUNT_AMT = (TOTAL_LINE + TOTAL_TAX) * (rate / 100d);

                    this.addArDrIliEntry(arInvoice.getArDrNextLine(), "DISCOUNT",
                            EJBCommon.TRUE, DISCOUNT_AMT,
                            adPaymentTerm.getGlChartOfAccount().getCoaCode(),
                            arInvoice, AD_BRNCH, AD_CMPNY);

                }

                // add receivable distribution

               /* try {

                    LocalAdBranchCustomer adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

                    this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE",
                            EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT,
                            RECEIVABLE_ACCNT,
                            null, arInvoice, AD_BRNCH, AD_CMPNY);

                } catch(FinderException ex) {

                }*/

                // compute invoice amount due

                amountDue = TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT;

                //set invoice amount due

                arInvoice.setInvAmountDue(amountDue);

                // remove all payment schedule

                Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();

                i = arInvoicePaymentSchedules.iterator();

                while (i.hasNext()) {

                    LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();

                    i.remove();

                    arInvoicePaymentSchedule.remove();

                }


                // create invoice payment schedule

                short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
                double TOTAL_PAYMENT_SCHEDULE =  0d;

                GregorianCalendar gcPrevDateDue = new GregorianCalendar();
                GregorianCalendar gcDateDue = new GregorianCalendar();
                gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

                Collection adPaymentSchedules = adPaymentTerm.getAdPaymentSchedules();

                i = adPaymentSchedules.iterator();

                while (i.hasNext()) {

                    LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)i.next();

                    // get date due

                    if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")){

                    	gcDateDue.setTime(arInvoice.getInvEffectivityDate());
                    	gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

                    } else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")){

                    	gcDateDue = gcPrevDateDue;
                    	gcDateDue.add(Calendar.MONTH, 1);
                    	gcPrevDateDue = gcDateDue;

                    } else if(arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

                    	gcDateDue = gcPrevDateDue;

                    	if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
                    		if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31 && gcPrevDateDue.get(Calendar.DATE) > 15 && gcPrevDateDue.get(Calendar.DATE) < 31){
                    			gcDateDue.add(Calendar.DATE, 16);
                    		} else {
                        		gcDateDue.add(Calendar.DATE, 15);
                        	}
                    	} else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
                    		if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28 && gcPrevDateDue.get(Calendar.DATE) == 14) {
                    			gcDateDue.add(Calendar.DATE, 14);
                    		} else if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28 && gcPrevDateDue.get(Calendar.DATE) >= 15 && gcPrevDateDue.get(Calendar.DATE) < 28) {
                    			gcDateDue.add(Calendar.DATE, 13);
                    		} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29 && gcPrevDateDue.get(Calendar.DATE) >= 15 && gcPrevDateDue.get(Calendar.DATE) < 29) {
                    			gcDateDue.add(Calendar.DATE, 14);
                    		} else {
                    			gcDateDue.add(Calendar.DATE, 15);
                    		}
                    	}

                    	gcPrevDateDue = gcDateDue;

                	}

                    // create a payment schedule

                    double PAYMENT_SCHEDULE_AMOUNT = 0;

                    // if last payment schedule subtract to avoid rounding difference error

                    if (i.hasNext()) {

                        PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount() / adPaymentTerm.getPytBaseAmount()) * arInvoice.getInvAmountDue(), precisionUnit);

                    } else {

                        PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_PAYMENT_SCHEDULE;

                    }

                    LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
                        arInvoicePaymentScheduleHome.create(gcDateDue.getTime(),
                                adPaymentSchedule.getPsLineNumber(),
                                PAYMENT_SCHEDULE_AMOUNT,
                                0d, EJBCommon.FALSE,
                                (short)0, gcDateDue.getTime(), 0d, 0d,
                                AD_CMPNY);

                   // arInvoice.addArInvoicePaymentSchedule(arInvoicePaymentSchedule);

                    arInvoicePaymentSchedule.setArInvoice(arInvoice);
                    TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;

                }

            }

            Debug.print("ArInvoiceEntryControllerBean saveArInvEntry D Done");
            // generate approval status

            String INV_APPRVL_STATUS = null;

            Debug.print("ArInvoiceEntryControllerBean saveArInvEntry E");
            if (!isDraft) {

                LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

                //validate if amount due + unposted invoices' amount + current balance + unposted receipts' amount
                //does not exceed customer's credit limit

                double balance = 0;
                LocalAdApprovalDocument adInvoiceApprovalDocument =
                    adApprovalDocumentHome.findByAdcType("AR INVOICE", AD_CMPNY);

                if(arCustomer.getCstCreditLimit() > 0) {

                    balance = computeTotalBalance(details.getInvCode(), CST_CSTMR_CODE, AD_CMPNY);

                    balance += amountDue;

                    if(arCustomer.getCstCreditLimit() < balance &&
                            (adApproval.getAprEnableArInvoice() == EJBCommon.FALSE ||
                                    (adApproval.getAprEnableArInvoice() == EJBCommon.TRUE &&
                                            adInvoiceApprovalDocument.getAdcEnableCreditLimitChecking() == EJBCommon.FALSE))) {

                        throw new ArINVAmountExceedsCreditLimitException();

                    }

                }

//              find overdue invoices
                Collection arOverdueInvoices = arInvoicePaymentScheduleHome.
                	findOverdueIpsByInvDateAndCstCustomerCode(arInvoice.getInvDate(), CST_CSTMR_CODE, AD_CMPNY);

                // check if ar invoice approval is enabled

                if (adApproval.getAprEnableArInvoice() == EJBCommon.FALSE || (arCustomer.getCstCreditLimit() > balance &&
                		arOverdueInvoices.size() == 0)) {

                    INV_APPRVL_STATUS = "N/A";

                } else {

                    // check if invoice is self approved

                    LocalAdAmountLimit adAmountLimit = null;

                    try {

                        adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR INVOICE", "REQUESTER", details.getInvLastModifiedBy(), AD_CMPNY);

                    } catch (FinderException ex) {

                        throw new GlobalNoApprovalRequesterFoundException();

                    }

                    if (arInvoice.getInvAmountDue() < adAmountLimit.getCalAmountLimit() && (arCustomer.getCstCreditLimit() == 0 || arCustomer.getCstCreditLimit() > balance)) {

                        INV_APPRVL_STATUS = "N/A";

                    } else {

                        // for approval, create approval queue

                        Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AR INVOICE", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

                        if (adAmountLimits.isEmpty()) {

                            Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

                            if (adApprovalUsers.isEmpty()) {

                                throw new GlobalNoApprovalApproverFoundException();

                            }

                            Iterator j = adApprovalUsers.iterator();

                            while (j.hasNext()) {

                                LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

                                LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR INVOICE", arInvoice.getInvCode(),
                                        arInvoice.getInvNumber(), arInvoice.getInvDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                                adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);



                            }

                        } else {

                            boolean isApprovalUsersFound = false;

                            Iterator i = adAmountLimits.iterator();

                            while (i.hasNext()) {

                                LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

                                if (arInvoice.getInvAmountDue() < adNextAmountLimit.getCalAmountLimit()) {

                                    Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

                                    Iterator j = adApprovalUsers.iterator();

                                    while (j.hasNext()) {

                                        isApprovalUsersFound = true;

                                        LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

                                        LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR INVOICE", arInvoice.getInvCode(),
                                                arInvoice.getInvNumber(), arInvoice.getInvDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                                        adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                                    }

                                    break;

                                } else if (!i.hasNext()) {

                                    Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

                                    Iterator j = adApprovalUsers.iterator();

                                    while (j.hasNext()) {

                                        isApprovalUsersFound = true;

                                        LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

                                        LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR INVOICE", arInvoice.getInvCode(),
                                                arInvoice.getInvNumber(), arInvoice.getInvDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

                                        adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

                                    }

                                    break;

                                }

                                adAmountLimit = adNextAmountLimit;

                            }

                            if (!isApprovalUsersFound) {

                                throw new GlobalNoApprovalApproverFoundException();

                            }

                        }

                        INV_APPRVL_STATUS = "PENDING";
                    }
                }
            }

            Debug.print("ArInvoiceEntryControllerBean saveArInvEntry E Done");
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

                this.executeArInvPost(arInvoice.getInvCode(), arInvoice.getInvLastModifiedBy(), AD_BRNCH, AD_CMPNY);

            }

            // set invoice approval status

            arInvoice.setInvApprovalStatus(INV_APPRVL_STATUS);
            System.out.println("arInvoice.getInvCode()="+arInvoice.getInvCode());
            System.out.println("arInvoice.getInvNumber()="+arInvoice.getInvNumber());
            return arInvoice.getInvNumber();


        } catch (GlobalRecordAlreadyDeletedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalConversionDateNotExistException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalPaymentTermInvalidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (ArINVAmountExceedsCreditLimitException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalNoApprovalRequesterFoundException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }


    }



// private methods

    private LocalArInvoiceLine addArIlEntry(ArModInvoiceLineDetails mdetails, LocalArInvoice arInvoice, Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean addArIlEntry");

        LocalArInvoiceLineHome arInvoiceLineHome = null;
        LocalArStandardMemoLineHome arStandardMemoLineHome = null;


        // Initialize EJB Home

        try {

            arInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
            arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

            double IL_AMNT = 0d;
            double IL_TAX_AMNT = 0d;

            if (mdetails.getIlTax() == EJBCommon.TRUE) {

                LocalArTaxCode arTaxCode = arInvoice.getArTaxCode();

                // calculate net amount
                IL_AMNT = this.calculateIlNetAmount(mdetails, arTaxCode.getTcRate(), arTaxCode.getTcType(), precisionUnit);

                // calculate tax
                IL_TAX_AMNT = this.calculateIlTaxAmount(mdetails, arTaxCode.getTcRate(), arTaxCode.getTcType(), IL_AMNT, precisionUnit);

            } else {

                IL_AMNT = mdetails.getIlAmount();

            }

            LocalArInvoiceLine arInvoiceLine = arInvoiceLineHome.create(
                    mdetails.getIlDescription(), mdetails.getIlQuantity(),
                    mdetails.getIlUnitPrice(), IL_AMNT,
                    IL_TAX_AMNT, mdetails.getIlTax(), AD_CMPNY);

            //arInvoice.addArInvoiceLine(arInvoiceLine);
            arInvoiceLine.setArInvoice(arInvoice);

            LocalArStandardMemoLine arStandardMemoLine = arStandardMemoLineHome.findBySmlName(mdetails.getIlSmlName(), AD_CMPNY);
            //arStandardMemoLine.addArInvoiceLine(arInvoiceLine);
            arInvoiceLine.setArStandardMemoLine(arStandardMemoLine);

            return arInvoiceLine;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private double calculateIlNetAmount(ArModInvoiceLineDetails mdetails, double tcRate, String tcType, short precisionUnit) {

        double amount = 0d;

        if (tcType.equals("INCLUSIVE")) {

            amount = EJBCommon.roundIt(mdetails.getIlAmount() / (1 + (tcRate / 100)), precisionUnit);

        } else {

            // tax exclusive, none, zero rated or exempt

            amount = mdetails.getIlAmount();

        }

        return amount;

    }

    private double calculateIlTaxAmount(ArModInvoiceLineDetails mdetails, double tcRate, String tcType, double amount, short precisionUnit) {

        double taxAmount = 0d;

        if (!tcType.equals("NONE") &&
                !tcType.equals("EXEMPT")) {


            if (tcType.equals("INCLUSIVE")) {

                taxAmount = EJBCommon.roundIt(mdetails.getIlAmount() - amount, precisionUnit);

            } else if (tcType.equals("EXCLUSIVE")) {

                taxAmount = EJBCommon.roundIt(mdetails.getIlAmount() * tcRate / 100, precisionUnit);

            } else {

                // tax none zero-rated or exempt

            }

        }

        return taxAmount;

    }


    private void addArDrEntry(short DR_LN, String DR_CLSS,
            byte DR_DBT, double DR_AMNT, Integer COA_CODE, Integer SC_COA, LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalBranchAccountNumberInvalidException {

        Debug.print("ArInvoiceEntryControllerBean addArDrEntry");

        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            // get company

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);


            LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

            // create distribution record

            if (DR_AMNT < 0  ){

            	 DR_AMNT = DR_AMNT * -1;

            	 if (DR_DBT == 0)
            		 DR_DBT = 1;
            	 else if (DR_DBT == 1)
            		 DR_DBT = 0;

            }

            LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
                    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
                    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

            //arInvoice.addArDistributionRecord(arDistributionRecord);
            arDistributionRecord.setArInvoice(arInvoice);
            //glChartOfAccount.addArDistributionRecord(arDistributionRecord);
            arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

            if(DR_CLSS.equals("SC")){
            	if(SC_COA == null)
            		throw new GlobalBranchAccountNumberInvalidException();
            	else
            		arDistributionRecord.setDrScAccount(SC_COA);
            }

        } catch(FinderException ex) {

            throw new GlobalBranchAccountNumberInvalidException();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }


    private Integer getArGlCoaRevenueAccount(LocalArInvoiceLine arInvoiceLine, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArInvoiceLineEntryControllerBean getArGlCoaRevenueAccount");


        LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;

        // Initialize EJB Home

        try {

            arAutoAccountingSegmentHome = (LocalArAutoAccountingSegmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adBranchStandardMemoLineHome = (LocalAdBranchStandardMemoLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        // generate revenue account

        try {

            String GL_COA_ACCNT = "";

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            LocalGenField genField = adCompany.getGenField();

            String FL_SGMNT_SPRTR = String.valueOf(genField.getFlSegmentSeparator());

            LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;

            try {

                adBranchStandardMemoLine = adBranchStandardMemoLineHome.findBSMLBySMLCodeAndBrCode(arInvoiceLine.getArStandardMemoLine().getSmlCode(), AD_BRNCH, AD_CMPNY);

            } catch(FinderException ex) {

            }


            Collection arAutoAccountingSegments = arAutoAccountingSegmentHome.findByAaAccountType("REVENUE", AD_CMPNY);

            Iterator i = arAutoAccountingSegments.iterator();

            while (i.hasNext()) {

                LocalArAutoAccountingSegment arAutoAccountingSegment =
                    (LocalArAutoAccountingSegment) i.next();

                LocalGlChartOfAccount glChartOfAccount = null;

                if (arAutoAccountingSegment.getAasClassType().equals("AR CUSTOMER")) {


                	 System.out.println("is null 0");
                  	System.out.println(arInvoiceLine.getArInvoice().getArCustomer().getCstGlCoaRevenueAccount().toString());
                  	  System.out.println("is pass");

                    glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                            arInvoiceLine.getArInvoice().getArCustomer().getCstGlCoaRevenueAccount());

                    StringTokenizer st = new StringTokenizer(
                            glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

                    int ctr = 0;
                    while (st.hasMoreTokens()) {

                        ++ctr;

                        if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

                            GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR +
                            st.nextToken();

                            break;

                        } else {

                            st.nextToken();

                        }
                    }


                } else if (arAutoAccountingSegment.getAasClassType().equals("AR STANDARD MEMO LINE")) {

                    if(adBranchStandardMemoLine != null) {

                        try {
                        	 System.out.println("is null 1");
                         	System.out.println(adBranchStandardMemoLine.getBsmlGlCoaRevenueAccount().toString());
                         	  System.out.println("is pass");
                            glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaRevenueAccount());

                        } catch (FinderException ex) {

                        }


                    } else {


                        //glChartOfAccount = arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount();
                    	System.out.println("is null 2");
                     	System.out.println(arInvoiceLine.getArStandardMemoLine().getSmlGlCoaReceivableAccount().toString());
                     	  System.out.println("is pass");
                        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arInvoiceLine.getArStandardMemoLine().getSmlGlCoaReceivableAccount());


                    }

                    StringTokenizer st = new StringTokenizer(
                            glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

                    int ctr = 0;
                    while (st.hasMoreTokens()) {

                        ++ctr;

                        if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

                            GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR +
                            st.nextToken();

                            break;

                        } else {

                            st.nextToken();

                        }
                    }

                }
            }

            GL_COA_ACCNT = GL_COA_ACCNT.substring(1, GL_COA_ACCNT.length());

            try {

                LocalGlChartOfAccount glGeneratedChartOfAccount =
                    glChartOfAccountHome.findByCoaAccountNumber(GL_COA_ACCNT, AD_CMPNY);

                return glGeneratedChartOfAccount.getCoaCode();

            } catch (FinderException ex) {

                if(adBranchStandardMemoLine != null) {

                    LocalGlChartOfAccount glChartOfAccount = null;

                    try {
                    	System.out.println("null 2");
                    	System.out.println(adBranchStandardMemoLine.getBsmlGlCoaRevenueAccount().toString());
                    	System.out.println("pass");
                        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaRevenueAccount());;

                    } catch(FinderException e) {

                    }

                    return glChartOfAccount.getCoaCode();

                } else {


                    return arInvoiceLine.getArStandardMemoLine().getSmlGlCoaRevenueAccount();

                }

            }


        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


private Integer getArGlCoaReceivableAccount(LocalArInvoiceLine arInvoiceLine, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("ArInvoiceLineEntryControllerBean getArGlCoaReceivableAccount");


        LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;

        // Initialize EJB Home

        try {

            arAutoAccountingSegmentHome = (LocalArAutoAccountingSegmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adBranchStandardMemoLineHome = (LocalAdBranchStandardMemoLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        // generate revenue account

        try {

            String GL_COA_ACCNT = "";

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            LocalGenField genField = adCompany.getGenField();

            String FL_SGMNT_SPRTR = String.valueOf(genField.getFlSegmentSeparator());

            LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;

            try {

                adBranchStandardMemoLine = adBranchStandardMemoLineHome.findBSMLBySMLCodeAndBrCode(arInvoiceLine.getArStandardMemoLine().getSmlCode(), AD_BRNCH, AD_CMPNY);

            } catch(FinderException ex) {

            }



            Collection arAutoAccountingSegments = arAutoAccountingSegmentHome.findByAaAccountType("REVENUE", AD_CMPNY);

            Iterator i = arAutoAccountingSegments.iterator();

            while (i.hasNext()) {

                LocalArAutoAccountingSegment arAutoAccountingSegment =
                    (LocalArAutoAccountingSegment) i.next();

                LocalGlChartOfAccount glChartOfAccount = null;

                if (arAutoAccountingSegment.getAasClassType().equals("AR CUSTOMER")) {
                	System.out.println("AR CUSTOMER---------------->");

                	System.out.println("null 3");
                	System.out.println(arInvoiceLine.getArInvoice().getArCustomer().getCstGlCoaReceivableAccount());
                	System.out.println("pass");

                    glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                            arInvoiceLine.getArInvoice().getArCustomer().getCstGlCoaReceivableAccount());

                    StringTokenizer st = new StringTokenizer(
                            glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

                    int ctr = 0;
                    while (st.hasMoreTokens()) {

                        ++ctr;

                        if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

                            GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR +
                            st.nextToken();

                            break;

                        } else {

                            st.nextToken();

                        }
                    }


                } else if (arAutoAccountingSegment.getAasClassType().equals("AR STANDARD MEMO LINE")) {
                	System.out.println("AR STANDARD MEMO LINE---------------->");
                    if(adBranchStandardMemoLine != null) {

                        try {

                            glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaReceivableAccount());

                        } catch (FinderException ex) {

                        }


                    } else {


                        //glChartOfAccount = arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount();

                        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(arInvoiceLine.getArStandardMemoLine().getSmlGlCoaReceivableAccount());


                    }

                    StringTokenizer st = new StringTokenizer(
                            glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

                    int ctr = 0;
                    while (st.hasMoreTokens()) {

                        ++ctr;

                        if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

                            GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR +
                            st.nextToken();

                            break;

                        } else {

                            st.nextToken();

                        }
                    }

                }
            }
            System.out.println("GL_COA_ACCNT="+GL_COA_ACCNT);
            GL_COA_ACCNT = GL_COA_ACCNT.substring(1, GL_COA_ACCNT.length());

            try {

                LocalGlChartOfAccount glGeneratedChartOfAccount =
                    glChartOfAccountHome.findByCoaAccountNumber(GL_COA_ACCNT, AD_CMPNY);

                return glGeneratedChartOfAccount.getCoaCode();

            } catch (FinderException ex) {

                if(adBranchStandardMemoLine != null) {

                    LocalGlChartOfAccount glChartOfAccount = null;

                    try {

                        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlCoaReceivableAccount());;

                    } catch(FinderException e) {

                    }

                    return glChartOfAccount.getCoaCode();

                } else {


                    return arInvoiceLine.getArStandardMemoLine().getSmlGlCoaReceivableAccount();

                }

            }


        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    private void executeArInvPost(Integer INV_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
    GlobalRecordAlreadyDeletedException,
    GlobalTransactionAlreadyPostedException,
    GlobalTransactionAlreadyVoidException,
    GlJREffectiveDateNoPeriodExistException,
    GlJREffectiveDatePeriodClosedException,
    GlobalJournalNotBalanceException,
    GlobalBranchAccountNumberInvalidException,
    AdPRFCoaGlVarianceAccountNotFoundException,
	GlobalExpiryDateNotFoundException  {

        Debug.print("ArInvoiceEntryControllerBean executeApInvPost");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemHome invItemHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalGlForexLedgerHome glForexLedgerHome = null;
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

        LocalArInvoice arInvoice = null;
        LocalArInvoice arCreditedInvoice = null;

        LocalInvItemLocationHome invItemLocationHome = null;

        // Initialize EJB Home

        try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
            lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            // validate if invoice/credit memo is already deleted

            try {

                arInvoice = arInvoiceHome.findByPrimaryKey(INV_CODE);

            } catch (FinderException ex) {

                throw new GlobalRecordAlreadyDeletedException();

            }

            // validate if invoice/credit memo is already posted or void

            if (arInvoice.getInvPosted() == EJBCommon.TRUE) {

                throw new GlobalTransactionAlreadyPostedException();

            } else if (arInvoice.getInvVoid() == EJBCommon.TRUE) {

                throw new GlobalTransactionAlreadyVoidException();
            }

            // regenerate cogs & inventory dr
            if (adPreference.getPrfArAutoComputeCogs() == EJBCommon.TRUE) {
            	this.regenerateInventoryDr(arInvoice, AD_BRNCH, AD_CMPNY);
            }

            // post invoice/credit memo

            if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                // increase customer balance

                double INV_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
                        arInvoice.getGlFunctionalCurrency().getFcName(),
                        arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
                        arInvoice.getInvAmountDue(), AD_CMPNY);

                this.post(arInvoice.getInvDate(), INV_AMNT, arInvoice.getArCustomer(), AD_CMPNY);

                Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();
                Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

                if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

                    Iterator c = arInvoiceLineItems.iterator();

                    while(c.hasNext()) {

                        LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

                        if (arInvoiceLineItem.getIliEnableAutoBuild() == 1 && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

                            String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
                            String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();
                            double QTY_SLD = this.convertByUomFromAndItemAndQuantity(
                                    arInvoiceLineItem.getInvUnitOfMeasure(), arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

                            LocalInvCosting invCosting = null;

                            double TOTAL_AMOUNT = 0d;

                            Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

                            Iterator j = invBillOfMaterials.iterator();

                            while (j.hasNext()) {

                                LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)j.next();

                                LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

                                LocalInvCosting invBomCosting = null;

                                double BOM_QTY_NDD = 0d;
                                double COST = 0d;
                                double BOM_AMOUNT = 0d;

                                LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

                                try {

                                    invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), invBillOfMaterial.getBomIiName(), LOC_NM, AD_BRNCH, AD_CMPNY);


                                } catch (FinderException ex) {

                                }

                                if (invBomCosting == null) {

                                    // bom conversion
                                    BOM_QTY_NDD = this.convertByUomFromAndItemAndQuantity(
                                            invBillOfMaterial.getInvUnitOfMeasure(), invItem,
                                            EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() *  QTY_SLD,
                                            this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);

                                    BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * invItem.getIiUnitCost(), adCompany.getGlFunctionalCurrency().getFcPrecision());

                                    TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

                                    this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), -BOM_QTY_NDD, -BOM_AMOUNT, -BOM_QTY_NDD,
                                    		-BOM_AMOUNT, invBillOfMaterial.getBomIiName(), LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);


                                } else {

                                    // bom conversion
                                    BOM_QTY_NDD = this.convertByUomFromAndItemAndQuantity(
                                            invBillOfMaterial.getInvUnitOfMeasure(), invItem,
                                            EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() *  QTY_SLD,
                                            this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);

                                    COST = Math.abs(invBomCosting.getCstRemainingValue() / invBomCosting.getCstRemainingQuantity());

                                    BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * COST, adCompany.getGlFunctionalCurrency().getFcPrecision());

                                    TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

                                    this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), -BOM_QTY_NDD, -BOM_AMOUNT,
                                    		invBomCosting.getCstRemainingQuantity() - BOM_QTY_NDD,
                                    		invBomCosting.getCstRemainingValue() - (BOM_AMOUNT), invBillOfMaterial.getBomIiName(),
											LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);

                                }

                            }

                            try {

                                invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

                            } catch (FinderException ex) {

                            }

                            if (invCosting == null) {

                                this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, TOTAL_AMOUNT, QTY_SLD,
                                		TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);

                            } else {

                            	//compute cost variance
            					double CST_VRNC_VL = 0d;

            					if(invCosting.getCstRemainingQuantity() < 0)
            						CST_VRNC_VL = (invCosting.getCstRemainingQuantity() * (TOTAL_AMOUNT/QTY_SLD) -
            								invCosting.getCstRemainingValue());

                                this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, TOTAL_AMOUNT,
                                		invCosting.getCstRemainingQuantity() + QTY_SLD,
										invCosting.getCstRemainingValue() + TOTAL_AMOUNT, II_NM, LOC_NM, CST_VRNC_VL, USR_NM,
										AD_BRNCH, AD_CMPNY);

                            }

                            try {

                                invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

                            } catch (FinderException ex) {

                            }

                            if (invCosting == null) {

                                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, TOTAL_AMOUNT, -QTY_SLD,
                                		-TOTAL_AMOUNT, 0d, null, AD_BRNCH, AD_CMPNY);

                            } else {

                                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, TOTAL_AMOUNT,
                                		invCosting.getCstRemainingQuantity() - QTY_SLD,
										invCosting.getCstRemainingValue() - TOTAL_AMOUNT, 0d, null, AD_BRNCH, AD_CMPNY);

                            }

                        } else {

                            String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
                            String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

                            double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                                    arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

                            LocalInvCosting invCosting = null;

                            try {

                                invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

                            } catch (FinderException ex) {

                            }

                            double COST = 0d;

                            if (invCosting == null) {

                                COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

                                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, QTY_SLD * COST, -QTY_SLD,
                                		-(QTY_SLD * COST), 0d, null, AD_BRNCH, AD_CMPNY);

                            } else {


                                if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
                                {

	                                COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

	                                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, QTY_SLD * COST,
	                                		invCosting.getCstRemainingQuantity() - QTY_SLD,
	                                		invCosting.getCstRemainingValue() - (QTY_SLD * COST), 0d, null, AD_BRNCH, AD_CMPNY);
                                }
                                else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
                                {

            	        			double fifoCost = this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
            	        					-QTY_SLD, arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

            	        			//post entries to database
            	        			this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, fifoCost * QTY_SLD,
            	        					invCosting.getCstRemainingQuantity() - QTY_SLD,
            	        					invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD), 0d, null, AD_BRNCH, AD_CMPNY);

                                }
                                else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
                                {

            	        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

            	        			//post entries to database
            	        			this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), QTY_SLD, standardCost * QTY_SLD,
            	        					invCosting.getCstRemainingQuantity() - QTY_SLD,
            	        					invCosting.getCstRemainingValue() - (standardCost * QTY_SLD), 0d, null, AD_BRNCH, AD_CMPNY);

                                }

                            }

                        }

                    }

                } else if (arSalesOrderInvoiceLines != null && !arSalesOrderInvoiceLines.isEmpty()) {

                    Iterator c = arSalesOrderInvoiceLines.iterator();

                    while(c.hasNext()) {

                        LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine) c.next();
                        LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();

                        String II_NM = arSalesOrderLine.getInvItemLocation().getInvItem().getIiName();
                        String LOC_NM = arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName();
                        double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arSalesOrderLine.getInvUnitOfMeasure(),
                                arSalesOrderLine.getInvItemLocation().getInvItem(), arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);

                        LocalInvCosting invCosting = null;

                        try {

                            invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

                        } catch (FinderException ex) {

                        }

                        double COST = 0d;

                        if (invCosting == null ) {

                            COST = arSalesOrderLine.getInvItemLocation().getInvItem().getIiUnitCost();

                            this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD,  QTY_SLD * COST, -QTY_SLD,
                            		-(QTY_SLD * COST), 0d, null, AD_BRNCH, AD_CMPNY);

                        } else {


                            if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
                            {
	                            COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

	                            this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD, QTY_SLD * COST,
	                            		invCosting.getCstRemainingQuantity() - QTY_SLD,
	                            		invCosting.getCstRemainingValue() - (QTY_SLD * COST), 0d, null, AD_BRNCH, AD_CMPNY);
                            }
                            else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
                            {
        	        			double fifoCost = this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
        	        					-QTY_SLD, arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice(), true, AD_BRNCH, AD_CMPNY);

        	        			//post entries to database
        	        			this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD, fifoCost * QTY_SLD,
        	        					invCosting.getCstRemainingQuantity() - QTY_SLD,
        	        					invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD), 0d, null, AD_BRNCH, AD_CMPNY);
                            }
                            else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
                            {
        	        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

        	        			//post entries to database
        	        			this.postToInvSo(arSalesOrderInvoiceLine, arInvoice.getInvDate(), QTY_SLD, standardCost * QTY_SLD,
        	        					invCosting.getCstRemainingQuantity() - QTY_SLD,
        	        					invCosting.getCstRemainingValue() - (standardCost * QTY_SLD), 0d, null, AD_BRNCH, AD_CMPNY);
                            }

                        }
                    }
                }


            } else { // credit memo

                // get credited invoice

                arCreditedInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
                        arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

                // decrease customer balance

                double INV_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
                        arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
                        arCreditedInvoice.getInvConversionDate(), arCreditedInvoice.getInvConversionRate(),
                        arInvoice.getInvAmountDue(), AD_CMPNY) * -1;

                this.post(arInvoice.getInvDate(), INV_AMNT, arInvoice.getArCustomer(), AD_CMPNY);

                // decrease invoice and ips amounts and release lock

                double CREDIT_PERCENT = EJBCommon.roundIt(arInvoice.getInvAmountDue() / arCreditedInvoice.getInvAmountDue(), (short)6);

                arCreditedInvoice.setInvAmountPaid(arCreditedInvoice.getInvAmountPaid() + arInvoice.getInvAmountDue());

                double TOTAL_INVOICE_PAYMENT_SCHEDULE =  0d;

                Collection arInvoicePaymentSchedules = arCreditedInvoice.getArInvoicePaymentSchedules();

                Iterator i = arInvoicePaymentSchedules.iterator();

                while (i.hasNext()) {

                    LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
                        (LocalArInvoicePaymentSchedule)i.next();

                    double INVOICE_PAYMENT_SCHEDULE_AMOUNT = 0;

                    // if last payment schedule subtract to avoid rounding difference error

                    if (i.hasNext()) {

                        INVOICE_PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt(arInvoicePaymentSchedule.getIpsAmountDue() * CREDIT_PERCENT, this.getGlFcPrecisionUnit(AD_CMPNY));

                    } else {

                        INVOICE_PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_INVOICE_PAYMENT_SCHEDULE;

                    }

                    arInvoicePaymentSchedule.setIpsAmountPaid(arInvoicePaymentSchedule.getIpsAmountPaid() + INVOICE_PAYMENT_SCHEDULE_AMOUNT);

                    arInvoicePaymentSchedule.setIpsLock(EJBCommon.FALSE);

                    TOTAL_INVOICE_PAYMENT_SCHEDULE += INVOICE_PAYMENT_SCHEDULE_AMOUNT;

                }

                Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

                if (arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

                    Iterator c = arInvoiceLineItems.iterator();

                    while(c.hasNext()) {

                        LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

                        if (arInvoiceLineItem.getIliEnableAutoBuild() == 1 && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

                            String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
                            String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();
                            double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(), arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

                            LocalInvCosting invCosting = null;

                            double TOTAL_AMOUNT = 0d;

                            Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

                            Iterator j = invBillOfMaterials.iterator();

                            while (j.hasNext()) {

                                LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)j.next();

                                LocalInvCosting invBomCosting = null;

                                double BOM_QTY_NDD = 0d;
                                double COST = 0d;
                                double BOM_AMOUNT = 0d;

                                LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

                                try {

                                    invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), invBillOfMaterial.getBomIiName(), invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);


                                } catch (FinderException ex) {

                                }

                                if (invBomCosting == null) {

                                    // bom conversion
                                    BOM_QTY_NDD = this.convertByUomFromAndItemAndQuantity(
                                            invBillOfMaterial.getInvUnitOfMeasure(), invItem,
                                            EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() *  QTY_SLD,
                                            this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);

                                    BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * invItem.getIiUnitCost(), adCompany.getGlFunctionalCurrency().getFcPrecision());

                                    TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

                                    this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), BOM_QTY_NDD, BOM_AMOUNT, BOM_QTY_NDD,
                                    		BOM_AMOUNT, invBillOfMaterial.getBomIiName(), LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);


                                } else {

                                    // bom conversion
                                    BOM_QTY_NDD = this.convertByUomFromAndItemAndQuantity(
                                            invBillOfMaterial.getInvUnitOfMeasure(), invItem,
                                            EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() *  QTY_SLD,
                                            this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);

                                    COST = Math.abs(invBomCosting.getCstRemainingValue() / invBomCosting.getCstRemainingQuantity());

                                    BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * COST, adCompany.getGlFunctionalCurrency().getFcPrecision());

                                    TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);


                                	//compute cost variance
                					double CST_VRNC_VL = 0d;

                					if(invBomCosting.getCstRemainingQuantity() < 0)
                						CST_VRNC_VL = (invBomCosting.getCstRemainingQuantity() * (TOTAL_AMOUNT/QTY_SLD) -
                								invBomCosting.getCstRemainingValue());

                                    this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), BOM_QTY_NDD,  BOM_AMOUNT,
                                    		invBomCosting.getCstRemainingQuantity() + BOM_QTY_NDD,
                                    		invBomCosting.getCstRemainingValue() + (BOM_AMOUNT),
											invBillOfMaterial.getBomIiName(), LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);

                                }

                            }

                            try {

                                invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

                            } catch (FinderException ex) {

                            }

                            if (invCosting == null) {

                                this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD, -TOTAL_AMOUNT, -QTY_SLD, -TOTAL_AMOUNT,
                                		II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);

                            } else {

                                this.postToBua(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD, -TOTAL_AMOUNT,
                                		invCosting.getCstRemainingQuantity() - QTY_SLD,
										invCosting.getCstRemainingValue() - TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);

                            }

                            try {

                                invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

                            } catch (FinderException ex) {

                            }

                            if (invCosting == null) {

                                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD,  -TOTAL_AMOUNT, QTY_SLD, TOTAL_AMOUNT,
                                		0d, null, AD_BRNCH, AD_CMPNY);

                            } else {

                                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD, -TOTAL_AMOUNT,
                                		invCosting.getCstRemainingQuantity() + QTY_SLD,
										invCosting.getCstRemainingValue() + TOTAL_AMOUNT, 0d, null, AD_BRNCH, AD_CMPNY);

                            }

                        } else {

                            String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
                            String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();
                            double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                                    arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

                            LocalInvCosting invCosting = null;

                            try {

                                invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arInvoice.getInvDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

                            } catch (FinderException ex) {

                            }

                            double COST = 0d;

                            if (invCosting == null) {

                                COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

                                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD, -(QTY_SLD * COST), QTY_SLD,
                                		QTY_SLD * COST, 0d, null, AD_BRNCH, AD_CMPNY);

                            } else {

                               //compute cost variance
            					double CST_VRNC_VL = 0d;

            					if(invCosting.getCstRemainingQuantity() < 0)
            						CST_VRNC_VL = (invCosting.getCstRemainingQuantity() * COST -
            								invCosting.getCstRemainingValue());

                            	if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
                            	{
	                                COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

	                                this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD, -(QTY_SLD * COST),
	                                		invCosting.getCstRemainingQuantity() + QTY_SLD,
	                                		invCosting.getCstRemainingValue() + (QTY_SLD * COST), CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);

                            	}
                            	else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
                            	{

            	        			double fifoCost = this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
            	        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

            	        			//post entries to database
            	        			this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD, fifoCost * -QTY_SLD,
            	        					invCosting.getCstRemainingQuantity() + QTY_SLD,
            	        					invCosting.getCstRemainingValue() + (fifoCost * QTY_SLD), CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);

                            	}
                            	else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
                            	{

            	        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

            	        			//post entries to database
            	        			this.postToInv(arInvoiceLineItem, arInvoice.getInvDate(), -QTY_SLD, standardCost * -QTY_SLD,
            	        					invCosting.getCstRemainingQuantity() + QTY_SLD,
            	        					invCosting.getCstRemainingValue() + (standardCost * QTY_SLD), CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);

                            	}
                            }

                        }

                    }

                }

            }

            // set invoice post status

            arInvoice.setInvPosted(EJBCommon.TRUE);
            arInvoice.setInvPostedBy(USR_NM);
            arInvoice.setInvDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


            // post to gl if necessary

            if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

                // validate if date has no period and period is closed

                LocalGlSetOfBook glJournalSetOfBook = null;

                try {

                    glJournalSetOfBook = glSetOfBookHome.findByDate(arInvoice.getInvDate(), AD_CMPNY);

                } catch (FinderException ex) {

                    throw new GlJREffectiveDateNoPeriodExistException();

                }

                LocalGlAccountingCalendarValue glAccountingCalendarValue =
                    glAccountingCalendarValueHome.findByAcCodeAndDate(
                            glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), arInvoice.getInvDate(), AD_CMPNY);


                if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
                        glAccountingCalendarValue.getAcvStatus() == 'C' ||
                        glAccountingCalendarValue.getAcvStatus() == 'P') {

                    throw new GlJREffectiveDatePeriodClosedException();

                }

                // check if invoice is balance if not check suspense posting

                LocalGlJournalLine glOffsetJournalLine = null;

                Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);

                Iterator j = arDistributionRecords.iterator();

                double TOTAL_DEBIT = 0d;
                double TOTAL_CREDIT = 0d;

                while (j.hasNext()) {

                    LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

                    double DR_AMNT = 0d;

                    if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                        DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
                                arInvoice.getGlFunctionalCurrency().getFcName(),
                                arInvoice.getInvConversionDate(),
                                arInvoice.getInvConversionRate(),
                                arDistributionRecord.getDrAmount(), AD_CMPNY);

                    } else {

                        DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
                                arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
                                arCreditedInvoice.getInvConversionDate(),
                                arCreditedInvoice.getInvConversionRate(),
                                arDistributionRecord.getDrAmount(), AD_CMPNY);

                    }

                    if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

                        TOTAL_DEBIT += DR_AMNT;

                    } else {

                        TOTAL_CREDIT += DR_AMNT;

                    }

                }

                TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
                TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

                if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
                        TOTAL_DEBIT != TOTAL_CREDIT) {

                    LocalGlSuspenseAccount glSuspenseAccount = null;

                    try {

                        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES", arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS", AD_CMPNY);

                    } catch (FinderException ex) {

                        throw new GlobalJournalNotBalanceException();

                    }

                    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

                        glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1), EJBCommon.TRUE,
                                TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

                    } else {

                        glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1), EJBCommon.FALSE,
                                TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

                    }

                    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
                    //glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
                    glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);

                } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
                        TOTAL_DEBIT != TOTAL_CREDIT) {

                    throw new GlobalJournalNotBalanceException();

                }

                // create journal batch if necessary

                LocalGlJournalBatch glJournalBatch = null;
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

                try {

                    if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

                        glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arInvoice.getArInvoiceBatch().getIbName(), AD_BRNCH, AD_CMPNY);

                    } else {

                        glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", AD_BRNCH, AD_CMPNY);

                    }


                } catch (FinderException ex) {
                }

                if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
                        glJournalBatch == null) {

                    if (adPreference.getPrfEnableArInvoiceBatch() == EJBCommon.TRUE) {

                        glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arInvoice.getArInvoiceBatch().getIbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

                    } else {

                        glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES INVOICES", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

                    }


                }

                // create journal entry

                LocalGlJournal glJournal = glJournalHome.create(arInvoice.getInvReferenceNumber(),
                        arInvoice.getInvDescription(), arInvoice.getInvDate(),
                        0.0d, null, arInvoice.getInvNumber(), null, 1d, "N/A", null,
                        'N', EJBCommon.TRUE, EJBCommon.FALSE,
                        USR_NM, new Date(),
                        USR_NM, new Date(),
                        null, null,
                        USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
                        arInvoice.getArCustomer().getCstTin(),
                        arInvoice.getArCustomer().getCstName(), EJBCommon.FALSE,
                        null,
                        AD_BRNCH, AD_CMPNY);

                LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
                glJournal.setGlJournalSource(glJournalSource);

                LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
                glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

                LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName(arInvoice.getInvCreditMemo() == EJBCommon.FALSE ? "SALES INVOICES" : "CREDIT MEMOS", AD_CMPNY);
                glJournal.setGlJournalCategory(glJournalCategory);

                if (glJournalBatch != null) {

                    glJournal.setGlJournalBatch(glJournalBatch);
                }


                // create journal lines

                j = arDistributionRecords.iterator();

                while (j.hasNext()) {

                    LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

                    double DR_AMNT = 0d;

                    if (arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                        DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
                                arInvoice.getGlFunctionalCurrency().getFcName(),
                                arInvoice.getInvConversionDate(),
                                arInvoice.getInvConversionRate(),
                                arDistributionRecord.getDrAmount(), AD_CMPNY);

                    } else {

                        DR_AMNT = this.convertForeignToFunctionalCurrency(arCreditedInvoice.getGlFunctionalCurrency().getFcCode(),
                                arCreditedInvoice.getGlFunctionalCurrency().getFcName(),
                                arCreditedInvoice.getInvConversionDate(),
                                arCreditedInvoice.getInvConversionRate(),
                                arDistributionRecord.getDrAmount(), AD_CMPNY);

                    }

                    LocalGlJournalLine glJournalLine = glJournalLineHome.create(arDistributionRecord.getDrLine(),
                            arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

                    //arDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
                    glJournalLine.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());

                    //glJournal.addGlJournalLine(glJournalLine);
                    glJournalLine.setGlJournal(glJournal);
                    arDistributionRecord.setDrImported(EJBCommon.TRUE);

                    // for FOREX revaluation

                    LocalArInvoice arInvoiceTemp = arInvoice.getInvCreditMemo() == EJBCommon.FALSE ?
                            arInvoice: arCreditedInvoice;

                    if((arInvoiceTemp.getGlFunctionalCurrency().getFcCode() !=
                        adCompany.getGlFunctionalCurrency().getFcCode()) &&
                        glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
                        (glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
                                arInvoiceTemp.getGlFunctionalCurrency().getFcCode()))){

                        double CONVERSION_RATE = 1;

                        if (arInvoiceTemp.getInvConversionRate() != 0 && arInvoiceTemp.getInvConversionRate() != 1) {

                            CONVERSION_RATE = arInvoiceTemp.getInvConversionRate();

                        } else if (arInvoiceTemp.getInvConversionDate() != null){

                        	CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
                        			glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
                        			glJournal.getJrConversionDate(), AD_CMPNY);

                        }

                        Collection glForexLedgers = null;

                        try {

                            glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
                                    arInvoiceTemp.getInvDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

                        } catch(FinderException ex) {

                        }

                        LocalGlForexLedger glForexLedger =
                            (glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
                                (LocalGlForexLedger) glForexLedgers.iterator().next();

                        int FRL_LN = (glForexLedger != null &&
                                glForexLedger.getFrlDate().compareTo(arInvoiceTemp.getInvDate()) == 0) ?
                                        glForexLedger.getFrlLine().intValue() + 1 : 1;

                        // compute balance
                        double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
                        double FRL_AMNT = arDistributionRecord.getDrAmount();

                        if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
                            FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));
                        else
                            FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);

                        COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

                        glForexLedger = glForexLedgerHome.create(arInvoiceTemp.getInvDate(), new Integer (FRL_LN),
                                "SI", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

                        //glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
                        glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());

                        // propagate balances
                        try{

                            glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
                                    glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
                                    glForexLedger.getFrlAdCompany());

                        } catch (FinderException ex) {

                        }

                        Iterator itrFrl = glForexLedgers.iterator();

                        while (itrFrl.hasNext()) {

                            glForexLedger = (LocalGlForexLedger) itrFrl.next();
                            FRL_AMNT = arDistributionRecord.getDrAmount();

                            if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
                                FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT :
                                    (- 1 * FRL_AMNT));
                            else
                                FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) :
                                    FRL_AMNT);

                            glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

                        }

                    }

                }

                if (glOffsetJournalLine != null) {

                    //glJournal.addGlJournalLine(glOffsetJournalLine);
                    glOffsetJournalLine.setGlJournal(glJournal);

                }

                // post journal to gl

                Collection glJournalLines = glJournal.getGlJournalLines();

                Iterator i = glJournalLines.iterator();

                while (i.hasNext()) {

                    LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

                    // post current to current acv

                    this.postToGl(glAccountingCalendarValue,
                            glJournalLine.getGlChartOfAccount(),
                            true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


                    // post to subsequent acvs (propagate)

                    Collection glSubsequentAccountingCalendarValues =
                        glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                                glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                                glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

                    Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

                    while (acvsIter.hasNext()) {

                        LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                            (LocalGlAccountingCalendarValue)acvsIter.next();

                        this.postToGl(glSubsequentAccountingCalendarValue,
                                glJournalLine.getGlChartOfAccount(),
                                false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                    }

                    // post to subsequent years if necessary

                    Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

                    if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

                        adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
                        LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

                        Iterator sobIter = glSubsequentSetOfBooks.iterator();

                        while (sobIter.hasNext()) {

                            LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

                            String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

                            // post to subsequent acvs of subsequent set of book(propagate)

                            Collection glAccountingCalendarValues =
                                glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

                            Iterator acvIter = glAccountingCalendarValues.iterator();

                            while (acvIter.hasNext()) {

                                LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                                    (LocalGlAccountingCalendarValue)acvIter.next();

                                if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
                                        ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

                                    this.postToGl(glSubsequentAccountingCalendarValue,
                                            glJournalLine.getGlChartOfAccount(),
                                            false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                                } else { // revenue & expense

                                    this.postToGl(glSubsequentAccountingCalendarValue,
                                            glRetainedEarningsAccount,
                                            false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                                }

                            }

                            if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

                        }

                    }

                }

            }

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalExpiryDateNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        }catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }


    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST,
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {

		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;

	     // Initialize EJB Home

	     try {

	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	     }
	     catch (NamingException ex) {

	    	 throw new EJBException(ex.getMessage());
	     }

		try {

			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);

			if (invFifoCostings.size() > 0) {

				Iterator x = invFifoCostings.iterator();

	  			if (isAdjustFifo) {

	  				//executed during POST transaction

	  				double totalCost = 0d;
	  				double cost;

	  				if(CST_QTY < 0) {

	  					//for negative quantities
	 	  				double neededQty = -(CST_QTY);

	 	  				while(x.hasNext() && neededQty != 0) {

	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
	 		 	  			} else {
	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
	 		 	  			}

	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
	 			  				totalCost += (neededQty * cost);
	 			  				neededQty = 0d;
	 	  					} else {

	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
	 	  					}
	 	  				}

	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
	 	  				if(neededQty != 0) {

	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
	 	  				}

	 	  				cost = totalCost / -CST_QTY;
	  				}

	  				else {

	  					//for positive quantities
	  					cost = CST_COST;
	  				}
	  				return cost;
	  			}

	  			else {

	  				//executed during ENTRY transaction

	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			}
	  			}
			}
			else {

				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}

		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}



    private void post(Date INV_DT, double INV_AMNT, LocalArCustomer arCustomer, Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean post");

        LocalArCustomerBalanceHome arCustomerBalanceHome = null;

        // Initialize EJB Home

        try {

            arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);

        } catch (NamingException ex) {

            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

        try {

            // find customer balance before or equal invoice date

            Collection arCustomerBalances = arCustomerBalanceHome.findByBeforeOrEqualInvDateAndCstCustomerCode(INV_DT, arCustomer.getCstCustomerCode(), AD_CMPNY);

            if (!arCustomerBalances.isEmpty()) {

                // get last invoice

                ArrayList arCustomerBalanceList = new ArrayList(arCustomerBalances);

                LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance)arCustomerBalanceList.get(arCustomerBalanceList.size() - 1);

                if (arCustomerBalance.getCbDate().before(INV_DT)) {

                    // create new balance

                    LocalArCustomerBalance apNewCustomerBalance = arCustomerBalanceHome.create(
                            INV_DT, arCustomerBalance.getCbBalance() + INV_AMNT, AD_CMPNY);

                    //arCustomer.addArCustomerBalance(apNewCustomerBalance);
                    apNewCustomerBalance.setArCustomer(arCustomer);

                } else { // equals to invoice date

                    arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);

                }

            } else {

                // create new balance

                LocalArCustomerBalance apNewCustomerBalance = arCustomerBalanceHome.create(
                        INV_DT, INV_AMNT, AD_CMPNY);

                //arCustomer.addArCustomerBalance(apNewCustomerBalance);
                apNewCustomerBalance.setArCustomer(arCustomer);

            }

            // propagate to subsequent balances if necessary

            arCustomerBalances = arCustomerBalanceHome.findByAfterInvDateAndCstCustomerCode(INV_DT, arCustomer.getCstCustomerCode(), AD_CMPNY);

            Iterator i = arCustomerBalances.iterator();

            while (i.hasNext()) {

                LocalArCustomerBalance arCustomerBalance = (LocalArCustomerBalance)i.next();

                arCustomerBalance.setCbBalance(arCustomerBalance.getCbBalance() + INV_AMNT);

            }

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
            Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean convertForeignToFunctionalCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        // get company and extended precision

        try {

            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());

        }


        // Convert to functional currency if necessary

        if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

            AMOUNT = AMOUNT / CONVERSION_RATE;

        }
        return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

    }

    private LocalArInvoiceLineItem addArIliEntry(ArModInvoiceLineItemDetails mdetails, LocalArInvoice arInvoice, LocalInvItemLocation invItemLocation, Integer AD_CMPNY) throws GlobalMiscInfoIsRequiredException {

        Debug.print("ArInvoiceEntryControllerBean addArIliEntry");

        LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

        // Initialize EJB Home

        try {

            arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

            double ILI_AMNT = 0d;
            double ILI_TAX_AMNT = 0d;

            LocalArTaxCode arTaxCode = arInvoice.getArTaxCode();

            // calculate net amount
            ILI_AMNT = this.calculateIliNetAmount(mdetails, arTaxCode.getTcRate(), arTaxCode.getTcType(), precisionUnit);

            // calculate tax
            ILI_TAX_AMNT = this.calculateIliTaxAmount(mdetails, arTaxCode.getTcRate(), arTaxCode.getTcType(), ILI_AMNT, precisionUnit);


            LocalArInvoiceLineItem arInvoiceLineItem = arInvoiceLineItemHome.create(
                    mdetails.getIliLine(), mdetails.getIliQuantity(), mdetails.getIliUnitPrice(),
                    ILI_AMNT, ILI_TAX_AMNT, mdetails.getIliEnableAutoBuild(), mdetails.getIliDiscount1(),
                    mdetails.getIliDiscount2(),mdetails.getIliDiscount3(), mdetails.getIliDiscount4(),
                    mdetails.getIliTotalDiscount(), EJBCommon.TRUE, AD_CMPNY);

            //arInvoice.addArInvoiceLineItem(arInvoiceLineItem);
            arInvoiceLineItem.setArInvoice(arInvoice);

            //invItemLocation.addArInvoiceLineItem(arInvoiceLineItem);
            arInvoiceLineItem.setInvItemLocation(invItemLocation);

            LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getIliUomName(), AD_CMPNY);
            //invUnitOfMeasure.addArInvoiceLineItem(arInvoiceLineItem);
            arInvoiceLineItem.setInvUnitOfMeasure(invUnitOfMeasure);

            if(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()==1){
            	if(mdetails.getIliMisc()==null || mdetails.getIliMisc()==""){
            		throw new GlobalMiscInfoIsRequiredException();
            	}else{
            		System.out.println("mdetails.getIliMisc(): " + mdetails.getIliMisc());
            		int qty2Prpgt =0;
            		try{
            			//qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(mdetails.getIliMisc()));
            		}catch(Exception e){
            			qty2Prpgt =this.checkExpiryDates(mdetails.getIliMisc()+"fin$");
            		}

            		/*String miscList2Prpgt = this.checkExpiryDates(mdetails.getIliMisc(), qty2Prpgt, "False");
            		if(miscList2Prpgt!="Error"){
            			arInvoiceLineItem.setIliMisc(mdetails.getIliMisc());
            		}else{
            			throw new GlobalMiscInfoIsRequiredException();
            		}*/

            	}
            }else{
            	arInvoiceLineItem.setIliMisc(mdetails.getIliMisc());
            }

            return arInvoiceLineItem;

        }catch (GlobalMiscInfoIsRequiredException ex){

            getSessionContext().setRollbackOnly();
            throw ex;
        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    public String checkExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);

    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}

    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;

    	String miscList = new String();
    	String miscList2 = "";

    	for(int x=0; x<qty; x++) {

    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		String g= misc.substring(start, start + length);
    		System.out.println("g: " + g);
    		System.out.println("g length: " + g.length());
    		if(g.length()!=0){
    			if(g!=null || g!="" || g!="null"){
    				if(g.contains("null")){
    					miscList2 = "Error";
    				}else{
    					miscList = miscList + "$" + g;
    				}
    			}else{
    				miscList2 = "Error";
    			}

    			System.out.println("miscList G: " + miscList);
    		}else{
    			System.out.println("KABOOM");
    			miscList2 = "Error";
    		}
    	}
    	System.out.println("miscList2 :" + miscList2);
    	if(miscList2==""){
    		miscList = miscList+"$";
    	}else{
    		miscList = miscList2;
    	}

    	System.out.println("miscList :" + miscList);
    	return (miscList);
    }


    private double calculateIliNetAmount(ArModInvoiceLineItemDetails mdetails, double tcRate, String tcType, short precisionUnit) {

        double amount = 0d;

        if (tcType.equals("INCLUSIVE")) {

            amount = EJBCommon.roundIt(mdetails.getIliAmount() / (1 + (tcRate / 100)), precisionUnit);

        } else {

            // tax exclusive, none, zero rated or exempt

            amount = mdetails.getIliAmount();

        }

        return amount;

    }

    private double calculateIliTaxAmount(ArModInvoiceLineItemDetails mdetails, double tcRate, String tcType, double amount, short precisionUnit) {

        double taxAmount = 0d;

        if (!tcType.equals("NONE") &&
                !tcType.equals("EXEMPT")) {


            if (tcType.equals("INCLUSIVE")) {

                taxAmount = EJBCommon.roundIt(mdetails.getIliAmount() - amount, precisionUnit);

            } else if (tcType.equals("EXCLUSIVE")) {

                taxAmount = EJBCommon.roundIt(mdetails.getIliAmount() * tcRate / 100, precisionUnit);

            } else {

                // tax none zero-rated or exempt

            }

        }

        return taxAmount;

    }

    private void addArDrIliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalBranchAccountNumberInvalidException {

        Debug.print("ArInvoiceEntryControllerBean addArDrIliEntry");

        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            // get company

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

            // create distribution record

            LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
                    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
                    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

            //arInvoice.addArDistributionRecord(arDistributionRecord);
            arDistributionRecord.setArInvoice(arInvoice);
            //glChartOfAccount.addArDistributionRecord(arDistributionRecord);
            arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

        } catch (FinderException ex) {

            throw new GlobalBranchAccountNumberInvalidException();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private LocalArSalesOrderInvoiceLine addArSolEntry(ArModSalesOrderLineDetails mdetails, LocalArInvoice arInvoice, Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean addArSolEntry");

        LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalArSalesOrderLineHome arSalesOrderLineHome = null;
        LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;

        // Initialize EJB Home

        try {

            arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            arSalesOrderLineHome = (LocalArSalesOrderLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
            arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

            double ILI_AMNT = 0d;
            double ILI_TAX_AMNT = 0d;

            // calculate net amount

            LocalArTaxCode arTaxCode = arInvoice.getArTaxCode();

            if (arTaxCode.getTcType().equals("INCLUSIVE")) {

                ILI_AMNT = EJBCommon.roundIt(mdetails.getSolAmount() / (1 + (arTaxCode.getTcRate() / 100)), precisionUnit);

            } else {

                // tax exclusive, none, zero rated or exempt

                ILI_AMNT = mdetails.getSolAmount();

            }

            // calculate tax

            if (!arTaxCode.getTcType().equals("NONE") &&
                    !arTaxCode.getTcType().equals("EXEMPT")) {


                if (arTaxCode.getTcType().equals("INCLUSIVE")) {

                    ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getSolAmount() - ILI_AMNT, precisionUnit);

                } else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

                    ILI_TAX_AMNT = EJBCommon.roundIt(mdetails.getSolAmount() * arTaxCode.getTcRate() / 100, precisionUnit);

                } else {

                    // tax none zero-rated or exempt

                }

            }

            LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = arSalesOrderInvoiceLineHome.create(
                    mdetails.getSolQuantityDelivered(), ILI_AMNT, ILI_TAX_AMNT, mdetails.getSolDiscount1(),
                    mdetails.getSolDiscount2(), mdetails.getSolDiscount3(), mdetails.getSolDiscount4(),
                    mdetails.getSolTotalDiscount(), mdetails.getSolMisc(), EJBCommon.TRUE, AD_CMPNY);

          //  arInvoice.addArSalesOrderInvoiceLine(arSalesOrderInvoiceLine);

            arSalesOrderInvoiceLine.setArInvoice(arInvoice);

            LocalArSalesOrderLine arSalesOrderLine = arSalesOrderLineHome.findByPrimaryKey(mdetails.getSolCode());
            //arSalesOrderLine.addArSalesOrderInvoiceLine(arSalesOrderInvoiceLine);

            arSalesOrderInvoiceLine.setArSalesOrderLine(arSalesOrderLine);
            return arSalesOrderInvoiceLine;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }


    private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
            LocalGlChartOfAccount glChartOfAccount,
            boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean postToGl");

        LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
        LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            LocalGlChartOfAccountBalance glChartOfAccountBalance =
                glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
                        glAccountingCalendarValue.getAcvCode(),
                        glChartOfAccount.getCoaCode(), AD_CMPNY);

            String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
            short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



            if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
                    isDebit == EJBCommon.TRUE) ||
                    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
                            isDebit == EJBCommon.FALSE)) {

                glChartOfAccountBalance.setCoabEndingBalance(
                        EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

                if (!isCurrentAcv) {

                    glChartOfAccountBalance.setCoabBeginningBalance(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

                }


            } else {

                glChartOfAccountBalance.setCoabEndingBalance(
                        EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

                if (!isCurrentAcv) {

                    glChartOfAccountBalance.setCoabBeginningBalance(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

                }

            }

            if (isCurrentAcv) {

                if (isDebit == EJBCommon.TRUE) {

                    glChartOfAccountBalance.setCoabTotalDebit(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

                } else {

                    glChartOfAccountBalance.setCoabTotalCredit(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
                }

            }

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }


    }

    private void postToInv(LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_QTY_SLD, double CST_CST_OF_SLS,
    		double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException,
			GlobalExpiryDateNotFoundException {

        Debug.print("ArInvoiceEntryControllerBean postToInv");

        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

        // Initialize EJB Home

        try {

            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
            int CST_LN_NMBR = 0;

            CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
            CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

            if (CST_QTY_SLD > 0 && arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

            	invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - CST_QTY_SLD);

            }

            try {

                // generate line number

                LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

            } catch (FinderException ex) {

                CST_LN_NMBR = 1;

            }

            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	            //void subsequent cost variance adjustments
	            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
	            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
	            Iterator i = invAdjustmentLines.iterator();

	            while (i.hasNext()){

	            	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
	            	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

	            }
            }

            String prevExpiryDates = "";
            String miscListPrpgt ="";
            double qtyPrpgt = 0;
            double qtyPrpgt2 = 0;
            try {
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   Debug.print("ArInvoicePostControllerBean postToInv C");
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();

        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }

           }catch (Exception ex){
        	   System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
        	   prevExpiryDates="";
           }

            // create costing
            LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
            //invItemLocation.addInvCosting(invCosting);
            invCosting.setInvItemLocation(invItemLocation);
            invCosting.setArInvoiceLineItem(arInvoiceLineItem);

//          Get Latest Expiry Dates
            String check="";
            if(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
            	if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
            		if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
            			int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));

            			String miscList2Prpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt, "False");
            			ArrayList miscList = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty2Prpgt);
            			String propagateMiscPrpgt = "";
            			String ret = "";

            			System.out.println("CST_ST_QTY Before Trans: " + CST_QTY_SLD);
            			//ArrayList miscList2 = null;
            			if(CST_QTY_SLD<0){
            				prevExpiryDates = prevExpiryDates.substring(1);
            				propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
            			}else{
            				Iterator mi = miscList.iterator();
            				propagateMiscPrpgt = prevExpiryDates;
            				ret = propagateMiscPrpgt;
            				String Checker = "";
            				while(mi.hasNext()){
            					String miscStr = (String)mi.next();

            					Integer qTest = this.checkExpiryDates(ret+"fin$");
            					ArrayList miscList2 = this.expiryDates("$"+ret, Double.parseDouble(qTest.toString()));
            					Iterator m2 = miscList2.iterator();
            					ret = "";
            					String ret2 = "false";
            					int a = 0;

            					while(m2.hasNext()){
            						String miscStr2 = (String)m2.next();

            						if(ret2=="1st"){
            							ret2 = "false";
            						}

            						System.out.println("miscStr2: " + miscStr2);
            						System.out.println("miscStr: " + miscStr);
            						if(miscStr2.trim().equals(miscStr.trim())){
            							if(a==0){
            								a = 1;
            								ret2 = "1st";
            								Checker = "true";
            							}else{
            								a = a+1;
            								ret2 = "true";
            							}
            						}

            						if(!miscStr2.trim().equals(miscStr.trim()) || a>1){
            							if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
            								if (miscStr2!=""){
            									miscStr2 = "$" + miscStr2.trim();
            									ret = ret + miscStr2;
            									qtyPrpgt2++;
            									ret2 = "false";
            								}
            							}
            						}

            					}
            					ret = ret + "$";
            					System.out.println("qtyPrpgt: " + qtyPrpgt);
            					if(qtyPrpgt2==0){
            						qtyPrpgt2 = qtyPrpgt;
            					}
            					qtyPrpgt= qtyPrpgt -1;
            				}
            				//propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
            				propagateMiscPrpgt = ret;
            				System.out.println("propagateMiscPrpgt: " + propagateMiscPrpgt);
            				//propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
            				if(Checker.equals("true")){
            					//invCosting.setCstExpiryDate(ret);
            					System.out.println("check: " + check);
            				}else{
            					System.out.println("exA");
            					throw new GlobalExpiryDateNotFoundException(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());

            				}

            			}
            			invCosting.setCstExpiryDate(propagateMiscPrpgt);

            		}else{
            			invCosting.setCstExpiryDate(prevExpiryDates);
            		}
            	}else{
            		if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
            			int initialQty = Integer.parseInt(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
            			String initialPrpgt = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), initialQty, "False");

            			invCosting.setCstExpiryDate(initialPrpgt);
            		}else{
            			invCosting.setCstExpiryDate(prevExpiryDates);
            			System.out.println("prevExpiryDates");
            		}

            	}
            }



			// if cost variance is not 0, generate cost variance for the transaction
			if(CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"ARCR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
						arInvoiceLineItem.getArReceipt().getRctDescription(), arInvoiceLineItem.getArReceipt().getRctDate(),
						USR_NM, AD_BRNCH, AD_CMPNY);

			}

            // propagate balance if necessary
            Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            Iterator i = invCostings.iterator();
            String miscList ="";
            ArrayList miscList2 = null;


            System.out.println("miscList Propagate:" + miscList);
            String propagateMisc ="";
 		   	String ret = "";

            System.out.println("CST_ST_QTY: " + CST_QTY_SLD);
            while (i.hasNext()) {
            	String Checker = "";
            	String Checker2 = "";

            	LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

            	invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() - CST_QTY_SLD);
            	invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() - CST_CST_OF_SLS);

            	if(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
            		if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
            			System.out.println("BAGO ANG MALI: " + arInvoiceLineItem.getIliMisc());

            			double qty = Double.parseDouble(this.getQuantityExpiryDates(arInvoiceLineItem.getIliMisc()));
            			//double qty2 = this.checkExpiryDates2(arInvoiceLineItem.getIliMisc());
            			miscList = this.propagateExpiryDates(arInvoiceLineItem.getIliMisc(), qty, "False");
            			miscList2 = this.expiryDates(arInvoiceLineItem.getIliMisc(), qty);

            			System.out.println("invAdjustmentLine.getAlMisc(): "+arInvoiceLineItem.getIliMisc());
            			System.out.println("getAlAdjustQuantity(): "+arInvoiceLineItem.getIliQuantity());

            			if(arInvoiceLineItem.getIliQuantity()<0){
            				Iterator mi = miscList2.iterator();

            				propagateMisc = invPropagatedCosting.getCstExpiryDate();
            				ret = invPropagatedCosting.getCstExpiryDate();
            				while(mi.hasNext()){
            					String miscStr = (String)mi.next();

            					Integer qTest = this.checkExpiryDates(ret+"fin$");
            					ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

            					// ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
            					System.out.println("ret: " + ret);
            					Iterator m2 = miscList3.iterator();
            					ret = "";
            					String ret2 = "false";
            					int a = 0;
            					while(m2.hasNext()){
            						String miscStr2 = (String)m2.next();

            						if(ret2=="1st"){
            							ret2 = "false";
            						}
            						System.out.println("2 miscStr: "+miscStr);
            						System.out.println("2 miscStr2: "+miscStr2);
            						if(miscStr2.equals(miscStr)){
            							if(a==0){
            								a = 1;
            								ret2 = "1st";
            								Checker2 = "true";
            							}else{
            								a = a+1;
            								ret2 = "true";
            							}
            						}
            						System.out.println("Checker: "+Checker2);
            						if(!miscStr2.equals(miscStr) || a>1){
            							if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
            								if (miscStr2!=""){
            									miscStr2 = "$" + miscStr2;
            									ret = ret + miscStr2;
            									ret2 = "false";
            								}
            							}
            						}

            					}
            					if(Checker2!="true"){
            						throw new GlobalExpiryDateNotFoundException(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
            					}else{
            						System.out.println("TAE");
            					}

            					ret = ret + "$";
            					qtyPrpgt= qtyPrpgt -1;
            				}
            			}
            		}


            		if(arInvoiceLineItem.getIliMisc()!=null && arInvoiceLineItem.getIliMisc()!="" && arInvoiceLineItem.getIliMisc().length()!=0){
            			if(CST_QTY_SLD<0){
            				//miscList = miscList.substring(1);
            				//propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
            				propagateMisc = miscList+invPropagatedCosting.getCstExpiryDate().substring(1, invPropagatedCosting.getCstExpiryDate().length());
            				System.out.println("propagateMiscPrpgt : "+propagateMisc);

            			}else{
            				Iterator mi = miscList2.iterator();

            				propagateMisc = prevExpiryDates;
            				ret = propagateMisc;
            				while(mi.hasNext()){
            					String miscStr = (String)mi.next();
            					System.out.println("ret123: " + ret);
            					System.out.println("qtyPrpgt123: " + qtyPrpgt);
            					System.out.println("qtyPrpgt2: "+qtyPrpgt2);
            					if(qtyPrpgt<=0){
            						qtyPrpgt = qtyPrpgt2;
            					}

            					Integer qTest = this.checkExpiryDates(ret+"fin$");
            					ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));
            					Iterator m2 = miscList3.iterator();
            					ret = "";
            					String ret2 = "false";
            					int a = 0;
            					while(m2.hasNext()){
            						String miscStr2 = (String)m2.next();

            						if(ret2=="1st"){
            							ret2 = "false";
            						}

            						System.out.println("miscStr2: " + miscStr2);
            						System.out.println("miscStr: " + miscStr);
            						if(miscStr2.trim().equals(miscStr.trim())){
            							if(a==0){
            								a = 1;
            								ret2 = "1st";
            								Checker = "true";
            							}else{
            								a = a+1;
            								ret2 = "true";
            							}
            						}

            						if(!miscStr2.trim().equals(miscStr.trim()) || a>1){
            							if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
            								if (miscStr2!=""){
            									miscStr2 = "$" + miscStr2.trim();
            									ret = ret + miscStr2;
            									ret2 = "false";
            								}
            							}
            						}

            					}
            					ret = ret + "$";
            					qtyPrpgt= qtyPrpgt -1;
            				}
            				propagateMisc = ret;
            				System.out.println("propagateMiscPrpgt: " + propagateMisc);

            				if(Checker=="true"){
            					//invPropagatedCosting.setCstExpiryDate(propagateMisc);
            					System.out.println("Yes");
            				}else{
            					System.out.println("ex1");
            					//throw new GlobalExpiryDateNotFoundException(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());

            				}
            			}

            			invPropagatedCosting.setCstExpiryDate(propagateMisc);
            		}else{
            			invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
            		}
            	}


            }

            // regenerate cost variance
            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
            	this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
            }

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

        	throw ex;

        } catch (GlobalExpiryDateNotFoundException ex){
        	throw ex;
        }catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    public static int checkExpiryDates(String misc) throws Exception {

		String separator ="$";

		// Remove first $ character
		misc = misc.substring(1);
		//System.out.println("misc: " + misc);
		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;
		int numberExpry=0;
		String miscList = new String();
		String miscList2 = "";
		String g= "";
		try{
			while(g!="fin"){
				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				g= misc.substring(start, start + length);
				if(g.length()!=0){
					if(g!=null || g!="" || g!="null"){
						if(g.contains("null")){
							miscList2 = "Error";
						}else{
							miscList = miscList + "$" + g;
							numberExpry++;
						}
					}else{
						miscList2 = "Error";
					}

				}else{
					miscList2 = "Error";
				}
			}
		}catch(Exception e){

		}

		if(miscList2==""){
			miscList = miscList+"$";
		}else{
			miscList = miscList2;
		}

		return (numberExpry);
	}

    public String propagateExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	String miscList = new String();
    	try{
    		String separator = "";
        	if(reverse=="False"){
        		separator ="$";
        	}else{
        		separator =" ";
        	}

        	// Remove first $ character
        	misc = misc.substring(1);
        	System.out.println("misc: " + misc);
        	// Counter
        	int start = 0;
        	int nextIndex = misc.indexOf(separator, start);
        	int length = nextIndex - start;



     		for(int x=0; x<qty; x++) {

     			// Date
     			start = nextIndex + 1;
     			nextIndex = misc.indexOf(separator, start);
     			length = nextIndex - start;
     			String g= misc.substring(start, start + length);
     			System.out.println("g: " + g);
     			System.out.println("g length: " + g.length());
     				if(g.length()!=0){
     					miscList = miscList + "$" + g;
     					System.out.println("miscList G: " + miscList);
     				}
     		}

     		miscList = miscList+"$";
    	}catch(Exception e){
    		miscList="";
    	}

 		System.out.println("miscList :" + miscList);
 		return (miscList);
    }

    private ArrayList expiryDates(String misc, double qty) throws Exception{
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	ArrayList miscList = new ArrayList();
    	try{
    		System.out.println("misc: " + misc);
    		String separator ="$";


    		// Remove first $ character
    		misc = misc.substring(1);

    		// Counter
    		int start = 0;
    		int nextIndex = misc.indexOf(separator, start);
    		int length = nextIndex - start;

    		System.out.println("qty" + qty);


    		for(int x=0; x<qty; x++) {

    			// Date
    			start = nextIndex + 1;
    			nextIndex = misc.indexOf(separator, start);
    			length = nextIndex - start;
    			System.out.println("x"+x);
    			String checker = misc.substring(start, start + length);
    			System.out.println("checker"+checker);
    			if(checker.length()!=0 || checker!="null"){
    				miscList.add(checker);
    			}else{
    				miscList.add("null");
    			}
    		}
    	}catch(Exception e){

    		//miscList = "";
    	}



    	System.out.println("miscList :" + miscList);
    	return miscList;
    }

    public String getQuantityExpiryDates(String qntty){
    	String separator = "$";
    	String y="";
    	try{
//  		Remove first $ character
    		qntty = qntty.substring(1);

    		// Counter
    		int start = 0;
    		int nextIndex = qntty.indexOf(separator, start);
    		int length = nextIndex - start;

    		y = (qntty.substring(start, start + length));
    		System.out.println("Y " + y);
    	}catch(Exception e){
    		y="0";
    	}


    	return y;
    }

    public String propagateExpiryDates(String misc, double qty) throws Exception {
    	//ActionErrors errors = new ActionErrors();

    	Debug.print("ApReceivingItemControllerBean getExpiryDates");

    	String separator = "$";
    	String miscList = new String();
    	// Remove first $ character
    	try{
    		misc = misc.substring(1);

    		// Counter
    		int start = 0;
    		int nextIndex = misc.indexOf(separator, start);
    		int length = nextIndex - start;

    		System.out.println("qty" + qty);


    		for(int x=0; x<qty; x++) {

    			// Date
    			start = nextIndex + 1;
    			nextIndex = misc.indexOf(separator, start);
    			length = nextIndex - start;
    			/*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	        sdf.setLenient(false);*/
    			try {

    				miscList = miscList + "$" +(misc.substring(start, start + length));
    			} catch (Exception ex) {

    				throw ex;
    			}


    		}
    		miscList = miscList+"$";
    	}catch(Exception e){

    		miscList = "";
    	}



    	System.out.println("miscList :" + miscList);
    	return (miscList);
    }

    private void postToBua(LocalArInvoiceLineItem arInvoiceLineItem, Date CST_DT, double CST_ASSMBLY_QTY, double CST_ASSMBLY_CST,
    		double CST_RMNNG_QTY, double CST_RMNNG_VL, String II_NM, String LOC_NM, double CST_VRNC_VL, String USR_NM,
			Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {

        Debug.print("ArInvoiceEntryControllerBean postToBua");

        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

        // Initialize EJB Home

        try {

            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);
            int CST_LN_NMBR = 0;

            CST_ASSMBLY_QTY = EJBCommon.roundIt(CST_ASSMBLY_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_ASSMBLY_CST = EJBCommon.roundIt(CST_ASSMBLY_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
            CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

            if ((CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 0) ||
               		(CST_ASSMBLY_QTY < 0 && arInvoiceLineItem.getIliEnableAutoBuild() == 1 && !arInvoiceLineItem.getInvItemLocation().getInvItem().equals(invItemLocation.getInvItem()))) {

                invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ASSMBLY_QTY));

            }

            try {

                // generate line number

                LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

            } catch (FinderException ex) {

                CST_LN_NMBR = 1;

            }

            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	            //void subsequent cost variance adjustments
	            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
	            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
	            Iterator i = invAdjustmentLines.iterator();

	            while (i.hasNext()){

	            	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
	            	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

	            }
            }

            // create costing
            LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, CST_ASSMBLY_QTY, CST_ASSMBLY_CST, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ASSMBLY_QTY > 0 ? CST_ASSMBLY_QTY : 0, AD_BRNCH, AD_CMPNY);
            //invItemLocation.addInvCosting(invCosting);
            invCosting.setInvItemLocation(invItemLocation);
            invCosting.setArInvoiceLineItem(arInvoiceLineItem);

			// if cost variance is not 0, generate cost variance for the transaction
			if(CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"ARCR" + arInvoiceLineItem.getArReceipt().getRctNumber(),
						arInvoiceLineItem.getArReceipt().getRctDescription(), arInvoiceLineItem.getArReceipt().getRctDate(),
						USR_NM, AD_BRNCH, AD_CMPNY);

			}

            // propagate balance if necessary
            Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            Iterator i = invCostings.iterator();

            while (i.hasNext()) {

                LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

                invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ASSMBLY_QTY);
                invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ASSMBLY_CST);

            }

            // regenerate cost variance
            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
            	this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
            }

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

        	throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private void postToInvSo(LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine, Date CST_DT, double CST_QTY_SLD,
    		double CST_CST_OF_SLS, double CST_RMNNG_QTY, double CST_RMNNG_VL,  double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH,
			Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {

        Debug.print("ArInvoiceEntryControllerBean postToInvSo");

        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

        // Initialize EJB Home

        try {

            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            LocalArSalesOrderLine arSalesOrderLine = arSalesOrderInvoiceLine.getArSalesOrderLine();
            LocalInvItemLocation invItemLocation = arSalesOrderLine.getInvItemLocation();
            int CST_LN_NMBR = 0;

            CST_QTY_SLD = EJBCommon.roundIt(CST_QTY_SLD, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, adCompany.getGlFunctionalCurrency().getFcPrecision());
            CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

            try {

                // generate line number

                LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

            } catch (FinderException ex) {

                CST_LN_NMBR = 1;

            }

            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	            //void subsequent cost variance adjustments
	            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
	            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
	            Iterator i = invAdjustmentLines.iterator();

	            while (i.hasNext()){

	            	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
	            	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

	            }
            }

            // create costing
            LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, 0d, 0d, CST_QTY_SLD, CST_CST_OF_SLS, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
            //invItemLocation.addInvCosting(invCosting);
            invCosting.setInvItemLocation(invItemLocation);
            invCosting.setArSalesOrderInvoiceLine(arSalesOrderInvoiceLine);

	          // if cost variance is not 0, generate cost variance for the transaction
	          if(CST_VRNC_VL != 0 && adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {

	          	this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
	          			"ARCM" + arSalesOrderInvoiceLine.getArInvoice().getInvNumber(),
	          			arSalesOrderInvoiceLine.getArInvoice().getInvDescription(),
	          			arSalesOrderInvoiceLine.getArInvoice().getInvDate(), USR_NM, AD_BRNCH, AD_CMPNY);

	          }

            // propagate balance if necessary
            Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            Iterator i = invCostings.iterator();

            while (i.hasNext()) {

                LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

                invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_SLD);
                invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_CST_OF_SLS);

            }

            // regenerate cost varaince
            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
            	this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
            }

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }


    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_SLD, Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean convertByUomFromAndItemAndQuantity");

        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	Debug.print("ArInvoiceEntryControllerBean convertByUomFromAndItemAndQuantity A");
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            Debug.print("ArInvoiceEntryControllerBean convertByUomFromAndItemAndQuantity B");
            return EJBCommon.roundIt(QTY_SLD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());


        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private double computeTotalBalance(Integer invoiceCode, String CST_CSTMR_CODE, Integer AD_CMPNY) {

        LocalArCustomerBalanceHome arCustomerBalanceHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArReceiptHome arReceiptHome = null;
        LocalArPdcHome arPdcHome = null;

        try {

            arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
            lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
            arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
            lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            arPdcHome = (LocalArPdcHome)EJBHomeFactory.
            lookUpLocalHome(LocalArPdcHome.JNDI_NAME, LocalArPdcHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        double customerBalance = 0;

        try {

            //get latest balance

            Collection arCustomerBalances = arCustomerBalanceHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

            if(!arCustomerBalances.isEmpty()) {

                ArrayList customerBalanceList = new ArrayList(arCustomerBalances);

                customerBalance = ((LocalArCustomerBalance) customerBalanceList.get(customerBalanceList.size() - 1)).getCbBalance();

            }

            //get amount of unposted invoices/credit memos

            Collection arInvoices = arInvoiceHome.findUnpostedInvByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

            Iterator arInvIter = arInvoices.iterator();

            while(arInvIter.hasNext()) {

                LocalArInvoice mdetails = (LocalArInvoice)arInvIter.next();

                if(!mdetails.getInvCode().equals(invoiceCode)) {

                    if(mdetails.getInvCreditMemo() == EJBCommon.TRUE) {

                        customerBalance = customerBalance - mdetails.getInvAmountDue();

                    } else {

                        customerBalance = customerBalance + (mdetails.getInvAmountDue() - mdetails.getInvAmountPaid());

                    }

                }

            }

            //get amount of unposted receipts

            Collection arReceipts = arReceiptHome.findUnpostedRctByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

            Iterator arRctIter = arReceipts.iterator();

            while(arRctIter.hasNext()) {

                LocalArReceipt arReceipt = (LocalArReceipt)arRctIter.next();

                customerBalance = customerBalance - arReceipt.getRctAmount();

            }

            // get amount of pdc (unposted or posted) type PR findPdcByPdcType("PR", AD_CMPNY)

            Collection arPdcs = arPdcHome.findPdcByPdcType(AD_CMPNY);

            Iterator arPdcIter = arPdcs.iterator();

            while(arPdcIter.hasNext()) {

            	LocalArPdc arPdc = (LocalArPdc)arPdcIter.next();

            	customerBalance = customerBalance - arPdc.getPdcAmount();

            }

        } catch (FinderException ex) {

        }

        return customerBalance;

    }

    private void regenerateInventoryDr(LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalInventoryDateException, GlobalBranchAccountNumberInvalidException {

        Debug.print("ArInvoiceEntryControllerBean regenerateInventoryDr");

        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;

        // Initialize EJB Home

        try {

            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            // regenerate inventory distribution records

            Collection arDistributionRecords = arDistributionRecordHome.findImportableDrByInvCode(arInvoice.getInvCode(), AD_CMPNY);

            Iterator i = arDistributionRecords.iterator();

            while (i.hasNext()) {

                LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

                if(arDistributionRecord.getDrClass().equals("COGS") || arDistributionRecord.getDrClass().equals("INVENTORY")){

                    i.remove();
                    arDistributionRecord.remove();

                }

            }

            Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();
            Collection arSalesOrderInvoiceLines = arInvoice.getArSalesOrderInvoiceLines();

            if(arInvoiceLineItems != null && !arInvoiceLineItems.isEmpty()) {

                i = arInvoiceLineItems.iterator();

                while(i.hasNext()) {

                    LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();
                    LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();

                    // start date validation

                    if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    			arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
                    			invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                    	if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
                    }

                    // add cost of sales distribution and inventory

                    double COST = 0d;

                    try {

                        LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                        if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

                        	COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                        else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

                        	COST = this.getInvFifoCost(arInvoice.getInvDate(), invItemLocation.getIlCode(), arInvoiceLineItem.getIliQuantity(),
                        			arInvoiceLineItem.getIliUnitPrice(), false, AD_BRNCH, AD_CMPNY);

                        else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))

                        	COST = invItemLocation.getInvItem().getIiUnitCost();

                    } catch (FinderException ex) {

                        COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

                    }

                    double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
                            arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

                    LocalAdBranchItemLocation adBranchItemLocation = null;

                    try {

                        adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

                    } catch(FinderException ex) {

                    }

                    if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.FALSE) {

                        if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                            if(adBranchItemLocation != null) {

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "COGS", EJBCommon.TRUE, COST * QTY_SLD,
                                        adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
                                        adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                            } else {

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "COGS", EJBCommon.TRUE, COST * QTY_SLD,
                                        arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
                                        arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                            }

                        } else {

                            if(adBranchItemLocation != null) {

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "COGS", EJBCommon.FALSE, COST * QTY_SLD,
                                        adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "INVENTORY", EJBCommon.TRUE, COST * QTY_SLD,
                                        adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                            } else {

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "COGS", EJBCommon.FALSE, COST * QTY_SLD,
                                        arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "INVENTORY", EJBCommon.TRUE, COST * QTY_SLD,
                                        arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                            }

                        }

                    }

                    if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

                        byte DEBIT = EJBCommon.TRUE;

                        double TOTAL_AMOUNT = 0;
                        double ABS_TOTAL_AMOUNT = 0;

                        LocalGlChartOfAccount glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                                arInvoiceLineItem.getInvItemLocation().getIlGlCoaInventoryAccount());

                        Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

                        Iterator j = invBillOfMaterials.iterator();

                        while (j.hasNext()) {

                            LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

                            // add bill of material quantity needed to item location

                            LocalInvItemLocation invIlRawMaterial = null;

                            try {

                                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                                        invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

                            } catch(FinderException ex) {

                                throw new GlobalInvItemLocationNotFoundException(String.valueOf(arInvoiceLineItem.getIliLine()) +
                                        " - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");

                            }

                            // bom conversion
                            double quantityNeeded = this.convertByUomFromAndItemAndQuantity(
                                    invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                                    EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() *  QTY_SLD,
                                    this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);

                            // start date validation
                            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	                            Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	                                    arInvoice.getInvDate(), invIlRawMaterial.getInvItem().getIiName(),
	                                    invIlRawMaterial.getInvLocation().getLocName(),	AD_BRNCH, AD_CMPNY);
	                            if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
	                                    invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
                            }

                            LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

                            double COSTING = 0d;

                            try {

                                LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                        arInvoice.getInvDate(), invItem.getIiName(), invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);

                                COSTING = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                            } catch (FinderException ex) {

                                COSTING = invItem.getIiUnitCost();

                            }

                            // bom conversion
                            COSTING = this.convertCostByUom(invIlRawMaterial.getInvItem().getIiName(),
                                    invBillOfMaterial.getInvUnitOfMeasure().getUomName(), COSTING, true, AD_CMPNY);

                            double BOM_AMOUNT = EJBCommon.roundIt(
                                    (QTY_SLD * (invBillOfMaterial.getBomQuantityNeeded() * COSTING)),
                                    this.getGlFcPrecisionUnit(AD_CMPNY));

                            TOTAL_AMOUNT += BOM_AMOUNT;
                            ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

                            // add inventory account

                            LocalAdBranchItemLocation adBilRawMaterial = null;

                            try {

                                adBilRawMaterial = adBranchItemLocationHome.findBilByIlCodeAndBrCode(invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);

                                if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                                    this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(BOM_AMOUNT),
                                            adBilRawMaterial.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                                } else {

                                    this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE, Math.abs(BOM_AMOUNT),
                                            adBilRawMaterial.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                                }

                            } catch(FinderException ex) {

                                if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {

                                    this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(BOM_AMOUNT),
                                            invIlRawMaterial.getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                                } else {

                                    this.addArDrIliEntry(arInvoice.getArDrNextLine(), "INVENTORY", EJBCommon.TRUE, Math.abs(BOM_AMOUNT),
                                            invIlRawMaterial.getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                                }

                            }

                        }

                        // add cost of sales account

                        if(arInvoice.getInvCreditMemo() == EJBCommon.FALSE) {
                            if(adBranchItemLocation != null) {

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "COGS", EJBCommon.TRUE, Math.abs(TOTAL_AMOUNT),
                                        arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                            } else {

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "COGS", EJBCommon.TRUE, Math.abs(TOTAL_AMOUNT),
                                        arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                            }


                        } else {

                            if(adBranchItemLocation != null) {

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "COGS", EJBCommon.FALSE, Math.abs(TOTAL_AMOUNT),
                                        adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                            } else {

                                this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                        "COGS", EJBCommon.FALSE, Math.abs(TOTAL_AMOUNT),
                                        arInvoiceLineItem.getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                            }

                        }

                    }

                }

            } else if (arSalesOrderInvoiceLines != null && !arSalesOrderInvoiceLines.isEmpty()) {

                i = arSalesOrderInvoiceLines.iterator();

                while (i.hasNext()) {

                    LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)i.next();
                    LocalInvItemLocation invItemLocation = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation();

                    // start date validation
                    if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	                    Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	                            arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(),
	                            invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	                    if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
                    }


                    // add cost of sales distribution and inventory

                    double COST = 0d;

                    try {

                        LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                arInvoice.getInvDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                        if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

                        	COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                        else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

                        	COST = this.getInvFifoCost(arInvoice.getInvDate(), invItemLocation.getIlCode(), arSalesOrderInvoiceLine.getSilQuantityDelivered(),
                        			arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice(), false, AD_BRNCH, AD_CMPNY);

                        else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))

                        	COST = invItemLocation.getInvItem().getIiUnitCost();

                    } catch (FinderException ex) {

                        COST = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiUnitCost();

                    }

                    double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure(),
                            invItemLocation.getInvItem(), arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);

                    LocalAdBranchItemLocation adBranchItemLocation = null;

                    try {

                        adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

                        this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                "COGS", EJBCommon.TRUE, COST * QTY_SLD,
                                adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                        this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                "INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
                                adBranchItemLocation.getBilCoaGlInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                    } catch(FinderException ex) {

                        this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                "COGS", EJBCommon.TRUE, COST * QTY_SLD,
                                arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlGlCoaCostOfSalesAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                        this.addArDrIliEntry(arInvoice.getArDrNextLine(),
                                "INVENTORY", EJBCommon.FALSE, COST * QTY_SLD,
                                arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlGlCoaInventoryAccount(), arInvoice, AD_BRNCH, AD_CMPNY);

                    }

                    // add quantity to item location committed quantity

                    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure(), arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem(), arSalesOrderInvoiceLine.getSilQuantityDelivered(), AD_CMPNY);
                    invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

                }

            }

        } catch (GlobalInventoryDateException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean convertCostByUom");

        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            if (isFromDefault) {

                return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();

            } else {

                return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();

            }

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

        Debug.print("ArInvoicePostControllerBean convertByUomFromAndUomToAndQuantity");

        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ArMiscReceiptEntryControllerBean voidInvAdjustment");

    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();

    			Iterator i = invDistributionRecords.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				list.add(invDistributionRecord);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

    			}

    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				list.add(invAdjustmentLine);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}

    			invAdjustment.setAdjVoid(EJBCommon.TRUE);

    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH,
			Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArMiscReceiptEntryControllerBean generateCostVariance");

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;

    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		try{

    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);

    		} catch (FinderException ex) {

    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		}

    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment, CST_VRNC_VL,
    				EJBCommon.FALSE, AD_CMPNY);

    		// check for branch mapping

    		LocalAdBranchItemLocation adBranchItemLocation = null;

    		try{

    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		LocalGlChartOfAccount glInventoryChartOfAccount = null;

    		if (adBranchItemLocation == null) {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());

    		}


    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;

    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE",
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance");

    	try {

    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			if(prevInvCosting.getCstRemainingQuantity() < 0) {

    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance A");

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);

						Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance B");

    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();

    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);

						Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance C");

    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();

    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();

    					}

    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();

   						Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance D");

   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){

   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   							ADJ_RFRNC_NMBR = "ARCM" +
							invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();

   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   							ADJ_RFRNC_NMBR = "ARMR" +
							invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   						}

    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance E");

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" +
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();

    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();

    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {

    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);

    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();

    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);

    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}

    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" +
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();

    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" +
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);

						Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance F");

    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
						Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance G");
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();

    				} else {

    					prevInvCosting = invPropagatedCosting;
    					continue;

    				}

    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;

    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());

    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);

    			}

    			// set previous costing
    			prevInvCosting = invPropagatedCosting;

    		}

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}     */
    }

    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)

    throws GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArMiscReceiptEntryControllerBean addInvDrEntry");

    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;

    	// Initialize EJB Home

    	try {

    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// get company

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		// validate coa

    		LocalGlChartOfAccount glChartOfAccount = null;

    		try {

    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		} catch(FinderException ex) {

    			throw new GlobalBranchAccountNumberInvalidException ();

    		}

    		// create distribution record

    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					 AD_CMPNY);

    		//invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvAdjustment(invAdjustment);
    		//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

    	} catch(GlobalBranchAccountNumberInvalidException ex) {

    		throw new GlobalBranchAccountNumberInvalidException ();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {

    	Debug.print("ArMiscReceiptEntryControllerBean executeInvAdjPost");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;


    	// Initialize EJB Home

    	try {

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// validate if adjustment is already deleted

    		LocalInvAdjustment invAdjustment = null;

    		try {

    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

    		} catch (FinderException ex) {

    			throw new GlobalRecordAlreadyDeletedException();

    		}

    		// validate if adjustment is already posted or void

    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();

    		}

    		Collection invAdjustmentLines = null;

    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

    		Iterator i = invAdjustmentLines.iterator();

    		while(i.hasNext()) {


    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);

    		}

    		// post to gl if necessary

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    			// validate if date has no period and period is closed

    			LocalGlSetOfBook glJournalSetOfBook = null;

    			try {

    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

    			} catch (FinderException ex) {

    				throw new GlJREffectiveDateNoPeriodExistException();

    			}

    			LocalGlAccountingCalendarValue glAccountingCalendarValue =
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);


    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

    				throw new GlJREffectiveDatePeriodClosedException();

    			}

    			// check if invoice is balance if not check suspense posting

    			LocalGlJournalLine glOffsetJournalLine = null;

    			Collection invDistributionRecords = null;

    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			} else {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			}


    			Iterator j = invDistributionRecords.iterator();

    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    					TOTAL_DEBIT += DR_AMNT;

    				} else {

    					TOTAL_CREDIT += DR_AMNT;

    				}

    			}

    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				LocalGlSuspenseAccount glSuspenseAccount = null;

    				try {

    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);

    				} catch (FinderException ex) {

    					throw new GlobalJournalNotBalanceException();

    				}

    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE,
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

    				} else {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

    				}

    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				throw new GlobalJournalNotBalanceException();

    			}

    			// create journal batch if necessary

    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

    			try {

    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

    			} catch (FinderException ex) {

    			}

    			if (glJournalBatch == null) {

    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);

    			}

    			// create journal entry

    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);

    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);

    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);

    			if (glJournalBatch != null) {

    				glJournal.setGlJournalBatch(glJournalBatch);

    			}

    			// create journal lines

    			j = invDistributionRecords.iterator();

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

    				//invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());

    				//glJournal.addGlJournalLine(glJournalLine);
    				glJournalLine.setGlJournal(glJournal);

    				invDistributionRecord.setDrImported(EJBCommon.TRUE);


    			}

    			if (glOffsetJournalLine != null) {

    				//glJournal.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlJournal(glJournal);

    			}

    			// post journal to gl

    			Collection glJournalLines = glJournal.getGlJournalLines();

    			i = glJournalLines.iterator();

    			while (i.hasNext()) {

    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

    				// post current to current acv

    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


    				// post to subsequent acvs (propagate)

    				Collection glSubsequentAccountingCalendarValues =
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

    				while (acvsIter.hasNext()) {

    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    						(LocalGlAccountingCalendarValue)acvsIter.next();

    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    				}

    				// post to subsequent years if necessary

    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY);

    					Iterator sobIter = glSubsequentSetOfBooks.iterator();

    					while (sobIter.hasNext()) {

    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

    						// post to subsequent acvs of subsequent set of book(propagate)

    						Collection glAccountingCalendarValues =
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

    						Iterator acvIter = glAccountingCalendarValues.iterator();

    						while (acvIter.hasNext()) {

    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    								(LocalGlAccountingCalendarValue)acvIter.next();

    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							} else { // revenue & expense

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							}

    						}

    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

    					}

    				}

    			}

			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlJREffectiveDatePeriodClosedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalJournalNotBalanceException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalRecordAlreadyDeletedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalTransactionAlreadyPostedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {

    	Debug.print("ArMiscReceiptEntryControllerBean addInvAlEntry");

    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    	// Initialize EJB Home

    	try {

    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL,null, null, 0,0, AL_VD, AD_CMPNY);

    		// map adjustment, unit of measure, item location
    		//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvAdjustment(invAdjustment);
    		//invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvItemLocation(invItemLocation);
    		//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvItemLocation(invItemLocation);

    		return invAdjustmentLine;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ArMiscReceiptEntryControllerBean saveInvAdjustment");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home

    	try{

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;

    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

    		try {

    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		try {

    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		while (true) {

    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));
    					break;

    				}

    			} else {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					break;

    				}

    			}

    		}

    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

    		return invAdjustment;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}


    }

    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {

    	Debug.print("ArMiscReceiptEntryControllerBean postInvAdjustmentToInventory");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

    	// Initialize EJB Home

    	try {

    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;

    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

    		if (CST_ADJST_QTY < 0) {

    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

    		}

    		// create costing

    		try {

    			// generate line number

    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

    		} catch (FinderException ex) {

    			CST_LN_NMBR = 1;

    		}

    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		//invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvItemLocation(invItemLocation);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);

    		// propagate balance if necessary

    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

    		Iterator i = invCostings.iterator();

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);

    		}


    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}



    }




















































    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveArRctEntry(com.util.ArReceiptDetails details, LocalGlInvestorAccountBalance glInvestorAccountBalance, String BA_NM,
        String TC_NM, String WTC_NM, String FC_NM, String CST_CSTMR_CODE, String RB_NM, ArrayList ilList, boolean isDraft,
		String SLP_SLSPRSN_CODE, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
		GlobalDocumentNumberNotUniqueException,
		GlobalConversionDateNotExistException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalBranchAccountNumberInvalidException,
		GlobalRecordAlreadyAssignedException {

        Debug.print("ArMiscReceiptEntryControllerBean saveArRctEntry");

        LocalArReceiptHome arReceiptHome = null;
        LocalArReceiptBatchHome arReceiptBatchHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalArSalespersonHome arSalespersonHome = null;
        LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
        LocalAdBranchBankAccountHome adBranchBankAccountHome = null;
        LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;

        LocalArReceipt arReceipt = null;

        // Initialize EJB Home

        try {

        	adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);

            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            arReceiptBatchHome = (LocalArReceiptBatchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalArReceiptBatchHome.JNDI_NAME, LocalArReceiptBatchHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
            arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);
            adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
    				lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME, LocalAdBranchBankAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {


        	// validate if misc receipt is already deleted

        	try {

        		if (details.getRctCode() != null) {

        			arReceipt = arReceiptHome.findByPrimaryKey(details.getRctCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if misc receipt is already posted, void, approved or pending

	        if (details.getRctCode() != null && details.getRctVoid() == EJBCommon.FALSE) {

        		if (arReceipt.getRctApprovalStatus() != null) {

	        		if (arReceipt.getRctApprovalStatus().equals("APPROVED") ||
	        		    arReceipt.getRctApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (arReceipt.getRctApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (arReceipt.getRctPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (arReceipt.getRctVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}

        	// misc receipt is void

	    	if (details.getRctCode() != null && details.getRctVoid() == EJBCommon.TRUE) {

	    		if (arReceipt.getRctVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

	    		// check if misc receipt is applied for customer deposit

	    		if(arReceipt.getRctType().equals("MISC") && arReceipt.getRctCustomerDeposit() == EJBCommon.TRUE) {

	    			if(arReceipt.getRctAppliedDeposit() > 0) {

	    				throw new GlobalRecordAlreadyAssignedException();

	    			}

	    			double draftAppliedDeposit = 0d;

	    			Collection arUnpostedAppliedInvoices = null;

	    			try {

	    				arUnpostedAppliedInvoices = arAppliedInvoiceHome.findUnpostedAiWithDepositByCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode(), AD_BRNCH, AD_CMPNY);

	    			} catch (FinderException ex) {

	    			}

	    			Iterator i = arUnpostedAppliedInvoices.iterator();

	    			while(i.hasNext()){

	    				LocalArAppliedInvoice arUnpostedAppliedInvoice = (LocalArAppliedInvoice)i.next();

	    				draftAppliedDeposit += arUnpostedAppliedInvoice.getAiAppliedDeposit();

	    			}

	    			Collection arDepositReceipts = null;

	         		try {

	         			arDepositReceipts = arReceiptHome.findOpenDepositEnabledPostedRctByCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode(), AD_CMPNY);

	         		} catch (FinderException ex) {

	         		}

	         		i = arDepositReceipts.iterator();

	         		while(i.hasNext()){

	    				LocalArReceipt arDepositReceipt = (LocalArReceipt)i.next();

	    				if (arDepositReceipt.getRctCode().equals(arReceipt.getRctCode()) &&
	    						draftAppliedDeposit > 0) {

							throw new GlobalRecordAlreadyAssignedException();

						}

	    				draftAppliedDeposit -= arDepositReceipt.getRctAmount() - arDepositReceipt.getRctAppliedDeposit();

	    			}


	    		}

    			// check  if receipt is already deposited

	        	if (!arReceipt.getCmFundTransferReceipts().isEmpty()){

	        		throw new GlobalRecordAlreadyAssignedException ();

	        	}

        		if (arReceipt.getRctPosted() == EJBCommon.TRUE) {

		    	    // generate approval status

		    		String RCT_APPRVL_STATUS = null;

				    if (!isDraft) {

		        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

		        		// check if ar receipt approval is enabled

		        		if (adApproval.getAprEnableArReceipt() == EJBCommon.FALSE) {

		        			RCT_APPRVL_STATUS = "N/A";

		        		} else {

		        			// check if receipt is self approved

		        			LocalAdAmountLimit adAmountLimit = null;

		        			try {

		        				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR RECEIPT", "REQUESTER", details.getRctLastModifiedBy(), AD_CMPNY);

		        			} catch (FinderException ex) {

		        				throw new GlobalNoApprovalRequesterFoundException();

		        			}

		        			if (arReceipt.getRctAmount() <= adAmountLimit.getCalAmountLimit()) {

		        				RCT_APPRVL_STATUS = "N/A";

		        			} else {

		        				// for approval, create approval queue

		        				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AR RECEIPT", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

		        				 if (adAmountLimits.isEmpty()) {

		        				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

		        				 	if (adApprovalUsers.isEmpty()) {

		        				 		throw new GlobalNoApprovalApproverFoundException();

		        				 	}

		        				 	Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
		        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

		        				 } else {

		        				 	boolean isApprovalUsersFound = false;

		        				 	Iterator i = adAmountLimits.iterator();

		        				 	while (i.hasNext()) {

		        				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

		        				 		if (arReceipt.getRctAmount() <= adNextAmountLimit.getCalAmountLimit()) {

		        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

		        				 			Iterator j = adApprovalUsers.iterator();

				        				 	while (j.hasNext()) {

				        				 		isApprovalUsersFound = true;

				        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

				        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
				        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

				        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

				        				 	}

			    				 			break;

		        				 		} else if (!i.hasNext()) {

		        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

		        				 			Iterator j = adApprovalUsers.iterator();

				        				 	while (j.hasNext()) {

				        				 		isApprovalUsersFound = true;

				        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

				        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
				        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

				        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

				        				 	}

			    				 			break;

		        				 		}

		        				 		adAmountLimit = adNextAmountLimit;

		        				 	}

		        				 	if (!isApprovalUsersFound) {

		        				 		throw new GlobalNoApprovalApproverFoundException();

		        				 	}

		        			    }

		        			    RCT_APPRVL_STATUS = "PENDING";
		        			}
		        		}
		        	}


		    		// reverse distribution records

		    		Collection arDistributionRecords = arReceipt.getArDistributionRecords();
		    		ArrayList list = new ArrayList();

		    		Iterator i = arDistributionRecords.iterator();

		    		while (i.hasNext()) {

		    			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

		    			list.add(arDistributionRecord);

		    		}

		    		i = list.iterator();

		    		while (i.hasNext()) {

		    			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

		    			this.addArDrEntryMisc(arReceipt.getArDrNextLine(),
		    				arDistributionRecord.getDrClass(),
		    				arDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
		    				arDistributionRecord.getDrAmount(),
		    				arDistributionRecord.getGlChartOfAccount().getCoaCode(),
		    				EJBCommon.TRUE,
		    				arReceipt, AD_BRNCH, AD_CMPNY);

		    		}

		    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

		        	if (RCT_APPRVL_STATUS != null && RCT_APPRVL_STATUS.equals("N/A") && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

		        		arReceipt.setRctVoid(EJBCommon.TRUE);
		        		this.executeArRctPost(arReceipt.getRctCode(), details.getRctLastModifiedBy(), AD_BRNCH, AD_CMPNY);

		        	}

		        	 // set void approval status

		    	    arReceipt.setRctVoidApprovalStatus(RCT_APPRVL_STATUS);

		        }

		        arReceipt.setRctVoid(EJBCommon.TRUE);
		        arReceipt.setRctLastModifiedBy(details.getRctLastModifiedBy());
        		arReceipt.setRctDateLastModified(details.getRctDateLastModified());

	    	    return arReceipt.getRctCode();

	    	}

		    // validate if receipt number is unique receipt number is automatic then set next sequence

	    	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

        	if (details.getRctCode() == null) {

        		try {

 					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AR RECEIPT", AD_CMPNY);

 				} catch (FinderException ex) {

 				}

 				try {

 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

 				} catch (FinderException ex) {

 				}

	    		LocalArReceipt arExistingReceipt = null;

	    		try {

	    		    arExistingReceipt = arReceiptHome.findByRctNumberAndBrCode(details.getRctNumber(), AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	            if (arExistingReceipt != null) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

		        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
		            (details.getRctNumber() == null || details.getRctNumber().trim().length() == 0)) {

		            while (true) {

		            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

		            		try {

			            		arReceiptHome.findByRctNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

			            	} catch (FinderException ex) {

			            		details.setRctNumber(adDocumentSequenceAssignment.getDsaNextSequence());
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
					            break;

			            	}

		            	} else {

		            		try {

			            		arReceiptHome.findByRctNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

			            	} catch (FinderException ex) {

			            		details.setRctNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
					            break;

			            	}

		            	}

		            }

		        }

		    } else {

		    	LocalArReceipt arExistingReceipt = null;

		    	try {

	    		    arExistingReceipt = arReceiptHome.findByRctNumberAndBrCode(details.getRctNumber(), AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	        	if (arExistingReceipt != null &&
	                !arExistingReceipt.getRctCode().equals(details.getRctCode())) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

	            if (arReceipt.getRctNumber() != details.getRctNumber() &&
	                (details.getRctNumber() == null || details.getRctNumber().trim().length() == 0)) {

	                details.setRctNumber(arReceipt.getRctNumber());

	         	}

		    }

		    // validate if conversion date exists

	        try {

	      	    if (details.getRctConversionDate() != null) {

	      	    	LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
	      	    			FC_NM, AD_CMPNY);
	      	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	      	    	if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

	      	    		LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	      	    			glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
	      	    					details.getRctConversionDate(), AD_CMPNY);

	      	    	} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

	      	    		LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	      	    			glFunctionalCurrencyRateHome.findByFcCodeAndDate(
	      	    					adCompany.getGlFunctionalCurrency().getFcCode(), details.getRctConversionDate(), AD_CMPNY);

	      	    	}

		        }

	        } catch (FinderException ex) {

	        	 throw new GlobalConversionDateNotExistException();

	        }

            // used in checking if receipt should re-generate distribution records and re-calculate taxes
	        boolean isRecalculate = true;

        	// create misc receipt

        	if (details.getRctCode() == null) {

				arReceipt = arReceiptHome.create("MISC", details.getRctDescription(),
				    details.getRctDate(), details.getRctNumber(),details.getRctReferenceNumber(), details.getRctCheckNo(),details.getRctPayfileReferenceNumber(),
				    details.getRctChequeNumber(), null, null, null, null,
				    0d, 0d,0d,0d,0d,0d,0d,
				    details.getRctConversionDate(), details.getRctConversionRate(),
				    details.getRctSoldTo(), details.getRctPaymentMethod(), details.getRctCustomerDeposit(), 0d, null, null,
				    EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE,
				    EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
				    null, null, null, null, null,
				    details.getRctCreatedBy(), details.getRctDateCreated(), details.getRctLastModifiedBy(), details.getRctDateLastModified(),
				    null, null, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE, details.getRctSubjectToCommission(), null, null,
				    details.getRctInvtrBeginningBalance(), details.getRctInvtrInvestorFund(), details.getRctInvtrNextRunDate(),
				    AD_BRNCH, AD_CMPNY);

        	} else {

        		// check if critical fields are changed

        		if (!arReceipt.getArTaxCode().getTcName().equals(TC_NM) ||
        		    !arReceipt.getArWithholdingTaxCode().getWtcName().equals(WTC_NM) ||
					!arReceipt.getArCustomer().getCstCustomerCode().equals(CST_CSTMR_CODE) ||
					!arReceipt.getAdBankAccount().getBaName().equals(BA_NM) ||
					ilList.size() != arReceipt.getArInvoiceLines().size()) {

        			isRecalculate = true;

        		} else if (ilList.size() == arReceipt.getArInvoiceLines().size()) {

        			Iterator ilIter = arReceipt.getArInvoiceLines().iterator();
        			Iterator ilListIter = ilList.iterator();

        			while (ilIter.hasNext()) {

        				LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)ilIter.next();
        				ArModInvoiceLineDetails mdetails = (ArModInvoiceLineDetails)ilListIter.next();

        				if (!arInvoiceLine.getArStandardMemoLine().getSmlName().equals(mdetails.getIlSmlName()) ||
							arInvoiceLine.getIlQuantity() != mdetails.getIlQuantity() ||
							arInvoiceLine.getIlUnitPrice() != mdetails.getIlUnitPrice() ||
							arInvoiceLine.getIlTax() != mdetails.getIlTax()) {

        					isRecalculate = true;
        					break;

        				}

        				isRecalculate = false;

        			}

        		} else {

        			isRecalculate = false;

        		}

        		arReceipt.setRctDescription(details.getRctDescription());
        		arReceipt.setRctDate(details.getRctDate());
        		arReceipt.setRctNumber(details.getRctNumber());
        		arReceipt.setRctReferenceNumber(details.getRctReferenceNumber());
        		arReceipt.setRctConversionDate(details.getRctConversionDate());
        		arReceipt.setRctConversionRate(details.getRctConversionRate());
        		arReceipt.setRctSoldTo(details.getRctSoldTo());
        		arReceipt.setRctPaymentMethod(details.getRctPaymentMethod());
        		arReceipt.setRctCustomerDeposit(details.getRctCustomerDeposit());
        		arReceipt.setRctLastModifiedBy(details.getRctLastModifiedBy());
        		arReceipt.setRctDateLastModified(details.getRctDateLastModified());
        		arReceipt.setRctReasonForRejection(null);
        		arReceipt.setRctSubjectToCommission(details.getRctSubjectToCommission());
        		arReceipt.setRctCustomerName(null);
        		arReceipt.setRctInvtrInvestorFund(details.getRctInvtrInvestorFund());
        		arReceipt.setRctInvtrNextRunDate(details.getRctInvtrNextRunDate());

        	}

        	LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
        	//adBankAccount.addArReceipt(arReceipt);
        	arReceipt.setAdBankAccount(adBankAccount);
        	arReceipt.setGlInvestorAccountBalance(glInvestorAccountBalance);
        	LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
        	//glFunctionalCurrency.addArReceipt(arReceipt);
        	arReceipt.setGlFunctionalCurrency(glFunctionalCurrency);

        	LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
        	//arCustomer.addArReceipt(arReceipt);
        	arReceipt.setArCustomer(arCustomer);

        	if(details.getRctCustomerName().length() > 0 && !arCustomer.getCstName().equals(details.getRctCustomerName())) {
				arReceipt.setRctCustomerName(details.getRctCustomerName());
			}

        	LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
        	//arTaxCode.addArReceipt(arReceipt);
        	arReceipt.setArTaxCode(arTaxCode);

        	LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
        	//arWithholdingTaxCode.addArReceipt(arReceipt);
        	arReceipt.setArWithholdingTaxCode(arWithholdingTaxCode);

        	LocalArSalesperson arSalesperson = null;

        	if (SLP_SLSPRSN_CODE != null && SLP_SLSPRSN_CODE.length() > 0 && !SLP_SLSPRSN_CODE.equalsIgnoreCase("NO RECORD FOUND")) {

        		// if he tagged a salesperson for this receipt
        		arSalesperson = arSalespersonHome.findBySlpSalespersonCode(SLP_SLSPRSN_CODE, AD_CMPNY);
        		//arSalesperson.addArReceipt(arReceipt);
        		arReceipt.setArSalesperson(arSalesperson);

        	} else {

        		// if he untagged a salesperson for this receipt
        		if (arReceipt.getArSalesperson() != null) {

        			arSalesperson = arSalespersonHome.findBySlpSalespersonCode(arReceipt.getArSalesperson().getSlpSalespersonCode(), AD_CMPNY);
        			arSalesperson.dropArReceipt(arReceipt);

        		}

        		// if no salesperson is set, invoice should not be subject to commission
        		arReceipt.setRctSubjectToCommission((byte)0);

        	}

        	try {

        		LocalArReceiptBatch arReceiptBatch = arReceiptBatchHome.findByRbName(RB_NM, AD_BRNCH, AD_CMPNY);
        		//arReceiptBatch.addArReceipt(arReceipt);
        		arReceipt.setArReceiptBatch(arReceiptBatch);

        	} catch (FinderException ex) {

        	}

        	if (isRecalculate) {

        	    // remove all voucher line items

	        	Collection arInvoiceLineItems = arReceipt.getArInvoiceLineItems();

		  	    Iterator i = arInvoiceLineItems.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();

		  	   	    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(), arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
		  	   	    arInvoiceLineItem.getInvItemLocation().setIlCommittedQuantity(arInvoiceLineItem.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);


		  	  	    i.remove();

		  	  	    arInvoiceLineItem.remove();

		  	    }

	        	// remove all invoice lines

	        	Collection arInvoiceLines = arReceipt.getArInvoiceLines();

		  	    i = arInvoiceLines.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)i.next();

		  	  	    i.remove();

		  	  	    arInvoiceLine.remove();

		  	    }

	        	// remove all distribution records

		  	    Collection arDistributionRecords = arReceipt.getArDistributionRecords();

		  	    i = arDistributionRecords.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

		  	  	    i.remove();

		  	  	    arDistributionRecord.remove();

		  	    }

		  	    // add new invoice lines and distribution record

		  	    double TOTAL_TAX = 0d;
		  	    double TOTAL_LINE = 0d;
		  	    double TOTAL_UNTAXABLE = 0d;


	      	    i = ilList.iterator();

	      	    while (i.hasNext()) {

	      	  	    ArModInvoiceLineDetails mInvDetails = (ArModInvoiceLineDetails) i.next();

	      	  	    LocalArInvoiceLine arInvoiceLine = this.addArIlEntryMisc(mInvDetails, arReceipt, AD_CMPNY);

	      	  	    // add revenue/credit distributions

	      	  	    this.addArDrEntryMisc(arReceipt.getArDrNextLine(),
	      	  	        "REVENUE", EJBCommon.FALSE, arInvoiceLine.getIlAmount(),
	      	  	        this.getArGlCoaRevenueAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

	      	  	    TOTAL_LINE += arInvoiceLine.getIlAmount();
	      	  	    TOTAL_TAX += arInvoiceLine.getIlTaxAmount();

	      	  	    if(arInvoiceLine.getIlTax() == EJBCommon.FALSE)
	      	  	    	TOTAL_UNTAXABLE += arInvoiceLine.getIlAmount();

	      	    }


	      	    // add tax distribution if necessary

	      	    if (!arTaxCode.getTcType().equals("NONE") &&
	        	    !arTaxCode.getTcType().equals("EXEMPT")) {
	      	    	// add branch tax code
	      	    	LocalAdBranchArTaxCode adBranchTaxCode = null;
                    Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
                    try {
                  	  adBranchTaxCode = adBranchArTaxCodeHome.findBtcByTcCodeAndBrCode(arReceipt.getArTaxCode().getTcCode(), AD_BRNCH, AD_CMPNY);

                    } catch(FinderException ex) {

                    }

                    if(adBranchTaxCode != null){
                    	this.addArDrEntryMisc(arReceipt.getArDrNextLine(),
        		      	        "TAX", EJBCommon.FALSE, TOTAL_TAX, adBranchTaxCode.getBtcGlCoaTaxCode(),
        		      	        EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

                    } else {

                    	this.addArDrEntryMisc(arReceipt.getArDrNextLine(),
        		      	        "TAX", EJBCommon.FALSE, TOTAL_TAX, arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode(),
        		      	        EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);
                    }


		        }

	      	    // add wtax distribution if necessary

	      	    double W_TAX_AMOUNT = 0d;

	      	    if (arWithholdingTaxCode.getWtcRate() != 0) {

	      	    	W_TAX_AMOUNT = EJBCommon.roundIt((TOTAL_LINE - TOTAL_UNTAXABLE) * (arWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

	      	    	this.addArDrEntryMisc(arReceipt.getArDrNextLine(), "W-TAX",
	      	    	    EJBCommon.TRUE, W_TAX_AMOUNT, arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(),
	      	    	    EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

	      	    }


	      	    // add cash distribution
	      	  LocalAdBranchBankAccount adBranchBankAccount = null;
              Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
              try {
                  adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(arReceipt.getAdBankAccount().getBaCode(), AD_BRNCH, AD_CMPNY);

              } catch(FinderException ex) {

              }

              if(adBranchBankAccount != null){

            	  this.addArDrEntryMisc(arReceipt.getArDrNextLine(), "CASH",
      	      	        EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT,
      	      	        adBranchBankAccount.getBbaGlCoaCashAccount(),
      	      	        EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);


              } else {

            	  this.addArDrEntryMisc(arReceipt.getArDrNextLine(), "CASH",
        	      	        EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT,
        	      	      arReceipt.getAdBankAccount().getBaCoaGlCashAccount(),
        	      	        EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);


              }



	      	    // set receipt amount

	      	    arReceipt.setRctAmount(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT);

      	    }

      	    // generate approval status

            String RCT_APPRVL_STATUS = null;

        	if (!isDraft) {

        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        		// check if ar receipt approval is enabled

        		if (adApproval.getAprEnableArReceipt() == EJBCommon.FALSE) {

        			RCT_APPRVL_STATUS = "N/A";

        		} else {

        			// check if receipt is self approved

        			LocalAdAmountLimit adAmountLimit = null;

        			try {

        				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AR RECEIPT", "REQUESTER", details.getRctLastModifiedBy(), AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalNoApprovalRequesterFoundException();

        			}

        			if (arReceipt.getRctAmount() <= adAmountLimit.getCalAmountLimit()) {

        				RCT_APPRVL_STATUS = "N/A";

        			} else {

        				// for approval, create approval queue

        				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AR RECEIPT", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

        				 if (adAmountLimits.isEmpty()) {

        				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 	if (adApprovalUsers.isEmpty()) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        				 	Iterator j = adApprovalUsers.iterator();

        				 	while (j.hasNext()) {

        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

        				 	}

        				 } else {

        				 	boolean isApprovalUsersFound = false;

        				 	Iterator i = adAmountLimits.iterator();

        				 	while (i.hasNext()) {

        				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

        				 		if (arReceipt.getRctAmount() <= adNextAmountLimit.getCalAmountLimit()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
		        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		} else if (!i.hasNext()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AR RECEIPT", arReceipt.getRctCode(),
		        				 				arReceipt.getRctNumber(), arReceipt.getRctDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		}

        				 		adAmountLimit = adNextAmountLimit;

        				 	}

        				 	if (!isApprovalUsersFound) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        			    }

        			    RCT_APPRVL_STATUS = "PENDING";
        			}
        		}
        	}

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (RCT_APPRVL_STATUS != null && RCT_APPRVL_STATUS.equals("N/A") && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		this.executeArRctPost(arReceipt.getRctCode(), arReceipt.getRctLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}

        	// set receipt approval status

        	arReceipt.setRctApprovalStatus(RCT_APPRVL_STATUS);

      	    return arReceipt.getRctCode();


        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalConversionDateNotExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

         } catch (GlobalNoApprovalRequesterFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyAssignedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }


    }




    private void executeArRctPost(Integer RCT_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
    GlobalRecordAlreadyDeletedException,
    GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	AdPRFCoaGlVarianceAccountNotFoundException,
	GlobalExpiryDateNotFoundException {

    Debug.print("ArMiscReceiptEntryControllerBean executeArRctPost");

    LocalArReceiptHome arReceiptHome = null;
    LocalAdCompanyHome adCompanyHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalGlSetOfBookHome glSetOfBookHome = null;
    LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    LocalGlJournalHome glJournalHome = null;
    LocalGlJournalBatchHome glJournalBatchHome = null;
    LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    LocalGlJournalLineHome glJournalLineHome = null;
    LocalGlJournalSourceHome glJournalSourceHome = null;
    LocalGlJournalCategoryHome glJournalCategoryHome = null;
    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    LocalArDistributionRecordHome arDistributionRecordHome = null;
    LocalInvCostingHome invCostingHome = null;
    LocalInvItemHome invItemHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalAdBankAccountHome adBankAccountHome = null;
    LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
		LocalGlForexLedgerHome glForexLedgerHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

    LocalArReceipt arReceipt = null;

    // Initialize EJB Home

    try {

        arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
            lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
        	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
        glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
        glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
        glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
        glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
        glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
        glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
        glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
        glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
        glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
        arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
        	lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
        invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
        invItemHome = (LocalInvItemHome)EJBHomeFactory.
        	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
          	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
        adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
        glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
        glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

    } catch (NamingException ex) {

        throw new EJBException(ex.getMessage());

    }

    try {

        LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

    	// validate if receipt is already deleted

    	try {

    		arReceipt = arReceiptHome.findByPrimaryKey(RCT_CODE);

    	} catch (FinderException ex) {

    		throw new GlobalRecordAlreadyDeletedException();

    	}

    	// validate if receipt is already posted

    	if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() == EJBCommon.TRUE) {

    		throw new GlobalTransactionAlreadyPostedException();

    		// validate if receipt void is already posted

    	} else if (arReceipt.getRctVoid() == EJBCommon.TRUE && arReceipt.getRctVoidPosted() == EJBCommon.TRUE) {

    		throw new GlobalTransactionAlreadyVoidPostedException();

    	}



    	// post receipt

    	if (arReceipt.getRctVoid() == EJBCommon.FALSE && arReceipt.getRctPosted() == EJBCommon.FALSE) {


    		if (arReceipt.getRctType().equals("MISC") && !arReceipt.getArInvoiceLineItems().isEmpty()) {

        		Iterator c = arReceipt.getArInvoiceLineItems().iterator();

    			while(c.hasNext()) {

    				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

    				String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
    				String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

    				double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
    						arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

    				LocalInvCosting invCosting = null;

    				try {

    					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arReceipt.getRctDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

    				} catch (FinderException ex) {

    				}

    				double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

    				if (invCosting == null) {

						this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
								QTY_SLD, COST * QTY_SLD,
								-QTY_SLD, -COST * QTY_SLD,
								0d, null, AD_BRNCH, AD_CMPNY);
    				} else {

    					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

	    					double avgCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
	    							Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

							this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
									QTY_SLD, avgCost * QTY_SLD,
									invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (avgCost * QTY_SLD),
									0d, null, AD_BRNCH, AD_CMPNY);

    					} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

    	        			double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST :
    	        					this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
    	        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

    	        			this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
    	        					QTY_SLD, fifoCost * QTY_SLD,
    	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (fifoCost * QTY_SLD),
    	        					0d, null, AD_BRNCH, AD_CMPNY);

    					} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {

    						double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

    	        			this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
    	        					QTY_SLD, standardCost * QTY_SLD,
    	        					invCosting.getCstRemainingQuantity() - QTY_SLD, invCosting.getCstRemainingValue() - (standardCost * QTY_SLD),
    	        					0d, null, AD_BRNCH, AD_CMPNY);
    					}

    				}



    			}

        	}

    		// increase bank balance CASH
    		if(arReceipt.getRctAmountCash() > 0){

			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

			try {

				// find bankaccount balance before or equal receipt date

				Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

				if (!adBankAccountBalances.isEmpty()) {

					// get last check

					ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

					LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

					if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash(), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);
					} else { // equals to check date

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash());

					}

				} else {

					// create new balance
					System.out.println("arReceipt.getRctAmountCash()="+arReceipt.getRctAmountCash());
					LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
							arReceipt.getRctDate(), (arReceipt.getRctAmountCash()), "BOOK", AD_CMPNY);

					//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
					adNewBankAccountBalance.setAdBankAccount(adBankAccount);

				}

				// propagate to subsequent balances if necessary

				adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

				Iterator i = adBankAccountBalances.iterator();

				while (i.hasNext()) {

					LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

					adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCash());

				}

			} catch (Exception ex) {

				ex.printStackTrace();

			}
    		}

			// increase bank balance CARD 1
    		if(arReceipt.getRctAmountCard1() > 0){


    			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard1().getBaCode());

			try {

				// find bankaccount balance before or equal receipt date

				Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

				if (!adBankAccountBalances.isEmpty()) {

					// get last check

					ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

					LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

					if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1(), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);
					} else { // equals to check date

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1());

					}

				} else {

					// create new balance
					System.out.println("arReceipt.getRctAmountCard1()="+arReceipt.getRctAmountCard1());
					LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
							arReceipt.getRctDate(), (arReceipt.getRctAmountCard1()), "BOOK", AD_CMPNY);

					//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
					adNewBankAccountBalance.setAdBankAccount(adBankAccount);

				}

				// propagate to subsequent balances if necessary

				adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

				Iterator i = adBankAccountBalances.iterator();

				while (i.hasNext()) {

					LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

					adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard1());

				}

			} catch (Exception ex) {

				ex.printStackTrace();

			}

    		}

//increase bank balance CARD 2
    		if(arReceipt.getRctAmountCard2() > 0){
    			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard2().getBaCode());

			try {

				// find bankaccount balance before or equal receipt date

				Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

				if (!adBankAccountBalances.isEmpty()) {

					// get last check

					ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

					LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

					if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2(), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);
					} else { // equals to check date

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2());

					}

				} else {

					// create new balance
					System.out.println("arReceipt.getRctAmountCard2()="+arReceipt.getRctAmountCard2());
					LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
							arReceipt.getRctDate(), (arReceipt.getRctAmountCard2()), "BOOK", AD_CMPNY);

					//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
					adNewBankAccountBalance.setAdBankAccount(adBankAccount);

				}

				// propagate to subsequent balances if necessary

				adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

				Iterator i = adBankAccountBalances.iterator();

				while (i.hasNext()) {

					LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

					adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard2());

				}

			} catch (Exception ex) {

				ex.printStackTrace();

			}
    		}

//increase bank balance CARD 3
    		if(arReceipt.getRctAmountCard3() > 0){
    			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard3().getBaCode());

			try {

				// find bankaccount balance before or equal receipt date

				Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

				if (!adBankAccountBalances.isEmpty()) {

					// get last check

					ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

					LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

					if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3(), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);
					} else { // equals to check date

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3());

					}

				} else {

					// create new balance
					System.out.println("arReceipt.getRctAmountCard3()="+arReceipt.getRctAmountCard3());
					LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
							arReceipt.getRctDate(), (arReceipt.getRctAmountCard3()), "BOOK", AD_CMPNY);

					//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
					adNewBankAccountBalance.setAdBankAccount(adBankAccount);

				}

				// propagate to subsequent balances if necessary

				adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

				Iterator i = adBankAccountBalances.iterator();

				while (i.hasNext()) {

					LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

					adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3());

				}

			} catch (Exception ex) {

				ex.printStackTrace();

			}
    		}
    		// increase bank balance CHEQUE
    		if(arReceipt.getRctAmountCheque() > 0){
    			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

			try {

				// find bankaccount balance before or equal receipt date

				Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

				if (!adBankAccountBalances.isEmpty()) {

					// get last check

					ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

					LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

					if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque(), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);
					} else { // equals to check date

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque());

					}

				} else {

					// create new balance
					System.out.println("arReceipt.getRctAmountCheque()="+arReceipt.getRctAmountCheque());
					LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
							arReceipt.getRctDate(), (arReceipt.getRctAmountCheque()), "BOOK", AD_CMPNY);

					//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
					adNewBankAccountBalance.setAdBankAccount(adBankAccount);

				}

				// propagate to subsequent balances if necessary

				adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

				Iterator i = adBankAccountBalances.iterator();

				while (i.hasNext()) {

					LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

					adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCheque());

				}

			} catch (Exception ex) {

				ex.printStackTrace();

			}
    		}




    	   // set receipt post status

           arReceipt.setRctPosted(EJBCommon.TRUE);
           arReceipt.setRctPostedBy(USR_NM);
           arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


    	} else if (arReceipt.getRctVoid() == EJBCommon.TRUE && arReceipt.getRctVoidPosted() == EJBCommon.FALSE) { // void receipt
    	    // VOIDING MISC RECEIPT
    	     if (arReceipt.getRctType().equals("MISC") && !arReceipt.getArInvoiceLineItems().isEmpty()) {

    			Iterator c = arReceipt.getArInvoiceLineItems().iterator();

    			while(c.hasNext()) {

    				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) c.next();

    				String II_NM = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName();
    				String LOC_NM = arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName();

    				double QTY_SLD = this.convertByUomFromAndItemAndQuantity(arInvoiceLineItem.getInvUnitOfMeasure(),
    						arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(), AD_CMPNY);

    				LocalInvCosting invCosting = null;

    				try {

    					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(arReceipt.getRctDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

    				} catch (FinderException ex) {

    				}

    				double COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();

    				if (invCosting == null) {

						this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
								-QTY_SLD, -COST * QTY_SLD,
								 QTY_SLD,  COST * QTY_SLD,
								 0d, null, AD_BRNCH, AD_CMPNY);

    				} else {


    					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {

	    					double avgCost = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

							this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
									-QTY_SLD, -avgCost * QTY_SLD,
									invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (avgCost * QTY_SLD),
									0d, USR_NM, AD_BRNCH, AD_CMPNY);

    					} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {

    						double fifoCost = this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
    	        					QTY_SLD, arInvoiceLineItem.getIliUnitPrice(), true, AD_BRNCH, AD_CMPNY);

    	        			this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
    	        					-QTY_SLD, -fifoCost * QTY_SLD,
    	        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (fifoCost * QTY_SLD),
    	        					0d, USR_NM, AD_BRNCH, AD_CMPNY);

    					} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {

    	        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

    	        			this.postToInv(arInvoiceLineItem, arReceipt.getRctDate(),
    	        					-QTY_SLD, -standardCost * QTY_SLD,
    	        					invCosting.getCstRemainingQuantity() + QTY_SLD, invCosting.getCstRemainingValue() + (standardCost * QTY_SLD),
    	        					0d, USR_NM, AD_BRNCH, AD_CMPNY);

    					}

    				}

    			}

        	}

    		// decrease bank balance cash

     		if(arReceipt.getRctAmountCash() > 0){

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

				try {

					// find bankaccount balance before or equal receipt date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCash(), "BOOK", AD_CMPNY);

							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCash());

						}

					} else {

						// create new balance
						System.out.println("arReceipt.getRctAmountCash()="+arReceipt.getRctAmountCash());
						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), (-arReceipt.getRctAmountCash()), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCash());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}
     		}

				// decrease bank balance CARD 1
     		if(arReceipt.getRctAmountCard1() > 0){


     			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard1().getBaCode());

				try {

					// find bankaccount balance before or equal receipt date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard1(), "BOOK", AD_CMPNY);

							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard1());

						}

					} else {

						// create new balance
						System.out.println("arReceipt.getRctAmountCard1()="+arReceipt.getRctAmountCard1());
						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), (-arReceipt.getRctAmountCard1()), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard1());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}

     		}

     		// decrease bank balance CARD 2
     		if(arReceipt.getRctAmountCard2() > 0){
     			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard2().getBaCode());

				try {

					// find bankaccount balance before or equal receipt date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard2(), "BOOK", AD_CMPNY);

							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard2());

						}

					} else {

						// create new balance
						System.out.println("arReceipt.getRctAmountCard2()="+arReceipt.getRctAmountCard2());
						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), (-arReceipt.getRctAmountCard2()), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard2());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}
     		}

     		// decrease bank balance CARD 3
     		if(arReceipt.getRctAmountCard3() > 0){
     			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard3().getBaCode());

				try {

					// find bankaccount balance before or equal receipt date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard3(), "BOOK", AD_CMPNY);

							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + arReceipt.getRctAmountCard3());

						}

					} else {

						// create new balance
						System.out.println("arReceipt.getRctAmountCard3()="+arReceipt.getRctAmountCard3());
						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), (-arReceipt.getRctAmountCard3()), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCard3());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}
     		}
     		// decrease bank balance CHEQUE
     		if(arReceipt.getRctAmountCheque() > 0){
     			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());

				try {

					// find bankaccount balance before or equal receipt date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(arReceipt.getRctDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									arReceipt.getRctDate(), adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCheque(), "BOOK", AD_CMPNY);

							//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
							adNewBankAccountBalance.setAdBankAccount(adBankAccount);
						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCheque());

						}

					} else {

						// create new balance
						System.out.println("arReceipt.getRctAmountCheque()="+arReceipt.getRctAmountCheque());
						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								arReceipt.getRctDate(), (-arReceipt.getRctAmountCheque()), "BOOK", AD_CMPNY);

						//adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
						adNewBankAccountBalance.setAdBankAccount(adBankAccount);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(arReceipt.getRctDate(), arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - arReceipt.getRctAmountCheque());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}
     		}




     	   // set receipt post status

	           arReceipt.setRctPosted(EJBCommon.TRUE);
	           arReceipt.setRctPostedBy(USR_NM);
	           arReceipt.setRctDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

    	}

    	// post to gl if necessary

       if (adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

       	   // validate if date has no period and period is closed

       	   LocalGlSetOfBook glJournalSetOfBook = null;

       	   try {

       	   	   glJournalSetOfBook = glSetOfBookHome.findByDate(arReceipt.getRctDate(), AD_CMPNY);

       	   } catch (FinderException ex) {

       	       throw new GlJREffectiveDateNoPeriodExistException();

       	   }

		   LocalGlAccountingCalendarValue glAccountingCalendarValue =
		        glAccountingCalendarValueHome.findByAcCodeAndDate(
		        	glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), arReceipt.getRctDate(), AD_CMPNY);


		   if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
		        glAccountingCalendarValue.getAcvStatus() == 'C' ||
		        glAccountingCalendarValue.getAcvStatus() == 'P') {

		        throw new GlJREffectiveDatePeriodClosedException();

		   }

		   // check if invoice is balance if not check suspense posting

           LocalGlJournalLine glOffsetJournalLine = null;

           Collection arDistributionRecords = null;

           if (arReceipt.getRctVoid() == EJBCommon.FALSE) {

           	   arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.FALSE, arReceipt.getRctCode(), AD_CMPNY);

           } else {

           	   arDistributionRecords = arDistributionRecordHome.findImportableDrByDrReversedAndRctCode(EJBCommon.TRUE, arReceipt.getRctCode(), AD_CMPNY);

           }


           Iterator j = arDistributionRecords.iterator();

           double TOTAL_DEBIT = 0d;
           double TOTAL_CREDIT = 0d;

           while (j.hasNext()) {

        		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

        		double DR_AMNT = 0d;

        		if (arDistributionRecord.getArAppliedInvoice() != null) {

        			LocalArInvoice arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice();

            		DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
            			arInvoice.getGlFunctionalCurrency().getFcName(),
            			arInvoice.getInvConversionDate(),
            			arInvoice.getInvConversionRate(),
	        		    arDistributionRecord.getDrAmount(), AD_CMPNY);

        		} else {

        			DR_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
		        		    arReceipt.getGlFunctionalCurrency().getFcName(),
		        		    arReceipt.getRctConversionDate(),
		        		    arReceipt.getRctConversionRate(),
		        		    arDistributionRecord.getDrAmount(), AD_CMPNY);

        		}

        		if (arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

        			TOTAL_DEBIT += DR_AMNT;

        		} else {

        			TOTAL_CREDIT += DR_AMNT;

        		}

        	}

        	TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        	TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        	if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        	    TOTAL_DEBIT != TOTAL_CREDIT) {

        	    LocalGlSuspenseAccount glSuspenseAccount = null;

        	    try {

	        		glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("ACCOUNTS RECEIVABLES", "SALES RECEIPTS", AD_CMPNY);

        	    } catch (FinderException ex) {

        	    	throw new GlobalJournalNotBalanceException();

        	    }

        	    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

        	    	glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
        	    	    EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        	    } else {

        	    	glOffsetJournalLine = glJournalLineHome.create((short)(arDistributionRecords.size() + 1),
        	    	    EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        	    }

        	    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        	    //glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        	    glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
			    TOTAL_DEBIT != TOTAL_CREDIT) {

				throw new GlobalJournalNotBalanceException();

			}

	       // create journal batch if necessary

	       LocalGlJournalBatch glJournalBatch = null;
	       java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

	       try {

	       	   if (adPreference.getPrfEnableArReceiptBatch() == EJBCommon.TRUE) {

	       	       glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arReceipt.getArReceiptBatch().getRbName(), AD_BRNCH, AD_CMPNY);

	       	   } else {

	       	   	   glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES RECEIPTS", AD_BRNCH, AD_CMPNY);

	       	   }


	       } catch (FinderException ex) {
	       }

	       if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
	     		glJournalBatch == null) {

	     		if (adPreference.getPrfEnableArReceiptBatch() == EJBCommon.TRUE) {

	     		    glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " " + arReceipt.getArReceiptBatch().getRbName(), "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

	     		} else {

	     		    glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " SALES RECEIPTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

	     		}

	       }

		   // create journal entry
	       String customerName = arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() :
   				arReceipt.getRctCustomerName();

    	   LocalGlJournal glJournal = glJournalHome.create(arReceipt.getRctReferenceNumber(),
    		    arReceipt.getRctDescription(), arReceipt.getRctDate(),
    		    0.0d, null, arReceipt.getRctNumber(), null, 1d, "N/A", null,
    		    'N', EJBCommon.TRUE, EJBCommon.FALSE,
    		    USR_NM, new Date(),
    		    USR_NM, new Date(),
    		    null, null,
    		    USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
    		    arReceipt.getArCustomer().getCstTin(),
    		    customerName, EJBCommon.FALSE,
    		    null,
    		    AD_BRNCH, AD_CMPNY);

    	   LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("ACCOUNTS RECEIVABLES", AD_CMPNY);
    	   glJournal.setGlJournalSource(glJournalSource);

           LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
           glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

           LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("SALES RECEIPTS", AD_CMPNY);
           glJournal.setGlJournalCategory(glJournalCategory);

    	   if (glJournalBatch != null) {

    		   glJournal.setGlJournalBatch(glJournalBatch);
    	   }


           // create journal lines

           j = arDistributionRecords.iterator();

           while (j.hasNext()) {

        		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

        		double DR_AMNT = 0d;

        		LocalArInvoice arInvoice = null;

        		if (arDistributionRecord.getArAppliedInvoice() != null) {

        			arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice();

            		DR_AMNT = this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
            			arInvoice.getGlFunctionalCurrency().getFcName(),
            			arInvoice.getInvConversionDate(),
            			arInvoice.getInvConversionRate(),
	        		    arDistributionRecord.getDrAmount(), AD_CMPNY);

        		} else {

        			DR_AMNT = this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
		        		    arReceipt.getGlFunctionalCurrency().getFcName(),
		        		    arReceipt.getRctConversionDate(),
		        		    arReceipt.getRctConversionRate(),
		        		    arDistributionRecord.getDrAmount(), AD_CMPNY);

        		}

        		LocalGlJournalLine glJournalLine = glJournalLineHome.create(arDistributionRecord.getDrLine(),
        			arDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

                //arDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
                glJournalLine.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());

        	    //glJournal.addGlJournalLine(glJournalLine);
                glJournalLine.setGlJournal(glJournal);

        	    arDistributionRecord.setDrImported(EJBCommon.TRUE);

		       	// for FOREX revaluation

        	    int FC_CODE = arDistributionRecord.getArAppliedInvoice() != null ?
        	    	arInvoice.getGlFunctionalCurrency().getFcCode().intValue() :
        	    	arReceipt.getGlFunctionalCurrency().getFcCode().intValue();

		       	if((FC_CODE != adCompany.getGlFunctionalCurrency().getFcCode().intValue()) &&
					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null && (FC_CODE ==
						glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().intValue())){

		       		double CONVERSION_RATE = arDistributionRecord.getArAppliedInvoice() != null ?
		            	    arInvoice.getInvConversionRate() : arReceipt.getRctConversionRate();

		            Date DATE = arDistributionRecord.getArAppliedInvoice() != null ?
	    		    	arInvoice.getInvConversionDate() : arReceipt.getRctConversionDate();

		            if (DATE != null && (CONVERSION_RATE == 0 || CONVERSION_RATE == 1)){

		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
								glJournal.getJrConversionDate(), AD_CMPNY);

		            } else if (CONVERSION_RATE == 0) {

		            	CONVERSION_RATE = 1;

		       		}

		       		Collection glForexLedgers = null;

		       		DATE = arDistributionRecord.getArAppliedInvoice() != null ?
    	    		    arInvoice.getInvDate() : arReceipt.getRctDate();

		       		try {

		       			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
		       					DATE, glJournalLine.getGlChartOfAccount().getCoaCode(),
								AD_CMPNY);

		       		} catch(FinderException ex) {

		       		}

		       		LocalGlForexLedger glForexLedger =
		       			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
		       				(LocalGlForexLedger) glForexLedgers.iterator().next();

		       		int FRL_LN = (glForexLedger != null &&
		       			glForexLedger.getFrlDate().compareTo(DATE) == 0) ?
		       				glForexLedger.getFrlLine().intValue() + 1 : 1;

		       		// compute balance
		       		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
		       		double FRL_AMNT = arDistributionRecord.getDrAmount();

		       		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));
		       		else
		       			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);

		       		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

		       		glForexLedger = glForexLedgerHome.create(DATE, new Integer (FRL_LN), "OR", FRL_AMNT,
		       				CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

		       		//glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
		       		glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());

		       		// propagate balances
		       		try{

		       			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
		       					glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
								glForexLedger.getFrlAdCompany());

		       		} catch (FinderException ex) {

		       		}

		       		Iterator itrFrl = glForexLedgers.iterator();

		       		while (itrFrl.hasNext()) {

		       			glForexLedger = (LocalGlForexLedger) itrFrl.next();
		       			FRL_AMNT = arDistributionRecord.getDrAmount();

		       			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT :
		       					(- 1 * FRL_AMNT));
		       			else
		       				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) :
		       					FRL_AMNT);

		       			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

		       		}

		       	}

           }

           if (glOffsetJournalLine != null) {

        	   //glJournal.addGlJournalLine(glOffsetJournalLine);
        	   glOffsetJournalLine.setGlJournal(glJournal);
           }

           // post journal to gl

           Collection glJournalLines = glJournal.getGlJournalLines();

	       Iterator i = glJournalLines.iterator();

	       while (i.hasNext()) {

	       	LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

	       	// post current to current acv

	       	this.postToGl(glAccountingCalendarValue,
	       			glJournalLine.getGlChartOfAccount(),
					true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


	       	// post to subsequent acvs (propagate)

	       	Collection glSubsequentAccountingCalendarValues =
	       		glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
	       				glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
						glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

	       	Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

	       	while (acvsIter.hasNext()) {

	       		LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
	       			(LocalGlAccountingCalendarValue)acvsIter.next();

	       		this.postToGl(glSubsequentAccountingCalendarValue,
	       				glJournalLine.getGlChartOfAccount(),
						false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

	       	}

	       	// post to subsequent years if necessary

	       	Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

	       	if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

	       		adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	       		LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

	       		Iterator sobIter = glSubsequentSetOfBooks.iterator();

	       		while (sobIter.hasNext()) {

	       			LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

	       			String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

	       			// post to subsequent acvs of subsequent set of book(propagate)

	       			Collection glAccountingCalendarValues =
	       				glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

	       			Iterator acvIter = glAccountingCalendarValues.iterator();

	       			while (acvIter.hasNext()) {

	       				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
	       					(LocalGlAccountingCalendarValue)acvIter.next();

	       				if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
	       						ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

	       					this.postToGl(glSubsequentAccountingCalendarValue,
	       							glJournalLine.getGlChartOfAccount(),
									false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

	       				} else { // revenue & expense

	       					this.postToGl(glSubsequentAccountingCalendarValue,
	       							glRetainedEarningsAccount,
									false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

	       				}

	       			}

	       			if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

	       		}

	       	}


	       }



	      if(arReceipt.getArCustomer().getApSupplier()!=null){

	    	// Post Investors Account balance
		       String supplierClass = arReceipt.getArCustomer().getApSupplier().getApSupplierClass().getScName();
			    // post current to current acv
			if(supplierClass.contains("Investors")){
                           this.postToGlInvestor(glAccountingCalendarValue,
			       			arReceipt.getArCustomer().getApSupplier(),
							true, (byte)0, arReceipt.getRctAmount(), AD_CMPNY);


                            // post to subsequent acvs (propagate)

                            Collection glSubsequentAccountingCalendarValues =
                                    glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                                                    glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                                                            glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

                            Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

                            while (acvsIter.hasNext()) {

                                    LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                                            (LocalGlAccountingCalendarValue)acvsIter.next();


                                    this.postToGlInvestor(glSubsequentAccountingCalendarValue,
                                                    arReceipt.getArCustomer().getApSupplier(),
                                                    false, (byte)0, arReceipt.getRctAmount(), AD_CMPNY);

                            }


                            // post to subsequent years if necessary

                            Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

                            if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

                                    adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

                                    Iterator sobIter = glSubsequentSetOfBooks.iterator();

                                    while (sobIter.hasNext()) {

                                            LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

                                            // post to subsequent acvs of subsequent set of book(propagate)

                                            Collection glAccountingCalendarValues =
                                                    glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

                                            Iterator acvIter = glAccountingCalendarValues.iterator();

                                            while (acvIter.hasNext()) {

                                                    LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                                                            (LocalGlAccountingCalendarValue)acvIter.next();

                                                    this.postToGlInvestor(glSubsequentAccountingCalendarValue,
                                                                    arReceipt.getArCustomer().getApSupplier(),
                                                                    false, (byte)0, arReceipt.getRctAmount(), AD_CMPNY);

                                            }

                                            if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

                                    }

			       	}
                        }




	      }










       }

    } catch (GlJREffectiveDateNoPeriodExistException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (GlJREffectiveDatePeriodClosedException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (GlobalJournalNotBalanceException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (GlobalRecordAlreadyDeletedException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (GlobalTransactionAlreadyPostedException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (GlobalTransactionAlreadyVoidPostedException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

     	throw ex;

    } catch (GlobalExpiryDateNotFoundException ex) {

    	getSessionContext().setRollbackOnly();
    	throw ex;

    }catch (Exception ex) {

		Debug.printStackTrace(ex);
		getSessionContext().setRollbackOnly();
		throw new EJBException(ex.getMessage());

	}

}




    private LocalArInvoiceLine addArIlEntryMisc(ArModInvoiceLineDetails mdetails, LocalArReceipt arReceipt, Integer AD_CMPNY) {

		Debug.print("ArMiscReceiptEntryControllerBean addArIlEntry");

		LocalArInvoiceLineHome arInvoiceLineHome = null;
		LocalArStandardMemoLineHome arStandardMemoLineHome = null;


        // Initialize EJB Home

        try {

            arInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
            arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

        	double IL_AMNT = 0d;
        	double IL_TAX_AMNT = 0d;

        	if (mdetails.getIlTax() == EJBCommon.TRUE) {

				// calculate net amount

				LocalArTaxCode arTaxCode = arReceipt.getArTaxCode();

				if (arTaxCode.getTcType().equals("INCLUSIVE")) {

					IL_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() / (1 + (arTaxCode.getTcRate() / 100)), precisionUnit);

		        } else {

		            // tax exclusive, none, zero rated or exempt

		            IL_AMNT = mdetails.getIlAmount();

		    	}

		    	// calculate tax

		    	if (!arTaxCode.getTcType().equals("NONE") &&
		    	    !arTaxCode.getTcType().equals("EXEMPT")) {


		        	if (arTaxCode.getTcType().equals("INCLUSIVE")) {

		        		IL_TAX_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() - IL_AMNT, precisionUnit);

		        	} else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

		        		IL_TAX_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() * arTaxCode.getTcRate() / 100, precisionUnit);

		            } else {

		            	// tax none zero-rated or exempt

		        	}

			    }

			} else {

				IL_AMNT = mdetails.getIlAmount();

			}

        	LocalArInvoiceLine arInvoiceLine = arInvoiceLineHome.create(
        		mdetails.getIlDescription(), mdetails.getIlQuantity(),
        		mdetails.getIlUnitPrice(), IL_AMNT,
        		IL_TAX_AMNT, mdetails.getIlTax(), AD_CMPNY);

            //arReceipt.addArInvoiceLine(arInvoiceLine);
            arInvoiceLine.setArReceipt(arReceipt);

            LocalArStandardMemoLine arStandardMemoLine = arStandardMemoLineHome.findBySmlName(mdetails.getIlSmlName(), AD_CMPNY);
            //arStandardMemoLine.addArInvoiceLine(arInvoiceLine);
            arInvoiceLine.setArStandardMemoLine(arStandardMemoLine);

         	return arInvoiceLine;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}


    private void addArDrEntryMisc(short DR_LN, String DR_CLSS,
    	    byte DR_DBT, double DR_AMNT, Integer COA_CODE, byte DR_RVRSD, LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY)
        	throws GlobalBranchAccountNumberInvalidException {

    		Debug.print("ArMiscReceiptEntryControllerBean addArDrEntry");

    		LocalArDistributionRecordHome arDistributionRecordHome = null;
    		LocalGlChartOfAccountHome glChartOfAccountHome = null;
    		LocalAdCompanyHome adCompanyHome = null;


            // Initialize EJB Home

            try {

                arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
                glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                    lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
                adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


            } catch (NamingException ex) {

                throw new EJBException(ex.getMessage());

            }
            System.out.println("COA_CODE1: " + COA_CODE);
            try {

            	// get company

            	System.out.println("COA_CODE:2 " + COA_CODE);
            	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		    // create distribution record

    		    LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
    			    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
    			    EJBCommon.FALSE, DR_RVRSD, AD_CMPNY);

    			//arReceipt.addArDistributionRecord(arDistributionRecord);
    			arDistributionRecord.setArReceipt(arReceipt);
    			//glChartOfAccount.addArDistributionRecord(arDistributionRecord);

    			System.out.print(glChartOfAccount.toString());

    			arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

            } catch (FinderException ex) {

        		throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

        	} catch (Exception ex) {

            	Debug.printStackTrace(ex);
            	getSessionContext().setRollbackOnly();
            	throw new EJBException(ex.getMessage());

            }

    	}




    private void postToGlInvestor(LocalGlAccountingCalendarValue glAccountingCalendarValue,
		      LocalApSupplier apSupplier,
		      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

		      Debug.print("ArMiscReceiptEntryControllerBean postToGlInvestor");

		      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		      LocalAdCompanyHome adCompanyHome = null;

		      LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;


		       // Initialize EJB Home

		       try {

		           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
		              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
		           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
		           glInvestorAccountBalanceHome = (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
				              lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);




		       } catch (NamingException ex) {

		           throw new EJBException(ex.getMessage());

		       }

		       try {

		               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);


			       	   LocalGlInvestorAccountBalance glInvestorAccountBalance =
			       			glInvestorAccountBalanceHome.findByAcvCodeAndSplCode(
			       					glAccountingCalendarValue.getAcvCode(),
					               	  apSupplier.getSplCode(), AD_CMPNY);


			           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();


			           glInvestorAccountBalance.setIrabEndingBalance(
						       EJBCommon.roundIt(glInvestorAccountBalance.getIrabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));


					    if (!isCurrentAcv) {

					    	glInvestorAccountBalance.setIrabBeginningBalance(
					       		EJBCommon.roundIt(glInvestorAccountBalance.getIrabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

						}

				 	  if (isCurrentAcv) {

				 		 glInvestorAccountBalance.setIrabTotalCredit(
					 				EJBCommon.roundIt(glInvestorAccountBalance.getIrabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));

				 	}

		       } catch (Exception ex) {

		       	   Debug.printStackTrace(ex);
		       	   throw new EJBException(ex.getMessage());

		       }

		   }


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ArStandardMemoLineControllerBean ejbCreate");

    }

}

