
/*
 * ArStandardMemoLineSyncControllerBean.java
 *
 * Created on December 26, 2007, 2:01 PM
 *
 * @author  Jeffrey S Floresca 
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchStandardMemoLine;
import com.ejb.ad.LocalAdBranchStandardMemoLineHome;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineHome;

import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;


/**
 * @ejb:bean name="ArStandardMemoLineSyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *           
 * @wsee:port-component name="ArStandardMemoLineSync"
 * 
 * @jboss:port-component uri="omega-ejb/ArStandardMemoLineSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.ArStandardMemoLineSyncWS"
 * 
*/

public class ArStandardMemoLineSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
	
	/**
     * @ejb:interface-method
     **/
    public int getArStandardMemoLineAllNewLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ArStandardMemoLineSyncControllerBean getArStandardMemoLineAllNewLength");
    	
    	LocalArStandardMemoLineHome arStandardMemoLineHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	 arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection arStandardMemoLines  = arStandardMemoLineHome.findSmlBySmlNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
        	
        	return arStandardMemoLines.size();
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public int getArStandardMemoLineHomeAllUpdatedLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ArCustomerSyncControllerBean getArCustomersAllUpdatedLength");

    	LocalArStandardMemoLineHome arStandardMemoLineHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	System.out.println(BR_BRNCH_CODE);
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection arStandardMemoLines = arStandardMemoLineHome.findSmlBySmlNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U','U','X');
        	
        	return arStandardMemoLines.size();
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
	
	/**
     * @ejb:interface-method
     **/
    public String[] getArStandardMemoLineAllNewAndUpdated(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print(" ArStandardMemoLineSyncControllerBean getArStandardMemoLineAllNewAndUpdated");
    	
    	LocalArStandardMemoLineHome arStandardMemoLineHome = null;
    	LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);

        	Collection arStandardMemoLines = arStandardMemoLineHome.findSmlBySmlNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
        	
        	Collection arUpdatedStandardMemoLines =  arStandardMemoLineHome.findSmlBySmlNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');
        	
        	String[] results = new String[arStandardMemoLines.size() + arUpdatedStandardMemoLines.size()];
        	
        	Iterator i = arStandardMemoLines.iterator();
        	
        	int ctr = 0;
	        while (i.hasNext()) {
	        	
	        	LocalArStandardMemoLine arStandardMemoLine = (LocalArStandardMemoLine)i.next();
	        	results[ctr] = standardMemoLineRowEncode(arStandardMemoLine);
	        	ctr++;
	        	
	        }
	        
	        i = arUpdatedStandardMemoLines.iterator();
	        while (i.hasNext()) {
	        	
	        	LocalArStandardMemoLine arStandardMemoLine = (LocalArStandardMemoLine)i.next();
	        	results[ctr] = standardMemoLineRowEncode(arStandardMemoLine);
	        	ctr++;
	        	
	        }
	        
	        return results;
        	
        } 
        catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public int setArStandardMemoLineAllNewAndUpdatedSuccessConfirmation(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print(" ArStandardMemoLineSyncControllerBean setArStandardMemoLineAllNewAndUpdatedSuccessConfirmation");
    	
    	LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;
    	LocalAdBranchStandardMemoLine adBranchStandardMemoLine= null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	adBranchStandardMemoLineHome = (LocalAdBranchStandardMemoLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection adBranchStandardMemoLines = adBranchStandardMemoLineHome.findBSMLByBSMLNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'U', 'X');
        	
        	Iterator i = adBranchStandardMemoLines.iterator();        	
	        while (i.hasNext()) {
	        	
	        	adBranchStandardMemoLine= (LocalAdBranchStandardMemoLine)i.next();	        	
	        	adBranchStandardMemoLine.setBsmlStandardMemoLineDownloadStatus('D');     	
	        }
        	
        } catch (Exception ex) {
        	
        	ctx.setRollbackOnly();
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
        return 0;
        
    }
    
    private String standardMemoLineRowEncode(LocalArStandardMemoLine arStandardMemoLine) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	// Start separator
    	tempResult.append(separator);

    	
    	// Primary Key
		Debug.print("Get Primary Key " + arStandardMemoLine.getSmlCode());   
	    tempResult.append(arStandardMemoLine.getSmlCode());
	    tempResult.append(separator);
    	   	
    	
    	// SML Name
	    Debug.print("Get Name " + arStandardMemoLine.getSmlName());
    	tempResult.append(arStandardMemoLine.getSmlName());
    	tempResult.append(separator);
    	    	
    	
    	// SML Description
    	Debug.print("Get Description " + arStandardMemoLine.getSmlDescription());
    	tempResult.append(arStandardMemoLine.getSmlDescription());
    	tempResult.append(separator);
    	   	
    	
    	// SML Unit Price
    	Debug.print("Get Unit Price " + arStandardMemoLine.getSmlUnitPrice());
    	tempResult.append(Double.toString(arStandardMemoLine.getSmlUnitPrice()));
    	tempResult.append(separator);
    	
    	   
    	// SML Tax
    	Debug.print("Get Tax " + arStandardMemoLine.getSmlTax());
    	tempResult.append(Double.toString(arStandardMemoLine.getSmlTax()));
    	tempResult.append(separator);
    	  	
    	
        //  SML Enable
    	Debug.print("Get Enable " + arStandardMemoLine.getSmlEnable());
    	tempResult.append(Byte.toString(arStandardMemoLine.getSmlEnable()));
    	tempResult.append(separator);
    	  	
    	   	
    	//  SML Subject To Commission
    	Debug.print("Get Commission " + arStandardMemoLine.getSmlSubjectToCommission());
    	tempResult.append(Byte.toString(arStandardMemoLine.getSmlSubjectToCommission()));
    	tempResult.append(separator);
    	
    	
    	if(arStandardMemoLine.getSmlUnitOfMeasure()!=null)
    	{			
		    //  SML Unit of Measure
    		Debug.print("Get Unit of Measure " + arStandardMemoLine.getSmlUnitOfMeasure());
			tempResult.append(arStandardMemoLine.getSmlUnitOfMeasure());
			tempResult.append(separator);
    	}   	
    	else
    	{
    		 //  SML Unit of Measure
    		Debug.print("Get Unit of Measure " + arStandardMemoLine.getSmlUnitOfMeasure());		   
			tempResult.append("0");
			tempResult.append(separator);
    	}
    	
    	      	             
     	
    	//  SML GL Chart of Account
    	Debug.print("Get Chart of Account " + arStandardMemoLine.getGlChartOfAccount().getCoaAccountNumber());
    	tempResult.append(arStandardMemoLine.getGlChartOfAccount().getCoaAccountNumber());
    	tempResult.append(separator);
   	

       // remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();
    	encodedResult = encodedResult.replace("\"", " ");
    	encodedResult = encodedResult.replace("'", " ");
    	encodedResult = encodedResult.replace(";", " ");
    	encodedResult = encodedResult.replace("\\", " ");
    	encodedResult = encodedResult.replace("|", " ");
    	
    	return encodedResult;
    	
    }
    
    public void ejbCreate() throws CreateException {

       Debug.print("ArStandardMemoLineSyncControllerBean ejbCreate");
      
    }
    
    public void ejbRemove() {};

    public void ejbActivate() {}
    public void ejbPassivate() {}

    public void setSessionContext(SessionContext ctx) {}

}