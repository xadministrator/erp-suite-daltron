
/*
 * ArTaxCodeControllerBean.java
 *
 * Created on March 5, 2004, 1:03 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ar.*;
import com.ejb.ad.*;
import com.ejb.exception.AdBACoaGlAdjustmentAccountNotFoundException;
import com.ejb.exception.AdBACoaGlAdvanceAccountNotFoundException;
import com.ejb.exception.AdBACoaGlBankChargeAccountNotFoundException;
import com.ejb.exception.AdBACoaGlCashAccountNotFoundException;
import com.ejb.exception.AdBACoaGlInterestAccountNotFoundException;
import com.ejb.exception.AdBACoaGlSalesDiscountNotFoundException;
import com.ejb.exception.ArTCCoaGlTaxCodeAccountNotFoundException;
import com.ejb.exception.ArTCInterimAccountInvalidException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModBranchBankAccountDetails;
import com.util.AdModBranchStandardMemoLineDetails;
import com.util.AdResponsibilityDetails;
import com.util.AdModBranchArTaxCodeDetails;
import com.util.ArModTaxCodeDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArTaxCodeControllerEJB"
 *           display-name="Used for entering tax codes"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArTaxCodeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArTaxCodeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArTaxCodeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 *
*/

public class ArTaxCodeControllerBean extends AbstractSessionBean {


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public AdResponsibilityDetails getAdRsByRsCode(Integer RS_CODE)
    	throws GlobalNoRecordFoundException {

    	Debug.print("ArTaxCodeControllerBean getAdRsByRsCode");

    	LocalAdResponsibilityHome adResHome = null;
    	LocalAdResponsibility adRes = null;

    	try {
    		adResHome = (LocalAdResponsibilityHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
    	} catch (NamingException ex) {

    	}

    	try {
        	adRes = adResHome.findByPrimaryKey(RS_CODE);
    	} catch (FinderException ex) {

    	}

    	AdResponsibilityDetails details = new AdResponsibilityDetails();
    	details.setRsName(adRes.getRsName());

    	return details;
    }


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrTcAll(Integer BTC_CODE, String RS_NM, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException{

    	Debug.print("ArTaxCodeControllerBean getAdBrSMLAll");


    	LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;


    	LocalAdBranchArTaxCode adBranchArTaxCode = null;
    	LocalAdBranch adBranch = null;
    	LocalGlChartOfAccount glChartOfAccount = null;
    	LocalGlChartOfAccount glInterimAccount = null;

    	Collection adBranchArTaxCodes = null;

        ArrayList branchList = new ArrayList();

        // Initialize EJB Home

        try {

        	adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	adBranchArTaxCodes = adBranchArTaxCodeHome.findBBTCByTcCodeAndRsName(BTC_CODE, RS_NM, AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adBranchArTaxCodes == null || adBranchArTaxCodes.isEmpty()) {

            throw new GlobalNoRecordFoundException();

        }

        try {

	        Iterator i = adBranchArTaxCodes.iterator();

	        while(i.hasNext()) {

	        	adBranchArTaxCode = (LocalAdBranchArTaxCode)i.next();

	        	adBranch = adBranchHome.findByPrimaryKey(adBranchArTaxCode.getAdBranch().getBrCode());

	        	AdModBranchArTaxCodeDetails mdetails = new AdModBranchArTaxCodeDetails();

	        	mdetails.setBtcBranchCode(adBranch.getBrCode());
	        	mdetails.setBtcBranchName(adBranch.getBrName());

	        	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchArTaxCode.getBtcGlCoaTaxCode());
	        	mdetails.setBtcTaxCodeAccountNumber(glChartOfAccount.getCoaAccountNumber());
	        	mdetails.setBtcTaxCodeAccountDescription(glChartOfAccount.getCoaAccountDescription());

	        	try {
	        		if(adBranchArTaxCode.getBtcGlCoaInterimCode()!=null) {
	        			glInterimAccount = glChartOfAccountHome.findByPrimaryKey(adBranchArTaxCode.getBtcGlCoaInterimCode());
	        		}

	        	} catch (Exception ex) {

	        	}


	        	mdetails.setBtcInterimCodeAccountNumber(glInterimAccount != null ? glInterimAccount.getCoaAccountNumber() : null);
	        	mdetails.setBtcInterimCodeAccountDescription(glInterimAccount != null ? glInterimAccount.getCoaAccountDescription() : null);

		    	branchList.add(mdetails);
	        }

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        return branchList;

    }


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException{

        Debug.print("ArTaxCodeControllerBean getAdBrResAll");

        LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
        LocalAdBranchHome adBranchHome = null;

        LocalAdBranchResponsibility adBranchResponsibility = null;
        LocalAdBranch adBranch = null;

        Collection adBranchResponsibilities = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adBranchResponsibilities.isEmpty()) {

            throw new GlobalNoRecordFoundException();

        }

        try {

            Iterator i = adBranchResponsibilities.iterator();

            while(i.hasNext()) {

                adBranchResponsibility = (LocalAdBranchResponsibility)i.next();

                adBranch = adBranchResponsibility.getAdBranch();

                AdBranchDetails details = new AdBranchDetails();

                details.setBrCode(adBranch.getBrCode());
                details.setBrName(adBranch.getBrName());

                list.add(details);

            }

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        return list;

    }


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **//*
    public ArrayList getArBrTcAll(Integer TC_CODE, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException{

        Debug.print("ArTaxCodeControllerBean getArBrTcAll");

        LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;


        LocalAdBranchHome adBranchHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        LocalAdBranchArTaxCode adBranchArTaxCode = null;
        LocalAdBranch adBranch = null;
        LocalGlChartOfAccount glChartOfAccount = null;

        Collection adBranchArTaxCodes = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
    				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	System.out.println("TC_CODE="+TC_CODE);
            adBranchArTaxCodes = adBranchArTaxCodeHome.findBtcByTcCode(TC_CODE, AD_CMPNY);
            System.out.println("adBranchArTaxCodes="+adBranchArTaxCodes.size());
        } catch (FinderException ex) {
        	System.out.println("-------------1");
        }
        if (adBranchArTaxCodes.isEmpty()) {
        	System.out.println("3");
            throw new GlobalNoRecordFoundException();

        }

        System.out.println("adBranchArTaxCodes.size()="+adBranchArTaxCodes.size());
        try {

            Iterator i = adBranchArTaxCodes.iterator();

            while(i.hasNext()) {

                adBranchArTaxCode = (LocalAdBranchArTaxCode )i.next();
                System.out.println("adBranchArTaxCode.getAdBranch().getBrCode()="+adBranchArTaxCode.getAdBranch().getBrCode());
                adBranch = adBranchHome.findByPrimaryKey(adBranchArTaxCode.getAdBranch().getBrCode());


                //AdModBranchDocumentSequenceAssignmentDetails details = new AdModBranchDocumentSequenceAssignmentDetails();

                AdModBranchArTaxCodeDetails details = new AdModBranchArTaxCodeDetails();


                //AdBranchDetails details = new AdBranchDetails();
                details.setBtcBranchCode(adBranch.getBrCode());
                details.setBtcBranchName(adBranch.getBrName());

                if(adBranchArTaxCode.getBtcGlCoaTaxCode() != null){
                	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchArTaxCode.getBtcGlCoaTaxCode());
                    details.setBtcTaxCodeAccountNumber(glChartOfAccount.getCoaAccountNumber());
                    details.setBtcTaxCodeAccountDescription(glChartOfAccount.getCoaAccountDescription());

                }

                if(adBranchArTaxCode.getBtcGlCoaTaxCode() != null){
                	glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchArTaxCode.getBtcGlCoaTaxCode());
                    details.setBtcTaxCodeAccountNumber(glChartOfAccount.getCoaAccountNumber());
                    details.setBtcTaxCodeAccountDescription(glChartOfAccount.getCoaAccountDescription());

                }



	        	list.add(details);


            }

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        return list;


    }*/

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArTcAll(Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("ArTaxCodeControllerBean getArTcAll");

        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;


        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

	        Collection arTaxCodes = arTaxCodeHome.findTcAll(AD_CMPNY);

	        if (arTaxCodes.isEmpty()) {

	            throw new GlobalNoRecordFoundException();

	        }


	        Iterator i = arTaxCodes.iterator();

	        while (i.hasNext()) {

	        	LocalArTaxCode arTaxCode = (LocalArTaxCode)i.next();

	        	ArModTaxCodeDetails mdetails = new ArModTaxCodeDetails();
	        	LocalGlChartOfAccount glTaxCodeChartOfAccount = null;
		        LocalGlChartOfAccount glInterimChartOfAccount = null;


	        		mdetails.setTcCode(arTaxCode.getTcCode());
	                mdetails.setTcName(arTaxCode.getTcName());
	                mdetails.setTcDescription(arTaxCode.getTcDescription());
	                mdetails.setTcType(arTaxCode.getTcType());
	                mdetails.setTcRate(arTaxCode.getTcRate());
	                mdetails.setTcEnable(arTaxCode.getTcEnable());


	             // Get Cash Account Number with Description

	        		if (arTaxCode.getGlChartOfAccount()!= null) {

	        			try {

	        				glTaxCodeChartOfAccount =
	        					glChartOfAccountHome.findByPrimaryKey(arTaxCode.getGlChartOfAccount().getCoaCode());


	        				mdetails.setTcCoaGlTaxAccountNumber(glTaxCodeChartOfAccount.getCoaAccountNumber());
	        				mdetails.setTcCoaGlTaxDescription(glTaxCodeChartOfAccount.getCoaAccountDescription());
	        			} catch (FinderException ex) {

	        			}

	        		}



	                if (arTaxCode.getTcInterimAccount() != null) {

	                	glInterimChartOfAccount =
	        					glChartOfAccountHome.findByPrimaryKey(arTaxCode.getTcInterimAccount());

	                	mdetails.setTcInterimAccountNumber(glInterimChartOfAccount.getCoaAccountNumber());
					    mdetails.setTcInterimAccountDescription(glInterimChartOfAccount.getCoaAccountDescription());

                    }

	        		list.add(mdetails);

	        }

	        return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void addArTcEntry(com.util.ArTaxCodeDetails details, String TC_COA_ACCNT_NMBR, String TC_INTRM_ACCNT_NMBR, ArrayList branchList, Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException,
               GlobalAccountNumberInvalidException,
               ArTCInterimAccountInvalidException {

        Debug.print("ArTaxCodeControllerBean addArTcEntry");

        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;
        LocalAdBranchHome adBranchHome = null;

        LocalAdBranch adBranch = null;
        LocalAdBranchArTaxCode adBranchArTaxCode = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalArTaxCode arTaxCode = null;
            LocalGlChartOfAccount glChartOfAccount = null;
            LocalGlChartOfAccount glInterimChartOfAccount = null;

	        try {

	            arTaxCode = arTaxCodeHome.findByTcName(details.getTcName(), AD_CMPNY);

	            throw new GlobalRecordAlreadyExistException();

	         } catch (FinderException ex) {


	         }

	        // get glChartOfAccount to validate accounts

	        try {

	        	if (TC_COA_ACCNT_NMBR != null && TC_COA_ACCNT_NMBR.length() > 0) {

		            glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
		               TC_COA_ACCNT_NMBR, AD_CMPNY);

                }

	        } catch (FinderException ex) {

	            throw new GlobalAccountNumberInvalidException();

	        }

	         // get glChartOfAccount to validate accounts

	        try {

	        	if (TC_INTRM_ACCNT_NMBR != null && TC_INTRM_ACCNT_NMBR.length() > 0) {

		            glInterimChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
		               TC_INTRM_ACCNT_NMBR, AD_CMPNY);

                }

	        } catch (FinderException ex) {

	            throw new ArTCInterimAccountInvalidException();

	        }

	    	// create new tax code

	    	arTaxCode = arTaxCodeHome.create(
	    			details.getTcName(),
	    	        details.getTcDescription(),
	    	        details.getTcType(),
	    	        glInterimChartOfAccount != null ? glInterimChartOfAccount.getCoaCode() : null,
	    	        details.getTcRate(),
	    	        details.getTcEnable(), AD_CMPNY);

	    	if (TC_COA_ACCNT_NMBR != null && TC_COA_ACCNT_NMBR.length() > 0) {

	    		glChartOfAccount.addArTaxCode(arTaxCode);

	        }






	    	Iterator x = branchList.iterator();
	  	      System.out.println("branchList="+branchList.size());
	  	    System.out.println("--------------------->5");

	  	      while(x.hasNext()) {


	  	    	AdModBranchArTaxCodeDetails brTcDetails = (AdModBranchArTaxCodeDetails)x.next();

	  	    	LocalGlChartOfAccount glTaxCodeCOA = null;
	  	    	LocalGlChartOfAccount glInterimCodeCOA = null;



	  	    	if(brTcDetails.getBtcTaxCodeAccountNumber() != null && brTcDetails.getBtcTaxCodeAccountNumber().length() > 0){

	  	    		try {

	  	    			glTaxCodeCOA = glChartOfAccountHome.findByCoaAccountNumber(
	  	    					brTcDetails.getBtcTaxCodeAccountNumber(), AD_CMPNY);

	  		        } catch (FinderException ex) {

	  		            throw new ArTCCoaGlTaxCodeAccountNotFoundException();

	  		        }

	  	    	}


	  	    	if(brTcDetails.getBtcInterimCodeAccountNumber() != null && brTcDetails.getBtcInterimCodeAccountNumber().length() > 0){

	  	    		try {

	  	    			glInterimCodeCOA = glChartOfAccountHome.findByCoaAccountNumber(
	  	    					brTcDetails.getBtcInterimCodeAccountNumber(), AD_CMPNY);

	  		        } catch (FinderException ex) {

	  		            throw new ArTCCoaGlTaxCodeAccountNotFoundException();

	  		        }

	  	    	}



	  	    	adBranchArTaxCode = adBranchArTaxCodeHome.create(
	  	    			glTaxCodeCOA != null ? glTaxCodeCOA.getCoaCode() : null,
	  	    					glInterimCodeCOA != null ? glInterimCodeCOA.getCoaCode() : null,
	  	    					'N',AD_CMPNY);

	  	        arTaxCode.addAdBranchArTaxCode(adBranchArTaxCode);
	  	        adBranch = adBranchHome.findByPrimaryKey(brTcDetails.getBtcBranchCode());
	  	        adBranch.addAdBranchArTaxCode(adBranchArTaxCode);


	  	      }

        } catch (GlobalRecordAlreadyExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ArTCInterimAccountInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateArTcEntry(com.util.ArTaxCodeDetails details,
    		String TC_COA_ACCNT_NMBR,
    		String TC_INTRM_ACCNT_NMBR,
    		String RS_NM, ArrayList branchList,Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException,
               GlobalAccountNumberInvalidException,
               GlobalRecordAlreadyAssignedException,
               ArTCCoaGlTaxCodeAccountNotFoundException,
               ArTCInterimAccountInvalidException {

        Debug.print("ArTaxCodeControllerBean updateArTcEntry");

        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        LocalAdBranchArTaxCodeHome adBranchArTaxCodeHome = null;
        LocalAdBranchHome adBranchHome = null;

        LocalAdBranch adBranch = null;
        LocalAdBranchArTaxCode adBranchArTaxCode = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBranchArTaxCodeHome = (LocalAdBranchArTaxCodeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchArTaxCodeHome.JNDI_NAME, LocalAdBranchArTaxCodeHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalArTaxCode arTaxCode = null;
        	LocalArTaxCode arExistingTaxCode = null;
        	LocalGlChartOfAccount glChartOfAccount = null;
        	LocalGlChartOfAccount glInterimChartOfAccount = null;

        	try {

	            arExistingTaxCode = arTaxCodeHome.findByTcName(details.getTcName(), AD_CMPNY);

	            if (!arExistingTaxCode.getTcCode().equals(details.getTcCode())) {

	                 throw new GlobalRecordAlreadyExistException();

	            }

            } catch (FinderException ex) {

            }

	        // get glChartOfAccount to validate accounts

	        try {

	        	if (TC_COA_ACCNT_NMBR != null && TC_COA_ACCNT_NMBR.length() > 0) {

		            glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
		               TC_COA_ACCNT_NMBR, AD_CMPNY);

                }

	        } catch (FinderException ex) {

	            throw new GlobalAccountNumberInvalidException();

	        }

	        // get glChartOfAccount to validate accounts

	        try {

	        	if (TC_INTRM_ACCNT_NMBR != null && TC_INTRM_ACCNT_NMBR.length() > 0) {

		            glInterimChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
		               TC_INTRM_ACCNT_NMBR, AD_CMPNY);

                }

	        } catch (FinderException ex) {

	            throw new ArTCInterimAccountInvalidException();

	        }

			// find and update tax code

			arTaxCode = arTaxCodeHome.findByPrimaryKey(details.getTcCode());

	        try {

	        	if((!arTaxCode.getArInvoices().isEmpty() ||
	               !arTaxCode.getArReceipts().isEmpty())&&(details.getTcRate() != arTaxCode.getTcRate())) {

	        		throw new GlobalRecordAlreadyAssignedException();

	        	}

	        } catch (GlobalRecordAlreadyAssignedException ex) {

	        	throw ex;

	        }

			    arTaxCode.setTcName(details.getTcName());
			    arTaxCode.setTcDescription(details.getTcDescription());
			    arTaxCode.setTcType(details.getTcType());
			    arTaxCode.setTcInterimAccount(glInterimChartOfAccount != null ? glInterimChartOfAccount.getCoaCode() : null);
			    System.out.println("details.getTcRate()="+details.getTcRate());
			    arTaxCode.setTcRate(details.getTcRate());
			    arTaxCode.setTcEnable(details.getTcEnable());

			    if (arTaxCode.getGlChartOfAccount() != null) {

			    	arTaxCode.getGlChartOfAccount().dropArTaxCode(arTaxCode);

			    }

		    	if (TC_COA_ACCNT_NMBR != null && TC_COA_ACCNT_NMBR.length() > 0) {

		    		glChartOfAccount.addArTaxCode(arTaxCode);

		        }




		  		Collection adBranchArTaxCodes = adBranchArTaxCodeHome.findBBTCByTcCodeAndRsName(arTaxCode.getTcCode(), RS_NM, AD_CMPNY);


		  	      Iterator i = adBranchArTaxCodes.iterator();

		  	      //remove all adBranchDSA lines
		  	      while(i.hasNext()) {


		  	    	adBranchArTaxCode = (LocalAdBranchArTaxCode) i.next();

		  	    	arTaxCode.dropAdBranchArTaxCode(adBranchArTaxCode);

		  	      	adBranch = adBranchHome.findByPrimaryKey(adBranchArTaxCode.getAdBranch().getBrCode());
		  	      	adBranch.dropAdBranchArTaxCode(adBranchArTaxCode);
		  	      	adBranchArTaxCode.remove();
		  	      }




		    	Iterator x = branchList.iterator();
		  	      System.out.println("branchList="+branchList.size());
		  	    System.out.println("--------------------->5");

		  	      while(x.hasNext()) {


		  	    	AdModBranchArTaxCodeDetails brTcDetails = (AdModBranchArTaxCodeDetails)x.next();

		  	    	LocalGlChartOfAccount glTaxCodeCOA = null;
		  	    	LocalGlChartOfAccount glInterimCodeCOA = null;
		  	    	LocalGlChartOfAccount glBankCOA = null;


		  	    	if(brTcDetails.getBtcTaxCodeAccountNumber() != null && brTcDetails.getBtcTaxCodeAccountNumber().length() > 0){

		  	    		try {

		  	    			glTaxCodeCOA = glChartOfAccountHome.findByCoaAccountNumber(
		  	    					brTcDetails.getBtcTaxCodeAccountNumber(), AD_CMPNY);

		  		        } catch (FinderException ex) {

		  		            throw new ArTCCoaGlTaxCodeAccountNotFoundException();

		  		        }

		  	    	}

		  	    	if(brTcDetails.getBtcInterimCodeAccountNumber() != null && brTcDetails.getBtcInterimCodeAccountNumber().length() > 0){

		  	    		try {

		  	    			glInterimCodeCOA = glChartOfAccountHome.findByCoaAccountNumber(
		  	    					brTcDetails.getBtcInterimCodeAccountNumber(), AD_CMPNY);

		  		        } catch (FinderException ex) {

		  		            throw new ArTCCoaGlTaxCodeAccountNotFoundException();

		  		        }

		  	    	}


		  	    	adBranchArTaxCode = adBranchArTaxCodeHome.create(
		  	    			glTaxCodeCOA != null ? glTaxCodeCOA.getCoaCode() : null,
		  	    					glInterimCodeCOA != null ? glInterimCodeCOA.getCoaCode() : null,
		  	    					'N',AD_CMPNY);

		  	        arTaxCode.addAdBranchArTaxCode(adBranchArTaxCode);
		  	        adBranch = adBranchHome.findByPrimaryKey(brTcDetails.getBtcBranchCode());
		  	        adBranch.addAdBranchArTaxCode(adBranchArTaxCode);

		  	      }

        } catch (GlobalRecordAlreadyExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;



        } catch (ArTCCoaGlTaxCodeAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyAssignedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (ArTCInterimAccountInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteArTcEntry(Integer TC_CODE, Integer AD_CMPNY)
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("ArTaxCodeControllerBean deleteArTcEntry");

        LocalArTaxCodeHome arTaxCodeHome = null;

        // Initialize EJB Home

        try {

            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

      	    LocalArTaxCode arTaxCode = null;

	        try {

	            arTaxCode = arTaxCodeHome.findByPrimaryKey(TC_CODE);

	        } catch (FinderException ex) {

	            throw new GlobalRecordAlreadyDeletedException();

	        }

	        if (!arTaxCode.getArCustomerClasses().isEmpty() ||
	        	!arTaxCode.getArInvoices().isEmpty() ||
				!arTaxCode.getArReceipts().isEmpty()||
				!arTaxCode.getArPdcs().isEmpty() ||
				!arTaxCode.getArSalesOrders().isEmpty()) {

	            throw new GlobalRecordAlreadyAssignedException();

	        }

		    arTaxCode.remove();



        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyAssignedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ArTaxCodeControllerBean ejbCreate");

    }

}

