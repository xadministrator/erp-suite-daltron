
/*
 * ArWithholdingTaxCodeControllerBean.java
 *
 * Created on March 5, 2004, 2:29 PM
 *
 * @author  Neil Andrew M. Ajero
 */
 
package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.ArModWithholdingTaxCodeDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="ArWithholdingTaxCodeControllerEJB"
 *           display-name="Used for entering withholding tax codes"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/ArWithholdingTaxCodeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.ArWithholdingTaxCodeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.ArWithholdingTaxCodeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class ArWithholdingTaxCodeControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArWtcAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("ArWithholdingTaxCodeControllerBean getArWtcAll");

        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {

            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
	        Collection arWithholdingTaxCodes = arWithholdingTaxCodeHome.findWtcAll(AD_CMPNY);
	
	        if (arWithholdingTaxCodes.isEmpty()) {
	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	            
	        Iterator i = arWithholdingTaxCodes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArWithholdingTaxCode arWithholdingTaxCode = (LocalArWithholdingTaxCode)i.next();
	                                                                        
	        	ArModWithholdingTaxCodeDetails mdetails = new ArModWithholdingTaxCodeDetails();
	        		mdetails.setWtcCode(arWithholdingTaxCode.getWtcCode());        		
	                mdetails.setWtcName(arWithholdingTaxCode.getWtcName());                
	                mdetails.setWtcDescription(arWithholdingTaxCode.getWtcDescription());
	                mdetails.setWtcRate(arWithholdingTaxCode.getWtcRate());
	                mdetails.setWtcEnable(arWithholdingTaxCode.getWtcEnable());
	                
	                if (arWithholdingTaxCode.getGlChartOfAccount() != null) {
	                	
	                	mdetails.setWtcCoaGlWithholdingTaxAccountNumber(arWithholdingTaxCode.getGlChartOfAccount().getCoaAccountNumber());	                		                    
					    mdetails.setWtcCoaGlWithholdingTaxAccountDescription(arWithholdingTaxCode.getGlChartOfAccount().getCoaAccountDescription());
					    
                    }					    		    	                          
				    
	        		list.add(mdetails);
	        		
	        }              
	                                                        		        		        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addArWtcEntry(com.util.ArWithholdingTaxCodeDetails details, String WTC_COA_ACCNT_NMBR, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               GlobalAccountNumberInvalidException {
                    
        Debug.print("ArWithholdingTaxCodeControllerBean addArWtcEntry");
        
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
            LocalArWithholdingTaxCode arWithholdingTaxCode = null;	
            LocalGlChartOfAccount glChartOfAccount = null;
        
	        try { 
	            
	            arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName(details.getWtcName(), AD_CMPNY);
	            
	            throw new GlobalRecordAlreadyExistException();
	            
	         } catch (FinderException ex) {
	         	
	        	 	
	         }
	                            
	        // get glChartOfAccount to validate accounts 
	        
	        try {
	        	
	        	if (WTC_COA_ACCNT_NMBR != null && WTC_COA_ACCNT_NMBR.length() > 0) {
	       
		            glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
		               WTC_COA_ACCNT_NMBR, AD_CMPNY);
		               
                }		               
	               
	        } catch (FinderException ex) {
	        
	            throw new GlobalAccountNumberInvalidException();
	                       
	        }
	        
	    	// create new tax code
	    	
	    	arWithholdingTaxCode = arWithholdingTaxCodeHome.create(details.getWtcName(),
	    	        details.getWtcDescription(), details.getWtcRate(),
	    	        details.getWtcEnable(), AD_CMPNY);
	    	        
	    	if (WTC_COA_ACCNT_NMBR != null && WTC_COA_ACCNT_NMBR.length() > 0) {
	    		        
	    		glChartOfAccount.addArWithholdingTaxCode(arWithholdingTaxCode);
	    		
	        }
	        	        
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateArWtcEntry(com.util.ArWithholdingTaxCodeDetails details, String WTC_COA_ACCNT_NMBR, Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException, 
               GlobalAccountNumberInvalidException,
               GlobalRecordAlreadyAssignedException{
                    
        Debug.print("ArWithholdingTaxCodeControllerBean updateArWtcEntry");
        
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;                
        LocalGlChartOfAccountHome glChartOfAccountHome = null;                               
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);           
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalArWithholdingTaxCode arWithholdingTaxCode = null;
        	LocalArWithholdingTaxCode apExistingTaxCode = null;
        	LocalGlChartOfAccount glChartOfAccount = null;
        	
        	try {
                   
	            apExistingTaxCode = arWithholdingTaxCodeHome.findByWtcName(details.getWtcName(), AD_CMPNY);
	            
	            if (!apExistingTaxCode.getWtcCode().equals(details.getWtcCode())) {
	            
	                 throw new GlobalRecordAlreadyExistException();
	                 
	            }
                 
            } catch (FinderException ex) {
            	
            }
            	                                             
	        // get glChartOfAccount to validate accounts 
	        
	        try {
	       
	        	if (WTC_COA_ACCNT_NMBR != null && WTC_COA_ACCNT_NMBR.length() > 0) {
	       
		            glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
		               WTC_COA_ACCNT_NMBR, AD_CMPNY);
		               
                }		               
	               
	        } catch (FinderException ex) {
	        
	            throw new GlobalAccountNumberInvalidException();
	            
	        }	        
          	
			// find and update withholding tax code
			
			arWithholdingTaxCode = arWithholdingTaxCodeHome.findByPrimaryKey(details.getWtcCode());
	        
	        try {
	        	
	        	if ((!arWithholdingTaxCode.getArInvoices().isEmpty() ||
	        		!arWithholdingTaxCode.getArReceipts().isEmpty() ||
					!arWithholdingTaxCode.getAdPreferences().isEmpty())&&(details.getWtcRate() != arWithholdingTaxCode.getWtcRate())) {
	        		
	        		throw new GlobalRecordAlreadyAssignedException();
	        		
	        	}
	        	
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	        	throw ex;
	        	
	        }
				
			    arWithholdingTaxCode.setWtcName(details.getWtcName());
			    arWithholdingTaxCode.setWtcDescription(details.getWtcDescription());
			    arWithholdingTaxCode.setWtcRate(details.getWtcRate());
			    arWithholdingTaxCode.setWtcEnable(details.getWtcEnable());
			    
			    if (arWithholdingTaxCode.getGlChartOfAccount() != null) {
			    	
			    	arWithholdingTaxCode.getGlChartOfAccount().dropArWithholdingTaxCode(arWithholdingTaxCode);
			    	
			    }
			    
		    	if (WTC_COA_ACCNT_NMBR != null && WTC_COA_ACCNT_NMBR.length() > 0) {
		    		        
		    		glChartOfAccount.addArWithholdingTaxCode(arWithholdingTaxCode);
		    		
		        }			    
	                
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	                	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteArWtcEntry(Integer WTC_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("ArWithholdingTaxCodeControllerBean deleteArWtcEntry");
      
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;

        // Initialize EJB Home
        
        try {

            arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);           
      
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
      
        try {
      	
      	    LocalArWithholdingTaxCode arWithholdingTaxCode = null;                
      
	        try {
	      	
	            arWithholdingTaxCode = arWithholdingTaxCodeHome.findByPrimaryKey(WTC_CODE);
	         
	        } catch (FinderException ex) {
	      	
	            throw new GlobalRecordAlreadyDeletedException();
	            
	        }
	         
	        if (!arWithholdingTaxCode.getArCustomerClasses().isEmpty() || 
	        	!arWithholdingTaxCode.getArInvoices().isEmpty() || 
				!arWithholdingTaxCode.getArReceipts().isEmpty() ||
				!arWithholdingTaxCode.getAdPreferences().isEmpty()||
				!arWithholdingTaxCode.getArPdcs().isEmpty()) {
	  
	            throw new GlobalRecordAlreadyAssignedException();
	         
	        }
	                  	
		    arWithholdingTaxCode.remove();
	    
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }         
   
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("ArWithholdingTaxCodeControllerBean ejbCreate");
      
    }
    
}

