
/*
 * CmAdjustmentEntryControllerBean.java
 *
 * Created on November 19, 2003, 01:11 PM
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchBankAccount;
import com.ejb.ad.LocalAdBranchBankAccountHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ar.LocalArAppliedCredit;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.hr.LocalHrPayrollPeriod;
import com.ejb.hr.LocalHrPayrollPeriodHome;
import com.util.AbstractSessionBean;
import com.util.AdModBankAccountDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModSalesOrderDetails;
import com.util.CmModAdjustmentDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmAdjustmentEntryControllerEJB"
 *           display-name="Used for entering cash adjustments"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmAdjustmentEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmAdjustmentEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmAdjustmentEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 *
*/

public class CmAdjustmentEntryControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("CmFundTransferEntryControllerBean getAdBaAll");

        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

        	Iterator i = adBankAccounts.iterator();

        	while (i.hasNext()) {

        		LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();

        		list.add(adBankAccount.getBaName());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.CmModAdjustmentDetails getCmAdjByAdjCode(Integer ADJ_CODE, Integer AD_CMPNY) {

        Debug.print("CmAdjustmentEntryControllerBean getCmAdjByAdjCode");

        LocalCmAdjustmentHome cmAdjustmentHome = null;


        //initialized EJB Home

        try {

            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalCmAdjustment cmAdjustment = cmAdjustmentHome.findByPrimaryKey(ADJ_CODE);

            CmModAdjustmentDetails mdetails = new CmModAdjustmentDetails();

            mdetails.setAdjCode(cmAdjustment.getAdjCode());
            mdetails.setAdjType(cmAdjustment.getAdjType());
            mdetails.setAdjDate(cmAdjustment.getAdjDate());

            if(cmAdjustment.getArCustomer()!=null){
            	mdetails.setAdjCustomerCode(cmAdjustment.getArCustomer().getCstCustomerCode());
            	 mdetails.setAdjCustomerName(cmAdjustment.getArCustomer().getCstName() );
            }

            mdetails.setAdjDocumentNumber(cmAdjustment.getAdjDocumentNumber());
            mdetails.setAdjReferenceNumber(cmAdjustment.getAdjReferenceNumber());
            mdetails.setAdjCheckNumber(cmAdjustment.getAdjCheckNumber());
            mdetails.setAdjAmount(cmAdjustment.getAdjAmount());

            if(cmAdjustment.getArSalesOrder()!=null) {
            	mdetails.setAdjSoNumber(cmAdjustment.getArSalesOrder().getSoDocumentNumber());
            	mdetails.setAdjSoReferenceNumber(cmAdjustment.getAdjReferenceNumber());
            }


            // get applied credit

            Collection arAppliedCredits = cmAdjustment.getArAppliedCredits();

            Iterator x = arAppliedCredits.iterator();

            double totalAppliedCredit = 0d;

            while(x.hasNext()){

            	LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)x.next();

            	totalAppliedCredit += arAppliedCredit.getAcApplyCredit();

            }

            mdetails.setAdjAmountApplied(totalAppliedCredit);
            mdetails.setAdjConversionDate(cmAdjustment.getAdjConversionDate());
            mdetails.setAdjConversionRate(cmAdjustment.getAdjConversionRate());
            mdetails.setAdjMemo(cmAdjustment.getAdjMemo());
            mdetails.setAdjVoid(cmAdjustment.getAdjVoid());

            mdetails.setAdjRefund(cmAdjustment.getAdjRefund());
            mdetails.setAdjRefundAmount(cmAdjustment.getAdjRefundAmount());
            mdetails.setAdjRefundReferenceNumber(cmAdjustment.getAdjRefundReferenceNumber());

            mdetails.setAdjApprovalStatus(cmAdjustment.getAdjApprovalStatus());
        	mdetails.setAdjPosted(cmAdjustment.getAdjPosted());
        	mdetails.setAdjVoidApprovalStatus(cmAdjustment.getAdjVoidApprovalStatus());
        	mdetails.setAdjVoidPosted(cmAdjustment.getAdjVoidPosted());
        	mdetails.setAdjCreatedBy(cmAdjustment.getAdjCreatedBy());
        	mdetails.setAdjDateCreated(cmAdjustment.getAdjDateCreated());
        	mdetails.setAdjLastModifiedBy(cmAdjustment.getAdjLastModifiedBy());
        	mdetails.setAdjDateLastModified(cmAdjustment.getAdjDateLastModified());
        	mdetails.setAdjApprovedRejectedBy(cmAdjustment.getAdjApprovedRejectedBy());
        	mdetails.setAdjDateApprovedRejected(cmAdjustment.getAdjDateApprovedRejected());
        	mdetails.setAdjPostedBy(cmAdjustment.getAdjPostedBy());
        	mdetails.setAdjDatePosted(cmAdjustment.getAdjDatePosted());
            mdetails.setAdjBaName(cmAdjustment.getAdBankAccount().getBaName());
            mdetails.setAdjBaFcName(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName());
            mdetails.setAdjReasonForRejection(cmAdjustment.getAdjReasonForRejection());

            Collection cmAdjustments = cmAdjustment.getCmDistributionRecords();
            Iterator y = cmAdjustments.iterator();
            double totalDebit = 0;
            double totalCredit = 0;
            while(y.hasNext()){
            	LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)y.next();
            	if(cmDistributionRecord.getDrDebit() == EJBCommon.TRUE){
            		totalDebit += cmDistributionRecord.getDrAmount();
            	} else {
            		totalCredit += cmDistributionRecord.getDrAmount();

            	}

            }

            System.out.println("TOTAL DEBIT = "+totalDebit);
            System.out.println("TOTAL CREDIT = "+totalCredit);

            mdetails.setAdjTotalDebit(totalDebit);
            mdetails.setAdjTotalCredit(totalCredit);


            return mdetails;

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void checkCmAdjEntryUpload(com.util.CmAdjustmentDetails details,
        String BA_NM, String FC_NM, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalTransactionAlreadyApprovedException,
        GlobalConversionDateNotExistException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalDocumentNumberNotUniqueException,
		GlobalBranchAccountNumberInvalidException {



	   Debug.print("CmAdjustmentEntryControllerBean checkCmAdjEntryUpload");

	   LocalCmAdjustmentHome cmAdjustmentHome = null;
	   LocalAdBankAccountHome adBankAccountHome = null;
	   LocalAdApprovalHome adApprovalHome = null;
	   LocalAdAmountLimitHome adAmountLimitHome = null;
	   LocalAdApprovalUserHome adApprovalUserHome = null;
	   LocalAdApprovalQueueHome adApprovalQueueHome = null;
	   LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
	   LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
	   LocalAdPreferenceHome adPreferenceHome = null;
	   LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
	   LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
	   LocalAdCompanyHome adCompanyHome = null;

	   //initialized EJB Home

       try {

           cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
               lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
           adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
           adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
           adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
           adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
           adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
           glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
           glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
           adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
           	lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       LocalCmAdjustment cmAdjustment = null;


       try {

       	// validate if adjustment is already deleted

       	try {

       		if (details.getAdjCode() != null) {

       			cmAdjustment = cmAdjustmentHome.findByPrimaryKey(details.getAdjCode());

       		}

       	} catch (FinderException ex) {

       		throw new GlobalRecordAlreadyDeletedException();

       	}

       	// validate if adjustment is already posted, void, arproved or pending

           if (details.getAdjCode() != null) {

	    		if (cmAdjustment.getAdjApprovalStatus() != null) {

	        		if (cmAdjustment.getAdjApprovalStatus().equals("APPROVED") ||
	        		    cmAdjustment.getAdjApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();


	        		} else if (cmAdjustment.getAdjApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	            }

	            if (cmAdjustment.getAdjPosted() == EJBCommon.TRUE) {

       			throw new GlobalTransactionAlreadyPostedException();

       		} else if (cmAdjustment.getAdjVoid() == EJBCommon.TRUE) {

       			throw new GlobalTransactionAlreadyVoidException();

       		}

	        }



	    	// validate if document number is unique document number is automatic then set next sequence

	    	if (details.getAdjCode() == null) {

       		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
       		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	    		try {

	    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("CM ADJUSTMENT", AD_CMPNY);

	    		} catch (FinderException ex) {

	    		}

				try {

					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

				}

	    		LocalCmAdjustment cmExistingAdjustment = null;

	    		try {

	    		    cmExistingAdjustment = cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	            if (cmExistingAdjustment != null) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }



		    } else {

		    	LocalCmAdjustment cmExistingAdjustment = null;

		    	try {

		    		cmExistingAdjustment = cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {

	        	}

	        	if (cmExistingAdjustment != null &&
	                !cmExistingAdjustment.getAdjCode().equals(details.getAdjCode())) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }



		    }

	    	// validate if conversion date exists

	        try {

	      	    if (details.getAdjConversionDate() != null) {

	      	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	      	    	LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
	      	    		FC_NM, AD_CMPNY);

	 				if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

	 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
	 								details.getAdjConversionDate(), AD_CMPNY);

	 				} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

	 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(
	 							adCompany.getGlFunctionalCurrency().getFcCode(), details.getAdjConversionDate(), AD_CMPNY);

	 				}

		        }

	        } catch (FinderException ex) {

	        	 throw new GlobalConversionDateNotExistException();

	        }

       }catch (GlobalDocumentNumberNotUniqueException ex) {

   		getSessionContext().setRollbackOnly();
   		throw ex;

   	} catch (GlobalRecordAlreadyDeletedException ex) {

   		getSessionContext().setRollbackOnly();
   		throw ex;

    	} catch (GlobalConversionDateNotExistException ex) {

    	    getSessionContext().setRollbackOnly();
   		throw ex;

   	} catch (GlobalTransactionAlreadyApprovedException ex) {

       	getSessionContext().setRollbackOnly();
       	throw ex;

       } catch (GlobalTransactionAlreadyPendingException ex) {

       	getSessionContext().setRollbackOnly();
       	throw ex;

       } catch (GlobalTransactionAlreadyPostedException ex) {

       	getSessionContext().setRollbackOnly();
       	throw ex;

       } catch (GlobalTransactionAlreadyVoidException ex) {

       	getSessionContext().setRollbackOnly();
       	throw ex;



       } catch (Exception ex) {

           ex.printStackTrace();
           getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
       }

    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveCmAdjRefundDetails(com.util.ApCheckDetails details, Integer CHK_CODE, String ADJ_DCMNT_NMBR, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException {


    	 LocalCmAdjustmentHome cmAdjustmentHome = null;
    	 LocalApCheckHome apCheckHome = null;

    	//initialized EJB Home

         try {

             cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                 lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
             apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                     lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);


         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         LocalCmAdjustment cmAdjustment = null;
         LocalApCheck apCheck = null;

    	try {


    		try {

				if (CHK_CODE != null) {

					apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}


    		// validate if adjustment is already deleted

        	try {

        		if (ADJ_DCMNT_NMBR != null) {

        			cmAdjustment = cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(ADJ_DCMNT_NMBR, AD_BRNCH, AD_CMPNY);

        		}

        		cmAdjustment.setAdjRefund(EJBCommon.TRUE);
        		cmAdjustment.setAdjRefundAmount(apCheck.getChkAmount());
        		cmAdjustment.setAdjRefundReferenceNumber(apCheck.getChkDocumentNumber());

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

    	} catch (GlobalRecordAlreadyDeletedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;
    	}


    }




    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveCmAdjRefundDetailsByAdjCode(com.util.ApCheckDetails details, Integer CHK_CODE, Integer ADJ_CODE, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException {


    	 LocalCmAdjustmentHome cmAdjustmentHome = null;
    	 LocalApCheckHome apCheckHome = null;

    	//initialized EJB Home

         try {

             cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                 lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
             apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                     lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);


         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

         }

         LocalCmAdjustment cmAdjustment = null;
         LocalApCheck apCheck = null;

    	try {


    		try {

				if (CHK_CODE != null) {

					apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}


    		// validate if adjustment is already deleted

        	try {

        		if (ADJ_CODE != null) {

        			cmAdjustment = cmAdjustmentHome.findByPrimaryKey(ADJ_CODE);

        		}

        		// get applied credit

	              Collection arAppliedCredits = cmAdjustment.getArAppliedCredits();

	              Iterator y = arAppliedCredits.iterator();

	              double totalAppliedCredit = 0d;

	              while(y.hasNext()){

	              	LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)y.next();

	              	totalAppliedCredit += arAppliedCredit.getAcApplyCredit();

	              }


        		cmAdjustment.setAdjRefund(EJBCommon.TRUE);
        		//cmAdjustment.setAdjRefundAmount(apCheck.getChkAmount());
        		cmAdjustment.setAdjRefundAmount(cmAdjustment.getAdjAmount() - totalAppliedCredit - cmAdjustment.getAdjRefundAmount());
        		cmAdjustment.setAdjRefundReferenceNumber(apCheck.getChkDocumentNumber());

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

    	} catch (GlobalRecordAlreadyDeletedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;
    	}


    }


    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveCmAdjEntry(com.util.CmAdjustmentDetails details, String BA_NM,
        String CST_NM, String SO_NMBR, String FC_NM, boolean isDraft,Integer PYRLL_PRD, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalTransactionAlreadyApprovedException,
        GlobalConversionDateNotExistException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalRecordAlreadyAssignedException,
		GlobalDocumentNumberNotUniqueException,
		GlobalBranchAccountNumberInvalidException {

        Debug.print("CmAdjustmentEntryControllerBean saveCmAdjEntry");

        LocalCmAdjustmentHome cmAdjustmentHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalHrPayrollPeriodHome hrPayrollPeriodHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalArSalesOrderHome arSalesOrderHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBranchBankAccountHome adBranchBankAccountHome = null;


        //initialized EJB Home

        try {

            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
            hrPayrollPeriodHome = (LocalHrPayrollPeriodHome)EJBHomeFactory.
                    lookUpLocalHome(LocalHrPayrollPeriodHome.JNDI_NAME, LocalHrPayrollPeriodHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            arSalesOrderHome =  (LocalArSalesOrderHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
 			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);
 			adBranchBankAccountHome = (LocalAdBranchBankAccountHome)EJBHomeFactory.
 					lookUpLocalHome(LocalAdBranchBankAccountHome.JNDI_NAME,LocalAdBranchBankAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        System.out.print("Bank accoutn name is: " + BA_NM);
        LocalCmAdjustment cmAdjustment = null;

        try {

        	// validate if adjustment is already deleted

        	try {

        		if (details.getAdjCode() != null) {

        			cmAdjustment = cmAdjustmentHome.findByPrimaryKey(details.getAdjCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if adjustment is already posted, void, arproved or pending , refund

            if (details.getAdjCode() != null && details.getAdjVoid()  == EJBCommon.FALSE && details.getAdjRefund()  == EJBCommon.FALSE) {

	    		if (cmAdjustment.getAdjApprovalStatus() != null) {

	        		if (cmAdjustment.getAdjApprovalStatus().equals("APPROVED") ||
	        		    cmAdjustment.getAdjApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();


	        		} else if (cmAdjustment.getAdjApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	            }

	            if (cmAdjustment.getAdjPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (cmAdjustment.getAdjVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		} else if (cmAdjustment.getAdjRefund() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

	        }




    	    // adjustment void

	    	if (details.getAdjCode() != null && details.getAdjVoid() == EJBCommon.TRUE) {



	    		if (cmAdjustment.getAdjVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

	    		// check  if adjusment is already used

	    		System.out.println("cmAdjustment.getArAppliedCredits().isEmpty()="+cmAdjustment.getArAppliedCredits().size());
	        	if (!cmAdjustment.getArAppliedCredits().isEmpty()){

	        		throw new GlobalRecordAlreadyAssignedException ();

	        	}


	    		if( cmAdjustment.getAdjPosted() == EJBCommon.TRUE){

	    			// generate approval status

	                String ADJ_APPRVL_STATUS = null;

	            	if (!isDraft) {

	            		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

	            		// check if adjustment approval is enabled

	            		if (adApproval.getAprEnableCmAdjustment() == EJBCommon.FALSE) {

	            			ADJ_APPRVL_STATUS = "N/A";

	            		} else {

	            			// check if adjustment is self approved

	            			LocalAdAmountLimit adAmountLimit = null;

	            			try {

	            				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("CM ADJUSTMENT", "REQUESTER", details.getAdjLastModifiedBy(), AD_CMPNY);

	            			} catch (FinderException ex) {

	            				throw new GlobalNoApprovalRequesterFoundException();

	            			}

	            			if (cmAdjustment.getAdjAmount() <= adAmountLimit.getCalAmountLimit()) {

	            				ADJ_APPRVL_STATUS = "N/A";

	            			} else {

	            				// for approval, create approval queue

	            				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("CM ADJUSTMENT", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

	            				 if (adAmountLimits.isEmpty()) {

	            				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

	            				 	if (adApprovalUsers.isEmpty()) {

	            				 		throw new GlobalNoApprovalApproverFoundException();

	            				 	}

	            				 	Iterator j = adApprovalUsers.iterator();

	            				 	while (j.hasNext()) {

	            				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

	            				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "CM ADJUSTMENT", cmAdjustment.getAdjCode(),
	            				 				cmAdjustment.getAdjDocumentNumber(), cmAdjustment.getAdjDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

	            				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

	            				 	}

	            				 } else {

	            				 	boolean isApprovalUsersFound = false;

	            				 	Iterator n = adAmountLimits.iterator();

	            				 	while (n.hasNext()) {

	            				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)n.next();

	            				 		if (cmAdjustment.getAdjAmount() <= adNextAmountLimit.getCalAmountLimit()) {

	            				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

	            				 			Iterator j = adApprovalUsers.iterator();

	    		        				 	while (j.hasNext()) {

	    		        				 		isApprovalUsersFound = true;

	    		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

	    		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "CM ADJUSTMENT", cmAdjustment.getAdjCode(),
	    		        				 				cmAdjustment.getAdjDocumentNumber(), cmAdjustment.getAdjDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

	    		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

	    		        				 	}

	    	    				 			break;

	            				 		} else if (!n.hasNext()) {

	            				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

	            				 			Iterator j = adApprovalUsers.iterator();

	    		        				 	while (j.hasNext()) {

	    		        				 		isApprovalUsersFound = true;

	    		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

	    		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "CM ADJUSTMENT", cmAdjustment.getAdjCode(),
	    		        				 				cmAdjustment.getAdjDocumentNumber(), cmAdjustment.getAdjDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

	    		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

	    		        				 	}

	    	    				 			break;

	            				 		}

	            				 		adAmountLimit = adNextAmountLimit;

	            				 	}

	            				 	if (!isApprovalUsersFound) {

	            				 		throw new GlobalNoApprovalApproverFoundException();

	            				 	}

	            			    }

	            			    ADJ_APPRVL_STATUS = "PENDING";
	            			}
	            		}
	            	}



	            	// reverse distribution records

		    		Collection cmDistributionRecords = cmAdjustment.getCmDistributionRecords();
		    		ArrayList list = new ArrayList();

		    		Iterator i = cmDistributionRecords.iterator();

		    		while (i.hasNext()) {

		    			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)i.next();

		    			list.add(cmDistributionRecord);

		    		}

		    		i = list.iterator();

		    		while (i.hasNext()) {

		    			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)i.next();
		    			System.out.println("Check Point A");
		    			this.addCmDrEntry(cmAdjustment.getCmDrNextLine(), cmDistributionRecord.getDrClass(),
		    					cmDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
		    							cmDistributionRecord.getDrAmount(), cmDistributionRecord.getGlChartOfAccount().getCoaCode(),
		    			    cmAdjustment,  AD_BRNCH, AD_CMPNY);

		    		}


		    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

		        	if (ADJ_APPRVL_STATUS != null && ADJ_APPRVL_STATUS.equals("N/A") && adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

		        		cmAdjustment.setAdjVoid(EJBCommon.TRUE);
		        		this.executeCmAdjPost(cmAdjustment.getAdjCode(), details.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

		        	}

		        	// set void approval status

		        	cmAdjustment.setAdjVoidApprovalStatus(ADJ_APPRVL_STATUS);
	    		}

        		cmAdjustment.setAdjVoid(EJBCommon.TRUE);
        		cmAdjustment.setAdjLastModifiedBy(details.getAdjLastModifiedBy());
        		cmAdjustment.setAdjDateLastModified(details.getAdjDateLastModified());

        		return cmAdjustment.getAdjCode();

	    	}

	    	// validate if document number is unique document number is automatic then set next sequence

	    	if (details.getAdjCode() == null) {

        		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
        		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	    		try {

	    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("CM ADJUSTMENT", AD_CMPNY);

	    		} catch (FinderException ex) {

	    		}

 				try {

 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

 				} catch (FinderException ex) {

 				}

	    		LocalCmAdjustment cmExistingAdjustment = null;

	    		try {

	    		    cmExistingAdjustment = cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {
	        	}

	            if (cmExistingAdjustment != null) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

		        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
		            (details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {

		            while (true) {

		            	if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

			            	try {

			            		cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

			            	} catch (FinderException ex) {

			            		details.setAdjDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
					            break;

			            	}

		            	} else {

		            		try {

			            		cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

			            	} catch (FinderException ex) {

			            		details.setAdjDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
					            break;

			            	}

		            	}

		            }

		        }

		    } else {

		    	LocalCmAdjustment cmExistingAdjustment = null;

		    	try {

		    		cmExistingAdjustment = cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {

	        	}

	        	if (cmExistingAdjustment != null &&
	                !cmExistingAdjustment.getAdjCode().equals(details.getAdjCode())) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

	            if (cmAdjustment.getAdjDocumentNumber() != details.getAdjDocumentNumber() &&
	                (details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {

	                details.setAdjDocumentNumber(cmAdjustment.getAdjDocumentNumber());

	         	}

		    }

	    	// validate if conversion date exists

	        try {

	      	    if (details.getAdjConversionDate() != null) {

	      	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	      	    	LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
	      	    		FC_NM, AD_CMPNY);

	 				if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

	 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
	 								details.getAdjConversionDate(), AD_CMPNY);

	 				} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

	 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
	 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(
	 							adCompany.getGlFunctionalCurrency().getFcCode(), details.getAdjConversionDate(), AD_CMPNY);

	 				}

		        }

	        } catch (FinderException ex) {

	        	 throw new GlobalConversionDateNotExistException();

	        }

	        // used in checking if adjustment should re-generate distribution records

	        boolean isRecalculate = true;

            if (details.getAdjCode() == null) {

	            cmAdjustment = cmAdjustmentHome.create(
	            	 details.getAdjType(), details.getAdjDate(),
	            	 details.getAdjDocumentNumber(), details.getAdjReferenceNumber(), details.getAdjCheckNumber(),
	            	 details.getAdjAmount(), 0d,
	            	 details.getAdjConversionDate(), details.getAdjConversionRate(),
	             	 details.getAdjMemo(),null,
	             	 EJBCommon.FALSE , EJBCommon.FALSE, EJBCommon.FALSE, 0d,
	             	 null, EJBCommon.FALSE, null,
	             	 null, EJBCommon.FALSE,

	             	 details.getAdjCreatedBy(),
	             	 details.getAdjDateCreated(),
        	    	 details.getAdjLastModifiedBy(),
        	    	 details.getAdjDateLastModified(),
        	    	 null, null, null, null, null,
        	    	 AD_BRNCH, AD_CMPNY);

        	} else {

        		// check if critical fields are changed

        		if (!cmAdjustment.getAdjType().equals(details.getAdjType()) ||
        		    cmAdjustment.getAdjAmount() != details.getAdjAmount() ||
					!cmAdjustment.getAdBankAccount().getBaName().equals(BA_NM)) {

        			isRecalculate = true;

        		} else {

        			isRecalculate = false;

        		}

		 		// update adjustment

		 		cmAdjustment.setAdjType(details.getAdjType());
	            cmAdjustment.setAdjDate(details.getAdjDate());
	            cmAdjustment.setAdjDocumentNumber(details.getAdjDocumentNumber());
	            cmAdjustment.setAdjReferenceNumber(details.getAdjReferenceNumber());
	            cmAdjustment.setAdjCheckNumber(details.getAdjCheckNumber());
	            cmAdjustment.setAdjAmount(details.getAdjAmount());
	            cmAdjustment.setAdjAmountApplied(details.getAdjAmountApplied());
	            cmAdjustment.setAdjConversionDate(details.getAdjConversionDate());
	            cmAdjustment.setAdjConversionRate(details.getAdjConversionRate());
	            cmAdjustment.setAdjMemo(details.getAdjMemo());
	            cmAdjustment.setAdjLastModifiedBy(details.getAdjLastModifiedBy());
        		cmAdjustment.setAdjDateLastModified(details.getAdjDateLastModified());
	            cmAdjustment.setAdjReasonForRejection(null);

        	}
        	System.out.println("flag 1 ");
        	System.out.println(BA_NM + "bank account");
     	    //LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

     	   if(PYRLL_PRD != null) {

     		  try {
        		   LocalHrPayrollPeriod hrPayrollPeriod = hrPayrollPeriodHome.findByPrimaryKey(PYRLL_PRD);

        		   cmAdjustment.setHrPayrollPeriod(hrPayrollPeriod);
   	     	 } catch(FinderException ex) {

   	         }

     	   }


     	  LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaNameAndBrCode(BA_NM, AD_BRNCH, AD_CMPNY);

     	  LocalAdBranchBankAccount adBranchBankAccount = null;
          Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry J");
          try {
              adBranchBankAccount = adBranchBankAccountHome.findBbaByBaCodeAndBrCode(adBankAccount.getBaCode(), AD_BRNCH, AD_CMPNY);

          } catch(FinderException ex) {

          }
          adBranchBankAccount.getAdBankAccount().addCmAdjustment(cmAdjustment);

	    	System.out.println("flag 2 ");
	        if (isRecalculate) {

		        // remove all distribution records
	        	System.out.println("flag 3");
		  	    Collection cmDistributionRecords = cmAdjustment.getCmDistributionRecords();

		  	    Iterator i = cmDistributionRecords.iterator();

		  	    while (i.hasNext()) {

		  	   	    LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)i.next();

		  	  	    i.remove();

		  	  	    cmDistributionRecord.remove();

		  	    }
		  	  System.out.println("AdjustmentType is: " + cmAdjustment.getAdjType());
		  		System.out.println("flag 4");
		  	    // add new distribution record

		  	    if (cmAdjustment.getAdjType().equals("DEBIT MEMO")) {
		  	  	System.out.println("flag 4.1 ");
		  	  	
		  	  		this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
		      	  	    "ADJUSTMENT", EJBCommon.TRUE, cmAdjustment.getAdjAmount(),
		      	  	adBranchBankAccount.getBbaGlCoaAdjustmentAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

		  	  	
		  	    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
		      	  	    "CASH", EJBCommon.FALSE, cmAdjustment.getAdjAmount(),
		      	  	adBranchBankAccount.getBbaGlCoaCashAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

		      	  	
		  	    } else if (cmAdjustment.getAdjType().equals("CREDIT MEMO")) {
		  	   	System.out.println("flag 4.2 ");
		  	   	
		  	   		this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
		      	  	    "CASH", EJBCommon.TRUE, cmAdjustment.getAdjAmount(),
		      	  	adBranchBankAccount.getBbaGlCoaCashAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);
		      	  	
		  	    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
		      	  	    "ADJUSTMENT", EJBCommon.FALSE, cmAdjustment.getAdjAmount(),
		      	  	adBranchBankAccount.getBbaGlCoaAdjustmentAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

		  	    	

		  	    } else if (cmAdjustment.getAdjType().equals("INTEREST")){
		  	   	System.out.println("flag 4.3 ");
		  	    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
		      	  	    "CASH", EJBCommon.TRUE, cmAdjustment.getAdjAmount(),
		      	  	adBranchBankAccount.getBbaGlCoaCashAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

		      	  	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
		      	  	    "INTEREST", EJBCommon.FALSE, cmAdjustment.getAdjAmount(),
		      	  	adBranchBankAccount.getBbaGlCoaInterestAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

		  	    } else if (cmAdjustment.getAdjType().equals("BANK CHARGE")) {
		  	   	System.out.println("flag 4.4 ");
		  	    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
		      	  	    "BANK CHARGE", EJBCommon.TRUE, cmAdjustment.getAdjAmount(),
		      	  	adBranchBankAccount.getBbaGlCoaBankChargeAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

		  	    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
		      	  	    "CASH", EJBCommon.FALSE, cmAdjustment.getAdjAmount(),
		      	  	adBranchBankAccount.getBbaGlCoaCashAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);


		  	    } else if (cmAdjustment.getAdjType().equals("ADVANCE")) {
		  	   	System.out.println("flag 4.5 ");
		  	    	System.out.println("test 1");



		  	    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
		      	  	    "ADVANCE", EJBCommon.FALSE, cmAdjustment.getAdjAmount(),
		      	  	adBranchBankAccount.getBbaGlCoaAdvanceAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);


		  	    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
			      	  	    "CASH", EJBCommon.TRUE, cmAdjustment.getAdjAmount(),
			      	  	adBranchBankAccount.getBbaGlCoaCashAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);


		  	  	System.out.println("test 3="+CST_NM);
		  	    	LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_NM,AD_CMPNY);

		  	    	arCustomer.addCmAdjustment(cmAdjustment);

		  	    } else if (cmAdjustment.getAdjType().equals("SO ADVANCE")) {
		  	   	System.out.println("flag 4.5 ");
		  	    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
			      	  	    "ADVANCE", EJBCommon.FALSE, cmAdjustment.getAdjAmount(),
		      	  	adBranchBankAccount.getBbaGlCoaAdvanceAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);


		  	    	this.addCmDrEntry(cmAdjustment.getCmDrNextLine(),
			      	  	    "CASH", EJBCommon.TRUE, cmAdjustment.getAdjAmount(),
			      	  	adBranchBankAccount.getBbaGlCoaCashAccount(), cmAdjustment, AD_BRNCH, AD_CMPNY);

		  	    	LocalArSalesOrder arSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndBrCode(SO_NMBR, AD_BRNCH, AD_CMPNY);

		  	    	cmAdjustment.setArSalesOrder(arSalesOrder);
		  	    }

		  	}


        	// generate approval status

            String ADJ_APPRVL_STATUS = null;

        	if (!isDraft) {

        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

        		// check if adjustment approval is enabled

        		if (adApproval.getAprEnableCmAdjustment() == EJBCommon.FALSE) {

        			ADJ_APPRVL_STATUS = "N/A";

        		} else {

        			// check if adjustment is self approved

        			LocalAdAmountLimit adAmountLimit = null;

        			try {

        				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("CM ADJUSTMENT", "REQUESTER", details.getAdjLastModifiedBy(), AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalNoApprovalRequesterFoundException();

        			}

        			if (cmAdjustment.getAdjAmount() <= adAmountLimit.getCalAmountLimit()) {

        				ADJ_APPRVL_STATUS = "N/A";

        			} else {

        				// for approval, create approval queue

        				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("CM ADJUSTMENT", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

        				 if (adAmountLimits.isEmpty()) {

        				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 	if (adApprovalUsers.isEmpty()) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        				 	Iterator j = adApprovalUsers.iterator();

        				 	while (j.hasNext()) {

        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "CM ADJUSTMENT", cmAdjustment.getAdjCode(),
        				 				cmAdjustment.getAdjDocumentNumber(), cmAdjustment.getAdjDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

        				 	}

        				 } else {

        				 	boolean isApprovalUsersFound = false;

        				 	Iterator n = adAmountLimits.iterator();

        				 	while (n.hasNext()) {

        				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)n.next();

        				 		if (cmAdjustment.getAdjAmount() <= adNextAmountLimit.getCalAmountLimit()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "CM ADJUSTMENT", cmAdjustment.getAdjCode(),
		        				 				cmAdjustment.getAdjDocumentNumber(), cmAdjustment.getAdjDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		} else if (!n.hasNext()) {

        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

        				 			Iterator j = adApprovalUsers.iterator();

		        				 	while (j.hasNext()) {

		        				 		isApprovalUsersFound = true;

		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "CM ADJUSTMENT", cmAdjustment.getAdjCode(),
		        				 				cmAdjustment.getAdjDocumentNumber(), cmAdjustment.getAdjDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

		        				 	}

	    				 			break;

        				 		}

        				 		adAmountLimit = adNextAmountLimit;

        				 	}

        				 	if (!isApprovalUsersFound) {

        				 		throw new GlobalNoApprovalApproverFoundException();

        				 	}

        			    }

        			    ADJ_APPRVL_STATUS = "PENDING";
        			}
        		}
        	}

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	if (ADJ_APPRVL_STATUS != null && ADJ_APPRVL_STATUS.equals("N/A") && adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		this.executeCmAdjPost(cmAdjustment.getAdjCode(), cmAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}

        	// set adjustment approval status

        	cmAdjustment.setAdjApprovalStatus(ADJ_APPRVL_STATUS);

      	    return cmAdjustment.getAdjCode();

        } catch (GlobalDocumentNumberNotUniqueException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalRecordAlreadyDeletedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

     	} catch (GlobalConversionDateNotExistException ex) {

     	    getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalTransactionAlreadyApprovedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

         } catch (GlobalNoApprovalRequesterFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

         } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

         } catch (GlobalRecordAlreadyAssignedException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

            ex.printStackTrace();
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList getCmCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("CmAdjustmentBean getCmCstAll");

        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

            Iterator i = arCustomers.iterator();

            while (i.hasNext()) {

                LocalArCustomer arCustomer = (LocalArCustomer)i.next();
                //System.out.println( arCustomer.getCstSquareMeter()+"#"+arCustomer.getCstNumbersParking());
                list.add(arCustomer.getCstCustomerCode());


            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }




    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList getCmSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("CmAdjustmentBean getCmSplAll");

        LocalApSupplierHome apSupplierHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
            lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);

            Iterator i = apSuppliers.iterator();

            while (i.hasNext()) {

                LocalApSupplier apSupplier = (LocalApSupplier)i.next();
                //System.out.println( arCustomer.getCstSquareMeter()+"#"+arCustomer.getCstNumbersParking());
                list.add(apSupplier.getSplSupplierCode());


            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArModCustomerDetails getCmCstByCstCustomerCode(String CST_CSTMR_CODE, Integer AD_CMPNY)
    		throws GlobalNoRecordFoundException {

	        Debug.print("CmAdjustment getCmCstByCstCustomerCode");

	        LocalArCustomerHome arCustomerHome = null;
	        LocalArSalespersonHome arSalespersonHome = null;
	        // Initialize EJB Home

	        try {

	        	arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
	            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
	            lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

	        } catch (NamingException ex) {

	            throw new EJBException(ex.getMessage());

	        }

	        try {

	            LocalArCustomer arCustomer = null;


	            try {

	                arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

	            } catch (FinderException ex) {

	                throw new GlobalNoRecordFoundException();

	            }

	            ArModCustomerDetails mdetails = new ArModCustomerDetails();



	            mdetails.setCstCustomerCode(arCustomer.getCstCustomerCode());
	            mdetails.setCstPytName(arCustomer.getAdPaymentTerm() != null ?
	                    arCustomer.getAdPaymentTerm().getPytName() : null);
	            mdetails.setCstCcWtcName(arCustomer.getArCustomerClass().getArWithholdingTaxCode() != null ?
	                    arCustomer.getArCustomerClass().getArWithholdingTaxCode().getWtcName() : null);
	            mdetails.setCstBillToAddress(arCustomer.getCstBillToAddress());
	            mdetails.setCstShipToAddress(arCustomer.getCstShipToAddress());
	            mdetails.setCstBillToAddress(arCustomer.getCstBillToAddress());
	            mdetails.setCstBillToContact(arCustomer.getCstBillToContact());
	            mdetails.setCstBillToAltContact(arCustomer.getCstBillToAltContact());
	            mdetails.setCstBillToPhone(arCustomer.getCstBillToPhone());
	            mdetails.setCstBillingHeader(arCustomer.getCstBillingHeader());
	            mdetails.setCstBillingFooter(arCustomer.getCstBillingFooter());
	            mdetails.setCstBillingHeader2(arCustomer.getCstBillingHeader2());
	            mdetails.setCstBillingFooter2(arCustomer.getCstBillingFooter2());
	            mdetails.setCstBillingHeader3(arCustomer.getCstBillingHeader3());
	            mdetails.setCstBillingFooter3(arCustomer.getCstBillingFooter3());
	            mdetails.setCstBillingSignatory(arCustomer.getCstBillingSignatory());
	            mdetails.setCstSignatoryTitle(arCustomer.getCstSignatoryTitle());
	            mdetails.setCstShipToAddress(arCustomer.getCstShipToAddress());
	            mdetails.setCstShipToContact(arCustomer.getCstShipToContact());
	            mdetails.setCstShipToAltContact(arCustomer.getCstShipToAltContact());
	            mdetails.setCstShipToPhone(arCustomer.getCstShipToPhone());
	            mdetails.setCstName(arCustomer.getCstName());
	            mdetails.setCstNumbersParking(arCustomer.getCstNumbersParking());
	            mdetails.setCstSquareMeter(arCustomer.getCstSquareMeter());
	            mdetails.setCstRealPropertyTaxRate(arCustomer.getCstRealPropertyTaxRate());
	            if (arCustomer.getArSalesperson() != null && arCustomer.getCstArSalesperson2() == null) {

					mdetails.setCstSlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode());
					mdetails.setCstSlpName(arCustomer.getArSalesperson().getSlpName());

				} else if (arCustomer.getArSalesperson() == null && arCustomer.getCstArSalesperson2() != null) {

					LocalArSalesperson arSalesperson2 = null;
					arSalesperson2 = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());

					mdetails.setCstSlpSalespersonCode(arSalesperson2.getSlpSalespersonCode());
					mdetails.setCstSlpName(arSalesperson2.getSlpName());

				} if (arCustomer.getArSalesperson() != null && arCustomer.getCstArSalesperson2() != null) {

					mdetails.setCstSlpSalespersonCode(arCustomer.getArSalesperson().getSlpSalespersonCode());
					mdetails.setCstSlpName(arCustomer.getArSalesperson().getSlpName());
					LocalArSalesperson arSalesperson2 = null;
					arSalesperson2 = arSalespersonHome.findByPrimaryKey(arCustomer.getCstArSalesperson2());

					mdetails.setCstSlpSalespersonCode2(arSalesperson2.getSlpSalespersonCode());
					mdetails.setCstSlpName2(arSalesperson2.getSlpName());
				}

	            if(arCustomer.getArCustomerClass().getArTaxCode() != null) {

	                mdetails.setCstCcTcName(arCustomer.getArCustomerClass().getArTaxCode().getTcName());
	                mdetails.setCstCcTcRate(arCustomer.getArCustomerClass().getArTaxCode().getTcRate());
	                mdetails.setCstCcTcType(arCustomer.getArCustomerClass().getArTaxCode().getTcType());

	            }

	            if(arCustomer.getInvLineItemTemplate() != null) {
	            	mdetails.setCstLitName(arCustomer.getInvLineItemTemplate().getLitName());
	            }

	            return mdetails;

	        } catch (GlobalNoRecordFoundException ex) {

	            throw ex;

	        } catch (Exception ex) {

	            Debug.printStackTrace(ex);
	            throw new EJBException(ex.getMessage());

	        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArModSalesOrderDetails getCmSoBySoNumber(String SO_NMBR, Integer AD_BRNCH, Integer AD_CMPNY)
    		throws GlobalNoRecordFoundException {

	        Debug.print("CmAdjustment getCmSoBySoNumber");

	        LocalArSalesOrderHome arSalesOrderHome = null;
	        // Initialize EJB Home

	        try {

	        	arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);


	        } catch (NamingException ex) {

	            throw new EJBException(ex.getMessage());

	        }

	        try {

	            LocalArSalesOrder arSalesOrder = null;


	            try {

	            	arSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndBrCode(SO_NMBR, AD_BRNCH, AD_CMPNY);

	            } catch (FinderException ex) {

	                throw new GlobalNoRecordFoundException();

	            }


	            ArModSalesOrderDetails mdetails = new ArModSalesOrderDetails();


	            mdetails.setSoCode(arSalesOrder.getSoCode());
	            mdetails.setSoDate(arSalesOrder.getSoDate());
	            mdetails.setSoDocumentNumber(arSalesOrder.getSoDocumentNumber());
	            mdetails.setSoReferenceNumber(arSalesOrder.getSoReferenceNumber());
	            mdetails.setSoDescription(arSalesOrder.getSoDescription());
	            mdetails.setSoBoLock(arSalesOrder.getSoBoLock());
	            mdetails.setSoLock(arSalesOrder.getSoLock());
	            mdetails.setSoPosted(arSalesOrder.getSoPosted());

	            mdetails.setSoVoid(arSalesOrder.getSoVoid());

	            mdetails.setSoMemo(arSalesOrder.getSoMemo());







	            return mdetails;

	        } catch (GlobalNoRecordFoundException ex) {

	            throw ex;

	        } catch (Exception ex) {

	            Debug.printStackTrace(ex);
	            throw new EJBException(ex.getMessage());

	        }

    }





    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteCmAdjEntry(Integer ADJ_CODE, String AD_USR, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException {

        Debug.print("CmAdjustmentEntryControllerBean deleteCmAdjEntry");

        LocalCmAdjustmentHome cmAdjustmentHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home

        try {

            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalCmAdjustment cmAdjustment = cmAdjustmentHome.findByPrimaryKey(ADJ_CODE);

        	if (cmAdjustment.getAdjApprovalStatus() != null && cmAdjustment.getAdjApprovalStatus().equals("PENDING")) {

        		Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("CM ADJUSTMENT", cmAdjustment.getAdjCode(), AD_CMPNY);

        		Iterator i = adApprovalQueues.iterator();

        		while(i.hasNext()) {

        			LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

        			adApprovalQueue.remove();

        		}

        	}


        	adDeleteAuditTrailHome.create("CM ADJUSTMENT", cmAdjustment.getAdjDate(), cmAdjustment.getAdjDocumentNumber(), cmAdjustment.getAdjReferenceNumber(),
        			cmAdjustment.getAdjAmount(), AD_USR, new Date(), AD_CMPNY);

        	cmAdjustment.remove();

        } catch (FinderException ex) {

            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList getAdApprovalNotifiedUsersByAdjCode(Integer ADJ_CODE, Integer AD_CMPNY) {

       Debug.print("CmAdjustmentEntryControllerBean getAdApprovalNotifiedUsersByAdjCode");


       LocalAdApprovalQueueHome adApprovalQueueHome = null;
       LocalCmAdjustmentHome cmAdjustmentHome = null;

       ArrayList list = new ArrayList();


       // Initialize EJB Home

       try {

           adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
           cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
              lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalCmAdjustment cmAdjustment = cmAdjustmentHome.findByPrimaryKey(ADJ_CODE);

         if (cmAdjustment.getAdjPosted() == EJBCommon.TRUE) {

         	list.add("DOCUMENT POSTED");
         	return list;

         }

         Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("CM ADJUSTMENT", ADJ_CODE, AD_CMPNY);

         Iterator i = adApprovalQueues.iterator();

         while(i.hasNext()) {

         	LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

         	list.add(adApprovalQueue.getAdUser().getUsrDescription());

         }

         return list;

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }


   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdModBankAccountDetails getAdBaByBaName(String BA_NM, Integer AD_CMPNY) {

        Debug.print("CmAdjustmentEntryControllerBean getAdBaByBaName");

        LocalAdBankAccountHome adBankAccountHome = null;

        //initialized EJB Home

        try {

            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

            AdModBankAccountDetails mdetails = new AdModBankAccountDetails();

            if(adBankAccount.getGlFunctionalCurrency() != null) {

               mdetails.setBaFcName(adBankAccount.getGlFunctionalCurrency().getFcName());

            }

            return mdetails;


        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

    }


   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("CmAdjustmentEntryControllerBean getGlFcPrecisionUnit");


       LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         return  adCompany.getGlFunctionalCurrency().getFcPrecision();

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }

 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
 	throws GlobalConversionDateNotExistException {

 		Debug.print("GlJournalEntryControllerBean getFrRateByFrNameAndFrDate");

 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
 		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
 		LocalAdCompanyHome adCompanyHome = null;

 		// Initialize EJB Home

 		try {

 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
 			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

 		} catch (NamingException ex) {

 			throw new EJBException(ex.getMessage());

 		}

 		try {

 			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

 			double CONVERSION_RATE = 1;

 			// Get functional currency rate

 			if (!FC_NM.equals("USD")) {

 				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);

 				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

 			}

 			// Get set of book functional currency rate if necessary

 			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

 				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);

 				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

 			}

 			return CONVERSION_RATE;

 		} catch (FinderException ex) {

 			getSessionContext().setRollbackOnly();
 			throw new GlobalConversionDateNotExistException();

 		} catch (Exception ex) {

 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());

 		}

 	}

    // private methods

    private void addCmDrEntry(short DR_LN, String DR_CLSS,
	    byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalCmAdjustment cmAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArInvoiceEntryControllerBean addCmDrEntry");

		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


        // Initialize EJB Home

        try {

            cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// get company

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

		    // create distribution record

		    LocalCmDistributionRecord cmDistributionRecord = cmDistributionRecordHome.create(
			    DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_DBT,
			    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			cmAdjustment.addCmDistributionRecord(cmDistributionRecord);
			glChartOfAccount.addCmDistributionRecord(cmDistributionRecord);
        } catch (FinderException ex) {

        	throw new GlobalBranchAccountNumberInvalidException();

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

    private void executeCmAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException {

        Debug.print("CmAdjustmentEntryControllerBean executeCmAdjPost");

        LocalCmAdjustmentHome cmAdjustmentHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalCmDistributionRecordHome cmDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
        LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

        LocalCmAdjustment cmAdjustment = null;

        // Initialize EJB Home

        try {

            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
            glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// validate if adjustment is already deleted

        	try {

        		cmAdjustment = cmAdjustmentHome.findByPrimaryKey(ADJ_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if receipt is already posted
        	System.out.println("cmAdjustment.getAdjVoid()="+cmAdjustment.getAdjVoid());
        	System.out.println("cmAdjustment.getAdjPosted()="+cmAdjustment.getAdjPosted());

        	if (cmAdjustment.getAdjVoid() == EJBCommon.FALSE && cmAdjustment.getAdjPosted() == EJBCommon.TRUE) {
        		System.out.println("------------------------------>");
        		throw new GlobalTransactionAlreadyPostedException();

        		// validate if receipt void is already posted

        	} else if (cmAdjustment.getAdjVoid() == EJBCommon.TRUE && cmAdjustment.getAdjVoidPosted() == EJBCommon.TRUE) {

        		throw new GlobalTransactionAlreadyVoidPostedException();

        	}

        	// post adjustment

        	if (cmAdjustment.getAdjVoid() == EJBCommon.FALSE && cmAdjustment.getAdjPosted() == EJBCommon.FALSE) {

        		if(cmAdjustment.getAdjType().equals("INTEREST") || cmAdjustment.getAdjType().equals("DEBIT MEMO") || cmAdjustment.getAdjType().equals("ADVANCE") ) {

        			// increase bank account balances

    				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());

    				try {

    					// find bankaccount balance before or equal receipt date

    					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					if (!adBankAccountBalances.isEmpty()) {

    						// get last check

    						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

    						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {

    							// create new balance

    							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);

    							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

    						} else { // equals to check date

    							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());

    						}

    					} else {

    						// create new balance

    						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    								cmAdjustment.getAdjDate(), (cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);

    						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

    					}

    					// propagate to subsequent balances if necessary

    					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					Iterator i = adBankAccountBalances.iterator();

    					while (i.hasNext()) {

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

    						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());

    					}

    				} catch (Exception ex) {

    					ex.printStackTrace();

    				}

        		} else {

        			// decrease bank account balances

    				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());

    				try {

    					// find bankaccount balance before or equal receipt date

    					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					if (!adBankAccountBalances.isEmpty()) {

    						// get last check

    						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

    						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {

    							// create new balance

    							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);

    							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

    						} else { // equals to check date

    							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());

    						}

    					} else {

    						// create new balance

    						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    								cmAdjustment.getAdjDate(), (0 - cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);

    						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

    					}

    					// propagate to subsequent balances if necessary

    					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					Iterator i = adBankAccountBalances.iterator();

    					while (i.hasNext()) {

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

    						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());

    					}

    				} catch (Exception ex) {

    					ex.printStackTrace();

    				}

        		}
        		cmAdjustment.setAdjPosted(EJBCommon.TRUE);
            	cmAdjustment.setAdjPostedBy(USR_NM);
            	cmAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

        	}else if (cmAdjustment.getAdjVoid() == EJBCommon.TRUE && cmAdjustment.getAdjVoidPosted() == EJBCommon.FALSE) {// void receipt




            		if(cmAdjustment.getAdjType().equals("INTEREST") || cmAdjustment.getAdjType().equals("DEBIT MEMO") || cmAdjustment.getAdjType().equals("ADVANCE") ) {



            			// decrease bank account balances

        				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());

        				try {

        					// find bankaccount balance before or equal receipt date

        					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

        					if (!adBankAccountBalances.isEmpty()) {

        						// get last check

        						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

        						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

        						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {

        							// create new balance

        							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
        									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);

        							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

        						} else { // equals to check date

        							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());

        						}

        					} else {

        						// create new balance

        						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
        								cmAdjustment.getAdjDate(), (0 - cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);

        						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

        					}

        					// propagate to subsequent balances if necessary

        					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

        					Iterator i = adBankAccountBalances.iterator();

        					while (i.hasNext()) {

        						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

        						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());

        					}

        				} catch (Exception ex) {

        					ex.printStackTrace();

        				}



            		} else {


// increase bank account balances

        				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());

        				try {

        					// find bankaccount balance before or equal receipt date

        					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

        					if (!adBankAccountBalances.isEmpty()) {

        						// get last check

        						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

        						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

        						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {

        							// create new balance

        							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
        									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);

        							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

        						} else { // equals to check date

        							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());

        						}

        					} else {

        						// create new balance

        						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
        								cmAdjustment.getAdjDate(), (cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);

        						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

        					}

        					// propagate to subsequent balances if necessary

        					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

        					Iterator i = adBankAccountBalances.iterator();

        					while (i.hasNext()) {

        						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

        						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());

        					}

        				} catch (Exception ex) {

        					ex.printStackTrace();

        				}
            		}

            		// set cmAdjustment post status

     	           cmAdjustment.setAdjVoidPosted(EJBCommon.TRUE);
     	           cmAdjustment.setAdjPostedBy(USR_NM);
     	           cmAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());



        	}




        	// post to gl if necessary

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		// validate if date has no period and period is closed

        		LocalGlSetOfBook glJournalSetOfBook = null;

        		try {

        			glJournalSetOfBook = glSetOfBookHome.findByDate(cmAdjustment.getAdjDate(), AD_CMPNY);

        		} catch (FinderException ex) {

        			throw new GlJREffectiveDateNoPeriodExistException();

        		}

        		LocalGlAccountingCalendarValue glAccountingCalendarValue =
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), cmAdjustment.getAdjDate(), AD_CMPNY);


        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

        			throw new GlJREffectiveDatePeriodClosedException();

        		}

        		// check if invoice is balance if not check suspense posting

        		LocalGlJournalLine glOffsetJournalLine = null;

        		Collection cmDistributionRecords = cmDistributionRecordHome.findByDrReversalAndDrImportedAndAdjCode(EJBCommon.FALSE, EJBCommon.FALSE, cmAdjustment.getAdjCode(), AD_CMPNY);

        		Iterator j = cmDistributionRecords.iterator();

        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;

        		while (j.hasNext()) {

        			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

        			double DR_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
        					cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(),
							cmAdjustment.getAdjConversionDate(),
							cmAdjustment.getAdjConversionRate(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);

        			if (cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

        				TOTAL_DEBIT += DR_AMNT;

        			} else {

        				TOTAL_CREDIT += DR_AMNT;

        			}

        		}

        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			LocalGlSuspenseAccount glSuspenseAccount = null;

        			try {

        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("CASH MANAGEMENT", "BANK ADJUSTMENTS", AD_CMPNY);

        			} catch (FinderException ex) {

        				throw new GlobalJournalNotBalanceException();

        			}

        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(cmDistributionRecords.size() + 1),
								EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

        			} else {

        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(cmDistributionRecords.size() + 1),
								EJBCommon.FALSE,
								TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

        			}

        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			glChartOfAccount.addGlJournalLine(glOffsetJournalLine);


        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {

        			throw new GlobalJournalNotBalanceException();

        		}

        		// create journal batch if necessary

        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        		try {

        			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " BANK ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

        		} catch (FinderException ex) {
        		}

        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
        				glJournalBatch == null) {

        			glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " BANK ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

        		}

        		// create journal entry

        		LocalGlJournal glJournal = glJournalHome.create(cmAdjustment.getAdjReferenceNumber(),
        				cmAdjustment.getAdjMemo(), cmAdjustment.getAdjDate(),
						0.0d, null, cmAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);

        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("CASH MANAGEMENT", AD_CMPNY);
	    		glJournal.setGlJournalSource(glJournalSource);

	    		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
	    		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

	    		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("BANK ADJUSTMENTS", AD_CMPNY);
	    		glJournal.setGlJournalCategory(glJournalCategory);

	    		if (glJournalBatch != null) {

	    			glJournal.setGlJournalBatch(glJournalBatch);

	    		}


        		// create journal lines

        		j = cmDistributionRecords.iterator();

        		while (j.hasNext()) {

        			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

        			double DR_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
        					cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(),
							cmAdjustment.getAdjConversionDate(),
							cmAdjustment.getAdjConversionRate(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);

        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
        					cmDistributionRecord.getDrLine(),
							cmDistributionRecord.getDrDebit(),
							DR_AMNT, "", AD_CMPNY);

        			cmDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);

        			glJournal.addGlJournalLine(glJournalLine);

        			cmDistributionRecord.setDrImported(EJBCommon.TRUE);

    		  	  	// for FOREX revaluation
    		  	  	if((cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode() !=
    		  	  			adCompany.getGlFunctionalCurrency().getFcCode()) &&
    					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
    		  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
    		  	  			cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode()))){

    		  	  		double CONVERSION_RATE = 1;

    		  	  		if (cmAdjustment.getAdjConversionRate() != 0 && cmAdjustment.getAdjConversionRate() != 1) {

    		  	  			CONVERSION_RATE = cmAdjustment.getAdjConversionRate();

    		  	  		} else if (cmAdjustment.getAdjConversionDate() != null){

    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(),
    								glJournal.getJrConversionDate(), AD_CMPNY);

    		  	  		}

    		  	  		Collection glForexLedgers = null;

    		  	  		try {

    		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		  	  				cmAdjustment.getAdjDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

    		  	  		} catch(FinderException ex) {

    		  	  		}

    		  	  		LocalGlForexLedger glForexLedger =
    		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();

    		  	  		int FRL_LN = (glForexLedger != null &&
    		  	  				glForexLedger.getFrlDate().compareTo(cmAdjustment.getAdjDate()) == 0) ?
    		  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;

    		  	  		// compute balance
    		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		  	  		double FRL_AMNT = cmDistributionRecord.getDrAmount();

    		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));
    		  	  		else
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);

    		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

    		  	  		glForexLedger = glForexLedgerHome.create(cmAdjustment.getAdjDate(), new Integer (FRL_LN),
    		  	  			"OTH", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

    		  	  		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);

    		  	  		// propagate balances
    		  	  		try{

    		  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		  	  				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
								glForexLedger.getFrlAdCompany());

    		  	  		} catch (FinderException ex) {

    		  	  		}

    		  	  		Iterator itrFrl = glForexLedgers.iterator();

    		  	  		while (itrFrl.hasNext()) {

    		  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		  	  			FRL_AMNT = cmDistributionRecord.getDrAmount();

    		  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT :
    		  	  					(- 1 * FRL_AMNT));
    		  	  			else
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) :
    		  	  					FRL_AMNT);

    		  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

    		  	  		}

    		  	  	}

        		}

        		if (glOffsetJournalLine != null) {

        			glJournal.addGlJournalLine(glOffsetJournalLine);

        		}

        		// post journal to gl

        		Collection glJournalLines = glJournal.getGlJournalLines();

        		Iterator i = glJournalLines.iterator();

        		while (i.hasNext()) {

        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

        			// post current to current acv

        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


        			// post to subsequent acvs (propagate)

        			Collection glSubsequentAccountingCalendarValues =
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

        			while (acvsIter.hasNext()) {

        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        					(LocalGlAccountingCalendarValue)acvsIter.next();

        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        			}

        			// post to subsequent years if necessary

        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

        				Iterator sobIter = glSubsequentSetOfBooks.iterator();

        				while (sobIter.hasNext()) {

        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

        					// post to subsequent acvs of subsequent set of book(propagate)

        					Collection glAccountingCalendarValues =
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

        					Iterator acvIter = glAccountingCalendarValues.iterator();

        					while (acvIter.hasNext()) {

        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
        							(LocalGlAccountingCalendarValue)acvIter.next();

        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						} else { // revenue & expense

        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

        						}

        					}

        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

        				}

        			}

        		}

        	}

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

	    Debug.print("CmAdjustmentEntryControllerBean convertForeignToFunctionalCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT / CONVERSION_RATE;

         }

         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
      LocalGlChartOfAccount glChartOfAccount,
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

      Debug.print("CmAdjustmentEntryControllerBean postToGl");

      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance =
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);

	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {

 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

					}


			  } else {

				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				    if (!isCurrentAcv) {

				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

					}

		 	  }

		 	  if (isCurrentAcv) {

			 	 if (isDebit == EJBCommon.TRUE) {

		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

		 		 } else {

		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
		 		 }

		 	}

       } catch (Exception ex) {

       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());

       }


   }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmAdjustmentEntryControllerBean ejbCreate");

    }
}
