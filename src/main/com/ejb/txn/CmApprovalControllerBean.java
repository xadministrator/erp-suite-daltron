
/*
 * CmApprovalControllerBean.java
 *
 * Created on May 03, 2004, 2:07 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdModApprovalQueueDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmApprovalControllerEJB"
 *           display-name="Used for approving cm documents"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmApprovalControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmApprovalController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmApprovalControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 * 
*/

public class CmApprovalControllerBean extends AbstractSessionBean {   
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdAqByAqDocumentAndUserName(HashMap criteria,
        String USR_NM, Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("CmApprovalControllerBean getAdAqByAqDocumentAndUserName");
        
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalCmAdjustmentHome cmAdjustmentHome = null;
        LocalCmFundTransferHome cmFundTransferHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
            cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);              
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try {
			
			StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(aq) FROM AdApprovalQueue aq ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("document")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocument=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("document");
        		ctr++;
        	}
        	
        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	}
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("aq.aqAdBranch=" + AD_BRNCH + " AND aq.aqAdCompany=" + AD_CMPNY + " " + "AND aq.adUser.usrName='" + USR_NM + "'");
        	
        	String orderBy = null;
        	
        	if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
        		
        		orderBy = "aq.aqDocumentNumber";
        		
        	} else if (ORDER_BY.equals("DATE")) {
        		
        		orderBy = "aq.aqDate";
        		
        	}
        	
        	if (orderBy != null) {
        		
        		jbossQl.append("ORDER BY " + orderBy + ", aq.aqDate");
        		
        	} else {
        		
        		jbossQl.append("ORDER BY aq.aqDate");
        		
        	}
        	
        	jbossQl.append(" OFFSET ?" + (ctr + 1));	        
        	obj[ctr] = OFFSET;	      
        	ctr++;
        	
        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;
		           
		    Collection adApprovalQueues = adApprovalQueueHome.getAqByCriteria(jbossQl.toString(), obj);
		    
		    String AQ_DCMNT = (String)criteria.get("document"); 

		    if (adApprovalQueues.size() == 0)
        		throw new GlobalNoRecordFoundException();

		    Iterator i = adApprovalQueues.iterator();
		    
		    while (i.hasNext()) {
		    	
		    	LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
		    	
		    	AdModApprovalQueueDetails details = new AdModApprovalQueueDetails();
		    	
		    	details.setAqDocument(adApprovalQueue.getAqDocument());
		    	details.setAqDocumentCode(adApprovalQueue.getAqDocumentCode());
		    	
		    	if (AQ_DCMNT.equals("CM ADJUSTMENT")) {
		    		
		    		LocalCmAdjustment cmAdjustment = cmAdjustmentHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
		    				    		
		    		details.setAqDate(cmAdjustment.getAdjDate());
		    		details.setAqAmount(cmAdjustment.getAdjAmount());
		    		details.setAqReferenceNumber(cmAdjustment.getAdjReferenceNumber());
		    		details.setAqDocumentNumber(cmAdjustment.getAdjDocumentNumber());
		    		details.setAqDocumentType(cmAdjustment.getAdjType());
		    		
		    	} else {
		    		
		    		LocalCmFundTransfer cmFundTransfer = cmFundTransferHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
		    				    		
		    		details.setAqDate(cmFundTransfer.getFtDate());
		    		details.setAqAmount(cmFundTransfer.getFtAmount());
		    		details.setAqReferenceNumber(cmFundTransfer.getFtReferenceNumber());	
		    		details.setAqDocumentNumber(cmFundTransfer.getFtDocumentNumber());
		    		
		    	}
		    	
		    	list.add(details);
		    	
		    }
		    
		    return list;
		  
		} catch (GlobalNoRecordFoundException ex) {
		  	 
			throw ex;
		  	
		} catch (Exception ex) {
		  	
	
		  	ex.printStackTrace();
		  	throw new EJBException(ex.getMessage());
		  	
		}
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeCmApproval(String AQ_DCMNT, Integer AQ_DCMNT_CODE, String USR_NM, boolean isApproved, String RSN_FR_RJCTN, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlobalTransactionAlreadyApprovedException,
        GlobalConversionDateNotExistException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidPostedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException {
                    
        Debug.print("CmApprovalControllerBean executeCmApproval");
        
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalCmAdjustmentHome cmAdjustmentHome = null;  
        LocalCmFundTransferHome cmFundTransferHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;             
                
        LocalAdApprovalQueue adApprovalQueue = null;         
                
        // Initialize EJB Home
        
        try {
            
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);            
            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
            cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);            
                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	
        	// validate if approval queue is already deleted
        	
        	try {
        		        			
        		adApprovalQueue = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeAndUsrName(AQ_DCMNT, AQ_DCMNT_CODE, USR_NM, AD_CMPNY);
        			        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}        	
        	
        	// approve/reject
        	
        	Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        	
        	Collection adApprovalQueuesDesc = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeLessThanDesc(AQ_DCMNT, AQ_DCMNT_CODE, adApprovalQueue.getAqCode(), AD_CMPNY);
        	Collection adApprovalQueuesAsc = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeGreaterThanAsc(AQ_DCMNT, AQ_DCMNT_CODE, adApprovalQueue.getAqCode(), AD_CMPNY);
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	        	
        	if (AQ_DCMNT.equals("CM ADJUSTMENT")) {        		
        		
        		LocalCmAdjustment cmAdjustment = cmAdjustmentHome.findByPrimaryKey(AQ_DCMNT_CODE);
        		        		
        		if (isApproved) {
        			        			
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				
        				if (adApprovalQueues.size() == 1) {
        					        					
        					cmAdjustment.setAdjApprovalStatus("APPROVED");
        					cmAdjustment.setAdjApprovedRejectedBy(USR_NM);
        					cmAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					
        					if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		        		
        		        		this.executeCmAdjPost(cmAdjustment.getAdjCode(), USR_NM, AD_BRNCH, AD_CMPNY);
        		        		
        		        	}
        					
        					adApprovalQueue.remove();
        					
        				} else {
        					
        					// looping up
        					Iterator i = adApprovalQueuesDesc.iterator();
            				
            				while (i.hasNext()) {
            					
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            				
            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							
        							i.remove();
                					adRemoveApprovalQueue.remove();
                					
        						} else {
        							
        							break;
        							
        						}
            						
            				}
            				
            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {
            					
            					boolean first = true;
            					
            					i = adApprovalQueuesAsc.iterator();
                				
                				while (i.hasNext()) {
                					
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                					
                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							
            							i.remove();
                    					adRemoveApprovalQueue.remove();
                    					
                    					if(first)
                    						first = false;
                    					
            						} else {
            							
            							break;
            							
            						}
                						
                				}
                				
            				}
            				
            				adApprovalQueue.remove();
        					
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
            	        	
        					if(adRemoveApprovalQueues.size() == 0) {
            	        		
        						cmAdjustment.setAdjApprovalStatus("APPROVED");
            					cmAdjustment.setAdjApprovedRejectedBy(USR_NM);
            					cmAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
            					
            					if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
            		        		
            		        		this.executeCmAdjPost(cmAdjustment.getAdjCode(), USR_NM, AD_BRNCH, AD_CMPNY);
            		        		
            		        	}
            					
            	        	}
            				
        					
        				}
        				        				        				
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
        				
        				cmAdjustment.setAdjApprovalStatus("APPROVED");
        				cmAdjustment.setAdjApprovedRejectedBy(USR_NM);
        				cmAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				
        				Iterator i = adApprovalQueues.iterator();
        				
        				while (i.hasNext()) {
        					
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					
        					i.remove();
        					adRemoveApprovalQueue.remove();
        					
        				}
        				
        				if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
    		        		
    		        		this.executeCmAdjPost(cmAdjustment.getAdjCode(), USR_NM, AD_BRNCH, AD_CMPNY);
    		        		
    		        	}
        				
        				
        			}       			      			
        			        	
        			        			
        		} else if (!isApproved) {
        			
        			cmAdjustment.setAdjApprovalStatus(null);
        			cmAdjustment.setAdjReasonForRejection(RSN_FR_RJCTN);
    				cmAdjustment.setAdjApprovedRejectedBy(USR_NM);
    				cmAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
    				
    				Iterator i = adApprovalQueues.iterator();
    				
    				while (i.hasNext()) {
    					
    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
    					
    					i.remove();
    					adRemoveApprovalQueue.remove();
    					
    				}
        			        			
        		}
        		
        		
        	} else {
        		
        		LocalCmFundTransfer cmFundTransfer = cmFundTransferHome.findByPrimaryKey(AQ_DCMNT_CODE);
        		        		        		
        		if (isApproved) {
        			        			
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				
        				if (adApprovalQueues.size() == 1) {
        					
        					cmFundTransfer.setFtApprovalStatus("APPROVED");
        					cmFundTransfer.setFtApprovedRejectedBy(USR_NM);
        					cmFundTransfer.setFtDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					
        					if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		        		
        		        		this.executeCmFtPost(cmFundTransfer.getFtCode(), USR_NM, AD_BRNCH, AD_CMPNY);
        		        		
        		        	}
        					
        					adApprovalQueue.remove();
        					
        				} else {
        					        					
        					// looping up
        					Iterator i = adApprovalQueuesDesc.iterator();
            				
            				while (i.hasNext()) {
            					
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            				
            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							
        							i.remove();
                					adRemoveApprovalQueue.remove();
                					
        						} else {
        							
        							break;
        							
        						}
            						
            				}
            				
            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {
            					
            					boolean first = true;
            					
            					i = adApprovalQueuesAsc.iterator();
                				
                				while (i.hasNext()) {
                					
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                					
                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							
            							i.remove();
                    					adRemoveApprovalQueue.remove();
                    					
                    					if(first)
                    						first = false;
                    					
            						} else {
            							
            							break;
            							
            						}
                						
                				}
                				
            				}
            				
            				adApprovalQueue.remove();
        					
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
            	        	
        					if(adRemoveApprovalQueues.size() == 0) {
            					
            					cmFundTransfer.setFtApprovalStatus("APPROVED");
            					cmFundTransfer.setFtApprovedRejectedBy(USR_NM);
            					cmFundTransfer.setFtDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
            					
            					if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
            		        		
            		        		this.executeCmFtPost(cmFundTransfer.getFtCode(), USR_NM, AD_BRNCH, AD_CMPNY);
            		        		
            		        	}
            					
            				}
            				
        				}
        				        				        				
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
		
    					cmFundTransfer.setFtApprovalStatus("APPROVED");
        				cmFundTransfer.setFtApprovedRejectedBy(USR_NM);
        				cmFundTransfer.setFtDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				
        				Iterator i = adApprovalQueues.iterator();
        				
        				while (i.hasNext()) {
        					
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					
        					i.remove();
        					adRemoveApprovalQueue.remove();
        					
        				}
        				
        				if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
    		        		
    		        		this.executeCmFtPost(cmFundTransfer.getFtCode(), USR_NM, AD_BRNCH, AD_CMPNY);
    		        		
    		        	}
        				
        				
        			}
        					        	        			        			
        		} else if (!isApproved) {
        					
					cmFundTransfer.setFtApprovalStatus(null);
        			cmFundTransfer.setFtApprovedRejectedBy(USR_NM);
        			cmFundTransfer.setFtReasonForRejection(RSN_FR_RJCTN);
        			cmFundTransfer.setFtDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
    				
    				Iterator i = adApprovalQueues.iterator();
    				
    				while (i.hasNext()) {
    					
    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
    					
    					i.remove();
    					adRemoveApprovalQueue.remove();
    					
    				}
        			        			
        		}
        		
        	}        	        	        	
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyVoidPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }


   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("CmApprovalControllerBean getGlFcPrecisionUnit");

       	
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    // private methods
    
    private void executeCmAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException {
                    
        Debug.print("CmApprovalControllerBean executeCmAdjPost");
        
        LocalCmAdjustmentHome cmAdjustmentHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalCmDistributionRecordHome cmDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
        LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

        LocalCmAdjustment cmAdjustment = null;         
                
        // Initialize EJB Home
        
        try {
            
            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
            glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	// validate if adjustment is already deleted
        	
        	try {
        		
        		cmAdjustment = cmAdjustmentHome.findByPrimaryKey(ADJ_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if receipt is already posted
        	
        	if (cmAdjustment.getAdjVoid() == EJBCommon.FALSE && cmAdjustment.getAdjPosted() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyPostedException();
        		
        		// validate if receipt void is already posted
        		
        	} else if (cmAdjustment.getAdjVoid() == EJBCommon.TRUE && cmAdjustment.getAdjVoidPosted() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyVoidPostedException();
        		
        	}
        	
// post adjustment
        	
        	if (cmAdjustment.getAdjVoid() == EJBCommon.FALSE && cmAdjustment.getAdjPosted() == EJBCommon.FALSE) {            
        		
        		if(cmAdjustment.getAdjType().equals("INTEREST") || cmAdjustment.getAdjType().equals("DEBIT MEMO") || cmAdjustment.getAdjType().equals("ADVANCE") ) {
        			
        			// increase bank account balances 
        			
    				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());
    				
    				try {
    					
    					// find bankaccount balance before or equal receipt date
    					
    					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					if (!adBankAccountBalances.isEmpty()) {
    						
    						// get last check
    						
    						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
    						
    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
    						
    						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {
    							
    							// create new balance

    							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);
    							
    							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    							
    						} else { // equals to check date

    							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());
    							
    						} 
    						
    					} else {        	
    						
    						// create new balance
    						
    						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    								cmAdjustment.getAdjDate(), (cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);
    						
    						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    						
    					}
    					
    					// propagate to subsequent balances if necessary
    					
    					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					Iterator i = adBankAccountBalances.iterator();
    					
    					while (i.hasNext()) {
    						
    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
    						
    						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());
    						
    					}			
    					
    				} catch (Exception ex) {
    					
    					ex.printStackTrace();
    					
    				}

        		} else {
        			
        			// decrease bank account balances 
        			
    				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());
    				
    				try {
    					
    					// find bankaccount balance before or equal receipt date
    					
    					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					if (!adBankAccountBalances.isEmpty()) {
    						
    						// get last check
    						
    						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
    						
    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
    						
    						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {
    							
    							// create new balance

    							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);
    							
    							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    							
    						} else { // equals to check date

    							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());
    							
    						} 
    						
    					} else {        	
    						
    						// create new balance
    						
    						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    								cmAdjustment.getAdjDate(), (0 - cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);
    						
    						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
    						
    					}
    					
    					// propagate to subsequent balances if necessary
    					
    					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					Iterator i = adBankAccountBalances.iterator();
    					
    					while (i.hasNext()) {
    						
    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
    						
    						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());
    						
    					}			
    					
    				} catch (Exception ex) {
    					
    					ex.printStackTrace();
    					
    				}
    				
        		}
        		cmAdjustment.setAdjPosted(EJBCommon.TRUE);
            	cmAdjustment.setAdjPostedBy(USR_NM);
            	cmAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        		
        	}else if (cmAdjustment.getAdjVoid() == EJBCommon.TRUE && cmAdjustment.getAdjVoidPosted() == EJBCommon.FALSE) {// void receipt
        		
        		
                
        		
            		if(cmAdjustment.getAdjType().equals("INTEREST") || cmAdjustment.getAdjType().equals("DEBIT MEMO") || cmAdjustment.getAdjType().equals("ADVANCE") ) {
            			

            			
            			// decrease bank account balances 
            			
        				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());
        				
        				try {
        					
        					// find bankaccount balance before or equal receipt date
        					
        					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

        					if (!adBankAccountBalances.isEmpty()) {
        						
        						// get last check
        						
        						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
        						
        						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
        						
        						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {
        							
        							// create new balance

        							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
        									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);
        							
        							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
        							
        						} else { // equals to check date

        							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());
        							
        						} 
        						
        					} else {        	
        						
        						// create new balance
        						
        						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
        								cmAdjustment.getAdjDate(), (0 - cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);
        						
        						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
        						
        					}
        					
        					// propagate to subsequent balances if necessary
        					
        					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

        					Iterator i = adBankAccountBalances.iterator();
        					
        					while (i.hasNext()) {
        						
        						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
        						
        						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());
        						
        					}			
        					
        				} catch (Exception ex) {
        					
        					ex.printStackTrace();
        					
        				}
        				
            			

            		} else {
            			
            			
// increase bank account balances 
            			
        				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());
        				
        				try {
        					
        					// find bankaccount balance before or equal receipt date
        					
        					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

        					if (!adBankAccountBalances.isEmpty()) {
        						
        						// get last check
        						
        						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
        						
        						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
        						
        						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {
        							
        							// create new balance

        							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
        									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);
        							
        							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
        							
        						} else { // equals to check date

        							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());
        							
        						} 
        						
        					} else {        	
        						
        						// create new balance
        						
        						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
        								cmAdjustment.getAdjDate(), (cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);
        						
        						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);
        						
        					}
        					
        					// propagate to subsequent balances if necessary
        					
        					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

        					Iterator i = adBankAccountBalances.iterator();
        					
        					while (i.hasNext()) {
        						
        						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
        						
        						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());
        						
        					}			
        					
        				} catch (Exception ex) {
        					
        					ex.printStackTrace();
        					
        				}
            		}
            		
            		// set cmAdjustment post status
                    
     	           cmAdjustment.setAdjVoidPosted(EJBCommon.TRUE);
     	           cmAdjustment.setAdjPostedBy(USR_NM);
     	           cmAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
     	           
        		
        		
        	}
        	
        	// post to gl if necessary
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		
        		// validate if date has no period and period is closed
        		
        		LocalGlSetOfBook glJournalSetOfBook = null;
        		
        		try {
        			
        			glJournalSetOfBook = glSetOfBookHome.findByDate(cmAdjustment.getAdjDate(), AD_CMPNY);
        			
        		} catch (FinderException ex) {
        			
        			throw new GlJREffectiveDateNoPeriodExistException();
        			
        		}
        		
        		LocalGlAccountingCalendarValue glAccountingCalendarValue = 
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), cmAdjustment.getAdjDate(), AD_CMPNY);
        		
        		
        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
        			
        			throw new GlJREffectiveDatePeriodClosedException();
        			
        		}
        		
        		// check if invoice is balance if not check suspense posting
        		
        		LocalGlJournalLine glOffsetJournalLine = null;
        		
        		Collection cmDistributionRecords = cmDistributionRecordHome.findByDrReversalAndDrImportedAndAdjCode(EJBCommon.FALSE, EJBCommon.FALSE, cmAdjustment.getAdjCode(), AD_CMPNY);
        		
        		Iterator j = cmDistributionRecords.iterator();
        		
        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;
        		
        		while (j.hasNext()) {
        			
        			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();
        			
        			double DR_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
        					cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(), 
							cmAdjustment.getAdjConversionDate(),
							cmAdjustment.getAdjConversionRate(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);
        			
        			if (cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
        				
        				TOTAL_DEBIT += DR_AMNT;
        				
        			} else {
        				
        				TOTAL_CREDIT += DR_AMNT;
        				
        			}
        			
        		}
        		
        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		
        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			LocalGlSuspenseAccount glSuspenseAccount = null;
        			
        			try { 	
        				
        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("CASH MANAGEMENT", "BANK ADJUSTMENTS", AD_CMPNY);
        				
        			} catch (FinderException ex) {
        				
        				throw new GlobalJournalNotBalanceException();
        				
        			}
        			
        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(cmDistributionRecords.size() + 1),
								EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
        				
        			} else {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(cmDistributionRecords.size() + 1),
								EJBCommon.FALSE,
								TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
        				
        			}
        			
        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        			
        			
        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			throw new GlobalJournalNotBalanceException();		    	
        			
        		}
        		
        		// create journal batch if necessary
        		
        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        		
        		try {
        			
        			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " BANK ADJUSTMENTS", AD_BRNCH, AD_CMPNY);		       	   
        			
        		} catch (FinderException ex) {
        		}
        		
        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE && 
        				glJournalBatch == null) {
        			
        			glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " BANK ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        			
        		}
        		
        		// create journal entry			            	
        		
        		LocalGlJournal glJournal = glJournalHome.create(cmAdjustment.getAdjReferenceNumber(),
        				cmAdjustment.getAdjMemo(), cmAdjustment.getAdjDate(),
						0.0d, null, cmAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);
        		
        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("CASH MANAGEMENT", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);
        		
        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
        		
        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("BANK ADJUSTMENTS", AD_CMPNY);
        		glJournal.setGlJournalCategory(glJournalCategory);
        		
        		if (glJournalBatch != null) {
        			
        			glJournal.setGlJournalBatch(glJournalBatch);
        			
        		}           		    
        		
        		
        		// create journal lines
        		
        		j = cmDistributionRecords.iterator();
        		
        		while (j.hasNext()) {
        			
        			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();
        			
        			double DR_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
        					cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(), 
							cmAdjustment.getAdjConversionDate(),
							cmAdjustment.getAdjConversionRate(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);
        			
        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
        					cmDistributionRecord.getDrLine(),	            			
							cmDistributionRecord.getDrDebit(),
							DR_AMNT, "", AD_CMPNY);
        			
        			cmDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
        			
        			glJournal.addGlJournalLine(glJournalLine);
        			
        			cmDistributionRecord.setDrImported(EJBCommon.TRUE);
        			
    		  	  	// for FOREX revaluation
    		  	  	if((cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode() !=
    		  	  			adCompany.getGlFunctionalCurrency().getFcCode()) &&
    					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
    		  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
    		  	  			cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode()))){
    		  	  		
    		  	  		double CONVERSION_RATE = 1;
    		  	  		
    		  	  		if (cmAdjustment.getAdjConversionRate() != 0 && cmAdjustment.getAdjConversionRate() != 1) {
    		  	  			
    		  	  			CONVERSION_RATE = cmAdjustment.getAdjConversionRate();
    		  	  			
    		  	  		} else if (cmAdjustment.getAdjConversionDate() != null){
    		  	  			
    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
    								glJournal.getJrConversionDate(), AD_CMPNY);
    		  	  			
    		  	  		}
    		  	  		
    		  	  		Collection glForexLedgers = null;
    		  	  		
    		  	  		try {
    		  	  			
    		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		  	  				cmAdjustment.getAdjDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);
    		  	  			
    		  	  		} catch(FinderException ex) {
    		  	  			
    		  	  		}
    		  	  		
    		  	  		LocalGlForexLedger glForexLedger =
    		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();
    		  	  		
    		  	  		int FRL_LN = (glForexLedger != null &&
    		  	  				glForexLedger.getFrlDate().compareTo(cmAdjustment.getAdjDate()) == 0) ?
    		  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;
    		  	  		
    		  	  		// compute balance
    		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		  	  		double FRL_AMNT = cmDistributionRecord.getDrAmount();
    		  	  		
    		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));  
    		  	  		else
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);
    		  	  		
    		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
    		  	  		
    		  	  		glForexLedger = glForexLedgerHome.create(cmAdjustment.getAdjDate(), new Integer (FRL_LN),
    		  	  			"OTH", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);
    		  	  		
    		  	  		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
    		  	  		
    		  	  		// propagate balances
    		  	  		try{
    		  	  			
    		  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		  	  				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
								glForexLedger.getFrlAdCompany());
    		  	  			
    		  	  		} catch (FinderException ex) {
    		  	  			
    		  	  		}
    		  	  		
    		  	  		Iterator itrFrl = glForexLedgers.iterator();
    		  	  		
    		  	  		while (itrFrl.hasNext()) {
    		  	  			
    		  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		  	  			FRL_AMNT = cmDistributionRecord.getDrAmount();
    		  	  			
    		  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));
    		  	  			else
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);
    		  	  			
    		  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);
    		  	  			
    		  	  		}
    		  	  		
    		  	  	}        			

        		}
        		
        		if (glOffsetJournalLine != null) {
        			
        			glJournal.addGlJournalLine(glOffsetJournalLine);
        			
        		}		
        		
        		// post journal to gl
        		
        		Collection glJournalLines = glJournal.getGlJournalLines();
        		
        		Iterator i = glJournalLines.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
        			
        			// post current to current acv
        			
        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);
        			
        			
        			// post to subsequent acvs (propagate)
        			
        			Collection glSubsequentAccountingCalendarValues = 
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
        			
        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
        			
        			while (acvsIter.hasNext()) {
        				
        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        					(LocalGlAccountingCalendarValue)acvsIter.next();
        				
        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);
        				
        			}
        			
        			// post to subsequent years if necessary
        			
        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
        			
        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
        				
        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
        				
        				Iterator sobIter = glSubsequentSetOfBooks.iterator();
        				
        				while (sobIter.hasNext()) {
        					
        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
        					
        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
        					
        					// post to subsequent acvs of subsequent set of book(propagate)
        					
        					Collection glAccountingCalendarValues = 
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
        					
        					Iterator acvIter = glAccountingCalendarValues.iterator();
        					
        					while (acvIter.hasNext()) {
        						
        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        							(LocalGlAccountingCalendarValue)acvIter.next();
        						
        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);
        							
        						} else { // revenue & expense
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);					        
        							
        						}
        						
        					}
        					
        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
        					
        				}

        			}
        			
        		}	   			   
        		
        	}
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyVoidPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void executeCmFtPost(Integer FT_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException{
                    
        Debug.print("CmApprovalControllerBean executeCmFtPost");
        
        LocalCmFundTransferHome cmFundTransferHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalCmDistributionRecordHome cmDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
 		
        LocalCmFundTransfer cmFundTransfer = null;
        
        // Initialize EJB Home
        
        try {
            
            cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);  
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	// validate if fund transfer is already deleted
        	
        	try {
        		
        		cmFundTransfer = cmFundTransferHome.findByPrimaryKey(FT_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if fund transfer is already posted or void
        	
        	if (cmFundTransfer.getFtPosted() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyPostedException();
        		
        	} else if (cmFundTransfer.getFtVoid() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyVoidException();
        	}
        	
        	// post fund transfer
        	
        	if (cmFundTransfer.getFtVoid() == EJBCommon.FALSE && cmFundTransfer.getFtPosted() == EJBCommon.FALSE) {            
        		
        		// update bank account from balance (decrease) 
        		
				LocalAdBankAccount adBankAccountFrom = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());
				
				try {
					
					// find bankaccount balance before or equal receipt date
					
					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountFrom(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {
						
						// get last check
						
						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
						
						if (adBankAccountBalance.getBabDate().before(cmFundTransfer.getFtDate())) {
							
							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									cmFundTransfer.getFtDate(), adBankAccountBalance.getBabBalance() - cmFundTransfer.getFtAmount(), "BOOK", AD_CMPNY);
							
							adBankAccountFrom.addAdBankAccountBalance(adNewBankAccountBalance);
							
						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmFundTransfer.getFtAmount());
							
						} 
						
					} else {        	
						
						// create new balance
						
						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								cmFundTransfer.getFtDate(), (0 - cmFundTransfer.getFtAmount()), "BOOK", AD_CMPNY);
						
						adBankAccountFrom.addAdBankAccountBalance(adNewBankAccountBalance);
						
					}
					
					// propagate to subsequent balances if necessary
					
					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountFrom(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();
					
					while (i.hasNext()) {
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
						
						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmFundTransfer.getFtAmount());
						
					}			
					
				} catch (Exception ex) {
					
					ex.printStackTrace();
					
				}
        		
        		// update bank account to balance (increase)
        		
				LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountTo());
				
				try {
					
					// find bankaccount balance before or equal receipt date
					
					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountTo(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {
						
						// get last check
						
						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
						
						if (adBankAccountBalance.getBabDate().before(cmFundTransfer.getFtDate())) {
							
							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									cmFundTransfer.getFtDate(), adBankAccountBalance.getBabBalance() + cmFundTransfer.getFtAmount(), "BOOK", AD_CMPNY);
							
							adBankAccountTo.addAdBankAccountBalance(adNewBankAccountBalance);
							
						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmFundTransfer.getFtAmount());
							
						} 
						
					} else {        	
						
						// create new balance
						
						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								cmFundTransfer.getFtDate(), (cmFundTransfer.getFtAmount()), "BOOK", AD_CMPNY);
						
						adBankAccountTo.addAdBankAccountBalance(adNewBankAccountBalance);
						
					}
					
					// propagate to subsequent balances if necessary
					
					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountTo(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();
					
					while (i.hasNext()) {
						
						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
						
						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmFundTransfer.getFtAmount());
						
					}			
					
				} catch (Exception ex) {
					
					ex.printStackTrace();
					
				}
				
        	}
        	
        	// set post status
        	
        	cmFundTransfer.setFtPosted(EJBCommon.TRUE);
        	cmFundTransfer.setFtPostedBy(USR_NM);
        	cmFundTransfer.setFtDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        	
        	// post to gl if necessary
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		
        		// validate if date has no period and period is closed
        		
        		LocalGlSetOfBook glJournalSetOfBook = null;
        		
        		try {
        			
        			glJournalSetOfBook = glSetOfBookHome.findByDate(cmFundTransfer.getFtDate(), AD_CMPNY);
        			
        		} catch (FinderException ex) {
        			
        			throw new GlJREffectiveDateNoPeriodExistException();
        			
        		}
        		
        		LocalGlAccountingCalendarValue glAccountingCalendarValue = 
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), cmFundTransfer.getFtDate(), AD_CMPNY);
        		
        		
        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
        			
        			throw new GlJREffectiveDatePeriodClosedException();
        			
        		}
        		
        		// check if invoice is balance if not check suspense posting
        		
        		LocalGlJournalLine glOffsetJournalLine = null;
        		
        		Collection cmDistributionRecords = cmDistributionRecordHome.findByDrReversalAndDrImportedAndFtCode(EJBCommon.FALSE, EJBCommon.FALSE, cmFundTransfer.getFtCode(), AD_CMPNY);
        		
        		Iterator j = cmDistributionRecords.iterator();
        		
        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;
        		
        		while (j.hasNext()) {
        			
        			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();
        			
        			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());
        			
        			double DR_AMNT = this.convertForeignToFunctionalCurrency(adBankAccount.getGlFunctionalCurrency().getFcCode(),
        					adBankAccount.getGlFunctionalCurrency().getFcName(), 
							cmFundTransfer.getFtConversionDate(),
							cmFundTransfer.getFtConversionRateFrom(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);
        			
        			if (cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
        				
        				TOTAL_DEBIT += DR_AMNT;
        				
        			} else {
        				
        				TOTAL_CREDIT += DR_AMNT;
        				
        			}
        			
        		}
        		
        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		
        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			LocalGlSuspenseAccount glSuspenseAccount = null;
        			
        			try { 	
        				
        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("CASH MANAGEMENT", "FUND TRANSFERS", AD_CMPNY);
        				
        			} catch (FinderException ex) {
        				
        				throw new GlobalJournalNotBalanceException();
        				
        			}
        			
        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(cmDistributionRecords.size() + 1),
								EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
        				
        			} else {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(cmDistributionRecords.size() + 1),
								EJBCommon.FALSE,
								TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
        				
        			}
        			
        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        			
        			
        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			throw new GlobalJournalNotBalanceException();		    	
        			
        		}
        		
        		// create journal batch if necessary
        		
        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        		
        		try {
        			
        			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " FUND TRANSFERS", AD_BRNCH, AD_CMPNY);		       	   
        			
        		} catch (FinderException ex) {
        		}
        		
        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE && 
        				glJournalBatch == null) {
        			
        			glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " FUND TRANSFERS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        			
        		}
        		
        		// create journal entry			            	
        		
        		LocalGlJournal glJournal = glJournalHome.create(cmFundTransfer.getFtReferenceNumber(),
        				cmFundTransfer.getFtMemo(), cmFundTransfer.getFtDate(),
						0.0d, null, cmFundTransfer.getFtDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);
        		
        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("CASH MANAGEMENT", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);
        		
        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
        		
        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("FUND TRANSFERS", AD_CMPNY);
        		glJournal.setGlJournalCategory(glJournalCategory);
        		
        		if (glJournalBatch != null) {
        			
        			glJournal.setGlJournalBatch(glJournalBatch);
        			
        		}           		    
        		
        		
        		// create journal lines
        		
        		j = cmDistributionRecords.iterator();
        		
        		while (j.hasNext()) {
        			
        			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();
        			
        			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());
        			
        			double DR_AMNT = this.convertForeignToFunctionalCurrency(adBankAccount.getGlFunctionalCurrency().getFcCode(),
        					adBankAccount.getGlFunctionalCurrency().getFcName(), 
							cmFundTransfer.getFtConversionDate(),
							cmFundTransfer.getFtConversionRateFrom(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);
        			
        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
        					cmDistributionRecord.getDrLine(),	            			
							cmDistributionRecord.getDrDebit(),
							DR_AMNT, "", AD_CMPNY);
        			
        			cmDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
        			
        			glJournal.addGlJournalLine(glJournalLine);
        			
        			cmDistributionRecord.setDrImported(EJBCommon.TRUE);
        			
            	    LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountTo());
        			
    		  	  	// for FOREX revaluation
    		  	  	if(((adBankAccount.getGlFunctionalCurrency().getFcCode() !=
		  	  					adCompany.getGlFunctionalCurrency().getFcCode()) &&
		  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
		  	  				(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
		  	  				adBankAccount.getGlFunctionalCurrency().getFcCode()))) || 
		  	  			((adBankAccountTo.getGlFunctionalCurrency().getFcCode() !=
		  	  					adCompany.getGlFunctionalCurrency().getFcCode()) &&
		  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
		  	  				(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
		  	  				adBankAccountTo.getGlFunctionalCurrency().getFcCode())))){
    		  	  		
    		  	  		double CONVERSION_RATE = 1;
    		  	  		
    		  	  		if (glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
    		  	  			adBankAccount.getGlFunctionalCurrency().getFcCode()) &&
							cmFundTransfer.getFtConversionRateFrom() != 0 && cmFundTransfer.getFtConversionRateFrom() != 1) {
    		  	  			
    		  	  			CONVERSION_RATE = cmFundTransfer.getFtConversionRateFrom();

    		  	  		}else if (glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
    		  	  			adBankAccountTo.getGlFunctionalCurrency().getFcCode()) && 
							cmFundTransfer.getFtConversionRateTo() != 0 && cmFundTransfer.getFtConversionRateTo() != 1) {
    		  	  			
    		  	  			CONVERSION_RATE = cmFundTransfer.getFtConversionRateTo();

    		  	  		} else if (cmFundTransfer.getFtConversionDate() != null){
    		  	  			
    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
    								glJournal.getJrConversionDate(), AD_CMPNY);
    		  	  			
    		  	  		}
    		  	  		
    		  	  		Collection glForexLedgers = null;
    		  	  		
    		  	  		try {
    		  	  			
    		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		  	  				cmFundTransfer.getFtDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);
    		  	  			
    		  	  		} catch(FinderException ex) {
    		  	  			
    		  	  		}
    		  	  		
    		  	  		LocalGlForexLedger glForexLedger =
    		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();
    		  	  		
    		  	  		int FRL_LN = (glForexLedger != null &&
    		  	  				glForexLedger.getFrlDate().compareTo(cmFundTransfer.getFtDate()) == 0) ?
    		  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;
    		  	  		
    		  	  		// compute balance
    		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		  	  		double FRL_AMNT = cmDistributionRecord.getDrAmount();
    		  	  		
    		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));  
    		  	  		else
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);
    		  	  		
    		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
    		  	  		
    		  	  		glForexLedger = glForexLedgerHome.create(cmFundTransfer.getFtDate(), new Integer (FRL_LN),
    		  	  			"OTH", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);
    		  	  		
    		  	  		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
    		  	  		
    		  	  		// propagate balances
    		  	  		try{
    		  	  			
    		  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		  	  				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
								glForexLedger.getFrlAdCompany());
    		  	  			
    		  	  		} catch (FinderException ex) {
    		  	  			
    		  	  		}
    		  	  		
    		  	  		Iterator itrFrl = glForexLedgers.iterator();
    		  	  		
    		  	  		while (itrFrl.hasNext()) {
    		  	  			
    		  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		  	  			FRL_AMNT = cmDistributionRecord.getDrAmount();
    		  	  			
    		  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT :
    		  	  					(- 1 * FRL_AMNT));
    		  	  			else
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) :
    		  	  					FRL_AMNT);
    		  	  			
    		  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);
    		  	  			
    		  	  		}
    		  	  		
    		  	  	}        			

        		}
        		
        		if (glOffsetJournalLine != null) {
        			
        			glJournal.addGlJournalLine(glOffsetJournalLine);
        			
        		}		
        		
        		// post journal to gl
        		
        		Collection glJournalLines = glJournal.getGlJournalLines();
        		
        		Iterator i = glJournalLines.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
        			
        			// post current to current acv
        			
        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);
        			
        			
        			// post to subsequent acvs (propagate)
        			
        			Collection glSubsequentAccountingCalendarValues = 
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
        			
        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
        			
        			while (acvsIter.hasNext()) {
        				
        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        					(LocalGlAccountingCalendarValue)acvsIter.next();
        				
        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);
        				
        			}
        			
        			// post to subsequent years if necessary
        			
        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
        			
        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
        				
        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
        				
        				Iterator sobIter = glSubsequentSetOfBooks.iterator();
        				
        				while (sobIter.hasNext()) {
        					
        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
        					
        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
        					
        					// post to subsequent acvs of subsequent set of book(propagate)
        					
        					Collection glAccountingCalendarValues = 
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
        					
        					Iterator acvIter = glAccountingCalendarValues.iterator();
        					
        					while (acvIter.hasNext()) {
        						
        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        							(LocalGlAccountingCalendarValue)acvIter.next();
        						
        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);
        							
        						} else { // revenue & expense
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);					        
        							
        						}
        						
        					}
        					
        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
        					
        				}

        			}
        			
        		}	   			   
        		
        	}
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyVoidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("CmApprovalControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
	
   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
      LocalGlChartOfAccount glChartOfAccount, 
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_BRNCH, Integer AD_CMPNY) {
      	
      Debug.print("CmApprovalControllerBean postToGl");
      
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
              
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {          
               
               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
       	   
	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance = 
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
	               	  
	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
	           
	           
	               	  
	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {				    
				    			
 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
				    
		
			  } else {
	
				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
	
		 	  }
		 	  
		 	  if (isCurrentAcv) { 
		 	 
			 	 if (isDebit == EJBCommon.TRUE) {
			 	 	
		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 				
		 		 } else {
		 		 	
		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 		 }       	   
		 		 
		 	}
       	
       } catch (Exception ex) {
       	
       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());
       	
       }
      
      
   }		    
    
   private double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY) 
   throws GlobalConversionDateNotExistException {
   	
		Debug.print("CmApprovalControllerBean getFrRateByFrNameAndFrDate");
		
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			
			double CONVERSION_RATE = 1;
			
			// Get functional currency rate
			
			if (!FC_NM.equals("USD")) {
				
				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
							CONVERSION_DATE, AD_CMPNY);
				
				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
				
			}
			
			// Get set of book functional currency rate if necessary
			
			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
				
				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
							CONVERSION_DATE, AD_CMPNY);
				
				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();
				
			}
			
			return CONVERSION_RATE;
			
		} catch (FinderException ex) {	
			
			getSessionContext().setRollbackOnly();
			throw new GlobalConversionDateNotExistException();  
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

   
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmApprovalControllerBean ejbCreate");
      
    }
}
