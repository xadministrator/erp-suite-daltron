
/*
 * CmBankReconciliationControllerBean.java
 *
 * Created on November 5, 2003, 11:13 AM
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Collections;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdModBankAccountDetails;
import com.util.ArRepSalesRegisterDetails;
import com.util.CmModBankReconciliationDetails;
import com.util.CmRepBankReconciliationDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;

/**
 * @ejb:bean name="CmBankReconciliationControllerEJB"
 *           display-name="Used for reconciling bank accounts"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmBankReconciliationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmBankReconciliationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmBankReconciliationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 *
 */

public class CmBankReconciliationControllerBean extends AbstractSessionBean {


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("CmBankReconciliationControllerBean getAdBaAll");

		LocalAdBankAccountHome adBankAccountHome = null;

		Collection adBankAccounts = null;

		ArrayList list = new ArrayList();

		//initialized EJB Home

		try {

			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adBankAccounts = adBankAccountHome.findEnabledAndNotCashAccountBaAll(AD_BRNCH, AD_CMPNY);

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (adBankAccounts.isEmpty()) {

			return null;

		}

		Iterator i = adBankAccounts.iterator();

		while (i.hasNext()) {

			LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();

			list.add(adBankAccount.getBaName());

		}

		return list;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {

		Debug.print("CmBankReconciliationControllerBean getGlFcAllWithDefault");

		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		Collection glFunctionalCurrencies = null;

		LocalGlFunctionalCurrency glFunctionalCurrency = null;
		LocalAdCompany adCompany = null;


		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
					EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (glFunctionalCurrencies.isEmpty()) {

			return null;

		}

		Iterator i = glFunctionalCurrencies.iterator();

		while (i.hasNext()) {

			glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

			GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
					glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
					adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
							EJBCommon.TRUE : EJBCommon.FALSE);

			list.add(mdetails);

		}

		return list;

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdModBankAccountDetails getAdBaByBaName(String BA_NM, Date RCNCL_DT, Integer AD_CMPNY) throws
		GlobalRecordAlreadyExistException {

		Debug.print("CmBankReconciliationControllerBean getAdBaByBaName");

		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;

		//initialized EJB Home

		try {

			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		double bankAccountBalance = 0;
		double lastReconciledBalance = 0;
		Date lastReconciledDate = null;

		try {

			LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

			// check if there's existing prior bank recon

			Collection adExistingBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(RCNCL_DT, adBankAccount.getBaCode(), "RECON", AD_CMPNY);

			if(!adExistingBankAccountBalances.isEmpty())
				throw new GlobalRecordAlreadyExistException();

			// get latest bank account balance for current bank account as type "BOOK"

			Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(RCNCL_DT, adBankAccount.getBaCode(), "BOOK", AD_CMPNY);

			if (!adBankAccountBalances.isEmpty()) {

				// get last check

				ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

				LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

				bankAccountBalance = adBankAccountBalance.getBabBalance();

			}

			// get latest bank account balance for current bank account as type "RECON"

			adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(RCNCL_DT, adBankAccount.getBaCode(), "RECON", AD_CMPNY);

			if (!adBankAccountBalances.isEmpty()) {

				// get last check

				ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

				LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

				lastReconciledBalance = adBankAccountBalance.getBabBalance();
				lastReconciledDate = adBankAccountBalance.getBabDate();

			}

			AdModBankAccountDetails mdetails = new AdModBankAccountDetails();

			/*mdetails.setBaAvailableBalance(bankAccountBalance);
			mdetails.setBaLastReconciledDate(adBankAccount.getBaLastReconciledDate());
			mdetails.setBaLastReconciledBalance(adBankAccount.getBaLastReconciledBalance());
			mdetails.setBaFcName(adBankAccount.getGlFunctionalCurrency().getFcName());
			*/

			mdetails.setBaAvailableBalance(bankAccountBalance);
			mdetails.setBaLastReconciledBalance(lastReconciledBalance);
			mdetails.setBaLastReconciledDate(lastReconciledDate);
			mdetails.setBaFcName(adBankAccount.getGlFunctionalCurrency().getFcName());

			return mdetails;


		} catch (GlobalRecordAlreadyExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getCmDepositInTransitByBaName(String BA_NM, Date RCNCL_DT, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("CmBankReconciliationControllerBean getCmDepositInTransitByBaName");

		LocalArReceiptHome arReceiptHome = null;
		LocalCmAdjustmentHome cmAdjustmentHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;

		ArrayList list = new ArrayList();

		//initialized EJB Home

		try {

			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get cash receipts

			LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

			Collection arReceipts = arReceiptHome.findUnreconciledPostedRctByDateAndBaName(RCNCL_DT, BA_NM, AD_CMPNY);
			System.out.println("arReceipts.size()="+arReceipts.size());
			Iterator i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(arReceipt.getRctCode());
				mdetails.setBrType("RECEIPT CASH");
				mdetails.setBrDate(arReceipt.getRctDate());
				mdetails.setBrDocumentNumber(arReceipt.getRctNumber());
				mdetails.setBrReferenceNumber(arReceipt.getRctReferenceNumber());
				mdetails.setBrNumber("CASH");
				//mdetails.setBrAmount(arReceipt.getRctAmount());

				//pick up of receipt from convert to php
				LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());


				if ((adBankAccount.getBaCode()==adBankAccountTo.getBaCode())&& (arReceipt.getRctConversionRate() > 1))
					{
						mdetails.setBrAmount(arReceipt.getRctAmountCash() * arReceipt.getRctConversionRate());
					}
				else
					{
						mdetails.setBrAmount(arReceipt.getRctAmountCash());
					}

				mdetails.setBrName(arReceipt.getArCustomer().getCstCustomerCode());

				list.add(mdetails);

			}




			// get card 1

			adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

			Collection arReceiptsCard1 = arReceiptHome.findUnreconciledPostedCard1RctByDateAndBaName(RCNCL_DT, BA_NM, AD_CMPNY);
			System.out.println("arReceipts.size()="+arReceiptsCard1.size());
			i = arReceiptsCard1.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(arReceipt.getRctCode());
				mdetails.setBrType("RECEIPT CARD1");
				mdetails.setBrDate(arReceipt.getRctDate());
				mdetails.setBrDocumentNumber(arReceipt.getRctNumber());
				mdetails.setBrReferenceNumber(arReceipt.getRctReferenceNumber());
				mdetails.setBrNumber(arReceipt.getRctCardNumber1());

				//pick up of receipt from convert to php
				LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard1().getBaCode());


				if ((adBankAccount.getBaCode()==adBankAccountTo.getBaCode())&& (arReceipt.getRctConversionRate() > 1))
					{
						mdetails.setBrAmount(arReceipt.getRctAmountCard1() * arReceipt.getRctConversionRate());
					}
				else
					{
						mdetails.setBrAmount(arReceipt.getRctAmountCard1());
					}

				mdetails.setBrName(arReceipt.getArCustomer().getCstCustomerCode());

				list.add(mdetails);

			}


			// get card 2

			adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

			Collection arReceiptsCard2 = arReceiptHome.findUnreconciledPostedCard2RctByDateAndBaName(RCNCL_DT, BA_NM, AD_CMPNY);
			System.out.println("arReceiptsCard2.size()="+arReceiptsCard2.size());
			i = arReceiptsCard2.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(arReceipt.getRctCode());
				mdetails.setBrType("RECEIPT CARD2");
				mdetails.setBrDate(arReceipt.getRctDate());
				mdetails.setBrDocumentNumber(arReceipt.getRctNumber());
				mdetails.setBrReferenceNumber(arReceipt.getRctReferenceNumber());
				mdetails.setBrNumber(arReceipt.getRctCardNumber2());

				//pick up of receipt from convert to php
				LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard2().getBaCode());


				if ((adBankAccount.getBaCode()==adBankAccountTo.getBaCode())&& (arReceipt.getRctConversionRate() > 1))
					{
						mdetails.setBrAmount(arReceipt.getRctAmountCard2() * arReceipt.getRctConversionRate());
					}
				else
					{
						mdetails.setBrAmount(arReceipt.getRctAmountCard2());
					}

				mdetails.setBrName(arReceipt.getArCustomer().getCstCustomerCode());

				list.add(mdetails);

			}



			// get card 3

			adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

			Collection arReceiptsCard3 = arReceiptHome.findUnreconciledPostedCard3RctByDateAndBaName(RCNCL_DT, BA_NM, AD_CMPNY);
			System.out.println("arReceiptsCard3.size()="+arReceiptsCard3.size());
			i = arReceiptsCard3.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(arReceipt.getRctCode());
				mdetails.setBrType("RECEIPT CARD3");
				mdetails.setBrDate(arReceipt.getRctDate());
				mdetails.setBrDocumentNumber(arReceipt.getRctNumber());
				mdetails.setBrReferenceNumber(arReceipt.getRctReferenceNumber());
				mdetails.setBrNumber(arReceipt.getRctCardNumber3());

				//pick up of receipt from convert to php
				LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccountCard3().getBaCode());


				if ((adBankAccount.getBaCode()==adBankAccountTo.getBaCode()) && (arReceipt.getRctConversionRate() > 1))
					{
						mdetails.setBrAmount(arReceipt.getRctAmountCard3() * arReceipt.getRctConversionRate());
					}
				else
					{
						mdetails.setBrAmount(arReceipt.getRctAmountCard3());
					}

				mdetails.setBrName(arReceipt.getArCustomer().getCstCustomerCode());

				list.add(mdetails);

			}


			// get check receipts

			adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

			Collection arReceiptsCheque = arReceiptHome.findUnreconciledPostedChequeRctByDateAndBaName(RCNCL_DT, BA_NM, AD_CMPNY);
			System.out.println("arReceiptsCheque.size()="+arReceiptsCheque.size());
			i = arReceiptsCheque.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(arReceipt.getRctCode());
				mdetails.setBrType("RECEIPT CHEQUE");
				mdetails.setBrDate(arReceipt.getRctDate());
				mdetails.setBrDocumentNumber(arReceipt.getRctNumber());
				mdetails.setBrReferenceNumber(arReceipt.getRctReferenceNumber());
				mdetails.setBrNumber(arReceipt.getRctChequeNumber());

				//pick up of receipt from convert to php
				LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(arReceipt.getAdBankAccount().getBaCode());


				if ((adBankAccount.getBaCode()==adBankAccountTo.getBaCode())&& (arReceipt.getRctConversionRate() > 1))
					{
						mdetails.setBrAmount(arReceipt.getRctAmountCheque() * arReceipt.getRctConversionRate());
					}
				else
					{
						mdetails.setBrAmount(arReceipt.getRctAmountCheque());
					}

				mdetails.setBrName(arReceipt.getArCustomer().getCstCustomerCode());

				list.add(mdetails);

			}






			// get bank debits

			Collection cmAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "DEBIT MEMO", AD_CMPNY);
			System.out.println("DEBIT cmAdjustments.size()="+cmAdjustments.size());
			i = cmAdjustments.iterator();

			while (i.hasNext()) {

				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(cmAdjustment.getAdjCode());
				mdetails.setBrType("DEBIT MEMO");
				mdetails.setBrDate(cmAdjustment.getAdjDate());
				mdetails.setBrDocumentNumber(cmAdjustment.getAdjDocumentNumber());
				mdetails.setBrReferenceNumber(cmAdjustment.getAdjReferenceNumber());
				mdetails.setBrAmount(cmAdjustment.getAdjAmount());

				list.add(mdetails);

			}


			// get CM ADVANCE

			Collection cmAdjustmentAdvances = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "ADVANCE", AD_CMPNY);

			i = cmAdjustmentAdvances.iterator();
			System.out.println("cmAdjustmentAdvances="+cmAdjustmentAdvances.size());
			while (i.hasNext()) {

				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(cmAdjustment.getAdjCode());
				mdetails.setBrType("ADVANCE");
				mdetails.setBrDate(cmAdjustment.getAdjDate());
				mdetails.setBrDocumentNumber(cmAdjustment.getAdjDocumentNumber());
				mdetails.setBrReferenceNumber(cmAdjustment.getAdjReferenceNumber());
				mdetails.setBrName(cmAdjustment.getArCustomer().getCstCustomerCode());

				mdetails.setBrAmount(cmAdjustment.getAdjAmount());

				list.add(mdetails);

			}

			cmAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "INTEREST", AD_CMPNY);
			System.out.println("Interest cmAdjustments="+cmAdjustments.size());
			i = cmAdjustments.iterator();

			while (i.hasNext()) {

				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(cmAdjustment.getAdjCode());
				mdetails.setBrType("INTEREST");
				mdetails.setBrDate(cmAdjustment.getAdjDate());
				mdetails.setBrDocumentNumber(cmAdjustment.getAdjDocumentNumber());
				mdetails.setBrReferenceNumber(cmAdjustment.getAdjReferenceNumber());
				mdetails.setBrAmount(cmAdjustment.getAdjAmount());

				list.add(mdetails);

			}

			// get fund transfers


			Collection cmFundTransfers = cmFundTransferHome.findUnreconciledPostedFtAccountToByDateAndBaCode(RCNCL_DT, adBankAccount.getBaCode(), AD_CMPNY);
			System.out.println("cmFundTransfers="+cmFundTransfers.size());
			i = cmFundTransfers.iterator();

			while (i.hasNext()) {

				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(cmFundTransfer.getFtCode());
				mdetails.setBrType("FUND TRANSFER IN");
				mdetails.setBrDate(cmFundTransfer.getFtDate());
				mdetails.setBrDocumentNumber(cmFundTransfer.getFtDocumentNumber());
				mdetails.setBrReferenceNumber(cmFundTransfer.getFtReferenceNumber());

				//pick up of fund transfer from convert to php
				LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountTo());


				if ((adBankAccount.getBaCode()==adBankAccountTo.getBaCode())&& (cmFundTransfer.getFtConversionRateFrom() > 1))
					{
						mdetails.setBrAmount(cmFundTransfer.getFtAmount() * cmFundTransfer.getFtConversionRateFrom());
					}
				else
					{
						mdetails.setBrAmount(cmFundTransfer.getFtAmount());
					}


				list.add(mdetails);

			}
			System.out.println("list="+list.size());
			Collections.sort(list, CmModBankReconciliationDetails.NoGroupComparator);

			System.out.println("RETURN---------------------->");
			return list;


		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}


	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getCmOutstandingCheckByBaName(String BA_NM, Date RCNCL_DT, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("CmBankReconciliationControllerBean getCmOutstandingCheckByBaName");

		LocalApCheckHome apCheckHome = null;
		LocalCmAdjustmentHome cmAdjustmentHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;


		ArrayList list = new ArrayList();

		//initialized EJB Home

		try {

			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {


			Collection apChecks = apCheckHome.findUnreconciledPostedChkByDateAndBaName(RCNCL_DT, BA_NM, AD_CMPNY);

			Iterator i = apChecks.iterator();

			while (i.hasNext()) {

				LocalApCheck apCheck = (LocalApCheck)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(apCheck.getChkCode());
				mdetails.setBrType("CHECK");
				mdetails.setBrDate(apCheck.getChkDate());
				mdetails.setBrDocumentNumber(apCheck.getChkDocumentNumber());
				mdetails.setBrReferenceNumber(apCheck.getChkReferenceNumber());
				mdetails.setBrNumber(apCheck.getChkNumber());
				mdetails.setBrAmount(apCheck.getChkAmount());
				mdetails.setBrName(apCheck.getApSupplier().getSplSupplierCode());
				list.add(mdetails);

			}

			System.out.println("apChecks="+apChecks.size());

			// get bank credits

			Collection cmAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "CREDIT MEMO", AD_CMPNY);

			i = cmAdjustments.iterator();

			while (i.hasNext()) {

				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(cmAdjustment.getAdjCode());
				mdetails.setBrType("CREDIT MEMO");
				mdetails.setBrDate(cmAdjustment.getAdjDate());
				mdetails.setBrDocumentNumber(cmAdjustment.getAdjDocumentNumber());
				mdetails.setBrReferenceNumber(cmAdjustment.getAdjReferenceNumber());

				mdetails.setBrAmount(cmAdjustment.getAdjAmount());

				list.add(mdetails);

			}

			cmAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "BANK CHARGE", AD_CMPNY);

			i = cmAdjustments.iterator();

			while (i.hasNext()) {

				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(cmAdjustment.getAdjCode());
				mdetails.setBrType("BANK CHARGE");
				mdetails.setBrDate(cmAdjustment.getAdjDate());
				mdetails.setBrDocumentNumber(cmAdjustment.getAdjDocumentNumber());
				mdetails.setBrReferenceNumber(cmAdjustment.getAdjReferenceNumber());

				mdetails.setBrAmount(cmAdjustment.getAdjAmount());

				list.add(mdetails);

			}

			// get fund transfers

			LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);
			Collection cmFundTransfers = cmFundTransferHome.findUnreconciledPostedFtAccountFromByDateAndBaCode(RCNCL_DT, adBankAccount.getBaCode(), AD_CMPNY);

			i = cmFundTransfers.iterator();

			while (i.hasNext()) {

				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();

				CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();

				mdetails.setBrCode(cmFundTransfer.getFtCode());
				mdetails.setBrType("FUND TRANSFER OUT");
				mdetails.setBrDate(cmFundTransfer.getFtDate());
				mdetails.setBrDocumentNumber(cmFundTransfer.getFtDocumentNumber());
				mdetails.setBrReferenceNumber(cmFundTransfer.getFtReferenceNumber());

				mdetails.setBrAmount(cmFundTransfer.getFtAmount());

				list.add(mdetails);

			}
			Collections.sort(list, CmModBankReconciliationDetails.NoGroupComparator);

			return list;


		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}


	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void executeCmBankReconciliation(double OPNNG_BLNC, double ENDNG_BLNC, String BA_NM, com.util.CmAdjustmentDetails adjustmentDetails,
			com.util.CmAdjustmentDetails interestDetails, com.util.CmAdjustmentDetails serviceChargeDetails,
			ArrayList depositInTransitList, ArrayList outstandingCheckList, Date reconcileDate, boolean autoAdjust, Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalTransactionAlreadyPostedException,
			GlobalTransactionAlreadyVoidException,
			GlobalRecordAlreadyDeletedException,
			GlJREffectiveDateNoPeriodExistException,
			GlJREffectiveDatePeriodClosedException,
			GlobalJournalNotBalanceException {

		Debug.print("CmBankReconciliationControllerBean executeCmBankReconciliation");

		LocalAdBankAccountHome adBankAccountHome = null;
		LocalCmAdjustmentHome cmAdjustmentHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalApCheckHome apCheckHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;

		//initialized EJB Home

		try {

			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get bank account

			LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

			// get last cleared balance

			double CLRD_BLNC = OPNNG_BLNC;

			// reconcile deposit in transit
			System.out.println("depositInTransitList.size()="+depositInTransitList.size());
			Iterator i = depositInTransitList.iterator();

			while (i.hasNext()) {

				CmModBankReconciliationDetails mdetails = (CmModBankReconciliationDetails)i.next();

				if (mdetails.getBrType().equals("RECEIPT CASH")) {

					LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(mdetails.getBrCode());

					arReceipt.setRctReconciled(EJBCommon.TRUE);
					arReceipt.setRctDateReconciled(mdetails.getBrDateCleared());

					// update cleared balance

					CLRD_BLNC += arReceipt.getRctAmountCash();

				} else if (mdetails.getBrType().equals("RECEIPT CHEQUE")) {

					LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(mdetails.getBrCode());

					arReceipt.setRctReconciledCheque(EJBCommon.TRUE);
					arReceipt.setRctDateReconciledCheque(mdetails.getBrDateCleared());

					// update cleared balance

					CLRD_BLNC += arReceipt.getRctAmountCheque();

				} else if (mdetails.getBrType().equals("RECEIPT CARD1")) {

					LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(mdetails.getBrCode());

					arReceipt.setRctReconciledCard1(EJBCommon.TRUE);
					arReceipt.setRctDateReconciledCard1(mdetails.getBrDateCleared());

					// update cleared balance

					CLRD_BLNC += arReceipt.getRctAmountCard1();

				} else if (mdetails.getBrType().equals("RECEIPT CARD2")) {

					LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(mdetails.getBrCode());

					arReceipt.setRctReconciledCard2(EJBCommon.TRUE);
					arReceipt.setRctDateReconciledCard2(mdetails.getBrDateCleared());

					// update cleared balance

					CLRD_BLNC += arReceipt.getRctAmountCard2();


				} else if (mdetails.getBrType().equals("RECEIPT CARD3")) {

					LocalArReceipt arReceipt = arReceiptHome.findByPrimaryKey(mdetails.getBrCode());

					arReceipt.setRctReconciledCard3(EJBCommon.TRUE);
					arReceipt.setRctDateReconciledCard3(mdetails.getBrDateCleared());

					// update cleared balance

					CLRD_BLNC += arReceipt.getRctAmountCard3();

				} else if (mdetails.getBrType().equals("DEBIT MEMO") || mdetails.getBrType().equals("INTEREST") || mdetails.getBrType().equals("ADVANCE")) {

					LocalCmAdjustment cmAdjustment = cmAdjustmentHome.findByPrimaryKey(mdetails.getBrCode());

					cmAdjustment.setAdjReconciled(EJBCommon.TRUE);
					cmAdjustment.setAdjDateReconciled(mdetails.getBrDateCleared());

					// update cleared balance

					CLRD_BLNC += cmAdjustment.getAdjAmount();

				} else {

					LocalCmFundTransfer cmFundTransfer = cmFundTransferHome.findByPrimaryKey(mdetails.getBrCode());

					cmFundTransfer.setFtAccountToReconciled(EJBCommon.TRUE);
					cmFundTransfer.setFtAccountToDateReconciled(mdetails.getBrDateCleared());

					// update cleared balance

					CLRD_BLNC += cmFundTransfer.getFtAmount();
				}


			}

			// reconcile outstanding checks
			System.out.println("outstandingCheckList.size()="+outstandingCheckList.size());
			i = outstandingCheckList.iterator();

			while (i.hasNext()) {

				CmModBankReconciliationDetails mdetails = (CmModBankReconciliationDetails)i.next();

				if (mdetails.getBrType().equals("CHECK")) {

					LocalApCheck apCheck = apCheckHome.findByPrimaryKey(mdetails.getBrCode());

					apCheck.setChkReconciled(EJBCommon.TRUE);
					apCheck.setChkDateReconciled(mdetails.getBrDateCleared());

					// update cleared balance

					CLRD_BLNC -= apCheck.getChkAmount();

				} else if (mdetails.getBrType().equals("CREDIT MEMO") || mdetails.getBrType().equals("BANK CHARGE")) {

					LocalCmAdjustment cmAdjustment = cmAdjustmentHome.findByPrimaryKey(mdetails.getBrCode());

					cmAdjustment.setAdjReconciled(EJBCommon.TRUE);
					cmAdjustment.setAdjDateReconciled(mdetails.getBrDateCleared());

					// update cleared balance

					CLRD_BLNC -= cmAdjustment.getAdjAmount();

				} else {

					LocalCmFundTransfer cmFundTransfer = cmFundTransferHome.findByPrimaryKey(mdetails.getBrCode());

					cmFundTransfer.setFtAccountFromReconciled(EJBCommon.TRUE);
					cmFundTransfer.setFtAccountFromDateReconciled(mdetails.getBrDateCleared());

					// update cleared balance

					CLRD_BLNC -= cmFundTransfer.getFtAmount();
				}

			}


			// create interest adjustment if necessary

			if (interestDetails != null) {

				// generate document number

				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment =
					adDocumentSequenceAssignmentHome.findByDcName("CM ADJUSTMENT", AD_CMPNY);

				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

				try {

					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

				}

				String ADJ_DCMNT_NMBR = null;

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
						(ADJ_DCMNT_NMBR == null || ADJ_DCMNT_NMBR.trim().length() == 0)) {

					while (true) {

						if ( adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null ) {

							try {

								cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

							} catch (FinderException ex) {

								ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
								break;

							}

						} else {

							try {

								cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

							} catch (FinderException ex) {

								ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
								break;

							}

						}

					}

				}

				LocalCmAdjustment cmAdjustment = cmAdjustmentHome.create("INTEREST",
						interestDetails.getAdjDate(), ADJ_DCMNT_NMBR, interestDetails.getAdjReferenceNumber(), interestDetails.getAdjCheckNumber(),
						interestDetails.getAdjAmount(),0d, interestDetails.getAdjConversionDate(),
						interestDetails.getAdjConversionRate(), interestDetails.getAdjMemo(),null, EJBCommon.FALSE,
						EJBCommon.FALSE,
						EJBCommon.FALSE, 0d, null,
						EJBCommon.TRUE, interestDetails.getAdjDate(), "N/A", EJBCommon.FALSE,
						interestDetails.getAdjCreatedBy(), interestDetails.getAdjDateCreated(),
						interestDetails.getAdjLastModifiedBy(), interestDetails.getAdjDateLastModified(),
						null, null, interestDetails.getAdjPostedBy(), interestDetails.getAdjDatePosted(),
						null, AD_BRNCH, AD_CMPNY);

				adBankAccount.addCmAdjustment(cmAdjustment);

				// update bank balance

				try {

					// find bankaccount balance before or equal check date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(interestDetails.getAdjDate(), adBankAccount.getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(interestDetails.getAdjDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									interestDetails.getAdjDate(), adBankAccountBalance.getBabBalance() + interestDetails.getAdjAmount(), "BOOK", AD_CMPNY);

							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + interestDetails.getAdjAmount());

						}

					} else {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								interestDetails.getAdjDate(), (interestDetails.getAdjAmount()), "BOOK", AD_CMPNY);

						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(interestDetails.getAdjDate(), adBankAccount.getBaCode(), "BOOK", AD_CMPNY);

					Iterator j = adBankAccountBalances.iterator();

					while (j.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)j.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + interestDetails.getAdjAmount());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}

				// create distribution record

				this.createDistributionRecord(cmAdjustment, (short)1,
						interestDetails.getAdjDate(), "CASH",
						EJBCommon.TRUE, interestDetails.getAdjAmount(),
						EJBCommon.FALSE, adBankAccount.getBaCoaGlCashAccount(), AD_CMPNY);

				this.createDistributionRecord(cmAdjustment, (short)2,
						interestDetails.getAdjDate(), "BANK INTEREST",
						EJBCommon.FALSE, interestDetails.getAdjAmount(),
						EJBCommon.FALSE, adBankAccount.getBaCoaGlInterestAccount(), AD_CMPNY);

				// update cleared balance

				CLRD_BLNC += interestDetails.getAdjAmount();

				this.executeCmAdjPost(cmAdjustment.getAdjCode(), interestDetails.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

			}

			// create service charge adjustment if necessary

			if (serviceChargeDetails != null) {

				// generate document number

				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment =
					adDocumentSequenceAssignmentHome.findByDcName("CM ADJUSTMENT", AD_CMPNY);

				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

				try {

					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

				}

				String ADJ_DCMNT_NMBR = null;

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
						(ADJ_DCMNT_NMBR == null || ADJ_DCMNT_NMBR.trim().length() == 0)) {

					while (true) {

						if ( adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null ) {

							try {

								cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

							} catch (FinderException ex) {

								ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
								break;

							}

						} else {

							try {

								cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

							} catch (FinderException ex) {

								ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
								break;

							}

						}

					}

				}

				LocalCmAdjustment cmAdjustment = cmAdjustmentHome.create("BANK CHARGE",
						serviceChargeDetails.getAdjDate(), ADJ_DCMNT_NMBR, serviceChargeDetails.getAdjReferenceNumber(), serviceChargeDetails.getAdjCheckNumber(),
						serviceChargeDetails.getAdjAmount(),0d, serviceChargeDetails.getAdjConversionDate(),
						serviceChargeDetails.getAdjConversionRate(),  serviceChargeDetails.getAdjMemo(), null, EJBCommon.FALSE,
						EJBCommon.FALSE,
						EJBCommon.FALSE, 0d, null,

						EJBCommon.TRUE, serviceChargeDetails.getAdjDate(), "N/A", EJBCommon.FALSE,
						serviceChargeDetails.getAdjCreatedBy(), serviceChargeDetails.getAdjDateCreated(),
						serviceChargeDetails.getAdjLastModifiedBy(), serviceChargeDetails.getAdjDateLastModified(),
						null, null, serviceChargeDetails.getAdjPostedBy(), serviceChargeDetails.getAdjDatePosted(),
						null, AD_BRNCH, AD_CMPNY);

				adBankAccount.addCmAdjustment(cmAdjustment);

				// update bank balance

				try {

					// find bankaccount balance before or equal check date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(serviceChargeDetails.getAdjDate(), adBankAccount.getBaCode(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(serviceChargeDetails.getAdjDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									serviceChargeDetails.getAdjDate(), adBankAccountBalance.getBabBalance() - serviceChargeDetails.getAdjAmount(), "BOOK", AD_CMPNY);

							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - serviceChargeDetails.getAdjAmount());

						}

					} else {

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								serviceChargeDetails.getAdjDate(), (0 - serviceChargeDetails.getAdjAmount()), "BOOK", AD_CMPNY);

						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(serviceChargeDetails.getAdjDate(), adBankAccount.getBaCode(), "BOOK", AD_CMPNY);

					Iterator j = adBankAccountBalances.iterator();

					while (j.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)j.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - serviceChargeDetails.getAdjAmount());

					}

				} catch (Exception ex) {

					ex.printStackTrace();

				}

				// create distribution record

				this.createDistributionRecord(cmAdjustment, (short)1,
						serviceChargeDetails.getAdjDate(), "BANK CHARGE",
						EJBCommon.TRUE, serviceChargeDetails.getAdjAmount(),
						EJBCommon.FALSE, adBankAccount.getBaCoaGlBankChargeAccount(), AD_CMPNY);

				this.createDistributionRecord(cmAdjustment, (short)2,
						serviceChargeDetails.getAdjDate(), "CASH",
						EJBCommon.FALSE, serviceChargeDetails.getAdjAmount(),
						EJBCommon.FALSE, adBankAccount.getBaCoaGlCashAccount(), AD_CMPNY);

				// update cleared balance

				CLRD_BLNC -= serviceChargeDetails.getAdjAmount();

				this.executeCmAdjPost(cmAdjustment.getAdjCode(), serviceChargeDetails.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

			}


			// create adjustments if necessary

			CLRD_BLNC = EJBCommon.roundIt(CLRD_BLNC, this.getGlFcPrecisionUnit(AD_CMPNY));
			ENDNG_BLNC = EJBCommon.roundIt(ENDNG_BLNC, this.getGlFcPrecisionUnit(AD_CMPNY));


			// create recon bank account balance

			if (CLRD_BLNC != ENDNG_BLNC && autoAdjust) {

				LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
						reconcileDate, ENDNG_BLNC, "RECON", AD_CMPNY);

				adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

			} else {

				LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
						reconcileDate, CLRD_BLNC, "RECON", AD_CMPNY);

				adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);


			}


			if (CLRD_BLNC != ENDNG_BLNC && autoAdjust) {

				// generate document number

				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment =
					adDocumentSequenceAssignmentHome.findByDcName("CM ADJUSTMENT", AD_CMPNY);

				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

				try {

					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

				}

				String ADJ_DCMNT_NMBR = null;

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
						(ADJ_DCMNT_NMBR == null || ADJ_DCMNT_NMBR.trim().length() == 0)) {

					while (true) {

						if ( adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null ) {

							try {

								cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

							} catch (FinderException ex) {

								ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
								break;

							}

						} else {

							try {

								cmAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

							} catch (FinderException ex) {

								ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
								break;

							}

						}

					}

				}
				System.out.println("ADJ_DCMNT_NMBR="+ADJ_DCMNT_NMBR);
				LocalCmAdjustment cmAdjustment = cmAdjustmentHome.create(
						ENDNG_BLNC - CLRD_BLNC > 0 ? "DEBIT MEMO" : "CREDIT MEMO",adjustmentDetails.getAdjDate(),
						ADJ_DCMNT_NMBR, "RECONCILIATION ADJUSTMENT", adjustmentDetails.getAdjCheckNumber(),
						Math.abs(ENDNG_BLNC - CLRD_BLNC), 0d,
						adjustmentDetails.getAdjConversionDate(),adjustmentDetails.getAdjConversionRate(),
						"ADJUSTMENT",

						null, EJBCommon.FALSE ,
						EJBCommon.FALSE, EJBCommon.FALSE,
						0d, null,
		             	EJBCommon.TRUE, adjustmentDetails.getAdjDateReconciled(),
		             	"N/A", EJBCommon.TRUE,

		             	/*
		             	String ADJ_VD_APPRVL_STATUS, byte ADJ_VD_PSTD,
		                byte ADJ_VOID, byte ADJ_RFND,
		                double ADJ_RFND_AMNT, String ADJ_RFND_RFRNC_NMBR,
		                byte ADJ_RCNCLD, Date ADJ_DT_RCNCLD,
		                String ADJ_APPRVL_STATUS, byte ADJ_PSTD,


						EJBCommon.FALSE,EJBCommon.FALSE, EJBCommon.TRUE, 0d,
						adjustmentDetails.getAdjDateReconciled(),
						"N/A", EJBCommon.FALSE, */


						adjustmentDetails.getAdjCreatedBy(), adjustmentDetails.getAdjDateCreated(),
						adjustmentDetails.getAdjLastModifiedBy(), adjustmentDetails.getAdjDateLastModified(),
						null, null, null, null,null,
						AD_BRNCH, AD_CMPNY);
				cmAdjustment.setAdjPosted((byte)0);
				adBankAccount.addCmAdjustment(cmAdjustment);

				// create distribution record

				if (ENDNG_BLNC - CLRD_BLNC > 0) {

					this.createDistributionRecord(cmAdjustment, (short)1,
							adjustmentDetails.getAdjDate(), "CASH",
							EJBCommon.TRUE, Math.abs(ENDNG_BLNC - CLRD_BLNC),
							EJBCommon.FALSE, adBankAccount.getBaCoaGlCashAccount(), AD_CMPNY);

					this.createDistributionRecord(cmAdjustment, (short)2,
							adjustmentDetails.getAdjDate(), "ADJUSTMENT",
							EJBCommon.FALSE, Math.abs(ENDNG_BLNC - CLRD_BLNC),
							EJBCommon.FALSE, adBankAccount.getBaCoaGlAdjustmentAccount(), AD_CMPNY);

				} else {

					this.createDistributionRecord(cmAdjustment, (short)1,
							adjustmentDetails.getAdjDate(), "ADJUSTMENT",
							EJBCommon.TRUE, Math.abs(ENDNG_BLNC - CLRD_BLNC),
							EJBCommon.FALSE, adBankAccount.getBaCoaGlAdjustmentAccount(), AD_CMPNY);

					this.createDistributionRecord(cmAdjustment, (short)2,
							adjustmentDetails.getAdjDate(), "CASH",
							EJBCommon.FALSE, Math.abs(ENDNG_BLNC - CLRD_BLNC),
							EJBCommon.FALSE, adBankAccount.getBaCoaGlCashAccount(), AD_CMPNY);

				}
				System.out.println("pasok 111");
				this.executeCmAdjPost(cmAdjustment.getAdjCode(), adjustmentDetails.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);
			}


			// update bank account balances

			adBankAccount.setBaLastReconciledDate(EJBCommon.getGcCurrentDateWoTime().getTime());
			adBankAccount.setBaLastReconciledBalance(ENDNG_BLNC);



		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlJREffectiveDateNoPeriodExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlJREffectiveDatePeriodClosedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
		}


	}

	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public void recomputeAccountBalanceByBankCode(String BA_NM, Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("CmBankReconciliationControllerBean recomputeAccountBalance");


		LocalAdCompanyHome adCompanyHome = null;
		LocalApCheckHome apCheckHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalCmAdjustmentHome cmAdjustmentHome = null;
		LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;

		// Initialize EJB Home

		try { 

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);			
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
					lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);		
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
					lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
					lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
			adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
					lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}
		
		
		
		short FC_PRCSN = 0;
		
		try {
			
			try {

				LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	
				FC_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
	
			} catch (Exception ex) {
	
				throw new EJBException(ex.getMessage());
	
			}
			
			LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaNameAndBrCode(BA_NM, AD_BRNCH, AD_CMPNY);
			
			
			Collection adBankAccountBalances = adBankAccount.getAdBankAccountBalances();
			
			
			Iterator iBa = adBankAccountBalances.iterator();
			
			
			while(iBa.hasNext()) {
				
				
				LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)iBa.next();
				
				
				Date RCNCL_DT = adBankAccountBalance.getBabDate();
				
				/*
				Collection arReceipts = arReceiptHome.findPostedRctByDateAndBaName(RCNCL_DT, BA_NM,  AD_CMPNY);
				
				Collection cmDebitMemoAdjustments = cmAdjustmentHome.findPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "Debit Memo",   AD_CMPNY);
		
				Collection cmAdvanceAdjustments = cmAdjustmentHome.findPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "Advance",   AD_CMPNY);
				
				Collection cmInterestAdjustments = cmAdjustmentHome.findPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "Interest",   AD_CMPNY);
				
				Collection cmAccountToFundTransfers = cmFundTransferHome.findPostedFtAccountToByDateAndBaCode(RCNCL_DT, adBankAccount.getBaCode(),  AD_CMPNY);
				
				
			
				Collection apChecks = apCheckHome.findPostedChkByDateAndBaName(RCNCL_DT, BA_NM,  AD_CMPNY);
				
				Collection cmCreditMemoAdjustments = cmAdjustmentHome.findPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "Credit Memo",   AD_CMPNY);
				
				Collection cmBankChargeAdjustments = cmAdjustmentHome.findPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "Bank Charge",   AD_CMPNY);
				
				Collection cmAccountFromFundTransfers = cmFundTransferHome.findPostedFtAccountFromByDateAndBaCode(RCNCL_DT, adBankAccount.getBaCode(),  AD_CMPNY);
				*/
				double accountBalance = 0;
				
				
				// AR RECEIPT
				double totalReceiptAmount = 0;
				Iterator i ;
				Collection arReceipts = arReceiptHome.findUnreconciledPostedRctByDateAndBaName(RCNCL_DT, BA_NM,  AD_CMPNY);
				
			
				i = arReceipts.iterator();
				
				
				while(i.hasNext()) {
					LocalArReceipt arReceipt = (LocalArReceipt)i.next();
					
					 totalReceiptAmount += EJBCommon.roundIt(arReceipt.getRctAmountCash(), FC_PRCSN);
					
				}
				
				totalReceiptAmount = EJBCommon.roundIt(totalReceiptAmount, FC_PRCSN);
				System.out.println("Receipt amount total is: " + totalReceiptAmount);
				accountBalance += totalReceiptAmount;


				// CM DEBIT MEMO ADJ
				double totalDebitMemoAdjAmount = 0;
				Collection cmDebitMemoAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "Debit Memo",   AD_CMPNY);
		
				
				i = cmDebitMemoAdjustments.iterator();
				
				while(i.hasNext()) {
					LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
					totalDebitMemoAdjAmount +=  EJBCommon.roundIt(cmAdjustment.getAdjAmount(),FC_PRCSN);
				

				}
				
				totalDebitMemoAdjAmount = EJBCommon.roundIt(totalDebitMemoAdjAmount, FC_PRCSN);
				System.out.println("CM Debit Memo Adj amount total is: " + totalDebitMemoAdjAmount);
				accountBalance += totalDebitMemoAdjAmount;
				
				
				
				
				//CM ADVNCE ADJUSTMENT 
				double totalAdvanceAdjAmount = 0;
				Collection cmAdvanceAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "Advance",   AD_CMPNY);
				
				i = cmAdvanceAdjustments.iterator();
				
				while(i.hasNext()) {
					LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
					
					totalAdvanceAdjAmount += EJBCommon.roundIt(cmAdjustment.getAdjAmount(),FC_PRCSN);

				}
				
				totalAdvanceAdjAmount = EJBCommon.roundIt(totalAdvanceAdjAmount, FC_PRCSN);
				System.out.println("CM Advance Adj amount total is: " + totalAdvanceAdjAmount);
				accountBalance += totalAdvanceAdjAmount;
				
				
				//CM INTEREST ADJUSTMENT
				double totalInterestAdjAmount = 0;
				Collection cmInterestAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "Interest",   AD_CMPNY);
				
				i = cmInterestAdjustments.iterator();
				
				while(i.hasNext()) {
					LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
					
					totalInterestAdjAmount += EJBCommon.roundIt(cmAdjustment.getAdjAmount(),FC_PRCSN);

				}
				
				totalInterestAdjAmount = EJBCommon.roundIt(totalInterestAdjAmount, FC_PRCSN);
				System.out.println("CM Interest Adj amount total is: " + totalInterestAdjAmount);
				accountBalance += totalInterestAdjAmount;
				
				
				
				//CM ACCNT FUND TRANSFERS 
				double totalAccountToFundTransfersAmount = 0;
				Collection cmAccountToFundTransfers = cmFundTransferHome.findUnreconciledPostedFtAccountToByDateAndBaCode(RCNCL_DT, adBankAccount.getBaCode(),  AD_CMPNY);
				
				i = cmAccountToFundTransfers.iterator();
				
				while(i.hasNext()) {
					LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();
					
					totalAccountToFundTransfersAmount += EJBCommon.roundIt(cmFundTransfer.getFtAmount(),FC_PRCSN);

				}
				
				totalAccountToFundTransfersAmount = EJBCommon.roundIt(totalAccountToFundTransfersAmount, FC_PRCSN);
				System.out.println("CM Account to Fund amount total is: " + totalAccountToFundTransfersAmount);
				accountBalance += totalAccountToFundTransfersAmount;
				
				
			
			    //AP CHECK 
			    
			    double totalApCheckAmount = 0;
				Collection apChecks = apCheckHome.findUnreconciledPostedChkByDateAndBaName(RCNCL_DT, BA_NM,  AD_CMPNY);
				
				i = apChecks.iterator();
				
				while(i.hasNext()) {
					LocalApCheck apCheck = (LocalApCheck)i.next();
					
					totalApCheckAmount -= EJBCommon.roundIt(apCheck.getChkAmount(),FC_PRCSN);

				}
				
				totalApCheckAmount = EJBCommon.roundIt(totalApCheckAmount, FC_PRCSN);
				System.out.println("AP Check amount total is: " + totalApCheckAmount);
				accountBalance -= totalApCheckAmount;
				
				
				
				//CM Credit Memo Adjustment
				
				double totalCreditMemoAdjAmount = 0;
				Collection cmCreditMemoAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "Credit Memo",   AD_CMPNY);
				
				i = cmCreditMemoAdjustments.iterator();
				
				while(i.hasNext()) {
					LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
					
					totalCreditMemoAdjAmount -= EJBCommon.roundIt(cmAdjustment.getAdjAmount(),FC_PRCSN);

				}
				
				totalCreditMemoAdjAmount = EJBCommon.roundIt(totalCreditMemoAdjAmount, FC_PRCSN);
				System.out.println("CM Credit Memo Adj total is: " + totalCreditMemoAdjAmount);
				accountBalance -= totalCreditMemoAdjAmount;
				
				
				
				//CM Bank Charge Adj
				double totalBankChargeAdjAmount = 0;
				Collection cmBankChargeAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjType(RCNCL_DT, BA_NM, "Bank Charge",   AD_CMPNY);
				
				i= cmBankChargeAdjustments.iterator();
				
				while(i.hasNext()) {
					LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
					
					totalBankChargeAdjAmount -= EJBCommon.roundIt(cmAdjustment.getAdjAmount(),FC_PRCSN);

				}
				
				totalBankChargeAdjAmount = EJBCommon.roundIt(totalBankChargeAdjAmount, FC_PRCSN);
				System.out.println("CM Bank Charge Adj total is: " + totalBankChargeAdjAmount);
				accountBalance -= totalBankChargeAdjAmount;
				
				
				
				
				//CM Account from fund
				double totalAccountFromFundAmount = 0;
				Collection cmAccountFromFundTransfers = cmFundTransferHome.findUnreconciledPostedFtAccountFromByDateAndBaCode(RCNCL_DT, adBankAccount.getBaCode(),  AD_CMPNY);
				
				i= cmAccountToFundTransfers.iterator();
				
				while(i.hasNext()) {
					LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();
					
					totalAccountFromFundAmount -= EJBCommon.roundIt(cmFundTransfer.getFtAmount(),FC_PRCSN);

				}
				
				totalAccountFromFundAmount = EJBCommon.roundIt(totalAccountFromFundAmount, FC_PRCSN);
				System.out.println("CM Accnt from Fund Adj total is: " + totalAccountFromFundAmount);
				accountBalance -= totalAccountFromFundAmount;
				
				
				
				
				
				System.out.println("Account Balance in " + RCNCL_DT + " is: " + accountBalance);
				
				adBankAccountBalance.setBabBalance(accountBalance);
				
			}
			
	//		Collection adBankAccountBalances = adBankAccountBalanceHome.finb(BA_CODE, BAB_TYP, BAB_AD_CMPNY)apCheck.getChkCheckDate(), apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

			
			
			
			
			
			
		}catch(Exception ex) {
			
			
			getSessionContext().setRollbackOnly();
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
		}
		
		
		
		
	}
	
	

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("CmBankReconciliationControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	// private methods

	private void createDistributionRecord(LocalCmAdjustment cmAdjustment, short DR_LN,
			Date DR_DT, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL,
			Integer GL_COA, Integer AD_CMPNY) {

		Debug.print("CmBankReconciliationControllerBean createDistributionRecord");

		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		short FC_PRCSN = 0;

		// Initialize EJB Homes

		try {

			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and precision

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			FC_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}


		// create distribution record for adjustment

		try {

			LocalCmDistributionRecord cmDistributionRecord = cmDistributionRecordHome.create(
					DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, FC_PRCSN), DR_DBT, DR_RVRSL, EJBCommon.FALSE, AD_CMPNY);

			cmAdjustment.addCmDistributionRecord(cmDistributionRecord);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(GL_COA);
			glChartOfAccount.addCmDistributionRecord(cmDistributionRecord);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}

	}

	private void executeCmAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException {

		Debug.print("CmBankReconciliationControllerBean executeCmAdjPost");

		LocalCmAdjustmentHome cmAdjustmentHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGlJournalHome glJournalHome = null;
		LocalGlJournalBatchHome glJournalBatchHome = null;
		LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalGlJournalSourceHome glJournalSourceHome = null;
		LocalGlJournalCategoryHome glJournalCategoryHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;

		LocalCmAdjustment cmAdjustment = null;

		// Initialize EJB Home

		try {

			cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
			glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
			glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
			glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if adjustment is already deleted

			try {
				System.out.println(ADJ_CODE + " code");
				cmAdjustment = cmAdjustmentHome.findByPrimaryKey(ADJ_CODE);

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if adjustment is already posted or void

			if (cmAdjustment.getAdjPosted() == EJBCommon.TRUE) {

				throw new GlobalTransactionAlreadyPostedException();

			} else if (cmAdjustment.getAdjVoid() == EJBCommon.TRUE) {

				throw new GlobalTransactionAlreadyVoidException();
			}

			// post adjustment

			if (cmAdjustment.getAdjVoid() == EJBCommon.FALSE && cmAdjustment.getAdjPosted() == EJBCommon.FALSE) {

				if(cmAdjustment.getAdjType().equals("INTEREST") || cmAdjustment.getAdjType().equals("DEBIT MEMO") || cmAdjustment.getAdjType().equals("ADVANCE")) {

					// increase bank account balances

    				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());

    				try {

    					// find bankaccount balance before or equal receipt date

    					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					if (!adBankAccountBalances.isEmpty()) {

    						// get last check

    						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

    						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {

    							// create new balance

    							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);

    							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

    						} else { // equals to check date

    							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());

    						}

    					} else {

    						// create new balance

    						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    								cmAdjustment.getAdjDate(), (cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);

    						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

    					}

    					// propagate to subsequent balances if necessary

    					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					Iterator i = adBankAccountBalances.iterator();

    					while (i.hasNext()) {

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

    						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmAdjustment.getAdjAmount());

    					}

    				} catch (Exception ex) {

    					ex.printStackTrace();

    				}

				} else {

    				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getBaCode());

    				try {

    					// find bankaccount balance before or equal receipt date

    					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					if (!adBankAccountBalances.isEmpty()) {

    						// get last check

    						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

    						if (adBankAccountBalance.getBabDate().before(cmAdjustment.getAdjDate())) {

    							// create new balance

    							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    									cmAdjustment.getAdjDate(), adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount(), "BOOK", AD_CMPNY);

    							adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

    						} else { // equals to check date

    							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());

    						}

    					} else {

    						// create new balance

    						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
    								cmAdjustment.getAdjDate(), (0 - cmAdjustment.getAdjAmount()), "BOOK", AD_CMPNY);

    						adBankAccount.addAdBankAccountBalance(adNewBankAccountBalance);

    					}

    					// propagate to subsequent balances if necessary

    					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmAdjustment.getAdjDate(), cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

    					Iterator i = adBankAccountBalances.iterator();

    					while (i.hasNext()) {

    						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

    						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmAdjustment.getAdjAmount());

    					}

    				} catch (Exception ex) {

    					ex.printStackTrace();

    				}

				}

			}

			// set adjcher post status

			cmAdjustment.setAdjPosted(EJBCommon.TRUE);
			cmAdjustment.setAdjPostedBy(USR_NM);
			cmAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());


			// post to gl if necessary

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			if (!adPreference.getPrfCmGlPostingType().equals("USE JOURNAL INTERFACE")) {

				// validate if date has no period and period is closed

				LocalGlSetOfBook glJournalSetOfBook = null;

				try {

					glJournalSetOfBook = glSetOfBookHome.findByDate(cmAdjustment.getAdjDate(), AD_CMPNY);

				} catch (FinderException ex) {

					throw new GlJREffectiveDateNoPeriodExistException();

				}

				LocalGlAccountingCalendarValue glAccountingCalendarValue =
					glAccountingCalendarValueHome.findByAcCodeAndDate(
							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), cmAdjustment.getAdjDate(), AD_CMPNY);


				if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
						glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

					throw new GlJREffectiveDatePeriodClosedException();

				}

				// check if invoice is balance if not check suspense posting

				LocalGlJournalLine glOffsetJournalLine = null;

				Collection cmDistributionRecords = cmDistributionRecordHome.findByDrReversalAndDrImportedAndAdjCode(EJBCommon.FALSE, EJBCommon.FALSE, cmAdjustment.getAdjCode(), AD_CMPNY);

				Iterator j = cmDistributionRecords.iterator();

				double TOTAL_DEBIT = 0d;
				double TOTAL_CREDIT = 0d;

				while (j.hasNext()) {

					LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

					double DR_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
							cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(),
							cmAdjustment.getAdjConversionDate(),
							cmAdjustment.getAdjConversionRate(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);

					if (cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

						TOTAL_DEBIT += DR_AMNT;

					} else {

						TOTAL_CREDIT += DR_AMNT;

					}

				}

				TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
				TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

				if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
						TOTAL_DEBIT != TOTAL_CREDIT) {

					LocalGlSuspenseAccount glSuspenseAccount = null;

					try {

						glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("CASH MANAGEMENT", "BANK ADJUSTMENTS", AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalJournalNotBalanceException();

					}

					if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

						glOffsetJournalLine = glJournalLineHome.create(
								(short)(cmDistributionRecords.size() + 1),
								EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

					} else {

						glOffsetJournalLine = glJournalLineHome.create(
								(short)(cmDistributionRecords.size() + 1),
								EJBCommon.FALSE,
								TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

					}

					LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
					glChartOfAccount.addGlJournalLine(glOffsetJournalLine);


				} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
						TOTAL_DEBIT != TOTAL_CREDIT) {

					throw new GlobalJournalNotBalanceException();

				}

				// create journal batch if necessary

				LocalGlJournalBatch glJournalBatch = null;
				java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

				try {

					glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " BANK ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {
				}

				if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE &&
						glJournalBatch == null) {

					glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " BANK ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

				}

				// create journal entry

				LocalGlJournal glJournal = glJournalHome.create(cmAdjustment.getAdjReferenceNumber(),
						cmAdjustment.getAdjMemo(), cmAdjustment.getAdjDate(),
						0.0d, null, cmAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);

				LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("CASH MANAGEMENT", AD_CMPNY);
				glJournal.setGlJournalSource(glJournalSource);

				LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
				glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

				LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("BANK ADJUSTMENTS", AD_CMPNY);
				glJournal.setGlJournalCategory(glJournalCategory);

				if (glJournalBatch != null) {

					glJournal.setGlJournalBatch(glJournalBatch);

				}


				// create journal lines

				j = cmDistributionRecords.iterator();

				while (j.hasNext()) {

					LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

					double DR_AMNT = this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
							cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(),
							cmAdjustment.getAdjConversionDate(),
							cmAdjustment.getAdjConversionRate(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);

					LocalGlJournalLine glJournalLine = glJournalLineHome.create(
							cmDistributionRecord.getDrLine(),
							cmDistributionRecord.getDrDebit(),
							DR_AMNT, "", AD_CMPNY);

					cmDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);

					glJournal.addGlJournalLine(glJournalLine);

					cmDistributionRecord.setDrImported(EJBCommon.TRUE);


				}

				if (glOffsetJournalLine != null) {

					glJournal.addGlJournalLine(glOffsetJournalLine);

				}

				// post journal to gl

				Collection glJournalLines = glJournal.getGlJournalLines();

				Iterator i = glJournalLines.iterator();

				while (i.hasNext()) {

					LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

					// post current to current acv

					this.postToGl(glAccountingCalendarValue,
							glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);


					// post to subsequent acvs (propagate)

					Collection glSubsequentAccountingCalendarValues =
						glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
								glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

					Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

					while (acvsIter.hasNext()) {

						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
							(LocalGlAccountingCalendarValue)acvsIter.next();

						this.postToGl(glSubsequentAccountingCalendarValue,
								glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);

					}

					// post to subsequent years if necessary

					Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

					if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

						adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
						LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

						Iterator sobIter = glSubsequentSetOfBooks.iterator();

						while (sobIter.hasNext()) {

							LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

							String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

							// post to subsequent acvs of subsequent set of book(propagate)

							Collection glAccountingCalendarValues =
								glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

							Iterator acvIter = glAccountingCalendarValues.iterator();

							while (acvIter.hasNext()) {

								LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
									(LocalGlAccountingCalendarValue)acvIter.next();

								if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
										ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

									this.postToGl(glSubsequentAccountingCalendarValue,
											glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);

								} else { // revenue & expense

									this.postToGl(glSubsequentAccountingCalendarValue,
											glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);

								}

							}

							if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

						}

					}

				}

			}

		} catch (GlJREffectiveDateNoPeriodExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlJREffectiveDatePeriodClosedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("CmBankReconciliationControllerBean convertForeignToFunctionalCurrency");


		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}


		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

			AMOUNT = AMOUNT / CONVERSION_RATE;

		}
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

	private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
			LocalGlChartOfAccount glChartOfAccount,
			boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("CmBankReconciliationControllerBean postToGl");

		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccountBalance glChartOfAccountBalance =
				glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
						glAccountingCalendarValue.getAcvCode(),
						glChartOfAccount.getCoaCode(), AD_CMPNY);

			String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
			short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



			if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
					isDebit == EJBCommon.TRUE) ||
					(!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
							isDebit == EJBCommon.FALSE)) {

				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				if (!isCurrentAcv) {

					glChartOfAccountBalance.setCoabBeginningBalance(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

				}


			} else {

				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				if (!isCurrentAcv) {

					glChartOfAccountBalance.setCoabBeginningBalance(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				}

			}

			if (isCurrentAcv) {

				if (isDebit == EJBCommon.TRUE) {

					glChartOfAccountBalance.setCoabTotalDebit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

				} else {

					glChartOfAccountBalance.setCoabTotalCredit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
				}

			}

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}


	}

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("CmBankReconciliationControllerBean ejbCreate");

	}
}
