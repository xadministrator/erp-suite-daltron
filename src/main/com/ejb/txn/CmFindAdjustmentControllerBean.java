package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ar.LocalArAppliedCredit;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.CmModAdjustmentDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmFindAdjustmentControllerEJB"
 *           display-name="Used for releasing/unreleasing checks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmFindAdjustmentControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmFindAdjustmentController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmFindAdjustmentControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class CmFindAdjustmentControllerBean extends AbstractSessionBean {
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCustomerBatchAll(Integer AD_CMPNY) {
		
		Debug.print("CmFindAdjustmentControllerBean getAdLvCustomerBatchAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("AR CUSTOMER BATCH - SOA", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("CmFindAdjustmentControllerBean getArCstAll");
        
        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = arCustomers.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	
	        	list.add(arCustomer.getCstCustomerCode());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	
   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("CmFindAdjustmentControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        
        Collection adBankAccounts = null;
        
        LocalAdBankAccount adBankAccount = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBankAccounts.isEmpty()) {
        	
        	return null;
        	
        }
        
        Iterator i = adBankAccounts.iterator();
               
        while (i.hasNext()) {
        	
        	adBankAccount = (LocalAdBankAccount)i.next();
        	
        	list.add(adBankAccount.getBaName());
        	
        }
        
        return list;
            
    }
    

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getCmAdjByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("CmFindAdjustmentControllerBean getCmAdjByCriteria");
      
      LocalCmAdjustmentHome cmAdjustmentHome = null;  
        
	  ArrayList list = new ArrayList();
	  
      // Initialize EJB Home
	        
	  try {
	        
	      cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
	          lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }  
	  
	  try {
      
                       
	      StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(adj) FROM CmAdjustment adj ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
	      int criteriaSize = criteria.size() + 2;
	      Object obj[];
	      
	      if (criteria.containsKey("approvalStatus")) {
	      	
	      	 String approvalStatus = (String)criteria.get("approvalStatus");
	      	
	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
	      	 	
	      	 	 criteriaSize--;
	      	 	
	      	 }
	      	
	      }
	      
	      obj = new Object[criteriaSize];
	      
	      
	      if (criteria.containsKey("customerBatch")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("adj.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerBatch");
		   	  ctr++;
	   	  
	      }	




	      if (criteria.containsKey("customerCode")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("adj.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerCode");
		   	  ctr++;
	   	  
	      }	
	      	      
	      if (criteria.containsKey("bankAccount")) {
	      	
	      	 firstArgument = false;
	      	 
	      	 jbossQl.append("WHERE adj.adBankAccount.baName=?" + (ctr+1) + " ");
	      	 obj[ctr] = (String)criteria.get("bankAccount");
	      	 ctr++;
	      	 
	      } 
	      
	      if (criteria.containsKey("type")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }
	      	 jbossQl.append("adj.adjType=?" + (ctr+1) + " ");
	      	 obj[ctr] = (String)criteria.get("type");
	      	 ctr++;
	      	 
	      }
	      
	      if (criteria.containsKey("documentNumberFrom")) {
	      	
	      	if (!firstArgument) {
	      		jbossQl.append("AND ");
	      	} else {
	      		firstArgument = false;
	      		jbossQl.append("WHERE ");
	      	}
	      	jbossQl.append("adj.adjDocumentNumber>=?" + (ctr+1) + " ");
	      	obj[ctr] = (String)criteria.get("documentNumberFrom");
	      	ctr++;
	      }  
	      
	      if (criteria.containsKey("documentNumberTo")) {
	      	
	      	if (!firstArgument) {
	      		jbossQl.append("AND ");
	      	} else {
	      		firstArgument = false;
	      		jbossQl.append("WHERE ");
	      	}
	      	jbossQl.append("adj.adjDocumentNumber<=?" + (ctr+1) + " ");
	      	obj[ctr] = (String)criteria.get("documentNumberTo");
	      	ctr++;
	      	
	      }
	      
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }
	      	 jbossQl.append("adj.adjReferenceNumber=?" + (ctr+1) + " ");
	      	 obj[ctr] = (String)criteria.get("referenceNumber");
	      	 ctr++;
	      	 
	      }	      	       
	      
	      if (criteria.containsKey("dateFrom")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }
	      	 jbossQl.append("adj.adjDate>=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Date)criteria.get("dateFrom");
	      	 ctr++;
	      }  
	      
	      if (criteria.containsKey("dateTo")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }
	      	 jbossQl.append("adj.adjDate<=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Date)criteria.get("dateTo");
	      	 ctr++;
	      
		  }
	      
	      if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("adj.adjApprovalStatus IS NULL ");
	       	  	
	       	  } else if (approvalStatus.equals("REJECTED")) {
	       	  	
		       	  jbossQl.append("adj.adjReasonForRejection IS NOT NULL ");
	       	  	
	      	  } else {
	      	  	
		      	  jbossQl.append("adj.adjApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
	       	  
	      }
	      
	      if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	  } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	  }
	  	 
	  	  jbossQl.append("adj.adjVoid=?" + (ctr+1) + " ");
	  	  obj[ctr] = (Byte)criteria.get("adjustmentVoid");
	      ctr++;	      
	      
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjPosted=?" + (ctr+1) + " ");
	       	  
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }  
	      
	      if (!firstArgument) {
	          
	          jbossQl.append("AND ");
	          
	      } else {
	          
	          firstArgument = false;
	          jbossQl.append("WHERE ");
	          
	      }
	      
	      jbossQl.append("adj.adjAdBranch=" + AD_BRNCH + " ");
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("adj.adjAdCompany=" + AD_CMPNY + " ");

	      String orderBy = null;
	      
	      if (ORDER_BY != null && ORDER_BY.equals("REFERENCE NUMBER")) {	          
	          	      		
	      	  orderBy = "adj.adjReferenceNumber, adj.adjDate";
	      	  
	      } else if (ORDER_BY != null && ORDER_BY.equals("DOCUMENT NUMBER")) {	          
  	      		
	      	  orderBy = "adj.adjDocumentNumber, adj.adjDate";
	      	
	      } else {
	      	
	      	  orderBy = "adj.adjDate";
	      	
	      }
	      
	      jbossQl.append("ORDER BY " + orderBy);
	      		      
	      jbossQl.append(" OFFSET ?" + (ctr + 1));	        
	      obj[ctr] = OFFSET;	      
	      ctr++;
	      
	      jbossQl.append(" LIMIT ?" + (ctr + 1));
	      obj[ctr] = LIMIT;
	      ctr++;
	      	      	                     
	      Collection cmAdjustments = null;
	      System.out.print("sql is :" + jbossQl.toString());	
          cmAdjustments = cmAdjustmentHome.getAdjByCriteria(jbossQl.toString(), obj);	         
	      
	      if (cmAdjustments.size() == 0)
	         throw new GlobalNoRecordFoundException();
	         
	      Iterator i = cmAdjustments.iterator();
	      
	      while (i.hasNext()) {
	      	
	      	  LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();   	  
	      	  
	      	  CmModAdjustmentDetails mdetails = new CmModAdjustmentDetails();
	      	  mdetails.setAdjCode(cmAdjustment.getAdjCode());
	      	  mdetails.setAdjType(cmAdjustment.getAdjType());
	      	  mdetails.setAdjDate(cmAdjustment.getAdjDate());
	      	  mdetails.setAdjDocumentNumber(cmAdjustment.getAdjDocumentNumber());
	      	  mdetails.setAdjReferenceNumber(cmAdjustment.getAdjReferenceNumber());
	      	  String customerCode = "";
	      	  String customerName = "";
	      	  try{
	      		  customerCode = cmAdjustment.getArCustomer().getCstCustomerCode();
	      		  customerName = cmAdjustment.getArCustomer().getCstName();
	      	  }catch(Exception ex){
	      		  
	      	  }
	      	  mdetails.setAdjCustomerCode(customerCode);
	      	  mdetails.setAdjCustomerName(customerName);
	      	  mdetails.setAdjAmount(cmAdjustment.getAdjAmount());
	      	  
	      	// get applied credit
	      	  double totalAppliedCredit = 0d;
	           try{
	        	   
	        	   Collection arAppliedCredits = cmAdjustment.getArAppliedCredits();
		            System.out.println("arAppliedCredits="+arAppliedCredits.size());
		            Iterator x = arAppliedCredits.iterator();

		            while(x.hasNext()){
		            	
		            	LocalArAppliedCredit arAppliedCredit = (LocalArAppliedCredit)x.next();
		            	
		            	totalAppliedCredit += arAppliedCredit.getAcApplyCredit();
		            	
		            }
	           } catch (Exception ex){
	        	   
	           }
	            
	           System.out.println("CM_CODE="+cmAdjustment.getAdjCode());
	           System.out.println("cmAdjustment.getAdjDocumentNumber()="+cmAdjustment.getAdjDocumentNumber()); 
	          System.out.println("cmAdjustment.getAdjRefundAmount()="+cmAdjustment.getAdjRefundAmount());
	          System.out.println("totalAppliedCredit="+totalAppliedCredit);
	      	  mdetails.setAdjAmountApplied(totalAppliedCredit);
	      	  mdetails.setAdjRefundAmount(cmAdjustment.getAdjRefundAmount());
	      	  mdetails.setAdjVoid(cmAdjustment.getAdjVoid());
	      	  mdetails.setAdjBaName(cmAdjustment.getAdBankAccount().getBaName()); 
	      	  mdetails.setAdjPosted(cmAdjustment.getAdjPosted());
  	  	      	  	      	  		     
			  list.add(mdetails);
		         
	      }
	         
	      return list;
	      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	  
	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
  
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getCmAdjSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException {

       Debug.print("CmFindAdjustmentControllerBean getCmAdjSizeByCriteria");
       
       LocalCmAdjustmentHome cmAdjustmentHome = null;  
         
 	   // Initialize EJB Home
 	        
 	  try {
 	        
 	      cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
 	          lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
 	        
 	  } catch (NamingException ex) {
 	        
 	      throw new EJBException(ex.getMessage());
 	        
 	  }  
 	  
 	  try {
       
                        
 	      StringBuffer jbossQl = new StringBuffer();
 	      jbossQl.append("SELECT OBJECT(adj) FROM CmAdjustment adj ");
 	      
 	      boolean firstArgument = true;
 	      short ctr = 0;
 	      int criteriaSize = criteria.size();
 	      Object obj[];
 	      
 	      if (criteria.containsKey("approvalStatus")) {
 	      	
 	      	 String approvalStatus = (String)criteria.get("approvalStatus");
 	      	
 	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
 	      	 	
 	      	 	 criteriaSize--;
 	      	 	
 	      	 }
 	      	
 	      }
 	      
 	      obj = new Object[criteriaSize];
 	      
 	     if (criteria.containsKey("customerBatch")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("adj.arCustomer.cstCustomerBatch=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerBatch");
		   	  ctr++;
	   	  
	      }	




	      if (criteria.containsKey("customerCode")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("adj.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerCode");
		   	  ctr++;
	   	  
	      }
	      
 	      	      
 	      if (criteria.containsKey("bankAccount")) {
 	      	
 	      	 firstArgument = false;
 	      	 
 	      	 jbossQl.append("WHERE adj.adBankAccount.baName=?" + (ctr+1) + " ");
 	      	 obj[ctr] = (String)criteria.get("bankAccount");
 	      	 ctr++;
 	      	 
 	      } 
 	      
 	      if (criteria.containsKey("type")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 	jbossQl.append("AND ");
 	      	 } else {
 	      	 	firstArgument = false;
 	      	 	jbossQl.append("WHERE ");
 	      	 }
 	      	 jbossQl.append("adj.adjType=?" + (ctr+1) + " ");
 	      	 obj[ctr] = (String)criteria.get("type");
 	      	 ctr++;
 	      	 
 	      }
 	      
 	      if (criteria.containsKey("documentNumberFrom")) {
 	      	
 	      	if (!firstArgument) {
 	      		jbossQl.append("AND ");
 	      	} else {
 	      		firstArgument = false;
 	      		jbossQl.append("WHERE ");
 	      	}
 	      	jbossQl.append("adj.adjDocumentNumber>=?" + (ctr+1) + " ");
 	      	obj[ctr] = (String)criteria.get("documentNumberFrom");
 	      	ctr++;
 	      }  
 	      
 	      if (criteria.containsKey("documentNumberTo")) {
 	      	
 	      	if (!firstArgument) {
 	      		jbossQl.append("AND ");
 	      	} else {
 	      		firstArgument = false;
 	      		jbossQl.append("WHERE ");
 	      	}
 	      	jbossQl.append("adj.adjDocumentNumber<=?" + (ctr+1) + " ");
 	      	obj[ctr] = (String)criteria.get("documentNumberTo");
 	      	ctr++;
 	      	
 	      }
 	      
 	      if (criteria.containsKey("referenceNumber")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 	jbossQl.append("AND ");
 	      	 } else {
 	      	 	firstArgument = false;
 	      	 	jbossQl.append("WHERE ");
 	      	 }
 	      	 jbossQl.append("adj.adjReferenceNumber=?" + (ctr+1) + " ");
 	      	 obj[ctr] = (String)criteria.get("referenceNumber");
 	      	 ctr++;
 	      	 
 	      }	      	       
 	      
 	      if (criteria.containsKey("dateFrom")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 	jbossQl.append("AND ");
 	      	 } else {
 	      	 	firstArgument = false;
 	      	 	jbossQl.append("WHERE ");
 	      	 }
 	      	 jbossQl.append("adj.adjDate>=?" + (ctr+1) + " ");
 	      	 obj[ctr] = (Date)criteria.get("dateFrom");
 	      	 ctr++;
 	      }  
 	      
 	      if (criteria.containsKey("dateTo")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 	jbossQl.append("AND ");
 	      	 } else {
 	      	 	firstArgument = false;
 	      	 	jbossQl.append("WHERE ");
 	      	 }
 	      	 jbossQl.append("adj.adjDate<=?" + (ctr+1) + " ");
 	      	 obj[ctr] = (Date)criteria.get("dateTo");
 	      	 ctr++;
 	      
 		  }
 	      
 	      if (criteria.containsKey("approvalStatus")) {
 	       	
 	       	  if (!firstArgument) {
 	       	  	
 	       	     jbossQl.append("AND ");
 	       	     
 	       	  } else {
 	       	  	
 	       	  	 firstArgument = false;
 	       	  	 jbossQl.append("WHERE ");
 	       	  	 
 	       	  }
 	       	  
 	       	  String approvalStatus = (String)criteria.get("approvalStatus");
        	  
 	       	  if (approvalStatus.equals("DRAFT")) {
 	       	      
 		       	  jbossQl.append("adj.adjApprovalStatus IS NULL ");
 	       	  	
 	       	  } else if (approvalStatus.equals("REJECTED")) {
 	       	  	
 		       	  jbossQl.append("adj.adjReasonForRejection IS NOT NULL ");
 	       	  	
 	      	  } else {
 	      	  	
 		      	  jbossQl.append("adj.adjApprovalStatus=?" + (ctr+1) + " ");
 		       	  obj[ctr] = approvalStatus;
 		       	  ctr++;
 	      	  	
 	      	  }
 	       	  
 	      }
 	      
 	      if (!firstArgument) {
 	  	 	jbossQl.append("AND ");
 	  	  } else {
 	  	 	firstArgument = false;
 	  	 	jbossQl.append("WHERE ");
 	  	  }
 	  	 
 	  	  jbossQl.append("adj.adjVoid=?" + (ctr+1) + " ");
 	  	  obj[ctr] = (Byte)criteria.get("adjustmentVoid");
 	      ctr++;	      
 	      
 	      if (criteria.containsKey("posted")) {
 	       	
 	       	  if (!firstArgument) {
 	       	  	
 	       	     jbossQl.append("AND ");
 	       	     
 	       	  } else {
 	       	  	
 	       	  	 firstArgument = false;
 	       	  	 jbossQl.append("WHERE ");
 	       	  	 
 	       	  }
 	       	  
 	       	  jbossQl.append("adj.adjPosted=?" + (ctr+1) + " ");
 	       	  
 	       	  String posted = (String)criteria.get("posted");
 	       	  
 	       	  if (posted.equals("YES")) {
 	       	  	
 	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
 	       	  	
 	       	  } else {
 	       	  	
 	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
 	       	  	
 	       	  }       	  
 	       	 
 	       	  ctr++;
 	       	  
 	      }
 	      
 	      if (!firstArgument) {
 	          
 	          jbossQl.append("AND ");
 	          
 	      } else {
 	          
 	          firstArgument = false;
 	          jbossQl.append("WHERE ");
 	          
 	      }
 	      
 	      jbossQl.append("adj.adjAdBranch=" + AD_BRNCH + " ");
 	      
 	      if (!firstArgument) {
 	          
 	          jbossQl.append("AND ");
 	          
 	      } else {
 	          
 	          firstArgument = false;
 	          jbossQl.append("WHERE ");
 	          
 	      }
 	      
 	      jbossQl.append("adj.adjAdCompany=" + AD_CMPNY + " ");

 	      
 	      Collection cmAdjustments = null;
 	      	      	
           cmAdjustments = cmAdjustmentHome.getAdjByCriteria(jbossQl.toString(), obj);	         
 	      
 	      if (cmAdjustments.size() == 0)
 	         throw new GlobalNoRecordFoundException();
 	         
 	      return new Integer(cmAdjustments.size());
 	      
 	  } catch (GlobalNoRecordFoundException ex) {
 	  	 
 	  	  throw ex;
 	  	
 	  } catch (Exception ex) {
 	  	  
 	  	  ex.printStackTrace();
 	  	  throw new EJBException(ex.getMessage());
 	  	
 	  }
   
    }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("CmFindAdjustmentControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
 
   // private methods
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("CmFindAdjustmentControllerBean ejbCreate");
      
   }
   
}
