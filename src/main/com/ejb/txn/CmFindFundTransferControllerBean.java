package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.CmModFundTransferEntryDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmFindFundTransferControllerEJB"
 *           display-name="Used for releasing/unreleasing checks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmFindFundTransferControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmFindFundTransferController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmFindFundTransferControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class CmFindFundTransferControllerBean extends AbstractSessionBean {
	
   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("CmFindFundTransferControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        
        Collection adBankAccounts = null;
        
        LocalAdBankAccount adBankAccount = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBankAccounts.isEmpty()) {
        	
        	return null;
        	
        }
        
        Iterator i = adBankAccounts.iterator();
               
        while (i.hasNext()) {
        	
        	adBankAccount = (LocalAdBankAccount)i.next();
        	
        	list.add(adBankAccount.getBaName());
        	
        }
        
        return list;
            
    }	


   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getCmFtByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("CmFindFundTransferControllerBean getCmFtByCriteria");
      
      LocalCmFundTransferHome cmFundTransferHome = null;
      LocalAdBankAccountHome adBankAccountHome = null;      
        
	  ArrayList list = new ArrayList();
	  
      // Initialize EJB Home
	        
	  try {
	        
	      cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
	          lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
	      adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
	          lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }  
	  
	  try {
      
                       
	      StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(ft) FROM CmFundTransfer ft ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
	      int criteriaSize = criteria.size() + 2;
	      Object obj[];
	      
	      if (criteria.containsKey("approvalStatus")) {
	      	
	      	 String approvalStatus = (String)criteria.get("approvalStatus");
	      	
	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
	      	 	
	      	 	 criteriaSize--;
	      	 	
	      	 }
	      	
	      }
	      
	      obj = new Object[criteriaSize];
	      	      
	      if (criteria.containsKey("bankAccountFrom")) {
	      	
	      	 firstArgument = false;
	      	 
	      	 LocalAdBankAccount adBankAccountFrom = 
	      	     adBankAccountHome.findByBaName((String)criteria.get("bankAccountFrom"), AD_CMPNY);	      	 
 	 
	      	 jbossQl.append("WHERE ft.ftAdBaAccountFrom=?" + (ctr+1) + " ");
	      	 obj[ctr] = adBankAccountFrom.getBaCode();
	      	 ctr++;
	      	 
	      }
	      
	      if (criteria.containsKey("bankAccountTo")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }

	      	 LocalAdBankAccount adBankAccountTo = 
	      	     adBankAccountHome.findByBaName((String)criteria.get("bankAccountTo"), AD_CMPNY);	      	 
	      	 
	      	 jbossQl.append("ft.ftAdBaAccountTo=?" + (ctr+1) + " ");
	      	 obj[ctr] = adBankAccountTo.getBaCode();
	      	 ctr++;
	      }
	      
	      if (criteria.containsKey("documentNumberFrom")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }
	      	 jbossQl.append("ft.ftDocumentNumber>=?" + (ctr+1) + " ");
	      	 obj[ctr] = (String)criteria.get("documentNumberFrom");
	      	 ctr++;
	      	 
	      }
	      
	      if (criteria.containsKey("documentNumberTo")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }
	      	 jbossQl.append("ft.ftDocumentNumber<=?" + (ctr+1) + " ");
	      	 obj[ctr] = (String)criteria.get("documentNumberTo");
	      	 ctr++;
	      	 
	      }
	      
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }
	      	 jbossQl.append("ft.ftReferenceNumber=?" + (ctr+1) + " ");
	      	 obj[ctr] = (String)criteria.get("referenceNumber");
	      	 ctr++;
	      	 
	      }	      	       
	      
	      if (criteria.containsKey("dateFrom")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }
	      	 jbossQl.append("ft.ftDate>=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Date)criteria.get("dateFrom");
	      	 ctr++;
	      }  
	      
	      if (criteria.containsKey("dateTo")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }
	      	 jbossQl.append("ft.ftDate<=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Date)criteria.get("dateTo");
	      	 ctr++;
	      
		  }
		  
		  if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("ft.ftApprovalStatus IS NULL ");
	       	  	
	       	  } else if (approvalStatus.equals("REJECTED")) {
	       	  	
		       	  jbossQl.append("ft.ftReasonForRejection IS NOT NULL ");
	       	  	
	      	  } else {
	      	  	
		      	  jbossQl.append("ft.ftApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
	       	  
	      }
	      
	      if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	  } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	  }
	  	 
	  	  jbossQl.append("ft.ftVoid=?" + (ctr+1) + " ");
	  	  obj[ctr] = (Byte)criteria.get("fundTransferVoid");
	      ctr++;	      
	      
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("ft.ftPosted=?" + (ctr+1) + " ");
	       	  
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }  
	      
	      if (!firstArgument) {
	          
	          jbossQl.append("AND ");
	          
	      } else {
	          
	          firstArgument = false;
	          jbossQl.append("WHERE ");
	          
	      }
	      
	      jbossQl.append("ft.ftAdBranch=" + AD_BRNCH + " ");
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("ft.ftAdCompany=" + AD_CMPNY + " ");

	      String orderBy = null;
	      
	      if (ORDER_BY != null && ORDER_BY.equals("REFERENCE NUMBER")) {	          
	          	      		
	      	  orderBy = "ft.ftReferenceNumber, ft.ftDate";
	      	
	      } else if (ORDER_BY != null && ORDER_BY.equals("DOCUMENT NUMBER")) {	          
  	      		
	      	  orderBy = "ft.ftDocumentNumber, ft.ftDate";
	      	  
	      } else {
	      	
	      	  orderBy = "ft.ftDate";
	      	
	      }
	      
	      jbossQl.append("ORDER BY " + orderBy);
	      		      
	      jbossQl.append(" OFFSET ?" + (ctr + 1));	        
	      obj[ctr] = OFFSET;	      
	      ctr++;
	      
	      jbossQl.append(" LIMIT ?" + (ctr + 1));
	      obj[ctr] = LIMIT;
	      ctr++;
	      	      	                     
	      Collection cmFundTransfers = null;
	      	      	
          cmFundTransfers = cmFundTransferHome.getFtByCriteria(jbossQl.toString(), obj);	         
	      
	      if (cmFundTransfers.size() == 0)
	         throw new GlobalNoRecordFoundException();
	         
	      Iterator i = cmFundTransfers.iterator();
	      
	      while (i.hasNext()) {
	      	
	      	  LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();   	  
			
  			  LocalAdBankAccount adBankAccountFrom = 
				adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());
		
			  LocalAdBankAccount adBankAccountTo = 
				adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountTo());
	      	  
	      	  CmModFundTransferEntryDetails mdetails = new CmModFundTransferEntryDetails();
	      	  mdetails.setFtCode(cmFundTransfer.getFtCode());
	      	  mdetails.setFtAmount(cmFundTransfer.getFtAmount());
	      	  mdetails.setFtDate(cmFundTransfer.getFtDate());
	      	  mdetails.setFtDocumentNumber(cmFundTransfer.getFtDocumentNumber());
	      	  mdetails.setFtReferenceNumber(cmFundTransfer.getFtReferenceNumber());
	      	  mdetails.setFtVoid(cmFundTransfer.getFtVoid());
	      	  mdetails.setFtAdBankAccountNameFrom(adBankAccountFrom.getBaName());
	      	  mdetails.setFtAdBankAccountNameTo(adBankAccountTo.getBaName()); 	  
			  mdetails.setFtCurrencyFrom(adBankAccountFrom.getGlFunctionalCurrency().getFcName());			
			  mdetails.setFtCurrencyTo(adBankAccountTo.getGlFunctionalCurrency().getFcName());	      	  
  	  	      	  	      	  		     
			  list.add(mdetails);
		         
	      }
	         
	      return list;
	      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	  
	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
  
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getCmFtSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException {

       Debug.print("CmFindFundTransferControllerBean getCmFtSizeByCriteria");
       
       LocalCmFundTransferHome cmFundTransferHome = null;
       LocalAdBankAccountHome adBankAccountHome = null;      
         
 	   // Initialize EJB Home
 	        
 	  try {
 	        
 	      cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
 	          lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
 	      adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
 	          lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
 	        
 	  } catch (NamingException ex) {
 	        
 	      throw new EJBException(ex.getMessage());
 	        
 	  }  
 	  
 	  try {
       
                        
 	      StringBuffer jbossQl = new StringBuffer();
 	      jbossQl.append("SELECT OBJECT(ft) FROM CmFundTransfer ft ");
 	      
 	      boolean firstArgument = true;
 	      short ctr = 0;
 	      int criteriaSize = criteria.size();
 	      Object obj[];
 	      
 	      if (criteria.containsKey("approvalStatus")) {
 	      	
 	      	 String approvalStatus = (String)criteria.get("approvalStatus");
 	      	
 	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
 	      	 	
 	      	 	 criteriaSize--;
 	      	 	
 	      	 }
 	      	
 	      }
 	      
 	      obj = new Object[criteriaSize];
 	      	      
 	      if (criteria.containsKey("bankAccountFrom")) {
 	      	
 	      	 firstArgument = false;
 	      	 
 	      	 LocalAdBankAccount adBankAccountFrom = 
 	      	     adBankAccountHome.findByBaName((String)criteria.get("bankAccountFrom"), AD_CMPNY);	      	 
  	 
 	      	 jbossQl.append("WHERE ft.ftAdBaAccountFrom=?" + (ctr+1) + " ");
 	      	 obj[ctr] = adBankAccountFrom.getBaCode();
 	      	 ctr++;
 	      	 
 	      }
 	      
 	      if (criteria.containsKey("bankAccountTo")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 	jbossQl.append("AND ");
 	      	 } else {
 	      	 	firstArgument = false;
 	      	 	jbossQl.append("WHERE ");
 	      	 }

 	      	 LocalAdBankAccount adBankAccountTo = 
 	      	     adBankAccountHome.findByBaName((String)criteria.get("bankAccountTo"), AD_CMPNY);	      	 
 	      	 
 	      	 jbossQl.append("ft.ftAdBaAccountTo=?" + (ctr+1) + " ");
 	      	 obj[ctr] = adBankAccountTo.getBaCode();
 	      	 ctr++;
 	      }
 	      
 	      if (criteria.containsKey("documentNumberFrom")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 	jbossQl.append("AND ");
 	      	 } else {
 	      	 	firstArgument = false;
 	      	 	jbossQl.append("WHERE ");
 	      	 }
 	      	 jbossQl.append("ft.ftDocumentNumber>=?" + (ctr+1) + " ");
 	      	 obj[ctr] = (String)criteria.get("documentNumberFrom");
 	      	 ctr++;
 	      	 
 	      }
 	      
 	      if (criteria.containsKey("documentNumberTo")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 	jbossQl.append("AND ");
 	      	 } else {
 	      	 	firstArgument = false;
 	      	 	jbossQl.append("WHERE ");
 	      	 }
 	      	 jbossQl.append("ft.ftDocumentNumber<=?" + (ctr+1) + " ");
 	      	 obj[ctr] = (String)criteria.get("documentNumberTo");
 	      	 ctr++;
 	      	 
 	      }
 	      
 	      if (criteria.containsKey("referenceNumber")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 	jbossQl.append("AND ");
 	      	 } else {
 	      	 	firstArgument = false;
 	      	 	jbossQl.append("WHERE ");
 	      	 }
 	      	 jbossQl.append("ft.ftReferenceNumber=?" + (ctr+1) + " ");
 	      	 obj[ctr] = (String)criteria.get("referenceNumber");
 	      	 ctr++;
 	      	 
 	      }	      	       
 	      
 	      if (criteria.containsKey("dateFrom")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 	jbossQl.append("AND ");
 	      	 } else {
 	      	 	firstArgument = false;
 	      	 	jbossQl.append("WHERE ");
 	      	 }
 	      	 jbossQl.append("ft.ftDate>=?" + (ctr+1) + " ");
 	      	 obj[ctr] = (Date)criteria.get("dateFrom");
 	      	 ctr++;
 	      }  
 	      
 	      if (criteria.containsKey("dateTo")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 	jbossQl.append("AND ");
 	      	 } else {
 	      	 	firstArgument = false;
 	      	 	jbossQl.append("WHERE ");
 	      	 }
 	      	 jbossQl.append("ft.ftDate<=?" + (ctr+1) + " ");
 	      	 obj[ctr] = (Date)criteria.get("dateTo");
 	      	 ctr++;
 	      
 		  }
 		  
 		  if (criteria.containsKey("approvalStatus")) {
 	       	
 	       	  if (!firstArgument) {
 	       	  	
 	       	     jbossQl.append("AND ");
 	       	     
 	       	  } else {
 	       	  	
 	       	  	 firstArgument = false;
 	       	  	 jbossQl.append("WHERE ");
 	       	  	 
 	       	  }
 	       	  
 	       	  String approvalStatus = (String)criteria.get("approvalStatus");
        	  
 	       	  if (approvalStatus.equals("DRAFT")) {
 	       	      
 		       	  jbossQl.append("ft.ftApprovalStatus IS NULL ");
 	       	  	
 	       	  } else if (approvalStatus.equals("REJECTED")) {
 	       	  	
 		       	  jbossQl.append("ft.ftReasonForRejection IS NOT NULL ");
 	       	  	
 	      	  } else {
 	      	  	
 		      	  jbossQl.append("ft.ftApprovalStatus=?" + (ctr+1) + " ");
 		       	  obj[ctr] = approvalStatus;
 		       	  ctr++;
 	      	  	
 	      	  }
 	       	  
 	      }
 	      
 	      if (!firstArgument) {
 	  	 	jbossQl.append("AND ");
 	  	  } else {
 	  	 	firstArgument = false;
 	  	 	jbossQl.append("WHERE ");
 	  	  }
 	  	 
 	  	  jbossQl.append("ft.ftVoid=?" + (ctr+1) + " ");
 	  	  obj[ctr] = (Byte)criteria.get("fundTransferVoid");
 	      ctr++;	      
 	      
 	      if (criteria.containsKey("posted")) {
 	       	
 	       	  if (!firstArgument) {
 	       	  	
 	       	     jbossQl.append("AND ");
 	       	     
 	       	  } else {
 	       	  	
 	       	  	 firstArgument = false;
 	       	  	 jbossQl.append("WHERE ");
 	       	  	 
 	       	  }
 	       	  
 	       	  jbossQl.append("ft.ftPosted=?" + (ctr+1) + " ");
 	       	  
 	       	  String posted = (String)criteria.get("posted");
 	       	  
 	       	  if (posted.equals("YES")) {
 	       	  	
 	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
 	       	  	
 	       	  } else {
 	       	  	
 	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
 	       	  	
 	       	  }       	  
 	       	 
 	       	  ctr++;
 	       	  
 	      }  
 	      
 	      if (!firstArgument) {
 	          
 	          jbossQl.append("AND ");
 	          
 	      } else {
 	          
 	          firstArgument = false;
 	          jbossQl.append("WHERE ");
 	          
 	      }
 	      
 	      jbossQl.append("ft.ftAdBranch=" + AD_BRNCH + " ");

 	      
 	      if (!firstArgument) {
 	      	
 	      	jbossQl.append("AND ");
 	      	
 	      } else {
 	      	
 	      	firstArgument = false;
 	      	jbossQl.append("WHERE ");
 	      	
 	      }
 	      
 	      jbossQl.append("ft.ftAdCompany=" + AD_CMPNY + " ");

 	      	      	                     
 	      Collection cmFundTransfers = null;
 	      	      	
           cmFundTransfers = cmFundTransferHome.getFtByCriteria(jbossQl.toString(), obj);	         
 	      
 	      if (cmFundTransfers.size() == 0)
 	         throw new GlobalNoRecordFoundException();
 	         
 	      return new Integer(cmFundTransfers.size());
 	      
 	  } catch (GlobalNoRecordFoundException ex) {
 	  	 
 	  	  throw ex;
 	  	
 	  } catch (Exception ex) {
 	  	  
 	  	  ex.printStackTrace();
 	  	  throw new EJBException(ex.getMessage());
 	  	
 	  }
   
    }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("CmFindFundTransferControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
 
   // private methods
      
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("CmFindFundTransferControllerBean ejbCreate");
      
   }
   
}
