
/*
 * CmFundTransferControllerBean.java
 *
 * Created on November 24, 2003, 8:35 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.cm.LocalCmFundTransferReceipt;
import com.ejb.cm.LocalCmFundTransferReceiptHome;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalOverapplicationNotAllowedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdModBankAccountDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.CmModFundTransferEntryDetails;
import com.util.CmModFundTransferReceiptDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmFundTransferEntryControllerEJB"
 *           display-name="Used for transfering cash amount from one account to another account"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmFundTransferEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmFundTransferEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmFundTransferEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 * 
*/

public class CmFundTransferEntryControllerBean extends AbstractSessionBean {


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("CmFundTransferEntryControllerBean getAdBaAll");

		LocalAdBankAccountHome adBankAccountHome = null;               

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}        

		try {

			Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

			Iterator i = adBankAccounts.iterator();

			while (i.hasNext()) {

				LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();

				list.add(adBankAccount.getBaName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public CmModFundTransferEntryDetails getCmFtByFtCode(Integer FT_CODE, Integer AD_CMPNY) {

		Debug.print("CmFundTransferEntryControllerBean getCmFtByFtCode");

		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;

		LocalCmFundTransfer cmFundTransfer = null;
		LocalAdBankAccount adBankAccountFrom = null;
		LocalAdBankAccount adBankAccountTo = null;

		// Initialize EJB Home

		try {

			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try { 

			cmFundTransfer = cmFundTransferHome.findByPrimaryKey(FT_CODE);

			try {

				adBankAccountFrom = 
					adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

			} catch (FinderException ex) {

			}

			try {

				adBankAccountTo = 
					adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountTo());

			} catch (FinderException ex) {

			}       

			CmModFundTransferEntryDetails ftDetails = new CmModFundTransferEntryDetails();
			ftDetails.setFtCode(cmFundTransfer.getFtCode());
			ftDetails.setFtDate(cmFundTransfer.getFtDate());
			ftDetails.setFtDocumentNumber(cmFundTransfer.getFtDocumentNumber());
			ftDetails.setFtReferenceNumber(cmFundTransfer.getFtReferenceNumber());
			ftDetails.setFtMemo(cmFundTransfer.getFtMemo());
			ftDetails.setFtAdBankAccountNameFrom(adBankAccountFrom.getBaName());
			ftDetails.setFtCurrencyFrom(adBankAccountFrom.getGlFunctionalCurrency().getFcName());
			ftDetails.setFtAdBankAccountNameTo(adBankAccountTo.getBaName());		           
			ftDetails.setFtCurrencyTo(adBankAccountTo.getGlFunctionalCurrency().getFcName());
			ftDetails.setFtAmount(cmFundTransfer.getFtAmount());
			ftDetails.setFtConversionDate(cmFundTransfer.getFtConversionDate());
			ftDetails.setFtConversionRateFrom(cmFundTransfer.getFtConversionRateFrom());
			ftDetails.setFtVoid(cmFundTransfer.getFtVoid());
			ftDetails.setFtApprovalStatus(cmFundTransfer.getFtApprovalStatus());
			ftDetails.setFtPosted(cmFundTransfer.getFtPosted());
			ftDetails.setFtCreatedBy(cmFundTransfer.getFtCreatedBy());
			ftDetails.setFtDateCreated(cmFundTransfer.getFtDateCreated());
			ftDetails.setFtLastModifiedBy(cmFundTransfer.getFtLastModifiedBy());
			ftDetails.setFtDateLastModified(cmFundTransfer.getFtDateLastModified());
			ftDetails.setFtApprovedRejectedBy(cmFundTransfer.getFtApprovedRejectedBy());
			ftDetails.setFtDateApprovedRejected(cmFundTransfer.getFtDateApprovedRejected());
			ftDetails.setFtPostedBy(cmFundTransfer.getFtPostedBy());
			ftDetails.setFtDatePosted(cmFundTransfer.getFtDatePosted());
			ftDetails.setFtReasonForRejection(cmFundTransfer.getFtReasonForRejection());
			ftDetails.setFtType(cmFundTransfer.getFtType());
			ftDetails.setFtConversionRateTo(cmFundTransfer.getFtConversionRateTo());

			if (cmFundTransfer.getFtType().equals("DEPOSIT")) {

				ArrayList cmFTRList = new ArrayList();
				Collection fundTransferReceipts = cmFundTransfer.getCmFundTransferReceipts();
				Iterator i = fundTransferReceipts.iterator();

				while (i.hasNext()) {

					LocalCmFundTransferReceipt cmFundTransferReceipt  = (LocalCmFundTransferReceipt) i.next();

					CmModFundTransferReceiptDetails mdetails = new CmModFundTransferReceiptDetails();

					mdetails.setFtrCode(cmFundTransferReceipt.getFtrCode());
					mdetails.setFtrTotalAmount(cmFundTransferReceipt.getArReceipt().getRctAmount());
					mdetails.setFtrReceiptNumber(cmFundTransferReceipt.getArReceipt().getRctNumber());
					mdetails.setFtrAmountDeposited(cmFundTransferReceipt.getFtrAmountDeposited());
					mdetails.setFtrReceiptDate(cmFundTransferReceipt.getArReceipt().getRctDate());

					Collection cmFundTransferReceipts = cmFundTransferReceipt.getArReceipt().getCmFundTransferReceipts();

					if (cmFundTransferReceipts == null) {

						mdetails.setFtrAmountUndeposited(cmFundTransferReceipt.getArReceipt().getRctAmount());

					} else {

						double TTL_DPSTD_AMNT = 0;

						Iterator j = cmFundTransferReceipts.iterator();

						while (j.hasNext()){

							LocalCmFundTransferReceipt existingCmFundTransferReceipt =  (LocalCmFundTransferReceipt) j.next();

							if (existingCmFundTransferReceipt.getCmFundTransfer().getFtPosted() == EJBCommon.TRUE)
								TTL_DPSTD_AMNT = TTL_DPSTD_AMNT + existingCmFundTransferReceipt.getFtrAmountDeposited();

						}

						mdetails.setFtrAmountUndeposited(cmFundTransferReceipt.getArReceipt().getRctAmount() - TTL_DPSTD_AMNT);

					}

					cmFTRList.add(mdetails);

				}

				ftDetails.setFtCmFTRList(cmFTRList);

			}

			return ftDetails;

		} catch (Exception ex) {

			ex.printStackTrace();
			throw new EJBException(ex.getMessage());

		}

	} 


	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer saveCmFtEntry(com.util.CmFundTransferDetails details, String BNK_NM_FRM, 
			String BNK_NM_TO, String FC_NM, boolean isDraft, ArrayList list, Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalRecordAlreadyDeletedException,
			GlobalTransactionAlreadyApprovedException,
			GlobalConversionDateNotExistException,
			GlobalTransactionAlreadyPendingException,
			GlobalTransactionAlreadyPostedException,
			GlobalTransactionAlreadyVoidException,
			GlobalNoApprovalRequesterFoundException,
			GlobalNoApprovalApproverFoundException,
			GlJREffectiveDateNoPeriodExistException,
			GlJREffectiveDatePeriodClosedException,
			GlobalJournalNotBalanceException,
			GlobalDocumentNumberNotUniqueException,
			GlobalBranchAccountNumberInvalidException,
			GlobalNoRecordFoundException,
			GlobalOverapplicationNotAllowedException{

		Debug.print("CmFundTransferEntryControllerBean saveCmFtEntry");

		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;     
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalCmFundTransferReceiptHome cmFundTransferReceiptHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalCmFundTransfer cmFundTransfer = null;
		LocalAdBankAccount adBankAccountFrom = null;  
		LocalAdBankAccount adBankAccountTo = null;
		LocalCmDistributionRecord cmDistributionRecord = null;

		Collection cmDistributionRecords = null;

		try {

			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);  
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);    
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);    
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);     
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class); 
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			cmFundTransferReceiptHome = (LocalCmFundTransferReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferReceiptHome.JNDI_NAME, LocalCmFundTransferReceiptHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class); 			

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// find bank account from 

			try {

				adBankAccountFrom = adBankAccountHome.findByBaName(BNK_NM_FRM, AD_CMPNY);

			} catch (FinderException ex) {

			}

			// find bank account to

			try {

				adBankAccountTo = adBankAccountHome.findByBaName(BNK_NM_TO, AD_CMPNY);

			} catch (FinderException ex) {

			}


			// validate if fund transfer is already deleted

			try {

				if (details.getFtCode() != null) {

					cmFundTransfer = cmFundTransferHome.findByPrimaryKey(details.getFtCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if fund transfer is already posted, void, arproved or pending

			if (details.getFtCode() != null) {

				if (cmFundTransfer.getFtApprovalStatus() != null) {

					if (cmFundTransfer.getFtApprovalStatus().equals("APPROVED") ||
							cmFundTransfer.getFtApprovalStatus().equals("N/A")) {         		    	

						throw new GlobalTransactionAlreadyApprovedException(); 


					} else if (cmFundTransfer.getFtApprovalStatus().equals("PENDING")) {

						throw new GlobalTransactionAlreadyPendingException();

					}

				}

				if (cmFundTransfer.getFtPosted() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyPostedException();

				} else if (cmFundTransfer.getFtVoid() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyVoidException();

				}

			}

			// fund tranfer void

			if (details.getFtCode() != null && details.getFtVoid() == EJBCommon.TRUE &&
					cmFundTransfer.getFtPosted() == EJBCommon.FALSE) {

				cmFundTransfer.setFtVoid(EJBCommon.TRUE);
				cmFundTransfer.setFtLastModifiedBy(details.getFtLastModifiedBy());
				cmFundTransfer.setFtDateLastModified(details.getFtDateLastModified());

				return cmFundTransfer.getFtCode();

			}

			// validate if document number is unique document number is automatic then set next sequence

			if (details.getFtCode() == null) {

				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

				try {

					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("CM FUND TRANSFER", AD_CMPNY);

				} catch (FinderException ex) {

				}

				try {

					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

				}

				LocalCmFundTransfer cmExistingFundTransfer = null;

				try {

					cmExistingFundTransfer = cmFundTransferHome.findByFtDocumentNumberAndBrCode(details.getFtDocumentNumber(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) { 
				}

				if (cmExistingFundTransfer != null) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
						(details.getFtDocumentNumber() == null || details.getFtDocumentNumber().trim().length() == 0)) {

					while (true) {

						if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

							try {

								cmFundTransferHome.findByFtDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);		            		
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	

							} catch (FinderException ex) {

								details.setFtDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
								break;

							}	            			            			            			            	

						} else {

							try {

								cmFundTransferHome.findByFtDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	

							} catch (FinderException ex) {

								details.setFtDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
								break;

							}	            			            			            			            	

						}

					}		            

				}

			} else {

				LocalCmFundTransfer cmExistingTransfer = null;

				try {

					cmExistingTransfer = cmFundTransferHome.findByFtDocumentNumberAndBrCode(details.getFtDocumentNumber(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) { 

				}

				if (cmExistingTransfer != null && 
						!cmExistingTransfer.getFtCode().equals(details.getFtCode())) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (cmFundTransfer.getFtDocumentNumber() != details.getFtDocumentNumber() &&
						(details.getFtDocumentNumber() == null || details.getFtDocumentNumber().trim().length() == 0)) {

					details.setFtDocumentNumber(cmFundTransfer.getFtDocumentNumber());

				}

			}

			// validate if conversion date exists

			try {

				if (details.getFtConversionDate() != null) {

					LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
					LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

					if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = 
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
									details.getFtConversionDate(), AD_CMPNY);

					} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = 
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(
									adCompany.getGlFunctionalCurrency().getFcCode(), details.getFtConversionDate(), AD_CMPNY);

					}

				}	          

			} catch (FinderException ex) {

				throw new GlobalConversionDateNotExistException();

			}	


			// used in checking if fund transfer should re-generate distribution records

			boolean isRecalculate = true;

			if (details.getFtCode() == null) {

				// create new fund transfer

				cmFundTransfer = cmFundTransferHome.create(details.getFtDate(), 
						details.getFtDocumentNumber(), details.getFtReferenceNumber(), details.getFtMemo(),
						adBankAccountFrom.getBaCode(), adBankAccountTo.getBaCode(),
						details.getFtAmount(), details.getFtConversionDate(), 
						details.getFtConversionRateFrom(), EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE, null, EJBCommon.FALSE, details.getFtCreatedBy(), details.getFtDateCreated(),
						details.getFtLastModifiedBy(), details.getFtDateLastModified(), null, null, null, null, null,
						details.getFtType(), details.getFtConversionRateTo(), null, null, AD_BRNCH, AD_CMPNY);			        	

			} else {

				// update fund transfer 

				cmFundTransfer = cmFundTransferHome.findByPrimaryKey(details.getFtCode());

				// check if critical fields are changed

				if (!cmFundTransfer.getFtAdBaAccountFrom().equals(adBankAccountFrom.getBaCode()) ||
						!cmFundTransfer.getFtAdBaAccountTo().equals(adBankAccountTo.getBaCode()) ||
						cmFundTransfer.getFtAmount() != details.getFtAmount() ||
						!cmFundTransfer.getFtType().equals(details.getFtType()) || 
						(cmFundTransfer.getFtType().equals("DEPOSIT") && list.size() != cmFundTransfer.getCmFundTransferReceipts().size())) {

					isRecalculate = true;

				} else if (cmFundTransfer.getFtType().equals("DEPOSIT") &&
						list.size() == cmFundTransfer.getCmFundTransferReceipts().size()) {
						
					Iterator iter = cmFundTransfer.getCmFundTransferReceipts().iterator();
        			Iterator listIter = list.iterator();
        			
        			while (iter.hasNext()) {
        				
        				LocalCmFundTransferReceipt cmFundTransferReceipt = (LocalCmFundTransferReceipt)iter.next();
        				CmModFundTransferReceiptDetails mFtrDetails = (CmModFundTransferReceiptDetails)listIter.next();
        				        				
        				if (!cmFundTransferReceipt.getArReceipt().getRctNumber().equals(mFtrDetails.getFtrReceiptNumber()) || 
        						cmFundTransferReceipt.getFtrAmountDeposited() != mFtrDetails.getFtrAmountDeposited()) {
        					
        					isRecalculate = true;
        					break;
        					
        				}
        				
        				isRecalculate = false;
        				
        			}
					
				} else {
					
					isRecalculate = false;
					
				}

				cmFundTransfer.setFtDate(details.getFtDate());
				cmFundTransfer.setFtDocumentNumber(details.getFtDocumentNumber());
				cmFundTransfer.setFtReferenceNumber(details.getFtReferenceNumber());
				cmFundTransfer.setFtMemo(details.getFtMemo());
				cmFundTransfer.setFtAdBaAccountFrom(adBankAccountFrom.getBaCode());
				cmFundTransfer.setFtAdBaAccountTo(adBankAccountTo.getBaCode());
				cmFundTransfer.setFtAmount(details.getFtAmount());
				cmFundTransfer.setFtConversionDate(details.getFtConversionDate());
				cmFundTransfer.setFtConversionRateFrom(details.getFtConversionRateFrom());
				cmFundTransfer.setFtVoid(details.getFtVoid());
				cmFundTransfer.setFtAccountFromReconciled(EJBCommon.FALSE);
				cmFundTransfer.setFtAccountToReconciled(EJBCommon.FALSE);
				cmFundTransfer.setFtAccountFromDateReconciled(null);
				cmFundTransfer.setFtAccountToDateReconciled(null);
				cmFundTransfer.setFtLastModifiedBy(details.getFtLastModifiedBy());
				cmFundTransfer.setFtDateLastModified(details.getFtDateLastModified());
				cmFundTransfer.setFtReasonForRejection(null);
				cmFundTransfer.setFtType(details.getFtType());
				cmFundTransfer.setFtConversionRateTo(details.getFtConversionRateTo());

			}

			if (isRecalculate) {

				// remove all distribution records

				cmDistributionRecords = cmFundTransfer.getCmDistributionRecords();

				Iterator i = cmDistributionRecords.iterator();     	  

				while (i.hasNext()) {

					cmDistributionRecord = (LocalCmDistributionRecord)i.next();

					i.remove();

					cmDistributionRecord.remove();

				}

				// add new distribution record

				this.addCmDrEntry(cmFundTransfer.getCmDrNextLine(), 
						"CASH", EJBCommon.TRUE, cmFundTransfer.getFtAmount(),
						adBankAccountTo.getBaCoaGlCashAccount(), cmFundTransfer, AD_BRNCH, AD_CMPNY);

				this.addCmDrEntry(cmFundTransfer.getCmDrNextLine(), 
						"CASH", EJBCommon.FALSE, cmFundTransfer.getFtAmount(),
						adBankAccountFrom.getBaCoaGlCashAccount(), cmFundTransfer, AD_BRNCH, AD_CMPNY);

				if (cmFundTransfer.getFtType().equals("DEPOSIT")){

					// remove all cmFundTransferReceipts

					Collection cmFundTransferReceipts = cmFundTransfer.getCmFundTransferReceipts();

					i = cmFundTransferReceipts.iterator();     	  

					while (i.hasNext()) {

						LocalCmFundTransferReceipt cmFundTransferReceipt = (LocalCmFundTransferReceipt)i.next();

						i.remove();
						cmFundTransferReceipt.getArReceipt().setRctLock(EJBCommon.FALSE);
						cmFundTransferReceipt.remove();

					}

					// add new cmFundTransferReceipt

					i = list.iterator();

					while (i.hasNext()) {

						CmModFundTransferReceiptDetails mFtrDetails = (CmModFundTransferReceiptDetails) i.next();

						if (mFtrDetails.getFtrAmountDeposited() > mFtrDetails.getFtrAmountUndeposited()){

							throw new GlobalOverapplicationNotAllowedException();

						}

						LocalCmFundTransferReceipt cmFundTransferReceipt = cmFundTransferReceiptHome.create(
								mFtrDetails.getFtrAmountDeposited(), AD_CMPNY);



						cmFundTransfer.addCmFundTransferReceipt(cmFundTransferReceipt);
						cmFundTransferReceipt.setCmFundTransfer(cmFundTransfer);

						LocalArReceipt arReceipt = null; 

						try {

							arReceipt = arReceiptHome.findByRctNumberAndBrCode(
									mFtrDetails.getFtrReceiptNumber(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {

							throw new GlobalNoRecordFoundException();

						}

						arReceipt.addCmFundTransferReceipt(cmFundTransferReceipt);
						cmFundTransferReceipt.setArReceipt(arReceipt);

						arReceipt.setRctLock(EJBCommon.TRUE);

					}


				}

			}

			// generate approval status

			String FT_APPPRVL_STATUS = null;

			if (!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if fund transfer approval is enabled

				if (adApproval.getAprEnableCmFundTransfer() == EJBCommon.FALSE) {

					FT_APPPRVL_STATUS = "N/A";

				} else {

					// check if fund transfer is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("CM FUND TRANSFER", "REQUESTER", details.getFtLastModifiedBy(), AD_CMPNY);       			

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (cmFundTransfer.getFtAmount() <= adAmountLimit.getCalAmountLimit()) {

						FT_APPPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("CM FUND TRANSFER", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "CM FUND TRANSFER", cmFundTransfer.getFtCode(), 
										cmFundTransfer.getFtDocumentNumber(), cmFundTransfer.getFtDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

							}        				 	

						} else {

							boolean isApprovalUsersFound = false;

							Iterator n = adAmountLimits.iterator();

							while (n.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)n.next();        				 		

								if (cmFundTransfer.getFtAmount() <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "CM FUND TRANSFER", cmFundTransfer.getFtCode(), 
												cmFundTransfer.getFtDocumentNumber(), cmFundTransfer.getFtDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}        				 			

									break;

								} else if (!n.hasNext()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "CM FUND TRANSFER", cmFundTransfer.getFtCode(), 
												cmFundTransfer.getFtDocumentNumber(), cmFundTransfer.getFtDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}        				 			

									break;

								}        				 		

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}        				 

						}

						FT_APPPRVL_STATUS = "PENDING";
					}        			        			        			
				}        		        		        		
			}

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			if (FT_APPPRVL_STATUS != null && FT_APPPRVL_STATUS.equals("N/A") && adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

				this.executeCmFtPost(cmFundTransfer.getFtCode(), cmFundTransfer.getFtLastModifiedBy(), AD_BRNCH, AD_CMPNY);

			}

			// set fund transfer approval status

			cmFundTransfer.setFtApprovalStatus(FT_APPPRVL_STATUS);

			return cmFundTransfer.getFtCode();

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;     

		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		}  catch (GlobalConversionDateNotExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex; 

		} catch (GlJREffectiveDateNoPeriodExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlJREffectiveDatePeriodClosedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			getSessionContext().setRollbackOnly();
			throw ex; 	

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex; 	

		} catch (GlobalNoRecordFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalOverapplicationNotAllowedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;        	

		} catch (Exception ex) {

			ex.printStackTrace();
			getSessionContext().setRollbackOnly();     
			throw new EJBException(ex.getMessage());

		} 

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteCmFtEntry(Integer FT_CODE, String AD_USR, Integer AD_CMPNY) throws 
	GlobalRecordAlreadyDeletedException {

		Debug.print("CmFundTransferEntryControllerBean deleteCmFtEntry");

		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

		// Initialize EJB Home

		try {

			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);  


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}        

		try {

			LocalCmFundTransfer cmFundTransfer = cmFundTransferHome.findByPrimaryKey(FT_CODE);

			if (cmFundTransfer.getFtApprovalStatus() != null && cmFundTransfer.getFtApprovalStatus().equals("PENDING")) {

				Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("CM FUND TRANSFER", cmFundTransfer.getFtCode(), AD_CMPNY);

				Iterator i = adApprovalQueues.iterator();

				while(i.hasNext()) {

					LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

					adApprovalQueue.remove();

				}

			}

			// remove all cmFundTransferReceipts

			Collection cmFundTransferReceipts = cmFundTransfer.getCmFundTransferReceipts();

			Iterator i = cmFundTransferReceipts.iterator();     	  

			while (i.hasNext()) {

				LocalCmFundTransferReceipt cmFundTransferReceipt = (LocalCmFundTransferReceipt)i.next();

				cmFundTransferReceipt.getArReceipt().setRctLock(EJBCommon.FALSE);

			}
			
			adDeleteAuditTrailHome.create("CM FUND TRANSFER", cmFundTransfer.getFtDate(), cmFundTransfer.getFtDocumentNumber(), cmFundTransfer.getFtReferenceNumber(),
					cmFundTransfer.getFtAmount(), AD_USR, new Date(), AD_CMPNY);

			cmFundTransfer.remove();

		} catch (FinderException ex) {	

			getSessionContext().setRollbackOnly();
			throw new GlobalRecordAlreadyDeletedException();      	

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalNotifiedUsersByFtCode(Integer FT_CODE, Integer AD_CMPNY) {

		Debug.print("CmFundTransferEntryControllerBean getAdApprovalNotifiedUsersByFtCode");


		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;

		ArrayList list = new ArrayList();


		// Initialize EJB Home

		try {

			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);       
			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalCmFundTransfer cmFundTransfer = cmFundTransferHome.findByPrimaryKey(FT_CODE);

			if (cmFundTransfer.getFtPosted() == EJBCommon.TRUE) {

				list.add("DOCUMENT POSTED");
				return list;

			}

			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("CM FUND TRANSFER", FT_CODE, AD_CMPNY);

			Iterator i = adApprovalQueues.iterator();

			while(i.hasNext()) {

				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

				list.add(adApprovalQueue.getAdUser().getUsrDescription());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}    


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdModBankAccountDetails getAdBaByBaName(String BA_NM, Integer AD_CMPNY) {

		Debug.print("CmFundTransferEntryControllerBean getAdBaByBaName");

		LocalAdBankAccountHome adBankAccountHome = null;

		//initialized EJB Home

		try {

			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(BA_NM, AD_CMPNY);

			AdModBankAccountDetails mdetails = new AdModBankAccountDetails();

			if(adBankAccount.getGlFunctionalCurrency() != null) {

				mdetails.setBaFcName(adBankAccount.getGlFunctionalCurrency().getFcName());
				mdetails.setBaIsCashAccount(adBankAccount.getBaIsCashAccount());

			}

			return mdetails;


		} catch (Exception ex) {

			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
		}

	}       

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("CmFundTransferEntryControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY) 
	throws GlobalConversionDateNotExistException {

		Debug.print("CmFundTransferEntryControllerBean getFrRateByFrNameAndFrDate");

		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

			double CONVERSION_RATE = 1;

			// Get functional currency rate

			if (!FC_NM.equals("USD")) {

				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
							CONVERSION_DATE, AD_CMPNY);

				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

			}

			// Get set of book functional currency rate if necessary

			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
							CONVERSION_DATE, AD_CMPNY);

				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();

			}

			return CONVERSION_RATE;

		} catch (FinderException ex) {	

			getSessionContext().setRollbackOnly();
			throw new GlobalConversionDateNotExistException();  

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getPostedArReceiptByRctDateFromAndRctDateToAndBankAccountName (Date RCT_DT_FRM, Date RCT_DT_TO, String BA_NM, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		ArrayList list = new ArrayList();
		LocalArReceiptHome arReceiptHome = null;

		// Initialize EJB Home

		try {

			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);                   

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		Collection arReceipts = null;

		try {

			if(RCT_DT_FRM != null && RCT_DT_TO != null) {

				arReceipts = arReceiptHome.findUndepositedPostedRctByBaNameAndBrCodeAndRctDateRange(RCT_DT_FRM, RCT_DT_TO, BA_NM, AD_BRNCH, AD_CMPNY);

			} else {

				arReceipts = arReceiptHome.findUndepositedPostedRctByBaNameAndBrCode(BA_NM, AD_BRNCH, AD_CMPNY);

			}
		}  catch (FinderException ex) {

			throw new GlobalNoRecordFoundException();

		}

		if (arReceipts.size() == 0) {

			throw new GlobalNoRecordFoundException();

		}

		try { 

			Iterator i = arReceipts.iterator();

			while (i.hasNext()){

				LocalArReceipt arReceipt = (LocalArReceipt) i.next();

				CmModFundTransferReceiptDetails details= new CmModFundTransferReceiptDetails();

				details.setFtrReceiptDate(arReceipt.getRctDate());
				details.setFtrAdCompany(arReceipt.getRctAdCompany());
				details.setFtrReceiptNumber(arReceipt.getRctNumber());
				details.setFtrTotalAmount(arReceipt.getRctAmount());

				Collection cmFundTransferReceipts = arReceipt.getCmFundTransferReceipts();

				if (cmFundTransferReceipts == null) {

					details.setFtrAmountDeposited(arReceipt.getRctAmount());
					details.setFtrAmountUndeposited(arReceipt.getRctAmount());

				} else {

					double TTL_DPSTD_AMNT = 0;

					Iterator j = cmFundTransferReceipts.iterator();

					while (j.hasNext()){

						LocalCmFundTransferReceipt cmFundTransferReceipt =  (LocalCmFundTransferReceipt) j.next();

						if (cmFundTransferReceipt.getCmFundTransfer().getFtPosted() == EJBCommon.TRUE) {

							TTL_DPSTD_AMNT = TTL_DPSTD_AMNT + cmFundTransferReceipt.getFtrAmountDeposited();

						}

					}

					details.setFtrAmountDeposited(arReceipt.getRctAmount() - TTL_DPSTD_AMNT);
					details.setFtrAmountUndeposited(arReceipt.getRctAmount() - TTL_DPSTD_AMNT);

				}

				details.setOrderBy(ORDER_BY);
				
				if (details.getFtrAmountDeposited() != 0) {

					list.add(details);

				}

			}

			if (list.size() == 0) {

				throw new GlobalNoRecordFoundException ();

			} else {

				Collections.sort(list, CmModFundTransferReceiptDetails.NoGroupComparator);
				
				return list;

			}

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void clearCmFundTransferReceipts(Integer FT_CODE, Integer AD_CMPNY) throws 
	GlobalRecordAlreadyDeletedException {

		Debug.print("CmFundTransferEntryControllerBean clearCmFundTransferReceipts");
		
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalCmFundTransferReceiptHome cmFundTransferReceiptHome = null;   
		
		// Initialize EJB Home

		try {

			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);  
			cmFundTransferReceiptHome = (LocalCmFundTransferReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferReceiptHome.JNDI_NAME, LocalCmFundTransferReceiptHome.class);            
		
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}
		
		try {

			LocalCmFundTransfer cmFundTransfer = cmFundTransferHome.findByPrimaryKey(FT_CODE);

			// remove all cmFundTransferReceipts

			Collection cmFundTransferReceipts = cmFundTransfer.getCmFundTransferReceipts();

			Iterator i = cmFundTransferReceipts.iterator();     	  

			while (i.hasNext()) {

				LocalCmFundTransferReceipt cmFundTransferReceipt = (LocalCmFundTransferReceipt)i.next();

				i.remove();
				cmFundTransferReceipt.getArReceipt().setRctLock(EJBCommon.FALSE);
				cmFundTransferReceipt.remove();

			}
		} catch (FinderException ex) {	

			getSessionContext().setRollbackOnly();
			throw new GlobalRecordAlreadyDeletedException();      	

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}
		
	}

	//private methods
	private void addCmDrEntry(short DR_LN, String DR_CLSS,
			byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalCmFundTransfer cmFundTransfer, 
			Integer AD_BRNCH, Integer AD_CMPNY) 
	throws GlobalBranchAccountNumberInvalidException {

		Debug.print("CmFundTransferEntryControllerBean addCmDrEntry");

		LocalCmDistributionRecordHome cmDistributionRecordHome = null;        
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);            
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}            

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

			// create distribution record 

			LocalCmDistributionRecord cmDistributionRecord = cmDistributionRecordHome.create(
					DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_DBT,
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			cmFundTransfer.addCmDistributionRecord(cmDistributionRecord);
			glChartOfAccount.addCmDistributionRecord(cmDistributionRecord);		   

		} catch (FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void executeCmFtPost(Integer FT_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,		
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException{

		Debug.print("CmFundTransferEntryControllerBean executeCmFtPost");

		LocalCmFundTransferHome cmFundTransferHome = null;        
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGlJournalHome glJournalHome = null;
		LocalGlJournalBatchHome glJournalBatchHome = null;
		LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalGlJournalSourceHome glJournalSourceHome = null;
		LocalGlJournalCategoryHome glJournalCategoryHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
		LocalGlForexLedgerHome glForexLedgerHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

		LocalCmFundTransfer cmFundTransfer = null;

		// Initialize EJB Home

		try {

			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);  
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
			glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
			glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
			glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}       

		try {


			// validate if fund transfer is already deleted

			try {

				cmFundTransfer = cmFundTransferHome.findByPrimaryKey(FT_CODE);

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if fund transfer is already posted or void

			if (cmFundTransfer.getFtPosted() == EJBCommon.TRUE) {

				throw new GlobalTransactionAlreadyPostedException();

			} else if (cmFundTransfer.getFtVoid() == EJBCommon.TRUE) {

				throw new GlobalTransactionAlreadyVoidException();
			}

			// post fund transfer

			if (cmFundTransfer.getFtVoid() == EJBCommon.FALSE && cmFundTransfer.getFtPosted() == EJBCommon.FALSE) {            

				// update bank account from balance (decrease) 

				LocalAdBankAccount adBankAccountFrom = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

				try {

					// find bankaccount balance before or equal receipt date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountFrom(), "BOOK", AD_CMPNY);

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(cmFundTransfer.getFtDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									cmFundTransfer.getFtDate(), adBankAccountBalance.getBabBalance() - cmFundTransfer.getFtAmount(), "BOOK", AD_CMPNY);

							adBankAccountFrom.addAdBankAccountBalance(adNewBankAccountBalance);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmFundTransfer.getFtAmount());

						} 

					} else {        	

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								cmFundTransfer.getFtDate(), (0 - cmFundTransfer.getFtAmount()), "BOOK", AD_CMPNY);

						adBankAccountFrom.addAdBankAccountBalance(adNewBankAccountBalance);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountFrom(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmFundTransfer.getFtAmount());

					}			

				} catch (Exception ex) {

					ex.printStackTrace();

				}

				// update bank account to balance (increase)

				LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountTo());

				try {

					// find bankaccount balance before or equal receipt date

					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountTo(), "BOOK", AD_CMPNY);

					// get convert amount to bank to currency
					double CNVRSN_RT_FRM_TO = cmFundTransfer.getFtConversionRateFrom()/cmFundTransfer.getFtConversionRateTo();
					double CNVRTD_AMNT = EJBCommon.roundIt(cmFundTransfer.getFtAmount() *  CNVRSN_RT_FRM_TO,
							getGlFcPrecisionUnit(AD_CMPNY));

					if (!adBankAccountBalances.isEmpty()) {

						// get last check

						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);

						if (adBankAccountBalance.getBabDate().before(cmFundTransfer.getFtDate())) {

							// create new balance

							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
									cmFundTransfer.getFtDate(), adBankAccountBalance.getBabBalance() + CNVRTD_AMNT, "BOOK", AD_CMPNY);

							adBankAccountTo.addAdBankAccountBalance(adNewBankAccountBalance);

						} else { // equals to check date

							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + CNVRTD_AMNT);

						} 

					} else {        	

						// create new balance

						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
								cmFundTransfer.getFtDate(), (CNVRTD_AMNT), "BOOK", AD_CMPNY);

						adBankAccountTo.addAdBankAccountBalance(adNewBankAccountBalance);

					}

					// propagate to subsequent balances if necessary

					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountTo(), "BOOK", AD_CMPNY);

					Iterator i = adBankAccountBalances.iterator();

					while (i.hasNext()) {

						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();

						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + CNVRTD_AMNT);

					}			

				} catch (Exception ex) {

					ex.printStackTrace();

				}

			}

			// set post status

			cmFundTransfer.setFtPosted(EJBCommon.TRUE);
			cmFundTransfer.setFtPostedBy(USR_NM);
			cmFundTransfer.setFtDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

			// post to gl if necessary

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			if (adPreference.getPrfCmGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

				// validate if date has no period and period is closed

				LocalGlSetOfBook glJournalSetOfBook = null;

				try {

					glJournalSetOfBook = glSetOfBookHome.findByDate(cmFundTransfer.getFtDate(), AD_CMPNY);

				} catch (FinderException ex) {

					throw new GlJREffectiveDateNoPeriodExistException();

				}

				LocalGlAccountingCalendarValue glAccountingCalendarValue = 
					glAccountingCalendarValueHome.findByAcCodeAndDate(
							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), cmFundTransfer.getFtDate(), AD_CMPNY);


				if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
						glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

					throw new GlJREffectiveDatePeriodClosedException();

				}

				// check if invoice is balance if not check suspense posting

				LocalGlJournalLine glOffsetJournalLine = null;

				Collection cmDistributionRecords = cmDistributionRecordHome.findByDrReversalAndDrImportedAndFtCode(EJBCommon.FALSE, EJBCommon.FALSE, cmFundTransfer.getFtCode(), AD_CMPNY);

				Iterator j = cmDistributionRecords.iterator();

				double TOTAL_DEBIT = 0d;
				double TOTAL_CREDIT = 0d;

				while (j.hasNext()) {

					LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

					LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

					double DR_AMNT = this.convertForeignToFunctionalCurrency(adBankAccount.getGlFunctionalCurrency().getFcCode(),
							adBankAccount.getGlFunctionalCurrency().getFcName(), 
							cmFundTransfer.getFtConversionDate(),
							cmFundTransfer.getFtConversionRateFrom(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);

					if (cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

						TOTAL_DEBIT += DR_AMNT;

					} else {

						TOTAL_CREDIT += DR_AMNT;

					}

				}

				TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
				TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

				if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
						TOTAL_DEBIT != TOTAL_CREDIT) {

					LocalGlSuspenseAccount glSuspenseAccount = null;

					try { 	

						glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("CASH MANAGEMENT", "FUND TRANSFERS", AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalJournalNotBalanceException();

					}

					if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

						glOffsetJournalLine = glJournalLineHome.create(
								(short)(cmDistributionRecords.size() + 1),
								EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

					} else {

						glOffsetJournalLine = glJournalLineHome.create(
								(short)(cmDistributionRecords.size() + 1),
								EJBCommon.FALSE,
								TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

					}

					LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
					glChartOfAccount.addGlJournalLine(glOffsetJournalLine);


				} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
						TOTAL_DEBIT != TOTAL_CREDIT) {

					throw new GlobalJournalNotBalanceException();		    	

				}

				// create journal batch if necessary

				LocalGlJournalBatch glJournalBatch = null;
				java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

				try {

					glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " FUND TRANSFERS", AD_BRNCH, AD_CMPNY);		       	   

				} catch (FinderException ex) {
				}

				if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE && 
						glJournalBatch == null) {

					glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " FUND TRANSFERS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

				}

				// create journal entry			            	

				LocalGlJournal glJournal = glJournalHome.create(cmFundTransfer.getFtReferenceNumber(),
						cmFundTransfer.getFtMemo(), cmFundTransfer.getFtDate(),
						0.0d, null, cmFundTransfer.getFtDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);

				LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("CASH MANAGEMENT", AD_CMPNY);
				glJournal.setGlJournalSource(glJournalSource);

				LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
				glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

				LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("FUND TRANSFERS", AD_CMPNY);
				glJournal.setGlJournalCategory(glJournalCategory);

				if (glJournalBatch != null) {

					glJournal.setGlJournalBatch(glJournalBatch);

				}           		    


				// create journal lines

				j = cmDistributionRecords.iterator();

				while (j.hasNext()) {

					LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

					LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

					double DR_AMNT = this.convertForeignToFunctionalCurrency(adBankAccount.getGlFunctionalCurrency().getFcCode(),
							adBankAccount.getGlFunctionalCurrency().getFcName(), 
							cmFundTransfer.getFtConversionDate(),
							cmFundTransfer.getFtConversionRateFrom(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);

					LocalGlJournalLine glJournalLine = glJournalLineHome.create(
							cmDistributionRecord.getDrLine(),	            			
							cmDistributionRecord.getDrDebit(),
							DR_AMNT, "", AD_CMPNY);

					cmDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);

					glJournal.addGlJournalLine(glJournalLine);

					cmDistributionRecord.setDrImported(EJBCommon.TRUE);

					LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountTo());

					// for FOREX revaluation
					if(((adBankAccount.getGlFunctionalCurrency().getFcCode() !=
						adCompany.getGlFunctionalCurrency().getFcCode()) &&
						glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
						(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
								adBankAccount.getGlFunctionalCurrency().getFcCode()))) || 
								((adBankAccountTo.getGlFunctionalCurrency().getFcCode() !=
									adCompany.getGlFunctionalCurrency().getFcCode()) &&
									glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
									(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
											adBankAccountTo.getGlFunctionalCurrency().getFcCode())))){

						double CONVERSION_RATE = 1;

						if (glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
								adBankAccount.getGlFunctionalCurrency().getFcCode()) &&
								cmFundTransfer.getFtConversionRateFrom() != 0 && cmFundTransfer.getFtConversionRateFrom() != 1) {

							CONVERSION_RATE = cmFundTransfer.getFtConversionRateFrom();

						} else if (glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
								adBankAccount.getGlFunctionalCurrency().getFcCode()) && cmFundTransfer.getFtConversionRateTo() != 0
								&& cmFundTransfer.getFtConversionRateTo() != 1) {

							CONVERSION_RATE = cmFundTransfer.getFtConversionRateTo();

						} else if (cmFundTransfer.getFtConversionDate() != null){

							CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
									glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
									glJournal.getJrConversionDate(), AD_CMPNY);

						}

						Collection glForexLedgers = null;

						try {

							glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
									cmFundTransfer.getFtDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);

						} catch(FinderException ex) {

						}

						LocalGlForexLedger glForexLedger =
							(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
								(LocalGlForexLedger) glForexLedgers.iterator().next();

						int FRL_LN = (glForexLedger != null &&
								glForexLedger.getFrlDate().compareTo(cmFundTransfer.getFtDate()) == 0) ?
										glForexLedger.getFrlLine().intValue() + 1 : 1;

										// compute balance
										double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
										double FRL_AMNT = cmDistributionRecord.getDrAmount();

										if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
											FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));  
										else
											FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);

										COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;

										glForexLedger = glForexLedgerHome.create(cmFundTransfer.getFtDate(), new Integer (FRL_LN),
												"OTH", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);

										glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);

										// propagate balances
										try{

											glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
													glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
													glForexLedger.getFrlAdCompany());

										} catch (FinderException ex) {

										}

										Iterator itrFrl = glForexLedgers.iterator();

										while (itrFrl.hasNext()) {

											glForexLedger = (LocalGlForexLedger) itrFrl.next();
											FRL_AMNT = cmDistributionRecord.getDrAmount();

											if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
												FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT :
													(- 1 * FRL_AMNT));
											else
												FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) :
													FRL_AMNT);

											glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);

										}

					}        			

				}

				if (glOffsetJournalLine != null) {

					glJournal.addGlJournalLine(glOffsetJournalLine);

				}		

				// post journal to gl

				Collection glJournalLines = glJournal.getGlJournalLines();

				Iterator i = glJournalLines.iterator();

				while (i.hasNext()) {

					LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	

					// post current to current acv

					this.postToGl(glAccountingCalendarValue,
							glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);


					// post to subsequent acvs (propagate)

					Collection glSubsequentAccountingCalendarValues = 
						glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
								glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     

					Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

					while (acvsIter.hasNext()) {

						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
							(LocalGlAccountingCalendarValue)acvsIter.next();

						this.postToGl(glSubsequentAccountingCalendarValue,
								glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);

					}

					// post to subsequent years if necessary

					Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

					if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

						adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
						LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	

						Iterator sobIter = glSubsequentSetOfBooks.iterator();

						while (sobIter.hasNext()) {

							LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

							String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

							// post to subsequent acvs of subsequent set of book(propagate)

							Collection glAccountingCalendarValues = 
								glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     

							Iterator acvIter = glAccountingCalendarValues.iterator();

							while (acvIter.hasNext()) {

								LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
									(LocalGlAccountingCalendarValue)acvIter.next();

								if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
										ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

									this.postToGl(glSubsequentAccountingCalendarValue,
											glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);

								} else { // revenue & expense

									this.postToGl(glSubsequentAccountingCalendarValue,
											glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);					        

								}

							}

							if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

						}

					}

				}

				Collection cmFundTransferReceipts = cmFundTransfer.getCmFundTransferReceipts();

				i = cmFundTransferReceipts.iterator();

				while (i.hasNext()) {

					LocalCmFundTransferReceipt cmFundTransferReceipt = (LocalCmFundTransferReceipt) i.next();

					LocalArReceipt arReceipt = cmFundTransferReceipt.getArReceipt();

					arReceipt.setRctLock(EJBCommon.FALSE);

				}

			}

		} catch (GlJREffectiveDateNoPeriodExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlJREffectiveDatePeriodClosedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalJournalNotBalanceException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("CmFundTransferEntryControllerBean convertForeignToFunctionalCurrency");


		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}	     


		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

			AMOUNT = AMOUNT / CONVERSION_RATE;

		}
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

	private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
			LocalGlChartOfAccount glChartOfAccount, 
			boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("CmFundTransferEntryControllerBean postToGl");

		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {          

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       

			LocalGlChartOfAccountBalance glChartOfAccountBalance = 
				glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
						glAccountingCalendarValue.getAcvCode(),
						glChartOfAccount.getCoaCode(), AD_CMPNY);

			String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
			short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();



			if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
					isDebit == EJBCommon.TRUE) ||
					(!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
							isDebit == EJBCommon.FALSE)) {				    

				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   

				if (!isCurrentAcv) {

					glChartOfAccountBalance.setCoabBeginningBalance(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   

				}


			} else {

				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

				if (!isCurrentAcv) {

					glChartOfAccountBalance.setCoabBeginningBalance(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   

				}

			}

			if (isCurrentAcv) { 

				if (isDebit == EJBCommon.TRUE) {

					glChartOfAccountBalance.setCoabTotalDebit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			

				} else {

					glChartOfAccountBalance.setCoabTotalCredit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
				}       	   

			}

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}


	}

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("CmFundTransferEntryControllerBean ejbCreate");

	}
}
