
/*
 * CmFundTransferPostControllerBean.java
 *
 * Created on May 04, 2004, 7:18 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.cm.LocalCmFundTransferReceipt;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.util.AbstractSessionBean;
import com.util.CmModFundTransferEntryDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmFundTransferPostControllerEJB"
 *           display-name="Used for posting fund transfers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmFundTransferPostControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmFundTransferPostController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmFundTransferPostControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 * 
*/

public class CmFundTransferPostControllerBean extends AbstractSessionBean {

    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("CmFundTransferPostControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = adBankAccounts.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
	        	
	        	list.add(adBankAccount.getBaName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAll(Integer AD_CMPNY) {
                    
        Debug.print("CmFundTransferPostControllerBean getGlFcAll");
        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);
	        
	        Iterator i = glFunctionalCurrencies.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
	        	
	        	list.add(glFunctionalCurrency.getFcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getCmFtPostableByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("CmFundTransferPostControllerBean getCmFtPostableByCriteria");
        
        LocalCmFundTransferHome cmFundTransferHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
	          lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(ft) FROM CmFundTransfer ft ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size() + 2;  
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }	      
	      	      
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("referenceNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("ft.ftReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
		  	 
		  }
			
		  if (criteria.containsKey("bankAccountFrom")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }
	      	 
	      	 LocalAdBankAccount adBankAccountFrom = 
	      	     adBankAccountHome.findByBaName((String)criteria.get("bankAccountFrom"), AD_CMPNY);	      	 
 	 
	      	 jbossQl.append("ft.ftAdBaAccountFrom=?" + (ctr+1) + " ");
	      	 obj[ctr] = adBankAccountFrom.getBaCode();
	      	 ctr++;
	      	 
	      }
	      
	      if (criteria.containsKey("bankAccountTo")) {
	      	
	      	 if (!firstArgument) {
	      	 	jbossQl.append("AND ");
	      	 } else {
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 }

	      	 LocalAdBankAccount adBankAccountTo = 
	      	     adBankAccountHome.findByBaName((String)criteria.get("bankAccountTo"), AD_CMPNY);	      	 
	      	 
	      	 jbossQl.append("ft.ftAdBaAccountTo=?" + (ctr+1) + " ");
	      	 obj[ctr] = adBankAccountTo.getBaCode();
	      	 ctr++;
	      }	
	      
	      if (criteria.containsKey("documentNumberFrom")) {
		  	
		  	if (!firstArgument) {
		  		jbossQl.append("AND ");
		  	} else {
		  		firstArgument = false;
		  		jbossQl.append("WHERE ");
		  	}
		  	jbossQl.append("ft.ftDocumentNumber>=?" + (ctr+1) + " ");
		  	obj[ctr] = (String)criteria.get("documentNumberFrom");
		  	ctr++;
		  }  
		  
		  if (criteria.containsKey("documentNumberTo")) {
		  	
		  	if (!firstArgument) {
		  		jbossQl.append("AND ");
		  	} else {
		  		firstArgument = false;
		  		jbossQl.append("WHERE ");
		  	}
		  	jbossQl.append("ft.ftDocumentNumber<=?" + (ctr+1) + " ");
		  	obj[ctr] = (String)criteria.get("documentNumberTo");
		  	ctr++;
		  	
		  }
          			
	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("ft.ftDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("ft.ftDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
	      
	      if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("ft.ftApprovalStatus=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("approvalStatus");
	       	  ctr++;
	       	  
	      } else {
	      	
	      	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("(ft.ftApprovalStatus='APPROVED' OR ft.ftApprovalStatus='N/A') ");
	      	
	      }
	      
	      if (!firstArgument) {
	       	  	
	   	     jbossQl.append("AND ");
	   	     
	   	  } else {
	   	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");
	   	  	 
	   	  }
	   	  
	   	  jbossQl.append("ft.ftAdBranch=" + AD_BRNCH + " ");
	      	      
	      if (!firstArgument) {
	       	  	
	   	     jbossQl.append("AND ");
	   	     
	   	  } else {
	   	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");
	   	  	 
	   	  }
	   	  
	   	  jbossQl.append("ft.ftPosted = 0 AND ft.ftVoid = 0 AND ft.ftAdCompany=" + AD_CMPNY + " ");
	   	  
		  String orderBy = null;
		  
		  if (ORDER_BY.equals("BANK ACCOUNT")) {
	          
	          orderBy = "ft.ftAdBaAccountFrom";

	      } else if (ORDER_BY.equals("REFERENCE NUMBER")) {
	      	
	      	  orderBy = "ft.ftReferenceNumber";
	      	  
	      } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
	      	
	      	  orderBy = "ft.ftDocumentNumber";	  
	      	
	      }

	  	  if (orderBy != null) {
		
		  	  jbossQl.append("ORDER BY " + orderBy + ", ft.ftDate");

		  } else {
		  	
		  	  jbossQl.append("ORDER BY ft.ftDate");
		  	
		  }
		  
		  jbossQl.append(" OFFSET ?" + (ctr + 1));	        
		  obj[ctr] = OFFSET;	      
		  ctr++;
		  
		  jbossQl.append(" LIMIT ?" + (ctr + 1));
		  obj[ctr] = LIMIT;
		  ctr++;		      
			  	      	
	      Collection cmFundTransfers = cmFundTransferHome.getFtByCriteria(jbossQl.toString(), obj);	         
		  
		  if (cmFundTransfers.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = cmFundTransfers.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next(); 
		  	  
		  	  LocalAdBankAccount adBankAccountFrom = 
				adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());
		
			  LocalAdBankAccount adBankAccountTo = 
				adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountTo());  	  
		  	  
		  	  CmModFundTransferEntryDetails mdetails = new CmModFundTransferEntryDetails();
		  	  mdetails.setFtCode(cmFundTransfer.getFtCode());
		  	  mdetails.setFtAmount(cmFundTransfer.getFtAmount());
	      	  mdetails.setFtDate(cmFundTransfer.getFtDate());
	      	  mdetails.setFtDocumentNumber(cmFundTransfer.getFtDocumentNumber());
	      	  mdetails.setFtReferenceNumber(cmFundTransfer.getFtReferenceNumber());
	      	  mdetails.setFtAdBankAccountNameFrom(adBankAccountFrom.getBaName());
	      	  mdetails.setFtAdBankAccountNameTo(adBankAccountTo.getBaName()); 	  
			  mdetails.setFtCurrencyFrom(adBankAccountFrom.getGlFunctionalCurrency().getFcName());			
			  mdetails.setFtCurrencyTo(adBankAccountTo.getGlFunctionalCurrency().getFcName()); 	  
			      	  	      	  		     
			  list.add(mdetails);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeCmFtPost(Integer FT_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException{
                    
        Debug.print("CmFundTransferPostControllerBean executeCmFtPost");
        
        LocalCmFundTransferHome cmFundTransferHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalCmDistributionRecordHome cmDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;        
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
 		
        LocalCmFundTransfer cmFundTransfer = null;
        
        // Initialize EJB Home
        
        try {
            
            cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);  
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	// validate if fund transfer is already deleted
        	
        	try {
        		
        		cmFundTransfer = cmFundTransferHome.findByPrimaryKey(FT_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if fund transfer is already posted or void
        	
        	if (cmFundTransfer.getFtPosted() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyPostedException();
        		
        	} else if (cmFundTransfer.getFtVoid() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyVoidException();
        	}
        	
        	// post fund transfer
        	
        	if (cmFundTransfer.getFtVoid() == EJBCommon.FALSE && cmFundTransfer.getFtPosted() == EJBCommon.FALSE) {            
        		
                // update bank account from balance (decrease)
                
 				LocalAdBankAccount adBankAccountFrom = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());
 				
 				try {
 					
 					// find bankaccount balance before or equal receipt date
 					
 					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountFrom(), "BOOK", AD_CMPNY);

 					if (!adBankAccountBalances.isEmpty()) {
 						
 						// get last check
 						
 						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
 						
 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
 						
 						if (adBankAccountBalance.getBabDate().before(cmFundTransfer.getFtDate())) {
 							
 							// create new balance

 							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 									cmFundTransfer.getFtDate(), adBankAccountBalance.getBabBalance() - cmFundTransfer.getFtAmount(), "BOOK", AD_CMPNY);
 							
 							adBankAccountFrom.addAdBankAccountBalance(adNewBankAccountBalance);
 							
 						} else { // equals to check date

 							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmFundTransfer.getFtAmount());
 							
 						} 
 						
 					} else {        	
 						
 						// create new balance
 						
 						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 								cmFundTransfer.getFtDate(), (0 - cmFundTransfer.getFtAmount()), "BOOK", AD_CMPNY);
 						
 						adBankAccountFrom.addAdBankAccountBalance(adNewBankAccountBalance);
 						
 					}
 					
 					// propagate to subsequent balances if necessary
 					
 					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountFrom(), "BOOK", AD_CMPNY);

 					Iterator i = adBankAccountBalances.iterator();
 					
 					while (i.hasNext()) {
 						
 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
 						
 						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() - cmFundTransfer.getFtAmount());
 						
 					}			
 					
 				} catch (Exception ex) {
 					
 					ex.printStackTrace();
 					
 				}
                    
                // update bank account to balance (increase)
                    
 				LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountTo());
 				
 				try {
 					
 					// find bankaccount balance before or equal receipt date
 					
 					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountTo(), "BOOK", AD_CMPNY);

					// get convert amount to bank to currency
					double CNVRSN_RT_FRM_TO = cmFundTransfer.getFtConversionRateFrom()/cmFundTransfer.getFtConversionRateTo();
					double CNVRTD_AMNT = EJBCommon.roundIt(cmFundTransfer.getFtAmount() *  CNVRSN_RT_FRM_TO,
							getGlFcPrecisionUnit(AD_CMPNY));
 					
 					if (!adBankAccountBalances.isEmpty()) {
 						
 						// get last check
 						
 						ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
 						
 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
 						
 						if (adBankAccountBalance.getBabDate().before(cmFundTransfer.getFtDate())) {
 							
 							// create new balance

 							LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 									cmFundTransfer.getFtDate(), adBankAccountBalance.getBabBalance() + CNVRTD_AMNT, "BOOK", AD_CMPNY);
 							
 							adBankAccountTo.addAdBankAccountBalance(adNewBankAccountBalance);
 							
 						} else { // equals to check date

 							adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + CNVRTD_AMNT);
 							
 						} 
 						
 					} else {        	
 						
 						// create new balance
 						
 						LocalAdBankAccountBalance adNewBankAccountBalance = adBankAccountBalanceHome.create(
 								cmFundTransfer.getFtDate(), (CNVRTD_AMNT), "BOOK", AD_CMPNY);
 						
 						adBankAccountTo.addAdBankAccountBalance(adNewBankAccountBalance);
 						
 					}
 					
 					// propagate to subsequent balances if necessary
 					
 					adBankAccountBalances = adBankAccountBalanceHome.findByAfterDateAndBaCodeAndType(cmFundTransfer.getFtDate(), cmFundTransfer.getFtAdBaAccountTo(), "BOOK", AD_CMPNY);

 					Iterator i = adBankAccountBalances.iterator();
 					
 					while (i.hasNext()) {
 						
 						LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)i.next();
 						
 						adBankAccountBalance.setBabBalance(adBankAccountBalance.getBabBalance() + cmFundTransfer.getFtAmount());
 						
 					}			
 					
 				} catch (Exception ex) {
 					
 					ex.printStackTrace();
 					
 				}
        		
        	}
        	
        	// set post status
        	
        	cmFundTransfer.setFtPosted(EJBCommon.TRUE);
        	cmFundTransfer.setFtPostedBy(USR_NM);
        	cmFundTransfer.setFtDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        	
        	// post to gl if necessary
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	if (adPreference.getPrfCmGlPostingType().equals("USE SL POSTING")) {
        		
        		// validate if date has no period and period is closed
        		
        		LocalGlSetOfBook glJournalSetOfBook = null;
        		
        		try {
        			
        			glJournalSetOfBook = glSetOfBookHome.findByDate(cmFundTransfer.getFtDate(), AD_CMPNY);
        			
        		} catch (FinderException ex) {
        			
        			throw new GlJREffectiveDateNoPeriodExistException();
        			
        		}
        		
        		LocalGlAccountingCalendarValue glAccountingCalendarValue = 
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), cmFundTransfer.getFtDate(), AD_CMPNY);
        		
        		
        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
        			
        			throw new GlJREffectiveDatePeriodClosedException();
        			
        		}
        		
        		// check if invoice is balance if not check suspense posting
        		
        		LocalGlJournalLine glOffsetJournalLine = null;
        		
        		Collection cmDistributionRecords = cmDistributionRecordHome.findByDrReversalAndDrImportedAndFtCode(EJBCommon.FALSE, EJBCommon.FALSE, cmFundTransfer.getFtCode(), AD_CMPNY);
        		
        		Iterator j = cmDistributionRecords.iterator();
        		
        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;
        		
        		while (j.hasNext()) {
        			
        			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();
        			
        			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());
        			
        			double DR_AMNT = this.convertForeignToFunctionalCurrency(adBankAccount.getGlFunctionalCurrency().getFcCode(),
        					adBankAccount.getGlFunctionalCurrency().getFcName(), 
							cmFundTransfer.getFtConversionDate(),
							cmFundTransfer.getFtConversionRateFrom(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);
        			
        			if (cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
        				
        				TOTAL_DEBIT += DR_AMNT;
        				
        			} else {
        				
        				TOTAL_CREDIT += DR_AMNT;
        				
        			}
        			
        		}
        		
        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		
        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			LocalGlSuspenseAccount glSuspenseAccount = null;
        			
        			try { 	
        				
        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("CASH MANAGEMENT", "FUND TRANSFERS", AD_CMPNY);
        				
        			} catch (FinderException ex) {
        				
        				throw new GlobalJournalNotBalanceException();
        				
        			}
        			
        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(cmDistributionRecords.size() + 1),
								EJBCommon.TRUE,
								TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
        				
        			} else {
        				
        				glOffsetJournalLine = glJournalLineHome.create(
        						(short)(cmDistributionRecords.size() + 1),
								EJBCommon.FALSE,
								TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
        				
        			}
        			
        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        			glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        			
        			
        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			throw new GlobalJournalNotBalanceException();		    	
        			
        		}
        		
        		// create journal batch if necessary
        		
        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        		
        		try {
        			
        			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " FUND TRANSFERS", AD_BRNCH, AD_CMPNY);		       	   
        			
        		} catch (FinderException ex) {
        		}
        		
        		if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE && 
        				glJournalBatch == null) {
        			
        			glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " FUND TRANSFERS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        			
        		}
        		
        		// create journal entry			            	
        		
        		LocalGlJournal glJournal = glJournalHome.create(cmFundTransfer.getFtReferenceNumber(),
        				cmFundTransfer.getFtMemo(), cmFundTransfer.getFtDate(),
						0.0d, null, cmFundTransfer.getFtDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null,
						AD_BRNCH, AD_CMPNY);
        		
        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("CASH MANAGEMENT", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);
        		
        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
        		
        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("FUND TRANSFERS", AD_CMPNY);
        		glJournal.setGlJournalCategory(glJournalCategory);
        		
        		if (glJournalBatch != null) {
        			
        			glJournal.setGlJournalBatch(glJournalBatch);
        			
        		}           		    
        		
        		
        		// create journal lines
        		
        		j = cmDistributionRecords.iterator();
        		
        		while (j.hasNext()) {
        			
        			LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();
        			
        			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());
        			
        			double DR_AMNT = this.convertForeignToFunctionalCurrency(adBankAccount.getGlFunctionalCurrency().getFcCode(),
        					adBankAccount.getGlFunctionalCurrency().getFcName(), 
							cmFundTransfer.getFtConversionDate(),
							cmFundTransfer.getFtConversionRateFrom(),
							cmDistributionRecord.getDrAmount(), AD_CMPNY);
        			
        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
        					cmDistributionRecord.getDrLine(),	            			
							cmDistributionRecord.getDrDebit(),
							DR_AMNT, "", AD_CMPNY);
        			
        			cmDistributionRecord.getGlChartOfAccount().addGlJournalLine(glJournalLine);
        			
        			glJournal.addGlJournalLine(glJournalLine);
        			
        			cmDistributionRecord.setDrImported(EJBCommon.TRUE);
        			
            	    LocalAdBankAccount adBankAccountTo = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountTo());
        			
    		  	  	// for FOREX revaluation
    		  	  	if(((adBankAccount.getGlFunctionalCurrency().getFcCode() !=
		  	  					adCompany.getGlFunctionalCurrency().getFcCode()) &&
		  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
		  	  				(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
		  	  						adBankAccount.getGlFunctionalCurrency().getFcCode()))) || 
		  	  			((adBankAccountTo.getGlFunctionalCurrency().getFcCode() !=
		  	  					adCompany.getGlFunctionalCurrency().getFcCode()) &&
		  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
		  	  				(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
		  	  				adBankAccountTo.getGlFunctionalCurrency().getFcCode())))){
    		  	  		
    		  	  		double CONVERSION_RATE = 1;
    		  	  		
    		  	  		if (glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
	  	  					adBankAccount.getGlFunctionalCurrency().getFcCode()) && 
							cmFundTransfer.getFtConversionRateFrom() != 0 && cmFundTransfer.getFtConversionRateFrom() != 1) {
    		  	  			
    		  	  			CONVERSION_RATE = cmFundTransfer.getFtConversionRateFrom();
    		  	  		
    		  	  		} else if (glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
    	  	  				adBankAccount.getGlFunctionalCurrency().getFcCode()) && 
							cmFundTransfer.getFtConversionRateTo() != 0 && cmFundTransfer.getFtConversionRateTo() != 1) {
	    		  	  			
	    		  	  		CONVERSION_RATE = cmFundTransfer.getFtConversionRateTo();
    		  	  			
    		  	  		} else if (cmFundTransfer.getFtConversionDate() != null){
    		  	  			
    		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
    		  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
    							glJournal.getJrConversionDate(), AD_CMPNY);
    		  	  			
    		  	  		}
    		  	  		
    		  	  		Collection glForexLedgers = null;
    		  	  		
    		  	  		try {
    		  	  			
    		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
    		  	  				cmFundTransfer.getFtDate(), glJournalLine.getGlChartOfAccount().getCoaCode(), AD_CMPNY);
    		  	  			
    		  	  		} catch(FinderException ex) {
    		  	  			
    		  	  		}
    		  	  		
    		  	  		LocalGlForexLedger glForexLedger =
    		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
    		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();
    		  	  		
    		  	  		int FRL_LN = (glForexLedger != null &&
    		  	  				glForexLedger.getFrlDate().compareTo(cmFundTransfer.getFtDate()) == 0) ?
    		  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;
    		  	  		
    		  	  		// compute balance
    		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
    		  	  		double FRL_AMNT = cmDistributionRecord.getDrAmount();
    		  	  		
    		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));  
    		  	  		else
    		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);
    		  	  		
    		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
    		  	  		
    		  	  		glForexLedger = glForexLedgerHome.create(cmFundTransfer.getFtDate(), new Integer (FRL_LN),
    		  	  			"OTH", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);
    		  	  		
    		  	  		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
    		  	  		
    		  	  		// propagate balances
    		  	  		try{
    		  	  			
    		  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
    		  	  				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
								glForexLedger.getFrlAdCompany());
    		  	  			
    		  	  		} catch (FinderException ex) {
    		  	  			
    		  	  		}
    		  	  		
    		  	  		Iterator itrFrl = glForexLedgers.iterator();
    		  	  		
    		  	  		while (itrFrl.hasNext()) {
    		  	  			
    		  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
    		  	  			FRL_AMNT = cmDistributionRecord.getDrAmount();
    		  	  			
    		  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT :
    		  	  					(- 1 * FRL_AMNT));
    		  	  			else
    		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) :
    		  	  					FRL_AMNT);
    		  	  			
    		  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);
    		  	  			
    		  	  		}
    		  	  		
    		  	  	}        			

        		}
        		
        		if (glOffsetJournalLine != null) {
        			
        			glJournal.addGlJournalLine(glOffsetJournalLine);
        			
        		}		
        		
        		// post journal to gl
        		
        		Collection glJournalLines = glJournal.getGlJournalLines();
        		
        		Iterator i = glJournalLines.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
        			
        			// post current to current acv
        			
        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);
        			
        			
        			// post to subsequent acvs (propagate)
        			
        			Collection glSubsequentAccountingCalendarValues = 
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
        			
        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
        			
        			while (acvsIter.hasNext()) {
        				
        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        					(LocalGlAccountingCalendarValue)acvsIter.next();
        				
        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);
        				
        			}
        			
        			// post to subsequent years if necessary
        			
        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
        			
        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
        				
        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
        				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
        				
        				Iterator sobIter = glSubsequentSetOfBooks.iterator();
        				
        				while (sobIter.hasNext()) {
        					
        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
        					
        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
        					
        					// post to subsequent acvs of subsequent set of book(propagate)
        					
        					Collection glAccountingCalendarValues = 
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
        					
        					Iterator acvIter = glAccountingCalendarValues.iterator();
        					
        					while (acvIter.hasNext()) {
        						
        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        							(LocalGlAccountingCalendarValue)acvIter.next();
        						
        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);
        							
        						} else { // revenue & expense
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_BRNCH, AD_CMPNY);					        
        							
        						}
        						
        					}
        					
        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
        					
        				}
        				
        			}
        			
        		}
        		
        		Collection cmFundTransferReceipts = cmFundTransfer.getCmFundTransferReceipts();
        		
        		i = cmFundTransferReceipts.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalCmFundTransferReceipt cmFundTransferReceipt = (LocalCmFundTransferReceipt) i.next();
        			
        			LocalArReceipt arReceipt = cmFundTransferReceipt.getArReceipt();
        			
        			arReceipt.setRctLock(EJBCommon.FALSE);
        		}
        		
        	}
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyVoidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }


   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("CmFundTransferPostControllerBean getGlFcPrecisionUnit");

       	
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
     private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("CmFundTransferPostControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
	
   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
      LocalGlChartOfAccount glChartOfAccount, 
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_BRNCH, Integer AD_CMPNY) {
      	
      Debug.print("CmFundTransferPostControllerBean postToGl");
      
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
              
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {          
               
               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
       	   
	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance = 
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
	               	  
	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
	           
	           
	               	  
	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {				    
				    			
 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
				    
		
			  } else {
	
				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
	
		 	  }
		 	  
		 	  if (isCurrentAcv) { 
		 	 
			 	 if (isDebit == EJBCommon.TRUE) {
			 	 	
		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 				
		 		 } else {
		 		 	
		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 		 }       	   
		 		 
		 	}
       	
       } catch (Exception ex) {
       	
       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());
       	
       }
      
      
   }
    
	private double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY) 
	throws GlobalConversionDateNotExistException {
		
		Debug.print("CmFundTransferPostControllerBean getFrRateByFrNameAndFrDate");
		
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			
			double CONVERSION_RATE = 1;
			
			// Get functional currency rate
			
			if (!FC_NM.equals("USD")) {
				
				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
							CONVERSION_DATE, AD_CMPNY);
				
				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
				
			}
			
			// Get set of book functional currency rate if necessary
			
			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
				
				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
							CONVERSION_DATE, AD_CMPNY);
				
				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();
				
			}
			
			return CONVERSION_RATE;
			
		} catch (FinderException ex) {	
			
			getSessionContext().setRollbackOnly();
			throw new GlobalConversionDateNotExistException();  
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmFundTransferPostControllerBean ejbCreate");
      
    }
}
