
/*
 * CmGlJournalInterfaceControllerBean.java
 *
 * Created on December 19, 2003, 10:47 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalInterface;
import com.ejb.gl.LocalGlJournalInterfaceHome;
import com.ejb.gl.LocalGlJournalLineInterface;
import com.ejb.gl.LocalGlJournalLineInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmGlJournalInterfaceControllerEJB"
 *           display-name="Used for importing ap transactions to gl journal interface"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmGlJournalInterfaceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmGlJournalInterfaceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmGlJournalInterfaceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class CmGlJournalInterfaceControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public long executeCmGlJriImport(Date JRI_DT_FRM, Date JRI_DT_TO, Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("CmGlJournalInterfaceControllerBean executeCmGlJriImport");
                
        LocalCmFundTransferHome cmFundTransferHome = null;
        LocalCmAdjustmentHome cmAdjustmentHome = null;        
        LocalGlJournalInterfaceHome glJournalInterfaceHome = null;
        LocalGlJournalLineInterfaceHome glJournalLineInterfaceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalCmDistributionRecordHome cmDistributionRecordHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;              
        
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");               

        long IMPORTED_JOURNALS = 0L;
        short lineNumber = 0;  
               
        //initialized EJB Home
        
        try {
            
            cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
            cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);                
            glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);
            glJournalLineInterfaceHome = (LocalGlJournalLineInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalLineInterfaceHome.JNDI_NAME, LocalGlJournalLineInterfaceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
                            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
		}
        
        try {   
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            LocalGlFunctionalCurrency glFunctionalCurrency = adCompany.getGlFunctionalCurrency(); 

            // fund transfers    	
       		
      	    Collection cmFundTransfers = cmFundTransferHome.findPostedFtByFtDateRange(JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
      	    
        	Iterator i = cmFundTransfers.iterator();
        	
        	while (i.hasNext()) {
        		        		
        		LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();
				
				Collection cmFtDistributionRecords = cmDistributionRecordHome.findByDrReversalAndDrImportedAndFtCode(EJBCommon.FALSE, EJBCommon.FALSE, cmFundTransfer.getFtCode(), AD_CMPNY);        		
        		
        		if (!cmFtDistributionRecords.isEmpty()) {
        			
        			// create journal interface     
        			
	        		LocalGlJournalInterface glJournalInterface = glJournalInterfaceHome.create(
	        			cmFundTransfer.getFtReferenceNumber(),
	        			cmFundTransfer.getFtMemo(),
	        			cmFundTransfer.getFtDate(),
	        			"FUND TRANSFERS",
	        			"CASH MANAGEMENT",
	        			glFunctionalCurrency.getFcName(),
	        			null, cmFundTransfer.getFtDocumentNumber(), null, 1d, 'N', EJBCommon.FALSE, null, null, null, AD_BRNCH, AD_CMPNY);       			

	        	 	IMPORTED_JOURNALS++;
	        	 	lineNumber = 0;
	        	 	
	        		// create journal lines for the fund transfer
	        		
	        		Iterator j = cmFtDistributionRecords.iterator();
	        		
	        		while (j.hasNext()) {
	        			
	        			LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());
	        			
	        			LocalCmDistributionRecord cmDistributionRecord  = (LocalCmDistributionRecord)j.next();     			       		
	        			       		
	        		    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber,
	        		    	cmDistributionRecord.getDrDebit(),
	        		    	this.convertForeignToFunctionalCurrency(adBankAccount.getGlFunctionalCurrency().getFcCode(),
	        		    	    adBankAccount.getGlFunctionalCurrency().getFcName(), 
	        		    	    cmFundTransfer.getFtConversionDate(), cmFundTransfer.getFtConversionRateFrom(), cmDistributionRecord.getDrAmount(), AD_CMPNY),
	        		    	cmDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    	
	        		    cmDistributionRecord.setDrImported(EJBCommon.TRUE);	        		    	
	        		    	
	        		}
	        		
	            }
	            
	        }
   
            // adjustments

        	Collection cmAdjustments = cmAdjustmentHome.findPostedAdjByAdjDateRange(JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
        	    
        	i = cmAdjustments.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();

         		Collection cmAdjDistributionRecords = cmDistributionRecordHome.findByDrReversalAndDrImportedAndAdjCode(EJBCommon.FALSE, EJBCommon.FALSE, cmAdjustment.getAdjCode(), AD_CMPNY);
        		
        		if (!cmAdjDistributionRecords.isEmpty()) {
      		       			        		        		
        		    // create journal interface	interest
        			
		        	LocalGlJournalInterface	glJournalInterface = glJournalInterfaceHome.create(
		        			cmAdjustment.getAdjReferenceNumber(),
		        			cmAdjustment.getAdjMemo(),
		        			cmAdjustment.getAdjDate(),
		        			"BANK ADJUSTMENTS",
		        			"CASH MANAGEMENT",
		        			glFunctionalCurrency.getFcName(),
		        			null, cmAdjustment.getAdjDocumentNumber(), null, 1d, 'N', EJBCommon.FALSE, null, null, null, AD_BRNCH, AD_CMPNY);       			
	        	 	
	        	 	IMPORTED_JOURNALS++;
	        	 	lineNumber = 0;
	        	 	
	        		// create journal lines for the adjusment interest
	        		
	        		Iterator j = cmAdjDistributionRecords.iterator();
	        		
	        		while (j.hasNext()) {        			
	        				        			
	        			LocalCmDistributionRecord cmDistributionRecord  = (LocalCmDistributionRecord)j.next();     			       		
       		
	        		    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber,
	        		    	cmDistributionRecord.getDrDebit(),
	        		    	this.convertForeignToFunctionalCurrency(cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
	        		    	    cmAdjustment.getAdBankAccount().getGlFunctionalCurrency().getFcName(), 
	        		    	    cmAdjustment.getAdjConversionDate(), cmAdjustment.getAdjConversionRate(), cmDistributionRecord.getDrAmount(), AD_CMPNY),
	        		    	cmDistributionRecord.getGlChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    
	        		    cmDistributionRecord.setDrImported(EJBCommon.TRUE);
	        		    
	            	}
        		    	
        		}
        		
            }
            
            return IMPORTED_JOURNALS; 
        	
        } catch (Exception ex) {
        	
        	ex.printStackTrace();
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
     
    }        
        

    // private methods
	
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("CmGlJournalInterfaceControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmGlJournalInterfaceControllerBean ejbCreate");
      
    }
}
