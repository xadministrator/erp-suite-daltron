
/*
 * CmJournalControllerBean.java
 *
 * Created on December 24, 2003, 10:31 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.CmModDistributionRecordDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmJournalControllerEJB"
 *           display-name="Used for editing ap journals"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmJournalControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmJournalController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmJournalControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class CmJournalControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getCmDrByFtCode(Integer FT_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("CmJournalControllerBean getCmDrByFtCode");
        
        LocalCmDistributionRecordHome cmDistributionRecordHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {

        	Collection cmDistributionRecords = cmDistributionRecordHome.findByFtCode(FT_CODE, AD_CMPNY);

        	short lineNumber = 1;
        	
        	Iterator i = cmDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalCmDistributionRecord cmDistributionRecord = 
        			(LocalCmDistributionRecord)i.next();
        		
        		CmModDistributionRecordDetails mdetails = new CmModDistributionRecordDetails();
        		
        		mdetails.setDrCode(cmDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(cmDistributionRecord.getDrClass());
        		mdetails.setDrAmount(cmDistributionRecord.getDrAmount());
        		mdetails.setDrDebit(cmDistributionRecord.getDrDebit());
        		mdetails.setDrReversal(cmDistributionRecord.getDrReversal());
        		mdetails.setDrCoaAccountNumber(cmDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
        		mdetails.setDrCoaAccountDescription(cmDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
        		
        		list.add(mdetails);
        		
        		lineNumber++;
        		
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
     
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getCmDrByAdjCode(Integer ADJ_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("CmJournalControllerBean getCmDrByAdjCode");
        
        LocalCmDistributionRecordHome cmDistributionRecordHome = null;
  
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {

        	Collection cmDistributionRecords = cmDistributionRecordHome.findByAdjCode(ADJ_CODE, AD_CMPNY);
        	
        	short lineNumber = 1;
        	
        	Iterator i = cmDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalCmDistributionRecord cmDistributionRecord = 
        			(LocalCmDistributionRecord)i.next();
        		
        		CmModDistributionRecordDetails mdetails = new CmModDistributionRecordDetails();
        		
        		mdetails.setDrCode(cmDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(cmDistributionRecord.getDrClass());
        		mdetails.setDrDebit(cmDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(cmDistributionRecord.getDrAmount());
        		mdetails.setDrReversal(cmDistributionRecord.getDrReversal());
        		mdetails.setDrCoaAccountNumber(cmDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
        		mdetails.setDrCoaAccountDescription(cmDistributionRecord.getGlChartOfAccount().getCoaAccountDescription());
        		
        		list.add(mdetails);
        		
        		lineNumber++;

        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
            
    } 
    
    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("CmJournalControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
        
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveCmDrEntry(Integer PRMRY_KEY, ArrayList drList, String transactionType, Integer AD_BRNCH, Integer AD_CMPNY) 
       throws GlobalAccountNumberInvalidException {
       
       Debug.print("CmJournalControllerBean saveCmDrEntry");

       LocalCmDistributionRecordHome cmDistributionRecordHome = null;
       LocalGlChartOfAccountHome glChartOfAccountHome = null;
       LocalCmAdjustmentHome cmAdjustmentHome = null;
       LocalCmFundTransferHome cmFundTransferHome = null;
       

       // Initialize EJB Home
      
       try {
            
       	   cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
               lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
           glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
           cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
           	   lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
           cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
       	   	   lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {
       
	       LocalCmAdjustment cmAdjustment = null;
	       LocalCmFundTransfer cmFundTransfer = null;
	       
	       Collection cmDistributionRecords = null;
	       Iterator i;
	       
	       if (transactionType.equals("BANK ADJUSTMENTS")) {
	       	
	       	cmAdjustment = cmAdjustmentHome.findByPrimaryKey(PRMRY_KEY);
	       		
	            // remove all distribution records
	       		
	       		cmDistributionRecords = cmAdjustment.getCmDistributionRecords();
	       		
	       	    i = cmDistributionRecords.iterator();     	  
	       	  
	       	    while (i.hasNext()) {
	       	  	
	       	   	    LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)i.next();
	       	  	   
	       	  	    i.remove();
	       	  	    
	       	  	    cmDistributionRecord.remove();
	       	  	
	       	    }
	       	           	    
	       	    // add new distribution records
	       	  
	       	    i = drList.iterator();
	       	  
	       	    while (i.hasNext()) {
	       	  	
	       	  	    CmModDistributionRecordDetails mDrDetails = (CmModDistributionRecordDetails) i.next();      	  	  
	       	  	          	  	  
	       	  	    this.addCmDrEntry(mDrDetails, cmAdjustment, null, AD_BRNCH, AD_CMPNY);
	       	  	
	       	    }
	       			       			       	
	       } else if (transactionType.equals("FUND TRANSFER")) {
	       	
	       	    cmFundTransfer = cmFundTransferHome.findByPrimaryKey(PRMRY_KEY);
	       	    
	            // remove all distribution records
	       	    
	       	    cmDistributionRecords = cmFundTransfer.getCmDistributionRecords();
	       	    
	       	    i = cmDistributionRecords.iterator();     	  
	       	  
	       	    while (i.hasNext()) {
	       	  	
	       	   	    LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)i.next();
	       	  	   
	       	  	    i.remove();
	       	  	    
	       	  	    cmDistributionRecord.remove();
	       	  	
	       	    }
	       	           	    
	       	    // add new distribution records
	       	  
	       	    i = drList.iterator();
	       	  
	       	    while (i.hasNext()) {
	       	  	
	       	  	    CmModDistributionRecordDetails mDrDetails = (CmModDistributionRecordDetails) i.next();      	  	  
	       	  	          	  	  
	       	  	    this.addCmDrEntry(mDrDetails, null, cmFundTransfer, AD_BRNCH, AD_CMPNY);
	       	  	
	       	    }
	       	
	       }
                   
       } catch (GlobalAccountNumberInvalidException ex) {
       	    
       	   getSessionContext().setRollbackOnly();
       	   throw ex;

       } catch (Exception ex) {
          
       	  Debug.printStackTrace(ex);
          getSessionContext().setRollbackOnly();
          throw new EJBException(ex.getMessage());
       }

       
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getAdPrfGlJournalLineNumber(Integer AD_CMPNY) {

        Debug.print("CmJournalControllerBean getAdPrfGlJournalLineNumber");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       
        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfGlJournalLineNumber();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }    

    // private methods
    
    private void addCmDrEntry(CmModDistributionRecordDetails mdetails, LocalCmAdjustment cmAdjustment, LocalCmFundTransfer cmFundTransfer, Integer AD_BRNCH, Integer AD_CMPNY) 
    	throws GlobalAccountNumberInvalidException {
		
    	Debug.print("CmJournalControllerBean addCmDrEntry");
	
    	LocalCmDistributionRecordHome cmDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
                   
	    // Initialize EJB Home
	    
	    try {
	        
	        cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
	            lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);            
	        glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
	         
	                    
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }            
	            
	    try {
	    	
	    	// get company
	    	
	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	    	
	    	// validate if coa exists
	    	
	    	LocalGlChartOfAccount glChartOfAccount = null;
	    	
	    	try {
	    	
	    		glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(mdetails.getDrCoaAccountNumber(), AD_BRNCH, AD_CMPNY);
	    		
	    		if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
	    		    throw new GlobalAccountNumberInvalidException(String.valueOf(mdetails.getDrLine()));
	    		
	    	} catch (FinderException ex) {
	    		
	    		throw new GlobalAccountNumberInvalidException(String.valueOf(mdetails.getDrLine()));
	    		
	    	}
	    	        			    
		    // create distribution record 
		    
		    LocalCmDistributionRecord cmDistributionRecord = cmDistributionRecordHome.create(
			    mdetails.getDrLine(), 
			    mdetails.getDrClass(), 
			    EJBCommon.roundIt(mdetails.getDrAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision()),			    
			    mdetails.getDrDebit(), EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
		    
		    glChartOfAccount.addCmDistributionRecord(cmDistributionRecord);
			    
		    if (cmAdjustment != null) {
		    	
		    	cmAdjustment.addCmDistributionRecord(cmDistributionRecord);
		    					
		    } else if (cmFundTransfer != null) {
		    	
		    	cmFundTransfer.addCmDistributionRecord(cmDistributionRecord);			
		    
		    }
		    		    
		} catch (GlobalAccountNumberInvalidException ex) {        	
	    	
	    	getSessionContext().setRollbackOnly();
	    	throw ex;
		    		            		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
	
    }      
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmJournalControllerBean ejbCreate");
      
    }      
}
