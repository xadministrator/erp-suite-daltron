
/*
 * CmReleasingChecksControllerBean.java
 *
 * Created on December 08, 2005, 04:43 PM
 *
 * @author  Farrah S. Garing
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckBatch;
import com.ejb.ap.LocalApCheckBatchHome;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierBalance;
import com.ejb.ap.LocalApSupplierBalanceHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.exception.ApCHKCheckNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItemLocation;
import com.util.AbstractSessionBean;
import com.util.ApModCheckDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmReleasingChecksControllerEJB"
 *           display-name="Used for posting checks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmReleasingChecksControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmReleasingChecksController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmReleasingChecksControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
 */

public class CmReleasingChecksControllerBean extends AbstractSessionBean {
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("CmReleasingChecksControllerBean getApSplAll");
		
		LocalApSupplierHome apSupplierHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);
			
			Iterator i = apSuppliers.iterator();
			
			while (i.hasNext()) {
				
				LocalApSupplier apSupplier = (LocalApSupplier)i.next();
				
				list.add(apSupplier.getSplSupplierCode());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	} 
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("CmReleasingChecksControllerBean getAdBaAll");
		
		LocalAdBankAccountHome adBankAccountHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);
			
			Iterator i = adBankAccounts.iterator();
			
			while (i.hasNext()) {
				
				LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
				
				list.add(adBankAccount.getBaName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlFcAll(Integer AD_CMPNY) {
		
		Debug.print("CmReleasingChecksControllerBean getGlFcAll");
		
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);
			
			Iterator i = glFunctionalCurrencies.iterator();
			
			while (i.hasNext()) {
				
				LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
				
				list.add(glFunctionalCurrency.getFcName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApOpenCbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("CmReleasingChecksControllerBean getApOpenCbAll");
		
		LocalApCheckBatchHome apCheckBatchHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			apCheckBatchHome = (LocalApCheckBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckBatchHome.JNDI_NAME, LocalApCheckBatchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection apCheckBatches = apCheckBatchHome.findOpenCbAll(AD_BRNCH, AD_CMPNY);
			
			Iterator i = apCheckBatches.iterator();
			
			while (i.hasNext()) {
				
				LocalApCheckBatch apCheckBatch = (LocalApCheckBatch)i.next();
				
				list.add(apCheckBatch.getCbName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}  
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfEnableApCheckBatch(Integer AD_CMPNY) {
		
		Debug.print("CmReleasingChecksControllerBean getAdPrfEnableApCheckBatch");
		
		LocalAdPreferenceHome adPreferenceHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfEnableApCheckBatch();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getCmChkReleasableByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {
		
		Debug.print("CmReleasingChecksControllerBean getCmChkReleasableByCriteria");
		
		LocalApCheckHome apCheckHome = null;
		
		// Initialize EJB Home
		
		try {
			
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);          
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			ArrayList chkList = new ArrayList();
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(chk) FROM ApCheck chk ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size() + 2;

			Object obj[] = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				firstArgument = false;
				
				jbossQl.append("WHERE chk.adBankAccount.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
				
			} 
			
			if (criteria.containsKey("currency")) {
				
				firstArgument = false;
				
				jbossQl.append("WHERE chk.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("currency");
				ctr++;
				
			}
			
			if (criteria.containsKey("batchName")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("chk.apCheckBatch.cbName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("batchName");
				ctr++;
			}
			
			if (criteria.containsKey("supplierCode")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("chk.apSupplier.splSupplierCode=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("supplierCode");
				ctr++;
			}      
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("chk.chkCheckDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("chk.chkCheckDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}    
			
			if (criteria.containsKey("checkNumberFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("chk.chkNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("checkNumberFrom");
				ctr++;
				
			}  
			
			if (criteria.containsKey("checkNumberTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("chk.chkNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("checkNumberTo");
				ctr++;
				
			} 	      
			
			if (criteria.containsKey("documentNumberFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("chk.chkDocumentNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberFrom");
				ctr++;
				
			}  
			
			if (criteria.containsKey("documentNumberTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("chk.chkDocumentNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberTo");
				ctr++;
				
			} 	    

			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("chk.chkAdBranch=" + AD_BRNCH + " ");
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("chk.chkReleased = 0 AND chk.chkPosted = 1 AND chk.chkAdCompany=" + AD_CMPNY + " ");	      
			
			String orderBy = null;
			
			if (ORDER_BY.equals("BANK ACCOUNT")) {
				
				orderBy = "chk.adBankAccount.baName";
				
			} else if (ORDER_BY.equals("SUPPLIER CODE")) {
				
				orderBy = "chk.apSupplier.splSupplierCode";
				
			} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
				
				orderBy = "chk.chkDocumentNumber";
				
			} else if (ORDER_BY.equals("CHECK NUMBER")) {
				
				orderBy = "chk.chkNumber";
				
			}
			
			
			if (orderBy != null) {
				
				jbossQl.append("ORDER BY " + orderBy + ", chk.chkDate");
				
			}  else {
				
				jbossQl.append("ORDER BY chk.chkDate");
				
			} 
			
			
			jbossQl.append(" OFFSET ?" + (ctr + 1));
			obj[ctr] = OFFSET;
			ctr++;

			jbossQl.append(" LIMIT ?" + (ctr + 1));
			obj[ctr] = LIMIT;
			
			System.out.println("QL + " + jbossQl);
			
			Collection apChecks = null;

			try {
				
				apChecks = apCheckHome.getChkByCriteria(jbossQl.toString(), obj);
				
			} catch (Exception ex) {
				
				throw new EJBException(ex.getMessage());
				
			}
			
			if (apChecks.isEmpty())
				throw new GlobalNoRecordFoundException();
			
			Iterator i = apChecks.iterator();
			while (i.hasNext()) {
				
				LocalApCheck apCheck = (LocalApCheck) i.next();
				
				ApModCheckDetails mdetails = new ApModCheckDetails();
				mdetails.setChkCode(apCheck.getChkCode());
				mdetails.setChkDate(apCheck.getChkDate());
				mdetails.setChkNumber(apCheck.getChkNumber());
				mdetails.setChkDocumentNumber(apCheck.getChkDocumentNumber());
				mdetails.setChkAmount(apCheck.getChkAmount());
				mdetails.setChkVoid(apCheck.getChkVoid());
				mdetails.setChkSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode() + "-" + apCheck.getApSupplier().getSplName());
				mdetails.setChkBaName(apCheck.getAdBankAccount().getBaName());
				mdetails.setChkCheckDate(apCheck.getChkCheckDate());
				
				if(!apCheck.getApVoucherLineItems().isEmpty()) {
					
					mdetails.setChkType("ITEMS");
					
				} else {
					
					mdetails.setChkType("EXPENSES");
					
				} 
				
				
				chkList.add(mdetails);
				
			}
			
			return chkList;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void executeCmReleasingChecks(Integer CHK_CODE, String CHK_NMBR, Date CHK_CHCK_DT, Date CHK_DT_RLSD, String CHK_RMRKS, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	ApCHKCheckNumberNotUniqueException {
		
		Debug.print("CmReleasingChecksControllerBean executeCmReleasingChecks");
		
		LocalApCheckHome apCheckHome = null;        
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGlJournalHome glJournalHome = null;
		LocalGlJournalBatchHome glJournalBatchHome = null;
		LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalGlJournalSourceHome glJournalSourceHome = null;
		LocalGlJournalCategoryHome glJournalCategoryHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		
		LocalApCheck apCheck = null;         
		
		// Initialize EJB Home
		
		try {
			
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
			glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
			glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
			glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);  
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}       
		
		try {
			
			// validate if check is already deleted
			
			try 
			{
				
				apCheck = apCheckHome.findByPrimaryKey(CHK_CODE);
				
			} catch (FinderException ex) {
				
				throw new GlobalRecordAlreadyDeletedException();
				
			}
			
			// validate if check is already posted
			
			if (apCheck.getChkReleased() == EJBCommon.TRUE) {
				
				//throw new GlobalTransactionAlreadyPostedException();
				
			}
			
			
			//validate if check number is unique
			
			LocalApCheck apExistingCheck = null;
			
			try 
			{
				apExistingCheck = apCheckHome.findByChkNumberAndBaName(CHK_NMBR, apCheck.getAdBankAccount().getBaName(), AD_CMPNY);
			} 
			catch (FinderException ex) 
			{	
			}
			
			System.out.println("CHK_NMBR					  : " + CHK_NMBR);

			if (apExistingCheck != null && !apCheck.getChkNumber().equals(CHK_NMBR))
			{
				System.out.println("apExistingCheck.getChkNumber(): " + apExistingCheck.getChkNumber());
				System.out.println("CHK_NMBR					  : " + CHK_NMBR);
				throw new ApCHKCheckNumberNotUniqueException();
			}

			apCheck.setChkNumber(CHK_NMBR);
			apCheck.setChkCheckDate(CHK_CHCK_DT);
			apCheck.setChkReleased(EJBCommon.TRUE);
			apCheck.setChkDateReleased (CHK_DT_RLSD);
			
			System.out.println("CHK_RMRKS : " + CHK_RMRKS);
			System.out.println("apCheck.getChkMemo() : " + apCheck.getChkMemo());
			apCheck.setChkMemo(apCheck.getChkMemo() + "\n" + CHK_RMRKS);
			
			System.out.println("AFTER : apCheck.getChkMemo() : " + apCheck.getChkMemo());
			
			
		} catch (GlobalRecordAlreadyDeletedException ex) {
			     
			getSessionContext().setRollbackOnly();
			throw ex;        	        	
			
			//        } catch (GlobalTransactionAlreadyPostedException ex) {
			//        	
			//        	getSessionContext().setRollbackOnly();
			//        	throw ex;
			
			
		} catch (ApCHKCheckNumberNotUniqueException ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("CmReleasingChecksControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {
		
		Debug.print("CmReleasingChecksControllerBean getAdPrfApUseSupplierPulldown");
		
		LocalAdPreferenceHome adPreferenceHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfApUseSupplierPulldown();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// private methods
	
	private void post(Date CHK_DT, double CHK_AMNT, LocalApSupplier apSupplier, Integer AD_CMPNY) {
		
		Debug.print("CmReleasingChecksControllerBean post");
		
		LocalApSupplierBalanceHome apSupplierBalanceHome = null;
		
		// Initialize EJB Home
		
		try {
			
			apSupplierBalanceHome = (LocalApSupplierBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierBalanceHome.JNDI_NAME, LocalApSupplierBalanceHome.class);              
			
		} catch (NamingException ex) {
			
			getSessionContext().setRollbackOnly();            
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			// find supplier balance before or equal voucher date
			
			Collection apSupplierBalances = apSupplierBalanceHome.findByBeforeOrEqualVouDateAndSplSupplierCode(CHK_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);
			
			if (!apSupplierBalances.isEmpty()) {
				
				// get last voucher
				
				ArrayList apSupplierBalanceList = new ArrayList(apSupplierBalances);
				
				LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)apSupplierBalanceList.get(apSupplierBalanceList.size() - 1);
				
				if (apSupplierBalance.getSbDate().before(CHK_DT)) {
					
					// create new balance
					
					LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
							CHK_DT, apSupplierBalance.getSbBalance() + CHK_AMNT, AD_CMPNY);
					
					apSupplier.addApSupplierBalance(apNewSupplierBalance);
					
				} else { // equals to voucher date
					
					apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + CHK_AMNT);
					
				} 
				
			} else {        	
				
				// create new balance
				
				LocalApSupplierBalance apNewSupplierBalance = apSupplierBalanceHome.create(
						CHK_DT, CHK_AMNT, AD_CMPNY);
				
				apSupplier.addApSupplierBalance(apNewSupplierBalance);
				
			}
			
			// propagate to subsequent balances if necessary
			
			apSupplierBalances = apSupplierBalanceHome.findByAfterVouDateAndSplSupplierCode(CHK_DT, apSupplier.getSplSupplierCode(), AD_CMPNY);
			
			Iterator i = apSupplierBalances.iterator();
			
			while (i.hasNext()) {
				
				LocalApSupplierBalance apSupplierBalance = (LocalApSupplierBalance)i.next();
				
				apSupplierBalance.setSbBalance(apSupplierBalance.getSbBalance() + CHK_AMNT);
				
			}
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();            
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
		
		Debug.print("CmReleasingChecksControllerBean convertForeignToFunctionalCurrency");
		
		
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		LocalAdCompany adCompany = null;
		
		// Initialize EJB Homes
		
		try {
			
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		// get company and extended precision
		
		try {
			
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}	     
		
		
		// Convert to functional currency if necessary
		
		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
			
			AMOUNT = AMOUNT * CONVERSION_RATE;
			
		}
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
		
	}
	
	private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
			LocalGlChartOfAccount glChartOfAccount, 
			boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
		
		Debug.print("ApVoucherPostControllerBean postToGl");
		
		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
			
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {          
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
			
			LocalGlChartOfAccountBalance glChartOfAccountBalance = 
				glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
						glAccountingCalendarValue.getAcvCode(),
						glChartOfAccount.getCoaCode(), AD_CMPNY);
			
			String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
			short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
			
			
			
			if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
					isDebit == EJBCommon.TRUE) ||
					(!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
							isDebit == EJBCommon.FALSE)) {				    
				
				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				
				if (!isCurrentAcv) {
					
					glChartOfAccountBalance.setCoabBeginningBalance(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
					
				}
				
				
			} else {
				
				glChartOfAccountBalance.setCoabEndingBalance(
						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
				
				if (!isCurrentAcv) {
					
					glChartOfAccountBalance.setCoabBeginningBalance(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
					
				}
				
			}
			
			if (isCurrentAcv) { 
				
				if (isDebit == EJBCommon.TRUE) {
					
					glChartOfAccountBalance.setCoabTotalDebit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
					
				} else {
					
					glChartOfAccountBalance.setCoabTotalCredit(
							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
				}       	   
				
			}
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
		
	}
	
	private void postToInv(LocalApVoucherLineItem apVoucherLineItem, Date CST_DT, double CST_QTY_RCVD, double CST_ITM_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("CmReleasingChecksControllerBean postToInv");
		
		LocalInvCostingHome invCostingHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalInvItemLocation invItemLocation = apVoucherLineItem.getInvItemLocation();
			int CST_LN_NMBR = 0;
			
			CST_QTY_RCVD = EJBCommon.roundIt(CST_QTY_RCVD, adPreference.getPrfInvQuantityPrecisionUnit());
			CST_ITM_CST = EJBCommon.roundIt(CST_ITM_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
			CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
			CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
			
			// create costing
			
			try {
				
				// generate line number
				
				LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
				CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
				
			} catch (FinderException ex) {
				
				CST_LN_NMBR = 1;
				
			}
			
			LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, CST_QTY_RCVD, CST_ITM_CST, 0d, 0d, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_QTY_RCVD > 0 ? CST_QTY_RCVD : 0, AD_BRNCH, AD_CMPNY);
			invItemLocation.addInvCosting(invCosting);
			invCosting.setApVoucherLineItem(apVoucherLineItem);
			
			// propagate balance if necessary           
			
			Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
			
			Iterator i = invCostings.iterator();
			
			while (i.hasNext()) {
				
				LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
				
				invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_QTY_RCVD);
				invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ITM_CST);
				
			}                           
			
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("CmReleasingChecksControllerBean ejbCreate");
		
	}
	
	// private methods
	
	
}
