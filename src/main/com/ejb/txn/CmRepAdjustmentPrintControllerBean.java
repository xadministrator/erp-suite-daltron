package com.ejb.txn;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArModReceiptDetails;
import com.util.CmModAdjustmentDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmRepAdjustmentPrintControllerEJB"
 *           display-name="Used for printing adjustment transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmRepAdjustmentPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmRepAdjustmentPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmRepAdjustmentPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class CmRepAdjustmentPrintControllerBean  extends AbstractSessionBean{

	
	 /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeCmRepAdjustmentPrint(ArrayList adjCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
    	
    	 Debug.print("CmRepAdjustmentPrintControllerBean executeCmRepAdjustmentPrint");
    	 
    	 
    	 LocalCmAdjustmentHome cmAdjustmentHome = null;    
         LocalAdApprovalHome adApprovalHome = null;
         LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
         LocalArDistributionRecordHome arDistributionRecordHome = null;
    	 
         ArrayList list = new ArrayList();
         
         // Initialize EJB Home
         
         try {
             
        	 cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                 lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
             adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
             adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
             arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
         		lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
             
         } catch (NamingException ex) {
             
             throw new EJBException(ex.getMessage());
             
         }   
    	try{
    		System.out.println(adjCodeList.size() + " --" );
    		Iterator i = adjCodeList.iterator();

        	while (i.hasNext()) {
        	
        		Integer ADJ_CODE = (Integer) i.next();

        		LocalCmAdjustment cmAdjustment = null;

        		try {

        			cmAdjustment = cmAdjustmentHome.findByPrimaryKey(ADJ_CODE);

        		} catch (FinderException ex) {

        			continue;

        		}
        		
				System.out.println("pass 1");
        		
        		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
				LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("CM ADJUSTMENT", AD_CMPNY);	       		

				if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {

					if (cmAdjustment.getAdjApprovalStatus() == null || 
							cmAdjustment.getAdjApprovalStatus().equals("PENDING")) {

						continue;

					} 

				} else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {

					if (cmAdjustment.getAdjApprovalStatus() != null && 
							(cmAdjustment.getAdjApprovalStatus().equals("N/A") || 
									cmAdjustment.getAdjApprovalStatus().equals("APPROVED"))) {

						continue;

					}

				}
				System.out.println("pass 2");
				if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE && 
						cmAdjustment.getAdjPrinted() == EJBCommon.TRUE){

					continue;	

				}

				// show duplicate

				boolean showDuplicate = false;

				if (adApprovalDocument.getAdcTrackDuplicate() == EJBCommon.TRUE &&
						cmAdjustment.getAdjPrinted() == EJBCommon.TRUE) {

					showDuplicate = true;

				}

				// set printed

				cmAdjustment.setAdjPrinted(EJBCommon.TRUE);

				System.out.println("pass final " +  cmAdjustment.getAdjType());

				CmModAdjustmentDetails mdetails = new CmModAdjustmentDetails();

			
				System.out.println("pass final 1");
				mdetails.setAdjType(cmAdjustment.getAdjType());
				if(cmAdjustment.getArCustomer()!=null){
					mdetails.setAdjCustomerCode(cmAdjustment.getArCustomer().getCstCustomerCode());
					mdetails.setAdjCustomerName(cmAdjustment.getArCustomer().getCstName());
				}
				System.out.println("pass final 2");
				mdetails.setAdjDate(cmAdjustment.getAdjDate());
				mdetails.setAdjDocumentNumber(cmAdjustment.getAdjDocumentNumber());
				
				System.out.println("pass final 3");
				mdetails.setAdjBaName(cmAdjustment.getAdBankAccount().getAdBank().getBnkName());
				System.out.println("pass final 4");
				mdetails.setAdjReferenceNumber(cmAdjustment.getAdjReferenceNumber());
				mdetails.setAdjAmount(cmAdjustment.getAdjAmount());
				mdetails.setAdjAmountApplied(cmAdjustment.getAdjAmountApplied());
				mdetails.setAdjMemo(cmAdjustment.getAdjMemo());
			
        		list.add(mdetails);
        	}
        	
        	if (list.isEmpty()) {

        		throw new GlobalNoRecordFoundException();

        	}      	
        	
        	return list; 
	    } catch (GlobalNoRecordFoundException ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	throw ex;
	    	
	    	        	
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	throw new EJBException(ex.getMessage());
	    	
	    }
         
         
    }
	
	
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("ArRepReceiptPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}  
	
	
	
	// private methods
	
	private short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmRepAdjustmentPrintBean ejbCreate");
      
    }
}
