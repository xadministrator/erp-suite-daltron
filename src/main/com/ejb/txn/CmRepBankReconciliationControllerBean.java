package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.CmRepBankReconciliationDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmRepBankReconciliationControllerEJB"
 *           display-name="Used for viewing bank reconciliation report"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmRepBankReconciliationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmRepBankReconciliationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmRepBankReconciliationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 * 
*/

public class CmRepBankReconciliationControllerBean extends AbstractSessionBean {


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdBaAll(Integer AD_CMPNY) {
		
		Debug.print("CmRepBankReconciliationControllerBean getAdBaAll");
		
		LocalAdBankAccountHome adBankAccountHome = null;
		
		Collection adBankAccounts = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adBankAccounts = adBankAccountHome.findEnabledAndNotCashAccountBaAll(AD_CMPNY);
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (adBankAccounts.isEmpty()) {
			
			return null;
			
		}
		
		Iterator i = adBankAccounts.iterator();
		
		while (i.hasNext()) {
			
			LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
			
			list.add(adBankAccount.getBaName());
			
		}
		
		return list;
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public CmRepBankReconciliationDetails executeCmRepBankReconciliation(String BA_NM, Date DT_FRM, Date DT_TO, Integer AD_BRNCH, Integer AD_CMPNY,String isDraftBCBD) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("CmRepBankReconciliationControllerBean executeCmRepBankReconciliation");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
        
        LocalArReceiptHome arReceiptHome = null;
        LocalApCheckHome apCheckHome = null;
		LocalCmAdjustmentHome cmAdjustmentHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;

		CmRepBankReconciliationDetails mdetails = new CmRepBankReconciliationDetails();

		//initialized EJB Home
        
		try {

			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
			lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);

		} catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try {
			
			double BR_BGNNNG_BLNC_BK = 0d;
			double BR_BGNNNG_BLNC_BNK = 0d;
			double BR_DPST_AMNT_BK = 0d;
			double BR_DPST_AMNT_BNK = 0d;
			double BR_DSBRSMNT_AMNT_BK = 0d;
			double BR_DSBRSMNT_AMNT_BNK = 0d;
			double BR_DPST_IN_TRNST_AMNT = 0d;
			double BR_OUTSTNDNG_CHCK_AMNT = 0d;
			double BR_BNK_CRDT_AMNT=0d;
			double BR_BNK_DBT_AMNT=0d;
			double receipt = 0d;
			double cmAdjustmentDebitMemo = 0d;
			double fundTransferFrom = 0d;
			
			
			LocalAdBankAccount adBankAccount = null;
			
			try {
				
				adBankAccount = adBankAccountHome.findByBaNameAndBrCode(BA_NM, AD_BRNCH, AD_CMPNY);
				
				mdetails.setBrBankAccount(adBankAccount.getBaName());
				mdetails.setBrDateFrom(EJBCommon.convertSQLDateToString(DT_FRM));
				mdetails.setBrDateTo(EJBCommon.convertSQLDateToString(DT_TO));
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			
			// BOOK
			// get beginning balance
			
			Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeDateAndBaCodeAndType(DT_FRM, adBankAccount.getBaCode(), "BOOK", AD_CMPNY);

			if (!adBankAccountBalances.isEmpty()) {
				
				ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
				LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
				BR_BGNNNG_BLNC_BK = adBankAccountBalance.getBabBalance();
				
			}
			
//-------------------------------------------------------------------------------------
// AR / BR_DPST_AMNT_BK
			
			Collection arReceipts = arReceiptHome.findPostedRctByBaNameAndRctDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			Iterator i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_AMNT_BK += arReceipt.getRctAmountCash();				

			}
			
			arReceipts = arReceiptHome.findPostedCard1RctByBaNameAndRctDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_AMNT_BK += arReceipt.getRctAmountCard1();				

			}
			
			arReceipts = arReceiptHome.findPostedCard2RctByBaNameAndRctDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_AMNT_BK += arReceipt.getRctAmountCard2();				

			}
			
			arReceipts = arReceiptHome.findPostedCard3RctByBaNameAndRctDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_AMNT_BK += arReceipt.getRctAmountCard3();				

			}
			
			arReceipts = arReceiptHome.findPostedChequeRctByBaNameAndRctDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_AMNT_BK += arReceipt.getRctAmountCheque();				

			}

			Collection cmAdjustments = cmAdjustmentHome.findPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
					BA_NM, DT_FRM, DT_TO, "DEBIT MEMO", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_DPST_AMNT_BK += cmAdjustment.getAdjAmount();
				
			}

			cmAdjustments = cmAdjustmentHome.findPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
					BA_NM, DT_FRM, DT_TO, "ADVANCE", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_DPST_AMNT_BK += cmAdjustment.getAdjAmount();
				
			}
			
			Collection cmFundTransfers = cmFundTransferHome.findPostedFtAccountToByBaCodeAndDateRangeAndBrCode(
					adBankAccount.getBaCode(), DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = cmFundTransfers.iterator();
			
			while (i.hasNext()) {
				
				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();
				BR_DPST_AMNT_BK += cmFundTransfer.getFtAmount();
				
			}
			
			
			if(isDraftBCBD.equals("Yes")){
				cmAdjustments = cmAdjustmentHome.findDraftAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
						BA_NM, DT_FRM, DT_TO, "INTEREST", AD_BRNCH, AD_CMPNY);
				i = cmAdjustments.iterator();
				
				while (i.hasNext()) {

					LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();

					BR_DPST_AMNT_BK += cmAdjustment.getAdjAmount();

				}	
			}
			cmAdjustments = cmAdjustmentHome.findPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
					BA_NM, DT_FRM, DT_TO, "INTEREST", AD_BRNCH, AD_CMPNY);
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {

				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();

				BR_DPST_AMNT_BK += cmAdjustment.getAdjAmount();

			}
		
//-------------------------------------------------------------------------------------
// AP / BR_DSBRSMNT_AMNT_BK			
			Collection apChecks = apCheckHome.findPostedChkByBaNameAndChkDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = apChecks.iterator();
			
			while (i.hasNext()) {
				
				LocalApCheck apCheck = (LocalApCheck)i.next();
				BR_DSBRSMNT_AMNT_BK += apCheck.getChkAmount();
				
			}
			

			cmAdjustments = cmAdjustmentHome.findPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
					BA_NM, DT_FRM, DT_TO, "CREDIT MEMO", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_DSBRSMNT_AMNT_BK += cmAdjustment.getAdjAmount();
			
			}

			cmFundTransfers = cmFundTransferHome.findPostedFtAccountFromByBaCodeAndDateRangeAndBrCode(
					adBankAccount.getBaCode(), DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = cmFundTransfers.iterator();
			
			while (i.hasNext()) {
				
				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();
				BR_DSBRSMNT_AMNT_BK += cmFundTransfer.getFtAmount();
				
			}
			
			
			if(isDraftBCBD.equals("Yes")){
				cmAdjustments = cmAdjustmentHome.findDraftAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
						BA_NM, DT_FRM, DT_TO, "BANK CHARGE", AD_BRNCH, AD_CMPNY);
				
				i = cmAdjustments.iterator();
				
				while (i.hasNext()) {
					
					LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
					//BR_DSBRSMNT_AMNT_BK += cmAdjustment.getAdjAmount();
					BR_DSBRSMNT_AMNT_BK += cmAdjustment.getAdjAmount();
				
				}
			}
			
			cmAdjustments = cmAdjustmentHome.findPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
					BA_NM, DT_FRM, DT_TO, "BANK CHARGE", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				//BR_DSBRSMNT_AMNT_BK += cmAdjustment.getAdjAmount();
				BR_DSBRSMNT_AMNT_BK += cmAdjustment.getAdjAmount();
			
			}
			
			
			
		
			// BANK RECONCILED
			// get beginning balance 
			
			adBankAccountBalances = adBankAccountBalanceHome.findByBeforeDateAndBaCodeAndType(DT_FRM, adBankAccount.getBaCode(), "RECON", AD_CMPNY);
			System.out.println("DT_FRM: "+ DT_FRM);
			System.out.println("adBankAccount.getBaCode(): "+ adBankAccount.getBaCode());
			System.out.println("AD_CMPNY" + AD_CMPNY);
			System.out.println("adBankAccountBalancesSize: " +adBankAccountBalances.size());
			if (!adBankAccountBalances.isEmpty()) {

				ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
				LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);
				BR_BGNNNG_BLNC_BNK = adBankAccountBalance.getBabBalance();
				System.out.print("adBankAccountBalance.getBabBalance(): "+adBankAccountBalance.getBabBalance());
			}
			
			
//-------------------------------------------------------------------------------------
// AR
// RECONCILED TRANSACTIONS / BR_DPST_AMNT_BNK
// DEPOSIT(RECEIPT) / DEBIT MEMO / ADVANCE / INTEREST / FUND TRANSFER IN
			
			arReceipts = arReceiptHome.findReconciledPostedRctByBaNameAndRctDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_AMNT_BNK += arReceipt.getRctAmountCash();				

			}
			
			arReceipts = arReceiptHome.findReconciledPostedCard1RctByBaNameAndRctDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_AMNT_BNK += arReceipt.getRctAmountCard1();				

			}
			
			arReceipts = arReceiptHome.findReconciledPostedCard2RctByBaNameAndRctDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_AMNT_BNK += arReceipt.getRctAmountCard2();				

			}
			
			arReceipts = arReceiptHome.findReconciledPostedCard3RctByBaNameAndRctDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_AMNT_BNK += arReceipt.getRctAmountCard3();				

			}
			
			arReceipts = arReceiptHome.findReconciledPostedChequeRctByBaNameAndRctDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_AMNT_BNK += arReceipt.getRctAmountCheque();				

			}

			cmAdjustments = cmAdjustmentHome.findReconciledPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
					BA_NM, DT_FRM, DT_TO, "DEBIT MEMO", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_DPST_AMNT_BNK += cmAdjustment.getAdjAmount();
				
			}
			

			cmAdjustments = cmAdjustmentHome.findReconciledPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
					BA_NM, DT_FRM, DT_TO, "ADVANCE", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_DPST_AMNT_BNK += cmAdjustment.getAdjAmount();
				
			}
			
			
			cmAdjustments = cmAdjustmentHome.findReconciledPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
					BA_NM, DT_FRM, DT_TO, "INTEREST", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {

				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_DPST_AMNT_BNK += cmAdjustment.getAdjAmount();

			}

			// get fund transfers
			
			cmFundTransfers = cmFundTransferHome.findReconciledPostedFtAccountToByBaCodeAndDateRangeAndBrCode(
					adBankAccount.getBaCode(), DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = cmFundTransfers.iterator();
			
			while (i.hasNext()) {
				
				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();
				BR_DPST_AMNT_BNK += cmFundTransfer.getFtAmount();
				
			}

//-------------------------------------------------------------------------------------
// AP
// RECONCILED TRANSACTIONS / BR_DSBRSMNT_AMNT_BNK
// CHECK / CREDIT MEMO / BANK CHARGE / FUND TRANSFER OUT
			
			
			apChecks = apCheckHome.findReconciledPostedChkByBaNameAndChkDateRangeAndBrCode(
					BA_NM, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = apChecks.iterator();
			
			while (i.hasNext()) {
				
				LocalApCheck apCheck = (LocalApCheck)i.next();
				BR_DSBRSMNT_AMNT_BNK += apCheck.getChkAmount();
				
			}
			
			cmAdjustments = cmAdjustmentHome.findReconciledPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
					BA_NM, DT_FRM, DT_TO, "CREDIT MEMO", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_DSBRSMNT_AMNT_BNK += cmAdjustment.getAdjAmount();
			
			}
			
			cmAdjustments = cmAdjustmentHome.findReconciledPostedAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
					BA_NM, DT_FRM, DT_TO, "BANK CHARGE", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_DSBRSMNT_AMNT_BNK += cmAdjustment.getAdjAmount();
			
			}
			
			cmFundTransfers = cmFundTransferHome.findReconciledPostedFtAccountFromByBaCodeAndDateRangeAndBrCode(
					adBankAccount.getBaCode(), DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
			
			i = cmFundTransfers.iterator();
			
			while (i.hasNext()) {
				
				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();
				BR_DSBRSMNT_AMNT_BNK += cmFundTransfer.getFtAmount();
				
			}
			
//-------------------------------------------------------------------------------------
// AR			
// UNRECONCILED TRANSACTIONS / BR_DPST_IN_TRNST_AMNT
// DEBIT
			
			arReceipts = arReceiptHome.findUnreconciledPostedRctByDateAndBaNameAndBrCode(
					DT_TO, BA_NM, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_IN_TRNST_AMNT += arReceipt.getRctAmountCash();				

			}

			arReceipts = arReceiptHome.findUnreconciledPostedCard1RctByDateAndBaNameAndBrCode(
					DT_TO, BA_NM, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_IN_TRNST_AMNT += arReceipt.getRctAmountCard1();				

			}

			arReceipts = arReceiptHome.findUnreconciledPostedCard2RctByDateAndBaNameAndBrCode(
					DT_TO, BA_NM, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_IN_TRNST_AMNT += arReceipt.getRctAmountCard2();				

			}

			arReceipts = arReceiptHome.findUnreconciledPostedCard3RctByDateAndBaNameAndBrCode(
					DT_TO, BA_NM, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_IN_TRNST_AMNT += arReceipt.getRctAmountCard3();				

			}

			arReceipts = arReceiptHome.findUnreconciledPostedChequeRctByDateAndBaNameAndBrCode(
					DT_TO, BA_NM, AD_BRNCH, AD_CMPNY);
			
			i = arReceipts.iterator();

			while (i.hasNext()) {

				LocalArReceipt arReceipt = (LocalArReceipt)i.next();
				BR_DPST_IN_TRNST_AMNT += arReceipt.getRctAmountCheque();				

			}

			cmAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjTypeAndBrCode(DT_TO, BA_NM, "DEBIT MEMO", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_DPST_IN_TRNST_AMNT += cmAdjustment.getAdjAmount();
				
			}
			
			cmAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjTypeAndBrCode(DT_TO, BA_NM, "ADVANCE", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_DPST_IN_TRNST_AMNT += cmAdjustment.getAdjAmount();
				
			}
			
			cmFundTransfers = cmFundTransferHome.findUnreconciledPostedFtAccountToByDateAndBaCodeAndBrCode(DT_TO, adBankAccount.getBaCode(), AD_BRNCH, AD_CMPNY);
			
			i = cmFundTransfers.iterator();
			
			while (i.hasNext()) {
				
				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();
				BR_DPST_IN_TRNST_AMNT += cmFundTransfer.getFtAmount();
				
			}
			
			cmAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjTypeAndBrCode(
					DT_TO, BA_NM, "INTEREST", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {

				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_DPST_IN_TRNST_AMNT += cmAdjustment.getAdjAmount();

			}

			if(isDraftBCBD.equals("Yes")){
				cmAdjustments = cmAdjustmentHome.findDraftAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
						BA_NM, DT_FRM, DT_TO, "INTEREST", AD_BRNCH, AD_CMPNY);
				i = cmAdjustments.iterator();
				
				while (i.hasNext()) {

					LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
	
					BR_DPST_IN_TRNST_AMNT += cmAdjustment.getAdjAmount();

				}	
			}

//-------------------------------------------------------------------------------------
// AP			
// UNRECONCILED TRANSACTIONS / BR_OUTSTNDNG_CHCK_AMNT
// CREDIT
			
			apChecks = apCheckHome.findUnreconciledPostedChkByDateAndBaNameAndBrCode(
					DT_TO, BA_NM, AD_BRNCH, AD_CMPNY);
			
			i = apChecks.iterator();
			
			while (i.hasNext()) {
				
				LocalApCheck apCheck = (LocalApCheck)i.next();
				BR_OUTSTNDNG_CHCK_AMNT += apCheck.getChkAmount();
				
			}
			
			cmAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjTypeAndBrCode(
					DT_TO, BA_NM, "CREDIT MEMO", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_OUTSTNDNG_CHCK_AMNT += cmAdjustment.getAdjAmount();
			
			}
			
			
			cmFundTransfers = cmFundTransferHome.findUnreconciledPostedFtAccountFromByDateAndBaCodeAndBrCode(
					DT_TO, adBankAccount.getBaCode(), AD_BRNCH, AD_CMPNY);
			
			i = cmFundTransfers.iterator();
			
			while (i.hasNext()) {
				
				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer)i.next();
				BR_OUTSTNDNG_CHCK_AMNT += cmFundTransfer.getFtAmount();
				
			}
			
			
			cmAdjustments = cmAdjustmentHome.findUnreconciledPostedAdjByDateAndBaNameAndAdjTypeAndBrCode(
					DT_TO, BA_NM, "BANK CHARGE", AD_BRNCH, AD_CMPNY);
			
			i = cmAdjustments.iterator();
			
			while (i.hasNext()) {
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
				BR_OUTSTNDNG_CHCK_AMNT += cmAdjustment.getAdjAmount();
			
			}

			if(isDraftBCBD.equals("Yes")){
				cmAdjustments = cmAdjustmentHome.findDraftAdjByBaNameAndAdjDateRangeAndAdjTypeAndBrCode(
						BA_NM, DT_FRM, DT_TO, "BANK CHARGE", AD_BRNCH, AD_CMPNY);
				
				i = cmAdjustments.iterator();
				
				while (i.hasNext()) {
					
					LocalCmAdjustment cmAdjustment = (LocalCmAdjustment)i.next();
					BR_OUTSTNDNG_CHCK_AMNT += cmAdjustment.getAdjAmount();
				
				}
			}
			
//-------------------------------------------------------------------------------------
			
			
			
			mdetails.setBrBeginningBalancePerBook(BR_BGNNNG_BLNC_BK);
			mdetails.setBrBeginningBalancePerBank(BR_BGNNNG_BLNC_BNK);
			mdetails.setBrDepositAmountPerBook(BR_DPST_AMNT_BK);
			mdetails.setBrDepositAmountPerBank(BR_DPST_AMNT_BNK);
			mdetails.setBrDisbursementAmountPerBook(BR_DSBRSMNT_AMNT_BK);
			mdetails.setBrDisbursementAmountPerBank(BR_DSBRSMNT_AMNT_BNK);
			mdetails.setBrDepositInTransitAmount(BR_DPST_IN_TRNST_AMNT);
			mdetails.setBrOutstandingChecksAmount(BR_OUTSTNDNG_CHCK_AMNT);
			mdetails.setBrBankCreditAmount(BR_BNK_CRDT_AMNT);
			mdetails.setBrBankDebitAmount(BR_BNK_DBT_AMNT);
			
			return mdetails;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("CmRepBankReconciliationControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/   
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
        
        Debug.print("CmRepBankReconciliationControllerBean getAdBrResAll");
        
        LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
        LocalAdBranchHome adBranchHome = null;
        
        LocalAdBranchResponsibility adBranchResponsibility = null;
        LocalAdBranch adBranch = null;
        
        Collection adBranchResponsibilities = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
            
            throw new GlobalNoRecordFoundException();
            
        }
        
        try {
            
            Iterator i = adBranchResponsibilities.iterator();
            
            while(i.hasNext()) {
                
                adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
                
                adBranch = adBranchResponsibility.getAdBranch();
                
                AdBranchDetails details = new AdBranchDetails();
                
                details.setBrCode(adBranch.getBrCode());
                details.setBrBranchCode(adBranch.getBrBranchCode());
                details.setBrName(adBranch.getBrName());
                details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
                
                list.add(details);
                
            }	               
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
        
    }	
	
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmRepBankReconciliationControllerBean ejbCreate");
      
    }
}
