
/*
 * CmRepCashPositionControllerBean.java
 *
 * Created on August 14, 2008
 * Author: Ariel De Guzman & Reginald Pasco
 */

package com.ejb.txn;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.cm.LocalCmFundTransferReceipt;
import com.ejb.cm.LocalCmFundTransferReceiptHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepReceivingItemsDetails;
import com.util.CmRepCashPositionDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmRepCashPositionControllerEJB"
 *           display-name="Used for cash positioning"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmRepCashPositionControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmRepCashPositionController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmRepCashPositionControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 * 
*/

public class CmRepCashPositionControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("CmRepCashPositionControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccount adBankAccount = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = adBankAccounts.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	adBankAccount = (LocalAdBankAccount)i.next();
	        	
	        	if (adBankAccount.getBaIsCashAccount() == EJBCommon.FALSE) 
	        		list.add(adBankAccount.getBaName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
  
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeCmRepCashPosition(HashMap criteria, Integer AD_CMPNY)
    throws GlobalNoRecordFoundException{
                    
        Debug.print("CmRepCashPositionControllerBean executeCmRepCashPosition");
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalCmFundTransferReceiptHome cmFundTransferReceiptHome = null;
		LocalCmAdjustmentHome cmAdjustmentHome = null;
		LocalApCheckHome apCheckHome = null;

        //initialized EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
    		arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
				lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			cmFundTransferReceiptHome = (LocalCmFundTransferReceiptHome)EJBHomeFactory.
				lookUpLocalHome(LocalCmFundTransferReceiptHome.JNDI_NAME, LocalCmFundTransferReceiptHome.class);			
			cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			
            

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	ArrayList list = new ArrayList();
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);        	
        

        	//get last day of last month
        	Date beginningBalanceDate = this.getBeginningBalanceDate((Date)criteria.get("dateFrom"));
    		
    		//get bank account balance and bankAccountCode IF a bank account is selected        
        	if(criteria.containsKey("bankAccount")) {
        		//if a bank account is selected
        		LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName((String)criteria.get("bankAccount"), AD_CMPNY);
	        	
	    		criteria.put("bankAccountCode", adBankAccount.getBaCode());
        	}
        	
        	
    		//For Cash Receipts
			
			boolean firstArgument = true;
			short ctr = 0;			
			int criteriaSize = criteria.size() - 1;
						
			
			StringBuffer jbossQl = new StringBuffer();
			
			//get all receipt 
			
			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
			}
			
			if (!(criteria.containsKey("bankAccount") || criteria.containsKey("bankAccountCode"))) {
				
				criteriaSize++;
			}
			
			obj = new Object[criteriaSize];
			
			System.out.println("CriteriaSize : " + criteriaSize);
			
			System.out.println("OBJ SIZE : " + obj.length);
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rct.adBankAccount.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
					
				} else {
					
					jbossQl.append("rct.rctVoid = 0 ");  	  
				}   	 
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append(" rct.rctAdCompany = " + AD_CMPNY + " ");
			
			Collection dcpas = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
			Iterator i = dcpas.iterator();
			
			while (i.hasNext()){

				LocalArReceipt arReceipt = (LocalArReceipt) i.next();
				
				CmRepCashPositionDetails cpDetails = new CmRepCashPositionDetails();
				
				Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(
	    				beginningBalanceDate, arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

				if (adBankAccountBalances.size() > 0) {
	        		ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
	        		LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);				
	        		cpDetails.setCpBegBal(adBankAccountBalance.getBabBalance());
				}
				
				
        		cpDetails.setCpAccount(arReceipt.getAdBankAccount().getBaName());
				cpDetails.setCpAmount(arReceipt.getRctAmountCash());
				cpDetails.setCpDate(arReceipt.getRctDate());
				cpDetails.setCpDesc(arReceipt.getRctDescription());
				cpDetails.setCpNum(arReceipt.getRctNumber());
				cpDetails.setCpRefNum(arReceipt.getRctReferenceNumber());
				cpDetails.setCpType("Receipts");
				cpDetails.setCpDateFrom(beginningBalanceDate);
				cpDetails.setCpDateTo((Date)criteria.get("dateTo"));
				
				list.add(cpDetails);
			}
			
			
			
			
			
			
			
			
			
			
//get all receipt Card 1
			
			jbossQl = new StringBuffer();
			
			ctr = 0;
			criteriaSize = criteria.size() - 1;
			firstArgument = true;
			
			
			
			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
			
			      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
			}
			
			if (!(criteria.containsKey("bankAccount") || criteria.containsKey("bankAccountCode"))) {
				
				criteriaSize++;
			}
			
			obj = new Object[criteriaSize];
			
			System.out.println("CriteriaSize : " + criteriaSize);
			
			System.out.println("OBJ SIZE : " + obj.length);
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rct.adBankAccountCard1.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
					
				} else {
					
					jbossQl.append("rct.rctVoid = 0 ");  	  
				}   	 
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append(" rct.rctAdCompany = " + AD_CMPNY + " ");
			
			 dcpas = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();
			
			while (i.hasNext()){

				LocalArReceipt arReceipt = (LocalArReceipt) i.next();
				
				CmRepCashPositionDetails cpDetails = new CmRepCashPositionDetails();
				
				Collection adBankAccountCard1Balances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(
	    				beginningBalanceDate, arReceipt.getAdBankAccountCard1().getBaCode(), "BOOK", AD_CMPNY);

				if (adBankAccountCard1Balances.size() > 0) {
	        		ArrayList adBankAccountCard1BalanceList = new ArrayList(adBankAccountCard1Balances);
	        		LocalAdBankAccountBalance adBankAccountCard1Balance = (LocalAdBankAccountBalance)adBankAccountCard1BalanceList.get(adBankAccountCard1BalanceList.size() - 1);				
	        		cpDetails.setCpBegBal(adBankAccountCard1Balance.getBabBalance());
				}
				
				
        		cpDetails.setCpAccount(arReceipt.getAdBankAccountCard1().getBaName());
				cpDetails.setCpAmount(arReceipt.getRctAmountCard1());
				cpDetails.setCpDate(arReceipt.getRctDate());
				cpDetails.setCpDesc(arReceipt.getRctDescription());
				cpDetails.setCpNum(arReceipt.getRctNumber());
				cpDetails.setCpRefNum(arReceipt.getRctReferenceNumber());
				cpDetails.setCpType("Receipts");
				cpDetails.setCpDateFrom(beginningBalanceDate);
				cpDetails.setCpDateTo((Date)criteria.get("dateTo"));
				
				list.add(cpDetails);
			}
			
			
			
			
//get all receipt Card2
			
			jbossQl = new StringBuffer();
			
			ctr = 0;
			criteriaSize = criteria.size() - 1;
			firstArgument = true;
			
			
			
			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
			
			      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
			}
			
			if (!(criteria.containsKey("bankAccount") || criteria.containsKey("bankAccountCode"))) {
				
				criteriaSize++;
			}
			
			obj = new Object[criteriaSize];
			
			System.out.println("CriteriaSize : " + criteriaSize);
			
			System.out.println("OBJ SIZE : " + obj.length);
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rct.adBankAccountCard2.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
					
				} else {
					
					jbossQl.append("rct.rctVoid = 0 ");  	  
				}   	 
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append(" rct.rctAdCompany = " + AD_CMPNY + " ");
			
			 dcpas = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();
			
			while (i.hasNext()){

				LocalArReceipt arReceipt = (LocalArReceipt) i.next();
				
				CmRepCashPositionDetails cpDetails = new CmRepCashPositionDetails();
				
				Collection adBankAccountCard2Balances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(
	    				beginningBalanceDate, arReceipt.getAdBankAccountCard2().getBaCode(), "BOOK", AD_CMPNY);

				if (adBankAccountCard2Balances.size() > 0) {
	        		ArrayList adBankAccountCard2BalanceList = new ArrayList(adBankAccountCard2Balances);
	        		LocalAdBankAccountBalance adBankAccountCard2Balance = (LocalAdBankAccountBalance)adBankAccountCard2BalanceList.get(adBankAccountCard2BalanceList.size() - 1);				
	        		cpDetails.setCpBegBal(adBankAccountCard2Balance.getBabBalance());
				}
				
				
        		cpDetails.setCpAccount(arReceipt.getAdBankAccountCard2().getBaName());
				cpDetails.setCpAmount(arReceipt.getRctAmountCard2());
				cpDetails.setCpDate(arReceipt.getRctDate());
				cpDetails.setCpDesc(arReceipt.getRctDescription());
				cpDetails.setCpNum(arReceipt.getRctNumber());
				cpDetails.setCpRefNum(arReceipt.getRctReferenceNumber());
				cpDetails.setCpType("Receipts");
				cpDetails.setCpDateFrom(beginningBalanceDate);
				cpDetails.setCpDateTo((Date)criteria.get("dateTo"));
				
				list.add(cpDetails);
			}
			
			
			
			
			
			
			
			
			
//get all receipt Card3
			
			jbossQl = new StringBuffer();
			
			ctr = 0;
			criteriaSize = criteria.size() - 1;
			firstArgument = true;
			
			
			
			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
			
			      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
			}
			
			if (!(criteria.containsKey("bankAccount") || criteria.containsKey("bankAccountCode"))) {
				
				criteriaSize++;
			}
			
			obj = new Object[criteriaSize];
			
			System.out.println("CriteriaSize : " + criteriaSize);
			
			System.out.println("OBJ SIZE : " + obj.length);
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rct.adBankAccountCard3.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
					
				} else {
					
					jbossQl.append("rct.rctVoid = 0 ");  	  
				}   	 
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append(" rct.rctAdCompany = " + AD_CMPNY + " ");
			
			 dcpas = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();
			
			while (i.hasNext()){

				LocalArReceipt arReceipt = (LocalArReceipt) i.next();
				
				CmRepCashPositionDetails cpDetails = new CmRepCashPositionDetails();
				
				Collection adBankAccountCard3Balances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(
	    				beginningBalanceDate, arReceipt.getAdBankAccountCard3().getBaCode(), "BOOK", AD_CMPNY);

				if (adBankAccountCard3Balances.size() > 0) {
	        		ArrayList adBankAccountCard3BalanceList = new ArrayList(adBankAccountCard3Balances);
	        		LocalAdBankAccountBalance adBankAccountCard3Balance = (LocalAdBankAccountBalance)adBankAccountCard3BalanceList.get(adBankAccountCard3BalanceList.size() - 1);				
	        		cpDetails.setCpBegBal(adBankAccountCard3Balance.getBabBalance());
				}
				
				
        		cpDetails.setCpAccount(arReceipt.getAdBankAccountCard3().getBaName());
				cpDetails.setCpAmount(arReceipt.getRctAmountCard3());
				cpDetails.setCpDate(arReceipt.getRctDate());
				cpDetails.setCpDesc(arReceipt.getRctDescription());
				cpDetails.setCpNum(arReceipt.getRctNumber());
				cpDetails.setCpRefNum(arReceipt.getRctReferenceNumber());
				cpDetails.setCpType("Receipts");
				cpDetails.setCpDateFrom(beginningBalanceDate);
				cpDetails.setCpDateTo((Date)criteria.get("dateTo"));
				
				list.add(cpDetails);
			}
			
			
			
			
			
			
			
			
			
//get all receipt Cheque
			
			jbossQl = new StringBuffer();
			
			ctr = 0;
			criteriaSize = criteria.size() - 1;
			firstArgument = true;
			
			
			
			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
			
			      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
			}
			
			if (!(criteria.containsKey("bankAccount") || criteria.containsKey("bankAccountCode"))) {
				
				criteriaSize++;
			}
			
			obj = new Object[criteriaSize];
			
			System.out.println("CriteriaSize : " + criteriaSize);
			
			System.out.println("OBJ SIZE : " + obj.length);
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rct.adBankAccount.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
					
				} else {
					
					jbossQl.append("rct.rctVoid = 0 ");  	  
				}   	 
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append(" rct.rctAdCompany = " + AD_CMPNY + " ");
			
			 dcpas = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();
			
			while (i.hasNext()){

				LocalArReceipt arReceipt = (LocalArReceipt) i.next();
				
				CmRepCashPositionDetails cpDetails = new CmRepCashPositionDetails();
				
				Collection adBankAccountChequeBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(
	    				beginningBalanceDate, arReceipt.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);

				if (adBankAccountChequeBalances.size() > 0) {
	        		ArrayList adBankAccountChequeBalanceList = new ArrayList(adBankAccountChequeBalances);
	        		LocalAdBankAccountBalance adBankAccountChequeBalance = (LocalAdBankAccountBalance)adBankAccountChequeBalanceList.get(adBankAccountChequeBalanceList.size() - 1);				
	        		cpDetails.setCpBegBal(adBankAccountChequeBalance.getBabBalance());
				}
				
				
        		cpDetails.setCpAccount(arReceipt.getAdBankAccount().getBaName());
				cpDetails.setCpAmount(arReceipt.getRctAmountCheque());
				cpDetails.setCpDate(arReceipt.getRctDate());
				cpDetails.setCpDesc(arReceipt.getRctDescription());
				cpDetails.setCpNum(arReceipt.getRctNumber());
				cpDetails.setCpRefNum(arReceipt.getRctReferenceNumber());
				cpDetails.setCpType("Receipts");
				cpDetails.setCpDateFrom(beginningBalanceDate);
				cpDetails.setCpDateTo((Date)criteria.get("dateTo"));
				
				list.add(cpDetails);
			}
			
			
			
		
			
			//	get all deposit transfer receipt
			
			jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(ftr) FROM CmFundTransferReceipt ftr ");
			
			ctr = 0;
			criteriaSize = criteria.size() - 1;
			firstArgument = true;
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
			}
			
			if (!(criteria.containsKey("bankAccount") || criteria.containsKey("bankAccountCode"))) {
				
				criteriaSize++;
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccountCode")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {		       	  	
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("ftr.cmFundTransfer.ftAdBaAccountTo=?" + (ctr+1) + " ");
				obj[ctr] = (Integer)criteria.get("bankAccountCode");
				ctr++;
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("ftr.cmFundTransfer.ftDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("ftr.cmFundTransfer.ftDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");

				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	
				
				if (unposted.equals("NO")) {				

					jbossQl.append("ftr.cmFundTransfer.ftPosted = 1 " );
					
				} else {
					
					jbossQl.append("ftr.cmFundTransfer.ftVoid = 0 " );
				}
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append(" ftr.ftrAdCompany = " + AD_CMPNY + " ");
			
			dcpas = cmFundTransferReceiptHome.getFtrByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();

			while (i.hasNext()){

				LocalCmFundTransferReceipt cmFundTransferReceipt = (LocalCmFundTransferReceipt) i.next();
				
				CmRepCashPositionDetails cpDetails = new CmRepCashPositionDetails();
				
				Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(
	    				beginningBalanceDate, cmFundTransferReceipt.getArReceipt().getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);
	        	
				if (adBankAccountBalances.size() > 0) {
	        		ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
	        		LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);				
	        		cpDetails.setCpBegBal(adBankAccountBalance.getBabBalance());
				}
    			cpDetails.setCpAccount(cmFundTransferReceipt.getArReceipt().getAdBankAccount().getBaName());        		
        		cpDetails.setCpAmount(cmFundTransferReceipt.getFtrAmountDeposited());
				cpDetails.setCpDate(cmFundTransferReceipt.getCmFundTransfer().getFtDate());
				cpDetails.setCpDesc("DEPOSITED RECEIPT");
				cpDetails.setCpNum(cmFundTransferReceipt.getCmFundTransfer().getFtDocumentNumber());
				cpDetails.setCpRefNum(cmFundTransferReceipt.getCmFundTransfer().getFtReferenceNumber());
				cpDetails.setCpType("Receipts");				
				cpDetails.setCpDateFrom(beginningBalanceDate);
				cpDetails.setCpDateTo((Date)criteria.get("dateTo"));
				
				list.add(cpDetails);
			}
			
			//	get all fund transfer to
			
			jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(ft) FROM CmFundTransfer ft ");
			
			ctr = 0;
			criteriaSize = criteria.size() - 1;
			firstArgument = true;
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
			}

			if (!(criteria.containsKey("bankAccount") || criteria.containsKey("bankAccountCode"))) {
				
				criteriaSize++;
			}

			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccountCode")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("ft.ftAdBaAccountTo=?" + (ctr+1) + " ");
				obj[ctr] = (Integer)criteria.get("bankAccountCode");
				ctr++;
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("ft.ftDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("ft.ftDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	
				
				if (unposted.equals("NO")) {

					jbossQl.append("ft.ftPosted = 1 " );
					
				}   	 
				 else {
					
					jbossQl.append("ft.ftVoid = 0 " );
				}
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append(" ft.ftAdCompany = " + AD_CMPNY + " ");
			
			dcpas = cmFundTransferHome.getFtByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();

			while (i.hasNext()){
				
				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer) i.next();
				Collection cmFundTransferReceipts = cmFundTransfer.getCmFundTransferReceipts();
				
				if (cmFundTransferReceipts.isEmpty()) {
					CmRepCashPositionDetails cpDetails = new CmRepCashPositionDetails();

					
					Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(
		    				beginningBalanceDate, cmFundTransfer.getFtAdBaAccountTo(), "BOOK", AD_CMPNY);
		        	
					if (adBankAccountBalances.size() > 0) {
		        		ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
		        		LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);				
		        		cpDetails.setCpBegBal(adBankAccountBalance.getBabBalance());
					}
					
					LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountTo());
					
        			cpDetails.setCpAccount(adBankAccount.getBaName());
        			cpDetails.setCpAmount(cmFundTransfer.getFtAmount());
					cpDetails.setCpDate(cmFundTransfer.getFtDate());
					cpDetails.setCpDesc(cmFundTransfer.getFtMemo());
					cpDetails.setCpNum(cmFundTransfer.getFtDocumentNumber());
					cpDetails.setCpRefNum(cmFundTransfer.getFtReferenceNumber());
					cpDetails.setCpType("Receipts");				
					cpDetails.setCpDateFrom(beginningBalanceDate);
					cpDetails.setCpDateTo((Date)criteria.get("dateTo"));
					
					list.add(cpDetails);
				}
			}
			
			//	get all positve adjustments
			
			jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(adj) FROM CmAdjustment adj ");
			
			ctr = 0;
			criteriaSize = criteria.size() - 1;
			firstArgument = true;
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
			}

			if (!(criteria.containsKey("bankAccount") || criteria.containsKey("bankAccountCode"))) {
				
				criteriaSize++;
			}

			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {	
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("adj.adBankAccount.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
			}

			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("adj.adjDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("adj.adjDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");

				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	
				
				if (unposted.equals("NO")) {

					jbossQl.append("adj.adjPosted = 1 " );
					
				} else {
					
					jbossQl.append("adj.adjVoid = 0 " );
				}   	 
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append(" (adj.adjType = 'INTEREST' OR adj.adjType = 'DEBIT MEMO' OR adj.adjType = 'ADVANCE') AND adj.adjAdCompany = " + AD_CMPNY + " ");

			dcpas = cmAdjustmentHome.getAdjByCriteria(jbossQl.toString(), obj);	 
			i = dcpas.iterator();

			while (i.hasNext()){				

				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment) i.next();	
				
				CmRepCashPositionDetails cpDetails = new CmRepCashPositionDetails();		

				Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(
	    				beginningBalanceDate, cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);
	        	
				if (adBankAccountBalances.size() > 0) {
	        		ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
	        		LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);				
	        		cpDetails.setCpBegBal(adBankAccountBalance.getBabBalance());
				}
				cpDetails.setCpAccount(cmAdjustment.getAdBankAccount().getBaName());
				cpDetails.setCpAmount(cmAdjustment.getAdjAmount());
				cpDetails.setCpDate(cmAdjustment.getAdjDate());
				cpDetails.setCpDesc(cmAdjustment.getAdjMemo());
				cpDetails.setCpNum(cmAdjustment.getAdjDocumentNumber());
				cpDetails.setCpRefNum(cmAdjustment.getAdjReferenceNumber());
				cpDetails.setCpType("Receipts");		
				cpDetails.setCpDateFrom(beginningBalanceDate);
				cpDetails.setCpDateTo((Date)criteria.get("dateTo"));
				
				list.add(cpDetails);
			}
			

			// For Disbursements
    		
			//get bank account balance
			firstArgument = true;
			ctr = 0;
			criteriaSize = criteria.size() - 1;	      
			
			jbossQl = new StringBuffer();
			
			//get all released checks 
			
			jbossQl.append("SELECT OBJECT(chk) FROM ApCheck chk ");
			
			firstArgument = true;
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
			}

			if (!(criteria.containsKey("bankAccount") || criteria.containsKey("bankAccountCode"))) {
				
				criteriaSize++;
			}

			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("chk.adBankAccount.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("chk.chkCheckDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("chk.chkCheckDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("chk.chkPosted = 1 AND chk.chkVoid = 0"); // AND chk.chkReleased = 1");
					
				} else {
					
					jbossQl.append("chk.chkVoid = 0 ");  	  
				}   	 
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append(/*chk.chkReleased = 1 AND*/ " chk.chkAdCompany = " + AD_CMPNY + " ");
			
			Collection dcpls = apCheckHome.getChkByCriteria(jbossQl.toString(), obj);	         
			i = dcpls.iterator();
	
			while (i.hasNext()){
				
				LocalApCheck apCheck = (LocalApCheck) i.next();
				
				CmRepCashPositionDetails cpDetails = new CmRepCashPositionDetails();
        		
				Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(
	    				beginningBalanceDate, apCheck.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);
	        	
				if (adBankAccountBalances.size() > 0) {
	        		ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
	        		LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);				
	        		cpDetails.setCpBegBal(adBankAccountBalance.getBabBalance());
				}
				cpDetails.setCpAccount(apCheck.getAdBankAccount().getBaName());
				cpDetails.setCpAmount(apCheck.getChkAmount());
				cpDetails.setCpDate(apCheck.getChkDate());
				cpDetails.setCpDesc(apCheck.getChkDescription());
				cpDetails.setCpNum(apCheck.getChkDocumentNumber());
				cpDetails.setCpRefNum(apCheck.getChkReferenceNumber());
				cpDetails.setCpType("Disbursements");	
				cpDetails.setCpDateFrom(beginningBalanceDate);
				cpDetails.setCpDateTo((Date)criteria.get("dateTo"));
				
				list.add(cpDetails);
			}

			//	get all fund transfer from
			
			jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(ft) FROM CmFundTransfer ft ");
			
			ctr = 0;
			criteriaSize = criteria.size() - 1;
			firstArgument = true;
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
			}

			if (!(criteria.containsKey("bankAccount") || criteria.containsKey("bankAccountCode"))) {
				
				criteriaSize++;
			}

			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccountCode")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("ft.ftAdBaAccountFrom=?" + (ctr+1) + " ");
				obj[ctr] = (Integer)criteria.get("bankAccountCode");
				ctr++;
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("ft.ftDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("ft.ftDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				if (unposted.equals("NO")) {

					jbossQl.append("ft.ftPosted = 1 " );
					
				} else {
					
					jbossQl.append("ft.ftVoid = 0 " );
				}   	 
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append(" ft.ftAdCompany = " + AD_CMPNY + " ");

			dcpls = cmFundTransferHome.getFtByCriteria(jbossQl.toString(), obj);	         
			i = dcpls.iterator();

			while (i.hasNext()){
				
				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer) i.next();
				
				CmRepCashPositionDetails cpDetails = new CmRepCashPositionDetails();
								
				Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(
	    				beginningBalanceDate, cmFundTransfer.getFtAdBaAccountFrom(), "BOOK", AD_CMPNY);
	        	
				if (adBankAccountBalances.size() > 0) {
	        		ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
	        		LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);				
	        		cpDetails.setCpBegBal(adBankAccountBalance.getBabBalance());
				}

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());
				
    			cpDetails.setCpAccount(adBankAccount.getBaName());
        		cpDetails.setCpAmount(cmFundTransfer.getFtAmount());
				cpDetails.setCpDate(cmFundTransfer.getFtDate());
				cpDetails.setCpDesc(cmFundTransfer.getFtMemo());
				cpDetails.setCpNum(cmFundTransfer.getFtDocumentNumber());
				cpDetails.setCpRefNum(cmFundTransfer.getFtReferenceNumber());
				cpDetails.setCpType("Disbursements");
				cpDetails.setCpDateFrom(beginningBalanceDate);
				cpDetails.setCpDateTo((Date)criteria.get("dateTo"));
				
				
				list.add(cpDetails);												
			}
			
			//	get all negative adjustments
			
			jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(adj) FROM CmAdjustment adj ");
			
			ctr = 0;
			criteriaSize = criteria.size() - 1;
			firstArgument = true;
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
			}

			if (!(criteria.containsKey("bankAccount") || criteria.containsKey("bankAccountCode"))) {
				
				criteriaSize++;
			}

			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("adj.adBankAccount.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("adj.adjDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				jbossQl.append("adj.adjDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				
				if (unposted.equals("NO")) {

					jbossQl.append("adj.adjPosted = 1 " );
					
				} else {
					
					jbossQl.append("adj.adjVoid = 0 " );
				}   	 
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			
			jbossQl.append(" (adj.adjType = 'BANK CHARGE' OR adj.adjType = 'CREDIT MEMO') AND adj.adjAdCompany = " + AD_CMPNY + " ");
			dcpls = cmAdjustmentHome.getAdjByCriteria(jbossQl.toString(), obj);	         
			i = dcpls.iterator();

 			while (i.hasNext()){
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment) i.next();

				CmRepCashPositionDetails cpDetails = new CmRepCashPositionDetails();

				Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeOrEqualDateAndBaCodeAndType(
	    				beginningBalanceDate, cmAdjustment.getAdBankAccount().getBaCode(), "BOOK", AD_CMPNY);
	        	
				if (adBankAccountBalances.size() > 0) {
	        		ArrayList adBankAccountBalanceList = new ArrayList(adBankAccountBalances);
	        		LocalAdBankAccountBalance adBankAccountBalance = (LocalAdBankAccountBalance)adBankAccountBalanceList.get(adBankAccountBalanceList.size() - 1);				
	        		cpDetails.setCpBegBal(adBankAccountBalance.getBabBalance());
				}
        		cpDetails.setCpAccount(cmAdjustment.getAdBankAccount().getBaName());
				cpDetails.setCpAmount(cmAdjustment.getAdjAmount());
				cpDetails.setCpDate(cmAdjustment.getAdjDate());
				cpDetails.setCpDesc(cmAdjustment.getAdjMemo());
				cpDetails.setCpNum(cmAdjustment.getAdjDocumentNumber());
				cpDetails.setCpRefNum(cmAdjustment.getAdjReferenceNumber());
				cpDetails.setCpType("Disbursements");	
				cpDetails.setCpDateFrom(beginningBalanceDate);
				cpDetails.setCpDateTo((Date)criteria.get("dateTo"));
			
				list.add(cpDetails);
			}
 			
			Collections.sort(list, CmRepCashPositionDetails.BankAccountComparator);

    		if (list.isEmpty())
        		throw new GlobalNoRecordFoundException();
			
		
        	
    		return list;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  }
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("CmRepCashPositionControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  }
	}    
	
	// private methods
	private Date getBeginningBalanceDate(Date dateEntered){
		
		Debug.print("CmRepCashPositionControllerBean getBeginningBalanceDate");

		try {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(dateEntered);
			calendar.add(GregorianCalendar.MONTH, -1);
			
			GregorianCalendar calendar2 = new GregorianCalendar(
					calendar.get(Calendar.YEAR), 
					calendar.get(Calendar.MONTH), 
					calendar.getActualMaximum(Calendar.DATE), 
    				0, 0, 0);
			
			return calendar2.getTime();
			
		} finally {
			
		}
		//return calendar.getTime();
		
	}
	
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmRepCashPositionControllerBean ejbCreate");
    }
}
