
/*
 * CmRepDailyCashForecastDetailControllerBean.java
 *
 * Created on January 25, 2006 2:00 PM
 *
 * @author  Farrah S. Garing
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherPaymentSchedule;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.CmRepDailyCashForecastDetailAddDetails;
import com.util.CmRepDailyCashForecastDetailDetails;
import com.util.CmRepDailyCashForecastDetailLessDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmRepDailyCashForecastDetailControllerEJB"
 *           display-name="Used for daily cash forecasting"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmRepDailyCashForecastDetailControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmRepDailyCashForecastDetailController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmRepDailyCashForecastDetailControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 * 
*/

public class CmRepDailyCashForecastDetailControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("CmRepDailyCashForecastDetailControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccount adBankAccount = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = adBankAccounts.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	adBankAccount = (LocalAdBankAccount)i.next();
	        	
	        	list.add(adBankAccount.getBaName());

	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
  
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public CmRepDailyCashForecastDetailDetails executeCmRepDailyCashForecastDetail(HashMap criteria, Integer AD_CMPNY)
    throws GlobalNoRecordFoundException{
                    
        Debug.print("CmRepDailyCashForecastDetailControllerBean executeCmRepDailyCashForecastDetail");
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
        
        //initialized EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
            

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	String[] selectedBankAccounts = (String[])criteria.get("selectedBankAccount");
        	Integer[] selectedBankAccountCodes = new Integer[selectedBankAccounts.length];
        	
        	for(int i=0; i < selectedBankAccounts.length; i++) {

        		LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName(selectedBankAccounts[i], AD_CMPNY);
        		selectedBankAccountCodes[i] = adBankAccount.getBaCode();
        	
        	}
        	
	        Date date = null;
        	
        	if (criteria.containsKey("date")){
        		
        		date = (Date)criteria.get("date");
        		
        	}
			
        	String reportType = null;
        	
        	if (criteria.containsKey("reportType")){
        		
        		reportType = (String)criteria.get("reportType");
        		
        	}
        	
    		CmRepDailyCashForecastDetailDetails mdetails = new CmRepDailyCashForecastDetailDetails();

        	if (reportType.equals("FIVE DAYS")) {

        		mdetails.setDcfdDate5(date);
        		mdetails.setDcfdDate4(computeDate(mdetails.getDcfdDate5()));
        		mdetails.setDcfdDate3(computeDate(mdetails.getDcfdDate4()));
        		mdetails.setDcfdDate2(computeDate(mdetails.getDcfdDate3()));
        		mdetails.setDcfdDate1(computeDate(mdetails.getDcfdDate2()));

        		criteria.put("dateTo",mdetails.getDcfdDate5());

        	} else if (reportType.equals("SEVEN DAYS")) {

        		mdetails.setDcfdDate7(date);
        		mdetails.setDcfdDate6(computeDate(date));
        		mdetails.setDcfdDate5(computeDate(mdetails.getDcfdDate6()));
        		mdetails.setDcfdDate4(computeDate(mdetails.getDcfdDate5()));
        		mdetails.setDcfdDate3(computeDate(mdetails.getDcfdDate4()));
        		mdetails.setDcfdDate2(computeDate(mdetails.getDcfdDate3()));
        		mdetails.setDcfdDate1(computeDate(mdetails.getDcfdDate2()));
        		criteria.put("dateTo",mdetails.getDcfdDate7());

        	}
        	
        	criteria.put("dateFrom",mdetails.getDcfdDate1());
        	
        	mdetails.setDcfdAddList(executeCmRepDailyCashForecastDetailAdd(criteria, mdetails, AD_CMPNY));
        	mdetails.setDcfdLessList(executeCmRepDailyCashForecastDetailLess(criteria, mdetails, AD_CMPNY));

        	if (mdetails.getDcfdAddList().isEmpty() && mdetails.getDcfdLessList().isEmpty())
        		throw new GlobalNoRecordFoundException();
        	
        	double TTL_ACCNT_BLNC = 0d;
        	
        	for(int j = 0; j < selectedBankAccountCodes.length; j++) {
        		
        		Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeDateAndBaCodeAndType(mdetails.getDcfdDate1(), selectedBankAccountCodes[j], "BOOK", AD_CMPNY);
        		
        		LocalAdBankAccountBalance adBankAccountBalance = null;
        		
        		if (!adBankAccountBalances.isEmpty()) {
        			
        			Iterator i = adBankAccountBalances.iterator();
        			
        			while(i.hasNext()){
        				
        				adBankAccountBalance = (LocalAdBankAccountBalance) i.next();
        				
        			}
        			
        		}
        		
        		TTL_ACCNT_BLNC = TTL_ACCNT_BLNC + (adBankAccountBalance != null ? adBankAccountBalance.getBabBalance():0);
        		
        	}

    		mdetails.setDcfdBeginningBalance1(TTL_ACCNT_BLNC);
    		
    		mdetails = computeBeginningBalancesAndAvailableCashBalance(mdetails, AD_CMPNY);
    		
        	return mdetails;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("CmRepDailyCashForecastDetailControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("CmRepDailyCashForecastDetailControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
    
	private Date computeDate(Date dateEntered){
		
		Debug.print("CmRepDailyCashForecastDetailControllerBean computeDate");
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(dateEntered);
		calendar.add(GregorianCalendar.DATE, -1);
		
		return calendar.getTime();
		
	}
	
    
	private ArrayList executeCmRepDailyCashForecastDetailAdd(HashMap criteria, CmRepDailyCashForecastDetailDetails details,Integer AD_CMPNY)  {
		
		Debug.print("CmRepDailyCashForecastDetailControllerBean executeCmRepDailyCashForecastDetailAdd");
		
		LocalArInvoiceHome arInvoiceHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			StringBuffer jbossQl = new StringBuffer();
			
			//get all receipt 
			
			jbossQl.append("SELECT OBJECT(inv) FROM ArInvoice inv ");
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("selectedBankAccount")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("date")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("reportType")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {

	        		
					
					jbossQl.append("inv.invDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					
	        		
					ctr++;
					
				} else {
					
					jbossQl.append("inv.invDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("inv.invDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			String unposted = null;
			
			if (criteria.containsKey("includedUnposted")) {
				
				unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("inv.invPosted = 1 ");
					
				} else {
					
					jbossQl.append("inv.invVoid = 0 " );
					
				}
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" inv.invCreditMemo = 0 AND inv.invAmountDue > inv.invAmountPaid AND inv.invAdCompany = " + AD_CMPNY + " ");

			System.out.println(jbossQl.toString());
			
			Collection dcfad = arInvoiceHome.getInvByCriteria(jbossQl.toString(), obj);	         
			Iterator i = dcfad.iterator();
			ArrayList dcfaList = new ArrayList();
			
			while (i.hasNext()){
				
				LocalArInvoice arInvoice = (LocalArInvoice) i.next();
				
				CmRepDailyCashForecastDetailAddDetails addDetails = new CmRepDailyCashForecastDetailAddDetails();
				
				addDetails.setDcfdaCustomer(arInvoice.getArCustomer().getCstName());				
				addDetails.setDcfdaDescription (arInvoice.getInvDescription());
				addDetails.setDcfdaInvoiceNumber(arInvoice.getInvNumber());

				String reportType = (String) criteria.get("reportType");
				
				Iterator ipsIter = arInvoice.getArInvoicePaymentSchedules().iterator();
				
				while(ipsIter.hasNext())
				{
					LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule) ipsIter.next();
					
					addDetails.setDcfdaDateTransaction(arInvoicePaymentSchedule.getIpsDueDate());
					
					double dcfAmount = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
					  	  	arInvoice.getGlFunctionalCurrency().getFcCode(), 
							arInvoice.getGlFunctionalCurrency().getFcName(), arInvoice.getInvConversionDate(),
					  	  	arInvoice.getInvConversionRate(), arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid(), AD_CMPNY),
							adCompany.getGlFunctionalCurrency().getFcPrecision());
					
					if (arInvoicePaymentSchedule.getIpsDueDate().equals(details.getDcfdDate1())) {
						
						addDetails.setDcfdaAmount1(dcfAmount);
						
					} else if (arInvoicePaymentSchedule.getIpsDueDate().equals(details.getDcfdDate2())) {
						
						addDetails.setDcfdaAmount2(dcfAmount);
						
					} else if (arInvoicePaymentSchedule.getIpsDueDate().equals(details.getDcfdDate3())) {
						
						addDetails.setDcfdaAmount3(dcfAmount);
						
					} else if (arInvoicePaymentSchedule.getIpsDueDate().equals(details.getDcfdDate4())) {
						
						addDetails.setDcfdaAmount4(dcfAmount);
						
					} else if (arInvoicePaymentSchedule.getIpsDueDate().equals(details.getDcfdDate5())) {
						
						addDetails.setDcfdaAmount5(dcfAmount);
						
					} else if (arInvoicePaymentSchedule.getIpsDueDate().equals(details.getDcfdDate6())) {
						
						addDetails.setDcfdaAmount6(dcfAmount);
						
					} else if (arInvoicePaymentSchedule.getIpsDueDate().equals(details.getDcfdDate7())) {
						
						addDetails.setDcfdaAmount7(dcfAmount);
						
					}
					
					dcfaList.add(addDetails);
				}					
				
			}
			
			return dcfaList;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
    
    
	private ArrayList executeCmRepDailyCashForecastDetailLess(HashMap criteria, CmRepDailyCashForecastDetailDetails details,Integer AD_CMPNY)  {
		
		Debug.print("CmRepDailyCashForecastDetailControllerBean executeCmRepDailyCashForecastDetailLess");
		
		LocalApVoucherHome apVoucherHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalApCheckHome apCheckHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			

		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			StringBuffer jbossQl = new StringBuffer();
			
			//get all receipt 
			
			jbossQl.append("SELECT OBJECT(vou) FROM ApVoucher vou ");
			
			
			firstArgument = true;
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("selectedBankAccount")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("date")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("reportType")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {
					
					jbossQl.append("vou.vouDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				} else {
					
					jbossQl.append("vou.vouDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("vou.vouDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			String unposted = null;
			
			if (criteria.containsKey("includedUnposted")) {
				
				unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("vou.vouPosted = 1 ");
					
				} else {
					
					jbossQl.append("vou.vouVoid = 0 " );
					
				}  	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" vou.vouDebitMemo = 0 AND vou.vouAmountDue > vou.vouAmountPaid AND vou.vouAdCompany = " + AD_CMPNY + " ");
			
			Collection dcfld = apVoucherHome.getVouByCriteria(jbossQl.toString(), obj);	         
			Iterator i = dcfld.iterator();
			ArrayList dcflList = new ArrayList();
			
			while (i.hasNext()){
				
				LocalApVoucher apVoucher = (LocalApVoucher) i.next();
				
				CmRepDailyCashForecastDetailLessDetails lessDetails = new CmRepDailyCashForecastDetailLessDetails();
				
				lessDetails.setDcfdlSupplier(apVoucher.getApSupplier().getSplName());				
				lessDetails.setDcfdlDescription (apVoucher.getVouDescription());
				lessDetails.setDcfdlVoucherNumber(apVoucher.getVouDocumentNumber());

				String reportType = (String) criteria.get("reportType");
				
				double UN_PSTD_CHCKS = 0d;
				
				Collection apVoucherPaymentSchedules = apVoucher.getApVoucherPaymentSchedules();
				Iterator vpsItr = apVoucherPaymentSchedules.iterator();
				
				while (vpsItr.hasNext()){
					
					LocalApVoucherPaymentSchedule apVoucherPaymentSchedule =
						(LocalApVoucherPaymentSchedule) vpsItr.next();
					
					lessDetails.setDcfdlDateTransaction(apVoucher.getVouDate());
					
					if (!unposted.equals("NO")) {
						Collection arAppliedVoucher = apVoucherPaymentSchedule.getApAppliedVouchers();
						Iterator avItr = arAppliedVoucher.iterator();
						
						while (avItr.hasNext()){
							
							LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher) avItr.next();
							
							if (apAppliedVoucher.getApCheck().getChkPosted() == EJBCommon.FALSE) {
								
								UN_PSTD_CHCKS = UN_PSTD_CHCKS + apAppliedVoucher.getAvApplyAmount() + apAppliedVoucher.getAvTaxWithheld() + apAppliedVoucher.getAvDiscountAmount();
								
							}
							
						}
					}
					
					
					lessDetails.setDcfdlDateTransaction(apVoucher.getVouDate());
					
					double dcfdlAmount = EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
							apVoucher.getGlFunctionalCurrency().getFcCode(),
							apVoucher.getGlFunctionalCurrency().getFcName(), apVoucher.getVouConversionDate(),
							apVoucher.getVouConversionRate(), 
							apVoucherPaymentSchedule.getVpsAmountDue() - apVoucherPaymentSchedule.getVpsAmountPaid() - UN_PSTD_CHCKS, 
							AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision());
					
					if (apVoucherPaymentSchedule.getVpsDueDate().equals(details.getDcfdDate1())) {
						
						lessDetails.setDcfdlAmount1(dcfdlAmount);
						
					} else if (apVoucherPaymentSchedule.getVpsDueDate().equals(details.getDcfdDate2())) {
						
						lessDetails.setDcfdlAmount2(dcfdlAmount);
						
					} else if (apVoucherPaymentSchedule.getVpsDueDate().equals(details.getDcfdDate3())) {
						
						lessDetails.setDcfdlAmount3(dcfdlAmount);
						
					} else if (apVoucherPaymentSchedule.getVpsDueDate().equals(details.getDcfdDate4())) {
						
						lessDetails.setDcfdlAmount4(dcfdlAmount);
						
					} else if (apVoucherPaymentSchedule.getVpsDueDate().equals(details.getDcfdDate5())) {
						
						lessDetails.setDcfdlAmount5(dcfdlAmount);
						
					} else if (apVoucherPaymentSchedule.getVpsDueDate().equals(details.getDcfdDate6())) {
						
						lessDetails.setDcfdlAmount6(dcfdlAmount);
						
					} else if (apVoucherPaymentSchedule.getVpsDueDate().equals(details.getDcfdDate6())) {
						
						lessDetails.setDcfdlAmount7(dcfdlAmount);
						
					}
					
					if (dcfdlAmount!=0)
						dcflList.add(lessDetails);

				}
				
			
				
				
		
			}
			
			// get unreleased check
			
			firstArgument = true;
			ctr = 0;
			criteriaSize = criteria.size();	      
			
			jbossQl = new StringBuffer();
			
			jbossQl.append("SELECT OBJECT(chk) FROM ApCheck chk ");
			
			
			firstArgument = true;
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}

			if (criteria.containsKey("selectedBankAccount")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("date")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("reportType")) {
				
				criteriaSize--;
				
			}

			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("selectedBankAccount")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");		
					
				} else {		       	  	
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("chk.adBankAccount.baName in (");
				
				boolean firstLoop = true;
				String[] adBankAccounts = (String[])criteria.get("selectedBankAccount");
				
				for (int j=0; j < adBankAccounts.length; j++) {
					
					if (adBankAccounts[j] != null) {
						
						if(firstLoop == false) {
							
							jbossQl.append(", ");
							
						} else {
							
							firstLoop = false;
							
						}
						
						jbossQl.append("'" + adBankAccounts[j] + "'");
						
					}

				}
				
				jbossQl.append(") ");
				firstArgument = false;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {
					
					jbossQl.append("chk.chkCheckDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				} else {
					
					jbossQl.append("chk.chkCheckDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("chk.chkCheckDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("((chk.chkPosted = 1 AND chk.chkVoid = 0) OR (chk.chkPosted = 1 AND chk.chkVoid = 1 AND chk.chkVoidPosted = 0)) ");
					
				} else {
					
					jbossQl.append("chk.chkVoid = 0 ");  	  
					
				}   	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" chk.chkReleased = 0 AND chk.chkAdCompany = " + AD_CMPNY + " ");
			dcfld = apCheckHome.getChkByCriteria(jbossQl.toString(), obj);	         
			i = dcfld.iterator();

			while (i.hasNext()){
				
				LocalApCheck apCheck = (LocalApCheck) i.next();
				
				CmRepDailyCashForecastDetailLessDetails lessDetails = new CmRepDailyCashForecastDetailLessDetails();
				
				lessDetails.setDcfdlSupplier(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
				lessDetails.setDcfdlDateTransaction(apCheck.getChkCheckDate());
				lessDetails.setDcfdlDescription (apCheck.getChkDescription());
				lessDetails.setDcfdlVoucherNumber(apCheck.getChkDocumentNumber());

				lessDetails.setDcfdlAmount1(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						apCheck.getGlFunctionalCurrency().getFcCode(), apCheck.getGlFunctionalCurrency().getFcName(),
						apCheck.getChkConversionDate(), apCheck.getChkConversionRate(), apCheck.getChkAmount(),
						AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));

				dcflList.add(lessDetails);

			}

			return dcflList;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	private CmRepDailyCashForecastDetailDetails computeBeginningBalancesAndAvailableCashBalance(CmRepDailyCashForecastDetailDetails details, Integer AD_CMPNY){
		
		Debug.print("CmRepDailyCashForecastDetailControllerBean computeBeginningBalancesAndAvailableCashBalance");
		
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdCompany adCompany = null;
		
		double TOTAL_AMOUNT1 = 0;
		double TOTAL_AMOUNT2 = 0;
		double TOTAL_AMOUNT3 = 0;
		double TOTAL_AMOUNT4 = 0;
		double TOTAL_AMOUNT5 = 0;
		double TOTAL_AMOUNT6 = 0;
		double TOTAL_AMOUNT7 = 0;
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		} catch (FinderException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		ArrayList list = details.getDcfdAddList();
		Iterator i = list.iterator();
		
		while (i.hasNext()){
			
			CmRepDailyCashForecastDetailAddDetails addDetails = (CmRepDailyCashForecastDetailAddDetails) i.next();
			
			TOTAL_AMOUNT1 = TOTAL_AMOUNT1 + addDetails.getDcfdaAmount1();
			TOTAL_AMOUNT2 = TOTAL_AMOUNT2 + addDetails.getDcfdaAmount2();
			TOTAL_AMOUNT3 = TOTAL_AMOUNT3 + addDetails.getDcfdaAmount3();
			TOTAL_AMOUNT4 = TOTAL_AMOUNT4 + addDetails.getDcfdaAmount4();
			TOTAL_AMOUNT5 = TOTAL_AMOUNT5 + addDetails.getDcfdaAmount5();
			TOTAL_AMOUNT6 = TOTAL_AMOUNT6 + addDetails.getDcfdaAmount6();
			TOTAL_AMOUNT7 = TOTAL_AMOUNT7 + addDetails.getDcfdaAmount7();
			
		}
		
		list = details.getDcfdLessList();
		i = list.iterator();
		
		while (i.hasNext()){
			
			CmRepDailyCashForecastDetailLessDetails lessDetails = (CmRepDailyCashForecastDetailLessDetails) i.next();
			
			TOTAL_AMOUNT1 = TOTAL_AMOUNT1 - lessDetails.getDcfdlAmount1();
			TOTAL_AMOUNT2 = TOTAL_AMOUNT2 - lessDetails.getDcfdlAmount2();
			TOTAL_AMOUNT3 = TOTAL_AMOUNT3 - lessDetails.getDcfdlAmount3();
			TOTAL_AMOUNT4 = TOTAL_AMOUNT4 - lessDetails.getDcfdlAmount4();
			TOTAL_AMOUNT5 = TOTAL_AMOUNT5 - lessDetails.getDcfdlAmount5();
			TOTAL_AMOUNT6 = TOTAL_AMOUNT6 - lessDetails.getDcfdlAmount6();
			TOTAL_AMOUNT7 = TOTAL_AMOUNT7 - lessDetails.getDcfdlAmount7();
			
		}
		
		details.setDcfdAvailableCashBalance1(EJBCommon.roundIt(details.getDcfdBeginningBalance1() + TOTAL_AMOUNT1,
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcfdBeginningBalance2(EJBCommon.roundIt(details.getDcfdAvailableCashBalance1(),
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcfdAvailableCashBalance2(EJBCommon.roundIt(details.getDcfdBeginningBalance2() + TOTAL_AMOUNT2,
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcfdBeginningBalance3(EJBCommon.roundIt(details.getDcfdAvailableCashBalance2(),
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcfdAvailableCashBalance3(EJBCommon.roundIt(details.getDcfdBeginningBalance3() + TOTAL_AMOUNT3,
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcfdBeginningBalance4(EJBCommon.roundIt(details.getDcfdAvailableCashBalance3(),
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcfdAvailableCashBalance4(EJBCommon.roundIt(details.getDcfdBeginningBalance4() + TOTAL_AMOUNT4,
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcfdBeginningBalance5(EJBCommon.roundIt(details.getDcfdAvailableCashBalance4(),
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcfdAvailableCashBalance5(EJBCommon.roundIt(details.getDcfdBeginningBalance5() + TOTAL_AMOUNT5,
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcfdBeginningBalance6(EJBCommon.roundIt(details.getDcfdAvailableCashBalance5(),
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcfdAvailableCashBalance6(EJBCommon.roundIt(details.getDcfdBeginningBalance6() + TOTAL_AMOUNT6,
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcfdBeginningBalance7(EJBCommon.roundIt(details.getDcfdAvailableCashBalance6(),
				adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcfdAvailableCashBalance7(EJBCommon.roundIt(details.getDcfdBeginningBalance7() + TOTAL_AMOUNT7,
				adCompany.getGlFunctionalCurrency().getFcPrecision()));		
		
		return details;
		
	}
	
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmRepDailyCashForecastDetailControllerBean ejbCreate");
      
    }
}
