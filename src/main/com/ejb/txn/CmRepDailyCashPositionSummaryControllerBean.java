
/*
 * CmRepDailyCashPositionSummaryControllerBean.java
 *
 * Created on January 23, 2006 9:30 AM
 *
 * @author  Farrah S. Garing
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountBalance;
import com.ejb.ad.LocalAdBankAccountBalanceHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.cm.LocalCmFundTransferReceipt;
import com.ejb.cm.LocalCmFundTransferReceiptHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.CmRepDailyCashPositionSummaryAddDetails;
import com.util.CmRepDailyCashPositionSummaryDetails;
import com.util.CmRepDailyCashPositionSummaryLessDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmRepDailyCashPositionSummaryControllerEJB"
 *           display-name="Used for summarizing daily cash positioning"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmRepDailyCashPositionSummaryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmRepDailyCashPositionSummaryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmRepDailyCashPositionSummaryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 * 
*/

public class CmRepDailyCashPositionSummaryControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("CmRepDailyCashPositionSummaryControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccount adBankAccount = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = adBankAccounts.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	adBankAccount = (LocalAdBankAccount)i.next();
	        	
	        	if (adBankAccount.getBaIsCashAccount() == EJBCommon.FALSE) 
	        		list.add(adBankAccount.getBaName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
  
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public CmRepDailyCashPositionSummaryDetails executeCmRepDailyCashPositionSummary(HashMap criteria, boolean includeDirectChecks, 
    		Integer AD_CMPNY)
    throws GlobalNoRecordFoundException{
                    
        Debug.print("CmRepDailyCashPositionSummaryControllerBean executeCmRepDailyCashPositionSummary");
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccountBalanceHome adBankAccountBalanceHome = null;
        
        //initialized EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            adBankAccountBalanceHome = (LocalAdBankAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBankAccountBalanceHome.JNDI_NAME, LocalAdBankAccountBalanceHome.class);
            

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	LocalAdBankAccount adBankAccount = adBankAccountHome.findByBaName((String)criteria.get("bankAccount"), AD_CMPNY);
        	
    		criteria.put("bankAccountCode", adBankAccount.getBaCode());
        	
	        Date date = null;
        	
        	if (criteria.containsKey("date")){
        		
        		date = (Date)criteria.get("date");
        		
        	}
			
        	String reportType = null;
        	
        	if (criteria.containsKey("reportType")){
        		
        		reportType = (String)criteria.get("reportType");
        		
        	}
        	
    		CmRepDailyCashPositionSummaryDetails mdetails = new CmRepDailyCashPositionSummaryDetails();

        	if (reportType.equals("ONE DAY")) {
        		
        		mdetails.setDcpsDate1(date);
        		
        	} else if (reportType.equals("FIVE DAYS")) {

        		mdetails.setDcpsDate5(date);
        		mdetails.setDcpsDate4(computeDate(mdetails.getDcpsDate5()));
        		mdetails.setDcpsDate3(computeDate(mdetails.getDcpsDate4()));
        		mdetails.setDcpsDate2(computeDate(mdetails.getDcpsDate3()));
        		mdetails.setDcpsDate1(computeDate(mdetails.getDcpsDate2()));

        		criteria.put("dateTo",mdetails.getDcpsDate5());

        	} else if (reportType.equals("SEVEN DAYS")) {

        		mdetails.setDcpsDate7(date);
        		mdetails.setDcpsDate6(computeDate(date));
        		mdetails.setDcpsDate5(computeDate(mdetails.getDcpsDate6()));
        		mdetails.setDcpsDate4(computeDate(mdetails.getDcpsDate5()));
        		mdetails.setDcpsDate3(computeDate(mdetails.getDcpsDate4()));
        		mdetails.setDcpsDate2(computeDate(mdetails.getDcpsDate3()));
        		mdetails.setDcpsDate1(computeDate(mdetails.getDcpsDate2()));
        		criteria.put("dateTo",mdetails.getDcpsDate7());

        	}
        	
        	criteria.put("dateFrom",mdetails.getDcpsDate1());
        	
        	mdetails.setDcpsAddList(executeCmRepDailyCashPositionSummaryAdd(criteria, mdetails, AD_CMPNY));
        	mdetails.setDcpsLessList(executeCmRepDailyCashPositionSummaryLess(criteria, mdetails, includeDirectChecks, AD_CMPNY));
        	
        	if (mdetails.getDcpsAddList().isEmpty() && mdetails.getDcpsLessList().isEmpty())
        		throw new GlobalNoRecordFoundException();
        	
    		Collection adBankAccountBalances = adBankAccountBalanceHome.findByBeforeDateAndBaCodeAndType(mdetails.getDcpsDate1(), adBankAccount.getBaCode(), "BOOK", AD_CMPNY);
    		
    		LocalAdBankAccountBalance adBankAccountBalance = null;
    		
    		if (!adBankAccountBalances.isEmpty()) {
    			
    			Iterator i = adBankAccountBalances.iterator();
    			
    			while(i.hasNext()){
    				
    				adBankAccountBalance = (LocalAdBankAccountBalance) i.next();

    			}

    		}

    		mdetails.setDcpsBeginningBalance1(adBankAccountBalance != null ? adBankAccountBalance.getBabBalance():0);
    		
    		mdetails = computeBeginningBalancesAndAvailableCashBalance(mdetails, AD_CMPNY);
    		
        	return mdetails;

        } catch (GlobalNoRecordFoundException ex) {

        	throw ex;

        } catch (Exception ex) {

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("CmRepDailyCashPositionSummaryControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("CmRepDailyCashPositionSummaryControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
    
	private Date computeDate(Date dateEntered){
		
		Debug.print("CmRepDailyCashPositionSummaryControllerBean computeDate");
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(dateEntered);
		calendar.add(GregorianCalendar.DATE, -1);
		
		return calendar.getTime();
		
	}
	
    
	private ArrayList executeCmRepDailyCashPositionSummaryAdd(HashMap criteria, CmRepDailyCashPositionSummaryDetails details,Integer AD_CMPNY)  {
		
		Debug.print("CmRepDailyCashPositionSummaryControllerBean executeCmRepDailyCashPositionSummaryAdd");
		
		LocalArReceiptHome arReceiptHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalCmFundTransferReceiptHome cmFundTransferReceiptHome = null;
		LocalCmAdjustmentHome cmAdjustmentHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
				lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
				lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			cmFundTransferReceiptHome = (LocalCmFundTransferReceiptHome)EJBHomeFactory.
				lookUpLocalHome(LocalCmFundTransferReceiptHome.JNDI_NAME, LocalCmFundTransferReceiptHome.class);			
			cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);			

		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size() - 3;	      
			
			StringBuffer jbossQl = new StringBuffer();
			
			//get all cash receipt 
			
			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rct.adBankAccount.baName=?" + (ctr+1) + " ");
				

				
				obj[ctr] = (String)criteria.get("bankAccount");
				

				
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {

	        		
					
					jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					
	        		
					ctr++;
					
				} else {
					
					jbossQl.append("rct.rctDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
					
				} else {
					
					jbossQl.append("rct.rctVoid = 0 ");  	  
					
				}   	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" rct.rctAdCompany = " + AD_CMPNY + " ");

			Collection dcpas = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
			Iterator i = dcpas.iterator();
			ArrayList dcpaList = new ArrayList();
			CmRepDailyCashPositionSummaryAddDetails addDetails = new CmRepDailyCashPositionSummaryAddDetails();			

			while (i.hasNext()){
				
				LocalArReceipt arReceipt = (LocalArReceipt) i.next();
				
				addDetails.setDcpsaDescription ("RECEIPT");

				String reportType = (String) criteria.get("reportType");
				
				if (arReceipt.getRctDate().equals(details.getDcpsDate1())) {
					
					addDetails.setDcpsaAmount1(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
				  	  	arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
				  	  	arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount1() + arReceipt.getRctAmount(), AD_CMPNY),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate2())) {
					
					addDetails.setDcpsaAmount2(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
					  	arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount2() + arReceipt.getRctAmount(), AD_CMPNY),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate3())) {
					
					addDetails.setDcpsaAmount3(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount3() + arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate4())) {
					
					addDetails.setDcpsaAmount4(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount4() + arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate5())) {
					
					addDetails.setDcpsaAmount5(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount5() + arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (arReceipt.getRctDate().equals(details.getDcpsDate6())) {
					
					addDetails.setDcpsaAmount6(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount6() + arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (arReceipt.getRctDate().equals(details.getDcpsDate7())) {
					
					addDetails.setDcpsaAmount7(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount7() + arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				}

				
				
			}

			if (addDetails.getDcpsaDescription() != null) {
				
				dcpaList.add(addDetails);
				
			}
			
			
			
			
			
			
			
			
			
			firstArgument = true;
			ctr = 0;
			criteriaSize = criteria.size() - 3;	      
			
			jbossQl = new StringBuffer();
			
			//get all cheque receipt 
			
			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
			      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rct.adBankAccount.baName=?" + (ctr+1) + " ");
				

				
				obj[ctr] = (String)criteria.get("bankAccount");
				

				
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {

	        		
					
					jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					
	        		
					ctr++;
					
				} else {
					
					jbossQl.append("rct.rctDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
					
				} else {
					
					jbossQl.append("rct.rctVoid = 0 ");  	  
					
				}   	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" rct.rctAdCompany = " + AD_CMPNY + " ");

			dcpas = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();
			dcpaList = new ArrayList();
			addDetails = new CmRepDailyCashPositionSummaryAddDetails();			

			while (i.hasNext()){
				
				LocalArReceipt arReceipt = (LocalArReceipt) i.next();
				
				addDetails.setDcpsaDescription ("RECEIPT");

				String reportType = (String) criteria.get("reportType");
				
				if (arReceipt.getRctDate().equals(details.getDcpsDate1())) {
					
					addDetails.setDcpsaAmount1(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
				  	  	arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
				  	  	arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount1() + arReceipt.getRctAmountCheque(), AD_CMPNY),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate2())) {
					
					addDetails.setDcpsaAmount2(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
					  	arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount2() + arReceipt.getRctAmountCheque(), AD_CMPNY),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate3())) {
					
					addDetails.setDcpsaAmount3(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount3() + arReceipt.getRctAmountCheque(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate4())) {
					
					addDetails.setDcpsaAmount4(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount4() + arReceipt.getRctAmountCheque(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate5())) {
					
					addDetails.setDcpsaAmount5(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount5() + arReceipt.getRctAmountCheque(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (arReceipt.getRctDate().equals(details.getDcpsDate6())) {
					
					addDetails.setDcpsaAmount6(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount6() + arReceipt.getRctAmountCheque(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (arReceipt.getRctDate().equals(details.getDcpsDate7())) {
					
					addDetails.setDcpsaAmount7(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount7() + arReceipt.getRctAmountCheque(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				}

				
				
			}

			if (addDetails.getDcpsaDescription() != null) {
				
				dcpaList.add(addDetails);
				
			}
			
			
			
			
			
			
			
			firstArgument = true;
			ctr = 0;
			criteriaSize = criteria.size() - 3;	      
			
			jbossQl = new StringBuffer();
			
			//get all Card1 receipt 
			
			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
			      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rct.adBankAccountCard1.baName=?" + (ctr+1) + " ");
				

				
				obj[ctr] = (String)criteria.get("bankAccount");
				

				
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {

	        		
					
					jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					
	        		
					ctr++;
					
				} else {
					
					jbossQl.append("rct.rctDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
					
				} else {
					
					jbossQl.append("rct.rctVoid = 0 ");  	  
					
				}   	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" rct.rctAdCompany = " + AD_CMPNY + " ");

			dcpas = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();
			dcpaList = new ArrayList();
			addDetails = new CmRepDailyCashPositionSummaryAddDetails();			

			while (i.hasNext()){
				
				LocalArReceipt arReceipt = (LocalArReceipt) i.next();
				
				addDetails.setDcpsaDescription ("RECEIPT");

				String reportType = (String) criteria.get("reportType");
				
				if (arReceipt.getRctDate().equals(details.getDcpsDate1())) {
					
					addDetails.setDcpsaAmount1(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
				  	  	arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
				  	  	arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount1() + arReceipt.getRctAmountCard1(), AD_CMPNY),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate2())) {
					
					addDetails.setDcpsaAmount2(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
					  	arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount2() + arReceipt.getRctAmountCard1(), AD_CMPNY),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate3())) {
					
					addDetails.setDcpsaAmount3(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount3() + arReceipt.getRctAmountCard1(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate4())) {
					
					addDetails.setDcpsaAmount4(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount4() + arReceipt.getRctAmountCard1(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate5())) {
					
					addDetails.setDcpsaAmount5(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount5() + arReceipt.getRctAmountCard1(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (arReceipt.getRctDate().equals(details.getDcpsDate6())) {
					
					addDetails.setDcpsaAmount6(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount6() + arReceipt.getRctAmountCard1(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (arReceipt.getRctDate().equals(details.getDcpsDate7())) {
					
					addDetails.setDcpsaAmount7(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount7() + arReceipt.getRctAmountCard1(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				}

				
				
			}

			if (addDetails.getDcpsaDescription() != null) {
				
				dcpaList.add(addDetails);
				
			}
			
			
			
			
			
			firstArgument = true;
			ctr = 0;
			criteriaSize = criteria.size() - 3;	      
			
			jbossQl = new StringBuffer();
			
			//get all Card2 receipt 
			
			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
			      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rct.adBankAccountCard2.baName=?" + (ctr+1) + " ");
				

				
				obj[ctr] = (String)criteria.get("bankAccount");
				

				
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {

	        		
					
					jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					
	        		
					ctr++;
					
				} else {
					
					jbossQl.append("rct.rctDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
					
				} else {
					
					jbossQl.append("rct.rctVoid = 0 ");  	  
					
				}   	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" rct.rctAdCompany = " + AD_CMPNY + " ");

			dcpas = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();
			dcpaList = new ArrayList();
			addDetails = new CmRepDailyCashPositionSummaryAddDetails();			

			while (i.hasNext()){
				
				LocalArReceipt arReceipt = (LocalArReceipt) i.next();
				
				addDetails.setDcpsaDescription ("RECEIPT");

				String reportType = (String) criteria.get("reportType");
				
				if (arReceipt.getRctDate().equals(details.getDcpsDate1())) {
					
					addDetails.setDcpsaAmount1(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
				  	  	arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
				  	  	arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount1() + arReceipt.getRctAmountCard2(), AD_CMPNY),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate2())) {
					
					addDetails.setDcpsaAmount2(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
					  	arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount2() + arReceipt.getRctAmountCard2(), AD_CMPNY),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate3())) {
					
					addDetails.setDcpsaAmount3(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount3() + arReceipt.getRctAmountCard2(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate4())) {
					
					addDetails.setDcpsaAmount4(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount4() + arReceipt.getRctAmountCard2(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate5())) {
					
					addDetails.setDcpsaAmount5(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount5() + arReceipt.getRctAmountCard2(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (arReceipt.getRctDate().equals(details.getDcpsDate6())) {
					
					addDetails.setDcpsaAmount6(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount6() + arReceipt.getRctAmountCard2(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (arReceipt.getRctDate().equals(details.getDcpsDate7())) {
					
					addDetails.setDcpsaAmount7(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount7() + arReceipt.getRctAmountCard2(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				}

				
				
			}

			if (addDetails.getDcpsaDescription() != null) {
				
				dcpaList.add(addDetails);
				
			}
			
			
			
			
			firstArgument = true;
			ctr = 0;
			criteriaSize = criteria.size() - 3;	      
			
			jbossQl = new StringBuffer();
			
			//get all Card3 receipt 
			
			jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct ");
			      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("rct.adBankAccountCard3.baName=?" + (ctr+1) + " ");
				

				
				obj[ctr] = (String)criteria.get("bankAccount");
				

				
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {

	        		
					
					jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					
	        		
					ctr++;
					
				} else {
					
					jbossQl.append("rct.rctDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("rct.rctPosted = 1 AND rct.rctVoid = 0 ");
					
				} else {
					
					jbossQl.append("rct.rctVoid = 0 ");  	  
					
				}   	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" rct.rctAdCompany = " + AD_CMPNY + " ");

			dcpas = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();
			dcpaList = new ArrayList();
			addDetails = new CmRepDailyCashPositionSummaryAddDetails();			

			while (i.hasNext()){
				
				LocalArReceipt arReceipt = (LocalArReceipt) i.next();
				
				addDetails.setDcpsaDescription ("RECEIPT");

				String reportType = (String) criteria.get("reportType");
				
				if (arReceipt.getRctDate().equals(details.getDcpsDate1())) {
					
					addDetails.setDcpsaAmount1(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
				  	  	arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
				  	  	arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount1() + arReceipt.getRctAmountCard3(), AD_CMPNY),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate2())) {
					
					addDetails.setDcpsaAmount2(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
					  	arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount2() + arReceipt.getRctAmountCard3(), AD_CMPNY),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate3())) {
					
					addDetails.setDcpsaAmount3(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount3() + arReceipt.getRctAmountCard3(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate4())) {
					
					addDetails.setDcpsaAmount4(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount4() + arReceipt.getRctAmountCard3(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( arReceipt.getRctDate().equals(details.getDcpsDate5())) {
					
					addDetails.setDcpsaAmount5(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount5() + arReceipt.getRctAmountCard3(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (arReceipt.getRctDate().equals(details.getDcpsDate6())) {
					
					addDetails.setDcpsaAmount6(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount6() + arReceipt.getRctAmountCard3(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (arReceipt.getRctDate().equals(details.getDcpsDate7())) {
					
					addDetails.setDcpsaAmount7(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						arReceipt.getGlFunctionalCurrency().getFcCode(), 
						arReceipt.getGlFunctionalCurrency().getFcName(), arReceipt.getRctConversionDate(),
						arReceipt.getRctConversionRate(),
						addDetails.getDcpsaAmount7() + arReceipt.getRctAmountCard3(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				}

				
				
			}

			if (addDetails.getDcpsaDescription() != null) {
				
				dcpaList.add(addDetails);
				
			}
			
			
			
			
			
			
			
			
			
			
			
			//	get all deposit transfer receipt
			
			jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(ftr) FROM CmFundTransferReceipt ftr ");
			
			ctr = 0;
			criteriaSize = criteria.size() - 3;
			firstArgument = true;
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccountCode")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("ftr.cmFundTransfer.ftAdBaAccountTo=?" + (ctr+1) + " ");
				obj[ctr] = (Integer)criteria.get("bankAccountCode");
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {
					
					jbossQl.append("ftr.cmFundTransfer.ftDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				} else {
					
					jbossQl.append("ftr.cmFundTransfer.ftDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("ftr.cmFundTransfer.ftDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (unposted.equals("NO")) {				

					jbossQl.append("ftr.cmFundTransfer.ftPosted = 1 " );
					
				} else {
					
					jbossQl.append("ftr.cmFundTransfer.ftVoid = 0 " );
					
				}   	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" ftr.ftrAdCompany = " + AD_CMPNY + " ");

			dcpas = cmFundTransferReceiptHome.getFtrByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();
			addDetails = new CmRepDailyCashPositionSummaryAddDetails();
			
			while (i.hasNext()){
				
				LocalCmFundTransferReceipt cmFundTransferReceipt = (LocalCmFundTransferReceipt) i.next();
				addDetails.setDcpsaDescription ("DEPOSITED RECEIPT");
				String reportType = (String) criteria.get("reportType");
				
				if (cmFundTransferReceipt.getCmFundTransfer().getFtDate().equals(details.getDcpsDate1())) {
					
					addDetails.setDcpsaAmount1(
						EJBCommon.roundIt(addDetails.getDcpsaAmount1() +
								cmFundTransferReceipt.getFtrAmountDeposited(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmFundTransferReceipt.getCmFundTransfer().getFtDate().equals(details.getDcpsDate2())) {
					
					addDetails.setDcpsaAmount2(
						EJBCommon.roundIt(addDetails.getDcpsaAmount2() +
								cmFundTransferReceipt.getFtrAmountDeposited(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmFundTransferReceipt.getCmFundTransfer().getFtDate().equals(details.getDcpsDate3())) {
					
					addDetails.setDcpsaAmount3(
						EJBCommon.roundIt(addDetails.getDcpsaAmount3() +
								cmFundTransferReceipt.getFtrAmountDeposited(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmFundTransferReceipt.getCmFundTransfer().getFtDate().equals(details.getDcpsDate4())) {
					
					addDetails.setDcpsaAmount4(
						EJBCommon.roundIt(addDetails.getDcpsaAmount4() +
								cmFundTransferReceipt.getFtrAmountDeposited(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmFundTransferReceipt.getCmFundTransfer().getFtDate().equals(details.getDcpsDate5())) {
					
					addDetails.setDcpsaAmount5(
						EJBCommon.roundIt(addDetails.getDcpsaAmount5() +
								cmFundTransferReceipt.getFtrAmountDeposited(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (cmFundTransferReceipt.getCmFundTransfer().getFtDate().equals(details.getDcpsDate6())) {
					
					addDetails.setDcpsaAmount6(
						EJBCommon.roundIt(addDetails.getDcpsaAmount6() +
								cmFundTransferReceipt.getFtrAmountDeposited(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (cmFundTransferReceipt.getCmFundTransfer().getFtDate().equals(details.getDcpsDate7())) {
					
					addDetails.setDcpsaAmount7(
						EJBCommon.roundIt(addDetails.getDcpsaAmount7() + 
								cmFundTransferReceipt.getFtrAmountDeposited(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				}

			}

			if (addDetails.getDcpsaDescription() != null) {
				
				dcpaList.add(addDetails);
				
			}
			
			//	get all fund transfer to
			
			jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(ft) FROM CmFundTransfer ft ");
			
			ctr = 0;
			criteriaSize = criteria.size() - 3;
			firstArgument = true;
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccountCode")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("ft.ftAdBaAccountTo=?" + (ctr+1) + " ");
				obj[ctr] = (Integer)criteria.get("bankAccountCode");
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {
					
					jbossQl.append("ft.ftDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				} else {
					
					jbossQl.append("ft.ftDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("ft.ftDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
								
				if (unposted.equals("NO")) {

					jbossQl.append("ft.ftPosted = 1 " );
					
				} else {
					
					jbossQl.append("ft.ftVoid = 0 " );
					
				}   	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" ft.ftAdCompany = " + AD_CMPNY + " ");

			dcpas = cmFundTransferHome.getFtByCriteria(jbossQl.toString(), obj);	         
			i = dcpas.iterator();
			addDetails = new CmRepDailyCashPositionSummaryAddDetails();
			
			while (i.hasNext()){
				
				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer) i.next();
				
				Collection cmFundTransferReceipts = cmFundTransfer.getCmFundTransferReceipts();
				
				if (cmFundTransferReceipts.isEmpty()) {

					addDetails.setDcpsaDescription ("FUND TRANSFER");
					
					String reportType = (String) criteria.get("reportType");
					
					if (cmFundTransfer.getFtDate().equals(details.getDcpsDate1())) {
						
						addDetails.setDcpsaAmount1(
							EJBCommon.roundIt(addDetails.getDcpsaAmount1() + cmFundTransfer.getFtAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
						
					} else if ( cmFundTransfer.getFtDate().equals(details.getDcpsDate2())) {
						
						addDetails.setDcpsaAmount2(
							EJBCommon.roundIt(addDetails.getDcpsaAmount2() + cmFundTransfer.getFtAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
						
					} else if ( cmFundTransfer.getFtDate().equals(details.getDcpsDate3())) {
						
						addDetails.setDcpsaAmount3(
							EJBCommon.roundIt(addDetails.getDcpsaAmount3() + cmFundTransfer.getFtAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
						
					} else if ( cmFundTransfer.getFtDate().equals(details.getDcpsDate4())) {
						
						addDetails.setDcpsaAmount4(
							EJBCommon.roundIt(addDetails.getDcpsaAmount4() + cmFundTransfer.getFtAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
						
					} else if ( cmFundTransfer.getFtDate().equals(details.getDcpsDate5())) {
						
						addDetails.setDcpsaAmount5(
							EJBCommon.roundIt(addDetails.getDcpsaAmount5() + cmFundTransfer.getFtAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
						
					} else if (cmFundTransfer.getFtDate().equals(details.getDcpsDate6())) {
						
						addDetails.setDcpsaAmount6(
							EJBCommon.roundIt(addDetails.getDcpsaAmount6() + cmFundTransfer.getFtAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
						
					} else if (cmFundTransfer.getFtDate().equals(details.getDcpsDate7())) {
						
						addDetails.setDcpsaAmount7(
							EJBCommon.roundIt(addDetails.getDcpsaAmount7() + cmFundTransfer.getFtAmount(),
								adCompany.getGlFunctionalCurrency().getFcPrecision()));
						
					}

				}

			}
			
			if (addDetails.getDcpsaDescription() != null) {
				
				dcpaList.add(addDetails);
				
			}

			//	get all positve adjustments
			
			jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(adj) FROM CmAdjustment adj ");
			
			ctr = 0;
			criteriaSize = criteria.size() - 3;
			firstArgument = true;
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {	
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("adj.adBankAccount.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
				
			}
			
			

			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {
					
					jbossQl.append("adj.adjDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				} else {
					
					jbossQl.append("adj.adjDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("adj.adjDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				if (unposted.equals("NO")) {

					jbossQl.append("adj.adjPosted = 1 " );
					
				} else {
					
					jbossQl.append("adj.adjVoid = 0 " );
					
				}   	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" (adj.adjType = 'INTEREST' OR adj.adjType = 'DEBIT MEMO' OR adj.adjType = 'ADVANCE') AND adj.adjAdCompany = " + AD_CMPNY + " ");

			dcpas = cmAdjustmentHome.getAdjByCriteria(jbossQl.toString(), obj);	 
			i = dcpas.iterator();
			addDetails = new CmRepDailyCashPositionSummaryAddDetails();
			
			while (i.hasNext()){
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment) i.next();

				addDetails.setDcpsaDescription ("ADJUSTMENT");
				
				String reportType = (String) criteria.get("reportType");
				
				if (cmAdjustment.getAdjDate().equals(details.getDcpsDate1())) {
					
					addDetails.setDcpsaAmount1(
						EJBCommon.roundIt(addDetails.getDcpsaAmount1() + cmAdjustment.getAdjAmount(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmAdjustment.getAdjDate().equals(details.getDcpsDate2())) {
					
					addDetails.setDcpsaAmount2(
						EJBCommon.roundIt(addDetails.getDcpsaAmount2() + cmAdjustment.getAdjAmount(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmAdjustment.getAdjDate().equals(details.getDcpsDate3())) {
					
					addDetails.setDcpsaAmount3(
						EJBCommon.roundIt(addDetails.getDcpsaAmount3() + cmAdjustment.getAdjAmount(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmAdjustment.getAdjDate().equals(details.getDcpsDate4())) {
					
					addDetails.setDcpsaAmount4(
						EJBCommon.roundIt(addDetails.getDcpsaAmount4() + cmAdjustment.getAdjAmount(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmAdjustment.getAdjDate().equals(details.getDcpsDate5())) {
					
					addDetails.setDcpsaAmount5(
						EJBCommon.roundIt(addDetails.getDcpsaAmount5() + cmAdjustment.getAdjAmount(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (cmAdjustment.getAdjDate().equals(details.getDcpsDate6())) {
					
					addDetails.setDcpsaAmount6(
						EJBCommon.roundIt(addDetails.getDcpsaAmount6() + cmAdjustment.getAdjAmount(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (cmAdjustment.getAdjDate().equals(details.getDcpsDate7())) {

					addDetails.setDcpsaAmount7(
						EJBCommon.roundIt(addDetails.getDcpsaAmount7() + cmAdjustment.getAdjAmount(),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				}

			}

			if (addDetails.getDcpsaDescription() != null) {
			
				dcpaList.add(addDetails);
			
			}
			
			return dcpaList;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
    
    
	private ArrayList executeCmRepDailyCashPositionSummaryLess(HashMap criteria, CmRepDailyCashPositionSummaryDetails details,
			boolean includeDirectChecks, Integer AD_CMPNY)  {
		
		Debug.print("CmRepDailyCashPositionSummaryControllerBean executeCmRepDailyCashPositionSummaryLess");
		
		LocalApCheckHome apCheckHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalCmFundTransferHome cmFundTransferHome = null;
		LocalCmAdjustmentHome cmAdjustmentHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			apCheckHome = (LocalApCheckHome)EJBHomeFactory.
				lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
				lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
			cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);			

		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size() - 3;	      
			
			StringBuffer jbossQl = new StringBuffer();
			
			//get all receipt 
			
			jbossQl.append("SELECT OBJECT(chk) FROM ApCheck chk ");
			
			
			firstArgument = true;
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("chk.adBankAccount.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {
					
					jbossQl.append("chk.chkCheckDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				} else {
					
					jbossQl.append("chk.chkCheckDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("chk.chkCheckDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				if (unposted.equals("NO")) {
					
					jbossQl.append("chk.chkPosted = 1 AND chk.chkVoid = 0 ");
					
				} else {
					
					jbossQl.append("chk.chkVoid = 0 ");  	  
					
				}   	 
				
			}	
			
			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			if (includeDirectChecks==true) {
				jbossQl.append("(chk.chkReleased = 1 OR (chk.chkType='DIRECT' AND chk.chkReleased = 0)) ");

			}else{
				jbossQl.append("chk.chkReleased = 1 ");
			}

			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" chk.chkAdCompany = " + AD_CMPNY + " ");

			Collection dcpls = apCheckHome.getChkByCriteria(jbossQl.toString(), obj);
			System.out.println("jbossQl.toString(): " + jbossQl.toString());
			Iterator i = dcpls.iterator();
			ArrayList dcplList = new ArrayList();
			CmRepDailyCashPositionSummaryLessDetails lessDetails = new CmRepDailyCashPositionSummaryLessDetails();			
			
			while (i.hasNext()){
				
				LocalApCheck apCheck = (LocalApCheck) i.next();
				lessDetails.setDcpslDescription ("RELEASED CHECK");

				String reportType = (String) criteria.get("reportType");
				
				if (apCheck.getChkDate().equals(details.getDcpsDate1())) {
					
					lessDetails.setDcpslAmount1(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
						apCheck.getGlFunctionalCurrency().getFcCode(), apCheck.getGlFunctionalCurrency().getFcName(),
						apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
						lessDetails.getDcpslAmount1() + apCheck.getChkAmount(), AD_CMPNY),
						adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( apCheck.getChkDate().equals(details.getDcpsDate2())) {
					
					lessDetails.setDcpslAmount2(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
							apCheck.getGlFunctionalCurrency().getFcCode(), apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
							lessDetails.getDcpslAmount2() + apCheck.getChkAmount(), AD_CMPNY),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( apCheck.getChkDate().equals(details.getDcpsDate3())) {
					
					lessDetails.setDcpslAmount3(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
							apCheck.getGlFunctionalCurrency().getFcCode(), apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
							lessDetails.getDcpslAmount3() + apCheck.getChkAmount(), AD_CMPNY),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( apCheck.getChkDate().equals(details.getDcpsDate4())) {
					
					lessDetails.setDcpslAmount4(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
							apCheck.getGlFunctionalCurrency().getFcCode(), apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
							lessDetails.getDcpslAmount4() + apCheck.getChkAmount(), AD_CMPNY),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( apCheck.getChkDate().equals(details.getDcpsDate5())) {
					
					lessDetails.setDcpslAmount5(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
							apCheck.getGlFunctionalCurrency().getFcCode(), apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
							lessDetails.getDcpslAmount5() + apCheck.getChkAmount(), AD_CMPNY),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (apCheck.getChkDate().equals(details.getDcpsDate6())) {
					
					lessDetails.setDcpslAmount6(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
							apCheck.getGlFunctionalCurrency().getFcCode(), apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
							lessDetails.getDcpslAmount6() + apCheck.getChkAmount(), AD_CMPNY),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (apCheck.getChkDate().equals(details.getDcpsDate7())) {
					
					lessDetails.setDcpslAmount7(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
							apCheck.getGlFunctionalCurrency().getFcCode(), apCheck.getGlFunctionalCurrency().getFcName(),
							apCheck.getChkConversionDate(), apCheck.getChkConversionRate(),
							lessDetails.getDcpslAmount7() + apCheck.getChkAmount(), AD_CMPNY),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				}

			}
			
			if (lessDetails.getDcpslDescription() != null) {
			
				dcplList.add(lessDetails);
				
			}
			

			//	get all fund transfer from
			
			jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(ft) FROM CmFundTransfer ft ");
			
			ctr = 0;
			criteriaSize = criteria.size() - 3;
			firstArgument = true;
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccountCode")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("ft.ftAdBaAccountFrom=?" + (ctr+1) + " ");
				obj[ctr] = (Integer)criteria.get("bankAccountCode");
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {
					
					jbossQl.append("ft.ftDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				} else {
					
					jbossQl.append("ft.ftDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("ft.ftDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
				
				if (unposted.equals("NO")) {

					jbossQl.append("ft.ftPosted = 1 " );
					
				} else {
					
					jbossQl.append("ft.ftVoid = 0 " );
					
				}   	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" ft.ftAdCompany = " + AD_CMPNY + " ");

			dcpls = cmFundTransferHome.getFtByCriteria(jbossQl.toString(), obj);	         
			i = dcpls.iterator();
			lessDetails = new CmRepDailyCashPositionSummaryLessDetails();
			
			while (i.hasNext()){
				
				LocalCmFundTransfer cmFundTransfer = (LocalCmFundTransfer) i.next();

				lessDetails.setDcpslDescription ("FUND TRANSFER");
				
				String reportType = (String) criteria.get("reportType");
				
				if (cmFundTransfer.getFtDate().equals(details.getDcpsDate1())) {
					
					lessDetails.setDcpslAmount1(
							EJBCommon.roundIt(lessDetails.getDcpslAmount1() + cmFundTransfer.getFtAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmFundTransfer.getFtDate().equals(details.getDcpsDate2())) {
					
					lessDetails.setDcpslAmount2(
							EJBCommon.roundIt(lessDetails.getDcpslAmount2() + cmFundTransfer.getFtAmount(),
									adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmFundTransfer.getFtDate().equals(details.getDcpsDate3())) {
					
					lessDetails.setDcpslAmount3(
							EJBCommon.roundIt(lessDetails.getDcpslAmount3() + cmFundTransfer.getFtAmount(),
									adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmFundTransfer.getFtDate().equals(details.getDcpsDate4())) {
					
					lessDetails.setDcpslAmount4(
							EJBCommon.roundIt(lessDetails.getDcpslAmount4() + cmFundTransfer.getFtAmount(),
									adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmFundTransfer.getFtDate().equals(details.getDcpsDate5())) {
					
					lessDetails.setDcpslAmount5(
							EJBCommon.roundIt(lessDetails.getDcpslAmount5() + cmFundTransfer.getFtAmount(),
									adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (cmFundTransfer.getFtDate().equals(details.getDcpsDate6())) {
					
					lessDetails.setDcpslAmount6(
							EJBCommon.roundIt(lessDetails.getDcpslAmount6() + cmFundTransfer.getFtAmount(),
									adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (cmFundTransfer.getFtDate().equals(details.getDcpsDate7())) {
					
					lessDetails.setDcpslAmount7(
							EJBCommon.roundIt(lessDetails.getDcpslAmount7() + cmFundTransfer.getFtAmount(),
									adCompany.getGlFunctionalCurrency().getFcPrecision()));
				}

			}
			
			if(lessDetails.getDcpslDescription() != null){ 
			
				dcplList.add(lessDetails);
			
			}

			//	get all negative adjustments
			
			jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(adj) FROM CmAdjustment adj ");
			
			ctr = 0;
			criteriaSize = criteria.size() - 3;
			firstArgument = true;
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includedUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("bankAccount")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("adj.adBankAccount.baName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("bankAccount");
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				if (criteria.containsKey("dateTo")) {
					
					jbossQl.append("adj.adjDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				} else {
					
					jbossQl.append("adj.adjDate=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("adj.adjDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("includedUnposted")) {
				
				String unposted = (String)criteria.get("includedUnposted");
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	       	  
								
				if (unposted.equals("NO")) {

					jbossQl.append("adj.adjPosted = 1 " );
					
				} else {
					
					jbossQl.append("adj.adjVoid = 0 " );
					
				}   	 
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append(" (adj.adjType = 'BANK CHARGE' OR adj.adjType = 'CREDIT MEMO') AND adj.adjAdCompany = " + AD_CMPNY + " ");

			dcpls = cmAdjustmentHome.getAdjByCriteria(jbossQl.toString(), obj);	         
			i = dcpls.iterator();
			lessDetails = new CmRepDailyCashPositionSummaryLessDetails();

			while (i.hasNext()){
				
				LocalCmAdjustment cmAdjustment = (LocalCmAdjustment) i.next();

				lessDetails.setDcpslDescription ("ADJUSTMENT");
				
				String reportType = (String) criteria.get("reportType");
				
				if (cmAdjustment.getAdjDate().equals(details.getDcpsDate1())) {
					
					lessDetails.setDcpslAmount1(
						EJBCommon.roundIt(lessDetails.getDcpslAmount1() + cmAdjustment.getAdjAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmAdjustment.getAdjDate().equals(details.getDcpsDate2())) {
					
					lessDetails.setDcpslAmount2(
						EJBCommon.roundIt(lessDetails.getDcpslAmount2() + cmAdjustment.getAdjAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmAdjustment.getAdjDate().equals(details.getDcpsDate3())) {
					
					lessDetails.setDcpslAmount3(
						EJBCommon.roundIt(lessDetails.getDcpslAmount3() + cmAdjustment.getAdjAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmAdjustment.getAdjDate().equals(details.getDcpsDate4())) {
					
					lessDetails.setDcpslAmount4(
						EJBCommon.roundIt(lessDetails.getDcpslAmount4() + cmAdjustment.getAdjAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if ( cmAdjustment.getAdjDate().equals(details.getDcpsDate5())) {
					
					lessDetails.setDcpslAmount5(
						EJBCommon.roundIt(lessDetails.getDcpslAmount5() + cmAdjustment.getAdjAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (cmAdjustment.getAdjDate().equals(details.getDcpsDate6())) {
					
					lessDetails.setDcpslAmount6(
						EJBCommon.roundIt(lessDetails.getDcpslAmount6() + cmAdjustment.getAdjAmount(),
								adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				} else if (cmAdjustment.getAdjDate().equals(details.getDcpsDate7())) {
					
					lessDetails.setDcpslAmount7(
						EJBCommon.roundIt(lessDetails.getDcpslAmount7() + cmAdjustment.getAdjAmount(),
							adCompany.getGlFunctionalCurrency().getFcPrecision()));
					
				}

			}
			
			if (lessDetails.getDcpslDescription() != null){
			
				dcplList.add(lessDetails);
				
			}
			
			
			return dcplList;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	private CmRepDailyCashPositionSummaryDetails computeBeginningBalancesAndAvailableCashBalance(CmRepDailyCashPositionSummaryDetails details, Integer AD_CMPNY){
		
		Debug.print("CmRepDailyCashPositionSummaryControllerBean computeBeginningBalancesAndAvailableCashBalance");
		
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdCompany adCompany = null;
		
		double TOTAL_AMOUNT1 = 0;
		double TOTAL_AMOUNT2 = 0;
		double TOTAL_AMOUNT3 = 0;
		double TOTAL_AMOUNT4 = 0;
		double TOTAL_AMOUNT5 = 0;
		double TOTAL_AMOUNT6 = 0;
		double TOTAL_AMOUNT7 = 0;
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		} catch (FinderException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}

		ArrayList list = details.getDcpsAddList();
		Iterator i = list.iterator();
		
		while (i.hasNext()){
			
			CmRepDailyCashPositionSummaryAddDetails addDetails = (CmRepDailyCashPositionSummaryAddDetails) i.next();
			
			TOTAL_AMOUNT1 = TOTAL_AMOUNT1 + addDetails.getDcpsaAmount1();
			TOTAL_AMOUNT2 = TOTAL_AMOUNT2 + addDetails.getDcpsaAmount2();
			TOTAL_AMOUNT3 = TOTAL_AMOUNT3 + addDetails.getDcpsaAmount3();
			TOTAL_AMOUNT4 = TOTAL_AMOUNT4 + addDetails.getDcpsaAmount4();
			TOTAL_AMOUNT5 = TOTAL_AMOUNT5 + addDetails.getDcpsaAmount5();
			TOTAL_AMOUNT6 = TOTAL_AMOUNT6 + addDetails.getDcpsaAmount6();
			TOTAL_AMOUNT7 = TOTAL_AMOUNT7 + addDetails.getDcpsaAmount7();
			
		}

		list = details.getDcpsLessList();
		i = list.iterator();
		
		while (i.hasNext()){
			
			CmRepDailyCashPositionSummaryLessDetails lessDetails = (CmRepDailyCashPositionSummaryLessDetails) i.next();
			
			TOTAL_AMOUNT1 = TOTAL_AMOUNT1 - lessDetails.getDcpslAmount1();
			TOTAL_AMOUNT2 = TOTAL_AMOUNT2 - lessDetails.getDcpslAmount2();
			TOTAL_AMOUNT3 = TOTAL_AMOUNT3 - lessDetails.getDcpslAmount3();
			TOTAL_AMOUNT4 = TOTAL_AMOUNT4 - lessDetails.getDcpslAmount4();
			TOTAL_AMOUNT5 = TOTAL_AMOUNT5 - lessDetails.getDcpslAmount5();
			TOTAL_AMOUNT6 = TOTAL_AMOUNT6 - lessDetails.getDcpslAmount6();
			TOTAL_AMOUNT7 = TOTAL_AMOUNT7 - lessDetails.getDcpslAmount7();
			
		}

		details.setDcpsAvailableCashBalance1(EJBCommon.roundIt(details.getDcpsBeginningBalance1() + TOTAL_AMOUNT1,
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcpsBeginningBalance2(EJBCommon.roundIt(details.getDcpsAvailableCashBalance1(),
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcpsAvailableCashBalance2(EJBCommon.roundIt(details.getDcpsBeginningBalance2() + TOTAL_AMOUNT2,
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcpsBeginningBalance3(EJBCommon.roundIt(details.getDcpsAvailableCashBalance2(),
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcpsAvailableCashBalance3(EJBCommon.roundIt(details.getDcpsBeginningBalance3() + TOTAL_AMOUNT3,
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcpsBeginningBalance4(EJBCommon.roundIt(details.getDcpsAvailableCashBalance3(),
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcpsAvailableCashBalance4(EJBCommon.roundIt(details.getDcpsBeginningBalance4() + TOTAL_AMOUNT4,
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcpsBeginningBalance5(EJBCommon.roundIt(details.getDcpsAvailableCashBalance4(),
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcpsAvailableCashBalance5(EJBCommon.roundIt(details.getDcpsBeginningBalance5() + TOTAL_AMOUNT5,
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcpsBeginningBalance6(EJBCommon.roundIt(details.getDcpsAvailableCashBalance5(),
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcpsAvailableCashBalance6(EJBCommon.roundIt(details.getDcpsBeginningBalance6() + TOTAL_AMOUNT6,
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		details.setDcpsBeginningBalance7(EJBCommon.roundIt(details.getDcpsAvailableCashBalance6(),
			adCompany.getGlFunctionalCurrency().getFcPrecision()));
		
		details.setDcpsAvailableCashBalance7(EJBCommon.roundIt(details.getDcpsBeginningBalance7() + TOTAL_AMOUNT7,
			adCompany.getGlFunctionalCurrency().getFcPrecision()));

		return details;
		
	}
	
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmRepDailyCashPositionSummaryControllerBean ejbCreate");
      
    }
}
