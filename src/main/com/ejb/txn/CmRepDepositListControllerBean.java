
/*
 * CmRepDepositListControllerBean.java
 *
 * Created on Decmeber 06 , 01:20 PM
 *
 * @author  Farrah S. Garing
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerType;
import com.ejb.ar.LocalArCustomerTypeHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.cm.LocalCmFundTransferReceipt;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.CmRepDepositListDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmRepDepositListControllerEJB"
 *           display-name="Used for viewing offial receipts"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmRepDepositListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmRepDepositListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmRepDepositListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class CmRepDepositListControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCcAll(Integer AD_CMPNY) {
                    
        Debug.print("CmRepDepositListControllerBean getArCcAll");
        
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalArCustomerClass arCustomerClass = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerClasses = arCustomerClassHome.findEnabledCcAll(AD_CMPNY);
	        
	        Iterator i = arCustomerClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerClass = (LocalArCustomerClass)i.next();
	        	
	        	list.add(arCustomerClass.getCcName());
	        	
	        }
	        
	        return list;
          
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCtAll(Integer AD_CMPNY) {
                    
        Debug.print("CmRepDepositListControllerBean getArCtAll");
        
        LocalArCustomerTypeHome arCustomerTypeHome = null;
        LocalArCustomerType arCustomerType = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerTypeHome = (LocalArCustomerTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerTypeHome.JNDI_NAME, LocalArCustomerTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomerTypes = arCustomerTypeHome.findEnabledCtAll(AD_CMPNY);

	        Iterator i = arCustomerTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	arCustomerType = (LocalArCustomerType)i.next();
	        	
	        	list.add(arCustomerType.getCtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("CmRepDepositListControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalAdBankAccount adBankAccount = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = adBankAccounts.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	adBankAccount = (LocalAdBankAccount)i.next();
	        	
	        	if (adBankAccount.getBaIsCashAccount() == EJBCommon.TRUE) 
	        		list.add(adBankAccount.getBaName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("CmRepDepositListControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeCmRepDepositList(HashMap criteria, ArrayList branchList, String ORDER_BY, String GROUP_BY,  String status, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("CmRepDepositListControllerBean executeCmRepDepositList");
        
        LocalArReceiptHome arReceiptHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
                lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
		
		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           
		  boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(rct) FROM ArReceipt rct WHERE (");
		  
		  if (branchList.isEmpty())
		  	throw new GlobalNoRecordFoundException();

		  Iterator brIter = branchList.iterator();
		  
		  AdBranchDetails details = (AdBranchDetails)brIter.next();
		  jbossQl.append("rct.rctAdBranch=" + details.getBrCode());
		  
		  while(brIter.hasNext()) {
		  	
		  		details = (AdBranchDetails)brIter.next();
		  		
		  		jbossQl.append(" OR rct.rctAdBranch=" + details.getBrCode());
		  	
		  }
		  
		  jbossQl.append(") ");
		  firstArgument = false;
		  
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("customerCode")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

          if (criteria.containsKey("includedUnposted")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
          
          obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("customerCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("rct.arCustomer.cstCustomerCode LIKE '%" + (String)criteria.get("customerCode") + "%' ");
		  	 
		  }

		  if (criteria.containsKey("bankAccount")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("rct.adBankAccount.baName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("bankAccount");
		   	  ctr++;
	   	  
	      }

		  if (criteria.containsKey("customerType")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("rct.arCustomer.arCustomerType.ctName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerType");
		   	  ctr++;
	   	  
	      }	
			
		  if (criteria.containsKey("customerClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("rct.arCustomer.arCustomerClass.ccName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerClass");
		   	  ctr++;
	   	  
	      }			     
          			
	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("rct.rctDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("rct.rctDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }
		  
		  if (criteria.containsKey("receiptType")) {
		  	
		  	if (!firstArgument) {
		  		jbossQl.append("AND ");
		  	} else {
		  		firstArgument = false;
		  		jbossQl.append("WHERE ");
		  	}
		  	jbossQl.append("rct.rctType>=?" + (ctr+1) + " ");
		  	obj[ctr] = (String)criteria.get("receiptType");
		  	ctr++;
		  }
		  
		  if (criteria.containsKey("receiptNumberFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("rct.rctNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("receiptNumberFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("receiptNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("rct.rctNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("receiptNumberTo");
		  	 ctr++;
		  	 
		  }
	      
	      if (criteria.containsKey("includedUnposted")) {
	          
	          String unposted = (String)criteria.get("includedUnposted");
	          
	          if (!firstArgument) {
		       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }	       	  
	                  	
	       	  if (unposted.equals("NO")) {
	       			       	  		       	  
				  jbossQl.append("((rct.rctPosted = 1 AND rct.rctVoid = 0) OR (rct.rctPosted = 1 AND rct.rctVoid = 1 AND rct.rctVoidPosted = 0)) ");
		       	  
	       	  } else {
	       	  	
	       	      jbossQl.append("rct.rctVoid = 0 ");  	  
	       	  	
	       	  }   	 
	       	  	       	  
	      }	

	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append(" rct.adBankAccount.baIsCashAccount = 1 AND rct.rctAdCompany = " + AD_CMPNY + " ");
			
       	  Collection arReceipts = arReceiptHome.getRctByCriteria(jbossQl.toString(), obj);	         
		  
		  if (arReceipts.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = arReceipts.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalArReceipt arReceipt = (LocalArReceipt)i.next();   	  
		  	  
		  	  CmRepDepositListDetails mdetails = new CmRepDepositListDetails();
		  	  mdetails.setDlDate(arReceipt.getRctDate());
		  	  mdetails.setDlReceiptNumber(arReceipt.getRctNumber());
		  	  mdetails.setDlReferenceNumber(arReceipt.getRctReferenceNumber());
		  	  mdetails.setDlDescription(arReceipt.getRctDescription());		  	  
		  	  mdetails.setDlCstCustomerCode(arReceipt.getArCustomer().getCstCustomerCode() + "-" + 
		  			  arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());	
		  	  mdetails.setDlCstCustomerClass(arReceipt.getArCustomer().getArCustomerClass().getCcName());
		  	  
		  	  // type
		  	  if(arReceipt.getArCustomer().getArCustomerType() == null) {
		  	  
		  	  	mdetails.setDlCstCustomerType("UNDEFINE");
		  	  
		  	  } else {
		  	  
		  	  	mdetails.setDlCstCustomerType(arReceipt.getArCustomer().getArCustomerType().getCtName());
		  	  
		  	  }
		  	  
		  	  mdetails.setDlAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
		  	  		arReceipt.getGlFunctionalCurrency().getFcCode(),
		  	  		arReceipt.getGlFunctionalCurrency().getFcName(),
		  	  		arReceipt.getRctConversionDate(),
		  	  		arReceipt.getRctConversionRate(),
		  	  		arReceipt.getRctAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
		  	  mdetails.setDlBankAccount(arReceipt.getAdBankAccount().getBaName());
		  	  mdetails.setOrderBy(ORDER_BY);
		  	  
		  	  Collection cmFundTransferReceipt = arReceipt.getCmFundTransferReceipts();
		  	  
		  	  Iterator iFTR = cmFundTransferReceipt.iterator();

		  	  double TTL_DPSTD_AMNT = 0;
		  	  
		  	  while (iFTR.hasNext()) {
		  	  	
		  	  	LocalCmFundTransferReceipt cmFundTransferReciept =  (LocalCmFundTransferReceipt) iFTR.next();
		  	  	
		  	  	if (((String)criteria.get("includedUnposted")).equals("YES") || 
		  	  		(((String)criteria.get("includedUnposted")).equals("NO") &&
		  	  			(cmFundTransferReciept.getCmFundTransfer().getFtPosted() == 1)))
		  	  	
		  	  		TTL_DPSTD_AMNT = TTL_DPSTD_AMNT + cmFundTransferReciept.getFtrAmountDeposited();

		  	  }

		  	  mdetails.setDlAmountDeposited(TTL_DPSTD_AMNT);
		  	  
		  	if ((status.equals("")) || (status.equals("UNDEPOSITED") && TTL_DPSTD_AMNT <= 0 ) ||
		  			(status.equals("DEPOSITED") && TTL_DPSTD_AMNT > 0)) {
		  		
		  		list.add(mdetails);
		  		
		  	} 
		  	
		  }
			     
		  // sort
		  
		  if (GROUP_BY.equals("CUSTOMER CODE")) {
		  	
		  	Collections.sort(list, CmRepDepositListDetails.CustomerCodeComparator);
		  	
		  } else if (GROUP_BY.equals("CUSTOMER TYPE")) {
		  	
		  	Collections.sort(list, CmRepDepositListDetails.CustomerTypeComparator);
		  	
		  } else if (GROUP_BY.equals("CUSTOMER CLASS")) {
		  	
		  	Collections.sort(list, CmRepDepositListDetails.CustomerClassComparator);
		  	
		  } else {
		  	
		  	Collections.sort(list, CmRepDepositListDetails.NoGroupComparator);
		  	
		  }
       	  
		  if (list.size() > 0 ){
		  	
		  	return list;
		  	
		  } else {
		  	
		  	throw new GlobalNoRecordFoundException();
		  	
		  }
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("CmRepDepositListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("CmRepDepositListControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmRepDepositListControllerBean ejbCreate");
      
    }
}
