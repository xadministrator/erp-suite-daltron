
/*
 * CmRepReleasedChecksControllerBean.java
 *
 * Created on December 08, 2005, 04:44 PM
 *
 * @author  Farrah S. Garing
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierType;
import com.ejb.ap.LocalApSupplierTypeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.CmRepReleasedChecksDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="CmRepReleasedChecksControllerEJB"
 *           display-name="Used for finding check registers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/CmRepReleasedChecksControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.CmRepReleasedChecksController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.CmRepReleasedChecksControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="apuser"
 *                        role-link="apuserlink"
 *
 * @ejb:permission role-name="apuser"
 * 
*/

public class CmRepReleasedChecksControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApScAll(Integer AD_CMPNY) {
                    
        Debug.print("CmRepReleasedChecksControllerBean getApScAll");
        
        LocalApSupplierClassHome apSupplierClassHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection apSupplierClasses = apSupplierClassHome.findEnabledScAll(AD_CMPNY);

	        Iterator i = apSupplierClasses.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApSupplierClass apSupplierClass = (LocalApSupplierClass)i.next();
	        	
	        	list.add(apSupplierClass.getScName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getApStAll(Integer AD_CMPNY) {
                    
        Debug.print("CmRepReleasedChecksControllerBean getApStAll");
        
        LocalApSupplierTypeHome apSupplierTypeHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            apSupplierTypeHome = (LocalApSupplierTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierTypeHome.JNDI_NAME, LocalApSupplierTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection apSupplierTypes = apSupplierTypeHome.findEnabledStAll(AD_CMPNY);

	        Iterator i = apSupplierTypes.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalApSupplierType apSupplierType = (LocalApSupplierType)i.next();
	        	
	        	list.add(apSupplierType.getStName());
	        
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBaAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("CmRepReleasedChecksControllerBean getAdBaAll");
        
        LocalAdBankAccountHome adBankAccountHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection adBankAccounts = adBankAccountHome.findEnabledBaAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = adBankAccounts.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalAdBankAccount adBankAccount = (LocalAdBankAccount)i.next();
	        	
	        	list.add(adBankAccount.getBaName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeCmRepReleasedChecks(HashMap criteria, String ORDER_BY, String GROUP_BY, ArrayList adBrnchList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("CmRepReleasedChecksControllerBean executeCmRepReleasedChecks");
        
        LocalApCheckHome apCheckHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalApPurchaseOrderHome apPurchaseOrderHome = null;
        LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
                
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            apCheckHome = (LocalApCheckHome)EJBHomeFactory.
                lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);            
            apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);            

            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
		
		  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
                      
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(chk) FROM ApCheck chk ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("supplierCode")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

          if (criteria.containsKey("includedUnposted")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
          
          if (criteria.containsKey("status")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

          
	      
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("supplierCode")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("chk.apSupplier.splSupplierCode LIKE '%" + (String)criteria.get("supplierCode") + "%' ");
		  	 
		  }

		  if (criteria.containsKey("bankAccount")) {
		  	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		  	 
		  	 jbossQl.append("chk.adBankAccount.baName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("bankAccount");
		  	 ctr++;
		  	 
		  }

		  if (criteria.containsKey("supplierType")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("chk.apSupplier.apSupplierType.stName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("supplierType");
		   	  ctr++;
	   	  
	      }	
			
		  if (criteria.containsKey("supplierClass")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("chk.apSupplier.apSupplierClass.scName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("supplierClass");
		   	  ctr++;
	   	  
	      }			     
          			
		  if (criteria.containsKey("dateFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkCheckDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkCheckDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		      
		  if (criteria.containsKey("checkNumberFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("checkNumberFrom");
		  	 ctr++;
		  	 
		  }  
		      
		  if (criteria.containsKey("checkNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("checkNumberTo");
		  	 ctr++;
		  	 
		  } 	      
		      
		  if (criteria.containsKey("documentNumberFrom")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDocumentNumber>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberFrom");
		  	 ctr++;
		  	 
		  }  
		      
		  if (criteria.containsKey("documentNumberTo")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("chk.chkDocumentNumber<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("documentNumberTo");
		  	 ctr++;
		  	 
		  }
	      
	      if (criteria.containsKey("includedUnposted")) {
	          
	          String unposted = (String)criteria.get("includedUnposted");
	          
	          if (!firstArgument) {
		       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	                  	
	       	  if (unposted.equals("NO")) {	       	
		       	  	      	  		       	  
		       	  jbossQl.append("((chk.chkPosted = 1 AND chk.chkVoid = 0) OR (chk.chkPosted = 1 AND chk.chkVoid = 1 AND chk.chkVoidPosted = 0)) ");
		       	  
	       	  } else {
	       	  	
	       	  	  jbossQl.append("chk.chkVoid = 0 ");
	       	  	
	       	  }   	 
	       	  	       	  
	      }	  
	      
	      if(adBrnchList.isEmpty()) {
	      	
	      	throw new GlobalNoRecordFoundException();
	          
	      } else {
	      	
	          if (!firstArgument) {
	              
	              jbossQl.append("AND ");
	              
	          } else {
	              
	              firstArgument = false;
	              jbossQl.append("WHERE ");
	              
	          }	      
	          
	          jbossQl.append("chk.chkAdBranch in (");
	          
	          boolean firstLoop = true;
	          
	          Iterator i = adBrnchList.iterator();
	          
	          while(i.hasNext()) {
	              
	              if(firstLoop == false) { 
	                  jbossQl.append(", "); 
	              }
	              else { 
	                  firstLoop = false; 
	              }
	              
	              AdBranchDetails mdetails = (AdBranchDetails) i.next();
	              
	              jbossQl.append(mdetails.getBrCode());
	              
	          }
	          
	          jbossQl.append(") ");
	          
	          firstArgument = false;
	          
	      }
	      
	      if ( criteria.containsKey("status") && !(((String)criteria.get("status")).equals(""))) { 
	      	
	      	if (!firstArgument) {
	      		
	      		jbossQl.append("AND ");
	      		
	      	} else {
	      		
	      		firstArgument = false;
	      		jbossQl.append("WHERE ");
	      		
	      	}	      
	      	
	      	String STTS = (String)criteria.get("status");
			
	      	if (STTS.equals("RELEASED")) {
	      		
	      		jbossQl.append("chk.chkReleased = 1 ");
	      		
	      	} else if (STTS.equals("UNRELEASED")) {

	      		jbossQl.append("chk.chkReleased = 0 ");
	      		
	      	}
	      	
	      }
	      
	      if (!firstArgument) {
		       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("chk.chkAdCompany = " + AD_CMPNY + " ");
       	  
       	  System.out.println(jbossQl.toString());
			
       	  Collection apChecks = apCheckHome.getChkByCriteria(jbossQl.toString(), obj);	         
		  
		  if (apChecks.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = apChecks.iterator();
		
		  while (i.hasNext()) {
		  	
		  	LocalApCheck apCheck = (LocalApCheck)i.next();   	  
		  	
		  	CmRepReleasedChecksDetails details = new CmRepReleasedChecksDetails();
		  	details.setRrcDate(apCheck.getChkDate());
		  	details.setRrcCheckNumber(apCheck.getChkNumber());
		  	details.setRrcReferenceNumber(apCheck.getChkReferenceNumber());
		  	details.setRrcDescription(apCheck.getChkDescription());
		  	//details.setRrcSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode() + "-" + 
		  	//		apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());		  	  
		  	details.setRrcSplSupplierClass(apCheck.getApSupplier().getApSupplierClass().getScName());
		  	
		  	details.setRrcSplSupplierCode(apCheck.getApSupplier().getSplSupplierCode());
		  	
		  	System.out.println("[01] : " + details.getRrcSplSupplierCode());
		  	System.out.println("[02] : " + apCheck.getApSupplier().getSplSupplierCode() + "-" +	apCheck.getChkSupplierName());
		  	System.out.println("[03] : " + apCheck.getApSupplier().getSplName());
		  	
		  	details.setRrcChkDate(apCheck.getChkCheckDate());
		  	details.setRrcChkDateReleased(apCheck.getChkDateReleased());
		  	
		  	Collection apAppliedVouchers = apCheck.getApAppliedVouchers();
		  	
		  	Iterator avIter = apAppliedVouchers.iterator();
		  	while (avIter.hasNext()) {
		  		
		  		LocalApAppliedVoucher apAppliedVoucher = (LocalApAppliedVoucher)avIter.next();
		  		
		  		if (!apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getApPurchaseOrderLines().isEmpty()) {
		  			
		  			// find rr from ap_prchs_ordr where po_number = getVouPoNumber
		  			Collection apPurchaseOrderLines = apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getApPurchaseOrderLines();
		  			ArrayList apPurchaseOrderLineList = new ArrayList(apPurchaseOrderLines);
		  			LocalApPurchaseOrderLine apReceivingItem = (LocalApPurchaseOrderLine)apPurchaseOrderLineList.get(0);
		  			details.setRrcVouDate(apReceivingItem.getApPurchaseOrder().getPoDate());
		  			details.setRrcVouReferenceNumber(apReceivingItem.getApPurchaseOrder().getPoReferenceNumber());
		  			
		  		} else {
		  			details.setRrcVouDate(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouDate());	 
		  			details.setRrcVouReferenceNumber(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getVouReferenceNumber());
		  		}		  		
		  		details.setRrcVpsDueDate(apAppliedVoucher.getApVoucherPaymentSchedule().getVpsDueDate());
		  		
		  		// Set Vou Due Date
		  		Calendar vouDueDate = new GregorianCalendar();		  		
		  		
		  		vouDueDate.setTime(details.getRrcVouDate());		  			  		

		  		ArrayList adPaymntSchedules = new ArrayList(apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher().getAdPaymentTerm().getAdPaymentSchedules());		  		
		  		
		  		vouDueDate.add(Calendar.DAY_OF_MONTH, ((LocalAdPaymentSchedule)adPaymntSchedules.get(0)).getPsDueDay());
		  		details.setVouDueDate(vouDueDate.getTime());
		  	}
		  	details.setRrcMemo(apCheck.getChkMemo());
		  	
		  	// type
		  	if(apCheck.getApSupplier().getApSupplierType() == null){
		  		
		  		details.setRrcSplSupplierType("UNDEFINE");
		  		
		  	} else {
		  		
		  		details.setRrcSplSupplierType(apCheck.getApSupplier().getApSupplierType().getStName());
		  		
		  	}
		  	
		  	details.setRrcAmount(EJBCommon.roundIt(this.convertForeignToFunctionalCurrency(
		  			apCheck.getGlFunctionalCurrency().getFcCode(),
					apCheck.getGlFunctionalCurrency().getFcName(),
					apCheck.getChkConversionDate(),
					apCheck.getChkConversionRate(),
					apCheck.getChkAmount(), AD_CMPNY), adCompany.getGlFunctionalCurrency().getFcPrecision()));
		  	details.setRrcBankAccount(apCheck.getAdBankAccount().getBaName());
		  	details.setOrderBy(ORDER_BY);
		  	details.setRrcChkReleased(apCheck.getChkReleased()== 1 ? "YES": "NO");

		  	list.add(details);
		  	
		  }
		  	
		  // sort
		  
		  if (GROUP_BY.equals("SUPPLIER CODE")) {
       	  	
       	  	Collections.sort(list, CmRepReleasedChecksDetails.SupplierCodeComparator);
       	  	
       	  } else if (GROUP_BY.equals("SUPPLIER TYPE")) {
       	  	
       	  	Collections.sort(list, CmRepReleasedChecksDetails.SupplierTypeComparator);
       	  	
       	  } else if (GROUP_BY.equals("SUPPLIER CLASS")) {
       	  	
       	  	Collections.sort(list, CmRepReleasedChecksDetails.SupplierClassComparator);
       	  	
       	  } else {
       	  	
       	  	Collections.sort(list, CmRepReleasedChecksDetails.NoGroupComparator);
       	 
       	  }
		  
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("CmRepReleasedChecksControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}  
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException{
	    
	    Debug.print("CmRepReleasedChecksControllerBean getAdBrResAll");
	    
	    LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
	    LocalAdBranchHome adBranchHome = null;
	    
	    LocalAdBranchResponsibility adBranchResponsibility = null;
	    LocalAdBranch adBranch = null;
	    
	    Collection adBranchResponsibilities = null;
	    
	    ArrayList list = new ArrayList();
	    
	    // Initialize EJB Home
	    
	    try {
	        
	        adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
	        adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
	        
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	    
	    try {
	        
	        adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
	        
	    } catch (FinderException ex) {
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    if (adBranchResponsibilities.isEmpty()) {
	        
	        throw new GlobalNoRecordFoundException();
	        
	    }
	    
	    try {
	        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	            
	            adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	            
	            adBranch = adBranchResponsibility.getAdBranch();
	            
	            AdBranchDetails details = new AdBranchDetails();
	            
	            details.setBrCode(adBranch.getBrCode());
	            details.setBrBranchCode(adBranch.getBrBranchCode());
	            details.setBrName(adBranch.getBrName());
	            details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
	            
	            list.add(details);
	            
	        }	               
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    return list;
	    
	}    

	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("CmRepReleasedChecksControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("CmRepReleasedChecksControllerBean ejbCreate");
      
    }
}
