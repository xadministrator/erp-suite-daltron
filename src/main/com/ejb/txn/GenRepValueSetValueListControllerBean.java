
/*
 * GenRepValueSetValueListControllerBean.java
 *
 * Created on March 08, 2005, 02:37 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenValueSet;
import com.ejb.genfld.LocalGenValueSetHome;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GenRepValueSetValueListDetails;

/**
 * @ejb:bean name="GenRepValueSetValueListControllerEJB"
 *           display-name="Used for viewing value set value lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GenRepValueSetValueListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GenRepValueSetValueListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GenRepValueSetValueListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GenRepValueSetValueListControllerBean extends AbstractSessionBean {


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGenVsAll(Integer AD_CMPNY) {
                    
        Debug.print("GenValueSetValueControllerBean getGenVsAll");
        
        LocalGenValueSetHome genValueSetHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            genValueSetHome = (LocalGenValueSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetHome.JNDI_NAME, LocalGenValueSetHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {

        	Collection genValueSets = genValueSetHome.findVsAll(AD_CMPNY);

        	Iterator i = genValueSets.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGenValueSet genValueSet = (LocalGenValueSet)i.next();
        		
        		list.add(genValueSet.getVsName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
            
    }
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeGenRepValueSetValueList(HashMap criteria, String ORDER_BY, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GenRepValueSetValueListControllerBean executeGenRepValueSetValueList");
        
        LocalGenValueSetValueHome genValueSetValueHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(vsv) FROM GenValueSetValue vsv ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("valueSetName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }

	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("valueSetName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("vsv.genValueSet.vsName LIKE '%" + (String)criteria.get("valueSetName") + "%' ");
		  	 
		  }

		  if (criteria.containsKey("enable") && criteria.containsKey("disable")) {
		  	
		  	if (!firstArgument) {
		  		jbossQl.append("AND ");
		  	} else {
		  		firstArgument = false;
		  		jbossQl.append("WHERE ");
		  	}
		  	
		  	jbossQl.append("(vsv.vsvEnable=?" + (ctr+1) + " OR ");
		  	obj[ctr] = new Byte(EJBCommon.TRUE);
		  	ctr++;
		  	
		  	jbossQl.append("vsv.vsvEnable=?" + (ctr+1) + ") ");
		  	obj[ctr] = new Byte(EJBCommon.FALSE);
		  	ctr++;
		  	
		  } else {
		  	
		  	if (criteria.containsKey("enable")) {
		  		
		  		if (!firstArgument) {
		  			jbossQl.append("AND ");
		  		} else {
		  			firstArgument = false;
		  			jbossQl.append("WHERE ");
		  		}
		  		jbossQl.append("vsv.vsvEnable=?" + (ctr+1) + " ");
		  		obj[ctr] = new Byte(EJBCommon.TRUE);
		  		ctr++;
		  		
		  	} 
		  	
		  	if (criteria.containsKey("disable")) {
		  		
		  		if (!firstArgument) {
		  			jbossQl.append("AND ");
		  		} else {
		  			firstArgument = false;
		  			jbossQl.append("WHERE ");
		  		}
		  		jbossQl.append("vsv.vsvEnable=?" + (ctr+1) + " ");		      	 		      	 		      	 	      	 
		  		obj[ctr] = new Byte(EJBCommon.FALSE);
		  		ctr++;
		  		
		  	}
		  	
		  }
		  
	      if (!firstArgument) {		       	  	
	   	     jbossQl.append("AND ");		       	     
	   	  } else {		       	  	
	   	  	 firstArgument = false;
	   	  	 jbossQl.append("WHERE ");		       	  	 
	   	  }	     
	   	  
	   	  jbossQl.append("vsv.vsvAdCompany=" + AD_CMPNY + " ");

		  String orderBy = null;
          
          if (ORDER_BY.equals("VALUE NAME")) {
	      	 
	      	  orderBy = "vsv.genValueSet.vsName";
	      	  
	      } 

		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }     
			  	      	
	      Collection genValueSetValues = genValueSetValueHome.getVsvByCriteria(jbossQl.toString(), obj);	         
		  
		  if (genValueSetValues.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = genValueSetValues.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalGenValueSetValue genValueSetValue = (LocalGenValueSetValue)i.next();   	  
		  	  
		  	  GenRepValueSetValueListDetails details = new GenRepValueSetValueListDetails();

		  	  details.setVslValueSetName(genValueSetValue.getGenValueSet().getVsName());
		  	  details.setVslValue(genValueSetValue.getVsvValue());
		  	  details.setVslDescription(genValueSetValue.getVsvDescription());
		  	  
		  	  if (genValueSetValue.getGenQualifier() != null) {
		  	  
		  	  	details.setVslAccountType(genValueSetValue.getGenQualifier().getQlAccountType());
		  	  
		  	  }
		  	  
		  	  details.setVslEnable(genValueSetValue.getVsvEnable());

			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("GenRepValueSetValueListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GenRepValueSetValueListControllerBean ejbCreate");
      
    }
}
