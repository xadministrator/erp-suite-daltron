
/*
 * GenValueSetValueControllerBean.java
 *
 * Created on June 23, 2003, 9:48 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GenVSVNoValueSetValueFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalSegmentValueInvalidException;
import com.ejb.genfld.LocalGenQualifier;
import com.ejb.genfld.LocalGenQualifierHome;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSet;
import com.ejb.genfld.LocalGenValueSetHome;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlJournalLineInterface;
import com.ejb.gl.LocalGlJournalLineInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GenModValueSetValueDetails;

/**
 * @ejb:bean name="GenValueSetValueControllerEJB"
 *           display-name="Used for assigning value set values"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GenValueSetValueControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GenValueSetValueController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GenValueSetValueControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GenValueSetValueControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGenVsAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GenValueSetValueControllerBean getGenVsAll");
        
        LocalGenValueSetHome genValueSetHome = null;
        
        Collection genValueSets = null;
        
        LocalGenValueSet genValueSet = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            genValueSetHome = (LocalGenValueSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetHome.JNDI_NAME, LocalGenValueSetHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            genValueSets = genValueSetHome.findVsAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (genValueSets.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = genValueSets.iterator();
               
        while (i.hasNext()) {
        	
        	genValueSet = (LocalGenValueSet)i.next();
        	
        	list.add(genValueSet.getVsName());
        	
        }
        
        return list;
            
    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGenQlfrAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GenValueSetValueControllerBean getGenQlfrAll");
        
        LocalGenQualifierHome genQualifierHome = null;
        
        Collection genQualifiers = null;
        
        LocalGenQualifier genQualifier = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            genQualifierHome = (LocalGenQualifierHome)EJBHomeFactory.
                 lookUpLocalHome(LocalGenQualifierHome.JNDI_NAME, LocalGenQualifierHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            genQualifiers = genQualifierHome.findQlfrAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (genQualifiers.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = genQualifiers.iterator();
               
        while (i.hasNext()) {
        	
        	genQualifier = (LocalGenQualifier)i.next();
        	
        	list.add(genQualifier.getQlAccountType());
        	
        }
        
        return list;
            
    }


   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public ArrayList getGenVsvByVsName(String VS_NM, Integer AD_CMPNY)
      throws GenVSVNoValueSetValueFoundException {

      Debug.print("GenValueSetValueControllerBean getGenVsvByVsName");
     
      LocalGenValueSetValueHome genValueSetValueHome = null;
      LocalGenQualifierHome genQualifierHome = null;
      
      ArrayList list = new ArrayList();
      
      Collection genValueSetValues = null;
      
      LocalGenQualifier genQualifier = null;
      
      // Initialize EJB Home
        
      try {
            
          genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
             lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
          genQualifierHome = (LocalGenQualifierHome)EJBHomeFactory.
             lookUpLocalHome(LocalGenQualifierHome.JNDI_NAME, LocalGenQualifierHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
    	  System.out.println("VS_NM="+VS_NM);
    	  System.out.println("AD_CMPNY="+AD_CMPNY);
         genValueSetValues = genValueSetValueHome.findByVsName(VS_NM, AD_CMPNY);
         System.out.println("SIZE="+genValueSetValues.size());
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }

      if (genValueSetValues.size() == 0) {
      	
         throw new GenVSVNoValueSetValueFoundException();
      
      }
 
      Iterator i = genValueSetValues.iterator();
      
      while (i.hasNext()) {
      	
         LocalGenValueSetValue genValueSetValue = (LocalGenValueSetValue) i.next();
	       
		 GenModValueSetValueDetails mdetails = new GenModValueSetValueDetails();
		 
		 	mdetails.setVsvCode(genValueSetValue.getVsvCode());
		 	mdetails.setVsvValue(genValueSetValue.getVsvValue()); 		 
		 	mdetails.setVsvDescription(genValueSetValue.getVsvDescription());
		 	mdetails.setVsvParent(genValueSetValue.getVsvParent()); 
		 	mdetails.setVsvLevel(genValueSetValue.getVsvLevel()); 
		 	mdetails.setVsvEnable(genValueSetValue.getVsvEnable());	
		 	mdetails.setVsvVsName(genValueSetValue.getGenValueSet().getVsName());	
		 	
		 	System.out.println("genValueSetValue.getVsvValue()="+genValueSetValue.getVsvValue
		 			());
		 	
		    mdetails.setVsvQlAccountType(genValueSetValue.getGenQualifier() != null ? 
		        genValueSetValue.getGenQualifier().getQlAccountType() : null);
		    System.out.println("TYPE="+mdetails.getVsvQlAccountType());	 	
		    
	     list.add(mdetails);
      
      }

      return list;
   }


   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public char getGenSgSegmentTypeByVsName(String VS_NM, Integer AD_CMPNY) {

      Debug.print("GenValueSetValueControllerBean getGenSgSegmentTypeByVsName");
     
      LocalGenValueSetHome genValueSetHome = null;

      char SG_SGMNT_TYP = 0;
      
      Collection genSegments = null;
      
      LocalGenValueSet genValueSet = null;
      LocalGenSegment genSegment = null;
      
      
      // Initialize EJB Home
        
      try {
            
          genValueSetHome = (LocalGenValueSetHome)EJBHomeFactory.
             lookUpLocalHome(LocalGenValueSetHome.JNDI_NAME, LocalGenValueSetHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
    	  
      	
         genValueSet = genValueSetHome.findByVsName(VS_NM, AD_CMPNY);
         
      } catch (FinderException ex) {
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }

      try {
      	
         genSegments = genValueSet.getGenSegments();
	     
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
      }
 
      Iterator i = genSegments.iterator();
      
        while (i.hasNext()) {
        	
        	genSegment = (LocalGenSegment)i.next();
        	
        	SG_SGMNT_TYP = genSegment.getSgSegmentType();
        	
        }
        System.out.println("SG_SGMNT_TYP="+SG_SGMNT_TYP);
        
        return SG_SGMNT_TYP;
   }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGenVsvEntry(com.util.GenValueSetValueDetails details, String VS_NM, 
        String QL_ACCNT_TYP, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
        	   GlobalRecordInvalidException, GlobalSegmentValueInvalidException {
                    
        Debug.print("GenValueSetValueControllerBean addGenVsvEntry");
        
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenValueSetHome genValueSetHome = null;
        LocalGenQualifierHome genQualifierHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenSegmentHome genSegmentHome = null;
        
        Collection genSegments = null;
                
        // Initialize EJB Home
        
        try {
            
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
            genValueSetHome = (LocalGenValueSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetHome.JNDI_NAME, LocalGenValueSetHome.class);
            genQualifierHome = (LocalGenQualifierHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenQualifierHome.JNDI_NAME, LocalGenQualifierHome.class); 
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
    
        try { 
            
           LocalGenValueSetValue genValueSetValue = genValueSetValueHome.findByVsvValueAndVsName(details.getVsvValue(), VS_NM, AD_CMPNY);
	 
           throw new GlobalRecordAlreadyExistException();

        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
        
        try {

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            String segmentSeparator = String.valueOf(adCompany.getGenField().getFlSegmentSeparator());

            if(details.getVsvValue().contains(segmentSeparator)) {
            
            	throw new GlobalRecordInvalidException();
        
        	}
        	
        } catch (GlobalRecordInvalidException ex) {
        	
           throw ex;	
         
        } catch (Exception ex) {
        
        	throw new EJBException(ex.getMessage());
        
        }
        
        //check segment value
        try {

        	LocalGenValueSet genValueSet = genValueSetHome.findByVsName(VS_NM, AD_CMPNY);
            LocalGenSegment genSegment = genSegmentHome.findByVsCode(genValueSet.getVsCode(), AD_CMPNY);
            int segmentSize = details.getVsvValue().length();

            if(details.getVsvValue().length() != (int)genSegment.getSgMaxSize()) {
            
            	throw new GlobalSegmentValueInvalidException();
        
        	}
        	
        } catch (GlobalSegmentValueInvalidException ex) {
        	
           throw ex;	
         
        } catch (Exception ex) {
        
        	throw new EJBException(ex.getMessage());
        
        }

        try {
        	
        	// create new value set values
        	
        	LocalGenValueSetValue genValueSetValue = genValueSetValueHome.create(
        	   details.getVsvValue(), details.getVsvDescription(), details.getVsvParent(),
        	   details.getVsvLevel(), details.getVsvEnable(), AD_CMPNY); 
        	   
        	LocalGenValueSet genValueSet = genValueSetHome.findByVsName(VS_NM, AD_CMPNY);
		    //genValueSet.addGenValueSetValue(genValueSetValue);	
        	genValueSetValue.setGenValueSet(genValueSet);
		      	
		    if(QL_ACCNT_TYP != null && QL_ACCNT_TYP.length() > 0) {	
		      	
			    LocalGenQualifier genQualifier = genQualifierHome.findByQlAccountType(QL_ACCNT_TYP, AD_CMPNY);
			    //genQualifier.addGenValueSetValue(genValueSetValue); 
			    genValueSetValue.setGenQualifier(genQualifier);
		        
		    }     	        
		    
                	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateGenVsvEntry(com.util.GenValueSetValueDetails details, String VS_NM, 
        String QL_ACCNT_TYP, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               GlobalRecordAlreadyAssignedException,
               GlobalRecordInvalidException,
               GlobalSegmentValueInvalidException {
                    
        Debug.print("GenValueSetValueControllerBean updateGenVsvEntry");
        
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenValueSetHome genValueSetHome = null;
        LocalGenQualifierHome genQualifierHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalGenSegmentHome genSegmentHome = null;
        LocalGlJournalLineInterfaceHome glJournalLineInterfaceHome = null;
                        
        // Initialize EJB Home
        
        try {
            
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
            genValueSetHome = (LocalGenValueSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetHome.JNDI_NAME, LocalGenValueSetHome.class);
            genQualifierHome = (LocalGenQualifierHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenQualifierHome.JNDI_NAME, LocalGenQualifierHome.class);                
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
            glJournalLineInterfaceHome = (LocalGlJournalLineInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalLineInterfaceHome.JNDI_NAME, LocalGlJournalLineInterfaceHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        LocalGenValueSetValue genValueSetValue = null;
        LocalAdCompany adCompany = null;
        String accountSeparator = null;
        LocalGenSegment genSegment = null;
        
        try {
         
            genValueSetValue = genValueSetValueHome.findByPrimaryKey(details.getVsvCode());
            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            accountSeparator = String.valueOf(adCompany.getGenField().getFlSegmentSeparator());
            genSegment = genSegmentHome.findByVsCode(genValueSetValue.getGenValueSet().getVsCode(), AD_CMPNY);
            
            if(details.getVsvValue().contains(accountSeparator)) {
            
            	throw new GlobalRecordInvalidException();
        
        	}
        	
        } catch (GlobalRecordInvalidException ex) {
        	
           throw ex;
         
        } catch (Exception ex) {
        
        	throw new EJBException(ex.getMessage());
        
        }                     

        try { 
            
           LocalGenValueSetValue genExistingValueSetValue = genValueSetValueHome.
              findByVsvValueAndVsName(details.getVsvValue(), VS_NM, AD_CMPNY);
              
              if(genExistingValueSetValue != null &&
              	  !genExistingValueSetValue.getVsvCode().equals(details.getVsvCode())) {
	 
                 throw new GlobalRecordAlreadyExistException();
                 
              }
               
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
        
        // check segment value
        try {

        	LocalGenValueSet genValueSet = genValueSetHome.findByVsName(VS_NM, AD_CMPNY);
            LocalGenSegment genSegment2 = genSegmentHome.findByVsCode(genValueSet.getVsCode(), AD_CMPNY);
            int segmentSize = details.getVsvValue().length();

            if(details.getVsvValue().length() != (int)genSegment2.getSgMaxSize()) {
            
            	throw new GlobalSegmentValueInvalidException();
        
        	}
        	
        } catch (GlobalSegmentValueInvalidException ex) {
        	
           throw ex;	
         
        } catch (Exception ex) {
        
        	throw new EJBException(ex.getMessage());
        
        }
        
        // validate if qualifer is not updated
        
        try {
        
           StringBuffer jbossQl = new StringBuffer();

      	   jbossQl.append("SELECT OBJECT(coa) FROM GlChartOfAccount coa WHERE coa.coaSegment" + genSegment.getSgSegmentNumber() + "='" + genValueSetValue.getVsvValue() + "' AND coa.coaAdCompany=" + AD_CMPNY + " ");
           Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), new Object[0]);
           if(QL_ACCNT_TYP != null){
	           if (!glChartOfAccounts.isEmpty() && QL_ACCNT_TYP != null && QL_ACCNT_TYP.length() > 0 &&
	           	   !genValueSetValue.getGenQualifier().getQlAccountType().equals(QL_ACCNT_TYP)) 
	           	   throw new GlobalRecordAlreadyAssignedException();
           }
            
        } catch (GlobalRecordAlreadyAssignedException ex) {
        
           throw ex;
        
        } catch (Exception ex) {
        
            throw new EJBException(ex.getMessage());
        
        }
              

        // Find and Update Value Set Value
               
        try {
        	        	        
        	System.out.println("1.--------------->"+QL_ACCNT_TYP);
        	// propagate changes to value to other tables if necessary
        	if(QL_ACCNT_TYP != null){       	        	
	        	if (!details.getVsvValue().equals(genValueSetValue.getVsvValue()) ||
	       			!details.getVsvDescription().equals(genValueSetValue.getVsvDescription()) ||
					!genValueSetValue.getGenQualifier().getQlAccountType().equals(QL_ACCNT_TYP)) {
        	}   
                StringBuffer jbossQl = new StringBuffer();
                
          		jbossQl.append("SELECT OBJECT(coa) FROM GlChartOfAccount coa WHERE coa.coaSegment" + genSegment.getSgSegmentNumber() + "='" + genValueSetValue.getVsvValue() + "' AND coa.coaAdCompany=" + AD_CMPNY + " ");
        	    Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), new Object[0]);
        	    
        	    Iterator i = glChartOfAccounts.iterator();
        	    System.out.println("2.--------------->"); 
        	    while (i.hasNext()) {
        	    
        	        LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)i.next();
        	                	        
        	        switch(genSegment.getSgSegmentNumber()) {
        	        
        	        	case 1:
        	            	glChartOfAccount.setCoaSegment1(details.getVsvValue());
        	            	break;
        	        	case 2:
        	            	glChartOfAccount.setCoaSegment2(details.getVsvValue());
        	            	break;
        	        	case 3:
        	            	glChartOfAccount.setCoaSegment3(details.getVsvValue());
        	            	break;
        	        	case 4:
        	            	glChartOfAccount.setCoaSegment4(details.getVsvValue());
        	            	break;
        	        	case 5:
        	            	glChartOfAccount.setCoaSegment5(details.getVsvValue());
        	            	break;
        	        	case 6:
        	            	glChartOfAccount.setCoaSegment6(details.getVsvValue());
        	            	break;
        	        	case 7:
        	            	glChartOfAccount.setCoaSegment7(details.getVsvValue());
        	            	break;
        	        	case 8:
        	            	glChartOfAccount.setCoaSegment8(details.getVsvValue());
        	            	break;
        	        	case 9:
        	            	glChartOfAccount.setCoaSegment9(details.getVsvValue());
        	            	break;
        	        	case 10:
        	            	glChartOfAccount.setCoaSegment10(details.getVsvValue());
        	            	break;
        	            default:
        	            	break;
        	            	        	        
        	        }
        	        
        	        String newAccountNumber = "";
        	        String newAccountDescription = "";

        	        int j = 1;
        	        StringTokenizer st = new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), accountSeparator);
        	        System.out.println("3.--------------->");
        	        while (st.hasMoreTokens()) {
        	        	        	        	
        	        	if (j == genSegment.getSgSegmentNumber()) {
        	        		
        	        		newAccountNumber = newAccountNumber + details.getVsvValue();        	
        	        		newAccountDescription = newAccountDescription + details.getVsvDescription();        		        	        		
        	        		st.nextToken();
        	        		System.out.println("3.1--------------->");
        	        	} else {
        	        		
        	        		String vsvValue = (String)st.nextToken();
        	        		newAccountNumber = newAccountNumber + vsvValue;
        	        		
        	        		LocalGenSegment genOtherSegment = genSegmentHome.findByFlCodeAndSgSegmentNumber(adCompany.getGenField().getFlCode(), (short)j, AD_CMPNY);        	        		
        	        		LocalGenValueSetValue genOtherValueSetValue = genValueSetValueHome.findByVsCodeAndVsvValue(genOtherSegment.getGenValueSet().getVsCode(), vsvValue, AD_CMPNY);
        	        		newAccountDescription = newAccountDescription + genOtherValueSetValue.getVsvDescription();
        	        		        	        		        	        		
        	        		
        	        	}
        	        	
        	        	j++;       
        	        	
		        	    if (st.hasMoreTokens()) {
						        	
		        	        newAccountNumber = newAccountNumber + accountSeparator;
		        	        newAccountDescription = newAccountDescription + accountSeparator;
		        	        
					    }
					    					            	        	        	        	        	        	
        	        }
        	        System.out.println("4.--------------->");
        	        // replace coa in ad company if necessary
        	        
        	        if (glChartOfAccount.getCoaAccountNumber().equals(adCompany.getCmpRetainedEarnings())) {
        	        	
        	        	adCompany.setCmpRetainedEarnings(newAccountNumber);
        	        	
        	        }
        	        
        	        // replace coa in gl journal interface table if necessary
        	        
        	        Collection glJournalLineInterfaces = glJournalLineInterfaceHome.findJliByJliCoaAccountNumber(glChartOfAccount.getCoaAccountNumber(), AD_CMPNY);
        	        
        	        Iterator jliIter = glJournalLineInterfaces.iterator();
        	        System.out.println("5.--------------->");
        	        while (jliIter.hasNext()) {
        	        	
        	        	LocalGlJournalLineInterface glJournalLineInterface = (LocalGlJournalLineInterface)jliIter.next();
        	        	        	        		
        	        	glJournalLineInterface.setJliCoaAccountNumber(newAccountNumber);
        	        		        	        	
        	        }
        	        
        	        glChartOfAccount.setCoaAccountNumber(newAccountNumber);
        	        glChartOfAccount.setCoaAccountDescription(newAccountDescription);
        	        
        	        if(QL_ACCNT_TYP != null && QL_ACCNT_TYP.length() > 0) {
        	        	
        	        	glChartOfAccount.setCoaAccountType(QL_ACCNT_TYP);
        	        	
        	        }
        	        

        	    }        	            	    
        	            	
        	}
        		
            genValueSetValue.setVsvValue(details.getVsvValue());
            genValueSetValue.setVsvDescription(details.getVsvDescription());
            genValueSetValue.setVsvParent(details.getVsvParent());
            genValueSetValue.setVsvLevel(details.getVsvLevel());
            genValueSetValue.setVsvEnable(details.getVsvEnable());
            System.out.println("6.--------------->");
        	LocalGenValueSet genValueSet = genValueSetHome.findByVsName(VS_NM, AD_CMPNY);
		    //genValueSet.addGenValueSetValue(genValueSetValue);
		    genValueSetValue.setGenValueSet(genValueSet);
		    		      	
		    if(QL_ACCNT_TYP != null && QL_ACCNT_TYP.length() > 0) {	
		      	
			    LocalGenQualifier genQualifier = genQualifierHome.findByQlAccountType(QL_ACCNT_TYP, AD_CMPNY);
			    //genQualifier.addGenValueSetValue(genValueSetValue); 
			    genValueSetValue.setGenQualifier(genQualifier);
      	     
		    }
		    System.out.println("7.--------------->");    	        	                            	
        } catch (Exception ex) {
            
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void deleteGenVsvEntry(Integer VSV_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyAssignedException {
                    
        Debug.print("GenValueSetValueControllerBean deleteGenVsvEntry");
        
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenValueSetHome genValueSetHome = null;
        LocalGenQualifierHome genQualifierHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalGenSegmentHome genSegmentHome = null;
                        
        // Initialize EJB Home
        
        try {
            
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
            genValueSetHome = (LocalGenValueSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetHome.JNDI_NAME, LocalGenValueSetHome.class);
            genQualifierHome = (LocalGenQualifierHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenQualifierHome.JNDI_NAME, LocalGenQualifierHome.class);                
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        LocalGenValueSetValue genValueSetValue = null;
        LocalAdCompany adCompany = null;
        String accountSeparator = null;
        LocalGenSegment genSegment = null;
        
        try {
         
            genValueSetValue = genValueSetValueHome.findByPrimaryKey(VSV_CODE);
            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            accountSeparator = String.valueOf(adCompany.getGenField().getFlSegmentSeparator());
            genSegment = genSegmentHome.findByVsCode(genValueSetValue.getGenValueSet().getVsCode(), AD_CMPNY);
         
        } catch (Exception ex) {
        
        	throw new EJBException(ex.getMessage());
        
        }                        
        
        // validate if value set value still not assigned
        
        try {
        
           StringBuffer jbossQl = new StringBuffer();
      	   jbossQl.append("SELECT OBJECT(coa) FROM GlChartOfAccount coa WHERE coa.coaSegment" + genSegment.getSgSegmentNumber() + "='" + genValueSetValue.getVsvValue() + "' AND coa.coaAdCompany=" + AD_CMPNY + " ");
           Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), new Object[0]);
           
           if (!glChartOfAccounts.isEmpty()) 
           	   throw new GlobalRecordAlreadyAssignedException();
        
            
        } catch (GlobalRecordAlreadyAssignedException ex) {
        
           throw ex;
        
        } catch (Exception ex) {
        
            throw new EJBException(ex.getMessage());
        
        }
              
              
        try {
        	        	        
        	// delete value set value
        	genValueSetValue.remove();
        	
        	      	        	                            	
        } catch (Exception ex) {
            
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GenValueSetValueControllerBean ejbCreate");
      
    }      
}
