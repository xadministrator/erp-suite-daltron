package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.GlACAccountingCalendarAlreadyAssignedException;
import com.ejb.exception.GlACAccountingCalendarAlreadyDeletedException;
import com.ejb.exception.GlACAccountingCalendarAlreadyExistException;
import com.ejb.exception.GlACNoAccountingCalendarFoundException;
import com.ejb.exception.GlPTNoPeriodTypeFoundException;
import com.ejb.gl.LocalGlAccountingCalendar;
import com.ejb.gl.LocalGlAccountingCalendarHome;
import com.ejb.gl.LocalGlPeriodType;
import com.ejb.gl.LocalGlPeriodTypeHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlAccountingCalendarDetails;
import com.util.GlModAccountingCalendarDetails;
import com.util.GlPeriodTypeDetails;

/**
 * @ejb:bean name="GlAccountingCalendarControllerEJB"
 *           display-name="Used for setting up the organizations calendar"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlAccountingCalendarControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlAccountingCalendarController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlAccountingCalendarControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
 */

public class GlAccountingCalendarControllerBean extends AbstractSessionBean {
	
	
	/*****************************************************************************
	 Business methods:
	 
	 (1) getGlAcAll - returns an ArrayList of all AC including period type
	 and description
	 
	 (2) getGlPtAll - returns an ArrayList of all PT
	 
	 (3) addGlAcEntry - adds an AC and assign it to PT
	 
	 (4) updateGlAcEntry - updates an AC entry only if it is not yet assigned
	 
	 (5) deleteGlAcEntry - deletes an AC entry only if it is not yet assigned
	 
	 Private methods:
	 
	 (1) addGlAccountingCalendarToGlPeriodType - adds an AC to a PT
	 
	 (2) hasRelation - returns true if has relation with other tables else false
	 
	 (3) hasAccountingCalendarValue - return true if has ACV already else false
	 
	 *******************************************************************************/
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlAcAll(Integer AD_CMPNY)
	throws GlACNoAccountingCalendarFoundException,
	GlPTNoPeriodTypeFoundException {
		
		Debug.print("GlAccountingCalendarControllerBean getGlAcAll");
		
		/******************************************************
		 Returns all Accounting Calendar including the
		 related Period Type name
		 ******************************************************/
		
		ArrayList acAllList = new ArrayList();
		Collection glAccountingCalendars = null;
		
		LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
		
		// Initialize EJB Home
		
		try {
			
			glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			glAccountingCalendars = glAccountingCalendarHome.findAcAll(AD_CMPNY);
		} catch (FinderException ex) {
			throw new EJBException(ex.getMessage());
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
		
		if (glAccountingCalendars.size() == 0)
			throw new GlACNoAccountingCalendarFoundException();
		
		Iterator i = glAccountingCalendars.iterator();
		while (i.hasNext()) {
			LocalGlAccountingCalendar glAccountingCalendar = (LocalGlAccountingCalendar) i.next();
			LocalGlPeriodType glPeriodType = null;
			
			glPeriodType = glAccountingCalendar.getGlPeriodType();
			
			String MAC_PT_NM = null;
			
			try { 
				MAC_PT_NM = glPeriodType.getPtName();
			} catch (Exception ex) {
				throw new GlPTNoPeriodTypeFoundException();
			}
			
			GlModAccountingCalendarDetails details = new GlModAccountingCalendarDetails(
					glAccountingCalendar.getAcCode(), glAccountingCalendar.getAcName(),
					glAccountingCalendar.getAcDescription(), MAC_PT_NM);
			acAllList.add(details);
		}
		
		return acAllList;
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlPtAll(Integer AD_CMPNY)
	throws GlPTNoPeriodTypeFoundException {
		
		Debug.print("GlAccountingCalendarControllerBean getGlPtAll");
		
		ArrayList ptAllList = new ArrayList();
		Collection glPeriodTypes = null;
		
		LocalGlPeriodTypeHome glPeriodTypeHome = null;
		
		// Initialize EJB Home
		
		try {
			
			glPeriodTypeHome = (LocalGlPeriodTypeHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlPeriodTypeHome.JNDI_NAME, LocalGlPeriodTypeHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			glPeriodTypes = glPeriodTypeHome.findPtAll(AD_CMPNY);
		} catch (FinderException ex) {
			throw new EJBException(ex.getMessage());
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
		
		if (glPeriodTypes.size() == 0)
			throw new GlPTNoPeriodTypeFoundException();
		
		Iterator i = glPeriodTypes.iterator();
		while (i.hasNext()) {
			LocalGlPeriodType glPeriodType = (LocalGlPeriodType) i.next();
			GlPeriodTypeDetails details = new GlPeriodTypeDetails(
					glPeriodType.getPtCode(), glPeriodType.getPtName(),
					glPeriodType.getPtDescription(), glPeriodType.getPtPeriodPerYear(),
					glPeriodType.getPtYearType());
			ptAllList.add(details);
		}
		
		return ptAllList;
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void addGlAcEntry(GlAccountingCalendarDetails details, String AC_PT_NM, Integer AD_CMPNY)
	throws GlACAccountingCalendarAlreadyExistException,
	GlPTNoPeriodTypeFoundException {
		
		Debug.print("GlAccountingCalendarControllerBean addGlAcEntry");
		
		LocalGlAccountingCalendar glAccountingCalendar = null;
		LocalGlPeriodType glPeriodType = null;
		
		LocalGlPeriodTypeHome glPeriodTypeHome = null;
		LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
		
		// Initialize EJB Home
		
		try {
			
			glPeriodTypeHome = (LocalGlPeriodTypeHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlPeriodTypeHome.JNDI_NAME, LocalGlPeriodTypeHome.class);
			glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		/****************************************************************
		 Add AC and Assign it to PT, this is a required field
		 ****************************************************************/
		
		try {
			glPeriodType = glPeriodTypeHome.findByPtName(AC_PT_NM, AD_CMPNY);
		} catch (FinderException ex) {
			throw new GlPTNoPeriodTypeFoundException();
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
		
		try {
			
			glAccountingCalendarHome.findByAcName(details.getAcName(), AD_CMPNY);
			
			getSessionContext().setRollbackOnly();
			throw new GlACAccountingCalendarAlreadyExistException();
			
		} catch (FinderException ex) {
			
			
		}
		
		try {
			glAccountingCalendar =
				glAccountingCalendarHome.create(
						details.getAcName(), details.getAcDescription(), null, AD_CMPNY);
		} catch (Exception ex) {
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
		}
		
		addGlAccountingCalendarToGlPeriodType(glAccountingCalendar.getAcCode(), 
				glPeriodType.getPtCode(), AD_CMPNY);
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void updateGlAcEntry(GlAccountingCalendarDetails details, String AC_PT_NM, Integer AD_CMPNY)
	throws GlACAccountingCalendarAlreadyExistException,
	GlACAccountingCalendarAlreadyAssignedException, 
	GlPTNoPeriodTypeFoundException,
	GlACAccountingCalendarAlreadyDeletedException {
		
		Debug.print("GlAccountingCalendarControllerBean updateGlAcEntry");
		
		LocalGlAccountingCalendar glAccountingCalendar = null;
		LocalGlPeriodType glPeriodType = null;
		
		LocalGlPeriodTypeHome glPeriodTypeHome = null;
		LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
		
		// Initialize EJB Home
		
		try {
			
			glPeriodTypeHome = (LocalGlPeriodTypeHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlPeriodTypeHome.JNDI_NAME, LocalGlPeriodTypeHome.class);
			glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			glAccountingCalendar = glAccountingCalendarHome.findByPrimaryKey(
					details.getAcCode());
		} catch (FinderException ex) {
			throw new GlACAccountingCalendarAlreadyDeletedException();
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
		
		/***********************************************************
		 Check if AC is already assigned in related tables,
		 if already assigned, thie item should never be deleted,
		 otherwise, update AC including the relation with PT
		 ***********************************************************/
		if (hasRelation(glAccountingCalendar, AD_CMPNY) && !AC_PT_NM.equals(glAccountingCalendar.getGlPeriodType().getPtName())) 
			throw new GlACAccountingCalendarAlreadyAssignedException();
		else {
			
			/***********************************************************
			 AC has no relation, validate fields and add AC to PT
			 ************************************************************/
			
			LocalGlAccountingCalendar glAccountingCalendar2 = null;
			
			try {
				glPeriodType = glPeriodTypeHome.findByPtName(AC_PT_NM, AD_CMPNY);
			} catch (FinderException ex) {
				throw new GlPTNoPeriodTypeFoundException();
			} catch (Exception ex) {
				throw new EJBException(ex.getMessage());
			}
			
			try {
				glAccountingCalendar2 =
					glAccountingCalendarHome.findByAcName(details.getAcName(), AD_CMPNY);
			} catch (FinderException ex) {
				
				glAccountingCalendar.setAcName(details.getAcName());
				glAccountingCalendar.setAcDescription(details.getAcDescription());
				addGlAccountingCalendarToGlPeriodType(details.getAcCode(),
						glPeriodType.getPtCode(), AD_CMPNY);
								
			}
			
			if (glAccountingCalendar2 != null &&
					!glAccountingCalendar.getAcCode().equals(glAccountingCalendar2.getAcCode())) {
				
				getSessionContext().setRollbackOnly();
				throw new GlACAccountingCalendarAlreadyExistException();
				
			} else {
				if (glAccountingCalendar2 != null &&
						glAccountingCalendar.getAcCode().equals(glAccountingCalendar2.getAcCode())) {
											
					glAccountingCalendar.setAcName(details.getAcName());
					glAccountingCalendar.setAcDescription(details.getAcDescription());
					addGlAccountingCalendarToGlPeriodType(details.getAcCode(),
							glPeriodType.getPtCode(), AD_CMPNY);
					
					
				}
			}
		}
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteGlAcEntry(Integer AC_CODE, Integer AD_CMPNY)
	throws GlACAccountingCalendarAlreadyAssignedException,
	GlACAccountingCalendarAlreadyDeletedException {
		
		Debug.print("GlAccountingCalendarControllerBean deleteGlAcEntry");
		
		LocalGlAccountingCalendar glAccountingCalendar;
		
		LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
		
		// Initialize EJB Home
		
		try {
			
			glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		/******************************************************
		 Check if the AC has any relation, if it has one or 
		 more relation, the AC should not be deleted, if
		 has no relation cascaded delete in the composite
		 accounting calendar value will take place
		 *******************************************************/
		
		try {
			glAccountingCalendar = glAccountingCalendarHome.findByPrimaryKey(AC_CODE);
		} catch (FinderException ex) {
			throw new GlACAccountingCalendarAlreadyDeletedException();
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
		
		if (hasRelation(glAccountingCalendar, AD_CMPNY))
			throw new GlACAccountingCalendarAlreadyAssignedException();
		else {
			try {
				glAccountingCalendar.remove();
			} catch (RemoveException ex) {
				getSessionContext().setRollbackOnly();
				throw new EJBException(ex.getMessage());
			} catch (Exception ex) {
				getSessionContext().setRollbackOnly();
				throw new EJBException(ex.getMessage());
			}
		}
	}
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("GlAccountingCalendarControllerBean ejbCreate");
		
	}
	
	// private methods
	
	private void addGlAccountingCalendarToGlPeriodType(Integer AC_CODE, Integer PT_CODE, Integer AD_CMPNY) {
		
		Debug.print("GlAccountingCalendarControllerBean addGlAccountingCalendarToGlPeriodType");
		
		/**************************************************
		 Add relationship of accounting calendar to the 
		 period type
		 **************************************************/
		
		
		LocalGlPeriodTypeHome glPeriodTypeHome = null;
		LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			glPeriodTypeHome = (LocalGlPeriodTypeHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlPeriodTypeHome.JNDI_NAME, LocalGlPeriodTypeHome.class);
			glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			LocalGlAccountingCalendar glAccountingCalendar = glAccountingCalendarHome.findByPrimaryKey(AC_CODE);
			LocalGlPeriodType glPeriodType = glPeriodTypeHome.findByPrimaryKey(PT_CODE);
			glPeriodType.addGlAccountingCalendar(glAccountingCalendar);
		} catch (FinderException ex) {
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
		} catch (Exception ex) {
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
		}
	}
	
	private boolean hasRelation(LocalGlAccountingCalendar glAccountingCalendar, Integer AD_CMPNY) {
		
		Debug.print("GlAccountingCalendarControllerBean hasRelation");
		
		Collection glSetOfBooks = null;
		
		/*********************************************
		 Check if has relation with GlSetOfBook
		 *********************************************/
		
		try {
			glSetOfBooks = 
				glAccountingCalendar.getGlSetOfBooks();
		} catch (Exception ex) {
		}
		
		if (glSetOfBooks.size() != 0) return true;
		else return false;
		
	}
		
}
