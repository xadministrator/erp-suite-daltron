package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.exception.GlACAccountingCalendarAlreadyAssignedException;
import com.ejb.exception.GlACNoAccountingCalendarFoundException;
import com.ejb.exception.GlACVDateIsNotSequentialOrHasGapException;
import com.ejb.exception.GlACVLastPeriodIncorrectException;
import com.ejb.exception.GlACVNoAccountingCalendarValueFoundException;
import com.ejb.exception.GlACVNotTotalToOneYearException;
import com.ejb.exception.GlACVPeriodNumberNotUniqueException;
import com.ejb.exception.GlACVPeriodOverlappedException;
import com.ejb.exception.GlACVPeriodPrefixNotUniqueException;
import com.ejb.exception.GlACVQuarterIsNotSequentialOrHasGapException;
import com.ejb.exception.GlPTNoPeriodTypeFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.gl.LocalGlAccountingCalendar;
import com.ejb.gl.LocalGlAccountingCalendarHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlPeriodType;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlTransactionCalendar;
import com.ejb.gl.LocalGlTransactionCalendarHome;
import com.ejb.gl.LocalGlTransactionCalendarValue;
import com.ejb.gl.LocalGlTransactionCalendarValueHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlAccountingCalendarDetails;
import com.util.GlAccountingCalendarValueDetails;
import com.util.GlModAccountingCalendarDetails;

/**
 * @ejb:bean name="GlAccountingCalendarValueControllerEJB"
 *           display-name="Used for setting up the organizations calendar details"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlAccountingCalendarValueControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlAccountingCalendarValueController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlAccountingCalendarValueControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlAccountingCalendarValueControllerBean extends AbstractSessionBean {


   /*******************************************************************************
      Business methods:

      (1) getGlAcAll - returns an ArrayList of all AC

      (2) getGlAcvByGlAcName - returns an ArrayList of all ACV related to a
      			       specified AC

      (3) saveGlAcvEntry - saves a set of ACV with validation, also rollbacks
      			   if any error occur

      Private methods:

      (1) addGlAccountingCalendarValueToGlAccountingCalendar - adds an ACV to an AC

      (2) hasRelation - returns true if has relationship with other tables else
      			returns false

      (3) isPeriodPrefixUnique - returns true if each ACVs period prefix of a set 
                                 are all unique

      (4) isPeriodNumberUnique - returns true if each ACVs period number of a set
      			         are all unique

      (5) isPeriodDateCoveredNotOverlapped - returns true if date coverage does 
      				             not overlap each other

      (6) isLastPeriodEqualToPeriodTypeLastPeriod - returns true if last period
      						    equals number of period specified
						    in PT

      (7) isQuarterSequentialAndHasNoGap - returns true if quarters of the ACV set
      				  	   is sequential and has no gap

      (8) isDateSequentialAndHasNoGap - returns true if Date coverage is sequential
      					and has no gap

      (9) isAccountingCalendarOneYear - returns true if ACV set total number of days
      					 equals one year with or without a leap year

   **********************************************************************************/

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlAcAll(Integer AD_CMPNY)
      throws GlACNoAccountingCalendarFoundException {

      Debug.print("GlAccountingCalendarControllerBean getGlAcAll");

      /******************************************************
         Returns all Accounting Calendar names
      ******************************************************/

      ArrayList acAllList = new ArrayList();
      Collection glAccountingCalendars = null;
      
      LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
      
      // Initialize EJB Home
        
      try {
            
          glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         glAccountingCalendars = glAccountingCalendarHome.findAcAll(AD_CMPNY);
      } catch (FinderException ex) {
         throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glAccountingCalendars.size() == 0)
         throw new GlACNoAccountingCalendarFoundException();

      Iterator i = glAccountingCalendars.iterator();
      while (i.hasNext()) {
         LocalGlAccountingCalendar glAccountingCalendar = (LocalGlAccountingCalendar) i.next();

         GlAccountingCalendarDetails details = new GlAccountingCalendarDetails(
         glAccountingCalendar.getAcName());
         acAllList.add(details);
      }

      return acAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public GlModAccountingCalendarDetails getGlPtNameAndAcDescriptionByGlAcName(String AC_NM, Integer AD_CMPNY)
      throws GlACNoAccountingCalendarFoundException, 
      GlPTNoPeriodTypeFoundException {

      Debug.print("GlAccountingCalendarValueControllerBean getGlPtNameAndAcDescriptionByGlAcName");

      /***********************************************************
         Returns the related period type name and AC description
       **********************************************************/

       LocalGlAccountingCalendar glAccountingCalendar = null;
       LocalGlPeriodType glPeriodType = null;
       
       LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
      
       // Initialize EJB Home
        
       try {
            
          glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

      try {
         glAccountingCalendar = glAccountingCalendarHome.findByAcName(AC_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new GlACNoAccountingCalendarFoundException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      try {
         glPeriodType = glAccountingCalendar.getGlPeriodType();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
      
      if (glPeriodType == null) {
      	 throw new GlPTNoPeriodTypeFoundException();     	
      }

      return new GlModAccountingCalendarDetails(glAccountingCalendar.getAcDescription(),
         glPeriodType.getPtName());

   } 

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlAcvByGlAcName(String AC_NM, Integer AD_CMPNY)
      throws GlACVNoAccountingCalendarValueFoundException,
      GlACNoAccountingCalendarFoundException {

      Debug.print("GlAccountingCalendarValueControllerBean getGlAcvByAcName");

      /********************************************************
         Returns all Accounting Calendar values of a related
	 Accounting Calendar
      *********************************************************/

      LocalGlAccountingCalendar glAccountingCalendar = null;

      ArrayList acvAllList = new ArrayList();
      Collection glAccountingCalendarValues = null;
      
      LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
      LocalGlSetOfBookHome glSetOfBookHome = null;
      
      Collection glSetOfBooks = null;
      
      // Initialize EJB Home
        
      try {
            
          glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);
          glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {

         glAccountingCalendar = glAccountingCalendarHome.findByAcName(AC_NM, AD_CMPNY);
         
      } catch (FinderException ex) {
      	
         throw new GlACNoAccountingCalendarFoundException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }

      try {
      	
         glAccountingCalendarValues = 
	    	glAccountingCalendar.getGlAccountingCalendarValues(); 
	    	
	     glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
	    	
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }

      if (glAccountingCalendarValues.isEmpty() && glSetOfBooks.isEmpty())
         throw new GlACVNoAccountingCalendarValueFoundException();
         
      try {
      	
      	  if (!glAccountingCalendarValues.isEmpty()) {
 
		      Iterator i = glAccountingCalendarValues.iterator();
		      
		      while (i.hasNext()) {
		      	
		         LocalGlAccountingCalendarValue glAccountingCalendarValue = 
			       (LocalGlAccountingCalendarValue) i.next();
			       
				 GlAccountingCalendarValueDetails details = new GlAccountingCalendarValueDetails(
				    glAccountingCalendarValue.getAcvCode(), glAccountingCalendarValue.getAcvPeriodPrefix(),
				    glAccountingCalendarValue.getAcvQuarter(), glAccountingCalendarValue.getAcvPeriodNumber(),
				    glAccountingCalendarValue.getAcvDateFrom(), glAccountingCalendarValue.getAcvDateTo(),
				    glAccountingCalendarValue.getAcvStatus(), glAccountingCalendarValue.getAcvDateOpened(),
				    glAccountingCalendarValue.getAcvDateClosed(), glAccountingCalendarValue.getAcvDatePermanentlyClosed(), 
				    glAccountingCalendarValue.getAcvDateFutureEntered());
		
			     acvAllList.add(details);
	      	 }
	      	 
	     } else if (!glSetOfBooks.isEmpty()) {
	     	
	     	 ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);
	     	 
	     	 LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(glSetOfBookList.size() - 1);
	     	 
	     	 Collection glPreviousAccountingCalendarValues = glSetOfBook.getGlAccountingCalendar().getGlAccountingCalendarValues();
	     	 
	     	 Iterator i = glPreviousAccountingCalendarValues.iterator();
	     	 
	     	 while (i.hasNext()) {
	     	 	
	     	 	LocalGlAccountingCalendarValue glAccountingCalendarValue = 
			       (LocalGlAccountingCalendarValue) i.next();
			       
			    GregorianCalendar gcFrom = new GregorianCalendar();
			    GregorianCalendar gcTo = new GregorianCalendar();
			    			    			    			    
			    gcFrom.setTime(glAccountingCalendarValue.getAcvDateFrom());
			    gcTo.setTime(glAccountingCalendarValue.getAcvDateTo());
			    
			    gcFrom.add(Calendar.YEAR, 1);
			    gcTo.add(Calendar.YEAR, 1);
			    	    
			    
			    if (gcTo.isLeapYear(gcTo.get(Calendar.YEAR)) && 
			        gcTo.get(Calendar.MONTH) == 1 &&
			        gcTo.get(Calendar.DATE) == 28) {
			    	
			        gcTo.add(Calendar.DATE, 1);	
			    	
			    } else if (!gcTo.isLeapYear(gcTo.get(Calendar.YEAR)) && 
			        gcTo.get(Calendar.MONTH) == 2 &&
			        gcTo.get(Calendar.DATE) == 1) {
			        	
			        gcTo.add(Calendar.DATE, -1);
			        	
			    }
			    
			    			    			    			    			    			       
			    GlAccountingCalendarValueDetails details = new GlAccountingCalendarValueDetails(
				    null, glAccountingCalendarValue.getAcvPeriodPrefix(),
				    glAccountingCalendarValue.getAcvQuarter(), glAccountingCalendarValue.getAcvPeriodNumber(),
				    gcFrom.getTime(), gcTo.getTime(),
				    'N', null, null, null, null);	
				    
				    
				    
			    acvAllList.add(details);			    
					     	 	
	     	 }
	     	
	     }
	     
      	 return acvAllList;
      
      } catch (Exception ex) {
      	
	      Debug.printStackTrace(ex);
	      throw new EJBException(ex.getMessage());
      	
      }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void saveGlAcvEntry(ArrayList acvList, String AC_NM, Integer AD_CMPNY) 
      throws GlACAccountingCalendarAlreadyAssignedException,
      GlACNoAccountingCalendarFoundException,
      GlACVPeriodPrefixNotUniqueException,
      GlACVPeriodNumberNotUniqueException,
      GlACVPeriodOverlappedException,
      GlACVLastPeriodIncorrectException,
      GlACVQuarterIsNotSequentialOrHasGapException,
      GlACVDateIsNotSequentialOrHasGapException,
      GlACVNotTotalToOneYearException,
	  GlobalRecordInvalidException,
	  GlobalRecordAlreadyExistException {

      Debug.print("GlAccountingCalendarValueControllerBean saveGlAcvEntry");

      LocalGlAccountingCalendar glAccountingCalendar = null;
      
      LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      LocalGlTransactionCalendarHome glTransactionCalendarHome = null;
      LocalGlTransactionCalendarValueHome glTransactionCalendarValueHome = null;
      LocalGlSetOfBookHome glSetOfBookHome = null;
      LocalGlChartOfAccountHome glChartOfAccountHome = null;
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      
      LocalGlAccountingCalendarValue glFirstAccountingCalendarValue = null;
      LocalGlAccountingCalendarValue glLastAccountingCalendarValue = null;
      
      LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;
      LocalApSupplierHome apSupplierHome = null;
     
      
      
      // Initialize EJB Home
        
      try {
            
          glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);
          glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
          glTransactionCalendarHome = (LocalGlTransactionCalendarHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlTransactionCalendarHome.JNDI_NAME, LocalGlTransactionCalendarHome.class);
          glTransactionCalendarValueHome = (LocalGlTransactionCalendarValueHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlTransactionCalendarValueHome.JNDI_NAME, LocalGlTransactionCalendarValueHome.class);
          glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
          glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
          glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
          
          glInvestorAccountBalanceHome = (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
                  lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);
          
          apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                  lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
          
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
      	
         glAccountingCalendar = 
	    	glAccountingCalendarHome.findByAcName(AC_NM, AD_CMPNY); 
	    	
      } catch (FinderException ex) {
      	
         throw new GlACNoAccountingCalendarFoundException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }

      if (hasRelation(glAccountingCalendar, AD_CMPNY))
      
         throw new GlACAccountingCalendarAlreadyAssignedException();
         
      else {
         
         ArrayList acvCodeList = new ArrayList();
         Collection glAccountingCalendarValues = null;

		 try {
		 	
		    glAccountingCalendarValues =
		       glAccountingCalendar.getGlAccountingCalendarValues();
		       
	     } catch (Exception ex) {
	     	
		    throw new EJBException(ex.getMessage());
		    
		 }
	
		 if (glAccountingCalendarValues.size() != 0) {
	
		    Iterator i = glAccountingCalendarValues.iterator();
		    
		    while (i.hasNext()) {
		    	
			    Integer ACV_CODE = null;
			    LocalGlAccountingCalendarValue glAccountingCalendarValue = 
			         (LocalGlAccountingCalendarValue) i.next();
			    try {
			    	
			       ACV_CODE = glAccountingCalendarValue.getAcvCode();
			       
			    } catch (Exception ex) {
			    	
			       throw new EJBException(ex.getMessage());
			       
			    } 
			    acvCodeList.add(ACV_CODE);
			    
		    }
	
		    i = acvCodeList.iterator();
		    while (i.hasNext()) {
		    	
		       Integer ACV_CODE = (Integer) i.next();
		       try {
		       	
		          LocalGlAccountingCalendarValue glAccountingCalendarValue =
		             glAccountingCalendarValueHome.findByPrimaryKey(ACV_CODE);
		          glAccountingCalendarValue.remove();
		          
		       } catch (FinderException ex) {
		       	
		          getSessionContext().setRollbackOnly();
			  	  throw new GlACNoAccountingCalendarFoundException();
			  	  
		       } catch (RemoveException ex) {
		       	
		          getSessionContext().setRollbackOnly();
			      throw new EJBException(ex.getMessage());
			      
		       } catch (Exception ex) {
		       	
		          getSessionContext().setRollbackOnly();
			      throw new EJBException(ex.getMessage());
			      
		       }
	        }
	
		 }
	
	      /***************************************************
		     Validations before committing   
		  ***************************************************/
	
	      Iterator i = acvList.iterator();
		  while (i.hasNext()) {
		
			    GlAccountingCalendarValueDetails details =
			       (GlAccountingCalendarValueDetails) i.next();
			
			    if (!isPeriodPrefixUnique(details, glAccountingCalendar, AD_CMPNY)) {
		
			       Debug.print("Period Prefix Not Unique ROLLBACK");
			       getSessionContext().setRollbackOnly();
			       throw new GlACVPeriodPrefixNotUniqueException();
			
			    } else if (!isPeriodNumberUnique(details, glAccountingCalendar, AD_CMPNY)) {
		
			       Debug.print("Period Number Not Unique ROLLBACK");
			       getSessionContext().setRollbackOnly();
			       throw new GlACVPeriodNumberNotUniqueException();
			
			    } else if (!isPeriodDateCoveredNotOverlapped(details, glAccountingCalendar, AD_CMPNY)) {
		
		               Debug.print("Period Overlapped ROLLBACK");
			       getSessionContext().setRollbackOnly();
			       throw new GlACVPeriodOverlappedException();
			
			    } else if (!i.hasNext() &&
			       !isLastPeriodEqualToPeriodTypeLastPeriod(details, glAccountingCalendar, AD_CMPNY)) {
			     
			       Debug.print("Last period not equal to PTs specified last ROLLBACK");
			       getSessionContext().setRollbackOnly();
			       throw new GlACVLastPeriodIncorrectException();
		
			    } else if (!i.hasNext() &&
			       !isQuarterSequentialAndHasNoGap(details, glAccountingCalendar, AD_CMPNY)) {
		 
			       Debug.print("Quarter is not sequential and/or has gap ROLLBACK");
			       getSessionContext().setRollbackOnly();
			       throw new GlACVQuarterIsNotSequentialOrHasGapException();
		
			    } else if (!i.hasNext() &&
			       !isDateSequentialAndHasNoGap(details, glAccountingCalendar, AD_CMPNY)) {
		
			       Debug.print("Dates is not sequential and/or has gap ROLLBACK");
			       getSessionContext().setRollbackOnly();
			       throw new GlACVDateIsNotSequentialOrHasGapException();
		
			    } else if (!i.hasNext() &&
			       !isAccountingCalendarOneYear(details, glAccountingCalendar, AD_CMPNY)) {
		
			       Debug.print("Calendar is not equal to one year ROLLBACK");
			       getSessionContext().setRollbackOnly();
			       throw new GlACVNotTotalToOneYearException();
			       		
			    } else {
			    	
			    	if (!i.hasNext()) {
			    	
				    	GregorianCalendar gc = new GregorianCalendar();
				    	gc.setTime(details.getAcvDateTo());
					  	
				    	
					    Collection glSetOfBooks = null;
					    
					    try {
					    	
					    	glSetOfBooks = glSetOfBookHome.findBySobYearEndClosedAndGreaterThanAcYear(EJBCommon.TRUE, new Integer(gc.get(Calendar.YEAR)), AD_CMPNY);
					    	
					    } catch (FinderException ex) {
					    
					    }
					    	
					    	
				    	if (!glSetOfBooks.isEmpty()) {
				    		getSessionContext().setRollbackOnly();
				    		throw new GlobalRecordInvalidException();
				    		
				    	}	
			       }
			    	
			       try {
			       
			          LocalGlAccountingCalendarValue glAccountingCalendarValue =
					     glAccountingCalendarValueHome.create(
					     details.getAcvPeriodPrefix(), details.getAcvQuarter(),
					     details.getAcvPeriodNumber(), details.getAcvDateFrom(),
					     details.getAcvDateTo(),
					     'N', null, null, null, null, AD_CMPNY);
					     
					  addGlAccountingCalendarValueToGlAccountingCalendar(
					     glAccountingCalendarValue.getAcvCode(), 
					     glAccountingCalendar.getAcCode(), AD_CMPNY);
					     
					  // check if first to be used in tc generation
					  
					  if (glAccountingCalendarValue.getAcvPeriodNumber() == 1) {
					  	
					  	  glFirstAccountingCalendarValue = glAccountingCalendarValue;
					  	
					  }
					  
					  // check if last to be used in tc generation
					  
					  if (!i.hasNext()) {
					  	
					  	  glLastAccountingCalendarValue = glAccountingCalendarValue;
					  	  
					  	  GregorianCalendar gcAcvDateTo = new GregorianCalendar();
					  	  gcAcvDateTo.setTime(glLastAccountingCalendarValue.getAcvDateTo());

					  	  try {
					  	  
					  	  	 glAccountingCalendarHome.findByAcYear(new Integer(gcAcvDateTo.get(Calendar.YEAR)), AD_CMPNY);
					  	  	
					  	     throw new GlobalRecordAlreadyExistException();
					  	  
					  	  } catch (FinderException ex) {
					  	  	
					  	  }
					  	  
					  	  glAccountingCalendar.setAcYear(new Integer(gcAcvDateTo.get(Calendar.YEAR)));
					  	  
					  }
					  
			       } catch (GlobalRecordAlreadyExistException ex) {  
			       	
			       	   getSessionContext().setRollbackOnly();
			       	   throw ex;
					     
			       } catch (Exception ex) {
			       	
			       	  Debug.printStackTrace(ex);
			          getSessionContext().setRollbackOnly();
				  	  throw new EJBException(ex.getMessage());
				  	  
			       }
		       }
	      }
	      
	      
	      
	      try {
	      	
	          // generate transaction calendar
	          
		      LocalGlTransactionCalendar glTransactionCalendar = 
		          glTransactionCalendarHome.create(glAccountingCalendar.getAcName(),
		              glAccountingCalendar.getAcDescription(), AD_CMPNY);
		      
		      GregorianCalendar gc = new GregorianCalendar();
		      gc.setTime(glFirstAccountingCalendarValue.getAcvDateFrom());
		      
		      while (!gc.getTime().after(glLastAccountingCalendarValue.getAcvDateTo())) {
		      			      	  		      	  
		      	  LocalGlTransactionCalendarValue glTransactionCalendarValue =
		      	      glTransactionCalendarValueHome.create(gc.getTime(),
		      	          (short)gc.get(Calendar.DAY_OF_WEEK),
		      	          EJBCommon.TRUE, AD_CMPNY);
		      	          
		      	 //glTransactionCalendar.addGlTransactionCalendarValue(glTransactionCalendarValue);
		      	glTransactionCalendarValue.setGlTransactionCalendar(glTransactionCalendar);
		      	  gc.add(Calendar.DATE, 1);
		      	          
		      	
		      }
		      
		      // generate set of book
		      
		      LocalGlSetOfBook glSetOfBook = glSetOfBookHome.create(EJBCommon.FALSE, AD_CMPNY);
		      
		      //glAccountingCalendar.addGlSetOfBook(glSetOfBook);
		      glSetOfBook.setGlAccountingCalendar(glAccountingCalendar);
		      //glTransactionCalendar.addGlSetOfBook(glSetOfBook);
		      glSetOfBook.setGlTransactionCalendar(glTransactionCalendar);
		      
		      // generate coa balances
		      
		      Collection glChartOfAccounts = glChartOfAccountHome.findCoaAll(AD_CMPNY);
		      glAccountingCalendarValues = glAccountingCalendar.getGlAccountingCalendarValues();
		      
		      Iterator iterCoa = glChartOfAccounts.iterator();
		      
		      while (iterCoa.hasNext()) {
		      	
		      	 LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount) iterCoa.next();
		      	 
		         Iterator iterAcv = glAccountingCalendarValues.iterator();
		         
			     while (iterAcv.hasNext()) {
			     	
			     	LocalGlAccountingCalendarValue glAccountingCalendarValue =
			      	   (LocalGlAccountingCalendarValue) iterAcv.next();
			      	
			      	LocalGlChartOfAccountBalance glChartOfAccountBalance = glChartOfAccountBalanceHome.create(0d,0d,0d,0d, AD_CMPNY);
			      	//glAccountingCalendarValue.addGlChartOfAccountBalance(glChartOfAccountBalance);
			      	glChartOfAccountBalance.setGlAccountingCalendarValue(glAccountingCalendarValue);
		      		//glChartOfAccount.addGlChartOfAccountBalance(glChartOfAccountBalance);
		      		glChartOfAccountBalance.setGlChartOfAccount(glChartOfAccount);
		      		
		         }
		         
		      }
		      
		      
		      
		      
		      
		      
		   // generate investor balances
			     
		      Collection glInvestors = apSupplierHome.findAllSplInvestor(AD_CMPNY);
		      glAccountingCalendarValues = glAccountingCalendar.getGlAccountingCalendarValues();
		      
		      Iterator iterInvtr = glInvestors.iterator();
		      
		      while (iterInvtr.hasNext()) {
		      	
		    	  LocalApSupplier apSupplier = (LocalApSupplier) iterInvtr.next();
		      	  
		         Iterator iterAcv = glAccountingCalendarValues.iterator();
		         
			     while (iterAcv.hasNext()) {
			     	
			     	LocalGlAccountingCalendarValue glAccountingCalendarValue =
			      	   (LocalGlAccountingCalendarValue) iterAcv.next();
			      	
			     	LocalGlInvestorAccountBalance glInvestorAccountBalance = glInvestorAccountBalanceHome.create(
			     			0d, 0d, (byte)0 , (byte)0, 0d, 0d, 0d, 0d, 0d, 0d, AD_CMPNY);
			     	
			     	glAccountingCalendarValue.addGlInvestorAccountBalance(glInvestorAccountBalance);
			     	apSupplier.addGlInvestorAccountBalance(glInvestorAccountBalance);
			      	
		         }
		         
		      }
		      
		  } catch (Exception ex) {
		  	
		  	  Debug.printStackTrace(ex);
	          getSessionContext().setRollbackOnly();
		  	  throw new EJBException(ex.getMessage());
		  	
		  }
	      
	  }
   }
	      
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlAccountingCalendarValueControllerBean ejbCreate");
      
   }

   // private methods

   private void addGlAccountingCalendarValueToGlAccountingCalendar(Integer ACV_CODE, Integer AC_CODE, Integer AD_CMPNY) {

      Debug.print("GlAccountingCalendarValueControllerBean addGlAccountingCalendarValueToGlAccountingCalendar");

      /********************************************************
         Add relationship of accounting calendar value to the 
	 accounting calendar
      *********************************************************/
      
      
      LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      
      
      // Initialize EJB Home
        
      try {
            
          glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);
          glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
    	  LocalGlAccountingCalendarValue glAccountingCalendarValue = 
    		  glAccountingCalendarValueHome.findByPrimaryKey(ACV_CODE);
    	  LocalGlAccountingCalendar glAccountingCalendar = glAccountingCalendarHome.findByPrimaryKey(AC_CODE);
    	  //glAccountingCalendar.addGlAccountingCalendarValue(glAccountingCalendarValue);
    	  glAccountingCalendarValue.setGlAccountingCalendar(glAccountingCalendar);
      } catch (FinderException ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }
   }

   private boolean hasRelation(LocalGlAccountingCalendar glAccountingCalendar, Integer AD_CMPNY) {

      Debug.print("GlAccountingCalendarValueControllerBean hasRelation");

      Collection glSetOfBooks = null;

      /*********************************************
         Check if has relation with GlSetOfBook
      *********************************************/

      try {
         glSetOfBooks = 
	    glAccountingCalendar.getGlSetOfBooks();
      } catch (Exception ex) {
      }

      if (glSetOfBooks.size() != 0) return true;
      else return false;

   }

   private boolean isPeriodPrefixUnique(GlAccountingCalendarValueDetails details,
      LocalGlAccountingCalendar glAccountingCalendar, Integer AD_CMPNY) {

      Debug.print("GlAccountingCalendarValueControllerBean isPeriodPrefixUnique");

      /************************************************************
         Return true if the ACV's period prefix is unique with the
	 prior added ACVs period prefix, if not unique return 
	 false
      *************************************************************/
      
      
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      
      
      // Initialize EJB Home
        
      try {
            
         glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         LocalGlAccountingCalendarValue glAccountingCalendarValue = 
	    glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
	    glAccountingCalendar.getAcCode(), details.getAcvPeriodPrefix(), AD_CMPNY);
         return false;
      } catch (FinderException ex) {
         return true;
      } catch (Exception ex) {
         return true;
      }
   }

   private boolean isPeriodNumberUnique(GlAccountingCalendarValueDetails details,
      LocalGlAccountingCalendar glAccountingCalendar, Integer AD_CMPNY) {

      Debug.print("GlAccountingCalendarValueControllerBean isPeriodNumberUnique");

      /************************************************************
         Return true if the ACV's period number is unique with the
	 prior added ACVs period number, if not unique return
	 false  
      *************************************************************/
      
      
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      
      
      // Initialize EJB Home
        
      try {
            
         glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         LocalGlAccountingCalendarValue glAccountingCalendarValue =
	    glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
            glAccountingCalendar.getAcCode(), details.getAcvPeriodNumber(), AD_CMPNY);
	 return false;
      } catch (FinderException ex) {
         return true;
      }
   }

   private boolean isPeriodDateCoveredNotOverlapped(GlAccountingCalendarValueDetails details,
      LocalGlAccountingCalendar glAccountingCalendar, Integer AD_CMPNY) {

      Debug.print("GlAccountingCalendarValueControllerBean isPeriodDateCoveredNotOverlapped");

      /***********************************************************
         Return true if ACV's coverage overlaps with prior
	 added ACV's coverage
      ***********************************************************/

      Collection glAccountingCalendarValues = null;

      try {
         glAccountingCalendarValues = glAccountingCalendar.getGlAccountingCalendarValues();
      } catch (Exception ex) {
      }

      Iterator i = glAccountingCalendarValues.iterator();
      while (i.hasNext()) {
         LocalGlAccountingCalendarValue glAccountingCalendarValue =
	    (LocalGlAccountingCalendarValue) i.next();

	 if (details.getAcvDateFrom().getTime() >= glAccountingCalendarValue.getAcvDateFrom().getTime() &&
	    details.getAcvDateFrom().getTime() <= glAccountingCalendarValue.getAcvDateTo().getTime() ||
	    details.getAcvDateTo().getTime() >= glAccountingCalendarValue.getAcvDateFrom().getTime() &&
	    details.getAcvDateTo().getTime() <= glAccountingCalendarValue.getAcvDateTo().getTime()) 
	    return false;

      }

      return true;
   }

   private boolean isLastPeriodEqualToPeriodTypeLastPeriod(GlAccountingCalendarValueDetails details,
      LocalGlAccountingCalendar glAccountingCalendar, Integer AD_CMPNY) {

      Debug.print("GlAccountingCalendarValueControllerBean lastPeriodCheck");

      /****************************************************************
         Return true if last period is equal to PTs last period
      *****************************************************************/

      Collection glAccountingCalendarValues = null;
      LocalGlPeriodType glPeriodType = null;
      
      
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      
      
      // Initialize EJB Home
        
      try {
            
         glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         glPeriodType = glAccountingCalendar.getGlPeriodType();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      try {
         glAccountingCalendarValues = 
	    glAccountingCalendarValueHome.findByAcCodeAndPtPeriodPerYear(
	    glAccountingCalendar.getAcCode(), glPeriodType.getPtPeriodPerYear(), AD_CMPNY);
      } catch (Exception ex) {
      }

      if (glAccountingCalendarValues.size() == (glPeriodType.getPtPeriodPerYear() - 1) &&
         details.getAcvPeriodNumber() > 0 && 
	 details.getAcvPeriodNumber() <= glPeriodType.getPtPeriodPerYear())
         return true;
      else 
         return false;

   }

   private boolean isQuarterSequentialAndHasNoGap(GlAccountingCalendarValueDetails details,
      LocalGlAccountingCalendar glAccountingCalendar, Integer AD_CMPNY) {

      Debug.print("GlAccountingCalendarValueControllerBean isQuarterSequentialAndHasNoGap");

      /************************************************************
         Return true if ACV's quarter is sequential and has
         no gaps
       ************************************************************/
       
     
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      
      
      // Initialize EJB Home
        
      try {
            
         glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }  

      LocalGlPeriodType glPeriodType = null;

      try {
         glPeriodType = glAccountingCalendar.getGlPeriodType();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      LocalGlAccountingCalendarValue glAccountingCalendarValue = null;
      LocalGlAccountingCalendarValue glAccountingCalendarValueNext = null;

      for (short i = 1; i < glPeriodType.getPtPeriodPerYear() - 1; i++) {

         try {
            glAccountingCalendarValue =
	       glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
	       glAccountingCalendar.getAcCode(), i, AD_CMPNY);

	    glAccountingCalendarValueNext =
	       glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
	       glAccountingCalendar.getAcCode(), (short)(i + 1), AD_CMPNY);
	 } catch (Exception ex) {
	    throw new EJBException();
	 }

	 if (i == 1 && glAccountingCalendarValue.getAcvQuarter() != 1)
	    return false;

	 if (glAccountingCalendarValueNext.getAcvQuarter() != 
	    glAccountingCalendarValue.getAcvQuarter() &&
	    glAccountingCalendarValueNext.getAcvQuarter() !=
	    glAccountingCalendarValue.getAcvQuarter() + 1)
	    return false;
      }

      if (details.getAcvQuarter() != 4 ||
         details.getAcvQuarter() != glAccountingCalendarValueNext.getAcvQuarter() &&
	 details.getAcvQuarter() != glAccountingCalendarValueNext.getAcvQuarter() + 1)
         return false;

      return true;
   } 

   private boolean isDateSequentialAndHasNoGap(GlAccountingCalendarValueDetails details, 
      LocalGlAccountingCalendar glAccountingCalendar, Integer AD_CMPNY) {

      Debug.print("GlAccountingCalendarValueControllerBean isDateSequentialAndHasNoGap"); 

      /******************************************************************
         Returns true if ACVs date has no gap and is sequential but
	 excludes validation on adjusting periods
      ******************************************************************/
 
      LocalGlPeriodType glPeriodType = null;
      GregorianCalendar gc = new GregorianCalendar();
      GregorianCalendar gcNext = new GregorianCalendar();
      LocalGlAccountingCalendarValue glAccountingCalendarValue = null;
      LocalGlAccountingCalendarValue glAccountingCalendarValueNext = null;
      
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      
      
      // Initialize EJB Home
        
      try {
            
         glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         glPeriodType = glAccountingCalendar.getGlPeriodType();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      for (short i = 1; i < glPeriodType.getPtPeriodPerYear() - 1; i++) {
      
         try {
            glAccountingCalendarValue =
               glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
               glAccountingCalendar.getAcCode(), i, AD_CMPNY);

            glAccountingCalendarValueNext =
               glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
               glAccountingCalendar.getAcCode(), (short)(i + 1), AD_CMPNY);
         } catch (FinderException ex) {
            throw new EJBException();
         } catch (Exception ex) {
	    throw new EJBException(ex.getMessage());
	 }

	 gc.setTime(new Date(glAccountingCalendarValue.getAcvDateTo().getTime()));
	 gcNext.setTime(new Date(glAccountingCalendarValueNext.getAcvDateFrom().getTime()));

	 gc.add(Calendar.DATE, 1);

         if (!(gc.get(Calendar.MONTH) == gcNext.get(Calendar.MONTH) &&
	    gc.get(Calendar.DATE) == gcNext.get(Calendar.DATE) &&
	    gc.get(Calendar.YEAR) == gcNext.get(Calendar.YEAR)))
	    return false;

      }

      gc.setTime(new Date(glAccountingCalendarValueNext.getAcvDateTo().getTime()));
      gcNext.setTime(new Date(details.getAcvDateFrom().getTime()));
      gc.add(Calendar.DATE, 1);

      if (!(gc.get(Calendar.MONTH) == gcNext.get(Calendar.MONTH) &&
         gc.get(Calendar.DATE) == gcNext.get(Calendar.DATE) &&
	 gc.get(Calendar.YEAR) == gcNext.get(Calendar.YEAR)))
	 return false;

      return true;

   }

   private boolean isAccountingCalendarOneYear(GlAccountingCalendarValueDetails details,
      LocalGlAccountingCalendar glAccountingCalendar, Integer AD_CMPNY) {

      Debug.print("GlAccountingCalendarValueControllerBean isAccountingCalendarOneYear");

      /************************************************************
         Returns true if first period's date from and last
	 period's date to equals one year with or without a
	 leap year, otherwise return false
       ***********************************************************/

      GregorianCalendar gcFirstDateFrom = new GregorianCalendar();
      GregorianCalendar gcLastDateTo = new GregorianCalendar();
      LocalGlAccountingCalendarValue glAccountingCalendarValue = null;
      
      
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      
      
      // Initialize EJB Home
        
      try {
            
         glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
             lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         glAccountingCalendarValue =
            glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
            glAccountingCalendar.getAcCode(), (short)1, AD_CMPNY);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      gcFirstDateFrom.setTime(new Date(glAccountingCalendarValue.getAcvDateFrom().getTime()));
      gcLastDateTo.setTime(new Date(details.getAcvDateTo().getTime()));

      gcFirstDateFrom.add(Calendar.DATE, 364);

      if (gcFirstDateFrom.get(Calendar.MONTH) == gcLastDateTo.get(Calendar.MONTH) &&
         gcFirstDateFrom.get(Calendar.DATE) == gcLastDateTo.get(Calendar.DATE) &&
         gcFirstDateFrom.get(Calendar.YEAR) == gcLastDateTo.get(Calendar.YEAR))
         return true;
      else {
         gcFirstDateFrom.add(Calendar.DATE, 1);
	 if (gcFirstDateFrom.get(Calendar.MONTH) == gcLastDateTo.get(Calendar.MONTH) &&
            gcFirstDateFrom.get(Calendar.DATE) == gcLastDateTo.get(Calendar.DATE) &&
	    gcFirstDateFrom.get(Calendar.YEAR) == gcLastDateTo.get(Calendar.YEAR))
	    return true;
      }

      return false;
   }


}
