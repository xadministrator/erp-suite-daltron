
/*
 * GlApprovalControllerBean.java
 *
 * Created on March 24, 2004, 8:37 PM
 *
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.util.AbstractSessionBean;
import com.util.AdModApprovalQueueDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="GlApprovalControllerEJB"
 *           display-name="Used for approving gl documents"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlApprovalControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlApprovalController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlApprovalControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlApprovalControllerBean extends AbstractSessionBean {   
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdAqByAqDocumentAndUserName(HashMap criteria,
        String USR_NM, Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlApprovalControllerBean getAdAqByAqDocumentAndUserName");
        
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalGlJournalHome glJournalHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try {
			
			StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(aq) FROM AdApprovalQueue aq ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("document")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocument=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("document");
        		ctr++;
        	}
        	
        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("aq.aqDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	}
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("aq.aqAdBranch=" + AD_BRNCH + " AND aq.aqAdCompany=" + AD_CMPNY + " " + "AND aq.adUser.usrName='" + USR_NM + "'");
        	
        	String orderBy = null;
        	
        	if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
        		
        		orderBy = "aq.aqDocumentNumber";
        		
        	} else if (ORDER_BY.equals("DATE")) {
        		
        		orderBy = "aq.aqDate";
        		
        	}
        	
        	if (orderBy != null) {
        		
        		jbossQl.append("ORDER BY " + orderBy + ", aq.aqDate");
        		
        	} else {
        		
        		jbossQl.append("ORDER BY aq.aqDate");
        		
        	}
        	
        	jbossQl.append(" OFFSET ?" + (ctr + 1));	        
        	obj[ctr] = OFFSET;	      
        	ctr++;
        	
        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;
		           
		    Collection adApprovalQueues = adApprovalQueueHome.getAqByCriteria(jbossQl.toString(), obj);
		    
		    String AQ_DCMNT = (String)criteria.get("document"); 

		    if (adApprovalQueues.size() == 0)
        		throw new GlobalNoRecordFoundException();

		    Iterator i = adApprovalQueues.iterator();
		    
		    while (i.hasNext()) {
		    	
		    	LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
		    	
		    	AdModApprovalQueueDetails details = new AdModApprovalQueueDetails();
		    	
		    	details.setAqDocument(adApprovalQueue.getAqDocument());
		    	details.setAqDocumentCode(adApprovalQueue.getAqDocumentCode());
		    			    		
		    	LocalGlJournal glJournal = glJournalHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
		    	
		    	details.setAqJrName(glJournal.getJrName());
		    	details.setAqDate(glJournal.getJrEffectiveDate());
		    	details.setAqDocumentNumber(glJournal.getJrDocumentNumber());
		    	
		    	double TOTAL_DEBIT = 0d;
		    	
		    	Collection glJournalLines = glJournal.getGlJournalLines();
		    	
		    	Iterator j = glJournalLines.iterator();	    	
		    			    	
		    	while (j.hasNext()) {
		    		
		    		LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();
		    		
		    		if (glJournalLine.getJlDebit() == EJBCommon.TRUE) {
		    			
		    			TOTAL_DEBIT += glJournalLine.getJlAmount();
		    			
		    		}
		    		
		    	}
		    	
		    	details.setAqAmount(TOTAL_DEBIT);		    				    		
		    	
		    	list.add(details);
		    	
		    }
		    
		    return list;
		  
		} catch (GlobalNoRecordFoundException ex) {
		  	 
			throw ex;
		  	
		} catch (Exception ex) {
		  	
	
		  	ex.printStackTrace();
		  	throw new EJBException(ex.getMessage());
		  	
		}
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeGlApproval(String AQ_DCMNT, Integer AQ_DCMNT_CODE, String USR_NM, boolean isApproved, String RSN_FR_RJCTN, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlJREffectiveDatePeriodClosedException,
        GlobalTransactionAlreadyPostedException {
                    
        Debug.print("GlApprovalControllerBean executeGlApproval");
        
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
                
        LocalAdApprovalQueue adApprovalQueue = null;         
                
        // Initialize EJB Home
        
        try {
            
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);            
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
                                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	
        	// validate if approval queue is already deleted
        	
        	try {
        		        			
        		adApprovalQueue = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeAndUsrName(AQ_DCMNT, AQ_DCMNT_CODE, USR_NM, AD_CMPNY);
        			        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}        	
        	
        	// approve/reject
        	
        	Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        	
        	Collection adApprovalQueuesDesc = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeLessThanDesc(AQ_DCMNT, AQ_DCMNT_CODE, adApprovalQueue.getAqCode(), AD_CMPNY);
        	Collection adApprovalQueuesAsc = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeGreaterThanAsc(AQ_DCMNT, AQ_DCMNT_CODE, adApprovalQueue.getAqCode(), AD_CMPNY);
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	        	        		
    		LocalGlJournal glJournal = glJournalHome.findByPrimaryKey(AQ_DCMNT_CODE);
    		        		
    		if (isApproved) {
    			        			
    			if (adApprovalQueue.getAqAndOr().equals("AND")) {
    				
    				if (adApprovalQueues.size() == 1) {
    					        					
    					glJournal.setJrApprovalStatus("APPROVED");
    					glJournal.setJrApprovedRejectedBy(USR_NM);
    					glJournal.setJrDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
    					
    					if (adPreference.getPrfGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
    		
			    		   this.executeGlJrPost(glJournal.getJrCode(), USR_NM, AD_BRNCH, AD_CMPNY);
			    		
			    	    }
    					
    					adApprovalQueue.remove();
    					
    				} else {
    					    					
    					// looping up
    					Iterator i = adApprovalQueuesDesc.iterator();
        				
        				while (i.hasNext()) {
        					
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        				
        					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
    							
    							i.remove();
            					adRemoveApprovalQueue.remove();
            					
    						} else {
    							
    							break;
    							
    						}
        						
        				}
        				
        				// looping down
        				if(adApprovalQueue.getAqUserOr() == (byte)1) {
        					
        					boolean first = true;
        					
        					i = adApprovalQueuesAsc.iterator();
            				
            				while (i.hasNext()) {
            					
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            					
            					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							
        							i.remove();
                					adRemoveApprovalQueue.remove();
                					
                					if(first)
                						first = false;
                					
        						} else {
        							
        							break;
        							
        						}
            						
            				}
            				
        				}
        				
        				adApprovalQueue.remove();
    					
    					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        	        	
    					if(adRemoveApprovalQueues.size() == 0) {
        	        		
    						glJournal.setJrApprovalStatus("APPROVED");
        					glJournal.setJrApprovedRejectedBy(USR_NM);
        					glJournal.setJrDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					
        					if (adPreference.getPrfGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		
    			    		   this.executeGlJrPost(glJournal.getJrCode(), USR_NM, AD_BRNCH, AD_CMPNY);
    			    		
    			    	    }
        					
        	        	}
        				
    				}
    				        				        				
    			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
    				
    				glJournal.setJrApprovalStatus("APPROVED");
    				glJournal.setJrApprovedRejectedBy(USR_NM);
    				glJournal.setJrDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
    				
    				Iterator i = adApprovalQueues.iterator();
    				
    				while (i.hasNext()) {
    					
    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
    					
    					i.remove();
    					adRemoveApprovalQueue.remove();
    					
    				}
    				
    				if (adPreference.getPrfGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
    		
		    		   this.executeGlJrPost(glJournal.getJrCode(), USR_NM, AD_BRNCH, AD_CMPNY);
		    		
		    	    }
    				    				
    			}
    			    			    			    			    			        			
    		} else if (!isApproved) {
    			
    			glJournal.setJrApprovalStatus(null);
    			glJournal.setJrReasonForRejection(RSN_FR_RJCTN);
				glJournal.setJrApprovedRejectedBy(USR_NM);
				glJournal.setJrDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
				
				Iterator i = adApprovalQueues.iterator();
				
				while (i.hasNext()) {
					
					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
					
					i.remove();
					adRemoveApprovalQueue.remove();
					
				}
    			        			
    		}       	        	        	
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }


   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("GlApprovalControllerBean getGlFcPrecisionUnit");

       	
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    // private methods
    
    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("GlApprovalControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}		     
	
	private void executeGlJrPost(Integer JR_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws 
    	GlobalTransactionAlreadyPostedException,
		GlJREffectiveDatePeriodClosedException,
		GlobalRecordAlreadyDeletedException {

       Debug.print("GlApprovalControllerBean executeGlJrPost");

      
       LocalGlSetOfBookHome glSetOfBookHome = null;
       LocalGlJournalHome glJournalHome = null;
       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       LocalGlChartOfAccountHome glChartOfAccountHome = null;
       LocalGlForexLedgerHome glForexLedgerHome = null;
       LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;      
     
       // Initialize EJB Home
        
       try {
            
           glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);       
           glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);          
           glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);          
           glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
           glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
		   	  lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
           glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
		   	  lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);

       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
       	   // get journal
       	
       	   LocalGlJournal glJournal = null;
       	
       	   try {
       	   	
       	   	   glJournal = glJournalHome.findByPrimaryKey(JR_CODE);
       	   	
       	   } catch (FinderException ex) {
       	   	
       	   	   throw new GlobalRecordAlreadyDeletedException();
       	   	
       	   }       	  
       	          	  	 
       	   // validate if period is closed       	  	 
       	          	  	     	
	       LocalGlSetOfBook glSetOfBook = glSetOfBookHome.findByDate(glJournal.getJrEffectiveDate(), AD_CMPNY);
	     	
	       LocalGlAccountingCalendarValue glAccountingCalendarValue =
	     	   glAccountingCalendarValueHome.findByAcCodeAndDate(
	     	     	glSetOfBook.getGlAccountingCalendar().getAcCode(),
	     	    	glJournal.getJrEffectiveDate(), AD_CMPNY);
	     	
	       if (glAccountingCalendarValue.getAcvStatus() == 'C' ||
	           glAccountingCalendarValue.getAcvStatus() == 'P' ||
			   glAccountingCalendarValue.getAcvStatus() == 'N') {
	           		           	
	           throw new GlJREffectiveDatePeriodClosedException(glJournal.getJrName());
	           	
	       }
	       
	       
	       // validate if journal is already posted
	       
	       if (glJournal.getJrPosted() == EJBCommon.TRUE) {
	       	
	       	   throw new GlobalTransactionAlreadyPostedException(glJournal.getJrName());	       	
	       }
	       
	       // post journal
	       
	       Collection glJournalLines = glJournal.getGlJournalLines();
	       
	       Iterator i = glJournalLines.iterator();
	       
	       while (i.hasNext()) {
	       	
	           LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
	           
	           double JL_AMNT = this.convertForeignToFunctionalCurrency(
						      	   glJournal.getGlFunctionalCurrency().getFcCode(),
						      	   glJournal.getGlFunctionalCurrency().getFcName(),
						      	   glJournal.getJrConversionDate(),
						      	   glJournal.getJrConversionRate(),
						      	   glJournalLine.getJlAmount(), AD_CMPNY);
	           
	           // post current to current acv
	             
	           this.post(glAccountingCalendarValue,
	               glJournalLine.getGlChartOfAccount(),
	               true, glJournalLine.getJlDebit(), JL_AMNT, AD_BRNCH, AD_CMPNY);
		 	 
	         
	           // post to subsequent acvs (propagate)
	         
	           Collection glSubsequentAccountingCalendarValues = 
	               glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
	                   glSetOfBook.getGlAccountingCalendar().getAcCode(),
	                   glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
	                 
	           Iterator j = glSubsequentAccountingCalendarValues.iterator();
	         
	           while (j.hasNext()) {
	         	
	         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
	         	       (LocalGlAccountingCalendarValue)j.next();
	         	       
	         	   this.post(glSubsequentAccountingCalendarValue,
	         	       glJournalLine.getGlChartOfAccount(),
	         	       false, glJournalLine.getJlDebit(), JL_AMNT, AD_BRNCH, AD_CMPNY);
	         		         	
	           }             
	           
	           // post to subsequent years if necessary
	           
	           Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
	           LocalAdCompany  adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	           
		  	  	if (!glSubsequentSetOfBooks.isEmpty() && glSetOfBook.getSobYearEndClosed() == 1) {

			  	  	LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
		  	  	
			  	  	Iterator sobIter = glSubsequentSetOfBooks.iterator();
			  	  	
			  	  	while (sobIter.hasNext()) {
			  	  		
			  	  		LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
			  	  		
			  	  		String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
			  	  		
			  	  		// post to subsequent acvs of subsequent set of book(propagate)
	         
			           Collection glAccountingCalendarValues = 
			               glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
			                 
			           Iterator acvIter = glAccountingCalendarValues.iterator();
			         
			           while (acvIter.hasNext()) {
			         	
			         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
			         	       (LocalGlAccountingCalendarValue)acvIter.next();
			         	       
			         	    if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
				 				ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
			         	       
					         	this.post(glSubsequentAccountingCalendarValue,
					         	     glJournalLine.getGlChartOfAccount(),
					         	     false, glJournalLine.getJlDebit(), JL_AMNT, AD_BRNCH, AD_CMPNY);
					         	     
					        } else { // revenue & expense
					        					             					             
					             this.post(glSubsequentAccountingCalendarValue,
					         	     glRetainedEarningsAccount,
					         	     false, glJournalLine.getJlDebit(), JL_AMNT, AD_BRNCH, AD_CMPNY);					        
					        
					        }
			         		         	
			           }
			  	  		
			  	  	   if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
			  	  		
			  	  	}
			  	  	
		  	  	}
		  	  	
		  	  	// for FOREX revaluation
		  	  	if((glJournal.getGlFunctionalCurrency().getFcCode() !=
		  	  			adCompany.getGlFunctionalCurrency().getFcCode()) &&
					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
		  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
		  	  			glJournal.getGlFunctionalCurrency().getFcCode()))){
		  	  		
		  	  		double CONVERSION_RATE = 1;
		  	  		
		  	  		if (glJournal.getJrConversionRate() != 0 && glJournal.getJrConversionRate() != 1) {
		  	  			
		  	  			CONVERSION_RATE = glJournal.getJrConversionRate();
		  	  			
		  	  		} else if (glJournal.getJrConversionDate() != null){
		  	  			
		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
								glJournal.getJrConversionDate(), AD_CMPNY);
		  	  			
		  	  		}
		  	  		
		  	  		Collection glForexLedgers = null;
		  	  		
		  	  		try {
		  	  			
		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
		  	  					glJournal.getJrEffectiveDate(), glJournalLine.getGlChartOfAccount().getCoaCode(),
								AD_CMPNY);
		  	  			
		  	  		} catch(FinderException ex) {
		  	  			
		  	  		}
		  	  		
		  	  		LocalGlForexLedger glForexLedger =
		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();
		  	  		
		  	  		int FRL_LN = (glForexLedger != null &&
		  	  				glForexLedger.getFrlDate().compareTo(glJournal.getJrEffectiveDate()) == 0) ?
		  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;
		  	  		
		  	  		// compute balance
		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
		  	  		double FRL_AMNT = glJournalLine.getJlAmount();
		  	  		
		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));  
		  	  		else
		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);
		  	  		
		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
		  	  		
		  	  		glForexLedger = glForexLedgerHome.create(glJournal.getJrEffectiveDate(), new Integer (FRL_LN),
		  	  				"JOURNAL", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);
		  	  		
		  	  		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
		  	  		
		  	  		// propagate balances
		  	  		try{
		  	  			
		  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
		  	  					glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
								glForexLedger.getFrlAdCompany());
		  	  			
		  	  		} catch (FinderException ex) {
		  	  			
		  	  		}
		  	  		
		  	  		Iterator itrFrl = glForexLedgers.iterator();
		  	  		
		  	  		while (itrFrl.hasNext()) {
		  	  			
		  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
		  	  			FRL_AMNT = glJournalLine.getJlAmount();
		  	  			
		  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT :
		  	  					(- 1 * FRL_AMNT));
		  	  			else
		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);
		  	  			
		  	  			glForexLedger.setFrlBalance(glForexLedger.getFrlBalance() + FRL_AMNT);
		  	  			
		  	  		}
		  	  		
		  	  	}

           }
           
           // set journal post status
           
           glJournal.setJrPosted(EJBCommon.TRUE);
           glJournal.setJrPostedBy(USR_NM);
           glJournal.setJrDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
	       
       	         	
       	  
       } catch (GlobalRecordAlreadyDeletedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       
       } catch (GlJREffectiveDatePeriodClosedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       	   
       } catch (GlobalTransactionAlreadyPostedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
                                             
       } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
       }

    }
    
   private void post(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
      LocalGlChartOfAccount glChartOfAccount, 
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_BRNCH, Integer AD_CMPNY) {
      	
      Debug.print("GlJournalBatchSubmitControllerBean post");
      
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
              
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {          
               
               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
       	   
	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance = 
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
	               	  
	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
	           
	           
	               	  
	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {				    
				    			
 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
				    
		
			  } else {
	
				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
	
		 	  }
		 	  
		 	  if (isCurrentAcv) { 
		 	 
			 	 if (isDebit == EJBCommon.TRUE) {
			 	 	
		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 				
		 		 } else {
		 		 	
		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 		 }       	   
		 		 
		 	}
       	
       } catch (Exception ex) {
       	
       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());
       	
       }
      
      
   }		    
    
   private double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY) 
   throws GlobalConversionDateNotExistException {
   	
		Debug.print("GlApprovalControllerBean getFrRateByFrNameAndFrDate");
		
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			
			double CONVERSION_RATE = 1;
			
			// Get functional currency rate
			
			if (!FC_NM.equals("USD")) {
				
				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
							CONVERSION_DATE, AD_CMPNY);
				
				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
				
			}
			
			// Get set of book functional currency rate if necessary
			
			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
				
				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
							CONVERSION_DATE, AD_CMPNY);
				
				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();
				
			}
			
			return CONVERSION_RATE;
			
		} catch (FinderException ex) {	
			
			getSessionContext().setRollbackOnly();
			throw new GlobalConversionDateNotExistException();  
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

   
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlApprovalControllerBean ejbCreate");
      
    }
}
