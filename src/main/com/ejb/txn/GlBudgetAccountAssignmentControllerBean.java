package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlBgtAAAccountFromInvalidException;
import com.ejb.exception.GlBgtAAAccountOverlappedException;
import com.ejb.exception.GlBgtAAAccountToInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenFieldHome;
import com.ejb.gl.LocalGlBudgetAccountAssignment;
import com.ejb.gl.LocalGlBudgetAccountAssignmentHome;
import com.ejb.gl.LocalGlBudgetOrganization;
import com.ejb.gl.LocalGlBudgetOrganizationHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlBudgetAccountAssignmentDetails;
import com.util.GlBudgetOrganizationDetails;

/**
 * @ejb:bean name="GlBudgetAccountAssignmentControllerEJB"
 *           display-name="used for defining budget account assignment details"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlBudgetAccountAssignmentControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlBudgetAccountAssignmentController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlBudgetAccountAssignmentControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlBudgetAccountAssignmentControllerBean extends AbstractSessionBean {
	

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlBaaByBoCode(Integer BO_CODE, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlBudgetAccountAssignmentControllerBean getGlBaaByBoCode");
        
        LocalGlBudgetAccountAssignmentHome glBudgetAccountAssignmentHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glBudgetAccountAssignmentHome = (LocalGlBudgetAccountAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetAccountAssignmentHome.JNDI_NAME, LocalGlBudgetAccountAssignmentHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glBudgetAccountAssignments = glBudgetAccountAssignmentHome.findByBoCode(BO_CODE, AD_CMPNY);
            
            if (glBudgetAccountAssignments.isEmpty()) {
            	
            	throw new GlobalNoRecordFoundException();
            	
            }

	        Iterator i = glBudgetAccountAssignments.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalGlBudgetAccountAssignment glBudgetAccountAssignment = (LocalGlBudgetAccountAssignment)i.next();
	        	
	        	GlBudgetAccountAssignmentDetails mdetails = new GlBudgetAccountAssignmentDetails();
	        	mdetails.setBaaCode(glBudgetAccountAssignment.getBaaCode());
	        	mdetails.setBaaAccountFrom(glBudgetAccountAssignment.getBaaAccountFrom());
	        	mdetails.setBaaAccountTo(glBudgetAccountAssignment.getBaaAccountTo());
	        	mdetails.setBaaType(glBudgetAccountAssignment.getBaaType());

	        	list.add(mdetails);
	        	
	        }
	        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;    
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public GlBudgetOrganizationDetails getGlBoByBoCode(Integer BO_CODE, Integer AD_CMPNY) {
                    
        Debug.print("GlBudgetAccountAssignmentControllerBean getGlBoByBoCode");
        
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	LocalGlBudgetOrganization glBudgetOrganization = glBudgetOrganizationHome.findByPrimaryKey(BO_CODE);

        	GlBudgetOrganizationDetails details = new GlBudgetOrganizationDetails();
        	details.setBoName(glBudgetOrganization.getBoName());
        	details.setBoSegmentOrder(glBudgetOrganization.getBoSegmentOrder());
        	
        	return details;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGlBaaEntry(GlBudgetAccountAssignmentDetails details, Integer BO_CODE, Integer AD_CMPNY)
        throws GlBgtAAAccountFromInvalidException,
               GlBgtAAAccountToInvalidException,
               GlBgtAAAccountOverlappedException {
         
        Debug.print("GlBudgetAccountAssignmentControllerBean addGlBaaEntry");
        
        LocalGlBudgetAccountAssignmentHome glBudgetAccountAssignmentHome = null;
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenFieldHome genFieldHome = null;        

        // Initialize EJB Home
        
        try {

        	glBudgetAccountAssignmentHome = (LocalGlBudgetAccountAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetAccountAssignmentHome.JNDI_NAME, LocalGlBudgetAccountAssignmentHome.class);             
        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);             
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                
        	genFieldHome = (LocalGenFieldHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenFieldHome.JNDI_NAME, LocalGenFieldHome.class);    
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
        	
        	// Get GenField's separator and number of segments
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	LocalGenField genField = adCompany.getGenField();
	    	char chrSeparator = genField.getFlSegmentSeparator();
	    	String strSeparator = String.valueOf(chrSeparator);
	    	short genNumberOfSegment = genField.getFlNumberOfSegment();
	        	
	        // Validate Account From	
	        
	        StringTokenizer stAccountFrom = new StringTokenizer(details.getBaaAccountFrom(), strSeparator);
	        
	        if (stAccountFrom.countTokens() != genNumberOfSegment) { 
	        	
	        	throw new GlBgtAAAccountFromInvalidException();
	        	
	        }
	        
	        // Validate Account To
	        
	        StringTokenizer stAccountTo = new StringTokenizer(details.getBaaAccountTo(), strSeparator);
	        
	        if (stAccountTo.countTokens() != genNumberOfSegment) { 
	        	
	        	throw new GlBgtAAAccountToInvalidException();
	        	
	        } 
	        
	        // validate if account range entered does not overlap other budget organizations or self
	        
	        String[] newAccountFromSegmentValue = new String[genNumberOfSegment];
		    String[] newAccountToSegmentValue = new String[genNumberOfSegment];
		    
		    StringTokenizer st = new StringTokenizer(details.getBaaAccountFrom(), strSeparator);
		    int stIndex = 0;
			
			while(st.hasMoreTokens()){
				newAccountFromSegmentValue[stIndex] = st.nextToken();
				stIndex++;
				
			}
			
			st = new StringTokenizer(details.getBaaAccountTo(), strSeparator);
		    stIndex = 0;
			
			while(st.hasMoreTokens()){
				newAccountToSegmentValue[stIndex] = st.nextToken();
				stIndex++;
			}
						
		    
		    
	        
	        Collection glBudgetOrganizations = glBudgetOrganizationHome.findBoAll(AD_CMPNY);
	        
	        Iterator i = glBudgetOrganizations.iterator();
			
			while (i.hasNext()) {
				
				LocalGlBudgetOrganization glBudgetOrganization = (LocalGlBudgetOrganization) i.next();
				
				Collection glBudgetAccountAssignments = glBudgetOrganization.getGlBudgetAccountAssignments();
				
				Iterator j = glBudgetAccountAssignments.iterator();
				
				while (j.hasNext()) {
					
					LocalGlBudgetAccountAssignment glBudgetAccountAssignment = (LocalGlBudgetAccountAssignment)j.next();
					
					String[] accountFromSegmentValue = new String[genNumberOfSegment];
				    String[] accountToSegmentValue = new String[genNumberOfSegment];
				    
					
				    st = new StringTokenizer(glBudgetAccountAssignment.getBaaAccountFrom(), strSeparator);
					stIndex = 0;
					
					while(st.hasMoreTokens()){
						accountFromSegmentValue[stIndex] = st.nextToken();
						stIndex++;
					}
					
					st = new StringTokenizer(glBudgetAccountAssignment.getBaaAccountTo(), strSeparator);
					stIndex = 0;
					
					while(st.hasMoreTokens()){
						accountToSegmentValue[stIndex] = st.nextToken();
						stIndex++;
					}
					
					boolean isOverlapped = false;
					
					/*for (int k=0; k<genNumberOfSegment; k++) {
					     
						if(((newAccountFromSegmentValue[k].compareTo(accountFromSegmentValue[k]) >= 0  && 
							 newAccountFromSegmentValue[k].compareTo(accountToSegmentValue[k]) <= 0) ||		   
							(newAccountToSegmentValue[k].compareTo(accountFromSegmentValue[k]) <= 0 &&
							 newAccountToSegmentValue[k].compareTo(accountToSegmentValue[k]) >= 0)) ||
										
						   ((accountFromSegmentValue[k].compareTo(newAccountFromSegmentValue[k]) >= 0 &&
							 accountFromSegmentValue[k].compareTo(newAccountToSegmentValue[k]) <= 0) ||
							(accountToSegmentValue[k].compareTo(newAccountFromSegmentValue[k]) <= 0 &&
							 accountToSegmentValue[k].compareTo(newAccountToSegmentValue[k]) >= 0))) {
							
							isOverlapped = true;
							
						} else {
							
							isOverlapped = false;
							break;
						}
						
					} */
					
					if (isOverlapped) {
						
						throw new GlBgtAAAccountOverlappedException();
						
					}
					
				}
				
			}
	
			// create new budget account assignment
			
	        LocalGlBudgetAccountAssignment glBudgetAccountAssignment = glBudgetAccountAssignmentHome.create(details.getBaaAccountFrom(), 
					details.getBaaAccountTo(), details.getBaaType(), AD_CMPNY); 
	    	
	    	LocalGlBudgetOrganization glBudgetOrganization = glBudgetOrganizationHome.findByPrimaryKey(BO_CODE);
	    	glBudgetOrganization.addGlBudgetAccountAssignment(glBudgetAccountAssignment);       	

        } catch (GlBgtAAAccountFromInvalidException ex) {
        	
        	throw ex;
	    
        } catch (GlBgtAAAccountToInvalidException ex) {
        	
        	throw ex;
        	
        } catch (GlBgtAAAccountOverlappedException ex) {
        	
        	throw ex;
	    	
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
        	Debug.printStackTrace(ex);        	
            throw new EJBException(ex.getMessage());
        	
        }        
                                    	                                        
    }
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateGlBaaEntry(GlBudgetAccountAssignmentDetails details, Integer BO_CODE, Integer AD_CMPNY)
    	throws GlBgtAAAccountFromInvalidException,
			   GlBgtAAAccountToInvalidException,
			   GlBgtAAAccountOverlappedException {
  
        Debug.print("GlBudgetAccountAssignmentControllerBean updateGlBaaEntry");
        
        LocalGlBudgetAccountAssignmentHome glBudgetAccountAssignmentHome = null;
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenFieldHome genFieldHome = null;    
        
        // Initialize EJB Home
        
        try {

        	glBudgetAccountAssignmentHome = (LocalGlBudgetAccountAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetAccountAssignmentHome.JNDI_NAME, LocalGlBudgetAccountAssignmentHome.class);             
        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);             
	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                
	    	genFieldHome = (LocalGenFieldHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenFieldHome.JNDI_NAME, LocalGenFieldHome.class);  
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	// Get GenField's separator and number of segments
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	LocalGenField genField = adCompany.getGenField();
	    	char chrSeparator = genField.getFlSegmentSeparator();
	    	String strSeparator = String.valueOf(chrSeparator);
	    	short genNumberOfSegment = genField.getFlNumberOfSegment();
	        	
	        // Validate Account From	
	        
	        StringTokenizer stAccountFrom = new StringTokenizer(details.getBaaAccountFrom(), strSeparator);
	        
	        if (stAccountFrom.countTokens() != genNumberOfSegment) { 
	        	
	        	throw new GlBgtAAAccountFromInvalidException();
	        	
	        }
	        
	        // Validate Account To
	        
	        StringTokenizer stAccountTo = new StringTokenizer(details.getBaaAccountTo(), strSeparator);
	        
	        if (stAccountTo.countTokens() != genNumberOfSegment) { 
	        	
	        	throw new GlBgtAAAccountToInvalidException();
	        	
	        }
	        
			// validate if account range entered does not overlap other budget organizations or self
	        
	        String[] newAccountFromSegmentValue = new String[genNumberOfSegment];
		    String[] newAccountToSegmentValue = new String[genNumberOfSegment];
		    
		    StringTokenizer st = new StringTokenizer(details.getBaaAccountFrom(), strSeparator);
		    int stIndex = 0;
			
			while(st.hasMoreTokens()){
				newAccountFromSegmentValue[stIndex] = st.nextToken();
				stIndex++;
			}
			
			st = new StringTokenizer(details.getBaaAccountTo(), strSeparator);
		    stIndex = 0;
			
			while(st.hasMoreTokens()){
				newAccountToSegmentValue[stIndex] = st.nextToken();
				stIndex++;
			}														
			
	        Collection glBudgetOrganizations = glBudgetOrganizationHome.findBoAll(AD_CMPNY);
	        
	        Iterator i = glBudgetOrganizations.iterator();
			
			while (i.hasNext()) {
				
				LocalGlBudgetOrganization glBudgetOrganization = (LocalGlBudgetOrganization) i.next();
				
				Collection glBudgetAccountAssignments = glBudgetOrganization.getGlBudgetAccountAssignments();
				
				Iterator j = glBudgetAccountAssignments.iterator();
				
				while (j.hasNext()) {
					
					LocalGlBudgetAccountAssignment glBudgetAccountAssignment = (LocalGlBudgetAccountAssignment)j.next();
					
					if (!glBudgetAccountAssignment.getBaaCode().equals(details.getBaaCode())) {
					
						String[] accountFromSegmentValue = new String[genNumberOfSegment];
					    String[] accountToSegmentValue = new String[genNumberOfSegment];
					    
						
					    st = new StringTokenizer(glBudgetAccountAssignment.getBaaAccountFrom(), strSeparator);
						stIndex = 0;
						
						while(st.hasMoreTokens()){
							accountFromSegmentValue[stIndex] = st.nextToken();
							stIndex++;
						}
						
						st = new StringTokenizer(glBudgetAccountAssignment.getBaaAccountTo(), strSeparator);
						stIndex = 0;
						
						while(st.hasMoreTokens()){
							accountToSegmentValue[stIndex] = st.nextToken();
							stIndex++;
						}
						
						boolean isOverlapped = false;
						
						/*for (int k=0; k<genNumberOfSegment; k++) {
							if(((newAccountFromSegmentValue[k].compareTo(accountFromSegmentValue[k]) >= 0  && 
								 newAccountFromSegmentValue[k].compareTo(accountToSegmentValue[k]) <= 0) ||		   
								(newAccountToSegmentValue[k].compareTo(accountFromSegmentValue[k]) <= 0 &&
								 newAccountToSegmentValue[k].compareTo(accountToSegmentValue[k]) >= 0)) ||
											
							   ((accountFromSegmentValue[k].compareTo(newAccountFromSegmentValue[k]) >= 0 &&
								 accountFromSegmentValue[k].compareTo(newAccountToSegmentValue[k]) <= 0) ||
								(accountToSegmentValue[k].compareTo(newAccountFromSegmentValue[k]) <= 0 &&
								 accountToSegmentValue[k].compareTo(newAccountToSegmentValue[k]) >= 0))) {
								
								isOverlapped = true;
								
							} else {
								
								isOverlapped = false;
								break;
							}
							
						} 
						*/
						if (isOverlapped) {
							
							throw new GlBgtAAAccountOverlappedException();
							
						}
					}
					
				}
				
			}
			
	        LocalGlBudgetAccountAssignment glBudgetAccountAssignment = glBudgetAccountAssignmentHome.findByPrimaryKey(details.getBaaCode());

        	// Find and Update Budget Account Assignment
        	        	
        	glBudgetAccountAssignment.setBaaAccountFrom(details.getBaaAccountFrom());
        	glBudgetAccountAssignment.setBaaAccountTo(details.getBaaAccountTo());
        	glBudgetAccountAssignment.setBaaType(details.getBaaType());
        	
        	LocalGlBudgetOrganization glBudgetOrganization = glBudgetOrganizationHome.findByPrimaryKey(BO_CODE);
        	glBudgetOrganization.addGlBudgetAccountAssignment(glBudgetAccountAssignment); 
        	
        } catch (GlBgtAAAccountFromInvalidException ex) {
        	
        	throw ex;
	    
        } catch (GlBgtAAAccountToInvalidException ex) {
        	
        	throw ex;
        	
        } catch (GlBgtAAAccountOverlappedException ex) {
        	
        	throw ex;
      	        	                            	
        } catch (Exception ex) {
            
        	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }           	
                                                	                                        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteGlBaaEntry(Integer BAA_CODE, Integer AD_CMPNY) 
    throws GlobalRecordAlreadyDeletedException {               
    	
    	Debug.print("GlBudgetAccountAssignmentControllerBean deleteGlBaaEntry");
    	
    	LocalGlBudgetAccountAssignment glBudgetAccountAssignment = null;
    	LocalGlBudgetAccountAssignmentHome glBudgetAccountAssignmentHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		glBudgetAccountAssignmentHome = (LocalGlBudgetAccountAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlBudgetAccountAssignmentHome.JNDI_NAME, LocalGlBudgetAccountAssignmentHome.class);           
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	} 
    	
    	try {
    	
    		try {
    			
    			glBudgetAccountAssignment = glBudgetAccountAssignmentHome.findByPrimaryKey(BAA_CODE);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlobalRecordAlreadyDeletedException();
    			
    		}
    		
    		glBudgetAccountAssignment.remove();
    		
    		
    	} catch (GlobalRecordAlreadyDeletedException ex) {
    		
    		throw ex;

    	} catch (Exception ex) {
    		
    		getSessionContext().setRollbackOnly();	    
    		throw new EJBException(ex.getMessage());
    		
    	}	      
    	
    } 
    
 	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlBudgetAccountAssignmentControllerBean ejbCreate");
      
    }
    
   // private methods

}