package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.exception.GlBGTFirstLastPeriodDifferentAcYearException;
import com.ejb.exception.GlBGTFirstPeriodGreaterThanLastPeriodException;
import com.ejb.exception.GlBGTStatusAlreadyDefinedException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlBudget;
import com.ejb.gl.LocalGlBudgetHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlBudgetDetails;
import com.util.GlModAccountingCalendarValueDetails;

/**
 * @ejb:bean name="GlBudgetDefinitionControllerEJB"
 *           display-name="used for defining budget details"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlBudgetDefinitionControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlBudgetDefinitionController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlBudgetDefinitionControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlBudgetDefinitionControllerBean extends AbstractSessionBean {
	
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlAcvAll(Integer AD_CMPNY) {
                    
        Debug.print("GlBudgetDefinitionControllerBean getGlAcvAll");
        
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
        	glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
        
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	Collection glSetOfBooks = glSetOfBookHome.findBySobYearEndClosed(EJBCommon.FALSE, AD_CMPNY);
        	
        	Iterator i = glSetOfBooks.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();
        		
        		Collection glAccountingCalendarValues = glSetOfBook.getGlAccountingCalendar().getGlAccountingCalendarValues();
        		
        		Iterator j = glAccountingCalendarValues.iterator();
    	        
    	         while (j.hasNext()) {
    	        	
    	        	LocalGlAccountingCalendarValue glAccountingCalendarValue = (LocalGlAccountingCalendarValue)j.next();
    	        	
    	        	GlModAccountingCalendarValueDetails mdetails = new GlModAccountingCalendarValueDetails();

    	        	mdetails.setAcvPeriodPrefix(glAccountingCalendarValue.getAcvPeriodPrefix());
    	        		            		
            		GregorianCalendar gc = new GregorianCalendar();
            		gc.setTime(glAccountingCalendarValue.getAcvDateTo());
            		
            		mdetails.setAcvYear(gc.get(Calendar.YEAR));
    	        	
    	        	list.add(mdetails);
    	        	
    	         }
        		
        	}

	        return list;

        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlBgtAll(Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {
                    
        Debug.print("GlBudgetDefinitionControllerBean getGlBgtAll");
        
        LocalGlBudgetHome glBudgetHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glBudgetHome = (LocalGlBudgetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetHome.JNDI_NAME, LocalGlBudgetHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glBudgets = glBudgetHome.findBgtAll(AD_CMPNY);
            
            if (glBudgets.isEmpty()) {
            	
            	throw new GlobalNoRecordFoundException();
            	
            }

	        Iterator i = glBudgets.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalGlBudget glBudget = (LocalGlBudget)i.next();
	        	
	        	GlBudgetDetails details = new GlBudgetDetails();
	        	details.setBgtCode(glBudget.getBgtCode());
	        	details.setBgtName(glBudget.getBgtName());
	        	details.setBgtDescription(glBudget.getBgtDescription());
	        	details.setBgtStatus(glBudget.getBgtStatus());
	        	details.setBgtFirstPeriod(glBudget.getBgtFirstPeriod());
	        	details.setBgtLastPeriod(glBudget.getBgtLastPeriod());

	        	list.add(details);
	        	
	        }
	        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;

        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGlBgtEntry(GlBudgetDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
        	GlBGTStatusAlreadyDefinedException,
        	GlBGTFirstPeriodGreaterThanLastPeriodException,
        	GlBGTFirstLastPeriodDifferentAcYearException {
         
        Debug.print("GlBudgetDefinitionControllerBean addGlBgtEntry");
        
        LocalGlBudgetHome glBudgetHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
   
        LocalGlBudget glBudget = null;

        // Initialize EJB Home
        
        try {

        	glBudgetHome = (LocalGlBudgetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetHome.JNDI_NAME, LocalGlBudgetHome.class);  	
        	glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
        	glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class); 
                             
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                                   
        try {
        	
        	try {
        		
        		glBudget = glBudgetHome.findByBgtName(details.getBgtName(), AD_CMPNY);
        		
        		if (glBudget != null) {
        		
        			throw new GlobalRecordAlreadyExistException();
        			
        		}        		
        		        		        		
        	} catch (FinderException ex) {
        		
        	}
        	
        	try {
        	
	        	glBudget = glBudgetHome.findByBgtStatus("CURRENT", AD_CMPNY);
	    		
	    		if (glBudget != null && details.getBgtStatus().equals("CURRENT")) {
	    			
	    			throw new GlBGTStatusAlreadyDefinedException();
	    			
	    		}
    		
	        } catch (FinderException ex) {
	    		
	    	}
	        
	        // validate if first period is not greater than last period
	        
	        Collection glFirstSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(details.getBgtFirstPeriod().substring(0, details.getBgtFirstPeriod().indexOf('-')), 
	                EJBCommon.getIntendedDate(Integer.parseInt(details.getBgtFirstPeriod().substring(details.getBgtFirstPeriod().indexOf('-') + 1))), AD_CMPNY);
            ArrayList glFirstSetOfBookList = new ArrayList(glFirstSetOfBooks);            
            LocalGlSetOfBook glFirstSetOfBook = (LocalGlSetOfBook)glFirstSetOfBookList.get(0);
            LocalGlAccountingCalendarValue glFirstAccountingCalendarValue =
			     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
			     	glFirstSetOfBook.getGlAccountingCalendar().getAcCode(),
			     	details.getBgtFirstPeriod().substring(0, details.getBgtFirstPeriod().indexOf('-')), AD_CMPNY);
            
            
            Collection glLastSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(details.getBgtLastPeriod().substring(0, details.getBgtLastPeriod().indexOf('-')), 
	                EJBCommon.getIntendedDate(Integer.parseInt(details.getBgtLastPeriod().substring(details.getBgtLastPeriod().indexOf('-') + 1))), AD_CMPNY);
            ArrayList glLastSetOfBookList = new ArrayList(glLastSetOfBooks);            
            LocalGlSetOfBook glLastSetOfBook = (LocalGlSetOfBook)glLastSetOfBookList.get(0);
            LocalGlAccountingCalendarValue glLastAccountingCalendarValue =
			     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
			     	glLastSetOfBook.getGlAccountingCalendar().getAcCode(),
			     	details.getBgtLastPeriod().substring(0, details.getBgtLastPeriod().indexOf('-')), AD_CMPNY);
            
            if (glFirstAccountingCalendarValue.getAcvDateTo().after(glLastAccountingCalendarValue.getAcvDateTo())) {
            	
            	throw new GlBGTFirstPeriodGreaterThanLastPeriodException();
            	
            }
            
            if (!glFirstSetOfBook.getSobCode().equals(glLastSetOfBook.getSobCode())) {
            	
            	throw new GlBGTFirstLastPeriodDifferentAcYearException();
            	
            }
        		
    		// create new budget definition
    		
    		glBudget = glBudgetHome.create(details.getBgtName(), 
    				details.getBgtDescription(), details.getBgtStatus(), 
					details.getBgtFirstPeriod(), details.getBgtLastPeriod(), AD_CMPNY); 
        	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
           
        } catch (GlBGTStatusAlreadyDefinedException ex) {
        	
           throw ex;
           
        } catch (GlBGTFirstPeriodGreaterThanLastPeriodException ex) {
        	
           throw ex;
           
        } catch (GlBGTFirstLastPeriodDifferentAcYearException ex) {
        	
           throw ex;

        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();        	
            throw new EJBException(ex.getMessage());
        	
        }        
                                    	                                        
    }
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateGlBgtEntry(GlBudgetDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               GlobalRecordAlreadyAssignedException,
               GlBGTStatusAlreadyDefinedException,
               GlBGTFirstPeriodGreaterThanLastPeriodException,
               GlBGTFirstLastPeriodDifferentAcYearException {

         
        Debug.print("GlBudgetDefinitionControllerBean updateGlBgtEntry");
        
        LocalGlBudgetHome glBudgetHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        
        LocalGlBudget glExistingBudget = null;
                
        // Initialize EJB Home
        
        try {

        	glBudgetHome = (LocalGlBudgetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetHome.JNDI_NAME, LocalGlBudgetHome.class);
        	glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
        	glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class); 
                                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	        	
	        try {
	        	
	        	glExistingBudget = glBudgetHome.findByBgtName(details.getBgtName(), AD_CMPNY);

	        	if(glExistingBudget != null &&
	        		!glExistingBudget.getBgtCode().equals(details.getBgtCode()))  {  
	        		
	        		throw new GlobalRecordAlreadyExistException();
	        		
	        	}
	        	
	        	

	        } catch (FinderException ex) {
	        		        	
	        }
	        
	        try {
	        
	        	glExistingBudget = glBudgetHome.findByBgtStatus("CURRENT", AD_CMPNY);
	    		
	    		if (glExistingBudget != null &&
	    			!glExistingBudget.getBgtCode().equals(details.getBgtCode()) &&
	    		    details.getBgtStatus().equals("CURRENT")) {
	    			
	    			throw new GlBGTStatusAlreadyDefinedException();
	    			
	    		}
    		
			} catch (FinderException ex) {
			        	
			}
			
			// validate if first period is not greater than last period
	        
	        Collection glFirstSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(details.getBgtFirstPeriod().substring(0, details.getBgtFirstPeriod().indexOf('-')), 
	                EJBCommon.getIntendedDate(Integer.parseInt(details.getBgtFirstPeriod().substring(details.getBgtFirstPeriod().indexOf('-') + 1))), AD_CMPNY);
            ArrayList glFirstSetOfBookList = new ArrayList(glFirstSetOfBooks);            
            LocalGlSetOfBook glFirstSetOfBook = (LocalGlSetOfBook)glFirstSetOfBookList.get(0);
            LocalGlAccountingCalendarValue glFirstAccountingCalendarValue =
			     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
			     	glFirstSetOfBook.getGlAccountingCalendar().getAcCode(),
			     	details.getBgtFirstPeriod().substring(0, details.getBgtFirstPeriod().indexOf('-')), AD_CMPNY);
            
            
            Collection glLastSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(details.getBgtLastPeriod().substring(0, details.getBgtLastPeriod().indexOf('-')), 
	                EJBCommon.getIntendedDate(Integer.parseInt(details.getBgtLastPeriod().substring(details.getBgtLastPeriod().indexOf('-') + 1))), AD_CMPNY);
            ArrayList glLastSetOfBookList = new ArrayList(glLastSetOfBooks);            
            LocalGlSetOfBook glLastSetOfBook = (LocalGlSetOfBook)glLastSetOfBookList.get(0);
            LocalGlAccountingCalendarValue glLastAccountingCalendarValue =
			     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
			     	glLastSetOfBook.getGlAccountingCalendar().getAcCode(),
			     	details.getBgtLastPeriod().substring(0, details.getBgtLastPeriod().indexOf('-')), AD_CMPNY);
            
            if (glFirstAccountingCalendarValue.getAcvDateTo().after(glLastAccountingCalendarValue.getAcvDateTo())) {
            	
            	throw new GlBGTFirstPeriodGreaterThanLastPeriodException();
            	
            }
            
            if (!glFirstSetOfBook.getSobCode().equals(glLastSetOfBook.getSobCode())) {
            	
            	throw new GlBGTFirstLastPeriodDifferentAcYearException();
            	
            }
	        
	        LocalGlBudget glBudget = glBudgetHome.findByPrimaryKey(details.getBgtCode());
        	
        	if (!glBudget.getGlBudgetAmounts().isEmpty() && (!glBudget.getBgtName().equals(details.getBgtName()) || 
        	    !glBudget.getBgtFirstPeriod().equals(details.getBgtFirstPeriod()) || 
        	    !glBudget.getBgtLastPeriod().equals(details.getBgtLastPeriod()))) {
        		
        		throw new GlobalRecordAlreadyAssignedException();
        	}
        	
        	// Find and Update Budget Definition
        	        	
        	glBudget.setBgtName(details.getBgtName());
        	glBudget.setBgtDescription(details.getBgtDescription());
        	glBudget.setBgtStatus(details.getBgtStatus());
        	glBudget.setBgtFirstPeriod(details.getBgtFirstPeriod());
        	glBudget.setBgtLastPeriod(details.getBgtLastPeriod());
        	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	throw ex;
        	
        } catch (GlBGTStatusAlreadyDefinedException ex) {
        	
        	throw ex;
        	
        } catch (GlBGTFirstPeriodGreaterThanLastPeriodException ex) {
        	
        	throw ex;
        	
        } catch (GlBGTFirstLastPeriodDifferentAcYearException ex) {
        	
           throw ex;
      	        	                            	
        } catch (Exception ex) {
            
        	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }           	
                                                	                                        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
     public void deleteGlBgtEntry(Integer BGT_CODE, Integer AD_CMPNY) 
         throws GlobalRecordAlreadyDeletedException,
               GlobalRecordAlreadyAssignedException {               

       Debug.print("GlFrgColumnEntryControllerBean deleteGlBgtEntry");

       LocalGlBudget glBudget = null;
       LocalGlBudgetHome glBudgetHome = null;

       // Initialize EJB Home
         
       try {

        	glBudgetHome = (LocalGlBudgetHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlBudgetHome.JNDI_NAME, LocalGlBudgetHome.class);           
       
       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }                
       
       try {
       	
        	glBudget = glBudgetHome.findByPrimaryKey(BGT_CODE);
          
        } catch (FinderException ex) {
       	
          throw new GlobalRecordAlreadyDeletedException();
          
       	}
        
        try {

       		if (!glBudget.getGlBudgetAmounts().isEmpty()) {
       		
       			throw new GlobalRecordAlreadyAssignedException();
       		}
          	
        	glBudget.remove();
 	    
 	  } catch (GlobalRecordAlreadyAssignedException ex) {
    
 	      throw ex;
 	    
 	  } catch (Exception ex) {
 	 	
 	      getSessionContext().setRollbackOnly();	    
 	      throw new EJBException(ex.getMessage());
 	    
 	  }	      
       
    } 
    
 	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlBudgetDefinitionControllerBean ejbCreate");
      
    }
    
   // private methods

}