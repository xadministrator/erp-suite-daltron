 package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlBGAPasswordInvalidException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlBudget;
import com.ejb.gl.LocalGlBudgetAccountAssignment;
import com.ejb.gl.LocalGlBudgetAmount;
import com.ejb.gl.LocalGlBudgetAmountCoa;
import com.ejb.gl.LocalGlBudgetAmountCoaHome;
import com.ejb.gl.LocalGlBudgetAmountHome;
import com.ejb.gl.LocalGlBudgetAmountPeriod;
import com.ejb.gl.LocalGlBudgetAmountPeriodHome;
import com.ejb.gl.LocalGlBudgetHome;
import com.ejb.gl.LocalGlBudgetOrganization;
import com.ejb.gl.LocalGlBudgetOrganizationHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModBudgetAmountCoaDetails;
import com.util.GlModBudgetAmountDetails;
import com.util.GlModBudgetAmountPeriodDetails;

/**
 * @ejb:bean name="GlBudgetEntryControllerEJB"
 *           display-name="used for entring budget details"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlBudgetEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlBudgetEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlBudgetEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlBudgetEntryControllerBean extends AbstractSessionBean {
	
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlBoAll(Integer AD_CMPNY) {
                    
        Debug.print("GlBudgetEntryControllerBean getGlBoAll");
        
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glBudgetOrganizations = glBudgetOrganizationHome.findBoAll(AD_CMPNY);

	        Iterator i = glBudgetOrganizations.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalGlBudgetOrganization glBudgetOrganization = (LocalGlBudgetOrganization)i.next();

	        	list.add(glBudgetOrganization.getBoName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlBgtAll(Integer AD_CMPNY) {
                    
        Debug.print("GlBudgetEntryControllerBean getGlBgtAll");
        
        LocalGlBudgetHome glBudgetHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glBudgetHome = (LocalGlBudgetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetHome.JNDI_NAME, LocalGlBudgetHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glBudgets = glBudgetHome.findBgtAll(AD_CMPNY);

	        Iterator i = glBudgets.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalGlBudget glBudget = (LocalGlBudget)i.next();

	        	list.add(glBudget.getBgtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public GlModBudgetAmountDetails getGlBgaByBgaCode(Integer BGA_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException  {
                    
        Debug.print("GlBudgetEntryControllerBean getGlBgaByBgaCode");
        
        LocalGlBudgetAmountHome glBudgetAmountHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;
        
        // Initialize EJB Home
        
        try {
            
        	glBudgetAmountHome = (LocalGlBudgetAmountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetAmountHome.JNDI_NAME, LocalGlBudgetAmountHome.class);
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
        	genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalGlBudgetAmount glBudgetAmount = null;

        	try {
        		
        		glBudgetAmount = glBudgetAmountHome.findByPrimaryKey(BGA_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList glBgaBudgetAmountCoaList = new ArrayList();
        	
        	// get budget amount coa lines
        	
        	Collection glBudgetAmountCoas = glBudgetAmount.getGlBudgetAmountCoas();       	            
        	
        	Iterator i = glBudgetAmountCoas.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlBudgetAmountCoa glBudgetAmountCoa = (LocalGlBudgetAmountCoa)i.next();
        		
        		GlModBudgetAmountCoaDetails bcDetails = new GlModBudgetAmountCoaDetails();

		        // get all budget amount periods per coa
		        
		        ArrayList glBcBudgetAmountPeriodList = new ArrayList();
		        
		        Collection glBudgetAmountPeriods = glBudgetAmountCoa.getGlBudgetAmountPeriods();
		        
		        Iterator n = glBudgetAmountPeriods.iterator();
		        
		        while (n.hasNext()) {
	        		
	        		LocalGlBudgetAmountPeriod glBudgetAmountPeriod = (LocalGlBudgetAmountPeriod)n.next();
	        		
	        		GlModBudgetAmountPeriodDetails bapDetails = new GlModBudgetAmountPeriodDetails();
	        		bapDetails.setBapAmount(glBudgetAmountPeriod.getBapAmount());
	        		bapDetails.setBapAcvPeriodPrefix(glBudgetAmountPeriod.getGlAccountingCalendarValue().getAcvPeriodPrefix());
	        		
	        		GregorianCalendar gc = new GregorianCalendar();
            		gc.setTime(glBudgetAmountPeriod.getGlAccountingCalendarValue().getAcvDateTo());
            		
            		bapDetails.setBapAcvYear(gc.get(Calendar.YEAR));
            		
            		glBcBudgetAmountPeriodList.add(bapDetails);
            		
		        }
		        
        		bcDetails.setBcCoaAccountNumber(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
        		bcDetails.setBcCoaAccountDescription(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountDescription());
        		bcDetails.setBcDescription(glBudgetAmountCoa.getBcDescription());
        		bcDetails.setBcRuleType(glBudgetAmountCoa.getBcRuleType());
        		bcDetails.setBcRuleAmount(glBudgetAmountCoa.getBcRuleAmount());
        		bcDetails.setBcRulePercentage1(glBudgetAmountCoa.getBcRulePercentage1());
        		bcDetails.setBcRulePercentage2(glBudgetAmountCoa.getBcRulePercentage2());
		        bcDetails.setBcBudgetAmountPeriodList(glBcBudgetAmountPeriodList);
		        
		        glBgaBudgetAmountCoaList.add(bcDetails);
        		
        	}
        	
        	GlModBudgetAmountDetails mdetails = new GlModBudgetAmountDetails();
        	mdetails.setBgaCode(glBudgetAmount.getBgaCode());
        	mdetails.setBgaPassword(glBudgetAmount.getGlBudgetOrganization().getBoPassword());
        	mdetails.setBgaCreatedBy(glBudgetAmount.getBgaCreatedBy());
        	mdetails.setBgaDateCreated(glBudgetAmount.getBgaDateCreated());
        	mdetails.setBgaLastModifiedBy(glBudgetAmount.getBgaLastModifiedBy());
        	mdetails.setBgaDateLastModified(glBudgetAmount.getBgaDateLastModified());
        	mdetails.setBgaBoName(glBudgetAmount.getGlBudgetOrganization().getBoName());
        	mdetails.setBgaBgtName(glBudgetAmount.getGlBudget().getBgtName());
        	mdetails.setBgaBudgetAmountCoaList(glBgaBudgetAmountCoaList);

        	return mdetails;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;

        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /*
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     *
    public ArrayList getGlBgaTemplateByBoNameAndBgtName(String BO_NM, String BGT_NM, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException   {
                    
        Debug.print("GlBudgetEntryControllerBean getGlBgtTemplateByBoNameAndBgtName");
        
        LocalGlBudgetHome glBudgetHome = null;
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glBudgetHome = (LocalGlBudgetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetHome.JNDI_NAME, LocalGlBudgetHome.class);
        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);        
	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	    	genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
	    	genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        
            LocalGlBudget glBudget = glBudgetHome.findByBgtName(BGT_NM, AD_CMPNY);
            
            ArrayList periodList = new ArrayList();
            periodList.add(glBudget.getBgtFirstPeriod());   
            
            // get remaining periods up to last period defined         
                                                            
            Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(glBudget.getBgtFirstPeriod().substring(0, glBudget.getBgtFirstPeriod().indexOf('-')), 
                EJBCommon.getIntendedDate(Integer.parseInt(glBudget.getBgtFirstPeriod().substring(glBudget.getBgtFirstPeriod().indexOf('-') + 1))), AD_CMPNY);
            ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);            
            LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
                                    
            LocalGlAccountingCalendarValue glAccountingCalendarValue =
			     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
			     	glSetOfBook.getGlAccountingCalendar().getAcCode(),
			     	glBudget.getBgtFirstPeriod().substring(0, glBudget.getBgtFirstPeriod().indexOf('-')), AD_CMPNY);                  
            
            while (!glBudget.getBgtFirstPeriod().equals(glBudget.getBgtLastPeriod())) {
            
	            glAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
	                glSetOfBook.getGlAccountingCalendar().getAcCode(),
	                (short)(glAccountingCalendarValue.getAcvPeriodNumber() + 1), AD_CMPNY);
	                
	            GregorianCalendar gc = new GregorianCalendar();
                gc.setTime(glAccountingCalendarValue.getAcvDateTo());	                
	            periodList.add(glAccountingCalendarValue.getAcvPeriodPrefix() + "-" + gc.get(Calendar.YEAR));	                
                
                if (glBudget.getBgtLastPeriod().equals(glAccountingCalendarValue.getAcvPeriodPrefix() + "-" + gc.get(Calendar.YEAR))) {
                
                    break;
            		
            	}
				     	
	        }
        	
        	LocalGlBudgetOrganization glBudgetOrganization = glBudgetOrganizationHome.findByBoName(BO_NM, AD_CMPNY);
        	        	        	
        	Collection glBudgetAccountAssignments = glBudgetOrganization.getGlBudgetAccountAssignments();        	        	
        	                        	
        	Iterator baaIter = glBudgetAccountAssignments.iterator();
        	
        	while (baaIter.hasNext()) {
        		
        	  LocalGlBudgetAccountAssignment glBudgetAccountAssignment = (LocalGlBudgetAccountAssignment)baaIter.next();
        		        	          		
        	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);	  	  
		  	  LocalGenField genField = adCompany.getGenField();
		  	  Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
		      String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
		      int genNumberOfSegment = genField.getFlNumberOfSegment();
		      
		      // get coa selected
		      
		      StringBuffer jbossQl = new StringBuffer();
	          jbossQl.append("SELECT OBJECT(coa) FROM GlChartOfAccount coa WHERE ");
	          
	          StringTokenizer stAccountFrom = new StringTokenizer(glBudgetAccountAssignment.getBaaAccountFrom(), strSeparator);
	          StringTokenizer stAccountTo = new StringTokenizer(glBudgetAccountAssignment.getBaaAccountTo(), strSeparator);
		      
		      try {
	      	
		          String var = "1";
		          
		          if (genNumberOfSegment > 1) {
		          	
		          	for (int i=0; i<genNumberOfSegment; i++) {
		          		
		          		if (i == 0 && i < genNumberOfSegment - 1) {
		          			
		          			// for first segment
		          			
		          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");
		          			
		          			var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)+1 ";
		          			
		          			
		          		} else if (i != 0 && i < genNumberOfSegment - 1){     		
		          			
		          			// for middle segments
		          			
		          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")) - (" + var +")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");
		          			
		          			var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";
		          			
		          		} else if (i != 0 && i == genNumberOfSegment - 1) {
		          			
		          			// for last segment
		          			
		          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' ");
		          			
		          			
		          		}	     		       	      	   	            
		          		
		          	}
		          	
		          } else if(genNumberOfSegment == 1) {
		          	
		          	String accountFrom = stAccountFrom.nextToken();
		       		String accountTo = stAccountTo.nextToken();
		       		
		       		jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + accountFrom + "' AND '" + accountTo + "' OR " +
		       				"coa.coaAccountNumber BETWEEN '" + accountFrom + "' AND '" + accountTo + "' ");
		          	
		          }
		          
				  jbossQl.append("AND coa.coaEnable=1 AND coa.coaAdCompany=" + AD_CMPNY + " ");
				  			  
			 } catch (NumberFormatException ex) {
			 	
			 	throw new GlobalAccountNumberInvalidException();
			 	
			 }
			 
	         // generate order by coa natural account
	
		     short accountSegmentNumber = 0;
		      
		     try {
		      
		      	  LocalGenSegment genSegment = genSegmentHome.findByFlCodeAndVsName(genField.getFlCode(), glBudgetAccountAssignment.getGlBudgetOrganization().getBoSegmentOrder(), AD_CMPNY);
		      	  accountSegmentNumber = genSegment.getSgSegmentNumber();
		      	
		     } catch (Exception ex) {
		      
		         throw new EJBException(ex.getMessage());
		      
		     }
		            
		      	  
			 jbossQl.append("ORDER BY coa.coaSegment" + accountSegmentNumber + ", coa.coaAccountNumber ");
			 		 
			 System.out.println(jbossQl.toString());
			 
			 Object obj[] = new Object[0];
		 
	  	     Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), obj);
	  	     
	  	     Iterator glCoaIter = glChartOfAccounts.iterator();
	  	     
	  	     while(glCoaIter.hasNext()) {
	  	     	
	  	     	LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)glCoaIter.next();
	  	     	
	  	     	GlModBudgetAmountCoaDetails bcDetails = new GlModBudgetAmountCoaDetails();
	  	     	bcDetails.setBcCoaAccountNumber(glChartOfAccount.getCoaAccountNumber());
	  	     	bcDetails.setBcCoaAccountDescription(glChartOfAccount.getCoaAccountDescription());
	  	     	
	  	     	ArrayList glBcBudgetAmountPeriodList = new ArrayList();
	  	     	
	  	     	Iterator periodIter = periodList.iterator();	  	         
	  	     	
	  	     	while (periodIter.hasNext()) {
	  	     		
	  	     		String periodName = (String)periodIter.next();
	  	     		
	  	     		GlModBudgetAmountPeriodDetails periodDetails = new GlModBudgetAmountPeriodDetails();	  	             
	  	     		
	  	     		periodDetails.setBapAcvPeriodPrefix(periodName.substring(0, periodName.indexOf('-')));
	  	     		periodDetails.setBapAcvYear(Integer.parseInt(periodName.substring(periodName.indexOf('-') + 1)));
	  	     		
	  	     		glBcBudgetAmountPeriodList.add(periodDetails);
	  	     		
	  	     	}
	  	     	
	  	     	bcDetails.setBcBudgetAmountPeriodList(glBcBudgetAmountPeriodList);
	  	     	
	  	     	list.add(bcDetails);
	  	     	
	  	     }
 
           }
           
           if (list.isEmpty()) throw new GlobalNoRecordFoundException();

	       return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    } */
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlBgaTemplateByBoNameAndBgtName(String BO_NM, String BGT_NM, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException   {
                    
        Debug.print("GlBudgetEntryControllerBean getGlBgtTemplateByBoNameAndBgtName");
        
        LocalGlBudgetHome glBudgetHome = null;
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glBudgetHome = (LocalGlBudgetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetHome.JNDI_NAME, LocalGlBudgetHome.class);
        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);        
	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	    	genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
	    	genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {

        	LocalGlBudget glBudget = glBudgetHome.findByBgtName(BGT_NM, AD_CMPNY);
        	
        	ArrayList glBcBudgetAmountPeriodList = new ArrayList();
        	GlModBudgetAmountPeriodDetails periodDetails = new GlModBudgetAmountPeriodDetails();	  	             
        	String periodName = null;
        	
        	// get first period
        	
        	periodName = glBudget.getBgtFirstPeriod();
	     	periodDetails.setBapAcvPeriodPrefix(periodName.substring(0, periodName.indexOf('-')));
	     	periodDetails.setBapAcvYear(Integer.parseInt(periodName.substring(periodName.indexOf('-') + 1)));
	     	glBcBudgetAmountPeriodList.add(periodDetails);
        	
        	// get remaining periods up to last period defined         

        	Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(glBudget.getBgtFirstPeriod().substring(0, glBudget.getBgtFirstPeriod().indexOf('-')), 
        			EJBCommon.getIntendedDate(Integer.parseInt(glBudget.getBgtFirstPeriod().substring(glBudget.getBgtFirstPeriod().indexOf('-') + 1))), AD_CMPNY);
        	ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);            
        	LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);

        	LocalGlAccountingCalendarValue glAccountingCalendarValue =
        		glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
        				glSetOfBook.getGlAccountingCalendar().getAcCode(),
        				glBudget.getBgtFirstPeriod().substring(0, glBudget.getBgtFirstPeriod().indexOf('-')), AD_CMPNY);                  

        	while (!glBudget.getBgtFirstPeriod().equals(glBudget.getBgtLastPeriod())) {

        		glAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
        				glSetOfBook.getGlAccountingCalendar().getAcCode(),
        				(short)(glAccountingCalendarValue.getAcvPeriodNumber() + 1), AD_CMPNY);

        		GregorianCalendar gc = new GregorianCalendar();
        		gc.setTime(glAccountingCalendarValue.getAcvDateTo());	                
        		
        		periodName = glAccountingCalendarValue.getAcvPeriodPrefix() + "-" + gc.get(Calendar.YEAR);
        		periodDetails = new GlModBudgetAmountPeriodDetails();	  	             
            	periodDetails.setBapAcvPeriodPrefix(periodName.substring(0, periodName.indexOf('-')));
    	     	periodDetails.setBapAcvYear(Integer.parseInt(periodName.substring(periodName.indexOf('-') + 1)));
    	     	glBcBudgetAmountPeriodList.add(periodDetails);
            	
        		if (glBudget.getBgtLastPeriod().equals(glAccountingCalendarValue.getAcvPeriodPrefix() + "-" + gc.get(Calendar.YEAR))) {

        			break;

        		}

        	}       
        	
        	if (glBcBudgetAmountPeriodList.isEmpty()) throw new GlobalNoRecordFoundException();

        	return glBcBudgetAmountPeriodList;

        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveGlBgaEntry(GlModBudgetAmountDetails details, String BO_NM, String BGT_NM, String COA_ACCNT_NMBR, Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException,
			GlobalAccountNumberInvalidException,
			GlBGAPasswordInvalidException {
                    
        Debug.print("GlBudgetEntryControllerBean saveGlBgaEntry");
        
        LocalGlBudgetAmountHome glBudgetAmountHome = null;
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;
        LocalGlBudgetHome glBudgetHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;

        // Initialize EJB Home
        
        try {
            
        	glBudgetAmountHome = (LocalGlBudgetAmountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetAmountHome.JNDI_NAME, LocalGlBudgetAmountHome.class);
        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);
        	glBudgetHome = (LocalGlBudgetHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlBudgetHome.JNDI_NAME, LocalGlBudgetHome.class);
	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	    	genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
	    	genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalGlBudgetOrganization glBudgetOrganization = null;
        	
        	// validate if budget already exist

            try {
            	System.out.println("**********************************");
    			System.out.println("COA_ACCNT_NMBR: " + COA_ACCNT_NMBR);
    			System.out.println("**********************************");
    			System.out.println("**********************************");
    			System.out.println("details.getBgaCode(): " + details.getBgaCode());
    			System.out.println("**********************************");
            	LocalGlBudgetAmount glExistingBudgetAmount = glBudgetAmountHome.findByBoNameAndBgtNameAndCoaAccountNumber(BO_NM, BGT_NM, COA_ACCNT_NMBR, AD_CMPNY);
            	System.out.println("**********************************");
    			System.out.println("glExistingBudgetAmount.getBgaCode(): " + glExistingBudgetAmount.getBgaCode());
    			System.out.println("**********************************");
    		    if (details.getBgaCode() == null ||
    		        details.getBgaCode() != null && !glExistingBudgetAmount.getBgaCode().equals(details.getBgaCode())) {
    		    	
    		        throw new GlobalRecordAlreadyExistException();
    		        
    		    }

    		} catch (FinderException ex) {
    		    
    		}
    		
    		// validate if password is required
    		System.out.println("BO_NM: " + BO_NM);
    		System.out.println("AD_CMPNY: " + AD_CMPNY);
			glBudgetOrganization = glBudgetOrganizationHome.findByBoName(BO_NM, AD_CMPNY);
		
		    if (glBudgetOrganization.getBoPassword() != null && 
		    	!glBudgetOrganization.getBoPassword().equals(details.getBgaPassword())) {
		    	
		        throw new GlBGAPasswordInvalidException();
		        
		    }

        	LocalGlBudgetAmount glBudgetAmount = null;

        	// create budget amount
                                    
        	System.out.println("details.getBgaCreatedBy(): "+ details.getBgaCreatedBy());
            if (details.getBgaCode() == null) {    
   	    		
            	glBudgetAmount = glBudgetAmountHome.create(details.getBgaCreatedBy(), details.getBgaDateCreated(),
            			details.getBgaLastModifiedBy(), details.getBgaDateLastModified(), AD_CMPNY);       	    	

			} else {
			
			    glBudgetAmount = glBudgetAmountHome.findByPrimaryKey(details.getBgaCode());
				
				glBudgetAmount.setBgaLastModifiedBy(details.getBgaLastModifiedBy());
				glBudgetAmount.setBgaDateLastModified(details.getBgaDateLastModified());
				
			}

            glBudgetOrganization.addGlBudgetAmount(glBudgetAmount);
        	
            LocalGlBudget glBudget = glBudgetHome.findByBgtName(BGT_NM, AD_CMPNY);
            glBudget.addGlBudgetAmount(glBudgetAmount);
            
            //  remove all budget amount lines
       	   
 	  	    Collection glBudgetAmountCoas = glBudgetAmount.getGlBudgetAmountCoas();
 	  	  
 	  	    Iterator i = glBudgetAmountCoas.iterator();     	  
 	  	  
 	  	    while (i.hasNext()) {
 	  	  	
 	  	   	    LocalGlBudgetAmountCoa glBudgetAmountCoa = (LocalGlBudgetAmountCoa)i.next(); 	  	  	   
 	  	   	    
 	  	  	    i.remove();
 	  	  	    
 	  	  	    glBudgetAmountCoa.remove();
 	  	  	
 	  	    }
 	  	    
 	  	    //  add new budget amount lines
 	  	    
 	  	    Collection glBgaBudgetAmountCoaList = details.getBgaBudgetAmountCoaList();
 	      	  
      	    i = glBgaBudgetAmountCoaList.iterator();
      	  
      	    while (i.hasNext()) {
      	  	
      	    	GlModBudgetAmountCoaDetails coaDetails = (GlModBudgetAmountCoaDetails)i.next();      	  	  

      	    	this.addGlBcEntry(coaDetails, glBudgetAmount, glBudgetOrganization, AD_CMPNY);
				
      	    }

        	
        	return glBudgetAmount.getBgaCode();
        	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalAccountNumberInvalidException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlBGAPasswordInvalidException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("GlBudgetEntryControllerBean getGlFcPrecisionUnit");

        LocalAdCompanyHome adCompanyHome = null;

        // Initialize EJB Home
         
        try {
             
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
             
        } catch (NamingException ex) {
             
            throw new EJBException(ex.getMessage());
             
        }

        try {
        	
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             
          return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                      
        } catch (Exception ex) {
        	 
        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
          
        }

     }
    
    // private methods
    
    private void addGlBcEntry(GlModBudgetAmountCoaDetails coaDetails, LocalGlBudgetAmount glBudgetAmount, LocalGlBudgetOrganization glBudgetOrganization, Integer AD_CMPNY) 
        throws GlobalAccountNumberInvalidException {
			
		Debug.print("GlBudgetEntryControllerBean addLines");
		
		LocalGlBudgetAmountCoaHome glBudgetAmountCoaHome = null;
		LocalGlBudgetAmountPeriodHome glBudgetAmountPeriodHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null; 
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlSetOfBookHome glSetOfBookHome = null;
		
		LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;

                
        // Initialize EJB Home
        
        try {
            
        	glBudgetAmountCoaHome = (LocalGlBudgetAmountCoaHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetAmountCoaHome.JNDI_NAME, LocalGlBudgetAmountCoaHome.class);
        	glBudgetAmountPeriodHome = (LocalGlBudgetAmountPeriodHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetAmountPeriodHome.JNDI_NAME, LocalGlBudgetAmountPeriodHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
                
        try {
        	        	
        	// validate if coa exists
        	
        	LocalGlChartOfAccount glChartOfAccount = null;
        	
        	try {
        	
        		glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(coaDetails.getBcCoaAccountNumber(), AD_CMPNY);
        		
        		if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
        		    throw new GlobalAccountNumberInvalidException(coaDetails.getBcCoaAccountNumber());
        	
        		// validate if coa is included in budget organization
        		
        		Collection glBudgetAccountAssignments = glBudgetOrganization.getGlBudgetAccountAssignments();        	        	

        		Iterator baaIter = glBudgetAccountAssignments.iterator();

        		while (baaIter.hasNext()) {

        			LocalGlBudgetAccountAssignment glBudgetAccountAssignment = (LocalGlBudgetAccountAssignment)baaIter.next();

        			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);	  	  
        			LocalGenField genField = adCompany.getGenField();
        			Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
        			String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
        			int genNumberOfSegment = genField.getFlNumberOfSegment();

        			// get coa selected

        			StringBuffer jbossQl = new StringBuffer();
        			jbossQl.append("SELECT OBJECT(coa) FROM GlChartOfAccount coa WHERE ");

        			StringTokenizer stAccountFrom = new StringTokenizer(glBudgetAccountAssignment.getBaaAccountFrom(), strSeparator);
        			StringTokenizer stAccountTo = new StringTokenizer(glBudgetAccountAssignment.getBaaAccountTo(), strSeparator);
        			System.out.println("genNumberOfSegment: " + genNumberOfSegment);
        			try {

        				String var = "1";

        				if (genNumberOfSegment > 1) {

        					for (int i=0; i<genNumberOfSegment; i++) {

        						if (i == 0 && i < genNumberOfSegment - 1) {

        							// for first segment

        							jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");

        							var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)+1 ";
        							System.out.println("FIRST");

        						} else if (i != 0 && i < genNumberOfSegment - 1){     		

        							// for middle segments

        							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")) - (" + var +")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");

        							var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";
        							System.out.println("SECOND");
        						} else if (i != 0 && i == genNumberOfSegment - 1) {

        							// for last segment

        							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' ");
        							System.out.println("THIRD");

        						}	     		       	      	   	            

        					}

        				} else if(genNumberOfSegment == 1) {

        					String accountFrom = stAccountFrom.nextToken();
        					String accountTo = stAccountTo.nextToken();

        					jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + accountFrom + "' AND '" + accountTo + "' OR " +
        							"coa.coaAccountNumber BETWEEN '" + accountFrom + "' AND '" + accountTo + "' ");

        				}

        				jbossQl.append("AND coa.coaEnable=1 AND coa.coaAdCompany=" + AD_CMPNY + " ");

        			} catch (NumberFormatException ex) {

        				throw new GlobalAccountNumberInvalidException();

        			}

        			// generate order by coa natural account

        			short accountSegmentNumber = 0;

        			try {

        				LocalGenSegment genSegment = genSegmentHome.findByFlCodeAndVsName(genField.getFlCode(), glBudgetAccountAssignment.getGlBudgetOrganization().getBoSegmentOrder(), AD_CMPNY);
        				accountSegmentNumber = genSegment.getSgSegmentNumber();

        			} catch (Exception ex) {

        				throw new EJBException(ex.getMessage());

        			}


        			jbossQl.append("ORDER BY coa.coaSegment" + accountSegmentNumber + ", coa.coaAccountNumber ");

        			System.out.println(jbossQl.toString());

        			Object obj[] = new Object[0];

        			Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), obj);
        			System.out.println("glChartOfAccounts="+glChartOfAccounts);
        			System.out.println("size="+glChartOfAccounts.size());
        			
        			System.out.println("glChartOfAccount="+glChartOfAccount);
        			
        			if(!glChartOfAccounts.contains(glChartOfAccount)){
        				System.out.println("---------------->GLOBAL INVALID");
        				throw new GlobalAccountNumberInvalidException(coaDetails.getBcCoaAccountNumber());
            			
        			}
        				
        		}

        	} catch (FinderException ex) {
        		
        		throw new GlobalAccountNumberInvalidException(coaDetails.getBcCoaAccountNumber());
        		
        	}
        	        			    
		    // create budget amount coa and budget amount period
		    
		    LocalGlBudgetAmountCoa glBudgetAmountCoa = glBudgetAmountCoaHome.create(
		    		coaDetails.getBcRuleAmount(),coaDetails.getBcRulePercentage1(),coaDetails.getBcRulePercentage2(),coaDetails.getBcDescription(), coaDetails.getBcRuleType(), AD_CMPNY);
		    
		    glBudgetAmount.addGlBudgetAmountCoa(glBudgetAmountCoa);
		    glChartOfAccount.addGlBudgetAmountCoa(glBudgetAmountCoa);
		    
		    Collection glBudgetAmountPeriods = coaDetails.getBcBudgetAmountPeriodList();
		    
		    Iterator i = glBudgetAmountPeriods.iterator();
		    
		    while (i.hasNext()) {
		    	
		    	GlModBudgetAmountPeriodDetails bapDetails = (GlModBudgetAmountPeriodDetails)i.next();
		    	
		    	LocalGlBudgetAmountPeriod glBudgetAmountPeriod = glBudgetAmountPeriodHome.create(
		    			bapDetails.getBapAmount(), AD_CMPNY);
		    	
		    	glBudgetAmountCoa.addGlBudgetAmountPeriod(glBudgetAmountPeriod);
		    	
		    	Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(bapDetails.getBapAcvPeriodPrefix(), EJBCommon.getIntendedDate(bapDetails.getBapAcvYear()), AD_CMPNY);
		          ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);            
		          LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
		         
		        LocalGlAccountingCalendarValue glAccountingCalendarValue =
				     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
				     	glSetOfBook.getGlAccountingCalendar().getAcCode(),
				     	bapDetails.getBapAcvPeriodPrefix(), AD_CMPNY);
				     					     	
				glAccountingCalendarValue.addGlBudgetAmountPeriod(glBudgetAmountPeriod);
		    	
		    }
		    		    
		    
		} catch (GlobalAccountNumberInvalidException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
		    		            		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}
 
 	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlBudgetEntryControllerBean ejbCreate");
      
    }
   

}