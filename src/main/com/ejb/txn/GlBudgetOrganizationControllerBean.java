package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.genfld.LocalGenValueSet;
import com.ejb.genfld.LocalGenValueSetHome;
import com.ejb.gl.LocalGlBudgetOrganization;
import com.ejb.gl.LocalGlBudgetOrganizationHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlBudgetOrganizationDetails;

/**
 * @ejb:bean name="GlBudgetOrganizationControllerEJB"
 *           display-name="used for defining budget organization details"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlBudgetOrganizationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlBudgetOrganizationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlBudgetOrganizationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlBudgetOrganizationControllerBean extends AbstractSessionBean {
	

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGenVsAll(Integer AD_CMPNY) {
                    
        Debug.print("GlBudgetOrganizationControllerBean getGenVsAll");
        
        LocalGenValueSetHome genValueSetHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	genValueSetHome = (LocalGenValueSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetHome.JNDI_NAME, LocalGenValueSetHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection genValueSets = genValueSetHome.findVsAll(AD_CMPNY);

	        Iterator i = genValueSets.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalGenValueSet genValueSet = (LocalGenValueSet)i.next();
	        	
	        	list.add(genValueSet.getVsName());
	        	
	        }
	        
	        return list;
	        
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlBoAll(Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlBudgetOrganizationControllerBean getGlBoAll");
        
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glBudgetOrganizations = glBudgetOrganizationHome.findBoAll(AD_CMPNY);
            
            if (glBudgetOrganizations.isEmpty()) {
            	
            	throw new GlobalNoRecordFoundException();
            	
            }

	        Iterator i = glBudgetOrganizations.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalGlBudgetOrganization glBudgetOrganization = (LocalGlBudgetOrganization)i.next();
	        	
	        	GlBudgetOrganizationDetails details = new GlBudgetOrganizationDetails();
	        	details.setBoCode(glBudgetOrganization.getBoCode());
	        	details.setBoName(glBudgetOrganization.getBoName());
	        	details.setBoDescription(glBudgetOrganization.getBoDescription());
	        	details.setBoSegmentOrder(glBudgetOrganization.getBoSegmentOrder());
	        	details.setBoPassword(glBudgetOrganization.getBoPassword());

	        	list.add(details);
	        	
	        }
	        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;    
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGlBoEntry(GlBudgetOrganizationDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
         
        Debug.print("GlBudgetOrganizationControllerBean addGlBoEntry");
        
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;

        // Initialize EJB Home
        
        try {

        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);             
                             
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                                   
        try {

            LocalGlBudgetOrganization glBudgetOrganization = null;
        	
        	try {
        		
        		glBudgetOrganization = glBudgetOrganizationHome.findByBoName(details.getBoName(), AD_CMPNY);
        		
        		throw new GlobalRecordAlreadyExistException();
        		        		
        	} catch (FinderException ex) {
        		
        	}
        		
    		// create new budget organization
    		
        	glBudgetOrganization = glBudgetOrganizationHome.create(details.getBoName(), 
    				details.getBoDescription(), details.getBoSegmentOrder(), details.getBoPassword(), AD_CMPNY); 
        	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;

        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();        	
            throw new EJBException(ex.getMessage());
        	
        }        
                                    	                                        
    }
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateGlBoEntry(GlBudgetOrganizationDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {

         
        Debug.print("GlBudgetOrganizationControllerBean updateGlBoEntry");
        
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;

        // Initialize EJB Home
        
        try {

        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);             
                             
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	        	
	        try {
	        	
	        	LocalGlBudgetOrganization glExistingBudgetOrganization  = glBudgetOrganizationHome.findByBoName(details.getBoName(), AD_CMPNY);

	        	if(glExistingBudgetOrganization != null &&
	        		!glExistingBudgetOrganization.getBoCode().equals(details.getBoCode()))  {  
	        		
	        		throw new GlobalRecordAlreadyExistException();
	        		
	        	}

	        } catch (FinderException ex) {
	        		        	
	        }
	        
	        LocalGlBudgetOrganization glBudgetOrganization = glBudgetOrganizationHome.findByPrimaryKey(details.getBoCode());

        	// Find and Update Budget Organization
        	        	
        	glBudgetOrganization.setBoName(details.getBoName());
        	glBudgetOrganization.setBoDescription(details.getBoDescription());
        	glBudgetOrganization.setBoSegmentOrder(details.getBoSegmentOrder());
        	glBudgetOrganization.setBoPassword(details.getBoPassword());
        	
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	throw ex;
      	        	                            	
        } catch (Exception ex) {
            
        	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }           	
                                                	                                        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
     public void deleteGlBoEntry(Integer BO_CODE, Integer AD_CMPNY) 
         throws GlobalRecordAlreadyDeletedException,
               GlobalRecordAlreadyAssignedException {               

       Debug.print("GlBudgetOrganizationControllerBean deleteGlBoEntry");

       LocalGlBudgetOrganization glBudgetOrganization = null;
       LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;

       // Initialize EJB Home
         
       try {

        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);           
       
       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }                
       
       try {
       	
       		glBudgetOrganization = glBudgetOrganizationHome.findByPrimaryKey(BO_CODE);
          
        } catch (FinderException ex) {
       	
          throw new GlobalRecordAlreadyDeletedException();
          
       	}
        
        try {

       		if (!glBudgetOrganization.getGlBudgetAmounts().isEmpty() || 
       			!glBudgetOrganization.getGlBudgetAccountAssignments().isEmpty()) {
       		
       			throw new GlobalRecordAlreadyAssignedException();
       		}
          	
       		glBudgetOrganization.remove();
 	    
 	  } catch (GlobalRecordAlreadyAssignedException ex) {
    
 	      throw ex;
 	    
 	  } catch (Exception ex) {
 	 	
 	      getSessionContext().setRollbackOnly();	    
 	      throw new EJBException(ex.getMessage());
 	    
 	  }	      
       
    } 
    
 	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlBudgetOrganizationControllerBean ejbCreate");
      
    }
    
   // private methods

}