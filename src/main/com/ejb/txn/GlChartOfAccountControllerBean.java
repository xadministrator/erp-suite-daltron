package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApprovalCoaLineHome;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchCoa;
import com.ejb.ad.LocalAdBranchCoaHome;
import com.ejb.ad.LocalAdBranchCustomerHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchOverheadMemoLineHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdBranchStandardMemoLineHome;
import com.ejb.ad.LocalAdBranchSupplierHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.exception.GlCOAAccountNumberAlreadyAssignedException;
import com.ejb.exception.GlCOAAccountNumberAlreadyDeletedException;
import com.ejb.exception.GlCOAAccountNumberAlreadyExistException;
import com.ejb.exception.GlCOAAccountNumberHasParentValueException;
import com.ejb.exception.GlCOAAccountNumberIsInvalidException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyAssignedException;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenFieldHome;
import com.ejb.genfld.LocalGenQualifier;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSet;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlRecurringJournalHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvOverheadMemoLineHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdResponsibilityDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlChartOfAccountDetails;
import com.util.GlModChartOfAccountDetails;
import com.util.GlModFunctionalCurrencyDetails;

/**
 * @ejb:bean name="GlChartOfAccountControllerEJB"
 *           display-name="Used for setting up the organizations chart of account"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlChartOfAccountControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlChartOfAccountController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlChartOfAccountControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlChartOfAccountControllerBean extends AbstractSessionBean {

	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvCitCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("GlChartOfAccountControllerBean getAdLvAccountCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("GL COA CATEGORY - CIT", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	
	

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvSAWCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("GlChartOfAccountControllerBean getAdLvSAWCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("GL COA CATEGORY - SAW", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				System.out.println("adLookUpValue.getLvName()="+adLookUpValue.getLvName());
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
   
	
	
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvIiTCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("GlChartOfAccountControllerBean getAdLvIITCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("GL COA CATEGORY - IIT", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				System.out.println("adLookUpValue.getLvName()="+adLookUpValue.getLvName());
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
   

	  /**
	   * @ejb:interface-method view-type="remote"
	   * @jboss:method-attributes read-only="true"
	   **/
	   public com.util.GlModChartOfAccountDetails getGlCoaDescAndAccountTypeByCoaAccountNumber(
	   	  String COA_ACCNT_NMBR, Integer AD_CMPNY) 
	      throws GlCOAAccountNumberIsInvalidException, GlCOAAccountNumberHasParentValueException {
	
	      Debug.print("GlChartOfAccountControllerBean getGlCoaDescAndAccountTypeByCoaAccountNumber");
	
	      LocalAdCompany adCompany = null;
	      LocalGenField genField = null;
	      
	      Collection genSegments = null;
	      char chrSeparator;
	      String strSeparator = new String();
	      
	      LocalAdCompanyHome adCompanyHome = null;
	      LocalGenFieldHome genFieldHome = null;
	      LocalGlChartOfAccountHome glChartOfAccountHome = null;
	      LocalGenSegmentHome genSegmentHome = null;
	      
	      // Initialize EJB Home
	        
	      try {
	            
	          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	          genFieldHome = (LocalGenFieldHome)EJBHomeFactory.
	              lookUpLocalHome(LocalGenFieldHome.JNDI_NAME, LocalGenFieldHome.class);
	          genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
	              lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
	            
	      } catch (NamingException ex) {
	            
	          throw new EJBException(ex.getMessage());
	            
	      }
	
	      	
	      try {
	         adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	      } catch (FinderException ex) {
	         throw new EJBException(ex.getMessage());
	      } 
	     
	      	      
	      try {
	      	
	      	  genField = adCompany.getGenField();
	          genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
	          chrSeparator = genField.getFlSegmentSeparator();
	          strSeparator = String.valueOf(chrSeparator);
	          
	      } catch (Exception ex) {
	      	  
	      	  throw new EJBException(ex.getMessage());
	      	
	      }	      
	
	      	      	
	      ArrayList genValueSetValueList = null;
	
	      GlModChartOfAccountDetails modCoaDetails = null;
		  String MCOA_DESC = "";
		  String MCOA_ACCNT_TYP = null;
		
		  try {
		  	
	         genValueSetValueList = getGenValueSetValueWithValue(genField, COA_ACCNT_NMBR, AD_CMPNY);
	        
	         if (genValueSetValueList.size() != genField.getFlNumberOfSegment()) {
	        	
	        	 throw new GlCOAAccountNumberIsInvalidException();
	        	
	         }
	        
		  } catch (GlCOAAccountNumberIsInvalidException ex) {
		  	
		     throw ex;
		    
		  } catch (GlCOAAccountNumberHasParentValueException ex) {
		  	
		     throw ex;
		    
		  } 
	

		  Iterator j = genValueSetValueList.iterator();
		  Iterator k = genSegments.iterator();
	
		  while (j.hasNext()) {
		 	
			    LocalGenValueSetValue genValueSetValue = (LocalGenValueSetValue) j.next();
			    LocalGenSegment genSegment = (LocalGenSegment) k.next();
			    
			    if (genSegment.getSgSegmentType() == 'N') {
			    	
			       LocalGenQualifier genQualifier = genValueSetValue.getGenQualifier();
		               MCOA_ACCNT_TYP = genQualifier.getQlAccountType();
		               
			    }
			    
		        MCOA_DESC = MCOA_DESC + genValueSetValue.getVsvDescription();
		
		        if (j.hasNext()) {
		        	
		           MCOA_DESC = MCOA_DESC + strSeparator;
		           
		        }
		  }
				         
		  modCoaDetails = new GlModChartOfAccountDetails(MCOA_DESC, MCOA_ACCNT_TYP);
	         

	      return modCoaDetails;
	     
	   }
	   
	   
	   /**
	   * @ejb:interface-method view-type="remote"
	   * @jboss:method-attributes read-only="true"
	   **/
	   public com.util.GlModChartOfAccountDetails getGlCoaByCoaCode(Integer COA_CODE, Integer AD_CMPNY) {
	
	      Debug.print("GlChartOfAccountControllerBean getGlCoaByCoaCode");

	      LocalGlChartOfAccountHome glChartOfAccountHome = null;

	      // Initialize EJB Home
	        
	      try {
	            
	          glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	              lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	            
	      } catch (NamingException ex) {
	            
	          throw new EJBException(ex.getMessage());
	            
	      }
	      
	      try {
	      	
	      	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(COA_CODE);

	      	GlModChartOfAccountDetails modCoaDetails = new GlModChartOfAccountDetails(glChartOfAccount.getCoaCode(),
	      			glChartOfAccount.getCoaAccountNumber(),
	      			glChartOfAccount.getCoaCitCategory(),
	      			glChartOfAccount.getCoaSawCategory(),
	      			glChartOfAccount.getCoaIitCategory(),
					glChartOfAccount.getCoaDateFrom(),
					glChartOfAccount.getCoaDateTo(),
					glChartOfAccount.getCoaEnable(),
					glChartOfAccount.getCoaAccountDescription(), 
					glChartOfAccount.getCoaAccountType(),
					glChartOfAccount.getCoaTaxType(),
					glChartOfAccount.getGlFunctionalCurrency() == null ?
							null : glChartOfAccount.getGlFunctionalCurrency().getFcName(),
					glChartOfAccount.getCoaForRevaluation());

	      	return modCoaDetails;
	      	
	      } catch (Exception ex) {
	      	
	      	throw new EJBException(ex.getMessage());
	      	
	      }
	      
	      
	   }
	
	   /**
	    * @ejb:interface-method view-type="remote"
	    **/
	   public void addGlCoaEntry(GlChartOfAccountDetails details, ArrayList branchList, String GL_FUNCTIONAL_CURRENCY,
	   Integer AD_CMPNY) 
	   throws GlCOAAccountNumberIsInvalidException,GlCOAAccountNumberAlreadyExistException, 
	   GlCOAAccountNumberHasParentValueException, GlFCNoFunctionalCurrencyFoundException,
	   GlFCFunctionalCurrencyAlreadyAssignedException {
	   	
	   	Debug.print("GlChartOfAccountControllerBean addGlCoaEntry");
	   	
	   	LocalGenField genField = null;
	   	ArrayList genValueSetValueList = new ArrayList();
	   	LocalAdBranchCoa adBranchCoa = null;
	   	LocalAdBranch adBranch = null;
	   	
	   	LocalGlSetOfBookHome glSetOfBookHome = null;
	   	LocalGenFieldHome genFieldHome = null;
	   	LocalGlChartOfAccountHome glChartOfAccountHome = null;
	   	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
	   	LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
	   	LocalAdCompanyHome adCompanyHome = null;
	   	LocalAdBranchCoaHome adBranchCoaHome = null;
	   	LocalAdBranchHome adBranchHome = null;
	   	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
	   	
	   	// Initialize EJB Home
	   	
	   	try {
	   		
	   		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
	   		genFieldHome = (LocalGenFieldHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenFieldHome.JNDI_NAME, LocalGenFieldHome.class);
	   		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	   		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
	   		glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
	   		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	   		adBranchCoaHome = (LocalAdBranchCoaHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchCoaHome.JNDI_NAME, LocalAdBranchCoaHome.class);
	   		adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
	   		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
	   		
	   	} catch (NamingException ex) {
	   		
	   		throw new EJBException(ex.getMessage());
	   		
	   	}	
	   	
	   	// check if account number is valid
	   	
	   	LocalAdCompany adCompany = null;
	   	
	   	try {
	   		
	   		adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	   		
	   		genField = adCompany.getGenField();
	   		
	   		genValueSetValueList = getGenValueSetValueWithValue(
	   				genField, details.getCoaAccountNumber(), AD_CMPNY);
	   		
	   	} catch (GlCOAAccountNumberHasParentValueException ex) {
	   		
	   		throw ex;
	   		
	   	} catch (Exception ex) {
	   		
	   		throw new GlCOAAccountNumberIsInvalidException();
	   		
	   	}
	   	
	   	if (genValueSetValueList.size() != genField.getFlNumberOfSegment())
	   		throw new GlCOAAccountNumberIsInvalidException();
	   	
	   	try {
	   		
	   		glChartOfAccountHome.findByCoaAccountNumber(details.getCoaAccountNumber(), AD_CMPNY);
	   		
	   		getSessionContext().setRollbackOnly();
	   		throw new GlCOAAccountNumberAlreadyExistException();
	   		
	   	} catch (FinderException ex) {
	   		
	   		
	   	}
	   	
	   	// create coa
	   	
	   	LocalGlChartOfAccount glChartOfAccount = null;
	   	
	   	try {
	   		
	   		glChartOfAccount = glChartOfAccountHome.create(
	   				details.getCoaAccountNumber(), 
					details.getCoaAccountDescription(), details.getCoaAccountType(),
					details.getCoaTaxType(),
					details.getCoaCitCategory(), details.getCoaSawCategory(), details.getCoaIitCategory(),
					details.getCoaDateFrom(), details.getCoaDateTo(),
					null, null, null, null, null,
					null, null, null, null, null,
					details.getCoaEnable(), details.getCoaForRevaluation(), AD_CMPNY);
	   		
	   		StringTokenizer st = new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), new Character(genField.getFlSegmentSeparator()).toString());
	   		
	   		int i = 0;
	   		
	   		while (st.hasMoreTokens()) {
	   			i++;
	   			String value = st.nextToken();	        	
	   			
	   			switch(i) {
	   			
	   			case 1: 
	   				glChartOfAccount.setCoaSegment1(value);
	   				break;
	   			case 2: 
	   				glChartOfAccount.setCoaSegment2(value);
	   				break;
	   			case 3: 
	   				glChartOfAccount.setCoaSegment3(value);
	   				break;
	   			case 4: 
	   				glChartOfAccount.setCoaSegment4(value);
	   				break;
	   			case 5: 
	   				glChartOfAccount.setCoaSegment5(value);
	   				break;
	   			case 6: 
	   				glChartOfAccount.setCoaSegment6(value);
	   				break;
	   			case 7: 
	   				glChartOfAccount.setCoaSegment7(value);
	   				break;
	   			case 8: 
	   				glChartOfAccount.setCoaSegment8(value);
	   				break;
	   			case 9: 
	   				glChartOfAccount.setCoaSegment9(value);
	   				break;
	   			case 10: 
	   				glChartOfAccount.setCoaSegment10(value);
	   				break;
	   			default:
	   				break;
	   			
	   			}
	   		}
	   		
	   		// mapping to functional currency
	   		
	   		if ((GL_FUNCTIONAL_CURRENCY.length() < 0) && (GL_FUNCTIONAL_CURRENCY == null) &&
	   				(GL_FUNCTIONAL_CURRENCY.equalsIgnoreCase("NO RECORD FOUND"))){

	   			LocalGlFunctionalCurrency glFunctionalCurrency = null;
	   			
	   			try{

	   				glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(GL_FUNCTIONAL_CURRENCY, AD_CMPNY);
	   				
	   				// check if FC selected is default company FC
	   				if (adCompany.getGlFunctionalCurrency().getFcCode() == glFunctionalCurrency.getFcCode())
	   					throw new GlFCFunctionalCurrencyAlreadyAssignedException();
	   				
	   				glFunctionalCurrency.addGlChartOfAccount(glChartOfAccount);

	   			}catch(FinderException ex){
	   				
	   				throw new GlFCNoFunctionalCurrencyFoundException();
	   				
	   			}
	   			
	   		}
	   		
	   	}catch(GlFCNoFunctionalCurrencyFoundException ex){
   			
	   		getSessionContext().setRollbackOnly();
	   		throw ex;

	   	}catch(GlFCFunctionalCurrencyAlreadyAssignedException ex){
   			
	   		getSessionContext().setRollbackOnly();
	   		throw ex;

	   	} catch (Exception ex) {
	   		
	   		getSessionContext().setRollbackOnly();
	   		throw new EJBException(ex.getMessage());
	   		
	   	}
	   	
	   	
	   	try {
	   		
	   		// create balances to all existing set of books
	   		
	   		Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
	   		
	   		Iterator i = glSetOfBooks.iterator();
	   		
	   		while (i.hasNext()) {
	   			
	   			LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();
	   			
	   			Collection glAccountingCalendarValues = 
	   				glSetOfBook.getGlAccountingCalendar().getGlAccountingCalendarValues();
	   			
	   			Iterator iterAcv = glAccountingCalendarValues.iterator();
	   			
	   			while (iterAcv.hasNext()) {
	   				
	   				LocalGlAccountingCalendarValue glAccountingCalendarValue =
	   					(LocalGlAccountingCalendarValue) iterAcv.next();
	   				
	   				LocalGlChartOfAccountBalance glChartOfAccountBalance = null;
	   				
	   				try {
	   					
	   					glChartOfAccountBalance = glChartOfAccountBalanceHome.create(0d,0d,0d,0d, AD_CMPNY);
	   					glChartOfAccountBalance.setGlAccountingCalendarValue(glAccountingCalendarValue);
	   					glChartOfAccountBalance.setGlChartOfAccount(glChartOfAccount);
	   					
	   				} catch (Exception ex) {
	   					
	   					getSessionContext().setRollbackOnly();
	   					throw new EJBException(ex.getMessage());
	   					
	   				}
	   			}
	   			
	   		}
	   		
	   	} catch (Exception ex) {
	   		
	   		getSessionContext().setRollbackOnly();
	   		throw new EJBException(ex.getMessage());
	   		
	   	}
	   	
	   	// add branch coa
	   	
	   	try {
	   		
	   		Iterator i = branchList.iterator();
	   		
	   		while(i.hasNext()) {	          	          
	   			
	   			AdBranchDetails brDetails = (AdBranchDetails)i.next();
	   			adBranchCoa = adBranchCoaHome.create(AD_CMPNY);
	   			
	   			glChartOfAccount.addAdBranchCoa(adBranchCoa);
	   			
	   			adBranch = adBranchHome.findByPrimaryKey(brDetails.getBrCode());
	   			adBranch.addAdBranchCoa(adBranchCoa);
	   			
	   		}
	   		
	   	} catch (Exception ex) {
	   		
	   		getSessionContext().setRollbackOnly();
	   		throw new EJBException(ex.getMessage());
	   		
	   	}    
	   	
	   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlCoaEntry(GlChartOfAccountDetails details, String RS_NM, ArrayList branchList,
   	  String GL_FUNCTIONAL_CURRENCY, Integer AD_CMPNY) 
      throws GlCOAAccountNumberAlreadyAssignedException,
      GlCOAAccountNumberHasParentValueException, 
      GlCOAAccountNumberIsInvalidException, 
      GlCOAAccountNumberAlreadyExistException,
      GlCOAAccountNumberAlreadyDeletedException,
      GlFCNoFunctionalCurrencyFoundException,
      GlFCFunctionalCurrencyAlreadyAssignedException{

      Debug.print("GlChartOfAccountControllerBean updateGlCoaEntry");
      
      LocalAdCompanyHome adCompanyHome = null;
      LocalGenFieldHome genFieldHome = null;
      LocalGlChartOfAccountHome glChartOfAccountHome = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      LocalAdBranchCoaHome adBranchCoaHome = null;
      LocalAdBranchHome adBranchHome = null;
      LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome =  null;
	  
      // Initialize EJB Home
        
      try {
            
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          genFieldHome = (LocalGenFieldHome)EJBHomeFactory.
              lookUpLocalHome(LocalGenFieldHome.JNDI_NAME, LocalGenFieldHome.class);
          glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
          glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
          adBranchCoaHome = (LocalAdBranchCoaHome)EJBHomeFactory.
          	  lookUpLocalHome(LocalAdBranchCoaHome.JNDI_NAME, LocalAdBranchCoaHome.class);
          adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
      	  	  lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
          glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
		  	  lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      LocalGlChartOfAccount glChartOfAccount = null;
      
      LocalAdBranchCoa adBranchCoa = null;
      LocalAdBranch adBranch = null;

      try {
      	
         glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
            details.getCoaCode());
            
      } catch (FinderException ex) {
      	
         throw new GlCOAAccountNumberAlreadyDeletedException();
         
      }

      if (hasRelation(glChartOfAccount, AD_CMPNY)) {
      
         updateGlCoaEntryWithRelation(glChartOfAccount, details, GL_FUNCTIONAL_CURRENCY, AD_CMPNY);
         
      } else {

         LocalAdCompany adCompany = null;
         LocalGenField genField = null;
	 	 LocalGlChartOfAccount glChartOfAccount2 = null;
         ArrayList genValueSetValueList = new ArrayList();

		 try {
		 	
	        adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		    	genField = adCompany.getGenField();
		    	
	     } catch (FinderException ex) {
	     	
	         throw new EJBException(ex.getMessage());
	         
	     }
	    
	     try {
	     	
	        genValueSetValueList = getGenValueSetValueWithValue(
	        genField, details.getCoaAccountNumber(), AD_CMPNY);
	        
	     } catch (GlCOAAccountNumberHasParentValueException ex) {
	         	
		    throw ex;
		    
		 } catch (Exception ex) {
		 	
		    throw new GlCOAAccountNumberIsInvalidException(); 
		    
		 }

         if (genValueSetValueList.size() != genField.getFlNumberOfSegment())
	        throw new GlCOAAccountNumberIsInvalidException();


		 try {
		 	
		    glChartOfAccount2 = 
		       glChartOfAccountHome.findByCoaAccountNumber(details.getCoaAccountNumber(), AD_CMPNY);
		       
	     } catch (FinderException ex) {
	     	
		    glChartOfAccount.setCoaAccountNumber(details.getCoaAccountNumber());
		    glChartOfAccount.setCoaCitCategory(details.getCoaCitCategory()); 
		    glChartOfAccount.setCoaSawCategory(details.getCoaSawCategory()); 
		    glChartOfAccount.setCoaIitCategory(details.getCoaIitCategory()); 
		    glChartOfAccount.setCoaAccountDescription(details.getCoaAccountDescription());
		    glChartOfAccount.setCoaAccountType(details.getCoaAccountType());
		    glChartOfAccount.setCoaTaxType(details.getCoaTaxType());
            glChartOfAccount.setCoaDateFrom(details.getCoaDateFrom());
            glChartOfAccount.setCoaDateTo(details.getCoaDateTo());
            glChartOfAccount.setCoaEnable(details.getCoaEnable());
            glChartOfAccount.setCoaForRevaluation(details.getCoaForRevaluation());
	            
            StringTokenizer st = new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), new Character(genField.getFlSegmentSeparator()).toString());
	        
	        int i = 0;
	        
	        while (st.hasMoreTokens()) {
	        	i++;
	        	String value = st.nextToken();	        	
	        	
	            switch(i) {
	            	
	            	case 1: 
	            		glChartOfAccount.setCoaSegment1(value);
	            		break;
	            	case 2: 
	            		glChartOfAccount.setCoaSegment2(value);
	            		break;
	            	case 3: 
	            		glChartOfAccount.setCoaSegment3(value);
	            		break;
	            	case 4: 
	            		glChartOfAccount.setCoaSegment4(value);
	            		break;
	            	case 5: 
	            		glChartOfAccount.setCoaSegment5(value);
	            		break;
	            	case 6: 
	            		glChartOfAccount.setCoaSegment6(value);
	            		break;
	            	case 7: 
	            		glChartOfAccount.setCoaSegment7(value);
	            		break;
	            	case 8: 
	            		glChartOfAccount.setCoaSegment8(value);
	            		break;
	            	case 9: 
	            		glChartOfAccount.setCoaSegment9(value);
	            		break;
	            	case 10: 
	            		glChartOfAccount.setCoaSegment10(value);
	            		break;
	            	default:
	            		break;
	            	
	            }
	        }

	        // mapping to functional currency

	        try {
	        	
	        	if (GL_FUNCTIONAL_CURRENCY.length() > 0 && GL_FUNCTIONAL_CURRENCY != null && (!GL_FUNCTIONAL_CURRENCY.equalsIgnoreCase("NO RECORD FOUND"))) {
	        		
	        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(GL_FUNCTIONAL_CURRENCY, AD_CMPNY);
	        		
			   		// check if FC selected is default company FC
			   		if (adCompany.getGlFunctionalCurrency().getFcCode() == glFunctionalCurrency.getFcCode())
			   			throw new GlFCFunctionalCurrencyAlreadyAssignedException();

	        		glFunctionalCurrency.addGlChartOfAccount(glChartOfAccount);
	        		
	        	}

	        } catch (FinderException e) {
	        		
	        	getSessionContext().setRollbackOnly();
	        	throw new GlFCNoFunctionalCurrencyFoundException();
	        	
	        } catch (GlFCFunctionalCurrencyAlreadyAssignedException e) {
        		
	        	getSessionContext().setRollbackOnly();
	        	throw e;
	        	
	        }

		 }

		 if (glChartOfAccount2 != null &&
		    !glChartOfAccount.getCoaCode().equals(glChartOfAccount2.getCoaCode())) {
		    	
	        getSessionContext().setRollbackOnly();
		    throw new GlCOAAccountNumberAlreadyExistException();
		    
		 } else {
		 
		    if (glChartOfAccount2 != null &&
		    		glChartOfAccount.getCoaCode().equals(glChartOfAccount2.getCoaCode())) {
		    	//glChartOfAccount.setCoaAccountCategory(details.getCoaAccountCategory());
		    	glChartOfAccount.setCoaCitCategory(details.getCoaCitCategory());
		    	glChartOfAccount.setCoaSawCategory(details.getCoaSawCategory()); 
		    	glChartOfAccount.setCoaIitCategory(details.getCoaIitCategory());
		    	glChartOfAccount.setCoaAccountDescription(details.getCoaAccountDescription());
		    	glChartOfAccount.setCoaAccountType(details.getCoaAccountType());
		    	glChartOfAccount.setCoaTaxType(details.getCoaTaxType());
		    	glChartOfAccount.setCoaDateFrom(details.getCoaDateFrom());
		    	glChartOfAccount.setCoaDateTo(details.getCoaDateTo());
		    	glChartOfAccount.setCoaEnable(details.getCoaEnable());
		    	 glChartOfAccount.setCoaForRevaluation(details.getCoaForRevaluation());
		    	
		    	// remove mapping to previous functional currency
		    	
		    	LocalGlFunctionalCurrency glFunctionalCurrency = glChartOfAccount.getGlFunctionalCurrency() == null ? null : glChartOfAccount.getGlFunctionalCurrency();

		    	//checks if there is no previous FC mapping
		    	if(glFunctionalCurrency != null)
		    		glFunctionalCurrency.dropGlChartOfAccount(glChartOfAccount);
				
		    	// mapping to new functional currency
		    	
		    	if (GL_FUNCTIONAL_CURRENCY.length() > 0 && GL_FUNCTIONAL_CURRENCY != null && (!GL_FUNCTIONAL_CURRENCY.equalsIgnoreCase("NO RECORD FOUND"))) {
		    		
		    		try {
		    			
		    			glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(GL_FUNCTIONAL_CURRENCY, AD_CMPNY);
		    			
				   		// check if FC selected is default company FC
				   		if (adCompany.getGlFunctionalCurrency().getFcCode() == glFunctionalCurrency.getFcCode())
				   			throw new GlFCFunctionalCurrencyAlreadyAssignedException();

		    			glFunctionalCurrency.addGlChartOfAccount(glChartOfAccount);
		    			
		    		} catch (FinderException e) {
		    			
		    			getSessionContext().setRollbackOnly();
		    			throw new GlFCNoFunctionalCurrencyFoundException();
		    			
		    		} catch (GlFCFunctionalCurrencyAlreadyAssignedException e) {
		    			
		    			getSessionContext().setRollbackOnly();
		    			throw e;
		    			
		    		}

		    	}
		    	
		    }
		    
		 }
		 
      }

      // update branch coa
      
      try {
          
          Collection adBranchCoas = adBranchCoaHome.findBcoaByCoaCodeAndRsName(glChartOfAccount.getCoaCode(), RS_NM, AD_CMPNY);
          
          Iterator i = adBranchCoas.iterator();
          
          while(i.hasNext()) {
              
              adBranchCoa = (LocalAdBranchCoa) i.next();
              
              // check if existing mapping is included in the list
              
              boolean brIncluded = false;
              
              Iterator brListIter = branchList.iterator();
              
              while(brListIter.hasNext()) {
                  
                  AdBranchDetails brDetails = (AdBranchDetails)brListIter.next();
                  
                  adBranch = adBranchHome.findByPrimaryKey(brDetails.getBrCode());
                  if(adBranchCoa.getAdBranch().getBrCode() == adBranch.getBrCode()){
                  		brIncluded = true;
                  		break;
                  }
                  	
              }
              
              if(!brIncluded){
              	if(hasRelation(adBranchCoa, AD_CMPNY))
              		throw new GlCOAAccountNumberAlreadyAssignedException("Branch mapping cannot be deleted for "+ adBranchCoa.getAdBranch().getBrName());
              }
              
              // remove ad branch coa line 
              // if it is included in the list or
              // if it is not included & has no existing unposted transaction
             
              glChartOfAccount.dropAdBranchCoa(adBranchCoa);
              adBranch = adBranchHome.findByPrimaryKey(adBranchCoa.getAdBranch().getBrCode());
              adBranch.dropAdBranchCoa(adBranchCoa);
              adBranchCoa.remove();
              
          }
          
          // add adBranch coa lines
          
          Iterator brListIter = branchList.iterator();
          
          while(brListIter.hasNext()) {
              
              AdBranchDetails brDetails = (AdBranchDetails)brListIter.next();
              adBranchCoa = adBranchCoaHome.create(AD_CMPNY);
              
              glChartOfAccount.addAdBranchCoa(adBranchCoa);
              
              adBranch = adBranchHome.findByPrimaryKey(brDetails.getBrCode());
              adBranch.addAdBranchCoa(adBranchCoa);
              
          }
          
      } catch (GlCOAAccountNumberAlreadyAssignedException e) {
      	
      	getSessionContext().setRollbackOnly();
      	throw e;
      	
      }catch (Exception ex) {
      	
      	getSessionContext().setRollbackOnly();
      	throw new EJBException(ex.getMessage());
      	
      }    
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteGlCoaEntry(Integer COA_CODE, Integer AD_CMPNY) 
      throws GlCOAAccountNumberAlreadyAssignedException,
      GlCOAAccountNumberAlreadyDeletedException {

      Debug.print("GlChartOfAccountControllerBean deleteGlCoaEntry");

      LocalGlChartOfAccount glChartOfAccount;

/**********************************************************
    Check if the the COA has any relation, if it has one or
	more relation, the COA should not be deleted
**********************************************************/
      
      LocalGlChartOfAccountHome glChartOfAccountHome = null;
      
      // Initialize EJB Home
        
      try {
            
          glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
                   
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
      	
         glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(COA_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlCOAAccountNumberAlreadyDeletedException();
         
      }

      if (hasRelation(glChartOfAccount, AD_CMPNY)) {
      	
         throw new GlCOAAccountNumberAlreadyAssignedException();
         
      } else {
      	
	     try {
	     	
	        glChartOfAccount.remove();
	        
		 } catch (Exception ex) {
		 	
		    getSessionContext().setRollbackOnly();
		    throw new EJBException(ex.getMessage());
		    
		 }
      }	
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/   
   public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
   	   throws GlobalNoRecordFoundException{
       
       Debug.print("GlChartOfAccountControllerBean getAdBrResAll");
       
       LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
       LocalAdBranchHome adBranchHome = null;
       
       LocalAdBranchResponsibility adBranchResponsibility = null;
       LocalAdBranch adBranch = null;
       
       Collection adBranchResponsibilities = null;
       
       ArrayList list = new ArrayList();
       
       // Initialize EJB Home
       
       try {
           
           adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
           	lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
           adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
           
       } catch (NamingException ex) {
           
           throw new EJBException(ex.getMessage());
           
       }
       
       try {
           
           adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
           
       } catch (FinderException ex) {
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       if (adBranchResponsibilities.isEmpty()) {
           
           throw new GlobalNoRecordFoundException();
           
       }
       
       try {
           
           Iterator i = adBranchResponsibilities.iterator();
           
           while(i.hasNext()) {
               
               adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
               
               adBranch = adBranchResponsibility.getAdBranch();
               
               AdBranchDetails details = new AdBranchDetails();
               
               details.setBrCode(adBranch.getBrCode());
               details.setBrName(adBranch.getBrName());
               
               list.add(details);
               
           }	               
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       return list;
       
   }
   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/   
   public ArrayList getAdBrCoaAll(Integer COA_CODE, Integer AD_CMPNY) 
   	   throws GlobalNoRecordFoundException{
       
       Debug.print("GlChartOfAccountControllerBean getAdBrCoaAll");
       
       LocalAdBranchCoaHome adBranchCoaHome  = null;
       LocalAdBranchHome adBranchHome = null;
       
       LocalAdBranchCoa adBranchCoa = null;
       LocalAdBranch adBranch = null;
       
       Collection adBranchCoas = null;
       
       ArrayList list = new ArrayList();
       
       // Initialize EJB Home
       
       try {
           
           adBranchCoaHome = (LocalAdBranchCoaHome)EJBHomeFactory.
           		lookUpLocalHome(LocalAdBranchCoaHome.JNDI_NAME, LocalAdBranchCoaHome.class);
           adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
           		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
           
       } catch (NamingException ex) {
           
           throw new EJBException(ex.getMessage());
           
       }
       
       try {
           
           adBranchCoas = adBranchCoaHome.findBcoaByCoaCode(COA_CODE, AD_CMPNY);
           
       } catch (FinderException ex) {
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       if (adBranchCoas.isEmpty()) {
           
           throw new GlobalNoRecordFoundException();
           
       }
       
       try {
           
           Iterator i = adBranchCoas.iterator();
           
           while(i.hasNext()) {
               
               adBranchCoa = (LocalAdBranchCoa)i.next();
               
               adBranch = adBranchHome.findByPrimaryKey(adBranchCoa.getAdBranch().getBrCode());                              
               
               AdBranchDetails details = new AdBranchDetails();
               
               details.setBrCode(adBranch.getBrCode());                                          
               
               list.add(details);
               
           }
           
       } catch (FinderException ex) {
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       return list;
       
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public AdResponsibilityDetails getAdRsByRsCode(Integer RS_CODE) 
   	   throws GlobalNoRecordFoundException {
       
       Debug.print("GlChartOfAccountControllerBean getAdRsByRsCode");
       
       LocalAdResponsibilityHome adResHome = null;
       LocalAdResponsibility adRes = null;
       
       try {
           adResHome = (LocalAdResponsibilityHome)EJBHomeFactory.
   				lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
       } catch (NamingException ex) {
           
       }
       
       try {
           adRes = adResHome.findByPrimaryKey(RS_CODE);    		
       } catch (FinderException ex) {
           
       }
       
       AdResponsibilityDetails details = new AdResponsibilityDetails();
       details.setRsName(adRes.getRsName());
       
       return details;
   }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {
                   
       Debug.print("GlChartOfAccountControllerBean getGlFcAllWithDefault");
       
       LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       
       Collection glFunctionalCurrencies = null;
       
       LocalGlFunctionalCurrency glFunctionalCurrency = null;
       LocalAdCompany adCompany = null;
       
       
       ArrayList list = new ArrayList();
       
       // Initialize EJB Home
       
       try {
           
           glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
           
       } catch (NamingException ex) {
           
           throw new EJBException(ex.getMessage());
           
       }
       
       try {
           
           adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	               
           glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
           	EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       if (glFunctionalCurrencies.isEmpty()) {
       	
       	return null;
       	
       }
       
       Iterator i = glFunctionalCurrencies.iterator();
              
       while (i.hasNext()) {
       	
       	glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
       	
       	GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
       	    glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
       	    adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
       	       EJBCommon.TRUE : EJBCommon.FALSE);
       	
       	list.add(mdetails);
       	
       }
       
       return list;
           
   }   
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {
   
      Debug.print("GlChartOfAccountControllerBean ejbCreate");
      
   }

   // private methods

   private ArrayList getGenValueSetValueWithValue(LocalGenField genField, String COA_ACCNT_NMBR, Integer AD_CMPNY)
      throws GlCOAAccountNumberIsInvalidException, GlCOAAccountNumberHasParentValueException {

      Debug.print("GlChartOfAccountControllerBean getGenValueSetValueWithValue");

      LocalGenSegment genSegment = null;
      LocalGenValueSetValue genValueSetValue = null;

      Collection genSegments = null;
      ArrayList genValueSetValueList = new ArrayList();
      
      LocalGenValueSetValueHome genValueSetValueHome = null;
      LocalGenSegmentHome genSegmentHome = null;
      
      // Initialize EJB Home
        
      try {
            
          genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
          genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
              lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
                   
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }


      char chrSeparator;
      String strSeparator = new String();

      try {
      	
         genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
         chrSeparator = genField.getFlSegmentSeparator();
         strSeparator = String.valueOf(chrSeparator);
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }

      StringTokenizer st = new StringTokenizer(COA_ACCNT_NMBR, strSeparator);
      Iterator j = genSegments.iterator();
      
      while (st.hasMoreTokens()) {

         LocalGenValueSet genValueSet = null;
         genSegment = (LocalGenSegment) j.next();        
         genValueSet = genSegment.getGenValueSet();
           
         try {
         	
         	genValueSetValue = genValueSetValueHome.findByVsCodeAndVsvValue(genValueSet.getVsCode(), st.nextToken(), AD_CMPNY);
         	
         } catch (NoSuchElementException ex) {
         	
            throw new GlCOAAccountNumberIsInvalidException();
            
         } catch (FinderException ex) {
         	
            throw new GlCOAAccountNumberIsInvalidException();
            
         } catch (Exception ex) {
         	
	        throw new EJBException(ex.getMessage());
	        
	     }

         if (genValueSetValue.getVsvParent() == EJBCommon.TRUE) {
         	
            throw new GlCOAAccountNumberHasParentValueException();
            
         }

         genValueSetValueList.add(genValueSetValue);
      }

      return genValueSetValueList;
   }

   private void updateGlCoaEntryWithRelation(LocalGlChartOfAccount glChartOfAccount,
      GlChartOfAccountDetails details, String GL_FUNCTIONAL_CURRENCY, Integer AD_CMPNY)
      throws GlCOAAccountNumberAlreadyAssignedException {
	  
      LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
      
      // Initialize EJB Home
        
      try {
            
    	  glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);

                   
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
   	
   	if (!glChartOfAccount.getCoaAccountNumber().equals(details.getCoaAccountNumber()) ||
   			glChartOfAccount.getCoaDateFrom().getTime() != details.getCoaDateFrom().getTime()) {
   		
   		// account number or date from  or functional currency is modified then update
   		
   		throw new GlCOAAccountNumberAlreadyAssignedException("Account, Effective Date From or Currency cannot be updated");
   	
   	} else {      	
   		
   		try {
   			
   			// glChartOfAccount.setCoaAccountDescription(details.getCoaAccountDescription()); // to be deleted
   			// glChartOfAccount.setCoaAccountType(details.getCoaAccountType()); // to be deleted
   			glChartOfAccount.setCoaCitCategory(details.getCoaCitCategory());
   			glChartOfAccount.setCoaSawCategory(details.getCoaSawCategory()); 
   			glChartOfAccount.setCoaIitCategory(details.getCoaIitCategory());
   			glChartOfAccount.setCoaDateTo(details.getCoaDateTo());
   			glChartOfAccount.setCoaEnable(details.getCoaEnable());
   			glChartOfAccount.setCoaTaxType(details.getCoaTaxType());
   		    glChartOfAccount.setCoaForRevaluation(details.getCoaForRevaluation());
   			
   			if (GL_FUNCTIONAL_CURRENCY != null && GL_FUNCTIONAL_CURRENCY.length() > 0) {
	   			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(GL_FUNCTIONAL_CURRENCY, AD_CMPNY);
	   			glFunctionalCurrency.addGlChartOfAccount(glChartOfAccount);
   			} else {
   				if (glChartOfAccount.getGlFunctionalCurrency() != null) {   					
   					glChartOfAccount.getGlFunctionalCurrency().dropGlChartOfAccount(glChartOfAccount);
   				}
   			}

   			
   		} catch (Exception ex) {
   			
   			ex.printStackTrace();
   			
   			getSessionContext().setRollbackOnly();
   			throw new EJBException();
   			
   		}
   	}
   }

   private boolean hasRelation(LocalGlChartOfAccount glChartOfAccount, Integer AD_CMPNY) {

      Debug.print("GlChartOfAccountControllerBean hasRelation");
      
      LocalApSupplierClassHome apSupplierClassHome = null;
      LocalArCustomerClassHome arCustomerClassHome = null;
      LocalAdBankAccountHome adBankAccountHome = null;
      LocalApSupplierHome apSupplierHome = null;
      LocalArCustomerHome arCustomerHome = null;
      LocalInvItemLocationHome invItemLocationHome = null;
      LocalInvOverheadMemoLineHome invOverheadMemoLineHome = null;
            
      LocalAdCompanyHome adCompanyHome = null;
      
      LocalAdBranchSupplierHome adBranchSupplierHome = null;
      LocalAdBranchCustomerHome adBranchCustomerHome = null;
      
      LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
      LocalAdBranchOverheadMemoLineHome adBranchOverheadMemoLineHome = null;
      
      LocalArStandardMemoLineHome arStandardMemoLineHome = null;
      LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome =  null;
      LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
      LocalGlRecurringJournalHome glRecurringJournalHome = null;
      LocalGlJournalHome glJournalHome = null;
      LocalApDistributionRecordHome apDistributionRecordHome = null;
      LocalArDistributionRecordHome arDistributionRecordHome = null;
      LocalCmDistributionRecordHome cmDistributionRecordHome = null;
      LocalInvDistributionRecordHome invDistributionRecordHome = null;
      LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;
      LocalAdPaymentTermHome adPaymentTermHome  = null;
      LocalApTaxCodeHome apTaxCodeHome = null;
      LocalArTaxCodeHome arTaxCodeHome = null;
      LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
      LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
      LocalInvAdjustmentHome invAdjustmentHome = null;
      
      
      // Initialize EJB Home
        
      try {
            
          apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
              lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
          arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
              lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class); 
          adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);       
          apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
          	  lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
          arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
          	  lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
          invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
		  	  lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);                  
          invOverheadMemoLineHome = (LocalInvOverheadMemoLineHome)EJBHomeFactory.
		  	   lookUpLocalHome(LocalInvOverheadMemoLineHome.JNDI_NAME, LocalInvOverheadMemoLineHome.class);  
          
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	  	   		lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class); 
          adBranchSupplierHome = (LocalAdBranchSupplierHome)EJBHomeFactory.
	   			lookUpLocalHome(LocalAdBranchSupplierHome.JNDI_NAME, LocalAdBranchSupplierHome.class); 
          adBranchCustomerHome = (LocalAdBranchCustomerHome)EJBHomeFactory.
 			lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class); 
         adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
          adBranchOverheadMemoLineHome = (LocalAdBranchOverheadMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchOverheadMemoLineHome.JNDI_NAME, LocalAdBranchOverheadMemoLineHome.class); 
          arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class); 
          adBranchStandardMemoLineHome = (LocalAdBranchStandardMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class); 
          glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class); 
          glRecurringJournalHome = (LocalGlRecurringJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlRecurringJournalHome.JNDI_NAME, LocalGlRecurringJournalHome.class); 
          glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class); 
          apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class); 
          arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class); 
          cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class); 
          invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
          adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class); 
          adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class); 
          apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class); 
          arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class); 
          apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class); 
          arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class); 
          invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class); 

          
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      try {
          
          // supplier class
	            
	      Collection apSupplierClasses1 = apSupplierClassHome.findByScGlCoaPayableAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection apSupplierClasses2 = apSupplierClassHome.findByScGlCoaExpenseAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      
	      // customer class
	      
	      Collection arCustomerClasses1 = arCustomerClassHome.findByCcGlCoaChargeAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection arCustomerClasses2 = arCustomerClassHome.findByCcGlCoaReceivableAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection arCustomerClasses3 = arCustomerClassHome.findByCcGlCoaRevenueAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	            
	      // bank account
	      
	      Collection adBankAccount1 = adBankAccountHome.findByBaGlCoaCashAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection adBankAccount2 = adBankAccountHome.findByBaGlCoaOnAccountReceipt(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection adBankAccount3 = adBankAccountHome.findByBaGlCoaUnappliedReceipt(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection adBankAccount4 = adBankAccountHome.findByBaGlCoaBankChargeAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection adBankAccount5 = adBankAccountHome.findByBaGlCoaClearingAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection adBankAccount6 = adBankAccountHome.findByBaGlCoaInterestAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection adBankAccount7 = adBankAccountHome.findByBaGlCoaAdjustmentAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection adBankAccount8 = adBankAccountHome.findByBaGlCoaCashDiscount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection adBankAccount9 = adBankAccountHome.findByBaGlCoaSalesDiscount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection adBankAccount10 = adBankAccountHome.findByBaGlCoaUnappliedCheck(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      
	      // supplier
	            
	      Collection apSupplier1 = apSupplierHome.findBySplCoaGlPayableAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection apSupplier2 = apSupplierHome.findBySplCoaGlExpenseAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      
	      Collection adBranchSuppliers1 = adBranchSupplierHome.findByBsplGlCoaPayableAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          Collection adBranchSuppliers2 = adBranchSupplierHome.findByBsplGlCoaExpenseAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          
	      // customer
	            
	      Collection arCustomer1 = arCustomerHome.findByCstGlCoaReceivableAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection arCustomer2 = arCustomerHome.findByCstGlCoaRevenueAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      
	      Collection adBranchCustomers1 = adBranchCustomerHome.findByBcstGlCoaReceivableAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          Collection adBranchCustomers2 = adBranchCustomerHome.findByBcstGlCoaRevenueAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          
	      // inventory item location
	      Collection  invItemLocation1 = invItemLocationHome.findByIlGlCoaSalesAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection  invItemLocation2 = invItemLocationHome.findByIlGlCoaInventoryAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection  invItemLocation3 = invItemLocationHome.findByIlGlCoaCostOfSalesAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection  invItemLocation4 = invItemLocationHome.findByIlGlCoaWipAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection  invItemLocation5 = invItemLocationHome.findByIlGlCoaAccruedInventoryAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      
	      Collection adBranchItemLocations1 = adBranchItemLocationHome.findByBilGlCoaSalesAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          Collection adBranchItemLocations2 = adBranchItemLocationHome.findByBilGlCoaInventoryAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          Collection adBranchItemLocations3 = adBranchItemLocationHome.findByBilGlCoaCostOfSalesAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          Collection adBranchItemLocations4 = adBranchItemLocationHome.findByBilGlCoaWipAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          Collection adBranchItemLocations5 = adBranchItemLocationHome.findByBilGlCoaAccruedInventoryAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          
	      // inventory overhead memo line
	      Collection  invOverheadMemoLine1 = invOverheadMemoLineHome.findByOmlGlCoaOverheadAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      Collection  invOverheadMemoLine2 = invOverheadMemoLineHome.findByOmlGlCoaLiabilityAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
	      
	      Collection adBranchOverheadMemoLines1 = adBranchOverheadMemoLineHome.findByBomlGlCoaOverheadAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          Collection adBranchOverheadMemoLines2 = adBranchOverheadMemoLineHome.findByBomlGlCoaLiabilityAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          
          // branch standard memo lines
          Collection adBranchStandardMemoLines1 = adBranchStandardMemoLineHome.findByBSMLGlAccount(glChartOfAccount.getCoaCode(), AD_CMPNY);
          
          
	      if (!glChartOfAccount.getGlSuspenseAccounts().isEmpty() ||
	          !glChartOfAccount.getGlRecurringJournalLines().isEmpty() ||
	          !glChartOfAccount.getGlJournalLines().isEmpty() ||
	          !glChartOfAccount.getApDistributionRecords().isEmpty() ||
	          !glChartOfAccount.getApTaxCodes().isEmpty() ||
	          !glChartOfAccount.getApWithholdingTaxCodes().isEmpty() ||
	          !glChartOfAccount.getArTaxCodes().isEmpty() ||
	          !glChartOfAccount.getArWithholdingTaxCodes().isEmpty() ||
	          !glChartOfAccount.getArDistributionRecords().isEmpty() ||
	          !glChartOfAccount.getArStandardMemoLines().isEmpty() ||
	          !glChartOfAccount.getAdApprovalCoaLines().isEmpty() ||
	          !glChartOfAccount.getCmDistributionRecords().isEmpty() ||
	          !glChartOfAccount.getGlBudgetAmountCoas().isEmpty() ||
	          !glChartOfAccount.getInvAdjustments().isEmpty() ||
	          !glChartOfAccount.getInvDistributionRecords().isEmpty() ||
	          !glChartOfAccount.getAdPaymentTerms().isEmpty()||
	          !apSupplierClasses1.isEmpty() ||
			  !apSupplierClasses2.isEmpty() ||
			  !arCustomerClasses1.isEmpty() ||
			  !arCustomerClasses2.isEmpty() ||
			  !arCustomerClasses3.isEmpty() ||
			  !adBankAccount1.isEmpty() ||
			  !adBankAccount2.isEmpty() ||
			  !adBankAccount3.isEmpty() ||
			  !adBankAccount4.isEmpty() ||
			  !adBankAccount5.isEmpty() ||
			  !adBankAccount6.isEmpty() ||
			  !adBankAccount7.isEmpty() || 
			  !adBankAccount8.isEmpty() ||
			  !adBankAccount9.isEmpty() ||
			  !adBankAccount10.isEmpty() ||
			  !apSupplier1.isEmpty() ||
			  !apSupplier2.isEmpty() ||
			  !arCustomer1.isEmpty() ||
			  !arCustomer2.isEmpty() ||
			  !invItemLocation1.isEmpty() ||
			  !invItemLocation2.isEmpty() ||
			  !invItemLocation3.isEmpty() ||
			  !invItemLocation4.isEmpty()||
			  !invItemLocation5.isEmpty()||
			  !invOverheadMemoLine1.isEmpty()||
			  !invOverheadMemoLine2.isEmpty()||
			  !adBranchSuppliers1.isEmpty()||
			  !adBranchSuppliers2.isEmpty()||
			  !adBranchCustomers1.isEmpty()||
			  !adBranchCustomers2.isEmpty()||
			  !adBranchItemLocations1.isEmpty()||
			  !adBranchItemLocations2.isEmpty()||
			  !adBranchItemLocations3.isEmpty()||
			  !adBranchItemLocations4.isEmpty()||
			  !adBranchItemLocations5.isEmpty()||
			  !adBranchOverheadMemoLines1.isEmpty()||
			  !adBranchOverheadMemoLines2.isEmpty()||
			  !adBranchStandardMemoLines1.isEmpty()) {
			  
	      	  return true;
	      } 
	      
	      return false;
      
      } catch (Exception ex) {

          throw new EJBException(ex.getMessage());
      
      }
      
   }
   
   private boolean hasRelation(LocalAdBranchCoa adBranchCoa, Integer AD_CMPNY) {
   	
   	Debug.print("GlChartOfAccountControllerBean hasRelation");
   	
   	LocalGlJournalLineHome glJournalLineHome = null;
   	LocalApDistributionRecordHome apDistributionRecordHome = null;
   	LocalArDistributionRecordHome arDistributionRecordHome = null;
   	LocalCmDistributionRecordHome cmDistributionRecordHome = null;
   	LocalInvDistributionRecordHome invDistributionRecordHome = null;
   	LocalInvAdjustmentHome invAdjustmentHome = null;
   	
   	// Initialize EJB Home
   	
   	try {
   		
   		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
		lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class); 
   		apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
		lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class); 
   		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
		lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class); 
   		cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
		lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class); 
   		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
   		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class); 
   		
   	} catch (NamingException ex) {
   		
   		throw new EJBException(ex.getMessage());
   		
   	}
   	
   	try {
   		
   		// check if coa has existing unposted transactions
   		
   		Collection glJournalLines = glJournalLineHome.findUnpostedJrByJlCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		
   		Collection apDistributionRecords1 = apDistributionRecordHome.findUnpostedVouByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		Collection apDistributionRecords2 = apDistributionRecordHome.findUnpostedChkByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		Collection apDistributionRecords3 = apDistributionRecordHome.findUnpostedPoByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		
   		Collection arDistributionRercords1 = arDistributionRecordHome.findUnpostedInvByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		Collection arDistributionRercords2 = arDistributionRecordHome.findUnpostedRctByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		
   		Collection cmDistributionRercords1 = cmDistributionRecordHome.findUnpostedAdjByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		Collection cmDistributionRercords2 = cmDistributionRecordHome.findUnpostedFtByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		
   		Collection invAdjustments = invAdjustmentHome.findUnpostedAdjByAdjCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		Collection invDistributionRecords1 = invDistributionRecordHome.findUnpostedAdjByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		Collection invDistributionRecords2 = invDistributionRecordHome.findUnpostedBuaByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		Collection invDistributionRecords3 = invDistributionRecordHome.findUnpostedSiByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		Collection invDistributionRecords4 = invDistributionRecordHome.findUnpostedAtrByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		Collection invDistributionRecords5 = invDistributionRecordHome.findUnpostedStByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		Collection invDistributionRecords6 = invDistributionRecordHome.findUnpostedBstByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		Collection invDistributionRecords7 = invDistributionRecordHome.findUnpostedOhByDrCoaAccountNumberAndBrCode(
   				adBranchCoa.getGlChartOfAccount().getCoaCode(), adBranchCoa.getAdBranch().getBrCode(), AD_CMPNY);
   		
   		if(!glJournalLines.isEmpty() || !apDistributionRecords1.isEmpty() || 
   				!apDistributionRecords2.isEmpty() || !apDistributionRecords3.isEmpty() || 
				!arDistributionRercords1.isEmpty() || !arDistributionRercords2.isEmpty() || 
				!cmDistributionRercords1.isEmpty() || !cmDistributionRercords2.isEmpty() || 
				!invAdjustments.isEmpty() || !invDistributionRecords1.isEmpty() || 
				!invDistributionRecords2.isEmpty() || !invDistributionRecords3.isEmpty() || 
				!invDistributionRecords4.isEmpty() || !invDistributionRecords5.isEmpty() || 
				!invDistributionRecords6.isEmpty() || !invDistributionRecords7.isEmpty()) {
   			
   			return true;
   			
   		} 
   		
   		return false;
   		
   	} catch (Exception ex) {
   		
   		throw new EJBException(ex.getMessage());
   		
   	}
   	
   }

}
