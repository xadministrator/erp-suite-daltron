package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.GlFCFunctionalCurrencyAlreadyAssignedException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyDeletedException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyExistException;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlFunctionalCurrencyDetails;

/**
 * @ejb:bean name="GlCurrencyMaintenanceControllerEJB"
 *           display-name="Used for maintenance of currencies"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlCurrencyMaintenanceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlCurrencyMaintenanceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlCurrencyMaintenanceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlCurrencyMaintenanceControllerBean extends AbstractSessionBean {


   /*******************************************************************
   
   	Business methods:

	(1) getFcAll - returns an Arraylist of FC

	(2) addGlFcEntry - validate input and duplicate entries
	                   before adding the FC

	(3) updateGlFcEntry - validate if FC to be updated is
	                      already assigned to related
	                      tables, if already assigned then
	                      description, date to and enabled
			      fields are the only fields that 
			      can be updated, otherwise
	                      all fields can be entered

	(4) deleteGlFcEntry - validate if the FC to be deleted is
	                      already assigned to related tables,
	                      if already assigned then this FC
	                      cannot be deleted

	
	Private methods:

	(1) hasRelation - returns true if a specific FC has a relation
	                  with any other table otherwise returns
	                  false
 
      
   *******************************************************************/
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlFcAll(Integer AD_CMPNY)
      throws GlFCNoFunctionalCurrencyFoundException {

      Debug.print("GlCurrencyMaintenanceControllerBean getGlFcAll");

      /***************************************************************
	Gets all FC	
      ***************************************************************/
      
      LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
      
      // Initialize EJB Home
        
      try {
            
          glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      ArrayList fcAllList = new ArrayList();
      Collection glFunctionalCurrencies = null;

      try {
         glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glFunctionalCurrencies.size() == 0)
         throw new GlFCNoFunctionalCurrencyFoundException();
         
      Iterator i = glFunctionalCurrencies.iterator();
      while (i.hasNext()) {
         LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency) i.next();
         GlFunctionalCurrencyDetails details = new GlFunctionalCurrencyDetails(
            glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(), 
            glFunctionalCurrency.getFcDescription(), glFunctionalCurrency.getFcCountry(),
	    glFunctionalCurrency.getFcSymbol(), glFunctionalCurrency.getFcPrecision(),
	    glFunctionalCurrency.getFcExtendedPrecision(), glFunctionalCurrency.getFcMinimumAccountUnit(),
	    glFunctionalCurrency.getFcDateFrom(), glFunctionalCurrency.getFcDateTo(),
	    glFunctionalCurrency.getFcEnable());
         fcAllList.add(details);
      }

      return fcAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void addGlFcEntry(GlFunctionalCurrencyDetails details, Integer AD_CMPNY)
      throws GlFCFunctionalCurrencyAlreadyExistException {

      Debug.print("GlFunctionalCurrencyControllerBean addGlFcEntry");
      
      /**************************
     	Adds a FC entry
      **************************/

      
      LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
      
      // Initialize EJB Home
        
      try {
            
          glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      try {
      	
      	glFunctionalCurrencyHome.findByFcName(details.getFcName(), AD_CMPNY);
        
        getSessionContext().setRollbackOnly();
        throw new GlFCFunctionalCurrencyAlreadyExistException();
        
      } catch (FinderException ex) {
        
      }

      try {
         LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.create(
	    details.getFcName(), details.getFcDescription(), details.getFcCountry(),
	    details.getFcSymbol(), details.getFcPrecision(), details.getFcExtendedPrecision(),
	    details.getFcMinimumAccountUnit(), details.getFcDateFrom(), details.getFcDateTo(),
	    details.getFcEnable(), AD_CMPNY);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
	 throw new EJBException(ex.getMessage()); 
      }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlFcEntry(GlFunctionalCurrencyDetails details, Integer AD_CMPNY)
      throws GlFCFunctionalCurrencyAlreadyExistException,
      GlFCFunctionalCurrencyAlreadyAssignedException,
      GlFCFunctionalCurrencyAlreadyDeletedException {

      Debug.print("GlFunctionalCurrencyControllerBean updateGlFcEntry");

      /*******************************
	   Updates an existing FC entry
      *******************************/
      
      LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
      
      // Initialize EJB Home
        
      try {
            
          glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      LocalGlFunctionalCurrency glFunctionalCurrency = null;

      try {
         glFunctionalCurrency = glFunctionalCurrencyHome.findByPrimaryKey(
	    details.getFcCode());
      } catch (FinderException ex){
      	throw new GlFCFunctionalCurrencyAlreadyDeletedException();
      } catch (Exception ex) {
        throw new EJBException(ex.getMessage());
      }

      if (hasRelation(glFunctionalCurrency, AD_CMPNY))
         updateGlFcEntryWithRelation(glFunctionalCurrency, details, AD_CMPNY);
      else {
       
         LocalGlFunctionalCurrency glFunctionalCurrency2 = null;

         try {
	    glFunctionalCurrency2 = 
	       glFunctionalCurrencyHome.findByFcName(details.getFcName(), AD_CMPNY);
	 } catch (FinderException ex) {
	    glFunctionalCurrency.setFcName(details.getFcName());
	    glFunctionalCurrency.setFcDescription(details.getFcDescription());
	    glFunctionalCurrency.setFcCountry(details.getFcCountry());
	    glFunctionalCurrency.setFcSymbol(details.getFcSymbol());
	    glFunctionalCurrency.setFcPrecision(details.getFcPrecision());
	    glFunctionalCurrency.setFcExtendedPrecision(details.getFcExtendedPrecision());
	    glFunctionalCurrency.setFcMinimumAccountUnit(details.getFcMinimumAccountUnit());
	    glFunctionalCurrency.setFcDateFrom(details.getFcDateFrom());
	    glFunctionalCurrency.setFcDateTo(details.getFcDateTo());
	    glFunctionalCurrency.setFcEnable(details.getFcEnable());
	 } catch (Exception ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 }

	 if(glFunctionalCurrency2 != null && !glFunctionalCurrency.getFcCode().equals(glFunctionalCurrency2.getFcCode())) {
	    getSessionContext().setRollbackOnly();
	    throw new GlFCFunctionalCurrencyAlreadyExistException();
	 } else 
	    if(glFunctionalCurrency2 != null && glFunctionalCurrency.getFcCode().equals(glFunctionalCurrency2.getFcCode())) {
	       glFunctionalCurrency.setFcDescription(details.getFcDescription());
	       glFunctionalCurrency.setFcCountry(details.getFcCountry());
               glFunctionalCurrency.setFcSymbol(details.getFcSymbol());
               glFunctionalCurrency.setFcPrecision(details.getFcPrecision());
               glFunctionalCurrency.setFcExtendedPrecision(details.getFcExtendedPrecision());
               glFunctionalCurrency.setFcMinimumAccountUnit(details.getFcMinimumAccountUnit());
               glFunctionalCurrency.setFcDateFrom(details.getFcDateFrom());
               glFunctionalCurrency.setFcDateTo(details.getFcDateTo());
               glFunctionalCurrency.setFcEnable(details.getFcEnable());	
	 }
      }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteGlFcEntry(Integer FC_CODE, Integer AD_CMPNY) 
      throws GlFCFunctionalCurrencyAlreadyDeletedException,
      GlFCFunctionalCurrencyAlreadyAssignedException {

      Debug.print("GlFunctionalCurrencyControllerBean deleteGlFcEntry");

      /*******************************
	   Deletes an existing FC entry      
      *******************************/		
      
      LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
      
      // Initialize EJB Home
        
      try {
            
          glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      LocalGlFunctionalCurrency glFunctionalCurrency = null;

      try {
         glFunctionalCurrency = glFunctionalCurrencyHome.findByPrimaryKey(FC_CODE);
      } catch (FinderException ex) {
         throw new GlFCFunctionalCurrencyAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (hasRelation(glFunctionalCurrency, AD_CMPNY))
         throw new GlFCFunctionalCurrencyAlreadyAssignedException();
      else {
         try {
	    glFunctionalCurrency.remove();
	 } catch (RemoveException ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 } catch (Exception ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 }
      }
   }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlFunctionalCurrencyControllerBean ejbCreate");
      
   }

   // private methods

   private void updateGlFcEntryWithRelation(LocalGlFunctionalCurrency glFunctionalCurrency,
      GlFunctionalCurrencyDetails details, Integer AD_CMPNY)
      throws GlFCFunctionalCurrencyAlreadyAssignedException {

    /************************************************************
       This method assumes the seleced FC has at least one
       relationship, in this case, functional currency name,
       country, symbol, precision, extended precision, 
       minimum account unit and date from fields should never 
       be modified
    ***********************************************************/

       if (!glFunctionalCurrency.getFcName().equals(details.getFcName()) ||
           !glFunctionalCurrency.getFcCountry().equals(details.getFcCountry()) ||
	   glFunctionalCurrency.getFcSymbol() != details.getFcSymbol() ||
	   glFunctionalCurrency.getFcPrecision() != details.getFcPrecision() ||
	   glFunctionalCurrency.getFcExtendedPrecision() != details.getFcExtendedPrecision() ||
	   glFunctionalCurrency.getFcMinimumAccountUnit() != details.getFcMinimumAccountUnit() ||
	   glFunctionalCurrency.getFcDateFrom().getTime() != details.getFcDateFrom().getTime()) {
	   
            throw new GlFCFunctionalCurrencyAlreadyAssignedException();
       } else {

           try {
              glFunctionalCurrency.setFcDescription(details.getFcDescription());
	      glFunctionalCurrency.setFcDateTo(details.getFcDateTo());
              glFunctionalCurrency.setFcEnable(details.getFcEnable());
           } catch (Exception ex) {
              getSessionContext().setRollbackOnly();
              throw new EJBException();
           }
       }
   }

    private boolean hasRelation(LocalGlFunctionalCurrency glFunctionalCurrency, Integer AD_CMPNY) {

       Debug.print("GlFunctionalCurrencyControllerBean hasRelation");

       if(!glFunctionalCurrency.getGlJournals().isEmpty() ||
		  !glFunctionalCurrency.getGlFunctionalCurrencyRates().isEmpty() ||
		  !glFunctionalCurrency.getApRecurringVouchers().isEmpty() ||
		  !glFunctionalCurrency.getApVouchers().isEmpty() ||
		  !glFunctionalCurrency.getApChecks().isEmpty() ||
		  !glFunctionalCurrency.getArInvoices().isEmpty() ||
		  !glFunctionalCurrency.getArReceipts().isEmpty() ||
		  !glFunctionalCurrency.getAdBankAccounts().isEmpty() ||
		  !glFunctionalCurrency.getAdCompanies().isEmpty()||
		  !glFunctionalCurrency.getApPurchaseOrders().isEmpty()||
		  !glFunctionalCurrency.getApPurchaseRequisitions().isEmpty()||
		  !glFunctionalCurrency.getArPdcs().isEmpty()||
		  !glFunctionalCurrency.getArReceipts().isEmpty()||
		  !glFunctionalCurrency.getArSalesOrders().isEmpty()||
          !glFunctionalCurrency.getGlChartOfAccounts().isEmpty()) {
          return true;
          
       }
       
       return false;
    }

}
