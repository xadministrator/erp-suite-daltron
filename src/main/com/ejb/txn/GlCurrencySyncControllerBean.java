/*
 * GlCurrencySyncControllerBean
 *
 * Created on May 13, 2007, 9:03 AM
 *
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="GlCurrencySyncControllerBean" type="Stateless"
 *           view-type="service-endpoint"
 * 
 * @wsee:port-component name="GlCurrencySync"
 * 
 * @jboss:port-component uri="omega-ejb/GlCurrencySyncWS"
 * 
 * @ejb:interface service-endpoint-class="com.ejb.txn.GlCurrencySyncWS"
 * 
 */

public class GlCurrencySyncControllerBean implements SessionBean {

	private SessionContext ctx;

	/**
	 * @ejb:interface-method
	 */
	public String[] getGlFcAll(Integer AD_CMPNY) {

		Debug.print("GlCurrencySyncControllerBean getGlFcAll");

		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME,
							LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
							LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			Collection glFunctionalCurrencies = glFunctionalCurrencyHome
					.findFcAllEnabled(new Date(), AD_CMPNY);

			String[] results = new String[glFunctionalCurrencies.size()];

			Iterator i = glFunctionalCurrencies.iterator();
			int ctr = 0;
			while (i.hasNext()) {

				LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency) i
						.next();

				results[ctr] = currencyRowEncode(glFunctionalCurrency,
						adCompany);
				System.out.println("results-" + results[ctr]);
				ctr++;

			}

			return removeDuplicate(results);

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}

	private String currencyRowEncode(
			LocalGlFunctionalCurrency glFunctionalCurrency,
			LocalAdCompany adCompany) {

		char separator = EJBCommon.SEPARATOR;
		StringBuffer tempResult = new StringBuffer();
		String encodedResult = new String();

		// Start separator
		tempResult.append(separator);

		// Primary Key
		tempResult.append(glFunctionalCurrency.getFcCode());
		tempResult.append(separator);

		// Currency name
		tempResult.append(glFunctionalCurrency.getFcName());
		tempResult.append(separator);

		// Base Currency
		if (glFunctionalCurrency.getFcName().equals(
				adCompany.getGlFunctionalCurrency().getFcName())) {
			tempResult.append("1");
			tempResult.append(separator);
		} else {
			tempResult.append("0");
			tempResult.append(separator);
		}

		// remove unwanted chars from encodedResult;
		encodedResult = tempResult.toString();
		encodedResult = encodedResult.replace("\"", " ");
		encodedResult = encodedResult.replace("'", " ");
		encodedResult = encodedResult.replace(";", " ");
		encodedResult = encodedResult.replace("\\", " ");
		encodedResult = encodedResult.replace("|", " ");

		return encodedResult;

	}

	/**
	 * @ejb:interface-method
	 */
	public String[] getGlCurrentFcRates(Integer AD_CMPNY) {

		Debug.print("GlCurrencySyncControllerBean getGlCurrentFcRates");

		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome) EJBHomeFactory
					.lookUpLocalHome(
							LocalGlFunctionalCurrencyRateHome.JNDI_NAME,
							LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
					.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME,
							LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
							LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			float fcRate = 0;
            boolean isFcExist = true;
			try {

				// get fc rate to usd
				LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome
						.findByFcName(adCompany.getGlFunctionalCurrency()
								.getFcName(), AD_CMPNY);
				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome
						.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
								EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);
				fcRate = (float)glFunctionalCurrencyRate.getFrXToUsd();
			} catch (FinderException ex) {

				fcRate = 1;
				isFcExist = false;

			}

			Collection glFunctionalCurrencyRates = glFunctionalCurrencyRateHome
					.findByFrDate(EJBCommon.getGcCurrentDateWoTime().getTime(),
							AD_CMPNY);

			System.out.println("glFunctionalCurrencyRates-"
					+ glFunctionalCurrencyRates.size());

			String[] results = null;
			if (isFcExist) {
				results = new String[glFunctionalCurrencyRates.size() + 1];
			} else {
				results = new String[glFunctionalCurrencyRates.size()];
			}

			Iterator i = glFunctionalCurrencyRates.iterator();
			int ctr = 0;
			while (i.hasNext()) {

				LocalGlFunctionalCurrencyRate glForeignCurrencyRate = (LocalGlFunctionalCurrencyRate) i
						.next();
				// get foreign currency rate to usd
				float foreignRate = (float)glForeignCurrencyRate.getFrXToUsd();
				
				results[ctr] = this.currencyRateRowEncode(String
						.valueOf(glForeignCurrencyRate
								.getGlFunctionalCurrency().getFcCode()), String
						.valueOf(foreignRate / fcRate));
				ctr++;

			}
			
			if (isFcExist) {
			
				// for usd
				LocalGlFunctionalCurrency glUsdFunctionalCurrency = glFunctionalCurrencyHome
				.findByFcName("USD", AD_CMPNY);
				results[ctr] = this.currencyRateRowEncode(String
						.valueOf(glUsdFunctionalCurrency.getFcCode()), String
						.valueOf(1 / fcRate));
				
			} 

			return removeDuplicate(results);

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}

	private String currencyRateRowEncode(String primaryKey, String foreignRate) {

		char separator = EJBCommon.SEPARATOR;
		StringBuffer tempResult = new StringBuffer();
		String encodedResult = new String();

		// Start separator
		tempResult.append(separator);

		// Primary Key
		tempResult.append(primaryKey);
		tempResult.append(separator);

		// Foreign Rate
		tempResult.append(foreignRate);
		tempResult.append(separator);


		// remove unwanted chars from encodedResult;
		encodedResult = tempResult.toString();
		encodedResult = encodedResult.replace("\"", " ");
		encodedResult = encodedResult.replace("'", " ");
		encodedResult = encodedResult.replace(";", " ");
		encodedResult = encodedResult.replace("\\", " ");
		encodedResult = encodedResult.replace("|", " ");

		return encodedResult;

	}

	private String[] removeDuplicate(String[] s) {

		HashSet mySet = new HashSet();

		for (int x = 0; x < s.length; x++) {
			mySet.add(s[x]);
			System.out.println("To be added to set = " + s[x]);
		}

		System.out.println("mySet size=" + mySet.size());

		String[] newResults = new String[mySet.size()];

		Iterator iter = mySet.iterator();

		int newCtr = 0;

		String myString;

		while (iter.hasNext()) {
			myString = (String) iter.next();
			newResults[newCtr] = myString;
			System.out.println("Eto yung string = " + myString);
			newCtr++;
		}

		return newResults;
	}

	public void ejbCreate() throws CreateException {

		Debug.print("ArCustomerEntryControllerBean ejbCreate");

	}

	public void ejbRemove() {
	};

	public void ejbActivate() {
	}

	public void ejbPassivate() {
	}

	public void setSessionContext(SessionContext ctx) {
	}

}