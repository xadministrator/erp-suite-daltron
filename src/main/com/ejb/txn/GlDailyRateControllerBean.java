package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.exception.GlFCRFunctionalCurrencyRateAlreadyDeletedException;
import com.ejb.exception.GlFCRFunctionalCurrencyRateAlreadyExistException;
import com.ejb.exception.GlFCRNoFunctionalCurrencyRateFoundException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlFunctionalCurrencyDetails;
import com.util.GlFunctionalCurrencyRateDetails;
import com.util.GlModFunctionalCurrencyRateDetails;

/**
 * @ejb:bean name="GlDailyRateControllerEJB"
 *           display-name="Used for lookup of daily foreign exchange rates"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlDailyRateControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlDailyRateController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlDailyRateControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlDailyRateControllerBean extends AbstractSessionBean {


	
	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public com.util.GlModFunctionalCurrencyRateDetails getFrRateByFrName(String FC_NM, Integer AD_CMPNY)
 	throws GlobalConversionDateNotExistException {

 		Debug.print("GlDailyRateControllerBean getFrRateByFrName");

 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
 		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

 		// Initialize EJB Home

 		try {

 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
 			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
 			
 		} catch (NamingException ex) {

 			throw new EJBException(ex.getMessage());

 		}

 		try {

 			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

 			// Get functional currency rate

 			GlModFunctionalCurrencyRateDetails mdetails = new GlModFunctionalCurrencyRateDetails(null,1d,null,FC_NM );
 					
 			try {
 				
 				Collection glFunctionalCurrencyRates = glFunctionalCurrencyRateHome.findPriorByFcCode(
 						glFunctionalCurrency.getFcCode(), AD_CMPNY);
 				
 				ArrayList glFunctionalCurrencyRateList = new ArrayList(glFunctionalCurrencyRates);
 			  	LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = (LocalGlFunctionalCurrencyRate)glFunctionalCurrencyRateList.get(0);
 				
 			  	mdetails = new GlModFunctionalCurrencyRateDetails(
 			  		    glFunctionalCurrencyRate.getFrCode(), glFunctionalCurrencyRate.getFrXToUsd(),
 			  		    glFunctionalCurrencyRate.getFrDate(), glFunctionalCurrencyRate.getGlFunctionalCurrency().getFcName());

 			} catch (Exception e) {
				// TODO: handle exception
			}
 			

 			return mdetails;

 		} catch (FinderException ex) {

 			getSessionContext().setRollbackOnly();
 			throw new GlobalConversionDateNotExistException();

 		} catch (Exception ex) {

 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());

 		}

 	}
 	
 	
 	
 	
 	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY)
	throws GlobalConversionDateNotExistException {

		Debug.print("GlDailyRateControllerBean getFrRateByFrNameAndFrDate");

		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

		// Initialize EJB Home

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

			double CONVERSION_RATE = 1;

			LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
							CONVERSION_DATE, AD_CMPNY);

				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();

			return CONVERSION_RATE;

		} catch (FinderException ex) {

			getSessionContext().setRollbackOnly();
			throw new GlobalConversionDateNotExistException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
 	
 	
 	
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlFcAll(Integer AD_CMPNY)
      throws GlFCNoFunctionalCurrencyFoundException {

      Debug.print("GlDailyRateControllerBean getGlFcAll");

      /***************************************************************
	Gets all enabled FC	
      ***************************************************************/

      ArrayList fcAllList = new ArrayList();
      Collection glFunctionalCurrencies = null;
      
      LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
      
      // Initialize EJB Home
        
      try {
            
          glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
	    new Date(EJBCommon.getGcCurrentDateWoTime().getTime().getTime()), AD_CMPNY);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glFunctionalCurrencies.size() == 0)
         throw new GlFCNoFunctionalCurrencyFoundException();
         
      Iterator i = glFunctionalCurrencies.iterator();
      while (i.hasNext()) {
         LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency) i.next();
         GlFunctionalCurrencyDetails details = new GlFunctionalCurrencyDetails(
            glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(), 
            glFunctionalCurrency.getFcDescription(), glFunctionalCurrency.getFcCountry(),
	    glFunctionalCurrency.getFcSymbol(), glFunctionalCurrency.getFcPrecision(),
	    glFunctionalCurrency.getFcExtendedPrecision(), glFunctionalCurrency.getFcMinimumAccountUnit(),
	    glFunctionalCurrency.getFcDateFrom(), glFunctionalCurrency.getFcDateTo(),
	    glFunctionalCurrency.getFcEnable());
         fcAllList.add(details);
      }

      return fcAllList;
   }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public com.util.GlModFunctionalCurrencyRateDetails getGlFcrByFrCode(Integer FR_CODE, Integer AD_CMPNY)
      throws GlFCRNoFunctionalCurrencyRateFoundException {

      Debug.print("GlDailyRateControllerBean getGlFcrByFrCode");

      /***************************************************************
         Gets all FCR including functional currency name
      ***************************************************************/
     
      LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
      
      LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = null;
      
      // Initialize EJB Home
        
      try {
            
          glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
      	
         glFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByPrimaryKey(FR_CODE);
         
      } catch (FinderException ex) {
      	
      	 throw new GlFCRNoFunctionalCurrencyRateFoundException();
      	
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }
 
      
	  GlModFunctionalCurrencyRateDetails mdetails = new GlModFunctionalCurrencyRateDetails(
	    glFunctionalCurrencyRate.getFrCode(), glFunctionalCurrencyRate.getFrXToUsd(),
	    glFunctionalCurrencyRate.getFrDate(), glFunctionalCurrencyRate.getGlFunctionalCurrency().getFcName());
	    
      return mdetails;
		     
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void addGlFcrEntry(GlFunctionalCurrencyRateDetails details, String FC_NM, Integer AD_CMPNY)
      throws GlFCNoFunctionalCurrencyFoundException,
      GlFCRFunctionalCurrencyRateAlreadyExistException {
	   
	   Debug.print("GlDailyRateControllerBean addGlFcrEntry");
	   
	   /***************************************************************
	    Adds a FCR
	    ***************************************************************/
	   
	   LocalGlFunctionalCurrency glFunctionalCurrency = null;
	   LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = null;
	   
	   LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
	   LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
	   
	   // Initialize EJB Home
	   
	   try {
		   
		   glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
		   lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
		   glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
		   lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);    
		   
		   
	   } catch (NamingException ex) {
		   
		   throw new EJBException(ex.getMessage());
		   
	   }
	   
	   try {
		   glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
	   } catch (FinderException ex) {
		   throw new GlFCNoFunctionalCurrencyFoundException();
	   } catch (Exception ex) {
		   throw new EJBException(ex.getMessage());
	   }
	   
	   try {
		   
		   glFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(), details.getFrDate(), AD_CMPNY);
		   throw new GlFCRFunctionalCurrencyRateAlreadyExistException();
		   
	   } catch (FinderException ex) {
		   
	   } catch (GlFCRFunctionalCurrencyRateAlreadyExistException ex) {
		   
		   throw ex;
		   
	   } catch (Exception ex) {
		   throw new EJBException(ex.getMessage());
	   }
	   
	   
	   
	   try {
		   System.out.println("Rates: " + details.getFrXToUsd());
		   glFunctionalCurrencyRate = glFunctionalCurrencyRateHome.create(
				   details.getFrXToUsd(), details.getFrDate(), AD_CMPNY);
		   System.out.println("Date: " + glFunctionalCurrencyRate.getFrXToUsd());
		   
		   
	   } catch (Exception ex) {
		   getSessionContext().setRollbackOnly();
		   throw new EJBException(ex.getMessage()); 
	   }
	   
	   try {
		   glFunctionalCurrency.addGlFunctionalCurrencyRate(glFunctionalCurrencyRate);
	   } catch (Exception ex) {
		   getSessionContext().setRollbackOnly();
		   throw new EJBException(ex.getMessage());
	   }
	   
   }
   
   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlFcrEntry(GlFunctionalCurrencyRateDetails details, String FC_NM, Integer AD_CMPNY)
      throws GlFCNoFunctionalCurrencyFoundException,
      GlFCRFunctionalCurrencyRateAlreadyExistException,
      GlFCRFunctionalCurrencyRateAlreadyDeletedException {

      Debug.print("GlFunctionalCurrencyRateBean updateGlFcrEntry");
      
      /********************************************************
         Updates a particular functional currency rate
      ********************************************************/
      
      LocalGlFunctionalCurrency glFunctionalCurrency = null;
      LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = null;
      
      LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
      LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
      
      // Initialize EJB Home
        
      try {
            
          glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
          glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);    
              
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      try {
         glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new GlFCNoFunctionalCurrencyFoundException();
      }

      try {
         glFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByPrimaryKey(
	        details.getFrCode());
      } catch (FinderException ex) {
         throw new GlFCRFunctionalCurrencyRateAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      try {
      	
         LocalGlFunctionalCurrencyRate glExistingFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(), details.getFrDate(), AD_CMPNY);
         
         if (!glExistingFunctionalCurrencyRate.getFrCode().equals(glFunctionalCurrencyRate.getFrCode())) {
         	
             throw new GlFCRFunctionalCurrencyRateAlreadyExistException();
             
      	 }
         
      } catch (FinderException ex) {
      	
      } catch (GlFCRFunctionalCurrencyRateAlreadyExistException ex) {
      	  
      	 throw ex;
      	
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
      
      try {
      	
      	glFunctionalCurrencyRate.setFrXToUsd(details.getFrXToUsd());
      	glFunctionalCurrencyRate.setFrDate(details.getFrDate());
      	glFunctionalCurrency.addGlFunctionalCurrencyRate(glFunctionalCurrencyRate);
      	
      	
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	
      }
      
   }
   
   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteGlFcrEntry(Integer FR_CODE, Integer AD_CMPNY)
      throws GlFCRFunctionalCurrencyRateAlreadyDeletedException {
    
      Debug.print("GlFunctionalCurrencyRateBean deleteGlFcrEntry");
      
      /*******************************
         Deletes an existing FCR entry
      *******************************/
      
      LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = null;
      
      LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
      
      // Initialize EJB Home
        
      try {
            
          glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);    
              
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         glFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByPrimaryKey(FR_CODE);
      } catch (FinderException ex) {
         throw new GlFCRFunctionalCurrencyRateAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      try {
         glFunctionalCurrencyRate.remove();
      } catch (RemoveException ex) {
         getSessionContext().setRollbackOnly();
	 throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
      
   }
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlFunctionalCurrencyRateControllerBean ejbCreate");

   }
    
   // private methods

   
}
