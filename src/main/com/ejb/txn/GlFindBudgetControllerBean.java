
/*
 * GlFindBudgetControllerBean.java
 *
 * Created on July 23, 2004, 11:00 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlBudget;
import com.ejb.gl.LocalGlBudgetAmount;
import com.ejb.gl.LocalGlBudgetAmountCoa;
import com.ejb.gl.LocalGlBudgetAmountHome;
import com.ejb.gl.LocalGlBudgetHome;
import com.ejb.gl.LocalGlBudgetOrganization;
import com.ejb.gl.LocalGlBudgetOrganizationHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlModBudgetAmountCoaDetails;
import com.util.GlModBudgetAmountDetails;

/**
 * @ejb:bean name="GlFindBudgetControllerEJB"
 *           display-name="Used for finding budgets"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFindBudgetControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFindBudgetController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFindBudgetControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFindBudgetControllerBean extends AbstractSessionBean {


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlBoAll(Integer AD_CMPNY) {
                    
        Debug.print("GlBudgetEntryControllerBean getGlBoAll");
        
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glBudgetOrganizations = glBudgetOrganizationHome.findBoAll(AD_CMPNY);

	        Iterator i = glBudgetOrganizations.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalGlBudgetOrganization glBudgetOrganization = (LocalGlBudgetOrganization)i.next();

	        	list.add(glBudgetOrganization.getBoName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlBgtAll(Integer AD_CMPNY) {
                    
        Debug.print("GlBudgetEntryControllerBean getGlBgtAll");
        
        LocalGlBudgetHome glBudgetHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glBudgetHome = (LocalGlBudgetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetHome.JNDI_NAME, LocalGlBudgetHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glBudgets = glBudgetHome.findBgtAll(AD_CMPNY);

	        Iterator i = glBudgets.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalGlBudget glBudget = (LocalGlBudget)i.next();

	        	list.add(glBudget.getBgtName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
   
  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlBgaByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_CMPNY)
	  throws GlobalNoRecordFoundException {
	
	  Debug.print("GlFindBudgetControllerBean getGlBgaByCriteria");
	  
	  LocalGlBudgetAmountHome glBudgetAmountHome = null;
	  
	  // Initialize EJB Home
	    
	  try {
	  	
	  	glBudgetAmountHome = (LocalGlBudgetAmountHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlBudgetAmountHome.JNDI_NAME, LocalGlBudgetAmountHome.class);          
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	  
	  try { 
	      
	      ArrayList bgaList = new ArrayList();
	      
	      StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(bga) FROM GlBudgetAmount bga ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
		  Object obj[] = new Object[criteria.size() + 2];
		  
		  if (criteria.containsKey("accountNumber")) {
			  
			  firstArgument = false;

			  jbossQl.append(", In(bga.glBudgetAmountCoas) bc WHERE bc.glChartOfAccount.coaAccountNumber = ?" + (ctr+1) + " ");
			  obj[ctr] = (String)criteria.get("accountNumber");
			  ctr++;

		  } 
		  	
		  if (criteria.containsKey("budgetOrganization")) {

			  if (!firstArgument) {

				  jbossQl.append("AND ");

			  } else {

				  firstArgument = false;
				  jbossQl.append("WHERE ");

			  }

			  jbossQl.append("bga.glBudgetOrganization.boName=?" + (ctr+1) + " ");
			  obj[ctr] = (String)criteria.get("budgetOrganization");
			  ctr++;

		  }	
		  
		  if (criteria.containsKey("budgetName")) {
		  	
		  	if (!firstArgument) {
			  	 
		  	 	jbossQl.append("AND ");
		  	 
		  	 } else {
		  	 
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 
		  	 }
		  	 
		  	 jbossQl.append("bga.glBudget.bgtName=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("budgetName");
		  	 ctr++;
		  	 
		  } 

		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("bga.bgaAdCompany=" + AD_CMPNY + " ");
  	     
	      String orderBy = null;
		      
	      if (ORDER_BY.equals("BUDGET ORGANIZATION")) {
	          
	          orderBy = "bga.glBudgetOrganization.boName";
	          	
	      } else if (ORDER_BY.equals("BUDGET NAME")) {
	      	 
	      	  orderBy = "bga.glBudget.bgtName";
	      	  
	      } 

		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }
    
	      jbossQl.append(" OFFSET ?" + (ctr + 1));
	      obj[ctr] = OFFSET;
	      ctr++;
	      
	      jbossQl.append(" LIMIT ?" + (ctr + 1));
	      obj[ctr] = LIMIT;
	      
	      System.out.println("QL + " + jbossQl);
	
	      Collection glBudgetAmounts = null;
	      
	      try {
	      	
	      	glBudgetAmounts = glBudgetAmountHome.getBgaByCriteria(jbossQl.toString(), obj);
	         
	      } catch (Exception ex) {
	      	
	      	 throw new EJBException(ex.getMessage());
	      	 
	      }
	      
	      if (glBudgetAmounts.isEmpty())
	         throw new GlobalNoRecordFoundException();
	         
	      Iterator i = glBudgetAmounts.iterator();
	      while (i.hasNext()) {
	
	         LocalGlBudgetAmount glBudgetAmount = (LocalGlBudgetAmount) i.next();

		     GlModBudgetAmountDetails mdetails = new GlModBudgetAmountDetails();
		     mdetails.setBgaCode(glBudgetAmount.getBgaCode());
		     mdetails.setBgaBoName(glBudgetAmount.getGlBudgetOrganization().getBoName());
		     mdetails.setBgaBgtName(glBudgetAmount.getGlBudget().getBgtName());

		     ArrayList glBgaBudgetAmountCoaList = new ArrayList();
		     Collection glBudgetAmountCoas = glBudgetAmount.getGlBudgetAmountCoas();       	            

		     Iterator j = glBudgetAmountCoas.iterator();

		     while (j.hasNext()) {

		    	 LocalGlBudgetAmountCoa glBudgetAmountCoa = (LocalGlBudgetAmountCoa)j.next();

		    	 GlModBudgetAmountCoaDetails bcDetails = new GlModBudgetAmountCoaDetails();
		    	 bcDetails.setBcCoaAccountNumber(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
		    	 bcDetails.setBcCoaAccountDescription(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountDescription());
		    	 glBgaBudgetAmountCoaList.add(bcDetails);
		    	 
		     }
		     
		     mdetails.setBgaBudgetAmountCoaList(glBgaBudgetAmountCoaList);
		     
		     bgaList.add(mdetails);
	      	
	      }
	         
	      return bgaList;
  
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getGlBgaSizeByCriteria(HashMap criteria, Integer AD_CMPNY)
 	  throws GlobalNoRecordFoundException {
 	
 	  Debug.print("GlFindBudgetControllerBean getGlBgaSizeByCriteria");
 	  
 	  LocalGlBudgetAmountHome glBudgetAmountHome = null;
 	  
 	  // Initialize EJB Home
 	    
 	  try {
 	  	
 	  	glBudgetAmountHome = (LocalGlBudgetAmountHome)EJBHomeFactory.
 	          lookUpLocalHome(LocalGlBudgetAmountHome.JNDI_NAME, LocalGlBudgetAmountHome.class);          
 	        
 	  } catch (NamingException ex) {
 	        
 	      throw new EJBException(ex.getMessage());
 	        
 	  }
 	  
 	  try { 

 		  StringBuffer jbossQl = new StringBuffer();
 		  jbossQl.append("SELECT OBJECT(bga) FROM GlBudgetAmount bga ");

 		  boolean firstArgument = true;
 		  short ctr = 0;
 		  Object obj[] = new Object[criteria.size()];

 		  if (criteria.containsKey("accountNumber")) {

 			  firstArgument = false;

 			  jbossQl.append(", In(bga.glBudgetAmountCoas) bc WHERE bc.glChartOfAccount.coaAccountNumber = ?" + (ctr+1) + " ");
 			  obj[ctr] = (String)criteria.get("accountNumber");
 			  ctr++;

 		  } 

 		  if (criteria.containsKey("budgetOrganization")) {

 			  if (!firstArgument) {

 				  jbossQl.append("AND ");

 			  } else {

 				  firstArgument = false;
 				  jbossQl.append("WHERE ");

 			  }

 			  jbossQl.append("bga.glBudgetOrganization.boName=?" + (ctr+1) + " ");
 			  obj[ctr] = (String)criteria.get("budgetOrganization");
 			  ctr++;
 		  }	

 		  if (criteria.containsKey("budgetName")) {

 			  if (!firstArgument) {

 				  jbossQl.append("AND ");

 			  } else {

 				  firstArgument = false;
 				  jbossQl.append("WHERE ");

 			  }

 			  jbossQl.append("bga.glBudget.bgtName=?" + (ctr+1) + " ");
 			  obj[ctr] = (String)criteria.get("budgetName");
 			  ctr++;

 		  } 

 		  if (!firstArgument) {

 			  jbossQl.append("AND ");

 		  } else {

 			  firstArgument = false;
 			  jbossQl.append("WHERE ");

 		  }

 		  jbossQl.append("bga.bgaAdCompany=" + AD_CMPNY + " ");

 		  Collection glBudgetAmounts = null;

 		  try {

 			  glBudgetAmounts = glBudgetAmountHome.getBgaByCriteria(jbossQl.toString(), obj);

 		  } catch (Exception ex) {

 			  throw new EJBException(ex.getMessage());

 		  }

 		  if (glBudgetAmounts.isEmpty())
 			  throw new GlobalNoRecordFoundException();

 		  return new Integer(glBudgetAmounts.size());

 	  } catch (GlobalNoRecordFoundException ex) {
 	  	 
 	  	  throw ex;
 	  	
 	  } catch (Exception ex) {
 	  	

 	  	  ex.printStackTrace();
 	  	  throw new EJBException(ex.getMessage());
 	  	
 	  }
         
    }

   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("GlFindBudgetControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlFindBudgetControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
