package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyRateDetails;

/**
 * @ejb:bean name="GlFindDailyRateControllerEJB"
 *           display-name="Used for searching of daily rates"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFindDailyRateControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFindDailyRateController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFindDailyRateControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFindDailyRateControllerBean extends AbstractSessionBean {
	
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlFrByCriteria(HashMap criteria, Integer OFFSET, Integer LIMIT, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("GlFindDailyRateControllerBean getGlFrByCriteria");
      
	  LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
	  
	  ArrayList list = new ArrayList();
	  
	          
      // Initialize EJB Home
	        
	  try {
	        	  
	      glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }      
           
      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(fr) FROM GlFunctionalCurrencyRate fr ");
      
      boolean firstArgument = true;
      short ctr = 0;
      Object obj[] = new Object[criteria.size() + 2];
      
      
      if (criteria.containsKey("frDate")) {
      	 
      	 firstArgument = false;

      	 jbossQl.append("WHERE fr.frDate=?" + (ctr+1) + " ");
      	 obj[ctr] = (Date)criteria.get("frDate");
      	 ctr++;
      	 
      } 
      
      if (criteria.containsKey("fcName")) {
      	
      	 if (!firstArgument) {
      	 	jbossQl.append("AND ");
      	 } else {
      	 	jbossQl.append("WHERE ");
      	 	firstArgument = false;
      	 }
      	 jbossQl.append("fr.glFunctionalCurrency.fcName=?" + (ctr+1));
      	 obj[ctr] = (String)criteria.get("fcName");
      	 ctr++;
      }
      
      if (!firstArgument) {
   	  	
   	     jbossQl.append("AND ");
   	     
   	  } else {
   	  	
   	  	 firstArgument = false;
   	  	 jbossQl.append("WHERE ");
   	  	 
   	  }
   	  
   	  jbossQl.append("fr.frAdCompany=" + AD_CMPNY + " ");
      
      jbossQl.append(" ORDER BY fr.frDate");
                 
      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;
            
      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      ctr++;
                     
      Collection glFunctionalCurrencyRates = null;

      try {
      	
         glFunctionalCurrencyRates = glFunctionalCurrencyRateHome.getFrByCriteria(jbossQl.toString(), obj);
         
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	 
      }
      
      if (glFunctionalCurrencyRates.size() == 0)
         throw new GlobalNoRecordFoundException();
         
      Iterator i = glFunctionalCurrencyRates.iterator();
      
      while (i.hasNext()) {
      	
      	  LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = (LocalGlFunctionalCurrencyRate)i.next();
      	      	  		         
		  GlModFunctionalCurrencyRateDetails mdetails = new GlModFunctionalCurrencyRateDetails(
		  	  glFunctionalCurrencyRate.getFrCode(),
		  	  glFunctionalCurrencyRate.getFrXToUsd(),
		  	  glFunctionalCurrencyRate.getFrDate(),
		  	  glFunctionalCurrencyRate.getGlFunctionalCurrency().getFcName());

		     
		 list.add(mdetails);

         
      }
         
      return list;
  
   }   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getGlFrSizeByCriteria(HashMap criteria, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException {

       Debug.print("GlFindDailyRateControllerBean getGlFrSizeByCriteria");
       
 	  LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
 	  
 	  // Initialize EJB Home
 	        
 	  try {
 	        	  
 	      glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
 	          lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
 	        
 	  } catch (NamingException ex) {
 	        
 	      throw new EJBException(ex.getMessage());
 	        
 	  }      
            
       StringBuffer jbossQl = new StringBuffer();
       jbossQl.append("SELECT OBJECT(fr) FROM GlFunctionalCurrencyRate fr ");
       
       boolean firstArgument = true;
       short ctr = 0;
       Object obj[] = new Object[criteria.size()];
       
       
       if (criteria.containsKey("frDate")) {
       	 
       	 firstArgument = false;

       	 jbossQl.append("WHERE fr.frDate=?" + (ctr+1) + " ");
       	 obj[ctr] = (Date)criteria.get("frDate");
       	 ctr++;
       	 
       } 
       
       if (criteria.containsKey("fcName")) {
       	
       	 if (!firstArgument) {
       	 	jbossQl.append("AND ");
       	 } else {
       	 	jbossQl.append("WHERE ");
       	 	firstArgument = false;
       	 }
       	 jbossQl.append("fr.glFunctionalCurrency.fcName=?" + (ctr+1));
       	 obj[ctr] = (String)criteria.get("fcName");
       	 ctr++;
       }
       
       if (!firstArgument) {
    	  	
    	     jbossQl.append("AND ");
    	     
    	  } else {
    	  	
    	  	 firstArgument = false;
    	  	 jbossQl.append("WHERE ");
    	  	 
    	  }
    	  
       jbossQl.append("fr.frAdCompany=" + AD_CMPNY + " ");
       
       Collection glFunctionalCurrencyRates = null;

       try {
       	
          glFunctionalCurrencyRates = glFunctionalCurrencyRateHome.getFrByCriteria(jbossQl.toString(), obj);
          
       } catch (Exception ex) {
       	
       	 throw new EJBException(ex.getMessage());
       	 
       }
       
       if (glFunctionalCurrencyRates.size() == 0)
          throw new GlobalNoRecordFoundException();
          
       return new Integer(glFunctionalCurrencyRates.size());
   
    } 
   
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlFcAll(Integer AD_CMPNY) {

      Debug.print("GlFindDailyRateControllerBean getGlFcAll");

      
      ArrayList list = new ArrayList();
      Collection glFunctionalCurrencies = null;
      
      LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
      
      // Initialize EJB Home
        
      try {
            
          glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
      	
         glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);
	        
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }

      Iterator i = glFunctionalCurrencies.iterator();
      
      while (i.hasNext()) {
      	
         LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency) i.next();
         
         list.add(glFunctionalCurrency.getFcName());
         
      }

      return list;
   }
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlJournalBatchControllerBean ejbCreate");
      
   }
   
}
