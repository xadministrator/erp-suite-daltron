package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlJournalBatchDetails;

/**
 * @ejb:bean name="GlFindJournalBatchControllerEJB"
 *           display-name="Used for searching journal batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFindJournalBatchControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFindJournalBatchController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFindJournalBatchControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFindJournalBatchControllerBean extends AbstractSessionBean {


   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlJbByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("GlFindJournalBatchControllerBean getGlJbByCriteria");
      
      LocalGlJournalBatchHome glJournalBatchHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
           
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
            
      ArrayList list = new ArrayList();
      
      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(jb) FROM GlJournalBatch jb ");
      
      boolean firstArgument = true;
      short ctr = 0;
      Object obj[];
      
       // Allocate the size of the object parameter

       
      if (criteria.containsKey("batchName")) {
      	
      	 obj = new Object[(criteria.size()-1) + 2];
      	 
      } else {
      	
      	 obj = new Object[criteria.size() + 2];
	
      }
      
  
         
      
      if (criteria.containsKey("batchName")) {
      	
      	 if (!firstArgument) {
      	 
      	    jbossQl.append("AND ");	
      	 	
         } else {
         	
         	firstArgument = false;
         	jbossQl.append("WHERE ");
         	
         }
         
      	 jbossQl.append("jb.jbName LIKE '%" + (String)criteria.get("batchName") + "%' ");
      	 
      }
      	 
        
      if (criteria.containsKey("status")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jb.jbStatus=?" + (ctr+1) + " ");
	  	 obj[ctr] = (String)criteria.get("status");
	  	 ctr++;
	  }  
	  
	  if (criteria.containsKey("dateCreated")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jb.jbDateCreated=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("dateCreated");
	  	 ctr++;
	  } 
	  
	  if (!firstArgument) {
   	  	
   	     jbossQl.append("AND ");
   	     
   	  } else {
   	  	
   	  	 firstArgument = false;
   	  	 jbossQl.append("WHERE ");
   	  	 
   	  }
   	  
   	  jbossQl.append("jb.jbAdBranch=" + AD_BRNCH + " AND jb.jbAdCompany=" + AD_CMPNY + " ");
	      
      String orderBy = null;
	      
	  if (ORDER_BY.equals("BATCH NAME")) {
	
	  	  orderBy = "jb.jbName";
	  	  
	  } else if (ORDER_BY.equals("DATE CREATED")) {
	
	  	  orderBy = "jb.jbDateCreated";
	  	
	  }
	  
	  if (orderBy != null) {
	  
	  	jbossQl.append("ORDER BY " + orderBy);
	  	
	  }
               
      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;
      
      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      
      System.out.println("QL + " + jbossQl);
      
      

      Collection glJournalBatches = null;
      
      try {
      	
         glJournalBatches = glJournalBatchHome.getJbByCriteria(jbossQl.toString(), obj);
         
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	 
      }
      
      if (glJournalBatches.isEmpty())
         throw new GlobalNoRecordFoundException();
         
      Iterator i = glJournalBatches.iterator();
      while (i.hasNext()) {
      	      	
         LocalGlJournalBatch glJournalBatch = (LocalGlJournalBatch) i.next();
         
         GlJournalBatchDetails details = new GlJournalBatchDetails();
         
         details.setJbCode(glJournalBatch.getJbCode());
         details.setJbName(glJournalBatch.getJbName());
         details.setJbDescription(glJournalBatch.getJbDescription());
         details.setJbStatus(glJournalBatch.getJbStatus());
         details.setJbDateCreated(glJournalBatch.getJbDateCreated());
         details.setJbCreatedBy(glJournalBatch.getJbCreatedBy());
         
         list.add(details);
      	
      }
         
      return list;
  
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getGlJbSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException {
    	
    	Debug.print("GlFindJournalBatchControllerBean getGlJbSizeByCriteria");
    	
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	StringBuffer jbossQl = new StringBuffer();
    	jbossQl.append("SELECT OBJECT(jb) FROM GlJournalBatch jb ");
    	
    	boolean firstArgument = true;
    	short ctr = 0;
    	Object obj[];
    	
    	// Allocate the size of the object parameter
    	
    	
    	if (criteria.containsKey("batchName")) {
    		
    		obj = new Object[criteria.size()-1];
    		
    	} else {
    		
    		obj = new Object[criteria.size()];
    		
    	}
    	
    	
    	if (criteria.containsKey("batchName")) {
    		
    		if (!firstArgument) {
    			
    			jbossQl.append("AND ");	
    			
    		} else {
    			
    			firstArgument = false;
    			jbossQl.append("WHERE ");
    			
    		}
    		
    		jbossQl.append("jb.jbName LIKE '%" + (String)criteria.get("batchName") + "%' ");
    		
    	}
    	
    	
    	if (criteria.containsKey("status")) {
    		
    		if (!firstArgument) {
    			jbossQl.append("AND ");
    		} else {
    			firstArgument = false;
    			jbossQl.append("WHERE ");
    		}
    		jbossQl.append("jb.jbStatus=?" + (ctr+1) + " ");
    		obj[ctr] = (String)criteria.get("status");
    		ctr++;
    	}  
    	
    	if (criteria.containsKey("dateCreated")) {
    		
    		if (!firstArgument) {
    			jbossQl.append("AND ");
    		} else {
    			firstArgument = false;
    			jbossQl.append("WHERE ");
    		}
    		jbossQl.append("jb.jbDateCreated=?" + (ctr+1) + " ");
    		obj[ctr] = (Date)criteria.get("dateCreated");
    		ctr++;
    	} 
    	
    	if (!firstArgument) {
    		
    		jbossQl.append("AND ");
    		
    	} else {
    		
    		firstArgument = false;
    		jbossQl.append("WHERE ");
    		
    	}
    	
    	jbossQl.append("jb.jbAdBranch=" + AD_BRNCH + " AND jb.jbAdCompany=" + AD_CMPNY + " ");
    	
    	Collection glJournalBatches = null;
    	
    	try {
    		
    		glJournalBatches = glJournalBatchHome.getJbByCriteria(jbossQl.toString(), obj);
    		
    	} catch (Exception ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	if (glJournalBatches.isEmpty())
    		throw new GlobalNoRecordFoundException();
    	
    	return new Integer(glJournalBatches.size());
    	
}
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlJournalBatchControllerBean ejbCreate");
      
   }

   // private methods

     
}
