
/*
 * GlFindJournalInterfaceControllerBean.java
 *
 * Created on March 31, 2004, 9:07 AM
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlJournalInterface;
import com.ejb.gl.LocalGlJournalInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlJournalInterfaceDetails;

/**
 * @ejb:bean name="GlFindJournalInterfaceControllerEJB"
 *           display-name="Used for finding journal interfaces"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFindJournalInterfaceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFindJournalInterfaceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFindJournalInterfaceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFindJournalInterfaceControllerBean extends AbstractSessionBean {
    
    
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public ArrayList getGlJriByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_CMPNY) 
       throws GlobalNoRecordFoundException {

       Debug.print("GlFindJournalInterfaceControllerBean getGlJriByCriteria");
       
       LocalGlJournalInterfaceHome glJournalInterfaceHome = null;
       LocalGlJournalInterface glJournalInterface = null;
       Collection glJournalInterfaces = null;
       
       ArrayList list = new ArrayList();
       
       // Initialize EJB Home
      
       try {
            
           glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
   
       try {
               
          StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(jri) FROM GlJournalInterface jri ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
	      int x = 0;	      
	      
		  if (criteria.containsKey("jriName")) {
		  			     
		     x++;
		     
          }
		     
		  if (criteria.containsKey("jriDescription")) {
		  	
		  	 x++;
		  			  	 
		  }
		  
		  if (criteria.containsKey("jriJournalCategory")) {
		  	
		  	 x++;
		  	 
          }
          
          if (criteria.containsKey("jriJournalSource")) {
          	
          	 x++;
          	 
          }
          
          if (criteria.containsKey("jriFunctionalCurrency")) {
          	
          	 x++;
          	 
          }
          
          Object obj[] = new Object[(criteria.size()-x) + 2];
	      	      
	      if (criteria.containsKey("jriName")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriName LIKE '%" + (String)criteria.get("jriName") + "%' ");
	         firstArgument = false;	 
	         
	      }	      

	      if (criteria.containsKey("jriDescription")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
             } else {
             	
             	firstArgument = false;
             	jbossQl.append("WHERE ");
             }
             
             jbossQl.append("jri.jriDescription LIKE '%" + (String)criteria.get("jriDescription") + "%' ");
             firstArgument = false;
             
          }		      	 		      	 

	      if (criteria.containsKey("jriEffectiveDate")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriEffectiveDate=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Date)criteria.get("jriEffectiveDate");
	      	 ctr++;
	      	 
	      }	                

	      if (criteria.containsKey("jriJournalCategory")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriJournalCategory LIKE '%" + (String)criteria.get("jriJournalCategory") + "%' ");
             firstArgument = false;
             
	      }	      

	      if (criteria.containsKey("jriJournalSource")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriJournalSource LIKE '%" + (String)criteria.get("jriJournalSource") + "%' ");
             firstArgument = false;
             
	      }	      

	      if (criteria.containsKey("jriFunctionalCurrency")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;	      	 	
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriFunctionalCurrency LIKE '%" + (String)criteria.get("jriFunctionalCurrency") + "%' ");
             firstArgument = false;
             
	      }	      

	      if (criteria.containsKey("jriDateReversal")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriDateReversal=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Date)criteria.get("jriDateReversal");
	      	 ctr++;
	      	 
	      }	      

	      if (criteria.containsKey("jriDocumentNumber")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriDocumentNumber=?" + (ctr+1) + " ");
	      	 obj[ctr] = (String)criteria.get("jriDocumentNumber");
	      	 ctr++;
	      	 
	      }	      

	      if (criteria.containsKey("jriConversionDate")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriConversionDate=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Date)criteria.get("jriConversionDate");
	      	 ctr++;
	      	 
	      }	      

	      if (criteria.containsKey("jriConversionRate")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriConversionRate=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Double)criteria.get("jriConversionRate");
	      	 ctr++;
	      	 
	      }	      

	      if (criteria.containsKey("jriReversed")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriReversed=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Byte)criteria.get("jriReversed");
	      	 ctr++;
	      	 
	      }	  
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jri.jriAdCompany=" + AD_CMPNY + " ");

	      String orderBy = null;
	      
	      if (ORDER_BY.equals("NAME")) {

	      	  orderBy = "jri.jriName";
	      	  	      	      
	          jbossQl.append("ORDER BY " + orderBy + ", jri.jriEffectiveDate");
	      
	      } else {
	      	
          jbossQl.append("ORDER BY jri.jriEffectiveDate");
          
          }	      	
	      		      	
	      jbossQl.append(" OFFSET ?" + (ctr + 1));	        
	      obj[ctr] = OFFSET;	      
	      ctr++;

	      jbossQl.append(" LIMIT ?" + (ctr + 1));
	      obj[ctr] = LIMIT;
	      ctr++;

          glJournalInterfaces = glJournalInterfaceHome.getJriByCriteria(jbossQl.toString(), obj);	         

	      if (glJournalInterfaces.size() == 0)
	         throw new GlobalNoRecordFoundException();
       	
       	   Iterator i = glJournalInterfaces.iterator();
       	   
       	   while (i.hasNext()) {
       	   	
       	      glJournalInterface = (LocalGlJournalInterface)i.next();
       	      
      	      GlJournalInterfaceDetails details = new GlJournalInterfaceDetails();
              details.setJriCode(glJournalInterface.getJriCode());
              details.setJriName(glJournalInterface.getJriName());
              details.setJriDescription(glJournalInterface.getJriDescription());
              details.setJriDocumentNumber(glJournalInterface.getJriDocumentNumber());
              details.setJriEffectiveDate(glJournalInterface.getJriEffectiveDate());
              details.setJriJournalCategory(glJournalInterface.getJriJournalCategory());
              details.setJriJournalSource(glJournalInterface.getJriJournalSource());
           
              list.add(details);
       	   	
       	   }
       	
       return list;
       
	  } catch (GlobalNoRecordFoundException ex) {
	  	
	  	  throw ex;
	  	
	  } catch (Exception ex) {

	  	  throw new EJBException(ex.getMessage());
	  	
	  }
  
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public Integer getGlJriSizeByCriteria(HashMap criteria, Integer AD_CMPNY) 
       throws GlobalNoRecordFoundException {

       Debug.print("GlFindJournalInterfaceControllerBean getGlJriSizeByCriteria");
       
       LocalGlJournalInterfaceHome glJournalInterfaceHome = null;
       LocalGlJournalInterface glJournalInterface = null;
       Collection glJournalInterfaces = null;
       
       ArrayList list = new ArrayList();
       
       // Initialize EJB Home
      
       try {
            
           glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
   
       try {
               
          StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(jri) FROM GlJournalInterface jri ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
	      int x = 0;	      
	      
		  if (criteria.containsKey("jriName")) {
		  			     
		     x++;
		     
          }
		     
		  if (criteria.containsKey("jriDescription")) {
		  	
		  	 x++;
		  			  	 
		  }
		  
		  if (criteria.containsKey("jriJournalCategory")) {
		  	
		  	 x++;
		  	 
          }
          
          if (criteria.containsKey("jriJournalSource")) {
          	
          	 x++;
          	 
          }
          
          if (criteria.containsKey("jriFunctionalCurrency")) {
          	
          	 x++;
          	 
          }
          
          Object obj[] = new Object[criteria.size()-x];
	      	      
	      if (criteria.containsKey("jriName")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriName LIKE '%" + (String)criteria.get("jriName") + "%' ");
	         firstArgument = false;	 
	         
	      }	      

	      if (criteria.containsKey("jriDescription")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
             } else {
             	
             	firstArgument = false;
             	jbossQl.append("WHERE ");
             }
             
             jbossQl.append("jri.jriDescription LIKE '%" + (String)criteria.get("jriDescription") + "%' ");
             firstArgument = false;
             
          }		      	 		      	 

	      if (criteria.containsKey("jriEffectiveDate")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriEffectiveDate=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Date)criteria.get("jriEffectiveDate");
	      	 ctr++;
	      	 
	      }	                

	      if (criteria.containsKey("jriJournalCategory")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriJournalCategory LIKE '%" + (String)criteria.get("jriJournalCategory") + "%' ");
             firstArgument = false;
             
	      }	      

	      if (criteria.containsKey("jriJournalSource")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriJournalSource LIKE '%" + (String)criteria.get("jriJournalSource") + "%' ");
             firstArgument = false;
             
	      }	      

	      if (criteria.containsKey("jriFunctionalCurrency")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;	      	 	
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriFunctionalCurrency LIKE '%" + (String)criteria.get("jriFunctionalCurrency") + "%' ");
             firstArgument = false;
             
	      }	      

	      if (criteria.containsKey("jriDateReversal")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriDateReversal=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Date)criteria.get("jriDateReversal");
	      	 ctr++;
	      	 
	      }	      

	      if (criteria.containsKey("jriDocumentNumber")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriDocumentNumber=?" + (ctr+1) + " ");
	      	 obj[ctr] = (String)criteria.get("jriDocumentNumber");
	      	 ctr++;
	      	 
	      }	      

	      if (criteria.containsKey("jriConversionDate")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriConversionDate=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Date)criteria.get("jriConversionDate");
	      	 ctr++;
	      	 
	      }	      

	      if (criteria.containsKey("jriConversionRate")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriConversionRate=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Double)criteria.get("jriConversionRate");
	      	 ctr++;
	      	 
	      }	      

	      if (criteria.containsKey("jriReversed")) {

	      	 if (!firstArgument) {
	      	 	
	      	 	jbossQl.append("AND ");
	      	 	
	      	 } else {
	      	 	
	      	 	firstArgument = false;
	      	 	jbossQl.append("WHERE ");
	      	 	
	      	 }
	      	 
	      	 jbossQl.append("jri.jriReversed=?" + (ctr+1) + " ");
	      	 obj[ctr] = (Byte)criteria.get("jriReversed");
	      	 ctr++;
	      	 
	      }	  
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jri.jriAdCompany=" + AD_CMPNY + " ");

       	  
       	  	
       	  glJournalInterfaces = glJournalInterfaceHome.getJriByCriteria(jbossQl.toString(), obj);
       	  		
       	  if (glJournalInterfaces.size() == 0)
	         throw new GlobalNoRecordFoundException();
       	
       	  return new Integer(glJournalInterfaces.size()); 
       
	  } catch (GlobalNoRecordFoundException ex) {
	  	
	  	  throw ex;
	  	
	  } catch (Exception ex) {

	  	  throw new EJBException(ex.getMessage());
	  	
	  }
  
   }
   
    // private methods
        
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlFindJournalInterfaceControllerBean ejbCreate");
      
    }
}
