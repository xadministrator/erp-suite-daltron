package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlRecurringJournal;
import com.ejb.gl.LocalGlRecurringJournalHome;
import com.ejb.gl.LocalGlRecurringJournalLine;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModRecurringJournalDetails;

/**
 * @ejb:bean name="GlFindRecurringJournalControllerEJB"
 *           display-name="Used for searching recurring journals"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFindRecurringJournalControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFindRecurringJournalController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFindRecurringJournalControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFindRecurringJournalControllerBean extends AbstractSessionBean {


   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlJcAll(Integer AD_CMPNY) {
                    
        Debug.print("GlFindRecurringJournalControllerBean getGlJcAll");
        
        LocalGlJournalCategoryHome glJournalCategoryHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalCategories = glJournalCategoryHome.findJcAll(AD_CMPNY);
        	
        	Iterator i = glJournalCategories.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalCategory glJournalCategory = (LocalGlJournalCategory)i.next();
        		
        		list.add(glJournalCategory.getJcName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
   
  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlRjByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("GlFindRecurringJournalControllerBean getGlJrPostableByCriteria");
      
      LocalGlRecurringJournalHome glRecurringJournalHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          glRecurringJournalHome = (LocalGlRecurringJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlRecurringJournalHome.JNDI_NAME, LocalGlRecurringJournalHome.class);          
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      
      ArrayList rjList = new ArrayList();
      
      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(rj) FROM GlRecurringJournal rj ");
      
      boolean firstArgument = true;
      short ctr = 0;
      int criteriaSize = criteria.size() + 2;
      Object obj[];
      
       // Allocate the size of the object parameter

       
      if (criteria.containsKey("name")) {
      	
      	  criteriaSize--; 
      	 
      } 
            
      
      obj = new Object[criteriaSize];
         
      
      if (criteria.containsKey("name")) {
      	
      	 if (!firstArgument) {
      	 
      	    jbossQl.append("AND ");	
      	 	
         } else {
         	
         	firstArgument = false;
         	jbossQl.append("WHERE ");
         	
         }
         
      	 jbossQl.append("rj.rjName LIKE '%" + (String)criteria.get("name") + "%' ");
      	 
      }            	 
        
      if (criteria.containsKey("nextRunDateFrom")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("rj.rjNextRunDate>=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("nextRunDateFrom");
	  	 ctr++;
	  }  
	      
	  if (criteria.containsKey("nextRunDateTo")) {
	  	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("rj.rjNextRunDate<=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("nextRunDateTo");
	  	 ctr++;
	  	 
	  }    
	  	  	  
	  if (criteria.containsKey("category")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("rj.glJournalCategory.jcName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("category");
       	  ctr++;
       	  
      }   
	  
	  if (!firstArgument) {
   	  	
   	     jbossQl.append("AND ");
   	     
   	  } else {
   	  	
   	  	 firstArgument = false;
   	  	 jbossQl.append("WHERE ");
   	  	 
   	  }
   	  
   	  jbossQl.append("rj.rjAdBranch=" + AD_BRNCH + " AND rj.rjAdCompany=" + AD_CMPNY + " ");
              
             	     
      String orderBy = null;
	      
	  if (ORDER_BY.equals("NAME")) {
	
	  	  orderBy = "rj.rjName";
	  	  
	  } else if (ORDER_BY.equals("NEXT RUN DATE")) {
	
	  	  orderBy = "rj.rjNextRunDate";
	  	
	  } else if (ORDER_BY.equals("CATEGORY")) {
	
	  	  orderBy = "rj.glJournalCategory.jcName";
	  	  
	  } 
	  
	  if (orderBy != null) {
	  
	  	jbossQl.append("ORDER BY " + orderBy);
	  	
	  } 
	  
               
      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;
      
      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      
      System.out.println("QL + " + jbossQl);
      
      

      Collection glRecurringJournals = null;
      
      double TOTAL_DEBIT = 0;
      double TOTAL_CREDIT = 0;
      
      try {
      	
         glRecurringJournals = glRecurringJournalHome.getRjByCriteria(jbossQl.toString(), obj);
         
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	 
      }
      
      if (glRecurringJournals.isEmpty())
         throw new GlobalNoRecordFoundException();
         
      Iterator i = glRecurringJournals.iterator();
      while (i.hasNext()) {
      	
      	 TOTAL_DEBIT = 0d;
      	 TOTAL_CREDIT = 0d;
      	
         LocalGlRecurringJournal glRecurringJournal = (LocalGlRecurringJournal) i.next();
      	 Collection glRecurringJournalLines = glRecurringJournal.getGlRecurringJournalLines();
      	 
      	 Iterator j = glRecurringJournalLines.iterator();
      	 while (j.hasNext()) {
      	 	
      	 	LocalGlRecurringJournalLine glRecurringJournalLine = (LocalGlRecurringJournalLine) j.next();
      	 	
	        if (glRecurringJournalLine.getRjlDebit() == EJBCommon.TRUE) {
	     	
	           TOTAL_DEBIT += glRecurringJournalLine.getRjlAmount();
	        
	        } else {
	     	
	           TOTAL_CREDIT += glRecurringJournalLine.getRjlAmount();
	        }
	        
	     }
	     	     	     
	     GlModRecurringJournalDetails mdetails = new GlModRecurringJournalDetails();
	     mdetails.setRjCode(glRecurringJournal.getRjCode());
	     mdetails.setRjName(glRecurringJournal.getRjName());
	     mdetails.setRjDescription(glRecurringJournal.getRjDescription());
	     mdetails.setRjNextRunDate(glRecurringJournal.getRjNextRunDate());
	     mdetails.setRjSchedule(glRecurringJournal.getRjSchedule());
	     mdetails.setRjLastRunDate(glRecurringJournal.getRjLastRunDate());
	     mdetails.setRjTotalDebit(TOTAL_DEBIT);
	     mdetails.setRjTotalCredit(TOTAL_CREDIT);
	     mdetails.setRjJcName(glRecurringJournal.getGlJournalCategory().getJcName());	          
	           	  
      	 rjList.add(mdetails);
      	
      }
         
      return rjList;
  
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
   **/
   public Integer getGlRjSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("GlFindRecurringJournalControllerBean getGlRjSizeByCriteria");
      
      LocalGlRecurringJournalHome glRecurringJournalHome = null;
       
      // Initialize EJB Home
         
      try {
       	
           glRecurringJournalHome = (LocalGlRecurringJournalHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlRecurringJournalHome.JNDI_NAME, LocalGlRecurringJournalHome.class);          
             
      } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
             
      }

      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(rj) FROM GlRecurringJournal rj ");
       
      boolean firstArgument = true;
	       short ctr = 0;
	       int criteriaSize = criteria.size();
	       Object obj[];
       
      // Allocate the size of the object parameter

      if (criteria.containsKey("name")) {
       	
       	  criteriaSize--; 
       	 
      } 
             
       
      obj = new Object[criteriaSize];
          
       
      if (criteria.containsKey("name")) {
       	
       	 if (!firstArgument) {
       	 
       	    jbossQl.append("AND ");	
       	 	
          } else {
          	
          	firstArgument = false;
          	jbossQl.append("WHERE ");
          	
          }
          
       	 jbossQl.append("rj.rjName LIKE '%" + (String)criteria.get("name") + "%' ");
       	 
      }            	 
         
      if (criteria.containsKey("nextRunDateFrom")) {
 	      	
 	  	 if (!firstArgument) {
 	  	 	jbossQl.append("AND ");
 	  	 } else {
 	  	 	firstArgument = false;
 	  	 	jbossQl.append("WHERE ");
 	  	 }
 	  	 jbossQl.append("rj.rjNextRunDate>=?" + (ctr+1) + " ");
 	  	 obj[ctr] = (Date)criteria.get("nextRunDateFrom");
 	  	 ctr++;
 	  }  
 	      
 	  if (criteria.containsKey("nextRunDateTo")) {
 	  	
 	  	 if (!firstArgument) {
 	  	 	jbossQl.append("AND ");
 	  	 } else {
 	  	 	firstArgument = false;
 	  	 	jbossQl.append("WHERE ");
 	  	 }
 	  	 jbossQl.append("rj.rjNextRunDate<=?" + (ctr+1) + " ");
 	  	 obj[ctr] = (Date)criteria.get("nextRunDateTo");
 	  	 ctr++;
 	  	 
 	  }    
 	  	  	  
 	  if (criteria.containsKey("category")) {
 	  	
 	  	if (!firstArgument) {
 	  		
 	  		jbossQl.append("AND ");
 	  		
 	  	} else {
 	  		
 	  		firstArgument = false;
 	  		jbossQl.append("WHERE ");
 	  		
 	  	}
 	  	
 	  	jbossQl.append("rj.glJournalCategory.jcName=?" + (ctr+1) + " ");
 	  	obj[ctr] = (String)criteria.get("category");
 	  	ctr++;
 	  	
 	  }   
 	  
 	  if (!firstArgument) {
 	  	
 	  	jbossQl.append("AND ");
 	  	
 	  } else {
 	  	
 	  	firstArgument = false;
 	  	jbossQl.append("WHERE ");
 	  	
 	  }
    	  
 	  jbossQl.append("rj.rjAdBranch=" + AD_BRNCH + " AND rj.rjAdCompany=" + AD_CMPNY + " ");
               
      Collection glRecurringJournals = null;
       
       
      try {
       	
      		glRecurringJournals = glRecurringJournalHome.getRjByCriteria(jbossQl.toString(), obj);
          
      } catch (Exception ex) {
       	
       	 	throw new EJBException(ex.getMessage());
       	 
      }
       
      if (glRecurringJournals.isEmpty())
          	throw new GlobalNoRecordFoundException();
          
      
      return new Integer(glRecurringJournals.size());
   
    }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("GlFindRecurringJournalControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlJournalBatchControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
