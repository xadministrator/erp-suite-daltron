package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GenVSVNoValueSetValueFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenFieldHome;
import com.ejb.genfld.LocalGenQualifier;
import com.ejb.genfld.LocalGenQualifierHome;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSet;
import com.ejb.genfld.LocalGenValueSetHome;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GenModValueSetDetails;
import com.util.GenModValueSetValueDetails;

/**
 * @ejb:bean name="GlFindSegmentControllerEJB"
 *           display-name="Used for searching of segments"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFindSegmentControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFindSegmentController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFindSegmentControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
 */

public class GlFindSegmentControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	
	public ArrayList getGlValueSetValueByCriteria(HashMap criteria, ArrayList vsvDescList,
			short selectedCoaSegmentNumber, ArrayList adBrnchList, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {
		
		Debug.print("GlFindSegmentControllerBean getGlValueSetValueByCriteria");
		/*
		LocalAdCompanyHome adCompanyHome = null;
		LocalGenFieldHome genFieldHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		
		LocalAdCompany adCompany = null;
		LocalGenField genField = null;
		ArrayList list = new ArrayList();
		
		Collection genSegments = null;
		char chrSeparator;
		String strSeparator = null;
		int genNumberOfSegment = 0;

		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genFieldHome = (LocalGenFieldHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenFieldHome.JNDI_NAME, LocalGenFieldHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);

		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}

		try {
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		} catch (Exception ex) {
			throw new EJBException(ex.getMessage());
		}
		
		try {
			
			genField = adCompany.getGenField();
			genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
			chrSeparator = genField.getFlSegmentSeparator();
			strSeparator =  String.valueOf(chrSeparator);      
			genNumberOfSegment = genField.getFlNumberOfSegment();

		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}	
		
		try {

			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(coa) FROM GlChartOfAccount coa ");
			
			boolean firstArgument = true;
			short ctr = 0;
			Object obj[] = new Object[criteria.size()];      
			
			if(adBrnchList.isEmpty()) {
				
				jbossQl.append("WHERE ");
				
			} else {

				jbossQl.append(", in (coa.adBranchCoas) bcoa WHERE bcoa.adBranch.brCode in (");
				
				boolean firstLoop = true;
				
				Iterator i = adBrnchList.iterator();
				
				while(i.hasNext()) {
					
					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}
					
					AdBranchDetails mdetails = (AdBranchDetails) i.next();
					
					jbossQl.append(mdetails.getBrCode());
					
				}
				
				jbossQl.append(") ");
				
				firstArgument = false;
				
			}                             

			if (!vsvDescList.isEmpty()) {	      
				
				Iterator i = vsvDescList.iterator();
				
				while (i.hasNext()) {
					
					GenModValueSetValueDetails mdetails = (GenModValueSetValueDetails)i.next();		 	 				 				 		
					
					
					Collection genValueSetValues = null;
					
					try {
						
						genValueSetValues = genValueSetValueHome.findByVsvDescriptionAndVsName(mdetails.getVsvDescription(), mdetails.getVsvVsName(), AD_CMPNY);
						
					} catch (Exception ex) {
						
						throw new EJBException(ex.getMessage());
						
					}
					
					if (genValueSetValues.isEmpty()) {		 				 		
						
						throw new GlobalNoRecordFoundException();			 		
						
					}
					
					if (!firstArgument) {
						jbossQl.append("AND (");
					} else {
						jbossQl.append("(");
						firstArgument = false;
					}
					
					
					Iterator j = genValueSetValues.iterator();
					
					while (j.hasNext()) {
						
						
						LocalGenValueSetValue genValueSetValue = (LocalGenValueSetValue)j.next();		 		
						
						int genSegmentNumber = 0;
						
						try {
							
							LocalGenSegment genSegment = genSegmentHome.findByVsCode(genValueSetValue.getGenValueSet().getVsCode(), AD_CMPNY);
							genSegmentNumber = genSegment.getSgSegmentNumber();
							
						} catch (Exception ex) {
							
							throw new EJBException(ex.getMessage());
							
						}
						
						String var = "1";		 		
						
						for (int k=0; k<genSegmentNumber-1; k++) {
							
							
							var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";
							
							
						}
						
						if (genNumberOfSegment != genSegmentNumber) {
							
							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")) - (" + var +")) = '" + genValueSetValue.getVsvValue() + "' ");
							
						} else if (genNumberOfSegment == genSegmentNumber) {
							
							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) = '" + genValueSetValue.getVsvValue() + "' ");
						}	
						
						if (j.hasNext()) {
							
							jbossQl.append("OR ");
							
						} 		
						
					}
					
					jbossQl.append(") ");
					
					
				}
				
			}

			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				
			}
			
			jbossQl.append("coa.coaAdCompany=" + AD_CMPNY + " ");
			
			jbossQl.append("ORDER BY coa.coaSegment" + selectedCoaSegmentNumber + ", coa.coaAccountNumber ");
			
			System.out.println(jbossQl.toString());	                     
			Collection glChartOfAccounts = null;
			
			try {
				
				glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), obj);
				
			} catch (Exception ex) {
				
				throw new EJBException(ex.getMessage());
				
			}
			
			if (glChartOfAccounts.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			Iterator i = glChartOfAccounts.iterator();
			
			while (i.hasNext()) {
				
				LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)i.next();
				
				LocalGenSegment genSegment = genSegmentHome.findByFlCodeAndSgSegmentNumber(adCompany.getGenField().getFlCode(), (short)selectedCoaSegmentNumber, AD_CMPNY);
				
				LocalGenValueSetValue genValueSetValue = null;
				
				try {
					
					switch (selectedCoaSegmentNumber) {
					
					case 1:
						genValueSetValue = genValueSetValueHome.findByVsvValueAndVsName(glChartOfAccount.getCoaSegment1(), genSegment.getGenValueSet().getVsName(), AD_CMPNY);
						
						break;
					case 2:
						genValueSetValue = genValueSetValueHome.findByVsvValueAndVsName(glChartOfAccount.getCoaSegment2(), genSegment.getGenValueSet().getVsName(), AD_CMPNY);
						break;
					case 3:
						genValueSetValue = genValueSetValueHome.findByVsvValueAndVsName(glChartOfAccount.getCoaSegment3(), genSegment.getGenValueSet().getVsName(), AD_CMPNY);
						break;	
					case 4:
						genValueSetValue = genValueSetValueHome.findByVsvValueAndVsName(glChartOfAccount.getCoaSegment4(), genSegment.getGenValueSet().getVsName(), AD_CMPNY);
						break;
					case 5:
						genValueSetValue = genValueSetValueHome.findByVsvValueAndVsName(glChartOfAccount.getCoaSegment5(), genSegment.getGenValueSet().getVsName(), AD_CMPNY);
						break;
					case 6:
						genValueSetValue = genValueSetValueHome.findByVsvValueAndVsName(glChartOfAccount.getCoaSegment6(), genSegment.getGenValueSet().getVsName(), AD_CMPNY);
						break;
					case 7:
						genValueSetValue = genValueSetValueHome.findByVsvValueAndVsName(glChartOfAccount.getCoaSegment7(), genSegment.getGenValueSet().getVsName(), AD_CMPNY);
						break;
					case 8:
						genValueSetValue = genValueSetValueHome.findByVsvValueAndVsName(glChartOfAccount.getCoaSegment8(), genSegment.getGenValueSet().getVsName(), AD_CMPNY);
						break;
					case 9:
						genValueSetValue = genValueSetValueHome.findByVsvValueAndVsName(glChartOfAccount.getCoaSegment9(), genSegment.getGenValueSet().getVsName(), AD_CMPNY);
						break;
					case 10:
						genValueSetValue = genValueSetValueHome.findByVsvValueAndVsName(glChartOfAccount.getCoaSegment10(), genSegment.getGenValueSet().getVsName(), AD_CMPNY);
						break;
					default:
						break;
					}	     
					
				} catch(FinderException ex) {
					
				}
				
				if(genValueSetValue == null || this.containsSegment(list, genValueSetValue.getVsvValue())) {
					
					continue;
					
				}
				
				GenModValueSetValueDetails vsvDetails = new GenModValueSetValueDetails();		      
				vsvDetails.setVsvValue(genValueSetValue.getVsvValue());
				vsvDetails.setVsvDescription(genValueSetValue.getVsvDescription());
				
				list.add(vsvDetails);
				
				
			}
			
			if (list.isEmpty())
				throw new GlobalNoRecordFoundException();
			
		
		
		    
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}*/
		
		  LocalGenValueSetValueHome genValueSetValueHome = null;
	      LocalGenSegmentHome genSegmentHome = null;
	      LocalAdCompanyHome adCompanyHome = null;
	      
	      // Initialize EJB Home
	        
	      try {
	            
	          genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
	             lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
	          genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
	             lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
	          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	             lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	            
	      } catch (NamingException ex) {
	            
	          throw new EJBException(ex.getMessage());
	            
	      }

	      try {
	    	  
	      
		      ArrayList list = new ArrayList();
		      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
		      LocalGenField genField = adCompany.getGenField();
		      LocalGenSegment genSegment = genSegmentHome.findByFlCodeAndSgSegmentNumber(genField.getFlCode(), selectedCoaSegmentNumber, AD_CMPNY);
		      Collection genValueSetValues = null;
		      if (vsvDescList.isEmpty()) {
		    	  
		    	  genValueSetValues = genValueSetValueHome.findByVsName(genSegment.getGenValueSet().getVsName(), AD_CMPNY);
		    	  
		      } else {
		    	  
		    	  Iterator vsvIter = vsvDescList.iterator();
		    	  String vsvDescription = null;
		    	  while (vsvIter.hasNext()) {
		    		  
		    		  GenModValueSetValueDetails genVsvDetails = (GenModValueSetValueDetails)vsvIter.next();  
		    		  
		    		  if (genVsvDetails.getVsvVsName().equals(genSegment.getGenValueSet().getVsName())) {
		    			  
		    			  vsvDescription = genVsvDetails.getVsvDescription();
		    			  break;
		    		  }
		    	  }
		    	  
		    	  if (vsvDescription != null){
                               System.out.println("pass 1 : desc " + vsvDescription);
                               System.out.println("pass 1 : name " + genSegment.getGenValueSet().getVsName());
		    		  genValueSetValues = genValueSetValueHome.findByVsvDescriptionAndVsName(vsvDescription, genSegment.getGenValueSet().getVsName(), AD_CMPNY);
		    	 
                          }
                          else{
                                System.out.println("pass 2 : " + vsvDescription);
		    		  genValueSetValues = genValueSetValueHome.findByVsName(genSegment.getGenValueSet().getVsName(), AD_CMPNY);
		          
                          }
                           
		      }
		      
	          
		      if (genValueSetValues.size() == 0) {
		      	
		         throw new GlobalNoRecordFoundException();
		      
		      }
		 
		      Iterator i = genValueSetValues.iterator();
		      
		      while (i.hasNext()) {
		      	
		         LocalGenValueSetValue genValueSetValue = (LocalGenValueSetValue) i.next();
			       
				 GenModValueSetValueDetails mdetails = new GenModValueSetValueDetails();				 
				 
				 	mdetails.setVsvValue(genValueSetValue.getVsvValue()); 		 
				 	mdetails.setVsvDescription(genValueSetValue.getVsvDescription());
				 	
				    		 	
			       list.add(mdetails);
		      
		      }
			      
		      return list;
		      
	      } catch (GlobalNoRecordFoundException ex) {
				
				throw ex;
		      
	      } catch (Exception ex) {
	    	  
	    	  ex.printStackTrace();
		      throw new EJBException(ex.getMessage());
	    	  
	      }
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGenVsAll(Integer AD_CMPNY) {
		
		Debug.print("GlCoaGeneratorControllerBean getGenVsAll");
		
		LocalGenValueSetHome genValueSetHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			genValueSetHome = (LocalGenValueSetHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetHome.JNDI_NAME, LocalGenValueSetHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection genValueSets = genValueSetHome.findVsAll(AD_CMPNY);
			
			Iterator i = genValueSets.iterator();
			
			while (i.hasNext()) {
				
				LocalGenValueSet genValueSet = (LocalGenValueSet)i.next();
				LocalGenSegment genSegment = genSegmentHome.findByVsCode(genValueSet.getVsCode(), AD_CMPNY);
				
				GenModValueSetDetails mdetails = new GenModValueSetDetails();
				mdetails.setVsName(genValueSet.getVsName());
				mdetails.setVsSegmentNumber(genSegment.getSgSegmentNumber());
				
				if(genSegment.getSgSegmentType() == 'N') {
					
					mdetails.setVsSgNatural(EJBCommon.TRUE);
					
				} else {
					
					mdetails.setVsSgNatural(EJBCommon.FALSE);
					
				}
				
				list.add(mdetails);
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	
	public ArrayList getGenVsvAllByVsNameAndVsvDesc(String VS_NM, String VSV_DESC, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {
		
		Debug.print("GlFindSegmentControllerBean getGenVsvAllByName");
		
		LocalGenValueSetValueHome genValueSetValueHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection genValueSetValues = null;
			
			try {
				
				genValueSetValues = genValueSetValueHome.findByVsvDescriptionAndVsName(VSV_DESC, VS_NM, AD_CMPNY);
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			if(genValueSetValues.isEmpty()){
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			Iterator i = genValueSetValues.iterator();
			
			while (i.hasNext()) {
				
				LocalGenValueSetValue genValueSetValue = (LocalGenValueSetValue)i.next();
				
				
				GenModValueSetValueDetails details = new GenModValueSetValueDetails();
				
				details.setVsvValue(genValueSetValue.getVsvValue());
				details.setVsvDescription(genValueSetValue.getVsvDescription());
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex){

			throw ex;

		} catch (Exception ex){
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	//private method
	
	private boolean containsSegment(ArrayList list, String vsvValue) {
		
		Iterator i = list.iterator();
		
		while(i.hasNext()) {
			
			GenModValueSetValueDetails details = (GenModValueSetValueDetails)i.next();
			
			if(details.getVsvValue().equals(vsvValue)) {
				
				return true;
				
			}
			
		}
		
		return false;
		
	}
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	
	public void ejbCreate() throws CreateException {
		
		Debug.print("GlFindSegmentControllerBean ejbCreate");
		
	}
	
}