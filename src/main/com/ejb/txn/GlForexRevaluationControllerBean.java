/*
 * GlForexRevaluationControllerBean.java
 *
 * Created on May 15, 2006, 6:26 PM
 * 
 * @author  Farrah S. Garing
 */
package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlCOANoChartOfAccountFoundException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyAssignedException;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlJREffectiveDateViolationException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GenModSegmentDetails;
import com.util.GlModAccountingCalendarValueDetails;
import com.util.GlModFunctionalCurrencyDetails;

/**
 * @ejb:bean name="GlForexRevaluationControllerEJB"
 *           display-name="Used for searching of segments"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlForexRevaluationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlForexRevaluationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlForexRevaluationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
 */


public class GlForexRevaluationControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {
		
		Debug.print("GlForexRevaluationControllerBean getGlFcAllWithDefault");
		
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		Collection glFunctionalCurrencies = null;
		
		LocalGlFunctionalCurrency glFunctionalCurrency = null;
		LocalAdCompany adCompany = null;
		
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
					EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (glFunctionalCurrencies.isEmpty()) {
			
			return null;
			
		}
		
		Iterator i = glFunctionalCurrencies.iterator();
		
		while (i.hasNext()) {
			
			glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
			
			GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
					glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
					adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
							EJBCommon.TRUE : EJBCommon.FALSE);
			
			list.add(mdetails);
			
		}
		
		return list;
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlAcAllEditableOpenAndFutureEntry(Integer AD_CMPNY) {
		
		Debug.print("GlForexRevaluationControllerBean getGlAcAllEditableOpenAndFutureEntry");      
		
		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
			
			Iterator i = glSetOfBooks.iterator();
			
			while (i.hasNext()) {
				
				LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();
				
				Collection glAccountingCalendarValues = 
					glAccountingCalendarValueHome.findOpenAndFutureEntryAcvByAcCode(
							glSetOfBook.getGlAccountingCalendar().getAcCode(), 'O', 'F', AD_CMPNY);
				
				Iterator j = glAccountingCalendarValues.iterator();
				
				while (j.hasNext()) {
					
					LocalGlAccountingCalendarValue glAccountingCalendarValue = 
						(LocalGlAccountingCalendarValue)j.next();
					
					GlModAccountingCalendarValueDetails mdetails = new GlModAccountingCalendarValueDetails();
					
					mdetails.setAcvPeriodPrefix(glAccountingCalendarValue.getAcvPeriodPrefix());
					
					// get year
					
					GregorianCalendar gc = new GregorianCalendar();
					gc.setTime(glAccountingCalendarValue.getAcvDateTo());
					
					mdetails.setAcvYear(gc.get(Calendar.YEAR));
					
					list.add(mdetails);
					
				}
				
			}
			
			return list;
			
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("GlForexRevaluationControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGenSgAll(Integer AD_CMPNY) {
		
		Debug.print("GlRepDetailIncomeStatementControllerBean getGenSgAll");      
		
		LocalAdCompanyHome adCompanyHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			Collection genSegments = genSegmentHome.findByFlCode(adCompany.getGenField().getFlCode(), AD_CMPNY);
			
			Iterator i = genSegments.iterator();
			
			while (i.hasNext()) {
				
				LocalGenSegment genSegment = (LocalGenSegment)i.next();
				
				GenModSegmentDetails mdetails = new GenModSegmentDetails();
				mdetails.setSgMaxSize(genSegment.getSgMaxSize());
				mdetails.setSgFlSegmentSeparator(genSegment.getGenField().getFlSegmentSeparator());
				
				
				list.add(mdetails);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public void executeForexRevaluation(HashMap criteria, double CONVERSION_RATE, String UNRLZD_GN_LSS_ACCNT, int YR,
		String PRD_NM, String USR_NM, String JR_DCMNT_NMBR, Integer AD_BRNCH, Integer AD_CMPNY)
		throws GlJREffectiveDatePeriodClosedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDateViolationException,
		GlobalDocumentNumberNotUniqueException,
		GlFCNoFunctionalCurrencyFoundException,
		GlFCFunctionalCurrencyAlreadyAssignedException,
		GlCOANoChartOfAccountFoundException,
		GlobalAccountNumberInvalidException,
		GlobalRecordInvalidException,
		GlobalNoRecordFoundException {

		Debug.print("GlForexRevaluationControllerBean executeForexRevaluation");
		
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
	    LocalGlChartOfAccountHome glChartOfAccountHome = null;
	   	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
	   	LocalGlForexLedgerHome glForexLedgerHome = null;
	   	LocalGlJournalBatchHome glJournalBatchHome = null;
	   	LocalGlJournalHome glJournalHome = null;
	   	LocalGlSetOfBookHome glSetOfBookHome = null;
	   	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
	   	LocalGlJournalCategoryHome glJournalCategoryHome = null;
	   	LocalGlJournalSourceHome glJournalSourceHome = null;
	   	LocalGlJournalLineHome glJournalLineHome = null;
		
	   	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
	   	
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);			
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);			
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	   		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
	   		glForexLedgerHome = (LocalGlForexLedgerHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
	   		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
	   		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
	   		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
	   		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
	   		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
	   		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
	   		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
  	  		Date JR_EFFCTV_DT = null;

  	  		// validate if period exists and open
            
  	  		LocalGlSetOfBook glSetOfBook = null; 
  	  		
  	  		try {	  	 
            	
	            // get selected set of book
	            
	            Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(PRD_NM,
	            		EJBCommon.getIntendedDate(YR), AD_CMPNY);
	            ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);
	            
	            glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);

	            // get accounting calendar value 
		                           
	            LocalGlAccountingCalendarValue glAccountingCalendarValue = 
	                glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
	                	glSetOfBook.getGlAccountingCalendar().getAcCode(),
	                	PRD_NM, AD_CMPNY);
            	
            	if (glAccountingCalendarValue.getAcvStatus() == 'C' ||
            			glAccountingCalendarValue.getAcvStatus() == 'P' ||
						glAccountingCalendarValue.getAcvStatus() == 'N') {
            		
            		throw new GlJREffectiveDatePeriodClosedException(JR_DCMNT_NMBR);
            		
            	}
            	
            	JR_EFFCTV_DT = glAccountingCalendarValue.getAcvDateTo();
            	
            } catch (FinderException ex) {
            	
            	throw new GlJREffectiveDateNoPeriodExistException(JR_DCMNT_NMBR);
            	
            }

            // validate if document number is unique
  	  		
  	  		try {
  	  			
  	  			LocalGlJournal glExistingJournal = glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(
  	  					JR_DCMNT_NMBR, "MANUAL", AD_BRNCH, AD_CMPNY);
  	  			
  	  			throw new GlobalDocumentNumberNotUniqueException(JR_DCMNT_NMBR);
  	  			
  	  		} catch (FinderException ex) {
  	  		}
  	  		
  	  		// generate document number if necessary
  	  		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
  	  		
  	  		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment =
  	  			adDocumentSequenceAssignmentHome.findByDcName("GL JOURNAL", AD_CMPNY);
  	  		
  	  		try {
  	  			
  	  			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
  	  				adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
  	  			
  	  		} catch (FinderException ex) {
  	  			
  	  		}

  	  		if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'M' && 
  	  				(JR_DCMNT_NMBR == null || JR_DCMNT_NMBR.trim().length() == 0)) {
  	  			
  	  			throw new GlobalRecordInvalidException();
  	  		
  	  		} else if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
  	  				(JR_DCMNT_NMBR == null || JR_DCMNT_NMBR.trim().length() == 0)) {
  	  			
  	  			while (true) {
  	  				
  	  				if ( adBranchDocumentSequenceAssignment == null ||
  	  					adBranchDocumentSequenceAssignment.getBdsNextSequence() == null ) {
  	  					
  	  					try {
  	  						
  	  						glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(
  	  							adDocumentSequenceAssignment.getDsaNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
  	  						adDocumentSequenceAssignment.setDsaNextSequence(
  	  							EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
  	  						
  	  					} catch (FinderException ex) {
  	  						
  	  						JR_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
  	  						adDocumentSequenceAssignment.setDsaNextSequence(
  	  							EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
  	  						break;
  	  						
  	  					}	            	        			            	
  	  					
  	  				} else {
  	  					
  	  					try {
  	  						
  	  						glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(
  	  							adBranchDocumentSequenceAssignment.getBdsNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
  	  						adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
  	  							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
  	  						
  	  					} catch (FinderException ex) {
  	  						
  	  						JR_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
  	  						adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
  	  							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
  	  						break;
  	  						
  	  					}	            	        			            	
  	  					
  	  				}
  	  				
  	  			}		            
  	  			
  	  		}

			LocalGenField genField = adCompany.getGenField();
			String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
			int genNumberOfSegment = genField.getFlNumberOfSegment();		
			
			short ctr = 0;

			StringTokenizer stAccountFrom = new StringTokenizer((String)criteria.get(
					"accountNumberFrom"),strSeparator);
			StringTokenizer stAccountTo = new StringTokenizer((String)criteria.get(
					"accountNumberTo"), strSeparator);
			
			Object obj[] = new Object[criteria.size() - 2];
			
			//get COA selected
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT DISTINCT OBJECT(coa) FROM GlChartOfAccount coa WHERE ");
			
			// validate if account number is valid
			
			if (stAccountFrom.countTokens() != genNumberOfSegment || 
					stAccountTo.countTokens() != genNumberOfSegment) {
				
				throw new GlobalAccountNumberInvalidException();
				
			}
			
			try {
				
				String var = "1";
				
				if (genNumberOfSegment > 1) {
					
					for (int i=0; i<genNumberOfSegment; i++) {
						
						if (i == 0 && i < genNumberOfSegment - 1) {
							
							// for first segment
							
							jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator +
								"', coa.coaAccountNumber, 1)-1) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" +
								stAccountTo.nextToken() + "' AND ");
							
							var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)+1 ";
							
							
						} else if (i != 0 && i < genNumberOfSegment - 1){     		
							
							// for middle segments
							
							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator +
								"', coa.coaAccountNumber, " + var + ")) - (" + var +")) BETWEEN '" +
								stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");
							
							var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";
							
						} else if (i != 0 && i == genNumberOfSegment - 1) {
							
							// for last segment
							
							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var +
								", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) BETWEEN '" +
								stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' ");
							
							
						}	     		       	      	   	            
						
					}
					
				} else if(genNumberOfSegment == 1) {
					
					String accountFrom = stAccountFrom.nextToken();
					String accountTo = stAccountTo.nextToken();
					
					jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator +
						"', coa.coaAccountNumber, 1)-1) BETWEEN '" + accountFrom + "' AND '" + accountTo + "' OR " +
						"coa.coaAccountNumber BETWEEN '" + accountFrom + "' AND '" + accountTo + "' ");
					
				}
				
				String GL_FUNCTIONAL_CURRENCY = (String) criteria.get("functionalCurrency");
				
				if (GL_FUNCTIONAL_CURRENCY.length() > 0 && GL_FUNCTIONAL_CURRENCY != null &&
						(!GL_FUNCTIONAL_CURRENCY.equalsIgnoreCase("NO RECORD FOUND"))) {
					
					LocalGlFunctionalCurrency glFunctionalCurrency = null;
					
					try{
						
						glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
								GL_FUNCTIONAL_CURRENCY, AD_CMPNY);
						
					}catch(FinderException ex){
						
						throw new GlFCNoFunctionalCurrencyFoundException();
						
					}
					// check if FC selected is default company FC
					if (adCompany.getGlFunctionalCurrency().getFcCode() == glFunctionalCurrency.getFcCode())
						throw new GlFCFunctionalCurrencyAlreadyAssignedException();
					
					jbossQl.append(" AND coa.glFunctionalCurrency.fcCode=?" + (ctr + 1) + " ");
					obj[ctr] = glFunctionalCurrency.getFcCode();
					ctr++;
					
				}
				
				
				jbossQl.append("AND coa.coaEnable=1 AND coa.coaAdCompany=" + AD_CMPNY + " ");

			} catch (NumberFormatException ex) {
				
				throw new GlobalAccountNumberInvalidException();
				
			}

			Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), obj);
			
			if (glChartOfAccounts == null || glChartOfAccounts.isEmpty() || glChartOfAccounts.size() == 0){

				throw new GlobalNoRecordFoundException();
				
			}
			
  	  		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

  	  		//create jounal batch
  	  		

  	  		//create jounal  	  		
  	  		LocalGlJournal glJournal = glJournalHome.create("REVALUATION "
  	  			+ formatter.format(new java.util.Date()), "REVALUATION " + formatter.format(new java.util.Date()),
  	  			JR_EFFCTV_DT, 0d, null, JR_DCMNT_NMBR, null, 1d, null, null, 'N', EJBCommon.FALSE,
				EJBCommon.FALSE, USR_NM, new Date(), USR_NM, new Date(), null, null, null, null, null, null,
				EJBCommon.FALSE ,null,
				AD_BRNCH, AD_CMPNY);

  	  		try {
				
  	  			LocalGlJournalBatch glJournalBatch =
  	  	  			adPreference.getPrfEnableGlJournalBatch() == EJBCommon.FALSE ? null :
  	  	  			glJournalBatchHome.create("REVALUATION " + formatter.format(new java.util.Date()),
  	  	  			"REVALUATION JOURNAL " + EJBCommon.getGcCurrentDateWoTime().getTime(), "OPEN",
  					EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
				glJournal.setGlJournalBatch(glJournalBatch);
				
			} catch (Exception ex) {
				
			}
  	  		//map journal source
  	  		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("REVALUATION", AD_CMPNY);          
  	  		glJournal.setGlJournalSource(glJournalSource);

  	  		//map journal category
  	  		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("GENERAL", AD_CMPNY); 
  	  		glJournal.setGlJournalCategory(glJournalCategory);

	        glJournal.setGlFunctionalCurrency(adCompany.getGlFunctionalCurrency());

			Iterator i = glChartOfAccounts.iterator();
			
			double TTL_FRX_GN_LSS = 0d;
			short JL_LN = 1;
			
			LocalGlChartOfAccount glForexCoa= null;
			
			try {
				
				glForexCoa = glChartOfAccountHome.findByCoaAccountNumber(UNRLZD_GN_LSS_ACCNT, AD_BRNCH);
				
			} catch (FinderException ex){
				
				throw new GlCOANoChartOfAccountFoundException();
				
			}
			
			while (i.hasNext()){
				
				LocalGlChartOfAccount glChartOfAccount= (LocalGlChartOfAccount) i.next();
				
				// find all 
				Collection glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(JR_EFFCTV_DT,
					glChartOfAccount.getCoaCode(), AD_CMPNY);

				if (glForexLedgers == null || glForexLedgers.isEmpty()) continue;
				
				LocalGlForexLedger glLatestForexLedger = (LocalGlForexLedger) glForexLedgers.iterator().next();

				int FRL_LN = (glLatestForexLedger.getFrlDate().compareTo(JR_EFFCTV_DT) != 0) ? 1 :
					glLatestForexLedger.getFrlLine().intValue() + 1;

				double FRL_BLNC = glLatestForexLedger.getFrlBalance();
				double FRX_GN_LSS = 0d;  
				
				Iterator j = glForexLedgers.iterator();
				
				while(j.hasNext() &&  FRL_BLNC > 0){
					
					LocalGlForexLedger glForexLedger = (LocalGlForexLedger) j.next();

					if (glForexLedger.getFrlType().equals("REVAL") || glForexLedger.getFrlAmount() <= 0d) continue;

					if(FRL_BLNC - glForexLedger.getFrlAmount() <= 0d)
						FRX_GN_LSS = FRX_GN_LSS + (FRL_BLNC * (CONVERSION_RATE - glForexLedger.getFrlRate()));
					else
						FRX_GN_LSS = FRX_GN_LSS + (glForexLedger.getFrlAmount() *
							(CONVERSION_RATE - glForexLedger.getFrlRate()));
							
					FRL_BLNC = FRL_BLNC - glForexLedger.getFrlAmount();

				}
				
				if (FRX_GN_LSS != 0d) {
					
					byte JL_DBT = EJBCommon.FALSE;				
					
					if(glChartOfAccount.getCoaAccountType().equalsIgnoreCase("ASSET"))
						JL_DBT =  (FRX_GN_LSS >= 0) ? EJBCommon.TRUE : EJBCommon.FALSE; 
					else
						JL_DBT = (FRX_GN_LSS < 0) ? EJBCommon.TRUE : EJBCommon.FALSE;
					
					LocalGlJournalLine glJournalLine = glJournalLineHome.create(JL_LN, JL_DBT,
							Math.abs(FRX_GN_LSS), "", AD_CMPNY);
					
					glJournal.addGlJournalLine(glJournalLine);
					glChartOfAccount.addGlJournalLine(glJournalLine);
					JL_LN++;
					
					// create forex record
					
					LocalGlForexLedger newGlForexLedger = glForexLedgerHome.create(JR_EFFCTV_DT, new Integer (FRL_LN),
							"REVAL", 0d, CONVERSION_RATE, glLatestForexLedger.getFrlBalance(), FRX_GN_LSS, AD_CMPNY);
					
					glChartOfAccount.addGlForexLedger(newGlForexLedger);

					TTL_FRX_GN_LSS = TTL_FRX_GN_LSS + FRX_GN_LSS;

					LocalGlJournalLine glFOREXJournalLine = glJournalLineHome.create(JL_LN, 
						JL_DBT == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE, Math.abs(FRX_GN_LSS), 
						"", AD_CMPNY);
					
					glJournal.addGlJournalLine(glFOREXJournalLine);
					glForexCoa.addGlJournalLine(glFOREXJournalLine);
					JL_LN++;
					
				}

			}

			if (glJournal.getGlJournalLines() == null || glJournal.getGlJournalLines().isEmpty() ||
					glJournal.getGlJournalLines().size() == 0 || TTL_FRX_GN_LSS == 0)
				throw new GlobalNoRecordFoundException();

		} catch (GlobalNoRecordFoundException ex) {
			
			getSessionContext().setRollbackOnly();
            throw ex;
		
		} catch (GlobalRecordInvalidException ex) {
			
        	getSessionContext().setRollbackOnly();
            throw ex;
		
		} catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw ex;
        	
		} catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw ex;
        	
		} catch (GlobalDocumentNumberNotUniqueException ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw ex;
        	
		} catch (GlFCNoFunctionalCurrencyFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw ex;
        	
		} catch (GlFCFunctionalCurrencyAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw ex;
        	
		} catch (GlCOANoChartOfAccountFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw ex;
        	
		} catch (GlobalAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw ex;

		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("GlForexRevaluationControllerBean ejbCreate");
		
	}
	
}