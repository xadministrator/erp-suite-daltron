
/*
 * GlFrgAccountAssignmentControllerBean.java
 *
 * Created on Aug 8, 2003, 10:29 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlFrgAAAccountHighInvalidException;
import com.ejb.exception.GlFrgAAAccountLowInvalidException;
import com.ejb.exception.GlFrgAARowAlreadyHasCalculationException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenFieldHome;
import com.ejb.gl.LocalGlFrgAccountAssignment;
import com.ejb.gl.LocalGlFrgAccountAssignmentHome;
import com.ejb.gl.LocalGlFrgCalculation;
import com.ejb.gl.LocalGlFrgCalculationHome;
import com.ejb.gl.LocalGlFrgRow;
import com.ejb.gl.LocalGlFrgRowHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlFrgAccountAssignmentDetails;
import com.util.GlFrgRowDetails;

/**
 * @ejb:bean name="GlFrgAccountAssignmentControllerEJB"
 *           display-name="Used for entering account assignment for each rows"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFrgAccountAssignmentControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFrgAccountAssignmentController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFrgAccountAssignmentControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFrgAccountAssignmentControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgAaByRowCode(Integer ROW_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlFrgAccountAssignmentControllerBean getGlFrgAaByRowCode");

        LocalGlFrgAccountAssignmentHome glFrgAccountAssignmentHome = null;

        Collection glFrgAccountAssignments = null;

        LocalGlFrgAccountAssignment glFrgAccountAssignment = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            glFrgAccountAssignmentHome = (LocalGlFrgAccountAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgAccountAssignmentHome.JNDI_NAME, LocalGlFrgAccountAssignmentHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            glFrgAccountAssignments = glFrgAccountAssignmentHome.findByRowCode(ROW_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (glFrgAccountAssignments.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = glFrgAccountAssignments.iterator();
               
        while (i.hasNext()) {
        	
        	glFrgAccountAssignment = (LocalGlFrgAccountAssignment)i.next();
                                                                        
        	GlFrgAccountAssignmentDetails details = new GlFrgAccountAssignmentDetails();
		      details.setAaCode(glFrgAccountAssignment.getAaCode());
		      details.setAaAccountLow(glFrgAccountAssignment.getAaAccountLow());
		      details.setAaAccountHigh(glFrgAccountAssignment.getAaAccountHigh());
		      details.setAaActivityType(glFrgAccountAssignment.getAaActivityType());
		      
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.GlFrgRowDetails getGlFrgRowByRowCode(Integer ROW_CODE, Integer AD_CMPNY) {
                    
        Debug.print("GlFrgAccountAssignmentControllerBean getGlFrgRowByRowCode");
        		    	         	   	        	    
        LocalGlFrgRowHome glFrgRowHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalGlFrgRow glFrgRow = glFrgRowHome.findByPrimaryKey(ROW_CODE);
   
        	GlFrgRowDetails details = new GlFrgRowDetails();
        		details.setRowCode(glFrgRow.getRowCode());
        		details.setRowLineNumber(glFrgRow.getRowLineNumber());        		
                details.setRowName(glFrgRow.getRowName());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGlFrgAaEntry(com.util.GlFrgAccountAssignmentDetails details, Integer ROW_CODE, Integer AD_CMPNY) 
        throws GlFrgAARowAlreadyHasCalculationException,
               GlFrgAAAccountHighInvalidException,
               GlFrgAAAccountLowInvalidException {
               
                    
        Debug.print("GlFrgAccountAssignmentControllerBean addGlFrgAaEntry");
        
        LocalGlFrgAccountAssignmentHome glFrgAccountAssignmentHome = null;
        LocalGlFrgRowHome glFrgRowHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenFieldHome genFieldHome = null;        
        LocalGlFrgCalculationHome glFrgCalculationHome = null;
        
        LocalGlFrgAccountAssignment glFrgAccountAssignment = null;       
        LocalGlFrgRow glFrgRow = null;        
        LocalGenField genField = null;
        LocalAdCompany adCompany = null;       
        
        Collection glFrgAccountAssignments = null;
        
        String strSeparator = null;
        
        // Initialize EJB Home
        
        try {
        	
            glFrgAccountAssignmentHome = (LocalGlFrgAccountAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgAccountAssignmentHome.JNDI_NAME, LocalGlFrgAccountAssignmentHome.class);  
            glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);   
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                
            genFieldHome = (LocalGenFieldHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenFieldHome.JNDI_NAME, LocalGenFieldHome.class);
            glFrgCalculationHome = (LocalGlFrgCalculationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFrgCalculationHome.JNDI_NAME, LocalGlFrgCalculationHome.class);  
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        // Validate if row already has calculation
        
        try {
        	
        	glFrgRow = glFrgRowHome.findByPrimaryKey(ROW_CODE);
        	
        	Collection glFrgCalculations = glFrgRow.getGlFrgCalculations();
        	
        	if(!glFrgCalculations.isEmpty()){
        		
        		Iterator i = glFrgCalculations.iterator();
        		
        		while(i.hasNext()) {
        		
        			LocalGlFrgCalculation glFrgCalculation = (LocalGlFrgCalculation)i.next();
        			
        			if(glFrgCalculation.getCalType().equals("C1"))
        				throw new GlFrgAARowAlreadyHasCalculationException();
        		}
            }
                                             
        } catch (GlFrgAARowAlreadyHasCalculationException ex) {
        	
           throw ex;
           
        } catch (FinderException ex) {   
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }

	    // Get GenField's separator and number of segments 
	    
	    short genNumberOfSegment = 0;
	      
	    try {
	       
	        adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	        genField = adCompany.getGenField();
		    char chrSeparator = genField.getFlSegmentSeparator();
		    strSeparator = String.valueOf(chrSeparator);
		    genNumberOfSegment = genField.getFlNumberOfSegment();
	      
	    } catch (Exception ex) {
	      
	        throw new EJBException(ex.getMessage());
	      
	    }
	    
	    // Validate Account High	
	    		    	
	    StringTokenizer stAccountHigh = new StringTokenizer(details.getAaAccountHigh(), strSeparator);
	    
        if (stAccountHigh.countTokens() != genNumberOfSegment) { 
        
          throw new GlFrgAAAccountHighInvalidException();
          
        }
        
        // Validate Account Low
        	    	
	    StringTokenizer stAccountLow = new StringTokenizer(details.getAaAccountLow(), strSeparator);
	    	    
	    if (stAccountLow.countTokens() != genNumberOfSegment) { 
        
          throw new GlFrgAAAccountLowInvalidException();
          
        }        	    	
		    
	
	    try {
	    	
	    	// Create New Account Assignment
	    	
	    	glFrgAccountAssignment = glFrgAccountAssignmentHome.create(details.getAaAccountLow(),
	    	details.getAaAccountHigh(), details.getAaActivityType(), AD_CMPNY); 
	    	        
	    	glFrgRow = glFrgRowHome.findByPrimaryKey(ROW_CODE);
			      	glFrgRow.addGlFrgAccountAssignment(glFrgAccountAssignment);            	        
	    	        
	    } catch (Exception ex) {
	    	
	    	getSessionContext().setRollbackOnly();        	
	        throw new EJBException(ex.getMessage());
	    	
	    }       
                                    	                                        
    }	
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateGlFrgAaEntry(com.util.GlFrgAccountAssignmentDetails details, Integer ROW_CODE, Integer AD_CMPNY) 
        throws GlFrgAAAccountHighInvalidException,
               GlFrgAAAccountLowInvalidException {
               

        Debug.print("GlFrgAccountAssignmentControllerBean updateGlFrgAaEntry");
        
        LocalGlFrgAccountAssignmentHome glFrgAccountAssignmentHome = null;
        LocalGlFrgRowHome glFrgRowHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenFieldHome genFieldHome = null;        
        
        LocalGlFrgAccountAssignment glFrgAccountAssignment = null;       
        LocalGlFrgRow glFrgRow = null;        
        LocalGenField genField = null;
        LocalAdCompany adCompany = null;
        
        Collection glFrgAccountAssignments = null;
        
        String strSeparator = null;
        
        // Initialize EJB Home
        
        try {
        	
            glFrgAccountAssignmentHome = (LocalGlFrgAccountAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgAccountAssignmentHome.JNDI_NAME, LocalGlFrgAccountAssignmentHome.class);  
            glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);   
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                
            genFieldHome = (LocalGenFieldHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenFieldHome.JNDI_NAME, LocalGenFieldHome.class);                                       
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }                               

	    // Get GenField's separator and number of segments 
	    
	    short genNumberOfSegment = 0;
	      
	    try {
	       
	        adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	        genField = adCompany.getGenField();
		    char chrSeparator = genField.getFlSegmentSeparator();
		    strSeparator = String.valueOf(chrSeparator);
		    genNumberOfSegment = genField.getFlNumberOfSegment();
	      
	    } catch (Exception ex) {
	      
	        throw new EJBException(ex.getMessage());
	      
	    }		

		// Validate Account High	
	    		    	
	    StringTokenizer stAccountHigh = new StringTokenizer(details.getAaAccountHigh(), strSeparator);
	    
        if (stAccountHigh.countTokens() != genNumberOfSegment) { 
        
          throw new GlFrgAAAccountHighInvalidException();
          
        }
        
        // Validate Account Low
        	    	
	    StringTokenizer stAccountLow = new StringTokenizer(details.getAaAccountLow(), strSeparator);
	    	    
	    if (stAccountLow.countTokens() != genNumberOfSegment) { 
        
          throw new GlFrgAAAccountLowInvalidException();
          
        } 

        // Find and Update Account Assignments
               
        try {
	
        	glFrgAccountAssignment = glFrgAccountAssignmentHome.findByPrimaryKey(details.getAaCode());
        	
		      glFrgAccountAssignment.setAaAccountLow(details.getAaAccountLow());
		      glFrgAccountAssignment.setAaAccountHigh(details.getAaAccountHigh());
		      glFrgAccountAssignment.setAaActivityType(details.getAaActivityType());

        	glFrgRow = glFrgRowHome.findByPrimaryKey(ROW_CODE);
		      	glFrgRow.addGlFrgAccountAssignment(glFrgAccountAssignment);  
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }         	
                                                	                                        
    }	 
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteGlFrgAaEntry(Integer AA_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {               

      Debug.print("GlFrgAccountAssignmentControllerBean deleteGlFrgAaEntry");

      LocalGlFrgAccountAssignment glFrgAccountAssignment = null;
      LocalGlFrgAccountAssignmentHome glFrgAccountAssignmentHome = null;

      // Initialize EJB Home
        
      try {

          glFrgAccountAssignmentHome = (LocalGlFrgAccountAssignmentHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFrgAccountAssignmentHome.JNDI_NAME, LocalGlFrgAccountAssignmentHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
      	
         glFrgAccountAssignment = glFrgAccountAssignmentHome.findByPrimaryKey(AA_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlobalRecordAlreadyDeletedException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }
      
      try {
         	
	      glFrgAccountAssignment.remove();
	    
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }         
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlFrgAccountAssignmentControllerBean ejbCreate");
      
    }
       
}
