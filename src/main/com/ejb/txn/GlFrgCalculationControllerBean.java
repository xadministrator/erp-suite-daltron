
/*
 * GlFrgCalculationControllerBean.java
 *
 * Created on Aug 14, 2003, 10:40 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.GlFrgCALRowAlreadyHasAccountAssignmentException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlFrgCalculation;
import com.ejb.gl.LocalGlFrgCalculationHome;
import com.ejb.gl.LocalGlFrgColumn;
import com.ejb.gl.LocalGlFrgColumnHome;
import com.ejb.gl.LocalGlFrgRow;
import com.ejb.gl.LocalGlFrgRowHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlFrgCalculationDetails;
import com.util.GlFrgColumnDetails;
import com.util.GlFrgRowDetails;

/**
 * @ejb:bean name="GlFrgCalculationControllerEJB"
 *           display-name="Used for entering account assignment for each rows"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFrgCalculationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFrgCalculationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFrgCalculationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFrgCalculationControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgCalByRowCode(Integer ROW_CODE, String CAL_TYP, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlFrgCalculationControllerBean getGlFrgCalByRowCode");

        LocalGlFrgCalculationHome glFrgCalculationHome = null;

        Collection glFrgCalculations = null;

        LocalGlFrgCalculation glFrgCalculation = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            glFrgCalculationHome = (LocalGlFrgCalculationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgCalculationHome.JNDI_NAME, LocalGlFrgCalculationHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            glFrgCalculations = glFrgCalculationHome.findByRowCodeAndCalType(ROW_CODE, CAL_TYP, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (glFrgCalculations.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = glFrgCalculations.iterator();
               
        while (i.hasNext()) {
        	
        	glFrgCalculation = (LocalGlFrgCalculation)i.next();
                                                                        
        	GlFrgCalculationDetails details = new GlFrgCalculationDetails();
		      details.setCalCode(glFrgCalculation.getCalCode());
		      details.setCalSequenceNumber(glFrgCalculation.getCalSequenceNumber());
		      details.setCalOperator(glFrgCalculation.getCalOperator());
              details.setCalConstant(glFrgCalculation.getCalConstant());
              details.setCalRowColName(glFrgCalculation.getCalRowColName());
              details.setCalSequenceLow(glFrgCalculation.getCalSequenceLow());
              details.setCalSequenceHigh(glFrgCalculation.getCalSequenceHigh());
              details.setCalType(glFrgCalculation.getCalType());
		      
        	list.add(details);
        }
        
        return list;
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgCalByColCode(Integer COL_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlFrgCalculationControllerBean getGlFrgCalByColCode");

        LocalGlFrgCalculationHome glFrgCalculationHome = null;

        Collection glFrgCalculations = null;

        LocalGlFrgCalculation glFrgCalculation = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            glFrgCalculationHome = (LocalGlFrgCalculationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgCalculationHome.JNDI_NAME, LocalGlFrgCalculationHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            glFrgCalculations = glFrgCalculationHome.findByColCode(COL_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (glFrgCalculations.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = glFrgCalculations.iterator();
               
        while (i.hasNext()) {
        	
        	glFrgCalculation = (LocalGlFrgCalculation)i.next();
                                                                        
        	GlFrgCalculationDetails details = new GlFrgCalculationDetails();
		      details.setCalCode(glFrgCalculation.getCalCode());
		      details.setCalSequenceNumber(glFrgCalculation.getCalSequenceNumber());
		      details.setCalOperator(glFrgCalculation.getCalOperator());
              details.setCalConstant(glFrgCalculation.getCalConstant());
              details.setCalRowColName(glFrgCalculation.getCalRowColName());
              details.setCalSequenceLow(glFrgCalculation.getCalSequenceLow());
              details.setCalSequenceHigh(glFrgCalculation.getCalSequenceHigh());
		      
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.GlFrgRowDetails getGlFrgRowByRowCode(Integer ROW_CODE, Integer AD_CMPNY) {
                    
        Debug.print("GlFrgCalculationControllerBean getGlFrgRowByRowCode");
        		    	         	   	        	    
        LocalGlFrgRowHome glFrgRowHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalGlFrgRow glFrgRow = glFrgRowHome.findByPrimaryKey(ROW_CODE);
   
        	GlFrgRowDetails details = new GlFrgRowDetails();
        		details.setRowCode(glFrgRow.getRowCode());       		
                details.setRowLineNumber(glFrgRow.getRowLineNumber()); 
                details.setRowName(glFrgRow.getRowName());
                
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.GlFrgColumnDetails getGlFrgColByColCode(Integer COL_CODE, Integer AD_CMPNY) {
                    
        Debug.print("GlFrgCalculationControllerBean getGlFrgColByColCode");
        		    	         	   	        	    
        LocalGlFrgColumnHome glFrgColumnHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            glFrgColumnHome = (LocalGlFrgColumnHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnHome.JNDI_NAME, LocalGlFrgColumnHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalGlFrgColumn glFrgColumn = glFrgColumnHome.findByPrimaryKey(COL_CODE);
   
        	GlFrgColumnDetails details = new GlFrgColumnDetails();
        		details.setColCode(glFrgColumn.getColCode());
        		details.setColName(glFrgColumn.getColName());        		
                details.setColSequenceNumber(glFrgColumn.getColSequenceNumber());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGlFrgCalEntry(com.util.GlFrgCalculationDetails details, 
        Integer ROW_CODE, Integer COL_CODE, Integer AD_CMPNY) 
        throws GlFrgCALRowAlreadyHasAccountAssignmentException, 
		       GlobalRecordAlreadyExistException {
               
                    
        Debug.print("GlFrgCalculationControllerBean addGlFrgCalEntry");
        
        LocalGlFrgCalculationHome glFrgCalculationHome = null;
        LocalGlFrgRowHome glFrgRowHome = null;
        LocalGlFrgColumnHome glFrgColumnHome = null;      
        
        LocalGlFrgCalculation glFrgCalculation = null;       
        LocalGlFrgRow glFrgRow = null;
        LocalGlFrgColumn glFrgColumn = null;         

        // Initialize EJB Home
        
        try {
        	
            glFrgCalculationHome = (LocalGlFrgCalculationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgCalculationHome.JNDI_NAME, LocalGlFrgCalculationHome.class);  
            glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);   
            glFrgColumnHome = (LocalGlFrgColumnHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnHome.JNDI_NAME, LocalGlFrgColumnHome.class);                                                       
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {

	        // Validate if row already has account assignment
	        
	        if (ROW_CODE != null) {
	        	
		        try {
		        	
		        	glFrgRow = glFrgRowHome.findByPrimaryKey(ROW_CODE);
		        	
		        	Collection glFrgAccountAssignments = glFrgRow.getGlFrgAccountAssignments();
		        	
		        	if(!glFrgAccountAssignments.isEmpty()){
		        		
		        		if(details.getCalType().equals("C1"))
		        			throw new GlFrgCALRowAlreadyHasAccountAssignmentException();
		              
		            }
		                                             
		        } catch (GlFrgCALRowAlreadyHasAccountAssignmentException ex) {
		        	
		           throw ex;
		           
		        } catch (FinderException ex) {   
		        	 	
		        }
	        
	        }
	    	
	    	try {
	        	
	        	if (ROW_CODE != null && COL_CODE == null) {
	    		
	        		glFrgCalculation = glFrgCalculationHome.findByRowCodeAndCalSequenceNumberAndCalType(ROW_CODE, details.getCalSequenceNumber(), details.getCalType(), AD_CMPNY);

	        	} else {
	        		
	        		glFrgCalculation = glFrgCalculationHome.findByColCodeAndCalSequenceNumber(COL_CODE, details.getCalSequenceNumber(), AD_CMPNY);
	        		
	        	}
	    		
	            throw new GlobalRecordAlreadyExistException();

	    	} catch (FinderException ex) {   
	    	 	
	    	}

	    	// Create New Calculation
	    	
	    	String CAL_RW_NM = null;
	    	
	    	if (details.getCalRowColName() != null && details.getCalRowColName().length() > 0) {
	    		
	    		CAL_RW_NM = details.getCalRowColName();
	    		
	    	}
	    	
	    	glFrgCalculation = glFrgCalculationHome.create(details.getCalSequenceNumber(),
	    	details.getCalOperator(), details.getCalType(), details.getCalConstant(), CAL_RW_NM,
	    	details.getCalSequenceLow(), details.getCalSequenceHigh(), AD_CMPNY); 
	    	
	    	
	    	try {
	    		
		    	if(ROW_CODE != null && COL_CODE == null) {
			    	        
			    	glFrgRow = glFrgRowHome.findByPrimaryKey(ROW_CODE);
					      	glFrgRow.addGlFrgCalculation(glFrgCalculation);  
				
			    } else {          	        
		
			    	glFrgColumn = glFrgColumnHome.findByPrimaryKey(COL_CODE);
					      	glFrgColumn.addGlFrgCalculation(glFrgCalculation); 
				
				}
				
			} catch (FinderException ex) {
			        	
		    } 
	    	        
	    } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
	    } catch (GlFrgCALRowAlreadyHasAccountAssignmentException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;  	
      	        	                            	
        } catch (Exception ex) {
            
        	Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }       
                                    	                                        
    }	
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateGlFrgCalEntry(com.util.GlFrgCalculationDetails details, 
        Integer ROW_CODE, Integer COL_CODE, Integer AD_CMPNY)
        throws GlobalRecordAlreadyExistException {
               
        Debug.print("GlFrgCalculationControllerBean updateGlFrgCalEntry");
        
        LocalGlFrgCalculationHome glFrgCalculationHome = null;
        LocalGlFrgRowHome glFrgRowHome = null;
        LocalGlFrgColumnHome glFrgColumnHome = null;      
        
        LocalGlFrgCalculation glFrgCalculation = null;       
        LocalGlFrgRow glFrgRow = null;
        LocalGlFrgColumn glFrgColumn = null;         

        // Initialize EJB Home
        
        try {
        	
            glFrgCalculationHome = (LocalGlFrgCalculationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgCalculationHome.JNDI_NAME, LocalGlFrgCalculationHome.class);  
            glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);   
            glFrgColumnHome = (LocalGlFrgColumnHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnHome.JNDI_NAME, LocalGlFrgColumnHome.class);                                                       
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        // Find and Update Calculation
               
        try {
        	
        	try {

                LocalGlFrgCalculation glFrgExistingCalculation = null;
        		
            	if (ROW_CODE != null && COL_CODE == null) {
            		
            		glFrgExistingCalculation = glFrgCalculationHome.findByRowCodeAndCalSequenceNumberAndCalType(ROW_CODE, details.getCalSequenceNumber(), details.getCalType(), AD_CMPNY);

            		if (!glFrgExistingCalculation.getCalCode().equals(details.getCalCode())) {
            			
            			throw new GlobalRecordAlreadyExistException();

            		}
            		
            	} else {
            		
            		glFrgExistingCalculation = glFrgCalculationHome.findByColCodeAndCalSequenceNumber(COL_CODE, details.getCalSequenceNumber(), AD_CMPNY);
            		
            		if (!glFrgExistingCalculation.getCalCode().equals(details.getCalCode())) {
            			
            			throw new GlobalRecordAlreadyExistException();

            		}
            		
            	}
        		
        	} catch (FinderException ex) {   
        	 	
        	}
 
        	glFrgCalculation = glFrgCalculationHome.findByPrimaryKey(details.getCalCode());
		      glFrgCalculation.setCalSequenceNumber(details.getCalSequenceNumber());
		      glFrgCalculation.setCalOperator(details.getCalOperator());
		      glFrgCalculation.setCalConstant(details.getCalConstant());
		      
		      String CAL_RW_NM = null;
	    	
		      if (details.getCalRowColName() != null && details.getCalRowColName().length() > 0) {
		    		
		      	  CAL_RW_NM = details.getCalRowColName();
		    		
		      }
		      glFrgCalculation.setCalRowColName(CAL_RW_NM);
		      glFrgCalculation.setCalSequenceLow(details.getCalSequenceLow());
		      glFrgCalculation.setCalSequenceHigh(details.getCalSequenceHigh());
                      
		      try {
	    		
	      		if(ROW_CODE != null && COL_CODE == null) {
			    	        
			    	glFrgRow = glFrgRowHome.findByPrimaryKey(ROW_CODE);
					      	glFrgRow.addGlFrgCalculation(glFrgCalculation);  
				
			    } else {          	        
		
			    	glFrgColumn = glFrgColumnHome.findByPrimaryKey(COL_CODE);
					      	glFrgColumn.addGlFrgCalculation(glFrgCalculation); 
				
				}
				
		      } catch (FinderException ex) {
			        	
		      }
		      
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;      
      	        	                            	
        } catch (Exception ex) {
            
        	Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }         	
                                                	                                        
    }	 
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteGlFrgCalEntry(Integer CAL_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {               

      Debug.print("GlFrgCalculationControllerBean deleteGlFrgCalEntry");

      LocalGlFrgCalculation glFrgCalculation = null;
      LocalGlFrgCalculationHome glFrgCalculationHome = null;

      // Initialize EJB Home
        
      try {

          glFrgCalculationHome = (LocalGlFrgCalculationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFrgCalculationHome.JNDI_NAME, LocalGlFrgCalculationHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                

      try {
      	
      	try {
      		
      		glFrgCalculation = glFrgCalculationHome.findByPrimaryKey(CAL_CODE);
      		
      	} catch (FinderException ex) {
      		
      		throw new GlobalRecordAlreadyDeletedException();
      		
      	}
      	
      	glFrgCalculation.remove();
      	
      } catch (RemoveException ex) {
      	
      	getSessionContext().setRollbackOnly();	    
      	throw new EJBException(ex.getMessage());
      	
      } catch (Exception ex) {
      	
      	getSessionContext().setRollbackOnly();	    
      	throw new EJBException(ex.getMessage());
      	
      }	      
      
    }   
   
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgRowByRsCodeAndLessThanRowLineNumber(Integer RS_CODE, int RW_LN_NMBR, String CAL_TYP, Integer AD_CMPNY) {
                    
        Debug.print("GlFrgCalculationControllerBean getGlFrgRowByRsCodeAndLessThanRowLineNumber");
        
        LocalGlFrgRowHome glFrgRowHome = null;
        
        Collection glFrgRows = null;
        
        LocalGlFrgRow glFrgRow = null;
        
        ArrayList list = new ArrayList();
                                       
        // Initialize EJB Home
        
        try {
            
            glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	try {
            
        		if (CAL_TYP.equals("C1"))
        			glFrgRows = glFrgRowHome.findByRowSetAndRowLineNumber(RS_CODE, RW_LN_NMBR, AD_CMPNY);
        		else if (CAL_TYP.equals("C2"))
        			glFrgRows = glFrgRowHome.findByRsCode(RS_CODE, AD_CMPNY);
            
        	} catch (FinderException ex) {
        		
        	}

	        if (glFrgRows.isEmpty()) {
	        	
	        	return null;
	        	
	        }
	        
	        Iterator i = glFrgRows.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	glFrgRow = (LocalGlFrgRow)i.next();
	        	
	        	list.add(glFrgRow.getRowName());
	        	
	        }
	        
	        return list;

        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
            
    }         

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgColByCsCodeAndLessThanColumnSequenceNumber(Integer CS_CODE, int COL_SQNC_NMBR, Integer AD_CMPNY) {
                    
        Debug.print("GlFrgCalculationControllerBean getGlFrgColByCsCodeAndLessThanColumnSequenceNumber");
        
        LocalGlFrgColumnHome glFrgColumnHome = null;
        
        Collection glFrgColumns = null;
        
        LocalGlFrgColumn glFrgColumn = null;
        
        ArrayList list = new ArrayList();
                                       
        // Initialize EJB Home
        
        try {
            
            glFrgColumnHome = (LocalGlFrgColumnHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnHome.JNDI_NAME, LocalGlFrgColumnHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	try {
        		

    			glFrgColumns = glFrgColumnHome.findByColumnSetAndColSequenceNumber(CS_CODE, COL_SQNC_NMBR, AD_CMPNY);
            
            } catch (FinderException ex) {
            	
            }

	        if (glFrgColumns.isEmpty()) {
	        	
	        	return null;
	        	
	        }
	        
	        Iterator i = glFrgColumns.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	glFrgColumn = (LocalGlFrgColumn)i.next();
	        	
	        	list.add(glFrgColumn.getColName());
	        	
	        }
	        
	        return list;
        
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
            
    } 
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlFrgCalculationControllerBean ejbCreate");
      
    }
       
}
