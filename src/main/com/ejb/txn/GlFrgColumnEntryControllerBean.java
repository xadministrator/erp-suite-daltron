
/*
 * GlFrgColumnEntryControllerBean.java
 *
 * Created on Aug 6, 2003, 11:1 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlFrgCOLColumnNameAlreadyExistException;
import com.ejb.exception.GlFrgCOLSequenceNumberAlreadyExistException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.gl.LocalGlFrgColumn;
import com.ejb.gl.LocalGlFrgColumnHome;
import com.ejb.gl.LocalGlFrgColumnSet;
import com.ejb.gl.LocalGlFrgColumnSetHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFrgColumnDetails;
import com.util.GlFrgColumnSetDetails;
import com.util.GlModFunctionalCurrencyDetails;

/**
 * @ejb:bean name="GlFrgColumnEntryControllerEJB"
 *           display-name="Used for entering columns"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFrgColumnEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFrgColumnEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFrgColumnEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFrgColumnEntryControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgColByCsCode(Integer CS_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlFrgColumnEntryControllerBean getGlFrgColByCsCode");

        LocalGlFrgColumnHome glFrgColumnHome = null;

        Collection glFrgColumns = null;

        LocalGlFrgColumn glFrgColumn = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            glFrgColumnHome = (LocalGlFrgColumnHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnHome.JNDI_NAME, LocalGlFrgColumnHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            glFrgColumns = glFrgColumnHome.findByCsCode(CS_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (glFrgColumns.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = glFrgColumns.iterator();
               
        while (i.hasNext()) {
        	
        	glFrgColumn = (LocalGlFrgColumn)i.next();
                                                                        
        	GlModFrgColumnDetails mdetails = new GlModFrgColumnDetails();
        	mdetails.setColCode(glFrgColumn.getColCode());
        	mdetails.setColName(glFrgColumn.getColName());
        	mdetails.setColPosition(glFrgColumn.getColPosition());
        	mdetails.setColSequenceNumber(glFrgColumn.getColSequenceNumber());
        	mdetails.setColFormatMask(glFrgColumn.getColFormatMask());
        	mdetails.setColFactor(glFrgColumn.getColFactor());
        	mdetails.setColAmountType(glFrgColumn.getColAmountType());
        	mdetails.setColOffset(glFrgColumn.getColOffset());
        	mdetails.setColOverrideRowCalculation(glFrgColumn.getColOverrideRowCalculation());
			mdetails.setColFcName(glFrgColumn.getGlFunctionalCurrency().getFcName());
                                                        	
        	list.add(mdetails);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.GlFrgColumnSetDetails getGlFrgCsByCsCode(Integer CS_CODE, Integer AD_CMPNY) {
                    
        Debug.print("GlFrgColumnEntryControllerBean getGlFrgCsByCsCode");
        		    	         	   	        	    
        LocalGlFrgColumnSetHome glFrgColumnSetHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            glFrgColumnSetHome = (LocalGlFrgColumnSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnSetHome.JNDI_NAME, LocalGlFrgColumnSetHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalGlFrgColumnSet glFrgColumnSet = glFrgColumnSetHome.findByPrimaryKey(CS_CODE);
   
        	GlFrgColumnSetDetails details = new GlFrgColumnSetDetails();
        		details.setCsCode(glFrgColumnSet.getCsCode());        		
                details.setCsName(glFrgColumnSet.getCsName());
                details.setCsDescription(glFrgColumnSet.getCsDescription());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGlFrgColEntry(com.util.GlModFrgColumnDetails mdetails, Integer CS_CODE, Integer AD_CMPNY) 
        throws GlFrgCOLColumnNameAlreadyExistException,
                GlFrgCOLSequenceNumberAlreadyExistException {
               
                    
        Debug.print("GlFrgColumnEntryControllerBean addGlFrgColumnEntry");
        
        LocalGlFrgColumnHome glFrgColumnHome = null;
        LocalGlFrgColumnSetHome glFrgColumnSetHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
               
        LocalGlFrgColumn glFrgColumn = null;
        LocalGlFrgColumnSet glFrgColumnSet = null;
        LocalGlFunctionalCurrency glFunctionalCurrency = null;
        
        // Initialize EJB Home
        
        try {

            glFrgColumnHome = (LocalGlFrgColumnHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnHome.JNDI_NAME, LocalGlFrgColumnHome.class);             
            glFrgColumnSetHome = (LocalGlFrgColumnSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnSetHome.JNDI_NAME, LocalGlFrgColumnSetHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);               
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                                   
        try {

            glFrgColumn = glFrgColumnHome.findByColumnNameAndCsCode(mdetails.getColName(), CS_CODE, AD_CMPNY);
        
            throw new GlFrgCOLColumnNameAlreadyExistException();
                                             
        } catch (GlFrgCOLColumnNameAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }

        try {

            glFrgColumn = glFrgColumnHome.findBySequenceNumberAndCsCode(mdetails.getColSequenceNumber(), CS_CODE, AD_CMPNY);
        
            throw new GlFrgCOLSequenceNumberAlreadyExistException();
                                             
        } catch (GlFrgCOLSequenceNumberAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	// create new columns
        	
        	glFrgColumn = glFrgColumnHome.create(mdetails.getColName(), mdetails.getColPosition(),
        			mdetails.getColSequenceNumber(), mdetails.getColFormatMask(), mdetails.getColFactor(),
        			mdetails.getColAmountType(), mdetails.getColOffset(), mdetails.getColOverrideRowCalculation(), AD_CMPNY); 

        	glFrgColumnSet = glFrgColumnSetHome.findByPrimaryKey(CS_CODE);
        	glFrgColumnSet.addGlFrgColumn(glFrgColumn);   

        	glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(mdetails.getColFcName(), AD_CMPNY);
        	glFunctionalCurrency.addGlFrgColumn(glFrgColumn);
        	        
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();        	
            throw new EJBException(ex.getMessage());
        	
        }        
                                    	                                        
    }	
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateGlFrgColEntry(com.util.GlModFrgColumnDetails mdetails, Integer CS_CODE, Integer AD_CMPNY) 
        throws GlFrgCOLColumnNameAlreadyExistException,
                GlFrgCOLSequenceNumberAlreadyExistException  {
               
                    
        Debug.print("GlFrgColumnEntryControllerBean updateGlFrgColEntry");
        
        LocalGlFrgColumnHome glFrgColumnHome = null;
        LocalGlFrgColumnSetHome glFrgColumnSetHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
               
        LocalGlFrgColumn glFrgColumn = null;
        LocalGlFrgColumnSet glFrgColumnSet = null;
        LocalGlFunctionalCurrency glFunctionalCurrency = null;
        
        // Initialize EJB Home
        
        try {

            glFrgColumnHome = (LocalGlFrgColumnHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnHome.JNDI_NAME, LocalGlFrgColumnHome.class);             
            glFrgColumnSetHome = (LocalGlFrgColumnSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnSetHome.JNDI_NAME, LocalGlFrgColumnSetHome.class);  
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }                                

        try {

            glFrgColumn = glFrgColumnHome.findByColumnNameAndCsCode(mdetails.getColName(), CS_CODE, AD_CMPNY);
              
            if(glFrgColumn != null &&
                !glFrgColumn.getColCode().equals(mdetails.getColCode()))  {  
        
               throw new GlFrgCOLColumnNameAlreadyExistException();
             
            } 
                                             
        } catch (GlFrgCOLColumnNameAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }

        try {

            glFrgColumn = glFrgColumnHome.findBySequenceNumberAndCsCode(mdetails.getColSequenceNumber(), CS_CODE, AD_CMPNY);
              
            if(glFrgColumn != null &&
                !glFrgColumn.getColCode().equals(mdetails.getColCode()))  {  
        
               throw new GlFrgCOLSequenceNumberAlreadyExistException();
             
            } 
                                             
        } catch (GlFrgCOLSequenceNumberAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }

        // Find and Update Columns
               
        try {
        	
        	glFrgColumn = glFrgColumnHome.findByPrimaryKey(mdetails.getColCode());

        	glFrgColumn.setColName(mdetails.getColName());
        	glFrgColumn.setColPosition(mdetails.getColPosition());
        	glFrgColumn.setColSequenceNumber(mdetails.getColSequenceNumber());
        	glFrgColumn.setColFormatMask(mdetails.getColFormatMask());
        	glFrgColumn.setColFactor(mdetails.getColFactor());
        	glFrgColumn.setColAmountType(mdetails.getColAmountType());
        	glFrgColumn.setColOffset(mdetails.getColOffset());
        	glFrgColumn.setColOverrideRowCalculation(mdetails.getColOverrideRowCalculation());

        	glFrgColumnSet = glFrgColumnSetHome.findByPrimaryKey(CS_CODE);
        	glFrgColumnSet.addGlFrgColumn(glFrgColumn); 

        	glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(mdetails.getColFcName(), AD_CMPNY);
        	glFunctionalCurrency.addGlFrgColumn(glFrgColumn);

        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }           	
                                                	                                        
    }	 
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteGlFrgColEntry(Integer COL_CODE, Integer AD_CMPNY) 
    throws GlobalRecordAlreadyDeletedException {               

    	Debug.print("GlFrgColumnEntryControllerBean deleteGlFrgColEntry");

    	LocalGlFrgColumn glFrgColumn = null;
    	LocalGlFrgColumnHome glFrgColumnHome = null;

    	// Initialize EJB Home

    	try {

    		glFrgColumnHome = (LocalGlFrgColumnHome)EJBHomeFactory.
    		lookUpLocalHome(LocalGlFrgColumnHome.JNDI_NAME, LocalGlFrgColumnHome.class);           

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}                

    	try {

    		glFrgColumn = glFrgColumnHome.findByPrimaryKey(COL_CODE);

    	} catch (FinderException ex) {

    		throw new GlobalRecordAlreadyDeletedException();

    	} catch (Exception ex) {

    		throw new EJBException(ex.getMessage()); 

    	}

    	try {

    		glFrgColumn.remove();

    	} catch (RemoveException ex) {

    		getSessionContext().setRollbackOnly();	    
    		throw new EJBException(ex.getMessage());

    	} catch (Exception ex) {

    		getSessionContext().setRollbackOnly();	    
    		throw new EJBException(ex.getMessage());

    	}	      

    }         

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {

    	Debug.print("GlFrgColumnEntryControllerBean getGlFcAllWithDefault");

    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

    	Collection glFunctionalCurrencies = null;

    	LocalGlFunctionalCurrency glFunctionalCurrency = null;
    	LocalAdCompany adCompany = null;


    	ArrayList list = new ArrayList();

    	// Initialize EJB Home

    	try {

    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
    		lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
    				EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

    	} catch (Exception ex) {

    		throw new EJBException(ex.getMessage());
    	}

    	if (glFunctionalCurrencies.isEmpty()) {

    		return null;

    	}

    	Iterator i = glFunctionalCurrencies.iterator();

    	while (i.hasNext()) {

    		glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

    		GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
    				glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
    				adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
    						EJBCommon.TRUE : EJBCommon.FALSE);

    		list.add(mdetails);

    	}

    	return list;

    }
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlFrgColumnEntryControllerBean ejbCreate");
      
    }
}
