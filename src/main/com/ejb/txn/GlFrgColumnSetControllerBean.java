
/*
 * GlFrgColumnSetControllerBean.java
 *
 * Created on July 31, 2003, 9:13 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlFrgColumnSet;
import com.ejb.gl.LocalGlFrgColumnSetHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlFrgColumnSetDetails;

/**
 * @ejb:bean name="GlFrgColumnSetControllerEJB"
 *           display-name="Used for setting set of columns"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFrgColumnSetControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFrgColumnSetController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFrgColumnSetControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFrgColumnSetControllerBean extends AbstractSessionBean {
    

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgCsAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlFrgColumnSetControllerBean getGlFrgCsAll");
        
        LocalGlFrgColumnSetHome glFrgColumnSetHome = null;
        
        Collection glFrgColumns = null;
        
        LocalGlFrgColumnSet glFrgColumnSet = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFrgColumnSetHome = (LocalGlFrgColumnSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnSetHome.JNDI_NAME, LocalGlFrgColumnSetHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	glFrgColumns = glFrgColumnSetHome.findCsAll(AD_CMPNY);
        	       
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (glFrgColumns.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = glFrgColumns.iterator();
               
        while (i.hasNext()) {
        	
        	glFrgColumnSet = (LocalGlFrgColumnSet)i.next();       
                
        	GlFrgColumnSetDetails mdetails = new GlFrgColumnSetDetails();
				mdetails.setCsCode(glFrgColumnSet.getCsCode());
				mdetails.setCsName(glFrgColumnSet.getCsName());
				mdetails.setCsDescription(glFrgColumnSet.getCsDescription());
        		
        	list.add(mdetails);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGlFrgCsEntry(com.util.GlFrgColumnSetDetails details, Integer AD_CMPNY) 
        
        throws GlobalRecordAlreadyExistException{
                    
        Debug.print("GlFrgColumnSetControllerBean addGlFrgCsEntry");
        
        LocalGlFrgColumnSetHome glFrgColumnSetHome = null;
        
        LocalGlFrgColumnSet glFrgColumnSet = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFrgColumnSetHome = (LocalGlFrgColumnSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnSetHome.JNDI_NAME, LocalGlFrgColumnSetHome.class);  
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
           glFrgColumnSet = glFrgColumnSetHome.findByCsName(details.getCsName(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
            
        try {
        	
        	// create new set of columns
        	
        	glFrgColumnSet = glFrgColumnSetHome.create(details.getCsName(), 
        	    details.getCsDescription(), AD_CMPNY);   	        

        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateGlFrgCsEntry(com.util.GlFrgColumnSetDetails details, Integer AD_CMPNY)
        
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("GlFrgColumnSetControllerBean updateGlFrgFrEntry");
        
        LocalGlFrgColumnSetHome glFrgColumnSetHome = null;
        
        LocalGlFrgColumnSet glFrgColumnSet = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFrgColumnSetHome = (LocalGlFrgColumnSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnSetHome.JNDI_NAME, LocalGlFrgColumnSetHome.class);  
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalGlFrgColumnSet glExistingColumnSet = glFrgColumnSetHome.findByCsName(details.getCsName(), AD_CMPNY);
            
            if (!glExistingColumnSet.getCsCode().equals(details.getCsCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
      
        try {
        	        	
        	// find and update column set
        	
        	glFrgColumnSet = glFrgColumnSetHome.findByPrimaryKey(details.getCsCode());
        	
				glFrgColumnSet.setCsName(details.getCsName());
				glFrgColumnSet.setCsDescription(details.getCsDescription());
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteGlFrgCsEntry(Integer CS_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyAssignedException,
                GlobalRecordAlreadyDeletedException {
                    
        Debug.print("GlFrgColumnSetControllerBean deleteGlFrgCsEntry");
        
        LocalGlFrgColumnSetHome glFrgColumnSetHome = null; 
        LocalGlFrgColumnSet glFrgColumnSet = null;           
               
        // Initialize EJB Home
        
        try {
            
            glFrgColumnSetHome = (LocalGlFrgColumnSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnSetHome.JNDI_NAME, LocalGlFrgColumnSetHome.class);                      
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
            glFrgColumnSet = glFrgColumnSetHome.findByPrimaryKey(CS_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }

	    try {

    	    if (!glFrgColumnSet.getGlFrgFinancialReports().isEmpty()) {
    		
    		    throw new GlobalRecordAlreadyAssignedException();
    		
    	    }
    	    
    	    glFrgColumnSet.remove();  
    	    
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	 throw ex;    	        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }      	
        
	}

 
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlFrgColumnSetControllerBean ejbCreate");
      
    }
}
