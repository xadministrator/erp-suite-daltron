
/*
 * GlFrgFinancialReportEntryControllerBean.java
 *
 * Created on July 29, 2003, 11:33 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlFrgColumnSet;
import com.ejb.gl.LocalGlFrgColumnSetHome;
import com.ejb.gl.LocalGlFrgFinancialReport;
import com.ejb.gl.LocalGlFrgFinancialReportHome;
import com.ejb.gl.LocalGlFrgRowSet;
import com.ejb.gl.LocalGlFrgRowSetHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlModFrgFinancialReportDetails;

/**
 * @ejb:bean name="GlFrgFinancialReportEntryControllerEJB"
 *           display-name="Used for entering financial report"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFrgFinancialReportEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFrgFinancialReportEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFrgFinancialReportEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFrgFinancialReportEntryControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgCsAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlFrgFinancialReportEntryControllerBean getGlFrgCsAll");
        
        LocalGlFrgColumnSetHome glFrgColumnSetHome = null;
        
        Collection glFrgColumns = null;
        
        LocalGlFrgColumnSet glFrgColumnSet = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFrgColumnSetHome = (LocalGlFrgColumnSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnSetHome.JNDI_NAME, LocalGlFrgColumnSetHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            glFrgColumns = glFrgColumnSetHome.findCsAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }      
                
        if (glFrgColumns.isEmpty()) {
        	
        	return null;
        	
        }
        
        Iterator i = glFrgColumns.iterator();
               
        while (i.hasNext()) {
        	
        	glFrgColumnSet = (LocalGlFrgColumnSet)i.next();
        	
        	list.add(glFrgColumnSet.getCsName());
        	
        }
        
        return list;
            
    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgRsAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlFrgFinancialReportEntryControllerBean getGlFrgRsAll");
        
        LocalGlFrgRowSetHome glFrgRowSetHome = null;
        
        Collection glFrgRows = null;
        
        LocalGlFrgRowSet glFrgRowSet = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFrgRowSetHome = (LocalGlFrgRowSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowSetHome.JNDI_NAME, LocalGlFrgRowSetHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            glFrgRows = glFrgRowSetHome.findRsAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }      
                
        if (glFrgRows.isEmpty()) {
        	
        	return null;
        	
        }
        
        Iterator i = glFrgRows.iterator();
               
        while (i.hasNext()) {
        	
        	glFrgRowSet = (LocalGlFrgRowSet)i.next();
        	
        	list.add(glFrgRowSet.getRsName());
        	
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGlFrgFrEntry(com.util.GlFrgFinancialReportDetails details, 
        String RS_NM, String CS_NM, Integer AD_CMPNY) 
        
        throws GlobalRecordAlreadyExistException{
                    
        Debug.print("GlFrgFinancialReportEntryControllerBean addGlFrgFrEntry");
        
        LocalGlFrgFinancialReportHome glFrgFinancialReportHome = null;
        LocalGlFrgColumnSetHome glFrgColumnSetHome = null;
        LocalGlFrgRowSetHome glFrgRowSetHome = null;
        
        LocalGlFrgFinancialReport glFrgFinancialReport = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFrgFinancialReportHome = (LocalGlFrgFinancialReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgFinancialReportHome.JNDI_NAME, LocalGlFrgFinancialReportHome.class);
            glFrgColumnSetHome = (LocalGlFrgColumnSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnSetHome.JNDI_NAME, LocalGlFrgColumnSetHome.class);
            glFrgRowSetHome = (LocalGlFrgRowSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowSetHome.JNDI_NAME, LocalGlFrgRowSetHome.class);  
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
           glFrgFinancialReport = glFrgFinancialReportHome.findByFrName(details.getFrName(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
            
        try {
        	
        	// create new financial report
        	
        	glFrgFinancialReport = glFrgFinancialReportHome.create(details.getFrName(),
        	details.getFrDescription(), details.getFrTitle(), details.getFrFontSize(),
			details.getFrFontStyle(), details.getFrHorizontalAlign(), AD_CMPNY);   	        

			LocalGlFrgRowSet glFrgRowSet = glFrgRowSetHome.findByRsName(RS_NM, AD_CMPNY);
			glFrgRowSet.addGlFrgFinancialReport(glFrgFinancialReport); 

			LocalGlFrgColumnSet glFrgColumnSet = glFrgColumnSetHome.findByCsName(CS_NM, AD_CMPNY);
			glFrgColumnSet.addGlFrgFinancialReport(glFrgFinancialReport); 

        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateGlFrgFrEntry(com.util.GlFrgFinancialReportDetails details, 
        String RS_NM, String CS_NM, Integer AD_CMPNY)
        
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("GlFrgFinancialReportEntryControllerBean updateGlFrgFrEntry");
        
        LocalGlFrgFinancialReportHome glFrgFinancialReportHome = null;
        LocalGlFrgColumnSetHome glFrgColumnSetHome = null;
        LocalGlFrgRowSetHome glFrgRowSetHome = null;
        
        LocalGlFrgFinancialReport glFrgFinancialReport = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFrgFinancialReportHome = (LocalGlFrgFinancialReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgFinancialReportHome.JNDI_NAME, LocalGlFrgFinancialReportHome.class);
            glFrgColumnSetHome = (LocalGlFrgColumnSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgColumnSetHome.JNDI_NAME, LocalGlFrgColumnSetHome.class);
            glFrgRowSetHome = (LocalGlFrgRowSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowSetHome.JNDI_NAME, LocalGlFrgRowSetHome.class);  
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalGlFrgFinancialReport glExistingFinancialReport = 
                 glFrgFinancialReportHome.findByFrName(details.getFrName(), AD_CMPNY);
            
            if (!glExistingFinancialReport.getFrCode().equals(details.getFrCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
      
        try {
        	        	
        	// find and update financial report
        	
        	glFrgFinancialReport = glFrgFinancialReportHome.findByPrimaryKey(details.getFrCode());
        	
        	glFrgFinancialReport.setFrName(details.getFrName());
        	glFrgFinancialReport.setFrDescription(details.getFrDescription());
        	glFrgFinancialReport.setFrTitle(details.getFrTitle());
        	glFrgFinancialReport.setFrFontSize(details.getFrFontSize());
        	glFrgFinancialReport.setFrFontStyle(details.getFrFontStyle());
        	glFrgFinancialReport.setFrHorizontalAlign(details.getFrHorizontalAlign());
        	
        	try {	
        		
        		LocalGlFrgRowSet glFrgRowSet = glFrgRowSetHome.findByRsName(RS_NM, AD_CMPNY);
        		glFrgRowSet.addGlFrgFinancialReport(glFrgFinancialReport); 
        		
        	} catch (FinderException ex) {
        		
        	}
        	
        	try {	
        		
        		LocalGlFrgColumnSet glFrgColumnSet = glFrgColumnSetHome.findByCsName(CS_NM, AD_CMPNY);
        		glFrgColumnSet.addGlFrgFinancialReport(glFrgFinancialReport); 
        		
        	} catch (FinderException ex) {
        		
        	}
        	
        	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteGlFrgFrEntry(Integer FR_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("GlFrgFinancialReportEntryControllerBean deleteGlFrgFrEntry");
        
        LocalGlFrgFinancialReportHome glFrgFinancialReportHome = null; 
        
        Collection arPaymentMethods = null; 
        
        LocalGlFrgFinancialReport glFrgFinancialReport = null;           
               
        // Initialize EJB Home
        
        try {
            
            glFrgFinancialReportHome = (LocalGlFrgFinancialReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgFinancialReportHome.JNDI_NAME, LocalGlFrgFinancialReportHome.class);                      
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
            glFrgFinancialReport = glFrgFinancialReportHome.findByPrimaryKey(FR_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }

	    try {
	    	
    	    glFrgFinancialReport.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }      	
        
	}

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgFrAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlFrgFinancialReportEntryControllerBean getGlFrgFrAll");
        
        LocalGlFrgFinancialReportHome glFrgFinancialReportHome = null;
        LocalGlFrgColumnSetHome glFrgColumnSetHome = null;
        LocalGlFrgRowSetHome glFrgRowSetHome = null;
        
        Collection glFrgFinancialReports = null;
        
        LocalGlFrgFinancialReport glFrgFinancialReport = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFrgFinancialReportHome = (LocalGlFrgFinancialReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgFinancialReportHome.JNDI_NAME, LocalGlFrgFinancialReportHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	glFrgFinancialReports = glFrgFinancialReportHome.findFrAll(AD_CMPNY);
        	       
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (glFrgFinancialReports.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = glFrgFinancialReports.iterator();
               
        while (i.hasNext()) {
        	
        	glFrgFinancialReport = (LocalGlFrgFinancialReport)i.next();       
        	
        	GlModFrgFinancialReportDetails mdetails = new GlModFrgFinancialReportDetails();
        	mdetails.setFrCode(glFrgFinancialReport.getFrCode());
        	mdetails.setFrName(glFrgFinancialReport.getFrName());
        	mdetails.setFrDescription(glFrgFinancialReport.getFrDescription());
        	mdetails.setFrTitle(glFrgFinancialReport.getFrTitle());
        	mdetails.setFrRowName(glFrgFinancialReport.getGlFrgRowSet().getRsName());        	
        	mdetails.setFrColumnName(glFrgFinancialReport.getGlFrgColumnSet().getCsName());
        	mdetails.setFrFontSize(glFrgFinancialReport.getFrFontSize());
        	mdetails.setFrFontStyle(glFrgFinancialReport.getFrFontStyle());
        	mdetails.setFrHorizontalAlign(glFrgFinancialReport.getFrHorizontalAlign());
        	
        	list.add(mdetails);
        }
        
        return list;
            
    }        		
	
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlFrgFinancialReportEntryControllerBean ejbCreate");
      
    }
}
