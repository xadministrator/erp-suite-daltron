
/*
 * GlFrgRowEntryControllerBean.java
 *
 * Created on Aug 1, 2003, 10:35 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.GlFrgROWLineNumberAlreadyExistException;
import com.ejb.exception.GlFrgROWRowNameAlreadyExistException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.gl.LocalGlFrgRow;
import com.ejb.gl.LocalGlFrgRowHome;
import com.ejb.gl.LocalGlFrgRowSet;
import com.ejb.gl.LocalGlFrgRowSetHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlFrgRowDetails;
import com.util.GlFrgRowSetDetails;

/**
 * @ejb:bean name="GlFrgRowEntryControllerEJB"
 *           display-name="Used for entering rows"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFrgRowEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFrgRowEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFrgRowEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFrgRowEntryControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgRowByRowSetCode(Integer RS_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlFrgRowEntryControllerBean getGlFrgRowByRowSetCode");

        LocalGlFrgRowHome glFrgRowHome = null;

        Collection glFrgRows = null;

        LocalGlFrgRow glFrgRow = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            glFrgRows = glFrgRowHome.findByRsCode(RS_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (glFrgRows.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = glFrgRows.iterator();
               
        while (i.hasNext()) {
        	
        	glFrgRow = (LocalGlFrgRow)i.next();
                                                                        
        	GlFrgRowDetails details = new GlFrgRowDetails();
		      details.setRowCode(glFrgRow.getRowCode());
		      details.setRowLineNumber(glFrgRow.getRowLineNumber());
		      details.setRowName(glFrgRow.getRowName());
		      details.setRowIndent(glFrgRow.getRowIndent());
		      details.setRowLineToSkipBefore(glFrgRow.getRowLineToSkipBefore());
		      details.setRowLineToSkipAfter(glFrgRow.getRowLineToSkipAfter());
		      details.setRowUnderlineCharacterBefore(glFrgRow.getRowUnderlineCharacterBefore());
		      details.setRowUnderlineCharacterAfter(glFrgRow.getRowUnderlineCharacterAfter());
		      details.setRowPageBreakBefore(glFrgRow.getRowPageBreakBefore());
		      details.setRowPageBreakAfter(glFrgRow.getRowPageBreakAfter());
		      details.setRowOverrideColumnCalculation(glFrgRow.getRowOverrideColumnCalculation());
		      details.setRowHideRow(glFrgRow.getRowHideRow());
		      details.setRowFontStyle(glFrgRow.getRowFontStyle());
              details.setRowHorizontalAlign(glFrgRow.getRowHorizontalAlign());
              
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.GlFrgRowSetDetails getGlFrgRowSetByRowSetCode(Integer RS_CODE, Integer AD_CMPNY) {
                    
        Debug.print("GlFrgRowEntryControllerBean getGlFrgRowSetByRowSetCode");
        		    	         	   	        	    
        LocalGlFrgRowSetHome glFrgRowSetHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            glFrgRowSetHome = (LocalGlFrgRowSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowSetHome.JNDI_NAME, LocalGlFrgRowSetHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalGlFrgRowSet glFrgRowSet = glFrgRowSetHome.findByPrimaryKey(RS_CODE);
   
        	GlFrgRowSetDetails details = new GlFrgRowSetDetails();
        		details.setRsCode(glFrgRowSet.getRsCode());        		
                details.setRsName(glFrgRowSet.getRsName());
                details.setRsDescription(glFrgRowSet.getRsDescription());
                        
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGlFrgRowEntry(com.util.GlFrgRowDetails details, Integer RS_CODE, Integer AD_CMPNY) 
        throws GlFrgROWRowNameAlreadyExistException,
                GlFrgROWLineNumberAlreadyExistException {
               
                    
        Debug.print("GlFrgRowEntryControllerBean addGlFrgRowEntry");
        
        LocalGlFrgRowHome glFrgRowHome = null;
        LocalGlFrgRowSetHome glFrgRowSetHome = null;
               
        LocalGlFrgRow glFrgRow = null;
        LocalGlFrgRowSet glFrgRowSet = null;
        
        // Initialize EJB Home
        
        try {

            glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);             
            glFrgRowSetHome = (LocalGlFrgRowSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowSetHome.JNDI_NAME, LocalGlFrgRowSetHome.class);               
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                                   
        try {

            glFrgRow = glFrgRowHome.findByRowNameAndRsCode(details.getRowName(), RS_CODE, AD_CMPNY);
        
            throw new GlFrgROWRowNameAlreadyExistException();
                                             
        } catch (GlFrgROWRowNameAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }

        try {

            glFrgRow = glFrgRowHome.findByRowLineNumberAndRsCode(details.getRowLineNumber(), RS_CODE, AD_CMPNY);
        
            throw new GlFrgROWLineNumberAlreadyExistException();
                                             
        } catch (GlFrgROWLineNumberAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	// create new rows
        	
        	glFrgRow = glFrgRowHome.create(details.getRowLineNumber(), 
	        	details.getRowName(), details.getRowIndent(), details.getRowLineToSkipBefore(),
	        	details.getRowLineToSkipAfter(), details.getRowUnderlineCharacterBefore(), 
	        	details.getRowUnderlineCharacterAfter(), details.getRowPageBreakBefore(),
	        	details.getRowPageBreakAfter(), details.getRowOverrideColumnCalculation(),
	        	details.getRowHideRow(), details.getRowFontStyle(), details.getRowHorizontalAlign(), AD_CMPNY); 
        	        
        	glFrgRowSet = glFrgRowSetHome.findByPrimaryKey(RS_CODE);
		      	glFrgRowSet.addGlFrgRow(glFrgRow);            	        
        	        
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();        	
            throw new EJBException(ex.getMessage());
        	
        }        
                                    	                                        
    }	
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void updateGlFrgRowEntry(com.util.GlFrgRowDetails details, Integer RS_CODE, Integer AD_CMPNY) 
        throws GlFrgROWRowNameAlreadyExistException,
                GlFrgROWLineNumberAlreadyExistException  {
               
                    
        Debug.print("GlFrgRowEntryControllerBean updateGlFrgRowEntry");
        
        LocalGlFrgRowHome glFrgRowHome = null;
        LocalGlFrgRowSetHome glFrgRowSetHome = null;
               
        LocalGlFrgRow glFrgRow = null;
        LocalGlFrgRowSet glFrgRowSet = null;
        
        // Initialize EJB Home
        
        try {

            glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);             
            glFrgRowSetHome = (LocalGlFrgRowSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowSetHome.JNDI_NAME, LocalGlFrgRowSetHome.class);               
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }                                

        try {

            glFrgRow = glFrgRowHome.findByRowNameAndRsCode(details.getRowName(), RS_CODE, AD_CMPNY);
              
            if(glFrgRow != null &&
                !glFrgRow.getRowCode().equals(details.getRowCode()))  {  
        
               throw new GlFrgROWRowNameAlreadyExistException();
             
            } 
                                             
        } catch (GlFrgROWRowNameAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }

        try {

            glFrgRow = glFrgRowHome.findByRowLineNumberAndRsCode(details.getRowLineNumber(), RS_CODE, AD_CMPNY);
              
            if(glFrgRow != null &&
                !glFrgRow.getRowCode().equals(details.getRowCode()))  {  
        
               throw new GlFrgROWLineNumberAlreadyExistException();
             
            } 
                                             
        } catch (GlFrgROWLineNumberAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }

        // Find and Update Rows
               
        try {
        	
        	glFrgRow = glFrgRowHome.findByPrimaryKey(details.getRowCode());
        	
        	glFrgRow.setRowLineNumber(details.getRowLineNumber());
        	glFrgRow.setRowName(details.getRowName());
        	glFrgRow.setRowIndent(details.getRowIndent());
        	glFrgRow.setRowLineToSkipBefore(details.getRowLineToSkipBefore());
        	glFrgRow.setRowLineToSkipAfter(details.getRowLineToSkipAfter());
        	glFrgRow.setRowUnderlineCharacterBefore(details.getRowUnderlineCharacterBefore());
        	glFrgRow.setRowUnderlineCharacterAfter(details.getRowUnderlineCharacterAfter());
        	glFrgRow.setRowPageBreakBefore(details.getRowPageBreakBefore());
        	glFrgRow.setRowPageBreakAfter(details.getRowPageBreakAfter());
        	glFrgRow.setRowOverrideColumnCalculation(details.getRowOverrideColumnCalculation());
        	glFrgRow.setRowHideRow(details.getRowHideRow());
        	glFrgRow.setRowFontStyle(details.getRowFontStyle());
        	glFrgRow.setRowHorizontalAlign(details.getRowHorizontalAlign());
        	
        	glFrgRowSet = glFrgRowSetHome.findByPrimaryKey(RS_CODE);
        	glFrgRowSet.addGlFrgRow(glFrgRow);  
        	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }           	
                                                	                                        
    }	 
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteGlFrgRowEntry(Integer ROW_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {               

      Debug.print("GlFrgRowEntryControllerBean deleteGlFrgRowEntry");

      LocalGlFrgRow glFrgRow = null;
      LocalGlFrgRowHome glFrgRowHome = null;

      // Initialize EJB Home
        
      try {

          glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
      	
         glFrgRow = glFrgRowHome.findByPrimaryKey(ROW_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlobalRecordAlreadyDeletedException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }
      
      try {
         	
	      glFrgRow.remove();
	    
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }         
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlFrgRowEntryControllerBean ejbCreate");
      
    }
}
