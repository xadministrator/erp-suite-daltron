
/*
 * GlFrgRowSetControllerBean.java
 *
 * Created on July 31, 2003, 9:13 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlFrgRow;
import com.ejb.gl.LocalGlFrgRowHome;
import com.ejb.gl.LocalGlFrgRowSet;
import com.ejb.gl.LocalGlFrgRowSetHome;
import com.ejb.gl.LocalGlFrgAccountAssignment;
import com.ejb.gl.LocalGlFrgAccountAssignmentHome;
import com.ejb.gl.LocalGlFrgCalculation;
import com.ejb.gl.LocalGlFrgCalculationHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlFrgRowSetDetails;

/**
 * @ejb:bean name="GlFrgRowSetControllerEJB"
 *           display-name="Used for setting rows"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlFrgRowSetControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlFrgRowSetController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlFrgRowSetControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlFrgRowSetControllerBean extends AbstractSessionBean {
    

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFrgRsAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlFrgRowSetControllerBean getGlFrgRsAll");
        
        LocalGlFrgRowSetHome glFrgRowSetHome = null;
        
        Collection glFrgRows = null;
        
        LocalGlFrgRowSet glFrgRowSet = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFrgRowSetHome = (LocalGlFrgRowSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowSetHome.JNDI_NAME, LocalGlFrgRowSetHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	glFrgRows = glFrgRowSetHome.findRsAll(AD_CMPNY);
        	       
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (glFrgRows.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = glFrgRows.iterator();
               
        while (i.hasNext()) {
        	
        	glFrgRowSet = (LocalGlFrgRowSet)i.next();       
                
        	GlFrgRowSetDetails mdetails = new GlFrgRowSetDetails();
				mdetails.setRsCode(glFrgRowSet.getRsCode());
				mdetails.setRsName(glFrgRowSet.getRsName());
				mdetails.setRsDescription(glFrgRowSet.getRsDescription());
        		
        	list.add(mdetails);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGlFrgRsEntry(com.util.GlFrgRowSetDetails details, Integer AD_CMPNY) 
        
        throws GlobalRecordAlreadyExistException{
                    
        Debug.print("GlFrgRowSetControllerBean addGlFrgRsEntry");
        
        LocalGlFrgRowSetHome glFrgRowSetHome = null;
        
        LocalGlFrgRowSet glFrgRowSet = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFrgRowSetHome = (LocalGlFrgRowSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowSetHome.JNDI_NAME, LocalGlFrgRowSetHome.class);  
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
           glFrgRowSet = glFrgRowSetHome.findByRsName(details.getRsName(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
            
        try {
        	
        	// set new row
        	
        	glFrgRowSet = glFrgRowSetHome.create(details.getRsName(), details.getRsDescription(), AD_CMPNY);

        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void copyGlFrgRsEntry(com.util.GlFrgRowSetDetails details, Integer AD_CMPNY) 
        
        throws Exception{
                    
        Debug.print("GlFrgRowSetControllerBean copyGlFrgRsEntry");
        
        LocalGlFrgRowSetHome glFrgRowSetHome = null;
        LocalGlFrgRowHome glFrgRowHome = null;
        LocalGlFrgAccountAssignmentHome glFrgAccountAssignmentHome = null;
        LocalGlFrgCalculationHome glFrgCalculationHome = null;
        
        LocalGlFrgRowSet glFrgRowSetNew = null;
        LocalGlFrgRow glFrgRowNew = null;
        LocalGlFrgAccountAssignment glFrgAccountAssignmentNew = null;
        LocalGlFrgCalculation glFrgCalculationNew = null;
                
        // Initialize EJB Home
        
        try {
            
            glFrgRowSetHome = (LocalGlFrgRowSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowSetHome.JNDI_NAME, LocalGlFrgRowSetHome.class);  
            glFrgRowHome = (LocalGlFrgRowHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFrgRowHome.JNDI_NAME, LocalGlFrgRowHome.class);  
            glFrgAccountAssignmentHome = (LocalGlFrgAccountAssignmentHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlFrgAccountAssignmentHome.JNDI_NAME, LocalGlFrgAccountAssignmentHome.class);  
            glFrgCalculationHome = (LocalGlFrgCalculationHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlFrgCalculationHome.JNDI_NAME, LocalGlFrgCalculationHome.class);  

            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalGlFrgRowSet glFrgRowSetSource = glFrgRowSetHome.findByRsName(details.getRsName(), AD_CMPNY);
        	
        	//set new row set copy
        	glFrgRowSetNew = glFrgRowSetHome.create(("Copy of " + details.getRsName()), details.getRsDescription(), AD_CMPNY);
        	System.out.println("ROW SET COPY SUCCESS");
        	
        	//set new row copy
        	Collection glFrgRowSources = glFrgRowSetSource.getGlFrgRows();
        	
        	Iterator rowIter = glFrgRowSources.iterator();
        	
        	while(rowIter.hasNext()) {
        		
        		LocalGlFrgRow glFrgRowSource = (LocalGlFrgRow)rowIter.next();
        		
            	glFrgRowNew = glFrgRowHome.create(glFrgRowSource.getRowLineNumber(), (glFrgRowSource.getRowName()), 
            			glFrgRowSource.getRowIndent(), glFrgRowSource.getRowLineToSkipBefore(),
            			glFrgRowSource.getRowLineToSkipAfter(), glFrgRowSource.getRowUnderlineCharacterBefore(), 
            			glFrgRowSource.getRowUnderlineCharacterAfter(), glFrgRowSource.getRowPageBreakBefore(),
            			glFrgRowSource.getRowPageBreakAfter(), glFrgRowSource.getRowOverrideColumnCalculation(),
            			glFrgRowSource.getRowHideRow(), glFrgRowSource.getRowFontStyle(), glFrgRowSource.getRowHorizontalAlign(), AD_CMPNY);
            	
        		glFrgRowSetNew.addGlFrgRow(glFrgRowNew);
            	System.out.println("ROW COPY SUCCESS");
        		
            	
            	//set new account assignment copy
            	Collection glFrgAccountAssignmentSources = glFrgRowSource.getGlFrgAccountAssignments();
            	
            	if(glFrgAccountAssignmentSources.size() > 0) {
            	
	            	Iterator iter =  glFrgAccountAssignmentSources.iterator();
	            	
	            	while(iter.hasNext()) {
	            		
	            		LocalGlFrgAccountAssignment glFrgAccountAssignmentSource = (LocalGlFrgAccountAssignment)iter.next();
	            		
	            		glFrgAccountAssignmentNew = glFrgAccountAssignmentHome.create(glFrgAccountAssignmentSource.getAaAccountLow(),
	            				glFrgAccountAssignmentSource.getAaAccountHigh(), glFrgAccountAssignmentSource.getAaActivityType(), AD_CMPNY); 
	        	    	    	       
		    			glFrgRowNew.addGlFrgAccountAssignment(glFrgAccountAssignmentNew);
		    			
	            	}
	    			System.out.println("ACCOUNT ASSIGNMENT COPY SUCCESS");
            	}


    			//set new calculation copy
            	Collection glFrgcalculationSources = glFrgRowSource.getGlFrgCalculations();
            	
            	if(glFrgcalculationSources.size() > 0) {
            		
	            	Iterator iter = glFrgcalculationSources .iterator();
	            	
	            	while(iter.hasNext()) {
	            		
	            		LocalGlFrgCalculation glFrgCalculationSource = (LocalGlFrgCalculation)iter.next();
	            		
	        	    	glFrgCalculationNew = glFrgCalculationHome.create(glFrgCalculationSource.getCalSequenceNumber(), 
	        	    			glFrgCalculationSource.getCalOperator(), glFrgCalculationSource.getCalType(), 
	        	    			glFrgCalculationSource.getCalConstant(), (glFrgCalculationSource.getCalRowColName()),
	        	    			glFrgCalculationSource.getCalSequenceLow(), glFrgCalculationSource.getCalSequenceHigh(), AD_CMPNY); 
	
	        	    	glFrgRowNew.addGlFrgCalculation(glFrgCalculationNew);       
				      	
	            	}
	    			System.out.println("CALCULATION COPY SUCCESS");
            	}
        	}
        	
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }

    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateGlFrgRsEntry(com.util.GlFrgRowSetDetails details, Integer AD_CMPNY)
        
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("GlFrgRowSetControllerBean updateGlFrgFrEntry");
        
        LocalGlFrgRowSetHome glFrgRowSetHome = null;
        
        LocalGlFrgRowSet glFrgRowSet = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFrgRowSetHome = (LocalGlFrgRowSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowSetHome.JNDI_NAME, LocalGlFrgRowSetHome.class);  
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalGlFrgRowSet glExistingRowSet = glFrgRowSetHome.findByRsName(details.getRsName(), AD_CMPNY);
            
            if (!glExistingRowSet.getRsCode().equals(details.getRsCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
      
        try {
        	        	
        	// find and update row set
        	
        	glFrgRowSet = glFrgRowSetHome.findByPrimaryKey(details.getRsCode());
        	
				glFrgRowSet.setRsName(details.getRsName());
				glFrgRowSet.setRsDescription(details.getRsDescription());
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteGlFrgRsEntry(Integer RS_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyAssignedException,
                GlobalRecordAlreadyDeletedException {
                    
        Debug.print("GlFrgRowSetControllerBean deleteGlFrgRsEntry");
        
        LocalGlFrgRowSetHome glFrgRowSetHome = null; 
        LocalGlFrgRowSet glFrgRowSet = null;           
               
        // Initialize EJB Home
        
        try {
            
            glFrgRowSetHome = (LocalGlFrgRowSetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFrgRowSetHome.JNDI_NAME, LocalGlFrgRowSetHome.class);                      
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
            glFrgRowSet = glFrgRowSetHome.findByPrimaryKey(RS_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }

	    try {

    	    if (!glFrgRowSet.getGlFrgFinancialReports().isEmpty()) {
    		
    		    throw new GlobalRecordAlreadyAssignedException();
    		
    	    }
    	    
    	    glFrgRowSet.remove();  
    	    
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	 throw ex;    	        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}

 
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlFrgRowSetControllerBean ejbCreate");
      
    }
}
