
/*
 * GlJournalBatchCopyControllerBean.java
 *
 * Created on May 26, 2004, 8:35 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModJournalDetails;

/**
 * @ejb:bean name="GlJournalBatchCopyControllerEJB"
 *           display-name="Used for copying journal batch"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlJournalBatchCopyControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlJournalBatchCopyController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlJournalBatchCopyControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlJournalBatchCopyControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlOpenJbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("GlJournalBatchCopyControllerBean getGlOpenJbAll");
        
        LocalGlJournalBatchHome glJournalBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalBatches = glJournalBatchHome.findOpenJbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = glJournalBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalBatch glJournalBatch = (LocalGlJournalBatch)i.next();
        		
        		list.add(glJournalBatch.getJbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }   
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeGlJrBatchCopy(ArrayList list, String JB_NM_TO, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalTransactionBatchCloseException {
                    
        Debug.print("ApVoucherBatchCopyControllerBean executeGlJrBatchCopy");
        
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;        

        // Initialize EJB Home
        
        try {
            
        	glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			    lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
        	glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
                         
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {

        	// find journal batch to
        	
        	LocalGlJournalBatch glJournalBatchTo = glJournalBatchHome.findByJbName(JB_NM_TO, AD_BRNCH, AD_CMPNY);

        	// validate if batch to is closed
        	
        	if(glJournalBatchTo.getJbStatus().equals("CLOSED")) {
        		
        		throw new GlobalTransactionBatchCloseException();
        		
        	}
        	
        	Iterator i = list.iterator();       	        	        	
   			        	
        	while (i.hasNext()) {
        		
        		LocalGlJournal glJournal = glJournalHome.findByPrimaryKey((Integer)i.next());        		        		
        		glJournal.getGlJournalBatch().dropGlJournal(glJournal);
        		glJournalBatchTo.addGlJournal(glJournal);

        	}

        } catch (GlobalTransactionBatchCloseException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	         	
                                    
        } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }

    }
    
    /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlJrByJbName(String JB_NM, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("GlFindJournalControllerBean getGlJrByJbName");
      
      LocalGlJournalHome glJournalHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
           
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
            
      ArrayList jrList = new ArrayList();
      
      Collection glJournals = null;
      
      try {
      	
      	glJournals = glJournalHome.findByJbName(JB_NM, AD_CMPNY);
      	 
      } catch (Exception ex) {
      	
      	Debug.printStackTrace(ex);
      	throw new EJBException(ex.getMessage());
      	
      }
      
      if (glJournals.isEmpty())
         throw new GlobalNoRecordFoundException();
                           
      Iterator i = glJournals.iterator();
      while (i.hasNext()) {
      	
      	 double TOTAL_DEBIT = 0d;
      	 double TOTAL_CREDIT = 0d;
      	
         LocalGlJournal glJournal = (LocalGlJournal) i.next();
      	 Collection glJournalLines = glJournal.getGlJournalLines();
      	 
      	 Iterator j = glJournalLines.iterator();
      	 while (j.hasNext()) {
      	 	
      	 	LocalGlJournalLine glJournalLine = (LocalGlJournalLine) j.next();
      	 	
	        if (glJournalLine.getJlDebit() == EJBCommon.TRUE) {
	     	
	           TOTAL_DEBIT += this.convertForeignToFunctionalCurrency(
	           	   glJournal.getGlFunctionalCurrency().getFcCode(),
	           	   glJournal.getGlFunctionalCurrency().getFcName(),
	           	   glJournal.getJrConversionDate(),
	           	   glJournal.getJrConversionRate(),
	           	   glJournalLine.getJlAmount(), AD_CMPNY);
	        
	        } else {
	     	
	           TOTAL_CREDIT += this.convertForeignToFunctionalCurrency(
	           	   glJournal.getGlFunctionalCurrency().getFcCode(),
	           	   glJournal.getGlFunctionalCurrency().getFcName(),
	           	   glJournal.getJrConversionDate(),
	           	   glJournal.getJrConversionRate(),
	           	   glJournalLine.getJlAmount(), AD_CMPNY);
	        }
	        
	     }
	     
	     GlModJournalDetails mdetails = new GlModJournalDetails();
	     mdetails.setJrCode(glJournal.getJrCode());
	     mdetails.setJrName(glJournal.getJrName());
	     mdetails.setJrDescription(glJournal.getJrDescription());
	     mdetails.setJrEffectiveDate(glJournal.getJrEffectiveDate());
	     mdetails.setJrDocumentNumber(glJournal.getJrDocumentNumber());
	     mdetails.setJrTotalDebit(TOTAL_DEBIT);
	     mdetails.setJrTotalCredit(TOTAL_CREDIT);
	     mdetails.setJrJcName(glJournal.getGlJournalCategory().getJcName());
	     mdetails.setJrJsName(glJournal.getGlJournalSource().getJsName());
	     mdetails.setJrFcName(glJournal.getGlFunctionalCurrency().getFcName());	     	     	        
      	  
      	 jrList.add(mdetails);
      	
      }
         
      return jrList;
  
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("GlJournalBatchCopyControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
   
   
// private methods

   private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("GlJournalBatchCopyControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlJournalBatchCopyControllerBean ejbCreate");
      
    }
}
