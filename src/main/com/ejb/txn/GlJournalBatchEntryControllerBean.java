package com.ejb.txn;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlJournalBatchDetails;

/**
 * @ejb:bean name="GlJournalBatchEntryControllerEJB"
 *           display-name="used for entering journal batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlJournalBatchEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlJournalBatchEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlJournalBatchEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlJournalBatchEntryControllerBean extends AbstractSessionBean {

    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.GlJournalBatchDetails getGlJbByJbCode(Integer JB_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlJournalBatchEntryControllerBean getGlJbByJbCode");
        
        LocalGlJournalBatchHome glJournalBatchHome = null;        
                
        // Initialize EJB Home
        
        try {
            
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalGlJournalBatch glJournalBatch = null;
        	
        	
        	try {
        		
        		glJournalBatch = glJournalBatchHome.findByPrimaryKey(JB_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	GlJournalBatchDetails details = new GlJournalBatchDetails();
        	details.setJbCode(glJournalBatch.getJbCode());
        	details.setJbName(glJournalBatch.getJbName());
        	details.setJbDescription(glJournalBatch.getJbDescription());
        	details.setJbStatus(glJournalBatch.getJbStatus());
        	details.setJbDateCreated(glJournalBatch.getJbDateCreated());
        	details.setJbCreatedBy(glJournalBatch.getJbCreatedBy());
        	
        	return details;
        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveGlJbEntry(com.util.GlJournalBatchDetails details, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyExistException,
        GlobalRecordAlreadyDeletedException,
		GlobalTransactionBatchCloseException {
                    
        Debug.print("GlJournalBatchEntryControllerBean saveGlJrEntry");
        
        LocalGlJournalBatchHome glJournalBatchHome = null;  
        LocalGlJournalHome glJournalHome = null;       
        
        LocalGlJournalBatch glJournalBatch = null;
         
                
        // Initialize EJB Home
        
        try {
            
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);     
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);       
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        
        try {
 
                		
			// validate if journal batch is already deleted
			
			try {
				
				if (details.getJbCode() != null) {
					
					glJournalBatch = glJournalBatchHome.findByPrimaryKey(details.getJbCode());
					
				}
				
		 	} catch (FinderException ex) {
		 		
		 		throw new GlobalRecordAlreadyDeletedException();	 		
				
			} 
	        
	        // validate if journal batch exists
	        		
			try {
				
			    LocalGlJournalBatch glExistingJournalBatch = glJournalBatchHome.findByJbName(details.getJbName(), AD_BRNCH, AD_CMPNY);
			
			    if (details.getJbCode() == null ||
			        details.getJbCode() != null && !glExistingJournalBatch.getJbCode().equals(details.getJbCode())) {
			    	
			        throw new GlobalRecordAlreadyExistException();
			        
			    }
			 
			} catch (GlobalRecordAlreadyExistException ex) {
				
				throw ex;
			
			} catch (FinderException ex) {
			    
			} 	
			
			// validate if journal batch closing
			
			if (details.getJbStatus().equals("CLOSED")) {
				
				Collection glJournals = glJournalHome.findByJrPostedAndJbName(EJBCommon.FALSE, details.getJbName(), AD_CMPNY);
				
				if (!glJournals.isEmpty()) {
					
					throw new GlobalTransactionBatchCloseException();
					
				}
				
			}
			
			
			if (details.getJbCode() == null) {
				
				glJournalBatch = glJournalBatchHome.create(details.getJbName(),
				   details.getJbDescription(), details.getJbStatus(),
				   details.getJbDateCreated(), details.getJbCreatedBy(), AD_BRNCH, AD_CMPNY);
				
			} else {
				
				glJournalBatch.setJbName(details.getJbName());
				glJournalBatch.setJbDescription(details.getJbDescription());
				glJournalBatch.setJbStatus(details.getJbStatus());
				
			}
			
			return glJournalBatch.getJbCode();				
		      	            	
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	 
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
        
        } catch (GlobalRecordAlreadyExistException ex) {	 	
        
            getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	    
      	} catch (GlobalTransactionBatchCloseException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
                	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
            
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteGlJbEntry(Integer JB_CODE, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException,
        GlobalRecordAlreadyAssignedException {
                    
        Debug.print("GlJournalBatchEntryControllerBean deleteGlJbEntry");
        
        LocalGlJournalBatchHome glJournalBatchHome = null;  
        LocalGlJournalHome glJournalHome = null;               
                
        // Initialize EJB Home
        
        try {
            
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);     
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);       
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalGlJournalBatch glJournalBatch = glJournalBatchHome.findByPrimaryKey(JB_CODE);

        	if (!glJournalBatch.getGlJournals().isEmpty()) {
        		
        		throw new GlobalRecordAlreadyAssignedException();
        		
        	}
        	
        	glJournalBatch.remove();
        	
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw ex; 
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
              
	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlJournalBatchEntryControllerBean ejbCreate");
      
    }
    
   // private methods

   

}