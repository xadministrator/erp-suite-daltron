package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModJournalDetails;

/**
 * @ejb:bean name="GlJournalBatchPrintControllerEJB"
 *           display-name="Used for searching journals"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlJournalBatchPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlJournalBatchPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlJournalBatchPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlJournalBatchPrintControllerBean extends AbstractSessionBean {


   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlJcAll(Integer AD_CMPNY) {
                    
        Debug.print("GlJournalBatchPrintControllerBean getGlJcAll");
        
        LocalGlJournalCategoryHome glJournalCategoryHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalCategories = glJournalCategoryHome.findJcAll(AD_CMPNY);
        	
        	Iterator i = glJournalCategories.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalCategory glJournalCategory = (LocalGlJournalCategory)i.next();
        		
        		list.add(glJournalCategory.getJcName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlJsAll(Integer AD_CMPNY) {
                    
        Debug.print("GlJournalBatchPrintControllerBean getGlJsAll");
        
        LocalGlJournalSourceHome glJournalSourceHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalSources = glJournalSourceHome.findJsAll(AD_CMPNY);
        	
        	Iterator i = glJournalSources.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalSource glJournalSource = (LocalGlJournalSource)i.next();
        		
        		list.add(glJournalSource.getJsName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {
                    
        Debug.print("GlJournalBatchPrintControllerBean getGlFcAllWithDefault");
        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        Collection glFunctionalCurrencies = null;
        
        LocalGlFunctionalCurrency glFunctionalCurrency = null;
        LocalAdCompany adCompany = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	               
            glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
            	EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (glFunctionalCurrencies.isEmpty()) {
        	
        	return null;
        	
        }
        
        Iterator i = glFunctionalCurrencies.iterator();
               
        while (i.hasNext()) {
        	
        	glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
        	
        	GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
        	    glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
        	    adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
        	       EJBCommon.TRUE : EJBCommon.FALSE);
        	
        	list.add(mdetails);
        	
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlOpenJbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("GlJournalBatchPrintControllerBean getGlOpenJbAll");
        
        LocalGlJournalBatchHome glJournalBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalBatches = glJournalBatchHome.findOpenJbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = glJournalBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalBatch glJournalBatch = (LocalGlJournalBatch)i.next();
        		
        		list.add(glJournalBatch.getJbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableGlJournalBatch(Integer AD_CMPNY) {

        Debug.print("GlJournalBatchPrintControllerBean getAdPrfEnableGlJournalBatch");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableGlJournalBatch();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlJrByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("GlJournalBatchPrintControllerBean getGlJrByCriteria");
      
      LocalGlJournalHome glJournalHome = null;
            
      // Initialize EJB Home
        
      try {
      	
          glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
                     
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
            
      ArrayList jrList = new ArrayList();
      
      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(jr) FROM GlJournal jr ");
      
      boolean firstArgument = true;
      short ctr = 0;
      int criteriaSize = criteria.size() + 2;
      
      Object obj[];
      
       // Allocate the size of the object parameter
       
      if (criteria.containsKey("journalName")) {
      	
      	 criteriaSize--;
      	 
      }
      
      if (criteria.containsKey("approvalStatus")) {
      	
      	 String approvalStatus = (String)criteria.get("approvalStatus");
      	
      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
      	 	
      	 	 criteriaSize--;
      	 	
      	 }
      	
      }
      
      obj = new Object[criteriaSize];      
              
      
      if (criteria.containsKey("journalName")) {
      	
      	 if (!firstArgument) {
      	 
      	    jbossQl.append("AND ");	
      	 	
         } else {
         	
         	firstArgument = false;
         	jbossQl.append("WHERE ");
         	
         }
         
      	 jbossQl.append("jr.jrName LIKE '%" + (String)criteria.get("journalName") + "%' ");
      	 
      }
      
      if (criteria.containsKey("batchName")) {
      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.glJournalBatch.jbName=?" + (ctr+1) + " ");
	  	 obj[ctr] = (String)criteria.get("batchName");
	  	 ctr++;
	  } 

      if (criteria.containsKey("dateFrom")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrEffectiveDate>=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("dateFrom");
	  	 ctr++;
	  }  
	      
	  if (criteria.containsKey("dateTo")) {
	  	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrEffectiveDate<=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("dateTo");
	  	 ctr++;
	  	 
	  }    
	  
	  if (criteria.containsKey("documentNumberFrom")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrDocumentNumber>=?" + (ctr+1) + " ");
	  	 obj[ctr] = (String)criteria.get("documentNumberFrom");
	  	 ctr++;
	  }  
	      
	  if (criteria.containsKey("documentNumberTo")) {
	  	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrDocumentNumber<=?" + (ctr+1) + " ");
	  	 obj[ctr] = (String)criteria.get("documentNumberTo");
	  	 ctr++;
	  	 
	  }
	  
	  if (criteria.containsKey("category")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jr.glJournalCategory.jcName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("category");
       	  ctr++;
       	  
      }
      
      if (criteria.containsKey("source")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jr.glJournalSource.jsName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("source");
       	  ctr++;
       	  
      }
      
      if (criteria.containsKey("currency")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jr.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("currency");
       	  ctr++;
       	  
      }
      
      if (criteria.containsKey("approvalStatus")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  String approvalStatus = (String)criteria.get("approvalStatus");
     	  
     	  if (approvalStatus.equals("DRAFT")) {
     	      
	       	  jbossQl.append("jr.jrApprovalStatus IS NULL ");
     	  	
     	  } else if (approvalStatus.equals("REJECTED")) {
     	  	
	       	  jbossQl.append("jr.jrReasonForRejection IS NOT NULL ");
     	  	
    	  } else {
    	  	
	      	  jbossQl.append("jr.jrApprovalStatus=?" + (ctr+1) + " ");
	       	  obj[ctr] = approvalStatus;
	       	  ctr++;
    	  	
    	  }    
       	  
      }
      
      
      if (criteria.containsKey("posted")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jr.jrPosted=?" + (ctr+1) + " ");
       	  
       	  String posted = (String)criteria.get("posted");
       	  
       	  if (posted.equals("YES")) {
       	  	
       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
       	  	
       	  } else {
       	  	
       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
       	  	
       	  }       	  
       	 
       	  ctr++;
       	  
      }      
      
      if (!firstArgument) {
   	  	
   	     jbossQl.append("AND ");
   	     
   	  } else {
   	  	
   	  	 firstArgument = false;
   	  	 jbossQl.append("WHERE ");
   	  	 
   	  }
      
      jbossQl.append("jr.jrAdBranch=" + AD_BRNCH + " AND jr.jrAdCompany=" + AD_CMPNY + " ");
         
      String orderBy = null;
	      
	  if (ORDER_BY.equals("NAME")) {
	
	  	  orderBy = "jr.jrName";
	  	  
	  } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
	
	  	  orderBy = "jr.jrDocumentNumber";
	  	
	  } else if (ORDER_BY.equals("CATEGORY")) {
	
	  	  orderBy = "jr.glJournalCategory.jcName";
	  	  
	  } else if (ORDER_BY.equals("SOURCE")) {
	
	  	  orderBy = "jr.glJournalSource.jsName";
	  	  
	  }
	  
	  if (orderBy != null) {
	  
	  	jbossQl.append("ORDER BY " + orderBy + ", jr.jrEffectiveDate");
	  	
	  } else {
	  	
	  	jbossQl.append("ORDER BY jr.jrEffectiveDate");
	  	
	  }
               
      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;
      
      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      
      System.out.println("QL + " + jbossQl);
      
      

      Collection glJournals = null;
      
      double TOTAL_DEBIT = 0;
      double TOTAL_CREDIT = 0;
      
      try {
      	
         glJournals = glJournalHome.getJrByCriteria(jbossQl.toString(), obj);
         
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	 
      }
      
      if (glJournals.isEmpty())
         throw new GlobalNoRecordFoundException();               
         
      Iterator i = glJournals.iterator();
      while (i.hasNext()) {
      	
      	 TOTAL_DEBIT = 0d;
      	 TOTAL_CREDIT = 0d;
      	
         LocalGlJournal glJournal = (LocalGlJournal) i.next();                  
         
      	 Collection glJournalLines = glJournal.getGlJournalLines();
      	 
      	 Iterator j = glJournalLines.iterator();
      	 while (j.hasNext()) {
      	 	
      	 	LocalGlJournalLine glJournalLine = (LocalGlJournalLine) j.next();
      	 	
	        if (glJournalLine.getJlDebit() == EJBCommon.TRUE) {
	     	
	           TOTAL_DEBIT += this.convertForeignToFunctionalCurrency(
	           	   glJournal.getGlFunctionalCurrency().getFcCode(),
	           	   glJournal.getGlFunctionalCurrency().getFcName(),
	           	   glJournal.getJrConversionDate(),
	           	   glJournal.getJrConversionRate(),
	           	   glJournalLine.getJlAmount(), AD_CMPNY);
	        
	        } else {
	     	
	           TOTAL_CREDIT += this.convertForeignToFunctionalCurrency(
	           	   glJournal.getGlFunctionalCurrency().getFcCode(),
	           	   glJournal.getGlFunctionalCurrency().getFcName(),
	           	   glJournal.getJrConversionDate(),
	           	   glJournal.getJrConversionRate(),
	           	   glJournalLine.getJlAmount(), AD_CMPNY);
	        }
	        
	     }
	     
	     GlModJournalDetails mdetails = new GlModJournalDetails();
	     mdetails.setJrCode(glJournal.getJrCode());
	     mdetails.setJrName(glJournal.getJrName());
	     mdetails.setJrDescription(glJournal.getJrDescription());
	     mdetails.setJrEffectiveDate(glJournal.getJrEffectiveDate());
	     mdetails.setJrDocumentNumber(glJournal.getJrDocumentNumber());
	     mdetails.setJrTotalDebit(TOTAL_DEBIT);
	     mdetails.setJrTotalCredit(TOTAL_CREDIT);
	     mdetails.setJrJcName(glJournal.getGlJournalCategory().getJcName());
	     mdetails.setJrJsName(glJournal.getGlJournalSource().getJsName());
	     mdetails.setJrFcName(glJournal.getGlFunctionalCurrency().getFcName());	     	     	        
      	  
      	 jrList.add(mdetails);
      	
      }
         
      return jrList;
  
   }
   
   
     
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("GlJournalBatchPrintControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlJournalBatchControllerBean ejbCreate");
      
   }

   // private methods

   private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("GlJournalBatchPrintControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
   
}
