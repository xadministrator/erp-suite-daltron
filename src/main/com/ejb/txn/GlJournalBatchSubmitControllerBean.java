package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalCoaLineHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModJournalDetails;

/**
 * @ejb:bean name="GlJournalBatchSubmitControllerEJB"
 *           display-name="Used for submitting journals"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlJournalBatchSubmitControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlJournalBatchSubmitController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlJournalBatchSubmitControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlJournalBatchSubmitControllerBean extends AbstractSessionBean {


   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlJcAll(Integer AD_CMPNY) {
                    
        Debug.print("GlJournalBatchSubmitControllerBean getGlJcAll");
        
        LocalGlJournalCategoryHome glJournalCategoryHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalCategories = glJournalCategoryHome.findJcAll(AD_CMPNY);
        	
        	Iterator i = glJournalCategories.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalCategory glJournalCategory = (LocalGlJournalCategory)i.next();
        		
        		list.add(glJournalCategory.getJcName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlJsAll(Integer AD_CMPNY) {
                    
        Debug.print("GlJournalBatchSubmitControllerBean getGlJsAll");
        
        LocalGlJournalSourceHome glJournalSourceHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalSources = glJournalSourceHome.findJsAll(AD_CMPNY);
        	
        	Iterator i = glJournalSources.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalSource glJournalSource = (LocalGlJournalSource)i.next();
        		
        		list.add(glJournalSource.getJsName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {
                    
        Debug.print("GlJournalBatchSubmitControllerBean getGlFcAllWithDefault");
        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        Collection glFunctionalCurrencies = null;
        
        LocalGlFunctionalCurrency glFunctionalCurrency = null;
        LocalAdCompany adCompany = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	               
            glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
            	EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (glFunctionalCurrencies.isEmpty()) {
        	
        	return null;
        	
        }
        
        Iterator i = glFunctionalCurrencies.iterator();
               
        while (i.hasNext()) {
        	
        	glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
        	
        	GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
        	    glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
        	    adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
        	       EJBCommon.TRUE : EJBCommon.FALSE);
        	
        	list.add(mdetails);
        	
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlOpenJbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("GlJournalBatchSubmitControllerBean getGlOpenJbAll");
        
        LocalGlJournalBatchHome glJournalBatchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalBatches = glJournalBatchHome.findOpenJbAll(AD_BRNCH, AD_CMPNY);
        	
        	Iterator i = glJournalBatches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalBatch glJournalBatch = (LocalGlJournalBatch)i.next();
        		
        		list.add(glJournalBatch.getJbName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableGlJournalBatch(Integer AD_CMPNY) {

        Debug.print("GlJournalBatchSubmitControllerBean getAdPrfEnableGlJournalBatch");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableGlJournalBatch();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlJrByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("GlJournalBatchSubmitControllerBean getGlJrByCriteria");
      
      LocalGlJournalHome glJournalHome = null;
      LocalGlSetOfBookHome glSetOfBookHome = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
          glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
          glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
           
      ArrayList jrList = new ArrayList();
      
      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(jr) FROM GlJournal jr ");
      
      boolean firstArgument = true;
      short ctr = 0;
      int criteriaSize = criteria.size() + 2;
      Object obj[];
      
       // Allocate the size of the object parameter

       
      if (criteria.containsKey("journalName")) {
      	
      	  criteriaSize--; 
      	 
      } 
            
      
      obj = new Object[criteriaSize];
         
      
      if (criteria.containsKey("journalName")) {
      	
      	 if (!firstArgument) {
      	 
      	    jbossQl.append("AND ");	
      	 	
         } else {
         	
         	firstArgument = false;
         	jbossQl.append("WHERE ");
         	
         }
         
      	 jbossQl.append("jr.jrName LIKE '%" + (String)criteria.get("journalName") + "%' ");
      	 
      }
      
	  if (criteria.containsKey("batchName")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jr.glJournalBatch.jbName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("batchName");
       	  ctr++;
       	  
      }
        
      if (criteria.containsKey("dateFrom")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrEffectiveDate>=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("dateFrom");
	  	 ctr++;
	  }  
	      
	  if (criteria.containsKey("dateTo")) {
	  	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrEffectiveDate<=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("dateTo");
	  	 ctr++;
	  	 
	  }    
	  
	  if (criteria.containsKey("documentNumberFrom")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrDocumentNumber>=?" + (ctr+1) + " ");
	  	 obj[ctr] = (String)criteria.get("documentNumberFrom");
	  	 ctr++;
	  }  
	      
	  if (criteria.containsKey("documentNumberTo")) {
	  	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrDocumentNumber<=?" + (ctr+1) + " ");
	  	 obj[ctr] = (String)criteria.get("documentNumberTo");
	  	 ctr++;
	  	 
	  }
	  
	  if (criteria.containsKey("category")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jr.glJournalCategory.jcName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("category");
       	  ctr++;
       	  
      }
      
      if (criteria.containsKey("source")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jr.glJournalSource.jsName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("source");
       	  ctr++;
       	  
      }
      
      if (criteria.containsKey("currency")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jr.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("currency");
       	  ctr++;
       	  
      }
                   	
      if (!firstArgument) {
       	  	
         jbossQl.append("AND ");
       	     
      } else {
       	  	
      	 firstArgument = false;
      	 jbossQl.append("WHERE ");
       	  	 
      }
      
      jbossQl.append("jr.jrApprovalStatus IS NULL AND jr.jrPosted=0 AND jr.jrAdBranch=" + AD_BRNCH + " AND jr.jrAdCompany=" + AD_CMPNY + " ");
       	     
      String orderBy = null;
	      
	  if (ORDER_BY.equals("REFERENCE NUMBER")) {
	
	  	  orderBy = "jr.jrName";
	  	  
	  } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
	
	  	  orderBy = "jr.jrDocumentNumber";
	  	
	  } else if (ORDER_BY.equals("CATEGORY")) {
	
	  	  orderBy = "jr.glJournalCategory.jcName";
	  	  
	  } else if (ORDER_BY.equals("SOURCE")) {
	
	  	  orderBy = "jr.glJournalSource.jsName";
	  	  
	  }
	  
	  if (orderBy != null) {
	  
	  	jbossQl.append("ORDER BY " + orderBy + ", jr.jrEffectiveDate");
	  	
	  } else {
	  	
	  	jbossQl.append("ORDER BY jr.jrEffectiveDate");
	  	
	  }
               
      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;
      
      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      
      System.out.println("QL + " + jbossQl);
      
      

      Collection glJournals = null;
      
      double TOTAL_DEBIT = 0;
      double TOTAL_CREDIT = 0;
      
      try {
      	
         glJournals = glJournalHome.getJrByCriteria(jbossQl.toString(), obj);
         
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	 
      }
      
      if (glJournals.isEmpty())
         throw new GlobalNoRecordFoundException();
         
      Iterator i = glJournals.iterator();
      while (i.hasNext()) {
      	
      	 TOTAL_DEBIT = 0d;
      	 TOTAL_CREDIT = 0d;
      	
         LocalGlJournal glJournal = (LocalGlJournal) i.next();
      	 Collection glJournalLines = glJournal.getGlJournalLines();
      	 
      	 Iterator j = glJournalLines.iterator();
      	 while (j.hasNext()) {
      	 	
      	 	LocalGlJournalLine glJournalLine = (LocalGlJournalLine) j.next();
      	 	
	        if (glJournalLine.getJlDebit() == EJBCommon.TRUE) {
	     	
	           TOTAL_DEBIT += this.convertForeignToFunctionalCurrency(
	           	   glJournal.getGlFunctionalCurrency().getFcCode(),
	           	   glJournal.getGlFunctionalCurrency().getFcName(),
	           	   glJournal.getJrConversionDate(),
	           	   glJournal.getJrConversionRate(),
	           	   glJournalLine.getJlAmount(), AD_CMPNY);
	        
	        } else {
	     	
	           TOTAL_CREDIT += this.convertForeignToFunctionalCurrency(
	           	   glJournal.getGlFunctionalCurrency().getFcCode(),
	           	   glJournal.getGlFunctionalCurrency().getFcName(),
	           	   glJournal.getJrConversionDate(),
	           	   glJournal.getJrConversionRate(),
	           	   glJournalLine.getJlAmount(), AD_CMPNY);
	        }
	        
	     }
	     
	     GlModJournalDetails mdetails = new GlModJournalDetails();
	     mdetails.setJrCode(glJournal.getJrCode());
	     mdetails.setJrName(glJournal.getJrName());
	     mdetails.setJrDescription(glJournal.getJrDescription());
	     mdetails.setJrEffectiveDate(glJournal.getJrEffectiveDate());
	     mdetails.setJrDocumentNumber(glJournal.getJrDocumentNumber());
	     mdetails.setJrTotalDebit(TOTAL_DEBIT);
	     mdetails.setJrTotalCredit(TOTAL_CREDIT);
	     mdetails.setJrJcName(glJournal.getGlJournalCategory().getJcName());
	     mdetails.setJrJsName(glJournal.getGlJournalSource().getJsName());
	     mdetails.setJrFcName(glJournal.getGlFunctionalCurrency().getFcName());	     	     	        
	     
	     // get acv status
	     
	     try {	     	
	     	
	     	LocalGlSetOfBook glJournalSetOfBook = glSetOfBookHome.findByDate(glJournal.getJrEffectiveDate(), AD_CMPNY);
	     	
	     	LocalGlAccountingCalendarValue glAccountingCalendarValue =
	     	    glAccountingCalendarValueHome.findByAcCodeAndDate(
	     	    	glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
	     	    	glJournal.getJrEffectiveDate(), AD_CMPNY);
	     	
	     	mdetails.setJrAcvStatus(glAccountingCalendarValue.getAcvStatus());
	     	
	     } catch (Exception ex) {
	     	
	     	throw new EJBException(ex.getMessage());
	     	
	     }
      	  
      	 jrList.add(mdetails);
      	
      }
         
      return jrList;
  
   }
   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("GlJournalBatchSubmitControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }

    
    
    /**
    * @ejb:interface-method view-type="remote"
    **/
    public void executeGlJrBatchSubmit(Integer JR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException,
    	GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlJREffectiveDatePeriodClosedException
		{

       Debug.print("GlJournalBatchSubmitControllerBean executeGlJrBatchSubmit");     

       LocalGlJournalHome glJournalHome = null;  
       LocalAdApprovalHome adApprovalHome = null;
       LocalAdAmountLimitHome adAmountLimitHome = null;
       LocalAdApprovalUserHome adApprovalUserHome = null;
       LocalAdApprovalQueueHome adApprovalQueueHome = null;
       LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;
       LocalAdPreferenceHome adPreferenceHome = null;
     
       // Initialize EJB Home
        
       try {
            
           glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);                     
           adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
           adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
           adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
           adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
           adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
             
                          
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
       	   LocalGlJournal glJournal = null;
       	   
       	   try {
       	   	
       	   	  glJournal = glJournalHome.findByPrimaryKey(JR_CODE);
       	   	
       	   } catch (FinderException ex) {
       	   	
       	   	  throw new GlobalRecordAlreadyDeletedException();
       	   	
       	   }
       	          	   
       	   // validate journal
       	   
       	   if (glJournal.getJrApprovalStatus() != null) {
        			
	    	   if (glJournal.getJrApprovalStatus().equals("APPROVED") ||
	    		   glJournal.getJrApprovalStatus().equals("N/A")) {         		    	
	    		 
	    		   throw new GlobalTransactionAlreadyApprovedException(); 
	    		    	        		    	
	    	   } else if (glJournal.getJrApprovalStatus().equals("PENDING")) {
	    			
	    		   throw new GlobalTransactionAlreadyPendingException();
	    			
	    	   }
	    		
	       }
				
		   if (glJournal.getJrPosted() == EJBCommon.TRUE) {
				
			   throw new GlobalTransactionAlreadyPostedException();
				
		   }
		   
		   // generate approval status
        		       		
           String JR_APPRVL_STATUS = null;
        		
        		        		       		
    	   LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
    		
    	   // check if gl journal approval is enabled
    		
    	   if (adApproval.getAprEnableGlJournal() == EJBCommon.FALSE) {
    			        			        			
    		    JR_APPRVL_STATUS = "N/A";
    			
    	   } else {
    			
    			// get highest coa amount coa line
    			
    			Integer HGHST_COA = null;
    			double HGHST_COA_AMNT = 0d;
    		
        		Collection glHighestJournalLines = glJournal.getGlJournalLines();	        			        		
        		
        		Iterator jlIter = glHighestJournalLines.iterator();
        		
        		while (jlIter.hasNext()) {
        			
        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)jlIter.next();
        			
        			Collection adApprovalCoaLines = glJournalLine.getGlChartOfAccount().getAdApprovalCoaLines();
        			
        			if (!adApprovalCoaLines.isEmpty()) {
        				
        				if (glJournalLine.getJlAmount() > HGHST_COA_AMNT) {
        					
        					HGHST_COA = glJournalLine.getGlChartOfAccount().getCoaCode();
        					HGHST_COA_AMNT = glJournalLine.getJlAmount();	        					
        					
        				}
        				
        			}
        			        			
        		}
        		
        		if (HGHST_COA == null) {
        			
        			JR_APPRVL_STATUS = "N/A";
        		
        		} else  {
    			
        			// check if journal is self approved
        			
        			LocalAdAmountLimit adAmountLimit = null;
        			
        			try {
        				
        				adAmountLimit = adAmountLimitHome.findByCoaCodeAndAuTypeAndUsrName(HGHST_COA, "REQUESTER", glJournal.getJrLastModifiedBy(), AD_CMPNY);       			
        				
        			} catch (FinderException ex) {
        				
        				throw new GlobalNoApprovalRequesterFoundException();
        				
        			}
        			
        			if (HGHST_COA_AMNT <= adAmountLimit.getCalAmountLimit()) {
        				
        				JR_APPRVL_STATUS = "N/A";
        				
        			} else {
        				
        				// for approval, create approval queue
        				
        				 Collection adAmountLimits = adAmountLimitHome.findByCoaCodeAndGreaterThanCalAmountLimit(HGHST_COA, adAmountLimit.getCalAmountLimit(), AD_CMPNY);
        				 
        				 if (adAmountLimits.isEmpty()) {
        				 	
        				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);
        				 	
        				 	if (adApprovalUsers.isEmpty()) {
        				 		
        				 		throw new GlobalNoApprovalApproverFoundException();
        				 		
        				 	}
        				 	        				 	
        				 	Iterator j = adApprovalUsers.iterator();
        				 	
        				 	while (j.hasNext()) {
        				 		
        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
        				 		
        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "GL JOURNAL", glJournal.getJrCode(), 
        				 				glJournal.getJrDocumentNumber(), glJournal.getJrEffectiveDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
        				 		
        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
        				 		
        				 	}        				 	
        				 	        				 	
        				 } else {
        				 	
        				 	boolean isApprovalUsersFound = false;
        				 	
        				 	Iterator i = adAmountLimits.iterator();
        				 	        				 	
        				 	while (i.hasNext()) {
        				 		
        				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();        				 		
        				 		        				 		        				 		        				 		
        				 		if (HGHST_COA_AMNT <= adNextAmountLimit.getCalAmountLimit()) {
        				 			
        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
        				 			
        				 			Iterator j = adApprovalUsers.iterator();
		        				 	
		        				 	while (j.hasNext()) {
		        				 		
		        				 		isApprovalUsersFound = true;
		        				 		
		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
		        				 		
		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "GL JOURNAL", glJournal.getJrCode(), 
		        				 				glJournal.getJrDocumentNumber(), glJournal.getJrEffectiveDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
		        				 		
		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
		        				 		
		        				 	}        				 			
	    				 			        				 			        				 			       				 			
	    				 			break;
		        				 	        				 			
        				 		} else if (!i.hasNext()) {
        				 			
        				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
        				 			
        				 			Iterator j = adApprovalUsers.iterator();
		        				 	
		        				 	while (j.hasNext()) {
		        				 		
		        				 		isApprovalUsersFound = true;
		        				 		
		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
		        				 		
		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "GL JOURNAL", glJournal.getJrCode(), 
		        				 				glJournal.getJrDocumentNumber(), glJournal.getJrEffectiveDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
		        				 		
		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
		        				 		
		        				 	}        				 			
	    				 			        				 			        				 			       				 			
	    				 			break;
        				 			
        				 		}        				 		
        				 		        				 		
        				 		adAmountLimit = adNextAmountLimit;
        				 		
        				 	}
        				 	
        				 	if (!isApprovalUsersFound) {
        				 		
        				 		throw new GlobalNoApprovalApproverFoundException();
        				 		
        				 	}        				 
        				 	
        			    }
        			    
        			    JR_APPRVL_STATUS = "PENDING";
        			    
        			}
    			} 
    				 			        			        			
    	   }  
    	   
    	   LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    	   
    	   if (JR_APPRVL_STATUS != null && JR_APPRVL_STATUS.equals("N/A") && adPreference.getPrfGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
    		
    		   this.executeGlJrPost(glJournal.getJrCode(), glJournal.getJrLastModifiedBy(), AD_BRNCH, AD_CMPNY);
    		
    	   }
        	
           // set journal approval status
        	
           glJournal.setJrApprovalStatus(JR_APPRVL_STATUS); 	      	   
	      
       } catch (GlobalRecordAlreadyDeletedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	         	
       	  
       } catch (GlobalTransactionAlreadyApprovedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       
       } catch (GlobalTransactionAlreadyPendingException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       	   
       } catch (GlobalTransactionAlreadyPostedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	   
       } catch (GlobalNoApprovalRequesterFoundException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	   
       } catch (GlobalNoApprovalApproverFoundException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       
       } catch (GlJREffectiveDatePeriodClosedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
       	                                                
       } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
       }

    }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlJournalBatchControllerBean ejbCreate");
      
   }

   // private methods

   private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("GlJournalBatchSubmitControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}		     
	
	private void executeGlJrPost(Integer JR_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws 
    	GlobalTransactionAlreadyPostedException,
		GlJREffectiveDatePeriodClosedException,
		GlobalRecordAlreadyDeletedException {

       Debug.print("GlJournalBatchSubmitControllerBean executeGlJrPost");

      
       LocalGlSetOfBookHome glSetOfBookHome = null;
       LocalGlJournalHome glJournalHome = null;
       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       LocalGlChartOfAccountHome glChartOfAccountHome = null;
       LocalGlForexLedgerHome glForexLedgerHome = null;
       LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;       
           
       // Initialize EJB Home
        
       try {
            
           glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);       
           glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);          
           glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);          
           glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
           glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
		   	  lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
           glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
		   	  lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
           
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
       	   // get journal
       	
       	   LocalGlJournal glJournal = null;
       	
       	   try {
       	   	
       	   	   glJournal = glJournalHome.findByPrimaryKey(JR_CODE);
       	   	
       	   } catch (FinderException ex) {
       	   	
       	   	   throw new GlobalRecordAlreadyDeletedException();
       	   	
       	   }       	  
       	          	  	 
       	   // validate if period is closed       	  	 
       	          	  	     	
	       LocalGlSetOfBook glSetOfBook = glSetOfBookHome.findByDate(glJournal.getJrEffectiveDate(), AD_CMPNY);
	     	
	       LocalGlAccountingCalendarValue glAccountingCalendarValue =
	     	   glAccountingCalendarValueHome.findByAcCodeAndDate(
	     	     	glSetOfBook.getGlAccountingCalendar().getAcCode(),
	     	    	glJournal.getJrEffectiveDate(), AD_CMPNY);
	     	
	       if (glAccountingCalendarValue.getAcvStatus() == 'C' ||
	           glAccountingCalendarValue.getAcvStatus() == 'P' ||
			   glAccountingCalendarValue.getAcvStatus() == 'N') {
	           		           	
	           throw new GlJREffectiveDatePeriodClosedException(glJournal.getJrName());
	           	
	       }
	       
	       
	       // validate if journal is already posted
	       
	       if (glJournal.getJrPosted() == EJBCommon.TRUE) {
	       	
	       	   throw new GlobalTransactionAlreadyPostedException(glJournal.getJrName());	       	
	       }
	       
	       // post journal
	       
	       Collection glJournalLines = glJournal.getGlJournalLines();
	       
	       Iterator i = glJournalLines.iterator();
	       
	       while (i.hasNext()) {
	       	
	           LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
	           
	           double JL_AMNT = this.convertForeignToFunctionalCurrency(
						      	   glJournal.getGlFunctionalCurrency().getFcCode(),
						      	   glJournal.getGlFunctionalCurrency().getFcName(),
						      	   glJournal.getJrConversionDate(),
						      	   glJournal.getJrConversionRate(),
						      	   glJournalLine.getJlAmount(), AD_CMPNY);
	           
	           // post current to current acv
	             
	           this.post(glAccountingCalendarValue,
	               glJournalLine.getGlChartOfAccount(),
	               true, glJournalLine.getJlDebit(), JL_AMNT, AD_BRNCH, AD_CMPNY);
		 	 
	         
	           // post to subsequent acvs (propagate)
	         
	           Collection glSubsequentAccountingCalendarValues = 
	               glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
	                   glSetOfBook.getGlAccountingCalendar().getAcCode(),
	                   glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
	                 
	           Iterator j = glSubsequentAccountingCalendarValues.iterator();
	         
	           while (j.hasNext()) {
	         	
	         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
	         	       (LocalGlAccountingCalendarValue)j.next();
	         	       
	         	   this.post(glSubsequentAccountingCalendarValue,
	         	       glJournalLine.getGlChartOfAccount(),
	         	       false, glJournalLine.getJlDebit(), JL_AMNT, AD_BRNCH, AD_CMPNY);
	         		         	
	           }
	           	           
	           // post to subsequent years if necessary
	           
	           Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
	           LocalAdCompany  adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		  	  	if (!glSubsequentSetOfBooks.isEmpty() && glSetOfBook.getSobYearEndClosed() == 1) {
		  	  	
			  	  	LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
		  	  	
			  	  	Iterator sobIter = glSubsequentSetOfBooks.iterator();
			  	  	
			  	  	while (sobIter.hasNext()) {
			  	  		
			  	  		LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
			  	  		
			  	  		String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
			  	  		
			  	  		// post to subsequent acvs of subsequent set of book(propagate)
	         
			           Collection glAccountingCalendarValues = 
			               glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
			                 
			           Iterator acvIter = glAccountingCalendarValues.iterator();
			         
			           while (acvIter.hasNext()) {
			         	
			         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
			         	       (LocalGlAccountingCalendarValue)acvIter.next();
			         	       
			         	    if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
				 				ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
			         	       
					         	this.post(glSubsequentAccountingCalendarValue,
					         	     glJournalLine.getGlChartOfAccount(),
					         	     false, glJournalLine.getJlDebit(), JL_AMNT, AD_BRNCH, AD_CMPNY);
					         	     
					        } else { // revenue & expense
					        					             					             
					             this.post(glSubsequentAccountingCalendarValue,
					         	     glRetainedEarningsAccount,
					         	     false, glJournalLine.getJlDebit(), JL_AMNT, AD_BRNCH, AD_CMPNY);					        
					        
					        }
			         		         	
			           }
			  	  		
			  	  	   if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
			  	  		
			  	  	}
			  	  	
		  	  	}
		  	  	
		  	  	// for FOREX revaluation
		  	  	if((glJournal.getGlFunctionalCurrency().getFcCode() !=
		  	  			adCompany.getGlFunctionalCurrency().getFcCode()) &&
					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
		  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
		  	  			glJournal.getGlFunctionalCurrency().getFcCode()))){
		  	  		
		  	  		double CONVERSION_RATE = 1;
		  	  		
		  	  		if (glJournal.getJrConversionRate() != 0 && glJournal.getJrConversionRate() != 1) {
		  	  			
		  	  			CONVERSION_RATE = glJournal.getJrConversionRate();
		  	  			
		  	  		} else if (glJournal.getJrConversionDate() != null){
		  	  			
		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
			  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
								glJournal.getJrConversionDate(), AD_CMPNY);
		  	  			
		  	  		}
		  	  		
		  	  		Collection glForexLedgers = null;
		  	  		
		  	  		try {
		  	  			
		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
		  	  					glJournal.getJrEffectiveDate(), glJournalLine.getGlChartOfAccount().getCoaCode(),
								AD_CMPNY);
		  	  			
		  	  		} catch(FinderException ex) {
		  	  			
		  	  		}
		  	  		
		  	  		LocalGlForexLedger glForexLedger =
		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();
		  	  		
		  	  		int FRL_LN = (glForexLedger != null &&
		  	  				glForexLedger.getFrlDate().compareTo(glJournal.getJrEffectiveDate()) == 0) ?
		  	  						glForexLedger.getFrlLine().intValue() + 1 : 1;
		  	  		
		  	  		// compute balance
		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
		  	  		double FRL_AMNT = glJournalLine.getJlAmount();
		  	  		
		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));  
		  	  		else 
		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);
		  	  		
		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
		  	  		
		  	  		glForexLedger = glForexLedgerHome.create(glJournal.getJrEffectiveDate(), new Integer (FRL_LN),
		  	  				"JOURNAL", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0d, AD_CMPNY);
		  	  		
		  	  		glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
		  	  		
		  	  		// propagate balances
		  	  		try{
		  	  			
		  	  			glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
		  	  					glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
								glForexLedger.getFrlAdCompany());
		  	  			
		  	  		} catch (FinderException ex) {
		  	  			
		  	  		}
		  	  		
		  	  		Iterator itrFrl = glForexLedgers.iterator();
		  	  		
		  	  		while (itrFrl.hasNext()) {
		  	  			
		  	  			glForexLedger = (LocalGlForexLedger) itrFrl.next();
		  	  			FRL_AMNT = glJournalLine.getJlAmount();
		  	  			
		  	  			if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? FRL_AMNT : (- 1 * FRL_AMNT));
		  	  			else
		  	  				FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE  ? (- 1 * FRL_AMNT) : FRL_AMNT);
		  	  			
		  	  			glForexLedger.setFrlBalance( glForexLedger.getFrlBalance() + FRL_AMNT);
		  	  			
		  	  		}
		  	  		
		  	  	}

           }
           
           // set journal post status
           
           glJournal.setJrPosted(EJBCommon.TRUE);
           glJournal.setJrPostedBy(USR_NM);
           glJournal.setJrDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
	       
       	         	
       	  
       } catch (GlobalRecordAlreadyDeletedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       
       } catch (GlJREffectiveDatePeriodClosedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       	   
       } catch (GlobalTransactionAlreadyPostedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;
                                             
       } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
       }

    }
    
   private void post(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
      LocalGlChartOfAccount glChartOfAccount, 
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_BRNCH, Integer AD_CMPNY) {
      	
      Debug.print("GlJournalBatchSubmitControllerBean post");
      
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
              
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {          
               
               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
       	   
	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance = 
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
	               	  
	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
	           
	           
	               	  
	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {				    
				    			
 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
				    
		
			  } else {
	
				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
	
		 	  }
		 	  
		 	  if (isCurrentAcv) { 
		 	 
			 	 if (isDebit == EJBCommon.TRUE) {
			 	 	
		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 				
		 		 } else {
		 		 	
		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 		 }       	   
		 		 
		 	}
       	
       } catch (Exception ex) {
       	
       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());
       	
       }
      
      
   } 
   
   private double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY) 
   throws GlobalConversionDateNotExistException {
   	
   	Debug.print("GlJournalBatchSubmitControllerBean getFrRateByFrNameAndFrDate");
   	
   	LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
   	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
   	LocalAdCompanyHome adCompanyHome = null;
   	
   	// Initialize EJB Home
   	
   	try {
   		
   		glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
		lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
   		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
		lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
   		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
   		
   	} catch (NamingException ex) {
   		
   		throw new EJBException(ex.getMessage());
   		
   	}
   	
   	try {
   		
   		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
   		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
   		
   		double CONVERSION_RATE = 1;
   		
   		// Get functional currency rate
   		
   		if (!FC_NM.equals("USD")) {
   			
   			LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
   				glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
   						CONVERSION_DATE, AD_CMPNY);
   			
   			CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
   			
   		}
   		
   		// Get set of book functional currency rate if necessary
   		
   		if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
   			
   			LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
   				glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
   						CONVERSION_DATE, AD_CMPNY);
   			
   			CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();
   			
   		}
   		
   		return CONVERSION_RATE;
   		
   	} catch (FinderException ex) {	
   		
   		getSessionContext().setRollbackOnly();
   		throw new GlobalConversionDateNotExistException();  
   		
   	} catch (Exception ex) {
   		
   		Debug.printStackTrace(ex);
   		throw new EJBException(ex.getMessage());
   		
   	}
   	
   }

	   
}
