package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.GlJCJournalCategoryAlreadyAssignedException;
import com.ejb.exception.GlJCJournalCategoryAlreadyDeletedException;
import com.ejb.exception.GlJCJournalCategoryAlreadyExistException;
import com.ejb.exception.GlJCNoJournalCategoryFoundException;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlJournalCategoryDetails;

/**
 * @ejb:bean name="GlJournalCategoryControllerEJB"
 *           display-name="Used for setting up journal category"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlJournalCategoryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlJournalCategoryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlJournalCategoryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlJournalCategoryControllerBean extends AbstractSessionBean {

   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlJcAll(Integer AD_CMPNY)
      throws GlJCNoJournalCategoryFoundException {

      Debug.print("GlJournalCategoryControllerBean getGlJcAll");

      /***************************************************************
        Gets all JC        
      ***************************************************************/

      ArrayList jcAllList = new ArrayList();
      Collection glJournalCategories = null;
      
      LocalGlJournalCategoryHome glJournalCategoryHome = null;

      // Initialize EJB Home
        
      try {
            
          glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      try {
         glJournalCategories = glJournalCategoryHome.findJcAll(AD_CMPNY);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glJournalCategories.size() == 0)
         throw new GlJCNoJournalCategoryFoundException();
         
      Iterator i = glJournalCategories.iterator();
      while (i.hasNext()) {
         LocalGlJournalCategory glJournalCategory = (LocalGlJournalCategory) i.next();
         GlJournalCategoryDetails details = new GlJournalCategoryDetails(
            glJournalCategory.getJcCode(), glJournalCategory.getJcName(), 
            glJournalCategory.getJcDescription(), glJournalCategory.getJcReversalMethod());
         jcAllList.add(details);
      }

      return jcAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void addGlJcEntry(GlJournalCategoryDetails details, Integer AD_CMPNY)
      throws GlJCJournalCategoryAlreadyExistException {

      Debug.print("GlJournalCategoryControllerBean addGlJcEntry");

      /**************************
        Adds a JC entry
      **************************/
      
      LocalGlJournalCategoryHome glJournalCategoryHome = null;

      // Initialize EJB Home
        
      try {
            
          glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      try {
      	
        glJournalCategoryHome.findByJcName(details.getJcName(), AD_CMPNY);
        
        getSessionContext().setRollbackOnly();
        throw new GlJCJournalCategoryAlreadyExistException();
        
      } catch (FinderException ex) {
        
      }

      try {
         LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.create(
            details.getJcName(), details.getJcDescription(),
            details.getJcReversalMethod(), AD_CMPNY);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage()); 
      }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlJcEntry(GlJournalCategoryDetails details, Integer AD_CMPNY)
      throws GlJCJournalCategoryAlreadyExistException,
      GlJCJournalCategoryAlreadyAssignedException,
      GlJCJournalCategoryAlreadyDeletedException {

      Debug.print("GlJournalCategoryControllerBean updateGlJcEntry");

      /*******************************
        Updates an existing JC entry
      *******************************/

      LocalGlJournalCategory glJournalCategory = null;
      
      LocalGlJournalCategoryHome glJournalCategoryHome = null;

      // Initialize EJB Home
        
      try {
            
          glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      try {
         glJournalCategory = glJournalCategoryHome.findByPrimaryKey(
            details.getJcCode());
      } catch (FinderException ex){
              throw new GlJCJournalCategoryAlreadyDeletedException();
      } catch (Exception ex) {
        throw new EJBException(ex.getMessage());
      }

      if (hasRelation(glJournalCategory, AD_CMPNY))
         throw new GlJCJournalCategoryAlreadyAssignedException();
      else {
       
         LocalGlJournalCategory glJournalCategory2 = null;

         try {
            glJournalCategory2 = 
               glJournalCategoryHome.findByJcName(details.getJcName(), AD_CMPNY);
         } catch (FinderException ex) {
            glJournalCategory.setJcName(details.getJcName());
            glJournalCategory.setJcDescription(details.getJcDescription());
            glJournalCategory.setJcReversalMethod(details.getJcReversalMethod());
         } catch (Exception ex) {
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
         }

         if(glJournalCategory2 != null && !glJournalCategory.getJcCode().equals(glJournalCategory2.getJcCode())) {
            getSessionContext().setRollbackOnly();
            throw new GlJCJournalCategoryAlreadyExistException();
         } else 
            if(glJournalCategory2 != null && glJournalCategory.getJcCode().equals(glJournalCategory2.getJcCode())) {
               glJournalCategory.setJcDescription(details.getJcDescription());
               glJournalCategory.setJcReversalMethod(details.getJcReversalMethod());
         }
      }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteGlJcEntry(Integer JC_CODE, Integer AD_CMPNY) 
      throws GlJCJournalCategoryAlreadyDeletedException,
      GlJCJournalCategoryAlreadyAssignedException {

      Debug.print("GlJournalCategoryControllerBean deleteGlJcEntry");

      /*******************************
        Deletes an existing JC entry      
      *******************************/                
      
      LocalGlJournalCategoryHome glJournalCategoryHome = null;

      // Initialize EJB Home
        
      try {
            
          glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      LocalGlJournalCategory glJournalCategory = null;

      try {
         glJournalCategory = glJournalCategoryHome.findByPrimaryKey(JC_CODE);
      } catch (FinderException ex) {
         throw new GlJCJournalCategoryAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (hasRelation(glJournalCategory, AD_CMPNY))
         throw new GlJCJournalCategoryAlreadyAssignedException();
      else {
         try {
            glJournalCategory.remove();
         } catch (RemoveException ex) {
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
         } catch (Exception ex) {
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
         }
      }
   }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlJournalCategoryControllerBean ejbCreate");
      
   }

   // private methods

   private boolean hasRelation(LocalGlJournalCategory glJournalCategory, Integer AD_CMPNY) {

      Debug.print("GlJournalCategoryControllerBean hasRelation");

      if (!glJournalCategory.getGlSuspenseAccounts().isEmpty() ||
          !glJournalCategory.getGlRecurringJournals().isEmpty() ||
          !glJournalCategory.getGlJournals().isEmpty()) {
      	
      	return true;
      	
      } 
      
      return false;

    }

    
}
