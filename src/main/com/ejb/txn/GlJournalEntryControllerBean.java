 package com.ejb.txn;
 
 import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalCoaLineHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlJLChartOfAccountNotFoundException;
import com.ejb.exception.GlJLChartOfAccountPermissionDeniedException;
import com.ejb.exception.GlJRDateReversalNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlJREffectiveDateViolationException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountRange;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlOrganization;
import com.ejb.gl.LocalGlResponsibility;
import com.ejb.gl.LocalGlResponsibilityHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.gl.LocalGlTransactionCalendarValue;
import com.ejb.gl.LocalGlTransactionCalendarValueHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModJournalDetails;
import com.util.GlModJournalLineDetails;
 
 /**
  * @ejb:bean name="GlJournalEntryControllerEJB"
  *           display-name="used for entering journals"
  *           type="Stateless"
  *           view-type="remote"
  *           jndi-name="ejb/GlJournalEntryControllerEJB"
  *
  * @ejb:interface remote-class="com.ejb.txn.GlJournalEntryController"
  *                extends="javax.ejb.EJBObject"
  *
  * @ejb:home remote-class="com.ejb.txn.GlJournalEntryControllerHome"
  *           extends="javax.ejb.EJBHome"
  *
  * @ejb:transaction type="Required"
  *
  * @ejb:security-role-ref role-name="gluser"
  *                        role-link="gluserlink"
  *
  * @ejb:permission role-name="gluser"
  * 
  */
 
 public class GlJournalEntryControllerBean extends AbstractSessionBean {
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean getGlFcAllWithDefault");
 		
 		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		
 		Collection glFunctionalCurrencies = null;
 		
 		LocalGlFunctionalCurrency glFunctionalCurrency = null;
 		LocalAdCompany adCompany = null;
 		
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			
 			glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
 					EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);
 			
 		} catch (Exception ex) {
 			
 			throw new EJBException(ex.getMessage());
 		}
 		
 		if (glFunctionalCurrencies.isEmpty()) {
 			
 			return null;
 			
 		}
 		
 		Iterator i = glFunctionalCurrencies.iterator();
 		
 		while (i.hasNext()) {
 			
 			glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
 			
 			GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
 					glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
					adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
							EJBCommon.TRUE : EJBCommon.FALSE);
 			
 			list.add(mdetails);
 			
 		}
 		
 		return list;
 		
 	}
 	
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getGlJcAll(Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean getGlJcAll");
 		
 		LocalGlJournalCategoryHome glJournalCategoryHome = null;               
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}        
 		
 		try {
 			
 			Collection glJournalCategories = glJournalCategoryHome.findJcAll(AD_CMPNY);
 			
 			Iterator i = glJournalCategories.iterator();
 			
 			while (i.hasNext()) {
 				
 				LocalGlJournalCategory glJournalCategory = (LocalGlJournalCategory)i.next();
 				
 				list.add(glJournalCategory.getJcName());
 				
 			}
 			
 			return list;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public short getAdPrfGlJournalLineNumber(Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean getAdPrfGlJournalLineNumber");
 		
 		LocalAdPreferenceHome adPreferenceHome = null;
 		
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		
 		try {
 			
 			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
 			
 			return adPreference.getPrfGlJournalLineNumber();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public GlModJournalDetails getGlJrByJrCode(Integer JR_CODE, Integer AD_CMPNY) 
 	throws GlobalNoRecordFoundException {
 		
 		Debug.print("GlJournalEntryControllerBean getGlJrByJrCode");
 		
 		LocalGlJournalHome glJournalHome = null;   
 		LocalGlJournalLineHome glJournalLineHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		LocalGenValueSetValueHome genValueSetValueHome = null;
 		LocalGenSegmentHome genSegmentHome = null;
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
 			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
 			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
 			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}        
 		
 		try {
 			
 			LocalGlJournal glJournal = null;
 			
 			
 			try {
 				
 				glJournal = glJournalHome.findByPrimaryKey(JR_CODE);
 				
 			} catch (FinderException ex) {
 				
 				throw new GlobalNoRecordFoundException();
 				
 			}
 			
 			ArrayList jrJlList = new ArrayList();
 			
 			// get journal lines
 			
 			Collection glJournalLines = glJournalLineHome.findByJrCode(glJournal.getJrCode(), AD_CMPNY);      	            
 			
 			short lineNumber = 1;
 			
 			Iterator i = glJournalLines.iterator();
 			
 			double TOTAL_DEBIT = 0;
 			double TOTAL_CREDIT = 0;
 			
 			while (i.hasNext()) {
 				
 				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();
 				
 				if (glJournalLine.getJlDebit() == EJBCommon.TRUE) {
 					
 					TOTAL_DEBIT += this.convertForeignToFunctionalCurrency(
 							glJournal.getGlFunctionalCurrency().getFcCode(),
							glJournal.getGlFunctionalCurrency().getFcName(),
							glJournal.getJrConversionDate(),
							glJournal.getJrConversionRate(),
							glJournalLine.getJlAmount(), AD_CMPNY);
 					
 				} else {
 					
 					TOTAL_CREDIT += this.convertForeignToFunctionalCurrency(
 							glJournal.getGlFunctionalCurrency().getFcCode(),
							glJournal.getGlFunctionalCurrency().getFcName(),
							glJournal.getJrConversionDate(),
							glJournal.getJrConversionRate(),
							glJournalLine.getJlAmount(), AD_CMPNY);
 				}        		
 				
 				GlModJournalLineDetails mdetails = new GlModJournalLineDetails();
 				
 				mdetails.setJlCode(glJournalLine.getJlCode());
 				mdetails.setJlLineNumber(lineNumber);
 				mdetails.setJlDebit(glJournalLine.getJlDebit());
 				mdetails.setJlAmount(glJournalLine.getJlAmount());
 				mdetails.setJlCoaAccountNumber(glJournalLine.getGlChartOfAccount().getCoaAccountNumber());
 				mdetails.setJlCoaAccountDescription(glJournalLine.getGlChartOfAccount().getCoaAccountDescription());
 				mdetails.setJlDescription(glJournalLine.getJlDescription());
 				
 				jrJlList.add(mdetails);
 				
 				lineNumber++;
 				
 			}
 			
 			GlModJournalDetails mJrDetails = new GlModJournalDetails();
 			
 			mJrDetails.setJrCode(glJournal.getJrCode());
 			mJrDetails.setJrName(glJournal.getJrName());
 			mJrDetails.setJrDescription(glJournal.getJrDescription());
 			mJrDetails.setJrEffectiveDate(glJournal.getJrEffectiveDate());
 			mJrDetails.setJrDateReversal(glJournal.getJrDateReversal());
 			mJrDetails.setJrDocumentNumber(glJournal.getJrDocumentNumber());
 			mJrDetails.setJrConversionDate(glJournal.getJrConversionDate());
 			mJrDetails.setJrConversionRate(glJournal.getJrConversionRate());
 			mJrDetails.setJrApprovalStatus(glJournal.getJrApprovalStatus());
 			mJrDetails.setJrReasonForRejection(glJournal.getJrReasonForRejection());
 			mJrDetails.setJrPosted(glJournal.getJrPosted());
 			mJrDetails.setJrReversed(glJournal.getJrReversed());
 			mJrDetails.setJrCreatedBy(glJournal.getJrCreatedBy());
 			mJrDetails.setJrDateCreated(glJournal.getJrDateCreated());
 			mJrDetails.setJrLastModifiedBy(glJournal.getJrLastModifiedBy());
 			mJrDetails.setJrDateLastModified(glJournal.getJrDateLastModified());
 			mJrDetails.setJrApprovedRejectedBy(glJournal.getJrApprovedRejectedBy());
 			mJrDetails.setJrDateApprovedRejected(glJournal.getJrDateApprovedRejected());
 			mJrDetails.setJrPostedBy(glJournal.getJrPostedBy());
 			mJrDetails.setJrDatePosted(glJournal.getJrDatePosted());
 			mJrDetails.setJrJcName(glJournal.getGlJournalCategory().getJcName());
 			mJrDetails.setJrFcName(glJournal.getGlFunctionalCurrency().getFcName());
 			mJrDetails.setJrJsName(glJournal.getGlJournalSource().getJsName());
 			mJrDetails.setJrTotalDebit(TOTAL_DEBIT);
 			mJrDetails.setJrTotalCredit(TOTAL_CREDIT);        
 			mJrDetails.setJrFrozen(glJournal.getGlJournalSource().getJsFreezeJournal());
 			mJrDetails.setJrJlList(jrJlList);
 			mJrDetails.setJrJbName(glJournal.getGlJournalBatch() != null ? glJournal.getGlJournalBatch().getJbName() : null);
 			
 			return mJrDetails;
 			
 		} catch (GlobalNoRecordFoundException ex) {
 			
 			throw ex;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 **/
 	public Integer saveGlJrEntry(com.util.GlJournalDetails details, String JC_NM, 
 			String FC_NM, Integer RES_CODE, String JS_NM, String JB_NM, ArrayList jlList, boolean isDraft, 
			Integer AD_BRNCH, Integer AD_CMPNY) throws        
			GlobalRecordAlreadyDeletedException,
			GlJREffectiveDateNoPeriodExistException,
			GlJREffectiveDatePeriodClosedException,
			GlJREffectiveDateViolationException,		
			GlobalDocumentNumberNotUniqueException,
			GlJRDateReversalNoPeriodExistException,				
			GlobalConversionDateNotExistException,
			GlJLChartOfAccountNotFoundException,
			GlJLChartOfAccountPermissionDeniedException,
			GlobalTransactionAlreadyApprovedException,
			GlobalTransactionAlreadyPendingException,
			GlobalTransactionAlreadyPostedException,
			GlobalNoApprovalRequesterFoundException,
			GlobalNoApprovalApproverFoundException,
			GlobalNoRecordFoundException {
 		
 		Debug.print("GlJournalEntryControllerBean saveGlJrEntry");
 		
 		LocalGlJournalHome glJournalHome = null;        
 		LocalGlJournalBatchHome glJournalBatchHome = null;
 		LocalGlSetOfBookHome glSetOfBookHome = null;  
 		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
 		LocalGlTransactionCalendarValueHome glTransactionCalendarValueHome = null;
 		LocalGlJournalSourceHome glJournalSourceHome = null;        
 		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
 		LocalGlJournalCategoryHome glJournalCategoryHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
 		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
 		LocalAdApprovalHome adApprovalHome = null;
 		LocalAdAmountLimitHome adAmountLimitHome = null;
 		LocalAdApprovalUserHome adApprovalUserHome = null;
 		LocalAdApprovalQueueHome adApprovalQueueHome = null;
 		LocalAdApprovalCoaLineHome adApprovalCoaLineHome = null;
 		LocalAdPreferenceHome adPreferenceHome = null;
 		LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
 		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		LocalGlJournal glJournal = null;
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
 			glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
 			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
 			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
 			glTransactionCalendarValueHome = (LocalGlTransactionCalendarValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlTransactionCalendarValueHome.JNDI_NAME, LocalGlTransactionCalendarValueHome.class);              
 			glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);              
 			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);              
 			glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);              
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);              
 			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);              
 			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
 			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
 			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
 			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
 			adApprovalCoaLineHome = (LocalAdApprovalCoaLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdApprovalCoaLineHome.JNDI_NAME, LocalAdApprovalCoaLineHome.class);
 			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
 			glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
 			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}       
 		
 		// validate if journal is already deleted
 		
 		try {
 			
 			if (details.getJrCode() != null) {
 				
 				glJournal = glJournalHome.findByPrimaryKey(details.getJrCode());
 				
 			}
 			
 		} catch (FinderException ex) {
 			
 			throw new GlobalRecordAlreadyDeletedException();	 		
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}                				
 		
 		// validate if effective date has no period and period is closed
 		
 		try {
 			System.out.println("details.getJrEffectiveDate(: " +details.getJrEffectiveDate());
 			System.out.println("AD_CMPNY: " +AD_CMPNY);
 			LocalGlSetOfBook glJournalSetOfBook = glSetOfBookHome.findByDate(details.getJrEffectiveDate(), AD_CMPNY);
 			
 			LocalGlAccountingCalendarValue glAccountingCalendarValue = 
 				glAccountingCalendarValueHome.findByAcCodeAndDate(
 						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), details.getJrEffectiveDate(), AD_CMPNY);
 			
 			
 			if (!isDraft && (glAccountingCalendarValue.getAcvStatus() == 'N' ||
 					glAccountingCalendarValue.getAcvStatus() == 'C' ||
					glAccountingCalendarValue.getAcvStatus() == 'P')) {
 				
 				throw new GlJREffectiveDatePeriodClosedException();
 				
 			}
 			
 		} catch (GlJREffectiveDatePeriodClosedException ex) {
 			
 			throw ex;
 			
 		} catch (FinderException ex) {
 			
 			throw new GlJREffectiveDateNoPeriodExistException();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		
 		// validate effective date violation
 		
 		try {
 			
 			LocalGlSetOfBook glJournalSetOfBook = glSetOfBookHome.findByDate(details.getJrEffectiveDate(), AD_CMPNY);
 			
 			LocalGlTransactionCalendarValue glTransactionCalendarValue = 
 				glTransactionCalendarValueHome.findByTcCodeAndTcvDate(glJournalSetOfBook.getGlTransactionCalendar().getTcCode(),
 						details.getJrEffectiveDate(), AD_CMPNY);
 			
 			LocalGlJournalSource glValidateJournalSource = glJournalSourceHome.findByJsName(JS_NM, AD_CMPNY);          
 			
 			if (glTransactionCalendarValue.getTcvBusinessDay() == EJBCommon.FALSE &&
 					glValidateJournalSource.getJsEffectiveDateRule() == 'F') {
 				
 				throw new GlJREffectiveDateViolationException();
 				
 			}
 			
 		} catch (GlJREffectiveDateViolationException ex) {      	  
 			
 			throw ex;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		
 		// validate if document number is unique document number is automatic then set next sequence
 		
 		try {
 			
 			LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
			
 			if (details.getJrCode() == null) {
 				
 				try {
 					
 					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("GL JOURNAL", AD_CMPNY);
 					
 				} catch (FinderException ex) {

 				}
 				
 				try {
 					
 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
 					
 				} catch (FinderException ex) {
 					
 				}
 				
 				LocalGlJournal glExistingJournal = null;
 				
 				try {
 					
 					glExistingJournal = glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode (
 							details.getJrDocumentNumber(), "MANUAL", AD_BRNCH, AD_CMPNY);
 					
 				} catch (FinderException ex) { 
 					
 				}
 				
 				if (glExistingJournal != null) {
 					
 					throw new GlobalDocumentNumberNotUniqueException();
 					
 				}
 				
 				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
 						(details.getJrDocumentNumber() == null || details.getJrDocumentNumber().trim().length() == 0)) {
 					
 					while (true) {
 						
 						if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
 							
 							try {
 								
 								glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
 								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
 								
 							} catch (FinderException ex) {
 								
 								details.setJrDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
 								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
 								break;
 								
 							}
 							
 						} else {
 							
 							try {
 								
 								glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
 								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
 								
 							} catch (FinderException ex) {
 								
 								details.setJrDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
 								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
 								break;
 								
 							}
 							
 						}
 						
 					}		            
 					
 				}
 				
 			} else {
 				
 				LocalGlJournal glExistingJournal = null;
 				
 				try {
 					
 					glExistingJournal = glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(
 							details.getJrDocumentNumber(), "MANUAL", AD_BRNCH, AD_CMPNY);
 					
 				} catch (FinderException ex) { 
 				
 				}
 				
 				if (glExistingJournal != null && 
 						!glExistingJournal.getJrCode().equals(details.getJrCode())) {
 					
 					throw new GlobalDocumentNumberNotUniqueException();
 					
 				}
 				
 				if (glJournal.getJrDocumentNumber() != details.getJrDocumentNumber() &&
 						(details.getJrDocumentNumber() == null || details.getJrDocumentNumber().trim().length() == 0)) {
 					
 					details.setJrDocumentNumber(glJournal.getJrDocumentNumber());
 					
 				}
 				
 			}
 			
 		} catch (GlobalDocumentNumberNotUniqueException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		// validate if date reversal has period
 		
 		try {	    	
 			
 			if (details.getJrDateReversal() != null) {
 				
 				LocalGlSetOfBook glJrDateReversalSetOfBook = glSetOfBookHome.findByDate(details.getJrDateReversal(), AD_CMPNY);
 				
 				LocalGlAccountingCalendarValue glReversalPeriodAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndDate(
 						glJrDateReversalSetOfBook.getGlAccountingCalendar().getAcCode(), details.getJrDateReversal(), AD_CMPNY);
 				
 			}
 			
 		} catch (FinderException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw new GlJRDateReversalNoPeriodExistException();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		// validate if conversion date exists
 		
 		try {
 			
 			if (details.getJrConversionDate() != null) {

				LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 				LocalGlFunctionalCurrency glValidateFunctionalCurrency =
 					glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

 				if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {
 				
 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = 
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
 							details.getJrConversionDate(), AD_CMPNY);
 					
 				} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){
 					
 					LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(
 							adCompany.getGlFunctionalCurrency().getFcCode(), details.getJrConversionDate(), AD_CMPNY);

 				}

 			}	          
 			
 		} catch (FinderException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw new GlobalConversionDateNotExistException();
 			
 		} catch (Exception ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		// validate if journal is already posted, approved or pending
 		
 		try {
 			
 			if (details.getJrCode() != null) {
 				
 				if (glJournal.getJrApprovalStatus() != null) {
 					
 					if (glJournal.getJrApprovalStatus().equals("APPROVED") ||
 							glJournal.getJrApprovalStatus().equals("N/A")) {         		    	
 						
 						throw new GlobalTransactionAlreadyApprovedException(); 
 						
 					} else if (glJournal.getJrApprovalStatus().equals("PENDING")) {
 						
 						throw new GlobalTransactionAlreadyPendingException();
 						
 					}
 					
 				}
 				
 				if (glJournal.getJrPosted() == EJBCommon.TRUE) {
 					
 					throw new GlobalTransactionAlreadyPostedException();
 					
 				}
 				
 			}
 			
 		} catch (GlobalTransactionAlreadyApprovedException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (GlobalTransactionAlreadyPendingException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (GlobalTransactionAlreadyPostedException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (Exception ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		
 		// create journal
 		
 		
 		GlModJournalLineDetails mJlDetails = null;
 		
 		try {
 			
 			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
 			
 			if (details.getJrCode() == null) {    
 				
 				glJournal = glJournalHome.create(
 						details.getJrName(), details.getJrDescription(),
						details.getJrEffectiveDate(), 0.0d,
						details.getJrDateReversal(), details.getJrDocumentNumber(),
						details.getJrConversionDate(), details.getJrConversionRate(),
						null, null, 'N', EJBCommon.FALSE, EJBCommon.FALSE,
						details.getJrCreatedBy(), details.getJrDateCreated(),
						details.getJrLastModifiedBy(), details.getJrDateLastModified(),
						null, null, null, null, null, null, EJBCommon.FALSE, details.getJrReferenceNumber(), 
						AD_BRNCH, AD_CMPNY);       	    	
 				
 				
 			} else {
 				
 				glJournal.setJrName(details.getJrName());
 				glJournal.setJrDescription(details.getJrDescription());
 				glJournal.setJrEffectiveDate(details.getJrEffectiveDate());
 				glJournal.setJrDateReversal(details.getJrDateReversal());
 				glJournal.setJrDocumentNumber(details.getJrDocumentNumber());
 				glJournal.setJrConversionDate(details.getJrConversionDate());
 				glJournal.setJrConversionRate(details.getJrConversionRate());
 				glJournal.setJrLastModifiedBy(details.getJrLastModifiedBy());
 				glJournal.setJrDateLastModified(details.getJrDateLastModified());
 				glJournal.setJrReasonForRejection(null);
 				
 			}    	
 			
 			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName(JS_NM, AD_CMPNY);
 			glJournal.setGlJournalSource(glJournalSource);
 			
 			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
 			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
 			
 			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName(JC_NM, AD_CMPNY);
 			glJournal.setGlJournalCategory(glJournalCategory);
 			
 			try {
 				
 				LocalGlJournalBatch glJournalBatch = glJournalBatchHome.findByJbName(JB_NM, AD_BRNCH, AD_CMPNY);
 				glJournal.setGlJournalBatch(glJournalBatch);
 				
 			} catch (FinderException ex) {
 				
 			}
 			
 			
 			// remove all journal lines
 			
 			Collection glJournalLines = glJournal.getGlJournalLines();
 			
 			Iterator i = glJournalLines.iterator();     	  
 			
 			while (i.hasNext()) {
 				
 				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();
 				
 				i.remove();
 				
 				glJournalLine.remove();
 				
 			}
 			
 			// add new journal lines
 			
 			double TOTAL_DEBIT = 0;
 			double TOTAL_CREDIT = 0;
 			short lineNumber = 0;	  	    
 			
 			i = jlList.iterator();
 			
 			while (i.hasNext()) {
 				
 				mJlDetails = (GlModJournalLineDetails) i.next();
 				
 				if (mJlDetails.getJlDebit() == 1) {
 					
 					TOTAL_DEBIT += mJlDetails.getJlAmount();
 					
 				} else {
 					
 					TOTAL_CREDIT += mJlDetails.getJlAmount();
 					
 				}
 				
 				TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, this.getGlFcPrecisionUnit(AD_CMPNY));
 				TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, this.getGlFcPrecisionUnit(AD_CMPNY));
 				
 				lineNumber = mJlDetails.getJlLineNumber();
 				
 				this.addGlJlEntry(mJlDetails, glJournal, RES_CODE, AD_BRNCH, AD_CMPNY);
 				
 			}
 			
 			if (!(adPreference.getPrfAllowSuspensePosting() == 1) && TOTAL_DEBIT != TOTAL_CREDIT) {
 				
 				try {
 					
 					LocalGlSuspenseAccount glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName(JS_NM, JC_NM, AD_CMPNY);
 					String coaAccountNumber = glSuspenseAccount.getGlChartOfAccount().getCoaAccountNumber();
 					
 					GlModJournalLineDetails jlModDetails = new GlModJournalLineDetails();
 					
 					jlModDetails.setJlLineNumber(++lineNumber);	  	    		
 					jlModDetails.setJlCoaAccountNumber(coaAccountNumber);
 					
 					double AMOUNT = 0;
 					
 					if(TOTAL_DEBIT < TOTAL_CREDIT) {
 						
 						AMOUNT = TOTAL_CREDIT - TOTAL_DEBIT;
 						jlModDetails.setJlDebit(EJBCommon.TRUE);
 						jlModDetails.setJlAmount(AMOUNT);
 						this.addGlJlEntry(jlModDetails, glJournal, RES_CODE, AD_BRNCH, AD_CMPNY);
 						
 					} else if(TOTAL_DEBIT > TOTAL_CREDIT) {
 						
 						AMOUNT = TOTAL_DEBIT - TOTAL_CREDIT;
 						jlModDetails.setJlDebit(EJBCommon.FALSE);
 						jlModDetails.setJlAmount(AMOUNT);
 						this.addGlJlEntry(jlModDetails, glJournal, RES_CODE, AD_BRNCH, AD_CMPNY);
 						
 					}
 					
 				} catch(FinderException ex) {
 					
 					throw new GlobalNoRecordFoundException();
 					
 				}
 				
 			}
 			
 			// generate approval status
 			
 			String JR_APPRVL_STATUS = null;
 			
 			if (!isDraft) {
 				
 				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
 				
 				// check if gl journal approval is enabled
 				
 				if (adApproval.getAprEnableGlJournal() == EJBCommon.FALSE) {
 					
 					JR_APPRVL_STATUS = "N/A";
 					
 				} else {
 					
 					// get highest coa amount coa line
 					
 					Integer HGHST_COA = null;
 					double HGHST_COA_AMNT = 0d;
 					
 					Collection glHighestJournalLines = glJournal.getGlJournalLines();	        			        		
 					
 					Iterator jlIter = glHighestJournalLines.iterator();
 					
 					while (jlIter.hasNext()) {
 						
 						LocalGlJournalLine glJournalLine = (LocalGlJournalLine)jlIter.next();
 						
 						Collection adApprovalCoaLines = glJournalLine.getGlChartOfAccount().getAdApprovalCoaLines();
 						
 						if (!adApprovalCoaLines.isEmpty()) {
 							
 							if (glJournalLine.getJlAmount() > HGHST_COA_AMNT) {
 								
 								HGHST_COA = glJournalLine.getGlChartOfAccount().getCoaCode();
 								HGHST_COA_AMNT = glJournalLine.getJlAmount();	        					
 								
 							}
 							
 						}
 						
 					}
 					
 					if (HGHST_COA == null) {
 						
 						JR_APPRVL_STATUS = "N/A";
 						
 					} else  {
 						
 						// check if journal is self approved
 						
 						LocalAdAmountLimit adAmountLimit = null;
 						
 						try {
 							
 							adAmountLimit = adAmountLimitHome.findByCoaCodeAndAuTypeAndUsrName(HGHST_COA, "REQUESTER", details.getJrLastModifiedBy(), AD_CMPNY);       			
 							
 						} catch (FinderException ex) {
 							
 							throw new GlobalNoApprovalRequesterFoundException();
 							
 						}
 						
 						if (HGHST_COA_AMNT <= adAmountLimit.getCalAmountLimit()) {
 							
 							JR_APPRVL_STATUS = "N/A";
 							
 						} else {
 							
 							// for approval, create approval queue
 							
 							Collection adAmountLimits = adAmountLimitHome.findByCoaCodeAndGreaterThanCalAmountLimit(HGHST_COA, adAmountLimit.getCalAmountLimit(), AD_CMPNY);
 							
 							if (adAmountLimits.isEmpty()) {
 								
 								Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);
 								
 								if (adApprovalUsers.isEmpty()) {
 									
 									throw new GlobalNoApprovalApproverFoundException();
 									
 								}
 								
 								Iterator j = adApprovalUsers.iterator();
 								
 								while (j.hasNext()) {
 									
 									LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
 									
 									LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "GL JOURNAL", glJournal.getJrCode(), 
 											glJournal.getJrDocumentNumber(), glJournal.getJrEffectiveDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
 									
 									adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
 									
 								}        				 	
 								
 							} else {
 								
 								boolean isApprovalUsersFound = false;
 								
 								i = adAmountLimits.iterator();
 								
 								while (i.hasNext()) {
 									
 									LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();        				 		
 									
 									if (HGHST_COA_AMNT <= adNextAmountLimit.getCalAmountLimit()) {
 										
 										Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
 										
 										Iterator j = adApprovalUsers.iterator();
 										
 										while (j.hasNext()) {
 											
 											isApprovalUsersFound = true;
 											
 											LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
 											
 											LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "GL JOURNAL", glJournal.getJrCode(), 
 													glJournal.getJrDocumentNumber(), glJournal.getJrEffectiveDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
 											
 											adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
 											
 										}        				 			
 										
 										break;
 										
 									} else if (!i.hasNext()) {
 										
 										Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
 										
 										Iterator j = adApprovalUsers.iterator();
 										
 										while (j.hasNext()) {
 											
 											isApprovalUsersFound = true;
 											
 											LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
 											
 											LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "GL JOURNAL", glJournal.getJrCode(), 
 													glJournal.getJrDocumentNumber(), glJournal.getJrEffectiveDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
 											
 											adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
 											
 										}        				 			
 										
 										break;
 										
 									}        				 		
 									
 									adAmountLimit = adNextAmountLimit;
 									
 								}
 								
 								if (!isApprovalUsersFound) {
 									
 									throw new GlobalNoApprovalApproverFoundException();
 									
 								}        				 
 								
 							}
 							
 							JR_APPRVL_STATUS = "PENDING";
 							
 						}
 					} 
 					
 				}        		        		        		
 			}
 			
 			if (JR_APPRVL_STATUS != null && JR_APPRVL_STATUS.equals("N/A") && adPreference.getPrfGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
 				
 				this.executeGlJrPost(glJournal.getJrCode(), glJournal.getJrLastModifiedBy(), AD_CMPNY);
 				
 			}
 			
 			// set journal approval status
 			
 			glJournal.setJrApprovalStatus(JR_APPRVL_STATUS);
 			
 			return glJournal.getJrCode();
 			
 		} catch (GlJLChartOfAccountNotFoundException ex) {
 			
 			getSessionContext().setRollbackOnly();      	  
 			throw new GlJLChartOfAccountNotFoundException(String.valueOf(mJlDetails.getJlLineNumber()));
 			
 		} catch (GlJLChartOfAccountPermissionDeniedException ex) {	 	
 			
 			getSessionContext().setRollbackOnly();      	  
 			throw new GlJLChartOfAccountPermissionDeniedException(String.valueOf(mJlDetails.getJlLineNumber()));
 			
 		} catch (GlobalNoApprovalRequesterFoundException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (GlobalNoApprovalApproverFoundException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex; 
 			
 		} catch (GlobalRecordAlreadyDeletedException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (GlJREffectiveDatePeriodClosedException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (GlobalTransactionAlreadyPostedException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (GlobalNoRecordFoundException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 **/
 	public void deleteGlJrEntry(Integer JR_CODE, String AD_USR, Integer AD_CMPNY) throws 
	GlobalRecordAlreadyDeletedException {
 		
 		Debug.print("GlJournalEntryControllerBean deleteGlJrEntry");
 		
 		LocalGlJournalHome glJournalHome = null; 
 		LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
 			adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);  
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}        
 		
 		try {
 			
 			LocalGlJournal glJournal = glJournalHome.findByPrimaryKey(JR_CODE);
 			
 			double total = 0d;
 			Collection glJournalLines = glJournal.getGlJournalLines();
 			Iterator i = glJournalLines.iterator();
 			while (i.hasNext()) { 				
 				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();
 				if (glJournalLine.getJlDebit() == EJBCommon.TRUE) { 					
 					total  += glJournalLine.getJlAmount(); 					
 				} 				
 			}
 			
 			
 			adDeleteAuditTrailHome.create("GL JOURNAL", glJournal.getJrEffectiveDate(), glJournal.getJrDocumentNumber(), glJournal.getJrReferenceNumber(),
 					total, AD_USR, new Date(), AD_CMPNY);
 			
 			glJournal.remove();
 			
 		} catch (FinderException ex) {	
 			
 			getSessionContext().setRollbackOnly();
 			throw new GlobalRecordAlreadyDeletedException();      	
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean getGlFcPrecisionUnit");
 		
 		
 		LocalAdCompanyHome adCompanyHome = null;
 		
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			
 			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getAdApprovalNotifiedUsersByJrCode(Integer JR_CODE, Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean getAdApprovalNotifiedUsersByJrCode");
 		
 		
 		LocalAdApprovalQueueHome adApprovalQueueHome = null;
 		LocalGlJournalHome glJournalHome = null;
 		
 		ArrayList list = new ArrayList();
 		
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);       
 			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			LocalGlJournal glJournal = glJournalHome.findByPrimaryKey(JR_CODE);
 			
 			if (glJournal.getJrPosted() == EJBCommon.TRUE) {
 				
 				list.add("DOCUMENT POSTED");
 				return list;
 				
 			}
 			
 			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("GL JOURNAL", JR_CODE, AD_CMPNY);
 			
 			Iterator i = adApprovalQueues.iterator();
 			
 			while(i.hasNext()) {
 				
 				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
 				
 				list.add(adApprovalQueue.getAdUser().getUsrDescription());
 				
 			}
 			
 			return list;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public byte getAdPrfEnableGlJournalBatch(Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean getAdPrfApEnableApVoucherBatch");
 		
 		LocalAdPreferenceHome adPreferenceHome = null;
 		
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		
 		try {
 			
 			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
 			
 			return adPreference.getPrfEnableGlJournalBatch();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public byte getAdPrfEnableAllowSuspensePosting(Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean getAdPrfEnableAllowSuspensePosting");
 		
 		LocalAdPreferenceHome adPreferenceHome = null;
 		
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		
 		try {
 			
 			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
 			
 			return adPreference.getPrfAllowSuspensePosting();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getGlOpenJbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean getGlOpenJbAll");
 		
 		LocalGlJournalBatchHome glJournalBatchHome = null;               
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}        
 		
 		try {
 			
 			Collection glJournalBatches = glJournalBatchHome.findOpenJbAll(AD_BRNCH, AD_CMPNY);
 			
 			Iterator i = glJournalBatches.iterator();
 			
 			while (i.hasNext()) {
 				
 				LocalGlJournalBatch glJournalBatch = (LocalGlJournalBatch)i.next();
 				
 				list.add(glJournalBatch.getJbName());
 				
 			}
 			
 			return list;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}      
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY) 
 	throws GlobalConversionDateNotExistException {
 		
 		Debug.print("GlJournalEntryControllerBean getFrRateByFrNameAndFrDate");
 		
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
 		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
 			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
 			
 			double CONVERSION_RATE = 1;
 			
 			// Get functional currency rate
 			
 			if (!FC_NM.equals("USD")) {
 				
 				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);
 				
 				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
 				
 			}

 			// Get set of book functional currency rate if necessary

 			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
 				
 				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
 					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
 							CONVERSION_DATE, AD_CMPNY);
 				
 				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();
 				
 			}

 			return CONVERSION_RATE;
 			
 		} catch (FinderException ex) {	
 			
 			getSessionContext().setRollbackOnly();
 			throw new GlobalConversionDateNotExistException();  
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	// private methods
 	
 	private void addGlJlEntry(GlModJournalLineDetails mdetails, LocalGlJournal glJournal, 
 			Integer RES_CODE, Integer AD_BRNCH, Integer AD_CMPNY) 
 	throws 
	GlJLChartOfAccountNotFoundException,
	GlJLChartOfAccountPermissionDeniedException {
 		
 		Debug.print("GlJournalEntryControllerBean addGlJlEntry");
 		
 		LocalGlJournalLineHome glJournalLineHome = null;        
 		LocalGlChartOfAccountHome glChartOfAccountHome = null;
 		LocalGlResponsibilityHome glResponsibilityHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);            
 			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
 			glResponsibilityHome = (LocalGlResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlResponsibilityHome.JNDI_NAME, LocalGlResponsibilityHome.class);            
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
 			
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}            
 		
 		try {
 			
 			// get company
 			
 			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			
 			// validate if coa exists
 			
 			LocalGlChartOfAccount glChartOfAccount = null;
 			
 			try {
 				
 				glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(mdetails.getJlCoaAccountNumber(), AD_BRNCH, AD_CMPNY);
 				
 				if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
 					throw new GlJLChartOfAccountNotFoundException();
 				
 			} catch (FinderException ex) {
 				
 				throw new GlJLChartOfAccountNotFoundException();
 				
 			}
 			
 			// validate responsibility coa permission
 			
 			LocalGlResponsibility glResponsibility = null; 
 			
 			try {
 				
 				glResponsibility = glResponsibilityHome.findByPrimaryKey(RES_CODE);
 				
 			} catch (FinderException ex) {
 				
 				throw new GlJLChartOfAccountPermissionDeniedException();
 				
 			}
 			
 			if (!this.isPermitted(glResponsibility.getGlOrganization(), glChartOfAccount, adCompany.getGenField(), AD_CMPNY)) {
 				System.out.println("respnsibility problem");
 				throw new GlJLChartOfAccountPermissionDeniedException();
 				
 			}        	        	
 			
 			// create journal 
 			
 			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
 					mdetails.getJlLineNumber(), 
					mdetails.getJlDebit(), 
					EJBCommon.roundIt(mdetails.getJlAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision()), 
					mdetails.getJlDescription(), AD_CMPNY);
 			
 			//glJournal.addGlJournalLine(glJournalLine);
 			glJournalLine.setGlJournal(glJournal);
 			//glChartOfAccount.addGlJournalLine(glJournalLine);
 			glJournalLine.setGlChartOfAccount(glChartOfAccount);
 			
 			
 		} catch (GlJLChartOfAccountPermissionDeniedException ex) {        	
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (GlJLChartOfAccountNotFoundException ex) {        	
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;    
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	
 	private boolean isPermitted(LocalGlOrganization glOrganization,
 			LocalGlChartOfAccount glChartOfAccount, LocalGenField genField, Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean isPermitted");
 		
 		/*** Get Generic Field's Segment Separator and Number Of Segments**/
 		
 		String strSeparator = null;
 		short numberOfSegment = 0;
 		short genNumberOfSegment = 0;
 		
 		try {
 			char chrSeparator = genField.getFlSegmentSeparator();
 			genNumberOfSegment = genField.getFlNumberOfSegment();
 			strSeparator = String.valueOf(chrSeparator);
 		} catch (Exception ex) {
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 		}
 		
 		Collection glAccountRanges = glOrganization.getGlAccountRanges();
 		
 		Iterator i = glAccountRanges.iterator();
 		
 		while (i.hasNext()) {
 			
 			LocalGlAccountRange glAccountRange = (LocalGlAccountRange)i.next(); 
 			
 			String[] chartOfAccountSegmentValue = new String[genNumberOfSegment];      	  
 			String[] accountLowSegmentValue = new String[genNumberOfSegment];
 			String[] accountHighSegmentValue = new String[genNumberOfSegment];
 			
 			// tokenize coa
 			
 			StringTokenizer st = new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), strSeparator);
 			
 			int j = 0;
 			
 			while (st.hasMoreTokens()) {
 				
 				chartOfAccountSegmentValue[j] = st.nextToken();
 				j++;
 				
 			}
 			
 			// tokenize account low 
 			
 			st = new StringTokenizer(glAccountRange.getArAccountLow(), strSeparator);
 			
 			j = 0;
 			
 			while (st.hasMoreTokens()) {
 				
 				accountLowSegmentValue[j] = st.nextToken();
 				j++;
 				
 			}
 			
 			// tokenize account high
 			st = new StringTokenizer(glAccountRange.getArAccountHigh(), strSeparator);
 			
 			j = 0;
 			
 			while (st.hasMoreTokens()) {
 				
 				accountHighSegmentValue[j] = st.nextToken();
 				j++;
 				
 			} 
 			
 			boolean isOverlapped = false;
 			
 			for (int k=0; k<genNumberOfSegment; k++) {
 				
 				if(chartOfAccountSegmentValue[k].compareTo(accountLowSegmentValue[k]) >= 0 &&
 						chartOfAccountSegmentValue[k].compareTo(accountHighSegmentValue[k]) <= 0) {
 					
 					isOverlapped = true;
 					
 				} else {
 					
 					isOverlapped = false;
 					break;
 					
 				}
 			}
 			
 			if (isOverlapped) return true;
 			
 			
 			
 		}
 		
 		return false;
 		
 	}
 	
 	
 	
 	
 	// SessionBean methods
 	
 	/**
 	 * @ejb:create-method view-type="remote"
 	 **/
 	public void ejbCreate() throws CreateException {
 		
 		Debug.print("GlJournalEntryControllerBean ejbCreate");
 		
 	}
 	
 	// private methods
 	
 	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
 			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean convertForeignToFunctionalCurrency");
 		
 		
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		
 		LocalAdCompany adCompany = null;
 		
 		// Initialize EJB Homes
 		
 		try {
 			
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		// get company and extended precision
 		
 		try {
 			
 			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			
 		} catch (Exception ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}	     

 		// Convert to functional currency if necessary
 		
 		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
 			
 			AMOUNT = AMOUNT / CONVERSION_RATE;
 			
 		}
 		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
 		
 	}  
 	
 	private void executeGlJrPost(Integer JR_CODE, String USR_NM, Integer AD_CMPNY) throws 
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDatePeriodClosedException,
	GlobalRecordAlreadyDeletedException {
 		
 		Debug.print("GlJournalEntryControllerBean executeGlJrPost");
 		
 		
 		LocalGlSetOfBookHome glSetOfBookHome = null;
 		LocalGlJournalHome glJournalHome = null;
 		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		LocalGlChartOfAccountHome glChartOfAccountHome = null;
 		LocalGlForexLedgerHome glForexLedgerHome = null;
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		
 		// Initialize EJB Home
 		
 		try {
 			
 			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);       
 			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);          
 			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);          
 			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
 			glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			// get journal
 			
 			LocalGlJournal glJournal = null;
 			
 			try {
 				
 				glJournal = glJournalHome.findByPrimaryKey(JR_CODE);
 				
 			} catch (FinderException ex) {
 				
 				throw new GlobalRecordAlreadyDeletedException();
 				
 			}       	  
 			
 			// validate if period is closed       	  	 
 			
 			LocalGlSetOfBook glSetOfBook = glSetOfBookHome.findByDate(glJournal.getJrEffectiveDate(), AD_CMPNY);
 			
 			LocalGlAccountingCalendarValue glAccountingCalendarValue =
 				glAccountingCalendarValueHome.findByAcCodeAndDate(
 						glSetOfBook.getGlAccountingCalendar().getAcCode(),
						glJournal.getJrEffectiveDate(), AD_CMPNY);
 			
 			if (glAccountingCalendarValue.getAcvStatus() == 'C' ||
 					glAccountingCalendarValue.getAcvStatus() == 'P' ||
					glAccountingCalendarValue.getAcvStatus() == 'N') {
 				
 				throw new GlJREffectiveDatePeriodClosedException(glJournal.getJrName());
 				
 			}
 			
 			
 			// validate if journal is already posted
 			
 			if (glJournal.getJrPosted() == EJBCommon.TRUE) {
 				
 				throw new GlobalTransactionAlreadyPostedException(glJournal.getJrName());	       	
 			}
 			
 			// post journal
 			
 			Collection glJournalLines = glJournal.getGlJournalLines();
 			
 			Iterator i = glJournalLines.iterator();
 			
 			while (i.hasNext()) {
 				
 				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
 				
 				double JL_AMNT = this.convertForeignToFunctionalCurrency(
 						glJournal.getGlFunctionalCurrency().getFcCode(),
						glJournal.getGlFunctionalCurrency().getFcName(),
						glJournal.getJrConversionDate(),
						glJournal.getJrConversionRate(),
						glJournalLine.getJlAmount(), AD_CMPNY);
 				
 				// post current to current acv
 				
 				this.post(glAccountingCalendarValue,
 						glJournalLine.getGlChartOfAccount(),
						true, glJournalLine.getJlDebit(), JL_AMNT, AD_CMPNY);
 				
 				
 				// post to subsequent acvs (propagate)
 				
 				Collection glSubsequentAccountingCalendarValues = 
 					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
 							glSetOfBook.getGlAccountingCalendar().getAcCode(),
							glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
 				
 				Iterator j = glSubsequentAccountingCalendarValues.iterator();
 				
 				while (j.hasNext()) {
 					
 					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
 						(LocalGlAccountingCalendarValue)j.next();
 					
 					this.post(glSubsequentAccountingCalendarValue,
 							glJournalLine.getGlChartOfAccount(),
							false, glJournalLine.getJlDebit(), JL_AMNT, AD_CMPNY);
 					
 				}

 				// post to subsequent years if necessary
 				
 				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
 				LocalAdCompany  adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

 				if (!glSubsequentSetOfBooks.isEmpty() && glSetOfBook.getSobYearEndClosed() == 1) {

 					LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
 					
 					Iterator sobIter = glSubsequentSetOfBooks.iterator();
 					
 					while (sobIter.hasNext()) {
 						
 						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
 						
 						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
 						
 						// post to subsequent acvs of subsequent set of book(propagate)
 						
 						Collection glAccountingCalendarValues = 
 							glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
 						
 						Iterator acvIter = glAccountingCalendarValues.iterator();
 						
 						while (acvIter.hasNext()) {
 							
 							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
 								(LocalGlAccountingCalendarValue)acvIter.next();
 							
 							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
 									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
 								
 								this.post(glSubsequentAccountingCalendarValue,
 										glJournalLine.getGlChartOfAccount(),
										false, glJournalLine.getJlDebit(), JL_AMNT, AD_CMPNY);
 								
 							} else { // revenue & expense
 								
 								this.post(glSubsequentAccountingCalendarValue,
 										glRetainedEarningsAccount,
										false, glJournalLine.getJlDebit(), JL_AMNT, AD_CMPNY);					        
 								
 							}
 							
 						}
 						
 						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
 						
 					}
 					
 				}
 				
		  	  	// for FOREX revaluation
		  	  	if((glJournal.getGlFunctionalCurrency().getFcCode() !=
		  	        adCompany.getGlFunctionalCurrency().getFcCode()) &&
					glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
		  	  		(glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(glJournal.getGlFunctionalCurrency().getFcCode()))){
		  	  		
		  	  		double CONVERSION_RATE = 1;
		  	  		
		  	  		if (glJournal.getJrConversionRate() != 0 && glJournal.getJrConversionRate() != 1) {
		  	  			
		  	  			CONVERSION_RATE = glJournal.getJrConversionRate();
		  	  			
		  	  		} else if (glJournal.getJrConversionDate() != null){
		  	  			
		  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
		  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
							glJournal.getJrConversionDate(), AD_CMPNY);
		  	  			
		  	  		}

		  	  		Collection glForexLedgers = null;
		  	  		
		  	  		try {
		  	  			
		  	  			glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
		  	  					glJournal.getJrEffectiveDate(), glJournalLine.getGlChartOfAccount().getCoaCode(),
								AD_CMPNY);
		  	  			
		  	  		} catch(FinderException ex) {

		  	  		}
		  	  		
		  	  		LocalGlForexLedger glForexLedger =
		  	  			(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
		  	  				(LocalGlForexLedger) glForexLedgers.iterator().next();

		  	  		int FRL_LN = (glForexLedger != null &&
			  	  		glForexLedger.getFrlDate().compareTo(glJournal.getJrEffectiveDate()) == 0) ?
			  	  			glForexLedger.getFrlLine().intValue() + 1 : 1;

		  	  		// compute balance
		  	  		double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
		  	  		double FRL_AMNT = glJournalLine.getJlAmount();
		  	  		
		  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));  
		  	  		else
		  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);
		  	  		
		  	  		COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
		  	  		
		  	  		glForexLedger = glForexLedgerHome.create(glJournal.getJrEffectiveDate(), new Integer (FRL_LN),
		  	  			"JOURNAL", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0, AD_CMPNY);
					
					//glJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
					glForexLedger.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());
					
					// propagate balances
					try{
					
						glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
							glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
							glForexLedger.getFrlAdCompany());
						
					} catch (FinderException ex) {

					}
					
					Iterator itrFrl = glForexLedgers.iterator();
					
					while (itrFrl.hasNext()) {
						
						glForexLedger = (LocalGlForexLedger) itrFrl.next();
						FRL_AMNT = glJournalLine.getJlAmount();
						
			  	  		if(glJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
			  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? FRL_AMNT : (- 1 * FRL_AMNT));
			  	  		else
			  	  			FRL_AMNT = (glJournalLine.getJlDebit() == EJBCommon.TRUE ? (- 1 * FRL_AMNT) : FRL_AMNT);
			  	  		
			  	  		glForexLedger.setFrlBalance( glForexLedger.getFrlBalance() + FRL_AMNT);

					}

		  	  	}
 				
 			}
 			
 			// set journal post status
 			
 			glJournal.setJrPosted(EJBCommon.TRUE);
 			glJournal.setJrPostedBy(USR_NM);
 			glJournal.setJrDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

 		} catch (GlobalRecordAlreadyDeletedException ex) {
 			
 			getSessionContext().setRollbackOnly();    	
 			throw ex;	
 			
 		} catch (GlJREffectiveDatePeriodClosedException ex) {
 			
 			getSessionContext().setRollbackOnly();    	
 			throw ex;	
 			
 		} catch (GlobalTransactionAlreadyPostedException ex) {
 			
 			getSessionContext().setRollbackOnly();    	
 			throw ex;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	private void post(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
 			LocalGlChartOfAccount glChartOfAccount, 
			boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean post");
 		
 		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
 			
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {          
 			
 			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
 			
 			LocalGlChartOfAccountBalance glChartOfAccountBalance = 
 				glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
 						glAccountingCalendarValue.getAcvCode(),
						glChartOfAccount.getCoaCode(), AD_CMPNY);
 			
 			String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
 			short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
 			
 			
 			
 			if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
 					isDebit == EJBCommon.TRUE) ||
					(!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
							isDebit == EJBCommon.FALSE)) {				    
 				
 				glChartOfAccountBalance.setCoabEndingBalance(
 						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
 				
 				if (!isCurrentAcv) {
 					
 					glChartOfAccountBalance.setCoabBeginningBalance(
 							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
 					
 				}
 				
 				
 			} else {
 				
 				glChartOfAccountBalance.setCoabEndingBalance(
 						EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
 				
 				if (!isCurrentAcv) {
 					
 					glChartOfAccountBalance.setCoabBeginningBalance(
 							EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
 					
 				}
 				
 			}
 			
 			if (isCurrentAcv) { 
 				
 				if (isDebit == EJBCommon.TRUE) {
 					
 					glChartOfAccountBalance.setCoabTotalDebit(
 							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
 					
 				} else {
 					
 					glChartOfAccountBalance.setCoabTotalCredit(
 							EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
 				}       	   
 				
 			}
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		
 	}
 	
 }