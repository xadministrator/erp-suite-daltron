
/*
 * GlJournalImportControllerBean.java
 *
 * Created on February 04, 2004, 3:56 AM
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.genfld.LocalGenField;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountRange;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalInterface;
import com.ejb.gl.LocalGlJournalInterfaceHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalLineInterface;
import com.ejb.gl.LocalGlJournalLineInterfaceHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlOrganization;
import com.ejb.gl.LocalGlResponsibility;
import com.ejb.gl.LocalGlResponsibilityHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.gl.LocalGlTransactionCalendarValue;
import com.ejb.gl.LocalGlTransactionCalendarValueHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModAccountingCalendarValueDetails;

/**
 * @ejb:bean name="GlJournalImportControllerEJB"
 *           display-name="Used for importing journals from journal interface to journal tables"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlJournalImportControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlJournalImportController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlJournalImportControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlJournalImportControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlJsAll(Integer AD_CMPNY) {
                    
        Debug.print("GlJournalImportControllerBean getGlJsAll");
        
        LocalGlJournalSourceHome glJournalSourceHome = null;
        
        ArrayList list = new ArrayList();
                
        //initialized EJB Home
        
        try {
            
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
		}
		
		
		try {
			
			Collection glJournalSources = glJournalSourceHome.findJsAll(AD_CMPNY);
			
			Iterator i = glJournalSources.iterator();
			
			while (i.hasNext()) {
				
				LocalGlJournalSource glJournalSource = (LocalGlJournalSource)i.next();
				
				list.add(glJournalSource.getJsName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	   
   
  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlAcAllEditableOpenAndFutureEntry(Integer AD_CMPNY) {

      Debug.print("GlRepDetailIncomeStatementControllerBean getGlAcAllEditableOpenAndFutureEntry");      
      
      LocalGlSetOfBookHome glSetOfBookHome = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      
      ArrayList list = new ArrayList();

	  // Initialize EJB Home
	    
	  try {
	        
	       glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
	       glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try {
      	
      	  Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
      	  
      	  Iterator i = glSetOfBooks.iterator();
      	  
      	  while (i.hasNext()) {
      	  	
      	  	  LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();
      	  	  
      	  	  Collection glAccountingCalendarValues = 
      	  	      glAccountingCalendarValueHome.findOpenAndFutureEntryAcvByAcCode(
      	  	      	 glSetOfBook.getGlAccountingCalendar().getAcCode(), 'O', 'F', AD_CMPNY);
      	  	  
      	  	  Iterator j = glAccountingCalendarValues.iterator();
      	  	  
      	  	  while (j.hasNext()) {
      	  	  
	      	  	  LocalGlAccountingCalendarValue glAccountingCalendarValue = 
	      	  	      (LocalGlAccountingCalendarValue)j.next();
	      	  	      
	      	  	  GlModAccountingCalendarValueDetails mdetails = new GlModAccountingCalendarValueDetails();
	      	  	  
	      	  	  mdetails.setAcvPeriodPrefix(glAccountingCalendarValue.getAcvPeriodPrefix());
	      	  	  
	      	  	  // get year
	      	  	  
	      	  	  GregorianCalendar gc = new GregorianCalendar();
	      	  	  gc.setTime(glAccountingCalendarValue.getAcvDateTo());
	      	  	  
	      	  	  mdetails.setAcvYear(gc.get(Calendar.YEAR));
	      	  	  	      	  	  
	      	  	  list.add(mdetails);
	      	  	      
	      	  }
      	  	
      	  }
      	  
      	  return list;
      	  
      	
      } catch (Exception ex) {
      	
      	  Debug.printStackTrace(ex);
      	  throw new EJBException(ex.getMessage());
      	
      }
      
   }
   
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public long executeGlJrImport(String JS_NM, String PRD_NM, int YR, String USR_NM, Integer RES_CODE, Integer AD_BRNCH, Integer AD_CMPNY)  {
                    
        Debug.print("GlJournalImportControllerBean executeGlJrImport");
        
        LocalGlJournalInterfaceHome glJournalInterfaceHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineInterfaceHome glJournalLineInterfaceHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlResponsibilityHome glResponsibilityHome = null;
        LocalGlTransactionCalendarValueHome glTransactionCalendarValueHome = null;
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        
        long IMPORTED_JOURNALS = 0L;
        
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
        
        LocalGlJournalBatch glJournalBatch = null;
                      
        //initialized EJB Home
        
        try {
            
            glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);  
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);      
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);     
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);         
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);         
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);         
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);         
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);         
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);         
            glJournalLineInterfaceHome = (LocalGlJournalLineInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalLineInterfaceHome.JNDI_NAME, LocalGlJournalLineInterfaceHome.class);    
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glResponsibilityHome = (LocalGlResponsibilityHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlResponsibilityHome.JNDI_NAME, LocalGlResponsibilityHome.class);
            glTransactionCalendarValueHome = (LocalGlTransactionCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlTransactionCalendarValueHome.JNDI_NAME, LocalGlTransactionCalendarValueHome.class);  
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);  
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
		      lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
		}
		
		        
        try {
        	
        	// get preference
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            // get company
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        
            // get selected set of book
            
            Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(PRD_NM, EJBCommon.getIntendedDate(YR), AD_CMPNY);
            ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);
            
            LocalGlSetOfBook glSelectedSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
            
            LocalGlFunctionalCurrency glFunctionalCurrency = null;
            LocalGlJournalCategory glJournalCategory = null;
            
            // get accounting calendar value 
	                           
            LocalGlAccountingCalendarValue glAccountingCalendarValue = 
                glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
                	glSelectedSetOfBook.getGlAccountingCalendar().getAcCode(),
                	PRD_NM, AD_CMPNY);
            
            // get journal source
           
            ArrayList glJournalSourceList = null;
            
            if (JS_NM == null || JS_NM.length() == 0) {
            	
            	Collection glJournalSources = glJournalSourceHome.findJsAll(AD_CMPNY);
            	glJournalSourceList = new ArrayList(glJournalSources);
            	
            } else {
                                	
            	LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName(JS_NM, AD_CMPNY);
            	
            	glJournalSourceList = new ArrayList();
            	glJournalSourceList.add(glJournalSource);
            	
            }
            
            
            Iterator jsIter = glJournalSourceList.iterator();
            
            while (jsIter.hasNext()) {
            
                LocalGlJournalSource glJournalSource = (LocalGlJournalSource)jsIter.next();
             	            	      	   	
	            Collection glJournalInterfaces = glJournalInterfaceHome.findByJsNameAndDateRange(glJournalSource.getJsName(),
	                glAccountingCalendarValue.getAcvDateFrom(), glAccountingCalendarValue.getAcvDateTo(), AD_CMPNY);        	    
	            
	            if (!glJournalInterfaces.isEmpty()) {
	            	
	            	Iterator i = glJournalInterfaces.iterator();
	            	            
		            ArrayList glJournalInterfaceList = new ArrayList();
		            
		            while (i.hasNext()) {
		            	
		            	LocalGlJournalInterface glJournalInterface = (LocalGlJournalInterface)i.next();
		            	
		            	// validate if calendar is open
		            	
		            	if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
		            	    glAccountingCalendarValue.getAcvStatus() == 'C' ||
		            	    glAccountingCalendarValue.getAcvStatus() == 'P') {
		            	    	
		            	    continue; 	
		            	    	
		            	}		           	           	            	
		            	         			            		    
		                // validate if reversal date has period and it is after journal    
		                   	
		            	try {
		            		
		            		if (glJournalInterface.getJriDateReversal() != null) {
		  	
						      	  LocalGlAccountingCalendarValue glReversalPeriodAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndDate(
						      	  	  glSelectedSetOfBook.getGlAccountingCalendar().getAcCode(), glJournalInterface.getJriDateReversal(), AD_CMPNY);
						      	  	  
						      	  if (!glJournalInterface.getJriDateReversal().after(glJournalInterface.getJriEffectiveDate())) {
						      	  	
						      	  	  continue;
						      	  	
						      	  }
						      	  	  		      	  	  
						    }				    
						    	            		
		            	} catch (FinderException ex) {
		            		
		            		continue;
		            		
		            	}
		            	
		            	// validate if date is valid
		            		            		
		        		LocalGlTransactionCalendarValue glTransactionCalendarValue = 
				                glTransactionCalendarValueHome.findByTcCodeAndTcvDate(glSelectedSetOfBook.getGlTransactionCalendar().getTcCode(),
				                   glJournalInterface.getJriEffectiveDate(), AD_CMPNY);
				                   
				        LocalGlJournalSource glValidateJournalSource = glJournalSourceHome.findByJsName(glJournalSource.getJsName(), AD_CMPNY);          
				                  
				        if (glTransactionCalendarValue.getTcvBusinessDay() == EJBCommon.FALSE &&
						        glValidateJournalSource.getJsEffectiveDateRule() == 'F') {
						      	
						    continue;
						      		      	
						}
						
						// validate conversion date and rate
						
						try {
							
							if (glJournalInterface.getJriConversionDate() == null &&
							    glJournalInterface.getJriConversionRate() == 0) {
							   
							    continue; 	
							    	
							}
							
							if (glJournalInterface.getJriConversionDate() != null &&
							    glJournalInterface.getJriConversionRate() != 0) {
							   
							    continue; 	
							    	
							}
	      	  
				      	    if (glJournalInterface.getJriConversionDate() != null) {
				      	 
				      	
					        	  LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(glJournalInterface.getJriFunctionalCurrency(), AD_CMPNY);
					        	  LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = 
					      	      glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
					      	          glJournalInterface.getJriConversionDate(), AD_CMPNY);
					      	          
					        }	          
				      	      	      	
				        } catch (FinderException ex) {
				      	  
				        	 continue;
				      	
				        }
				        
				        // validate if journal category exists
				        
				        try {
				        	
				        	glJournalCategory = glJournalCategoryHome.findByJcName(glJournalInterface.getJriJournalCategory(), AD_CMPNY);
				        	
				        } catch (FinderException ex) {
				        	
				        	continue;
				        	
				        }
				        
				        // validate if currency exists
				        
				        try {
				        	
				        	glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(glJournalInterface.getJriFunctionalCurrency(), AD_CMPNY);
				        	
				        } catch (FinderException ex) {
				        	
				        	continue;
				        	
				        }
				        
				        // validate journal line interfaces
				        
				        Collection glValidateJournalLineInterfaces = glJournalInterface.getGlJournalLineInterfaces();
				        
				        
				        // validate if journal interface has lines
				        
				        if (glValidateJournalLineInterfaces.size() < 2) {
				        	
				        	continue;
				        	
				        }			        
				        
				        
				        boolean journalLineInterfaceHasErrors = false;
				        
				        Iterator glValidateJournalLineInterfaceIterator = glValidateJournalLineInterfaces.iterator();
				        
				        while (glValidateJournalLineInterfaceIterator.hasNext()) {
				        	
				        	LocalGlJournalLineInterface glJournalLineInterface = 
				        	    (LocalGlJournalLineInterface)glValidateJournalLineInterfaceIterator.next();
				        	    
				        	// validate if account exists
				        	
				        	LocalGlChartOfAccount glChartOfAccount = null;
				        	
				        	try {
				        		
				        	    glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(
				        	    	glJournalLineInterface.getJliCoaAccountNumber(), AD_CMPNY);
				        	    
				        	} catch (FinderException ex) {
				        		
				        		journalLineInterfaceHasErrors = true;
				        		break;
				        		
				        	}
				        	
				        	// validate if chart of account has permission to user
				        	
				        	LocalGlResponsibility glResponsibility = glResponsibilityHome.findByPrimaryKey(RES_CODE);
	        	
				        	if (!this.isPermitted(glResponsibility.getGlOrganization(), glChartOfAccount, adCompany.getGenField(), AD_CMPNY)) {
				          	
				          	    journalLineInterfaceHasErrors = true;
				        		break;
				          	
				            } 
				            
				            			        	
				        }
				        
				        if (journalLineInterfaceHasErrors) {
				        	
				        	continue;
				        	
				        }        			               			            	
		            		            	
		            	
		            	// check if journal interface is balance if not check suspense posting
		            	
		            	LocalGlJournalLine glOffsetJournalLine = null;
		            	
		            	Collection glJournalLineInterfaces = glJournalInterface.getGlJournalLineInterfaces();
		                
		            	Iterator j = glJournalLineInterfaces.iterator();
		            	
		            	double TOTAL_DEBIT = 0d;
		            	double TOTAL_CREDIT = 0d;
		            	
		            	while (j.hasNext()) {
		            		
		            		LocalGlJournalLineInterface glJournalLineInterface = (LocalGlJournalLineInterface)j.next();
		            		
		            		if (glJournalLineInterface.getJliDebit() == EJBCommon.TRUE) {
		            			
		            			TOTAL_DEBIT += glJournalLineInterface.getJliAmount();
		            			            			
		            		} else {
		            			
		            			TOTAL_CREDIT += glJournalLineInterface.getJliAmount();
		            			
		            		}
		            		
		            	}
		            	
		            	TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
		            	TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
		            	            	            	            	
		            	if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
		            	    TOTAL_DEBIT != TOTAL_CREDIT) {
		            	    	
		            	    LocalGlSuspenseAccount glSuspenseAccount = null;
		            	    	
		            	    try { 	
		            	    	
		            	        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName(glJournalSource.getJsName(), glJournalInterface.getJriJournalCategory(), AD_CMPNY);
		            	        
		            	    } catch (FinderException ex) {
		            	    	
		            	    	continue;
		            	    }
		            	               	    
		            	    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
		            	    	
		            	    	glOffsetJournalLine = glJournalLineHome.create(
		            	    	    (short)(glJournalLineInterfaces.size() + 1),
		            	    	    EJBCommon.TRUE,
		            	    	    TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
		            	    	              	        
		            	    } else {
		            	    	
		            	    	glOffsetJournalLine = glJournalLineHome.create(
		            	    	    (short)(glJournalLineInterfaces.size() + 1),
		            	    	    EJBCommon.FALSE,
		            	    	    TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
		            	    	
		            	    }
		            	    
		            	    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
		            	    glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
		            	    
		            	    	
		 				} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
						    TOTAL_DEBIT != TOTAL_CREDIT) {
						    
							continue;		    	
						    	
						}
		 				
		 				
		 				if (glJournalInterface.getJriJournalSource().equals("MANUAL")) {
		 					
		 					String JR_DCMNT_NMBR = null;
		 				
			 				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment =
			 		            adDocumentSequenceAssignmentHome.findByDcName("GL JOURNAL", AD_CMPNY);

			 				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
			 				
			 		        try {
			 		        	
			 		        	adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
			 		        	
			 		        } catch (FinderException ex) {
			 		        	
			 		        }
			 				
			 		        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
			 		            (JR_DCMNT_NMBR == null || JR_DCMNT_NMBR.trim().length() == 0)) {
			 		            	
			 		            while (true) {
			 		            	
			 		            	if ( adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null ) {

			 		            		try {
			 			            		
			 			            		glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
			 			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
			 			            		
			 			            	} catch (FinderException ex) {
			 			            		
			 			            		JR_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
			 			            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
			 					            break;
			 			            		
			 			            	}	            	        			            	
			 		            		
			 		            	} else {

			 		            		try {
			 			            		
			 			            		glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
			 			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
			 			            		
			 			            	} catch (FinderException ex) {
			 			            		
			 			            		JR_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
			 			            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
			 					            break;
			 			            		
			 			            	}	            	        			            	
			 		            		
			 		            	}
			 		            	
			 		            }		            
			 		            		            	
			 		        }
			 		        
			 		        glJournalInterface.setJriDocumentNumber(JR_DCMNT_NMBR);
			 		        
		 				}
						
						// validate if document number is unique
			   
					    try {
					    
			 		   	   LocalGlJournal glExistingJournal = glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(glJournalInterface.getJriDocumentNumber(), "MANUAL", AD_BRNCH, AD_CMPNY);
			 		   	  
			 		   	   continue;
			 		   	  
			 		   } catch (FinderException ex) {
			 		   	
				   	   }

				     	// create journal batch if necessary
				     	
				     	if (adPreference.getPrfEnableGlJournalBatch() ==  EJBCommon.TRUE && 
				     		glJournalBatch == null) {
				     		
				     		glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " +  formatter.format(new Date()), "JOURNAL IMPORT", "OPEN", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
				     		
				     	}
		            	            	            	
		            	// create journal	            	
		            		            	
		            	LocalGlJournal glJournal = null;
		            	
		            	try {
		            		
		            		// generate approval status
		            		
		            		String JR_APPRVL_STATUS = null;
		            		
		            		if (glJournalSource.getJsJournalApproval() == EJBCommon.FALSE) {
		            			
		            			JR_APPRVL_STATUS = "N/A";
		            			
		            		}
	        														     
		            	    glJournal = glJournalHome.create(
		            		    glJournalInterface.getJriName(),
		            		    glJournalInterface.getJriDescription(),
		            		    glJournalInterface.getJriEffectiveDate(),
		            		    0.0d,
		            		    glJournalInterface.getJriDateReversal(),
		            		    glJournalInterface.getJriDocumentNumber(),
		            		    glJournalInterface.getJriConversionDate(),
		 	          		    glJournalInterface.getJriConversionRate(),
		 	          		    JR_APPRVL_STATUS, null,
		            		    glJournalInterface.getJriFundStatus(),
		            		    EJBCommon.FALSE,	            		    
		            		    glJournalInterface.getJriReversed(),
		            		    USR_NM, new Date(),
		            		    USR_NM, new Date(),
		            		    null, null,
		            		    null, null, glJournalInterface.getJriTin(), glJournalInterface.getJriSubLedger(),
								EJBCommon.FALSE, null,
								AD_BRNCH, AD_CMPNY);
		            		    
		            	    glJournal.setGlJournalSource(glJournalSource);
		            		glJournal.setGlJournalCategory(glJournalCategory);
		            		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
		            		
		            		if (glJournalBatch != null) {
		            			
		            			glJournal.setGlJournalBatch(glJournalBatch);
		            			
		            		}
		            		    
		            	} catch (CreateException ex) {
		            		     		
		            		getSessionContext().setRollbackOnly();
		            		continue;
		            	}	            	
		            		            	
		            	// create journal lines
		            	          	
		            	j = glJournalLineInterfaces.iterator();
		            	
		            	while (j.hasNext()) {
		            		
		            		LocalGlJournalLineInterface glJournalLineInterface = (LocalGlJournalLineInterface)j.next();
		            		
		            		LocalGlJournalLine glJournalLine = glJournalLineHome.create(
		            			glJournalLineInterface.getJliLineNumber(),	            			
		            			glJournalLineInterface.getJliDebit(),
		            			glJournalLineInterface.getJliAmount(), "", AD_CMPNY);
		            			
		            	    LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(glJournalLineInterface.getJliCoaAccountNumber(), AD_CMPNY);
		            	    glChartOfAccount.addGlJournalLine(glJournalLine);
		            	    
		            	    glJournal.addGlJournalLine(glJournalLine);
		            	    
		            		
		            	}
		            	
		            	if (glOffsetJournalLine != null) {
		            		
		            		glJournal.addGlJournalLine(glOffsetJournalLine);
		            		
		            	}   
		            	
		            	++IMPORTED_JOURNALS;
		            	glJournalInterfaceList.add(glJournalInterface);      	
		            	
		         	}
		         	
		         	// delete imported journals
	            
		            i = glJournalInterfaceList.iterator();
		            
		            while (i.hasNext()) {
		            	
		            	LocalGlJournalInterface glJournalInterface = (LocalGlJournalInterface)i.next();
		            	
		            	i.remove();
		            	glJournalInterface.remove();
		            	
		            }            	
	            	
	            }
	            
	        }
            
            return IMPORTED_JOURNALS;
        	
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
              
    }
    
    
    private boolean isPermitted(LocalGlOrganization glOrganization,
      LocalGlChartOfAccount glChartOfAccount, LocalGenField genField, Integer AD_CMPNY) {

      Debug.print("GlJournalEntryControllerBean isPermitted");
        
      /*** Get Generic Field's Segment Separator and Number Of Segments**/
       
      String strSeparator = null;
      short numberOfSegment = 0;
      short genNumberOfSegment = 0;
       
      try {
         char chrSeparator = genField.getFlSegmentSeparator();
         genNumberOfSegment = genField.getFlNumberOfSegment();
         strSeparator = String.valueOf(chrSeparator);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }
      
      Collection glAccountRanges = glOrganization.getGlAccountRanges();
      
      Iterator i = glAccountRanges.iterator();
      
      while (i.hasNext()) {
      	
      	  LocalGlAccountRange glAccountRange = (LocalGlAccountRange)i.next(); 
          
          String[] chartOfAccountSegmentValue = new String[genNumberOfSegment];      	  
          String[] accountLowSegmentValue = new String[genNumberOfSegment];
          String[] accountHighSegmentValue = new String[genNumberOfSegment];
      	  
      	  // tokenize coa
      	  
      	  StringTokenizer st = new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), strSeparator);
      	  
      	  int j = 0;
      	  
      	  while (st.hasMoreTokens()) {
      	  	
      	  	  chartOfAccountSegmentValue[j] = st.nextToken();
      	  	  j++;
      	  	
      	  }
      	  
      	  // tokenize account low 
      	  
      	  st = new StringTokenizer(glAccountRange.getArAccountLow(), strSeparator);
      	  
      	  j = 0;
      	  
      	  while (st.hasMoreTokens()) {
      	  	
      	  	  accountLowSegmentValue[j] = st.nextToken();
      	  	  j++;
      	  	
      	  }
      	  
      	  // tokenize account high
      	  st = new StringTokenizer(glAccountRange.getArAccountHigh(), strSeparator);
      	  
      	  j = 0;
      	  
      	  while (st.hasMoreTokens()) {
      	  	  
      	  	  accountHighSegmentValue[j] = st.nextToken();
      	  	  j++;
      	  	
      	  } 
      	  
      	  boolean isOverlapped = false;

	      for (int k=0; k<genNumberOfSegment; k++) {
	      	      	
             if(chartOfAccountSegmentValue[k].compareTo(accountLowSegmentValue[k]) >= 0 &&
                chartOfAccountSegmentValue[k].compareTo(accountHighSegmentValue[k]) <= 0) {
                	
		  		   isOverlapped = true;
		  		   
             } else {
                	
                   isOverlapped = false;
                   break;
                   
             }
          }

	      if (isOverlapped) return true;


      
       }
       
       return false;
    
   }
    
     
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlJournalImportControllerBean ejbCreate");
      
    }
}
