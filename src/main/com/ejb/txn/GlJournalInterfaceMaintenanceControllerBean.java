/*
 * GlJournalInterfaceMaintenanceControllerBean.java
 *
 * Created on March 30, 2004, 4:56 PM
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlCOANoChartOfAccountFoundException;
import com.ejb.exception.GlJLINoJournalLineInterfacesFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlJournalInterface;
import com.ejb.gl.LocalGlJournalInterfaceHome;
import com.ejb.gl.LocalGlJournalLineInterface;
import com.ejb.gl.LocalGlJournalLineInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlJournalInterfaceDetails;
import com.util.GlJournalLineInterfaceDetails;
import com.util.GlModJournalLineInterfaceDetails;

/**
 * @ejb:bean name="GlJournalInterfaceMaintenanceControllerEJB"
 *           display-name="Used for journal interface details"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlJournalInterfaceMaintenanceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlJournalInterfaceMaintenanceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlJournalInterfaceMaintenanceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlJournalInterfaceMaintenanceControllerBean extends AbstractSessionBean {

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList getGlJliByJriCode(Integer JRI_CODE, Integer AD_CMPNY)
        throws GlJLINoJournalLineInterfacesFoundException {

       	Debug.print("GlJournalInterfaceMaintenanceControllerBean getGlJliByJriCode");

      	ArrayList jliAllOfJriList = new ArrayList();
		LocalGlJournalInterface glJournalInterface = null;
		Collection glJournalLineInterfaces = null;
		
		LocalGlJournalInterfaceHome glJournalInterfaceHome = null;
		LocalGlJournalLineInterfaceHome glJournalLineInterfaceHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;

		
		// Initialize EJB Home
		
		try {
			
			glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
			  	lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);
			glJournalLineInterfaceHome = (LocalGlJournalLineInterfaceHome)EJBHomeFactory.
			  	lookUpLocalHome(LocalGlJournalLineInterfaceHome.JNDI_NAME, LocalGlJournalLineInterfaceHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
		  		lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			
      	} catch (NamingException ex) {
            
          	throw new EJBException(ex.getMessage());
            
      	}

      	try {
      	
        	glJournalInterface = glJournalInterfaceHome.findByPrimaryKey(JRI_CODE);
         
      		glJournalLineInterfaces = glJournalInterface.getGlJournalLineInterfaces();

      		if (glJournalLineInterfaces.size() == 0)
      
         		throw new GlJLINoJournalLineInterfacesFoundException();

      		Iterator i = glJournalLineInterfaces.iterator();
      
      		while (i.hasNext()) {

         		LocalGlJournalLineInterface glJournalLineInterface = (LocalGlJournalLineInterface) i.next();
 		        
         		LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(glJournalLineInterface.getJliCoaAccountNumber(), AD_CMPNY);
         		
				GlModJournalLineInterfaceDetails details = new GlModJournalLineInterfaceDetails(	
					glJournalLineInterface.getJliCode(), glJournalLineInterface.getJliLineNumber(),
				    glJournalLineInterface.getJliDebit(), glJournalLineInterface.getJliAmount(),
				    glJournalLineInterface.getJliCoaAccountNumber(), glChartOfAccount.getCoaAccountDescription());
	
		        jliAllOfJriList.add(details);
	        
      		}

      		return jliAllOfJriList;
      		
      	} catch (GlJLINoJournalLineInterfacesFoundException ex) {
      		
      		throw ex;
      		
      	} catch (Exception ex) {
      		
      		Debug.printStackTrace(ex);
      		throw new EJBException(ex.getMessage());
      		
      	}
      	
   	}
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/   
   	public com.util.GlJournalInterfaceDetails getGlJriByJriCode(Integer JRI_CODE, Integer AD_CMPNY) {

		Debug.print("GlJournalInterfaceMaintenanceControllerBean getGlJriByJriCode");
		      
		LocalGlJournalInterfaceHome glJournalInterfaceHome = null;
		LocalGlJournalInterface glJournalInterface = null;

		// Initialize EJB Home
		
		try {
            
        	glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);
            
       	} catch (NamingException ex) {
            
           	throw new EJBException(ex.getMessage());
            
       	}
       
       	try {

       	   	glJournalInterface = glJournalInterfaceHome.findByPrimaryKey(JRI_CODE);

       	   	GlJournalInterfaceDetails details = new GlJournalInterfaceDetails();
       	       	details.setJriCode(glJournalInterface.getJriCode());
       	       	details.setJriName(glJournalInterface.getJriName());
       	       	details.setJriDescription(glJournalInterface.getJriDescription());
       	       	details.setJriEffectiveDate(glJournalInterface.getJriEffectiveDate());
       	       	details.setJriJournalCategory(glJournalInterface.getJriJournalCategory());
       	       	details.setJriJournalSource(glJournalInterface.getJriJournalSource());
       	       	details.setJriFunctionalCurrency(glJournalInterface.getJriFunctionalCurrency());
       	       	details.setJriDateReversal(glJournalInterface.getJriDateReversal());
       	       	details.setJriDocumentNumber(glJournalInterface.getJriDocumentNumber());
       	       	details.setJriConversionDate(glJournalInterface.getJriConversionDate());
       	       	details.setJriConversionRate(glJournalInterface.getJriConversionRate());
       	       	details.setJriFundStatus(glJournalInterface.getJriFundStatus());
      	       	details.setJriReversed(glJournalInterface.getJriReversed());

       	   	return details;    
       	          	
       	} catch (Exception ex) {
       	
       	   	throw new EJBException(ex.getMessage());
       	
       	}
       
   	}           
   
   /**
   	* @ejb:interface-method view-type="remote"
   	**/
   	public void saveGlJriEntry(com.util.GlJournalInterfaceDetails details, ArrayList list, Integer AD_CMPNY)
      	throws GlobalRecordAlreadyExistException,
            GlCOANoChartOfAccountFoundException {

      	Debug.print("GlJournalInterfaceMaintenanceControllerBean saveGlJriEntry");
      
		LocalGlJournalInterfaceHome glJournalInterfaceHome = null;
		LocalGlJournalInterface glJournalInterface = null;
		
		GlJournalLineInterfaceDetails lineDetails = null;   
		    
		// Initialize EJB Home
		
		try {           
          
          	glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
              	lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);
            
      	} catch (NamingException ex) {
            
          	throw new EJBException(ex.getMessage());
            
      	}
      
      	try {

      	  	glJournalInterface = glJournalInterfaceHome.findByJriName(details.getJriName(), AD_CMPNY);

          	if (!glJournalInterface.getJriCode().equals(details.getJriCode())) {

		    	throw new GlobalRecordAlreadyExistException();
		
		  	}
		  
      	} catch (GlobalRecordAlreadyExistException ex) {
        	
        	throw ex;		     
          
      	} catch (FinderException ex) {
    	 	
      	} catch (Exception ex) {
        
          	throw new EJBException(ex.getMessage());
        
      	}          
      
	  	try {

	  	  	glJournalInterface = glJournalInterfaceHome.findByPrimaryKey(details.getJriCode());

          	// remove all journal line interfaces

	  	  	Collection glJournalLineInterfaces = glJournalInterface.getGlJournalLineInterfaces();

	  	  	Iterator i = glJournalLineInterfaces.iterator();     	  

	  	  	while (i.hasNext()) {

	  	  	   	LocalGlJournalLineInterface glJournalLineInterface = (LocalGlJournalLineInterface)i.next();
	  	  	   
	  	  	   	i.remove();
	  	  	   
	  	  	   	glJournalLineInterface.remove();
	  	  	
	  	  	}      	        
	  
	  	  	// add new journal line interfaces
	  	 
	  	  	i = list.iterator();

	  	  	while (i.hasNext()) {

	  	  	  	GlJournalLineInterfaceDetails mdetails = (GlJournalLineInterfaceDetails) i.next();

	  	  	  	lineDetails = new GlJournalLineInterfaceDetails();
	  	  	  	lineDetails.setJliLineNumber(mdetails.getJliLineNumber());
	  	  	  	lineDetails.setJliDebit(mdetails.getJliDebit());
	  	  	  	lineDetails.setJliAmount(mdetails.getJliAmount());
	  	  	  	lineDetails.setJliCoaAccountNumber(mdetails.getJliCoaAccountNumber());

	  	  	  	this.addGlJliEntry(glJournalInterface, lineDetails, details.getJriCode(), AD_CMPNY);

	  	  	}

	  	  	glJournalInterface.setJriName(details.getJriName());
	  	  	glJournalInterface.setJriDescription(details.getJriDescription());
	  	  	glJournalInterface.setJriEffectiveDate(details.getJriEffectiveDate());
	  	  	glJournalInterface.setJriJournalCategory(details.getJriJournalCategory());
	  	  	glJournalInterface.setJriJournalSource(details.getJriJournalSource());
	  	  	glJournalInterface.setJriFunctionalCurrency(details.getJriFunctionalCurrency());
	  	  	glJournalInterface.setJriDateReversal(details.getJriDateReversal());
	  	  	glJournalInterface.setJriDocumentNumber(details.getJriDocumentNumber());
	  	  	glJournalInterface.setJriConversionDate(details.getJriConversionDate());
	  	  	glJournalInterface.setJriConversionRate(details.getJriConversionRate());
	  	  	glJournalInterface.setJriFundStatus(details.getJriFundStatus());
	  	  	glJournalInterface.setJriReversed(details.getJriReversed());    
	  	  
      	} catch (FinderException ex) {
    	 	
      	} catch (GlCOANoChartOfAccountFoundException ex) {

			getSessionContext().setRollbackOnly();      	  
			throw new GlCOANoChartOfAccountFoundException(String.valueOf(lineDetails.getJliLineNumber()));

      	} catch (Exception ex) {
      	
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
        
      	}          	  	    

   	}
   
  /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteGlJriEntry(Integer JRI_CODE, Integer AD_CMPNY) {

      Debug.print("GlJournalInterfaceMaintenanceControllerBean deleteGlJriEntry");
      
      LocalGlJournalInterfaceHome glJournalInterfaceHome = null;
      LocalGlJournalInterface glJournalInterface = null;  
      GlJournalLineInterfaceDetails lineDetails = null;   
            
      // Initialize EJB Home
        
      try {           
          
          glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      try {
      	
      	  glJournalInterface = glJournalInterfaceHome.findByPrimaryKey(JRI_CODE);      	            
      	        	  
      } catch (FinderException ex) {
      	
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }      	  
      	  
      // remove all journal line interfaces      	  
      	  
  	  Collection glJournalLineInterfaces = glJournalInterface.getGlJournalLineInterfaces();
  	  
  	  Iterator i = glJournalLineInterfaces.iterator();     	  
  	  
  	  while (i.hasNext()) {
  	  	
  	  	   LocalGlJournalLineInterface glJournalLineInterface = (LocalGlJournalLineInterface)i.next();
  	  	   
  	  	   i.remove();
  	  	   
	  	   try {
	  	  
	  	       glJournalLineInterface.remove();     	        
	  	  
		   } catch (RemoveException ex) {
		 	
		       getSessionContext().setRollbackOnly();
		    
		       throw new EJBException(ex.getMessage());
		    
		   } catch (Exception ex) {
		 	
		       getSessionContext().setRollbackOnly();
		    
		       throw new EJBException(ex.getMessage());
		    
		  }	     	  	   
  	  	   
  	  	
  	  } 
      	  
  	  // delete journal interface
  	  
  	  try {
  	  
  	      glJournalInterface.remove();     	        
  	  
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  }	        	  
      
   }   
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

      Debug.print("GlJournalInterfaceMaintenanceControllerBean getGlFcPrecisionUnit");

      /*** Get Precision Unit from Set Of Book's Functional Currency  */

       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }            
   
   // private methods
      
   private void addGlJliEntry(LocalGlJournalInterface glJournalInterface, GlJournalLineInterfaceDetails lineDetails, Integer JRI_CODE, Integer AD_CMPNY)
      throws GlCOANoChartOfAccountFoundException {

      Debug.print("GlJournalInterfaceMaintenanceControllerBean addGlJliEntry");

      /********************************************************
         Adds a journal line interface entry
       *******************************************************/

      LocalAdCompanyHome adCompanyHome = null;
      LocalGlJournalLineInterfaceHome glJournalLineInterfaceHome = null;
      LocalGlChartOfAccountHome glChartOfAccountHome = null;

      LocalGlJournalLineInterface glJournalLineInterface = null;
      LocalGlChartOfAccount glChartOfAccount = null;
      LocalAdCompany adCompany = null;
            
      // Initialize EJB Home
        
      try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
           glJournalLineInterfaceHome = (LocalGlJournalLineInterfaceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalLineInterfaceHome.JNDI_NAME, LocalGlJournalLineInterfaceHome.class);
           glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());
            
      }

      try {

            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      } catch (Exception ex) {

         throw new EJBException(ex.getMessage());
         
      }

      // get extended precision

      short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();

      // validate if coa exists

      try {

         glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(lineDetails.getJliCoaAccountNumber(), AD_CMPNY);

      } catch (FinderException ex) {

         throw new GlCOANoChartOfAccountFoundException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
         
      }

      if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) {
      	
         throw new GlCOANoChartOfAccountFoundException();
         
      }

      // create journal

      try {
      	
         glJournalLineInterface = glJournalLineInterfaceHome.create(
		    lineDetails.getJliLineNumber(), lineDetails.getJliDebit(), 
		    EJBCommon.roundIt(lineDetails.getJliAmount(), FC_EXTNDD_PRCSN), lineDetails.getJliCoaAccountNumber(), AD_CMPNY);		   
	     
      } catch (Exception ex) {
      	
         getSessionContext().setRollbackOnly();
	 	 throw new EJBException(ex.getMessage());
	 	
      }

     // add journal line interface to journal interface

      try {
      	
	     glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	     
      } catch (Exception ex) {
      	
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
         
      }
      
   }      

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlJournalInterfaceMaintenanceControllerBean ejbCreate");

   }

}
