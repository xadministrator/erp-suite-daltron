
/*
 * GlJournalInterfaceUploadControllerBean.java
 *
 * Created on Januray 09, 2004, 10:20 AM
 * @author  Dennis M. Hilario
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.exception.GlJRIJournalNotBalanceException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlJournalInterface;
import com.ejb.gl.LocalGlJournalInterfaceHome;
import com.ejb.gl.LocalGlJournalLineInterface;
import com.ejb.gl.LocalGlJournalLineInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlJournalLineInterfaceDetails;

/**
 * @ejb:bean name="GlJournalInterfaceUploadControllerEJB"
 *           display-name="Used for uploading journals from external file to journal interface"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlJournalInterfaceUploadControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlJournalInterfaceUploadController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlJournalInterfaceUploadControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlJournalInterfaceUploadControllerBean extends AbstractSessionBean {     
      
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveGlJriEntry(com.util.GlJournalInterfaceDetails details, ArrayList glJournalLineInterfaceList, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
		GlobalAccountNumberInvalidException,
		GlJRIJournalNotBalanceException
		{
                    
        Debug.print("GlJournalInterfaceUploadControllerBean saveGlJriEntry");
        
        LocalGlJournalInterfaceHome glJournalInterfaceHome = null;        
        LocalGlJournalLineInterfaceHome glJournalLineInterfaceHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;        
                                             
                             
        //initialized EJB Home
        
        try {
            
            glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);              
            glJournalLineInterfaceHome = (LocalGlJournalLineInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalLineInterfaceHome.JNDI_NAME, LocalGlJournalLineInterfaceHome.class);    
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);    
                                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
		}
		
		try {
			
			// check if journal exists
			
			LocalGlJournalInterface glJournalInterface = glJournalInterfaceHome.findByJriName(
				details.getJriName(), AD_CMPNY);
				
		    throw new GlobalRecordAlreadyExistException();					
																
		} catch (GlobalRecordAlreadyExistException ex) {
			
			throw ex;
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			// check if total debits and total credits are balance
			// check account number if valid
			
			double TOTAL_DEBIT = 0d;
			double TOTAL_CREDIT = 0d;
			
			Iterator i = glJournalLineInterfaceList.iterator();
			
			while (i.hasNext()) {
				
				GlJournalLineInterfaceDetails mdetails = 
				    (GlJournalLineInterfaceDetails) i.next();
				
				try {
					
					LocalGlChartOfAccount glChartOfAccount = 
				    	glChartOfAccountHome.findByCoaAccountNumber(mdetails.getJliCoaAccountNumber(), AD_CMPNY);
				    	
				} catch (FinderException ex) {
					
					throw new GlobalAccountNumberInvalidException();
					
				}
				    
				if (mdetails.getJliDebit() == EJBCommon.TRUE) {
					
					TOTAL_DEBIT = TOTAL_DEBIT + mdetails.getJliAmount();
					
				} else {
					
					TOTAL_CREDIT = TOTAL_CREDIT + mdetails.getJliAmount();
					
				}			

				
			}
			
			if (TOTAL_DEBIT != TOTAL_CREDIT) {
				
				throw new GlJRIJournalNotBalanceException();
				
			}
			
		} catch (GlJRIJournalNotBalanceException ex) {
			
			throw ex;
			
	    } catch (GlobalAccountNumberInvalidException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		        
        try {           
                   
            LocalGlJournalInterface glJournalInterface =
                glJournalInterfaceHome.create(details.getJriName(), 
				    details.getJriDescription(), 
				    details.getJriEffectiveDate(), 
				    "GENERAL",
				    "MANUAL", "PHP", null, null, null, 1.0, 
				    'N', EJBCommon.FALSE, null, null, null, AD_BRNCH, AD_CMPNY);
				    
		    
		    Iterator i = glJournalLineInterfaceList.iterator();
			
			short ctr = 0;
			
			while (i.hasNext()) {
				
				GlJournalLineInterfaceDetails mdetails = 
				    (GlJournalLineInterfaceDetails) i.next();
				 
				ctr++;    
				
			 	LocalGlJournalLineInterface glJournalLineInterface = 
			 	    glJournalLineInterfaceHome.create(
			 	    	ctr,
      					mdetails.getJliDebit(), 
      					mdetails.getJliAmount(), mdetails.getJliCoaAccountNumber(), AD_CMPNY);    
				    
				glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
				
			}
            
                     
        	
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }  
                      
    }
    
     
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlJournalInterfaceUploadControllerBean ejbCreate");
      
    }
}
