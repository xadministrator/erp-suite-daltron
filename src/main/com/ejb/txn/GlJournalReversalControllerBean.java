package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlJREffectiveDateViolationException;
import com.ejb.exception.GlJRJournalAlreadyReversedException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlTransactionCalendarValue;
import com.ejb.gl.LocalGlTransactionCalendarValueHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModJournalDetails;

/**
 * @ejb:bean name="GlJournalReversalControllerEJB"
 *           display-name="Used for reversing journals"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlJournalReversalControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlJournalReversalController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlJournalReversalControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlJournalReversalControllerBean extends AbstractSessionBean {


   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlJcAll(Integer AD_CMPNY) {
                    
        Debug.print("GlJournalReversalControllerBean getGlJcAll");
        
        LocalGlJournalCategoryHome glJournalCategoryHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalCategories = glJournalCategoryHome.findJcAll(AD_CMPNY);
        	
        	Iterator i = glJournalCategories.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalCategory glJournalCategory = (LocalGlJournalCategory)i.next();
        		
        		list.add(glJournalCategory.getJcName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlJsAll(Integer AD_CMPNY) {
                    
        Debug.print("GlJournalReversalControllerBean getGlJsAll");
        
        LocalGlJournalSourceHome glJournalSourceHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalSources = glJournalSourceHome.findJsAll(AD_CMPNY);
        	
        	Iterator i = glJournalSources.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalSource glJournalSource = (LocalGlJournalSource)i.next();
        		
        		list.add(glJournalSource.getJsName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {
                    
        Debug.print("GlJournalReversalControllerBean getGlFcAllWithDefault");
        
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        Collection glFunctionalCurrencies = null;
        
        LocalGlFunctionalCurrency glFunctionalCurrency = null;
        LocalAdCompany adCompany = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	               
            glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
            	EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (glFunctionalCurrencies.isEmpty()) {
        	
        	return null;
        	
        }
        
        Iterator i = glFunctionalCurrencies.iterator();
               
        while (i.hasNext()) {
        	
        	glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
        	
        	GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
        	    glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
        	    adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
        	       EJBCommon.TRUE : EJBCommon.FALSE);
        	
        	list.add(mdetails);
        	
        }
        
        return list;
            
    }
    

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlJrReversibleByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("GlJournalReversalControllerBean getGlJrReversibleByCriteria");
      
      LocalGlJournalHome glJournalHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      
      ArrayList jrList = new ArrayList();
      
      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(jr) FROM GlJournal jr ");
      
      boolean firstArgument = true;
      short ctr = 0;
      int criteriaSize = criteria.size() + 2;
      Object obj[];
      
       // Allocate the size of the object parameter
    
      if (criteria.containsKey("journalName")) {
      	
      	  criteriaSize--; 
      	 
      } 
            
      
      obj = new Object[criteriaSize];
         
      
      if (criteria.containsKey("journalName")) {
      	
      	 if (!firstArgument) {
      	 
      	    jbossQl.append("AND ");	
      	 	
         } else {
         	
         	firstArgument = false;
         	jbossQl.append("WHERE ");
         	
         }
         
      	 jbossQl.append("jr.jrName LIKE '%" + (String)criteria.get("journalName") + "%' ");
      	 
      }            	 
        
      if (criteria.containsKey("dateFrom")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrEffectiveDate>=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("dateFrom");
	  	 ctr++;
	  }  
	      
	  if (criteria.containsKey("dateTo")) {
	  	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrEffectiveDate<=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("dateTo");
	  	 ctr++;
	  	 
	  }    
	  
	  if (criteria.containsKey("documentNumberFrom")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrDocumentNumber>=?" + (ctr+1) + " ");
	  	 obj[ctr] = (String)criteria.get("documentNumberFrom");
	  	 ctr++;
	  }  
	      
	  if (criteria.containsKey("documentNumberTo")) {
	  	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrDocumentNumber<=?" + (ctr+1) + " ");
	  	 obj[ctr] = (String)criteria.get("documentNumberTo");
	  	 ctr++;
	  	 
	  }
	  
      if (criteria.containsKey("reversalDateFrom")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrDateReversal>=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("reversalDateFrom");
	  	 ctr++;
	  }  
	      
	  if (criteria.containsKey("reversalDateTo")) {
	  	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("jr.jrDateReversal<=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("reversalDateTo");
	  	 ctr++;
	  	 
	  }	  
	  
	  if (criteria.containsKey("category")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jr.glJournalCategory.jcName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("category");
       	  ctr++;
       	  
      }
      
      if (criteria.containsKey("source")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jr.glJournalSource.jsName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("source");
       	  ctr++;
       	  
      }
      
      if (criteria.containsKey("currency")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("jr.glFunctionalCurrency.fcName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("currency");
       	  ctr++;
       	  
      }
                   	
      if (!firstArgument) {
       	  	
         jbossQl.append("AND ");
       	     
      } else {
       	  	
      	 firstArgument = false;
      	 jbossQl.append("WHERE ");
       	  	 
      }
      
      jbossQl.append("jr.jrReversed=0 AND jr.jrPosted=1 AND jr.jrAdBranch=" + AD_BRNCH + " AND jr.jrAdCompany=" + AD_CMPNY + " ");
       	     
      String orderBy = null;
	      
	  if (ORDER_BY.equals("NAME")) {
	
	  	  orderBy = "jr.jrName";
	  	  
	  } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
	
	  	  orderBy = "jr.jrDocumentNumber";
	  	
	  } else if (ORDER_BY.equals("CATEGORY")) {
	
	  	  orderBy = "jr.glJournalCategory.jcName";
	  	  
	  } else if (ORDER_BY.equals("SOURCE")) {
	
	  	  orderBy = "jr.glJournalSource.jsName";
	  	  
	  }
	  
	  if (orderBy != null) {
	  
	  	jbossQl.append("ORDER BY " + orderBy + ", jr.jrEffectiveDate");
	  	
	  } else {
	  	
	  	jbossQl.append("ORDER BY jr.jrEffectiveDate");
	  	
	  }
               
      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;
      
      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      
      System.out.println("QL + " + jbossQl);
      
      

      Collection glJournals = null;
      
      double TOTAL_DEBIT = 0;
      double TOTAL_CREDIT = 0;
      
      try {
      	
         glJournals = glJournalHome.getJrByCriteria(jbossQl.toString(), obj);
         
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	 
      }
      
      if (glJournals.isEmpty())
         throw new GlobalNoRecordFoundException();
         
      Iterator i = glJournals.iterator();
      while (i.hasNext()) {
      	
      	 TOTAL_DEBIT = 0d;
      	 TOTAL_CREDIT = 0d;
      	
         LocalGlJournal glJournal = (LocalGlJournal) i.next();
         
	  	 Collection glJournalLines = glJournal.getGlJournalLines();
	  	 
	  	 Iterator j = glJournalLines.iterator();
	  	 while (j.hasNext()) {
	  	 	
	  	 	LocalGlJournalLine glJournalLine = (LocalGlJournalLine) j.next();
	  	 	
		        if (glJournalLine.getJlDebit() == EJBCommon.TRUE) {
		     	
		           TOTAL_DEBIT += this.convertForeignToFunctionalCurrency(
		           	   glJournal.getGlFunctionalCurrency().getFcCode(),
		           	   glJournal.getGlFunctionalCurrency().getFcName(),
		           	   glJournal.getJrConversionDate(),
		           	   glJournal.getJrConversionRate(),
		           	   glJournalLine.getJlAmount(), AD_CMPNY);
		        
		        } else {
		     	
		           TOTAL_CREDIT += this.convertForeignToFunctionalCurrency(
		           	   glJournal.getGlFunctionalCurrency().getFcCode(),
		           	   glJournal.getGlFunctionalCurrency().getFcName(),
		           	   glJournal.getJrConversionDate(),
		           	   glJournal.getJrConversionRate(),
		           	   glJournalLine.getJlAmount(), AD_CMPNY);
		        }
		        
		     }
		     
		     GlModJournalDetails mdetails = new GlModJournalDetails();
		     mdetails.setJrCode(glJournal.getJrCode());
		     mdetails.setJrName(glJournal.getJrName());
		     mdetails.setJrDescription(glJournal.getJrDescription());
		     mdetails.setJrEffectiveDate(glJournal.getJrEffectiveDate());
		     mdetails.setJrDocumentNumber(glJournal.getJrDocumentNumber());
		     mdetails.setJrDateReversal(glJournal.getJrDateReversal());
		     mdetails.setJrTotalDebit(TOTAL_DEBIT);
		     mdetails.setJrTotalCredit(TOTAL_CREDIT);    	     	        		     

      	     jrList.add(mdetails);
		          	 
	     }
	     
      return jrList;
  
   }
   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("GlJournalReversalControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }

    
    
    /**
    * @ejb:interface-method view-type="remote"
    **/
    public void executeGlJrReverse(Integer JR_CODE, String JR_DCMNT_NMBR, 
       Date JR_DT_RVRSL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws 
       GlobalRecordAlreadyExistException,
	   GlobalRecordAlreadyDeletedException,
	   GlJREffectiveDateNoPeriodExistException,
       GlobalDocumentNumberNotUniqueException,
	   GlJREffectiveDateViolationException,
	   GlJREffectiveDatePeriodClosedException,
	   GlJRJournalAlreadyReversedException {

       Debug.print("GlJournalLineControllerBean executeGlJrReverse");

      
       LocalGlSetOfBookHome glSetOfBookHome = null;
       LocalGlJournalHome glJournalHome = null;
       LocalGlJournalBatchHome glJournalBatchHome = null;
       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
       LocalGlJournalCategoryHome glJournalCategoryHome = null;
       LocalGlJournalSourceHome glJournalSourceHome = null;
       LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
       LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
       LocalGlTransactionCalendarValueHome glTransactionCalendarValueHome = null;
       LocalGlJournalLineHome glJournalLineHome = null;
       LocalAdPreferenceHome adPreferenceHome = null;
       LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       LocalGlForexLedgerHome glForexLedgerHome = null;
       LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;       
       
       // Initialize EJB Home
        
       try {
            
           glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);       
           glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);          
           glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
           	  lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
           glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);          
           glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);          
           glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);          
           glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
           adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);              
           glTransactionCalendarValueHome = (LocalGlTransactionCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlTransactionCalendarValueHome.JNDI_NAME, LocalGlTransactionCalendarValueHome.class);              
           glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);              
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
           	  lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
           adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
		      lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		      lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
           glForexLedgerHome  = (LocalGlForexLedgerHome)EJBHomeFactory.
		   	  lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
           glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
		   	  lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);           
                        
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
       	   // get journal
       	
       	   LocalGlJournal glJournal = null;

       	   try {
       	   	
       	   	   glJournal = glJournalHome.findByPrimaryKey(JR_CODE);
       	   	
       	   } catch (FinderException ex) {
       	   	
       	   	   throw new GlobalRecordAlreadyDeletedException();
       	   	
       	   }
       	   
	        // validate if journal exists
	        		
			try {
				java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
			    LocalGlJournal glExistingJournal = glJournalHome.findByJrName("REVERSED " + formatter.format(new Date()), AD_CMPNY);
			
			    if (JR_CODE == null ||
			        JR_CODE != null && !glExistingJournal.getJrCode().equals(JR_CODE)) {
			    	
			        throw new GlobalRecordAlreadyExistException(glJournal.getJrName());
			        
			    }
			 			
			} catch (FinderException ex) {
			    
			}      	   
			
			// validate if effective date has no period and period is closed
			
			LocalGlSetOfBook glSetOfBook = null;
			
		   try {			
		   
		       glSetOfBook = glSetOfBookHome.findByDate(JR_DT_RVRSL, AD_CMPNY);
			   				
		       LocalGlAccountingCalendarValue glAccountingCalendarValue = 
			       glAccountingCalendarValueHome.findByAcCodeAndDate(
			       	glSetOfBook.getGlAccountingCalendar().getAcCode(), JR_DT_RVRSL, AD_CMPNY);
			    	
				if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
				    glAccountingCalendarValue.getAcvStatus() == 'C' ||
				    glAccountingCalendarValue.getAcvStatus() == 'P') {
				    	
				    throw new GlJREffectiveDatePeriodClosedException(glJournal.getJrName());
				    
				}
						
		   } catch (FinderException ex) {
				
			   throw new GlJREffectiveDateNoPeriodExistException(glJournal.getJrName());
					
		   } 
 

			// validate reversal date violation	
	    		  	
	        LocalGlTransactionCalendarValue glTransactionCalendarValue = 
	            glTransactionCalendarValueHome.findByTcCodeAndTcvDate(glSetOfBook.getGlTransactionCalendar().getTcCode(),
	               JR_DT_RVRSL, AD_CMPNY);
	               
	        LocalGlJournalSource glValidateJournalSource = glJournalSourceHome.findByJsName("JOURNAL REVERSAL", AD_CMPNY);          
	              
	        if (glTransactionCalendarValue.getTcvBusinessDay() == EJBCommon.FALSE &&
			        glValidateJournalSource.getJsEffectiveDateRule() == 'F') {
			      	
			        throw new GlJREffectiveDateViolationException(glJournal.getJrName());
			      		      	
			}     	   
		   
	        // if validate if document number is unique document number is automatic then set next sequence
	
			try {
				
			    LocalGlJournal glExistingDocumentJournal = 
	               glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(JR_DCMNT_NMBR, "MANUAL", AD_BRNCH, AD_CMPNY);
	               
	            throw new GlobalDocumentNumberNotUniqueException(glJournal.getJrName());
				    
			} catch (FinderException ex) { 
			
			}
					
	        LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment =
	            adDocumentSequenceAssignmentHome.findByDcName("GL JOURNAL", AD_CMPNY);
	        
	        LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	        
	        try {
	        	
	        	adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
	        	
	        } catch (FinderException ex) {
	        	
	        }

	        
	        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
	            (JR_DCMNT_NMBR == null || JR_DCMNT_NMBR.trim().length() == 0)) {
	            	
	            while (true) {
	            	
	            	if ( adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null ) {

	            		try {
		            		
		            		glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
		            		
		            	} catch (FinderException ex) {
		            		
		            		JR_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
				            break;
		            		
		            	}	            	        			            	
	            		
	            	} else {

	            		try {
		            		
		            		glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
		            		
		            	} catch (FinderException ex) {
		            		
		            		JR_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
				            break;
		            		
		            	}	            	        			            	
	            		
	            	}
	            	
	            }		            
	            		            	
	        }           		  		   
 
	       // validate if journal is already reversed
	       
	       if (glJournal.getJrReversed() == EJBCommon.TRUE) {
	       	
	       	   throw new GlJRJournalAlreadyReversedException(glJournal.getJrName());	       	
	       }
	       
		   // generate approval status
		    
		   LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("JOURNAL REVERSAL", AD_CMPNY);
		   String JR_APPRVL_STATUS = null;
			    			
		   if (glJournalSource.getJsJournalApproval() == EJBCommon.FALSE) {
    	    	    	    	
    	    	JR_APPRVL_STATUS = "N/A";
    	    	
    	   }
		   
		   // create reversal journal batch if necessary
		   
		   LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
		   LocalGlJournalBatch glJournalBatch = null;
		   
		   if (adPreference.getPrfEnableGlJournalBatch() == EJBCommon.TRUE) {
		   	
		   	   java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
		       glJournalBatch = glJournalBatchHome.create(glJournal.getJrName() != null ? ("REVERSED " + glJournal.getJrName()) : ("JOURNAL REVERSAL " + formatter.format(new Date())), "JOURNAL REVERSAL", "OPEN", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
		   
		   }
		   
		   // create reversal journal
		   
       	   LocalGlJournal glNewJournal = glJournalHome.create(
		    	glJournal.getJrName() != null ? ("REVERSED " + glJournal.getJrName()) : null, glJournal.getJrDescription(),
		    	JR_DT_RVRSL, 0.0d,
		    	null, JR_DCMNT_NMBR,
		    	glJournal.getJrConversionDate(), glJournal.getJrConversionRate(),
		    	JR_APPRVL_STATUS, null, 'N', EJBCommon.FALSE, EJBCommon.TRUE,
		    	USR_NM, new Date(),
		    	USR_NM, new Date(),
		    	null, null, null, null, glJournal.getJrTin(), glJournal.getJrSubLedger(), EJBCommon.FALSE,
				glJournal.getJrReferenceNumber(), 
				AD_BRNCH, AD_CMPNY);  
           
           //glJournalSource.addGlJournal(glNewJournal);
           glNewJournal.setGlJournalSource(glJournalSource);
        	
           LocalGlFunctionalCurrency glFunctionalCurrency = glJournal.getGlFunctionalCurrency();
           glNewJournal.setGlFunctionalCurrency(glFunctionalCurrency);
        	
           LocalGlJournalCategory glJournalCategory = glJournal.getGlJournalCategory();
           glNewJournal.setGlJournalCategory(glJournalCategory);
           
           if (glJournalBatch != null) {
           
               glNewJournal.setGlJournalBatch(glJournalBatch);
           
           }       
           
           // reverse line
           
           Collection glJournalLines = glJournal.getGlJournalLines();
           
	  	   Iterator i = glJournalLines.iterator();	  	   
	  	      	  	  	  
	  	   while (i.hasNext()) {

	  	   	   LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();
	  	   	   
	  	   	   LocalGlJournalLine glNewJournalLine = null;
	  	   	   
	  	   	   if (glJournal.getGlJournalCategory().getJcReversalMethod() == 'S') {

	  	   	   		glNewJournalLine = glJournalLineHome.create(
					    glJournalLine.getJlLineNumber(), 
					    glJournalLine.getJlDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE, 
					    glJournalLine.getJlAmount(), glJournalLine.getJlDescription(), AD_CMPNY);
					    
					//glJournalLine.getGlChartOfAccount().addGlJournalLine(glNewJournalLine);
					glNewJournalLine.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());
					//glNewJournal.addGlJournalLine(glNewJournalLine);
					glNewJournalLine.setGlJournal(glNewJournal);
	  	   	   	   	  	   	   	
	  	   	   } else {
	  	   	   	   
	  	   	   	   
	  	   	   	    glNewJournalLine = glJournalLineHome.create(
					    glJournalLine.getJlLineNumber(), 
					    glJournalLine.getJlDebit(), 
					    glJournalLine.getJlAmount() * -1, glJournalLine.getJlDescription(), AD_CMPNY);
					    
					//glJournalLine.getGlChartOfAccount().addGlJournalLine(glNewJournalLine);
					glNewJournalLine.setGlChartOfAccount(glJournalLine.getGlChartOfAccount());
					//glNewJournal.addGlJournalLine(glNewJournalLine);
					glNewJournalLine.setGlJournal(glNewJournal);
	  	   	   	
	  	   	   }
	  	   	   
	  	   	   // for FOREX revaluation
	  	   	   
	  	   	   LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY); 
	  	   	   
	  	   	   if((glNewJournal.getGlFunctionalCurrency().getFcCode() !=
	  	   	   	adCompany.getGlFunctionalCurrency().getFcCode()) &&
				glNewJournalLine.getGlChartOfAccount().getGlFunctionalCurrency() != null &&
				(glNewJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcCode().equals(
					glNewJournal.getGlFunctionalCurrency().getFcCode()))){
	  	   	   	
	  	   	   	double CONVERSION_RATE = 1;
	  	   	   	
	  	   	   	if (glJournal.getJrConversionRate() != 0 && glNewJournal.getJrConversionRate() != 1) {
	  	   	   		
	  	   	   		CONVERSION_RATE = glNewJournal.getJrConversionRate();
	  	   	   		
	  	   	   	} else if (glJournal.getJrConversionDate() != null){
	  	   	   		
	  	  			CONVERSION_RATE = this.getFrRateByFrNameAndFrDate(
		  	  				glJournalLine.getGlChartOfAccount().getGlFunctionalCurrency().getFcName(), 
							glJournal.getJrConversionDate(), AD_CMPNY);
	  	   	   		
	  	   	   	}
	  	   	   	
	  	   	   	Collection glForexLedgers = null;
	  	   	   	
	  	   	   	try {
	  	   	   		
	  	   	   		glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(
	  	   	   				glJournal.getJrEffectiveDate(), glNewJournalLine.getGlChartOfAccount().getCoaCode(),
							AD_CMPNY);
	  	   	   		
	  	   	   	} catch(FinderException ex) {
	  	   	   		
	  	   	   	}
	  	   	   	
	  	   	   	LocalGlForexLedger glForexLedger =
	  	   	   		(glForexLedgers.isEmpty() || glForexLedgers == null) ? null :
	  	   	   			(LocalGlForexLedger) glForexLedgers.iterator().next();
	  	   	   	
	  	   	   	int FRL_LN = (glForexLedger != null &&
	  	   	   			glForexLedger.getFrlDate().compareTo(glJournal.getJrEffectiveDate()) == 0) ?
	  	   	   					glForexLedger.getFrlLine().intValue() + 1 : 1;
	  	   	   	
	  	   	   	// compute balance
	  	   	   	double COA_FRX_BLNC = glForexLedger == null ? 0 : glForexLedger.getFrlBalance();
	  	   	   	double FRL_AMNT = glNewJournalLine.getJlAmount();
	  	   	   	
	  	   	   	if(glNewJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET")) {
	  	   	   		
	  	   	   		FRL_AMNT = (glNewJournalLine.getJlDebit() == 1 ? FRL_AMNT : (- 1 * FRL_AMNT));  
	  	   	   		
	  	   	   	} else {
	  	   	   		
	  	   	   		FRL_AMNT = (glNewJournalLine.getJlDebit() == 1 ? (- 1 * FRL_AMNT) : FRL_AMNT);
	  	   	   		
	  	   	   	}
	  	   	   	
	  	   	   	COA_FRX_BLNC = COA_FRX_BLNC + FRL_AMNT;
	  	   	   	
	  	   	   	glForexLedger = glForexLedgerHome.create(glNewJournal.getJrEffectiveDate(), new Integer (FRL_LN),
	  	   	   			"JOURNAL", FRL_AMNT, CONVERSION_RATE, COA_FRX_BLNC, 0, AD_CMPNY);
	  	   	   	
	  	   	   	//glNewJournalLine.getGlChartOfAccount().addGlForexLedger(glForexLedger);
	  	   	   	glForexLedger.setGlChartOfAccount(glNewJournalLine.getGlChartOfAccount());
	  	   	   	
	  	   	   	// propagate balances
	  	   	   	try{
	  	   	   		
	  	   	   		glForexLedgers = glForexLedgerHome.findByGreaterThanFrlDateAndCoaCode(
	  	   	   				glForexLedger.getFrlDate(), glForexLedger.getGlChartOfAccount().getCoaCode(),
							glForexLedger.getFrlAdCompany());
	  	   	   		
	  	   	   	} catch (FinderException ex) {
	  	   	   		
	  	   	   	}
	  	   	   	
	  	   	   	Iterator itrFrl = glForexLedgers.iterator();
	  	   	   	
	  	   	   	while (itrFrl.hasNext()) {
	  	   	   		
	  	   	   		glForexLedger = (LocalGlForexLedger) itrFrl.next();
	  	   	   		FRL_AMNT = glNewJournalLine.getJlAmount();
	  	   	   		
	  	   	   		if(glNewJournalLine.getGlChartOfAccount().getCoaAccountType().equalsIgnoreCase("ASSET"))
	  	   	   			FRL_AMNT = (glNewJournalLine.getJlDebit() == 1 ? FRL_AMNT : (- 1 * FRL_AMNT));
	  	   	   		else
	  	   	   			FRL_AMNT = (glNewJournalLine.getJlDebit() == 1 ? (- 1 * FRL_AMNT) : FRL_AMNT);
	  	   	   		
	  	   	   		glForexLedger.setFrlBalance( glForexLedger.getFrlBalance() + FRL_AMNT);
	  	   	   		
	  	   	   	}
	  	   	   	
	  	   	   }

	  	   }                                
	  	   
	  	   
	  	   // set journal status and log
	  	   glJournal.setJrReversed(EJBCommon.TRUE);
	  	   glJournal.setJrLastModifiedBy(USR_NM);
	  	   glJournal.setJrDateLastModified(EJBCommon.getGcCurrentDateWoTime().getTime());
           
	
       } catch (GlobalRecordAlreadyExistException ex) {
       	
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       	
       } catch (GlJREffectiveDateNoPeriodExistException ex) {
       	
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       	
       } catch (GlobalDocumentNumberNotUniqueException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       	 
       } catch (GlJREffectiveDateViolationException ex) {
       	
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       	
       } catch (GlJRJournalAlreadyReversedException ex) {
       	
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       	
       } catch (GlobalRecordAlreadyDeletedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       
       } catch (GlJREffectiveDatePeriodClosedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
                                             
       } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
       }

    }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlJournalBatchControllerBean ejbCreate");
      
   }

   // private methods

   private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("GlJournalReversalControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT / CONVERSION_RATE;
             	
         }
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
   
   private double getFrRateByFrNameAndFrDate(String FC_NM, Date CONVERSION_DATE, Integer AD_CMPNY) 
   throws GlobalConversionDateNotExistException {
   	
		Debug.print("GlJournalReversalControllerBean getFrRateByFrNameAndFrDate");
		
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null; 
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			
			double CONVERSION_RATE = 1;
			
			// Get functional currency rate
			
			if (!FC_NM.equals("USD")) {
				
				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(glFunctionalCurrency.getFcCode(),
							CONVERSION_DATE, AD_CMPNY);
				
				CONVERSION_RATE = glFunctionalCurrencyRate.getFrXToUsd();
				
			}
			
			// Get set of book functional currency rate if necessary
			
			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
				
				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency(). getFcCode(),
							CONVERSION_DATE, AD_CMPNY);
				
				CONVERSION_RATE = CONVERSION_RATE / glCompanyFunctionalCurrencyRate.getFrXToUsd();
				
			}
			
			return CONVERSION_RATE;
			
		} catch (FinderException ex) {	
			
			getSessionContext().setRollbackOnly();
			throw new GlobalConversionDateNotExistException();  
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

  
}
