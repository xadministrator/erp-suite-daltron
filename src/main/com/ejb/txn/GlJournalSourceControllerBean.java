package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.GlJSJournalSourceAlreadyAssignedException;
import com.ejb.exception.GlJSJournalSourceAlreadyDeletedException;
import com.ejb.exception.GlJSJournalSourceAlreadyExistException;
import com.ejb.exception.GlJSNoJournalSourceFoundException;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlJournalSourceDetails;

/**
 * @ejb:bean name="GlJournalSourceControllerEJB"
 *           display-name="Used for maintenance of journal sources"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlJournalSourceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlJournalSourceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlJournalSourceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlJournalSourceControllerBean extends AbstractSessionBean {
   

   /*******************************************************************
   
   	Business methods:

	(1) getJsAll - returns an Arraylist of JS

	(2) addGlJsEntry - validate input and duplicate entries
	                   before adding the JS

	(3) updateGlJsEntry - validate if JS to be updated is
	                      already assigned to related
	                      tables, if already assigned then
	                      description field is the only
	                      field that can be updated, otherwise
	                      all fields can be entered

	(4) deleteGlJsEntry - validate if the JS to be deleted is
	                      already assigned to related tables,
	                      if already assigned then this JS
	                      cannot be deleted

	
	Private methods:

	(1) hasRelation - returns true if a specific PT has a relation
	                  with any other table otherwise returns
	                  false
 
      
   *******************************************************************/
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlJsAll(Integer AD_CMPNY)
      throws GlJSNoJournalSourceFoundException {

      Debug.print("GlJournalSourceControllerBean getGlJsAll");

      /***************************************************************
	Gets all JS	
      ***************************************************************/

      ArrayList jsAllList = new ArrayList();
      Collection glJournalCategories = null;
      
      LocalGlJournalSourceHome glJournalSourceHome = null; 
      
      // Initialize EJB Home
        
      try {

          glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);

      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }
      
      try {
         glJournalCategories = glJournalSourceHome.findJsAll(AD_CMPNY);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glJournalCategories.size() == 0)
         throw new GlJSNoJournalSourceFoundException();
         
      Iterator i = glJournalCategories.iterator();
      while (i.hasNext()) {
         LocalGlJournalSource glJournalSource = (LocalGlJournalSource) i.next();
         GlJournalSourceDetails details = new GlJournalSourceDetails(
            glJournalSource.getJsCode(), glJournalSource.getJsName(), 
            glJournalSource.getJsDescription(), glJournalSource.getJsFreezeJournal(),
	    glJournalSource.getJsJournalApproval(), glJournalSource.getJsEffectiveDateRule());
         jsAllList.add(details);
      }

      return jsAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void addGlJsEntry(GlJournalSourceDetails details, Integer AD_CMPNY)
      throws GlJSJournalSourceAlreadyExistException {

      Debug.print("GlJournalSourceControllerBean addGlJsEntry");

      /**************************
	Adds a JS entry
      **************************/
      LocalGlJournalSourceHome glJournalSourceHome = null;
      
      // Initialize EJB Home
        
      try {

          glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);

      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }
	  
      try {
      	
      	 glJournalSourceHome.findByJsName(details.getJsName(), AD_CMPNY);
         
         getSessionContext().setRollbackOnly();
         throw new GlJSJournalSourceAlreadyExistException();
      	
      } catch (FinderException ex) {
      	
      	      	
      }
      
      try {
         LocalGlJournalSource glJournalSource = glJournalSourceHome.create(
	    details.getJsName(), details.getJsDescription(),
	    details.getJsFreezeJournal(), details.getJsJournalApproval(),
	    details.getJsEffectiveDateRule(), AD_CMPNY);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
	 throw new EJBException(ex.getMessage()); 
      }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlJsEntry(GlJournalSourceDetails details, Integer AD_CMPNY)
      throws GlJSJournalSourceAlreadyExistException,
      GlJSJournalSourceAlreadyAssignedException,
      GlJSJournalSourceAlreadyDeletedException {

      Debug.print("GlJournalSourceControllerBean updateGlJsEntry");

      /*******************************
	Updates an existing JS entry
      *******************************/

      LocalGlJournalSource glJournalSource = null;
      
      LocalGlJournalSourceHome glJournalSourceHome = null;

      // Initialize EJB Home			
      
      try {

          glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);

      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }      
      
      try {
         glJournalSource = glJournalSourceHome.findByPrimaryKey(
	    details.getJsCode());
      } catch (FinderException ex){
      	throw new GlJSJournalSourceAlreadyDeletedException();
      } catch (Exception ex) {
        throw new EJBException(ex.getMessage());
      }

      if (hasRelation(glJournalSource, AD_CMPNY) && !glJournalSource.getJsName().equals(details.getJsName()))
         throw new GlJSJournalSourceAlreadyAssignedException();
      else {
       
         LocalGlJournalSource glJournalSource2 = null;

         try {
	    glJournalSource2 = 
	       glJournalSourceHome.findByJsName(details.getJsName(), AD_CMPNY);
	 } catch (FinderException ex) {
	    glJournalSource.setJsName(details.getJsName());
	    glJournalSource.setJsDescription(details.getJsDescription());
	    glJournalSource.setJsFreezeJournal(details.getJsFreezeJournal());
	    glJournalSource.setJsJournalApproval(details.getJsJournalApproval());
	    glJournalSource.setJsEffectiveDateRule(details.getJsEffectiveDateRule());

	 } catch (Exception ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 }

	 if(glJournalSource2 != null && !glJournalSource.getJsCode().equals(glJournalSource2.getJsCode())) {
	    getSessionContext().setRollbackOnly();
	    throw new GlJSJournalSourceAlreadyExistException();
	 } else 
	    if(glJournalSource2 != null && glJournalSource.getJsCode().equals(glJournalSource2.getJsCode())) {
	       glJournalSource.setJsDescription(details.getJsDescription());
	       glJournalSource.setJsFreezeJournal(details.getJsFreezeJournal());
	       glJournalSource.setJsJournalApproval(details.getJsJournalApproval());
	       glJournalSource.setJsEffectiveDateRule(details.getJsEffectiveDateRule());
	 }
      }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteGlJsEntry(Integer JS_CODE, Integer AD_CMPNY) 
      throws GlJSJournalSourceAlreadyDeletedException,
      GlJSJournalSourceAlreadyAssignedException {

      Debug.print("GlJournalSourceControllerBean deleteGlJsEntry");

      /*******************************
	Deletes an existing JS entry      
      *******************************/
      
      LocalGlJournalSource glJournalSource = null;
      
      LocalGlJournalSourceHome glJournalSourceHome = null;

      // Initialize EJB Home
        
      try {

          glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);

      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }      
      
      try {
         glJournalSource = glJournalSourceHome.findByPrimaryKey(JS_CODE);
      } catch (FinderException ex) {
         throw new GlJSJournalSourceAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (hasRelation(glJournalSource, AD_CMPNY))
         throw new GlJSJournalSourceAlreadyAssignedException();
      else {
         try {
	    glJournalSource.remove();
	 } catch (RemoveException ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 } catch (Exception ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 }
      }
   }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {
      Debug.print("GlJournalSourceControllerBean ejbCreate");
      
   }

   // private methods

   private boolean hasRelation(LocalGlJournalSource glJournalSource, Integer AD_CMPNY) {

      Debug.print("GlJournalSourceControllerBean hasRelation");

      if(!glJournalSource.getGlSuspenseAccounts().isEmpty() ||
         !glJournalSource.getGlJournals().isEmpty()) {
      	
      	return true;
      	
      } 
      
      return false;

   }
   
}
