
/*
 * GlJournalSyncControllerBean
 *
 * Created on 2007
 *
 * @author 
 */

package com.ejb.txn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;
import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlJournalLine;
import com.util.GlModJournalDetails;
import com.util.GlModJournalLineDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="GlJournalSyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *           
 * @wsee:port-component name="GlJournalSync"
 *
 * @jboss:port-component uri="omega-ejb/GlJournalSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.GlJournalSyncWS"
 * 
 */

public class GlJournalSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;	
	
	/**
	 * @ejb:interface-method
	 **/
	public int setGlJournalAllNew(String[] newJR, String BR_BRNCH_CODE, Integer AD_CMPNY) {               
	
		
		Debug.print("GlJournalSyncControllerControllerBean setGlJournalAllNew");        
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        //LocalGlJournalCategoryHome glJournalCategoryHome = null;
        //LocalGlJournalSourceHome glJournalSourceHome=null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome =null;
        //LocalGlJournalBatchHome glJournalBatchHome=null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBranchHome adBranchHome = null; 
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome=null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		
		try {
			
			glJournalHome=(LocalGlJournalHome)EJBHomeFactory.lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
			glJournalBatchHome=(LocalGlJournalBatchHome)EJBHomeFactory.lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
			glJournalCategoryHome=(LocalGlJournalCategoryHome)EJBHomeFactory.lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
			glJournalSourceHome=(LocalGlJournalSourceHome)EJBHomeFactory.lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
			glJournalLineHome=(LocalGlJournalLineHome)EJBHomeFactory.lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);			
			//glJournalSourceHome=(LocalGlJournalSourceHome)EJBHomeFactory.lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);			
			glFunctionalCurrencyHome=(LocalGlFunctionalCurrencyHome)EJBHomeFactory.lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);			
			//glJournalBatchHome=(LocalGlJournalBatchHome)EJBHomeFactory.lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);					
			
		}
		catch (NamingException ex) {			
			throw new EJBException(ex.getMessage());			
		}		
		
		try {			

			int success = 0;	
			
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			

			for(int i=0;i<newJR.length;i++) 
			{					            
				GlModJournalDetails glModJournalDetails = journalDecode(newJR[i]);
				
				glModJournalDetails.setJrName("Journal Upload "+glModJournalDetails.getJrDocumentNumber());
				
				LocalGlJournal glJournal = glJournalHome.create(glModJournalDetails.getJrName(), 
						glModJournalDetails.getJrDescription(),glModJournalDetails.getJrEffectiveDate(),
						glModJournalDetails.getJrControlTotal(),glModJournalDetails.getJrDateReversal(),
						glModJournalDetails.getJrDocumentNumber(),glModJournalDetails.getJrConversionDate(),
						glModJournalDetails.getJrConversionRate(),glModJournalDetails.getJrApprovalStatus(),						
						glModJournalDetails.getJrReasonForRejection(),glModJournalDetails.getJrFundStatus(),
						glModJournalDetails.getJrPosted(),glModJournalDetails.getJrReversed(),
						glModJournalDetails.getJrCreatedBy(),glModJournalDetails.getJrDateCreated(),
						glModJournalDetails.getJrLastModifiedBy(),glModJournalDetails.getJrDateLastModified(),
						glModJournalDetails.getJrApprovedRejectedBy(),glModJournalDetails.getJrDateApprovedRejected(),
						glModJournalDetails.getJrPostedBy(),glModJournalDetails.getJrDatePosted(),						
						glModJournalDetails.getJrTin(),glModJournalDetails.getJrSubLedger(),
						glModJournalDetails.getJrPrinted(),glModJournalDetails.getJrReferenceNumber(),
						adBranch.getBrCode(),AD_CMPNY);
				

				try {
					
        			LocalGlJournalBatch glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL SYNC",adBranch.getBrCode(),AD_CMPNY);
        			glJournal.setGlJournalBatch(glJournalBatch);
        			
        		} catch (Exception ex) {
        			
        			Debug.print("Error Journal Batch:"+ ex.getMessage());
        		}
        		
        		try {
        			
        			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("GENERAL",AD_CMPNY);
        			glJournal.setGlJournalCategory(glJournalCategory);
        			
        		} catch (Exception ex) {
        			Debug.print("Error Journal Category:"+ ex.getMessage());
        			ctx.setRollbackOnly();
        			throw new EJBException (ex.getMessage());
        		}
        		
        		try {
        			
        			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("MANUAL",AD_CMPNY);
        			glJournal.setGlJournalSource(glJournalSource);
        			
        		} catch (Exception ex) {
        			Debug.print("Error Journal Source:"+ ex.getMessage());
        			ctx.setRollbackOnly();
        			throw new EJBException (ex.getMessage());
        		}
        		
        		try {
        			
        			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(glModJournalDetails.getJrFcName(),AD_CMPNY);
        			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
        			
        		} catch (Exception ex) {
        			Debug.print("Error Functional Currency:"+ ex.getMessage());
        			ctx.setRollbackOnly();
        			throw new EJBException (ex.getMessage());
        		}
				Iterator iter = glModJournalDetails.getJrJlList().iterator();
				short lineNumber = 0;
				while (iter.hasNext()){
	            								        		
					 GlModJournalLineDetails glModJournalLineDetails = (GlModJournalLineDetails)iter.next();					
					
					 LocalGlJournalLine glJournalLine = glJournalLineHome.create(++lineNumber,
							glModJournalLineDetails.getJlDebit(),glModJournalLineDetails.getJlAmount(),
							glModJournalLineDetails.getJlDescription(),AD_CMPNY);
					 
					 glJournal.addGlJournalLine(glJournalLine);
					 
					 try {						
							LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(glModJournalLineDetails.getJlCoaAccountNumber(),AD_CMPNY);
							glChartOfAccount.addGlJournalLine(glJournalLine);						
						} catch (Exception ex) {
							System.out.println("Error Journal Line COA:"+ ex.getMessage());
							ctx.setRollbackOnly();
							throw new EJBException (ex.getMessage());
						}
					 
					
	          }	//end of while			
		 }// end of for loop
			
			
			success=1;
		Debug.print(newJR+" is Accepted");
		return success;
		
		} catch (Exception ex) {
			
			ex.printStackTrace();
			ctx.setRollbackOnly();
			throw new EJBException (ex.getMessage());
			
		}
	}       
	
	
	private GlModJournalDetails journalDecode(String strJournal) throws Exception {			
		Debug.print("GlJournalSyncControllerControllerBean journalDecode");		
		
		String separator = "$";		
		GlModJournalDetails glModJournalDetails = new GlModJournalDetails();
		
		//Remove first $ character				
		strJournal = strJournal.substring(1);		
	
		//Name		
		int start = 0;
		int nextIndex = strJournal.indexOf(separator, start);
		int length = nextIndex - start;		
		glModJournalDetails.setJrName(strJournal.substring(start, start + length));
				
		//Description
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrDescription(strJournal.substring(start, start + length));		
		
		//Effectivity Date
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        sdf.setLenient(false);
        try {
        	glModJournalDetails.setJrEffectiveDate(sdf.parse(strJournal.substring(start, start + length)));        	
        } catch (Exception ex) {
        	
        	throw ex;
        }		
		
		//Control Total
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrControlTotal(Double.parseDouble(strJournal.substring(start, start + length)));		
	
		//Document Number
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrDocumentNumber(strJournal.substring(start, start + length));		
		
		//Conversion Date
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
        try {        	
        	//glModJournalDetails.setJrConversionDate(sdf.parse(strJournal.substring(start, start + length))); 
        	//glModJournalDetails.setJrConversionDate(null);        	
        } catch (Exception ex) {
        	
        	throw ex;
        }				
	
		//Conversion Rate
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrConversionRate(Double.parseDouble(strJournal.substring(start, start + length)));		
	
		//Approval Status
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		//glModJournalDetails.setJrApprovalStatus(strJournal.substring(start, start + length));		
	
		//Find Status
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrFundStatus(strJournal.substring(start, start + length).charAt(0));				
	
		//Posted
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrPosted((byte)Integer.parseInt(strJournal.substring(start, start + length)));
		
		//Reversed
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrReversed((byte)Integer.parseInt(strJournal.substring(start, start + length)));
		
		//Created By
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrCreatedBy(strJournal.substring(start, start + length));
	
		//Date Created
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;		
        try {        	
        	glModJournalDetails.setJrDateCreated(sdf.parse(strJournal.substring(start, start + length)));  
        } catch (Exception ex) {        	
        	throw ex;
        }		
	
		//Last Modified By
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrLastModifiedBy(strJournal.substring(start, start + length));
	
		//Date Last Modified
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
        try {        	
        	glModJournalDetails.setJrDateLastModified(sdf.parse(strJournal.substring(start, start + length)));
        } catch (Exception ex) {
        	
        	throw ex;
        }
	
        //TIN
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		//glModJournalDetails.setJrTin(strJournal.substring(start, start + length));
	
		//Printed
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrPrinted((byte)Integer.parseInt(strJournal.substring(start, start + length)));
	
		//Reference Number
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrReferenceNumber(strJournal.substring(start, start + length));
		
		//Functional Currency
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		glModJournalDetails.setJrFcName(strJournal.substring(start, start + length));
	
		//Ad Branch
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		//glModJournalDetails.setJrAdBranch(Integer.parseInt(strJournal.substring(start, start + length)));
		
		//Ad Company
		start = nextIndex + 1;		
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		//glModJournalDetails.setJrAdCompany(Integer.parseInt(strJournal.substring(start, start + length)));
		
		//end separator
		start = nextIndex + 1;
		nextIndex = strJournal.indexOf(separator, start);
		length = nextIndex - start;
		
		String lineSeparator = "~";
		
		// begin lineSeparator
		start = nextIndex + 1;
		nextIndex = strJournal.indexOf(lineSeparator, start);
		length = nextIndex - start;
		
		ArrayList glJlList = new ArrayList();
		short lineNumber=0;
		while (true) {
			
			GlModJournalLineDetails glModJournalLineDetails = new GlModJournalLineDetails();
			
			//begin separator
			start = nextIndex + 1;
			nextIndex = strJournal.indexOf(separator, start);
			length = nextIndex - start;
			
			//Debit
			start = nextIndex + 1;
			nextIndex = strJournal.indexOf(separator, start);
			length = nextIndex - start;
			glModJournalLineDetails.setJlDebit((byte)Short.parseShort(strJournal.substring(start, start + length)));
			
			//Amount
			start = nextIndex + 1;
			nextIndex = strJournal.indexOf(separator, start);
			length = nextIndex - start;	
			glModJournalLineDetails.setJlAmount(Double.parseDouble(strJournal.substring(start, start + length)));
			
			//Description
			start = nextIndex + 1;
			nextIndex = strJournal.indexOf(separator, start);
			length = nextIndex - start;	
			glModJournalLineDetails.setJlDescription(strJournal.substring(start, start + length));
			
			// GL Chart Of Account
			start = nextIndex + 1;
			nextIndex = strJournal.indexOf(separator, start);
			length = nextIndex - start;	
			glModJournalLineDetails.setJlCoaAccountNumber(strJournal.substring(start, start + length));
			
			glJlList.add(glModJournalLineDetails);
			
			//begin lineSeparator
			start = nextIndex + 1;
			nextIndex = strJournal.indexOf(lineSeparator, start);
			length = nextIndex - start;			
			
			
			//if(strJournal.substring(start, start + length).trim().length()<1) break;
			int tempStart = nextIndex + 1;			
			if (strJournal.indexOf(separator, tempStart) == -1) break;
		}
		
		glModJournalDetails.setJrJlList(glJlList);
		return glModJournalDetails;
		
	}
	
	public void ejbCreate() throws CreateException {       
		Debug.print("GlJournalSyncControllerBean ejbCreate");        
	}                
	
	public void ejbRemove() {}
	
	public void ejbActivate() {}
	public void ejbPassivate() {}
	
	public void setSessionContext(SessionContext ctx) {}
	
	
}

