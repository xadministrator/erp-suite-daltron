package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalNoSetOfBookFoundException;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlAccountingCalendarValueDetails;

/**
 * @ejb:bean name="GlOpenClosePeriodControllerEJB"
 *           display-name="Used for opening and closing of periods"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlOpenClosePeriodControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlOpenClosePeriodController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlOpenClosePeriodControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlOpenClosePeriodControllerBean extends AbstractSessionBean {
    
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getEditableYears(Integer AD_CMPNY) throws
      GlobalNoSetOfBookFoundException {

      Debug.print("GlOpenClosePeriodControllerBean getEditableYears");

      LocalGlSetOfBookHome glSetOfBookHome  = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
            
      ArrayList list = new ArrayList();

      
      
      // Initialize EJB Home
        
      try {

          glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
          glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
          
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }      
      
      try {
      	
      	  Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
      	  
      	  Iterator i = glSetOfBooks.iterator();
      	  
      	  while (i.hasNext()) {
      	  	
      	  	  LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();
      	  	  
      	  	  Collection glAccountingCalendarValues = glAccountingCalendarValueHome.findByAcCode(glSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);
      	  	  
      	  	  ArrayList glAcvList = new ArrayList(glAccountingCalendarValues);
      	  	  
      	  	  LocalGlAccountingCalendarValue glAccountingCalendarValue = 
      	  	      (LocalGlAccountingCalendarValue)glAcvList.get(glAcvList.size() - 1);
      	  	      
      	  	  GregorianCalendar gc = new GregorianCalendar();
      	  	  gc.setTime(glAccountingCalendarValue.getAcvDateTo());
      	  	      
      	  	  list.add(new Integer(gc.get(Calendar.YEAR)));
      	  	        	  	  
      	  }
      	  
      	  return list;
      	
      } catch (Exception ex) {
      	
      	  Debug.printStackTrace(ex);
      	  throw new EJBException(ex.getMessage());
      	
      }

   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlAcvByYear(int YR, Integer AD_CMPNY) {

      Debug.print("GlOpenClosePeriodControllerBean getGlAcvByYear");

      LocalGlSetOfBookHome glSetOfBookHome = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      

      ArrayList list = new ArrayList();      
      
      // Initialize EJB Home
        
      try {

          glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
          glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
                    
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }
      
      try {
      	
         LocalGlSetOfBook glSetOfBook = glSetOfBookHome.findByDate(
	    	  EJBCommon.getIntendedDate(YR), AD_CMPNY);
	    	  	     
	     Collection glAccountingCalendarValues = glAccountingCalendarValueHome.findByAcCode(glSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);
	     
	     Iterator i = glAccountingCalendarValues.iterator();
	     
	     while (i.hasNext()) {
	     	
	         LocalGlAccountingCalendarValue glAccountingCalendarValue = 
		        (LocalGlAccountingCalendarValue) i.next();
	
	         GlAccountingCalendarValueDetails details = new GlAccountingCalendarValueDetails(
			    glAccountingCalendarValue.getAcvCode(),
			    glAccountingCalendarValue.getAcvPeriodPrefix(),
			    glAccountingCalendarValue.getAcvQuarter(),
			    glAccountingCalendarValue.getAcvPeriodNumber(),
			    glAccountingCalendarValue.getAcvDateFrom(),
			    glAccountingCalendarValue.getAcvDateTo(),
			    glAccountingCalendarValue.getAcvStatus());
			    
	         list.add(details);

	     }
	     
	     return list;
	    	  
      } catch (Exception ex) {
      	
      	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
      }

   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlAcvStatus(GlAccountingCalendarValueDetails details, int YR, Integer AD_CMPNY) {

      Debug.print("GlOpenClosePeriodControllerBean updateGlAcvStatus");     
 
      LocalGlSetOfBookHome glSetOfBookHome = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome =  null;
      LocalAdPreferenceHome adPreferenceHome = null;
                              
      // Initialize EJB Home
        
      try {

          glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
          glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }
      
      try {
      	
      	LocalGlSetOfBook glSetOfBook = glSetOfBookHome.findByDate(EJBCommon.getIntendedDate(YR), AD_CMPNY);
      	
      	LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome.findByPrimaryKey(details.getAcvCode());
      	      	      	
	      	if (details.getAcvStatus() == 'F') {
	      						
				Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
	  	  
			    Iterator i = glSetOfBooks.iterator();
			  
			    while (i.hasNext()) {
			  	
			  	    LocalGlSetOfBook glRunningSetOfBook = (LocalGlSetOfBook)i.next();	  	    
			  	    
			  	    Collection glToChangeAccountingCalendarValues =  	    
			  	        glAccountingCalendarValueHome.findByAcCodeAndAcvStatus(glRunningSetOfBook.getGlAccountingCalendar().getAcCode(), 'N', AD_CMPNY);
			  	  
			  	    Iterator j = glToChangeAccountingCalendarValues.iterator();
			  	    
			  	    while (j.hasNext()) {
			  	    	
			  	    	LocalGlAccountingCalendarValue glToChangeAccountingCalendarValue = 
			  	    	    (LocalGlAccountingCalendarValue)j.next();
			  	    	    
			  	    	if (glRunningSetOfBook.equals(glSetOfBook) &&
			  	    	    glAccountingCalendarValue.getAcvPeriodNumber() < glToChangeAccountingCalendarValue.getAcvPeriodNumber()) {
			  	    		
			  	    		break;
			  	    		
			  	    	}
			  	    	    	  	  	    	    
			  	    	glToChangeAccountingCalendarValue.setAcvStatus('F');
			  	    	glToChangeAccountingCalendarValue.setAcvDateFutureEntered(EJBCommon.getGcCurrentDateWoTime().getTime());
			  	    	
			  	    				  	    	
			  	    }
			  	    
			  	    if (glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	        break;
			  	    		
			  	    }
			    	        	  	  
			    }     
				
			} else if (details.getAcvStatus() == 'O' && glAccountingCalendarValue.getAcvDateClosed() == null) {
							
				Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
	  	  
			    Iterator i = glSetOfBooks.iterator();
			  
			    while (i.hasNext()) {
			  	
			  	    LocalGlSetOfBook glRunningSetOfBook = (LocalGlSetOfBook)i.next();	  	    
			  	    
			  	    Collection glToChangeAccountingCalendarValues =  	    
			  	        glAccountingCalendarValueHome.findByAcCodeAndAcvStatus(glRunningSetOfBook.getGlAccountingCalendar().getAcCode(), 'N', AD_CMPNY);
			  	  
			  	    Iterator j = glToChangeAccountingCalendarValues.iterator();
			  	    
			  	    while (j.hasNext()) {
			  	    	
			  	    	LocalGlAccountingCalendarValue glToChangeAccountingCalendarValue = 
			  	    	    (LocalGlAccountingCalendarValue)j.next();
			  	    	    
			  	    	if (glRunningSetOfBook.equals(glSetOfBook) &&
			  	    	    glAccountingCalendarValue.getAcvPeriodNumber() < glToChangeAccountingCalendarValue.getAcvPeriodNumber()) {
			  	    		
			  	    		break;
			  	    		
			  	    	}
			  	    	    	  	  	    	    
			  	    	glToChangeAccountingCalendarValue.setAcvStatus('O');
			  	    	glToChangeAccountingCalendarValue.setAcvDateOpened(EJBCommon.getGcCurrentDateWoTime().getTime());			  	    				  	    				  	    	
			  	    	
			  	    }
			  	    
			  	    glToChangeAccountingCalendarValues =  	    
			  	        glAccountingCalendarValueHome.findByAcCodeAndAcvStatus(glRunningSetOfBook.getGlAccountingCalendar().getAcCode(), 'F', AD_CMPNY);
			  	  
			  	    j = glToChangeAccountingCalendarValues.iterator();
			  	    
			  	    while (j.hasNext()) {
			  	    	
			  	    	LocalGlAccountingCalendarValue glToChangeAccountingCalendarValue = 
			  	    	    (LocalGlAccountingCalendarValue)j.next();
			  	    	    
			  	    	if (glRunningSetOfBook.equals(glSetOfBook) &&
			  	    	    glAccountingCalendarValue.getAcvPeriodNumber() < glToChangeAccountingCalendarValue.getAcvPeriodNumber()) {
			  	    		
			  	    		break;
			  	    		
			  	    	}
			  	    	    	  	  	    	    
			  	    	glToChangeAccountingCalendarValue.setAcvStatus('O');
			  	    	glToChangeAccountingCalendarValue.setAcvDateOpened(EJBCommon.getGcCurrentDateWoTime().getTime());
			  	    				  	    	
			  	    }
			  	    
			  	    if (glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	        break;
			  	    		
			  	    }
			  	    
			    }
			    
			} else if (details.getAcvStatus() == 'O' && glAccountingCalendarValue.getAcvDateClosed() != null) {
							
				glAccountingCalendarValue.setAcvStatus('O');
				glAccountingCalendarValue.setAcvDateOpened(EJBCommon.getGcCurrentDateWoTime().getTime());
		    	        	  	  
		    } else if (details.getAcvStatus() == 'C') {      
		    
		        Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
		  
			    Iterator i = glSetOfBooks.iterator();
			  
			    while (i.hasNext()) {
			  	
			  	    LocalGlSetOfBook glRunningSetOfBook = (LocalGlSetOfBook)i.next();	  	    
			  	    
			  	    Collection glToChangeAccountingCalendarValues =  	    
			  	        glAccountingCalendarValueHome.findByAcCodeAndAcvStatus(glRunningSetOfBook.getGlAccountingCalendar().getAcCode(), 'O', AD_CMPNY);
			  	  
			  	    Iterator j = glToChangeAccountingCalendarValues.iterator();
			  	    
			  	    while (j.hasNext()) {
			  	    	
			  	    	LocalGlAccountingCalendarValue glToChangeAccountingCalendarValue = 
			  	    	    (LocalGlAccountingCalendarValue)j.next();
			  	    	    
			  	    	if (glRunningSetOfBook.equals(glSetOfBook) &&
			  	    	    glAccountingCalendarValue.getAcvPeriodNumber() < glToChangeAccountingCalendarValue.getAcvPeriodNumber()) {
			  	    		
			  	    		break;
			  	    		
			  	    	}
			  	    	    	  	  	    	    
			  	    	glToChangeAccountingCalendarValue.setAcvStatus('C');
			  	    	glToChangeAccountingCalendarValue.setAcvDateClosed(EJBCommon.getGcCurrentDateWoTime().getTime());
			  	    	
			  	    				  	    	
			  	    }
			  	    
			  	    if (glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	        break;
			  	    		
			  	    }
			    	        	  	  
			    }     
		    
		    } else if (details.getAcvStatus() == 'P') {      
		    
		        Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
		  
			    Iterator i = glSetOfBooks.iterator();
			  
			    while (i.hasNext()) {
			  	
			  	    LocalGlSetOfBook glRunningSetOfBook = (LocalGlSetOfBook)i.next();	  	    
			  	    
			  	    Collection glToChangeAccountingCalendarValues =  	    
			  	        glAccountingCalendarValueHome.findByAcCodeAndAcvStatus(glRunningSetOfBook.getGlAccountingCalendar().getAcCode(), 'O', AD_CMPNY);
			  	  
			  	    Iterator j = glToChangeAccountingCalendarValues.iterator();
			  	    
			  	    while (j.hasNext()) {
			  	    	
			  	    	LocalGlAccountingCalendarValue glToChangeAccountingCalendarValue = 
			  	    	    (LocalGlAccountingCalendarValue)j.next();
			  	    	    
			  	    	if (glRunningSetOfBook.equals(glSetOfBook) &&
			  	    	    glAccountingCalendarValue.getAcvPeriodNumber() < glToChangeAccountingCalendarValue.getAcvPeriodNumber()) {
			  	    		
			  	    		break;
			  	    		
			  	    	}
			  	    	    	  	  	    	    
			  	    	glToChangeAccountingCalendarValue.setAcvStatus('P');
			  	    	glToChangeAccountingCalendarValue.setAcvDatePermanentlyClosed(EJBCommon.getGcCurrentDateWoTime().getTime());

			  	    	
			  	    }
			  	    
			  	    glToChangeAccountingCalendarValues =  	    
			  	        glAccountingCalendarValueHome.findByAcCodeAndAcvStatus(glRunningSetOfBook.getGlAccountingCalendar().getAcCode(), 'C', AD_CMPNY);
			  	  
			  	    j = glToChangeAccountingCalendarValues.iterator();
			  	    
			  	    while (j.hasNext()) {
			  	    	
			  	    	LocalGlAccountingCalendarValue glToChangeAccountingCalendarValue = 
			  	    	    (LocalGlAccountingCalendarValue)j.next();
			  	    	
			  	    	if (glRunningSetOfBook.equals(glSetOfBook) &&
			  	    	    glAccountingCalendarValue.getAcvPeriodNumber() < glToChangeAccountingCalendarValue.getAcvPeriodNumber()) {
			  	    		
			  	    		break;
			  	    		
			  	    	}
			  	    	    	  	  	    	    
			  	    	glToChangeAccountingCalendarValue.setAcvStatus('P');
			  	    	glToChangeAccountingCalendarValue.setAcvDatePermanentlyClosed(EJBCommon.getGcCurrentDateWoTime().getTime());
			  	    				  	    	
			  	    }
			  	    
			  	    if (glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	        break;
			  	    		
			  	    }
			    	        	  	  
			    }     
		    
		    }      		      			    	        	  	      		
      	      	
      } catch (Exception ex) {
      	
      	 Debug.printStackTrace(ex);
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
         
      }
            
   }
   
  /**
   * @ejb:interface-method view-type="remote"
   **/
   public boolean isGlAcvPriorPeriodStatusNeedConfirm(Integer ACV_CODE, int YR, Integer AD_CMPNY) {

      Debug.print("GlOpenClosePeriodControllerBean isGlAcvPriorPeriodStatusNeedConfirm");

      LocalGlSetOfBookHome glSetOfBookHome  = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
            
      ArrayList list = new ArrayList();

      
      
      // Initialize EJB Home
        
      try {

          glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
          glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
          
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }      
      
      try { 
      
            LocalGlSetOfBook glSetOfBook = glSetOfBookHome.findByDate(EJBCommon.getIntendedDate(YR), AD_CMPNY);
	      		      	
	      	LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome.findByPrimaryKey(ACV_CODE);
	      		      	
	      	      	      	
		  	if (glAccountingCalendarValue.getAcvStatus() == 'N') {
	  		
				
				Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
		  
			    Iterator i = glSetOfBooks.iterator();
			  
			    while (i.hasNext()) {
			  	
			  	    LocalGlSetOfBook glRunningSetOfBook = (LocalGlSetOfBook)i.next();	  	    
			  	    
			  	    Collection glToChangeAccountingCalendarValues =  	    
			  	        glAccountingCalendarValueHome.findByAcCodeAndAcvStatus(glRunningSetOfBook.getGlAccountingCalendar().getAcCode(), 'N', AD_CMPNY);
			  	  
			  	    Iterator j = glToChangeAccountingCalendarValues.iterator();
			  	    
			  	    while (j.hasNext()) {
			  	    	
			  	    	LocalGlAccountingCalendarValue glToChangeAccountingCalendarValue = 
			  	    	    (LocalGlAccountingCalendarValue)j.next();
			  	    	    
			  	    	if (!glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	    		return true;
			  	    	
			  	    	} else {
			  	    		
			  	    		if (glAccountingCalendarValue.getAcvPeriodNumber() > glToChangeAccountingCalendarValue.getAcvPeriodNumber()) {
			  	    			
			  	    			return true;
			  	    			
			  	    		}
			  	    		
			  	    	}			  	    	
			  	    				  	    				  	    	
			  	    }
			  	    
			  	    if (glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	        break;
			  	    		
			  	    }
			    	        	  	  
			    }    
			    
			} else if (glAccountingCalendarValue.getAcvStatus() == 'F') {
	  		
				
				Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
		  
			    Iterator i = glSetOfBooks.iterator();
			  
			    while (i.hasNext()) {
			  	
			  	    LocalGlSetOfBook glRunningSetOfBook = (LocalGlSetOfBook)i.next();	  	    
			  	    
			  	    Collection glToChangeAccountingCalendarValues =  	    
			  	        glAccountingCalendarValueHome.findByAcCodeAndAcvStatus(glRunningSetOfBook.getGlAccountingCalendar().getAcCode(), 'F', AD_CMPNY);
			  	  
			  	    Iterator j = glToChangeAccountingCalendarValues.iterator();
			  	    
			  	    while (j.hasNext()) {
			  	    	
			  	    	LocalGlAccountingCalendarValue glToChangeAccountingCalendarValue = 
			  	    	    (LocalGlAccountingCalendarValue)j.next();
			  	    	    
			  	    	if (!glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	    		return true;
			  	    	
			  	    	} else {
			  	    		
			  	    		if (glAccountingCalendarValue.getAcvPeriodNumber() > glToChangeAccountingCalendarValue.getAcvPeriodNumber()) {
			  	    			
			  	    			return true;
			  	    			
			  	    		}
			  	    		
			  	    	}				  	    	
			  	    				  	    				  	    	
			  	    }
			  	    
			  	    if (glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	        break;
			  	    		
			  	    }
			    	        	  	  
			    }     
						    	        	  	  
		    } else if (glAccountingCalendarValue.getAcvStatus() == 'O') {      
		    
		        Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
		  
			    Iterator i = glSetOfBooks.iterator();
			  
			    while (i.hasNext()) {
			  	
			  	    LocalGlSetOfBook glRunningSetOfBook = (LocalGlSetOfBook)i.next();	  	    
			  	    
			  	    Collection glToChangeAccountingCalendarValues =  	    
			  	        glAccountingCalendarValueHome.findByAcCodeAndAcvStatus(glRunningSetOfBook.getGlAccountingCalendar().getAcCode(), 'O', AD_CMPNY);
			  	  
			  	    Iterator j = glToChangeAccountingCalendarValues.iterator();
			  	    
			  	    while (j.hasNext()) {
			  	    	
			  	    	LocalGlAccountingCalendarValue glToChangeAccountingCalendarValue = 
			  	    	    (LocalGlAccountingCalendarValue)j.next();
			  	    	    
			  	    	if (!glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	    		return true;
			  	    	
			  	    	} else {
			  	    		
			  	    		if (glAccountingCalendarValue.getAcvPeriodNumber() > glToChangeAccountingCalendarValue.getAcvPeriodNumber()) {
			  	    			
			  	    			return true;
			  	    			
			  	    		}
			  	    		
			  	    	}			  	    	    	  	  	    	    			  	    	  	    	
			  	    				  	    	
			  	    }
			  	    
			  	    if (glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	        break;
			  	    		
			  	    }
			    	        	  	  
			    }     
		    
		    } else if (glAccountingCalendarValue.getAcvStatus() == 'C') {      
		    
		        Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
		  
			    Iterator i = glSetOfBooks.iterator();
			  
			    while (i.hasNext()) {
			  	
			  	    LocalGlSetOfBook glRunningSetOfBook = (LocalGlSetOfBook)i.next();	  	    
			  	    
			  	    Collection glToChangeAccountingCalendarValues =  	    
			  	        glAccountingCalendarValueHome.findByAcCodeAndAcvStatus(glRunningSetOfBook.getGlAccountingCalendar().getAcCode(), 'C', AD_CMPNY);
			  	  
			  	    Iterator j = glToChangeAccountingCalendarValues.iterator();
			  	    
			  	    while (j.hasNext()) {
			  	    	
			  	    	LocalGlAccountingCalendarValue glToChangeAccountingCalendarValue = 
			  	    	    (LocalGlAccountingCalendarValue)j.next();
			  	    	    
			  	    	if (!glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	    		return true;
			  	    	
			  	    	} else {
			  	    		
			  	    		if (glAccountingCalendarValue.getAcvPeriodNumber() > glToChangeAccountingCalendarValue.getAcvPeriodNumber()) {
			  	    			
			  	    			return true;
			  	    			
			  	    		}
			  	    		
			  	    	}			  	    	   	  	  	    	    
			  	    				  	    	
			  	    }
			  	    
			  	    if (glRunningSetOfBook.equals(glSetOfBook)) {
			  	    		
			  	        break;
			  	    		
			  	    }			  	    
			  	    			    	        	  	  
			    }     
		    
		    }
		    
		    return false;      		      			    	        	  	      		
      	      	
      } catch (Exception ex) {
      	
      	 Debug.printStackTrace(ex);
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
         
      }

   }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlOpenClosePeriodControllerBean ejbCreate");
       
   }

   // private methods
   
}   


