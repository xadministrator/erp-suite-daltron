package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlARAccountNumberOfSegmentInvalidException;
import com.ejb.exception.GlARAccountRangeAlreadyDeletedException;
import com.ejb.exception.GlARAccountRangeInvalidException;
import com.ejb.exception.GlARAccountRangeNoAccountFoundException;
import com.ejb.exception.GlARAccountRangeOverlappedException;
import com.ejb.exception.GlARNoAccountRangeFoundException;
import com.ejb.exception.GlARResponsibilityNotAllowedException;
import com.ejb.exception.GlORGNoOrganizationFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.gl.LocalGlAccountRange;
import com.ejb.gl.LocalGlAccountRangeHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlOrganization;
import com.ejb.gl.LocalGlOrganizationHome;
import com.ejb.gl.LocalGlResponsibility;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlAccountRangeDetails;
import com.util.GlOrganizationDetails;

/**
 * @ejb:bean name="GlOrganizationAccountAssignmentControllerEJB"
 *           display-name="Used for assignment of account numbers to organizations"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlOrganizationAccountAssignmentControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlOrganizationAccountAssignmentController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlOrganizationAccountAssignmentControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlOrganizationAccountAssignmentControllerBean extends AbstractSessionBean {
 
   
   /*******************************************************************
   
 	Business methods:

	(1) getGlOrgAll - returns an Arraylist of all ORG

	(2) getGlOrgDescriptionByGlOrgName - returns GlOrganizationDetails

	(3) getGlArByGlOrgName - returns an ArrayList of all AR

	(2) addGlArEntry - validate input and duplicate entries
	                   before adding the AR

	(3) updateGlArEntry - validate input and duplicate entries
	                       before updating the AR

	(4) deleteGlArEntry - deletes Ar

   *******************************************************************/
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlOrgAll(Integer AD_CMPNY)
      throws GlORGNoOrganizationFoundException { 

      Debug.print("GlOrganizationControllerBean getGlOrgAll");

      /***************************************************************
         Gets all ORG
      ***************************************************************/

      ArrayList orgAllList = new ArrayList();
      Collection glOrganizations;

      LocalGlOrganizationHome glOrganizationHome = null;
      
      // Initialize EJB Home
        
      try {

          glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }      
      
      try {
         glOrganizations = glOrganizationHome.findOrgAll(AD_CMPNY);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glOrganizations.size() == 0)
         throw new GlORGNoOrganizationFoundException();
 
      Iterator i = glOrganizations.iterator();

      while (i.hasNext()) {
         LocalGlOrganization glOrganization = (LocalGlOrganization) i.next();
	 
	 GlOrganizationDetails details = new GlOrganizationDetails(glOrganization.getOrgCode(),
	    glOrganization.getOrgName(), glOrganization.getOrgDescription(), glOrganization.getOrgMasterCode());
	 orgAllList.add(details);
      }

      return orgAllList;
		     
   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public GlOrganizationDetails getGlOrgDescriptionByGlOrgName(String ORG_NM, Integer AD_CMPNY) 
      throws GlORGNoOrganizationFoundException {

      Debug.print("GlOrganizationAccountAssignmentControllerBean getGlOrgDescriptionByGlOrgName");

      /***************************************************************
         Returns the related description
      ***************************************************************/

      LocalGlOrganization glOrganization = null;

      LocalGlOrganizationHome glOrganizationHome = null;
      
      // Initialize EJB Home
        
      try {

          glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }            
      
      try {
         glOrganization = glOrganizationHome.findByOrgName(ORG_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new GlORGNoOrganizationFoundException();
      } catch (EJBException ex) {
         throw new EJBException(ex.getMessage());
      }

      return new GlOrganizationDetails(glOrganization.getOrgName(), 
         glOrganization.getOrgDescription(), glOrganization.getOrgMasterCode());
   }  

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlArByGlOrgName(String ORG_NM, Integer AD_CMPNY)
      throws GlORGNoOrganizationFoundException,
      GlARNoAccountRangeFoundException {

      Debug.print("GlOrganizationAccountAssignmentControllerBean getGlArByGlOrgName");

      /***************************************************************
         Returns the related account ranges
      ***************************************************************/

      LocalGlOrganization glOrganization = null;

      ArrayList arAllList = new ArrayList();
      Collection glAccountRanges = null; 

      LocalGlOrganizationHome glOrganizationHome = null;
      
      // Initialize EJB Home
        
      try {

          glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }            
      
      try {
         glOrganization = glOrganizationHome.findByOrgName(ORG_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new GlORGNoOrganizationFoundException();
      } catch (EJBException ex) {
         throw new EJBException(ex.getMessage());
      }

      try {
         glAccountRanges = glOrganization.getGlAccountRanges();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glAccountRanges.size() == 0)
         throw new GlARNoAccountRangeFoundException(); 

      Iterator i = glAccountRanges.iterator();

      while(i.hasNext()) {
         LocalGlAccountRange glAccountRange = (LocalGlAccountRange)i.next();
         
	 GlAccountRangeDetails details = new GlAccountRangeDetails(
	    glAccountRange.getArCode(), glAccountRange.getArLine(),
	    glAccountRange.getArAccountLow(), glAccountRange.getArAccountHigh());

	 arAllList.add(details);
      }

      return arAllList;
		     
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void addGlArEntry(GlAccountRangeDetails details, String ORG_NM, Integer RES_CODE, Integer AD_CMPNY)
      throws GlORGNoOrganizationFoundException,
      GlARAccountRangeOverlappedException,
      GlARAccountNumberOfSegmentInvalidException,
      GlARAccountRangeNoAccountFoundException,
      GlARResponsibilityNotAllowedException,
      GlARAccountRangeInvalidException{

      Debug.print("GlOrganizationAccountAssignmentControllerBean addGlArEntry");

      /***************************************************************
         Adds an Account Range to Organization
      ***************************************************************/

      LocalGlOrganization glOrganization = null;
      LocalAdCompany adCompany = null;
      LocalGlChartOfAccount glChartOfAccount = null;
      LocalGlAccountRange glAccountRange = null;
      LocalGenField genField = null;
      LocalGlOrganization glMasterOrganization = null;
      Collection glResponsibilities = null;
      boolean responsibilityFound = false;

      LocalGlOrganizationHome glOrganizationHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      LocalGlChartOfAccountHome glChartOfAccountHome = null;
      LocalGlAccountRangeHome glAccountRangeHome = null;
      
      // Initialize EJB Home
        
      try {

          glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);          
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
          glAccountRangeHome = (LocalGlAccountRangeHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountRangeHome.JNDI_NAME, LocalGlAccountRangeHome.class);
          
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }            
      
      try {
         glOrganization = glOrganizationHome.findByOrgName(ORG_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new GlORGNoOrganizationFoundException();
      } catch (EJBException ex) {
         throw new EJBException(ex.getMessage());
      }
      
      /*** Get company's Generic Field **/
      try {
         adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         genField = adCompany.getGenField();
      } catch (FinderException ex) {
         throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
								  
      
      if (glOrganization.getOrgMasterCode() != null && glOrganization.getOrgMasterCode().intValue() != 0) {
          /*** Check if passed responsibility has permission to add/edit accounts **/
         try{
            glMasterOrganization = glOrganizationHome.findByPrimaryKey(glOrganization.getOrgMasterCode());
         } catch (FinderException ex){
            throw new EJBException(ex.getMessage());
         } catch (Exception ex) {
            throw new EJBException(ex.getMessage());
         }

         try {
	    glResponsibilities = glMasterOrganization.getGlResponsibilities();
	 } catch (Exception ex) {
	    throw new EJBException(ex.getMessage());
	 }

	 if (glResponsibilities != null) {
	    Iterator i = glResponsibilities.iterator();
	    while(i.hasNext()) {
	       if(RES_CODE.equals(((LocalGlResponsibility)i.next()).getResCode())) {
	          responsibilityFound = true;
		  break;
	       }
	    }
	    if(!responsibilityFound) 
	       throw new GlARResponsibilityNotAllowedException();
	 }

	 /*** Check if entered account range is within the master code's account range **/
	 if(!isEnteredAccountWithinMasterCodeRange(glMasterOrganization, details, genField, AD_CMPNY))
	    throw new GlARAccountRangeInvalidException();
      } 

      

      ArrayList coaWithinRangeList = getCoaWithinRange(details, glOrganization, genField, AD_CMPNY);

      if (coaWithinRangeList.size() == 0) {
         throw new GlARAccountRangeNoAccountFoundException();
      }

      try {
         glAccountRange = glAccountRangeHome.create(
	    details.getArLine(), details.getArAccountLow(), details.getArAccountHigh(), AD_CMPNY);

	 glOrganization.addGlAccountRange(glAccountRange);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlArEntry(GlAccountRangeDetails details, String ORG_NM, Integer RES_CODE, Integer AD_CMPNY)
      throws GlORGNoOrganizationFoundException,
      GlARAccountRangeOverlappedException,
      GlARAccountNumberOfSegmentInvalidException,
      GlARAccountRangeNoAccountFoundException,
      GlARAccountRangeAlreadyDeletedException,
      GlARResponsibilityNotAllowedException,
      GlARAccountRangeInvalidException {

       Debug.print("GlOrganizationAccountAssignmentControllerBean updateGlArEntry");

      /***************************************************************
         Updates an Account Range to Organization
      ***************************************************************/

      LocalGlOrganization glOrganization = null;
      LocalAdCompany adCompany = null;
      LocalGlChartOfAccount glChartOfAccount = null;
      LocalGlAccountRange glAccountRange = null;
      LocalGenField genField = null;
      Collection glAccountRanges = null;
      boolean isAccountRangeChanged = false;
      LocalGlOrganization glMasterOrganization = null;
      Collection glResponsibilities = null;
      boolean responsibilityFound = false;
								
      LocalGlOrganizationHome glOrganizationHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      LocalGlChartOfAccountHome glChartOfAccountHome = null;
      LocalGlAccountRangeHome glAccountRangeHome = null;
      
      // Initialize EJB Home
        
      try {

          glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);          
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
          glAccountRangeHome = (LocalGlAccountRangeHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountRangeHome.JNDI_NAME, LocalGlAccountRangeHome.class);
          
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }            
      
      try {
         glOrganization = glOrganizationHome.findByOrgName(ORG_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new GlORGNoOrganizationFoundException();
      } catch (EJBException ex) {
         throw new EJBException(ex.getMessage());
      }

     
       /*** Check if Account Range if changed **/
       try {
          glAccountRange = glAccountRangeHome.findByPrimaryKey(
             details.getArCode());
       } catch (FinderException ex) {
          throw new GlARAccountRangeAlreadyDeletedException();
       } catch (Exception ex) {
          throw new EJBException(ex.getMessage());
       }

       if (glAccountRange.getArAccountLow().equals(details.getArAccountLow()) && 
           glAccountRange.getArAccountHigh().equals(details.getArAccountHigh())) {
	   
           isAccountRangeChanged = false;
       } else {
           isAccountRangeChanged = true;
       }
	    

      if (isAccountRangeChanged) {

          /*** Get company's Generic Field **/
          try {
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             genField = adCompany.getGenField();
          } catch (FinderException ex) {
             getSessionContext().setRollbackOnly();
             throw new EJBException(ex.getMessage());
          } catch (Exception ex) {
             getSessionContext().setRollbackOnly();
             throw new EJBException(ex.getMessage());
          }
	  if (glOrganization.getOrgMasterCode() != null && glOrganization.getOrgMasterCode().intValue() != 0) {
             /*** Check if passed responsibility has permission to add/edit accounts **/
             try{
                glMasterOrganization = glOrganizationHome.findByPrimaryKey(glOrganization.getOrgMasterCode());
             } catch (FinderException ex){
	       throw new EJBException(ex.getMessage());
             } catch (Exception ex) {
               throw new EJBException(ex.getMessage());
             }
	     
             try {
	        glResponsibilities = glMasterOrganization.getGlResponsibilities();
	     } catch (Exception ex) {
	        throw new EJBException(ex.getMessage());
	     }
	     
             if (glResponsibilities != null) {
	        Iterator i = glResponsibilities.iterator();
	        while(i.hasNext()) {
	           if(RES_CODE.equals(((LocalGlResponsibility)i.next()).getResCode())) {
	              responsibilityFound = true;
	              break;
	           }
	        }
	        if(!responsibilityFound)
	           throw new GlARResponsibilityNotAllowedException();
	     }
	     
             /*** Check if entered account range is within the master code's account range **/
	     if(!isEnteredAccountWithinMasterCodeRange(glMasterOrganization, details, genField, AD_CMPNY))
	        throw new GlARAccountRangeInvalidException();
          }

          
          /*** Get Coa within the range **/
          ArrayList coaWithinRangeList = getCoaWithinRange(details, glOrganization, genField, AD_CMPNY);

         if (coaWithinRangeList.size() == 0) {
            getSessionContext().setRollbackOnly();
            throw new GlARAccountRangeNoAccountFoundException();
         }
	 
        /*** Updates the selected account range **/
        try {
	   glAccountRange.setArAccountLow(details.getArAccountLow());
    	   glAccountRange.setArAccountHigh(details.getArAccountHigh());
        } catch (Exception ex) {
           getSessionContext().setRollbackOnly();
	   throw new EJBException(ex.getMessage());
        }
     }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteGlArEntry(Integer AR_CODE,Integer RES_CODE, Integer AD_CMPNY)
      throws GlARAccountRangeAlreadyDeletedException,
      GlARResponsibilityNotAllowedException {
      
      Debug.print("GlOrganizationAccountAssignmentControllerBean deleteGlArEntry");

      /***************************************************************
         Updates an Account Range to Organization
      ***************************************************************/
      
      LocalGlOrganization glOrganization = null;
      LocalGlAccountRange glAccountRange = null;
      LocalGlChartOfAccount glChartOfAccount = null;
      LocalAdCompany adCompany = null;
      LocalGenField genField = null;
      LocalGlOrganization glMasterOrganization = null;
      Collection glResponsibilities = null;
      boolean responsibilityFound = false;
      
      LocalGlOrganizationHome glOrganizationHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      LocalGlAccountRangeHome glAccountRangeHome = null;
      
      // Initialize EJB Home
        
      try {

          glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);          
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          glAccountRangeHome = (LocalGlAccountRangeHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountRangeHome.JNDI_NAME, LocalGlAccountRangeHome.class);
          
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                  

      try {
         glAccountRange = glAccountRangeHome.findByPrimaryKey(AR_CODE);
      } catch (FinderException ex) {
         throw new GlARAccountRangeAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      try {
         glOrganization = glAccountRange.getGlOrganization();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
								  

      /*** Get company's Generic Field **/
      try {
         adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         genField = adCompany.getGenField();
      } catch (FinderException ex) {
         throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glOrganization.getOrgMasterCode() != null) {
        /*** Check if passed responsibility has permission to add/edit accounts **/
        try{
           glMasterOrganization = glOrganizationHome.findByPrimaryKey(glOrganization.getOrgMasterCode());
        } catch (FinderException ex){
           throw new EJBException(ex.getMessage());
        } catch (Exception ex) {
           throw new EJBException(ex.getMessage());
        }
	
        try {
	   glResponsibilities = glMasterOrganization.getGlResponsibilities();
        } catch (Exception ex) {
           throw new EJBException(ex.getMessage());
        }
	
        if (glResponsibilities != null) {
	   Iterator i = glResponsibilities.iterator();
	   while(i.hasNext()) {
	      if(RES_CODE.equals(((LocalGlResponsibility)i.next()).getResCode())) {
	         responsibilityFound = true;
	         break;
	      }
	   }
 
         if(!responsibilityFound)
	    throw new GlARResponsibilityNotAllowedException();
        } 
    }
		
	 /*** Removes selected account range **/
         try {
            glAccountRange.remove();
         } catch (RemoveException ex) {
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
         } catch (Exception ex) {
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
         }
   }
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlOrganizationAccountAssignmentControllerBean ejbCreate");
      
   }

   private boolean isEnteredAccountWithinMasterCodeRange(LocalGlOrganization glMasterOrganization, 
      GlAccountRangeDetails details, LocalGenField genField, Integer AD_CMPNY) {

      Debug.print("GlOrganizationAccountAssignmentBean isEnteredAccountWithinMasterCodeRange");

      Collection glAccountRanges = null;
      LocalGlAccountRange glAccountRange = null;
      String strSeparator = null;
      short genNumberOfSegment = 0;
      boolean isWithinRange = false;

      try {
         char chrSeparator = genField.getFlSegmentSeparator();
	 genNumberOfSegment = genField.getFlNumberOfSegment();
	 strSeparator = String.valueOf(chrSeparator);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }
      
      try {
         glAccountRanges = glMasterOrganization.getGlAccountRanges();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glAccountRanges != null) {
         Iterator i = glAccountRanges.iterator();
	 while(i.hasNext()) {
	    glAccountRange = (LocalGlAccountRange)i.next();
            StringTokenizer stMasterAccountLow = new StringTokenizer(glAccountRange.getArAccountLow(), strSeparator);
            StringTokenizer stMasterAccountHigh = new StringTokenizer(glAccountRange.getArAccountHigh(), strSeparator);
            StringTokenizer stAccountLow = new StringTokenizer(details.getArAccountLow(), strSeparator);
            StringTokenizer stAccountHigh = new StringTokenizer(details.getArAccountHigh(), strSeparator);
            String[] masterAccountLowSegmentValue = new String[genNumberOfSegment];
            String[] masterAccountHighSegmentValue = new String[genNumberOfSegment];
            String[] accountLowSegmentValue = new String[genNumberOfSegment];
            String[] accountHighSegmentValue = new String[genNumberOfSegment];

            int j = 0;
            while (stAccountLow.hasMoreTokens()) {
	       if (j == genNumberOfSegment) {
	          return true;
	       }
	       try {
	          accountLowSegmentValue[j] = stAccountLow.nextToken();
	       } catch (Exception ex) {
	          return true;
	       }
               j++;
            }
	    if (j < genNumberOfSegment) {
	       return true;
	    }
            j = 0;
            while (stAccountHigh.hasMoreTokens()) {
	       if (j == genNumberOfSegment) {
	          return true;
	       }
	       try {
	          accountHighSegmentValue[j] =  stAccountHigh.nextToken();
	       } catch (Exception ex) {
	          return true;
               }						
               j++;
            }

	    if (j < genNumberOfSegment) {
               return true;
            }
	    j = 0;
            while(stMasterAccountLow.hasMoreTokens()){
                  masterAccountLowSegmentValue[j] = stMasterAccountLow.nextToken();
                  masterAccountHighSegmentValue[j]= stMasterAccountHigh.nextToken();
		  j++;
            }
      
            for (int k=0; k<genNumberOfSegment; k++) {
                if((accountLowSegmentValue[k].compareTo(masterAccountLowSegmentValue[k]) >= 0 &&
                    accountLowSegmentValue[k].compareTo(masterAccountHighSegmentValue[k]) <= 0) ||
                    (accountHighSegmentValue[k].compareTo(masterAccountLowSegmentValue[k]) <= 0 &&
                    accountHighSegmentValue[k].compareTo(masterAccountHighSegmentValue[k]) >= 0)) {
	       
	            isWithinRange = true;
	        } else {
								    
	           isWithinRange = false;
	           break;
	        }
            }
	    if (isWithinRange) {
	       return true;
	    }
         }
      }
      return isWithinRange;

      
   }

    // private methods
   private boolean isRegisteredAccountWithinRange(LocalGlAccountRange glAccountRange, LocalGlChartOfAccount glChartOfAccount,
      LocalGenField genField, Integer AD_CMPNY) {

      Debug.print("GlOrganizationAccountAssignmentControllerBean isRegisteredAccountWithinRange");

      /*** Get Generic Field's Segment Separator and Number Of Segments**/

      String strSeparator = null;
      short genNumberOfSegment = 0;

      try {
         char chrSeparator = genField.getFlSegmentSeparator();
         genNumberOfSegment = genField.getFlNumberOfSegment();
         strSeparator = String.valueOf(chrSeparator);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

      StringTokenizer stCoa = new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), strSeparator);
      StringTokenizer stArAccountLow = new StringTokenizer(glAccountRange.getArAccountLow(), strSeparator);
      StringTokenizer stArAccountHigh = new StringTokenizer(glAccountRange.getArAccountHigh(), strSeparator);

      String[] coaSegmentValue = new String[genNumberOfSegment];
      String[] arAccountLowSegmentValue = new String[genNumberOfSegment];
      String[]  arAccountHighSegmentValue = new String[genNumberOfSegment];

      int j = 0;

      while(stCoa.hasMoreTokens()){
         coaSegmentValue[j] = stCoa.nextToken();
         arAccountLowSegmentValue[j]= stArAccountLow.nextToken();
         arAccountHighSegmentValue[j] = stArAccountHigh.nextToken();
         j++;
      }
      
      boolean isOverlapped = false;

      for (int k=0; k<genNumberOfSegment; k++) {
         if(coaSegmentValue[k].compareTo(arAccountLowSegmentValue[k]) >= 0 &&
            coaSegmentValue[k].compareTo(arAccountHighSegmentValue[k]) <= 0) {
            isOverlapped = true;
         } else {
            isOverlapped = false;
            break;
         }
      }

      if (isOverlapped) {
         return true;
      }
      return false;
      
   }

   private ArrayList getCoaWithinRange(GlAccountRangeDetails details, LocalGlOrganization glOrganization,
      LocalGenField genField, Integer AD_CMPNY) 
      throws GlARAccountNumberOfSegmentInvalidException,
      GlARAccountRangeOverlappedException, GlARAccountRangeInvalidException {

      Debug.print("GlOrganizationAccountAssignmentControllerBean getCoaWithinRange");
        
      /*** Get Generic Field's Segment Separator and Number Of Segments**/
       
      Collection glAccountRanges = null;
      ArrayList coaWithinRangeList = new ArrayList();
      
      LocalGlChartOfAccountHome glChartOfAccountHome = null;
      
      // Initialize EJB Home
        
      try {

          glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }            
       
      String strSeparator = null;
      short numberOfSegment = 0;
      short genNumberOfSegment = 0;
       
      try {
         char chrSeparator = genField.getFlSegmentSeparator();
         genNumberOfSegment = genField.getFlNumberOfSegment();
         strSeparator = String.valueOf(chrSeparator);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

      /*** Check if number of segment of the passed account low and high is valid **/
       
      StringTokenizer st = new StringTokenizer(details.getArAccountLow(), strSeparator);
      String[] newAccountLowSegmentValue = new String[genNumberOfSegment];
      String[] newAccountHighSegmentValue = new String[genNumberOfSegment];
      String[] accountLowSegmentValue = new String[genNumberOfSegment];
      String[] accountHighSegmentValue = new String[genNumberOfSegment];
       
       while(st.hasMoreTokens()) {
	  if (numberOfSegment == genNumberOfSegment) {
	     throw new GlARAccountNumberOfSegmentInvalidException();
	  } else {
	     try {
	        newAccountLowSegmentValue[numberOfSegment] = st.nextToken();
             } catch (Exception ex) {
	        getSessionContext().setRollbackOnly();
	        throw new GlARAccountRangeInvalidException();
	     }
	  }
          numberOfSegment = (short)(numberOfSegment + 1); 
       }

       if (numberOfSegment < genNumberOfSegment) {
          getSessionContext().setRollbackOnly();
          throw new GlARAccountNumberOfSegmentInvalidException();
       }
       
       st = new StringTokenizer(details.getArAccountHigh(), strSeparator);
       
       numberOfSegment = 0;
       
       while(st.hasMoreTokens()) {
          if (numberOfSegment == genNumberOfSegment) {
          	 getSessionContext().setRollbackOnly();
             throw new GlARAccountNumberOfSegmentInvalidException();
          } else {
	     try {
                newAccountHighSegmentValue[numberOfSegment] = st.nextToken();
             } catch (Exception ex) {
                getSessionContext().setRollbackOnly();
                throw new GlARAccountRangeInvalidException();
             }
          }
          numberOfSegment = (short)(numberOfSegment + 1);
       }

       if (numberOfSegment < genNumberOfSegment) {
          getSessionContext().setRollbackOnly();
          throw new GlARAccountNumberOfSegmentInvalidException();
       }

       /*** Check if passed account range overlaps an existing account range **/
       
       try {
          glAccountRanges = glOrganization.getGlAccountRanges();
       } catch (Exception ex) {
          getSessionContext().setRollbackOnly();
          throw new EJBException(ex.getMessage());
       }

       if (!glAccountRanges.isEmpty()) {
          Iterator i = glAccountRanges.iterator();

		  while(i.hasNext()) {
		     LocalGlAccountRange glAccountRange = (LocalGlAccountRange)i.next();  
		     
		     if ((details.getArCode() != null && (!details.getArCode().equals(glAccountRange.getArCode()))) || 
		        details.getArCode() == null) {
	                StringTokenizer stAccountLow = new StringTokenizer(glAccountRange.getArAccountLow(), strSeparator);
		        StringTokenizer stAccountHigh = new StringTokenizer(glAccountRange.getArAccountHigh(), strSeparator);
		     
		        int j = 0;
		     
		        while(stAccountLow.hasMoreTokens()){
		           accountLowSegmentValue[j] = stAccountLow.nextToken();
			   j++;
		        }
		     
		        j = 0;
		        while(stAccountHigh.hasMoreTokens()){
		           accountHighSegmentValue[j] = stAccountHigh.nextToken();
		   	   j++;
		        }
	
		        boolean isOverlapped = false;
	
		        for (int k=0; k<genNumberOfSegment; k++) {
		           if(((newAccountLowSegmentValue[k].compareTo(accountLowSegmentValue[k]) >= 0  && 
				      newAccountLowSegmentValue[k].compareTo(accountHighSegmentValue[k]) <= 0) ||		   
				      (newAccountHighSegmentValue[k].compareTo(accountLowSegmentValue[k]) <= 0 &&
				      newAccountHighSegmentValue[k].compareTo(accountHighSegmentValue[k]) >= 0)) ||
				   
				      ((accountLowSegmentValue[k].compareTo(newAccountLowSegmentValue[k]) >= 0 &&
		                       accountLowSegmentValue[k].compareTo(newAccountHighSegmentValue[k]) <= 0) ||
		                      (accountHighSegmentValue[k].compareTo(newAccountLowSegmentValue[k]) <= 0 &&
		                      accountHighSegmentValue[k].compareTo(newAccountHighSegmentValue[k]) >= 0))) {
		
				      isOverlapped = true;
			   	   } else {
		
				      isOverlapped = false;
				      break;
				   }
			
		        } 
	
		        if (isOverlapped) {
		           getSessionContext().setRollbackOnly();
		           throw new GlARAccountRangeOverlappedException();
	            }
		     }
	  	}
       }

       Collection glChartOfAccounts = null;
       try {
          glChartOfAccounts = glChartOfAccountHome.findCoaAllEnabled(
             new Date(EJBCommon.getGcCurrentDateWoTime().getTime().getTime()), AD_CMPNY);
       } catch (FinderException ex) {
       } catch (EJBException ex) {
          getSessionContext().setRollbackOnly();
          throw new EJBException(ex.getMessage());
       }

       if (glChartOfAccounts != null) {
          Iterator i = glChartOfAccounts.iterator();

          while (i.hasNext()) {
             LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)i.next();
	     StringTokenizer stCoa = new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), strSeparator);
          
	     String[] coaSegmentValue = new String[genNumberOfSegment];
	  
             int j = 0;
	  
             while(stCoa.hasMoreTokens()){
	       coaSegmentValue[j] = stCoa.nextToken();
	        j++;
	     }

	     boolean isOverlapped = false;

	     for (int k=0; k<genNumberOfSegment; k++) {
                if(coaSegmentValue[k].compareTo(newAccountLowSegmentValue[k]) >= 0 &&
                   coaSegmentValue[k].compareTo(newAccountHighSegmentValue[k]) <= 0) {
		  		   isOverlapped = true;
                } else {
                   isOverlapped = false;
                   break;
                }
            }

	     if (isOverlapped) {
	        coaWithinRangeList.add(glChartOfAccount.getCoaCode());
	     }
          }
       }

       return coaWithinRangeList;
						     
    }

}
