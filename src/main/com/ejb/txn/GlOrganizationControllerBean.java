package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.GlORGNoOrganizationCodeFoundException;
import com.ejb.exception.GlORGNoOrganizationFoundException;
import com.ejb.exception.GlORGOrganizationAlreadyAssignedException;
import com.ejb.exception.GlORGOrganizationAlreadyDeletedException;
import com.ejb.exception.GlORGOrganizationAlreadyExistException;
import com.ejb.gl.LocalGlOrganization;
import com.ejb.gl.LocalGlOrganizationHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlOrganizationDetails;

/**
 * @ejb:bean name="GlOrganizationControllerEJB"
 *           display-name="Used for maintenance of organizational structure"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlOrganizationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlOrganizationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlOrganizationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlOrganizationControllerBean extends AbstractSessionBean {


   /*******************************************************************
   
   	Business methods:

	(1) getGlOrgAll - returns an Arraylist of all ORG

	(2) addGlOrgEntry - validate input and duplicate entries
	                   before adding the ORG

	(3) updateGlOrgEntry - validate input and duplicate entries
	                       before updating the ORG

	(4) deleteGlOrgEntry - deletes ORG

   *******************************************************************/
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlOrgAll(Integer AD_CMPNY)
      throws GlORGNoOrganizationFoundException { 

      Debug.print("GlOrganizationControllerBean getGlOrgAll");

      /***************************************************************
         Gets all ORG
      ***************************************************************/

      ArrayList orgAllList = new ArrayList();
      Collection glOrganizations;

      LocalGlOrganizationHome glOrganizationHome = null;
      
      // Initialize EJB Home
        
      try {

          glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }            
      
      try {
         glOrganizations = glOrganizationHome.findOrgAll(AD_CMPNY);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glOrganizations.size() == 0)
         throw new GlORGNoOrganizationFoundException();
 
      Iterator i = glOrganizations.iterator();

      while (i.hasNext()) {
         LocalGlOrganization glOrganization = (LocalGlOrganization) i.next();
	 
	 GlOrganizationDetails details = new GlOrganizationDetails(
	    glOrganization.getOrgCode(), glOrganization.getOrgName(),
	    glOrganization.getOrgDescription(), glOrganization.getOrgMasterCode());
	    orgAllList.add(details);
      }

      return orgAllList;
		     
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void addGlOrgEntry(GlOrganizationDetails details, Integer AD_CMPNY)
      throws GlORGNoOrganizationCodeFoundException,
      GlORGOrganizationAlreadyExistException {

      Debug.print("GlOrganizationControllerBean addGlOrgEntry");

      /***************************************************************
         Adds an ORG
      ***************************************************************/

      LocalGlOrganization glOrganization = null;

      LocalGlOrganizationHome glOrganizationHome = null;
      
      // Initialize EJB Home
        
      try {

          glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }            
      
      if(details.getOrgMasterCode() != null){
         try {
            glOrganization = glOrganizationHome.findByPrimaryKey(details.getOrgMasterCode());
         } catch (FinderException ex) {
            throw new GlORGNoOrganizationCodeFoundException();
         } catch (Exception ex) {
            throw new EJBException(ex.getMessage());
         } 
      }
      
      try {
      	
         glOrganizationHome.findByOrgName(details.getOrgName(), AD_CMPNY);
         
         getSessionContext().setRollbackOnly();
         throw new GlORGOrganizationAlreadyExistException();
      	
      } catch (FinderException ex) {
      	
      	      	
      }

      try {
         glOrganization = glOrganizationHome.create(
	    details.getOrgName(), details.getOrgDescription(),
	    details.getOrgMasterCode(), AD_CMPNY);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage()); 
      }

   }
   
   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlOrgEntry(GlOrganizationDetails details, Integer AD_CMPNY)
      throws GlORGNoOrganizationCodeFoundException,
      GlORGOrganizationAlreadyExistException,
      GlORGOrganizationAlreadyAssignedException,
      GlORGOrganizationAlreadyDeletedException {

      Debug.print("GlOrganizationBean updateGlOrgEntry");
      
      /********************************************************
         Updates a particular organization
      ********************************************************/
      
      LocalGlOrganization  glOrganization = null;

      LocalGlOrganizationHome glOrganizationHome = null;
      
      // Initialize EJB Home
        
      try {

          glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }            
      
      if(details.getOrgMasterCode() != null){
         try {
            glOrganization = glOrganizationHome.findByPrimaryKey(details.getOrgMasterCode());
         } catch (FinderException ex) {
            throw new GlORGNoOrganizationCodeFoundException();
         } catch (EJBException ex) {
            throw new EJBException(ex.getMessage());
         } 
      }

      try {
         glOrganization = glOrganizationHome.findByPrimaryKey(
	    details.getOrgCode());
      } catch (FinderException ex) {
         throw new GlORGOrganizationAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (hasRelation(glOrganization, AD_CMPNY))
         throw new GlORGOrganizationAlreadyAssignedException();
      else {

         LocalGlOrganization glOrganization2 = null;

         try {
            glOrganization2 = glOrganizationHome.findByOrgName(details.getOrgName(), AD_CMPNY);
         } catch (FinderException ex) {
	    try {
	       glOrganization.setOrgName(details.getOrgName());
	       glOrganization.setOrgDescription(details.getOrgDescription());
	       glOrganization.setOrgMasterCode(details.getOrgMasterCode());
	    } catch (Exception e) {
	       getSessionContext().setRollbackOnly();
	       throw new EJBException(e.getMessage());
	    }
         } catch (Exception ex) {
            getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
         }

         if (glOrganization2 != null && 
            !glOrganization.getOrgCode().equals(glOrganization2.getOrgCode())) {
	 
	    getSessionContext().setRollbackOnly();
	    throw new GlORGOrganizationAlreadyExistException();
         } else
            if (glOrganization2 != null &&
	       glOrganization.getOrgCode().equals(glOrganization2.getOrgCode())) {

               try {
	          glOrganization.setOrgDescription(details.getOrgDescription());
		  glOrganization.setOrgMasterCode(details.getOrgMasterCode()); 
	       } catch (Exception e) {
	          getSessionContext().setRollbackOnly();
	          throw new EJBException(e.getMessage());
	       }
         }
      }
   }
   
   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteGlOrgEntry(Integer ORG_CODE, Integer AD_CMPNY)
      throws GlORGOrganizationAlreadyAssignedException,
      GlORGOrganizationAlreadyDeletedException{
    
      Debug.print("GlOrganizationBean deleteGlOrgEntry");
      
      /*******************************
         Deletes an existing ORG entry
      *******************************/
      
      LocalGlOrganization glOrganization = null;

      LocalGlOrganizationHome glOrganizationHome = null;
      
      // Initialize EJB Home
        
      try {

          glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }            
      
      try {
         glOrganization = glOrganizationHome.findByPrimaryKey(ORG_CODE);
      } catch (FinderException ex) {
         throw new GlORGOrganizationAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (hasRelation(glOrganization, AD_CMPNY))
         throw new GlORGOrganizationAlreadyAssignedException();
      else {
         try {
            glOrganization.remove();
         } catch (RemoveException ex) {
            getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
         } catch (Exception ex) {
            throw new EJBException(ex.getMessage());
         }
      }
      
   }
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlOrganizationControllerBean ejbCreate");

   }

   // private methods

   private boolean hasRelation(LocalGlOrganization glOrganization, Integer AD_CMPNY) {
    
      Debug.print("GlOrganizationControllerBean hasRelation");

      LocalGlOrganizationHome glOrganizationHome = null;
      
      // Initialize EJB Home
        
      try {

          glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }            
      
      try {
      	
          Collection glOrganizations = glOrganizationHome.findByMasterCode(glOrganization.getOrgCode(), AD_CMPNY);

	      if (!glOrganization.getGlResponsibilities().isEmpty() ||
	          !glOrganization.getGlAccountRanges().isEmpty() ||
			  !glOrganizations.isEmpty()) {
	      	
	      	return true;
	      	
	      } 
	      
	      return false;

      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage());
      }

   }

}
