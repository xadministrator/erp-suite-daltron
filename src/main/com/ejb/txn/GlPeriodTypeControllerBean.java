package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.GlPTNoPeriodTypeFoundException;
import com.ejb.exception.GlPTPeriodTypeAlreadyAssignedException;
import com.ejb.exception.GlPTPeriodTypeAlreadyDeletedException;
import com.ejb.exception.GlPTPeriodTypeAlreadyExistException;
import com.ejb.gl.LocalGlAccountingCalendarHome;
import com.ejb.gl.LocalGlPeriodType;
import com.ejb.gl.LocalGlPeriodTypeHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlPeriodTypeDetails;

/**
 * @ejb:bean name="GlPeriodTypeControllerEJB"
 *           display-name="Used for maintenance of period types"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlPeriodTypeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlPeriodTypeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlPeriodTypeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlPeriodTypeControllerBean extends AbstractSessionBean {

    
   /*******************************************************************
      Business methods:

      (1) getGlPtAll - returns an ArrayList of all TC

      (2) addGlPtEntry - validate input and duplicate entries
      			 before adding the PT

      (3) updateGlPtEntry - validate if PT to be updated is 
      			    already assigned to related
			    tables, if already assigned then
			    description field is the only
			    field that can be updated, otherwise
			    all fields can be entered

      (4) deleteGlPtEntry - validate if the PT to be deleted is
      			    already assigned to related tables,
			    if already assigned then this PT
			    cannot be deleted

      Private methods:

      (1) hasRelation - returns true if a specific PT has a relation
      			with any other table otherwise returns
			false

   *********************************************************************/

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlPtAll(Integer AD_CMPNY)
      throws GlPTNoPeriodTypeFoundException {

      Debug.print("GlPeriodTypeControllerBean getGlPtAll");

      /****************************************************
         Gets all PT
       ****************************************************/

      ArrayList ptAllList = new ArrayList();
      Collection glPeriodTypes = null;

      LocalGlPeriodTypeHome glPeriodTypeHome = null;
      
      // Initialize EJB Home
        
      try {

          glPeriodTypeHome = (LocalGlPeriodTypeHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlPeriodTypeHome.JNDI_NAME, LocalGlPeriodTypeHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }          
      
      try {
         glPeriodTypes = glPeriodTypeHome.findPtAll(AD_CMPNY);
      } catch (FinderException ex) {
         throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glPeriodTypes.size() == 0)
         throw new GlPTNoPeriodTypeFoundException();
         
      Iterator i = glPeriodTypes.iterator();
      while (i.hasNext()) {
         LocalGlPeriodType glPeriodType = (LocalGlPeriodType) i.next();
         GlPeriodTypeDetails details = new GlPeriodTypeDetails(
            glPeriodType.getPtCode(), glPeriodType.getPtName(), 
            glPeriodType.getPtDescription(), glPeriodType.getPtPeriodPerYear(),
            glPeriodType.getPtYearType());
         ptAllList.add(details);
      }

      return ptAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void addGlPtEntry(GlPeriodTypeDetails details, Integer AD_CMPNY)
      throws GlPTPeriodTypeAlreadyExistException {

      Debug.print("GlPeriodTypeControllerBean addGlPtEntry");

      /*******************************************************
         Adds a PT
      *******************************************************/

      LocalGlPeriodTypeHome glPeriodTypeHome = null;
      
      // Initialize EJB Home
        
      try {

          glPeriodTypeHome = (LocalGlPeriodTypeHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlPeriodTypeHome.JNDI_NAME, LocalGlPeriodTypeHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }
      
        try {

            glPeriodTypeHome.findByPtName(details.getPtName(), AD_CMPNY);
            
            getSessionContext().setRollbackOnly();
			throw new GlPTPeriodTypeAlreadyExistException();

        } catch (FinderException ex) {            	
        	
        }
      
      try {
         LocalGlPeriodType glPeriodType = glPeriodTypeHome.create(
	    details.getPtName(), details.getPtDescription(),
	    details.getPtPeriodPerYear(), details.getPtYearType(), AD_CMPNY);
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
	 throw new EJBException(ex.getMessage());
      }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlPtEntry(GlPeriodTypeDetails details, Integer AD_CMPNY)
      throws GlPTPeriodTypeAlreadyExistException,
      GlPTPeriodTypeAlreadyAssignedException,
      GlPTPeriodTypeAlreadyDeletedException {

      Debug.print("GlPeriodTypeControllerBean updateGlPtEntry");

      /********************************************************
         Updates a particular PT but only if the PT is not
	 yet assigned or PTs name to be entered is unique
      ********************************************************/

      LocalGlPeriodType glPeriodType = null;

      LocalGlPeriodTypeHome glPeriodTypeHome = null;
      
      // Initialize EJB Home
        
      try {

          glPeriodTypeHome = (LocalGlPeriodTypeHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlPeriodTypeHome.JNDI_NAME, LocalGlPeriodTypeHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
         glPeriodType = glPeriodTypeHome.findByPrimaryKey(
	    details.getPtCode());
      } catch (FinderException ex) {
         throw new GlPTPeriodTypeAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (hasRelation(glPeriodType, AD_CMPNY))
         throw new GlPTPeriodTypeAlreadyAssignedException();
      else {
       
         LocalGlPeriodType glPeriodType2 = null;

         try {
	    glPeriodType2 = 
	       glPeriodTypeHome.findByPtName(details.getPtName(), AD_CMPNY);
	 } catch (FinderException ex) {
	    glPeriodType.setPtName(details.getPtName());
	    glPeriodType.setPtDescription(details.getPtDescription());
	    glPeriodType.setPtPeriodPerYear(details.getPtPeriodPerYear());
	    glPeriodType.setPtYearType(details.getPtYearType());
	 } catch (Exception ex) {
	    throw new EJBException(ex.getMessage());
	 }

	 if(glPeriodType2 != null && !glPeriodType.getPtCode().equals(glPeriodType2.getPtCode())) {
	    getSessionContext().setRollbackOnly();
	    throw new GlPTPeriodTypeAlreadyExistException();
	 } else 
	    if(glPeriodType2 != null && glPeriodType.getPtCode().equals(glPeriodType2.getPtCode())) {
	       glPeriodType.setPtDescription(details.getPtDescription());
	       glPeriodType.setPtPeriodPerYear(details.getPtPeriodPerYear());
	       glPeriodType.setPtYearType(details.getPtYearType());
	 }
      }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteGlPtEntry(Integer PT_CODE, Integer AD_CMPNY) 
      throws GlPTPeriodTypeAlreadyDeletedException,
      GlPTPeriodTypeAlreadyAssignedException {

      Debug.print("GlPeriodTypeControllerBean deleteGlPtEntry");

      /********************************************************
         Deletes a PT but only if is still not assigned
       *******************************************************/

      LocalGlPeriodType glPeriodType = null;

      LocalGlPeriodTypeHome glPeriodTypeHome = null;
      
      // Initialize EJB Home
        
      try {

          glPeriodTypeHome = (LocalGlPeriodTypeHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlPeriodTypeHome.JNDI_NAME, LocalGlPeriodTypeHome.class);          
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
         glPeriodType = glPeriodTypeHome.findByPrimaryKey(PT_CODE);
      } catch (FinderException ex) {
         throw new GlPTPeriodTypeAlreadyDeletedException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage()); 
      }

      if (hasRelation(glPeriodType, AD_CMPNY))
         throw new GlPTPeriodTypeAlreadyAssignedException();
      else {
         try {
	    glPeriodType.remove();
	 } catch (RemoveException ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 } catch (Exception ex) {
	    getSessionContext().setRollbackOnly();
	    throw new EJBException(ex.getMessage());
	 }
      }
   }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlPeriodTypeControllerBean ejbCreate");
      
      LocalGlPeriodTypeHome glPeriodTypeHome = null;
      LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
      // Initialize EJB Home
        
      try {

          glPeriodTypeHome = (LocalGlPeriodTypeHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlPeriodTypeHome.JNDI_NAME, LocalGlPeriodTypeHome.class);          
          glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);          
          
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }              
      
   }

   // private methods

   private boolean hasRelation(LocalGlPeriodType glPeriodType, Integer AD_CMPNY) {

      Debug.print("GlPeriodTypeControllerBean hasRelation");

	  if(!glPeriodType.getGlAccountingCalendars().isEmpty()) {
	  
	  	return true;
	  
	  }
	  
	  return false;

   }

}
