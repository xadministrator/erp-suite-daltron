package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlRecurringJournal;
import com.ejb.gl.LocalGlRecurringJournalHome;
import com.ejb.gl.LocalGlRecurringJournalLine;
import com.ejb.gl.LocalGlRecurringJournalLineHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModRecurringJournalDetails;
import com.util.GlModRecurringJournalLineDetails;

/**
 * @ejb:bean name="GlRecurringJournalEntryControllerEJB"
 *           display-name="used for entering recurring journals"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRecurringJournalEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRecurringJournalEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRecurringJournalEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlRecurringJournalEntryControllerBean extends AbstractSessionBean {    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlJcAll(Integer AD_CMPNY) {
                    
        Debug.print("GlRecurringJournalEntryControllerBean getGlJcAll");
        
        LocalGlJournalCategoryHome glJournalCategoryHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalCategories = glJournalCategoryHome.findJcAll(AD_CMPNY);
        	
        	Iterator i = glJournalCategories.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalCategory glJournalCategory = (LocalGlJournalCategory)i.next();
        		
        		list.add(glJournalCategory.getJcName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {
                    
        Debug.print("GlRecurringJournalEntryControllerBean getAdUsrAll");
        
        LocalAdUserHome adUserHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adUsers = adUserHome.findUsrAll(AD_CMPNY);
        	
        	Iterator i = adUsers.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdUser adUser = (LocalAdUser)i.next();
        		
        		list.add(adUser.getUsrName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getAdPrfGlJournalLineNumber(Integer AD_CMPNY) {

       Debug.print("GlRecurringJournalEntryControllerBean getAdPrfGlJournalLineNumber");
                  
       LocalAdPreferenceHome adPreferenceHome = null;
      
      
       // Initialize EJB Home
        
       try {
            
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
       } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
       }
      

       try {
      	
          LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
         
          return adPreference.getPrfGlJournalLineNumber();
         
       } catch (Exception ex) {
        	 
          Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
         
       }
      
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public GlModRecurringJournalDetails getGlRjByRjCode(Integer RJ_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlRecurringJournalEntryControllerBean getGlRjByRjCode");
        
        LocalGlRecurringJournalHome glRecurringJournalHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;
        LocalGlRecurringJournalLineHome glRecurringJournalLineHome = null;
                
        // Initialize EJB Home
        
        try {
            
            glRecurringJournalHome = (LocalGlRecurringJournalHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlRecurringJournalHome.JNDI_NAME, LocalGlRecurringJournalHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
            glRecurringJournalLineHome = (LocalGlRecurringJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlRecurringJournalLineHome.JNDI_NAME, LocalGlRecurringJournalLineHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalGlRecurringJournal glRecurringJournal = null;
        	
        	
        	try {
        		
        		glRecurringJournal = glRecurringJournalHome.findByPrimaryKey(RJ_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList rjRjlList = new ArrayList();
        	
        	// get recurring journal lines
        	
        	Collection glRecurringJournalLines =  glRecurringJournalLineHome.findByRjCode(glRecurringJournal.getRjCode(), AD_CMPNY);            
        	
        	Iterator i = glRecurringJournalLines.iterator();
        	
        	double TOTAL_DEBIT = 0d;
        	double TOTAL_CREDIT = 0d;
        	
        	while (i.hasNext()) {
        		
        		LocalGlRecurringJournalLine glRecurringJournalLine = (LocalGlRecurringJournalLine)i.next();
        		
        		GlModRecurringJournalLineDetails mdetails = new GlModRecurringJournalLineDetails();
        		
        		mdetails.setRjlCode(glRecurringJournalLine.getRjlCode());
        		mdetails.setRjlLineNumber(glRecurringJournalLine.getRjlLineNumber());
        		mdetails.setRjlDebit(glRecurringJournalLine.getRjlDebit());
        		mdetails.setRjlAmount(glRecurringJournalLine.getRjlAmount());
        		mdetails.setRjlCoaAccountNumber(glRecurringJournalLine.getGlChartOfAccount().getCoaAccountNumber());
			    mdetails.setRjlCoaAccountDescription(glRecurringJournalLine.getGlChartOfAccount().getCoaAccountDescription());			    
			    
			    if(glRecurringJournalLine.getRjlDebit() == EJBCommon.TRUE) {
			    	
			    	TOTAL_DEBIT += glRecurringJournalLine.getRjlAmount();
			    	
			    } else {
			    	
			    	TOTAL_CREDIT += glRecurringJournalLine.getRjlAmount();
			    	
			    }
					
			    
        		rjRjlList.add(mdetails);
        		
        	}
        	
        	GlModRecurringJournalDetails mRjDetails = new GlModRecurringJournalDetails();
        	
        	mRjDetails.setRjCode(glRecurringJournal.getRjCode());
        	mRjDetails.setRjName(glRecurringJournal.getRjName());
        	mRjDetails.setRjDescription(glRecurringJournal.getRjDescription());
        	mRjDetails.setRjTotalDebit(TOTAL_DEBIT);
        	mRjDetails.setRjTotalCredit(TOTAL_CREDIT);
        	mRjDetails.setRjUserName1(glRecurringJournal.getRjUserName1());
        	mRjDetails.setRjUserName2(glRecurringJournal.getRjUserName2());
        	mRjDetails.setRjUserName3(glRecurringJournal.getRjUserName3());
        	mRjDetails.setRjUserName4(glRecurringJournal.getRjUserName4());
        	mRjDetails.setRjUserName5(glRecurringJournal.getRjUserName5());
        	mRjDetails.setRjSchedule(glRecurringJournal.getRjSchedule());
        	mRjDetails.setRjNextRunDate(glRecurringJournal.getRjNextRunDate());
        	mRjDetails.setRjLastRunDate(glRecurringJournal.getRjLastRunDate());
        	mRjDetails.setRjJcName(glRecurringJournal.getGlJournalCategory().getJcName());        	
        	mRjDetails.setRjRjlList(rjRjlList);
        	mRjDetails.setRjJbName(glRecurringJournal.getGlJournalBatch() != null ? glRecurringJournal.getGlJournalBatch().getJbName() : null);
        	
        	return mRjDetails;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveGlRjEntry(com.util.GlRecurringJournalDetails details, String JC_NM, 
    		String JB_NM, ArrayList rjlList, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyExistException,
		GlobalRecordAlreadyDeletedException,
		GlobalAccountNumberInvalidException {
                    
        Debug.print("GlRecurringJournalEntryControllerBean saveGlRjEntry");
        
        LocalGlRecurringJournalHome glRecurringJournalHome = null;     
        LocalGlJournalCategoryHome glJournalCategoryHome = null; 
        LocalGlJournalBatchHome glJournalBatchHome = null;
        
        LocalGlRecurringJournal glRecurringJournal = null;  
                
        // Initialize EJB Home
        
        try {
            
            glRecurringJournalHome = (LocalGlRecurringJournalHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlRecurringJournalHome.JNDI_NAME, LocalGlRecurringJournalHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
             
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }                      				                                
                
        try {        
        
        	// validate if recurring journal is already deleted
		
			try {
				
				if (details.getRjCode() != null) {
					
					glRecurringJournal = glRecurringJournalHome.findByPrimaryKey(details.getRjCode());
					
				}
				
		 	} catch (FinderException ex) {
		 		
		 		throw new GlobalRecordAlreadyDeletedException();	 		
				
			}     
			
			// validate if recurring journal exists
        		
			try {
				
			    LocalGlRecurringJournal glExistingRecurringJournal = glRecurringJournalHome.findByRjName(details.getRjName(), AD_CMPNY);
			    		
			    if (details.getRjCode() == null ||
			        details.getRjCode() != null && !glExistingRecurringJournal.getRjCode().equals(details.getRjCode())) {
			    	
			        throw new GlobalRecordAlreadyExistException();
			        
			    }
			 			
			} catch (FinderException ex) {
			    
			}  	
			
			// create recurring journal
        	        	
        	if (details.getRjCode() == null) {    
        	        	            	        	       	    		
        	    glRecurringJournal = glRecurringJournalHome.create(
        	    	details.getRjName(), details.getRjDescription(),
        	    	details.getRjUserName1(), details.getRjUserName2(),
        	    	details.getRjUserName3(), details.getRjUserName4(),
        	    	details.getRjUserName5(), details.getRjSchedule(),
        	    	details.getRjNextRunDate(), null, AD_BRNCH, AD_CMPNY);        	    
        	            	            	    	
        		
        	} else {
        		
        		glRecurringJournal.setRjName(details.getRjName());
        		glRecurringJournal.setRjDescription(details.getRjDescription());
        		glRecurringJournal.setRjUserName1(details.getRjUserName1());
        		glRecurringJournal.setRjUserName2(details.getRjUserName2());
        		glRecurringJournal.setRjUserName3(details.getRjUserName3());
        		glRecurringJournal.setRjUserName4(details.getRjUserName4());
        		glRecurringJournal.setRjUserName5(details.getRjUserName5());
        		glRecurringJournal.setRjSchedule(details.getRjSchedule());
        		glRecurringJournal.setRjNextRunDate(details.getRjNextRunDate());
        		
        	}    	
        	
        	LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName(JC_NM, AD_CMPNY);
        	glJournalCategory.addGlRecurringJournal(glRecurringJournal);   
        	
        	try {
 				
 				LocalGlJournalBatch glJournalBatch = glJournalBatchHome.findByJbName(JB_NM, AD_BRNCH, AD_CMPNY);
 				glJournalBatch.addGlRecurringJournal(glRecurringJournal);
 				
 			} catch (FinderException ex) {
 				
 			}
        	
        	
        	// remove all journal lines
      	   
	  	    Collection glRecurringJournalLines = glRecurringJournal.getGlRecurringJournalLines();
	  	  
	  	    Iterator i = glRecurringJournalLines.iterator();     	  
	  	  
	  	    while (i.hasNext()) {
	  	  	
	  	   	    LocalGlRecurringJournalLine glRecurringJournalLine = (LocalGlRecurringJournalLine)i.next();
	  	  	   
	  	  	    i.remove();
	  	  	    
	  	  	    glRecurringJournalLine.remove();
	  	  	
	  	    }
	  	    
	  	    
	  	    // add new recurring journal lines
      	  
      	    i = rjlList.iterator();
      	  
      	    while (i.hasNext()) {
      	  	
      	  	    GlModRecurringJournalLineDetails mRjlDetails = (GlModRecurringJournalLineDetails) i.next();      	  	  
      	  	          	  	  
      	  	    this.addGlRjlEntry(mRjlDetails, glRecurringJournal, AD_BRNCH, AD_CMPNY);
      	  	
      	    }
      	    
      	} catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	    
      	} catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	            	
        } catch (GlobalAccountNumberInvalidException ex) {
        	 
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
                	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteGlRjEntry(Integer RJ_CODE, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException {
                    
        Debug.print("GlRecurringJournalEntryControllerBean deleteGlRjEntry");
        
        LocalGlRecurringJournalHome glRecurringJournalHome = null;        
                
        // Initialize EJB Home
        
        try {
            
            glRecurringJournalHome = (LocalGlRecurringJournalHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlRecurringJournalHome.JNDI_NAME, LocalGlRecurringJournalHome.class);            
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalGlRecurringJournal glRecurringJournal = glRecurringJournalHome.findByPrimaryKey(RJ_CODE);
        	
        	glRecurringJournal.remove();
        	
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("GlRecurringJournalEntryControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public byte getAdPrfEnableGlJournalBatch(Integer AD_CMPNY) {
 		
 		Debug.print("GlRecurringJournalEntryControllerBean getAdPrfApEnableApVoucherBatch");
 		
 		LocalAdPreferenceHome adPreferenceHome = null;
 		
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		
 		try {
 			
 			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
 			
 			return adPreference.getPrfEnableGlJournalBatch();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getGlOpenJbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
 		
 		Debug.print("GlRecurringJournalEntryControllerBean getGlOpenJbAll");
 		
 		LocalGlJournalBatchHome glJournalBatchHome = null;               
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}        
 		
 		try {
 			
 			Collection glJournalBatches = glJournalBatchHome.findOpenJbAll(AD_BRNCH, AD_CMPNY);
 			
 			Iterator i = glJournalBatches.iterator();
 			
 			while (i.hasNext()) {
 				
 				LocalGlJournalBatch glJournalBatch = (LocalGlJournalBatch)i.next();
 				
 				list.add(glJournalBatch.getJbName());
 				
 			}
 			
 			return list;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
    
    // private methods
    
    private void addGlRjlEntry(GlModRecurringJournalLineDetails mdetails, LocalGlRecurringJournal glRecurringJournal, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws 
		GlobalAccountNumberInvalidException {
			
		Debug.print("GlRecurringJournalEntryControllerBean addGlRjlEntry");
		
		LocalGlRecurringJournalLineHome glRecurringJournalLineHome = null;        
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;
           
                
        // Initialize EJB Home
        
        try {
            
            glRecurringJournalLineHome = (LocalGlRecurringJournalLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlRecurringJournalLineHome.JNDI_NAME, LocalGlRecurringJournalLineHome.class);            
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
             
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
                
        try {
        	
        	// get company
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	// validate if coa exists
        	
        	LocalGlChartOfAccount glChartOfAccount = null;
        	
        	try {
        	
        		glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(mdetails.getRjlCoaAccountNumber(), AD_BRNCH, AD_CMPNY);
        		
        		if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
        		    throw new GlobalAccountNumberInvalidException(String.valueOf(mdetails.getRjlLineNumber()));
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalAccountNumberInvalidException(String.valueOf(mdetails.getRjlLineNumber()));
        		
        	}
        	      	        	
        			    
		    // create journal 
		    
		    LocalGlRecurringJournalLine glRecurringJournalLine = glRecurringJournalLineHome.create(
			    mdetails.getRjlLineNumber(), 
			    mdetails.getRjlDebit(), 
			    EJBCommon.roundIt(mdetails.getRjlAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision()), AD_CMPNY);
			    
			glRecurringJournal.addGlRecurringJournalLine(glRecurringJournalLine);
			glChartOfAccount.addGlRecurringJournalLine(glRecurringJournalLine);
		    
		    	    
		} catch (GlobalAccountNumberInvalidException ex) {        	
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;    
		            		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}
	
	
	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlRecurringJournalEntryControllerBean ejbCreate");
      
    }

}