package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlJREffectiveDateViolationException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlRecurringJournal;
import com.ejb.gl.LocalGlRecurringJournalHome;
import com.ejb.gl.LocalGlRecurringJournalLine;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlTransactionCalendarValue;
import com.ejb.gl.LocalGlTransactionCalendarValueHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModRecurringJournalDetails;

/**
 * @ejb:bean name="GlRecurringJournalGenerationControllerEJB"
 *           display-name="Used for searching journals"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRecurringJournalGenerationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRecurringJournalGenerationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRecurringJournalGenerationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlRecurringJournalGenerationControllerBean extends AbstractSessionBean {


   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlJcAll(Integer AD_CMPNY) {
                    
        Debug.print("GlRecurringJournalGenerationControllerBean getGlJcAll");
        
        LocalGlJournalCategoryHome glJournalCategoryHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection glJournalCategories = glJournalCategoryHome.findJcAll(AD_CMPNY);
        	
        	Iterator i = glJournalCategories.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlJournalCategory glJournalCategory = (LocalGlJournalCategory)i.next();
        		
        		list.add(glJournalCategory.getJcName());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
   
  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlRjByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("GlRecurringJournalGenerationControllerBean getGlJrPostableByCriteria");
      
      LocalGlRecurringJournalHome glRecurringJournalHome = null;
      
      // Initialize EJB Home
        
      try {
      	
          glRecurringJournalHome = (LocalGlRecurringJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlRecurringJournalHome.JNDI_NAME, LocalGlRecurringJournalHome.class);          
            
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }

      
      ArrayList rjList = new ArrayList();
      
      StringBuffer jbossQl = new StringBuffer();
      jbossQl.append("SELECT OBJECT(rj) FROM GlRecurringJournal rj ");
      
      boolean firstArgument = true;
      short ctr = 0;
      int criteriaSize = criteria.size() + 2;
      Object obj[];
      
       // Allocate the size of the object parameter

       
      if (criteria.containsKey("name")) {
      	
      	  criteriaSize--; 
      	 
      } 
            
      
      obj = new Object[criteriaSize];
         
      
      if (criteria.containsKey("name")) {
      	
      	 if (!firstArgument) {
      	 
      	    jbossQl.append("AND ");	
      	 	
         } else {
         	
         	firstArgument = false;
         	jbossQl.append("WHERE ");
         	
         }
         
      	 jbossQl.append("rj.rjName LIKE '%" + (String)criteria.get("name") + "%' ");
      	 
      }            	 
        
      if (criteria.containsKey("nextRunDateFrom")) {
	      	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("rj.rjNextRunDate>=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("nextRunDateFrom");
	  	 ctr++;
	  }  
	      
	  if (criteria.containsKey("nextRunDateTo")) {
	  	
	  	 if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	 } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	 }
	  	 jbossQl.append("rj.rjNextRunDate<=?" + (ctr+1) + " ");
	  	 obj[ctr] = (Date)criteria.get("nextRunDateTo");
	  	 ctr++;
	  	 
	  }    
	  	  	  
	  if (criteria.containsKey("category")) {
       	
       	  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("rj.glJournalCategory.jcName=?" + (ctr+1) + " ");
       	  obj[ctr] = (String)criteria.get("category");
       	  ctr++;
       	  
      }   
	  
	  if (!firstArgument) {
   	  	
   	     jbossQl.append("AND ");
   	     
   	  } else {
   	  	
   	  	 firstArgument = false;
   	  	 jbossQl.append("WHERE ");
   	  	 
   	  }
   	  
   	  jbossQl.append("rj.rjAdBranch=" + AD_BRNCH + " AND rj.rjAdCompany=" + AD_CMPNY + " ");
              
             	     
      String orderBy = null;
	      
	  if (ORDER_BY.equals("NAME")) {
	
	  	  orderBy = "rj.rjName";
	  	  
	  } else if (ORDER_BY.equals("NEXT RUN DATE")) {
	
	  	  orderBy = "rj.rjNextRunDate";
	  	
	  } else if (ORDER_BY.equals("CATEGORY")) {
	
	  	  orderBy = "rj.glJournalCategory.jcName";
	  	  
	  } 
	  
	  if (orderBy != null) {
	  
	  	jbossQl.append("ORDER BY " + orderBy);
	  	
	  } 
	  
               
      jbossQl.append(" OFFSET ?" + (ctr + 1));
      obj[ctr] = OFFSET;
      ctr++;
      
      jbossQl.append(" LIMIT ?" + (ctr + 1));
      obj[ctr] = LIMIT;
      
      System.out.println("QL + " + jbossQl);
      
      

      Collection glRecurringJournals = null;
      
      double TOTAL_DEBIT = 0;
      double TOTAL_CREDIT = 0;
      
      try {
      	
         glRecurringJournals = glRecurringJournalHome.getRjByCriteria(jbossQl.toString(), obj);
         
      } catch (Exception ex) {
      	
      	 throw new EJBException(ex.getMessage());
      	 
      }
      
      if (glRecurringJournals.isEmpty())
         throw new GlobalNoRecordFoundException();
         
      Iterator i = glRecurringJournals.iterator();
      while (i.hasNext()) {
      	
      	 TOTAL_DEBIT = 0d;
      	 TOTAL_CREDIT = 0d;
      	
         LocalGlRecurringJournal glRecurringJournal = (LocalGlRecurringJournal) i.next();
      	 Collection glRecurringJournalLines = glRecurringJournal.getGlRecurringJournalLines();
      	 
      	 Iterator j = glRecurringJournalLines.iterator();
      	 while (j.hasNext()) {
      	 	
      	 	LocalGlRecurringJournalLine glRecurringJournalLine = (LocalGlRecurringJournalLine) j.next();
      	 	
	        if (glRecurringJournalLine.getRjlDebit() == EJBCommon.TRUE) {
	     	
	           TOTAL_DEBIT += glRecurringJournalLine.getRjlAmount();
	        
	        } else {
	     	
	           TOTAL_CREDIT += glRecurringJournalLine.getRjlAmount();
	        }
	        
	     }
	     
	     // generate new next run date
	     
	     GregorianCalendar gc = new GregorianCalendar();
	     gc.setTime(glRecurringJournal.getRjNextRunDate());	     
	     	     
	     if (glRecurringJournal.getRjSchedule().equals("DAILY")) {
	     	
	     	gc.add(Calendar.DATE, 1);
	     	
	     } else if (glRecurringJournal.getRjSchedule().equals("WEEKLY")) {
	     	
	     	gc.add(Calendar.DATE, 7);
	     	
	     } else if (glRecurringJournal.getRjSchedule().equals("SEMI MONTHLY")) {
	     	
	     	gc.add(Calendar.DATE, 15);
	     	
	     } else if (glRecurringJournal.getRjSchedule().equals("MONTHLY")) {
	     	
	     	gc.add(Calendar.MONTH, 1);
	     	
	     } else if (glRecurringJournal.getRjSchedule().equals("QUARTERLY")) {
	     	
	     	gc.add(Calendar.MONTH, 3);
	     	
	     } else if (glRecurringJournal.getRjSchedule().equals("SEMI ANNUALLY")) {
	     	
	     	gc.add(Calendar.MONTH, 6);
	     	
	     } else if (glRecurringJournal.getRjSchedule().equals("ANNUALLY")) {
	     	
	     	gc.add(Calendar.YEAR, 1);
	     	
	     }
	     
	     GlModRecurringJournalDetails mdetails = new GlModRecurringJournalDetails();
	     mdetails.setRjCode(glRecurringJournal.getRjCode());
	     mdetails.setRjName(glRecurringJournal.getRjName());
	     mdetails.setRjNextRunDate(glRecurringJournal.getRjNextRunDate());
	     mdetails.setRjSchedule(glRecurringJournal.getRjSchedule());
	     mdetails.setRjLastRunDate(glRecurringJournal.getRjLastRunDate());
	     mdetails.setRjTotalDebit(TOTAL_DEBIT);
	     mdetails.setRjTotalCredit(TOTAL_CREDIT);	          
	     mdetails.setRjNewNextRunDate(gc.getTime());
	           	  
      	 rjList.add(mdetails);
      	
      }
         
      return rjList;
  
   }
   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("GlRecurringJournalGenerationControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }

    
    
    /**
    * @ejb:interface-method view-type="remote"
    **/
    public void executeGlRjGeneration(Integer RJ_CODE, Date RJ_NXT_RN_DT,
        String JR_DCMNT_NMBR, Date JR_EFFCTV_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws     	
		GlobalRecordAlreadyDeletedException,
		GlJREffectiveDateNoPeriodExistException,
		GlobalDocumentNumberNotUniqueException,
		GlJREffectiveDateViolationException,
		GlJREffectiveDatePeriodClosedException {

       Debug.print("GlJournalLineControllerBean executeGlRjGeneration");

      
       LocalGlSetOfBookHome glSetOfBookHome = null;
       LocalGlRecurringJournalHome glRecurringJournalHome = null;
       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
       LocalGlJournalHome glJournalHome = null;
       LocalGlJournalLineHome glJournalLineHome = null;
       LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
       LocalGlJournalSourceHome glJournalSourceHome = null;
       LocalGlTransactionCalendarValueHome glTransactionCalendarValueHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       LocalGlJournalBatchHome glJournalBatchHome = null;
       LocalAdPreferenceHome adPreferenceHome = null;
       LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
       
       LocalGlSetOfBook glSetOfBook = null;
       
       java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
      
     
       // Initialize EJB Home
        
       try {
            
           glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);       
           glRecurringJournalHome = (LocalGlRecurringJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlRecurringJournalHome.JNDI_NAME, LocalGlRecurringJournalHome.class);          
           glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);          
           glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);          
           glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);          
           adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);          
           glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);          
           glTransactionCalendarValueHome = (LocalGlTransactionCalendarValueHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlTransactionCalendarValueHome.JNDI_NAME, LocalGlTransactionCalendarValueHome.class);
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
           glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
		      lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		      lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);             
           adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
		      lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
           
       } catch (NamingException ex) {
       	
       	throw new EJBException(ex.getMessage());
            
       }

       try {
       	
       	   // get recurring journal
       	
       	   LocalGlRecurringJournal glRecurringJournal = null;
       	
       	   try {
       	   	
       	   	   glRecurringJournal = glRecurringJournalHome.findByPrimaryKey(RJ_CODE);
       	   	
       	   } catch (FinderException ex) {
       	   	
       	   	   throw new GlobalRecordAlreadyDeletedException();
       	   	
       	   }          	   
       	          	  	 
       	   // validate if period exists and open
       	   
       	   try {	  	 
       	          	  	     	
	       	   glSetOfBook = glSetOfBookHome.findByDate(JR_EFFCTV_DT, AD_CMPNY);	       	
	       	     	
		       LocalGlAccountingCalendarValue glAccountingCalendarValue =
		     	   glAccountingCalendarValueHome.findByAcCodeAndDate(
		     	     	glSetOfBook.getGlAccountingCalendar().getAcCode(),
		     	    	JR_EFFCTV_DT, AD_CMPNY);
		     	
		       if (glAccountingCalendarValue.getAcvStatus() == 'C' ||
		           glAccountingCalendarValue.getAcvStatus() == 'P' ||
		           glAccountingCalendarValue.getAcvStatus() == 'N') {
		           		           	
		           throw new GlJREffectiveDatePeriodClosedException(glRecurringJournal.getRjName());
		           	
		       }
		       
		   } catch (FinderException ex) {
		   	
		   	   throw new GlJREffectiveDateNoPeriodExistException(glRecurringJournal.getRjName());
		   	
		   }	  
		   
		   // validate effective date violation
      	        	
	  	
	       LocalGlTransactionCalendarValue glTransactionCalendarValue = 
	            glTransactionCalendarValueHome.findByTcCodeAndTcvDate(glSetOfBook.getGlTransactionCalendar().getTcCode(),
	               JR_EFFCTV_DT, AD_CMPNY);
	               
	       LocalGlJournalSource glValidateJournalSource = glJournalSourceHome.findByJsName("RECURRING", AD_CMPNY);          
	              
	       if (glTransactionCalendarValue.getTcvBusinessDay() == EJBCommon.FALSE &&
		       glValidateJournalSource.getJsEffectiveDateRule() == 'F') {
			      	
		      throw new GlJREffectiveDateViolationException(glRecurringJournal.getRjName());
			      		      	
		   }
		   
		   // validate if document number is unique
		   
		   try {
		   
 		   	  LocalGlJournal glExistingJournal = glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(JR_DCMNT_NMBR, "MANUAL", AD_BRNCH, AD_CMPNY);
 		   	  
 		   	  throw new GlobalDocumentNumberNotUniqueException(glRecurringJournal.getRjName());
 		   	  
 		   } catch (FinderException ex) {
	   	   }
	   	   
	   	   // generate document number if necessary
 		  LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
 		  
	   	   LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment =
		            adDocumentSequenceAssignmentHome.findByDcName("GL JOURNAL", AD_CMPNY);
	   	   
	   	   try {
	   	   	
	   	   		adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
	   	   		
	   	   } catch (FinderException ex) {
	   	   	
	   	   }
		            
	   	   if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
	            (JR_DCMNT_NMBR == null || JR_DCMNT_NMBR.trim().length() == 0)) {
	            	
	            while (true) {
	            	
	            	if ( adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null ) {

	            		try {
		            		
		            		glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
		            		
		            	} catch (FinderException ex) {
		            		
		            		JR_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
		            		adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
				            break;
		            		
		            	}	            	        			            	
	            		
	            	} else {

	            		try {
		            		
		            		glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
		            		
		            	} catch (FinderException ex) {
		            		
		            		JR_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
		            		adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
				            break;
		            		
		            	}	            	        			            	
	            		
	            	}
	            		            	
	            }		            
	            		            	
	        }
	        
	        // generate approval status
        	    
    	    LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    	    LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("RECURRING", AD_CMPNY);
    	    String JR_APPRVL_STATUS = null;  
    	    
    	    if (glJournalSource.getJsJournalApproval() == EJBCommon.FALSE) {
    	    	    	    	
    	    	JR_APPRVL_STATUS = "N/A";
    	    	
    	    }  							     
    	    
    	    // general journal batch if necessary
    	    
    	    LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
		    LocalGlJournalBatch glJournalBatch = null;
		   
		    if (adPreference.getPrfEnableGlJournalBatch() == EJBCommon.TRUE) {
		   
		       glJournalBatch = glRecurringJournal.getGlJournalBatch();
		    }
	        
	        // generate recurring journal
	        
	        LocalGlJournal glJournal = glJournalHome.create(
	        	glRecurringJournal.getRjName() + " " + formatter.format(new java.util.Date()),
	        	glRecurringJournal.getRjDescription(),
	        	JR_EFFCTV_DT, 0d,
	        	null, JR_DCMNT_NMBR, null, 1d, 
	        	JR_APPRVL_STATUS, null, 'N',
	        	EJBCommon.FALSE, EJBCommon.FALSE,
	        	USR_NM, new Date(),
	        	USR_NM, new Date(),
	        	null, null, null, null, null, null, EJBCommon.FALSE, null, 
	        	AD_BRNCH, AD_CMPNY);
	        	
	        glJournal.setGlJournalCategory(glRecurringJournal.getGlJournalCategory());
	        glJournal.setGlJournalSource(glJournalSource);
	        glJournal.setGlFunctionalCurrency(adCompany.getGlFunctionalCurrency());
	        
	        if (glJournalBatch != null) {
           
	        	glJournal.setGlJournalBatch(glJournalBatch);
           
            }
	        
	        Collection glRecurringJournalLines = glRecurringJournal.getGlRecurringJournalLines();
	        
	        Iterator i = glRecurringJournalLines.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalGlRecurringJournalLine glRecurringJournalLine = 
	        	    (LocalGlRecurringJournalLine)i.next();
	        	    
	        	LocalGlJournalLine glJournalLine = glJournalLineHome.create(
	        		glRecurringJournalLine.getRjlLineNumber(),
	        		glRecurringJournalLine.getRjlDebit(),
	        		glRecurringJournalLine.getRjlAmount(), "", AD_CMPNY);
	        		
	            glRecurringJournalLine.getGlChartOfAccount().addGlJournalLine(glJournalLine);
	            
	            glJournal.addGlJournalLine(glJournalLine);
	        	
	        }
	        
	        // set new run date
	        
	        glRecurringJournal.setRjNextRunDate(RJ_NXT_RN_DT);
	        glRecurringJournal.setRjLastRunDate(EJBCommon.getGcCurrentDateWoTime().getTime());
	   	   
	        		
       } catch (GlobalDocumentNumberNotUniqueException ex) {
       	 
       	   getSessionContext().setRollbackOnly();
      	   throw ex;
			              
       } catch (GlJREffectiveDateViolationException ex) {      	  
      	    
      	   getSessionContext().setRollbackOnly();
      	   throw ex;     	       	       	              	         	
       	  
       } catch (GlobalRecordAlreadyDeletedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       
       } catch (GlJREffectiveDatePeriodClosedException ex) {
       	   
       	   getSessionContext().setRollbackOnly();    	
       	   throw ex;	
       	   
       } catch (GlJREffectiveDateNoPeriodExistException ex) {
       	
       	   getSessionContext().setRollbackOnly();
       	   throw ex;       	   
                                                    
       } catch (Exception ex) {
       	 
       	   Debug.printStackTrace(ex);
       	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
       }

    }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlJournalBatchControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
