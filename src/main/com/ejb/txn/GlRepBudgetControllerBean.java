
/*
 * GlRepBudgetControllerBean.java
 *
 * Created on July 28, 2004, 9:14 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.exception.GlRepBGTPeriodOutOfRangeException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlBudget;
import com.ejb.gl.LocalGlBudgetAmountCoa;
import com.ejb.gl.LocalGlBudgetAmountHome;
import com.ejb.gl.LocalGlBudgetAmountPeriod;
import com.ejb.gl.LocalGlBudgetAmountPeriodHome;
import com.ejb.gl.LocalGlBudgetHome;
import com.ejb.gl.LocalGlBudgetOrganization;
import com.ejb.gl.LocalGlBudgetOrganizationHome;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepSalesDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModAccountingCalendarValueDetails;
import com.util.GlModBudgetDetails;
import com.util.GlRepBudgetDetails;

/**
 * @ejb:bean name="GlRepBudgetControllerEJB"
 *           display-name="Used for viewing budget"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepBudgetControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepBudgetController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepBudgetControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlRepBudgetControllerBean extends AbstractSessionBean {
	
  /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlAcvAll(Integer AD_CMPNY) {
                    
        Debug.print("GlRepBudgetControllerBean getGlAcvAll");
        
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
        	glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
        
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
        	
        	Iterator i = glSetOfBooks.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();
        		
        		Collection glAccountingCalendarValues = glSetOfBook.getGlAccountingCalendar().getGlAccountingCalendarValues();
        		
        		Iterator j = glAccountingCalendarValues.iterator();
    	        
    	         while (j.hasNext()) {
    	        	
    	        	LocalGlAccountingCalendarValue glAccountingCalendarValue = (LocalGlAccountingCalendarValue)j.next();
    	        	
    	        	GlModAccountingCalendarValueDetails mdetails = new GlModAccountingCalendarValueDetails();

    	        	mdetails.setAcvPeriodPrefix(glAccountingCalendarValue.getAcvPeriodPrefix());
    	        		            		
            		GregorianCalendar gc = new GregorianCalendar();
            		gc.setTime(glAccountingCalendarValue.getAcvDateTo());
            		
            		mdetails.setAcvYear(gc.get(Calendar.YEAR));
            		
            		// is current
	      	  	  
	      	  	    gc = EJBCommon.getGcCurrentDateWoTime();
	      	  	  
	      	  	    if ((glAccountingCalendarValue.getAcvDateFrom().before(gc.getTime()) ||
	      	  	         glAccountingCalendarValue.getAcvDateFrom().equals(gc.getTime())) &&
	      	  	        (glAccountingCalendarValue.getAcvDateTo().after(gc.getTime()) ||
	      	  	         glAccountingCalendarValue.getAcvDateTo().equals(gc.getTime()))) {
	      	  	      	
	      	  	      mdetails.setAcvCurrent(true);
	      	  	      	
	      	  	    }
            		    	        	
    	        	list.add(mdetails);
    	        	
    	         }
        		
        	}

	        return list;

        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlBoAll(Integer AD_CMPNY) {
                    
        Debug.print("GlRepBudgetControllerBean getGlBoAll");
        
        LocalGlBudgetOrganizationHome glBudgetOrganizationHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glBudgetOrganizationHome = (LocalGlBudgetOrganizationHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetOrganizationHome.JNDI_NAME, LocalGlBudgetOrganizationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glBudgetOrganizations = glBudgetOrganizationHome.findBoAll(AD_CMPNY);

	        Iterator i = glBudgetOrganizations.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalGlBudgetOrganization glBudgetOrganization = (LocalGlBudgetOrganization)i.next();

	        	list.add(glBudgetOrganization.getBoName());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlBgtAll(Integer AD_CMPNY) {
                    
        Debug.print("GlRepBudgetControllerBean getGlBgtAll");
        
        LocalGlBudgetHome glBudgetHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	glBudgetHome = (LocalGlBudgetHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetHome.JNDI_NAME, LocalGlBudgetHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection glBudgets = glBudgetHome.findBgtAll(AD_CMPNY);

	        Iterator i = glBudgets.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalGlBudget glBudget = (LocalGlBudget)i.next();
	        	
	        	GlModBudgetDetails mdetails = new GlModBudgetDetails();
	        	mdetails.setBgtName(glBudget.getBgtName());
	        	
	        	if(glBudget.getBgtStatus().equals("CURRENT")) {
	        	
	        		mdetails.setBgtIsDefault(true);
	        	
	        	}

	        	list.add(mdetails);
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeGlRepBudget(String BGT_BO_NM, String BGT_NM, String BGT_PRD, int BGT_YR,
    	      String BGT_AMNT_TYP, boolean DIS_INCLD_UNPSTD, boolean DIS_INCLD_UNPSTD_SL, 
    	      boolean DTB_SHW_ZRS,
    	      ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException,
              GlRepBGTPeriodOutOfRangeException {
                    
        Debug.print("GlRepBudgetControllerBean executeGlRepBudget");
        
        LocalGlBudgetAmountHome glBudgetAmountHome = null;
        LocalGlBudgetAmountPeriodHome glBudgetAmountPeriodHome = null;
        LocalGlBudgetHome glBudgetHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;
        LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
        LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalApVoucherHome apVoucherHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalCmDistributionRecordHome cmDistributionRecordHome = null;
        LocalAdBankAccountHome adBankAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        
        
        ArrayList list = new ArrayList();

        // Initialize EJB Home
        
        try {
            
        	glBudgetAmountHome = (LocalGlBudgetAmountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlBudgetAmountHome.JNDI_NAME, LocalGlBudgetAmountHome.class);
        	glBudgetAmountPeriodHome = (LocalGlBudgetAmountPeriodHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlBudgetAmountPeriodHome.JNDI_NAME, LocalGlBudgetAmountPeriodHome.class);
        	glBudgetHome = (LocalGlBudgetHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlBudgetHome.JNDI_NAME, LocalGlBudgetHome.class);
        	glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
        	glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
    			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
        	genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
        	glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
     	           lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
        	arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
     	           lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class); 
        	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
     	           lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);    
 	       apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
 	       invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
 		           lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
 	       arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
 		           lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);    
 	       apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
	           lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);    
	       cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
	           lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class); 
	       adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
		           lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
	       glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
		           lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
	       
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            LocalGenField genField = adCompany.getGenField();
	        String FL_SGMNT_SPRTR = String.valueOf(genField.getFlSegmentSeparator());
	        Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
        	        	        	
        	Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(BGT_PRD, EJBCommon.getIntendedDate(BGT_YR), AD_CMPNY);
            ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);            
            LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
        	
        	LocalGlAccountingCalendarValue glAccountingCalendarValue =
			     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
			     	glSetOfBook.getGlAccountingCalendar().getAcCode(),
					BGT_PRD, AD_CMPNY);
			
			short BGT_PRD_NMBR = 0;
			     	
			if (BGT_AMNT_TYP.equals("PTD")) {
				
				BGT_PRD_NMBR = glAccountingCalendarValue.getAcvPeriodNumber();
				
			} else if (BGT_AMNT_TYP.equals("QTD")) {
				
			    Collection glQuarterAccountingCalendarValues = 
			      glAccountingCalendarValueHome.findByAcCodeAndAcvQuarterNumber(
			      	  glSetOfBook.getGlAccountingCalendar().getAcCode(),
			      	  glAccountingCalendarValue.getAcvQuarter(), AD_CMPNY);
			      	  
			    ArrayList glQuarterAccountingCalendarValueList = 
			       new ArrayList(glQuarterAccountingCalendarValues);
			       
			    LocalGlAccountingCalendarValue glQuarterAccountingCalendarValue = 
			        (LocalGlAccountingCalendarValue)glQuarterAccountingCalendarValueList.get(0);
			        
			    BGT_PRD_NMBR = glQuarterAccountingCalendarValue.getAcvPeriodNumber();
				
			} else if (BGT_AMNT_TYP.equals("YTD")) {
				
				BGT_PRD_NMBR = 1;
				
			}						        	
        	
			
			
			
		 	
        	// get budget amount coa lines
        	        	
        	StringBuffer jbossQl = new StringBuffer();
	        jbossQl.append("SELECT OBJECT(bc) FROM GlBudgetAmountCoa bc ");
        	
        	if (BGT_BO_NM.equals("ALL")) {
        	
        	    jbossQl.append("WHERE bc.glBudgetAmount.glBudget.bgtName = '" + BGT_NM + "' AND bc.bcAdCompany=" + AD_CMPNY + " ");
        	    
        	    LocalGenSegment genSegment = genSegmentHome.findByFlCodeAndSegmentType(genField.getFlCode(), 'N', AD_CMPNY);
        	            	    
        	    jbossQl.append("ORDER BY bc.glChartOfAccount.coaSegment" + genSegment.getSgSegmentNumber() + ", bc.glChartOfAccount.coaAccountNumber ");
        	        	
        	} else {
        	
        	    jbossQl.append("WHERE bc.glBudgetAmount.glBudget.bgtName = '" + BGT_NM + "' AND bc.glBudgetAmount.glBudgetOrganization.boName = '" + BGT_BO_NM + "' AND bc.bcAdCompany=" + AD_CMPNY + " ");
        	        	
        	}        	        	
        	
        	Collection glBudgetAmountCoas = glBudgetAmountHome.getBgaByCriteria(jbossQl.toString(), new Object[0]);
			System.out.println("jbossQl.toString()="+jbossQl.toString()); 
			
			System.out.println("DTB_SHW_ZRS="+DTB_SHW_ZRS);
        	if(DTB_SHW_ZRS){
        		
        		Iterator x = glBudgetAmountCoas.iterator();
    			
    			while (x.hasNext()) {

    				LocalGlBudgetAmountCoa glBudgetAmountCoa = (LocalGlBudgetAmountCoa)x.next();
    				
    				GlRepBudgetDetails details = new GlRepBudgetDetails();

    		        details.setBgtAccountNumber(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
    		        details.setBgtAccountDescription(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountDescription());
    		        details.setBgtDescription(glBudgetAmountCoa.getBcDescription());
    		        details.setBgtDate(glAccountingCalendarValue.getAcvDateTo());
    		        details.setBgtAccountBalance(0d);
    		        details.setBgtRulePercentage1(glBudgetAmountCoa.getBcRulePercentage1());
    		        details.setBgtRulePercentage2(glBudgetAmountCoa.getBcRulePercentage2());
    		        
    		        int BGT_RNNG_PRD = BGT_PRD_NMBR;
    		        double BGT_AMNT = 0d;
    		        
    		        while (BGT_RNNG_PRD <= glAccountingCalendarValue.getAcvPeriodNumber()) {
    		        	
    		        	try {
    		        		
    		        	
    			        	LocalGlAccountingCalendarValue glRunningAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
    			        			glSetOfBook.getGlAccountingCalendar().getAcCode(), (short)BGT_RNNG_PRD, AD_CMPNY);
    			        	
    			        	LocalGlBudgetAmountPeriod glBudgetAmountPeriod = glBudgetAmountPeriodHome.findByBcCodeAndAcvCode(glBudgetAmountCoa.getBcCode(),
    			        			glRunningAccountingCalendarValue.getAcvCode(), AD_CMPNY);
    			        	
    			        	BGT_AMNT += glBudgetAmountPeriod.getBapAmount();
    			        	
    			        	BGT_RNNG_PRD++;
    			        	
    		        	} catch (FinderException ex) {
    		        		
    		        		throw new GlRepBGTPeriodOutOfRangeException();
    		        		
    		        	}
    		        	
    		        			        	
    		        }
    		        
    		        details.setBgtAmount(BGT_AMNT);
    		        
    		 		list.add(details);
    				
    				
    			}
    			
        		
        	}
        	
        	Iterator i = glBudgetAmountCoas.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalGlBudgetAmountCoa glBudgetAmountCoa = (LocalGlBudgetAmountCoa)i.next();
        		System.out.println("COA="+glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
        		
        		
        		double COA_BLNC = 0d;
        				        
        		// get beginning balance
    		 	
    		 	LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue = 
    		 		glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
    		 				glSetOfBook.getGlAccountingCalendar().getAcCode(),
    		 				BGT_PRD_NMBR, AD_CMPNY);			    	
    		 	
    		 	LocalGlChartOfAccountBalance glBeginningChartOfAccountBalance = 
    		 		glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
    		 				glBeginningAccountingCalendarValue.getAcvCode(),
    		 				glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_CMPNY);
    		 	
    		 	// get ending balance		 	
    		 	
    		 	LocalGlChartOfAccountBalance glEndingChartOfAccountBalance = 
    		 		glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
    		 				glAccountingCalendarValue.getAcvCode(),
    		 				glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_CMPNY);		 	
    		 	
    		 	COA_BLNC = glEndingChartOfAccountBalance.getCoabEndingBalance() - 
    			glBeginningChartOfAccountBalance.getCoabBeginningBalance();
    		 	System.out.println("COA_BLNC="+COA_BLNC);
    		 	
    		 // get coa debit or credit balance in unposted journals if necessary
    		 	
  
    		 	
    		 	
    		 	
    		 	if (DIS_INCLD_UNPSTD) {
    		 		
    		 		Collection glJournalLines = glJournalLineHome.findUnpostedJlByJrEffectiveDateRangeAndCoaCode(
    		 				glBeginningAccountingCalendarValue.getAcvDateFrom(),
    						glAccountingCalendarValue.getAcvDateTo(), 
    						glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_CMPNY);
    		 		System.out.println("glJournalLines="+glJournalLines.size());
    		 		Iterator j = glJournalLines.iterator();
    		 		
    		 		while (j.hasNext()) {
    		 			
    		 			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();
    		 			
    		 			LocalGlJournal glJournal = glJournalLine.getGlJournal();
    		 			
    		 			double JL_AMNT = this.convertForeignToFunctionalCurrency(
    		 					glJournal.getGlFunctionalCurrency().getFcCode(),
    							glJournal.getGlFunctionalCurrency().getFcName(),
    							glJournal.getJrConversionDate(),
    							glJournal.getJrConversionRate(),
    							glJournalLine.getJlAmount(), AD_CMPNY);
    		 			
    		 			if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) &&
    		 					glJournalLine.getJlDebit() == EJBCommon.TRUE) ||
    							(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") &&
    									glJournalLine.getJlDebit() == EJBCommon.FALSE)) {
    		 				
    		 				COA_BLNC += EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		 				
    		 			} else {
    		 				
    		 				COA_BLNC -= EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		 				
    		 			}
    		 			
    		 			System.out.println("1DIS_INCLD_UNPSTD="+DIS_INCLD_UNPSTD);
    		 			System.out.println("glJournal="+glJournal.getJrDocumentNumber());
    			 		System.out.println("JL_AMNT="+JL_AMNT);
    			 		
    			 		GlRepBudgetDetails details = new GlRepBudgetDetails();

    			        details.setBgtAccountNumber(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
    			        details.setBgtAccountDescription(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountDescription());
    			        details.setBgtDescription(glBudgetAmountCoa.getBcDescription());
    			        details.setBgtDocumentNumber(glJournal.getJrDocumentNumber());
    			        details.setBgtDate(glJournal.getJrEffectiveDate());
    			        details.setBgtAccountBalance(JL_AMNT);
    			        details.setBgtRulePercentage1(glBudgetAmountCoa.getBcRulePercentage1());
    			        details.setBgtRulePercentage2(glBudgetAmountCoa.getBcRulePercentage2());
    			        
    			        int BGT_RNNG_PRD = BGT_PRD_NMBR;
    			        double BGT_AMNT = 0d;
    			        
    			        while (BGT_RNNG_PRD <= glAccountingCalendarValue.getAcvPeriodNumber()) {
    			        	
    			        	try {
    			        		
    			        	
    				        	LocalGlAccountingCalendarValue glRunningAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
    				        			glSetOfBook.getGlAccountingCalendar().getAcCode(), (short)BGT_RNNG_PRD, AD_CMPNY);
    				        	
    				        	LocalGlBudgetAmountPeriod glBudgetAmountPeriod = glBudgetAmountPeriodHome.findByBcCodeAndAcvCode(glBudgetAmountCoa.getBcCode(),
    				        			glRunningAccountingCalendarValue.getAcvCode(), AD_CMPNY);
    				        	
    				        	BGT_AMNT += glBudgetAmountPeriod.getBapAmount();
    				        	
    				        	BGT_RNNG_PRD++;
    				        	
    			        	} catch (FinderException ex) {
    			        		
    			        		throw new GlRepBGTPeriodOutOfRangeException();
    			        		
    			        	}
    			        	
    			        			        	
    			        }
    			        
    			        details.setBgtAmount(BGT_AMNT);
    			        
    			 		list.add(details);
    		 			
    		 			
    		 		}		 	 			 	 	 
    		 		
    		 	}
    		 		
    		 // get coa debit or credit balance
    		 	
    		 	if (!DIS_INCLD_UNPSTD) {
    		 			
    		 		Collection glJournalLines = glJournalLineHome.findPostedJlByJrEffectiveDateRangeAndCoaCode(
    		 				glBeginningAccountingCalendarValue.getAcvDateFrom(),
    						glAccountingCalendarValue.getAcvDateTo(), 
    						glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_CMPNY);
    		 		
    		 		Iterator j = glJournalLines.iterator();
    		 		
    		 		while (j.hasNext()) {
    		 			
    		 			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();
    		 			
    		 			LocalGlJournal glJournal = glJournalLine.getGlJournal();
    		 			
    		 			double JL_AMNT = this.convertForeignToFunctionalCurrency(
    		 					glJournal.getGlFunctionalCurrency().getFcCode(),
    							glJournal.getGlFunctionalCurrency().getFcName(),
    							glJournal.getJrConversionDate(),
    							glJournal.getJrConversionRate(),
    							glJournalLine.getJlAmount(), AD_CMPNY);
    		 			
    		 			if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) &&
    		 					glJournalLine.getJlDebit() == EJBCommon.TRUE) ||
    							(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") &&
    									glJournalLine.getJlDebit() == EJBCommon.FALSE)) {
    		 				
    		 				COA_BLNC += EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		 				
    		 			} else {
    		 				
    		 				COA_BLNC -= EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		 				
    		 			}
    		 			
    		 			
    		
    		 			System.out.println("2DIS_INCLD_UNPSTD="+DIS_INCLD_UNPSTD);
    		 			System.out.println("glJournal="+glJournal.getJrDocumentNumber());
    			 		System.out.println("JL_AMNT="+JL_AMNT);
    			 		
    			 		GlRepBudgetDetails details = new GlRepBudgetDetails();

    			        details.setBgtAccountNumber(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
    			        details.setBgtAccountDescription(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountDescription());
    			        details.setBgtDescription(glBudgetAmountCoa.getBcDescription());
    			        details.setBgtDocumentNumber(glJournal.getJrDocumentNumber());
    			        details.setBgtDate(glJournal.getJrEffectiveDate());
    			        details.setBgtAccountBalance(JL_AMNT);
    			        details.setBgtRulePercentage1(glBudgetAmountCoa.getBcRulePercentage1());
    			        details.setBgtRulePercentage2(glBudgetAmountCoa.getBcRulePercentage2());
    			        
    			        int BGT_RNNG_PRD = BGT_PRD_NMBR;
    			        double BGT_AMNT = 0d;
    			        
    			        while (BGT_RNNG_PRD <= glAccountingCalendarValue.getAcvPeriodNumber()) {
    			        	
    			        	try {
    			        		
    			        	
    				        	LocalGlAccountingCalendarValue glRunningAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
    				        			glSetOfBook.getGlAccountingCalendar().getAcCode(), (short)BGT_RNNG_PRD, AD_CMPNY);
    				        	
    				        	LocalGlBudgetAmountPeriod glBudgetAmountPeriod = glBudgetAmountPeriodHome.findByBcCodeAndAcvCode(glBudgetAmountCoa.getBcCode(),
    				        			glRunningAccountingCalendarValue.getAcvCode(), AD_CMPNY);
    				        	
    				        	BGT_AMNT += glBudgetAmountPeriod.getBapAmount();
    				        	
    				        	BGT_RNNG_PRD++;
    				        	
    			        	} catch (FinderException ex) {
    			        		
    			        		throw new GlRepBGTPeriodOutOfRangeException();
    			        		
    			        	}
    			        	
    			        			        	
    			        }
    			        
    			        details.setBgtAmount(BGT_AMNT);
    			        
    			 		list.add(details);
    			 		
    		 			
    		 		}
    		 	
        	}
    		 	
    		 Collection apVOUDrs = null;
    		 Collection apDMDrs = null;
    		 Collection apCHKDrs = null;
    		 
    		 	
    		 	
    		 // include unposted subledger transactions
    		 	
    		 	if (DIS_INCLD_UNPSTD_SL) {
    		 		
    		 		

    			 	apVOUDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
    		 				EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);
    			 	System.out.println("apVOUDrs="+apVOUDrs.size());
    			 	Iterator j = apVOUDrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

    			 		LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

    			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
    			 				apVoucher.getGlFunctionalCurrency().getFcCode(),
    			 				apVoucher.getGlFunctionalCurrency().getFcName(),
    			 				apVoucher.getVouConversionDate(),
    			 				apVoucher.getVouConversionRate(),
    			 				apDistributionRecord.getDrAmount(), AD_CMPNY);

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}
    			 		
    			 		
    			 		GlRepBudgetDetails details = new GlRepBudgetDetails();

    			        details.setBgtAccountNumber(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
    			        details.setBgtAccountDescription(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountDescription());
    			        details.setBgtDescription(glBudgetAmountCoa.getBcDescription());
    			        details.setBgtDocumentNumber(apVoucher.getVouDocumentNumber());
    			        details.setBgtDate(apVoucher.getVouDate());
    			        details.setBgtAccountBalance(JL_AMNT);
    			        details.setBgtRulePercentage1(glBudgetAmountCoa.getBcRulePercentage1());
    			        details.setBgtRulePercentage2(glBudgetAmountCoa.getBcRulePercentage2());
    			        
    			        int BGT_RNNG_PRD = BGT_PRD_NMBR;
    			        double BGT_AMNT = 0d;
    			        
    			        while (BGT_RNNG_PRD <= glAccountingCalendarValue.getAcvPeriodNumber()) {
    			        	
    			        	try {
    			        		
    			        	
    				        	LocalGlAccountingCalendarValue glRunningAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
    				        			glSetOfBook.getGlAccountingCalendar().getAcCode(), (short)BGT_RNNG_PRD, AD_CMPNY);
    				        	
    				        	LocalGlBudgetAmountPeriod glBudgetAmountPeriod = glBudgetAmountPeriodHome.findByBcCodeAndAcvCode(glBudgetAmountCoa.getBcCode(),
    				        			glRunningAccountingCalendarValue.getAcvCode(), AD_CMPNY);
    				        	
    				        	BGT_AMNT += glBudgetAmountPeriod.getBapAmount();
    				        	
    				        	BGT_RNNG_PRD++;
    				        	
    			        	} catch (FinderException ex) {
    			        		
    			        		throw new GlRepBGTPeriodOutOfRangeException();
    			        		
    			        	}
    			        	
    			        			        	
    			        }
    			        
    			        details.setBgtAmount(BGT_AMNT);
    			        
    			 		list.add(details);
						
    			 	}
    			 	
    			 	apDMDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
    		 				EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);	
    			 	System.out.println("apDMDrs="+apDMDrs.size());
    			 	j = apDMDrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

    			 		LocalApVoucher apDebitMemo = apDistributionRecord.getApVoucher();

    			 		LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
    			 				apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

    			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
    			 				apVoucher.getGlFunctionalCurrency().getFcCode(),
    			 				apVoucher.getGlFunctionalCurrency().getFcName(),
    			 				apVoucher.getVouConversionDate(),
    			 				apVoucher.getVouConversionRate(),
    			 				apDistributionRecord.getDrAmount(), AD_CMPNY);

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}
    			 		
    			 		GlRepBudgetDetails details = new GlRepBudgetDetails();

    			        details.setBgtAccountNumber(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
    			        details.setBgtAccountDescription(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountDescription());
    			        details.setBgtDescription(glBudgetAmountCoa.getBcDescription());
    			        details.setBgtDocumentNumber(apVoucher.getVouDmVoucherNumber());
    			        details.setBgtDate(apVoucher.getVouDate());
    			        details.setBgtAccountBalance(JL_AMNT);
    			        details.setBgtRulePercentage1(glBudgetAmountCoa.getBcRulePercentage1());
    			        details.setBgtRulePercentage2(glBudgetAmountCoa.getBcRulePercentage2());
    			        
    			        int BGT_RNNG_PRD = BGT_PRD_NMBR;
    			        double BGT_AMNT = 0d;
    			        
    			        while (BGT_RNNG_PRD <= glAccountingCalendarValue.getAcvPeriodNumber()) {
    			        	
    			        	try {
    			        		
    			        	
    				        	LocalGlAccountingCalendarValue glRunningAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
    				        			glSetOfBook.getGlAccountingCalendar().getAcCode(), (short)BGT_RNNG_PRD, AD_CMPNY);
    				        	
    				        	LocalGlBudgetAmountPeriod glBudgetAmountPeriod = glBudgetAmountPeriodHome.findByBcCodeAndAcvCode(glBudgetAmountCoa.getBcCode(),
    				        			glRunningAccountingCalendarValue.getAcvCode(), AD_CMPNY);
    				        	
    				        	BGT_AMNT += glBudgetAmountPeriod.getBapAmount();
    				        	
    				        	BGT_RNNG_PRD++;
    				        	
    			        	} catch (FinderException ex) {
    			        		
    			        		throw new GlRepBGTPeriodOutOfRangeException();
    			        		
    			        	}
    			        	
    			        			        	
    			        }
    			        
    			        details.setBgtAmount(BGT_AMNT);
    			        
    			 		list.add(details);
						

    			 	}
      	     		
    			 	apCHKDrs = apDistributionRecordHome.findUnpostedChkByDateRangeAndCoaAccountNumber(
    			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);	
    			 	System.out.println("glBeginningAccountingCalendarValue.getAcvDateFrom()="+glBeginningAccountingCalendarValue.getAcvDateFrom());
    			 	System.out.println("glBeginningAccountingCalendarValue.getAcvDateTo()="+glBeginningAccountingCalendarValue.getAcvDateTo());
    			 	System.out.println("glBudgetAmountCoa.getGlChartOfAccount().getCoaCode()="+glBudgetAmountCoa.getGlChartOfAccount().getCoaCode());
    			 	System.out.println("apCHKDrs="+apCHKDrs.size());
    			 	j = apCHKDrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

    			 		LocalApCheck apCheck = apDistributionRecord.getApCheck();

    			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
    			 				apCheck.getGlFunctionalCurrency().getFcCode(),
    			 				apCheck.getGlFunctionalCurrency().getFcName(),
    			 				apCheck.getChkConversionDate(),
    			 				apCheck.getChkConversionRate(),
    			 				apDistributionRecord.getDrAmount(), AD_CMPNY);
    			 		System.out.println(apDistributionRecord.getApCheck().getChkDocumentNumber());
    			 		
    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}
    			 		

    			 		System.out.println("JL_AMNT="+JL_AMNT);
    			 		
    			 		GlRepBudgetDetails details = new GlRepBudgetDetails();

    			        details.setBgtAccountNumber(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
    			        details.setBgtAccountDescription(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountDescription());
    			        details.setBgtDescription(glBudgetAmountCoa.getBcDescription());
    			        details.setBgtDocumentNumber(apCheck.getChkDocumentNumber());
    			        details.setBgtDate(apCheck.getChkDate());
    			        details.setBgtAccountBalance(JL_AMNT);
    			        details.setBgtRulePercentage1(glBudgetAmountCoa.getBcRulePercentage1());
    			        details.setBgtRulePercentage2(glBudgetAmountCoa.getBcRulePercentage2());
    			        
    			        int BGT_RNNG_PRD = BGT_PRD_NMBR;
    			        double BGT_AMNT = 0d;
    			        
    			        while (BGT_RNNG_PRD <= glAccountingCalendarValue.getAcvPeriodNumber()) {
    			        	
    			        	try {
    			        		
    			        	
    				        	LocalGlAccountingCalendarValue glRunningAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
    				        			glSetOfBook.getGlAccountingCalendar().getAcCode(), (short)BGT_RNNG_PRD, AD_CMPNY);
    				        	
    				        	LocalGlBudgetAmountPeriod glBudgetAmountPeriod = glBudgetAmountPeriodHome.findByBcCodeAndAcvCode(glBudgetAmountCoa.getBcCode(),
    				        			glRunningAccountingCalendarValue.getAcvCode(), AD_CMPNY);
    				        	
    				        	BGT_AMNT += glBudgetAmountPeriod.getBapAmount();
    				        	
    				        	BGT_RNNG_PRD++;
    				        	
    			        	} catch (FinderException ex) {
    			        		
    			        		throw new GlRepBGTPeriodOutOfRangeException();
    			        		
    			        	}
    			        	
    			        			        	
    			        }
    			        
    			        details.setBgtAmount(BGT_AMNT);
    			        
    			        list.add(details);

    			 	}
    			 	
    			 	
    			 	/*Collection arINVDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
	 				EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
	 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);
	 		System.out.println("arINVDrs="+arINVDrs.size());
		 	Iterator iteratorAr = arINVDrs.iterator();
				
		 	while (iteratorAr.hasNext()) {

		 		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)iteratorAr.next();

		 		LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

		 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
		 				arInvoice.getGlFunctionalCurrency().getFcCode(),
		 				arInvoice.getGlFunctionalCurrency().getFcName(),
		 				arInvoice.getInvConversionDate(),
		 				arInvoice.getInvConversionRate(),
		 				arDistributionRecord.getDrAmount(), AD_CMPNY);

		 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
		 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

		 			COA_BLNC += JL_AMNT;

		 		} else {

		 			COA_BLNC -= JL_AMNT;

		 		}
		 		
		 		
		 		
		 		GlRepBudgetDetails details = new GlRepBudgetDetails();

		        details.setBgtAccountNumber(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
		        details.setBgtAccountDescription(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountDescription());
		        details.setBgtDescription(glBudgetAmountCoa.getBcDescription());
		        details.setBgtDocumentNumber(arInvoice.getInvNumber());
		        details.setBgtDate(arInvoice.getInvDate());
		        details.setBgtAccountBalance(JL_AMNT);
		        details.setBgtMisc1("");
		        details.setBgtMisc2("");
		        details.setBgtMisc3("");
		        details.setBgtMisc4("");
		        details.setBgtMisc5("");
		        details.setBgtMisc6("");
		        
		        int BGT_RNNG_PRD = BGT_PRD_NMBR;
		        double BGT_AMNT = 0d;
		        
		        while (BGT_RNNG_PRD <= glAccountingCalendarValue.getAcvPeriodNumber()) {
		        	
		        	try {
		        		
		        	
			        	LocalGlAccountingCalendarValue glRunningAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
			        			glSetOfBook.getGlAccountingCalendar().getAcCode(), (short)BGT_RNNG_PRD, AD_CMPNY);
			        	
			        	LocalGlBudgetAmountPeriod glBudgetAmountPeriod = glBudgetAmountPeriodHome.findByBcCodeAndAcvCode(glBudgetAmountCoa.getBcCode(),
			        			glRunningAccountingCalendarValue.getAcvCode(), AD_CMPNY);
			        	
			        	BGT_AMNT += glBudgetAmountPeriod.getBapAmount();
			        	
			        	BGT_RNNG_PRD++;
			        	
		        	} catch (FinderException ex) {
		        		
		        		throw new GlRepBGTPeriodOutOfRangeException();
		        		
		        	}
		        	
		        			        	
		        }
		        
		        details.setBgtAmount(BGT_AMNT);
		        
		 		list.add(details);

		 	}
		 	
		 	
		 	
		 	
		 	
		 	

		 	Collection arCMDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
	 				EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
	 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);	

		 	j = arCMDrs.iterator();

		 	while (j.hasNext()) {

		 		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

		 		LocalArInvoice arCreditMemo = arDistributionRecord.getArInvoice();

		 		LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
		 				arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

		 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
		 				arInvoice.getGlFunctionalCurrency().getFcCode(),
		 				arInvoice.getGlFunctionalCurrency().getFcName(), 
		 				arInvoice.getInvConversionDate(),
		 				arInvoice.getInvConversionRate(),
		 				arDistributionRecord.getDrAmount(), AD_CMPNY);

		 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
		 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

		 			COA_BLNC += JL_AMNT;

		 		} else {

		 			COA_BLNC -= JL_AMNT;

		 		}
		 		
		 		
		 		GlRepBudgetDetails details = new GlRepBudgetDetails();

		        details.setBgtAccountNumber(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
		        details.setBgtAccountDescription(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountDescription());
		        details.setBgtDescription(glBudgetAmountCoa.getBcDescription());
		        details.setBgtDocumentNumber(arInvoice.getInvCmInvoiceNumber());
		        details.setBgtDate(arInvoice.getInvDate());
		        details.setBgtAccountBalance(JL_AMNT);
		        details.setBgtMisc1("");
		        details.setBgtMisc2("");
		        details.setBgtMisc3("");
		        details.setBgtMisc4("");
		        details.setBgtMisc5("");
		        details.setBgtMisc6("");
		        
		        int BGT_RNNG_PRD = BGT_PRD_NMBR;
		        double BGT_AMNT = 0d;
		        
		        while (BGT_RNNG_PRD <= glAccountingCalendarValue.getAcvPeriodNumber()) {
		        	
		        	try {
		        		
		        	
			        	LocalGlAccountingCalendarValue glRunningAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
			        			glSetOfBook.getGlAccountingCalendar().getAcCode(), (short)BGT_RNNG_PRD, AD_CMPNY);
			        	
			        	LocalGlBudgetAmountPeriod glBudgetAmountPeriod = glBudgetAmountPeriodHome.findByBcCodeAndAcvCode(glBudgetAmountCoa.getBcCode(),
			        			glRunningAccountingCalendarValue.getAcvCode(), AD_CMPNY);
			        	
			        	BGT_AMNT += glBudgetAmountPeriod.getBapAmount();
			        	
			        	BGT_RNNG_PRD++;
			        	
		        	} catch (FinderException ex) {
		        		
		        		throw new GlRepBGTPeriodOutOfRangeException();
		        		
		        	}
		        	
		        			        	
		        }
		        
		        details.setBgtAmount(BGT_AMNT);
		        
		 		list.add(details);

		 	}
		 	
		 	Collection arRCTDrs = arDistributionRecordHome.findUnpostedRctByDateRangeAndCoaAccountNumber(
		 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
	 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);	
		 	System.out.println("arRCTDrs="+arRCTDrs.size());
		 	j = arRCTDrs.iterator();

		 	while (j.hasNext()) {

		 		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

		 		LocalArReceipt arReceipt = arDistributionRecord.getArReceipt();

		 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
		 				arReceipt.getGlFunctionalCurrency().getFcCode(),
		 				arReceipt.getGlFunctionalCurrency().getFcName(),
		 				arReceipt.getRctConversionDate(),
		 				arReceipt.getRctConversionRate(),
		 				arDistributionRecord.getDrAmount(), AD_CMPNY);

		 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
		 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

		 			COA_BLNC += JL_AMNT;

		 		} else {

		 			COA_BLNC -= JL_AMNT;

		 		}
		 		
		 		GlRepBudgetDetails details = new GlRepBudgetDetails();

		        details.setBgtAccountNumber(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountNumber());
		        details.setBgtAccountDescription(glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountDescription());
		        details.setBgtDescription(glBudgetAmountCoa.getBcDescription());
		        details.setBgtDocumentNumber(arReceipt.getRctNumber());
		        details.setBgtDate(arReceipt.getRctDate());
		        details.setBgtAccountBalance(JL_AMNT);
		        details.setBgtMisc1("");
		        details.setBgtMisc2("");
		        details.setBgtMisc3("");
		        details.setBgtMisc4("");
		        details.setBgtMisc5("");
		        details.setBgtMisc6("");
		        
		        int BGT_RNNG_PRD = BGT_PRD_NMBR;
		        double BGT_AMNT = 0d;
		        
		        while (BGT_RNNG_PRD <= glAccountingCalendarValue.getAcvPeriodNumber()) {
		        	
		        	try {
		        		
		        	
			        	LocalGlAccountingCalendarValue glRunningAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
			        			glSetOfBook.getGlAccountingCalendar().getAcCode(), (short)BGT_RNNG_PRD, AD_CMPNY);
			        	
			        	LocalGlBudgetAmountPeriod glBudgetAmountPeriod = glBudgetAmountPeriodHome.findByBcCodeAndAcvCode(glBudgetAmountCoa.getBcCode(),
			        			glRunningAccountingCalendarValue.getAcvCode(), AD_CMPNY);
			        	
			        	BGT_AMNT += glBudgetAmountPeriod.getBapAmount();
			        	
			        	BGT_RNNG_PRD++;
			        	
		        	} catch (FinderException ex) {
		        		
		        		throw new GlRepBGTPeriodOutOfRangeException();
		        		
		        	}
		        	
		        			        	
		        }
		        
		        details.setBgtAmount(BGT_AMNT);
		        
		 		list.add(details);

		 	}*/
    			 	
    			 	/*Collection apPODrs = apDistributionRecordHome.findUnpostedPoByDateRangeAndCoaAccountNumber(
    			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);
    			 	System.out.println("apPODrs="+apPODrs.size());
    			 	j = apPODrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

    			 		LocalApPurchaseOrder apPurchaseOrder = apDistributionRecord.getApPurchaseOrder();

    			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
    			 				apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
    			 				apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
    			 				apPurchaseOrder.getPoConversionDate(),
    			 				apPurchaseOrder.getPoConversionRate(),
    			 				apDistributionRecord.getDrAmount(), AD_CMPNY);

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}

    			 	}

    			 	Collection cmADJDrs = cmDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
    			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);
    			 	System.out.println("cmADJDrs="+cmADJDrs.size());
    			 	j = cmADJDrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

    			 		LocalCmAdjustment cmAdjustment = cmDistributionRecord.getCmAdjustment();

    			 		LocalAdBankAccount adBankAccount = cmAdjustment.getAdBankAccount();

    			 		//LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getFtAdBaAccountFrom());

    			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
    			 				adBankAccount.getGlFunctionalCurrency().getFcCode(),
    			 				adBankAccount.getGlFunctionalCurrency().getFcName(),
    			 				cmAdjustment.getAdjConversionDate(),
    			 				cmAdjustment.getAdjConversionRate(),
    			 				cmDistributionRecord.getDrAmount(), AD_CMPNY);

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}

    			 	}


    			 	Collection cmFTDrs = cmDistributionRecordHome.findUnpostedFtByDateRangeAndCoaAccountNumber(
    			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY); 	     			
    			 	System.out.println("cmFTDrs="+cmFTDrs.size());
    			 	j = cmFTDrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

    			 		LocalCmFundTransfer cmFundTransfer = cmDistributionRecord.getCmFundTransfer();

    			 		LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

    			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
    			 				adBankAccount.getGlFunctionalCurrency().getFcCode(),
    			 				adBankAccount.getGlFunctionalCurrency().getFcName(),
    			 				cmFundTransfer.getFtConversionDate(),
    			 				cmFundTransfer.getFtConversionRateFrom(),
    			 				cmDistributionRecord.getDrAmount(), AD_CMPNY);

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}

    			 	}
    			 	
    			 	Collection invADJDrs = invDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
    			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);
    			 	System.out.println("invADJDrs="+invADJDrs.size());
    			 	j = invADJDrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    			 		double JL_AMNT = invDistributionRecord.getDrAmount();

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}

    			 	}
    			 	
    			 	Collection invBUADrs = invDistributionRecordHome.findUnpostedBuaByDateRangeAndCoaAccountNumber(
    			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);

    			 	System.out.println("invBUADrs="+invBUADrs.size());
    			 	j = invBUADrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    			 		double JL_AMNT = invDistributionRecord.getDrAmount();

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}

    			 	}
    			 	
    			 	Collection invOHDrs = invDistributionRecordHome.findUnpostedOhByDateRangeAndCoaAccountNumber(
    			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);
    			 	System.out.println("invOHDrs="+invOHDrs.size());
    			 	j = invOHDrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    			 		double JL_AMNT = invDistributionRecord.getDrAmount();

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}

    			 	}

    			 	Collection invSIDrs = invDistributionRecordHome.findUnpostedSiByDateRangeAndCoaAccountNumber(
    			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);
    			 	System.out.println("invSIDrs="+invSIDrs.size());
    			 	j = invSIDrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    			 		double JL_AMNT = invDistributionRecord.getDrAmount();

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}

    			 	}

    			 	Collection invATRDrs = invDistributionRecordHome.findUnpostedAtrByDateRangeAndCoaAccountNumber(
    			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);
    			 	System.out.println("invATRDrs="+invATRDrs.size());
    			 	j = invATRDrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    			 		double JL_AMNT = invDistributionRecord.getDrAmount();

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}

    			 	}
    			 	
    			 	Collection invSTDrs = invDistributionRecordHome.findUnpostedStByDateRangeAndCoaAccountNumber(
    			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
    		 				glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);
    			 	System.out.println("invSTDrs="+invSTDrs.size());
    			 	j = invSTDrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    			 		double JL_AMNT = invDistributionRecord.getDrAmount();

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}

    			 	}
    			 	
    			 	Collection invBSTDrs = invDistributionRecordHome.findUnpostedBstByDateRangeAndCoaAccountNumber(
    			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
    			 			glAccountingCalendarValue.getAcvDateTo(), glBudgetAmountCoa.getGlChartOfAccount().getCoaCode(), AD_BRNCH, AD_CMPNY);
    			 	System.out.println("invBSTDrs="+invBSTDrs.size());
    			 	j = invBSTDrs.iterator();

    			 	while (j.hasNext()) {

    			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    			 		double JL_AMNT = invDistributionRecord.getDrAmount();

    			 		if (((glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") || glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
    			 				(!glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("ASSET") && !glBudgetAmountCoa.getGlChartOfAccount().getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

    			 			COA_BLNC += JL_AMNT;

    			 		} else {

    			 			COA_BLNC -= JL_AMNT;

    			 		}

    			 	}*/
    			 	
    		 	}
    		 	
    		 	
        		
		        Collections.sort(list, GlRepBudgetDetails.AccountNumberComparator);
		        
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	
        } catch (GlRepBGTPeriodOutOfRangeException ex) {
        	
        	throw ex;

        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("GlRepBudgetControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}   
	
	
	// private methods
	   
	   private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
		    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
		    	
		    Debug.print("GlRepBudgetControllerBean convertForeignToFunctionalCurrency");
		    
		    
	        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
	        LocalAdCompanyHome adCompanyHome = null;
	         
	        LocalAdCompany adCompany = null;
	                 
	        // Initialize EJB Homes
	         
	        try {
	         	
	            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
	               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
	            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
	                   
	         } catch (NamingException ex) {
	            
	            throw new EJBException(ex.getMessage());
	            
	         }
	         
	         // get company and extended precision
	         
	         try {
	         	
	             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	         	             
	         } catch (Exception ex) {
	         	
	             throw new EJBException(ex.getMessage());
	         	
	         }	     
	        
	         
	         // Convert to functional currency if necessary
	         
	         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
	         	         
	             AMOUNT = AMOUNT * CONVERSION_RATE;
	             	
	         } else if (CONVERSION_DATE != null) {
	         	
	         	 try {
	         	 	    
	         	 	 // Get functional currency rate
	         	 	 
	         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
	         	     
	         	     if (!FC_NM.equals("USD")) {
	         	     	
	        	         glReceiptFunctionalCurrencyRate = 
		     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
		     	             CONVERSION_DATE, AD_CMPNY);
		     	             
		     	         AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();
		     	             
	         	     }
	                 
	                 // Get set of book functional currency rate if necessary
	         	 	         
	                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
	                 
	                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
	                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
	                             getFcCode(), CONVERSION_DATE, AD_CMPNY);
	                     
	                     AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();
	                 
	                  }
	                                 
	         	             
	         	 } catch (Exception ex) {
	         	 	
	         	 	throw new EJBException(ex.getMessage());
	         	 	
	         	 }       
	         	
	         }
	         
	         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
	         
		}
	   
	   
	   
	   /**
	    * @ejb:interface-method view-type="remote"
	    * @jboss:method-attributes read-only="true"
	    **/   
	   public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
	   	throws GlobalNoRecordFoundException{
	       
	       Debug.print("GlRepBudgetControllerBean getAdBrResAll");
	       
	       LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
	       LocalAdBranchHome adBranchHome = null;
	       
	       LocalAdBranchResponsibility adBranchResponsibility = null;
	       LocalAdBranch adBranch = null;
	       
	       Collection adBranchResponsibilities = null;
	       
	       ArrayList list = new ArrayList();
	       
	       // Initialize EJB Home
	       
	       try {
	           
	           adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
	           adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
	           
	       } catch (NamingException ex) {
	           
	           throw new EJBException(ex.getMessage());
	           
	       }
	       
	       try {
	           
	           adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
	           
	       } catch (FinderException ex) {
	           
	       } catch (Exception ex) {
	           
	           throw new EJBException(ex.getMessage());
	       }
	       
	       if (adBranchResponsibilities.isEmpty()) {
	           
	           throw new GlobalNoRecordFoundException();
	           
	       }
	       
	       try {
	           
	           Iterator i = adBranchResponsibilities.iterator();
	           
	           while(i.hasNext()) {
	               
	               adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	               
	               adBranch = adBranchResponsibility.getAdBranch();
	               
	               AdBranchDetails details = new AdBranchDetails();
	               
	               details.setBrCode(adBranch.getBrCode());
	               details.setBrBranchCode(adBranch.getBrBranchCode());
	               details.setBrName(adBranch.getBrName());
	               details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
	               
	               list.add(details);
	               
	           }	               
	           
	       } catch (Exception ex) {
	           
	           throw new EJBException(ex.getMessage());
	       }
	       
	       return list;
	       
	   }
		
	   

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlRepBudgetControllerBean ejbCreate");
      
    }
}
