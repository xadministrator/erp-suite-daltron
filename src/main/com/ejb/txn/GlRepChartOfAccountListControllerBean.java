package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GenModSegmentDetails;
import com.util.GlRepChartOfAccountListDetails;

/**
 * @ejb:bean name="GlRepChartOfAccountListControllerEJB"
 *           display-name="Used for generation of chart of account list"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepChartOfAccountListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepChartOfAccountListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepChartOfAccountListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlRepChartOfAccountListControllerBean extends AbstractSessionBean {

    
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList executeGlRepChartOfAccountList(String COA_ACCNT_NMBR_FRM, String COA_ACCNT_NMBR_TO, 
      boolean COA_ENBL, boolean COA_DSBL, ArrayList branchList, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

      Debug.print("GlRepChartOfAccountListControllerBean executeGlRepChartOfAccountList");
      
      LocalGlChartOfAccountHome glChartOfAccountHome = null;
      LocalGenValueSetValueHome genValueSetValueHome = null;
      LocalGenSegmentHome genSegmentHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      
      ArrayList list = new ArrayList();
      
	   // Initialize EJB Home
	    
	  try {
	        
	       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	       genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
	       genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	           
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	  
	  try {
	  	
	  	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	  	  LocalGenField genField = adCompany.getGenField();
	  	  Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
	      String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
	      int genNumberOfSegment = genField.getFlNumberOfSegment();
	      
	      // get coa selected
	      
	      StringBuffer jbossQl = new StringBuffer();
          jbossQl.append("SELECT DISTINCT OBJECT(coa) FROM GlChartOfAccount coa ");

          // add branch criteria

		  if (branchList.isEmpty()) {
		  	
		  	throw new GlobalNoRecordFoundException();
		  	
		  }
		  else {
		  	
		  	jbossQl.append(", in (coa.adBranchCoas) bcoa WHERE bcoa.adBranch.brCode in (");
		  	
		  	boolean firstLoop = true;
		  	
		  	Iterator j = branchList.iterator();
		  	
		  	while(j.hasNext()) {
		  		
		  		if(firstLoop == false) { 
		  			jbossQl.append(", "); 
		  		}
		  		else { 
		  			firstLoop = false; 
		  		}
		  		
		  		AdBranchDetails mdetails = (AdBranchDetails) j.next();
		  		
		  		jbossQl.append(mdetails.getBrCode());
		  		
		  	}
		  	
		  	jbossQl.append(") AND ");
		  	
		  }

          StringTokenizer stAccountFrom = new StringTokenizer(COA_ACCNT_NMBR_FRM, strSeparator);
          StringTokenizer stAccountTo = new StringTokenizer(COA_ACCNT_NMBR_TO, strSeparator);
      
          // validate if account number is valid
        
          if (stAccountFrom.countTokens() != genNumberOfSegment || 
	         stAccountTo.countTokens() != genNumberOfSegment) {
	      	
	      	  throw new GlobalAccountNumberInvalidException();
	      	
	      }
	      
	      try {
      	
	          String var = "1";
	          
	          if (genNumberOfSegment > 1) {
	          	
	          	for (int i=0; i<genNumberOfSegment; i++) {
	          		
	          		if (i == 0 && i < genNumberOfSegment - 1) {
	          			
	          			// for first segment
	          			
	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");
	          			
	          			var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)+1 ";
	          			
	          			
	          		} else if (i != 0 && i < genNumberOfSegment - 1){     		
	          			
	          			// for middle segments
	          			
	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")) - (" + var +")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");
	          			
	          			var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";
	          			
	          		} else if (i != 0 && i == genNumberOfSegment - 1) {
	          			
	          			// for last segment
	          			
	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' ");
	          			
	          			
	          		}	     		       	      	   	            
	          		
	          	}
	          	
	          } else if(genNumberOfSegment == 1) {
	          	
	          	String accountFrom = stAccountFrom.nextToken();
	       		String accountTo = stAccountTo.nextToken();
	       		
	       		jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + accountFrom + "' AND '" + accountTo + "' OR " +
	       				"coa.coaAccountNumber BETWEEN '" + accountFrom + "' AND '" + accountTo + "' ");
	          	
	          }
	      	  
			  if (COA_ENBL && !COA_DSBL) {
			  	
			  	  jbossQl.append("AND coa.coaEnable=1 ");
			  	
			  } else if (!COA_ENBL && COA_DSBL) {
			  
			  	  jbossQl.append("AND coa.coaEnable=0 ");
			  	  
			  }
			  
			  jbossQl.append("AND coa.coaAdCompany=" + AD_CMPNY + " ");
			  			  
		 } catch (NumberFormatException ex) {
		 	
		 	throw new GlobalAccountNumberInvalidException();
		 	
		 }
		 
		 // generate order by coa natural account

	     short accountSegmentNumber = 0;
	      
	     try {
	      
	      	  LocalGenSegment genSegment = genSegmentHome.findByFlCodeAndSegmentType(genField.getFlCode(), 'N', AD_CMPNY);
	      	  accountSegmentNumber = genSegment.getSgSegmentNumber();
	      	
	     } catch (Exception ex) {
	      
	         throw new EJBException(ex.getMessage());
	      
	     }
	            
	      	  
		 jbossQl.append("ORDER BY coa.coaSegment" + accountSegmentNumber + ", coa.coaAccountNumber ");
		 
		 Object obj[] = new Object[0];
	 
  	     Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), obj);
		  
		 Iterator i = glChartOfAccounts.iterator();		 
		 		 		 
		 while (i.hasNext()) {
		 	
		 	LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)i.next();
		 	
		 	GlRepChartOfAccountListDetails details = new GlRepChartOfAccountListDetails();		 			 	 	 	 		 	 		 	 
		 	
		 	details.setCoaAccountNumber(glChartOfAccount.getCoaAccountNumber()); 
		 	details.setCoaDescription(glChartOfAccount.getCoaAccountDescription());
		 	details.setCoaAccountType(glChartOfAccount.getCoaAccountType());
		 	details.setCoaEnable(glChartOfAccount.getCoaEnable() == EJBCommon.TRUE ? true : false);

		 	list.add(details);
		 	
		 }
		 
		 if (list.isEmpty()) {
		 	
		 	throw new GlobalNoRecordFoundException();
		 	
		 }		 
		 		 
		 return list;
	  	  
	  } catch (GlobalAccountNumberInvalidException ex) {
	  	
	  	  throw ex;
	  	  
	  } catch (GlobalNoRecordFoundException ex) {
	  	
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	
	  	  Debug.printStackTrace(ex);
      	  throw new EJBException(ex.getMessage());
	  	
	  }

      

   }   
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

      Debug.print("GlRepChartOfAccountListControllerBean getAdCompany");      
      
      LocalAdCompanyHome adCompanyHome = null;
            
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try {
          
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         
         AdCompanyDetails details = new AdCompanyDetails();
         details.setCmpName(adCompany.getCmpName());
         
         return details;  	
      	       	
      } catch (Exception ex) {
      	
      	 Debug.printStackTrace(ex);
      	 throw new EJBException(ex.getMessage());
      	  
      }
      
   }
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGenSgAll(Integer AD_CMPNY) {
   	
      Debug.print("GlRepChartOfAccountListControllerBean getGenSgAll");      
      
      LocalAdCompanyHome adCompanyHome = null;
      LocalGenSegmentHome genSegmentHome = null;
      
      ArrayList list = new ArrayList();
            
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	       genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try {
          
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         
         Collection genSegments = genSegmentHome.findByFlCode(adCompany.getGenField().getFlCode(), AD_CMPNY);
         
         Iterator i = genSegments.iterator();
         
         while (i.hasNext()) {
         	
         	LocalGenSegment genSegment = (LocalGenSegment)i.next();
         	
         	GenModSegmentDetails mdetails = new GenModSegmentDetails();
         	mdetails.setSgMaxSize(genSegment.getSgMaxSize());
         	mdetails.setSgFlSegmentSeparator(genSegment.getGenField().getFlSegmentSeparator());
         	
         	
         	list.add(mdetails);
         	
         }

      	 return list;
      	 
      } catch (Exception ex) {
      	
      	 Debug.printStackTrace(ex);
      	 throw new EJBException(ex.getMessage());
      	  
      }
      
   }
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/   
   public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
   	throws GlobalNoRecordFoundException{
       
       Debug.print("AdRepBankAccountListControllerBean getAdBrResAll");
       
       LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
       LocalAdBranchHome adBranchHome = null;
       
       LocalAdBranchResponsibility adBranchResponsibility = null;
       LocalAdBranch adBranch = null;
       
       Collection adBranchResponsibilities = null;
       
       ArrayList list = new ArrayList();
       
       // Initialize EJB Home
       
       try {
           
           adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
           adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
           
       } catch (NamingException ex) {
           
           throw new EJBException(ex.getMessage());
           
       }
       
       try {
           
           adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
           
       } catch (FinderException ex) {
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       if (adBranchResponsibilities.isEmpty()) {
           
           throw new GlobalNoRecordFoundException();
           
       }
       
       try {
           
           Iterator i = adBranchResponsibilities.iterator();
           
           while(i.hasNext()) {
               
               adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
               
               adBranch = adBranchResponsibility.getAdBranch();
               
               AdBranchDetails details = new AdBranchDetails();
               
               details.setBrCode(adBranch.getBrCode());
               details.setBrBranchCode(adBranch.getBrBranchCode());
               details.setBrName(adBranch.getBrName());
               details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
               
               list.add(details);
               
           }	               
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       return list;
       
   }	

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlRepChartOfAccountListControllerBean ejbCreate");
      
   }  
   
}
