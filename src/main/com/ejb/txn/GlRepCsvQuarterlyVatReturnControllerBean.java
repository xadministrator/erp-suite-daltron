
/*
 * GlRepCsvQuarterlyVatReturnControllerBean.java
 *
 * Created on June 11, 2004, 9:16 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlTaxInterface;
import com.ejb.gl.LocalGlTaxInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApModSupplierDetails;
import com.util.ArModCustomerDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;


/**
 * @ejb:bean name="GlRepCsvQuarterlyVatReturnControllerEJB"
 *           display-name="Used for quarterly vat return CSV"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepCsvQuarterlyVatReturnControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepCsvQuarterlyVatReturnController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepCsvQuarterlyVatReturnControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:bill type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlRepCsvQuarterlyVatReturnControllerBean extends AbstractSessionBean {
			
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/	
	public ArrayList getCstSalesByDateRange(Date DT_FRM, Date DT_TO, Integer AD_CMPNY) {
		
		  	Debug.print("GlRepCsvQuarterlyVatReturnControllerBean getCstSalesByDateRange");
		  
		  	LocalArCustomerHome arCustomerHome = null;
		  	LocalArDistributionRecordHome arDistributionRecordHome = null;
		  	LocalArTaxCodeHome arTaxCodeHome = null;
	        LocalGlTaxInterfaceHome glTaxInterfaceHome = null;        
	        LocalGlJournalLineHome glJournalHome = null;                        
	        
	        ArrayList list = new ArrayList();
	        
	        try {
	        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
					lookUpLocalHome(LocalArCustomerHome.JNDI_NAME,LocalArCustomerHome.class);
	            arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
					lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
	            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
			  	  lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
	            glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
	        		lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);
	            glJournalHome = (LocalGlJournalLineHome)EJBHomeFactory.
					lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,LocalGlJournalLineHome.class);                        
	                                   
	        } catch (NamingException ex) 	{
	            
	            throw new EJBException(ex.getMessage());
	            
	        }
	        
	        try {	
	        	
	        	double TOTAL_ZERO_RATED_SALES = 0d;
	    	  	double TOTAL_TAXABLE_SALES = 0d;
	    	  	double TOTAL_OUTPUT_TAX = 0d;
	    	  	
	    	  	// get all available record
				
				StringBuffer jbossQl = new StringBuffer();
	  		    
	        	jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti ");
	        	
	        	boolean firstArgument = true;
	        	short ctr = 0;
	        	int criteriaSize = 0;
	        	
	        	if(! DT_FRM.equals(null)) {
	        	  	criteriaSize++;
	        	}
	        	
	        	if(! DT_TO.equals(null)) {
	        		criteriaSize++;
	        	}
	        	
	        	Object obj[] = new Object[criteriaSize];
	        	
	        	if (! DT_FRM.equals(null)) {
			      	
			  	   if (!firstArgument) {
			  	 	  jbossQl.append("AND ");
			  	   } else {
			  	 	  firstArgument = false;
			  	 	  jbossQl.append("WHERE ");
			  	   }
			  	   jbossQl.append("ti.tiTxnDate>=?" + (ctr+1) + " ");
			  	   obj[ctr] = DT_FRM;		  	   
			  	   ctr++;
			  	   
			    }  
	        	
	        	if (! DT_TO.equals(null)) {
	        		
	    		   if (!firstArgument) {
	  		  	 	  jbossQl.append("AND ");
	  		  	   } else {
	  		  	 	  firstArgument = false;
	  		  	 	  jbossQl.append("WHERE ");
	  		  	   }
	  		  	   jbossQl.append("ti.tiTxnDate<=?" + (ctr+1) + " ");
	  		  	   obj[ctr] = DT_TO;		  	   
	  		  	   ctr++;
	  		  	   
	        	}
	        	
	        	if (!firstArgument) {
		       	  	
	        		jbossQl.append("AND ");
	   	     
		   	    } else {
		   	  	
		   	  	    firstArgument = false;
		   	  	    jbossQl.append("WHERE ");
		   	  	 
		   	    }
	        	
	        	jbossQl.append("(ti.tiDocumentType='AR INVOICE' OR ti.tiDocumentType='AR CREDIT MEMO' OR " +
	    				"ti.tiDocumentType='AR RECEIPT' OR ti.tiDocumentType='GL JOURNAL') " +
	    				"AND ti.tiIsArDocument=1 AND ti.tiTcCode IS NOT NULL AND ti.tiWtcCode IS NULL AND ti.tiAdCompany=" + AD_CMPNY + 
						" ORDER BY ti.tiSlSubledgerCode");
	        	
	        	short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);
			    
			    Collection glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);
			    
			    ArrayList arCustomerList = new ArrayList();		    		  
			    		            			        		        		        	
	        	Iterator i = glTaxInterfaces.iterator();	        		        	
	        	
	        	while(i.hasNext()) {
	        		
	        		//get available records per customer
	        		
	        		LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();
	        		
	        		if (arCustomerList.contains(glTaxInterface.getTiSlSubledgerCode())) {
	        			
	        			continue;
	        			
	        		}
	        		
	        		if (glTaxInterface.getTiSlCode() == null ) {
	    				
	    				continue;
	    				
	    			}
	        		
	    			arCustomerList.add(glTaxInterface.getTiSlSubledgerCode());	    				    			
	        		        		
	        		jbossQl = new StringBuffer();
	        		
	        		jbossQl.append("SELECT DISTINCT OBJECT(ti) FROM GlTaxInterface ti " + 
	        					   "WHERE (ti.tiDocumentType='AR INVOICE' OR ti.tiDocumentType='AR CREDIT MEMO' " +
	        					   "OR ti.tiDocumentType='AR RECEIPT' OR ti.tiDocumentType='GL JOURNAL') ");        					   
	        		
	        		ctr = 0;        		        		
	        		criteriaSize = 0;
	        		String sbldgrCode = glTaxInterface.getTiSlSubledgerCode(); 
	        		
	        		if(DT_FRM != null ) { 
	        			criteriaSize++;        			
	        		}
	        		
	        		if(DT_TO != null) {
	        			criteriaSize++;        		
	        		}
	        		
	        		if(sbldgrCode != null) {
	        			criteriaSize++;        		
	        		}
	        		
	        		obj = new Object[criteriaSize];
	        		
	        		if(sbldgrCode != null) {
	        			    		       		     
	    			   jbossQl.append("AND ti.tiSlSubledgerCode = ?" + (ctr+1) + " ");
	    			   obj[ctr] = sbldgrCode;
					   ctr++;
	    			   
	        		} else {
	        			
	        			jbossQl.append("AND ti.tiSlSubledgerCode IS NULL ");
	        			
	        		}
	        		
	        		if (DT_FRM != null) {
	        		  	    		  	    		    
	    		  	   jbossQl.append("AND ti.tiTxnDate >=?" + (ctr+1) + " ");
	        		   obj[ctr] = DT_FRM;
	        		   ctr++;
	        		   
	    		    }
	        		
	        		if(DT_TO != null) {
	        			    			  
	       		  	   jbossQl.append("AND ti.tiTxnDate <=?" + (ctr+1) + " ");
	           		   obj[ctr] = DT_TO;
	           		   ctr++;
	           		   
	        		}        		        				    		    	   	    
	    	   	    
	    		    jbossQl.append("AND ti.tiIsArDocument=1 AND ti.tiTcCode IS NOT NULL AND ti.tiWtcCode IS NULL AND " + 
	    		    				"ti.tiAdCompany=" + AD_CMPNY +  " ORDER BY ti.tiTxnDate");
	    		    
	    		    double CST_ZERO_RATED_SALES = 0d;
	    	  		double CST_TAXABLE_SALES = 0d;
	    	  		double CST_OUTPUT_TAX = 0d;
	    	  		
	    	  		Collection glTaxInterfaceDocs = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(),obj);
            		
					Iterator j = glTaxInterfaceDocs.iterator();
				
					while(j.hasNext()) {
						    			    
						LocalGlTaxInterface glTaxInterfaceDoc = (LocalGlTaxInterface) j.next();
												
						LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName("ZERO-RATED",AD_CMPNY);
						
						//zero-rated
						if(glTaxInterfaceDoc.getTiTcCode().equals(arTaxCode.getTcCode())) {
							
							if(glTaxInterfaceDoc.getTiDocumentType().equals("GL JOURNAL")) {
								
								LocalGlJournalLine glJournal = glJournalHome.findByPrimaryKey(
													glTaxInterfaceDoc.getTiTxlCode());
								
								if(glJournal.getJlDebit() == EJBCommon.TRUE) {
									
									CST_ZERO_RATED_SALES -= glTaxInterfaceDoc.getTiNetAmount();
									
								} else {
									
									CST_ZERO_RATED_SALES += glTaxInterfaceDoc.getTiNetAmount();
									
								}
									
							} else {
								
								LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.findByPrimaryKey(
															glTaxInterface.getTiTxlCode());
								
								if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
									
									CST_ZERO_RATED_SALES -= glTaxInterfaceDoc.getTiNetAmount();
									
								} else {
									
									CST_ZERO_RATED_SALES += glTaxInterfaceDoc.getTiNetAmount();
									
								}
								
							}
							
						} else { //exclusive,inclusive
							
							if(glTaxInterfaceDoc.getTiDocumentType().equals("GL JOURNAL")) {
								
								LocalGlJournalLine glJournal = glJournalHome.findByPrimaryKey(
																	glTaxInterfaceDoc.getTiTxlCode());
								
								if(glJournal.getJlDebit() == EJBCommon.TRUE) {
									
									CST_TAXABLE_SALES -= glTaxInterfaceDoc.getTiNetAmount();									
									CST_OUTPUT_TAX -= glJournal.getJlAmount();
									
								}  else {
									
									CST_TAXABLE_SALES += glTaxInterfaceDoc.getTiNetAmount();									
									CST_OUTPUT_TAX += glJournal.getJlAmount();
								}
							} else {
								
								LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.findByPrimaryKey(
																			glTaxInterfaceDoc.getTiTxlCode());
								
								if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
									
									CST_TAXABLE_SALES -= glTaxInterfaceDoc.getTiNetAmount();
									CST_OUTPUT_TAX -= arDistributionRecord.getDrAmount();
									
								} else {
									
									CST_TAXABLE_SALES += glTaxInterfaceDoc.getTiNetAmount();
									CST_OUTPUT_TAX += arDistributionRecord.getDrAmount();
									
								}
 								
							}
							
						}
					}
																																														
					TOTAL_ZERO_RATED_SALES += CST_ZERO_RATED_SALES;
					TOTAL_TAXABLE_SALES += CST_TAXABLE_SALES;
					TOTAL_OUTPUT_TAX += CST_OUTPUT_TAX;
					
					ArModCustomerDetails details = new ArModCustomerDetails();
					
					LocalArCustomer arCustomer = arCustomerHome.findByPrimaryKey(
												glTaxInterface.getTiSlCode());																		
					
			  		details.setCstTin(arCustomer.getCstTin());
			  		details.setCstName(arCustomer.getCstName());
			  		details.setCstAddress(arCustomer.getCstAddress());
			  		details.setCstCity(arCustomer.getCstCity());
			  		details.setCstStateProvince(arCustomer.getCstStateProvince());
			  		details.setCstCountry(arCustomer.getCstCountry());
			  		
			  		details.setCstZeroRatedSales(CST_ZERO_RATED_SALES);
			  		details.setCstTotalZeroRatedSales(TOTAL_ZERO_RATED_SALES);
			  		details.setCstTaxableSales(CST_TAXABLE_SALES);
			  		details.setCstTotalTaxableSales(TOTAL_TAXABLE_SALES);
			  		details.setCstOutputTax(CST_OUTPUT_TAX);	
			  		details.setCstTotalOutputTax(TOTAL_OUTPUT_TAX);
			  		
			  		list.add(details); 			  					 	
					
	        	}
	    		    
	        	return list;
	        	
	        } catch (Exception ex) {
	    	  	
	   	  	 Debug.printStackTrace(ex);
	   	  	 throw new EJBException(ex.getMessage());
	   	  	  
	   	    }	   	  		  
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public ArrayList getSplPurchasesByDateRange(Date DT_FRM, Date DT_TO, Integer AD_CMPNY) {
		
		  Debug.print("GlRepCsvQuarterlyVatReturnControllerBean getSplPurchasesByDateRange");
		  
		  LocalApTaxCodeHome apTaxCodeHome = null;
		  LocalApDistributionRecordHome apDistributionRecordHome = null;
		  LocalApSupplierHome apSupplierHome = null;
		  LocalGlTaxInterfaceHome glTaxInterfaceHome = null;        
		  LocalGlJournalLineHome glJournalHome = null;		  
		  
		  ArrayList list = new ArrayList();
		  
		  try {
	          
		  	  apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			  	  lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
		      apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				  lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
		      apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			  	  lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
		      glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
				  lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);
		      glJournalHome = (LocalGlJournalLineHome)EJBHomeFactory.
				  lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,LocalGlJournalLineHome.class);                        
			                           
		  } catch (NamingException ex) 	{
		    
		    throw new EJBException(ex.getMessage());
		    
		  }  
		  
		  try {		  
			  double TOTAL_SPL_ZR_RTD_PRCHS = 0d;
			  double TOTAL_SPL_INPT_TX = 0d;
			  
		  	  //get all available record
			
			  StringBuffer jbossQl = new StringBuffer();
  		    
        	  jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti ");
        	
        	  boolean firstArgument = true;
        	  short ctr = 0;
        	  int criteriaSize = 0;
        	
        	  if(! DT_FRM.equals(null)) {
        	  	  criteriaSize++;
        	  }
        	
        	  if(! DT_TO.equals(null)) {
        		  criteriaSize++;
        	  }
        	
        	  Object obj[] = new Object[criteriaSize];
        	
        	  if (! DT_FRM.equals(null)) {
		      	
		  	     if (!firstArgument) {
		  	 	    jbossQl.append("AND ");
		  	     } else {
		  	 	    firstArgument = false;
		  	 	    jbossQl.append("WHERE ");
		  	     }
		  	     jbossQl.append("ti.tiTxnDate>=?" + (ctr+1) + " ");
		  	     obj[ctr] = DT_FRM;		  	   
		  	     ctr++;
		  	   
		      }  
        	
        	  if (! DT_TO.equals(null)) {
        		
    		     if (!firstArgument) {
  		  	 	    jbossQl.append("AND ");
  		  	     } else {
  		  	 	    firstArgument = false;
  		  	 	    jbossQl.append("WHERE ");
  		  	     }
  		  	     jbossQl.append("ti.tiTxnDate<=?" + (ctr+1) + " ");
  		  	     obj[ctr] = DT_TO;		  	   
  		  	     ctr++;
  		  	   
        	  }
        	
        	  if (!firstArgument) {
	       	  	
        		  jbossQl.append("AND ");
   	     
	   	      } else {
	   	  	
	   	  	     firstArgument = false;
	   	  	     jbossQl.append("WHERE ");
	   	  	 
	   	      }
        	
        	  jbossQl.append("(ti.tiDocumentType='AP VOUCHER' OR ti.tiDocumentType='AP DEBIT MEMO' OR " +
    				"ti.tiDocumentType='AP CHECK' OR ti.tiDocumentType='AP RECEIVING ITEM' OR " +
    				"ti.tiDocumentType='GL JOURNAL') AND ti.tiIsArDocument=0 " +
    				"AND ti.tiTcCode IS NOT NULL AND ti.tiWtcCode IS NULL" +
					" AND ti.tiAdCompany=" + AD_CMPNY +	" ORDER BY ti.tiSlSubledgerCode");
        	
        	  short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);
			    
        	  Collection glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);
        	  
        	  ArrayList apSupplierList = new ArrayList();		        	          	          	          	  
        	  
        	  Iterator i = glTaxInterfaces.iterator();
        	 
        	  while(i.hasNext()) {
        	 	
        	  		//get available record per supplier
        	  	
        	  		LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();
        	  		
        	  		if (apSupplierList.contains(glTaxInterface.getTiSlSubledgerCode())) {
            			
            			continue;
            			
            		}   
        	  		
        	  		if (glTaxInterface.getTiSlCode() == null ) {
	    				
	    				continue;
	    				
	    			}
            			        		
        			apSupplierList.add(glTaxInterface.getTiSlSubledgerCode());
        			
        			jbossQl = new StringBuffer();
            		
            		jbossQl.append("SELECT DISTINCT OBJECT(ti) FROM GlTaxInterface ti " + 
            					   "WHERE (ti.tiDocumentType='AP VOUCHER' OR ti.tiDocumentType='AP DEBIT MEMO' " +
            					   "OR ti.tiDocumentType='AP CHECK' OR ti.tiDocumentType='AP RECEIVING ITEM' OR " +
            					   "ti.tiDocumentType='GL JOURNAL') ");
            		
            		ctr = 0;        		        		
            		criteriaSize = 0;
            		String sbldgrCode = glTaxInterface.getTiSlSubledgerCode(); 
            		
            		if(DT_FRM != null ) { 
            			criteriaSize++;        			
            		}
            		
            		if(DT_TO != null) {
            			criteriaSize++;        		
            		}
            		
            		if(sbldgrCode != null) {
            			criteriaSize++;        		
            		}
            		
            		obj = new Object[criteriaSize];
            		
            		if(sbldgrCode != null) {
            			    		       		     
        			   jbossQl.append("AND ti.tiSlSubledgerCode = ?" + (ctr+1) + " ");
        			   obj[ctr] = sbldgrCode;
    				   ctr++;
        			   
            		} else {
            			
            			jbossQl.append("AND ti.tiSlSubledgerCode IS NULL ");
            			
            		}
            		
            		if (DT_FRM != null) {
            		  	    		  	    		    
        		  	   jbossQl.append("AND ti.tiTxnDate >=?" + (ctr+1) + " ");
            		   obj[ctr] = DT_FRM;
            		   ctr++;
            		   
        		    }
            		
            		if(DT_TO != null) {
            			    			  
           		  	   jbossQl.append("AND ti.tiTxnDate <=?" + (ctr+1) + " ");
               		   obj[ctr] = DT_TO;
               		   ctr++;
               		   
            		}        		        				    		    	   	    
        	   	    
        		    jbossQl.append("AND ti.tiIsArDocument=0 AND ti.tiTcCode IS NOT NULL AND ti.tiWtcCode IS NULL AND " + 
        		    				"ti.tiAdCompany=" + AD_CMPNY +  " ORDER BY ti.tiTxnDate");
        	  		
        		    double SPL_ZR_RTD_PRCHS = 0d;
        		    double SPL_INPT_TX = 0d;
        		    
        		    Collection glTaxInterfaceDocs = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(),obj);
            		
					Iterator j = glTaxInterfaceDocs.iterator();
				
					while(j.hasNext()) {
						
						LocalGlTaxInterface glTaxInterfaceDoc = (LocalGlTaxInterface) j.next();
																		
						LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName("ZERO-RATED",AD_CMPNY);
						
						//zero-rated
						if(glTaxInterfaceDoc.getTiTcCode().equals(apTaxCode.getTcCode())) {
							
							if(glTaxInterfaceDoc.getTiDocumentType().equals("GL JOURNAL")) {
								
								LocalGlJournalLine glJournal = glJournalHome.findByPrimaryKey(
													glTaxInterfaceDoc.getTiTxlCode());	
								
								if(glJournal.getJlDebit() == EJBCommon.TRUE) {									
									
									SPL_ZR_RTD_PRCHS += glTaxInterfaceDoc.getTiNetAmount();
									
								} else {									
									
									SPL_ZR_RTD_PRCHS -= glTaxInterfaceDoc.getTiNetAmount();
									
								}
								
							} else {
								
								LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.findByPrimaryKey(
																	glTaxInterfaceDoc.getTiTxlCode());
								
								if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
									
									SPL_ZR_RTD_PRCHS +=  glTaxInterfaceDoc.getTiNetAmount();
									
								} else {
									
									SPL_ZR_RTD_PRCHS -= glTaxInterfaceDoc.getTiNetAmount();
									
								}
								
							}																
						
						} else { //inclusive/exclusive
							
							if(glTaxInterfaceDoc.getTiDocumentType().equals("GL JOURNAL")) {
								
								LocalGlJournalLine glJournal = glJournalHome.findByPrimaryKey(
													glTaxInterfaceDoc.getTiTxlCode());
								
								if(glJournal.getJlDebit() == EJBCommon.TRUE) {
									
									SPL_INPT_TX += glJournal.getJlAmount();
									
								} else {
									
									SPL_INPT_TX -= glJournal.getJlAmount();
									 
								}
								
							} else {
								
								LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.findByPrimaryKey(
																	glTaxInterfaceDoc.getTiTxlCode());
								
								if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
									
									SPL_INPT_TX +=  apDistributionRecord.getDrAmount();
									
								} else {
									
									SPL_INPT_TX -= apDistributionRecord.getDrAmount();
									
								}
								
							}	
							
						}
						
						
					}
																														
					TOTAL_SPL_ZR_RTD_PRCHS += SPL_ZR_RTD_PRCHS;
					TOTAL_SPL_INPT_TX += SPL_INPT_TX;
					
					ApModSupplierDetails details = new ApModSupplierDetails();
						
					LocalApSupplier apSupplier = apSupplierHome.findByPrimaryKey(
												 glTaxInterface.getTiSlCode());
					
					details.setSplTin(apSupplier.getSplTin());
			  		details.setSplName(apSupplier.getSplName());
			  		details.setSplAddress(apSupplier.getSplAddress());
			  		details.setSplCity(apSupplier.getSplCity());
			  		details.setSplStateProvince(apSupplier.getSplStateProvince());
			  		details.setSplCountry(apSupplier.getSplCountry());
			  		
			  		details.setSplZeroRatedPurchase(SPL_ZR_RTD_PRCHS);
			  		details.setSplInputTax(SPL_INPT_TX);			  		
			  		details.setSplTotalZeroRatedPurchase(TOTAL_SPL_ZR_RTD_PRCHS);
			  		details.setSplTotalInputTax(TOTAL_SPL_INPT_TX);
			  					  		
			  		list.add(details);
			  						 																				  					  			  		  
        	  }
        	          	          	 
        	  return list;
        	  
		  } catch (Exception ex) {
		  	
		  	Debug.printStackTrace(ex);
		  	throw new EJBException(ex.getMessage());
		  	
		  }
		  
	}
	
	/**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getAdPrfApJournalLineNumber(Integer AD_CMPNY) {

       Debug.print("GlRepCsvQuarterlyVatReturnControllerBean getAdPrfApJournalLineNumber");
                  
       LocalAdPreferenceHome adPreferenceHome = null;
      
      
       // Initialize EJB Home
        
       try {
            
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
       } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
       }
      

       try {
      	
          LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
         
          return adPreference.getPrfApJournalLineNumber();
         
       } catch (Exception ex) {
        	 
          Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
         
       }
      
    }
		
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("GlRepCsvQuarterlyVatReturnControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpTin(adCompany.getCmpTin());
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpZip(adCompany.getCmpZip());
	     details.setCmpIndustry(adCompany.getCmpIndustry());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
   // private
	
	private short getGlFcPrecisionUnit(Integer AD_CMPNY) {
	      
	       LocalAdCompanyHome adCompanyHome = null;
	      
	     
	       // Initialize EJB Home
	        
	       try {
	            
	           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
	            
	       } catch (NamingException ex) {
	            
	           throw new EJBException(ex.getMessage());
	            
	       }

	       try {
	       	
	         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	            
	         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
	                                     
	       } catch (Exception ex) {
	       	 
	       	 Debug.printStackTrace(ex);
	         throw new EJBException(ex.getMessage());
	         
	       }

   }	
	   
   private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	  
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT * CONVERSION_RATE;
             	
         } else if (CONVERSION_DATE != null) {
         	
         	 try {
         	 	    
         	 	 // Get functional currency rate
         	 	 
         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
         	     
         	     if (!FC_NM.equals("USD")) {
         	     	
        	         glReceiptFunctionalCurrencyRate = 
	     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
	     	             CONVERSION_DATE, AD_CMPNY);
	     	             
	     	         AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();
	     	             
         	     }
                 
                 // Get set of book functional currency rate if necessary
         	 	         
                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
                 
                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                             getFcCode(), CONVERSION_DATE, AD_CMPNY);
                     
                     AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();
                 
                  }
                                 
         	             
         	 } catch (Exception ex) {
         	 	
         	 	throw new EJBException(ex.getMessage());
         	 	
         	 }       
         	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}	
            
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlRepQuarterlyVatReturnControllerBean ejbCreate");
      
    }
}
