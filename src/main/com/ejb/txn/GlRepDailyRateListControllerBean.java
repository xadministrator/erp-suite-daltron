/*
 * GlRepDailyRateListControllerBean.java
 *
 * Created on July 28, 2006 07:38 PM
 *
 * @author  Farrah S. Garing
 */
package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlRepDailyRateListDetails;

/**
 * @ejb:bean name="GlRepDailyRateListControllerEJB"
 *           display-name="Used for viewing daily rate lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepDailyRateListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepDailyRateListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepDailyRateListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 */
public class GlRepDailyRateListControllerBean extends AbstractSessionBean {
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("GlRepDailyRateListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlFcAll(Integer AD_CMPNY) throws GlFCNoFunctionalCurrencyFoundException {
		
		Debug.print("GlRepDailyRateListControllerBean getGlFcAll");

		ArrayList fcAllList = new ArrayList();
		Collection glFunctionalCurrencies = null;
		
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
					new Date(EJBCommon.getGcCurrentDateWoTime().getTime().getTime()), AD_CMPNY);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (glFunctionalCurrencies.size() == 0)
			throw new GlFCNoFunctionalCurrencyFoundException();
		
		Iterator i = glFunctionalCurrencies.iterator();
		
		while (i.hasNext()) {
			
			LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency) i.next();

			fcAllList.add(glFunctionalCurrency.getFcName());
			
		}
		
		return fcAllList;
		
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public ArrayList executeDailyRateList(HashMap criteria, Integer AD_CMPNY)
		throws GlobalNoRecordFoundException{
		
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		
		try {
			
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try{
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(fr) FROM GlFunctionalCurrencyRate fr ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	 
			Object obj[];
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("dateFrom") ){
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("fr.frDate >= ?" + (ctr+1) + " ");
				obj[ctr] = EJBCommon.convertStringToSQLDate((String)criteria.get("dateFrom"));
				ctr++;

			}
			
			if (criteria.containsKey("dateTo") ){
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}	     
				
				jbossQl.append("fr.frDate <= ?" + (ctr+1) + " ");
				obj[ctr] = EJBCommon.convertStringToSQLDate((String)criteria.get("dateTo"));
				ctr++;

			}
			
			if (criteria.containsKey("currency") ){
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("fr.glFunctionalCurrency.fcName = ?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("currency");
				ctr++;

			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}	     
			
			jbossQl.append("fr.frAdCompany=" + AD_CMPNY + " ORDER BY fr.glFunctionalCurrency.fcName");

			Collection glFunctionalCurrencyRates = glFunctionalCurrencyRateHome.getFrByCriteria(jbossQl.toString(), obj);
			Iterator i = glFunctionalCurrencyRates.iterator();
			ArrayList list = new ArrayList();
			
			while(i.hasNext()) {
				
				LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = (LocalGlFunctionalCurrencyRate) i.next();
				
				GlRepDailyRateListDetails mdetails = new GlRepDailyRateListDetails();
				
				mdetails.setDrFrConversionToUSD(glFunctionalCurrencyRate.getFrXToUsd());
				mdetails.setDrFrDate(glFunctionalCurrencyRate.getFrDate());
				mdetails.setDrFrFuncationalCurrrencyName(
						glFunctionalCurrencyRate.getGlFunctionalCurrency().getFcName());
				
				mdetails.setDrInverseRate(1/glFunctionalCurrencyRate.getFrXToUsd());

				list.add(mdetails);
				
			}

			if (list.isEmpty())
				throw new GlobalNoRecordFoundException();
			
			return list;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlRepDailyRateListControllerBean ejbCreate");
      
    }
    
}
