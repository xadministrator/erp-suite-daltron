package com.ejb.txn;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GenModSegmentDetails;
import com.util.GlModAccountingCalendarValueDetails;
import com.util.GlRepDetailBalanceSheetDetails;
import com.util.GlRepDetailTrialBalanceDetails;

/**
 * @ejb:bean name="GlRepDetailBalanceSheetControllerEJB"
 *           display-name="Used for generation of balance sheet reports"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepDetailBalanceSheetControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepDetailBalanceSheetController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepDetailBalanceSheetControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
 */

public class GlRepDetailBalanceSheetControllerBean extends AbstractSessionBean {


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlReportableAcvAll(Integer AD_CMPNY) {

		Debug.print("GlRepDetailBalanceSheetControllerBean getGlReportableAcvAll");      

		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);

			Iterator i = glSetOfBooks.iterator();

			while (i.hasNext()) {

				LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();

				Collection glAccountingCalendarValues = 
					glAccountingCalendarValueHome.findReportableAcvByAcCodeAndAcvStatus(
							glSetOfBook.getGlAccountingCalendar().getAcCode(), 'O', 'C', 'P', AD_CMPNY);

				Iterator j = glAccountingCalendarValues.iterator();

				while (j.hasNext()) {

					LocalGlAccountingCalendarValue glAccountingCalendarValue = 
						(LocalGlAccountingCalendarValue)j.next();

					GlModAccountingCalendarValueDetails mdetails = new GlModAccountingCalendarValueDetails();

					mdetails.setAcvPeriodPrefix(glAccountingCalendarValue.getAcvPeriodPrefix());

					// get year

					GregorianCalendar gc = new GregorianCalendar();
					gc.setTime(glAccountingCalendarValue.getAcvDateTo());

					mdetails.setAcvYear(gc.get(Calendar.YEAR));

					// is current

					gc = EJBCommon.getGcCurrentDateWoTime();

					if ((glAccountingCalendarValue.getAcvDateFrom().before(gc.getTime()) ||
							glAccountingCalendarValue.getAcvDateFrom().equals(gc.getTime())) &&
							(glAccountingCalendarValue.getAcvDateTo().after(gc.getTime()) ||
									glAccountingCalendarValue.getAcvDateTo().equals(gc.getTime()))) {

						mdetails.setAcvCurrent(true);

					}

					list.add(mdetails);

				}

			}

			return list;


		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeSpGlRepDetailBalanceSheet(ResultSet rs, String AMNT_TYP, String DBS_ACCNT_NMBR_FRM, String DBS_ACCNT_NMBR_TO, String DBS_PRD, int DBS_YR,
			boolean DBS_INCLD_UNPSTD, boolean DBS_INCLD_UNPSTD_SL, boolean DBS_SHW_ZRS, ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

		Debug.print("GlRepDetailBalanceSheetControllerBean executeSpGlRepDetailBalanceSheet");


		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalApVoucherHome apVoucherHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);    
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);    
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);    
			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);    
			invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);    
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);    
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);    
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(DBS_PRD, EJBCommon.getIntendedDate(DBS_YR), AD_CMPNY);
			
			ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);    
	
			LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
			LocalGenField genField = adCompany.getGenField();
			Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
			String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
			int genNumberOfSegment = genField.getFlNumberOfSegment();

			       

			StringTokenizer stAccountFrom = new StringTokenizer(DBS_ACCNT_NMBR_FRM, strSeparator);
			StringTokenizer stAccountTo = new StringTokenizer(DBS_ACCNT_NMBR_TO, strSeparator);

			// validate if account number is valid

			if (stAccountFrom.countTokens() != genNumberOfSegment || 
					stAccountTo.countTokens() != genNumberOfSegment) {

				throw new GlobalAccountNumberInvalidException();

			}

			double NET_INCOME = 0;
			double TOTAL_ASSET = 0;
			double TOTAL_LIABILITY = 0;
			double TOTAL_EQUITY = 0;
			
			while (rs.next()) {
				
				GlRepDetailBalanceSheetDetails details = new GlRepDetailBalanceSheetDetails();
			
				String ACCNT_NMBR = rs.getString("ACCNT_NMBR");
				String ACCNT_TYP = rs.getString("ACCNT_TYP"); 
				String ACCNT_DESC = rs.getString("ACCNT_DESC");
				Double DEBIT = rs.getDouble("DEBIT");
				Double CREDIT = rs.getDouble("CREDIT");
				Double BALANCE = rs.getDouble("BALANCE");
				
				
				System.out.println("ACCNT_NMBR="+ACCNT_NMBR + ": "+BALANCE);
				
				
				
				double COA_BEG = 0d;
			      
			      LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue =
		 			glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
		 					glSetOfBook.getGlAccountingCalendar().getAcCode(),
						(short)1, AD_CMPNY);
			      
			      LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(ACCNT_NMBR, AD_CMPNY);
			      
			      
			      LocalGlChartOfAccountBalance glBeginningChartOfAccountBalance = 
				 			glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
				 					glBeginningAccountingCalendarValue.getAcvCode(),
								glChartOfAccount.getCoaCode(), AD_CMPNY);
					      
			      if (AMNT_TYP.equals("YTD")) {
			     
			       COA_BEG = glBeginningChartOfAccountBalance.getCoabBeginningBalance();
			       BALANCE += COA_BEG;
			    
			      }
			      
				details.setDbsAccountNumber(ACCNT_NMBR);
				details.setDbsAccountDescription(ACCNT_DESC);
				details.setDbsAccountType(ACCNT_TYP);
				details.setDbsBalance(BALANCE);
				
				
				if(ACCNT_TYP.equals("ASSET")) {
					TOTAL_ASSET += BALANCE;
				}
				else if(ACCNT_TYP.equals("LIABILITY")) {
					TOTAL_LIABILITY += BALANCE;
				}
				else if(ACCNT_TYP.equals("OWNERS EQUITY")) {
					TOTAL_EQUITY += BALANCE;
				}
				
				
				list.add(details);

			}
			
			if (list.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}
			
			GlRepDetailBalanceSheetDetails details = new GlRepDetailBalanceSheetDetails();
			details.setDbsAccountNumber("");
			details.setDbsAccountDescription("NET INCOME");
			details.setDbsAccountType("OWNERS EQUITY");
			
			NET_INCOME  =TOTAL_ASSET-(TOTAL_LIABILITY+TOTAL_EQUITY);
			details.setDbsBalance(NET_INCOME);
			
			list.add(details);
			

			
			return list;

		} catch (GlobalAccountNumberInvalidException ex) {

			throw ex;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}



	}
	

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeGlRepDetailBalanceSheet(String DBS_ACCNT_NMBR_FRM, String DBS_ACCNT_NMBR_TO, String DBS_PRD, int DBS_YR,
			boolean DBS_INCLD_UNPSTD, boolean DBS_INCLD_UNPSTD_SL, boolean DBS_SHW_ZRS, ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

		Debug.print("GlRepDetailBalanceSheetControllerBean executeGlRepDetailBalanceSheet");


		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalApVoucherHome apVoucherHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);    
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);    
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);    
			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);    
			invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);    
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);    
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);    
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(DBS_PRD, EJBCommon.getIntendedDate(DBS_YR), AD_CMPNY);
			String PERIOD = "";
			if (DBS_PRD.equals("JAN"))
				PERIOD = "DEC";
			else if (DBS_PRD.equals("FEB"))
				PERIOD = "JAN";
			else if (DBS_PRD.equals("MAR"))
				PERIOD = "FEB";
			else if (DBS_PRD.equals("APR"))
				PERIOD = "MAR";
			else if (DBS_PRD.equals("MAY"))
				PERIOD = "APR";
			else if (DBS_PRD.equals("JUN"))
				PERIOD = "MAY";
			else if (DBS_PRD.equals("JUL"))
				PERIOD = "JUN";
			else if (DBS_PRD.equals("AUG"))
				PERIOD = "JUL";
			else if (DBS_PRD.equals("SEP"))
				PERIOD = "AUG";
			else if (DBS_PRD.equals("OCT"))
				PERIOD = "SEP";
			else if (DBS_PRD.equals("NOV"))
				PERIOD = "OCT";
			else if (DBS_PRD.equals("DEC"))
				PERIOD = "NOV";
			Collection glSetOfBooks1 = glSetOfBookHome.findByAcvPeriodPrefixAndDate(PERIOD, EJBCommon.getIntendedDate(DBS_YR), AD_CMPNY);
			ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);    
			ArrayList glSetOfBookList1 = new ArrayList(glSetOfBooks1);
			LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
			LocalGlSetOfBook glSetOfBook1 = (LocalGlSetOfBook)glSetOfBookList1.get(0);
			LocalGenField genField = adCompany.getGenField();
			Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
			String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
			int genNumberOfSegment = genField.getFlNumberOfSegment();

			// get coa selected

			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT DISTINCT OBJECT(coa) FROM GlChartOfAccount coa ");

			// add branch criteria

			if (branchList.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}
			else {

				jbossQl.append(", in (coa.adBranchCoas) bcoa WHERE bcoa.adBranch.brCode in (");

				boolean firstLoop = true;

				Iterator j = branchList.iterator();

				while(j.hasNext()) {

					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}

					AdBranchDetails mdetails = (AdBranchDetails) j.next();

					jbossQl.append(mdetails.getBrCode());

				}

				jbossQl.append(") AND ");

			}          

			StringTokenizer stAccountFrom = new StringTokenizer(DBS_ACCNT_NMBR_FRM, strSeparator);
			StringTokenizer stAccountTo = new StringTokenizer(DBS_ACCNT_NMBR_TO, strSeparator);

			// validate if account number is valid

			if (stAccountFrom.countTokens() != genNumberOfSegment || 
					stAccountTo.countTokens() != genNumberOfSegment) {

				throw new GlobalAccountNumberInvalidException();

			}

			try {

				String var = "1";

				if (genNumberOfSegment > 1) {

					for (int i=0; i<genNumberOfSegment; i++) {

						if (i == 0 && i < genNumberOfSegment - 1) {

							// for first segment

							jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");

							var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)+1 ";


						} else if (i != 0 && i < genNumberOfSegment - 1){     		

							// for middle segments

							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")) - (" + var +")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");

							var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";

						} else if (i != 0 && i == genNumberOfSegment - 1) {

							// for last segment

							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' ");


						}	     		       	      	   	            

					}

				} else if(genNumberOfSegment == 1) {

					String accountFrom = stAccountFrom.nextToken();
					String accountTo = stAccountTo.nextToken();

					jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + accountFrom + "' AND '" + accountTo + "' OR " +
							"coa.coaAccountNumber BETWEEN '" + accountFrom + "' AND '" + accountTo + "' ");

				}

				jbossQl.append("AND coa.coaEnable=1 AND coa.coaAdCompany=" + AD_CMPNY + " ");

			} catch (NumberFormatException ex) {

				throw new GlobalAccountNumberInvalidException();

			}

			// generate order by coa natural account

			short accountSegmentNumber = 0;

			try {

				LocalGenSegment genSegment = genSegmentHome.findByFlCodeAndSegmentType(genField.getFlCode(), 'N', AD_CMPNY);
				accountSegmentNumber = genSegment.getSgSegmentNumber();

			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}


			jbossQl.append("ORDER BY coa.coaSegment" + accountSegmentNumber + ", coa.coaAccountNumber ");

			Object obj[] = new Object[0];

			Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), obj);

			Iterator i = glChartOfAccounts.iterator();

			double DBS_NT_INCM = 0d;
			double DBS_NT_INCM1 = 0d;
			double DBS_NT_INCM_PREV = 0d;
			double DBS_NT_INCM_PREV1 = 0d;
			double AssetAmount = 0d;
			double LiabilityAmount = 0d;
			double OwnerAmount = 0d;
			while (i.hasNext()) {

				LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)i.next();

				GlRepDetailBalanceSheetDetails details = new GlRepDetailBalanceSheetDetails();		 			 	 	 	 

				double COA_BLNC = 0d;
				
				double COA_BLNC_PREV = 0d;
				double COA_BLNC_LST_MNTH = 0d;

				details.setDbsAccountNumber(glChartOfAccount.getCoaAccountNumber());  	
				details.setDbsAccountDescription(glChartOfAccount.getCoaAccountDescription());
				details.setDbsAccountType(glChartOfAccount.getCoaAccountType());

				// get coa debit or credit	in coa balance		 			    		 	 		 	 	 	 
				// for present

				LocalGlAccountingCalendarValue glAccountingCalendarValue =
					glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
							glSetOfBook.getGlAccountingCalendar().getAcCode(),
							DBS_PRD, AD_CMPNY);
				System.out.println("DBS_PRD: " + DBS_PRD);
				System.out.println("getAcvCode: " + glAccountingCalendarValue.getAcvCode());
				LocalGlChartOfAccountBalance glChartOfAccountBalance = 
					glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
							glAccountingCalendarValue.getAcvCode(),
							glChartOfAccount.getCoaCode(), AD_CMPNY);

				// for last month

				LocalGlAccountingCalendarValue glAccountingCalendarValue1 =
					glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
							glSetOfBook1.getGlAccountingCalendar().getAcCode(),
							PERIOD, AD_CMPNY);
				//System.out.println("PERIOD: " + PERIOD);
				LocalGlChartOfAccountBalance glChartOfAccountBalance1 = 
					glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
							glAccountingCalendarValue1.getAcvCode(),
							glChartOfAccount.getCoaCode(), AD_CMPNY);

				COA_BLNC = glChartOfAccountBalance.getCoabEndingBalance();
				COA_BLNC_LST_MNTH = glChartOfAccountBalance1.getCoabEndingBalance();
				//System.out.println("1: " + COA_BLNC);
				//System.out.println("2: " + COA_BLNC_LST_MNTH);
				// get coa debit or credit balance in unposted journals if necessary

				LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue =
					glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
							glSetOfBook.getGlAccountingCalendar().getAcCode(),
							(short)1, AD_CMPNY);

				LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue1 =
					glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
							glSetOfBook1.getGlAccountingCalendar().getAcCode(),
							(short)1, AD_CMPNY);

				GregorianCalendar endGc = new GregorianCalendar();
				endGc.setTime(glBeginningAccountingCalendarValue.getAcvDateFrom());
				endGc.add(Calendar.DATE, -1);

				GregorianCalendar endGc1 = new GregorianCalendar();
				endGc1.setTime(glBeginningAccountingCalendarValue1.getAcvDateFrom());
				endGc1.add(Calendar.DATE, -1);

				//System.out.println("endGc: "+endGc.getTime());

				if (DBS_INCLD_UNPSTD) {

					Collection glJournalLines = new ArrayList();
					Collection glJournalLinesPrev = new ArrayList();
					Collection glJournalLinesPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						glJournalLines = glJournalLineHome.findUnpostedJlByJrEffectiveDateAndCoaCode(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_CMPNY);

					} else {

						glJournalLines = glJournalLineHome.findUnpostedJlByJrEffectiveDateRangeAndCoaCode(
								glBeginningAccountingCalendarValue.getAcvDateFrom(), 
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_CMPNY);

						glJournalLinesPrev = glJournalLineHome.findUnpostedJlByJrEffectiveDateAndCoaCode(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_CMPNY);

						glJournalLinesPrev1 = glJournalLineHome.findUnpostedJlByJrEffectiveDateAndCoaCode(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_CMPNY);

					}

					Iterator j = glJournalLines.iterator();
					Iterator j1 = glJournalLines.iterator();

					while (j.hasNext()) {

						LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();

						LocalGlJournal glJournal = glJournalLine.getGlJournal();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								glJournal.getGlFunctionalCurrency().getFcCode(),
								glJournal.getGlFunctionalCurrency().getFcName(),
								glJournal.getJrConversionDate(),
								glJournal.getJrConversionRate(),
								glJournalLine.getJlAmount(), AD_CMPNY);
						// for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) &&
								glJournalLine.getJlDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") &&
										glJournalLine.getJlDebit() == EJBCommon.FALSE)) {

							COA_BLNC += EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						} else {

							COA_BLNC -= EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						}
						// for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) &&
								glJournalLine.getJlDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") &&
										glJournalLine.getJlDebit() == EJBCommon.FALSE)) {

							COA_BLNC_LST_MNTH += EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						} else {

							COA_BLNC_LST_MNTH -= EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						}

					}

					j = glJournalLinesPrev.iterator();
					j1 = glJournalLinesPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();

						LocalGlJournal glJournal = glJournalLine.getGlJournal();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								glJournal.getGlFunctionalCurrency().getFcCode(),
								glJournal.getGlFunctionalCurrency().getFcName(),
								glJournal.getJrConversionDate(),
								glJournal.getJrConversionRate(),
								glJournalLine.getJlAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) &&
								glJournalLine.getJlDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") &&
										glJournalLine.getJlDebit() == EJBCommon.FALSE)) {

							COA_BLNC_PREV += EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						} else {

							COA_BLNC_PREV -= EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j1.next();

						LocalGlJournal glJournal = glJournalLine.getGlJournal();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								glJournal.getGlFunctionalCurrency().getFcCode(),
								glJournal.getGlFunctionalCurrency().getFcName(),
								glJournal.getJrConversionDate(),
								glJournal.getJrConversionRate(),
								glJournalLine.getJlAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) &&
								glJournalLine.getJlDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") &&
										glJournalLine.getJlDebit() == EJBCommon.FALSE)) {

							COA_BLNC_PREV += EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						} else {

							COA_BLNC_PREV -= EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						}

					}

				}

				// include unposted subledger transactions

				if (DBS_INCLD_UNPSTD_SL) {

					// ar invoice
					Collection arINVDrs = new ArrayList();
					Collection arINVDrsPrev = new ArrayList();
					Collection arINVDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						arINVDrs = arDistributionRecordHome.findUnpostedInvByDateAndCoaAccountNumber(
								EJBCommon.FALSE, glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);

					} else {

						arINVDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
								EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						arINVDrsPrev = arDistributionRecordHome.findUnpostedInvByDateAndCoaAccountNumber(
								EJBCommon.FALSE, endGc.getTime(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);

						arINVDrsPrev1 = arDistributionRecordHome.findUnpostedInvByDateAndCoaAccountNumber(
								EJBCommon.FALSE, endGc1.getTime(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);

					}

					Iterator j = arINVDrs.iterator();
					Iterator j1 = arINVDrs.iterator();

					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(),
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);
						// for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						// for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}

					}

					j = arINVDrsPrev.iterator();
					j1 = arINVDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(),
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j1.next();

						LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(),
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// ar credit memo
					Collection arCMDrs = new ArrayList();	
					Collection arCMDrsPrev = new ArrayList();
					Collection arCMDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						arCMDrs = arDistributionRecordHome.findUnpostedInvByDateAndCoaAccountNumber(
								EJBCommon.TRUE, glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);	

					} else {

						arCMDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
								EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

						arCMDrsPrev = arDistributionRecordHome.findUnpostedInvByDateAndCoaAccountNumber(
								EJBCommon.TRUE, endGc.getTime(), glChartOfAccount.getCoaCode(),
								AD_BRNCH, AD_CMPNY);	

						arCMDrsPrev1 = arDistributionRecordHome.findUnpostedInvByDateAndCoaAccountNumber(
								EJBCommon.TRUE, endGc1.getTime(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);

					}

					j = arCMDrs.iterator();
					j1 = arCMDrs.iterator();


					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArInvoice arCreditMemo = arDistributionRecord.getArInvoice();

						LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
								arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(), 
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}

					}

					j = arCMDrsPrev.iterator();
					j1 = arCMDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArInvoice arCreditMemo = arDistributionRecord.getArInvoice();

						LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
								arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(), 
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for current month
					while (j1.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j1.next();

						LocalArInvoice arCreditMemo = arDistributionRecord.getArInvoice();

						LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
								arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(), 
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// ar receipt
					Collection arRCTDrs = new ArrayList();
					Collection arRCTDrsPrev = new ArrayList();
					Collection arRCTDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						arRCTDrs = arDistributionRecordHome.findUnpostedRctByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						arRCTDrs = arDistributionRecordHome.findUnpostedRctByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						arRCTDrsPrev = arDistributionRecordHome.findUnpostedRctByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						arRCTDrsPrev1 = arDistributionRecordHome.findUnpostedRctByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);
					}

					j = arRCTDrs.iterator();
					j1 = arRCTDrs.iterator();

					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArReceipt arReceipt = arDistributionRecord.getArReceipt();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arReceipt.getGlFunctionalCurrency().getFcCode(),
								arReceipt.getGlFunctionalCurrency().getFcName(),
								arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}

					}

					j = arRCTDrsPrev.iterator();
					j1 = arRCTDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArReceipt arReceipt = arDistributionRecord.getArReceipt();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arReceipt.getGlFunctionalCurrency().getFcCode(),
								arReceipt.getGlFunctionalCurrency().getFcName(),
								arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					//for last month
					while (j1.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j1.next();

						LocalArReceipt arReceipt = arDistributionRecord.getArReceipt();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arReceipt.getGlFunctionalCurrency().getFcCode(),
								arReceipt.getGlFunctionalCurrency().getFcName(),
								arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// ap voucher
					Collection apVOUDrs = new ArrayList();
					Collection apVOUDrsPrev = new ArrayList();
					Collection apVOUDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						apVOUDrs = apDistributionRecordHome.findUnpostedVouByDateAndCoaAccountNumber(
								EJBCommon.FALSE, glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);

					} else {

						apVOUDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
								EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						apVOUDrsPrev = apDistributionRecordHome.findUnpostedVouByDateAndCoaAccountNumber(
								EJBCommon.FALSE, endGc.getTime(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);

						apVOUDrsPrev = apDistributionRecordHome.findUnpostedVouByDateAndCoaAccountNumber(
								EJBCommon.FALSE, endGc1.getTime(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);
					}

					j = apVOUDrs.iterator();
					j1 = apVOUDrs.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
						// for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}
					}

					j = apVOUDrsPrev.iterator();
					j = apVOUDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j1.next();

						LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					// ap debit memo
					Collection apDMDrs = new ArrayList();
					Collection apDMDrsPrev = new ArrayList();
					Collection apDMDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						apDMDrs = apDistributionRecordHome.findUnpostedVouByDateAndCoaAccountNumber(
								EJBCommon.TRUE, glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);	

					} else {

						apDMDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
								EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

						apDMDrsPrev = apDistributionRecordHome.findUnpostedVouByDateAndCoaAccountNumber(
								EJBCommon.TRUE, endGc.getTime(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);

						apDMDrsPrev = apDistributionRecordHome.findUnpostedVouByDateAndCoaAccountNumber(
								EJBCommon.TRUE, endGc1.getTime(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);	


					}

					j = apDMDrs.iterator();
					j1 = apDMDrs.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApVoucher apDebitMemo = apDistributionRecord.getApVoucher();

						LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
								apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}

					}

					j = apDMDrsPrev.iterator();
					j1 = apDMDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApVoucher apDebitMemo = apDistributionRecord.getApVoucher();

						LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
								apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j1.next();

						LocalApVoucher apDebitMemo = apDistributionRecord.getApVoucher();

						LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
								apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					// ap check
					Collection apCHKDrs = new ArrayList();
					Collection apCHKDrsPrev = new ArrayList();
					Collection apCHKDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						apCHKDrs = apDistributionRecordHome.findUnpostedChkByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

					} else {

						apCHKDrs = apDistributionRecordHome.findUnpostedChkByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

						apCHKDrsPrev = apDistributionRecordHome.findUnpostedChkByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

						apCHKDrsPrev1 = apDistributionRecordHome.findUnpostedChkByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	
					}

					j = apCHKDrs.iterator();
					j1 = apCHKDrs.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApCheck apCheck = apDistributionRecord.getApCheck();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apCheck.getGlFunctionalCurrency().getFcCode(),
								apCheck.getGlFunctionalCurrency().getFcName(),
								apCheck.getChkConversionDate(),
								apCheck.getChkConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}

					}

					j = apCHKDrsPrev.iterator();
					j1 = apCHKDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApCheck apCheck = apDistributionRecord.getApCheck();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apCheck.getGlFunctionalCurrency().getFcCode(),
								apCheck.getGlFunctionalCurrency().getFcName(),
								apCheck.getChkConversionDate(),
								apCheck.getChkConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j1.next();

						LocalApCheck apCheck = apDistributionRecord.getApCheck();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apCheck.getGlFunctionalCurrency().getFcCode(),
								apCheck.getGlFunctionalCurrency().getFcName(),
								apCheck.getChkConversionDate(),
								apCheck.getChkConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					// ap purchase order
					Collection apPODrs = new ArrayList();
					Collection apPODrsPrev = new ArrayList();
					Collection apPODrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						apPODrs = apDistributionRecordHome.findUnpostedPoByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						apPODrs = apDistributionRecordHome.findUnpostedPoByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						apPODrsPrev = apDistributionRecordHome.findUnpostedPoByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						apPODrsPrev1 = apDistributionRecordHome.findUnpostedPoByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);
					}

					j = apPODrs.iterator();
					j1 = apPODrs.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApPurchaseOrder apPurchaseOrder = apDistributionRecord.getApPurchaseOrder();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
								apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
								apPurchaseOrder.getPoConversionDate(),
								apPurchaseOrder.getPoConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}

					}

					j = apPODrsPrev.iterator();
					j1 = apPODrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApPurchaseOrder apPurchaseOrder = apDistributionRecord.getApPurchaseOrder();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
								apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
								apPurchaseOrder.getPoConversionDate(),
								apPurchaseOrder.getPoConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					//for last month
					while (j1.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j1.next();

						LocalApPurchaseOrder apPurchaseOrder = apDistributionRecord.getApPurchaseOrder();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
								apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
								apPurchaseOrder.getPoConversionDate(),
								apPurchaseOrder.getPoConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// cm adjustment
					Collection cmADJDrs = new ArrayList();
					Collection cmADJDrsPrev = new ArrayList();
					Collection cmADJDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						cmADJDrs = cmDistributionRecordHome.findUnpostedAdjByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						cmADJDrs = cmDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						cmADJDrsPrev = cmDistributionRecordHome.findUnpostedAdjByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						cmADJDrsPrev = cmDistributionRecordHome.findUnpostedAdjByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = cmADJDrs.iterator();
					j1 = cmADJDrs.iterator();

					while (j.hasNext()) {

						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

						LocalCmAdjustment cmAdjustment = cmDistributionRecord.getCmAdjustment();

						LocalAdBankAccount adBankAccount = cmAdjustment.getAdBankAccount();

						//LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getFtAdBaAccountFrom());

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								adBankAccount.getGlFunctionalCurrency().getFcCode(),
								adBankAccount.getGlFunctionalCurrency().getFcName(),
								cmAdjustment.getAdjConversionDate(),
								cmAdjustment.getAdjConversionRate(),
								cmDistributionRecord.getDrAmount(), AD_CMPNY);
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}

					}

					j = cmADJDrsPrev.iterator();
					j1 = cmADJDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

						LocalCmAdjustment cmAdjustment = cmDistributionRecord.getCmAdjustment();

						LocalAdBankAccount adBankAccount = cmAdjustment.getAdBankAccount();

						//LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getFtAdBaAccountFrom());

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								adBankAccount.getGlFunctionalCurrency().getFcCode(),
								adBankAccount.getGlFunctionalCurrency().getFcName(),
								cmAdjustment.getAdjConversionDate(),
								cmAdjustment.getAdjConversionRate(),
								cmDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for current month
					while (j1.hasNext()) {

						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j1.next();

						LocalCmAdjustment cmAdjustment = cmDistributionRecord.getCmAdjustment();

						LocalAdBankAccount adBankAccount = cmAdjustment.getAdBankAccount();

						//LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getFtAdBaAccountFrom());

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								adBankAccount.getGlFunctionalCurrency().getFcCode(),
								adBankAccount.getGlFunctionalCurrency().getFcName(),
								cmAdjustment.getAdjConversionDate(),
								cmAdjustment.getAdjConversionRate(),
								cmDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					// cm fund transfer
					Collection cmFTDrs = new ArrayList();
					Collection cmFTDrsPrev = new ArrayList();
					Collection cmFTDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						cmFTDrs = cmDistributionRecordHome.findUnpostedFtByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY); 	     			

					} else {

						cmFTDrs = cmDistributionRecordHome.findUnpostedFtByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY); 	     			

						cmFTDrsPrev = cmDistributionRecordHome.findUnpostedFtByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY); 	     			

						cmFTDrsPrev1 = cmDistributionRecordHome.findUnpostedFtByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY); 
					}

					j = cmFTDrs.iterator();
					j1 = cmFTDrs.iterator();

					while (j.hasNext()) {

						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

						LocalCmFundTransfer cmFundTransfer = cmDistributionRecord.getCmFundTransfer();

						LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								adBankAccount.getGlFunctionalCurrency().getFcCode(),
								adBankAccount.getGlFunctionalCurrency().getFcName(),
								cmFundTransfer.getFtConversionDate(),
								cmFundTransfer.getFtConversionRateFrom(),
								cmDistributionRecord.getDrAmount(), AD_CMPNY);
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}
					}

					j = cmFTDrsPrev.iterator();
					j1 = cmFTDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

						LocalCmFundTransfer cmFundTransfer = cmDistributionRecord.getCmFundTransfer();

						LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								adBankAccount.getGlFunctionalCurrency().getFcCode(),
								adBankAccount.getGlFunctionalCurrency().getFcName(),
								cmFundTransfer.getFtConversionDate(),
								cmFundTransfer.getFtConversionRateFrom(),
								cmDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j1.next();

						LocalCmFundTransfer cmFundTransfer = cmDistributionRecord.getCmFundTransfer();

						LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								adBankAccount.getGlFunctionalCurrency().getFcCode(),
								adBankAccount.getGlFunctionalCurrency().getFcName(),
								cmFundTransfer.getFtConversionDate(),
								cmFundTransfer.getFtConversionRateFrom(),
								cmDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					// inv adjustment
					Collection invADJDrs = new ArrayList();
					Collection invADJDrsPrev = new ArrayList();
					Collection invADJDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invADJDrs = invDistributionRecordHome.findUnpostedAdjByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invADJDrs = invDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invADJDrsPrev = invDistributionRecordHome.findUnpostedAdjByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invADJDrsPrev = invDistributionRecordHome.findUnpostedAdjByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);
					}

					j = invADJDrs.iterator();
					j1 = invADJDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}
					}

					j = invADJDrsPrev.iterator();
					j1 = invADJDrsPrev.iterator();
					//for current month
					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j1.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					// inv build/unbuild assembly
					Collection invBUADrs = new ArrayList();
					Collection invBUADrsPrev = new ArrayList();
					Collection invBUADrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invBUADrs = invDistributionRecordHome.findUnpostedBuaByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invBUADrs = invDistributionRecordHome.findUnpostedBuaByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invBUADrsPrev = invDistributionRecordHome.findUnpostedBuaByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invBUADrsPrev1 = invDistributionRecordHome.findUnpostedBuaByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invBUADrs.iterator();
					j1 = invBUADrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}
					}

					j = invBUADrsPrev.iterator();
					j1 = invBUADrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j1.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					// inv overhead
					Collection invOHDrs = new ArrayList();
					Collection invOHDrsPrev = new ArrayList();
					Collection invOHDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invOHDrs = invDistributionRecordHome.findUnpostedOhByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invOHDrs = invDistributionRecordHome.findUnpostedOhByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invOHDrsPrev = invDistributionRecordHome.findUnpostedOhByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invOHDrsPrev = invDistributionRecordHome.findUnpostedOhByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invOHDrs.iterator();
					j1 = invOHDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}

					}

					j = invOHDrsPrev.iterator();
					j1 = invOHDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j1.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					// inv stock issuance
					Collection invSIDrs = new ArrayList();
					Collection invSIDrsPrev = new ArrayList();
					Collection invSIDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invSIDrs = invDistributionRecordHome.findUnpostedSiByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invSIDrs = invDistributionRecordHome.findUnpostedSiByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invSIDrsPrev = invDistributionRecordHome.findUnpostedSiByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invSIDrsPrev1 = invDistributionRecordHome.findUnpostedSiByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invSIDrs.iterator();
					j1 = invSIDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}
					}

					j = invSIDrsPrev.iterator();
					j1 = invSIDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j1.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					// inv assembly transfer
					Collection invATRDrs = new ArrayList();
					Collection invATRDrsPrev = new ArrayList();
					Collection invATRDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invATRDrs = invDistributionRecordHome.findUnpostedAtrByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invATRDrs = invDistributionRecordHome.findUnpostedAtrByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invATRDrsPrev = invDistributionRecordHome.findUnpostedAtrByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invATRDrsPrev = invDistributionRecordHome.findUnpostedAtrByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invATRDrs.iterator();
					j1 = invATRDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}
					}

					j = invATRDrsPrev.iterator();
					j1 = invATRDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j1.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					// inv stock transfer
					Collection invSTDrs = new ArrayList();
					Collection invSTDrsPrev = new ArrayList();
					Collection invSTDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invSTDrs = invDistributionRecordHome.findUnpostedStByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invSTDrs = invDistributionRecordHome.findUnpostedStByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invSTDrsPrev = invDistributionRecordHome.findUnpostedStByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invSTDrsPrev1 = invDistributionRecordHome.findUnpostedStByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invSTDrs.iterator();
					j1 = invSTDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();
						// for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}
					}

					j = invSTDrsPrev.iterator();
					j1 = invSTDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month
					while (j1.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j1.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					// inv branch stock transfer
					Collection invBSTDrs = new ArrayList();
					Collection invBSTDrsPrev = new ArrayList();
					Collection invBSTDrsPrev1 = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invBSTDrs = invDistributionRecordHome.findUnpostedBstByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invBSTDrs = invDistributionRecordHome.findUnpostedBstByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invBSTDrsPrev = invDistributionRecordHome.findUnpostedBstByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invBSTDrsPrev1 = invDistributionRecordHome.findUnpostedBstByDateAndCoaAccountNumber(
								endGc1.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invBSTDrs.iterator();
					j1 = invBSTDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();
						//for current month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}
						//for last month
						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_LST_MNTH += JL_AMNT;

						} else {

							COA_BLNC_LST_MNTH -= JL_AMNT;

						}
					}

					j = invBSTDrsPrev.iterator();
					j1 = invBSTDrsPrev1.iterator();
					//for current month
					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}
					//for last month month
					while (j1.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j1.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

				}

				if (!DBS_SHW_ZRS && COA_BLNC==0 && COA_BLNC_LST_MNTH== 0) continue;

				if (glChartOfAccount.getCoaAccountType().equals("REVENUE")) {

					DBS_NT_INCM += COA_BLNC;
					DBS_NT_INCM1 += COA_BLNC_LST_MNTH;
					DBS_NT_INCM_PREV += COA_BLNC_PREV;

					continue;

				} else if (glChartOfAccount.getCoaAccountType().equals("EXPENSE")) {

					DBS_NT_INCM -= COA_BLNC;
					DBS_NT_INCM -= COA_BLNC_LST_MNTH;
					DBS_NT_INCM_PREV -= COA_BLNC_PREV;
					continue;

				}			 
				details.setDbsPreviousPeriod(PERIOD+"-"+DBS_YR);
				details.setDbsBalance(COA_BLNC);
				
				if(glChartOfAccount.getCoaAccountType().equals("ASSET"))
				{
					AssetAmount +=COA_BLNC;
				}
				if(glChartOfAccount.getCoaAccountType().equals("LIABILITY"))
				{
					LiabilityAmount +=COA_BLNC;
				}
				if(glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY"))
				{
					OwnerAmount+=COA_BLNC;
				}
				details.setDbsPreviousBalance(COA_BLNC_LST_MNTH);
				list.add(details);

			}

			if (list.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}


			// add prev net income to retained earnings
			if (DBS_INCLD_UNPSTD || DBS_INCLD_UNPSTD_SL) {

				try {

					LocalGlChartOfAccount glRetainedEarningsCoa = 
						glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

					i = list.iterator();

					while (i.hasNext()) {

						GlRepDetailBalanceSheetDetails details = (GlRepDetailBalanceSheetDetails) i.next();

						if(!details.getDbsAccountNumber().equals(glRetainedEarningsCoa.getCoaAccountNumber())) continue;

						details.setDbsBalance(details.getDbsBalance() + DBS_NT_INCM_PREV);

					}

				} catch(FinderException ex) {

					throw new GlobalAccountNumberInvalidException();

				}

			}

			// sort asset, liability and equity

			ArrayList glAssetList = new ArrayList();
			ArrayList glLiabilityList = new ArrayList();
			ArrayList glOwnersEquityList = new ArrayList();

			Iterator j = list.iterator();

			while(j.hasNext()) {

				GlRepDetailBalanceSheetDetails sortDetails = (GlRepDetailBalanceSheetDetails)j.next();

				if ("ASSET".equals(sortDetails.getDbsAccountType())) {

					glAssetList.add(sortDetails);

				} else if ("LIABILITY".equals(sortDetails.getDbsAccountType())) {

					glLiabilityList.add(sortDetails);

				} else {

					glOwnersEquityList.add(sortDetails);
				} 

			}

			list = new ArrayList((Collection)glAssetList);
			list.addAll((Collection)glLiabilityList);
			list.addAll((Collection)glOwnersEquityList);

			
			System.out.println("Asset="+AssetAmount );
			System.out.println("Libility="+LiabilityAmount );
			System.out.println("Owners Equity="+OwnerAmount);
			
			//Alter value of DBS_NT_INCM
			DBS_NT_INCM=AssetAmount-(LiabilityAmount+OwnerAmount);
			
			// add net income
			GlRepDetailBalanceSheetDetails netIncomeDetails = new GlRepDetailBalanceSheetDetails();
			netIncomeDetails.setDbsAccountDescription("NET INCOME");
			netIncomeDetails.setDbsAccountType("OWNERS EQUITY");
			netIncomeDetails.setDbsBalance(DBS_NT_INCM);
			list.add(netIncomeDetails);
			
			return list;

		} catch (GlobalAccountNumberInvalidException ex) {

			throw ex;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}



	}   

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

		Debug.print("GlRepDetailBalanceSheetControllerBean getAdCompany");      

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());

			return details;  	

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGenSgAll(Integer AD_CMPNY) {

		Debug.print("GlRepDetailBalanceSheetControllerBean getGenSgAll");      

		LocalAdCompanyHome adCompanyHome = null;
		LocalGenSegmentHome genSegmentHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			Collection genSegments = genSegmentHome.findByFlCode(adCompany.getGenField().getFlCode(), AD_CMPNY);

			Iterator i = genSegments.iterator();

			while (i.hasNext()) {

				LocalGenSegment genSegment = (LocalGenSegment)i.next();

				GenModSegmentDetails mdetails = new GenModSegmentDetails();
				mdetails.setSgMaxSize(genSegment.getSgMaxSize());
				mdetails.setSgFlSegmentSeparator(genSegment.getGenField().getFlSegmentSeparator());


				list.add(mdetails);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{

		Debug.print("AdRepBankAccountListControllerBean getAdBrResAll");

		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;

		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;

		Collection adBranchResponsibilities = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (adBranchResponsibilities.isEmpty()) {

			throw new GlobalNoRecordFoundException();

		}

		try {

			Iterator i = adBranchResponsibilities.iterator();

			while(i.hasNext()) {

				adBranchResponsibility = (LocalAdBranchResponsibility)i.next();

				adBranch = adBranchResponsibility.getAdBranch();

				AdBranchDetails details = new AdBranchDetails();

				details.setBrCode(adBranch.getBrCode());
				details.setBrBranchCode(adBranch.getBrBranchCode());
				details.setBrName(adBranch.getBrName());
				details.setBrHeadQuarter(adBranch.getBrHeadQuarter());

				list.add(details);

			}	               

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		return list;

	}	

	// private methods

	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("GlRepDetailBalanceSheetControllerBean convertForeignToFunctionalCurrency");


		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}	     


		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

			AMOUNT = AMOUNT * CONVERSION_RATE;

		} else if (CONVERSION_DATE != null) {

			try {

				// Get functional currency rate

				LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;

				if (!FC_NM.equals("USD")) {

					glReceiptFunctionalCurrencyRate = 
						glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
								CONVERSION_DATE, AD_CMPNY);

					AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();

				}

				// Get set of book functional currency rate if necessary

				if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

					LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
						glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
								getFcCode(), CONVERSION_DATE, AD_CMPNY);

					AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();

				}


			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}       

		}

		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}


	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("GlRepDetailBalanceSheetControllerBean ejbCreate");

	}  

}
