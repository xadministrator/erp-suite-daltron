package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;

import java.sql.ResultSet;

import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GenModSegmentDetails;
import com.util.GlModAccountingCalendarValueDetails;
import com.util.GlRepDetailIncomeStatementDetails;

/**
 * @ejb:bean name="GlRepDetailIncomeStatementControllerEJB"
 *           display-name="Used for generation of income statement reports"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepDetailIncomeStatementControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepDetailIncomeStatementController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepDetailIncomeStatementControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlRepDetailIncomeStatementControllerBean extends AbstractSessionBean {

  
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlReportableAcvAll(Integer AD_CMPNY) {

      Debug.print("GlRepDetailIncomeStatementControllerBean getGlReportableAcvAll");      
      
      LocalGlSetOfBookHome glSetOfBookHome = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      
      ArrayList list = new ArrayList();

	  // Initialize EJB Home
	    
	  try {
	        
	       glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
	       glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try {
      	
      	  Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);
      	  
      	  Iterator i = glSetOfBooks.iterator();
      	  
      	  while (i.hasNext()) {
      	  	
      	  	  LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();
      	  	  
      	  	  Collection glAccountingCalendarValues = 
      	  	      glAccountingCalendarValueHome.findReportableAcvByAcCodeAndAcvStatus(
      	  	      	 glSetOfBook.getGlAccountingCalendar().getAcCode(), 'O', 'C', 'P', AD_CMPNY);
      	  	  
      	  	  Iterator j = glAccountingCalendarValues.iterator();
      	  	  
      	  	  while (j.hasNext()) {
      	  	  
	      	  	  LocalGlAccountingCalendarValue glAccountingCalendarValue = 
	      	  	      (LocalGlAccountingCalendarValue)j.next();
	      	  	      
	      	  	  GlModAccountingCalendarValueDetails mdetails = new GlModAccountingCalendarValueDetails();
	      	  	  
	      	  	  mdetails.setAcvPeriodPrefix(glAccountingCalendarValue.getAcvPeriodPrefix());
	      	  	  
	      	  	  // get year
	      	  	  
	      	  	  GregorianCalendar gc = new GregorianCalendar();
	      	  	  gc.setTime(glAccountingCalendarValue.getAcvDateTo());
	      	  	  
	      	  	  mdetails.setAcvYear(gc.get(Calendar.YEAR));
	      	  	  
	      	  	  // is current
	      	  	  
	      	  	  gc = EJBCommon.getGcCurrentDateWoTime();
	      	  	  
	      	  	  if ((glAccountingCalendarValue.getAcvDateFrom().before(gc.getTime()) ||
	      	  	      glAccountingCalendarValue.getAcvDateFrom().equals(gc.getTime())) &&
	      	  	      (glAccountingCalendarValue.getAcvDateTo().after(gc.getTime()) ||
	      	  	      glAccountingCalendarValue.getAcvDateTo().equals(gc.getTime()))) {
	      	  	      	
	      	  	      mdetails.setAcvCurrent(true);
	      	  	      	
	      	  	  }
	      	  	  
	      	  	  list.add(mdetails);
	      	  	      
	      	  }
      	  	
      	  }
      	  
      	  return list;
      	  
      	
      } catch (Exception ex) {
      	
      	  Debug.printStackTrace(ex);
      	  throw new EJBException(ex.getMessage());
      	
      }
      
   }
   
   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList executeSpGlRepDetailIncomeStatement(ResultSet rs, String DIS_ACCNT_NMBR_FRM, String DIS_ACCNT_NMBR_TO, String DIS_PRD, int DIS_YR,
       String DIS_AMNT_TYP, boolean DIS_INCLD_UNPSTD, boolean DIS_INCLD_UNPSTD_SL, boolean DIS_SHW_ZRS, ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY, String Format)
       throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

       Debug.print("GlRepDetailIncomeStatementControllerBean executeSpGlRepDetailIncomeStatement");
      
       LocalGenValueSetValueHome gvsv=null;
       LocalGlSetOfBookHome glSetOfBookHome = null;
       LocalGlChartOfAccountHome glChartOfAccountHome = null;
       LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
       LocalGenValueSetValueHome genValueSetValueHome = null;
       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
       LocalGenSegmentHome genSegmentHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       LocalGlJournalLineHome glJournalLineHome = null;
       LocalArDistributionRecordHome arDistributionRecordHome = null;
       LocalApDistributionRecordHome apDistributionRecordHome = null;
       LocalCmDistributionRecordHome cmDistributionRecordHome = null;
       LocalInvDistributionRecordHome invDistributionRecordHome = null;
       LocalAdBankAccountHome adBankAccountHome = null;
       LocalArInvoiceHome arInvoiceHome = null;
       LocalApVoucherHome apVoucherHome = null;
       
       ArrayList list = new ArrayList();
       
 	   // Initialize EJB Home
 	    
 	  try {
 		  
 		  gvsv = (LocalGenValueSetValueHome)EJBHomeFactory.
           	lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValue.class);
 	       glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
 	       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
 	       glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
 	       genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
 	       glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
 	       genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
 	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
 	       glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);    
 	       arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);    
 	       apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);    
 	       cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);    
 	       invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);    
 	       adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);    
 	       arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);    
 	       apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
 	        
 	  } catch (NamingException ex) {
 	        
 	      throw new EJBException(ex.getMessage());
 	        
 	  }
 	  
 	  try {
 	  	
 	  	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 	  	  Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(DIS_PRD, EJBCommon.getIntendedDate(DIS_YR), AD_CMPNY);
           ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);            
           LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
 	  	  LocalGenField genField = adCompany.getGenField();
 	  	  Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
 	      String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
 	      int genNumberOfSegment = genField.getFlNumberOfSegment();
         
           
           StringTokenizer stAccountFrom = new StringTokenizer(DIS_ACCNT_NMBR_FRM, strSeparator);
           StringTokenizer stAccountTo = new StringTokenizer(DIS_ACCNT_NMBR_TO, strSeparator);
       
           System.out.println("FROM: " + DIS_ACCNT_NMBR_FRM);
           System.out.println("TO:   " + DIS_ACCNT_NMBR_TO);
           // validate if account number is valid
         
           if (stAccountFrom.countTokens() != genNumberOfSegment || 
 	         stAccountTo.countTokens() != genNumberOfSegment) {
 	      	
 	      	  throw new GlobalAccountNumberInvalidException();
 	      	
 	      }
 	      
           System.out.println("rs.getRow()="+rs.getRow());
 		 		 		 
 		 while (rs.next()) {
 			 
 			GlRepDetailIncomeStatementDetails details = new GlRepDetailIncomeStatementDetails();
 			
 			String ACCNT_NMBR = rs.getString("ACCNT_NMBR");
			String ACCNT_TYP = rs.getString("ACCNT_TYP"); 
			String ACCNT_DESC = rs.getString("ACCNT_DESC");
			Double DEBIT = rs.getDouble("DEBIT");
			Double CREDIT = rs.getDouble("CREDIT");
			Double BALANCE = rs.getDouble("BALANCE");
			
			System.out.println("ACCNT_NMBR="+ACCNT_NMBR + " : "+BALANCE);
			
 			details.setDisAccountNumber(ACCNT_NMBR);
 			details.setDisAccountDescription(ACCNT_DESC);
 			details.setDisAccountType(ACCNT_TYP);
 			details.setDisBalance(BALANCE);
 			
 			list.add(details);
 			
 		 }
 		 
 		 if (list.isEmpty()) {
 		 	
 		 	throw new GlobalNoRecordFoundException();
 		 	
 		 }
 		 
 		 
 		 return list;
 	  	  
 	  } catch (GlobalAccountNumberInvalidException ex) {
 	  	
 	  	  throw ex;
 	  	  
 	  } catch (GlobalNoRecordFoundException ex) {
 	  	
 	  	  throw ex;
 	  	
 	  } catch (Exception ex) {
 	  	
 	  	  Debug.printStackTrace(ex);
       	  throw new EJBException(ex.getMessage());
 	  	
 	  }

       

    }
    
  
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList executeGlRepDetailIncomeStatement(String DIS_ACCNT_NMBR_FRM, String DIS_ACCNT_NMBR_TO, String DIS_PRD, int DIS_YR,
      String DIS_AMNT_TYP, boolean DIS_INCLD_UNPSTD, boolean DIS_INCLD_UNPSTD_SL, boolean DIS_SHW_ZRS, ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY, String Format)
      throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

      Debug.print("GlRepDetailIncomeStatementControllerBean executeGlRepDetailIncomeStatement");
      System.out.println("FORMAT:"+Format);
     
      LocalGenValueSetValueHome gvsv=null;
      LocalGlSetOfBookHome glSetOfBookHome = null;
      LocalGlChartOfAccountHome glChartOfAccountHome = null;
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalGenValueSetValueHome genValueSetValueHome = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      LocalGenSegmentHome genSegmentHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      LocalGlJournalLineHome glJournalLineHome = null;
      LocalArDistributionRecordHome arDistributionRecordHome = null;
      LocalApDistributionRecordHome apDistributionRecordHome = null;
      LocalCmDistributionRecordHome cmDistributionRecordHome = null;
      LocalInvDistributionRecordHome invDistributionRecordHome = null;
      LocalAdBankAccountHome adBankAccountHome = null;
      LocalArInvoiceHome arInvoiceHome = null;
      LocalApVoucherHome apVoucherHome = null;
      
      ArrayList list = new ArrayList();
      
	   // Initialize EJB Home
	    
	  try {
		  
		  gvsv = (LocalGenValueSetValueHome)EJBHomeFactory.
          	lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValue.class);
	       glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
	       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	       glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
	       genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
	       glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
	       genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	       glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);    
	       arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
	           lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);    
	       apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
	           lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);    
	       cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
	           lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);    
	       invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
	           lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);    
	       adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);    
	       arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
	           lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);    
	       apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
	           lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	  
	  try {
	  	
	  	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	  	  Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(DIS_PRD, EJBCommon.getIntendedDate(DIS_YR), AD_CMPNY);
          ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);            
          LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
	  	  LocalGenField genField = adCompany.getGenField();
	  	  Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
	      String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
	      int genNumberOfSegment = genField.getFlNumberOfSegment();
	      
	      // get coa selected
	      
	      StringBuffer jbossQl = new StringBuffer();
          jbossQl.append("SELECT DISTINCT OBJECT(coa) FROM GlChartOfAccount coa ");

          // add branch criteria

		  if (branchList.isEmpty()) {
		  	
		  	throw new GlobalNoRecordFoundException();
		  	
		  }
		  else {
		  	
		  	jbossQl.append(", in (coa.adBranchCoas) bcoa WHERE bcoa.adBranch.brCode in (");
		  	
		  	boolean firstLoop = true;
		  	
		  	Iterator j = branchList.iterator();
		  	
		  	while(j.hasNext()) {
		  		
		  		if(firstLoop == false) { 
		  			jbossQl.append(", "); 
		  		}
		  		else { 
		  			firstLoop = false; 
		  		}
		  		
		  		AdBranchDetails mdetails = (AdBranchDetails) j.next();
		  		
		  		jbossQl.append(mdetails.getBrCode());
		  		
		  	}
		  	
		  	jbossQl.append(") AND ");
		  	
		  }                    
          
          StringTokenizer stAccountFrom = new StringTokenizer(DIS_ACCNT_NMBR_FRM, strSeparator);
          StringTokenizer stAccountTo = new StringTokenizer(DIS_ACCNT_NMBR_TO, strSeparator);
      
          // validate if account number is valid
        
          if (stAccountFrom.countTokens() != genNumberOfSegment || 
	         stAccountTo.countTokens() != genNumberOfSegment) {
	      	
	      	  throw new GlobalAccountNumberInvalidException();
	      	
	      }
	      
	      try {
      	
	          String var = "1";
	          
	          if (genNumberOfSegment > 1) {
	          	
	          	for (int i=0; i<genNumberOfSegment; i++) {
	          		
	          		if (i == 0 && i < genNumberOfSegment - 1) {
	          			
	          			// for first segment
	          			
	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");
	          			
	          			var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)+1 ";
	          			
	          			
	          		} else if (i != 0 && i < genNumberOfSegment - 1){     		
	          			
	          			// for middle segments
	          			
	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")) - (" + var +")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");
	          			
	          			var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";
	          			
	          		} else if (i != 0 && i == genNumberOfSegment - 1) {
	          			
	          			// for last segment
	          			
	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' ");
	          			
	          			
	          		}	     		       	      	   	            
	          		
	          	}
	          	
	          } else if(genNumberOfSegment == 1) {
	          	
	          	String accountFrom = stAccountFrom.nextToken();
	       		String accountTo = stAccountTo.nextToken();
	       		
	       		jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + accountFrom + "' AND '" + accountTo + "' OR " +
	       				"coa.coaAccountNumber BETWEEN '" + accountFrom + "' AND '" + accountTo + "' ");
	          	
	          }
	          
			  jbossQl.append("AND coa.coaEnable=1 AND coa.coaAdCompany=" + AD_CMPNY + " ");
			  			  
		 } catch (NumberFormatException ex) {
		 	
		 	throw new GlobalAccountNumberInvalidException();
		 	
		 }
		 
		 // generate order by coa natural account

	     short accountSegmentNumber = 0;
	      
	     try {
	      
	      	  LocalGenSegment genSegment = genSegmentHome.findByFlCodeAndSegmentType(genField.getFlCode(), 'N', AD_CMPNY);
	      	  accountSegmentNumber = genSegment.getSgSegmentNumber();
	      	
	     } catch (Exception ex) {
	      
	         throw new EJBException(ex.getMessage());
	      
	     }
	            
	      	  
		 jbossQl.append("ORDER BY coa.coaSegment" + accountSegmentNumber + ", coa.coaAccountNumber ");
		 
		 System.out.println(jbossQl.toString());
		 
		 Object obj[] = new Object[0];
	 
  	     Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), obj);
		  
		 Iterator i = glChartOfAccounts.iterator();
		 System.out.println("glChartOfAccounts.size()="+glChartOfAccounts.size());
		 
		 double DIS_NT_INCM = 0d;
		 
		 //Collection BrList = gvsv.findByVsName("BRANCH", AD_CMPNY);
		 
		 
		 		 		 
		 while (i.hasNext()) {
		 	
		 	LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)i.next();
		 	
		 	System.out.println("glChartOfAccount.getCoaAccountNumber()="+glChartOfAccount.getCoaAccountNumber());
		 	
		 	GlRepDetailIncomeStatementDetails details = new GlRepDetailIncomeStatementDetails();		 			 	 	 	 		 	 		 	 
		 	
		 	double COA_BLNC = 0d;

		 	details.setDisAccountNumber(glChartOfAccount.getCoaAccountNumber()); 
		 	details.setDisAccountDescription(glChartOfAccount.getCoaAccountDescription());
		 	/*if(Format.length()>3)
		 	{
		 		int x=0;
		 		String sample = glChartOfAccount.getCoaAccountDescription();
		 		
		 		 Iterator ic = BrList.iterator();
		 		while (ic.hasNext()) {
		 			LocalGenValueSetValue glvsv = (LocalGenValueSetValue)ic.next();
		 			if(sample.contains(glvsv.getVsvDescription()+"-"))
		 					{
		 				System.out.println(glvsv.getVsvDescription());
		 				sample=glvsv.getVsvDescription();
		 					break;
		 					
		 					}
		 		}
		 		
		 		
	
			 		
		 		details.setDisAccountDescription(sample);
		 	}*/
		 	details.setDisAccountType(glChartOfAccount.getCoaAccountType());
		 
		 	if (glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") ||
		 			glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) continue;
		 	
		 	// get coa debit or credit	in coa balance			 			  		    		 	 		 	 	 	 
		 	
		 	LocalGlAccountingCalendarValue glAccountingCalendarValue =
		 		glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
		 				glSetOfBook.getGlAccountingCalendar().getAcCode(),
						DIS_PRD, AD_CMPNY);
		 	
		 	short DIS_PRD_NMBR = 0;
		 	
		 	if (DIS_AMNT_TYP.equals("PTD")) {
		 		
		 		DIS_PRD_NMBR = glAccountingCalendarValue.getAcvPeriodNumber();
		 		
		 	} else if (DIS_AMNT_TYP.equals("QTD")) {
		 		
		 		Collection glQuarterAccountingCalendarValues = 
		 			glAccountingCalendarValueHome.findByAcCodeAndAcvQuarterNumber(
		 					glSetOfBook.getGlAccountingCalendar().getAcCode(),
							glAccountingCalendarValue.getAcvQuarter(), AD_CMPNY);
		 		
		 		ArrayList glQuarterAccountingCalendarValueList = 
		 			new ArrayList(glQuarterAccountingCalendarValues);
		 		
		 		LocalGlAccountingCalendarValue glQuarterAccountingCalendarValue = 
		 			(LocalGlAccountingCalendarValue)glQuarterAccountingCalendarValueList.get(0);
		 		
		 		DIS_PRD_NMBR = glQuarterAccountingCalendarValue.getAcvPeriodNumber();
		 		
		 	} else if (DIS_AMNT_TYP.equals("YTD")) {
		 		
		 		DIS_PRD_NMBR = 1;
		 		
		 	}
		 	
		 	// get beginning balance
		 	
		 	LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue = 
		 		glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
		 				glSetOfBook.getGlAccountingCalendar().getAcCode(),
						DIS_PRD_NMBR, AD_CMPNY);			    	
		 	
		 	LocalGlChartOfAccountBalance glBeginningChartOfAccountBalance = 
		 		glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		 				glBeginningAccountingCalendarValue.getAcvCode(),
						glChartOfAccount.getCoaCode(), AD_CMPNY);
		 	
		 	// get ending balance		 	
		 	
		 	LocalGlChartOfAccountBalance glEndingChartOfAccountBalance = 
		 		glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		 				glAccountingCalendarValue.getAcvCode(),
						glChartOfAccount.getCoaCode(), AD_CMPNY);		 	
		 	
		 	COA_BLNC = glEndingChartOfAccountBalance.getCoabEndingBalance() - 
			glBeginningChartOfAccountBalance.getCoabBeginningBalance();
		 	
		 	// get coa debit or credit balance in unposted journals if necessary
		 	
		 	if (DIS_INCLD_UNPSTD) {
		 		
		 		Collection glJournalLines = glJournalLineHome.findUnpostedJlByJrEffectiveDateRangeAndCoaCode(
		 				glBeginningAccountingCalendarValue.getAcvDateFrom(),
						glAccountingCalendarValue.getAcvDateTo(), 
						glChartOfAccount.getCoaCode(), AD_CMPNY);
		 		
		 		Iterator j = glJournalLines.iterator();
		 		
		 		while (j.hasNext()) {
		 			
		 			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();
		 			
		 			LocalGlJournal glJournal = glJournalLine.getGlJournal();
		 			
		 			double JL_AMNT = this.convertForeignToFunctionalCurrency(
		 					glJournal.getGlFunctionalCurrency().getFcCode(),
							glJournal.getGlFunctionalCurrency().getFcName(),
							glJournal.getJrConversionDate(),
							glJournal.getJrConversionRate(),
							glJournalLine.getJlAmount(), AD_CMPNY);
		 			
		 			if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) &&
		 					glJournalLine.getJlDebit() == EJBCommon.TRUE) ||
							(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") &&
									glJournalLine.getJlDebit() == EJBCommon.FALSE)) {
		 				
		 				COA_BLNC += EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
		 				
		 			} else {
		 				
		 				COA_BLNC -= EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
		 				
		 			}
		 			
		 		}		 	 			 	 	 
		 		
		 	}
		 	
		 	// include unposted subledger transactions
		 	
		 	if (DIS_INCLD_UNPSTD_SL) {
		 		
		 		Collection arINVDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
		 				EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);
			 	
			 	Iterator j = arINVDrs.iterator();
   				
			 	while (j.hasNext()) {

			 		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

			 		LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
			 				arInvoice.getGlFunctionalCurrency().getFcCode(),
			 				arInvoice.getGlFunctionalCurrency().getFcName(),
			 				arInvoice.getInvConversionDate(),
			 				arInvoice.getInvConversionRate(),
			 				arDistributionRecord.getDrAmount(), AD_CMPNY);

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}

			 	Collection arCMDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
		 				EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

			 	j = arCMDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

			 		LocalArInvoice arCreditMemo = arDistributionRecord.getArInvoice();

			 		LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
			 				arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
			 				arInvoice.getGlFunctionalCurrency().getFcCode(),
			 				arInvoice.getGlFunctionalCurrency().getFcName(), 
			 				arInvoice.getInvConversionDate(),
			 				arInvoice.getInvConversionRate(),
			 				arDistributionRecord.getDrAmount(), AD_CMPNY);

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}
			 	
			 	Collection arRCTDrs = arDistributionRecordHome.findUnpostedRctByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

			 	j = arRCTDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

			 		LocalArReceipt arReceipt = arDistributionRecord.getArReceipt();

			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
			 				arReceipt.getGlFunctionalCurrency().getFcCode(),
			 				arReceipt.getGlFunctionalCurrency().getFcName(),
			 				arReceipt.getRctConversionDate(),
			 				arReceipt.getRctConversionRate(),
			 				arDistributionRecord.getDrAmount(), AD_CMPNY);

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}
			 	
			 	Collection apVOUDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
		 				EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

			 	j = apVOUDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

			 		LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
			 				apVoucher.getGlFunctionalCurrency().getFcCode(),
			 				apVoucher.getGlFunctionalCurrency().getFcName(),
			 				apVoucher.getVouConversionDate(),
			 				apVoucher.getVouConversionRate(),
			 				apDistributionRecord.getDrAmount(), AD_CMPNY);

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}
			 	
			 	Collection apDMDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
		 				EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

			 	j = apDMDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

			 		LocalApVoucher apDebitMemo = apDistributionRecord.getApVoucher();

			 		LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
			 				apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
			 				apVoucher.getGlFunctionalCurrency().getFcCode(),
			 				apVoucher.getGlFunctionalCurrency().getFcName(),
			 				apVoucher.getVouConversionDate(),
			 				apVoucher.getVouConversionRate(),
			 				apDistributionRecord.getDrAmount(), AD_CMPNY);

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}
  	     		
			 	Collection apCHKDrs = apDistributionRecordHome.findUnpostedChkByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

			 	j = apCHKDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

			 		LocalApCheck apCheck = apDistributionRecord.getApCheck();

			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
			 				apCheck.getGlFunctionalCurrency().getFcCode(),
			 				apCheck.getGlFunctionalCurrency().getFcName(),
			 				apCheck.getChkConversionDate(),
			 				apCheck.getChkConversionRate(),
			 				apDistributionRecord.getDrAmount(), AD_CMPNY);

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}
  	     		
			 	Collection apPODrs = apDistributionRecordHome.findUnpostedPoByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

			 	j = apPODrs.iterator();

			 	while (j.hasNext()) {

			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

			 		LocalApPurchaseOrder apPurchaseOrder = apDistributionRecord.getApPurchaseOrder();

			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
			 				apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
			 				apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
			 				apPurchaseOrder.getPoConversionDate(),
			 				apPurchaseOrder.getPoConversionRate(),
			 				apDistributionRecord.getDrAmount(), AD_CMPNY);

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}

			 	Collection cmADJDrs = cmDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

			 	j = cmADJDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

			 		LocalCmAdjustment cmAdjustment = cmDistributionRecord.getCmAdjustment();

			 		LocalAdBankAccount adBankAccount = cmAdjustment.getAdBankAccount();

			 		//LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getFtAdBaAccountFrom());

			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
			 				adBankAccount.getGlFunctionalCurrency().getFcCode(),
			 				adBankAccount.getGlFunctionalCurrency().getFcName(),
			 				cmAdjustment.getAdjConversionDate(),
			 				cmAdjustment.getAdjConversionRate(),
			 				cmDistributionRecord.getDrAmount(), AD_CMPNY);

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}


			 	Collection cmFTDrs = cmDistributionRecordHome.findUnpostedFtByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY); 	     			

			 	j = cmFTDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

			 		LocalCmFundTransfer cmFundTransfer = cmDistributionRecord.getCmFundTransfer();

			 		LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
			 				adBankAccount.getGlFunctionalCurrency().getFcCode(),
			 				adBankAccount.getGlFunctionalCurrency().getFcName(),
			 				cmFundTransfer.getFtConversionDate(),
			 				cmFundTransfer.getFtConversionRateFrom(),
			 				cmDistributionRecord.getDrAmount(), AD_CMPNY);

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}
			 	
			 	Collection invADJDrs = invDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

			 	j = invADJDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

			 		double JL_AMNT = invDistributionRecord.getDrAmount();

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}
			 	
			 	Collection invBUADrs = invDistributionRecordHome.findUnpostedBuaByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);


			 	j = invBUADrs.iterator();

			 	while (j.hasNext()) {

			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

			 		double JL_AMNT = invDistributionRecord.getDrAmount();

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}
			 	
			 	Collection invOHDrs = invDistributionRecordHome.findUnpostedOhByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

			 	j = invOHDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

			 		double JL_AMNT = invDistributionRecord.getDrAmount();

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}

			 	Collection invSIDrs = invDistributionRecordHome.findUnpostedSiByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

			 	j = invSIDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

			 		double JL_AMNT = invDistributionRecord.getDrAmount();

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}

			 	Collection invATRDrs = invDistributionRecordHome.findUnpostedAtrByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

			 	j = invATRDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

			 		double JL_AMNT = invDistributionRecord.getDrAmount();

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}
			 	
			 	Collection invSTDrs = invDistributionRecordHome.findUnpostedStByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

			 	j = invSTDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

			 		double JL_AMNT = invDistributionRecord.getDrAmount();

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}
			 	
			 	Collection invBSTDrs = invDistributionRecordHome.findUnpostedBstByDateRangeAndCoaAccountNumber(
			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
			 			glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

			 	j = invBSTDrs.iterator();

			 	while (j.hasNext()) {

			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

			 		double JL_AMNT = invDistributionRecord.getDrAmount();

			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

			 			COA_BLNC += JL_AMNT;

			 		} else {

			 			COA_BLNC -= JL_AMNT;

			 		}

			 	}
			 	
		 	}
		 	
		 	if (!DIS_SHW_ZRS && COA_BLNC == 0) continue;		 			 	 
		 	
		 	details.setDisBalance(COA_BLNC);
		 	//System.out.println("COA_BLNC "+COA_BLNC);

		 	list.add(details);
		 	
		 	
		 }
		 
		 if (list.isEmpty()) {
		 	
		 	throw new GlobalNoRecordFoundException();
		 	
		 }
		 
		 // sort revenue and expense
		 
		 ArrayList glRevenueList =  new ArrayList();
         ArrayList glExpenseList = new ArrayList();
		 
		 Iterator j = list.iterator();
		 
         while(j.hasNext()) {
         	
	         GlRepDetailIncomeStatementDetails sortDetails = (GlRepDetailIncomeStatementDetails)j.next();
	         
			 if ("REVENUE".equals(sortDetails.getDisAccountType())) {
			 	
			    glRevenueList.add(sortDetails);
			    
			 } else if ("EXPENSE".equals(sortDetails.getDisAccountType())) {
			 	
			    glExpenseList.add(sortDetails);
			    
			 }
			 
	     }
	      
	     list = new ArrayList((Collection)glRevenueList);
	     list.addAll((Collection)glExpenseList);

		 return list;
	  	  
	  } catch (GlobalAccountNumberInvalidException ex) {
	  	
	  	  throw ex;
	  	  
	  } catch (GlobalNoRecordFoundException ex) {
	  	
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	
	  	  Debug.printStackTrace(ex);
      	  throw new EJBException(ex.getMessage());
	  	
	  }

      

   }   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public double executeGlRepDetailIncomeStatementSummary(String DIS_ACCNT_NMBR_FRM, String DIS_ACCNT_NMBR_TO, String DIS_PRD, int DIS_YR,
       String DIS_AMNT_TYP, boolean DIS_INCLD_UNPSTD, boolean DIS_INCLD_UNPSTD_SL, boolean DIS_SHW_ZRS, ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY, String Format)
       throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

       Debug.print("GlRepDetailIncomeStatementControllerBean executeGlRepDetailIncomeStatement");
       System.out.println("FORMAT:"+Format);
       double total=0d;
       LocalGlSetOfBookHome glSetOfBookHome = null;
       LocalGlChartOfAccountHome glChartOfAccountHome = null;
       LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
       LocalGenValueSetValueHome genValueSetValueHome = null;
       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
       LocalGenSegmentHome genSegmentHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       LocalGlJournalLineHome glJournalLineHome = null;
       LocalArDistributionRecordHome arDistributionRecordHome = null;
       LocalApDistributionRecordHome apDistributionRecordHome = null;
       LocalCmDistributionRecordHome cmDistributionRecordHome = null;
       LocalInvDistributionRecordHome invDistributionRecordHome = null;
       LocalAdBankAccountHome adBankAccountHome = null;
       LocalArInvoiceHome arInvoiceHome = null;
       LocalApVoucherHome apVoucherHome = null;
       
       ArrayList list = new ArrayList();
       
 	   // Initialize EJB Home
 	    
 	  try {
 	        
 	       glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
 	       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
 	       glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
 	       genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
 	       glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
 	       genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
 	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
 	       glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);    
 	       arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);    
 	       apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);    
 	       cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);    
 	       invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);    
 	       adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);    
 	       arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);    
 	       apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
 	        
 	  } catch (NamingException ex) {
 	        
 	      throw new EJBException(ex.getMessage());
 	        
 	  }
 	  
 	  try {
 	  	
 	  	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 	  	  Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(DIS_PRD, EJBCommon.getIntendedDate(DIS_YR), AD_CMPNY);
           ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);            
           LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
 	  	  LocalGenField genField = adCompany.getGenField();
 	  	  Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
 	      String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
 	      int genNumberOfSegment = genField.getFlNumberOfSegment();
 	      
 	      // get coa selected
 	      
 	      StringBuffer jbossQl = new StringBuffer();
           jbossQl.append("SELECT DISTINCT OBJECT(coa) FROM GlChartOfAccount coa ");

           // add branch criteria

 		  if (branchList.isEmpty()) {
 		  	
 		  	throw new GlobalNoRecordFoundException();
 		  	
 		  }
 		  else {
 		  	
 		  	jbossQl.append(", in (coa.adBranchCoas) bcoa WHERE bcoa.adBranch.brCode in (");
 		  	
 		  	boolean firstLoop = true;
 		  	
 		  	Iterator j = branchList.iterator();
 		  	
 		  	while(j.hasNext()) {
 		  		
 		  		if(firstLoop == false) { 
 		  			jbossQl.append(", "); 
 		  		}
 		  		else { 
 		  			firstLoop = false; 
 		  		}
 		  		
 		  		AdBranchDetails mdetails = (AdBranchDetails) j.next();
 		  		
 		  		jbossQl.append(mdetails.getBrCode());
 		  		
 		  	}
 		  	
 		  	jbossQl.append(") AND ");
 		  	
 		  }                    
           
           StringTokenizer stAccountFrom = new StringTokenizer(DIS_ACCNT_NMBR_FRM, strSeparator);
           StringTokenizer stAccountTo = new StringTokenizer(DIS_ACCNT_NMBR_TO, strSeparator);
       
           // validate if account number is valid
         
           if (stAccountFrom.countTokens() != genNumberOfSegment || 
 	         stAccountTo.countTokens() != genNumberOfSegment) {
 	      	
 	      	  throw new GlobalAccountNumberInvalidException();
 	      	
 	      }
 	      
 	      try {
       	
 	          String var = "1";
 	          
 	          if (genNumberOfSegment > 1) {
 	          	
 	          	for (int i=0; i<genNumberOfSegment; i++) {
 	          		
 	          		if (i == 0 && i < genNumberOfSegment - 1) {
 	          			
 	          			// for first segment
 	          			
 	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");
 	          			
 	          			var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)+1 ";
 	          			
 	          			
 	          		} else if (i != 0 && i < genNumberOfSegment - 1){     		
 	          			
 	          			// for middle segments
 	          			
 	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")) - (" + var +")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");
 	          			
 	          			var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";
 	          			
 	          		} else if (i != 0 && i == genNumberOfSegment - 1) {
 	          			
 	          			// for last segment
 	          			
 	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' ");
 	          			
 	          			
 	          		}	     		       	      	   	            
 	          		
 	          	}
 	          	
 	          } else if(genNumberOfSegment == 1) {
 	          	
 	          	String accountFrom = stAccountFrom.nextToken();
 	       		String accountTo = stAccountTo.nextToken();
 	       		
 	       		jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + accountFrom + "' AND '" + accountTo + "' OR " +
 	       				"coa.coaAccountNumber BETWEEN '" + accountFrom + "' AND '" + accountTo + "' ");
 	          	
 	          }
 	          
 			  jbossQl.append("AND coa.coaEnable=1 AND coa.coaAdCompany=" + AD_CMPNY + " ");
 			  			  
 		 } catch (NumberFormatException ex) {
 		 	
 		 	throw new GlobalAccountNumberInvalidException();
 		 	
 		 }
 		 
 		 // generate order by coa natural account

 	     short accountSegmentNumber = 0;
 	      
 	     try {
 	      
 	      	  LocalGenSegment genSegment = genSegmentHome.findByFlCodeAndSegmentType(genField.getFlCode(), 'N', AD_CMPNY);
 	      	  accountSegmentNumber = genSegment.getSgSegmentNumber();
 	      	
 	     } catch (Exception ex) {
 	      
 	         throw new EJBException(ex.getMessage());
 	      
 	     }
 	            
 	      	  
 		 jbossQl.append("ORDER BY coa.coaSegment" + accountSegmentNumber + ", coa.coaAccountNumber ");
 		 
 		 System.out.println(jbossQl.toString());
 		 
 		 Object obj[] = new Object[0];
 	 
   	     Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), obj);
 		  
 		 Iterator i = glChartOfAccounts.iterator();
 		 
 		 double DIS_NT_INCM = 0d;		 
 		 		 		 
 		 while (i.hasNext()) {
 		 	
 		 	LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)i.next();
 		 	
 		 	GlRepDetailIncomeStatementDetails details = new GlRepDetailIncomeStatementDetails();		 			 	 	 	 		 	 		 	 
 		 	
 		 	double COA_BLNC = 0d;

 		 	details.setDisAccountNumber(glChartOfAccount.getCoaAccountNumber()); 
 		 	details.setDisAccountDescription(glChartOfAccount.getCoaAccountDescription());
 		 	details.setDisAccountType(glChartOfAccount.getCoaAccountType());
 		 	
 		 	if (glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") ||
 		 			glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) continue;
 		 	
 		 	// get coa debit or credit	in coa balance			 			  		    		 	 		 	 	 	 
 		 	
 		 	LocalGlAccountingCalendarValue glAccountingCalendarValue =
 		 		glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
 		 				glSetOfBook.getGlAccountingCalendar().getAcCode(),
 						DIS_PRD, AD_CMPNY);
 		 	
 		 	short DIS_PRD_NMBR = 0;
 		 	
 		 	if (DIS_AMNT_TYP.equals("PTD")) {
 		 		
 		 		DIS_PRD_NMBR = glAccountingCalendarValue.getAcvPeriodNumber();
 		 		
 		 	} else if (DIS_AMNT_TYP.equals("QTD")) {
 		 		
 		 		Collection glQuarterAccountingCalendarValues = 
 		 			glAccountingCalendarValueHome.findByAcCodeAndAcvQuarterNumber(
 		 					glSetOfBook.getGlAccountingCalendar().getAcCode(),
 							glAccountingCalendarValue.getAcvQuarter(), AD_CMPNY);
 		 		
 		 		ArrayList glQuarterAccountingCalendarValueList = 
 		 			new ArrayList(glQuarterAccountingCalendarValues);
 		 		
 		 		LocalGlAccountingCalendarValue glQuarterAccountingCalendarValue = 
 		 			(LocalGlAccountingCalendarValue)glQuarterAccountingCalendarValueList.get(0);
 		 		
 		 		DIS_PRD_NMBR = glQuarterAccountingCalendarValue.getAcvPeriodNumber();
 		 		
 		 	} else if (DIS_AMNT_TYP.equals("YTD")) {
 		 		
 		 		DIS_PRD_NMBR = 1;
 		 		
 		 	}
 		 	
 		 	// get beginning balance
 		 	
 		 	LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue = 
 		 		glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
 		 				glSetOfBook.getGlAccountingCalendar().getAcCode(),
 						DIS_PRD_NMBR, AD_CMPNY);			    	
 		 	
 		 	LocalGlChartOfAccountBalance glBeginningChartOfAccountBalance = 
 		 		glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
 		 				glBeginningAccountingCalendarValue.getAcvCode(),
 						glChartOfAccount.getCoaCode(), AD_CMPNY);
 		 	
 		 	// get ending balance		 	
 		 	
 		 	LocalGlChartOfAccountBalance glEndingChartOfAccountBalance = 
 		 		glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
 		 				glAccountingCalendarValue.getAcvCode(),
 						glChartOfAccount.getCoaCode(), AD_CMPNY);		 	
 		 	
 		 	COA_BLNC = glEndingChartOfAccountBalance.getCoabEndingBalance() - 
 			glBeginningChartOfAccountBalance.getCoabBeginningBalance();
 		 	
 		 	// get coa debit or credit balance in unposted journals if necessary
 		 	if (DIS_INCLD_UNPSTD) {
 		 		
 		 		Collection glJournalLines = glJournalLineHome.findUnpostedJlByJrEffectiveDateRangeAndCoaCode(
 		 				glBeginningAccountingCalendarValue.getAcvDateFrom(),
 						glAccountingCalendarValue.getAcvDateTo(), 
 						glChartOfAccount.getCoaCode(), AD_CMPNY);
 		 		
 		 		Iterator j = glJournalLines.iterator();
 		 		
 		 		while (j.hasNext()) {
 		 			
 		 			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();
 		 			
 		 			LocalGlJournal glJournal = glJournalLine.getGlJournal();
 		 			
 		 			double JL_AMNT = this.convertForeignToFunctionalCurrency(
 		 					glJournal.getGlFunctionalCurrency().getFcCode(),
 							glJournal.getGlFunctionalCurrency().getFcName(),
 							glJournal.getJrConversionDate(),
 							glJournal.getJrConversionRate(),
 							glJournalLine.getJlAmount(), AD_CMPNY);
 		 			
 		 			if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) &&
 		 					glJournalLine.getJlDebit() == EJBCommon.TRUE) ||
 							(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") &&
 									glJournalLine.getJlDebit() == EJBCommon.FALSE)) {
 		 				
 		 				COA_BLNC += EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
 		 				
 		 			} else {
 		 				
 		 				COA_BLNC -= EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
 		 				
 		 			}
 		 			
 		 		}		 	 			 	 	 
 		 		
 		 	}
 		 	
 		 	// include unposted subledger transactions
 		 	
 		 	if (DIS_INCLD_UNPSTD_SL) {
 		 		
 		 		Collection arINVDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
 		 				EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);
 			 	
 			 	Iterator j = arINVDrs.iterator();
    				
 			 	while (j.hasNext()) {

 			 		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

 			 		LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

 			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
 			 				arInvoice.getGlFunctionalCurrency().getFcCode(),
 			 				arInvoice.getGlFunctionalCurrency().getFcName(),
 			 				arInvoice.getInvConversionDate(),
 			 				arInvoice.getInvConversionRate(),
 			 				arDistributionRecord.getDrAmount(), AD_CMPNY);

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}

 			 	Collection arCMDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
 		 				EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

 			 	j = arCMDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

 			 		LocalArInvoice arCreditMemo = arDistributionRecord.getArInvoice();

 			 		LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
 			 				arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

 			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
 			 				arInvoice.getGlFunctionalCurrency().getFcCode(),
 			 				arInvoice.getGlFunctionalCurrency().getFcName(), 
 			 				arInvoice.getInvConversionDate(),
 			 				arInvoice.getInvConversionRate(),
 			 				arDistributionRecord.getDrAmount(), AD_CMPNY);

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}
 			 	
 			 	Collection arRCTDrs = arDistributionRecordHome.findUnpostedRctByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

 			 	j = arRCTDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

 			 		LocalArReceipt arReceipt = arDistributionRecord.getArReceipt();

 			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
 			 				arReceipt.getGlFunctionalCurrency().getFcCode(),
 			 				arReceipt.getGlFunctionalCurrency().getFcName(),
 			 				arReceipt.getRctConversionDate(),
 			 				arReceipt.getRctConversionRate(),
 			 				arDistributionRecord.getDrAmount(), AD_CMPNY);

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}
 			 	
 			 	Collection apVOUDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
 		 				EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

 			 	j = apVOUDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

 			 		LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

 			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
 			 				apVoucher.getGlFunctionalCurrency().getFcCode(),
 			 				apVoucher.getGlFunctionalCurrency().getFcName(),
 			 				apVoucher.getVouConversionDate(),
 			 				apVoucher.getVouConversionRate(),
 			 				apDistributionRecord.getDrAmount(), AD_CMPNY);

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}
 			 	
 			 	Collection apDMDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
 		 				EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

 			 	j = apDMDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

 			 		LocalApVoucher apDebitMemo = apDistributionRecord.getApVoucher();

 			 		LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
 			 				apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

 			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
 			 				apVoucher.getGlFunctionalCurrency().getFcCode(),
 			 				apVoucher.getGlFunctionalCurrency().getFcName(),
 			 				apVoucher.getVouConversionDate(),
 			 				apVoucher.getVouConversionRate(),
 			 				apDistributionRecord.getDrAmount(), AD_CMPNY);

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}
   	     		
 			 	Collection apCHKDrs = apDistributionRecordHome.findUnpostedChkByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

 			 	j = apCHKDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

 			 		LocalApCheck apCheck = apDistributionRecord.getApCheck();

 			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
 			 				apCheck.getGlFunctionalCurrency().getFcCode(),
 			 				apCheck.getGlFunctionalCurrency().getFcName(),
 			 				apCheck.getChkConversionDate(),
 			 				apCheck.getChkConversionRate(),
 			 				apDistributionRecord.getDrAmount(), AD_CMPNY);

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}
   	     		
 			 	Collection apPODrs = apDistributionRecordHome.findUnpostedPoByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

 			 	j = apPODrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

 			 		LocalApPurchaseOrder apPurchaseOrder = apDistributionRecord.getApPurchaseOrder();

 			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
 			 				apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
 			 				apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
 			 				apPurchaseOrder.getPoConversionDate(),
 			 				apPurchaseOrder.getPoConversionRate(),
 			 				apDistributionRecord.getDrAmount(), AD_CMPNY);

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}

 			 	Collection cmADJDrs = cmDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

 			 	j = cmADJDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

 			 		LocalCmAdjustment cmAdjustment = cmDistributionRecord.getCmAdjustment();

 			 		LocalAdBankAccount adBankAccount = cmAdjustment.getAdBankAccount();

 			 		//LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getFtAdBaAccountFrom());

 			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
 			 				adBankAccount.getGlFunctionalCurrency().getFcCode(),
 			 				adBankAccount.getGlFunctionalCurrency().getFcName(),
 			 				cmAdjustment.getAdjConversionDate(),
 			 				cmAdjustment.getAdjConversionRate(),
 			 				cmDistributionRecord.getDrAmount(), AD_CMPNY);

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}


 			 	Collection cmFTDrs = cmDistributionRecordHome.findUnpostedFtByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY); 	     			

 			 	j = cmFTDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

 			 		LocalCmFundTransfer cmFundTransfer = cmDistributionRecord.getCmFundTransfer();

 			 		LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

 			 		double JL_AMNT = this.convertForeignToFunctionalCurrency(
 			 				adBankAccount.getGlFunctionalCurrency().getFcCode(),
 			 				adBankAccount.getGlFunctionalCurrency().getFcName(),
 			 				cmFundTransfer.getFtConversionDate(),
 			 				cmFundTransfer.getFtConversionRateFrom(),
 			 				cmDistributionRecord.getDrAmount(), AD_CMPNY);

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}
 			 	
 			 	Collection invADJDrs = invDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

 			 	j = invADJDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

 			 		double JL_AMNT = invDistributionRecord.getDrAmount();

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}
 			 	
 			 	Collection invBUADrs = invDistributionRecordHome.findUnpostedBuaByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);


 			 	j = invBUADrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

 			 		double JL_AMNT = invDistributionRecord.getDrAmount();

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}
 			 	
 			 	Collection invOHDrs = invDistributionRecordHome.findUnpostedOhByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

 			 	j = invOHDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

 			 		double JL_AMNT = invDistributionRecord.getDrAmount();

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}

 			 	Collection invSIDrs = invDistributionRecordHome.findUnpostedSiByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

 			 	j = invSIDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

 			 		double JL_AMNT = invDistributionRecord.getDrAmount();

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}

 			 	Collection invATRDrs = invDistributionRecordHome.findUnpostedAtrByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

 			 	j = invATRDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

 			 		double JL_AMNT = invDistributionRecord.getDrAmount();

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}
 			 	
 			 	Collection invSTDrs = invDistributionRecordHome.findUnpostedStByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 		 				glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

 			 	j = invSTDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

 			 		double JL_AMNT = invDistributionRecord.getDrAmount();

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}
 			 	
 			 	Collection invBSTDrs = invDistributionRecordHome.findUnpostedBstByDateRangeAndCoaAccountNumber(
 			 			glBeginningAccountingCalendarValue.getAcvDateFrom(),
 			 			glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

 			 	j = invBSTDrs.iterator();

 			 	while (j.hasNext()) {

 			 		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

 			 		double JL_AMNT = invDistributionRecord.getDrAmount();

 			 		if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
 			 				(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

 			 			COA_BLNC += JL_AMNT;

 			 		} else {

 			 			COA_BLNC -= JL_AMNT;

 			 		}

 			 	}
 			 	
 		 	}
 		 	
 		 	if (!DIS_SHW_ZRS && COA_BLNC == 0) continue;		 			 	 
 		 	
 		 	details.setDisBalance(COA_BLNC);
 		 	//System.out.println("COA_BLNC "+total);
 		 	total+=COA_BLNC;
 		 	list.add(details);
 		 	
 		 	
 		 }
 		 
 		 if (list.isEmpty()) {
 		 	
 		 	throw new GlobalNoRecordFoundException();
 		 	
 		 }
 		 
 		 // sort revenue and expense
 		 
 		 ArrayList glRevenueList =  new ArrayList();
          ArrayList glExpenseList = new ArrayList();
 		 
 		 Iterator j = list.iterator();
 		 
          while(j.hasNext()) {
          	
 	         GlRepDetailIncomeStatementDetails sortDetails = (GlRepDetailIncomeStatementDetails)j.next();
 	         
 			 if ("REVENUE".equals(sortDetails.getDisAccountType())) {
 			 	
 			    glRevenueList.add(sortDetails);
 			    
 			 } else if ("EXPENSE".equals(sortDetails.getDisAccountType())) {
 			 	
 			    glExpenseList.add(sortDetails);
 			    
 			 }
 			 
 	     }
 	      
 	     list = new ArrayList((Collection)glRevenueList);
 	     list.addAll((Collection)glExpenseList);

 		 return total;
 	  	  
 	  } catch (GlobalAccountNumberInvalidException ex) {
 	  	
 	  	  throw ex;
 	  	  
 	  } catch (GlobalNoRecordFoundException ex) {
 	  	
 	  	  throw ex;
 	  	
 	  } catch (Exception ex) {
 	  	
 	  	  Debug.printStackTrace(ex);
       	  throw new EJBException(ex.getMessage());
 	  	
 	  }

       

    }  
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

      Debug.print("GlRepDetailIncomeStatementControllerBean getGlReportableAcvAll");      
      
      LocalAdCompanyHome adCompanyHome = null;
            
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try {
          
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         
         AdCompanyDetails details = new AdCompanyDetails();
         details.setCmpName(adCompany.getCmpName());
         
         return details;  	
      	       	
      } catch (Exception ex) {
      	
      	 Debug.printStackTrace(ex);
      	 throw new EJBException(ex.getMessage());
      	  
      }
      
   }
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGenSgAll(Integer AD_CMPNY) {
   	
      Debug.print("GlRepDetailIncomeStatementControllerBean getGenSgAll");      
      
      LocalAdCompanyHome adCompanyHome = null;
      LocalGenSegmentHome genSegmentHome = null;
      
      ArrayList list = new ArrayList();
            
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	       genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try {
          
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         
         Collection genSegments = genSegmentHome.findByFlCode(adCompany.getGenField().getFlCode(), AD_CMPNY);
         
         Iterator i = genSegments.iterator();
         
         while (i.hasNext()) {
         	
         	LocalGenSegment genSegment = (LocalGenSegment)i.next();
         	
         	GenModSegmentDetails mdetails = new GenModSegmentDetails();
         	mdetails.setSgMaxSize(genSegment.getSgMaxSize());
         	mdetails.setSgFlSegmentSeparator(genSegment.getGenField().getFlSegmentSeparator());
         	
         	
         	list.add(mdetails);
         	
         }

      	 return list;
      	 
      } catch (Exception ex) {
      	
      	 Debug.printStackTrace(ex);
      	 throw new EJBException(ex.getMessage());
      	  
      }
      
   }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/   
   public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
   	throws GlobalNoRecordFoundException{
       
       Debug.print("AdRepBankAccountListControllerBean getAdBrResAll");
       
       LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
       LocalAdBranchHome adBranchHome = null;
       
       LocalAdBranchResponsibility adBranchResponsibility = null;
       LocalAdBranch adBranch = null;
       
       Collection adBranchResponsibilities = null;
       
       ArrayList list = new ArrayList();
       
       // Initialize EJB Home
       
       try {
           
           adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
           adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
           
       } catch (NamingException ex) {
           
           throw new EJBException(ex.getMessage());
           
       }
       
       try {
           
           adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
           
       } catch (FinderException ex) {
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       if (adBranchResponsibilities.isEmpty()) {
           
           throw new GlobalNoRecordFoundException();
           
       }
       
       try {
           
           Iterator i = adBranchResponsibilities.iterator();
           
           while(i.hasNext()) {
               
               adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
               
               adBranch = adBranchResponsibility.getAdBranch();
               
               AdBranchDetails details = new AdBranchDetails();
               
               details.setBrCode(adBranch.getBrCode());
               details.setBrBranchCode(adBranch.getBrBranchCode());
               details.setBrName(adBranch.getBrName());
               details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
               
               list.add(details);
               
           }	               
           
       } catch (Exception ex) {
           
           throw new EJBException(ex.getMessage());
       }
       
       return list;
       
   }	
   
   // private methods
   
   private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    Debug.print("GlRepDetailIncomeStatementControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT * CONVERSION_RATE;
             	
         } else if (CONVERSION_DATE != null) {
         	
         	 try {
         	 	    
         	 	 // Get functional currency rate
         	 	 
         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
         	     
         	     if (!FC_NM.equals("USD")) {
         	     	
        	         glReceiptFunctionalCurrencyRate = 
	     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
	     	             CONVERSION_DATE, AD_CMPNY);
	     	             
	     	         AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();
	     	             
         	     }
                 
                 // Get set of book functional currency rate if necessary
         	 	         
                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
                 
                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                             getFcCode(), CONVERSION_DATE, AD_CMPNY);
                     
                     AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();
                 
                  }
                                 
         	             
         	 } catch (Exception ex) {
         	 	
         	 	throw new EJBException(ex.getMessage());
         	 	
         	 }       
         	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
         
	}
	
  
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlRepDetailIncomeStatementControllerBean ejbCreate");
      
   }  
   
}
