package com.ejb.txn;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GenModSegmentDetails;
import com.util.GlModAccountingCalendarValueDetails;
import com.util.GlRepDetailTrialBalanceDetails;

/**
 * @ejb:bean name="GlRepDetailTrialBalanceControllerEJB"
 *           display-name="Used for generation of trial balance reports"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepDetailTrialBalanceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepDetailTrialBalanceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepDetailTrialBalanceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
 */

public class GlRepDetailTrialBalanceControllerBean extends AbstractSessionBean {


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlReportableAcvAll(Integer AD_CMPNY) {

		Debug.print("GlRepDetailTrialBalanceControllerBean getGlReportableAcvAll");      

		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);

			Iterator i = glSetOfBooks.iterator();

			while (i.hasNext()) {

				LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();

				Collection glAccountingCalendarValues = 
					glAccountingCalendarValueHome.findReportableAcvByAcCodeAndAcvStatus(
							glSetOfBook.getGlAccountingCalendar().getAcCode(), 'O', 'C', 'P', AD_CMPNY);

				Iterator j = glAccountingCalendarValues.iterator();

				while (j.hasNext()) {

					LocalGlAccountingCalendarValue glAccountingCalendarValue = 
						(LocalGlAccountingCalendarValue)j.next();

					GlModAccountingCalendarValueDetails mdetails = new GlModAccountingCalendarValueDetails();

					mdetails.setAcvPeriodPrefix(glAccountingCalendarValue.getAcvPeriodPrefix());

					// get year

					GregorianCalendar gc = new GregorianCalendar();
					gc.setTime(glAccountingCalendarValue.getAcvDateTo());

					mdetails.setAcvYear(gc.get(Calendar.YEAR));

					// is current

					gc = EJBCommon.getGcCurrentDateWoTime();

					if ((glAccountingCalendarValue.getAcvDateFrom().before(gc.getTime()) ||
							glAccountingCalendarValue.getAcvDateFrom().equals(gc.getTime())) &&
							(glAccountingCalendarValue.getAcvDateTo().after(gc.getTime()) ||
									glAccountingCalendarValue.getAcvDateTo().equals(gc.getTime()))) {

						mdetails.setAcvCurrent(true);

					}

					list.add(mdetails);

				}

			}

			return list;


		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeSpGlRepDetailTrialBalance(ResultSet rs, String AMNT_TYP, String DTB_ACCNT_NMBR_FRM, String DTB_ACCNT_NMBR_TO, String DTB_PRD, int DTB_YR,
			boolean DTB_INCLD_UNPSTD, boolean DTB_INCLD_UNPSTD_SL, boolean DTB_SHW_ZRS, ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

		Debug.print("GlRepDetailTrialBalanceControllerBean executeSpGlRepDetailTrialBalance");

		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalApVoucherHome apVoucherHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);    
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);    
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);    
			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);    
			invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);    
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);    
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);    
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);	  	
			Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(DTB_PRD, EJBCommon.getIntendedDate(DTB_YR), AD_CMPNY);
			ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);            
			LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
			LocalGenField genField = adCompany.getGenField();
			Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
			String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
			int genNumberOfSegment = genField.getFlNumberOfSegment();


			StringTokenizer stAccountFrom = new StringTokenizer(DTB_ACCNT_NMBR_FRM, strSeparator);
			StringTokenizer stAccountTo = new StringTokenizer(DTB_ACCNT_NMBR_TO, strSeparator);
			


			// validate if account number is valid

			if (stAccountFrom.countTokens() != genNumberOfSegment || 
					stAccountTo.countTokens() != genNumberOfSegment) {

				throw new GlobalAccountNumberInvalidException();

			}
			
			



			while (rs.next()) {
				
				GlRepDetailTrialBalanceDetails details = new GlRepDetailTrialBalanceDetails();
				
				String ACCNT_NMBR = rs.getString("ACCNT_NMBR");
				String ACCNT_SGMNT1 = rs.getString(2);
				String ACCNT_TYP = rs.getString("ACCNT_TYP"); 
				String ACCNT_DESC = rs.getString("ACCNT_DESC");
				Double DEBIT = rs.getDouble("DEBIT");
				Double CREDIT = rs.getDouble("CREDIT");
				
				
				double COA_BEG = 0d;
			      
			      LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue =
		 			glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
		 					glSetOfBook.getGlAccountingCalendar().getAcCode(),
						(short)1, AD_CMPNY);
			      
			      LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(ACCNT_NMBR, AD_CMPNY);
			      
			      
			      LocalGlChartOfAccountBalance glBeginningChartOfAccountBalance = 
				 			glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
				 					glBeginningAccountingCalendarValue.getAcvCode(),
								glChartOfAccount.getCoaCode(), AD_CMPNY);
					      
			      if (AMNT_TYP.equals("YTD")) COA_BEG = glBeginningChartOfAccountBalance.getCoabBeginningBalance();
			     
			       
			      
				details.setDtbAccountNumber(ACCNT_NMBR);
				details.setDtbAccountSegment1(ACCNT_SGMNT1);
				details.setDtbAccountDescription(ACCNT_DESC);
				details.setDtbAccountDescription1(ACCNT_DESC);
				details.setDtbDebit(DEBIT);
				details.setDtbCredit(CREDIT);
				
				
				if (COA_BEG > 0 && (glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE"))) {

					details.setDtbDebitBeg(Math.abs(COA_BEG));					

				} else if (COA_BEG < 0 && (glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE"))) {

					details.setDtbCreditBeg(Math.abs(COA_BEG));


				} else if (COA_BEG > 0 && (glChartOfAccount.getCoaAccountType().equals("LIABILITY") || glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY") || glChartOfAccount.getCoaAccountType().equals("REVENUE"))) {

					details.setDtbCreditBeg(Math.abs(COA_BEG));


				} else if (COA_BEG < 0 && (glChartOfAccount.getCoaAccountType().equals("LIABILITY") || glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY") || glChartOfAccount.getCoaAccountType().equals("REVENUE"))) {

					details.setDtbDebitBeg(Math.abs(COA_BEG));

				}
				
				System.out.println("DEBIT + details.getDtbDebitBeg()"+DEBIT + details.getDtbDebitBeg());
				System.out.println("CREDIT + details.getDtbCreditBeg()="+CREDIT + details.getDtbCreditBeg());
				
				
				System.out.println(ACCNT_NMBR+" DEBIT:"+DEBIT+ " CREDIT:"+CREDIT + " COA_BEG:"+COA_BEG);
				
				
				if((DEBIT == 0 && CREDIT == 0 && COA_BEG == 0) ||
						(DEBIT + details.getDtbDebitBeg() == CREDIT + details.getDtbCreditBeg())) {
					System.out.println("COA Removed : " + details.getDtbAccountNumber());
					continue;
				}
			
				list.add(details);
				

			}

			if (list.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}
			

			return list;

		} catch (GlobalAccountNumberInvalidException ex) {

			throw ex;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
		
		
		finally
		{
			
		}

	}
	
	
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeSpGlRepDetailTrialBalanceGroup(ResultSet rs, String AMNT_TYP, String DTB_ACCNT_NMBR_FRM, String DTB_ACCNT_NMBR_TO, String DTB_PRD, int DTB_YR,
			boolean DTB_INCLD_UNPSTD, boolean DTB_INCLD_UNPSTD_SL, boolean DTB_SHW_ZRS, ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

		Debug.print("GlRepDetailTrialBalanceControllerBean executeSpGlRepDetailTrialBalanceGroup");

		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalApVoucherHome apVoucherHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);    
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);    
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);    
			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);    
			invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);    
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);    
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);    
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);	  	
			Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(DTB_PRD, EJBCommon.getIntendedDate(DTB_YR), AD_CMPNY);
			ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);            
			LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
			LocalGenField genField = adCompany.getGenField();
			Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
			String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
			int genNumberOfSegment = genField.getFlNumberOfSegment();


			StringTokenizer stAccountFrom = new StringTokenizer(DTB_ACCNT_NMBR_FRM, strSeparator);
			StringTokenizer stAccountTo = new StringTokenizer(DTB_ACCNT_NMBR_TO, strSeparator);
			


			// validate if account number is valid

			if (stAccountFrom.countTokens() != genNumberOfSegment || 
					stAccountTo.countTokens() != genNumberOfSegment) {

				throw new GlobalAccountNumberInvalidException();

			}
			
			



			while (rs.next()) {
				
				GlRepDetailTrialBalanceDetails details = new GlRepDetailTrialBalanceDetails();
				
				String ACCNT_NMBR = rs.getString("ACCNT_NMBR");
				String ACCNT_SGMNT1 = rs.getString(2);
				String ACCNT_TYP = rs.getString("ACCNT_TYP"); 
				String ACCNT_DESC = rs.getString("ACCNT_DESC");
				Double DEBIT = rs.getDouble("DEBIT");
				Double CREDIT = rs.getDouble("CREDIT");
				

		      LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue =
	 			glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
	 					glSetOfBook.getGlAccountingCalendar().getAcCode(),
					(short)1, AD_CMPNY);
		      
		      LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(ACCNT_NMBR, AD_CMPNY);
		      
		    
		      double COA_BEG = 0d;
		      
		      if (AMNT_TYP.equals("YTD")){
		    	  
		    	  Collection glBeginningChartOfAccountBalances = glChartOfAccountBalanceHome.findByAcvCodeAndCoaSegment1(
			    		  glBeginningAccountingCalendarValue.getAcvCode(), glChartOfAccount.getCoaSegment5(), AD_CMPNY);
			      
					      Iterator x = glBeginningChartOfAccountBalances.iterator();
					      
					      while(x.hasNext()){
					    	  
					    	  LocalGlChartOfAccountBalance glChartOfAccountBalance = (LocalGlChartOfAccountBalance) x.next();
					    	  
					    	   COA_BEG += glChartOfAccountBalance.getCoabBeginningBalance();
					    	  
					      }
					
					
					
				}
		      
		      	

				details.setDtbAccountNumber(ACCNT_NMBR);
				details.setDtbAccountSegment1(ACCNT_SGMNT1);
				details.setDtbAccountDescription(ACCNT_DESC);
				details.setDtbAccountDescription1(ACCNT_DESC);
				details.setDtbDebit(DEBIT);
				details.setDtbCredit(CREDIT);
				
				
				if (COA_BEG > 0 && (glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE"))) {

					details.setDtbDebitBeg(Math.abs(COA_BEG));					

				} else if (COA_BEG < 0 && (glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE"))) {

					details.setDtbCreditBeg(Math.abs(COA_BEG));


				} else if (COA_BEG > 0 && (glChartOfAccount.getCoaAccountType().equals("LIABILITY") || glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY") || glChartOfAccount.getCoaAccountType().equals("REVENUE"))) {

					details.setDtbCreditBeg(Math.abs(COA_BEG));


				} else if (COA_BEG < 0 && (glChartOfAccount.getCoaAccountType().equals("LIABILITY") || glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY") || glChartOfAccount.getCoaAccountType().equals("REVENUE"))) {

					details.setDtbDebitBeg(Math.abs(COA_BEG));

				}
				System.out.println("DEBIT + details.getDtbDebitBeg()"+DEBIT + details.getDtbDebitBeg());
				System.out.println("CREDIT + details.getDtbCreditBeg()="+CREDIT + details.getDtbCreditBeg());
				
				
				System.out.println(ACCNT_NMBR+"-DEBIT:"+DEBIT+ "-CREDIT:"+CREDIT + "-COA_BEG:"+COA_BEG);
				
				
				if((DEBIT == 0 && CREDIT == 0 && COA_BEG == 0) ||
							(DEBIT + details.getDtbDebitBeg() == CREDIT + details.getDtbCreditBeg())) continue;
				
				list.add(details);

			}

			if (list.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}
			

			return list;

		} catch (GlobalAccountNumberInvalidException ex) {

			throw ex;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
		
		
		finally
		{
			
		}

	}
	

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeGlRepDetailTrialBalance(String DTB_ACCNT_NMBR_FRM, String DTB_ACCNT_NMBR_TO, String DTB_PRD, int DTB_YR,
			boolean DTB_INCLD_UNPSTD, boolean DTB_INCLD_UNPSTD_SL, boolean DTB_SHW_ZRS, ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

		Debug.print("GlRepDetailTrialBalanceControllerBean executeGlRepDetailTrialBalance");

		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalApVoucherHome apVoucherHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);    
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);    
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);    
			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);    
			invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);    
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);    
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);    
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);	  	
			Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(DTB_PRD, EJBCommon.getIntendedDate(DTB_YR), AD_CMPNY);
			ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);            
			LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
			LocalGenField genField = adCompany.getGenField();
			Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
			String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
			int genNumberOfSegment = genField.getFlNumberOfSegment();

			// get coa selected
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT DISTINCT OBJECT(coa) FROM GlChartOfAccount coa ");
			
			// add branch criteria

			if (branchList.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}
			else {

				jbossQl.append(", in (coa.adBranchCoas) bcoa WHERE bcoa.adBranch.brCode in (");

				boolean firstLoop = true;

				Iterator j = branchList.iterator();

				while(j.hasNext()) {

					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}

					AdBranchDetails mdetails = (AdBranchDetails) j.next();

					jbossQl.append(mdetails.getBrCode());

				}

				jbossQl.append(") AND ");

			}                    

			// loop accountfrom at to
			// check if account from or account to has wildcard if so then convert the * to 00 or ZZ
			// 0000-**, ZZZZ-**, 0000-00, ZZZZ-ZZ
			// DTB_ACCNT_NMBR_FRM = 0000-00
			// DTB_ACCNT_NMBR_FRM = ZZZZ-ZZ
			// flag if *
			
			boolean hasWildcard = false;
			StringTokenizer stAccountFrom = new StringTokenizer(DTB_ACCNT_NMBR_FRM, strSeparator);
			StringTokenizer stAccountTo = new StringTokenizer(DTB_ACCNT_NMBR_TO, strSeparator);
			
			String newDtbAccountFrom = "";
			String newDtbAccountTo = "";
			String tokenHolder = "";
			int wildcardPositions[] = new int[stAccountFrom.countTokens() + stAccountTo.countTokens()];
			int charCtr = 0;
			int arrayCtr = 0;
			int tokenCtr = 0;
			
			while(stAccountFrom.hasMoreTokens()) {
				
				tokenHolder = stAccountFrom.nextToken();
				charCtr = tokenHolder.length();
				
				if (tokenHolder.contains("*"))
				{
					hasWildcard = true;
					wildcardPositions[arrayCtr] = tokenCtr;
					while(charCtr != 0) {
						newDtbAccountFrom += "0";
						charCtr--;
					}
				}
				else
				{
					wildcardPositions[arrayCtr] = -1;
					newDtbAccountFrom += tokenHolder;
				}
				
				if(stAccountFrom.hasMoreTokens())
					newDtbAccountFrom += strSeparator;
				
				tokenCtr++;
				arrayCtr++;
			}
			
			tokenCtr = 0;
			
			while(stAccountTo.hasMoreTokens()){
				
				tokenHolder = stAccountTo.nextToken();
				charCtr = tokenHolder.length();				
				
				if (tokenHolder.contains("*"))
				{
					hasWildcard = true;
					wildcardPositions[arrayCtr] = tokenCtr;
					while(charCtr != 0) {
						newDtbAccountTo += "Z";
						charCtr--;
					}
				}
				else
				{
					wildcardPositions[arrayCtr] = -1;
					newDtbAccountTo += tokenHolder;
				}
				
				if(stAccountTo.hasMoreTokens())
					newDtbAccountTo += strSeparator;
				
				tokenCtr++;
				arrayCtr++;
			}
			
			stAccountFrom = new StringTokenizer(newDtbAccountFrom, strSeparator);
			stAccountTo = new StringTokenizer(newDtbAccountTo, strSeparator);

			// validate if account number is valid

			if (stAccountFrom.countTokens() != genNumberOfSegment || 
					stAccountTo.countTokens() != genNumberOfSegment) {

				throw new GlobalAccountNumberInvalidException();

			}

			try {

				String var = "1";

				if (genNumberOfSegment > 1) {

					for (int i=0; i<genNumberOfSegment; i++) {

						if (i == 0 && i < genNumberOfSegment - 1) {

							// for first segment

							jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");

							var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)+1 ";


						} else if (i != 0 && i < genNumberOfSegment - 1){     		

							// for middle segments

							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")) - (" + var +")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");

							var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";

						} else if (i != 0 && i == genNumberOfSegment - 1) {

							// for last segment

							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' ");


						}	     		       	      	   	            

					}

				} else if(genNumberOfSegment == 1){

					String accountFrom = stAccountFrom.nextToken();
					String accountTo = stAccountTo.nextToken();

					jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + accountFrom + "' AND '" + accountTo + "' OR " +
							"coa.coaAccountNumber BETWEEN '" + accountFrom + "' AND '" + accountTo + "' ");

				}

				jbossQl.append("AND coa.coaEnable=1 AND coa.coaAdCompany=" + AD_CMPNY + " ");

			} catch (NumberFormatException ex) {

				throw new GlobalAccountNumberInvalidException();

			}

			// generate order by coa natural account

			short accountSegmentNumber = 0;

			try {

				LocalGenSegment genSegment = genSegmentHome.findByFlCodeAndSegmentType(genField.getFlCode(), 'N', AD_CMPNY);
				accountSegmentNumber = genSegment.getSgSegmentNumber();

			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}


			jbossQl.append("ORDER BY coa.coaSegment" + accountSegmentNumber + ", coa.coaAccountNumber ");

			Object obj[] = new Object[0];

			System.out.println("jbossQl="+jbossQl.toString());
			
			Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), obj);

			Iterator i = glChartOfAccounts.iterator();	

			double DTB_NT_INCM_PREV = 0d;
			double DTB_RVNUE = 0d;
			double DTB_EXPNS = 0d;

			while (i.hasNext()) {

				LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)i.next();

				GlRepDetailTrialBalanceDetails details = new GlRepDetailTrialBalanceDetails();		 			 	 	 	 

				double COA_BEG = 0d;
				double COA_BLNC = 0d;
				double COA_BLNC_PREV = 0d;
				
				
				
				// check if flag is true, if so then convert the segment to *
				//glChartOfAccount.getCoaAccountNumber() = '1020-01', '1020-**'

				if (hasWildcard)
				{
					String accountNumber = "";
					String accountDescription = "";
					String accountSegment1 = glChartOfAccount.getCoaSegment1();
					StringTokenizer stAccountNumber = new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), strSeparator);
					StringTokenizer stAccountDescription = new StringTokenizer(glChartOfAccount.getCoaAccountDescription(), strSeparator);
					tokenCtr = 0;
					
					while(stAccountNumber.hasMoreTokens()) {
						
						boolean isFound = false;
						
						tokenHolder = stAccountNumber.nextToken();
						charCtr = tokenHolder.length();
						
						for(arrayCtr=0; arrayCtr<wildcardPositions.length; arrayCtr++) {
							if(wildcardPositions[arrayCtr] == tokenCtr) {
								isFound = true;
								break;
							}
						}

						if(isFound) {
							while(charCtr != 0) {
								accountNumber += "*";
								charCtr--;
							}
						} 
						else 
							accountNumber += tokenHolder;
						
						if(stAccountNumber.hasMoreTokens())
							accountNumber += strSeparator;
						
						tokenCtr++;
					}
					details.setDtbAccountNumber(accountNumber);
					details.setDtbAccountSegment1(accountSegment1);
					
					tokenCtr = 0;
					while(stAccountDescription.hasMoreTokens()) {
						
						boolean isFound = false;
						
						tokenHolder = stAccountDescription.nextToken();
						charCtr = tokenHolder.length();
						
						for(arrayCtr=0; arrayCtr<wildcardPositions.length; arrayCtr++) {
							if(wildcardPositions[arrayCtr] == tokenCtr) {
								isFound = true;
								break;
							}
						}

						if(isFound) {
							accountDescription += "*";
						} 
						else 
							accountDescription += tokenHolder;
						
						if(stAccountDescription.hasMoreTokens())
							accountDescription += strSeparator;
						
						tokenCtr++;
					}
					
					details.setDtbAccountDescription(accountDescription);
				}
				else
				{
					details.setDtbAccountNumber(glChartOfAccount.getCoaAccountNumber());
					details.setDtbAccountSegment1(glChartOfAccount.getCoaSegment1());
					details.setDtbAccountDescription(glChartOfAccount.getCoaAccountDescription());
				}

				// get coa debit or credit	in coa balance		 			    		 	 		 	 	 	 
				
				LocalGlAccountingCalendarValue glAccountingCalendarValue =
					glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
							glSetOfBook.getGlAccountingCalendar().getAcCode(),
							DTB_PRD, AD_CMPNY);
				System.out.print("glAccountingCalendarValue.getAcvCode()="+glAccountingCalendarValue.getAcvCode());

				LocalGlChartOfAccountBalance glChartOfAccountBalance = 
					glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
							glAccountingCalendarValue.getAcvCode(),
							glChartOfAccount.getCoaCode(), AD_CMPNY);
				System.out.print("glChartOfAccountBalance.getCoabCode()="+glChartOfAccountBalance.getCoabCode());
				System.out.print("glChartOfAccount.getCoaCode()="+glChartOfAccount.getCoaCode());

				// get beg

				COA_BLNC = glChartOfAccountBalance.getCoabEndingBalance();
				COA_BEG = glChartOfAccountBalance.getCoabBeginningBalance();
				
				System.out.println("COA_BLNC="+COA_BLNC);
				System.out.println("COA_BEG="+COA_BEG);

				
				// get coa debit or credit balance in unposted journals if necessary

				LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue =
					glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
							glSetOfBook.getGlAccountingCalendar().getAcCode(),
							(short)1, AD_CMPNY);

				GregorianCalendar endGc = new GregorianCalendar();
				endGc.setTime(glBeginningAccountingCalendarValue.getAcvDateFrom());
				endGc.add(Calendar.DATE, -1);
				System.out.println("COA_BLNC=" + COA_BLNC);

				if (DTB_INCLD_UNPSTD) {

					Collection glJournalLines = new ArrayList();
					Collection glJournalLinesPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						glJournalLines = glJournalLineHome.findUnpostedJlByJrEffectiveDateAndCoaCode(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_CMPNY);

					} else {

						glJournalLines = glJournalLineHome.findUnpostedJlByJrEffectiveDateRangeAndCoaCode(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_CMPNY);

						glJournalLinesPrev = glJournalLineHome.findUnpostedJlByJrEffectiveDateAndCoaCode(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_CMPNY);

					}

					Iterator j = glJournalLines.iterator();

					while (j.hasNext()) {

						LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();

						LocalGlJournal glJournal = glJournalLine.getGlJournal();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								glJournal.getGlFunctionalCurrency().getFcCode(),
								glJournal.getGlFunctionalCurrency().getFcName(),
								glJournal.getJrConversionDate(),
								glJournal.getJrConversionRate(),
								glJournalLine.getJlAmount(), AD_CMPNY);		

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) &&
								glJournalLine.getJlDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") &&
										glJournalLine.getJlDebit() == EJBCommon.FALSE)) {

							COA_BLNC += EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						} else {

							COA_BLNC -= EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						}

					}

					j = glJournalLinesPrev.iterator();

					while (j.hasNext()) {

						LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();

						LocalGlJournal glJournal = glJournalLine.getGlJournal();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								glJournal.getGlFunctionalCurrency().getFcCode(),
								glJournal.getGlFunctionalCurrency().getFcName(),
								glJournal.getJrConversionDate(),
								glJournal.getJrConversionRate(),
								glJournalLine.getJlAmount(), AD_CMPNY);		 			

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) &&
								glJournalLine.getJlDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") &&
										glJournalLine.getJlDebit() == EJBCommon.FALSE)) {

							COA_BLNC_PREV += EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						} else {

							COA_BLNC_PREV -= EJBCommon.roundIt(JL_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

						}

					}

				}

				// include unposted subledger transactions

				if (DTB_INCLD_UNPSTD_SL) {

					// ar invoice
					Collection arINVDrs = new ArrayList();
					Collection arINVDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						arINVDrs = arDistributionRecordHome.findUnpostedInvByDateAndCoaAccountNumber(
								EJBCommon.FALSE, glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);

					} else {

						arINVDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
								EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						arINVDrsPrev = arDistributionRecordHome.findUnpostedInvByDateAndCoaAccountNumber(
								EJBCommon.FALSE, endGc.getTime(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);

					}

					Iterator j = arINVDrs.iterator();

					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(),
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = arINVDrsPrev.iterator();

					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(),
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// ar credit memo
					Collection arCMDrs = new ArrayList();
					Collection arCMDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						arCMDrs = arDistributionRecordHome.findUnpostedInvByDateAndCoaAccountNumber(
								EJBCommon.TRUE, glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);	

					} else {

						arCMDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
								EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

						arCMDrsPrev = arDistributionRecordHome.findUnpostedInvByDateAndCoaAccountNumber(
								EJBCommon.TRUE, endGc.getTime(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);	

					}

					j = arCMDrs.iterator();

					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArInvoice arCreditMemo = arDistributionRecord.getArInvoice();

						LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
								arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(), 
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = arCMDrsPrev.iterator();

					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArInvoice arCreditMemo = arDistributionRecord.getArInvoice();

						LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
								arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arInvoice.getGlFunctionalCurrency().getFcCode(),
								arInvoice.getGlFunctionalCurrency().getFcName(), 
								arInvoice.getInvConversionDate(),
								arInvoice.getInvConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// ar receipt
					Collection arRCTDrs = new ArrayList();
					Collection arRCTDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						arRCTDrs = arDistributionRecordHome.findUnpostedRctByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

					} else {

						arRCTDrs = arDistributionRecordHome.findUnpostedRctByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

						arRCTDrsPrev = arDistributionRecordHome.findUnpostedRctByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

					}

					j = arRCTDrs.iterator();

					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArReceipt arReceipt = arDistributionRecord.getArReceipt();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arReceipt.getGlFunctionalCurrency().getFcCode(),
								arReceipt.getGlFunctionalCurrency().getFcName(),
								arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = arRCTDrsPrev.iterator();

					while (j.hasNext()) {

						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

						LocalArReceipt arReceipt = arDistributionRecord.getArReceipt();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								arReceipt.getGlFunctionalCurrency().getFcCode(),
								arReceipt.getGlFunctionalCurrency().getFcName(),
								arReceipt.getRctConversionDate(),
								arReceipt.getRctConversionRate(),
								arDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// ap voucher
					Collection apVOUDrs = new ArrayList();
					Collection apVOUDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						apVOUDrs = apDistributionRecordHome.findUnpostedVouByDateAndCoaAccountNumber(
								EJBCommon.FALSE, glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);

					} else {

						apVOUDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
								EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						apVOUDrsPrev = apDistributionRecordHome.findUnpostedVouByDateAndCoaAccountNumber(
								EJBCommon.FALSE, endGc.getTime(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);

					}

					j = apVOUDrs.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = apVOUDrsPrev.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// ap debit memo
					Collection apDMDrs = new ArrayList();
					Collection apDMDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						apDMDrs = apDistributionRecordHome.findUnpostedVouByDateAndCoaAccountNumber(
								EJBCommon.TRUE, glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);	

					} else {

						apDMDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
								EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

						apDMDrsPrev = apDistributionRecordHome.findUnpostedVouByDateAndCoaAccountNumber(
								EJBCommon.TRUE, endGc.getTime(), glChartOfAccount.getCoaCode(), 
								AD_BRNCH, AD_CMPNY);	

					}

					j = apDMDrs.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApVoucher apDebitMemo = apDistributionRecord.getApVoucher();

						LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
								apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = apDMDrsPrev.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApVoucher apDebitMemo = apDistributionRecord.getApVoucher();

						LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
								apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apVoucher.getGlFunctionalCurrency().getFcCode(),
								apVoucher.getGlFunctionalCurrency().getFcName(),
								apVoucher.getVouConversionDate(),
								apVoucher.getVouConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// ap check
					Collection apCHKDrs = new ArrayList();
					Collection apCHKDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						apCHKDrs = apDistributionRecordHome.findUnpostedChkByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

					} else {

						apCHKDrs = apDistributionRecordHome.findUnpostedChkByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

						apCHKDrsPrev = apDistributionRecordHome.findUnpostedChkByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);	

					}

					j = apCHKDrs.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApCheck apCheck = apDistributionRecord.getApCheck();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apCheck.getGlFunctionalCurrency().getFcCode(),
								apCheck.getGlFunctionalCurrency().getFcName(),
								apCheck.getChkConversionDate(),
								apCheck.getChkConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = apCHKDrsPrev.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApCheck apCheck = apDistributionRecord.getApCheck();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apCheck.getGlFunctionalCurrency().getFcCode(),
								apCheck.getGlFunctionalCurrency().getFcName(),
								apCheck.getChkConversionDate(),
								apCheck.getChkConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// ap purchase order
					Collection apPODrs = new ArrayList();
					Collection apPODrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						apPODrs = apDistributionRecordHome.findUnpostedPoByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						apPODrs = apDistributionRecordHome.findUnpostedPoByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						apPODrsPrev = apDistributionRecordHome.findUnpostedPoByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = apPODrs.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApPurchaseOrder apPurchaseOrder = apDistributionRecord.getApPurchaseOrder();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
								apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
								apPurchaseOrder.getPoConversionDate(),
								apPurchaseOrder.getPoConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = apPODrsPrev.iterator();

					while (j.hasNext()) {

						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

						LocalApPurchaseOrder apPurchaseOrder = apDistributionRecord.getApPurchaseOrder();

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
								apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
								apPurchaseOrder.getPoConversionDate(),
								apPurchaseOrder.getPoConversionRate(),
								apDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// cm adjustment
					Collection cmADJDrs = new ArrayList();
					Collection cmADJDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						cmADJDrs = cmDistributionRecordHome.findUnpostedAdjByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						cmADJDrs = cmDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						cmADJDrsPrev = cmDistributionRecordHome.findUnpostedAdjByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = cmADJDrs.iterator();

					while (j.hasNext()) {

						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

						LocalCmAdjustment cmAdjustment = cmDistributionRecord.getCmAdjustment();

						LocalAdBankAccount adBankAccount = cmAdjustment.getAdBankAccount();

						//LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getFtAdBaAccountFrom());

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								adBankAccount.getGlFunctionalCurrency().getFcCode(),
								adBankAccount.getGlFunctionalCurrency().getFcName(),
								cmAdjustment.getAdjConversionDate(),
								cmAdjustment.getAdjConversionRate(),
								cmDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = cmADJDrsPrev.iterator();

					while (j.hasNext()) {

						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

						LocalCmAdjustment cmAdjustment = cmDistributionRecord.getCmAdjustment();

						LocalAdBankAccount adBankAccount = cmAdjustment.getAdBankAccount();

						//LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getFtAdBaAccountFrom());

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								adBankAccount.getGlFunctionalCurrency().getFcCode(),
								adBankAccount.getGlFunctionalCurrency().getFcName(),
								cmAdjustment.getAdjConversionDate(),
								cmAdjustment.getAdjConversionRate(),
								cmDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// cm fund transfer
					Collection cmFTDrs = new ArrayList();
					Collection cmFTDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						cmFTDrs = cmDistributionRecordHome.findUnpostedFtByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY); 	     			

					} else {

						cmFTDrs = cmDistributionRecordHome.findUnpostedFtByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY); 	     			

						cmFTDrsPrev = cmDistributionRecordHome.findUnpostedFtByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY); 	     			

					}

					j = cmFTDrs.iterator();

					while (j.hasNext()) {

						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

						LocalCmFundTransfer cmFundTransfer = cmDistributionRecord.getCmFundTransfer();

						LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								adBankAccount.getGlFunctionalCurrency().getFcCode(),
								adBankAccount.getGlFunctionalCurrency().getFcName(),
								cmFundTransfer.getFtConversionDate(),
								cmFundTransfer.getFtConversionRateFrom(),
								cmDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = cmFTDrsPrev.iterator();

					while (j.hasNext()) {

						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

						LocalCmFundTransfer cmFundTransfer = cmDistributionRecord.getCmFundTransfer();

						LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

						double JL_AMNT = this.convertForeignToFunctionalCurrency(
								adBankAccount.getGlFunctionalCurrency().getFcCode(),
								adBankAccount.getGlFunctionalCurrency().getFcName(),
								cmFundTransfer.getFtConversionDate(),
								cmFundTransfer.getFtConversionRateFrom(),
								cmDistributionRecord.getDrAmount(), AD_CMPNY);

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// inv adjustment
					Collection invADJDrs = new ArrayList();
					Collection invADJDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invADJDrs = invDistributionRecordHome.findUnpostedAdjByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invADJDrs = invDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invADJDrsPrev = invDistributionRecordHome.findUnpostedAdjByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invADJDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = invADJDrsPrev.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// inv build/unbuild assembly
					Collection invBUADrs = new ArrayList();
					Collection invBUADrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invBUADrs = invDistributionRecordHome.findUnpostedBuaByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invBUADrs = invDistributionRecordHome.findUnpostedBuaByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invBUADrsPrev = invDistributionRecordHome.findUnpostedBuaByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invBUADrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = invBUADrsPrev.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// inv overhead
					Collection invOHDrs = new ArrayList();
					Collection invOHDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invOHDrs = invDistributionRecordHome.findUnpostedOhByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invOHDrs = invDistributionRecordHome.findUnpostedOhByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invOHDrsPrev = invDistributionRecordHome.findUnpostedOhByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invOHDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = invOHDrsPrev.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// inv stock issuance
					Collection invSIDrs = new ArrayList();
					Collection invSIDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invSIDrs = invDistributionRecordHome.findUnpostedSiByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invSIDrs = invDistributionRecordHome.findUnpostedSiByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invSIDrsPrev = invDistributionRecordHome.findUnpostedSiByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invSIDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = invSIDrsPrev.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// inv assembly transfer
					Collection invATRDrs = new ArrayList();
					Collection invATRDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invATRDrs = invDistributionRecordHome.findUnpostedAtrByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invATRDrs = invDistributionRecordHome.findUnpostedAtrByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invATRDrsPrev = invDistributionRecordHome.findUnpostedAtrByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invATRDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = invATRDrsPrev.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// inv stock transfer
					Collection invSTDrs = new ArrayList();
					Collection invSTDrsPrev = new ArrayList();

					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invSTDrs = invDistributionRecordHome.findUnpostedStByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invSTDrs = invDistributionRecordHome.findUnpostedStByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invSTDrsPrev = invDistributionRecordHome.findUnpostedStByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invSTDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = invSTDrsPrev.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

					// inv branch stock transfer
					Collection invBSTDrs = new ArrayList();
					Collection invBSTDrsPrev = new ArrayList();
					System.out.println("INV BRANCH STOCK TRANSFER");
					if(glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("LIABILITY") || 
							glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {

						invBSTDrs = invDistributionRecordHome.findUnpostedBstByDateAndCoaAccountNumber(
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					} else {

						invBSTDrs = invDistributionRecordHome.findUnpostedBstByDateRangeAndCoaAccountNumber(
								glBeginningAccountingCalendarValue.getAcvDateFrom(),
								glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

						invBSTDrsPrev = invDistributionRecordHome.findUnpostedBstByDateAndCoaAccountNumber(
								endGc.getTime(), glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

					}

					j = invBSTDrs.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC += JL_AMNT;

						} else {

							COA_BLNC -= JL_AMNT;

						}

					}

					j = invBSTDrsPrev.iterator();

					while (j.hasNext()) {

						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

						double JL_AMNT = invDistributionRecord.getDrAmount();

						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {	

							COA_BLNC_PREV += JL_AMNT;

						} else {

							COA_BLNC_PREV -= JL_AMNT;

						}

					}

				}

				if (COA_BLNC > 0 && (glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE"))) {

					details.setDtbDebit(Math.abs(COA_BLNC));					

				} else if (COA_BLNC < 0 && (glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE"))) {

					details.setDtbCredit(Math.abs(COA_BLNC));


				} else if (COA_BLNC > 0 && (glChartOfAccount.getCoaAccountType().equals("LIABILITY") || glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY") || glChartOfAccount.getCoaAccountType().equals("REVENUE"))) {

					details.setDtbCredit(Math.abs(COA_BLNC));


				} else if (COA_BLNC < 0 && (glChartOfAccount.getCoaAccountType().equals("LIABILITY") || glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY") || glChartOfAccount.getCoaAccountType().equals("REVENUE"))) {

					details.setDtbDebit(Math.abs(COA_BLNC));

				}
				
				
				//***********************
				
				if (COA_BEG > 0 && (glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE"))) {

					details.setDtbDebitBeg(Math.abs(COA_BEG));					

				} else if (COA_BEG < 0 && (glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE"))) {

					details.setDtbCreditBeg(Math.abs(COA_BEG));


				} else if (COA_BEG > 0 && (glChartOfAccount.getCoaAccountType().equals("LIABILITY") || glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY") || glChartOfAccount.getCoaAccountType().equals("REVENUE"))) {

					details.setDtbCreditBeg(Math.abs(COA_BEG));


				} else if (COA_BEG < 0 && (glChartOfAccount.getCoaAccountType().equals("LIABILITY") || glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY") || glChartOfAccount.getCoaAccountType().equals("REVENUE"))) {

					details.setDtbDebitBeg(Math.abs(COA_BEG));

				}

				
			

				// add to prev net income
				if (glChartOfAccount.getCoaAccountType().equals("REVENUE")) {

					DTB_NT_INCM_PREV += COA_BLNC_PREV;
					DTB_RVNUE += COA_BLNC_PREV;

				} else if (glChartOfAccount.getCoaAccountType().equals("EXPENSE")) {

					DTB_NT_INCM_PREV -= COA_BLNC_PREV;
					DTB_EXPNS += COA_BLNC_PREV;

				}
				
				if (!DTB_SHW_ZRS && COA_BEG==0 && COA_BLNC == 0 ) continue;
				
				list.add(details);

			}

			if (list.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}
			System.out.println("DTB_RVNUE: " +DTB_RVNUE);
			System.out.println("DTB_EXPNS: " +DTB_EXPNS);
			System.out.println("DTB_NT_INCM_PREV: " +DTB_NT_INCM_PREV);

			// add prev net income to retained earnings
			
			if (DTB_INCLD_UNPSTD || DTB_INCLD_UNPSTD_SL) {

				try {

					LocalGlChartOfAccount glRetainedEarningsCoa = 
						glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);

					i = list.iterator();

					while (i.hasNext()) {

						GlRepDetailTrialBalanceDetails details = (GlRepDetailTrialBalanceDetails) i.next();

						if(!details.getDtbAccountNumber().equals(glRetainedEarningsCoa.getCoaAccountNumber())) continue;

						System.out.println("DTB_NT_INCM_PREV" + DTB_NT_INCM_PREV);
						System.out.println("details.getDtbCredit(" + details.getDtbCredit());
						System.out.println("details.getDtbDebit()" + details.getDtbDebit());
						double sum = DTB_NT_INCM_PREV + details.getDtbCredit() - details.getDtbDebit();
						System.out.println("sum" + sum);
						if (sum > 0) {
							details.setDtbDebit(0);
							details.setDtbCredit(Math.abs(sum));
						} else {
							details.setDtbDebit(Math.abs(sum));
							details.setDtbCredit(0);
						}					 
					}

				} catch(FinderException ex) {

					throw new GlobalAccountNumberInvalidException();

				}

			}

			return list;

		} catch (GlobalAccountNumberInvalidException ex) {

			throw ex;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
		
		
		finally
		{
			
		}

	}
	
	
	
	/**
	    * @ejb:interface-method view-type="remote"
	    * @jboss:method-attributes read-only="true"
	    **/
	    public void executeGlRecomputeCoaBalance(String GL_ACCNT_NMBR_FRM, String GL_ACCNT_NMBR_TO, 
	    	  Integer AD_CMPNY)
	       throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

	       Debug.print("GlRepGeneralLedgerControllerBean executeGlRecomputeCoaBalance");
	       
	       
	       LocalGlSetOfBookHome glSetOfBookHome = null;
	       LocalGlChartOfAccountHome glChartOfAccountHome = null;
	       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
	       LocalGenSegmentHome genSegmentHome = null;
	       LocalAdCompanyHome adCompanyHome = null;
	       LocalGlJournalLineHome glJournalLineHome = null;
	      
	       
	       ArrayList list = new ArrayList();
	       
	 	   // Initialize EJB Home
	 	    
	 	  try {
	 	        
	 	       glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
	 	           lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
	 	       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	 	           lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	 	       glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
	 	           lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
	 	       genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
	 	           lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
	 	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	 	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	 	       glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
	 	           lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);    
	 	       
	 	       
	 	  } catch (NamingException ex) {
	 	        
	 	      throw new EJBException(ex.getMessage());
	 	        
	 	  }
	 	  
	 	  try {
	 	  	
	 	  	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	 	  	  
	 	  	  
	 	  	  LocalGenField genField = adCompany.getGenField();
	 	  	  Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
	 	      String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
	 	      int genNumberOfSegment = genField.getFlNumberOfSegment();
	 	      
	 	      
	 	      StringTokenizer stAccountFrom = new StringTokenizer(GL_ACCNT_NMBR_FRM, strSeparator);
	           StringTokenizer stAccountTo = new StringTokenizer(GL_ACCNT_NMBR_TO, strSeparator);
	       
	           
	 	      if (stAccountFrom.countTokens() != genNumberOfSegment || 
	 	  	         stAccountTo.countTokens() != genNumberOfSegment) {
	 	  	      	
	   	      	  throw new GlobalAccountNumberInvalidException();
	   	      	
	   	      }
	 	      
	 	      
	 	      Collection journalLines = glJournalLineHome.findPostedJlByJrEffectiveDateAndCoaAccountNumberRange(
	 	    		  GL_ACCNT_NMBR_FRM, GL_ACCNT_NMBR_TO, AD_CMPNY);
	 	      
	 	      
		     Iterator i = journalLines.iterator(); 	

	 	      while(i.hasNext()){
	 	    	  
	 	    	  LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

	 	    	  String ACCNT_NMBR = glJournalLine.getGlChartOfAccount().getCoaAccountNumber();
	 	    	  String ACCNT_TYP = glJournalLine.getGlChartOfAccount().getCoaAccountType();
	 	    	  Date TRANS_DATE = glJournalLine.getGlJournal().getJrEffectiveDate();
	 	    	  Integer AD_BRNCH = glJournalLine.getGlJournal().getJrAdBranch();
	 	    	
	 	    	  byte DBT = glJournalLine.getJlDebit();
	 	    	  Double AMOUNT = glJournalLine.getJlAmount();

	 	    	  
	 	    		
	 	    	  // validate if period is closed       	  	 
		  	     	
	 		       LocalGlSetOfBook glSetOfBook = glSetOfBookHome.findByDate(TRANS_DATE, AD_CMPNY);
	 		     	
	 		       LocalGlAccountingCalendarValue glAccountingCalendarValue =
	 		     	   glAccountingCalendarValueHome.findByAcCodeAndDate(
	 		     	     	glSetOfBook.getGlAccountingCalendar().getAcCode(),
	 		     	     	TRANS_DATE, AD_CMPNY);

	 		      LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(ACCNT_NMBR, AD_CMPNY);
	 		      
	 		      
	 		    
				 // post current to current acv
				   
				 this.post(glAccountingCalendarValue,
						 glChartOfAccount,
				     true, DBT, AMOUNT, AD_CMPNY);
				
				
				 // post to subsequent acvs (propagate)
				
				 Collection glSubsequentAccountingCalendarValues = 
				     glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
				         glSetOfBook.getGlAccountingCalendar().getAcCode(),
				         glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
				       
				 Iterator j = glSubsequentAccountingCalendarValues.iterator();
				
				 while (j.hasNext()) {
					
					   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
					       (LocalGlAccountingCalendarValue)j.next();
					       
					   this.post(glSubsequentAccountingCalendarValue,
							   glChartOfAccount,
					       false, DBT, AMOUNT, AD_CMPNY);
						         	
				 }
				 	           
				 // post to subsequent years if necessary
				 
				 Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
				 
				
				  	if (!glSubsequentSetOfBooks.isEmpty() && glSetOfBook.getSobYearEndClosed() == 1) {
				  	
					  	LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
				  	
					  	Iterator sobIter = glSubsequentSetOfBooks.iterator();
					  	
					  	while (sobIter.hasNext()) {
					  		
					  		LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
					  		
					  		
					  		// post to subsequent acvs of subsequent set of book(propagate)
				
				         Collection glAccountingCalendarValues = 
				             glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
				               
				         Iterator acvIter = glAccountingCalendarValues.iterator();
				       
				         while (acvIter.hasNext()) {
				       	
				       	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
				       	       (LocalGlAccountingCalendarValue)acvIter.next();
				       	       
				       	    if (ACCNT_TYP.equals("ASSET") || ACCNT_TYP.equals("LIABILITY") ||
				       	    		ACCNT_TYP.equals("OWNERS EQUITY")) {
				       	       
						         	this.post(glSubsequentAccountingCalendarValue,
						         			glChartOfAccount,
						         	     false, DBT, AMOUNT, AD_CMPNY);
						         	     
						        } else { // revenue & expense
						        					             					             
						             this.post(glSubsequentAccountingCalendarValue,
						         	     glRetainedEarningsAccount,
						         	     false, DBT, AMOUNT, AD_CMPNY);					        
						        
						        }
				       		         	
				         }
					  		
					  	   if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
					  		
					  	}
					  	
				  	}
	 	    	  
	 	    	  
	 	      }
	 	      
	 	      
	 	      
	 	     
	 	  	  
	 	  } catch (GlobalAccountNumberInvalidException ex) {
	 	  	
	 	  	  throw ex;
	 	  	  
	 	 	
	 	  } catch (Exception ex) {
	 	  	
	 	  	  Debug.printStackTrace(ex);
	       	  throw new EJBException(ex.getMessage());
	 	  	
	 	  }
	 	

	    }

	    
	    
	    
	    
	    
/**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public void executeSpGlRecomputeCoaBalance(ResultSet rs, String GL_ACCNT_NMBR_FRM, String GL_ACCNT_NMBR_TO,
    	  Integer AD_CMPNY)
       throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

       Debug.print("GlRepGeneralLedgerControllerBean executeSpGlRecomputeCoaBalance");
       
       
       LocalGlSetOfBookHome glSetOfBookHome = null;
       LocalGlChartOfAccountHome glChartOfAccountHome = null;
       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
       LocalGenSegmentHome genSegmentHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       LocalGlJournalLineHome glJournalLineHome = null;
      
       
       ArrayList list = new ArrayList();
       
 	   // Initialize EJB Home
 	    
 	  try {
 	        
 	       glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
 	       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
 	       glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
 	       genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
 	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
 	       glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);    
 	       
 	       
 	  } catch (NamingException ex) {
 	        
 	      throw new EJBException(ex.getMessage());
 	        
 	  }
 	  
 	  try {
 	  	
 	  	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 	  	  
 	  	  
 	  	  LocalGenField genField = adCompany.getGenField();
 	  	  Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
 	      String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
 	      int genNumberOfSegment = genField.getFlNumberOfSegment();
 	      
 	      
 	      StringTokenizer stAccountFrom = new StringTokenizer(GL_ACCNT_NMBR_FRM, strSeparator);
           StringTokenizer stAccountTo = new StringTokenizer(GL_ACCNT_NMBR_TO, strSeparator);
       
           
 	      if (stAccountFrom.countTokens() != genNumberOfSegment || 
 	  	         stAccountTo.countTokens() != genNumberOfSegment) {
 	  	      	
   	      	  throw new GlobalAccountNumberInvalidException();
   	      	
   	      }
 	      
 	      
 	      
	     while (rs.next()) {
	    	  
	    	  String ACCNT_NMBR = rs.getString("ACCNT_NMBR");
	    	  String ACCNT_TYP = rs.getString("ACCNT_TYP");
	    	  Date TRANS_DATE = rs.getDate("DATE");
	    	  byte DEBIT = rs.getByte("DEBIT");
	    	  Double AMOUNT = rs.getDouble("AMOUNT");
	    

	    	  // validate if period is closed       	  	 
	  	     	
		       LocalGlSetOfBook glSetOfBook = glSetOfBookHome.findByDate(TRANS_DATE, AD_CMPNY);
		     	
		       LocalGlAccountingCalendarValue glAccountingCalendarValue =
		     	   glAccountingCalendarValueHome.findByAcCodeAndDate(
		     	     	glSetOfBook.getGlAccountingCalendar().getAcCode(),
		     	     	TRANS_DATE, AD_CMPNY);

		      LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(ACCNT_NMBR, AD_CMPNY);
		      
		      
		    
			 // post current to current acv
			   
			 this.post(glAccountingCalendarValue,
					 glChartOfAccount,
			     true, DEBIT, AMOUNT, AD_CMPNY);
			
			
			 // post to subsequent acvs (propagate)
			
			 Collection glSubsequentAccountingCalendarValues = 
			     glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
			         glSetOfBook.getGlAccountingCalendar().getAcCode(),
			         glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
			       
			 Iterator j = glSubsequentAccountingCalendarValues.iterator();
			
			 while (j.hasNext()) {
				
				   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
				       (LocalGlAccountingCalendarValue)j.next();
				       
				   this.post(glSubsequentAccountingCalendarValue,
						   glChartOfAccount,
				       false, DEBIT, AMOUNT, AD_CMPNY);
					         	
			 }
			 	           
			 // post to subsequent years if necessary
			 
			 Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
			 
			
			  	if (!glSubsequentSetOfBooks.isEmpty() && glSetOfBook.getSobYearEndClosed() == 1) {
			  	
				  	LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY); 	  	
			  	
				  	Iterator sobIter = glSubsequentSetOfBooks.iterator();
				  	
				  	while (sobIter.hasNext()) {
				  		
				  		LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
				  		
				  		
				  		// post to subsequent acvs of subsequent set of book(propagate)
			
			         Collection glAccountingCalendarValues = 
			             glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
			               
			         Iterator acvIter = glAccountingCalendarValues.iterator();
			       
			         while (acvIter.hasNext()) {
			       	
			       	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
			       	       (LocalGlAccountingCalendarValue)acvIter.next();
			       	       
			       	    if (ACCNT_TYP.equals("ASSET") || ACCNT_TYP.equals("LIABILITY") ||
			       	    		ACCNT_TYP.equals("OWNERS EQUITY")) {
			       	       
					         	this.post(glSubsequentAccountingCalendarValue,
					         			glChartOfAccount,
					         	     false, DEBIT, AMOUNT, AD_CMPNY);
					         	     
					        } else { // revenue & expense
					        					             					             
					             this.post(glSubsequentAccountingCalendarValue,
					         	     glRetainedEarningsAccount,
					         	     false, DEBIT, AMOUNT, AD_CMPNY);					        
					        
					        }
			       		         	
			         }
				  		
				  	   if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
				  		
				  	}
				  	
			  	}
	    	  
				

			}

			
		
 	  } catch (GlobalAccountNumberInvalidException ex) {
 	  	
 	  	  throw ex;
 	  	  
 	 	
 	  } catch (Exception ex) {
 	  	
 	  	  Debug.printStackTrace(ex);
       	  throw new EJBException(ex.getMessage());
 	  	
 	  }
 	

    }




	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

		Debug.print("GlRepDetailTrialBalanceControllerBean getAdCompany");      

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());

			return details;  	

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getPrfEnableGlRecomputeCoaBalance(Integer AD_CMPNY) {

        Debug.print("GlRepDetailTrialBalanceControllerBean getPrfEnableGlRecomputeCoaBalance");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableGlRecomputeCoaBalance();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGenSgAll(Integer AD_CMPNY) {

		Debug.print("GlRepDetailTrialBalanceControllerBean getGenSgAll");      

		LocalAdCompanyHome adCompanyHome = null;
		LocalGenSegmentHome genSegmentHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			Collection genSegments = genSegmentHome.findByFlCode(adCompany.getGenField().getFlCode(), AD_CMPNY);

			Iterator i = genSegments.iterator();

			while (i.hasNext()) {

				LocalGenSegment genSegment = (LocalGenSegment)i.next();

				GenModSegmentDetails mdetails = new GenModSegmentDetails();
				mdetails.setSgMaxSize(genSegment.getSgMaxSize());
				mdetails.setSgFlSegmentSeparator(genSegment.getGenField().getFlSegmentSeparator());


				list.add(mdetails);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{

		Debug.print("GlRepDetailTrialBalanceControllerBean getAdBrResAll");

		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;

		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;

		Collection adBranchResponsibilities = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (adBranchResponsibilities.isEmpty()) {

			throw new GlobalNoRecordFoundException();

		}

		try {

			Iterator i = adBranchResponsibilities.iterator();

			while(i.hasNext()) {

				adBranchResponsibility = (LocalAdBranchResponsibility)i.next();

				adBranch = adBranchResponsibility.getAdBranch();

				AdBranchDetails details = new AdBranchDetails();

				details.setBrCode(adBranch.getBrCode());
				details.setBrName(adBranch.getBrName());

				list.add(details);

			}	               

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		return list;

	}	

	// private methods

	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("GlRepDetailTrialBalanceControllerBean convertForeignToFunctionalCurrency");


		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}	     


		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

			AMOUNT = AMOUNT * CONVERSION_RATE;

		} else if (CONVERSION_DATE != null) {

			try {

				// Get functional currency rate

				LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;

				if (!FC_NM.equals("USD")) {

					glReceiptFunctionalCurrencyRate = 
						glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
								CONVERSION_DATE, AD_CMPNY);

					AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();

				}

				// Get set of book functional currency rate if necessary

				if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

					LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
						glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
								getFcCode(), CONVERSION_DATE, AD_CMPNY);

					AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();

				}


			} catch (Exception ex) {

				throw new EJBException(ex.getMessage());

			}       

		}

		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}
	
	
	
	private void post(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
		      LocalGlChartOfAccount glChartOfAccount, 
		      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
		      	
		      Debug.print("GlRepDetailTrialBalanceControllerBean post");
		      
		      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		      LocalAdCompanyHome adCompanyHome = null;
		      
		     
		       // Initialize EJB Home
		        
		       try {
		            
		           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
		              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
		           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
		              
		            
		       } catch (NamingException ex) {
		            
		           throw new EJBException(ex.getMessage());
		            
		       }
		       
		       try {          
		               
		               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
		       	   
			       	   LocalGlChartOfAccountBalance glChartOfAccountBalance = 
				            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
				               	  glAccountingCalendarValue.getAcvCode(),
				               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
			               	  
			           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
			           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
			           
			           
			               	  
			           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
						    isDebit == EJBCommon.TRUE) ||
						    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
						    isDebit == EJBCommon.FALSE)) {				    
						    			
		 			        glChartOfAccountBalance.setCoabEndingBalance(
						       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
						       
						    if (!isCurrentAcv) {
						    	
						    	glChartOfAccountBalance.setCoabBeginningBalance(
						       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
						    	
							}
						    
				
					  } else {
			
						    glChartOfAccountBalance.setCoabEndingBalance(
						       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
						       
						    if (!isCurrentAcv) {
						    	
						    	glChartOfAccountBalance.setCoabBeginningBalance(
						       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
						    	
							}
			
				 	  }
				 	  
				 	  if (isCurrentAcv) { 
				 	 
					 	 if (isDebit == EJBCommon.TRUE) {
					 	 	
				 			glChartOfAccountBalance.setCoabTotalDebit(
				 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
				 				
				 		 } else {
				 		 	
				 			glChartOfAccountBalance.setCoabTotalCredit(
				 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
				 		 }       	   
				 		 
				 	}
		       	
		       } catch (Exception ex) {
		       	
		       	   Debug.printStackTrace(ex);
		       	   throw new EJBException(ex.getMessage());
		       	
		       }
		      
		      
		   }



	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("GlRepDetailTrialBalanceControllerBean ejbCreate");

	}  

}
