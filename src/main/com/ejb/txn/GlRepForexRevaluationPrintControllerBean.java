/*
 * GlRepForexRevaluationPrintControllerBean.java
 *
 * Created on May 31, 2006, 6:26 PM
 * 
 * @author  Farrah S. Garing
 */
package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlCOANoChartOfAccountFoundException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyAssignedException;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDateViolationException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlForexLedger;
import com.ejb.gl.LocalGlForexLedgerHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlChartOfAccountDetails;
import com.util.GlRepForexRevaluationPrintDetails;

/**
 * @ejb:bean name="GlRepForexRevaluationPrintControllerEJB"
 *           display-name="Used for searching of segments"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepForexRevaluationPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepForexRevaluationPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepForexRevaluationPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
 */


public class GlRepForexRevaluationPrintControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public GlChartOfAccountDetails getGlChartOfAccount(String UNRLZD_GN_LSS_ACCNT, Integer AD_BRNCH,
			Integer AD_CMPNY) throws 
		GlCOANoChartOfAccountFoundException {
		
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		
		try {
			
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		LocalGlChartOfAccount glForexCoa= null;
		
		try {
			
			glForexCoa = glChartOfAccountHome.findByCoaAccountNumber(UNRLZD_GN_LSS_ACCNT, AD_BRNCH);
			
		} catch (FinderException ex){
			
			getSessionContext().setRollbackOnly();
			throw new GlCOANoChartOfAccountFoundException();
			
		}
		
		GlChartOfAccountDetails details = new GlChartOfAccountDetails(glForexCoa.getCoaAccountNumber(),
				glForexCoa.getCoaAccountDescription(), glForexCoa.getCoaAccountType(), glForexCoa.getCoaTaxType(), 
				glForexCoa.getCoaCitCategory(), glForexCoa.getCoaSawCategory(), glForexCoa.getCoaIitCategory(),
				glForexCoa.getCoaDateFrom(),
				glForexCoa.getCoaDateTo(), glForexCoa.getCoaEnable(), EJBCommon.TRUE);

		return details;
	
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("GlRepForexRevaluationPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}  

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("GRepForexRevaluationPrintControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeRepForexRevaluationPrint(HashMap criteria, double CONVERSION_RATE,  int YR,
		String PRD_NM, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY)
		throws GlJREffectiveDateViolationException,
		GlFCNoFunctionalCurrencyFoundException,
		GlFCFunctionalCurrencyAlreadyAssignedException,
		GlobalAccountNumberInvalidException,
		GlJREffectiveDateNoPeriodExistException,
		GlobalNoRecordFoundException {

		Debug.print("GlRepForexRevaluationPrintControllerBean executeRepForexRevaluationPrint");
		
		LocalAdCompanyHome adCompanyHome = null;
	    LocalGlChartOfAccountHome glChartOfAccountHome = null;
	   	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
	   	LocalGlForexLedgerHome glForexLedgerHome = null;
	   	LocalGlSetOfBookHome glSetOfBookHome = null;
	   	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;

	   	try {
	   		
	   		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	   			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	   		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	   			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	   		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
	   			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
	   		glForexLedgerHome = (LocalGlForexLedgerHome)EJBHomeFactory.
	   			lookUpLocalHome(LocalGlForexLedgerHome.JNDI_NAME, LocalGlForexLedgerHome.class);
	   		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
	   			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
	   		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
	   			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
	   		
	   	} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			ArrayList list = new ArrayList();
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			Date JR_EFFCTV_DT = null;

  	  		// validate if period exists and open
            
  	  		LocalGlSetOfBook glSetOfBook = null; 
  	  		
  	  		try {	  	 
            	
	            // get selected set of book
	            
	            Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(PRD_NM,
	            		EJBCommon.getIntendedDate(YR), AD_CMPNY);
	            ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);
	            
	            glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);

	            // get accounting calendar value 
		                           
	            LocalGlAccountingCalendarValue glAccountingCalendarValue = 
	                glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
	                	glSetOfBook.getGlAccountingCalendar().getAcCode(),
	                	PRD_NM, AD_CMPNY);

            	JR_EFFCTV_DT = glAccountingCalendarValue.getAcvDateTo();
            	
            } catch (FinderException ex) {
            	
            	throw new GlJREffectiveDateNoPeriodExistException();
            	
            }
			
			LocalGenField genField = adCompany.getGenField();
			String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
			int genNumberOfSegment = genField.getFlNumberOfSegment();		
			
			short ctr = 0;
			
			StringTokenizer stAccountFrom = new StringTokenizer((String)criteria.get(
			"accountNumberFrom"),strSeparator);
			StringTokenizer stAccountTo = new StringTokenizer((String)criteria.get(
			"accountNumberTo"), strSeparator);
			
			Object obj[] = new Object[criteria.size() - 2];
			
			//get COA selected
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT DISTINCT OBJECT(coa) FROM GlChartOfAccount coa WHERE ");
			
			// validate if account number is valid
			
			if (stAccountFrom.countTokens() != genNumberOfSegment || 
					stAccountTo.countTokens() != genNumberOfSegment) {
				
				throw new GlobalAccountNumberInvalidException();
				
			}
			
			try {
				
				String var = "1";
				
				if (genNumberOfSegment > 1) {
					
					for (int i=0; i<genNumberOfSegment; i++) {
						
						if (i == 0 && i < genNumberOfSegment - 1) {
							
							// for first segment
							
							jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator +
									"', coa.coaAccountNumber, 1)-1) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" +
									stAccountTo.nextToken() + "' AND ");
							
							var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)+1 ";
							
							
						} else if (i != 0 && i < genNumberOfSegment - 1){     		
							
							// for middle segments
							
							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator +
									"', coa.coaAccountNumber, " + var + ")) - (" + var +")) BETWEEN '" +
									stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");
							
							var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";
							
						} else if (i != 0 && i == genNumberOfSegment - 1) {
							
							// for last segment
							
							jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var +
									", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) BETWEEN '" +
									stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' ");
							
							
						}	     		       	      	   	            
						
					}
					
				} else if(genNumberOfSegment == 1) {
					
					String accountFrom = stAccountFrom.nextToken();
					String accountTo = stAccountTo.nextToken();
					
					jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator +
							"', coa.coaAccountNumber, 1)-1) BETWEEN '" + accountFrom + "' AND '" + accountTo + "' OR " +
							"coa.coaAccountNumber BETWEEN '" + accountFrom + "' AND '" + accountTo + "' ");
					
				}
				
				String GL_FUNCTIONAL_CURRENCY = (String) criteria.get("functionalCurrency");
				
				if (GL_FUNCTIONAL_CURRENCY != null && GL_FUNCTIONAL_CURRENCY.length() > 0 && 
						(!GL_FUNCTIONAL_CURRENCY.equalsIgnoreCase("NO RECORD FOUND"))) {
					
					LocalGlFunctionalCurrency glFunctionalCurrency = null;
					
					try{
						
						glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(
								GL_FUNCTIONAL_CURRENCY, AD_CMPNY);
						
					}catch(FinderException ex){
						
						throw new GlFCNoFunctionalCurrencyFoundException();
						
					}
					// check if FC selected is default company FC
					if (adCompany.getGlFunctionalCurrency().getFcCode() == glFunctionalCurrency.getFcCode())
						throw new GlFCFunctionalCurrencyAlreadyAssignedException();
					
					jbossQl.append(" AND coa.glFunctionalCurrency.fcCode=?" + (ctr + 1) + " ");
					obj[ctr] = glFunctionalCurrency.getFcCode();
					ctr++;
					
				}
				
				
				jbossQl.append("AND coa.coaEnable=1 AND coa.coaAdCompany=" + AD_CMPNY + " ");
				
			} catch (NumberFormatException ex) {
				
				throw new GlobalAccountNumberInvalidException();
				
			}
			
			Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), obj);
			
			if (glChartOfAccounts == null || glChartOfAccounts.isEmpty() || glChartOfAccounts.size() == 0){
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			Iterator i = glChartOfAccounts.iterator();
			
			while (i.hasNext()){
				
				LocalGlChartOfAccount glChartOfAccount= (LocalGlChartOfAccount) i.next();
				
				Collection glForexLedgers = glForexLedgerHome.findLatestGlFrlByFrlDateAndByCoaCode(JR_EFFCTV_DT,
						glChartOfAccount.getCoaCode(), AD_CMPNY);
				
				if (glForexLedgers == null || glForexLedgers.isEmpty()) continue;
				
				LocalGlForexLedger glLatestForexLedger = (LocalGlForexLedger) glForexLedgers.iterator().next();
				
				double FRL_BLNC = glLatestForexLedger.getFrlBalance();
				
				Iterator j = glForexLedgers.iterator();

				while(j.hasNext() &&  FRL_BLNC > 0){
					
					LocalGlForexLedger glForexLedger = (LocalGlForexLedger) j.next();
					
					if (glForexLedger.getFrlType().equals("REVAL") || glForexLedger.getFrlAmount() <= 0d) continue;
					
					double FRX_GN_LSS = 0d;
					
					if(FRL_BLNC - glForexLedger.getFrlAmount() <= 0d)
						FRX_GN_LSS = (FRL_BLNC * (CONVERSION_RATE - glForexLedger.getFrlRate()));
					else
						FRX_GN_LSS = (glForexLedger.getFrlAmount() * (CONVERSION_RATE - glForexLedger.getFrlRate()));
					
					FRL_BLNC = FRL_BLNC - glForexLedger.getFrlAmount();
					
					GlRepForexRevaluationPrintDetails details = new GlRepForexRevaluationPrintDetails();
					
					details.setFxpFrlCoaAccountNumber(glChartOfAccount.getCoaAccountNumber());
					details.setFxpFrlCoaAccountDescription(glChartOfAccount.getCoaAccountDescription());
					details.setFxpFrlCoaAccountType(glChartOfAccount.getCoaAccountType());
					details.setFxpFrlRate(glForexLedger.getFrlRate());
					details.setFxpFrlForexAmount(glForexLedger.getFrlAmount());
					details.setFxpForexGainLoss(FRX_GN_LSS);
					details.setFxpForexRemainingAmount(glLatestForexLedger.getFrlBalance());
					
					list.add(details);
					
				}
				
			}
			
			if(list == null || list.isEmpty() || list.size() <= 0)
				
				throw new GlobalNoRecordFoundException();
			
			return list;
			
			
		} catch (GlobalNoRecordFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;			
			

		} catch (GlJREffectiveDateNoPeriodExistException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;			
		} catch (GlFCNoFunctionalCurrencyFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw ex;
        	
		} catch (GlFCFunctionalCurrencyAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw ex;
        	
		} catch (GlobalAccountNumberInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw ex;

		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("GlRepForexRevaluationPrintControllerBean ejbCreate");
		
	}
	
}