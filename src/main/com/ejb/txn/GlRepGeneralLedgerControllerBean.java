package com.ejb.txn;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenQualifier;
import com.ejb.genfld.LocalGenQualifierHome;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSet;
import com.ejb.genfld.LocalGenValueSetHome;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.GlInvestorAccountBalanceBean;
import com.ejb.gl.LocalGlAccountingCalendar;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAssemblyTransfer;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.ejb.inv.LocalInvBuildUnbuildAssembly;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvOverhead;
import com.ejb.inv.LocalInvStockIssuance;
import com.ejb.inv.LocalInvStockTransfer;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GenModSegmentDetails;
import com.util.GenModValueSetValueDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlRepDetailInvestorAccountBalanceDetails;
import com.util.GlRepGeneralLedgerDetails;

/**
 * @ejb:bean name="GlRepGeneralLedgerControllerEJB"
 *           display-name="Used for generation of general ledger reports"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepGeneralLedgerControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepGeneralLedgerController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepGeneralLedgerControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
*/

public class GlRepGeneralLedgerControllerBean extends AbstractSessionBean {



	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("GlRepGeneralLedgerControllerBean getApSplAll");

		LocalApSupplierHome apSupplierHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection apSuppliers = apSupplierHome.findAllEnabledSplScLedger(AD_CMPNY);

			Iterator i = apSuppliers.iterator();

			while (i.hasNext()) {

				LocalApSupplier apSupplier = (LocalApSupplier)i.next();

				list.add(apSupplier.getSplSupplierCode());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

		Debug.print("GlRepGeneralLedgerControllerBean getAdPrfApUseSupplierPulldown");

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfApUseSupplierPulldown();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGenQlfrAll(Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("GlRepGeneralLedgerControllerBean getGenQlfrAll");

        LocalGenQualifierHome genQualifierHome = null;

        Collection genQualifiers = null;

        LocalGenQualifier genQualifier = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            genQualifierHome = (LocalGenQualifierHome)EJBHomeFactory.
                 lookUpLocalHome(LocalGenQualifierHome.JNDI_NAME, LocalGenQualifierHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            genQualifiers = genQualifierHome.findQlfrAll(AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (genQualifiers.isEmpty()) {

            throw new GlobalNoRecordFoundException();

        }

        Iterator i = genQualifiers.iterator();

        while (i.hasNext()) {

        	genQualifier = (LocalGenQualifier)i.next();

        	list.add(genQualifier.getQlAccountType());

        }

        return list;

    }


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {

		Debug.print("GlRepGeneralLedgerControllerBean getAdLvReportTypeAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("GL REPORT TYPE - GENERAL LEDGER", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}




   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList executeSpGlRepGeneralLedger(ResultSet rs, String GL_ACCNT_NMBR_FRM, String GL_ACCNT_NMBR_TO,
   	  ArrayList vsvDescList, Date GL_DT, String GL_AMNT_TYP, String JS_NM, boolean GL_INCLD_UNPSTD,
      boolean GL_SHW_ZRS_TXN, boolean GL_SHW_ZRS_ALL, boolean SL_INCLD_UNPSTD, boolean VAT_RLF,
      String CRRNCY, boolean SHW_ENTRS, String RPRT_TYP, String ORDER_BY, ArrayList qualifierList, ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

      Debug.print("GlRepGeneralLedgerControllerBean executeSpGlRepGeneralLedger");


      LocalGlSetOfBookHome glSetOfBookHome = null;
      LocalGlChartOfAccountHome glChartOfAccountHome = null;
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalGenValueSetValueHome genValueSetValueHome = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      LocalGenSegmentHome genSegmentHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      LocalGlJournalLineHome glJournalLineHome = null;
      LocalArDistributionRecordHome arDistributionRecordHome = null;
      LocalApDistributionRecordHome apDistributionRecordHome = null;
      LocalCmDistributionRecordHome cmDistributionRecordHome = null;
      LocalInvDistributionRecordHome invDistributionRecordHome = null;
      LocalAdBankAccountHome adBankAccountHome = null;
      LocalArInvoiceHome arInvoiceHome = null;
      LocalApVoucherHome apVoucherHome = null;
      LocalArCustomerHome arCustomerHome  = null;
      LocalApSupplierHome apSupplierHome = null;
      LocalApCheckHome apCheckHome = null;
      LocalArReceiptHome arReceiptHome = null;
      LocalApPurchaseOrderHome apPurchaseOrderHome = null;
      LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
      LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

      ArrayList list = new ArrayList();

	   // Initialize EJB Home

	  try {

	       glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
	       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	       glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
	       genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
	       glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
	       genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	       glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
	       arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
           	   lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
	       apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
               lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
	       cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
               lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
	       invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
	       adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
           	   lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
	       arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
       	       lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
	       apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
           	   lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
	       arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
       	   		lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
	       apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
       	   		lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
	       apCheckHome = (LocalApCheckHome)EJBHomeFactory.
	  	   		lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
	       arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
	  	   		lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
	       apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
 	   			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
	       glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
  				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
	       glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
   				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);


	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

	  try {

	  	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	  	  LocalGlSetOfBook glSetOfBook = null;

	  	  try {

	  	  	 glSetOfBook = glSetOfBookHome.findByDate(GL_DT, AD_CMPNY);

	  	  } catch (FinderException ex) {
	  	  	System.out.println("CHECK A");
	  	  	 throw new GlobalNoRecordFoundException();

	  	  }

	  	  System.out.println("CRRNCY="+CRRNCY);
	  	  System.out.println("GL_DT="+GL_DT);
	  	  System.out.println("SOB="+glSetOfBook.getGlAccountingCalendar().getAcName());

	  	  LocalGenField genField = adCompany.getGenField();
	  	  Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
	      String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
	      int genNumberOfSegment = genField.getFlNumberOfSegment();


	      StringTokenizer stAccountFrom = new StringTokenizer(GL_ACCNT_NMBR_FRM, strSeparator);
          StringTokenizer stAccountTo = new StringTokenizer(GL_ACCNT_NMBR_TO, strSeparator);


	      if (stAccountFrom.countTokens() != genNumberOfSegment ||
	  	         stAccountTo.countTokens() != genNumberOfSegment) {

  	      	  throw new GlobalAccountNumberInvalidException();

  	      }


	      while(rs.next()){

	    	  GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
	    	  String ACCNT_NMBR = rs.getString(1);
	    	  String ACCNT_TYP = rs.getString(2);
	    	  String ACCNT_DESC = rs.getString(3);
	    	  Date DATE = rs.getDate(4);
	    	  String DOCUMENT = rs.getString(5);
	    	  String REFERENCE = rs.getString(6);
	    	  String SOURCE = rs.getString(7);
	    	  String NAME = rs.getString(8);
	    	  String DESCN = rs.getString(9);
	    	  Double DEBIT = rs.getDouble(10);
	    	  Double CREDIT = rs.getDouble(11);
	    	  Double RUN_BAL = rs.getDouble(12);

	    	  String COA = rs.getString(13);
	    	  byte DBT = rs.getByte(14);
	    	  Double AMOUNT = rs.getDouble(15);

	    	  byte POSTED = rs.getByte(17);
	    	  Integer BRANCH = rs.getInt(18);

	    	  double COA_BGNNG_BLNC = rs.getDouble(16);
		    //  double COA_BGNNG_BLNC = 0d;
		      /*
		      LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue =
	 			glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
	 					glSetOfBook.getGlAccountingCalendar().getAcCode(),
					(short)1, AD_CMPNY);

		      LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(ACCNT_NMBR, AD_CMPNY);


		      LocalGlChartOfAccountBalance glBeginningChartOfAccountBalance =
	 			glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
	 					glBeginningAccountingCalendarValue.getAcvCode(),
					glChartOfAccount.getCoaCode(), AD_CMPNY);

		      COA_BGNNG_BLNC = glBeginningChartOfAccountBalance.getCoabBeginningBalance();

		      System.out.println("COAB_CODE="+glBeginningChartOfAccountBalance.getCoabCode());

		      */
		      System.out.println("COA_BGNNG_BLNC="+COA_BGNNG_BLNC);


	    	  details.setGlAccountNumber(ACCNT_NMBR);
	    	  details.setGlAccountType(ACCNT_TYP);
	    	  details.setGlAccountDescription(ACCNT_DESC);
	    	  details.setGlJrEffectiveDate(DATE);
	    	  details.setGlJrDocumentNumber(DOCUMENT);
	    	  details.setGlJrName(REFERENCE);
	    	  details.setGlJsName(SOURCE);
	    	  details.setGlJrSubLedger(NAME);
	    	  details.setGlJrDescription(DESCN);
	    	  details.setGlJlDebit(DBT);
	    	  details.setGlBeginningBalance(COA_BGNNG_BLNC);
	    	  details.setGlJlAmount(AMOUNT);
	    	  details.setGlJrPosted(POSTED);


	    	  list.add(details);


	      }



	      return list;

	  } catch (GlobalAccountNumberInvalidException ex) {

	  	  throw ex;

	  } catch (GlobalNoRecordFoundException ex) {

	  	  throw ex;

	  } catch (Exception ex) {

	  	  Debug.printStackTrace(ex);
      	  throw new EJBException(ex.getMessage());

	  }


   }













   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList executeGlRepGeneralLedger(String GL_ACCNT_NMBR_FRM, String GL_ACCNT_NMBR_TO,
    	  ArrayList vsvDescList, Date GL_DT, String GL_AMNT_TYP, String JS_NM, boolean GL_INCLD_UNPSTD,
       boolean GL_SHW_ZRS_TXN, boolean GL_SHW_ZRS_ALL, boolean SL_INCLD_UNPSTD, boolean VAT_RLF,
       String CRRNCY, boolean SHW_ENTRS, String RPRT_TYP, String ORDER_BY, ArrayList qualifierList, ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException, GlobalAccountNumberInvalidException {

       Debug.print("GlRepGeneralLedgerControllerBean executeGlRepGeneralLedger");


       LocalGlSetOfBookHome glSetOfBookHome = null;
       LocalGlChartOfAccountHome glChartOfAccountHome = null;
       LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
       LocalGenValueSetValueHome genValueSetValueHome = null;
       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
       LocalGenSegmentHome genSegmentHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       LocalGlJournalLineHome glJournalLineHome = null;
       LocalArDistributionRecordHome arDistributionRecordHome = null;
       LocalApDistributionRecordHome apDistributionRecordHome = null;
       LocalCmDistributionRecordHome cmDistributionRecordHome = null;
       LocalInvDistributionRecordHome invDistributionRecordHome = null;
       LocalAdBankAccountHome adBankAccountHome = null;
       LocalArInvoiceHome arInvoiceHome = null;
       LocalApVoucherHome apVoucherHome = null;
       LocalArCustomerHome arCustomerHome  = null;
       LocalApSupplierHome apSupplierHome = null;
       LocalApCheckHome apCheckHome = null;
       LocalArReceiptHome arReceiptHome = null;
       LocalApPurchaseOrderHome apPurchaseOrderHome = null;
       LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
       LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;

       ArrayList list = new ArrayList();

 	   // Initialize EJB Home

 	  try {

 	       glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
 	       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
 	       glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
 	       genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
 	       glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
 	       genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
 	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
 	       glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
 	       arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
            	   lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
 	       apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
 	       cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
 	       invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
 	       adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
            	   lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
 	       arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
        	       lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
 	       apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
            	   lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
 	       arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
        	   		lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
 	       apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
        	   		lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
 	       apCheckHome = (LocalApCheckHome)EJBHomeFactory.
 	  	   		lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
 	       arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
 	  	   		lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
 	       apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
  	   			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
 	       glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
   				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
 	       glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
    				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);


 	  } catch (NamingException ex) {

 	      throw new EJBException(ex.getMessage());

 	  }

 	  try {

 	  	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 	  	  LocalGlSetOfBook glSetOfBook = null;

 	  	  try {

 	  	  	 glSetOfBook = glSetOfBookHome.findByDate(GL_DT, AD_CMPNY);

 	  	  } catch (FinderException ex) {
 	  	  	System.out.println("CHECK A");
 	  	  	 throw new GlobalNoRecordFoundException();

 	  	  }

 	  	  System.out.println("CRRNCY="+CRRNCY);
 	  	  System.out.println("GL_DT="+GL_DT);
 	  	  System.out.println("SOB="+glSetOfBook.getGlAccountingCalendar().getAcName());

 	  	  LocalGenField genField = adCompany.getGenField();
 	  	  Collection genSegments = genSegmentHome.findByFlCode(genField.getFlCode(), AD_CMPNY);
 	      String strSeparator =  String.valueOf(genField.getFlSegmentSeparator());
 	      int genNumberOfSegment = genField.getFlNumberOfSegment();

 	      // get coa selected

 	      StringBuffer jbossQl = new StringBuffer();
           jbossQl.append("SELECT DISTINCT OBJECT(coa) FROM GlChartOfAccount coa ");




           // add branch criteria

 		  if (branchList.isEmpty()) {
 			  System.out.println("CHECK B");
 		  	throw new GlobalNoRecordFoundException();

 		  } else {

 		  	jbossQl.append(", in (coa.adBranchCoas) bcoa WHERE bcoa.adBranch.brCode in (");

 		  	boolean firstLoop = true;

 		  	Iterator j = branchList.iterator();

 		  	while(j.hasNext()) {

 		  		if(firstLoop == false) {
 		  			jbossQl.append(", ");
 		  		}
 		  		else {
 		  			firstLoop = false;
 		  		}

 		  		AdBranchDetails mdetails = (AdBranchDetails) j.next();

 		  		jbossQl.append(mdetails.getBrCode());

 		  	}

 		  	jbossQl.append(") AND ");

 		  }





 		  if (!qualifierList.isEmpty()) {


 			  Iterator j = qualifierList.iterator();


 			  jbossQl.append("coa.coaAccountType in (");

 		 	  boolean firstLoop = true;


 		 	  while(j.hasNext()) {

 		  		if(firstLoop == false) {
 		  			jbossQl.append(", ");
 		  		}
 		  		else {
 		  			firstLoop = false;
 		  		}


 		  		String QUALIFIER = (String) j.next();

 		  		jbossQl.append("'"+QUALIFIER+"'");


 			  	}

 		 	  jbossQl.append(") AND ");


 		  }



           StringTokenizer stAccountFrom = new StringTokenizer(GL_ACCNT_NMBR_FRM, strSeparator);
           StringTokenizer stAccountTo = new StringTokenizer(GL_ACCNT_NMBR_TO, strSeparator);

           System.out.println("GL_ACCNT_NMBR_FRM="+GL_ACCNT_NMBR_FRM);
           System.out.println("GL_ACCNT_NMBR_TO="+GL_ACCNT_NMBR_TO);
           // validate if account number is valid

           if (stAccountFrom.countTokens() != genNumberOfSegment ||
 	         stAccountTo.countTokens() != genNumberOfSegment) {

 	      	  throw new GlobalAccountNumberInvalidException();

 	      }

 	      try {

 	          String var = "1";

 	          if (genNumberOfSegment > 1) {

 	          	for (int i=0; i<genNumberOfSegment; i++) {

 	          		if (i == 0 && i < genNumberOfSegment - 1) {

 	          			// for first segment

 	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");

 	          			var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)+1 ";


 	          		} else if (i != 0 && i < genNumberOfSegment - 1){

 	          			// for middle segments

 	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")) - (" + var +")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' AND ");

 	          			var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";

 	          		} else if (i != 0 && i == genNumberOfSegment - 1) {

 	          			// for last segment

 	          			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) BETWEEN '" + stAccountFrom.nextToken() + "' AND '" + stAccountTo.nextToken() + "' ");


 	          		}

 	          	}

 	          } else if(genNumberOfSegment == 1) {

 	          	String accountFrom = stAccountFrom.nextToken();
 	       		String accountTo = stAccountTo.nextToken();

 	       		jbossQl.append("SUBSTRING(coa.coaAccountNumber, 1, LOCATE('" + strSeparator + "', coa.coaAccountNumber, 1)-1) BETWEEN '" + accountFrom + "' AND '" + accountTo + "' OR " +
 	       				"coa.coaAccountNumber BETWEEN '" + accountFrom + "' AND '" + accountTo + "' ");

 	          }

 		 } catch (NumberFormatException ex) {

 		 	throw new GlobalAccountNumberInvalidException();

 		 }

 		 if (!vsvDescList.isEmpty()) {

 			 Iterator i = vsvDescList.iterator();

 			 while (i.hasNext()) {

 			 	GenModValueSetValueDetails mdetails = (GenModValueSetValueDetails)i.next();


 			 	Collection genValueSetValues = null;

 			 	try {

 			 		genValueSetValues = genValueSetValueHome.findByVsvDescriptionAndVsName(mdetails.getVsvDescription(), mdetails.getVsvVsName(), AD_CMPNY);

 			 	} catch (Exception ex) {

 			 		throw new EJBException(ex.getMessage());

 			 	}

 			 	if (genValueSetValues.isEmpty()) {
 			 		System.out.println("CHECK C");
 			 	    throw new GlobalNoRecordFoundException();

 			 	}

 			 	jbossQl.append("AND (");

 			 	Iterator j = genValueSetValues.iterator();

 			 	while (j.hasNext()) {


 			 		LocalGenValueSetValue genValueSetValue = (LocalGenValueSetValue)j.next();

 			 		int genSegmentNumber = 0;

 			 		try {

 				 		LocalGenSegment genSegment = genSegmentHome.findByVsCode(genValueSetValue.getGenValueSet().getVsCode(), AD_CMPNY);
 				 		genSegmentNumber = genSegment.getSgSegmentNumber();

 				 	} catch (Exception ex) {

 				 		throw new EJBException(ex.getMessage());

 				 	}

 			 		String var = "1";

 			 		for (int k=0; k<genSegmentNumber-1; k++) {


 					 	var = "LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")+1 ";


 			 		}

 			 		if (genNumberOfSegment != genSegmentNumber) {

 			 			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LOCATE('" + strSeparator + "', coa.coaAccountNumber, " + var + ")) - (" + var +")) = '" + genValueSetValue.getVsvValue() + "' ");

 			 		} else if (genNumberOfSegment == genSegmentNumber) {

 			 			jbossQl.append("SUBSTRING(coa.coaAccountNumber, " + var + ", (LENGTH(coa.coaAccountNumber)+1) - (" + var + ")) = '" + genValueSetValue.getVsvValue() + "' ");
 			 	    }

 			 	    if (j.hasNext()) {

 			 	    	jbossQl.append("OR ");

 			 	    }

 			 	}

 		 		jbossQl.append(") ");


 			 }

 	 	 }

 		 // generate order by coa natural account

 	     short accountSegmentNumber = 0;

 	     try {

 	      	  LocalGenSegment genSegment = genSegmentHome.findByFlCodeAndSegmentType(genField.getFlCode(), 'N', AD_CMPNY);
 	      	  accountSegmentNumber = genSegment.getSgSegmentNumber();

 	     } catch (Exception ex) {

 	         throw new EJBException(ex.getMessage());

 	     }


 		 jbossQl.append("AND coa.coaEnable=1 AND coa.coaAdCompany=" + AD_CMPNY + " ORDER BY coa.coaSegment" + accountSegmentNumber + ", coa.coaAccountNumber ");

 		 Object obj[] = new Object[0];

 		 System.out.println("Ssql="+jbossQl.toString());
   	     Collection glChartOfAccounts = glChartOfAccountHome.getCoaByCriteria(jbossQl.toString(), obj);

   	     // get previous

   	     Collection glPreviousSetOfBooks = glSetOfBookHome.findPrecedingSobByAcYear(glSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
   	     Iterator prevItr = glPreviousSetOfBooks.iterator();
   	     LocalGlSetOfBook glPreviousSetOfBook = null;
   	     while(prevItr.hasNext()) {

   	     	glPreviousSetOfBook = (LocalGlSetOfBook)prevItr.next();
   	     	break;

   	     }

   	     // check if previous year is closed

   	     if(glPreviousSetOfBook != null && glPreviousSetOfBook.getSobYearEndClosed() == EJBCommon.FALSE) {

   	     	Iterator i = glChartOfAccounts.iterator();

   	     	while (i.hasNext()) {

   	     		LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)i.next();

   	     		if (GL_SHW_ZRS_ALL) {

   	     			GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();

   	     			details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     			details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     			details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     			details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     			details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     			details.setGlBeginningBalance(0d);
   	     			details.setGlEndingBalance(0d);
   	     			details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     			list.add(details);

   	     		}

   	     	}

   	     } else {//---------------------

   	     	Iterator i = glChartOfAccounts.iterator();

   	     	while (i.hasNext()) {

   	     		LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)i.next();
   	     		System.out.println("COA = "+glChartOfAccount.getCoaAccountNumber());
   	     		double COA_BLNC = 0d;

   	     		// get coa debit or credit	in coa balance

   	     		LocalGlAccountingCalendarValue glAccountingCalendarValue = null;

   	     		try {

   	     			glAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndDate(
   	     					glSetOfBook.getGlAccountingCalendar().getAcCode(),
 							GL_DT, AD_CMPNY);

   	     		} catch (FinderException ex) {

   	     			throw new GlobalNoRecordFoundException();

   	     		}

   	     		short GL_PRD_NMBR = 1;


   	     		// get coa beginning balance

   	     		double COA_BGNNG_BLNC = 0d;

   	     		LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue =
   	     			glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
   	     					glSetOfBook.getGlAccountingCalendar().getAcCode(),
 							GL_PRD_NMBR, AD_CMPNY);

   	     		LocalGlChartOfAccountBalance glBeginningChartOfAccountBalance =
   	     			glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
   	     					glBeginningAccountingCalendarValue.getAcvCode(),
 							glChartOfAccount.getCoaCode(), AD_CMPNY);

   	     		COA_BGNNG_BLNC = glBeginningChartOfAccountBalance.getCoabBeginningBalance();

   	     		// get unposted journals for beginning balance

   	     		if (GL_INCLD_UNPSTD) {

   	     			GregorianCalendar beginningGc = new GregorianCalendar();
   	     			beginningGc.setTime(glBeginningAccountingCalendarValue.getAcvDateFrom());
   	     			beginningGc.add(Calendar.DATE, -1);

   	     			GregorianCalendar endGc = new GregorianCalendar();
   	     			endGc.setTime(glBeginningAccountingCalendarValue.getAcvDateTo());
   	     			endGc.add(Calendar.DATE, -1);

   	     			Collection glBeginningJournalLines = new ArrayList();

   	     			if (JS_NM == null){

   	     				glBeginningJournalLines =
   	     					glJournalLineHome.findUnpostedJlByJrEffectiveDateAndCoaCode(
   	     							beginningGc.getTime(), glChartOfAccount.getCoaCode(), AD_CMPNY);

   	     			} else {

   	     				glBeginningJournalLines =
   	     					glJournalLineHome.findUnpostedJlByJrEffectiveDateAndCoaCodeAndJsName(
   	     							beginningGc.getTime(), glChartOfAccount.getCoaCode(), JS_NM,AD_CMPNY);

   	     			}

   	     			Iterator j = glBeginningJournalLines.iterator();

   	     			while (j.hasNext()) {

   	     				LocalGlJournalLine glBeginningJournalLine = (LocalGlJournalLine)j.next();

   	     				LocalGlJournal glBeginningJournal = glBeginningJournalLine.getGlJournal();

   	     				double JL_BGNNG_AMNT = 0;

   	     				GregorianCalendar gc = new GregorianCalendar();
   	     				gc.setTime(glBeginningAccountingCalendarValue.getAcvDateFrom());

   	     				if ( !(gc.compareTo(endGc) > 0 && (glChartOfAccount.getCoaAccountType().equals("REVENUE") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")))) {

   	     					JL_BGNNG_AMNT = this.convertForeignToFunctionalCurrency(
   	     							glBeginningJournal.getGlFunctionalCurrency().getFcCode(),
 									glBeginningJournal.getGlFunctionalCurrency().getFcName(),
 									glBeginningJournal.getJrConversionDate(),
 									glBeginningJournal.getJrConversionRate(),
 									glBeginningJournalLine.getJlAmount(), AD_CMPNY);
   	     				}



   	     				if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && glBeginningJournalLine.getJlDebit() == EJBCommon.TRUE) ||
   	     						(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && glBeginningJournalLine.getJlDebit() == EJBCommon.FALSE)) {

   	     					COA_BGNNG_BLNC += JL_BGNNG_AMNT;

   	     				} else {

   	     					COA_BGNNG_BLNC -= JL_BGNNG_AMNT;

   	     				}

   	     			}

   	     		}

   	     		// get coa debit or credit balance

   	     		Collection glJournalLines = new ArrayList();

   	     		if (!GL_INCLD_UNPSTD) {

   	     			if (JS_NM == null){

   	     				glJournalLines = glJournalLineHome.findPostedJlByJrEffectiveDateRangeAndCoaCode(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
 								GL_DT,
 								glChartOfAccount.getCoaCode(), AD_CMPNY);

   	     			} else {

   	     				glJournalLines =
   	     					glJournalLineHome.findPostedJlByJrEffectiveDateAndCoaCodeAndJsName(
   	     							GL_DT, glChartOfAccount.getCoaCode(), JS_NM,AD_CMPNY);

   	     			}

   	     		} else {

   	     			if (JS_NM == null){

   	     				glJournalLines = glJournalLineHome.findByJrEffectiveDateRangeAndCoaCode(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
 								GL_DT,
 								glChartOfAccount.getCoaCode(), AD_CMPNY);
   	     			} else {

   	     				glJournalLines =
   	     					glJournalLineHome.findByJrEffectiveDateAndCoaCodeAndJsName(
   	     							GL_DT, glChartOfAccount.getCoaCode(), JS_NM,AD_CMPNY);

   	     			}

   	     		}

   	     		Collection arINVDrs = new ArrayList();
   	     		Collection arCMDrs = new ArrayList();
   	     		Collection arRCTDrs = new ArrayList();
   	     		Collection apVOUDrs = new ArrayList();
   	     		Collection apDMDrs = new ArrayList();
   	     		Collection apCHKDrs = new ArrayList();
   	     		Collection apPODrs = new ArrayList();
   	     		Collection cmFTDrs = new ArrayList();
   	     		Collection cmADJDrs = new ArrayList();
   	     		Collection invADJDrs = new ArrayList();
   	     		Collection invBUADrs = new ArrayList();
   	     		Collection invOHDrs = new ArrayList();
   	     		Collection invSIDrs = new ArrayList();
   	     		Collection invATRDrs = new ArrayList();
   	     		Collection invSTDrs = new ArrayList();
   	     		Collection invBSTDrs = new ArrayList();

   	     		if (SL_INCLD_UNPSTD) {

   	     			if (JS_NM==null || JS_NM.equals("ACCOUNTS RECEIVABLES")){
   	     				arINVDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
   	     						EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				arCMDrs = arDistributionRecordHome.findUnpostedInvByDateRangeAndCoaAccountNumber(
   	     						EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				arRCTDrs = arDistributionRecordHome.findUnpostedRctByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);
   	     			}

   	     			if (JS_NM==null || JS_NM.equals("ACCOUNTS PAYABLES")){

   	     				apVOUDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
   	     						EJBCommon.FALSE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				apDMDrs = apDistributionRecordHome.findUnpostedVouByDateRangeAndCoaAccountNumber(
   	     						EJBCommon.TRUE, glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				apCHKDrs = apDistributionRecordHome.findUnpostedChkByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				apPODrs = apDistributionRecordHome.findUnpostedPoByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);
   	     			}

   	     			if (JS_NM==null || JS_NM.equals("CASH MANAGEMENT")){
   	     				cmFTDrs = cmDistributionRecordHome.findUnpostedFtByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				cmADJDrs = cmDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);
   	     			}

   	     			if (JS_NM==null || JS_NM.equals("INVENTORY")){

   	     				invADJDrs = invDistributionRecordHome.findUnpostedAdjByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				invBUADrs = invDistributionRecordHome.findUnpostedBuaByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				invOHDrs = invDistributionRecordHome.findUnpostedOhByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				invSIDrs = invDistributionRecordHome.findUnpostedSiByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				invATRDrs = invDistributionRecordHome.findUnpostedAtrByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				invSTDrs = invDistributionRecordHome.findUnpostedStByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);

   	     				invBSTDrs = invDistributionRecordHome.findUnpostedBstByDateRangeAndCoaAccountNumber(
   	     						glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     						GL_DT, glChartOfAccount.getCoaCode(), AD_BRNCH, AD_CMPNY);
   	     			}

   	     		}

   	     		if (glJournalLines.isEmpty() && GL_SHW_ZRS_ALL &&
   	     			(SL_INCLD_UNPSTD && arINVDrs.isEmpty() && arCMDrs.isEmpty() && arRCTDrs.isEmpty() &&
   	     		    apVOUDrs.isEmpty() && apDMDrs.isEmpty() && apCHKDrs.isEmpty() && apPODrs.isEmpty() &&
 					cmFTDrs.isEmpty() && cmADJDrs.isEmpty() && invADJDrs.isEmpty() && invBUADrs.isEmpty() &&
 					invOHDrs.isEmpty() && invSIDrs.isEmpty() && invATRDrs.isEmpty() && invSTDrs.isEmpty() &&
 					invBSTDrs.isEmpty())) {

   	     			GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();

   	     			details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     			details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     			details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     			details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     			details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     			details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     			details.setGlEndingBalance(COA_BGNNG_BLNC);
   	     			details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     			list.add(details);

   	     		} else {

   	     			double COA_ENDNG_BLNC = COA_BGNNG_BLNC;

   	     			Iterator j = glJournalLines.iterator();

   	     			while (j.hasNext()) {

   	     				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();

   	     				LocalGlJournal glJournal = glJournalLine.getGlJournal();

   	     				double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     						glJournal.getGlFunctionalCurrency().getFcCode(),
 								glJournal.getGlFunctionalCurrency().getFcName(),
 								glJournal.getJrConversionDate(),
 								glJournal.getJrConversionRate(),
 								glJournalLine.getJlAmount(), AD_CMPNY);

   	     				if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && glJournalLine.getJlDebit() == EJBCommon.TRUE) ||
   	     						(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && glJournalLine.getJlDebit() == EJBCommon.FALSE)) {

   	     					COA_ENDNG_BLNC += JL_AMNT;

   	     				} else {

   	     					COA_ENDNG_BLNC -= JL_AMNT;

   	     				}

   	     			}

   	     			// include unposted subledger transactions

   	     			if(SL_INCLD_UNPSTD){

   	     				if(arINVDrs!=null && arINVDrs.size() > 0){
   	     					j = arINVDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

   	     						LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

   	     						double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     								arInvoice.getGlFunctionalCurrency().getFcCode(),
   	     								arInvoice.getGlFunctionalCurrency().getFcName(),
   	     								arInvoice.getInvConversionDate(),
   	     								arInvoice.getInvConversionRate(),
   	     								arDistributionRecord.getDrAmount(), AD_CMPNY);

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(arCMDrs!=null && arCMDrs.size() > 0){
   	     					j = arCMDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

   	     						LocalArInvoice arCreditMemo = arDistributionRecord.getArInvoice();

   	     						LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
   	     								arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

   	     						double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     								arInvoice.getGlFunctionalCurrency().getFcCode(),
   	     								arInvoice.getGlFunctionalCurrency().getFcName(),
   	     								arInvoice.getInvConversionDate(),
   	     								arInvoice.getInvConversionRate(),
   	     								arDistributionRecord.getDrAmount(), AD_CMPNY);

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(arRCTDrs!=null && arRCTDrs.size() > 0){
   	     					j = arRCTDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

   	     						LocalArReceipt arReceipt = arDistributionRecord.getArReceipt();

   	     						double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     								arReceipt.getGlFunctionalCurrency().getFcCode(),
   	     								arReceipt.getGlFunctionalCurrency().getFcName(),
   	     								arReceipt.getRctConversionDate(),
   	     								arReceipt.getRctConversionRate(),
   	     								arDistributionRecord.getDrAmount(), AD_CMPNY);

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && arDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && arDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(apVOUDrs!=null && apVOUDrs.size() > 0){


   	     					j = apVOUDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

   	     						LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

   	     						double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     								apVoucher.getGlFunctionalCurrency().getFcCode(),
   	     								apVoucher.getGlFunctionalCurrency().getFcName(),
   	     								apVoucher.getVouConversionDate(),
   	     								apVoucher.getVouConversionRate(),
   	     								apDistributionRecord.getDrAmount(), AD_CMPNY);

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(apDMDrs!=null && apDMDrs.size() > 0){
   	     					j = apDMDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

   	     						LocalApVoucher apDebitMemo = apDistributionRecord.getApVoucher();

   	     						LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
   	     								apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

   	     						double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     								apVoucher.getGlFunctionalCurrency().getFcCode(),
   	     								apVoucher.getGlFunctionalCurrency().getFcName(),
   	     								apVoucher.getVouConversionDate(),
   	     								apVoucher.getVouConversionRate(),
   	     								apDistributionRecord.getDrAmount(), AD_CMPNY);

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(apCHKDrs!=null && apCHKDrs.size() > 0){
   	     					j = apCHKDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

   	     						LocalApCheck apCheck = apDistributionRecord.getApCheck();

   	     						double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     								apCheck.getGlFunctionalCurrency().getFcCode(),
   	     								apCheck.getGlFunctionalCurrency().getFcName(),
   	     								apCheck.getChkConversionDate(),
   	     								apCheck.getChkConversionRate(),
   	     								apDistributionRecord.getDrAmount(), AD_CMPNY);

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(apPODrs!=null && apPODrs.size() > 0){
   	     					j = apPODrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

   	     						LocalApPurchaseOrder apPurchaseOrder = apDistributionRecord.getApPurchaseOrder();

   	     						double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     								apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
   	     								apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
   	     								apPurchaseOrder.getPoConversionDate(),
   	     								apPurchaseOrder.getPoConversionRate(),
   	     								apDistributionRecord.getDrAmount(), AD_CMPNY);

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && apDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && apDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(cmFTDrs!=null && cmFTDrs.size() > 0){
   	     					j = cmFTDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

   	     						LocalCmFundTransfer cmFundTransfer = cmDistributionRecord.getCmFundTransfer();

   	     						LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

   	     						double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     								adBankAccount.getGlFunctionalCurrency().getFcCode(),
   	     								adBankAccount.getGlFunctionalCurrency().getFcName(),
   	     								cmFundTransfer.getFtConversionDate(),
   	     								cmFundTransfer.getFtConversionRateFrom(),
   	     								cmDistributionRecord.getDrAmount(), AD_CMPNY);

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(cmADJDrs!=null && cmADJDrs.size() > 0){
   	     					j = cmADJDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

   	     						LocalCmAdjustment cmAdjustment = cmDistributionRecord.getCmAdjustment();

   	     						LocalAdBankAccount adBankAccount = cmAdjustment.getAdBankAccount();

   	     						//LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getFtAdBaAccountFrom());

   	     						double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     								adBankAccount.getGlFunctionalCurrency().getFcCode(),
   	     								adBankAccount.getGlFunctionalCurrency().getFcName(),
   	     								cmAdjustment.getAdjConversionDate(),
   	     								cmAdjustment.getAdjConversionRate(),
   	     								cmDistributionRecord.getDrAmount(), AD_CMPNY);

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && cmDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(invADJDrs!=null && invADJDrs.size() > 0){
   	     					j = invADJDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     						double JL_AMNT = invDistributionRecord.getDrAmount();

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(invBUADrs!=null && invBUADrs.size() > 0){
   	     					j = invBUADrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     						double JL_AMNT = invDistributionRecord.getDrAmount();

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(invOHDrs!=null && invOHDrs.size() > 0){

   	     					j = invOHDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     						double JL_AMNT = invDistributionRecord.getDrAmount();

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(invSIDrs!=null && invSIDrs.size() > 0){
   	     					j = invSIDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     						double JL_AMNT = invDistributionRecord.getDrAmount();

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(invATRDrs!=null && invATRDrs.size() > 0){
   	     					j = invATRDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     						double JL_AMNT = invDistributionRecord.getDrAmount();

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(invSTDrs!=null && invSTDrs.size() > 0){
   	     					j = invSTDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     						double JL_AMNT = invDistributionRecord.getDrAmount();

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}

   	     					}
   	     				}

   	     				if(invBSTDrs!=null && invBSTDrs.size() > 0){
   	     					j = invBSTDrs.iterator();

   	     					while (j.hasNext()) {

   	     						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     						double JL_AMNT = invDistributionRecord.getDrAmount();

   	     						if (((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) && invDistributionRecord.getDrDebit() == EJBCommon.TRUE) ||
   	     								(!glChartOfAccount.getCoaAccountType().equals("ASSET") && !glChartOfAccount.getCoaAccountType().equals("EXPENSE") && invDistributionRecord.getDrDebit() == EJBCommon.FALSE)) {

   	     							COA_ENDNG_BLNC += JL_AMNT;

   	     						} else {

   	     							COA_ENDNG_BLNC -= JL_AMNT;

   	     						}
   	     					}
   	     				}

   	     			}



   	     			if (GL_SHW_ZRS_ALL || (GL_SHW_ZRS_TXN && COA_ENDNG_BLNC == 0) || COA_ENDNG_BLNC != 0) {

   	     				double GL_RNNG_BLNC = COA_BGNNG_BLNC;

   	     				j = glJournalLines.iterator();

   	     				while (j.hasNext()) {

   	     					LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();

   	     					LocalGlJournal glJournal = glJournalLine.getGlJournal();

   	     					double JL_AMNT = 0;
   	     					double CNVRSN_RT = 1.00;

   	     					if(glJournal.getGlJournalCategory().getJcName().equals("REVALUATION"))
 	     							continue;

 /*
   	     					if(CRRNCY.equals("")){

   	     						if(glJournal.getGlJournalCategory().getJcName().equals("REVALUATION"))
   	     							continue;

   	     					}else{

   	     						if(glJournal.getGlJournalCategory().getJcName().equals("REVALUATION") &&
   	     							!glJournal.getGlFunctionalCurrency().getFcName().equals(CRRNCY))
   	     								continue;
   	     					}
 */
   	     					if(!CRRNCY.equals(adCompany.getGlFunctionalCurrency().getFcName()) && !CRRNCY.equals("")) {

   	     						//	get conversion rate used in each transaction
   	    						if (glJournal.getGlJournalCategory().getJcName().equals("CHECKS") &&
   	    								glJournal.getGlJournalSource().getJsName().equals("ACCOUNTS PAYABLE")) {
   	    							LocalApCheck apCheck = apCheckHome.findByChkDocumentNumberAndBrCode(glJournal.getJrDocumentNumber(), AD_CMPNY, AD_CMPNY);
   	    							CNVRSN_RT = apCheck.getChkConversionRate();
   	    						} else if (glJournal.getGlJournalCategory().getJcName().equals("VOUCHERS") &&
   	    								glJournal.getGlJournalSource().getJsName().equals("ACCOUNTS PAYABLE")) {
   	    							LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(glJournal.getJrDocumentNumber(), EJBCommon.FALSE, AD_CMPNY, AD_CMPNY);
   	    							CNVRSN_RT = apVoucher.getVouConversionRate();
   	    						} else if (glJournal.getGlJournalCategory().getJcName().equals("DEBIT MEMOS") &&
   	    								glJournal.getGlJournalSource().getJsName().equals("ACCOUNTS PAYABLE")) {
   	    							LocalApVoucher apDebitMemo = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(glJournal.getJrDocumentNumber(), EJBCommon.TRUE, AD_CMPNY, AD_CMPNY);
   	    							LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_CMPNY, AD_CMPNY);
   	    							CNVRSN_RT = apVoucher.getVouConversionRate();
   	    						} else if (glJournal.getGlJournalCategory().getJcName().equals("SALES INVOICES") &&
   	    								glJournal.getGlJournalSource().getJsName().equals("ACCOUNTS RECEIVABLE")) {
   	    							LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(glJournal.getJrDocumentNumber(), EJBCommon.FALSE, AD_CMPNY, AD_CMPNY);
   	    							CNVRSN_RT = arInvoice.getInvConversionRate();
   	    						} else if (glJournal.getGlJournalCategory().getJcName().equals("CREDIT MEMOS") &&
   	    								glJournal.getGlJournalSource().getJsName().equals("ACCOUNTS RECEIVABLE")) {
   	    							LocalArInvoice arCreditMemo = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(glJournal.getJrDocumentNumber(), EJBCommon.TRUE, AD_CMPNY, AD_CMPNY);
   	    							LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_CMPNY, AD_CMPNY);
   	    							CNVRSN_RT = arInvoice.getInvConversionRate();
   	    						} else if (glJournal.getGlJournalCategory().getJcName().equals("SALES RECEIPTS") &&
   	    								glJournal.getGlJournalSource().getJsName().equals("ACCOUNTS RECEIVABLE")) {
   	    							LocalArReceipt arReceipt = arReceiptHome.findByRctNumberAndBrCode(glJournal.getJrDocumentNumber(), AD_CMPNY, AD_CMPNY);
   	    							CNVRSN_RT = arReceipt.getRctConversionRate();
   	    						}
     							if (CNVRSN_RT == 1) {

     								LocalGlFunctionalCurrencyRate glFcRate = glFunctionalCurrencyRateHome.findByFcCodeAndDate(
     										adCompany.getGlFunctionalCurrency().getFcCode(), glJournal.getJrEffectiveDate(), AD_CMPNY);
     								CNVRSN_RT = 1 / glFcRate.getFrXToUsd();
     							}

     							if (glJournal.getGlFunctionalCurrency().getFcCode().equals(adCompany.getGlFunctionalCurrency().getFcCode()))
     								JL_AMNT = glJournalLine.getJlAmount() / CNVRSN_RT;
     							else JL_AMNT = glJournalLine.getJlAmount();

   	    					} else {

   	    					    JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     							glJournal.getGlFunctionalCurrency().getFcCode(),
 									glJournal.getGlFunctionalCurrency().getFcName(),
 									glJournal.getJrConversionDate(),
 									glJournal.getJrConversionRate(),
 									glJournalLine.getJlAmount(), AD_CMPNY);
   	    					}



   	     					GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     					details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     					details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     					details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
 							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     					details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     					details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     					details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     					details.setGlJrEffectiveDate(glJournal.getJrEffectiveDate());
   	     					details.setGlJrDocumentNumber(glJournal.getJrDocumentNumber());
   	     					try {

   	     						LocalApCheck apCheck = apCheckHome.findByChkDocumentNumberAndBrCode(glJournal.getJrDocumentNumber(), AD_BRNCH, AD_CMPNY);
   	  	     					details.setGlJrCheckNumber(apCheck.getChkNumber());

   	     					}catch (FinderException ex) {
 								// TODO: handle exception
 							}


   	     					details.setGlJsName(glJournal.getGlJournalSource().getJsName());
   	     					details.setGlJrTin(glJournal.getJrTin());
   	     					details.setGlJrName(glJournal.getJrName());
   	     					details.setGlJrDescription(glJournalLine.getJlDescription() != null && glJournalLine.getJlDescription().length() > 0 ? glJournalLine.getJlDescription() : glJournal.getJrDescription());
   	     					details.setGlJlDebit(glJournalLine.getJlDebit());
   	     					details.setGlJlAmount(JL_AMNT);

   	     					details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     					details.setGlJrSubLedger(glJournal.getJrSubLedger());
   	     					details.setGlConversionRate(CNVRSN_RT);



   	     					//VAT RELIEF
   	     					if (RPRT_TYP != null || RPRT_TYP.contains("RELIEF")) {

   	     						details.setGlCompanyName(adCompany.getCmpName());
   	  	     					details.setGlCompanyTin(adCompany.getCmpTin());

   	  	     					details.setGlCompanyAddress(adCompany.getCmpAddress());
   	  	     					details.setGlCompanyCity(adCompany.getCmpCity());
   	  	     					details.setGlCompanyRevenueOffice(adCompany.getCmpRevenueOffice());
   	  	     					details.setGlCompanyFiscalYearEnding(adCompany.getCmpFiscalYearEnding());

   	  	     					details.setGlShowEntries(SHW_ENTRS);



   	     						if (details.getGlJsName().equals("ACCOUNTS PAYABLES")) {

   	     							try {
   	     								double AMOUNT_DUE = 0d;
   	     								String TAX_CODE = "";
   	     								LocalApSupplier apSupplier = null;
   	     								if (glJournal.getGlJournalCategory().getJcName().equals("VOUCHERS")) {
   	     									LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
   	     											glJournal.getJrDocumentNumber(), EJBCommon.FALSE, glJournal.getJrAdBranch(), AD_CMPNY);
   	     									apSupplier = apVoucher.getApSupplier();
   	     									try{
   	     										TAX_CODE = apVoucher.getApTaxCode().getTcName();
   	     									}catch (Exception e) {}

   	     									AMOUNT_DUE = apVoucher.getVouBillAmount();
   	     								} else if (glJournal.getGlJournalCategory().getJcName().equals("DEBIT MEMOS")) {
   	     									LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
   	     											glJournal.getJrDocumentNumber(), EJBCommon.TRUE, glJournal.getJrAdBranch(), AD_CMPNY);
   	     									apSupplier = apVoucher.getApSupplier();
   	     									try{
   	     										TAX_CODE = apVoucher.getApTaxCode().getTcName();
   	     									}catch (Exception e) {}

   	     									AMOUNT_DUE = apVoucher.getVouBillAmount();
   	     								} else if (glJournal.getGlJournalCategory().getJcName().equals("CHECKS")) {
   	     									LocalApCheck apCheck = apCheckHome.findByChkDocumentNumberAndBrCode(
   	     											glJournal.getJrDocumentNumber(), glJournal.getJrAdBranch(), AD_CMPNY);
   	     									apSupplier = apCheck.getApSupplier();
   	     									try{
   	     										TAX_CODE = apCheck.getApTaxCode().getTcName();
   	     									}catch (Exception e) {}

   	     									AMOUNT_DUE = apCheck.getChkAmount();
   	     								} else if (glJournal.getGlJournalCategory().getJcName().equals("RECEIVING ITEMS")) {
   	     									LocalApPurchaseOrder apPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
   	     											glJournal.getJrDocumentNumber(), EJBCommon.TRUE, glJournal.getJrAdBranch(), AD_CMPNY);
   	     									apSupplier = apPurchaseOrder.getApSupplier();
   	     									try{
   	     										TAX_CODE = apPurchaseOrder.getApTaxCode().getTcName();
   	     									}catch (Exception e) {}


   	     									Collection apPurchaseOrders = apPurchaseOrder.getApPurchaseOrderLines();
   	     									Iterator x = apPurchaseOrders.iterator();
   	     									AMOUNT_DUE = 0d;
   	     									while(x.hasNext()){
   	     										LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)x.next();
   	     										AMOUNT_DUE = apPurchaseOrderLine.getPlAmount();

   	     									}
   	     								}
   	     								details.setGlVatTaxCode(TAX_CODE);
   	     								details.setGlVatAmountDue(AMOUNT_DUE);
   	     								details.setGlVatTin(apSupplier.getSplTin() != null ? apSupplier.getSplTin().replace("-", "") : null);
   	     								details.setGlVatSubLedgerName(apSupplier.getSplName().toUpperCase());
   	     								details.setGlVatAddress1(apSupplier.getSplAddress() != null ? apSupplier.getSplAddress().toUpperCase() : null);
   	     								details.setGlVatAddress2(apSupplier.getSplCity() != null ? apSupplier.getSplCity().toUpperCase() : null);

   	     							} catch (FinderException ex) {

   	     							}
   	     						} else if (details.getGlJsName().equals("ACCOUNTS RECEIVABLES")) {

   	     							try {
   	     								double AMOUNT_DUE = 0d;
   	     								String TAX_CODE = "";
   	     								LocalArCustomer arCustomer = null;
   	     								if (glJournal.getGlJournalCategory().getJcName().equals("SALES INVOICES")) {
   	     									LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
   	     											glJournal.getJrDocumentNumber(), EJBCommon.FALSE, glJournal.getJrAdBranch(), AD_CMPNY);
   	     									arCustomer = arInvoice.getArCustomer();
   	     									AMOUNT_DUE = arInvoice.getInvAmountDue();
   	     									try{

   	     									}catch (Exception e) {}
   	     									TAX_CODE = arInvoice.getArTaxCode().getTcName();

   	     								} else if (glJournal.getGlJournalCategory().getJcName().equals("CREDIT MEMOS")) {
   	     									LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
   	     											glJournal.getJrDocumentNumber(), EJBCommon.TRUE, glJournal.getJrAdBranch(), AD_CMPNY);
   	     									arCustomer = arInvoice.getArCustomer();
   	     									AMOUNT_DUE = arInvoice.getInvAmountDue();
   	     									try{
   	     										TAX_CODE = arInvoice.getArTaxCode().getTcName();
   	     									}catch (Exception e) {}
   	     								} else if (glJournal.getGlJournalCategory().getJcName().equals("SALES RECEIPTS")) {
   	     									LocalArReceipt arReceipt = arReceiptHome.findByRctNumberAndBrCode(
   	     											glJournal.getJrDocumentNumber(), glJournal.getJrAdBranch(), AD_CMPNY);
   	     									arCustomer = arReceipt.getArCustomer();
   	     									AMOUNT_DUE = arReceipt.getRctAmount();
   	     									try{
   	     										TAX_CODE = arReceipt.getArTaxCode().getTcName();
   	     									}catch (Exception e) {}

   	     								}

   	     								details.setGlVatAmountDue(AMOUNT_DUE);
   	     								details.setGlVatTaxCode(TAX_CODE);
   	     								details.setGlVatTin(arCustomer.getCstTin() != null ? arCustomer.getCstTin().replace("-", "") : null);
   	     								details.setGlVatSubLedgerName(arCustomer.getCstName().toUpperCase());
   	     								details.setGlVatAddress1(arCustomer.getCstAddress() != null ? arCustomer.getCstAddress().toUpperCase() : null);
   	     								details.setGlVatAddress2(arCustomer.getCstCity() != null ? arCustomer.getCstCity().toUpperCase() : null);

   	     							} catch (FinderException ex) {

   	     							}

   	     						} else {
   	     							details.setGlVatSubLedgerName("" + glJournal.getJrDocumentNumber());
   	     						}


   	     					} else if(RPRT_TYP != null || RPRT_TYP.equals("GST RETURN")){
   	     						System.out.println("GST RETURN");

   	     						if (details.getGlJsName().equals("ACCOUNTS RECEIVABLES")) {

   	     							try {
   	     								double AMOUNT_DUE = 0d;
   	     								String TAX_CODE = "";
   	     								LocalArCustomer arCustomer = null;
   	     								if (glJournal.getGlJournalCategory().getJcName().equals("SALES INVOICES")) {
   	     									LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
   	     											glJournal.getJrDocumentNumber(), EJBCommon.FALSE, glJournal.getJrAdBranch(), AD_CMPNY);
   	     									arCustomer = arInvoice.getArCustomer();
   	     									AMOUNT_DUE = arInvoice.getInvAmountDue();
   	     									try{

   	     									}catch (Exception e) {}
   	     									TAX_CODE = arInvoice.getArTaxCode().getTcName();

   	     								} else if (glJournal.getGlJournalCategory().getJcName().equals("CREDIT MEMOS")) {
   	     									LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
   	     											glJournal.getJrDocumentNumber(), EJBCommon.TRUE, glJournal.getJrAdBranch(), AD_CMPNY);
   	     									arCustomer = arInvoice.getArCustomer();
   	     									AMOUNT_DUE = arInvoice.getInvAmountDue();
   	     									try{
   	     										TAX_CODE = arInvoice.getArTaxCode().getTcName();
   	     									}catch (Exception e) {}
   	     								} else if (glJournal.getGlJournalCategory().getJcName().equals("SALES RECEIPTS")) {
   	     									LocalArReceipt arReceipt = arReceiptHome.findByRctNumberAndBrCode(
   	     											glJournal.getJrDocumentNumber(), glJournal.getJrAdBranch(), AD_CMPNY);
   	     									arCustomer = arReceipt.getArCustomer();
   	     									AMOUNT_DUE = arReceipt.getRctAmount();
   	     									try{
   	     										TAX_CODE = arReceipt.getArTaxCode().getTcName();
   	     									}catch (Exception e) {}

   	     								}




   	     								details.setGlVatAmountDue(AMOUNT_DUE);
   	     								details.setGlVatTaxCode(TAX_CODE);

   	     							} catch (FinderException ex) {

   	     							}

   	     						} else {
   	     							details.setGlVatSubLedgerName("" + glJournal.getJrDocumentNumber());
   	     						}

   	     					}

   	     					details.setGlJrPosted(glJournal.getJrPosted());
   	     					details.setGlJrReferenceNumber(glJournal.getJrReferenceNumber());

   	     					list.add(details);

   	     				}

   	     				//include virtual revaluation entry
   	     				if(!CRRNCY.equals("") && glChartOfAccount.getCoaForRevaluation()==EJBCommon.TRUE) {

   	     					//get balances and COLUMN_AMOUNT
   	     					LocalGlAccountingCalendar glAccountingCalendar = glSetOfBook.getGlAccountingCalendar();

 	  	  					double BEGINNING_BALANCE = 0d;
 	  	  					double ACTIVITY = 0d;
 	  	  					int runningPeriod = GL_PRD_NMBR;
 	  	  					double COLUMN_AMOUNT = 0d;
 	  	  					double TEMP_AMOUNT = 0d;

 	  	  					while (runningPeriod <= glAccountingCalendarValue.getAcvPeriodNumber()) {

   								LocalGlAccountingCalendarValue glRunningAccountingCalendarValue =
   									glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
   											glAccountingCalendar.getAcCode(),
   											(short)runningPeriod, AD_CMPNY);

   								LocalGlChartOfAccountBalance glChartOfAccountBalance =
   									glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
   											glRunningAccountingCalendarValue.getAcvCode(),
   											glChartOfAccount.getCoaCode(), AD_CMPNY);

   								if (runningPeriod == GL_PRD_NMBR)
   									BEGINNING_BALANCE = glChartOfAccountBalance.getCoabBeginningBalance();

 								if (glChartOfAccount.getCoaAccountType().equals("ASSET") ||
 										glChartOfAccount.getCoaAccountType().equals("EXPENSE")) {

 									ACTIVITY += (glChartOfAccountBalance.getCoabTotalDebit() - glChartOfAccountBalance.getCoabTotalCredit());
 								} else {

 									ACTIVITY += (glChartOfAccountBalance.getCoabTotalCredit() - glChartOfAccountBalance.getCoabTotalDebit());
 								}

   								runningPeriod++;
   							}

 							COLUMN_AMOUNT += (BEGINNING_BALANCE + ACTIVITY);

   	     					double REVAL_AMOUNT = 0d;
   	     					double REVALUATED_BALANCE = 0d;
   	     					double CONVERSION_RATE = 1;
   	     					double JL_AMNT = 0d;
   	     					byte isDebit = EJBCommon.TRUE;
   	     					double CONVERSION_DISPLAY = 1;

   							LocalGlFunctionalCurrency glCoaCurrency = adCompany.getGlFunctionalCurrency();
   							if(glChartOfAccount.getGlFunctionalCurrency() != null)
   								glCoaCurrency = glChartOfAccount.getGlFunctionalCurrency();

   							if (CRRNCY.equals(adCompany.getGlFunctionalCurrency().getFcName())) {

   								//if CRRNCY = PHP
   		  	     				if (glChartOfAccount.getCoaAccountType().equals("ASSET") ||
   		  	     						glChartOfAccount.getCoaAccountType().equals("LIABILITY") ||
   		  	     						glChartOfAccount.getCoaAccountType().equals("OWNERS EQUITY")) {
   									if(!glCoaCurrency.getFcName().equals(CRRNCY)) {
   										if(glChartOfAccount.getCoaForRevaluation() == EJBCommon.TRUE) {

   											REVAL_AMOUNT += this.getRevaluatedColumnAmountInFunctionalCurrency(
   													glChartOfAccount, glAccountingCalendarValue, glCoaCurrency,
   													COLUMN_AMOUNT, GL_DT, glChartOfAccount.getCoaAccountType(), AD_CMPNY);

   										} else {

   											CONVERSION_RATE = this.getHistoricalRate(
   													glChartOfAccount, glAccountingCalendarValue, glCoaCurrency,
   													COLUMN_AMOUNT, GL_DT, glChartOfAccount.getCoaAccountType(), AD_CMPNY);

   											REVAL_AMOUNT += this.getCoaRevaluationAmount(glChartOfAccount,
   													glAccountingCalendarValue, adCompany.getGlFunctionalCurrency(), AD_CMPNY);
   										}
   									}
   								}

   							} else {

   								//if CRRNCY = USD

   								if(glChartOfAccount.getCoaAccountType().equals("REVENUE") || glChartOfAccount.getCoaAccountType().equals("EXPENSE")) {


   									if(COLUMN_AMOUNT != 0) {

   										CONVERSION_RATE = this.getHistoricalRate(
   												glChartOfAccount, glAccountingCalendarValue, glCoaCurrency,
   												COLUMN_AMOUNT, GL_DT, glChartOfAccount.getCoaAccountType(), AD_CMPNY);
   										TEMP_AMOUNT += (COLUMN_AMOUNT / CONVERSION_RATE);

   									}
   								} else {

 	  								REVAL_AMOUNT += this.getRevaluatedColumnAmountInForeignCurrency(
 	  										glChartOfAccount, glAccountingCalendarValue, glCoaCurrency,
 	  										COLUMN_AMOUNT, GL_DT, glChartOfAccount.getCoaAccountType(), AD_CMPNY);
   								}

   								CONVERSION_DISPLAY = 1 / (glFunctionalCurrencyRateHome.findByFcCodeAndDate(
   										glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(),
   												AD_CMPNY).getFcCode(), GL_DT, AD_CMPNY).getFrXToUsd());
   							}


   							if (!CRRNCY.equals(adCompany.getGlFunctionalCurrency().getFcName())) {


   								//USD
   								if(TEMP_AMOUNT != 0d)
   									REVALUATED_BALANCE = TEMP_AMOUNT;
   								else
 	  								REVALUATED_BALANCE = this.convertFunctionalToForeignCurrency(
 	  										glFunctionalCurrencyHome.findByFcName(CRRNCY, AD_CMPNY).getFcCode(), CRRNCY,
 	  										GL_DT, CONVERSION_RATE, COLUMN_AMOUNT + REVAL_AMOUNT, AD_CMPNY);
   							} else {

   								//PHP
   								REVALUATED_BALANCE = COLUMN_AMOUNT + REVAL_AMOUNT;
   							}

   							JL_AMNT = GL_RNNG_BLNC - REVALUATED_BALANCE;

      						if ((glChartOfAccount.getCoaAccountType().equals("ASSET") || glChartOfAccount.getCoaAccountType().equals("EXPENSE"))) {

      							if(JL_AMNT > 0) {
      								isDebit = EJBCommon.FALSE;
      								GL_RNNG_BLNC -= Math.abs(JL_AMNT);
      							}
      							else if(JL_AMNT < 0) {
      								isDebit = EJBCommon.TRUE;
      								GL_RNNG_BLNC += Math.abs(JL_AMNT);
      							}
      						} else {

      							if(JL_AMNT > 0) {
      								isDebit = EJBCommon.TRUE;
      								GL_RNNG_BLNC -= Math.abs(JL_AMNT);
      							}
      							else if(JL_AMNT < 0) {
      								isDebit = EJBCommon.FALSE;
      								GL_RNNG_BLNC += Math.abs(JL_AMNT);
      							}
      						}

   	     					GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     					details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     					details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     					details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
 							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     					details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     					details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     					details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     					details.setGlJrEffectiveDate(GL_DT);
   	     					details.setGlJrDocumentNumber("SYSTEM");
   	     					details.setGlJsName("MANUAL");
   	     					details.setGlJrTin(null);
   	     					details.setGlJrName(null);
   	     					details.setGlJrDescription("REVALUATION ON " + CRRNCY + " BOOKS");
   	     					details.setGlJlDebit(isDebit);
   	     					details.setGlJlAmount(Math.abs(JL_AMNT));

   	     					details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     					details.setGlJrSubLedger(null);
   	     					details.setGlConversionRate(CONVERSION_DISPLAY);
   	     					details.setGlJrPosted(EJBCommon.TRUE);
   	     					details.setGlJrReferenceNumber("SYSTEM");

   	     					list.add(details);
   	     				}


   	     				// include unposted subledger transactions

   	     				if(SL_INCLD_UNPSTD){

   	     					if(arINVDrs!=null && arINVDrs.size() > 0){
   	     						j = arINVDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

   	     							LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

   	     							double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     									arInvoice.getGlFunctionalCurrency().getFcCode(),
   	     									arInvoice.getGlFunctionalCurrency().getFcName(),
   	     									arInvoice.getInvConversionDate(),
   	     									arInvoice.getInvConversionRate(),
   	     									arDistributionRecord.getDrAmount(), AD_CMPNY);






   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(arInvoice.getInvDate());
   	     							details.setGlJrDocumentNumber(arInvoice.getInvNumber());
   	     							details.setGlJsName("ACCOUNTS RECEIVABLES");
   	     							details.setGlJrTin(arInvoice.getArCustomer().getCstTin());
   	     							details.setGlJrName(arInvoice.getInvReferenceNumber());
   	     							details.setGlJrDescription(arInvoice.getInvDescription());
   	     							details.setGlJlDebit(arDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(arInvoice.getArCustomer().getCstName());
   	     							details.setGlJrPosted(arInvoice.getInvPosted());
   	     							details.setGlJrReferenceNumber(null);

   	     							LocalArCustomer arCustomer = arInvoice.getArCustomer();
   	     							details.setGlVatTin(arCustomer.getCstTin() != null ? arCustomer.getCstTin().replace("-", "") : null);
   	     							details.setGlVatSubLedgerName(arCustomer.getCstName().toUpperCase());
   	     							details.setGlVatAddress1(arCustomer.getCstAddress() != null ? arCustomer.getCstAddress().toUpperCase() : null);
   	     							details.setGlVatAddress2(arCustomer.getCstCity() != null ? arCustomer.getCstCity().toUpperCase() : null);

   	     							details.setGlCompanyName(adCompany.getCmpName());
   	  	  	     					details.setGlCompanyTin(adCompany.getCmpTin());
   	  	  	     					details.setGlCompanyAddress(adCompany.getCmpAddress());
   	  	  	     					details.setGlCompanyCity(adCompany.getCmpCity());
   	  	  	     					details.setGlCompanyRevenueOffice(adCompany.getCmpRevenueOffice());
   	  	  	     					details.setGlCompanyFiscalYearEnding(adCompany.getCmpFiscalYearEnding());

   	     							list.add(details);

   	     						}
   	     					}

   	     					if(arCMDrs!=null && arCMDrs.size() > 0){
   	     						j = arCMDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

   	     							LocalArInvoice arCreditMemo = arDistributionRecord.getArInvoice();

   	     							LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
   	     									arCreditMemo.getInvCmInvoiceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

   	     							double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     									arInvoice.getGlFunctionalCurrency().getFcCode(),
   	     									arInvoice.getGlFunctionalCurrency().getFcName(),
   	     									arInvoice.getInvConversionDate(),
   	     									arInvoice.getInvConversionRate(),
   	     									arDistributionRecord.getDrAmount(), AD_CMPNY);


   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(arCreditMemo.getInvDate());
   	     							details.setGlJrDocumentNumber(arCreditMemo.getInvNumber());
   	     							details.setGlJsName("ACCOUNTS RECEIVABLES");
   	     							details.setGlJrTin(arCreditMemo.getArCustomer().getCstTin());
   	     							details.setGlJrName(arCreditMemo.getInvCmInvoiceNumber());
   	     							details.setGlJrDescription(arCreditMemo.getInvDescription());
   	     							details.setGlJlDebit(arDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(arCreditMemo.getArCustomer().getCstName());
   	     							details.setGlJrPosted(arCreditMemo.getInvPosted());
   	     							details.setGlJrReferenceNumber(null);

   	     							LocalArCustomer arCustomer = arCreditMemo.getArCustomer();
   	     							details.setGlVatTin(arCustomer.getCstTin() != null ? arCustomer.getCstTin().replace("-", "") : null);
   	     							details.setGlVatSubLedgerName(arCustomer.getCstName().toUpperCase());
   	     							details.setGlVatAddress1(arCustomer.getCstAddress() != null ? arCustomer.getCstAddress().toUpperCase() : null);
   	     							details.setGlVatAddress2(arCustomer.getCstCity() != null ? arCustomer.getCstCity().toUpperCase() : null);

   	     							details.setGlCompanyName(adCompany.getCmpName());
   	  	  	     					details.setGlCompanyTin(adCompany.getCmpTin());
   	  	  	     					details.setGlCompanyAddress(adCompany.getCmpAddress());
   	  	  	     					details.setGlCompanyCity(adCompany.getCmpCity());
   	  	  	     					details.setGlCompanyRevenueOffice(adCompany.getCmpRevenueOffice());
   	  	  	     					details.setGlCompanyFiscalYearEnding(adCompany.getCmpFiscalYearEnding());

   	     							list.add(details);

   	     						}
   	     					}

   	     					if(arRCTDrs!=null && arRCTDrs.size() > 0){
   	     						j = arRCTDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)j.next();

   	     							LocalArReceipt arReceipt = arDistributionRecord.getArReceipt();

   	     							double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     									arReceipt.getGlFunctionalCurrency().getFcCode(),
   	     									arReceipt.getGlFunctionalCurrency().getFcName(),
   	     									arReceipt.getRctConversionDate(),
   	     									arReceipt.getRctConversionRate(),
   	     									arDistributionRecord.getDrAmount(), AD_CMPNY);

   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(arReceipt.getRctDate());
   	     							details.setGlJrDocumentNumber(arReceipt.getRctNumber());
   	     							details.setGlJsName("ACCOUNTS RECEIVABLES");
   	     							details.setGlJrTin(arReceipt.getArCustomer().getCstTin());
   	     							details.setGlJrName(arReceipt.getRctReferenceNumber());
   	     							details.setGlJrDescription(arReceipt.getRctDescription());
   	     							details.setGlJlDebit(arDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(arReceipt.getRctCustomerName() == null ? arReceipt.getArCustomer().getCstName() : arReceipt.getRctCustomerName());
   	     							details.setGlJrPosted(arReceipt.getRctPosted());
   	     							details.setGlJrReferenceNumber(null);

   	     							LocalArCustomer arCustomer = arReceipt.getArCustomer();
   	     							details.setGlVatTin(arCustomer.getCstTin() != null ? arCustomer.getCstTin().replace("-", "") : null);
   	     							details.setGlVatSubLedgerName(arCustomer.getCstName().toUpperCase());
   	     							details.setGlVatAddress1(arCustomer.getCstAddress() != null ? arCustomer.getCstAddress().toUpperCase() : null);
   	     							details.setGlVatAddress2(arCustomer.getCstCity() != null ? arCustomer.getCstCity().toUpperCase() : null);

   	     							details.setGlCompanyName(adCompany.getCmpName());
   	  	  	     					details.setGlCompanyTin(adCompany.getCmpTin());
   	  	  	     					details.setGlCompanyAddress(adCompany.getCmpAddress());
   	  	  	     					details.setGlCompanyCity(adCompany.getCmpCity());
   	  	  	     					details.setGlCompanyRevenueOffice(adCompany.getCmpRevenueOffice());
   	  	  	     					details.setGlCompanyFiscalYearEnding(adCompany.getCmpFiscalYearEnding());

   	     							list.add(details);

   	     						}
   	     					}

   	     					if(apVOUDrs!=null && apVOUDrs.size() > 0){
   	     						j = apVOUDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

   	     							LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

   	     							double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     									apVoucher.getGlFunctionalCurrency().getFcCode(),
   	     									apVoucher.getGlFunctionalCurrency().getFcName(),
   	     									apVoucher.getVouConversionDate(),
   	     									apVoucher.getVouConversionRate(),
   	     									apDistributionRecord.getDrAmount(), AD_CMPNY);

   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(apVoucher.getVouDate());
   	     							details.setGlJrDocumentNumber(apVoucher.getVouDocumentNumber());
   	     							details.setGlJsName("ACCOUNTS PAYABLES");
   	     							details.setGlJrTin(apVoucher.getApSupplier().getSplTin());
   	     							details.setGlJrName(apVoucher.getVouReferenceNumber());
   	     							details.setGlJrDescription(apVoucher.getVouDescription());
   	     							details.setGlJlDebit(apDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(apVoucher.getApSupplier().getSplName());
   	     							details.setGlJrPosted(apVoucher.getVouPosted());
   	     							details.setGlJrReferenceNumber(null);

   	     							LocalApSupplier apSupplier = apVoucher.getApSupplier();
   	     							details.setGlVatTin(apSupplier.getSplTin() != null ? apSupplier.getSplTin().replace("-", "") : null);
   	     							details.setGlVatSubLedgerName(apSupplier.getSplName().toUpperCase());
   	     							details.setGlVatAddress1(apSupplier.getSplAddress() != null ? apSupplier.getSplAddress().toUpperCase() : null);
   	     							details.setGlVatAddress2(apSupplier.getSplCity() != null ? apSupplier.getSplCity().toUpperCase() : null);

   	     							details.setGlCompanyName(adCompany.getCmpName());
   	  	  	     					details.setGlCompanyTin(adCompany.getCmpTin());
   	  	  	     					details.setGlCompanyAddress(adCompany.getCmpAddress());
   	  	  	     					details.setGlCompanyCity(adCompany.getCmpCity());
   	  	  	     					details.setGlCompanyRevenueOffice(adCompany.getCmpRevenueOffice());
   	  	  	     					details.setGlCompanyFiscalYearEnding(adCompany.getCmpFiscalYearEnding());

   	     							list.add(details);

   	     						}
   	     					}

   	     					if(apDMDrs!=null && apDMDrs.size() > 0){
   	     						j = apDMDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

   	     							LocalApVoucher apDebitMemo = apDistributionRecord.getApVoucher();

   	     							LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
   	     									apDebitMemo.getVouDmVoucherNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

   	     							double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     									apVoucher.getGlFunctionalCurrency().getFcCode(),
   	     									apVoucher.getGlFunctionalCurrency().getFcName(),
   	     									apVoucher.getVouConversionDate(),
   	     									apVoucher.getVouConversionRate(),
   	     									apDistributionRecord.getDrAmount(), AD_CMPNY);

   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(apDebitMemo.getVouDate());
   	     							details.setGlJrDocumentNumber(apDebitMemo.getVouDocumentNumber());
   	     							details.setGlJsName("ACCOUNTS PAYABLES");
   	     							details.setGlJrTin(apDebitMemo.getApSupplier().getSplTin());
   	     							details.setGlJrName(apDebitMemo.getVouDmVoucherNumber());
   	     							details.setGlJrDescription(apDebitMemo.getVouDescription());
   	     							details.setGlJlDebit(apDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(apDebitMemo.getApSupplier().getSplName());
   	     							details.setGlJrPosted(apDebitMemo.getVouPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							LocalApSupplier apSupplier = apDebitMemo.getApSupplier();
   	     							details.setGlVatTin(apSupplier.getSplTin() != null ? apSupplier.getSplTin().replace("-", "") : null);
   	     							details.setGlVatSubLedgerName(apSupplier.getSplName().toUpperCase());
   	     							details.setGlVatAddress1(apSupplier.getSplAddress() != null ? apSupplier.getSplAddress().toUpperCase() : null);
   	     							details.setGlVatAddress2(apSupplier.getSplCity() != null ? apSupplier.getSplCity().toUpperCase() : null);

   	     							details.setGlCompanyName(adCompany.getCmpName());
   	  	  	     					details.setGlCompanyTin(adCompany.getCmpTin());
   	  	  	     					details.setGlCompanyAddress(adCompany.getCmpAddress());
   	  	  	     					details.setGlCompanyCity(adCompany.getCmpCity());
   	  	  	     					details.setGlCompanyRevenueOffice(adCompany.getCmpRevenueOffice());
   	  	  	     					details.setGlCompanyFiscalYearEnding(adCompany.getCmpFiscalYearEnding());

   	     							list.add(details);

   	     						}
   	     					}

   	     					if(apCHKDrs!=null && apCHKDrs.size() > 0){
   	     						j = apCHKDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

   	     							LocalApCheck apCheck = apDistributionRecord.getApCheck();

   	     							double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     									apCheck.getGlFunctionalCurrency().getFcCode(),
   	     									apCheck.getGlFunctionalCurrency().getFcName(),
   	     									apCheck.getChkConversionDate(),
   	     									apCheck.getChkConversionRate(),
   	     									apDistributionRecord.getDrAmount(), AD_CMPNY);

   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(apCheck.getChkDate());
   	     							details.setGlJrDocumentNumber(apCheck.getChkDocumentNumber());
   	     							details.setGlJrCheckNumber(apCheck.getChkNumber());
   	     							details.setGlJsName("ACCOUNTS PAYABLES");

   	     							details.setGlJrTin(apCheck.getApSupplier().getSplTin());
   	     							details.setGlJrName(apCheck.getChkReferenceNumber());
   	     							details.setGlJrDescription(apCheck.getChkDescription());
   	     							details.setGlJlDebit(apDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(apCheck.getChkSupplierName() == null ? apCheck.getApSupplier().getSplName() : apCheck.getChkSupplierName());
   	     							details.setGlJrPosted(apCheck.getChkPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							LocalApSupplier apSupplier = apCheck.getApSupplier();
   	     							details.setGlVatTin(apSupplier.getSplTin() != null ? apSupplier.getSplTin().replace("-", "") : null);
   	     							details.setGlVatSubLedgerName(apSupplier.getSplName().toUpperCase());
   	     							details.setGlVatAddress1(apSupplier.getSplAddress() != null ? apSupplier.getSplAddress().toUpperCase() : null);
   	     							details.setGlVatAddress2(apSupplier.getSplCity() != null ? apSupplier.getSplCity().toUpperCase() : null);

   	     							details.setGlCompanyName(adCompany.getCmpName());
   	  	  	     					details.setGlCompanyTin(adCompany.getCmpTin());
   	  	  	     					details.setGlCompanyAddress(adCompany.getCmpAddress());
   	  	  	     					details.setGlCompanyCity(adCompany.getCmpCity());
   	  	  	     					details.setGlCompanyRevenueOffice(adCompany.getCmpRevenueOffice());
   	  	  	     					details.setGlCompanyFiscalYearEnding(adCompany.getCmpFiscalYearEnding());

   	     							list.add(details);

   	     						}
   	     					}

   	     					if(apPODrs!=null && apPODrs.size() > 0){
   	     						j = apPODrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)j.next();

   	     							LocalApPurchaseOrder apPurchaseOrder = apDistributionRecord.getApPurchaseOrder();

   	     							double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     									apPurchaseOrder.getGlFunctionalCurrency().getFcCode(),
   	     									apPurchaseOrder.getGlFunctionalCurrency().getFcName(),
   	     									apPurchaseOrder.getPoConversionDate(),
   	     									apPurchaseOrder.getPoConversionRate(),
   	     									apDistributionRecord.getDrAmount(), AD_CMPNY);

   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(apPurchaseOrder.getPoDate());
   	     							details.setGlJrDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
   	     							details.setGlJsName("ACCOUNTS PAYABLES");
   	     							details.setGlJrTin(apPurchaseOrder.getApSupplier().getSplTin());
   	     							details.setGlJrName(apPurchaseOrder.getPoReferenceNumber());
   	     							details.setGlJrDescription(apPurchaseOrder.getPoDescription());
   	     							details.setGlJlDebit(apDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(apPurchaseOrder.getApSupplier().getSplName());
   	     							details.setGlJrPosted(apPurchaseOrder.getPoPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							LocalApSupplier apSupplier = apPurchaseOrder.getApSupplier();
   	     							details.setGlVatTin(apSupplier.getSplTin() != null ? apSupplier.getSplTin().replace("-", "") : null);
   	     							details.setGlVatSubLedgerName(apSupplier.getSplName().toUpperCase());
   	     							details.setGlVatAddress1(apSupplier.getSplAddress() != null ? apSupplier.getSplAddress().toUpperCase() : null);
   	     							details.setGlVatAddress2(apSupplier.getSplCity() != null ? apSupplier.getSplCity().toUpperCase() : null);

   	     							details.setGlCompanyName(adCompany.getCmpName());
   	  	  	     					details.setGlCompanyTin(adCompany.getCmpTin());
   	  	  	     					details.setGlCompanyAddress(adCompany.getCmpAddress());
   	  	  	     					details.setGlCompanyCity(adCompany.getCmpCity());
   	  	  	     					details.setGlCompanyRevenueOffice(adCompany.getCmpRevenueOffice());
   	  	  	     					details.setGlCompanyFiscalYearEnding(adCompany.getCmpFiscalYearEnding());

   	     							list.add(details);

   	     						}
   	     					}

   	     					if(cmFTDrs!=null && cmFTDrs.size() > 0){
   	     						j = cmFTDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

   	     							LocalCmFundTransfer cmFundTransfer = cmDistributionRecord.getCmFundTransfer();

   	     							LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmFundTransfer.getFtAdBaAccountFrom());

   	     							double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     									adBankAccount.getGlFunctionalCurrency().getFcCode(),
   	     									adBankAccount.getGlFunctionalCurrency().getFcName(),
   	     									cmFundTransfer.getFtConversionDate(),
   	     									cmFundTransfer.getFtConversionRateFrom(),
   	     									cmDistributionRecord.getDrAmount(), AD_CMPNY);


   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(cmFundTransfer.getFtDate());
   	     							details.setGlJrDocumentNumber(cmFundTransfer.getFtDocumentNumber());
   	     							details.setGlJsName("CASH MANAGEMENT");
   	     							details.setGlJrTin(null);
   	     							details.setGlJrName(cmFundTransfer.getFtReferenceNumber());
   	     							details.setGlJrDescription(cmFundTransfer.getFtMemo());
   	     							details.setGlJlDebit(cmDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(null);
   	     							details.setGlJrPosted(cmFundTransfer.getFtPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							details.setGlVatSubLedgerName(cmFundTransfer.getFtDocumentNumber());

   	     							list.add(details);

   	     						}
   	     					}

   	     					if(cmADJDrs!=null && cmADJDrs.size() > 0){
   	     						j = cmADJDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord)j.next();

   	     							LocalCmAdjustment cmAdjustment = cmDistributionRecord.getCmAdjustment();

   	     							LocalAdBankAccount adBankAccount = cmAdjustment.getAdBankAccount();

   	     							//LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmAdjustment.getAdBankAccount().getFtAdBaAccountFrom());

   	     							double JL_AMNT = this.convertForeignToFunctionalCurrency(
   	     									adBankAccount.getGlFunctionalCurrency().getFcCode(),
   	     									adBankAccount.getGlFunctionalCurrency().getFcName(),
   	     									cmAdjustment.getAdjConversionDate(),
   	     									cmAdjustment.getAdjConversionRate(),
   	     									cmDistributionRecord.getDrAmount(), AD_CMPNY);


   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(cmAdjustment.getAdjDate());
   	     							details.setGlJrDocumentNumber(cmAdjustment.getAdjDocumentNumber());
   	     							details.setGlJsName("CASH MANAGEMENT");
   	     							details.setGlJrTin(null);
   	     							details.setGlJrName(cmAdjustment.getAdjReferenceNumber());
   	     							details.setGlJrDescription(cmAdjustment.getAdjMemo());
   	     							details.setGlJlDebit(cmDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(null);
   	     							details.setGlJrPosted(cmAdjustment.getAdjPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							details.setGlVatSubLedgerName(cmAdjustment.getAdjDocumentNumber());
   	     							list.add(details);

   	     						}
   	     					}

   	     					if(invADJDrs!=null && invADJDrs.size() > 0){
   	     						j = invADJDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     							LocalInvAdjustment invAdjustment = invDistributionRecord.getInvAdjustment();

   	     							double JL_AMNT = invDistributionRecord.getDrAmount();


   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(invAdjustment.getAdjDate());
   	     							details.setGlJrDocumentNumber(invAdjustment.getAdjDocumentNumber());
   	     							details.setGlJsName("INVENTORY");
   	     							details.setGlJrTin(null);
   	     							details.setGlJrName(invAdjustment.getAdjReferenceNumber());
   	     							details.setGlJrDescription(invAdjustment.getAdjDescription());
   	     							details.setGlJlDebit(invDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(null);
   	     							details.setGlJrPosted(invAdjustment.getAdjPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							details.setGlVatSubLedgerName(invAdjustment.getAdjDocumentNumber());
   	     							list.add(details);

   	     						}
   	     					}

   	     					if(invBUADrs!=null && invBUADrs.size() > 0){
   	     						j = invBUADrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     							LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = invDistributionRecord.getInvBuildUnbuildAssembly();

   	     							double JL_AMNT = invDistributionRecord.getDrAmount();


   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(invBuildUnbuildAssembly.getBuaDate());
   	     							details.setGlJrDocumentNumber(invBuildUnbuildAssembly.getBuaDocumentNumber());
   	     							details.setGlJsName("INVENTORY");
   	     							details.setGlJrTin(null);
   	     							details.setGlJrName(invBuildUnbuildAssembly.getBuaReferenceNumber());
   	     							details.setGlJrDescription(invBuildUnbuildAssembly.getBuaDescription());
   	     							details.setGlJlDebit(invDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(null);
   	     							details.setGlJrPosted(invBuildUnbuildAssembly.getBuaPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							details.setGlVatSubLedgerName(invBuildUnbuildAssembly.getBuaDocumentNumber());
   	     							list.add(details);

   	     						}
   	     					}

   	     					if(invOHDrs!=null && invOHDrs.size() > 0){
   	     						j = invOHDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     							LocalInvOverhead invOverhead = invDistributionRecord.getInvOverhead();

   	     							double JL_AMNT = invDistributionRecord.getDrAmount();

   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(invOverhead.getOhDate());
   	     							details.setGlJrDocumentNumber(invOverhead.getOhDocumentNumber());
   	     							details.setGlJsName("INVENTORY");
   	     							details.setGlJrTin(null);
   	     							details.setGlJrName(invOverhead.getOhReferenceNumber());
   	     							details.setGlJrDescription(invOverhead.getOhDescription());
   	     							details.setGlJlDebit(invDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(null);
   	     							details.setGlJrPosted(invOverhead.getOhPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							details.setGlVatSubLedgerName(invOverhead.getOhDocumentNumber());
   	     							list.add(details);

   	     						}
   	     					}

   	     					if(invSIDrs!=null && invSIDrs.size() > 0){
   	     						j = invSIDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     							LocalInvStockIssuance invStockIssuance = invDistributionRecord.getInvStockIssuance();

   	     							double JL_AMNT = invDistributionRecord.getDrAmount();


   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(invStockIssuance.getSiDate());
   	     							details.setGlJrDocumentNumber(invStockIssuance.getSiDocumentNumber());
   	     							details.setGlJsName("INVENTORY");
   	     							details.setGlJrTin(null);
   	     							details.setGlJrName(invStockIssuance.getSiReferenceNumber());
   	     							details.setGlJrDescription(invStockIssuance.getSiDescription());
   	     							details.setGlJlDebit(invDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(null);
   	     							details.setGlJrPosted(invStockIssuance.getSiPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							details.setGlVatSubLedgerName(invStockIssuance.getSiDocumentNumber());
   	     							list.add(details);

   	     						}
   	     					}

   	     					if(invATRDrs!=null && invATRDrs.size() > 0){
   	     						j = invATRDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     							LocalInvAssemblyTransfer invAssemblyTransfer = invDistributionRecord.getInvAssemblyTransfer();

   	     							double JL_AMNT = invDistributionRecord.getDrAmount();

   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(invAssemblyTransfer.getAtrDate());
   	     							details.setGlJrDocumentNumber(invAssemblyTransfer.getAtrDocumentNumber());
   	     							details.setGlJsName("INVENTORY");
   	     							details.setGlJrTin(null);
   	     							details.setGlJrName(invAssemblyTransfer.getAtrReferenceNumber());
   	     							details.setGlJrDescription(invAssemblyTransfer.getAtrDescription());
   	     							details.setGlJlDebit(invDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(null);
   	     							details.setGlJrPosted(invAssemblyTransfer.getAtrPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							details.setGlVatSubLedgerName(invAssemblyTransfer.getAtrDocumentNumber());
   	     							list.add(details);

   	     						}
   	     					}

   	     					if(invSTDrs!=null && invSTDrs.size() > 0){
   	     						j = invSTDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     							LocalInvStockTransfer invStockTransfer = invDistributionRecord.getInvStockTransfer();

   	     							double JL_AMNT = invDistributionRecord.getDrAmount();

   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(invStockTransfer.getStDate());
   	     							details.setGlJrDocumentNumber(invStockTransfer.getStDocumentNumber());
   	     							details.setGlJsName("INVENTORY");
   	     							details.setGlJrTin(null);
   	     							details.setGlJrName(invStockTransfer.getStReferenceNumber());
   	     							details.setGlJrDescription(invStockTransfer.getStDescription());
   	     							details.setGlJlDebit(invDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(null);
   	     							details.setGlJrPosted(invStockTransfer.getStPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							details.setGlVatSubLedgerName(invStockTransfer.getStDocumentNumber());
   	     							list.add(details);

   	     						}
   	     					}

   	     					if(invBSTDrs!=null && invBSTDrs.size() > 0){
   	     						j = invBSTDrs.iterator();

   	     						while (j.hasNext()) {

   	     							LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

   	     							LocalInvBranchStockTransfer invBranchStockTransfer = invDistributionRecord.getInvBranchStockTransfer();

   	     							double JL_AMNT = invDistributionRecord.getDrAmount();

   	     							GlRepGeneralLedgerDetails details = new GlRepGeneralLedgerDetails();
   	     							details.setGlAccountNumber(glChartOfAccount.getCoaAccountNumber());
   	     							details.setGlCitCategory(glChartOfAccount.getCoaCitCategory());
   	     							details.setGlSawCategory(glChartOfAccount.getCoaSawCategory());
   	     							details.setGlIitCategory(glChartOfAccount.getCoaIitCategory());
   	     							details.setGlAccountDescription(glChartOfAccount.getCoaAccountDescription());
   	     							details.setGlBeginningBalance(COA_BGNNG_BLNC);
   	     							details.setGlEndingBalance(COA_ENDNG_BLNC);
   	     							details.setGlJrEffectiveDate(invBranchStockTransfer.getBstDate());
   	     							details.setGlJrDocumentNumber(invBranchStockTransfer.getBstNumber());
   	     							details.setGlJsName("INVENTORY");
   	     							details.setGlJrTin(null);
   	     							details.setGlJrName(null);
   	     							details.setGlJrDescription(invBranchStockTransfer.getBstDescription());
   	     							details.setGlJlDebit(invDistributionRecord.getDrDebit());
   	     							details.setGlJlAmount(JL_AMNT);

   	     							details.setGlAccountType(glChartOfAccount.getCoaAccountType());
   	     							details.setGlJrSubLedger(null);
   	     							details.setGlJrPosted(invBranchStockTransfer.getBstPosted());
   	     							details.setGlJrReferenceNumber(null);
   	     							details.setGlVatSubLedgerName(invBranchStockTransfer.getBstTransferOutNumber());
   	     							list.add(details);
   	     						}
   	     					}
   	     				}
   	     			}
   	     		}
 		 	}
   	     }//--------------------------------------------------------

 		 if (list.isEmpty()) {
 			 System.out.println("CHECK E");
 		 	throw new GlobalNoRecordFoundException();

 		 }


 		 if (ORDER_BY.contains("CHECK NUMBER")) {

 			 Collections.sort(list, GlRepGeneralLedgerDetails.OrderCheckNumberComparator);

 		 } else if (ORDER_BY.contains("DOCUMENT NUMBER")) {

 				 Collections.sort(list, GlRepGeneralLedgerDetails.OrderDocumentNumberComparator);

 		 } else if (ORDER_BY.contains("DATE")) {

 				 Collections.sort(list, GlRepGeneralLedgerDetails.OrderDateComparator);

 		 } else {

 			 Collections.sort(list, GlRepGeneralLedgerDetails.NoGroupComparator);


 		 }

 		 return list;

 		 /*Iterator testIter= list.iterator();
 		 ArrayList newList = new ArrayList();
 		 double beginningBalance = 0d;
 		 double runningBalance = 0d;
 		 String currentAccountNumber = GL_ACCNT_NMBR_FRM;

 		 Date firstDayOfPeriod = null;

 		 if(GL_AMNT_TYP.equals("PTD")) {
 			 Calendar dtCal = new GregorianCalendar();
 			 dtCal.setTime(GL_DT);

 			 //dtCal.add(Calendar.MONTH, -1);
 			 //dtCal.add(Calendar.DAY_OF_MONTH, 1);
 			 dtCal.set(Calendar.DATE,1);

 			 firstDayOfPeriod = dtCal.getTime();

 		 }else if(GL_AMNT_TYP.equals("QTD")) {

 			 LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndDate(
 					 glSetOfBook.getGlAccountingCalendar().getAcCode(), GL_DT, AD_CMPNY);

 			 Collection glQuarterAccountingCalendarValues =
 				 glAccountingCalendarValueHome.findByAcCodeAndAcvQuarterNumber(
 						 glSetOfBook.getGlAccountingCalendar().getAcCode(),
 						 glAccountingCalendarValue.getAcvQuarter(), AD_CMPNY);

 			 ArrayList glQuarterAccountingCalendarValueList = new ArrayList(glQuarterAccountingCalendarValues);

 			 LocalGlAccountingCalendarValue glQuarterAccountingCalendarValue =
 				 (LocalGlAccountingCalendarValue)glQuarterAccountingCalendarValueList.get(0);

 			 firstDayOfPeriod = glQuarterAccountingCalendarValue.getAcvDateFrom();
 		 }

 		 boolean isFirst = true;


 		 System.out.println("list.size()="+list.size());

 		 while(testIter.hasNext()) {

 			 GlRepGeneralLedgerDetails details = (GlRepGeneralLedgerDetails)testIter.next();
 			 System.out.println("details.getGlJrDocumentNumber()="+details.getGlJrDocumentNumber());
 			 System.out.println("details.getGlBeginningBalance()="+details.getGlBeginningBalance());
 			 if(details.getGlJrEffectiveDate()==null)
 				 details.setGlJrEffectiveDate(firstDayOfPeriod);

 			 System.out.println("isFirst="+isFirst);
 			 if(isFirst){

 				 beginningBalance = details.getGlBeginningBalance();
 				 System.out.println("isFirst beginningBalance="+beginningBalance);
 			 }else{
 				 if(!currentAccountNumber.equals(details.getGlAccountNumber())) {
 					 currentAccountNumber = details.getGlAccountNumber();
 					 beginningBalance = details.getGlBeginningBalance();
 				 }
 			 }

 			 isFirst = false;

 			 if(!GL_AMNT_TYP.equals("YTD")) {

 				 System.out.println("1---------------------------------->");
 				 if(details.getGlJrEffectiveDate().compareTo(firstDayOfPeriod) >= 0) {
 					 System.out.println("2---------------------------------->");

 					 details.setGlBeginningBalance(details.getGlBalance() +
 							 (details.getGlJlDebit() == (short)0 ? details.getGlJlAmount() : (details.getGlJlAmount() * -1)));
 					 if(details.getGlAccountType().equals("REVENUE") || details.getGlAccountType().equals("EXPENSE")){
 						 System.out.println("2.1---------------------------------->");
 						 details.setGlBalance(details.getGlBalance()-beginningBalance);
 						 details.setGlBeginningBalance(0d);
 					 }else{
 						 System.out.println("2.2---------------------------------->");
 						 details.setGlBeginningBalance(beginningBalance);
 					 }

 					 newList.add(details);

 				 } else {
 					 System.out.println("3---------------------------------->");
 					 beginningBalance = details.getGlBalance();
 					 System.out.println("else beginningBalance="+beginningBalance);
 				 }


 			 } else
 				 return list;
 		 }

 		 return newList;*/

 	  } catch (GlobalAccountNumberInvalidException ex) {

 	  	  throw ex;

 	  } catch (GlobalNoRecordFoundException ex) {

 	  	  throw ex;

 	  } catch (Exception ex) {

 	  	  Debug.printStackTrace(ex);
       	  throw new EJBException(ex.getMessage());

 	  }

    }








 //
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {

		Debug.print("GlJournalEntryControllerBean getGlFcAllWithDefault");

		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		Collection glFunctionalCurrencies = null;

		LocalGlFunctionalCurrency glFunctionalCurrency = null;
		LocalAdCompany adCompany = null;


		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
					EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (glFunctionalCurrencies.isEmpty()) {

			return null;

		}

		Iterator i = glFunctionalCurrencies.iterator();

		while (i.hasNext()) {

			glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

			GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
					glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
					adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
							EJBCommon.TRUE : EJBCommon.FALSE);

			list.add(mdetails);

		}

		return list;

	}


   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

      Debug.print("GlRepGeneralLedgerControllerBean getGlReportableAcvAll");

      LocalAdCompanyHome adCompanyHome = null;

	  // Initialize EJB Home

	  try {

	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

      try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         AdCompanyDetails details = new AdCompanyDetails();
         details.setCmpName(adCompany.getCmpName());
         details.setCmpTaxPayerName(adCompany.getCmpTaxPayerName());
         details.setCmpContact(adCompany.getCmpContact());
         details.setCmpPhone(adCompany.getCmpPhone());
         details.setCmpEmail(adCompany.getCmpEmail());
         details.setCmpTin(adCompany.getCmpTin());

         details.setCmpMailSectionNo(adCompany.getCmpMailSectionNo());
		  details.setCmpMailLotNo(adCompany.getCmpMailLotNo());
		    details.setCmpMailStreet(adCompany.getCmpMailStreet());
		    details.setCmpMailPoBox(adCompany.getCmpMailPoBox());
		    details.setCmpMailCountry(adCompany.getCmpMailCountry());
		    details.setCmpMailProvince(adCompany.getCmpMailProvince());
		    details.setCmpMailPostOffice(adCompany.getCmpMailPostOffice());
		    details.setCmpMailCareOff(adCompany.getCmpMailCareOff());
		    details.setCmpTaxPeriodFrom(adCompany.getCmpTaxPeriodFrom());
		    details.setCmpTaxPeriodTo(adCompany.getCmpTaxPeriodTo());
		    details.setCmpPublicOfficeName(adCompany.getCmpPublicOfficeName());
		    details.setCmpDateAppointment(adCompany.getCmpDateAppointment());

         return details;

      } catch (Exception ex) {

      	 Debug.printStackTrace(ex);
      	 throw new EJBException(ex.getMessage());

      }

   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGenSgAll(Integer AD_CMPNY) {

      Debug.print("GlRepGeneralLedgerControllerBean getGenSgAll");

      LocalAdCompanyHome adCompanyHome = null;
      LocalGenSegmentHome genSegmentHome = null;

      ArrayList list = new ArrayList();

	  // Initialize EJB Home

	  try {

	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	       genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

      try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         Collection genSegments = genSegmentHome.findByFlCode(adCompany.getGenField().getFlCode(), AD_CMPNY);

         Iterator i = genSegments.iterator();

         while (i.hasNext()) {

         	LocalGenSegment genSegment = (LocalGenSegment)i.next();

         	GenModSegmentDetails mdetails = new GenModSegmentDetails();
         	mdetails.setSgMaxSize(genSegment.getSgMaxSize());
         	mdetails.setSgFlSegmentSeparator(genSegment.getGenField().getFlSegmentSeparator());


         	list.add(mdetails);

         }

      	 return list;

      } catch (Exception ex) {

      	 Debug.printStackTrace(ex);
      	 throw new EJBException(ex.getMessage());

      }

   }

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList getGenVsAll(Integer AD_CMPNY) {

       Debug.print("GlRepGeneralLedgerController getGenVsAll");


       ArrayList list = new ArrayList();
       Collection genValueSets = null;

       LocalGenValueSetHome genValueSetHome = null;

       // Initialize EJB Home

       try {

           genValueSetHome = (LocalGenValueSetHome)EJBHomeFactory.
               lookUpLocalHome(LocalGenValueSetHome.JNDI_NAME, LocalGenValueSetHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

          genValueSets = genValueSetHome.findVsAll(AD_CMPNY);

       } catch (Exception ex) {

          throw new EJBException(ex.getMessage());

       }

       Iterator i = genValueSets.iterator();

       while (i.hasNext()) {

          LocalGenValueSet genValueSet = (LocalGenValueSet) i.next();

          list.add(genValueSet.getVsName());

       }

       return list;
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException{

        Debug.print("AdRepBankAccountListControllerBean getAdBrResAll");

        LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
        LocalAdBranchHome adBranchHome = null;

        LocalAdBranchResponsibility adBranchResponsibility = null;
        LocalAdBranch adBranch = null;

        Collection adBranchResponsibilities = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adBranchResponsibilities.isEmpty()) {
        	System.out.println("CHECK F");
            throw new GlobalNoRecordFoundException();

        }

        try {

            Iterator i = adBranchResponsibilities.iterator();

            while(i.hasNext()) {

                adBranchResponsibility = (LocalAdBranchResponsibility)i.next();

                adBranch = adBranchResponsibility.getAdBranch();

                AdBranchDetails details = new AdBranchDetails();

                details.setBrCode(adBranch.getBrCode());
                details.setBrBranchCode(adBranch.getBrBranchCode());
                details.setBrName(adBranch.getBrName());
                details.setBrHeadQuarter(adBranch.getBrHeadQuarter());

                list.add(details);

            }

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        return list;

    }

   // private methods

   private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

	    //Debug.print("GlRepGeneralLedgerControllerBean convertForeignToFunctionalCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT * CONVERSION_RATE;

         } else if (CONVERSION_DATE != null) {

         	 try {

         	 	 // Get functional currency rate

         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;

         	     if (!FC_NM.equals("USD")) {

        	         glReceiptFunctionalCurrencyRate =
	     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE,
	     	             CONVERSION_DATE, AD_CMPNY);

	     	         AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();

         	     }

                 // Get set of book functional currency rate if necessary

                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                             getFcCode(), CONVERSION_DATE, AD_CMPNY);

                     AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();

                  }


         	 } catch (Exception ex) {

         	 	throw new EJBException(ex.getMessage());

         	 }

         }

         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
	}

   // private methods from FRG Run

	private double getRevaluatedColumnAmountInFunctionalCurrency(LocalGlChartOfAccount glChartOfAccount,
			LocalGlAccountingCalendarValue glAccountingCalendarValue, LocalGlFunctionalCurrency glFunctionalCurrency,
			double COLUMN_AMOUNT, Date CONVERSION_DATE, String ACCOUNT_TYPE, Integer AD_CMPNY) {

		Debug.print("GlRepGeneralLedgerControllerBean getRevaluatedColumnAmountToFunctionalCurrency");

		LocalGlJournalLineHome glJournalLineHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalApVoucherHome apVoucherHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		double REVAL_AMOUNT = COLUMN_AMOUNT;
		double HIST_CNVRSN = 0d;
		double HIST_TOTAL_DEBIT = 0d;
		double HIST_TOTAL_CREDIT = 0d;
		double CURR_CNVRSN = 0d;
		double CURR_TOTAL_DEBIT = 0d;
		double CURR_TOTAL_CREDIT = 0d;

		// Initialize EJB Home

		try {

			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

           // find beg bal manual journals
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			Collection glJournalLines = glJournalLineHome.findJrByDateAndCoaAccountNumberAndCurrencyAndJrPostedAndJsName(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, "MANUAL", AD_CMPNY);

			Iterator i = glJournalLines.iterator();

			while (i.hasNext()) {

				LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION") &&
						!glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode().equals(adCompany.getGlFunctionalCurrency().getFcCode())){

					continue;
				}

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						glJournalLine.getJlAmount(), AD_CMPNY);

				// get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION"))
					HIST_CNVRSN=0;

				if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}
			}

            // find beg bal journal reversals
			glJournalLines = glJournalLineHome.findJrByDateAndCoaAccountNumberAndCurrencyAndJrPostedAndJsName(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, "JOURNAL REVERSAL", AD_CMPNY);

			i = glJournalLines.iterator();

			while (i.hasNext()) {

				LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION") &&
						!glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode().equals(adCompany.getGlFunctionalCurrency().getFcCode())){
					continue;
				}

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						glJournalLine.getJlAmount(), AD_CMPNY);

				// get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION"))
					HIST_CNVRSN=0;

				if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}
			}


			// get gl journals
		  glJournalLines = glJournalLineHome.findJrByDateAndCoaAccountNumberAndCurrencyAndJrPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			 i = glJournalLines.iterator();

			while (i.hasNext()) {

				LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION") &&
						!glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode().equals(adCompany.getGlFunctionalCurrency().getFcCode())){
					continue;
				}

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrConversionDate(),
						glJournalLine.getGlJournal().getJrConversionRate(),
						glJournalLine.getJlAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						glJournalLine.getJlAmount(), AD_CMPNY);

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION"))
					HIST_CNVRSN=0;

				if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}
			}

			// get ap vouchers
			Collection apDrVouchers = apDistributionRecordHome.findVouByDateAndCoaAccountNumberAndCurrencyAndVouPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrVouchers.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						apVoucher.getVouConversionDate(),
						apVoucher.getVouConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);


				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get ap debit memoes

			Collection apDrDebitMemoes = apDistributionRecordHome.findVouDebitMemoByDateAndCoaAccountNumberAndCurrencyAndVouPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrDebitMemoes.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

				LocalApVoucher apDebitedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
						apVoucher.getVouDmVoucherNumber(), EJBCommon.FALSE, apVoucher.getVouAdBranch(), AD_CMPNY);

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
						apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
						apDebitedVoucher.getVouConversionDate(),
						apDebitedVoucher.getVouConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
						apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get ap checks
			Collection apDrChecks = apDistributionRecordHome.findChkByDateAndCoaAccountNumberAndCurrencyAndChkPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrChecks.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				LocalGlFunctionalCurrency glDrFunctionalCurrency = null;
				Date conversionDate = null;
				double conversionRate = 0d;

				if (apDistributionRecord.getApAppliedVoucher() != null && apDistributionRecord.getDrClass().equals("PAYABLE")) {
					LocalApVoucher apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();
					glDrFunctionalCurrency = apVoucher.getGlFunctionalCurrency();
					conversionDate = apVoucher.getVouConversionDate();
					conversionRate = apVoucher.getVouConversionRate();
				} else {
					LocalApCheck apCheck = apDistributionRecord.getApCheck();
					glDrFunctionalCurrency = apCheck.getGlFunctionalCurrency();
					conversionDate = apCheck.getChkConversionDate();
					conversionRate = apCheck.getChkConversionRate();
				}

/*
				LocalApVoucher apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						apVoucher.getVouConversionDate(),
						apVoucher.getVouConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}*/

//				 get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						conversionDate,
						conversionRate,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}
			}


			// get ap direct checks
			Collection apDrDirectChecks = apDistributionRecordHome.findChkDirectByDateAndCoaAccountNumberAndCurrencyAndChkPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrDirectChecks.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcName(),
						apDistributionRecord.getApCheck().getChkConversionDate(),
						apDistributionRecord.getApCheck().getChkConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}
			}

			// get ap receiving items
			Collection apDrReceivingItems = apDistributionRecordHome.findPoByDateAndCoaAccountNumberAndCurrencyAndPoPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrReceivingItems.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcName(),
						apDistributionRecord.getApPurchaseOrder().getPoConversionDate(),
						apDistributionRecord.getApPurchaseOrder().getPoConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get ar invoices
			Collection arDrInvoices = arDistributionRecordHome.findInvByDateAndCoaAccountNumberAndCurrencyAndInvPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrInvoices.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						arInvoice.getGlFunctionalCurrency().getFcCode(),
						arInvoice.getGlFunctionalCurrency().getFcName(),
						arInvoice.getInvConversionDate(),
						arInvoice.getInvConversionRate(),
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						arInvoice.getGlFunctionalCurrency().getFcCode(),
						arInvoice.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get ar credit memoes
			Collection arDrCreditMemoes = arDistributionRecordHome.findInvCreditMemoByDateAndCoaAccountNumberAndCurrencyAndInvPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrCreditMemoes.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

				LocalArInvoice arCreditedVoucher = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
						arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						arCreditedVoucher.getGlFunctionalCurrency().getFcCode(),
						arCreditedVoucher.getGlFunctionalCurrency().getFcName(),
						arCreditedVoucher.getInvConversionDate(),
						arCreditedVoucher.getInvConversionRate(),
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						arCreditedVoucher.getGlFunctionalCurrency().getFcCode(),
						arCreditedVoucher.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get ar receipts
			Collection arDrReceipts = arDistributionRecordHome.findRctByDateAndCoaAccountNumberAndCurrencyAndRctPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrReceipts.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				LocalGlFunctionalCurrency glDrFunctionalCurrency = null;
				Date conversionDate = null;
				double conversionRate = 0d;

				if (arDistributionRecord.getArAppliedInvoice() != null && arDistributionRecord.getDrClass().equals("RECEIVABLE")) {
					LocalArInvoice arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice();
					glDrFunctionalCurrency = arInvoice.getGlFunctionalCurrency();
					conversionDate = arInvoice.getInvConversionDate();
					conversionRate = arInvoice.getInvConversionRate();
				} else {
					LocalArReceipt arReceipt= arDistributionRecord.getArReceipt();
					glDrFunctionalCurrency = arReceipt.getGlFunctionalCurrency();
					conversionDate = arReceipt.getRctConversionDate();
					conversionRate = arReceipt.getRctConversionRate();
				}

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						conversionDate,
						conversionRate,
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			Collection arDrMiscReceipts = arDistributionRecordHome.findRctMiscByDateAndCoaAccountNumberAndCurrencyAndRctPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrMiscReceipts.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcCode(),
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcName(),
						arDistributionRecord.getArReceipt().getRctConversionDate(),
						arDistributionRecord.getArReceipt().getRctConversionRate(),
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcCode(),
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get cm adjustment
			Collection cmDrAdjustments = cmDistributionRecordHome.findAdjByDateAndCoaAccountNumberAndCurrencyAndAdjPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = cmDrAdjustments.iterator();

			while (i.hasNext()) {

				LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcName(),
						cmDistributionRecord.getCmAdjustment().getAdjConversionDate(),
						cmDistributionRecord.getCmAdjustment().getAdjConversionRate(),
						cmDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						cmDistributionRecord.getDrAmount(), AD_CMPNY);

				if(cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get cm fund transfers
			Collection cmDrFundTransfers = cmDistributionRecordHome.findFtByDateAndCoaAccountNumberAndCurrencyAndFtPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = cmDrFundTransfers.iterator();

			while (i.hasNext()) {

				LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord) i.next();

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmDistributionRecord.getCmFundTransfer().getFtAdBaAccountFrom());

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						adBankAccount.getGlFunctionalCurrency().getFcCode(),
						adBankAccount.getGlFunctionalCurrency().getFcName(),
						cmDistributionRecord.getCmFundTransfer().getFtConversionDate(),
						cmDistributionRecord.getCmFundTransfer().getFtConversionRateFrom(),
						cmDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						adBankAccount.getGlFunctionalCurrency().getFcCode(),
						adBankAccount.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						cmDistributionRecord.getDrAmount(), AD_CMPNY);


				if(cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			if ("ASSET".equals(ACCOUNT_TYPE) || "EXPENSE".equals(ACCOUNT_TYPE)) {
				REVAL_AMOUNT = (CURR_TOTAL_DEBIT - CURR_TOTAL_CREDIT) - (HIST_TOTAL_DEBIT - HIST_TOTAL_CREDIT);
			} else {
				REVAL_AMOUNT = (CURR_TOTAL_CREDIT - CURR_TOTAL_DEBIT) - (HIST_TOTAL_CREDIT - HIST_TOTAL_DEBIT);
			}

			return REVAL_AMOUNT;


		} catch (Exception ex) {

			ex.printStackTrace();
			throw new EJBException(ex.getMessage());

		}

	}

	private double getRevaluatedColumnAmountInForeignCurrency(LocalGlChartOfAccount glChartOfAccount,
			LocalGlAccountingCalendarValue glAccountingCalendarValue, LocalGlFunctionalCurrency glFunctionalCurrency,
			double COLUMN_AMOUNT, Date CONVERSION_DATE, String ACCOUNT_TYPE, Integer AD_CMPNY) {

		Debug.print("GlRepGeneralLedgerControllerBean getRevaluatedColumnAmountInForeignCurrency");

		LocalGlJournalLineHome glJournalLineHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalApVoucherHome apVoucherHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		double REVAL_AMOUNT = COLUMN_AMOUNT;
		double HIST_CNVRSN = 0d;
		double HIST_TOTAL_DEBIT = 0d;
		double HIST_TOTAL_CREDIT = 0d;
		double CURR_CNVRSN = 0d;
		double CURR_TOTAL_DEBIT = 0d;
		double CURR_TOTAL_CREDIT = 0d;
		double FC_AMOUNT = 0d;

		// Initialize EJB Home

		try {

			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// find beg bal manual journals
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			Collection glJournalLines = glJournalLineHome.findJrByDateAndCoaAccountNumberAndCurrencyAndJrPostedAndJsName(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, "MANUAL", AD_CMPNY);

			Iterator i = glJournalLines.iterator();

			while (i.hasNext()) {

				LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION") &&
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode().equals(adCompany.getGlFunctionalCurrency().getFcCode())) {
					continue;
				}

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						glJournalLine.getJlAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION"))
					HIST_CNVRSN=0;

				if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}
			}

            // find beg bal journal reversals
			glJournalLines = glJournalLineHome.findJrByDateAndCoaAccountNumberAndCurrencyAndJrPostedAndJsName(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, "JOURNAL REVERSAL", AD_CMPNY);

			i = glJournalLines.iterator();

			while (i.hasNext()) {

				LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION") &&
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode().equals(adCompany.getGlFunctionalCurrency().getFcCode())) {
					continue;
				}

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						glJournalLine.getJlAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION"))
					HIST_CNVRSN=0;

				if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}
			}

			// get gl journals
		    glJournalLines = glJournalLineHome.findJrByDateAndCoaAccountNumberAndCurrencyAndJrPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);


			 i = glJournalLines.iterator();

			while (i.hasNext()) {

				LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION") &&
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode().equals(adCompany.getGlFunctionalCurrency().getFcCode())){
					continue;
				}

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrConversionDate(),
						glJournalLine.getGlJournal().getJrConversionRate(),
						glJournalLine.getJlAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrConversionDate(), glJournalLine.getGlJournal().getJrConversionRate(),
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);
				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION"))
					HIST_CNVRSN=0;

				if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}
			}

			// get ap vouchers
			Collection apDrVouchers = apDistributionRecordHome.findVouByDateAndCoaAccountNumberAndCurrencyAndVouPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrVouchers.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						apVoucher.getVouConversionDate(),
						apVoucher.getVouConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						apVoucher.getVouConversionDate(),
						apVoucher.getVouConversionRate(),
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get ap debit memoes
			Collection apDrDebitMemoes = apDistributionRecordHome.findVouDebitMemoByDateAndCoaAccountNumberAndCurrencyAndVouPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrDebitMemoes.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

				LocalApVoucher apDebitedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
						apVoucher.getVouDmVoucherNumber(), EJBCommon.FALSE, apVoucher.getVouAdBranch(), AD_CMPNY);

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
						apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
						apDebitedVoucher.getVouConversionDate(),
						apDebitedVoucher.getVouConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
						apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
						apDebitedVoucher.getVouConversionDate(),
						apDebitedVoucher.getVouConversionRate(),
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
						apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get ap checks
			Collection apDrChecks = apDistributionRecordHome.findChkByDateAndCoaAccountNumberAndCurrencyAndChkPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrChecks.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				LocalGlFunctionalCurrency glDrFunctionalCurrency = null;
				Date conversionDate = null;
				double conversionRate = 0d;

				if (apDistributionRecord.getApAppliedVoucher() != null && apDistributionRecord.getDrClass().equals("PAYABLE")) {
					LocalApVoucher apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();
					glDrFunctionalCurrency = apVoucher.getGlFunctionalCurrency();
					conversionDate = apVoucher.getVouConversionDate();
					conversionRate = apVoucher.getVouConversionRate();
				} else {
					LocalApCheck apCheck = apDistributionRecord.getApCheck();
					glDrFunctionalCurrency = apCheck.getGlFunctionalCurrency();
					conversionDate = apCheck.getChkConversionDate();
					conversionRate = apCheck.getChkConversionRate();
				}

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						conversionDate,
						conversionRate,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						conversionDate,
						conversionRate,
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}
			}

			// get ap direct checks
			Collection apDrDirectChecks = apDistributionRecordHome.findChkDirectByDateAndCoaAccountNumberAndCurrencyAndChkPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrDirectChecks.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcName(),
						apDistributionRecord.getApCheck().getChkConversionDate(),
						apDistributionRecord.getApCheck().getChkConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcName(),
						apDistributionRecord.getApCheck().getChkConversionDate(),
						apDistributionRecord.getApCheck().getChkConversionRate(),
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}
			}

			// get ap receiving items
			Collection apDrReceivingItems = apDistributionRecordHome.findPoByDateAndCoaAccountNumberAndCurrencyAndPoPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrReceivingItems.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcName(),
						apDistributionRecord.getApPurchaseOrder().getPoConversionDate(),
						apDistributionRecord.getApPurchaseOrder().getPoConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcName(),
						apDistributionRecord.getApPurchaseOrder().getPoConversionDate(),
						apDistributionRecord.getApPurchaseOrder().getPoConversionRate(),
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get ar invoices
			Collection arDrInvoices = arDistributionRecordHome.findInvByDateAndCoaAccountNumberAndCurrencyAndInvPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrInvoices.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						arInvoice.getGlFunctionalCurrency().getFcCode(),
						arInvoice.getGlFunctionalCurrency().getFcName(),
						arInvoice.getInvConversionDate(),
						arInvoice.getInvConversionRate(),
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						arInvoice.getGlFunctionalCurrency().getFcCode(),
						arInvoice.getGlFunctionalCurrency().getFcName(),
						arInvoice.getInvConversionDate(),
						arInvoice.getInvConversionRate(),
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						arInvoice.getGlFunctionalCurrency().getFcCode(),
						arInvoice.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get ar credit memoes
			Collection arDrCreditMemoes = arDistributionRecordHome.findInvCreditMemoByDateAndCoaAccountNumberAndCurrencyAndInvPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrCreditMemoes.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

				LocalArInvoice arCreditedVoucher = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
						arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						arCreditedVoucher.getGlFunctionalCurrency().getFcCode(),
						arCreditedVoucher.getGlFunctionalCurrency().getFcName(),
						arCreditedVoucher.getInvConversionDate(),
						arCreditedVoucher.getInvConversionRate(),
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						arCreditedVoucher.getGlFunctionalCurrency().getFcCode(),
						arCreditedVoucher.getGlFunctionalCurrency().getFcName(),
						arCreditedVoucher.getInvConversionDate(),
						arCreditedVoucher.getInvConversionRate(),
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						arCreditedVoucher.getGlFunctionalCurrency().getFcCode(),
						arCreditedVoucher.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get ar receipts
			Collection arDrReceipts = arDistributionRecordHome.findRctByDateAndCoaAccountNumberAndCurrencyAndRctPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrReceipts.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				LocalGlFunctionalCurrency glDrFunctionalCurrency = null;
				Date conversionDate = null;
				double conversionRate = 0d;

				if (arDistributionRecord.getArAppliedInvoice() != null && arDistributionRecord.getDrClass().equals("RECEIVABLE")) {
					LocalArInvoice arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice();
					glDrFunctionalCurrency = arInvoice.getGlFunctionalCurrency();
					conversionDate = arInvoice.getInvConversionDate();
					conversionRate = arInvoice.getInvConversionRate();
				} else {
					LocalArReceipt arReceipt= arDistributionRecord.getArReceipt();
					glDrFunctionalCurrency = arReceipt.getGlFunctionalCurrency();
					conversionDate = arReceipt.getRctConversionDate();
					conversionRate = arReceipt.getRctConversionRate();
				}

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						conversionDate,
						conversionRate,
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						conversionDate,
						conversionRate,
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get ar misc receipts
			Collection arDrMiscReceipts = arDistributionRecordHome.findRctMiscByDateAndCoaAccountNumberAndCurrencyAndRctPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrMiscReceipts.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcCode(),
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcName(),
						arDistributionRecord.getArReceipt().getRctConversionDate(),
						arDistributionRecord.getArReceipt().getRctConversionRate(),
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcCode(),
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcName(),
						arDistributionRecord.getArReceipt().getRctConversionDate(),
						arDistributionRecord.getArReceipt().getRctConversionRate(),
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcCode(),
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get cm adjustment
			Collection cmDrAdjustments = cmDistributionRecordHome.findAdjByDateAndCoaAccountNumberAndCurrencyAndAdjPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = cmDrAdjustments.iterator();

			while (i.hasNext()) {

				LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcName(),
						cmDistributionRecord.getCmAdjustment().getAdjConversionDate(),
						cmDistributionRecord.getCmAdjustment().getAdjConversionRate(),
						cmDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcName(),
						cmDistributionRecord.getCmAdjustment().getAdjConversionDate(),
						cmDistributionRecord.getCmAdjustment().getAdjConversionRate(),
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			// get cm fund transfers
			Collection cmDrFundTransfers = cmDistributionRecordHome.findFtByDateAndCoaAccountNumberAndCurrencyAndFtPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = cmDrFundTransfers.iterator();

			while (i.hasNext()) {

				LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord) i.next();

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmDistributionRecord.getCmFundTransfer().getFtAdBaAccountFrom());

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						adBankAccount.getGlFunctionalCurrency().getFcCode(),
						adBankAccount.getGlFunctionalCurrency().getFcName(),
						cmDistributionRecord.getCmFundTransfer().getFtConversionDate(),
						cmDistributionRecord.getCmFundTransfer().getFtConversionRateFrom(),
						cmDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						adBankAccount.getGlFunctionalCurrency().getFcCode(),
						adBankAccount.getGlFunctionalCurrency().getFcName(),
						cmDistributionRecord.getCmFundTransfer().getFtConversionDate(),
						cmDistributionRecord.getCmFundTransfer().getFtConversionRateFrom(),
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						adBankAccount.getGlFunctionalCurrency().getFcCode(),
						adBankAccount.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					CURR_TOTAL_DEBIT += CURR_CNVRSN;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					CURR_TOTAL_CREDIT += CURR_CNVRSN;
				}

			}

			if ("ASSET".equals(ACCOUNT_TYPE) || "EXPENSE".equals(ACCOUNT_TYPE)) {
				REVAL_AMOUNT = (CURR_TOTAL_DEBIT - CURR_TOTAL_CREDIT) - (HIST_TOTAL_DEBIT - HIST_TOTAL_CREDIT);
			} else {
				REVAL_AMOUNT = (CURR_TOTAL_CREDIT - CURR_TOTAL_DEBIT) - (HIST_TOTAL_CREDIT - HIST_TOTAL_DEBIT);
			}

			return REVAL_AMOUNT;

		} catch (Exception ex) {

			ex.printStackTrace();
			throw new EJBException(ex.getMessage());

		}

	}



	private double getHistoricalRate(LocalGlChartOfAccount glChartOfAccount,
			LocalGlAccountingCalendarValue glAccountingCalendarValue, LocalGlFunctionalCurrency glFunctionalCurrency,
			double COLUMN_AMOUNT, Date CONVERSION_DATE, String ACCOUNT_TYPE, Integer AD_CMPNY) {

		Debug.print("GlRepFinancialReportRunControllerBean getHistoricalRate");

		LocalGlJournalLineHome glJournalLineHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalApVoucherHome apVoucherHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		double REVAL_AMOUNT = COLUMN_AMOUNT;
		double HIST_CNVRSN = 0d;
		double HIST_TOTAL_DEBIT = 0d;
		double HIST_TOTAL_CREDIT = 0d;
		double FC_TOTAL_DEBIT = 0d;
		double FC_TOTAL_CREDIT = 0d;
		double CURR_CNVRSN = 0d;

		// Initialize EJB Home

		try {

			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {


           // find beg bal manual journals
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			Collection glJournalLines = glJournalLineHome.findJrByDateAndCoaAccountNumberAndCurrencyAndJrPostedAndJsName(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, "MANUAL", AD_CMPNY);

			Iterator i = glJournalLines.iterator();

			while (i.hasNext()) {

				LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						glJournalLine.getJlAmount(), AD_CMPNY);


				// get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}

					System.out.println("GL Manual Journal");
					System.out.println("HIST_CNVRSN : " + HIST_CNVRSN);
					System.out.println("FC_AMOUNT : " + FC_AMOUNT);
			}

            // find beg bal journal reversals
			glJournalLines = glJournalLineHome.findJrByDateAndCoaAccountNumberAndCurrencyAndJrPostedAndJsName(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, "JOURNAL REVERSAL", AD_CMPNY);

			i = glJournalLines.iterator();

			while (i.hasNext()) {

				LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						glJournalLine.getJlAmount(), AD_CMPNY);


				// get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						FC_AMOUNT, AD_CMPNY);

				if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}

					System.out.println("GL Journal Reversal");
					System.out.println("HIST_CNVRSN : " + HIST_CNVRSN);
					System.out.println("FC_AMOUNT : " + FC_AMOUNT);
			}


			// get gl journals
		  glJournalLines = glJournalLineHome.findJrByDateAndCoaAccountNumberAndCurrencyAndJrPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			 i = glJournalLines.iterator();

			while (i.hasNext()) {

				LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrConversionDate(),
						glJournalLine.getGlJournal().getJrConversionRate(),
						glJournalLine.getJlAmount(), AD_CMPNY);

				//	get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrConversionDate(),
						glJournalLine.getGlJournal().getJrConversionRate(),
						/*glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						glJournalLine.getGlJournal().getJrEffectiveDate(), 1d,*/
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						glJournalLine.getJlAmount(), AD_CMPNY);

				if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}

					System.out.println("GL Journal");
					System.out.println("HIST_CNVRSN : " + HIST_CNVRSN);
					System.out.println("FC_AMOUNT : " + FC_AMOUNT);
			}

			// get ap vouchers

			Collection apDrVouchers = apDistributionRecordHome.findVouByDateAndCoaAccountNumberAndCurrencyAndVouPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			if(apDrVouchers.isEmpty())
				apDrVouchers = apDistributionRecordHome.findVouByDateAndCoaAccountNumberAndCurrencyAndVouPosted(
						glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
						glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrVouchers.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

				Date conversionDate = null;
				double conversionRate = 0d;

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						apVoucher.getVouConversionDate(),
						apVoucher.getVouConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apVoucher.getVouConversionDate() != null) {

					conversionDate = apVoucher.getVouConversionDate();
					conversionRate = apVoucher.getVouConversionRate();

				} else {

					conversionDate = apVoucher.getVouDate();
					conversionRate = 1d;
				}

				//	get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						//apVoucher.getGlFunctionalCurrency().getFcCode(),
						//apVoucher.getGlFunctionalCurrency().getFcName(),
						//apVoucher.getVouConversionDate(),
						//apVoucher.getVouConversionRate(),
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						conversionDate, conversionRate,
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}
					System.out.println("AP Voucher");
					System.out.println("HIST_CNVRSN : " + HIST_CNVRSN);
					System.out.println("FC_AMOUNT : " + FC_AMOUNT);
			}

			// get ap debit memoes

			Collection apDrDebitMemoes = apDistributionRecordHome.findVouDebitMemoByDateAndCoaAccountNumberAndCurrencyAndVouPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrDebitMemoes.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				LocalApVoucher apVoucher = apDistributionRecord.getApVoucher();

				LocalApVoucher apDebitedVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
						apVoucher.getVouDmVoucherNumber(), EJBCommon.FALSE, apVoucher.getVouAdBranch(), AD_CMPNY);

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
						apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
						apDebitedVoucher.getVouConversionDate(),
						apDebitedVoucher.getVouConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

//				get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						/*apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
						apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
						apDebitedVoucher.getVouConversionDate(),
						apDebitedVoucher.getVouConversionRate(),*/
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						apDebitedVoucher.getVouDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDebitedVoucher.getGlFunctionalCurrency().getFcCode(),
						apDebitedVoucher.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}
			}

			// get ap checks
			Collection apDrChecks = apDistributionRecordHome.findChkByDateAndCoaAccountNumberAndCurrencyAndChkPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrChecks.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				LocalGlFunctionalCurrency glDrFunctionalCurrency = null;
				Date apDate = null;
				Date conversionDate = null;
				double conversionRate = 0d;

				if (apDistributionRecord.getApAppliedVoucher() != null && apDistributionRecord.getDrClass().equals("PAYABLE")) {
					LocalApVoucher apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();
					glDrFunctionalCurrency = apVoucher.getGlFunctionalCurrency();
					conversionDate = apVoucher.getVouConversionDate();
					conversionRate = apVoucher.getVouConversionRate();
					apDate = apVoucher.getVouDate();
				} else {
					LocalApCheck apCheck = apDistributionRecord.getApCheck();
					glDrFunctionalCurrency = apCheck.getGlFunctionalCurrency();
					conversionDate = apCheck.getChkConversionDate();
					conversionRate = apCheck.getChkConversionRate();
					apDate = apCheck.getChkDate();
				}

/*
				LocalApVoucher apVoucher = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						apVoucher.getVouConversionDate(),
						apVoucher.getVouConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apVoucher.getGlFunctionalCurrency().getFcCode(),
						apVoucher.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}*/

//				 get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						conversionDate,
						conversionRate,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

//				get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						/*glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						conversionDate,
						conversionRate,*/
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						apDate, 1d,
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}
			}


			// get ap direct checks
			Collection apDrDirectChecks = apDistributionRecordHome.findChkDirectByDateAndCoaAccountNumberAndCurrencyAndChkPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrDirectChecks.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcName(),
						apDistributionRecord.getApCheck().getChkConversionDate(),
						apDistributionRecord.getApCheck().getChkConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

//				get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						/*apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcName(),
						apDistributionRecord.getApCheck().getChkConversionDate(),
						apDistributionRecord.getApCheck().getChkConversionRate(),*/
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						apDistributionRecord.getApCheck().getChkDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}
			}

			// get ap receiving items
			Collection apDrReceivingItems = apDistributionRecordHome.findPoByDateAndCoaAccountNumberAndCurrencyAndPoPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = apDrReceivingItems.iterator();

			while (i.hasNext()) {

				LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcName(),
						apDistributionRecord.getApPurchaseOrder().getPoConversionDate(),
						apDistributionRecord.getApPurchaseOrder().getPoConversionRate(),
						apDistributionRecord.getDrAmount(), AD_CMPNY);

//				get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						/*apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcName(),
						apDistributionRecord.getApPurchaseOrder().getPoConversionDate(),
						apDistributionRecord.getApPurchaseOrder().getPoConversionRate(),*/
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						apDistributionRecord.getApPurchaseOrder().getPoDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcCode(),
						apDistributionRecord.getApPurchaseOrder().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						apDistributionRecord.getDrAmount(), AD_CMPNY);

				if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}
			}

			// get ar invoices
			Collection arDrInvoices = arDistributionRecordHome.findInvByDateAndCoaAccountNumberAndCurrencyAndInvPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			if(arDrInvoices.isEmpty())
				arDrInvoices = arDistributionRecordHome.findInvByDateAndCoaAccountNumberAndCurrencyAndInvPosted(
						glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
						glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrInvoices.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

				Date conversionDate = null;
				double conversionRate = 0d;

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						arInvoice.getGlFunctionalCurrency().getFcCode(),
						arInvoice.getGlFunctionalCurrency().getFcName(),
						arInvoice.getInvConversionDate(),
						arInvoice.getInvConversionRate(),
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				if(arInvoice.getInvConversionDate() != null) {

					conversionDate = arInvoice.getInvConversionDate();
					conversionRate = arInvoice.getInvConversionRate();

				} else {

					conversionDate = arInvoice.getInvDate();
					conversionRate = 1d;
				}

				//get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						/*arInvoice.getGlFunctionalCurrency().getFcCode(),
						arInvoice.getGlFunctionalCurrency().getFcName(),
						arInvoice.getInvConversionDate(),
						arInvoice.getInvConversionRate(),*/
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						conversionDate, conversionRate,
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						arInvoice.getGlFunctionalCurrency().getFcCode(),
						arInvoice.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}

					System.out.println("AR Invoice");
					System.out.println("HIST_CNVRSN : " + HIST_CNVRSN);
					System.out.println("FC_AMOUNT : " + FC_AMOUNT);
			}

			// get ar credit memoes
			Collection arDrCreditMemoes = arDistributionRecordHome.findInvCreditMemoByDateAndCoaAccountNumberAndCurrencyAndInvPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrCreditMemoes.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				LocalArInvoice arInvoice = arDistributionRecord.getArInvoice();

				LocalArInvoice arCreditedVoucher = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
						arInvoice.getInvCmInvoiceNumber(), EJBCommon.FALSE, arInvoice.getInvAdBranch(), AD_CMPNY);

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						arCreditedVoucher.getGlFunctionalCurrency().getFcCode(),
						arCreditedVoucher.getGlFunctionalCurrency().getFcName(),
						arCreditedVoucher.getInvConversionDate(),
						arCreditedVoucher.getInvConversionRate(),
						arDistributionRecord.getDrAmount(), AD_CMPNY);

//				get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						/*arCreditedVoucher.getGlFunctionalCurrency().getFcCode(),
						arCreditedVoucher.getGlFunctionalCurrency().getFcName(),
						arCreditedVoucher.getInvConversionDate(),
						arCreditedVoucher.getInvConversionRate(),*/
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						arCreditedVoucher.getInvDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						arCreditedVoucher.getGlFunctionalCurrency().getFcCode(),
						arCreditedVoucher.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}
			}

			// get ar receipts
			Collection arDrReceipts = arDistributionRecordHome.findRctByDateAndCoaAccountNumberAndCurrencyAndRctPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrReceipts.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				LocalGlFunctionalCurrency glDrFunctionalCurrency = null;
				Date arDate = null;
				Date conversionDate = null;
				double conversionRate = 0d;

				if (arDistributionRecord.getArAppliedInvoice() != null && arDistributionRecord.getDrClass().equals("RECEIVABLE")) {
					LocalArInvoice arInvoice = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice();
					glDrFunctionalCurrency = arInvoice.getGlFunctionalCurrency();
					conversionDate = arInvoice.getInvConversionDate();
					conversionRate = arInvoice.getInvConversionRate();
					arDate = arInvoice.getInvDate();
				} else {
					LocalArReceipt arReceipt= arDistributionRecord.getArReceipt();
					glDrFunctionalCurrency = arReceipt.getGlFunctionalCurrency();
					conversionDate = arReceipt.getRctConversionDate();
					conversionRate = arReceipt.getRctConversionRate();
					arDate = arReceipt.getRctDate();
				}

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						conversionDate,
						conversionRate,
						arDistributionRecord.getDrAmount(), AD_CMPNY);

//				get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						/*glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						conversionDate,
						conversionRate,*/
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						arDate, 1d,
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						glDrFunctionalCurrency.getFcCode(),
						glDrFunctionalCurrency.getFcName(),
						CONVERSION_DATE, 1d,
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}
			}

			Collection arDrMiscReceipts = arDistributionRecordHome.findRctMiscByDateAndCoaAccountNumberAndCurrencyAndRctPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = arDrMiscReceipts.iterator();

			while (i.hasNext()) {

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcCode(),
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcName(),
						arDistributionRecord.getArReceipt().getRctConversionDate(),
						arDistributionRecord.getArReceipt().getRctConversionRate(),
						arDistributionRecord.getDrAmount(), AD_CMPNY);

//				get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						/*arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcCode(),
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcName(),
						arDistributionRecord.getArReceipt().getRctConversionDate(),
						arDistributionRecord.getArReceipt().getRctConversionRate(),*/
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						arDistributionRecord.getArReceipt().getRctDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcCode(),
						arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						arDistributionRecord.getDrAmount(), AD_CMPNY);

				if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}
			}

			// get cm adjustment
			Collection cmDrAdjustments = cmDistributionRecordHome.findAdjByDateAndCoaAccountNumberAndCurrencyAndAdjPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = cmDrAdjustments.iterator();

			while (i.hasNext()) {

				LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord) i.next();

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcName(),
						cmDistributionRecord.getCmAdjustment().getAdjConversionDate(),
						cmDistributionRecord.getCmAdjustment().getAdjConversionRate(),
						cmDistributionRecord.getDrAmount(), AD_CMPNY);

//				get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						/*cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcName(),
						cmDistributionRecord.getCmAdjustment().getAdjConversionDate(),
						cmDistributionRecord.getCmAdjustment().getAdjConversionRate(),*/
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						cmDistributionRecord.getCmAdjustment().getAdjDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcCode(),
						cmDistributionRecord.getCmAdjustment().getAdBankAccount().getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						cmDistributionRecord.getDrAmount(), AD_CMPNY);

				if(cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}
			}

			// get cm fund transfers
			Collection cmDrFundTransfers = cmDistributionRecordHome.findFtByDateAndCoaAccountNumberAndCurrencyAndFtPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					adCompany.getGlFunctionalCurrency().getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			i = cmDrFundTransfers.iterator();

			while (i.hasNext()) {

				LocalCmDistributionRecord cmDistributionRecord = (LocalCmDistributionRecord) i.next();

				LocalAdBankAccount adBankAccount = adBankAccountHome.findByPrimaryKey(cmDistributionRecord.getCmFundTransfer().getFtAdBaAccountFrom());

				// get historical conversion
				HIST_CNVRSN = this.convertForeignToFunctionalCurrency(
						adBankAccount.getGlFunctionalCurrency().getFcCode(),
						adBankAccount.getGlFunctionalCurrency().getFcName(),
						cmDistributionRecord.getCmFundTransfer().getFtConversionDate(),
						cmDistributionRecord.getCmFundTransfer().getFtConversionRateFrom(),
						cmDistributionRecord.getDrAmount(), AD_CMPNY);

//				get current conversion
				double FC_AMOUNT = this.convertFunctionalToForeignCurrency(
						/*adBankAccount.getGlFunctionalCurrency().getFcCode(),
						adBankAccount.getGlFunctionalCurrency().getFcName(),
						cmDistributionRecord.getCmFundTransfer().getFtConversionDate(),
						cmDistributionRecord.getCmFundTransfer().getFtConversionRateFrom(),*/
						glFunctionalCurrency.getFcCode(),
						glFunctionalCurrency.getFcName(),
						cmDistributionRecord.getCmAdjustment().getAdjDate(), 1d,
						HIST_CNVRSN, AD_CMPNY);

				// get current conversion
				CURR_CNVRSN = this.convertForeignToFunctionalCurrency(
						adBankAccount.getGlFunctionalCurrency().getFcCode(),
						adBankAccount.getGlFunctionalCurrency().getFcName(),
						CONVERSION_DATE, 1d,
						cmDistributionRecord.getDrAmount(), AD_CMPNY);

				if(cmDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					HIST_TOTAL_DEBIT += HIST_CNVRSN;
					FC_TOTAL_DEBIT += FC_AMOUNT;
				} else {
					HIST_TOTAL_CREDIT += HIST_CNVRSN;
					FC_TOTAL_CREDIT += FC_AMOUNT;
				}
			}

			System.out.println("HIST_TOTAL_DEBIT : " + HIST_TOTAL_DEBIT);
			System.out.println("HIST_TOTAL_CREDIT : " + HIST_TOTAL_CREDIT);
			System.out.println("FC_TOTAL_DEBIT : " + FC_TOTAL_DEBIT);
			System.out.println("FC_TOTAL_CREDIT : " + FC_TOTAL_CREDIT);

			/*if ("ASSET".equals(ACCOUNT_TYPE) || "EXPENSE".equals(ACCOUNT_TYPE)) {
				return EJBCommon.roundIt((HIST_TOTAL_DEBIT - HIST_TOTAL_CREDIT) / (FC_TOTAL_DEBIT - FC_TOTAL_CREDIT), (short)40) ;
			} else {
				return EJBCommon.roundIt((HIST_TOTAL_CREDIT - HIST_TOTAL_DEBIT) / (FC_TOTAL_CREDIT - FC_TOTAL_DEBIT), (short)40) ;
			}*/

			if ("ASSET".equals(ACCOUNT_TYPE) || "EXPENSE".equals(ACCOUNT_TYPE)) {
				return ((HIST_TOTAL_DEBIT - HIST_TOTAL_CREDIT) / (FC_TOTAL_DEBIT - FC_TOTAL_CREDIT));
			} else {
				return ((HIST_TOTAL_CREDIT - HIST_TOTAL_DEBIT) / (FC_TOTAL_CREDIT - FC_TOTAL_DEBIT));
			}

		} catch (Exception ex) {

			ex.printStackTrace();
			throw new EJBException(ex.getMessage());

		}
	}

	private double getCoaRevaluationAmount(LocalGlChartOfAccount glChartOfAccount,
			LocalGlAccountingCalendarValue glAccountingCalendarValue,
			LocalGlFunctionalCurrency glFunctionalCurrency, Integer AD_CMPNY) {

		Debug.print("GlRepGeneralLedgerControllerBean getCoaRevaluationAmount");

		LocalGlJournalLineHome glJournalLineHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		double REVAL_AMOUNT = 0d;

		// Initialize EJB Home

		try {

			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			//tutuloy kapag REVALUATION LANG, else, hindi
			//tutuloy kapag ang NAHANAP na curency ng journal entry = ipinasa/hinahanap/kailangang currency

            //find beg bal manual journals
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			// get gl journals
			Collection glJournalLines = glJournalLineHome.findJrByDateAndCoaAccountNumberAndCurrencyAndJrPosted(
					glAccountingCalendarValue.getAcvDateTo(), glChartOfAccount.getCoaCode(),
					glFunctionalCurrency.getFcCode(), EJBCommon.TRUE, AD_CMPNY);

			Iterator i = glJournalLines.iterator();

			while (i.hasNext()) {

				LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();

				if (glJournalLine.getGlJournal().getGlJournalCategory().getJcName().equals("REVALUATION") &&
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode().equals(glFunctionalCurrency.getFcCode())){

					REVAL_AMOUNT += glJournalLine.getJlAmount();
				}
			}

			return REVAL_AMOUNT;

		} catch (Exception ex) {

			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
		}
	}

	private double convertFunctionalToForeignCurrency(Integer FC_CODE, String FC_NM,
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("GlRepGeneralLedgerControllerBean convertFunctionalToForeignCurrency");


		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}


		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

			AMOUNT = AMOUNT / CONVERSION_RATE;

		} else if (CONVERSION_DATE != null) {

			try {

				double FC_CONVERSION_RATE = 1f;
				double BK_CONVERSION_RATE = 1f;

				// Get functional currency rate

				if (!FC_NM.equals("USD")) {

					LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;

					glReceiptFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByFcCodeAndDate(
							FC_CODE, CONVERSION_DATE, AD_CMPNY);

					if (glReceiptFunctionalCurrencyRate != null) {

						FC_CONVERSION_RATE = glReceiptFunctionalCurrencyRate.getFrXToUsd();

					} else {

						// get latest daily rate prior to conversion date

						Collection glReceiptFunctionalCurrencyRates =
							glFunctionalCurrencyRateHome.findPriorByFcCodeAndDate(
									FC_CODE, CONVERSION_DATE, AD_CMPNY);

						Iterator i = glReceiptFunctionalCurrencyRates.iterator();

						if (i.hasNext()) {

							glReceiptFunctionalCurrencyRate = (LocalGlFunctionalCurrencyRate) i.next();
							FC_CONVERSION_RATE = glReceiptFunctionalCurrencyRate.getFrXToUsd();

						}

					}

					AMOUNT = AMOUNT / FC_CONVERSION_RATE;


				}

				// Get set of book functional currency rate if necessary

				if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

					LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = null;

					glCompanyFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByFcCodeAndDate(
							adCompany.getGlFunctionalCurrency().getFcCode(), CONVERSION_DATE, AD_CMPNY);

					if (glCompanyFunctionalCurrencyRate != null) {

						BK_CONVERSION_RATE = glCompanyFunctionalCurrencyRate.getFrXToUsd();

					} else {

						// get latest daily rate prior to conversion date

						Collection glCompanyFunctionalCurrencyRates =
							glFunctionalCurrencyRateHome.findPriorByFcCodeAndDate(
									adCompany.getGlFunctionalCurrency().getFcCode(), CONVERSION_DATE, AD_CMPNY);

						Iterator i = glCompanyFunctionalCurrencyRates.iterator();

						if (i.hasNext()) {

							glCompanyFunctionalCurrencyRate = (LocalGlFunctionalCurrencyRate) i.next();
							BK_CONVERSION_RATE = glCompanyFunctionalCurrencyRate.getFrXToUsd();

						}

					}

					//AMOUNT = AMOUNT * (1 / EJBCommon.roundIt(1 / BK_CONVERSION_RATE, (short)3));
					AMOUNT = AMOUNT * BK_CONVERSION_RATE;

				}

			} catch (Exception ex) {

			}

		}

		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}






   //retrieves all journal sources

   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public ArrayList getGlJsAll(Integer AD_CMPNY) {

       Debug.print("GlRepGeneralLedgerControllerBean getGlJsAll");

       LocalGlJournalSourceHome glJournalSourceHome = null;

       ArrayList list = new ArrayList();

       // Initialize EJB Home

       try {

           glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

       	Collection glJournalSources = glJournalSourceHome.findJsAll(AD_CMPNY);

       	Iterator i = glJournalSources.iterator();

       	while (i.hasNext()) {

       		LocalGlJournalSource glJournalSource = (LocalGlJournalSource)i.next();

       		list.add(glJournalSource.getJsName());

       	}

       	return list;

       } catch (Exception ex) {

       	Debug.printStackTrace(ex);
       	throw new EJBException(ex.getMessage());

       }

   }


   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlRepGeneralLedgerControllerBean ejbCreate");

   }

}
