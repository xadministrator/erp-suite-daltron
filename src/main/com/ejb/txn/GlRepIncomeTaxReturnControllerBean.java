
/*
 * GlRepIncomeTaxWithheldControllerBean.java
 *
 * Created on March 29, 2004, 11:22 AM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherPaymentScheduleHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlReportValue;
import com.ejb.gl.LocalGlReportValueHome;
import com.ejb.gl.LocalGlTaxInterface;
import com.ejb.gl.LocalGlTaxInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.AdModCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlRepIncomeTaxReturnDetails;
import com.util.GlRepIncomeTaxWithheldDetails;
import com.util.GlRepMonthlyVatDeclarationDetails;
import com.util.GlModReportValueDetails;
import com.util.GlModTaxInterfaceDetails;;

/**
 * @ejb:bean name="GlRepIncomeTaxReturnControllerEJB"
 *           display-name="Used for income tax return report"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepIncomeTaxReturnControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepIncomeTaxReturnController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepIncomeTaxReturnControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:bill type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
*/

public class GlRepIncomeTaxReturnControllerBean extends AbstractSessionBean {




	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getGlTaxInterfaceList(Date DT_FRM,Date DT_TO, Integer AD_CMPNY) {

		Debug.print("GlRepMonthlyVatDeclarationControllerBean executeGlRepMonthlyVatDeclaration");

		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalGlTaxInterfaceHome glTaxInterfaceHome = null;
        LocalGlJournalLineHome glJournalHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalApVoucherHome apVoucherHome = null;
        LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalArTaxCodeHome arTaxCodeHome = null;
        LocalApTaxCodeHome apTaxCodeHome = null;
        LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;

        try {

        	apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
        	arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
	        glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
	    		lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);
	        glJournalHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,LocalGlJournalLineHome.class);

	    	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
					lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

	    	arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
					lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);

	    	apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
					lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

	    	apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
					lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);

	    	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
					lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

	    	arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
					lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

	    	apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
					lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);

	    	arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
					lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);

	    	apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
					lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);


        }  catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

        try {
        	//get available records with ar documents

        	ArrayList list = new ArrayList();


        	StringBuffer jbossQl = new StringBuffer();

        	jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti ");

  	        boolean firstArgument = true;
  	        short ctr = 0;
  		    int criteriaSize = 0;

			if(! DT_FRM.equals(null)) {
				criteriaSize++;
			}

			if(! DT_TO.equals(null)) {
				criteriaSize++;
			}

  	        Object obj[] = new Object[criteriaSize];

  	        if (! DT_FRM.equals(null)) {

		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate>=?" + (ctr+1) + " ");
		  	   obj[ctr] = DT_FRM;
		  	   ctr++;
		    }

  	        if (! DT_TO.equals(null)) {

		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate<=?" + (ctr+1) + " ");
		  	   obj[ctr] = DT_TO;
		  	   ctr++;

		    }

		    if (!firstArgument) {

	   	       jbossQl.append("AND ");

	   	    } else {

	   	  	   firstArgument = false;
	   	  	   jbossQl.append("WHERE ");

	   	    }

		    jbossQl.append("(ti.tiTcCode IS NOT NULL AND ti.tiWtcCode IS NULL AND ti.tiAdCompany=" + AD_CMPNY +
							" ORDER BY ti.tiSlSubledgerCode");

		    short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);

		    Collection glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);

		    Iterator i = glTaxInterfaces.iterator();


		    while(i.hasNext()) {


		    	LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface)i.next();

		    	GlModTaxInterfaceDetails  details = new GlModTaxInterfaceDetails();


		    	details.setTiDocumentType(glTaxInterface.getTiDocumentType());
		    	details.setTiSource(glTaxInterface.getTiSource());
		    	details.setTiNetAmount(glTaxInterface.getTiNetAmount());
		    	details.setTiTaxAmount(glTaxInterface.getTiTaxAmount());
		    	details.setTiSalesAmount(glTaxInterface.getTiSalesAmount());
		    	details.setTiServicesAmount(glTaxInterface.getTiServicesAmount());
		    	details.setTiCapitalGoodsAmount(glTaxInterface.getTiCapitalGoodsAmount());
		    	details.setTiOtherCapitalGoodsAmount(glTaxInterface.getTiOtherCapitalGoodsAmount());
		    	details.setTiTxnCode(glTaxInterface.getTiTxnCode());
		    	details.setTiTxnDate(glTaxInterface.getTiTxnDate());
		    	details.setTiTxnDocumentNumber(glTaxInterface.getTiTxnDocumentNumber());
		    	details.setTiTaxExempt(glTaxInterface.getTiTaxExempt());
		    	details.setTiTaxZeroRated(glTaxInterface.getTiTaxZeroRated());
		    	details.setTiTxlCode(glTaxInterface.getTiTxlCode());
		    	details.setTiTxlCoaCode(glTaxInterface.getTiTxlCoaCode());
		    	details.setTiTcCode(glTaxInterface.getTiTcCode());
		    	details.setTiWtcCode(glTaxInterface.getTiWtcCode());
		    	details.setTiSlTin(glTaxInterface.getTiSlTin());
		    	details.setTiSlSubledgerCode(glTaxInterface.getTiSlSubledgerCode());
		    	details.setTiSlName(glTaxInterface.getTiSlName());
		    	details.setTiSlAddress(glTaxInterface.getTiSlAddress());
		    	details.setTiSlAddress2(glTaxInterface.getTiSlAddress2());
		    	details.setTiIsArDocument(glTaxInterface.getTiIsArDocument());
		    	details.setTiAdBranch(glTaxInterface.getTiAdBranch());
		    	details.setTiAdCompany(glTaxInterface.getTiAdCompany());


		    	if(glTaxInterface.getTiTxlCoaCode()!=null) {

		    		LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(glTaxInterface.getTiTxlCoaCode());

		    		details.setTiTxlCoaNumber(glChartOfAccount.getCoaAccountNumber());

		    	}


		    	if(glTaxInterface.getTiTcCode()!=null) {
		    		if(glTaxInterface.getTiIsArDocument()==EJBCommon.TRUE) {

		    			LocalArTaxCode arTaxCode = arTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiTcCode());
		    			details.setTiTcName(arTaxCode.getTcName());
		    			details.setTiTcRate(arTaxCode.getTcRate());

		    		}else {
		    			LocalApTaxCode apTaxCode = apTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiTcCode());
		    			details.setTiTcName(apTaxCode.getTcName());
		    			details.setTiTcRate(apTaxCode.getTcRate());

		    		}
		    	}

		    	if(glTaxInterface.getTiWtcCode()!=null) {
		    		if(glTaxInterface.getTiIsArDocument()==EJBCommon.TRUE) {

		    			LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiWtcCode());
		    			details.setTiWtcName(arWithholdingTaxCode.getWtcName());
		    			details.setTiWtcRate(arWithholdingTaxCode.getWtcRate());

		    		}else {
		    			LocalApWithholdingTaxCode apWithholdingTaxCode = apWithholdingTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiWtcCode());
		    			details.setTiWtcName(apWithholdingTaxCode.getWtcName());
		    			details.setTiWtcRate(apWithholdingTaxCode.getWtcRate());

		    		}
		    	}

		    	list.add(details);


		    }

		    return list;


        } catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

   	  	}

	}










	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public GlRepIncomeTaxReturnDetails executeGlIncomeTaxReturn(Date DT_FRM,Date DT_TO, Integer AD_CMPNY) {

		Debug.print("GlRepMonthlyVatDeclarationControllerBean executeGlRepMonthlyVatDeclaration");

		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalGlTaxInterfaceHome glTaxInterfaceHome = null;
        LocalGlJournalLineHome glJournalHome = null;
        LocalArInvoiceHome arInvoiceHome = null;
        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalApVoucherHome apVoucherHome = null;
        LocalApVoucherPaymentScheduleHome apVoucherPaymentScheduleHome = null;

        try {

        	apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
        	arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
	        glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
	    		lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);
	        glJournalHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,LocalGlJournalLineHome.class);

	    	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
					lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);

	    	arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
					lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);

	    	apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
					lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

	    	apVoucherPaymentScheduleHome = (LocalApVoucherPaymentScheduleHome)EJBHomeFactory.
					lookUpLocalHome(LocalApVoucherPaymentScheduleHome.JNDI_NAME, LocalApVoucherPaymentScheduleHome.class);

        }  catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

        try {
        	//get available records with ar documents

        	StringBuffer jbossQl = new StringBuffer();

        	jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti ");

  	        boolean firstArgument = true;
  	        short ctr = 0;
  		    int criteriaSize = 0;

			if(! DT_FRM.equals(null)) {
				criteriaSize++;
			}

			if(! DT_TO.equals(null)) {
				criteriaSize++;
			}

  	        Object obj[] = new Object[criteriaSize];

  	        if (! DT_FRM.equals(null)) {

		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate>=?" + (ctr+1) + " ");
		  	   obj[ctr] = DT_FRM;
		  	   ctr++;
		    }

  	        if (! DT_TO.equals(null)) {

		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate<=?" + (ctr+1) + " ");
		  	   obj[ctr] = DT_TO;
		  	   ctr++;

		    }

		    if (!firstArgument) {

	   	       jbossQl.append("AND ");

	   	    } else {

	   	  	   firstArgument = false;
	   	  	   jbossQl.append("WHERE ");

	   	    }

		    jbossQl.append("(ti.tiDocumentType='AR INVOICE' OR ti.tiDocumentType='AR CREDIT MEMO' OR " +
		    				"ti.tiDocumentType='AR RECEIPT' OR ti.tiDocumentType='GL JOURNAL') " +
		    				"AND ti.tiIsArDocument=1 AND ti.tiTcCode IS NOT NULL AND ti.tiWtcCode IS NULL AND ti.tiAdCompany=" + AD_CMPNY +
							" ORDER BY ti.tiSlSubledgerCode");

		    short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);

		    Collection glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);

		    System.out.println("AR SSQL="+jbossQl.toString());
	        System.out.println("AR glTaxInterfaces="+glTaxInterfaces.size());

        	Iterator i = glTaxInterfaces.iterator();

        	double TOTAL_NET_AMOUNT = 0d;
        	double TOTAL_OUTPUT_TAX = 0d;
        	double TOTAL_OUTPUT_TAX_NEW = 0d;
        	double TOTAL_SALES = 0d;
        	double TOTAL_SALES_EXEMPT = 0d;
        	double TOTAL_SALES_ZERO_RATED = 0d;
        	double TOTAL_SALES_EXEMPT_ZERO = 0d;





        	while(i.hasNext()) {

        		LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();



				TOTAL_OUTPUT_TAX += EJBCommon.roundIt(glTaxInterface.getTiTaxAmount(), PRECISION_UNIT);
				TOTAL_NET_AMOUNT += EJBCommon.roundIt(glTaxInterface.getTiNetAmount(), PRECISION_UNIT);
				TOTAL_OUTPUT_TAX_NEW += EJBCommon.roundIt(glTaxInterface.getTiTaxAmount(), PRECISION_UNIT);
				TOTAL_SALES += EJBCommon.roundIt(glTaxInterface.getTiSalesAmount(), PRECISION_UNIT);


				TOTAL_SALES_ZERO_RATED +=  EJBCommon.roundIt(glTaxInterface.getTiTaxZeroRated(), PRECISION_UNIT);
    			TOTAL_SALES_EXEMPT_ZERO += EJBCommon.roundIt(glTaxInterface.getTiTaxExempt(), PRECISION_UNIT);

        	}


        	TOTAL_SALES += TOTAL_SALES_EXEMPT_ZERO;
       // 	TOTAL_SALES_EXEMPT = TOTAL_SALES - TOTAL_SALES_EXEMPT;
        	TOTAL_SALES_EXEMPT_ZERO = TOTAL_SALES_EXEMPT + TOTAL_SALES_ZERO_RATED;


        	//get available records with ap documents

        	jbossQl = new StringBuffer();

        	jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti ");

        	firstArgument = true;
  	        ctr = 0;

  	        obj = new Object[criteriaSize];

  	        if (! DT_FRM.equals(null)) {

		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate>=?" + (ctr+1) + " ");
		  	   obj[ctr] = DT_FRM;
		  	   ctr++;
	        }

	        if (! DT_TO.equals(null)) {

		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate<=?" + (ctr+1) + " ");
		  	   obj[ctr] = DT_TO;
		  	   ctr++;

	        }

	        if (!firstArgument) {

	        	jbossQl.append("AND ");

	   	    } else {

	   	  	   firstArgument = false;
	   	  	   jbossQl.append("WHERE ");

	   	    }

	        jbossQl.append("(ti.tiDocumentType='AP VOUCHER' OR ti.tiDocumentType='AP DEBIT MEMO' OR " +
    				"ti.tiDocumentType='AP CHECK' OR ti.tiDocumentType='AP RECEIVING ITEM' OR " +
    				"ti.tiDocumentType='GL JOURNAL') AND ti.tiIsArDocument=0 AND ti.tiTcCode IS NOT NULL AND " +
					"ti.tiWtcCode IS NULL AND ti.tiAdCompany=" + AD_CMPNY +
					" ORDER BY ti.tiSlSubledgerCode");

	        glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);

	        i = glTaxInterfaces.iterator();
	        System.out.println("AP SSQL="+jbossQl.toString());
	        System.out.println("AP glTaxInterfaces="+glTaxInterfaces.size());
	        double TOTAL_INPUT_TAX = 0d;
	        double TOTAL_INPUT_TAX_NEW = 0d;
	        double TOTAL_REVENUE = 0d;
	        double TOTAL_REV_EXEMPT = 0d;
	        double TOTAL_REV_ZERO_RATED = 0d;
	        double TOTAL_REV_EXEMPT_ZERO = 0d;

	        while(i.hasNext()) {

				LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();

				TOTAL_INPUT_TAX -= EJBCommon.roundIt(glTaxInterface.getTiTaxAmount(), PRECISION_UNIT);
				TOTAL_INPUT_TAX_NEW -= EJBCommon.roundIt(glTaxInterface.getTiTaxAmount(), PRECISION_UNIT);
				TOTAL_REVENUE -= EJBCommon.roundIt(glTaxInterface.getTiSalesAmount(), PRECISION_UNIT);

				TOTAL_REV_ZERO_RATED +=  EJBCommon.roundIt(glTaxInterface.getTiTaxZeroRated(), PRECISION_UNIT);
    			TOTAL_REV_EXEMPT_ZERO += EJBCommon.roundIt(glTaxInterface.getTiTaxExempt(), PRECISION_UNIT);

			}



        	TOTAL_REVENUE += TOTAL_REV_EXEMPT_ZERO;
       // 	TOTAL_REV_EXEMPT = TOTAL_REVENUE - TOTAL_REV_EXEMPT;
        	TOTAL_REV_EXEMPT_ZERO = TOTAL_REV_EXEMPT + TOTAL_REV_ZERO_RATED;


        	GlRepIncomeTaxReturnDetails details = new GlRepIncomeTaxReturnDetails();

        	details.setGrossSalesReceipt(TOTAL_NET_AMOUNT + TOTAL_OUTPUT_TAX);
        	details.setTaxDue(TOTAL_OUTPUT_TAX);
  		  	details.setOnTaxableGoods(TOTAL_INPUT_TAX);

  		  	details.setOutputTax(TOTAL_OUTPUT_TAX_NEW);
  		  	details.setInputTax(TOTAL_INPUT_TAX_NEW);
  		  	details.setSalesAmount(TOTAL_SALES);
  		  	details.setRevenue(TOTAL_REVENUE);
  		  	details.setCostOfSales(TOTAL_REVENUE);

  		  	details.setSalesAmountExempt(TOTAL_SALES_EXEMPT);
  		  	details.setSaleAmountZeroRated(TOTAL_SALES_ZERO_RATED);
  		  	details.setTotalSalesExemptZero(TOTAL_SALES_EXEMPT_ZERO);

  		  	details.setRevenueAmountExempt(TOTAL_REV_EXEMPT);
  		  	details.setRevenueAmountZeroRated(TOTAL_REV_ZERO_RATED);
  		  	details.setTotalRevenueExemptZero(TOTAL_REV_EXEMPT_ZERO);



  		  	return details;

        } catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

   	  	}

	}


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {

		Debug.print("GlRepMonthlyVatDeclarationControllerBean getAdLvReportTypeAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("GL REPORT TYPE - INCOME TAX WITHHELD", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}





    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdCompanyDetails getArCmp(Integer AD_CMPNY) {

        Debug.print("GlRepIncomeTaxWithheldControllerBean getArCmp");

        LocalAdCompanyHome adCompanyHome = null;
        LocalAdCompany adCompany = null;

        // Initialize EJB Home

        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

	        adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			AdCompanyDetails details = new AdModCompanyDetails();
			    details.setCmpName(adCompany.getCmpName());
			    details.setCmpAddress(adCompany.getCmpAddress());
			    details.setCmpCity(adCompany.getCmpCity());
			    details.setCmpZip(adCompany.getCmpZip());
			    details.setCmpCountry(adCompany.getCmpCountry());
			    details.setCmpPhone(adCompany.getCmpPhone());
			    details.setCmpTin(adCompany.getCmpTin());
			    details.setCmpIndustry(adCompany.getCmpIndustry());

	        return details;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    private short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         return  adCompany.getGlFunctionalCurrency().getFcPrecision();

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }

    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT * CONVERSION_RATE;

         } else if (CONVERSION_DATE != null) {

         	 try {

         	 	 // Get functional currency rate

         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;

         	     if (!FC_NM.equals("USD")) {

        	         glReceiptFunctionalCurrencyRate =
	     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE,
	     	             CONVERSION_DATE, AD_CMPNY);

	     	         AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();

         	     }

                 // Get set of book functional currency rate if necessary

                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                             getFcCode(), CONVERSION_DATE, AD_CMPNY);

                     AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();

                  }


         	 } catch (Exception ex) {

         	 	throw new EJBException(ex.getMessage());

         	 }

         }

         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvProvincesAll(Integer AD_CMPNY) {

		Debug.print("GlRepMonthlyVatDeclarationControllerBean getAdLvProvincesAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;


		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);



		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("GL PROVINCES - MONTHLY VAT RETURN", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public ArrayList getAllReportValues(Integer AD_CMPNY) {


	      Debug.print("GlRepMonthlyVatDeclarationControllerBean getAllReportValues");


		  LocalGlReportValueHome glReportValueHome = null;
		  LocalGlChartOfAccountHome glChartOfAccountHome = null;

		  ArrayList list = new ArrayList();
		  // Initialize EJB Home

		  try {


		       glReportValueHome = (LocalGlReportValueHome)EJBHomeFactory.
			           lookUpLocalHome(LocalGlReportValueHome.JNDI_NAME, LocalGlReportValueHome.class);

		       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			           lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


		  } catch (NamingException ex) {

		      throw new EJBException(ex.getMessage());

		  }



		  try {



			  Collection glReportValueList = glReportValueHome.findRvByRvType("GL_REP_INCOME_TAX_RETURN", AD_CMPNY);




			  Iterator i = glReportValueList.iterator();

			  while(i.hasNext()) {

				  LocalGlReportValue glReportValue = (LocalGlReportValue)i.next();


				  GlModReportValueDetails details = new GlModReportValueDetails();

				  details.setRvCode(glReportValue.getRvCode());
				  details.setRvParameter(glReportValue.getRvParameter());
				  details.setRvDescription(glReportValue.getRvDescription());
				  details.setRvDefaultValue(glReportValue.getRvDefaultValue());

				  if(glReportValue.getGlChartOfAccount() == null) {
					  details.setRvCoaAccountNumber("");
					  details.setRvCoaAccountDescription("");
				  }else {

					  details.setRvCoaAccountNumber(glReportValue.getGlChartOfAccount().getCoaAccountNumber());
					  details.setRvCoaAccountDescription(glReportValue.getGlChartOfAccount().getCoaAccountDescription());

				  }


				  list.add(details);


			  }





		  }catch(Exception ex) {
			  Debug.printStackTrace(ex);
			  throw new EJBException(ex.getMessage());

		  }

		  return list;
	}








	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public void saveGlReportValue(ArrayList list, Integer AD_CMPNY) {


	      Debug.print("GlRepMonthlyVatDeclarationControllerBean saveGlReportValue");

		  LocalAdCompanyHome adCompanyHome = null;
		  LocalGlReportValueHome glReportValueHome = null;
		  LocalGlChartOfAccountHome glChartOfAccountHome = null;

		  // Initialize EJB Home

		  try {

		       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		       glReportValueHome = (LocalGlReportValueHome)EJBHomeFactory.
			           lookUpLocalHome(LocalGlReportValueHome.JNDI_NAME, LocalGlReportValueHome.class);

		       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			           lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


		  } catch (NamingException ex) {

		      throw new EJBException(ex.getMessage());

		  }



		  try {

			  //clear all data
			  /*
			  Collection glReportValueList = glReportValueHome.findRvByRvType("GL_REP_INCOME_TAX_RETURN", AD_CMPNY);

			  Iterator i = glReportValueList.iterator();

			  while(i.hasNext()) {


				  LocalGlReportValue glReportValue = (LocalGlReportValue)i.next();
				  glReportValue.remove();


			  }
				*/

			  Iterator i = list.iterator();
			  //save all new data
			  while(i.hasNext()) {

				  LocalGlReportValue glReportValue = null;

				  GlModReportValueDetails details = (GlModReportValueDetails)i.next();
				  System.out.println("code is: " + details.getRvCode());


				  if(details.getRvCode() != null) {

					  glReportValue = glReportValueHome.findByPrimaryKey(details.getRvCode());

					  if(details.getRvParameter().equals("")) {

						  glReportValue.remove();


					  }else {

						  glReportValue.setRvParameter(details.getRvParameter());
						  glReportValue.setRvDescription(details.getRvDescription());
						  glReportValue.setRvDefaultValue(details.getRvDefaultValue());
						  //if coa account exist

						  System.out.println("COA IS :" + details.getRvCoaAccountNumber());
						  if(!details.getRvCoaAccountNumber().equals("")) {

							  try {
								  LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getRvCoaAccountNumber(), AD_CMPNY);

								  glReportValue.setGlChartOfAccount(glChartOfAccount);
							  }catch(FinderException fx) {
								  glReportValue.setGlChartOfAccount(null);
							  }


						  }else {
							  System.out.println("removed chart of account");
							  glReportValue.setGlChartOfAccount(null);
						  }
					  }

					  continue;

				  }


				  if(details.getRvParameter().equals("")) {
					  continue;
				  }

				  glReportValue = glReportValueHome.create("GL_REP_INCOME_TAX_RETURN", details.getRvParameter(), details.getRvDescription(), details.getRvDefaultValue(), AD_CMPNY);


				  //if coa account exist
				  if(!details.getRvCoaAccountNumber().equals("")) {

					  try {
						  LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getRvCoaAccountNumber(), AD_CMPNY);

						  glReportValue.setGlChartOfAccount(glChartOfAccount);
					  }catch(FinderException fx) {
						  glReportValue.setGlChartOfAccount(null);
					  }

				  }

			  }



		  }catch(Exception ex) {
			  Debug.printStackTrace(ex);
			  throw new EJBException(ex.getMessage());

		  }

	}




    /**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

	  Debug.print("GlRepMonthlyVatDeclarationControllerBean getAdCompany");

	  LocalAdCompanyHome adCompanyHome = null;

	  // Initialize EJB Home

	  try {

	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

	  try {

	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	     AdCompanyDetails details = new AdCompanyDetails();


	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpZip(adCompany.getCmpZip());
	     details.setCmpIndustry(adCompany.getCmpIndustry());

	     details.setCmpName(adCompany.getCmpName());
         details.setCmpTaxPayerName(adCompany.getCmpTaxPayerName());
         details.setCmpContact(adCompany.getCmpContact());
         details.setCmpPhone(adCompany.getCmpPhone());
         details.setCmpEmail(adCompany.getCmpEmail());
         details.setCmpTin(adCompany.getCmpTin());

         details.setCmpRevenueOffice(adCompany.getCmpRevenueOffice());

	     details.setCmpMailSectionNo(adCompany.getCmpMailSectionNo());
	     details.setCmpMailLotNo(adCompany.getCmpMailLotNo());
	     details.setCmpMailStreet(adCompany.getCmpMailStreet());
	     details.setCmpMailPoBox(adCompany.getCmpMailPoBox());
	     details.setCmpMailCountry(adCompany.getCmpMailCountry());
	     details.setCmpMailProvince(adCompany.getCmpMailProvince());
	     details.setCmpMailPostOffice(adCompany.getCmpMailPostOffice());
	     details.setCmpMailCareOff(adCompany.getCmpMailCareOff());
	     details.setCmpTaxPeriodFrom(adCompany.getCmpTaxPeriodFrom());
	     details.setCmpTaxPeriodTo(adCompany.getCmpTaxPeriodTo());
	     details.setCmpPublicOfficeName(adCompany.getCmpPublicOfficeName());
	     details.setCmpDateAppointment(adCompany.getCmpDateAppointment());



	     return details;

	  } catch (Exception ex) {

	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());

	  }

	}


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlRepIncomeTaxWithheldControllerBean ejbCreate");

    }
}
