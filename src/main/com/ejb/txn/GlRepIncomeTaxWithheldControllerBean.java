
/*
 * GlRepIncomeTaxWithheldControllerBean.java
 *
 * Created on March 29, 2004, 11:22 AM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlTaxInterface;
import com.ejb.gl.LocalGlTaxInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.AdModCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlRepIncomeTaxWithheldDetails;

/**
 * @ejb:bean name="GlRepIncomeTaxWithheldControllerEJB"
 *           display-name="Used for income tax withheld report"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepIncomeTaxWithheldControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepIncomeTaxWithheldController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepIncomeTaxWithheldControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:bill type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
*/

public class GlRepIncomeTaxWithheldControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeGlRepIncomeTaxWithheld(Date DT_FRM,Date DT_TO, Integer AD_CMPNY) {

		Debug.print("GlRepIncomeTaxWithheldControllerBean executeGlRepIncomeTaxWithheld");

		LocalApDistributionRecordHome apDistributionRecordHome = null;
        LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
        LocalGlTaxInterfaceHome glTaxInterfaceHome = null;
        LocalGlJournalLineHome glJournalHome = null;

        ArrayList list = new ArrayList();

        try {

		    apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
		    apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME,LocalApWithholdingTaxCodeHome.class);
		    glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);
		    glJournalHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,LocalGlJournalLineHome.class);

		} catch (NamingException ex) 	{

		    throw new EJBException(ex.getMessage());

		}

		try {
			//get all available record

			StringBuffer jbossQl = new StringBuffer();

        	jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti ");

        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = 0;

        	if(! DT_FRM.equals(null)) {
        		criteriaSize++;
        	}

        	if(! DT_TO.equals(null)) {
        		criteriaSize++;
        	}

        	Object obj[] = new Object[criteriaSize];

        	if (! DT_FRM.equals(null)) {

		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate>=?" + (ctr+1) + " ");
		  	   obj[ctr] = DT_FRM;
		  	   ctr++;

		    }

        	if (! DT_TO.equals(null)) {

    		   if (!firstArgument) {
  		  	 	  jbossQl.append("AND ");
  		  	   } else {
  		  	 	  firstArgument = false;
  		  	 	  jbossQl.append("WHERE ");
  		  	   }
  		  	   jbossQl.append("ti.tiTxnDate<=?" + (ctr+1) + " ");
  		  	   obj[ctr] = DT_TO;
  		  	   ctr++;

        	}

        	if (!firstArgument) {

        		jbossQl.append("AND ");

	   	    } else {

	   	  	   firstArgument = false;
	   	  	   jbossQl.append("WHERE ");

	   	    }

        	jbossQl.append("(ti.tiDocumentType='AP VOUCHER' OR ti.tiDocumentType='AP DEBIT MEMO' OR " +
    				"ti.tiDocumentType='AP CHECK' OR ti.tiDocumentType='GL JOURNAL') " +
					"AND ti.tiIsArDocument=0 AND ti.tiTcCode IS NULL AND ti.tiWtcCode IS NOT NULL AND ti.tiAdCompany=" + AD_CMPNY +
					" ORDER BY ti.tiSlSubledgerCode");

        	 short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);

        	 Collection glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);

        	 ArrayList apWtcList = new ArrayList();

        	 Iterator i = glTaxInterfaces.iterator();

        	 while(i.hasNext()) {

        	 	//get available records per wtc code

        	 	LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();

        	 	if (apWtcList.contains(glTaxInterface.getTiWtcCode())) {

        			continue;

        		}

    			apWtcList.add(glTaxInterface.getTiWtcCode());

    			jbossQl = new StringBuffer();

    			jbossQl.append("SELECT DISTINCT OBJECT(ti) FROM GlTaxInterface ti " +
 					   "WHERE (ti.tiDocumentType='AP VOUCHER' OR ti.tiDocumentType='AP DEBIT MEMO' " +
 					   "OR ti.tiDocumentType='AP CHECK' OR ti.tiDocumentType='GL JOURNAL') ");

    			ctr = 0;
        		criteriaSize = 0;
        		Integer WTC_CD = glTaxInterface.getTiWtcCode();

        		if(DT_FRM != null) {
        			criteriaSize++;
        		}

        		if(DT_TO != null) {
        			criteriaSize++;
        		}

        		if(WTC_CD != null) {
        			criteriaSize++;
        		}

        		obj = new Object[criteriaSize];

        		if(WTC_CD != null) {

        		   jbossQl.append("AND ti.tiWtcCode =?" + (ctr+1) + " ");
         		   obj[ctr] = WTC_CD;
         		   ctr++;

        		}

        		if(DT_FRM != null) {

        		   jbossQl.append("AND ti.tiTxnDate >=?" + (ctr+1) + " ");
         		   obj[ctr] = DT_FRM;
         		   ctr++;

        		}

        		if(DT_TO != null) {

        		   jbossQl.append("AND ti.tiTxnDate <=?" + (ctr+1) + " ");
         		   obj[ctr] = DT_TO;
         		   ctr++;

        		}

        		jbossQl.append("AND ti.tiIsArDocument=0 AND ti.tiAdCompany=" + AD_CMPNY +  " ORDER BY ti.tiTxnDate");

        		Collection glTaxInterfaceWtcs = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(),obj);

        		Iterator j = glTaxInterfaceWtcs.iterator();

        		double WITHHOLDING_TAX = 0d;
			    double NET_AMOUNT = 0d;

        		while(j.hasNext()) {

        			LocalGlTaxInterface glTaxInterfaceWtc = (LocalGlTaxInterface) j.next();

        			if(glTaxInterfaceWtc.getTiDocumentType().equals("GL JOURNAL")) {

        				LocalGlJournalLine glJournal = glJournalHome.findByPrimaryKey(
        									glTaxInterfaceWtc.getTiTxlCode());

        				if(glJournal.getJlDebit() == EJBCommon.TRUE) {

        					WITHHOLDING_TAX -= EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT);
        					NET_AMOUNT -= EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT);

        				} else {

        					WITHHOLDING_TAX += EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT);
        					NET_AMOUNT += EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT);

        				}

        			} else {

						LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.findByPrimaryKey(
											glTaxInterfaceWtc.getTiTxlCode());

						if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

							WITHHOLDING_TAX -= EJBCommon.roundIt(apDistributionRecord.getDrAmount(), PRECISION_UNIT);
							NET_AMOUNT -= EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT);

						} else {

							WITHHOLDING_TAX += EJBCommon.roundIt(apDistributionRecord.getDrAmount(), PRECISION_UNIT);
							NET_AMOUNT += EJBCommon.roundIt(glTaxInterfaceWtc.getTiNetAmount(), PRECISION_UNIT);

						}

        			}

        		}

        		LocalApWithholdingTaxCode apWithholdingTaxCode = apWithholdingTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiWtcCode());

    			GlRepIncomeTaxWithheldDetails details = new GlRepIncomeTaxWithheldDetails();


        		details.setNatureOfIncomePayment(apWithholdingTaxCode.getWtcDescription());
        		details.setAtcCode(apWithholdingTaxCode.getWtcName());
        		details.setTaxBase(NET_AMOUNT);
        		details.setTaxRate(apWithholdingTaxCode.getWtcRate());
        		details.setTaxRequiredWithheld(WITHHOLDING_TAX);

        		list.add(details);
        	 }

        } catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

   	  	}

        return list;
	}


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {

		Debug.print("GlRepMonthlyVatDeclarationControllerBean getAdLvReportTypeAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("GL REPORT TYPE - INCOME TAX WITHHELD", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}





    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdCompanyDetails getArCmp(Integer AD_CMPNY) {

        Debug.print("GlRepIncomeTaxWithheldControllerBean getArCmp");

        LocalAdCompanyHome adCompanyHome = null;
        LocalAdCompany adCompany = null;

        // Initialize EJB Home

        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

	        adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			AdCompanyDetails details = new AdModCompanyDetails();
			    details.setCmpName(adCompany.getCmpName());
			    details.setCmpAddress(adCompany.getCmpAddress());
			    details.setCmpCity(adCompany.getCmpCity());
			    details.setCmpZip(adCompany.getCmpZip());
			    details.setCmpCountry(adCompany.getCmpCountry());
			    details.setCmpPhone(adCompany.getCmpPhone());
			    details.setCmpTin(adCompany.getCmpTin());
			    details.setCmpIndustry(adCompany.getCmpIndustry());

	        return details;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    private short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       LocalAdCompanyHome adCompanyHome = null;


       // Initialize EJB Home

       try {

           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         return  adCompany.getGlFunctionalCurrency().getFcPrecision();

       } catch (Exception ex) {

       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());

       }

    }

    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }

         // get company and extended precision

         try {

             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

         } catch (Exception ex) {

             throw new EJBException(ex.getMessage());

         }


         // Convert to functional currency if necessary

         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

             AMOUNT = AMOUNT * CONVERSION_RATE;

         } else if (CONVERSION_DATE != null) {

         	 try {

         	 	 // Get functional currency rate

         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;

         	     if (!FC_NM.equals("USD")) {

        	         glReceiptFunctionalCurrencyRate =
	     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE,
	     	             CONVERSION_DATE, AD_CMPNY);

	     	         AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();

         	     }

                 // Get set of book functional currency rate if necessary

                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                             getFcCode(), CONVERSION_DATE, AD_CMPNY);

                     AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();

                  }


         	 } catch (Exception ex) {

         	 	throw new EJBException(ex.getMessage());

         	 }

         }

         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlRepIncomeTaxWithheldControllerBean ejbCreate");

    }
}
