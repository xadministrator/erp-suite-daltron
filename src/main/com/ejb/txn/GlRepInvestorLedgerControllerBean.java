package com.ejb.txn;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccount;
import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmDistributionRecord;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransfer;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenQualifier;
import com.ejb.genfld.LocalGenQualifierHome;
import com.ejb.genfld.LocalGenSegment;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSet;
import com.ejb.genfld.LocalGenValueSetHome;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.GlInvestorAccountBalanceBean;
import com.ejb.gl.LocalGlAccountingCalendar;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAssemblyTransfer;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.ejb.inv.LocalInvBuildUnbuildAssembly;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvOverhead;
import com.ejb.inv.LocalInvStockIssuance;
import com.ejb.inv.LocalInvStockTransfer;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApModSupplierDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GenModSegmentDetails;
import com.util.GenModValueSetValueDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlRepDetailInvestorAccountBalanceDetails;
import com.util.GlRepInvestorLedgerDetails;

/**
 * @ejb:bean name="GlRepInvestorLedgerControllerEJB"
 *           display-name="Used for generation of investor ledger reports"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepInvestorLedgerControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepInvestorLedgerController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepInvestorLedgerControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlRepInvestorLedgerControllerBean extends AbstractSessionBean {
	
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("GlRepInvestorLedgerControllerBean getApSplAll");

		LocalApSupplierHome apSupplierHome = null;               

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}        

		try {

			Collection apSuppliers = apSupplierHome.findAllEnabledSplScLedger(AD_CMPNY);

			Iterator i = apSuppliers.iterator();

			while (i.hasNext()) {

				LocalApSupplier apSupplier = (LocalApSupplier)i.next();

				list.add(apSupplier.getSplSupplierCode());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	} 
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

		Debug.print("GlRepInvestorLedgerControllerBean getAdPrfApUseSupplierPulldown");

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfApUseSupplierPulldown();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGenQlfrAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlRepInvestorLedgerControllerBean getGenQlfrAll");
        
        LocalGenQualifierHome genQualifierHome = null;
        
        Collection genQualifiers = null;
        
        LocalGenQualifier genQualifier = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            genQualifierHome = (LocalGenQualifierHome)EJBHomeFactory.
                 lookUpLocalHome(LocalGenQualifierHome.JNDI_NAME, LocalGenQualifierHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            genQualifiers = genQualifierHome.findQlfrAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (genQualifiers.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = genQualifiers.iterator();
               
        while (i.hasNext()) {
        	
        	genQualifier = (LocalGenQualifier)i.next();
        	
        	list.add(genQualifier.getQlAccountType());
        	
        }
        
        return list;
            
    }

	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {
		
		Debug.print("GlRepInvestorLedgerControllerBean getAdLvReportTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("GL REPORT TYPE - INVESTOR LEDGER", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
    
   
   

   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList executeGlRepInvestorLedger(
    		String SUPPLIER_CODE, Date GL_DT, boolean SL_INCLD_UNPSTD,
    		String ORDER_BY, ArrayList branchList, 
    		Integer AD_BRNCH, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException {
 //
       Debug.print("GlRepInvestorLedgerControllerBean executeGlRepInvestorLedger");
       
       LocalGlSetOfBookHome glSetOfBookHome = null;
       LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
       LocalAdCompanyHome adCompanyHome = null;
       LocalAdBankAccountHome adBankAccountHome = null;
       LocalArCustomerHome arCustomerHome  = null;
       LocalApSupplierHome apSupplierHome = null;
       LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;
       
       LocalApCheckHome apCheckHome = null;
       LocalArReceiptHome arReceiptHome = null;
       LocalApVoucherHome apVoucherHome = null;

       LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
       LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
       
       ArrayList list = new ArrayList();
       
 	   //Initialize EJB Home
 	    
 	  try {
 	        
 	       glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
 	       glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
 	 	           lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
 	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
 	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
 	       adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
            	   lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);    
 	       apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
            	   lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
 	       arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
        	   		lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);  
 	       apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
        	   		lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
 	       glInvestorAccountBalanceHome = (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
      	   		lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);
 	       apCheckHome = (LocalApCheckHome)EJBHomeFactory.
 	  	   		lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);  
 	       arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
 	  	   		lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
 	       glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
   				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
 	       glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
    				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
 	       
 	       
 	  } catch (NamingException ex) {
 	        
 	      throw new EJBException(ex.getMessage());
 	        
 	  }
 	  
 	  try {
 	  	
 	  	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 	  	  LocalGlSetOfBook glSetOfBook = null;
 	  	  
 	  	  try {
 	  	  
 	  	  	 glSetOfBook = glSetOfBookHome.findByDate(GL_DT, AD_CMPNY);
 	  	  	
 	  	  } catch (FinderException ex) {
 	  	  	System.out.println("CHECK A");
 	  	  	 throw new GlobalNoRecordFoundException();
 	  	  	
 	  	  }
 	  	  

 	  	  System.out.println("GL_DT="+GL_DT);
 	  	  System.out.println("SOB="+glSetOfBook.getGlAccountingCalendar().getAcName());

 	      //get coa selected
 	      
 	      StringBuffer jbossQl = new StringBuffer();
           jbossQl.append("SELECT DISTINCT OBJECT(spl) FROM ApSupplier spl ");
           
           
           
		  	 

           //add branch criteria

 		  if (branchList.isEmpty()) {
 			  System.out.println("CHECK B");
 		  	throw new GlobalNoRecordFoundException();
 		  	
 		  } else {
 		  	
 		  	jbossQl.append(", in (spl.adBranchSuppliers) bspl WHERE bspl.adBranch.brCode in (");
 		  	
 		  	boolean firstLoop = true;
 		  	
 		  	Iterator j = branchList.iterator();
 		  	
 		  	while(j.hasNext()) {
 		  		
 		  		if(firstLoop == false) { 
 		  			jbossQl.append(", "); 
 		  		}
 		  		else { 
 		  			firstLoop = false; 
 		  		}
 		  		
 		  		AdBranchDetails mdetails = (AdBranchDetails) j.next();
 		  		
 		  		jbossQl.append(mdetails.getBrCode());
 		  		
 		  	}
 		  	
 		  	jbossQl.append(") AND ");
 		  	
 		  }     
 		  
 		 if(!SUPPLIER_CODE.equals("")){
 			 
      	   jbossQl.append("spl.splSupplierCode LIKE '%" +SUPPLIER_CODE + "%' AND ");
      	   
         }
           
            
 	      	  
 		 jbossQl.append("spl.splEnable=1 AND spl.apSupplierClass.scLedger = 1 AND spl.splAdCompany=" + AD_CMPNY + " ORDER BY spl.splSupplierCode ");
 		 
 		 Object obj[] = new Object[0];
 	 
 		 System.out.println("Ssql="+jbossQl.toString());
   	     Collection apSuppliers = apSupplierHome.getSplByCriteria(jbossQl.toString(), obj);

   	     //get previous 
   	     
   	     Collection glPreviousSetOfBooks = glSetOfBookHome.findPrecedingSobByAcYear(glSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
   	     Iterator prevItr = glPreviousSetOfBooks.iterator();
   	     LocalGlSetOfBook glPreviousSetOfBook = null;
   	     while(prevItr.hasNext()) {
   	     	
   	     	glPreviousSetOfBook = (LocalGlSetOfBook)prevItr.next();
   	     	break;
   	     	
   	     }
   	     
   	     //check if previous year is closed
   	     
   	     if(glPreviousSetOfBook != null && glPreviousSetOfBook.getSobYearEndClosed() == EJBCommon.FALSE) {
   	     	
   	     	
   	     	
   	     } else {
   	     
   	     	Iterator i = apSuppliers.iterator();		 
   	     	
   	     	while (i.hasNext()) {
   	     		
   	     		LocalApSupplier apSupplier = (LocalApSupplier)i.next();		 			 	 		 	 
   	     		
   	     		
   	     		//get coa debit or credit	in coa balance			 			  		    		 	 		 	 	 	 
   	     		
   	     		LocalGlAccountingCalendarValue glAccountingCalendarValue = null;
   	     		
   	     		

   	     		try {
   	     			
   	     			glAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndDate(
   	     					glSetOfBook.getGlAccountingCalendar().getAcCode(),
 							GL_DT, AD_CMPNY);
	
   	     		} catch (FinderException ex) {
   	     	
   	     			throw new GlobalNoRecordFoundException();
   	     			
   	     		}    	
   	     		
   	     		
   	     		
   	     		//get investor beginning balance
   	     		
   	     		double INVTR_BGNNG_BLNC = 0d;
   	     		
   	     		LocalGlAccountingCalendarValue glBeginningAccountingCalendarValue =
   	     			glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
   	     					glSetOfBook.getGlAccountingCalendar().getAcCode(),
 							(short)1, AD_CMPNY);

   	     		
   	     		
   	     		LocalGlInvestorAccountBalance glBeginningInvestorBalance = 
   	   	     			glInvestorAccountBalanceHome.findByAcvCodeAndSplCode(
   	   	     					glBeginningAccountingCalendarValue.getAcvCode(), apSupplier.getSplCode(), AD_CMPNY);
   	   	     		
   	     		INVTR_BGNNG_BLNC = glBeginningInvestorBalance.getIrabBeginningBalance();
   	     			
   	     		System.out.println("glBeginningInvestorBalance.getIrabCode()="+glBeginningInvestorBalance.getIrabCode());
   	     		System.out.println("INVTR_BGNNG_BLNC="+INVTR_BGNNG_BLNC);
   	     		
   	     		
   		
   	     		// get coa debit or credit balance
  	     		
  	     	
  	     		Collection arPostedRCTDrs = new ArrayList();
   	     		Collection apPostedCHKDrs = new ArrayList();
   	     		
   	     		Collection apPostedVOUDrs = new ArrayList();
   	     		

   	     		System.out.println("SL_INCLD_UNPSTD="+SL_INCLD_UNPSTD);
   	     		
  	     		//if (!SL_INCLD_UNPSTD) {} 

	   	     	 arPostedRCTDrs = arReceiptHome.findPostedRctByRctDateRangeAndSplCode(
	    						glBeginningAccountingCalendarValue.getAcvDateFrom(),
	    						GL_DT, apSupplier.getSplCode(), AD_BRNCH, AD_CMPNY);
	   	     			
	   	     	 apPostedCHKDrs = apCheckHome.findPostedChkByChkDateRangeAndSplCode(
	   	     					glBeginningAccountingCalendarValue.getAcvDateFrom(),
	   	     					GL_DT, apSupplier.getSplCode(), AD_BRNCH, AD_CMPNY);
	   	     	 
	   	     	 apPostedVOUDrs = apVoucherHome.findPostedVouByVouDateRangeAndSplCode(EJBCommon.FALSE, 
	     					glBeginningAccountingCalendarValue.getAcvDateFrom(),
	     					GL_DT, apSupplier.getSplCode(), AD_BRNCH, AD_CMPNY);
  	     			

   	     		Collection arUnpostedRCTDrs = new ArrayList();
   	     		Collection apUnpostedCHKDrs = new ArrayList();

   	     		Collection apUnpostedVOUDrs = new ArrayList();
   	     		
   	     		if (SL_INCLD_UNPSTD) {
   	     			
   	     			arUnpostedRCTDrs = arReceiptHome.findUnPostedRctByRctDateRangeAndSplCode(
    						glBeginningAccountingCalendarValue.getAcvDateFrom(),
    						GL_DT, apSupplier.getSplCode(), AD_BRNCH, AD_CMPNY);
   	     			
   	     			apUnpostedCHKDrs = apCheckHome.findUnPostedChkByChkDateRangeAndSplCode(
   	     					glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     					GL_DT, apSupplier.getSplCode(), AD_BRNCH, AD_CMPNY);
   	     			
   	     			apUnpostedVOUDrs = apVoucherHome.findUnPostedVouByVouDateRangeAndSplCode(EJBCommon.FALSE, 
   	     					glBeginningAccountingCalendarValue.getAcvDateFrom(),
   	     					GL_DT, apSupplier.getSplCode(), AD_BRNCH, AD_CMPNY);

   	     		}
   	     		
   	     		if (arPostedRCTDrs.isEmpty() && apPostedCHKDrs.isEmpty() && apPostedVOUDrs.isEmpty() &&
   	     			(SL_INCLD_UNPSTD && arUnpostedRCTDrs.isEmpty() && apUnpostedCHKDrs.isEmpty() && apUnpostedVOUDrs.isEmpty() )) {
   	     			
   	     			
   	     			/*GlRepInvestorLedgerDetails details = new GlRepInvestorLedgerDetails();

					details.setIlInvestorCode(apSupplier.getSplSupplierCode());
					details.setIlInvestorName(apSupplier.getSplName());
					details.setIlAmount(0d);
   	     			
   	     			list.add(details);*/
   	     			
   	     		} else {
   	     			

   	     				
   	     			Iterator j = null;
   	     			System.out.println("arPostedRCTDrs="+arPostedRCTDrs.size());
   	     		
   	     			if(arPostedRCTDrs!=null){
    						j = arPostedRCTDrs.iterator();
    						
    						while (j.hasNext()) {
    							
    							
    							LocalArReceipt arReceipt = (LocalArReceipt)j.next();
    							
    							double AMNT = this.convertForeignToFunctionalCurrency(
    									arReceipt.getGlFunctionalCurrency().getFcCode(),
    									arReceipt.getGlFunctionalCurrency().getFcName(),
    									arReceipt.getRctConversionDate(),
    									arReceipt.getRctConversionRate(),
    									arReceipt.getRctAmount(), AD_CMPNY);
    							if (AMNT==0) continue;
    							
    							GlRepInvestorLedgerDetails details = new GlRepInvestorLedgerDetails();

     							details.setIlInvestorCode(apSupplier.getSplSupplierCode());
     							details.setIlInvestorName(apSupplier.getSplName());
     							details.setIlInvestorContact(apSupplier.getSplContact());
     							details.setIlInvestorAddress(apSupplier.getSplAddress());
     							
     							details.setIlEffectiveDate(arReceipt.getRctDate());
     							details.setIlDocumentNumber(arReceipt.getRctNumber());
     							details.setIlReferenceNumber(arReceipt.getRctReferenceNumber());
     							details.setIlSourceName("ACCOUNTS RECEIVABLES");
     							details.setIlDescription(arReceipt.getRctDescription());
     							details.setIlDebit((byte)0);
     							details.setIlAmount(AMNT);
     							details.setIlBeginningBalance(INVTR_BGNNG_BLNC);
     							
     							details.setIlType("ADD");
     							details.setIlPosted(arReceipt.getRctPosted());
    							
    							
    							list.add(details);
    							
    						}
    					}
    					
    					
    					if(apPostedCHKDrs!=null){
    						j = apPostedCHKDrs.iterator();
    						
    						while (j.hasNext()) {
    							
    							LocalApCheck apCheck = (LocalApCheck)j.next();
    							
    							double AMNT = this.convertForeignToFunctionalCurrency(
    									apCheck.getGlFunctionalCurrency().getFcCode(),
    									apCheck.getGlFunctionalCurrency().getFcName(),
    									apCheck.getChkConversionDate(),
    									apCheck.getChkConversionRate(),
    									apCheck.getChkAmount(), AD_CMPNY);
    							if (AMNT==0) continue;
    							
    							GlRepInvestorLedgerDetails details = new GlRepInvestorLedgerDetails();

     							details.setIlInvestorCode(apSupplier.getSplSupplierCode());
     							details.setIlInvestorName(apSupplier.getSplName());
     							details.setIlInvestorContact(apSupplier.getSplContact());
     							details.setIlInvestorAddress(apSupplier.getSplAddress());
     							
     							details.setIlEffectiveDate(apCheck.getChkDate());
     							details.setIlDocumentNumber(apCheck.getChkDocumentNumber());
     							details.setIlReferenceNumber(apCheck.getChkReferenceNumber());
     							details.setIlSourceName("ACCOUNTS PAYABLES");
     							details.setIlDescription(apCheck.getChkDescription());
     							details.setIlDebit((byte)1);
     							details.setIlAmount(AMNT);
     							details.setIlBeginningBalance(INVTR_BGNNG_BLNC);
     							details.setIlType("SUB");
     							details.setIlPosted(apCheck.getChkPosted());

    							list.add(details);
    							
    						}
    					}
    					
    					
    					if(apPostedVOUDrs!=null){
     						j = apUnpostedVOUDrs.iterator();
     						
     						while (j.hasNext()) {
     							
     							LocalApVoucher apVoucher = (LocalApVoucher)j.next();
     							 
     							Collection apDebitMemos = apVoucherHome.findByVouDebitMemoAndVouDmVoucherNumberAndVouVoid(
     									apVoucher.getVouDmVoucherNumber(), AD_BRNCH, AD_CMPNY);
     							
     							Iterator x = apDebitMemos.iterator();
     							
     							double debitMemoAmount = 0d;
     							
     							while(x.hasNext()){
     								
     								LocalApVoucher apDebitMemo = (LocalApVoucher)x.next();
     								debitMemoAmount += apDebitMemo.getVouBillAmount();
     							}
     							
     							double AMNT = this.convertForeignToFunctionalCurrency(
     									apVoucher.getGlFunctionalCurrency().getFcCode(),
     									apVoucher.getGlFunctionalCurrency().getFcName(),
     									apVoucher.getVouConversionDate(),
     									apVoucher.getVouConversionRate(),
     									apVoucher.getVouAmountDue() - debitMemoAmount, AD_CMPNY);

     							if (AMNT==0) continue;
     							GlRepInvestorLedgerDetails details = new GlRepInvestorLedgerDetails();

     							details.setIlInvestorCode(apSupplier.getSplSupplierCode());
     							details.setIlInvestorName(apSupplier.getSplName());
     							details.setIlInvestorContact(apSupplier.getSplContact());
     							details.setIlInvestorAddress(apSupplier.getSplAddress());
     							
     							details.setIlEffectiveDate(apVoucher.getVouDate());
     							details.setIlDocumentNumber(apVoucher.getVouDocumentNumber());
     							details.setIlReferenceNumber(apVoucher.getVouReferenceNumber());
     							details.setIlSourceName("ACCOUNTS PAYABLES");
     							details.setIlDescription(apVoucher.getVouDescription());
     							details.setIlDebit((byte)0);
     							details.setIlAmount(AMNT);
     							details.setIlBeginningBalance(INVTR_BGNNG_BLNC);
     							details.setIlType("ADD");
     							details.setIlPosted(apVoucher.getVouPosted());

     							
     							list.add(details);
     							
     						}

     					
     					}
    					
    
  	     				
   	     				//include unposted subledger transactions
   	     				
   	     				if(SL_INCLD_UNPSTD){

   	     					if(arUnpostedRCTDrs!=null){
   	     						j = arUnpostedRCTDrs.iterator();
   	     						
   	     						while (j.hasNext()) {
   	     							
   	     							
   	     							LocalArReceipt arReceipt = (LocalArReceipt)j.next();
   	     							
   	     							double AMNT = this.convertForeignToFunctionalCurrency(
   	     									arReceipt.getGlFunctionalCurrency().getFcCode(),
   	     									arReceipt.getGlFunctionalCurrency().getFcName(),
   	     									arReceipt.getRctConversionDate(),
   	     									arReceipt.getRctConversionRate(),
   	     									arReceipt.getRctAmount(), AD_CMPNY);
   	     							if (AMNT==0) continue;
   	     							
   	     							
   	     							GlRepInvestorLedgerDetails details = new GlRepInvestorLedgerDetails();

	     							details.setIlInvestorCode(apSupplier.getSplSupplierCode());
	     							details.setIlInvestorName(apSupplier.getSplName());
	     							details.setIlInvestorContact(apSupplier.getSplContact());
	     							details.setIlInvestorAddress(apSupplier.getSplAddress());
	     							
	     							details.setIlEffectiveDate(arReceipt.getRctDate());
	     							details.setIlDocumentNumber(arReceipt.getRctNumber());
	     							details.setIlReferenceNumber(arReceipt.getRctReferenceNumber());
	     							details.setIlSourceName("ACCOUNTS RECEIVABLES");
	     							details.setIlDescription(arReceipt.getRctDescription());
	     							details.setIlDebit((byte)0);
	     							details.setIlAmount(AMNT);
	     							details.setIlBeginningBalance(INVTR_BGNNG_BLNC);
	     							details.setIlType("ADD");
	     							details.setIlPosted(arReceipt.getRctPosted());

   	     							list.add(details);
   	     							
   	     						}
   	     					}
   	     					
   	     					
   	     					if(apUnpostedCHKDrs!=null){
   	     						j = apUnpostedCHKDrs.iterator();
   	     						
   	     						while (j.hasNext()) {
   	     							
   	     							LocalApCheck apCheck = (LocalApCheck)j.next();
   	     							
   	     							double AMNT = this.convertForeignToFunctionalCurrency(
   	     									apCheck.getGlFunctionalCurrency().getFcCode(),
   	     									apCheck.getGlFunctionalCurrency().getFcName(),
   	     									apCheck.getChkConversionDate(),
   	     									apCheck.getChkConversionRate(),
   	     									apCheck.getChkAmount(), AD_CMPNY);

   	     							if (AMNT==0) continue;
   	     							GlRepInvestorLedgerDetails details = new GlRepInvestorLedgerDetails();

   	     							details.setIlInvestorCode(apSupplier.getSplSupplierCode());
   	     							details.setIlInvestorName(apSupplier.getSplName());
   	     							details.setIlInvestorContact(apSupplier.getSplContact());
   	     							details.setIlInvestorAddress(apSupplier.getSplAddress());
   	     							
   	     							details.setIlEffectiveDate(apCheck.getChkDate());
   	     							details.setIlDocumentNumber(apCheck.getChkDocumentNumber());
   	     							details.setIlReferenceNumber(apCheck.getChkReferenceNumber());
   	     							details.setIlSourceName("ACCOUNTS PAYABLES");
   	     							details.setIlDescription(apCheck.getChkDescription());
   	     							details.setIlDebit((byte)1);
   	     							details.setIlAmount(AMNT);
   	     							details.setIlBeginningBalance(INVTR_BGNNG_BLNC);
   	     							details.setIlType("SUB");
   	     							details.setIlPosted(apCheck.getChkPosted());

   	     							
   	     							list.add(details);
   	     							
   	     						}
   
   	     					
   	     					}
   	     					
   	     					if(apUnpostedVOUDrs!=null){
	     						j = apUnpostedVOUDrs.iterator();
	     						
	     						while (j.hasNext()) {
	     							
	     							LocalApVoucher apVoucher = (LocalApVoucher)j.next();
	     							
	     							double AMNT = this.convertForeignToFunctionalCurrency(
	     									apVoucher.getGlFunctionalCurrency().getFcCode(),
	     									apVoucher.getGlFunctionalCurrency().getFcName(),
	     									apVoucher.getVouConversionDate(),
	     									apVoucher.getVouConversionRate(),
	     									apVoucher.getVouAmountDue(), AD_CMPNY);

	     							if (AMNT==0) continue;
	     							GlRepInvestorLedgerDetails details = new GlRepInvestorLedgerDetails();

	     							details.setIlInvestorCode(apSupplier.getSplSupplierCode());
	     							details.setIlInvestorName(apSupplier.getSplName());
	     							details.setIlInvestorContact(apSupplier.getSplContact());
	     							details.setIlInvestorAddress(apSupplier.getSplAddress());
	     							
	     							details.setIlEffectiveDate(apVoucher.getVouDate());
	     							details.setIlDocumentNumber(apVoucher.getVouDocumentNumber());
	     							details.setIlReferenceNumber(apVoucher.getVouReferenceNumber());
	     							details.setIlSourceName("ACCOUNTS PAYABLES");
	     							details.setIlDescription(apVoucher.getVouDescription());
	     							details.setIlDebit((byte)0);
	     							details.setIlAmount(AMNT);
	     							details.setIlBeginningBalance(INVTR_BGNNG_BLNC);
	     							details.setIlType("ADD");
	     							details.setIlPosted(apVoucher.getVouPosted());

	     							
	     							list.add(details);
	     							
	     						}

	     					
	     					}
   	     					
   	     					
   	     				
   	     					
   	     					
   	     					
   	     				}
   	     			
   	     		}
 		 	}
   	     }
   	     
 		 
 		 if (list.isEmpty()) {
 			 System.out.println("CHECK E");
 		 	throw new GlobalNoRecordFoundException();
 		 	
 		 }		 
 		 
 		 if(ORDER_BY.equals("INVESTOR NAME")){
 			 
 			Collections.sort(list, GlRepInvestorLedgerDetails.InvestorNameGroupComparator);
 	 		
 			 
 		 } else {
 			 
 			Collections.sort(list, GlRepInvestorLedgerDetails.NoGroupComparator);
 	 		
 			 
 		 }
 			
 		
 		return list;
 	  	  
 	
 	  } catch (GlobalNoRecordFoundException ex) {
 	  	
 	  	  throw ex;
 	  	
 	  } catch (Exception ex) {
 	  	
 	  	  Debug.printStackTrace(ex);
       	  throw new EJBException(ex.getMessage());
 	  	
 	  }
 //
    } 
    
    
    
    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApModSupplierDetails getApSplBySplSupplierCode(String SPL_SPPLR_CODE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {

		Debug.print("GlRepInvestorLedgerControllerBean getApSplBySplSupplierCode");

		LocalApSupplierHome apSupplierHome = null;        

		// Initialize EJB Home

		try {

			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}        

		try {

			LocalApSupplier apSupplier = null;


			try {

				apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ApModSupplierDetails mdetails = new ApModSupplierDetails();

			mdetails.setSplPytName(apSupplier.getAdPaymentTerm() != null ?
					apSupplier.getAdPaymentTerm().getPytName() : null);
			mdetails.setSplScWtcName(apSupplier.getApSupplierClass().getApWithholdingTaxCode() != null ?
					apSupplier.getApSupplierClass().getApWithholdingTaxCode().getWtcName() : null);
			mdetails.setSplName(apSupplier.getSplName());

			if (apSupplier.getApSupplierClass().getApTaxCode() != null ){

				mdetails.setSplScTcName( apSupplier.getApSupplierClass().getApTaxCode().getTcName());
				mdetails.setSplScTcType( apSupplier.getApSupplierClass().getApTaxCode().getTcType());
				mdetails.setSplScTcRate( apSupplier.getApSupplierClass().getApTaxCode().getTcRate());

			}        	

			if(apSupplier.getInvLineItemTemplate() != null) {
				mdetails.setSplLitName(apSupplier.getInvLineItemTemplate().getLitName());
			}

			return mdetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
    
  
 //   
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlFcAllWithDefault(Integer AD_CMPNY) {
		
		Debug.print("GlRepInvestorLedgerControllerBean getGlFcAllWithDefault");
		
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		Collection glFunctionalCurrencies = null;
		
		LocalGlFunctionalCurrency glFunctionalCurrency = null;
		LocalAdCompany adCompany = null;
		
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAllEnabled(
					EJBCommon.getGcCurrentDateWoTime().getTime(), AD_CMPNY);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (glFunctionalCurrencies.isEmpty()) {
			
			return null;
			
		}
		
		Iterator i = glFunctionalCurrencies.iterator();
		
		while (i.hasNext()) {
			
			glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();
			
			GlModFunctionalCurrencyDetails mdetails = new GlModFunctionalCurrencyDetails(
					glFunctionalCurrency.getFcCode(), glFunctionalCurrency.getFcName(),
					adCompany.getGlFunctionalCurrency().getFcName().equals(glFunctionalCurrency.getFcName()) ?
							EJBCommon.TRUE : EJBCommon.FALSE);
			
			list.add(mdetails);
			
		}
		
		return list;
		
	}
   
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

      Debug.print("GlRepInvestorLedgerControllerBean getGlReportableAcvAll");      
      
      LocalAdCompanyHome adCompanyHome = null;
            
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try {
          
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         
         AdCompanyDetails details = new AdCompanyDetails();
         details.setCmpName(adCompany.getCmpName());
         details.setCmpTaxPayerName(adCompany.getCmpTaxPayerName());
         details.setCmpContact(adCompany.getCmpContact());
         details.setCmpPhone(adCompany.getCmpPhone());
         details.setCmpEmail(adCompany.getCmpEmail());
         details.setCmpTin(adCompany.getCmpTin());
         
         details.setCmpMailSectionNo(adCompany.getCmpMailSectionNo());
		  details.setCmpMailLotNo(adCompany.getCmpMailLotNo());
		    details.setCmpMailStreet(adCompany.getCmpMailStreet());
		    details.setCmpMailPoBox(adCompany.getCmpMailPoBox());
		    details.setCmpMailCountry(adCompany.getCmpMailCountry());
		    details.setCmpMailProvince(adCompany.getCmpMailProvince());
		    details.setCmpMailPostOffice(adCompany.getCmpMailPostOffice());
		    details.setCmpMailCareOff(adCompany.getCmpMailCareOff());
		    details.setCmpTaxPeriodFrom(adCompany.getCmpTaxPeriodFrom());
		    details.setCmpTaxPeriodTo(adCompany.getCmpTaxPeriodTo());
		    details.setCmpPublicOfficeName(adCompany.getCmpPublicOfficeName());
		    details.setCmpDateAppointment(adCompany.getCmpDateAppointment());
         
         return details;  	
      	       	
      } catch (Exception ex) {
      	
      	 Debug.printStackTrace(ex);
      	 throw new EJBException(ex.getMessage());
      	  
      }
      
   }
   
  
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/   
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
        
        Debug.print("GlRepInvestorLedgerControllerBean getAdBrResAll");
        
        LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
        LocalAdBranchHome adBranchHome = null;
        
        LocalAdBranchResponsibility adBranchResponsibility = null;
        LocalAdBranch adBranch = null;
        
        Collection adBranchResponsibilities = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	System.out.println("CHECK F");
            throw new GlobalNoRecordFoundException();
            
        }
        
        try {
            
            Iterator i = adBranchResponsibilities.iterator();
            
            while(i.hasNext()) {
                
                adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
                
                adBranch = adBranchResponsibility.getAdBranch();
                
                AdBranchDetails details = new AdBranchDetails();
                
                details.setBrCode(adBranch.getBrCode());
                details.setBrBranchCode(adBranch.getBrBranchCode());
                details.setBrName(adBranch.getBrName());
                details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
                
                list.add(details);
                
            }	               
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
        
    }	
   
   // private methods
   
   private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
	    Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
	    	
	    //Debug.print("GlRepInvestorLedgerControllerBean convertForeignToFunctionalCurrency");
	    
	    
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
         
        LocalAdCompany adCompany = null;
                 
        // Initialize EJB Homes
         
        try {
         	
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
               lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
                   
         } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
         
         // get company and extended precision
         
         try {
         	
             adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
         	             
         } catch (Exception ex) {
         	
             throw new EJBException(ex.getMessage());
         	
         }	     
        
         
         // Convert to functional currency if necessary
         
         if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
         	         
             AMOUNT = AMOUNT * CONVERSION_RATE;
             	
         } else if (CONVERSION_DATE != null) {
         	
         	 try {
         	 	    
         	 	 // Get functional currency rate
         	 	 
         	     LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
         	     
         	     if (!FC_NM.equals("USD")) {
         	     	
        	         glReceiptFunctionalCurrencyRate = 
	     	             glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
	     	             CONVERSION_DATE, AD_CMPNY);
	     	             
	     	         AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();
	     	             
         	     }
                 
                 // Get set of book functional currency rate if necessary
         	 	         
                 if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
                 
                     LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
                         glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                             getFcCode(), CONVERSION_DATE, AD_CMPNY);
                     
                     AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();
                 
                  }
                                 
         	             
         	 } catch (Exception ex) {
         	 	
         	 	throw new EJBException(ex.getMessage());
         	 	
         	 }       
         	
         }
         
         return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
	}
   
   // private methods from FRG Run
  
	
	
	
	private double convertFunctionalToForeignCurrency(Integer FC_CODE, String FC_NM, 
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

		Debug.print("GlRepInvestorLedgerControllerBean convertFunctionalToForeignCurrency");


		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		LocalAdCompany adCompany = null;

		// Initialize EJB Homes

		try {

			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		// get company and extended precision

		try {

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());

		}	     


		// Convert to functional currency if necessary

		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

			AMOUNT = AMOUNT / CONVERSION_RATE;

		} else if (CONVERSION_DATE != null) {

			try {
				
				double FC_CONVERSION_RATE = 1f;
				double BK_CONVERSION_RATE = 1f;

				// Get functional currency rate

				if (!FC_NM.equals("USD")) {
					
					LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
					
					glReceiptFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByFcCodeAndDate(
							FC_CODE, CONVERSION_DATE, AD_CMPNY);
					
					if (glReceiptFunctionalCurrencyRate != null) {
						
						FC_CONVERSION_RATE = glReceiptFunctionalCurrencyRate.getFrXToUsd();
						
					} else {
						
						// get latest daily rate prior to conversion date
						
						Collection glReceiptFunctionalCurrencyRates = 
							glFunctionalCurrencyRateHome.findPriorByFcCodeAndDate(
									FC_CODE, CONVERSION_DATE, AD_CMPNY);
						
						Iterator i = glReceiptFunctionalCurrencyRates.iterator();
						
						if (i.hasNext()) {
							
							glReceiptFunctionalCurrencyRate = (LocalGlFunctionalCurrencyRate) i.next();
							FC_CONVERSION_RATE = glReceiptFunctionalCurrencyRate.getFrXToUsd();
							
						}
							
					}

					AMOUNT = AMOUNT / FC_CONVERSION_RATE;
					

				}

				// Get set of book functional currency rate if necessary

				if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
					
					LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = null;

					glCompanyFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByFcCodeAndDate(
							adCompany.getGlFunctionalCurrency().getFcCode(), CONVERSION_DATE, AD_CMPNY);

					if (glCompanyFunctionalCurrencyRate != null) {
						
						BK_CONVERSION_RATE = glCompanyFunctionalCurrencyRate.getFrXToUsd();
						
					} else {
						
						// get latest daily rate prior to conversion date
						
						Collection glCompanyFunctionalCurrencyRates = 
							glFunctionalCurrencyRateHome.findPriorByFcCodeAndDate(
									adCompany.getGlFunctionalCurrency().getFcCode(), CONVERSION_DATE, AD_CMPNY);
						
						Iterator i = glCompanyFunctionalCurrencyRates.iterator();
						
						if (i.hasNext()) {
							
							glCompanyFunctionalCurrencyRate = (LocalGlFunctionalCurrencyRate) i.next();
							BK_CONVERSION_RATE = glCompanyFunctionalCurrencyRate.getFrXToUsd();
							
						}
						
					}
					
					//AMOUNT = AMOUNT * (1 / EJBCommon.roundIt(1 / BK_CONVERSION_RATE, (short)3));
					AMOUNT = AMOUNT * BK_CONVERSION_RATE;

				}

			} catch (Exception ex) {

			}    
	
		}
		
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

	}

   
      
  
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlRepInvestorLedgerControllerBean ejbCreate");
      
   }  
   
}
