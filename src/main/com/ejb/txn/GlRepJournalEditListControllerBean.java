
/*
 * GlRepJournalEditListControllerBean.java
 *
 * Created on June 25, 2004, 4:51 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlRepJournalEditListDetails;

/**
 * @ejb:bean name="GlRepJournalEditListControllerEJB"
 *           display-name="Used for printing journal list transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepJournalEditListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepJournalEditListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepJournalEditListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class GlRepJournalEditListControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeGlRepJournalEditList(ArrayList jrCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlRepJournalEditListControllerBean executeGlRepJournalEditList");
        
        LocalGlJournalHome glJournalHome = null;        
       
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
        	glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = jrCodeList.iterator();
        	
        	while (i.hasNext()) {
        	
        	    Integer JR_CODE = (Integer) i.next();
        	
	        	LocalGlJournal glJournal = null;

	        	try {
	        		
	        		glJournal = glJournalHome.findByPrimaryKey(JR_CODE);

	        	} catch (FinderException ex) {
	        		
	        		continue;
	        		
	        	}
	        		        	
	        	// get journal lines
	        	
	        	Collection glJournalLines = glJournal.getGlJournalLines();        	            
	        	
	        	Iterator drIter = glJournalLines.iterator();
	        	
	        	while (drIter.hasNext()) {
	        		
	        		LocalGlJournalLine glJournalLine = (LocalGlJournalLine)drIter.next();
	        		
	        		GlRepJournalEditListDetails details = new GlRepJournalEditListDetails();
	        			        			        		
	        		details.setJelJrDateCreated(glJournal.getJrDateCreated());
	        		details.setJelJrCreatedBy(glJournal.getJrCreatedBy());
	        		details.setJelJrDate(glJournal.getJrEffectiveDate());
	        		details.setJelJrDocumentNumber(glJournal.getJrDocumentNumber());
	        		details.setJelJrReferenceNumber(glJournal.getJrName());
	        		details.setJelJrDescription(glJournal.getJrDescription());
	        		details.setJelJrBatchName(glJournal.getGlJournalBatch() != null ? glJournal.getGlJournalBatch().getJbName() : "N/A");
	        		details.setJelJrBatchDescription(glJournal.getGlJournalBatch() != null ? glJournal.getGlJournalBatch().getJbDescription() : "N/A");
	        		details.setJelJrTransactionTotal(glJournal.getGlJournalBatch() != null ? glJournal.getGlJournalBatch().getGlJournals().size() : 0);
	        		details.setJelJlAccountNumber(glJournalLine.getGlChartOfAccount().getCoaAccountNumber());
	        		details.setJelJlDebit(glJournalLine.getJlDebit());
	        		details.setJelJlAmount(glJournalLine.getJlAmount());	        		
				    details.setJelJlAccountDescription(glJournalLine.getGlChartOfAccount().getCoaAccountDescription());
				    details.setJelJlDescription(glJournalLine.getJlDescription());
				    
	        		list.add(details);

	        	}
	        	
	        }
	        
	        if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	        }        	       	
        	
        	return list;        	
        	        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("GlRepJournalEditListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlRepJournalEditListControllerBean ejbCreate");
      
    }
}
