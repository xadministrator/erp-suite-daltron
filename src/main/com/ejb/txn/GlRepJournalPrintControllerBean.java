
/*
 * GlRepJournalPrintControllerBean.java
 *
 * Created on December 02, 2003, 1:09 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApRepCheckRegisterDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlRepJournalPrintDetails;

/**
 * @ejb:bean name="GlRepJournalPrintControllerEJB"
 *           display-name="Used for printing journal transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepJournalPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepJournalPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepJournalPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class GlRepJournalPrintControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeGlRepJournalPrint(ArrayList jrCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlRepJournalPrintControllerBean executeGlRepJournalPrint");
        
        LocalGlJournalHome glJournalHome = null; 
        LocalGlJournalLineHome glJournalLineHome = null; 
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
        LocalAdUserHome adUserHome = null;
        LocalAdBranchHome adBranchHome = null;
        
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = jrCodeList.iterator();
        	
        	while (i.hasNext()) {
        	
        	    Integer JR_CODE = (Integer) i.next();
        	            	    
	        	LocalGlJournal glJournal = null;
	        		        	
	        	try {
	        		
	        		glJournal = glJournalHome.findByPrimaryKey(JR_CODE);
	        		
	        	} catch (FinderException ex) {
	        		
	        		continue;
	        		
	        	}
	        	LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
	        	LocalAdApprovalDocument adApprovalDocument = adApprovalDocumentHome.findByAdcType("GL JOURNAL", AD_CMPNY);
	        		        		        		            
	        	
	        	if (adApprovalDocument.getAdcPrintOption().equals("PRINT APPROVED ONLY")) {
	        	
	        		if (glJournal.getJrApprovalStatus() == null || 
	        			glJournal.getJrApprovalStatus().equals("PENDING")) {
	        		    
	        		    continue;
	        		    
	        		} 
	        	
	        	
	        	} else if (adApprovalDocument.getAdcPrintOption().equals("PRINT UNAPPROVED ONLY")) {
	        	
	        		if (glJournal.getJrApprovalStatus() != null && 
	        			(glJournal.getJrApprovalStatus().equals("N/A") || 
	        			 glJournal.getJrApprovalStatus().equals("APPROVED"))) {
	        		
	        			continue;
	        		
	        		}
	        	
	        	}
	        	
	        	if (adApprovalDocument.getAdcAllowDuplicate() == EJBCommon.FALSE && 
	        		glJournal.getJrPrinted() == EJBCommon.TRUE){
	        		
	        		continue;	
	        		
	        	}
	        		        		        	
	        	// show duplicate
	        	
	        	boolean showDuplicate = false;
	        	
	        	if (adApprovalDocument.getAdcTrackDuplicate() == EJBCommon.TRUE &&
	        		glJournal.getJrPrinted() == EJBCommon.TRUE) {
	        		
	        		showDuplicate = true;
	        		
	        	}
	        	    
	        	// set jr printed
	        	
	        	glJournal.setJrPrinted(EJBCommon.TRUE);
	        		        	
	        	// get journal lines
	        	
	        	Collection glJournalLines = glJournalLineHome.findByJrCode(glJournal.getJrCode(), AD_CMPNY);      	            
	        	
	        	Iterator jlIter = glJournalLines.iterator();
	        	
	        	while (jlIter.hasNext()) {
	        		
	        		LocalGlJournalLine glJournalLine = (LocalGlJournalLine)jlIter.next();
	        		
	        		GlRepJournalPrintDetails details = new GlRepJournalPrintDetails();
	        		
	        		details.setJpJrEffectiveDate(glJournal.getJrEffectiveDate());
	        		details.setJpJrDocumentNumber(glJournal.getJrDocumentNumber());
	        		details.setJpJrCreatedBy(glJournal.getJrCreatedBy());
	        		details.setJpJrApprovedRejectedBy(glJournal.getJrApprovedRejectedBy());	        		
	        		details.setJpJlDebit(glJournalLine.getJlDebit());
	        		details.setJpJlAmount(glJournalLine.getJlAmount());
	        		details.setJpJlCoaAccountNumber(glJournalLine.getGlChartOfAccount().getCoaAccountNumber());
	        		details.setJpJlCoaAccountDescription(glJournalLine.getGlChartOfAccount().getCoaAccountDescription());
					details.setJpShowDuplicate(showDuplicate ? EJBCommon.TRUE : EJBCommon.FALSE);	        			        		
					details.setJpJrApprovalStatus(glJournal.getJrApprovalStatus());
					details.setJpJrPosted(glJournal.getJrPosted());
					details.setJpJrDescription(glJournal.getJrDescription());
					details.setJpJrDateReversal(glJournal.getJrDateReversal());
					details.setJpJrReversed(glJournal.getJrReversed());
					details.setJpJrPostedBy(glJournal.getJrPostedBy());
					details.setJpJrJcName(glJournal.getGlJournalCategory().getJcName());
					details.setJpJrJsName(glJournal.getGlJournalSource().getJsName());
					details.setJpJrFcName(glJournal.getGlFunctionalCurrency().getFcName());	
					details.setJpJrFcSymbol(glJournal.getGlFunctionalCurrency().getFcSymbol());	
					details.setJpJrJbName(glJournal.getGlJournalBatch() != null ? glJournal.getGlJournalBatch().getJbName() : null);
					details.setJpJrName(glJournal.getJrName());
					
					// get created by user description
					try{
						LocalAdUser adUser = adUserHome.findByUsrName(glJournal.getJrCreatedBy(), AD_CMPNY);
	    				details.setJpJrCreatedByDescription(adUser.getUsrDescription());
					}catch(Exception e){
						details.setJpJrCreatedByDescription(glJournal.getJrCreatedBy());
					}

    				try{
    					System.out.println("setJpJrApprovedByDescription(): "+glJournal.getJrApprovedRejectedBy());
        				LocalAdUser adUser3 = adUserHome.findByUsrName(glJournal.getJrApprovedRejectedBy(), AD_CMPNY);
        				details.setJpJrApprovedByDescription(adUser3.getUsrDescription());
        				
    				}catch(Exception e){
    					details.setJpJrApprovedByDescription("");
    				}
    				
    				//Include Branch
    				LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(glJournal.getJrAdBranch());
    				details.setBrBranchCode(adBranch.getBrBranchCode());
    				details.setBrName(adBranch.getBrName());
    				
    				details.setJpJlDescription(glJournalLine.getJlDescription());
    				
	        		list.add(details);
	        		
	        	}
	        	
	        }
        	
        	
	        
	        if (list.isEmpty()) {
	        	
	        	throw new GlobalNoRecordFoundException();
	        	
	        }        	       	
        	
	        Collections.sort(list, GlRepJournalPrintDetails.accountDescriptionComparator);
	        Collections.sort(list, GlRepJournalPrintDetails.debitComparator);
	        
	        
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("GlRepJournalPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}  

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlRepJournalPrintControllerBean ejbCreate");
      
    }
}
