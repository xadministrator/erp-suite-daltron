
/*
 * GlRepJournalRegisterControllerBean
 *
 * Created on March 15, 2006, 2:55 PM
 *
 * @author  Aliza D.J. Anos
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApRepApRegisterDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlRepJournalRegisterDetails;

/**
 * @ejb:bean name="GlRepJournalRegisterControllerEJB"
 *           display-name="Used for generation of journal register reports"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepJournalRegisterControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepJournalRegisterController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepJournalRegisterControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
 */

public class GlRepJournalRegisterControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeGlRepJournalRegister(HashMap criteria, String ORDER_BY, String GROUP_BY, ArrayList branchList, boolean SHOW_ENTRIES, boolean SUMMARIZE, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {
		
		Debug.print("GlRepJournalRegisterControllerBean executeGlRepJournalRegister");
		
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlJournalHome glJournalHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialize EJB Home
		
		try {
			
			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(jr) FROM GlJournal jr WHERE (");
			
			if (branchList.isEmpty())
				throw new GlobalNoRecordFoundException();
			
			Iterator brIter = branchList.iterator();
			
			AdBranchDetails details = (AdBranchDetails)brIter.next();
			jbossQl.append("jr.jrAdBranch=" + details.getBrCode());
			
			while(brIter.hasNext()) {
				
				details = (AdBranchDetails)brIter.next();
				
				jbossQl.append(" OR jr.jrAdBranch=" + details.getBrCode());
				
			}
			
			jbossQl.append(") ");
			firstArgument = false;
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("includeUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("journalNumberFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jr.jrDocumentNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("journalNumberFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("journalNumberTo")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jr.jrDocumentNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("journalNumberTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jr.jrEffectiveDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jr.jrEffectiveDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("source")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jr.glJournalSource.jsName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("source");
				ctr++;
				
			}
			
			if (criteria.containsKey("batchName")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jr.glJournalBatch.jbName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("batchName");
				ctr++;
				
			}
			
			if (criteria.containsKey("category")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("jr.glJournalCategory.jcName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;
				
			}
			
			if (criteria.containsKey("includeUnposted") && ((String)criteria.get("includeUnposted")).equals("NO")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	       	  
				
				jbossQl.append("jr.jrPosted=1 ");
				
			}	
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("jr.jrAdCompany=" + AD_CMPNY + " ");
			
			Collection glJournals = glJournalHome.getJrByCriteria(jbossQl.toString(), obj);	         
			
			if (glJournals.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			Iterator i = glJournals.iterator();
			
			double AMNT = 0d;
			
			if(SHOW_ENTRIES) {
				
				while (i.hasNext()) {
					
					LocalGlJournal glJournal = (LocalGlJournal)i.next();
					
					AMNT = 0d;
					
					Collection glJournalLines = glJournal.getGlJournalLines();
					Iterator j = glJournalLines.iterator();
					
					while(j.hasNext()) {
						
						LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();
						
						if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
							
							AMNT += glJournalLine.getJlAmount();
							
						}
																
					}
					
					glJournalLines = glJournal.getGlJournalLines();
					j = glJournalLines.iterator();
					
					while(j.hasNext()) {
						
						LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();
						
						GlRepJournalRegisterDetails mdetails = new GlRepJournalRegisterDetails();
						
						mdetails.setJrJrDocumentNumber(glJournal.getJrDocumentNumber());
						mdetails.setJrJrReferenceNumber(glJournal.getJrName());
						mdetails.setJrJrDescription(glJournal.getJrDescription());
						mdetails.setJrJrDate(glJournal.getJrEffectiveDate());
						mdetails.setJrJrSource(glJournal.getGlJournalSource().getJsName());
						mdetails.setJrJrCategory(glJournal.getGlJournalCategory().getJcName());
						mdetails.setJrJrBatch(glJournal.getGlJournalBatch() != null ? glJournal.getGlJournalBatch().getJbName() : null);
						mdetails.setJrJrAmount(AMNT);
						mdetails.setOrderBy(ORDER_BY);
						
						mdetails.setJrJlCoaAccountNumber(glJournalLine.getGlChartOfAccount().getCoaAccountNumber());
						mdetails.setJrJlCoaAccountDescription(glJournalLine.getGlChartOfAccount().getCoaAccountDescription());
						
						if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
							
							mdetails.setJrJlDebit(glJournalLine.getJlAmount());
							
						} else {
							
							mdetails.setJrJlCredit(glJournalLine.getJlAmount());
							
						}
						
						list.add(mdetails);
						
					}
					
				}
				
			} else {
								
				while (i.hasNext()) {
					
					LocalGlJournal glJournal = (LocalGlJournal)i.next();
					
					AMNT = 0d;
					
					Collection glJournalLines = glJournal.getGlJournalLines();
					Iterator j = glJournalLines.iterator();
					
					while(j.hasNext()) {
						
						LocalGlJournalLine glJournalLine = (LocalGlJournalLine)j.next();
						
						if(glJournalLine.getJlDebit() == EJBCommon.TRUE) {
							
							AMNT += glJournalLine.getJlAmount();
							
						}
																
					}
					
					GlRepJournalRegisterDetails mdetails = new GlRepJournalRegisterDetails();
					
					mdetails.setJrJrDocumentNumber(glJournal.getJrDocumentNumber());
					mdetails.setJrJrReferenceNumber(glJournal.getJrName());
					mdetails.setJrJrDescription(glJournal.getJrDescription());
					mdetails.setJrJrDate(glJournal.getJrEffectiveDate());
					mdetails.setJrJrSource(glJournal.getGlJournalSource().getJsName());
					mdetails.setJrJrCategory(glJournal.getGlJournalCategory().getJcName());
					mdetails.setJrJrBatch(glJournal.getGlJournalBatch() != null ? glJournal.getGlJournalBatch().getJbName() : null);
					mdetails.setJrJrAmount(AMNT);
					mdetails.setOrderBy(ORDER_BY);
					
					list.add(mdetails);
					
				}
					
			}
			
			// sort
			
			if (GROUP_BY.equals("SOURCE")) {
				
				Collections.sort(list, GlRepJournalRegisterDetails.JournalSourceComparator);
				
			} else if (GROUP_BY.equals("CATEGORY")) {
				
				Collections.sort(list, GlRepJournalRegisterDetails.JournalCategoryComparator);
				
			} else if (GROUP_BY.equals("BATCH")) {
				
				Collections.sort(list, GlRepJournalRegisterDetails.JournalBatchComparator);
				
			} else {
				
				Collections.sort(list, GlRepJournalRegisterDetails.NoGroupComparator);
				
			}

			if(SUMMARIZE) {

				Collections.sort(list, GlRepJournalRegisterDetails.CoaAccountNumberComparator);

			}
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}   
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
		
		Debug.print("GlRepJournalRegisterControllerBean getGlReportableAcvAll");      
		
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());
			
			return details;  	
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{
		
		Debug.print("GlRepJournalRegisterControllerBean getAdBrResAll");
		
		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;
		
		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;
		
		Collection adBranchResponsibilities = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (adBranchResponsibilities.isEmpty()) {
			
			throw new GlobalNoRecordFoundException();
			
		}
		
		try {
			
			Iterator i = adBranchResponsibilities.iterator();
			
			while(i.hasNext()) {
				
				adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
				
				adBranch = adBranchResponsibility.getAdBranch();
				
				AdBranchDetails details = new AdBranchDetails();
				
				details.setBrCode(adBranch.getBrCode());
				details.setBrBranchCode(adBranch.getBrBranchCode());
				details.setBrName(adBranch.getBrName());
				details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
				
				list.add(details);
				
			}	               
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		return list;
		
	}	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlJsAll(Integer AD_CMPNY) {
		
		Debug.print("GlRepGeneralLedgerControllerBean getGlJsAll");
		
		LocalGlJournalSourceHome glJournalSourceHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection glJournalSources = glJournalSourceHome.findJsAll(AD_CMPNY);
			
			Iterator i = glJournalSources.iterator();
			
			while (i.hasNext()) {
				
				LocalGlJournalSource glJournalSource = (LocalGlJournalSource)i.next();
				
				list.add(glJournalSource.getJsName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlJcAll(Integer AD_CMPNY) {
		
		Debug.print("GlRepGeneralLedgerControllerBean getGlJcAll");
		
		LocalGlJournalCategoryHome glJournalCategoryHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection glJournalCategories = glJournalCategoryHome.findJcAll(AD_CMPNY);
			
			Iterator i = glJournalCategories.iterator();
			
			while (i.hasNext()) {
				
				LocalGlJournalCategory glJournalCategory= (LocalGlJournalCategory)i.next();
				
				list.add(glJournalCategory.getJcName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlJbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
		
		Debug.print("GlRepGeneralLedgerControllerBean getGlJbAll");
		
		LocalGlJournalBatchHome glJournalBatchHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection glJournalBatches = glJournalBatchHome.findOpenJbAll(AD_BRNCH, AD_CMPNY);
			
			Iterator i = glJournalBatches.iterator();
			
			while (i.hasNext()) {
				
				LocalGlJournalBatch glJournalBatch = (LocalGlJournalBatch)i.next();
				
				list.add(glJournalBatch.getJbName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// private methods
	
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
		
		Debug.print("GlRepGeneralLedgerControllerBean convertForeignToFunctionalCurrency");
		
		
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		LocalAdCompany adCompany = null;
		
		// Initialize EJB Homes
		
		try {
			
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		// get company and extended precision
		
		try {
			
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}	     
		
		
		// Convert to functional currency if necessary
		
		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
			
			AMOUNT = AMOUNT * CONVERSION_RATE;
			
		} else if (CONVERSION_DATE != null) {
			
			try {
				
				// Get functional currency rate
				
				LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
				
				if (!FC_NM.equals("USD")) {
					
					glReceiptFunctionalCurrencyRate = 
						glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
								CONVERSION_DATE, AD_CMPNY);
					
					AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();
					
				}
				
				// Get set of book functional currency rate if necessary
				
				if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
					
					LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
						glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
								getFcCode(), CONVERSION_DATE, AD_CMPNY);
					
					AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();
					
				}
				
				
			} catch (Exception ex) {
				
				throw new EJBException(ex.getMessage());
				
			}       
			
		}
		
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
		
	}
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("GlRepGeneralLedgerControllerBean ejbCreate");
		
	}  
	
}
