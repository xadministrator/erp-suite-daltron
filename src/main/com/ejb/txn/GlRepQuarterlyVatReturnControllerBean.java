
/*
 * GlRepQuarterlyVatReturnControllerBean.java
 *
 * Created on March 29, 2004, 4:47 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlTaxInterface;
import com.ejb.gl.LocalGlTaxInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlRepQuarterlyVatReturnDetails;

/**
 * @ejb:bean name="GlRepQuarterlyVatReturnControllerEJB"
 *           display-name="Used for quarterly vat return report"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepQuarterlyVatReturnControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepQuarterlyVatReturnController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepQuarterlyVatReturnControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:bill type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
*/

public class GlRepQuarterlyVatReturnControllerBean extends AbstractSessionBean {

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public GlRepQuarterlyVatReturnDetails executeGlRepQuarterlyVatReturn(Date DT_FRM,Date DT_TO, Integer AD_CMPNY) {

		Debug.print("GlRepQuarterlyVatReturnControllerBean executeGlRepQuarterlyVatReturn");

		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
        LocalGlTaxInterfaceHome glTaxInterfaceHome = null;
        LocalGlJournalLineHome glJournalHome = null;

        try {

        	apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
        	arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
	        glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
	    		lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);
	        glJournalHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,LocalGlJournalLineHome.class);

        }  catch (NamingException ex) 	{

            throw new EJBException(ex.getMessage());

        }

        try {
        	//get available records with ar documents

        	StringBuffer jbossQl = new StringBuffer();

        	jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti ");

  	        boolean firstArgument = true;
  	        short ctr = 0;
  		    int criteriaSize = 0;

			if(! DT_FRM.equals(null)) {
				criteriaSize++;
			}

			if(! DT_TO.equals(null)) {
				criteriaSize++;
			}

  	        Object obj[] = new Object[criteriaSize];

  	        if (! DT_FRM.equals(null)) {

		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate>=?" + (ctr+1) + " ");
		  	   obj[ctr] = DT_FRM;
		  	   ctr++;
		    }

  	        if (! DT_TO.equals(null)) {

		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate<=?" + (ctr+1) + " ");
		  	   obj[ctr] = DT_TO;
		  	   ctr++;

		    }

		    if (!firstArgument) {

	   	       jbossQl.append("AND ");

	   	    } else {

	   	  	   firstArgument = false;
	   	  	   jbossQl.append("WHERE ");

	   	    }

		    jbossQl.append("(ti.tiDocumentType='AR INVOICE' OR ti.tiDocumentType='AR CREDIT MEMO' OR " +
		    				"ti.tiDocumentType='AR RECEIPT' OR ti.tiDocumentType='GL JOURNAL')" +
		    				" AND ti.tiIsArDocument=1 AND ti.tiTcCode IS NOT NULL AND ti.tiWtcCode IS NULL" +
							" AND ti.tiAdCompany=" + AD_CMPNY + " ORDER BY ti.tiSlSubledgerCode");

		    short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);

		    Collection glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);
		    System.out.println("AR jbossQl.toString()="+jbossQl.toString());
		    System.out.println("SIZE="+glTaxInterfaces.size());

        	Iterator i = glTaxInterfaces.iterator();

        	double TOTAL_NET_SALES = 0d;
        	double TOTAL_OUTPUT_TAX = 0d;

        	while(i.hasNext()) {

        		LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();

    			if(glTaxInterface.getTiDocumentType().equals("GL JOURNAL")) {

    				LocalGlJournalLine glJournal = glJournalHome.findByPrimaryKey(
    									glTaxInterface.getTiTxlCode());

    				if(glJournal.getJlDebit() == EJBCommon.TRUE) {

    					TOTAL_OUTPUT_TAX -= EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT);
    					TOTAL_NET_SALES -= EJBCommon.roundIt(glTaxInterface.getTiNetAmount(), PRECISION_UNIT);

    				} else {

    					TOTAL_OUTPUT_TAX += EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT);
    					TOTAL_NET_SALES += EJBCommon.roundIt(glTaxInterface.getTiNetAmount(), PRECISION_UNIT);

    				}

    			} else {

					LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.findByPrimaryKey(
										glTaxInterface.getTiTxlCode());

					if(arDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

						TOTAL_OUTPUT_TAX -= EJBCommon.roundIt(arDistributionRecord.getDrAmount(), PRECISION_UNIT);
						TOTAL_NET_SALES -= EJBCommon.roundIt(glTaxInterface.getTiNetAmount(), PRECISION_UNIT);

					} else {

						TOTAL_OUTPUT_TAX += EJBCommon.roundIt(arDistributionRecord.getDrAmount(), PRECISION_UNIT);
						TOTAL_NET_SALES += EJBCommon.roundIt(glTaxInterface.getTiNetAmount(), PRECISION_UNIT);

					}

    			}

        	}

        	//get available records with ap documents

        	jbossQl = new StringBuffer();

        	jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti ");

        	firstArgument = true;
  	        ctr = 0;

  	        obj = new Object[criteriaSize];

  	        if (! DT_FRM.equals(null)) {

		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate>=?" + (ctr+1) + " ");
		  	   obj[ctr] = DT_FRM;
		  	   ctr++;
	        }

	        if (! DT_TO.equals(null)) {

		  	   if (!firstArgument) {
		  	 	  jbossQl.append("AND ");
		  	   } else {
		  	 	  firstArgument = false;
		  	 	  jbossQl.append("WHERE ");
		  	   }
		  	   jbossQl.append("ti.tiTxnDate<=?" + (ctr+1) + " ");
		  	   obj[ctr] = DT_TO;
		  	   ctr++;

	        }

	        if (!firstArgument) {

	        	jbossQl.append("AND ");

	   	    } else {

	   	  	   firstArgument = false;
	   	  	   jbossQl.append("WHERE ");

	   	    }

	        jbossQl.append("(ti.tiDocumentType='AP VOUCHER' OR ti.tiDocumentType='AP DEBIT MEMO' OR " +
    				"ti.tiDocumentType='AP CHECK' OR ti.tiDocumentType='AP RECEIVING ITEM' OR " +
    				"ti.tiDocumentType='GL JOURNAL') AND ti.tiIsArDocument=0 AND ti.tiTcCode IS NOT NULL AND " +
					"ti.tiWtcCode IS NULL AND ti.tiAdCompany=" + AD_CMPNY +
					" ORDER BY ti.tiSlSubledgerCode");


	        glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);
	        System.out.println("AP jbossQl.toString()="+jbossQl.toString());
	        System.out.println("SIZE="+glTaxInterfaces.size());
	        i = glTaxInterfaces.iterator();

	        double TOTAL_NET_PURCHASE = 0d;
	        double TOTAL_INPUT_TAX = 0d;
	        double TOTAL_INPUT_TAX_NEW = 0d;
	        double TOTAL_REVENUE = 0d;
	        double TOTAL_REV_EXEMPT = 0d;
	        double TOTAL_REV_ZERO_RATED = 0d;
	        double TOTAL_REV_EXEMPT_ZERO = 0d;

	        while(i.hasNext()) {

				LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();

				TOTAL_INPUT_TAX -= EJBCommon.roundIt(glTaxInterface.getTiTaxAmount(), PRECISION_UNIT);
				TOTAL_INPUT_TAX_NEW -= EJBCommon.roundIt(glTaxInterface.getTiTaxAmount(), PRECISION_UNIT);
				TOTAL_REVENUE -= EJBCommon.roundIt(glTaxInterface.getTiSalesAmount(), PRECISION_UNIT);

				TOTAL_REV_ZERO_RATED +=  EJBCommon.roundIt(glTaxInterface.getTiTaxZeroRated(), PRECISION_UNIT);
    			TOTAL_REV_EXEMPT_ZERO += EJBCommon.roundIt(glTaxInterface.getTiTaxExempt(), PRECISION_UNIT);

			}
	        /*
	        while(i.hasNext()) {

				LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();

				if(glTaxInterface.getTiDocumentType().equals("GL JOURNAL")) {

					LocalGlJournalLine glJournal = glJournalHome.findByPrimaryKey(
							glTaxInterface.getTiTxlCode());

					if(glJournal.getJlDebit() == EJBCommon.TRUE) {

						TOTAL_INPUT_TAX += EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT);
						TOTAL_NET_PURCHASE += EJBCommon.roundIt(glTaxInterface.getTiNetAmount(), PRECISION_UNIT);

					} else {

						TOTAL_INPUT_TAX -= EJBCommon.roundIt(glJournal.getJlAmount(), PRECISION_UNIT);
						TOTAL_NET_PURCHASE -= EJBCommon.roundIt(glTaxInterface.getTiNetAmount(), PRECISION_UNIT);

					}

				} else {

					LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.findByPrimaryKey(
						glTaxInterface.getTiTxlCode());

					if(apDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

						TOTAL_INPUT_TAX += EJBCommon.roundIt(apDistributionRecord.getDrAmount(), PRECISION_UNIT);
						TOTAL_NET_PURCHASE += EJBCommon.roundIt(glTaxInterface.getTiNetAmount(), PRECISION_UNIT);

					} else {

						TOTAL_INPUT_TAX -= EJBCommon.roundIt(apDistributionRecord.getDrAmount(), PRECISION_UNIT);
						TOTAL_NET_PURCHASE -= EJBCommon.roundIt(glTaxInterface.getTiNetAmount(), PRECISION_UNIT);

					}

				}

			}
			*/


	        TOTAL_REVENUE += TOTAL_REV_EXEMPT_ZERO;
	        TOTAL_REV_EXEMPT_ZERO = TOTAL_REV_EXEMPT + TOTAL_REV_ZERO_RATED;


	        GlRepQuarterlyVatReturnDetails details = new GlRepQuarterlyVatReturnDetails();
	        details.setSalesReceiptForQuarter(TOTAL_NET_SALES);
	        details.setTaxOutputQuarter(TOTAL_OUTPUT_TAX);
	        details.setNetPurchasesQuarter(TOTAL_NET_PURCHASE);
	        details.setInputTaxQuarter(TOTAL_INPUT_TAX);

	        return details;

        } catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

   	  	}

	}



	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {

		Debug.print("GlRepQuarterlyVatReturnControllerBean getAdLvReportTypeAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("GL REPORT TYPE - QUARTERLY VAT RETURN", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

	  Debug.print("GlRepQuarterlyVatReturnControllerBean getAdCompany");

	  LocalAdCompanyHome adCompanyHome = null;

	  // Initialize EJB Home

	  try {

	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

	  } catch (NamingException ex) {

	      throw new EJBException(ex.getMessage());

	  }

	  try {

	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpTin(adCompany.getCmpTin());
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpZip(adCompany.getCmpZip());
	     details.setCmpIndustry(adCompany.getCmpIndustry());

	     return details;

	  } catch (Exception ex) {

	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());

	  }

	}

   // private

	private short getGlFcPrecisionUnit(Integer AD_CMPNY) {

	    Debug.print("GlRepQuarterlyVatReturnControllerBean getGlFcPrecisionUnit");

	    LocalAdCompanyHome adCompanyHome = null;


	    // Initialize EJB Home

	    try {

	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	      return  adCompany.getGlFunctionalCurrency().getFcPrecision();

	    } catch (Exception ex) {

	    	 Debug.printStackTrace(ex);
	      throw new EJBException(ex.getMessage());

	    }

	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlRepQuarterlyVatReturnControllerBean ejbCreate");

    }
}
