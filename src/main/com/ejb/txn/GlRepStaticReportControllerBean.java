
/*
 * GlRepStaticReportControllerBean.java
 *
 * Created on November 21, 2005, 8:46 AM
 * 
 * @author  Farrah S. Garing
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlUserStaticReport;
import com.ejb.gl.LocalGlUserStaticReportHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlModUserStaticReportDetails;

/**
 * @ejb:bean name="GlRepStaticReportControllerEJB"
 *           display-name="Used for entering payment schedules"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlRepStaticReportControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlRepStaticReportController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlRepStaticReportControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class GlRepStaticReportControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public GlModUserStaticReportDetails getGlUstrBySrCodeAndUsrCode(Integer SR_CODE, Integer USR_CODE, Integer AD_CMPNY) {
                    
        Debug.print("GlRepStaticReportControllerBean getGlUstrBySrCodeAndFfCode");

        LocalGlUserStaticReportHome glUserStaticReportHome = null;

        LocalGlUserStaticReport glUserStaticReport = null;

        // Initialize EJB Home

        try {
            
            glUserStaticReportHome = (LocalGlUserStaticReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlUserStaticReportHome.JNDI_NAME, LocalGlUserStaticReportHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
        	glUserStaticReport = glUserStaticReportHome.findBySrCodeAndUsrCode(SR_CODE, USR_CODE, AD_CMPNY);
        	
        	GlModUserStaticReportDetails mdetails = new GlModUserStaticReportDetails();
    		mdetails.setUstrCode(glUserStaticReport.getUstrCode());        		
            mdetails.setUstrUsrName(glUserStaticReport.getAdUser().getUsrName());
            
            return mdetails;
            
        } catch (FinderException ex) {
        	
        	return null;
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }        
            
    }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public ArrayList getGlUstrByUsrCode(Integer USR_CODE, Integer AD_CMPNY) 
      	throws GlobalNoRecordFoundException {
                     
         Debug.print("GlRepStaticReportControllerBean getGlUstrByUsrCode");

         LocalGlUserStaticReportHome glUserStaticReportHome = null;



         // Initialize EJB Home

         try {
             
             glUserStaticReportHome = (LocalGlUserStaticReportHome)EJBHomeFactory.
                 lookUpLocalHome(LocalGlUserStaticReportHome.JNDI_NAME, LocalGlUserStaticReportHome.class);

         } catch (NamingException ex) {
             
             throw new EJBException(ex.getMessage());
             
         }

         try {
             
         	Collection glUserStaticReports = null;
         	
         	try {
         		
         		glUserStaticReports = glUserStaticReportHome.findByUsrCode( USR_CODE, AD_CMPNY);
         		
         	} catch (FinderException ex) {
         		
         		throw new GlobalNoRecordFoundException();
         		
         	} 
         	
         	if (glUserStaticReports.isEmpty()) {
         		
         		throw new GlobalNoRecordFoundException();
         		
         	}
         	
         	Iterator i = glUserStaticReports.iterator();
         	
         	ArrayList list = new ArrayList();
         	
         	while (i.hasNext()){
         		
                LocalGlUserStaticReport glUserStaticReport = (LocalGlUserStaticReport) i.next();
         		
         		GlModUserStaticReportDetails mdetails = new GlModUserStaticReportDetails();
         		
         		System.out.println("USTR_CODE="+glUserStaticReport.getUstrCode());
         		mdetails.setUstrCode(glUserStaticReport.getUstrCode());
         		mdetails.setUstrSrFlName(glUserStaticReport.getGlStaticReport().getSrFileName());
         		mdetails.setUstrSrDateTo(glUserStaticReport.getGlStaticReport().getSrDateTo());
         		mdetails.setUstrSrName(glUserStaticReport.getGlStaticReport().getSrName());
         		mdetails.setUstrSrCode(glUserStaticReport.getGlStaticReport().getSrCode());
         		
         		list.add(mdetails);
         		
         	}
             
            return list;
             
         } catch (GlobalNoRecordFoundException ex) {
         	
         	throw ex;
             
         } catch (Exception ex) {
         	
             ex.printStackTrace();
             throw new EJBException(ex.getMessage());
         }        
             
     }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
     public void ejbCreate() throws CreateException {

       Debug.print("GlRepStaticReportControllerBean ejbCreate");
      
    }
}
