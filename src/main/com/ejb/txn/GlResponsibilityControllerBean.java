package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.GlORGNoOrganizationFoundException;
import com.ejb.exception.GlRESNoResponsibilityFoundException;
import com.ejb.exception.GlRESResponsibilityAlreadyDeletedException;
import com.ejb.exception.GlRESResponsibilityAlreadyExistException;
import com.ejb.gl.LocalGlOrganization;
import com.ejb.gl.LocalGlOrganizationHome;
import com.ejb.gl.LocalGlResponsibility;
import com.ejb.gl.LocalGlResponsibilityHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlOrganizationDetails;
import com.util.GlResponsibilityDetails;

/**
 * @ejb:bean name="GlResponsibilityControllerEJB"
 *           display-name="Used for maintenance of responsibility and roles"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlResponsibilityControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlResponsibilityController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlResponsibilityControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlResponsibilityControllerBean extends AbstractSessionBean {

   /*******************************************************************
      Business methods

      (1) getGlResByOrgName - returns an Arraylist of all Responsibilities 

      (2) getGlOrgDescriptionByOrgName - returns the description of an Organization

      (3) getGlOrgAll - returns an ArrayList of all Organizations

      (4) addGlResEntry - adds a new row in GL_RSPNSBLTY

      (5) updateResEntry - updates an existing row int GL_RSPNSBLTY

      (6) deleteResEntry - delete a row in GL_RSPNSBLTY

   *******************************************************************/
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlResByOrgName(String ORG_NM, Integer AD_CMPNY)
      throws GlRESNoResponsibilityFoundException,
      GlORGNoOrganizationFoundException{

      Debug.print("GlResponsibilityController getGlResByOrgName");

      /***************************************************************
    getGlResAll()
      ***************************************************************/
      
      ArrayList resAllList = new ArrayList();
      Collection glResponsibilities = null;
      LocalGlOrganization glOrg = null;
      LocalGlOrganizationHome glOrganizationHome = null;

      System.out.println("ORG_NM=" + ORG_NM);
	    // Initialize EJB Home
	  try {
	       glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);
	  } catch (NamingException ex) {
	      throw new EJBException(ex.getMessage());
	        
	  }
      try{
         glOrg = glOrganizationHome.findByOrgName(ORG_NM, AD_CMPNY);
      }catch(FinderException ex){
          throw new GlORGNoOrganizationFoundException();
      }catch(Exception ex){
          throw new EJBException(ex.getMessage());
      }
      glResponsibilities = glOrg.getGlResponsibilities();
      if (glResponsibilities.size() == 0){
    	  Debug.print("Checkpoint C");
         throw new GlRESNoResponsibilityFoundException();
      }
      Iterator i = glResponsibilities.iterator();
      while (i.hasNext()){
         LocalGlResponsibility glResponsibility = (LocalGlResponsibility) i.next();
         GlResponsibilityDetails details = new GlResponsibilityDetails(
            glResponsibility.getResCode(), glResponsibility.getResEnable());
         resAllList.add(details);
      }

      return resAllList;
   }
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public GlOrganizationDetails getGlOrgDescriptionByOrgName(String ORG_NM, Integer AD_CMPNY)
      throws GlORGNoOrganizationFoundException{

	   Debug.print("GlResponsibilityController getOrgDescriptionByOrgName");
	      
	   /*********************************************
	
	   getOrgDescriptionByOrgName
	
	   *********************************************/
	
	   LocalGlOrganization glOrg = null;
	   LocalGlOrganizationHome glOrganizationHome = null;
	
	   // Initialize EJB Home
	
	   try {
	    
	      glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
	         lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);
	    
	   } catch (NamingException ex) {
	    
	       throw new EJBException(ex.getMessage());
	    
	   }
	
	   try{
	      glOrg = glOrganizationHome.findByOrgName(ORG_NM, AD_CMPNY);
	   }catch(FinderException ex){
	      throw new GlORGNoOrganizationFoundException();
	   }catch(Exception ex){
	      throw new EJBException(ex.getMessage());
	   }
	
	   GlOrganizationDetails details = new GlOrganizationDetails(glOrg.getOrgName(), 
	      glOrg.getOrgDescription(), glOrg.getOrgMasterCode());
	
	   return details;
   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlOrgAll(Integer AD_CMPNY) 
      throws GlORGNoOrganizationFoundException{

   Debug.print("GlResponsibilityController getGlOrgAll");

   /****************************************************
      getGlOrgAll()
   ****************************************************/

   ArrayList orgList = new ArrayList();
   Collection glOrganizations = null;
   LocalGlOrganizationHome glOrganizationHome = null;

   // Initialize EJB Home

   try {
    
      glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
         lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);
    
   } catch (NamingException ex) {
    
       throw new EJBException(ex.getMessage());
    
   }
   
   try{
      glOrganizations = glOrganizationHome.findOrgAll(AD_CMPNY);
   }catch(FinderException ex){
      throw new EJBException(ex.getMessage());
   }catch(Exception ex){
      throw new EJBException(ex.getMessage());
   }

   if (glOrganizations.size() == 0)
      throw new GlORGNoOrganizationFoundException();
   
   Iterator i = glOrganizations.iterator();

   while(i.hasNext()){
      LocalGlOrganization glOrg = (LocalGlOrganization) i.next();

      GlOrganizationDetails details = new GlOrganizationDetails (glOrg.getOrgName(), glOrg.getOrgDescription(),
         glOrg.getOrgMasterCode());

      orgList.add(details);
   }

   return orgList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void addGlResEntry(GlResponsibilityDetails details, String RES_ORG_NM, Integer AD_CMPNY)
      throws GlRESResponsibilityAlreadyExistException,
      GlORGNoOrganizationFoundException{
      
      Debug.print("GlResponsibilityControllerBean addGlResEntry");
      
      /*********************************************************

      addGlResEntry

      *********************************************************/

      LocalGlOrganization glOrg = null;
      LocalGlOrganizationHome glOrganizationHome = null;
      LocalGlResponsibilityHome glResponsibilityHome = null;
      
	    // Initialize EJB Home
	    
	  try {
	        
	       glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);
	       glResponsibilityHome = (LocalGlResponsibilityHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlResponsibilityHome.JNDI_NAME, LocalGlResponsibilityHome.class);
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	       
      try{
         glOrg = glOrganizationHome.findByOrgName(RES_ORG_NM, AD_CMPNY);
      }catch(FinderException ex){
         throw new GlORGNoOrganizationFoundException();
      }catch(Exception ex){
         throw new EJBException(ex.getMessage());
      }
      
      try{
         LocalGlResponsibility glRes = glResponsibilityHome.create(details.getResCode(), 
            details.getResEnable(), AD_CMPNY);

	 glOrg.addGlResponsibility(glRes);   
      }catch(CreateException ex){
         throw new GlRESResponsibilityAlreadyExistException();
      }catch(Exception ex){
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }
   }


   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlResEntry(GlResponsibilityDetails details, String RES_ORG_NM, Integer AD_CMPNY)
      throws GlORGNoOrganizationFoundException,
      GlRESResponsibilityAlreadyDeletedException{

      Debug.print("GlResponsibilityControllerBean updateGlResEntry");

      /*********************************************************

      updateGlResEntry

      *********************************************************/

      LocalGlOrganization glOrg = null;
      LocalGlResponsibility glRes = null;
      LocalGlOrganizationHome glOrganizationHome = null;
      LocalGlResponsibilityHome glResponsibilityHome = null;
      
	    // Initialize EJB Home
	    
	  try {
	        
	       glOrganizationHome = (LocalGlOrganizationHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlOrganizationHome.JNDI_NAME, LocalGlOrganizationHome.class);
	       glResponsibilityHome = (LocalGlResponsibilityHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlResponsibilityHome.JNDI_NAME, LocalGlResponsibilityHome.class);
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
      
      try{
         glOrg = glOrganizationHome.findByOrgName(RES_ORG_NM, AD_CMPNY);
      }catch(FinderException ex){
         throw new GlORGNoOrganizationFoundException();
      }catch(Exception ex){
         throw new EJBException(ex.getMessage());
      }

      try{
         glRes = glResponsibilityHome.findByPrimaryKey(details.getResCode());
      }catch(FinderException ex){
         throw new GlRESResponsibilityAlreadyDeletedException();
      }catch(Exception ex){
         throw new EJBException(ex.getMessage());
      }
      
      try{
         glRes.setResEnable(details.getResEnable());
         glOrg.addGlResponsibility(glRes);
      }catch(Exception ex){
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }
      
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteGlResEntry(Integer RES_CODE, Integer AD_CMPNY)   
      throws GlRESResponsibilityAlreadyDeletedException{

      
      Debug.print("GlResponsibilityControllerBean updateGlResEntry");

      /*********************************************************

      deleteGlResEntry

      *********************************************************/

      LocalGlResponsibility glRes = null;
      LocalGlResponsibilityHome glResponsibilityHome = null;
      
	    // Initialize EJB Home
	    
	  try {
	        
	       glResponsibilityHome = (LocalGlResponsibilityHome)EJBHomeFactory.
	           lookUpLocalHome(LocalGlResponsibilityHome.JNDI_NAME, LocalGlResponsibilityHome.class);
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try{
         glRes = glResponsibilityHome.findByPrimaryKey(RES_CODE);
      }catch(FinderException ex){
         throw new GlRESResponsibilityAlreadyDeletedException();
      }catch(Exception ex){
         throw new EJBException(ex.getMessage());
      }

      try{
         glRes.remove();
      }catch(RemoveException ex){
         getSessionContext().setRollbackOnly();   
         throw new EJBException(ex.getMessage());
      }catch(Exception ex){         
         getSessionContext().setRollbackOnly();   
         throw new EJBException(ex.getMessage());
      }
   }
   // Session Methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() 
      throws CreateException {
      
      Debug.print("GlResponsibilityControllerBean ejbCreate");

   }
}
