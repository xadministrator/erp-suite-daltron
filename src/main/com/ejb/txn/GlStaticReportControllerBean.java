
/*
 * GlStaticReportControllerBean.java
 *
 * Created on June 17, 2003, 10:31 AM
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.gl.LocalGlStaticReport;
import com.ejb.gl.LocalGlStaticReportHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.util.AbstractSessionBean;
import com.util.GlStaticReportDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="GlStaticReportControllerEJB"
 *           display-name="Used for entering static reports"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlStaticReportControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlStaticReportController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlStaticReportControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class GlStaticReportControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getGlSrAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlStaticReportControllerBean getGlSrAll");

        LocalGlStaticReportHome glStaticReportHome = null;

        Collection glStaticReports = null;

        LocalGlStaticReport glStaticReport = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            glStaticReportHome = (LocalGlStaticReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlStaticReportHome.JNDI_NAME, LocalGlStaticReportHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
            glStaticReports = glStaticReportHome.findSrAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }

        if (glStaticReports.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        } else {
        
        	Iterator i = glStaticReports.iterator();
        	
        	while (i.hasNext()) {
        		
        		glStaticReport = (LocalGlStaticReport)i.next();
        		
        		GlStaticReportDetails details = new GlStaticReportDetails();
        		details.setSrCode(glStaticReport.getSrCode());        		
        		details.setSrName(glStaticReport.getSrName());
        		details.setSrDescription(glStaticReport.getSrDescription());
        		details.setSrFileName(glStaticReport.getSrFileName());                
        		details.setSrDateFrom(glStaticReport.getSrDateFrom());
        		details.setSrDateTo(glStaticReport.getSrDateTo());
        		
        		list.add(details);
        	}
        }
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addGlSrEntry(com.util.GlStaticReportDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("GlStaticReportControllerBean addGlSrEntry");
        
        LocalGlStaticReportHome glStaticReportHome = null;
               
        LocalGlStaticReport glStaticReport = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            glStaticReportHome = (LocalGlStaticReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlStaticReportHome.JNDI_NAME, LocalGlStaticReportHome.class);
                                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try { 
            
            glStaticReport = glStaticReportHome.findBySrName(details.getSrName(), AD_CMPNY);
                       
            throw new GlobalRecordAlreadyExistException();
                                             
         } catch (GlobalRecordAlreadyExistException ex) {
        	
        	throw ex;
        	
         } catch (FinderException ex) {
        	 	
         } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
         }
                            
        try {
        	
        	// create new responsibility

        	glStaticReport = glStaticReportHome.create(details.getSrName(), details.getSrDescription(),
        			details.getSrFileName(), details.getSrDateFrom(), details.getSrDateTo(), AD_CMPNY); 

        } catch (Exception ex) {
        	
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateGlSrEntry(com.util.GlStaticReportDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException { 
                    
        Debug.print("GlStaticReportControllerBean updateGlSrEntry");
        
        LocalGlStaticReportHome glStaticReportHome = null;                
                               
        LocalGlStaticReport glStaticReport = null;        
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            glStaticReportHome = (LocalGlStaticReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlStaticReportHome.JNDI_NAME, LocalGlStaticReportHome.class);           

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalGlStaticReport arExistingResponsibility = glStaticReportHome.findBySrName(details.getSrName(), AD_CMPNY);
            
            if (!arExistingResponsibility.getSrCode().equals(details.getSrCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                        
        try {
        	        	
        	// find and update responsibility
        	
        	glStaticReport = glStaticReportHome.findByPrimaryKey(details.getSrCode());
        	    	
                glStaticReport.setSrName(details.getSrName());
                glStaticReport.setSrDescription(details.getSrDescription());
                glStaticReport.setSrDateFrom(details.getSrDateFrom());
                glStaticReport.setSrDateTo(details.getSrDateTo());
                         	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteGlSrEntry(Integer SR_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
               GlobalRecordAlreadyAssignedException {

      Debug.print("GlStaticReportControllerBean deleteGlSrEntry");

      LocalGlStaticReport glStaticReport = null;
      LocalGlStaticReportHome glStaticReportHome = null;

      // Initialize EJB Home
        
      try {

          glStaticReportHome = (LocalGlStaticReportHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlStaticReportHome.JNDI_NAME, LocalGlStaticReportHome.class);           
      
      } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

      }                
      
      try {
      	
         glStaticReport = glStaticReportHome.findByPrimaryKey(SR_CODE);
         
      } catch (FinderException ex) {
      	
         throw new GlobalRecordAlreadyDeletedException();
         
      } catch (Exception ex) {
      	
         throw new EJBException(ex.getMessage()); 
         
      }
      
      try {

          if (!glStaticReport.getGlUserStaticReports().isEmpty()) {
      
             throw new GlobalRecordAlreadyAssignedException();
             
          }
          
      } catch (GlobalRecordAlreadyAssignedException ex) {                        
         
          throw ex;
          
      } catch (Exception ex) {
       
            throw new EJBException(ex.getMessage());
           
      }                         
         
      try {
         	
	      glStaticReport.remove();
	    
	  } catch (RemoveException ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  } catch (Exception ex) {
	 	
	      getSessionContext().setRollbackOnly();
	    
	      throw new EJBException(ex.getMessage());
	    
	  }	      
      
   }    

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public GlStaticReportDetails getGlSrBySrCode(Integer SR_CODE) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("GlStaticReportControllerBean getGlSrAll");

        LocalGlStaticReportHome glStaticReportHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            glStaticReportHome = (LocalGlStaticReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlStaticReportHome.JNDI_NAME, LocalGlStaticReportHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        GlStaticReportDetails details = new GlStaticReportDetails();
        
        try {
            
        	LocalGlStaticReport glStaticReport = glStaticReportHome.findByPrimaryKey(SR_CODE);

        	details.setSrCode(glStaticReport.getSrCode());        		
        	details.setSrName(glStaticReport.getSrName());
        	details.setSrDescription(glStaticReport.getSrDescription());
        	details.setSrFileName(glStaticReport.getSrFileName());                
        	details.setSrDateFrom(glStaticReport.getSrDateFrom());
        	details.setSrDateTo(glStaticReport.getSrDateTo());

        } catch (FinderException ex) {
        	
        	throw new GlobalNoRecordFoundException();
        	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        return details;
            
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlStaticReportControllerBean ejbCreate");
      
    }
}
