package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.RemoveException;
import javax.naming.NamingException;

import com.ejb.exception.GlCOANoChartOfAccountFoundException;
import com.ejb.exception.GlJCNoJournalCategoryFoundException;
import com.ejb.exception.GlJSNoJournalSourceFoundException;
import com.ejb.exception.GlSANoSuspenseAccountFoundException;
import com.ejb.exception.GlSASourceCategoryCombinationAlreadyExistException;
import com.ejb.exception.GlSASuspenseAccountAlreadyDeletedException;
import com.ejb.exception.GlSASuspenseAccountAlreadyExistException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlChartOfAccountDetails;
import com.util.GlJournalCategoryDetails;
import com.util.GlJournalSourceDetails;
import com.util.GlModSuspenseAccountDetails;
import com.util.GlSuspenseAccountDetails;

/**
 * @ejb:bean name="GlSuspenseAccountControllerEJB"
 *           display-name="Used for maintenance of suspense accounts"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlSuspenseAccountControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlSuspenseAccountController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlSuspenseAccountControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlSuspenseAccountControllerBean extends AbstractSessionBean {


   /*******************************************************************
      Business methods

      (1) getJcAll - returns and Arraylist of all Journal Category
      
      (2) getGlJsAll - returns all journal source.
      
      (3) getGlCoaAll - returns all chart of account.
      
      (4) getGlSaAll - returns all suspense account.
      
      (5) deleteGlSaEntry - deletes an existing row in GL_SSPNS_ACCNT
      
      (6) addGlSaEntry - add a new row in GL_SSPNS_ACCNT
      
      (7) updateGlSaEntry - update an existing entry in GlSuspenseAccount.

   *******************************************************************/
   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlJcAll(Integer AD_CMPNY)
      throws GlJCNoJournalCategoryFoundException{

      Debug.print("GlJCNoJournalCategoryFoundException getJcAll");
      
      ArrayList jcAllList = new ArrayList();
      Collection glJournalCategories = null;
      LocalGlJournalCategoryHome glJournalCategoryHome = null;
      
	  // Initialize EJB Home
	    
	  try {
	        
	       glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);	           
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try{
         glJournalCategories = glJournalCategoryHome.findJcAll(AD_CMPNY);
      }catch(FinderException ex){
         throw new EJBException(ex.getMessage());
      }catch(Exception ex){
         throw new EJBException(ex.getMessage());
      }

      if (glJournalCategories.size() == 0)
         throw new GlJCNoJournalCategoryFoundException();

      Iterator i = glJournalCategories.iterator();

      while (i.hasNext()){
         LocalGlJournalCategory glJournalCategory = (LocalGlJournalCategory) i.next();

         GlJournalCategoryDetails details = new GlJournalCategoryDetails(
            glJournalCategory.getJcCode(), glJournalCategory.getJcName(),
            glJournalCategory.getJcDescription(), glJournalCategory.getJcReversalMethod());

         jcAllList.add(details);
      }

      return jcAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlJsAll(Integer AD_CMPNY)
      throws GlJSNoJournalSourceFoundException{

      Debug.print("GlSuspenseAccountControllerBean getJsAll");

      ArrayList jsAllList = new ArrayList();
      Collection glJournalSources = null;
      LocalGlJournalSourceHome glJournalSourceHome = null;
      
	  // Initialize EJB Home
	    
	  try {
	        
	       glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);	           
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try{
         glJournalSources = glJournalSourceHome.findJsAll(AD_CMPNY);
      }catch (FinderException ex){
         throw new EJBException(ex.getMessage());
      }catch (Exception ex){
         throw new EJBException(ex.getMessage());
      }

      if (glJournalSources.size() == 0)
         throw new GlJSNoJournalSourceFoundException();

      Iterator i = glJournalSources.iterator();

      while (i.hasNext()){
         LocalGlJournalSource glJournalSource = (LocalGlJournalSource) i.next();

         GlJournalSourceDetails details = new GlJournalSourceDetails(
            glJournalSource.getJsCode(), glJournalSource.getJsName(), 
            glJournalSource.getJsDescription(), glJournalSource.getJsFreezeJournal(),
            glJournalSource.getJsJournalApproval(), glJournalSource.getJsEffectiveDateRule());

         jsAllList.add(details);
      }
      return jsAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlCoaAll(Integer AD_CMPNY)
      throws GlCOANoChartOfAccountFoundException{

      Debug.print("GlSuspenseAccountControllerBean getCoaAll");

      ArrayList coaAllList = new ArrayList();
      Collection glChartOfAccounts = null;
      LocalGlChartOfAccountHome glChartOfAccountHome = null;
      
	  // Initialize EJB Home
	    
	  try {
	        
	       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);	           
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try{
         glChartOfAccounts = glChartOfAccountHome.findCoaAllEnabled(
            new Date(EJBCommon.getGcCurrentDateWoTime().getTime().getTime()), AD_CMPNY);
      }catch(FinderException ex){
         throw new EJBException(ex.getMessage());
      }catch(Exception ex){
         throw new EJBException(ex.getMessage());
      }

      if (glChartOfAccounts.size() == 0)
         throw new GlCOANoChartOfAccountFoundException();

      Iterator i = glChartOfAccounts.iterator();
      while(i.hasNext()){
         LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount) i.next();

         GlChartOfAccountDetails details = 
            new GlChartOfAccountDetails( glChartOfAccount.getCoaCode(),
               glChartOfAccount.getCoaAccountNumber(), glChartOfAccount.getCoaAccountNumber(), 
               glChartOfAccount.getCoaCitCategory(), glChartOfAccount.getCoaSawCategory(), glChartOfAccount.getCoaIitCategory(),
               glChartOfAccount.getCoaAccountType(), glChartOfAccount.getCoaTaxType(), 
               glChartOfAccount.getCoaDateFrom(), 
			   glChartOfAccount.getCoaDateTo(), glChartOfAccount.getCoaEnable(), EJBCommon.TRUE);

         coaAllList.add(details);
      }

      return coaAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlSaAll(Integer AD_CMPNY) 
      throws GlSANoSuspenseAccountFoundException{
      	
      Debug.print("GlSuspenseAccountControllerBean getGlSaAll");

      LocalGlSuspenseAccountHome glSuspenseAccountHome = null;

      Collection glSuspenseAccounts = null;
      
      ArrayList saAllList = new ArrayList();
      
	  // Initialize EJB Home
	    
	  try {
	        
	       glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
	       
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try{
         glSuspenseAccounts = glSuspenseAccountHome.findSaAll(AD_CMPNY);
      }catch(FinderException ex){
         throw new EJBException(ex.getMessage());
      }catch(Exception ex){
         throw new EJBException(ex.getMessage());
      }

      if (glSuspenseAccounts.size() == 0)
         throw new GlSANoSuspenseAccountFoundException();

      Iterator i = glSuspenseAccounts.iterator();
      while(i.hasNext()){
      
        try {
        
	        LocalGlSuspenseAccount glSuspenseAccount = (LocalGlSuspenseAccount) i.next();
	        
	        GlModSuspenseAccountDetails details = 
	            new GlModSuspenseAccountDetails(glSuspenseAccount.getSaCode(),
	               glSuspenseAccount.getSaName(), glSuspenseAccount.getSaDescription(),
	               glSuspenseAccount.getGlJournalCategory().getJcName(),
	               glSuspenseAccount.getGlJournalSource().getJsName(),
	               glSuspenseAccount.getGlChartOfAccount().getCoaAccountNumber(),
	               glSuspenseAccount.getGlChartOfAccount().getCoaAccountDescription());
	
	         saAllList.add(details);
	         
	     } catch (Exception ex) {
			
		    throw new EJBException(ex.getMessage());
		
		 }
      }

      return saAllList;
   }
   
   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void deleteGlSaEntry(Integer SA_CODE, Integer AD_CMPNY)
      throws GlSASuspenseAccountAlreadyDeletedException{

      Debug.print("GlSuspenseAccountControllerBead deleteGlSaEntry");

      LocalGlSuspenseAccount glSuspenseAccount = null;
      LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
      
	  // Initialize EJB Home
	    
	  try {
	        
	       glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);	           
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try{
         glSuspenseAccount = glSuspenseAccountHome.findByPrimaryKey(SA_CODE);
      }catch(FinderException ex){
         throw new GlSASuspenseAccountAlreadyDeletedException();
      }catch(Exception ex){
         throw new EJBException(ex.getMessage());
      }

      try{
         glSuspenseAccount.remove();
      }catch(RemoveException ex){
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }catch(Exception ex){
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void addGlSaEntry(GlSuspenseAccountDetails details, String SA_JC_NM, 
      String SA_JS_NM, String SA_COA_ACCNT_NMBR, Integer AD_CMPNY)
      throws GlSASuspenseAccountAlreadyExistException,
                GlJSNoJournalSourceFoundException, GlJCNoJournalCategoryFoundException,
      GlCOANoChartOfAccountFoundException, GlSASourceCategoryCombinationAlreadyExistException{

      LocalGlJournalCategory glJournalCategory = null;
      LocalGlJournalSource glJournalSource = null;
      LocalGlChartOfAccount glChartOfAccount = null;

      Collection glSuspenseAccounts = null;
      
      Debug.print("GlSuspenseAccountControllerBean addGlSaEntry");
      
      LocalGlJournalCategoryHome glJournalCategoryHome = null; 
      LocalGlJournalSourceHome glJournalSourceHome = null; 
      LocalGlChartOfAccountHome glChartOfAccountHome = null; 
      LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
                 
	  // Initialize EJB Home
	    
	  try {

	       glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);	
	       glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);	           
	       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);	                  
	       glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);	           
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }


      /************************************
         Adds an SA Entry
      ************************************/

      try {
         glJournalCategory = glJournalCategoryHome.findByJcName(SA_JC_NM, AD_CMPNY);
      }catch(FinderException ex){
         throw new GlJCNoJournalCategoryFoundException();
      }catch(Exception ex){
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

      try{
         glJournalSource = glJournalSourceHome.findByJsName(SA_JS_NM, AD_CMPNY);
      }catch(FinderException ex){
         ex.printStackTrace();   
         throw new GlJSNoJournalSourceFoundException();
      }catch(Exception ex){
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

      try{
         glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(SA_COA_ACCNT_NMBR, AD_CMPNY);
      }catch(FinderException ex){
         throw new GlCOANoChartOfAccountFoundException();
      }catch(Exception ex){
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

      try{
         glSuspenseAccounts = glSuspenseAccountHome.findSaAll(AD_CMPNY);
      }catch(FinderException ex){
         throw new EJBException(ex.getMessage());
      }catch(Exception ex){
         throw new EJBException(ex.getMessage());
      }
      
      try {
      	
      	 glSuspenseAccountHome.findBySaName(details.getSaName(), AD_CMPNY);
         
         getSessionContext().setRollbackOnly();
         throw new GlSASuspenseAccountAlreadyExistException();
      	
      } catch (FinderException ex) {
      	
      	      	
      }

      
      if (glSuspenseAccounts.size() == 0){
      
         try{
            LocalGlSuspenseAccount glSuspenseAccount = glSuspenseAccountHome.create(
               details.getSaName(), details.getSaDescription(), AD_CMPNY);

            glJournalSource.addGlSuspenseAccount(glSuspenseAccount);
            glJournalCategory.addGlSuspenseAccount(glSuspenseAccount);
            glChartOfAccount.addGlSuspenseAccount(glSuspenseAccount);
         }catch (Exception ex){
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
         }
      
      } else {
         Iterator i = glSuspenseAccounts.iterator();
         while(i.hasNext()){
            LocalGlSuspenseAccount glSuspenseAccount = (LocalGlSuspenseAccount) i.next();

            String JC_NM = glSuspenseAccount.getGlJournalCategory().getJcName();
            String JS_NM = glSuspenseAccount.getGlJournalSource().getJsName();
         
            if( JC_NM.equals(SA_JC_NM) && JS_NM.equals( SA_JS_NM))
               throw new GlSASourceCategoryCombinationAlreadyExistException();                    
         }

         try{
            LocalGlSuspenseAccount glSuspenseAccount = glSuspenseAccountHome.create(
               details.getSaName(), details.getSaDescription(), AD_CMPNY);
         
            glJournalCategory.addGlSuspenseAccount(glSuspenseAccount);
            glJournalSource.addGlSuspenseAccount(glSuspenseAccount);
            glChartOfAccount.addGlSuspenseAccount(glSuspenseAccount);
         }catch(Exception ex){
            getSessionContext().setRollbackOnly();
            throw new EJBException();
         }
      } 
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlSaEntry(GlSuspenseAccountDetails details, String SA_JC_NM,
           String SA_JS_NM, String SA_COA_ACCNT_NMBR, Integer AD_CMPNY)
           throws GlSASuspenseAccountAlreadyExistException, GlJSNoJournalSourceFoundException, 
      GlJCNoJournalCategoryFoundException, GlCOANoChartOfAccountFoundException, 
      GlSASourceCategoryCombinationAlreadyExistException, GlSASuspenseAccountAlreadyDeletedException{
      
      Debug.print("GlSuspenseAccountController updateGlSaEntry");
      
      LocalGlJournalCategory glJournalCategory = null;
      LocalGlJournalSource glJournalSource = null;
      LocalGlChartOfAccount glChartOfAccount = null;
      LocalGlSuspenseAccount glSuspenseAccount = null;
      LocalGlSuspenseAccount glSuspenseAccountByName = null;
      
      Collection glSuspenseAccounts = null;

      LocalGlJournalCategoryHome glJournalCategoryHome = null; 
      LocalGlJournalSourceHome glJournalSourceHome = null; 
      LocalGlChartOfAccountHome glChartOfAccountHome = null; 
      LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
                 
	  // Initialize EJB Home
	    
	  try {

	       glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);	
	       glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);	           
	       glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);	                  
	       glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);	           
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	  
      /** check if JC, JS, COA or SA exist */

      try{
         glJournalCategory = glJournalCategoryHome.findByJcName(SA_JC_NM, AD_CMPNY);
      }catch(FinderException ex){
         throw new GlJCNoJournalCategoryFoundException();
      }catch(Exception ex){
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

      try{
         glJournalSource = glJournalSourceHome.findByJsName(SA_JS_NM, AD_CMPNY);
      }catch(FinderException ex){
         throw new GlJSNoJournalSourceFoundException();
      }catch(Exception ex){
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

      try{
         glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumber(SA_COA_ACCNT_NMBR, AD_CMPNY);
      }catch(FinderException ex){
         throw new GlCOANoChartOfAccountFoundException();
      }catch(Exception ex){
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

      try{
         glSuspenseAccounts = glSuspenseAccountHome.findSaAll(AD_CMPNY);
      }catch(FinderException ex){
         throw new GlSASuspenseAccountAlreadyDeletedException();
      }catch(Exception ex){
         throw new EJBException(ex.getMessage());
      }

      
      /** check combination if already exist */
      
      Iterator i = glSuspenseAccounts.iterator();
      while(i.hasNext()){
         LocalGlSuspenseAccount glSuspenseAccount2 = (LocalGlSuspenseAccount) i.next();
         
         String JC_NM = glSuspenseAccount2.getGlJournalCategory().getJcName();
         String JS_NM = glSuspenseAccount2.getGlJournalSource().getJsName();
         Integer SA_CODE = glSuspenseAccount2.getSaCode();
         boolean isCombinationExist = false;

         Debug.print(JC_NM + ", " + JS_NM + ", " + SA_CODE + " :: " + SA_JC_NM + ", " + 
                 SA_JS_NM + ", " + details.getSaCode());
         
         isCombinationExist = JC_NM.equals(SA_JC_NM) && JS_NM.equals(SA_JS_NM);               
         
         if (isCombinationExist){ 
            if (!SA_CODE.equals(details.getSaCode()))                                                 
               throw new GlSASourceCategoryCombinationAlreadyExistException();
         }
         

         if(glSuspenseAccount2.getSaName().equals(details.getSaName()) &&
            !glSuspenseAccount2.getSaCode().equals(details.getSaCode()))
            throw new GlSASuspenseAccountAlreadyExistException();
            
      }

      try{
         glSuspenseAccount =
            glSuspenseAccountHome.findByPrimaryKey(details.getSaCode());
      }catch(FinderException ex){
         throw new GlSASuspenseAccountAlreadyDeletedException();
      }catch(Exception ex){
         throw new EJBException(ex.getMessage());
      }                                                       
 
      try{
         glSuspenseAccount.setSaName(details.getSaName());
         glSuspenseAccount.setSaDescription(details.getSaDescription());
         
         glJournalCategory.addGlSuspenseAccount(glSuspenseAccount);
         glJournalSource.addGlSuspenseAccount(glSuspenseAccount);
         glChartOfAccount.addGlSuspenseAccount(glSuspenseAccount);
      }catch(Exception ex){
         throw new EJBException(ex.getMessage()) ; 
      }
   }

   // Session Methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() 
      throws CreateException {
      
      Debug.print("GlSuspenseAccountControllerBean ejbCreate");
      
   }
}
