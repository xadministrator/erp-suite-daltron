 package com.ejb.txn;
 
 import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlTaxInterface;
import com.ejb.gl.LocalGlTaxInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModTaxInterfaceDetails;
 
 /**
  * @ejb:bean name="GlTaxInterfaceMaintenanceControllerEJB"
  *           display-name="used for editing tax entry"
  *           type="Stateless"
  *           view-type="remote"
  *           jndi-name="ejb/GlTaxInterfaceMaintenanceControllerEJB"
  *
  * @ejb:interface remote-class="com.ejb.txn.GlTaxInterfaceMaintenanceController"
  *                extends="javax.ejb.EJBObject"
  *
  * @ejb:home remote-class="com.ejb.txn.GlTaxInterfaceMaintenanceControllerHome"
  *           extends="javax.ejb.EJBHome"
  *
  * @ejb:transaction type="Required"
  *
  * @ejb:security-role-ref role-name="gluser"
  *                        role-link="gluserlink"
  *
  * @ejb:permission role-name="gluser"
  * 
  */
 
 public class GlTaxInterfaceMaintenanceControllerBean extends AbstractSessionBean {
 	
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getArCstmrAll(Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getArCstmrAll");
 		
 		LocalArCustomerHome arCustomerHome = null;
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			Collection arCustomers = arCustomerHome.findEnabledCstAll(new Integer(1), AD_CMPNY);
 			
 			Iterator i = arCustomers.iterator();
 			
 			while (i.hasNext()) {
 				
 				LocalArCustomer arCustomer = (LocalArCustomer)i.next();
 				
 				list.add(arCustomer.getCstCustomerCode());
 				
 			}
 			
 			return list;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getArTcAll(Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getArTcAll");
 		
 		LocalArTaxCodeHome arTaxCodeHome = null;
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			Collection arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);
 			
 			Iterator i = arTaxCodes.iterator();
 			
 			while (i.hasNext()) {
 				
 				LocalArTaxCode arTaxCode = (LocalArTaxCode)i.next();
 				LocalGlChartOfAccount glChartOfAccount = arTaxCode.getGlChartOfAccount() ;
 				
 				if(glChartOfAccount != null) {
 					
 					list.add(arTaxCode.getTcName());
 					
 				}
 				
 			}
 			
 			return list;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public double getArTcRate(String TC_NM, Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getArTcRate");
 		
 		LocalArTaxCodeHome arTaxCodeHome = null;
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
 			
 			return arTaxCode.getTcRate();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getArWtcAll(Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getArWtcAll");
 		
 		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			Collection arWithholdingTaxCodes = arWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);
 			
 			Iterator i = arWithholdingTaxCodes.iterator();
 			
 			while (i.hasNext()) {
 				
 				LocalArWithholdingTaxCode arWithholdingTaxCode = (LocalArWithholdingTaxCode)i.next();
 				LocalGlChartOfAccount glChartOfAccount = arWithholdingTaxCode.getGlChartOfAccount() ;
 				
 				if(glChartOfAccount != null) {
 					
 					list.add(arWithholdingTaxCode.getWtcName());
 					
 				}
 				
 			}
 			
 			return list;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public double getArWtcRate(String WTC_NM, Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getArWtcRate");
 		
 		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
 			
 			return arWithholdingTaxCode.getWtcRate();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getApSpplrAll(Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getApSpplrAll");
 		
 		LocalApSupplierHome apSupplierHome = null;
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			Collection apSuppliers = apSupplierHome.findEnabledSplAll(new Integer(1), AD_CMPNY);
 			
 			Iterator i = apSuppliers.iterator();
 			
 			while (i.hasNext()) {
 				
 				LocalApSupplier apSupplier = (LocalApSupplier)i.next();
 				
 				list.add(apSupplier.getSplSupplierCode());
 				
 			}
 			
 			return list;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getApTcAll(Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getApTcAll");
 		
 		LocalApTaxCodeHome apTaxCodeHome = null;
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			Collection apTaxCodes = apTaxCodeHome.findEnabledTcAll(AD_CMPNY);
 			
 			Iterator i = apTaxCodes.iterator();
 			
 			while (i.hasNext()) {
 				
 				LocalApTaxCode apTaxCode = (LocalApTaxCode)i.next();
 				LocalGlChartOfAccount glChartOfAccount = apTaxCode.getGlChartOfAccount() ;
 				
 				if(glChartOfAccount != null) {
 					
 					list.add(apTaxCode.getTcName());
 					
 				}
 				
 			}
 			
 			return list;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public double getApTcRate(String TC_NM, Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getApTcRate");
 		
 		LocalApTaxCodeHome apTaxCodeHome = null;
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
 			
 			return apTaxCode.getTcRate();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getApWtcAll(Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getApWtcAll");
 		
 		LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			Collection arWithholdingTaxCodes = apWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);
 			
 			Iterator i = arWithholdingTaxCodes.iterator();
 			
 			while (i.hasNext()) {
 				
 				LocalApWithholdingTaxCode apWithholdingTaxCode = (LocalApWithholdingTaxCode)i.next();
 				LocalGlChartOfAccount glChartOfAccount = apWithholdingTaxCode.getGlChartOfAccount() ;
 				
 				if(glChartOfAccount != null) {
 					
 					list.add(apWithholdingTaxCode.getWtcName());
 					
 				}
 				
 			}
 			
 			return list;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public double getApWtcRate(String WTC_NM, Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getApWtcRate");
 		
 		LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
 		
 		ArrayList list = new ArrayList();
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			LocalApWithholdingTaxCode apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(WTC_NM, AD_CMPNY);
 			
 			return apWithholdingTaxCode.getWtcRate();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public ArrayList getGlTiByCriteria(HashMap criteria, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
 		throws GlobalNoRecordFoundException {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getGlTiByCriteria");
 		
 		LocalGlTaxInterfaceHome glTaxInterfaceHome = null;
 		LocalGlChartOfAccountHome glChartOfAccountHome = null;
 		LocalArTaxCodeHome arTaxCodeHome = null;
 		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
 		LocalApTaxCodeHome apTaxCodeHome = null;
 		LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
 		LocalArCustomerHome arCustomerHome = null;
 		LocalApSupplierHome apSupplierHome = null;
 		LocalArDistributionRecordHome arDistributionRecordHome = null;
 		LocalApDistributionRecordHome apDistributionRecordHome = null;
 		LocalGlJournalLineHome glJournalLineHome = null;
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);          
 			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);  
 			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try { 
 			
 			ArrayList tiList = new ArrayList();
 			
 			StringBuffer jbossQl = new StringBuffer();
 			jbossQl.append("SELECT OBJECT(ti) FROM GlTaxInterface ti ");
 			
 			boolean firstArgument = true;
 			short ctr = 0;
 			int criteriaSize = criteria.size() + 2;
 			Object obj[] = new Object[criteriaSize];
 			
 			if (criteria.containsKey("documentType")) {
 				
 				firstArgument = false;
 				
 				jbossQl.append("WHERE ti.tiDocumentType=?" + (ctr+1) + " ");
 				obj[ctr] = (String)criteria.get("documentType");
 				ctr++;
 				
 			}
 			
 			if (criteria.containsKey("docNumFrom")) {
 				
 				if (!firstArgument) {
 					jbossQl.append("AND ");
 					
 				} else {
 					
 					firstArgument = false;
 					jbossQl.append("WHERE ");
 					
 				}
 				
 				jbossQl.append("ti.tiTxnDocumentNumber>=?" + (ctr+1) + " ");
 				obj[ctr] = (String)criteria.get("docNumFrom");
 				ctr++;
 			}  
 			
 			if (criteria.containsKey("docNumTo")) {
 				
 				if (!firstArgument) {
 					
 					jbossQl.append("AND ");
 					
 				} else {
 					
 					firstArgument = false;
 					jbossQl.append("WHERE ");
 					
 				}
 				
 				jbossQl.append("ti.tiTxnDocumentNumber<=?" + (ctr+1) + " ");
 				obj[ctr] = (String)criteria.get("docNumTo");
 				ctr++;
 				
 			}    
 			
 			if (criteria.containsKey("refNumFrom")) {
 				
 				if (!firstArgument) {
 					jbossQl.append("AND ");
 					
 				} else {
 					
 					firstArgument = false;
 					jbossQl.append("WHERE ");
 					
 				}
 				
 				jbossQl.append("ti.tiTxnReferenceNumber>=?" + (ctr+1) + " ");
 				obj[ctr] = (String)criteria.get("refNumFrom");
 				ctr++;
 			}  
 			
 			if (criteria.containsKey("refNumTo")) {
 				
 				if (!firstArgument) {
 					
 					jbossQl.append("AND ");
 					
 				} else {
 					
 					firstArgument = false;
 					jbossQl.append("WHERE ");
 					
 				}
 				
 				jbossQl.append("ti.tiTxnReferenceNumber<=?" + (ctr+1) + " ");
 				obj[ctr] = (String)criteria.get("refNumTo");
 				ctr++;
 				
 			}	  	       
 			
 			if (criteria.containsKey("subledgerFrom")) {
 				
 				if (!firstArgument) {
 					jbossQl.append("AND ");
 					
 				} else {
 					
 					firstArgument = false;
 					jbossQl.append("WHERE ");
 					
 				}
 				
 				jbossQl.append("ti.tiSlSubledegrCode>=?" + (ctr+1) + " ");
 				obj[ctr] = (String)criteria.get("subledgerFrom");
 				ctr++;
 			}  
 			
 			if (criteria.containsKey("subledgerTo")) {
 				
 				if (!firstArgument) {
 					
 					jbossQl.append("AND ");
 					
 				} else {
 					
 					firstArgument = false;
 					jbossQl.append("WHERE ");
 					
 				}
 				
 				jbossQl.append("ti.tiSlSubledegrCode<=?" + (ctr+1) + " ");
 				obj[ctr] = (String)criteria.get("subledgerTo");
 				ctr++;
 				
 			}
 			
 			if (!firstArgument) {
 				
 				jbossQl.append("AND ");
 				
 			} else {
 				
 				firstArgument = false;
 				jbossQl.append("WHERE ");
 				
 			}
 			
 			jbossQl.append("ti.tiAdBranch=" + AD_BRNCH + " AND ti.tiAdCompany=" + AD_CMPNY + " ");
 			
 			
 			jbossQl.append(" OFFSET ?" + (ctr + 1));
 			obj[ctr] = OFFSET;
 			ctr++;
 			
 			jbossQl.append(" LIMIT ?" + (ctr + 1));
 			obj[ctr] = LIMIT;
 			
 			Collection glTaxInterfaces = null;
 			
 			try {
 				
 				glTaxInterfaces = glTaxInterfaceHome.getTiByCriteria(jbossQl.toString(), obj);
 				
 			} catch (Exception ex) {
 				
 				throw new EJBException(ex.getMessage());
 				
 			}
 			
 			if (glTaxInterfaces.isEmpty())
 				throw new GlobalNoRecordFoundException();
 			
 			Iterator i = glTaxInterfaces.iterator();
 			
 			while (i.hasNext()) {
 				
 				LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface) i.next();
 				
 				String docType = glTaxInterface.getTiDocumentType();
 				
 				GlModTaxInterfaceDetails mdetails = new GlModTaxInterfaceDetails();
 				
 				mdetails.setTiCode(glTaxInterface.getTiCode());
 				mdetails.setTiDocumentType(docType);
 				mdetails.setTiTxnCode(glTaxInterface.getTiTxnCode());
 				mdetails.setTiTxnDate(glTaxInterface.getTiTxnDate());
 				mdetails.setTiTxnDocumentNumber(glTaxInterface.getTiTxnDocumentNumber());
 				mdetails.setTiTxnReferenceNumber(glTaxInterface.getTiTxnReferenceNumber());
 				mdetails.setTiTxlCode(glTaxInterface.getTiTxlCode());
 				
 				// coa
 				LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(glTaxInterface.getTiTxlCoaCode());
 				mdetails.setTiTxlCoaNumber(glChartOfAccount.getCoaAccountNumber());
 				mdetails.setTiTxlCoaCode(glTaxInterface.getTiTxlCoaCode());
 				
 				double TC_AMNT = 0d;
 				double WTC_AMNT = 0d;
 				double NET_AMNT = glTaxInterface.getTiNetAmount();
 				
 				// tax & withholding tax
 				// ar records
 				if(docType.equals("AR INVOICE") || docType.equals("AR CREDIT MEMO")
 						|| docType.equals("AR RECEIPT")){
 					
 					mdetails.setTiIsArDocument(EJBCommon.TRUE);
 					mdetails.setTiEditGlDocument(EJBCommon.FALSE);
 					
 					if(glTaxInterface.getTiTcCode() != null){
 						
 						LocalArTaxCode arTaxCode = arTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiTcCode());
 						LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.findByPrimaryKey(glTaxInterface.getTiTxlCode()); 						
 						
 						TC_AMNT = arDistributionRecord.getDrDebit() == EJBCommon.TRUE ? arDistributionRecord.getDrAmount() * -1 : arDistributionRecord.getDrAmount();
 						NET_AMNT = arDistributionRecord.getDrDebit() == EJBCommon.TRUE ? NET_AMNT * -1 : NET_AMNT;
 						
 						mdetails.setTiNetAmount(NET_AMNT);
 		 				mdetails.setTiTaxAmount(TC_AMNT);
 		 				mdetails.setTiTcName(arTaxCode.getTcName());
 						mdetails.setTiWtcName(null);
 	 					
 	 					
 					} else {
 					
 						LocalArWithholdingTaxCode arWithholdingTaxCode = 
 							arWithholdingTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiWtcCode());
 						LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.findByPrimaryKey(glTaxInterface.getTiTxlCode()); 						
 						
 						WTC_AMNT = arDistributionRecord.getDrDebit() == EJBCommon.TRUE ? arDistributionRecord.getDrAmount() : arDistributionRecord.getDrAmount() * -1;
 						NET_AMNT = arDistributionRecord.getDrDebit() == EJBCommon.TRUE ? NET_AMNT : NET_AMNT * -1;
 						
 						mdetails.setTiNetAmount(NET_AMNT);
 		 				mdetails.setTiTaxAmount(WTC_AMNT);
 	 					mdetails.setTiTcName(null);
 	 					mdetails.setTiWtcName(arWithholdingTaxCode.getWtcName());
 						
 					}
 					
 					LocalArCustomer arCustomer = arCustomerHome.findByPrimaryKey(glTaxInterface.getTiSlCode());
 					mdetails.setTiSlCode(arCustomer.getCstCode());
 					mdetails.setTiSlSubledgerCode(arCustomer.getCstCustomerCode());
 				
 			    // ap records
 				} else if(docType.equals("AP VOUCHER") || docType.equals("AP DEBIT MEMO")
 						|| docType.equals("AP CHECK") || docType.equals("AP RECEIVING ITEM")){
 				
 					mdetails.setTiIsArDocument(EJBCommon.FALSE);
 					mdetails.setTiEditGlDocument(EJBCommon.FALSE);
 					
 					if(glTaxInterface.getTiTcCode() != null){
 						
 						LocalApTaxCode apTaxCode = apTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiTcCode());
 						LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.findByPrimaryKey(glTaxInterface.getTiTxlCode()); 						
 						
 						TC_AMNT = apDistributionRecord.getDrDebit() == EJBCommon.TRUE ? apDistributionRecord.getDrAmount() : apDistributionRecord.getDrAmount() * -1;
 						NET_AMNT = apDistributionRecord.getDrDebit() == EJBCommon.TRUE ? NET_AMNT : NET_AMNT * -1;
 						
 						mdetails.setTiNetAmount(NET_AMNT);
 		 				mdetails.setTiTaxAmount(TC_AMNT);
 		 				mdetails.setTiTcName(apTaxCode.getTcName());
 						mdetails.setTiWtcName(null);
 	 					
 					} else {
 					
 						LocalApWithholdingTaxCode apWithholdingTaxCode = 
 							apWithholdingTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiWtcCode());
 						LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.findByPrimaryKey(glTaxInterface.getTiTxlCode()); 						
 						
 						WTC_AMNT = apDistributionRecord.getDrDebit() == EJBCommon.TRUE ? apDistributionRecord.getDrAmount() * -1 : apDistributionRecord.getDrAmount();
 						NET_AMNT = apDistributionRecord.getDrDebit() == EJBCommon.TRUE ? NET_AMNT * -1 : NET_AMNT;
 						
 						mdetails.setTiNetAmount(NET_AMNT);
 		 				mdetails.setTiTaxAmount(WTC_AMNT);
 	 					mdetails.setTiTcName(null);
 	 					mdetails.setTiWtcName(apWithholdingTaxCode.getWtcName());
 						
 					}
 					
 					LocalApSupplier apSupplier = apSupplierHome.findByPrimaryKey(glTaxInterface.getTiSlCode());
 					mdetails.setTiSlCode(apSupplier.getSplCode());
 					mdetails.setTiSlSubledgerCode(apSupplier.getSplSupplierCode());
 					
 				// gl records
 				} else {
 					
 					LocalGlJournalLine glJournalLine = glJournalLineHome.findByPrimaryKey(glTaxInterface.getTiTxlCode()); 						
 					
 					TC_AMNT = glJournalLine.getJlDebit() == EJBCommon.TRUE ? glJournalLine.getJlAmount() * -1 : glJournalLine.getJlAmount();
 					NET_AMNT = glJournalLine.getJlDebit() == EJBCommon.TRUE ? NET_AMNT * -1 : NET_AMNT;
 					
 					mdetails.setTiTaxAmount(TC_AMNT);
 					mdetails.setTiNetAmount(NET_AMNT);
 					
 					// edited gl record
 					if(glTaxInterface.getTiTcCode()!= null || glTaxInterface.getTiWtcCode()!= null) {
 						
 						if(glTaxInterface.getTiIsArDocument() == EJBCommon.TRUE) {
 							
 							if(glTaxInterface.getTiTcCode() != null){
 								
 								LocalArTaxCode arTaxCode = arTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiTcCode());
 								
 								mdetails.setTiTcName(arTaxCode.getTcName());
 								mdetails.setTiWtcName(null);
 								
 								
 							} else {
 								
 								LocalArWithholdingTaxCode arWithholdingTaxCode = 
 									arWithholdingTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiWtcCode());
 								
 								mdetails.setTiTcName(null);
 								mdetails.setTiWtcName(arWithholdingTaxCode.getWtcName());
 								
 							}
 							
 							mdetails.setTiEditGlDocument(EJBCommon.FALSE);
 							mdetails.setTiIsArDocument(EJBCommon.TRUE);
 		 					
 							LocalArCustomer arCustomer = arCustomerHome.findByPrimaryKey(glTaxInterface.getTiSlCode());
 		 					mdetails.setTiSlCode(arCustomer.getCstCode());
 		 					mdetails.setTiSlSubledgerCode(arCustomer.getCstCustomerCode());
 							
 						} else {
 							
 							if(glTaxInterface.getTiTcCode() != null){
 								
 								LocalApTaxCode apTaxCode = apTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiTcCode());
 								
 								mdetails.setTiTcName(apTaxCode.getTcName());
 								mdetails.setTiWtcName(null);
 								
 								
 							} else {
 								
 								LocalApWithholdingTaxCode apWithholdingTaxCode = 
 									apWithholdingTaxCodeHome.findByPrimaryKey(glTaxInterface.getTiWtcCode());
 								
 								mdetails.setTiTcName(null);
 								mdetails.setTiWtcName(apWithholdingTaxCode.getWtcName());
 								
 							}
 							
 							mdetails.setTiEditGlDocument(EJBCommon.FALSE);
 							mdetails.setTiIsArDocument(EJBCommon.FALSE);
 		 					
 							LocalApSupplier apSupplier = apSupplierHome.findByPrimaryKey(glTaxInterface.getTiSlCode());
 		 					mdetails.setTiSlCode(apSupplier.getSplCode());
 		 					mdetails.setTiSlSubledgerCode(apSupplier.getSplSupplierCode());
 							
 						}
 						
 						
 					} else {
 						
 						mdetails.setTiEditGlDocument(EJBCommon.TRUE);
 						mdetails.setTiIsArDocument(EJBCommon.FALSE);
		 					
		 				mdetails.setTiTcName(null);
 		 				mdetails.setTiWtcName(null);
 		 				mdetails.setTiSlCode(null);
 		 				mdetails.setTiSlSubledgerCode(null);
 		 				
 					}
 					
 				}
 				
 				tiList.add(mdetails);
 				
 			}
 			
 			return tiList;
 			
 		} catch (GlobalNoRecordFoundException ex) {
 			
 			throw ex;
 			
 		} catch (Exception ex) {
 			
 			
 			ex.printStackTrace();
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 **/
 	public void saveGlTiMaintenance(GlModTaxInterfaceDetails mdetails, Integer AD_BRNCH, Integer AD_CMPNY)
 		throws GlobalRecordAlreadyDeletedException {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean saveGlTiMaintenance");
 		
 		LocalGlTaxInterfaceHome glTaxInterfaceHome = null;
 		LocalGlJournalLineHome glJournalLineHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		LocalArTaxCodeHome arTaxCodeHome = null;
 		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
 		LocalApTaxCodeHome apTaxCodeHome = null;
 		LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
 		LocalArCustomerHome arCustomerHome = null;
 		LocalApSupplierHome apSupplierHome = null;
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);
 			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
 			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
 			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
 			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
 			apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
 			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
 			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			LocalGlTaxInterface glTaxInterface = null;
 			
 			// validate if tax interface already exist
 			
 			try {
 				
 				glTaxInterface = glTaxInterfaceHome.findByPrimaryKey(mdetails.getTiCode());
 				
 			} catch (FinderException ex) {
 				
 				throw new GlobalRecordAlreadyDeletedException();
 				
 			}
 			
 			if(mdetails.getTiDocumentType().equals("GL JOURNAL")) {
 				
 				LocalGlJournalLine glJournalLine = null;
 				
 				try {
 					
 					glJournalLine = glJournalLineHome.findByPrimaryKey(mdetails.getTiTxlCode());
 					
 				} catch (FinderException ex) {
 					
 					throw new GlobalRecordAlreadyDeletedException();
 					
 				}
 				
 				double DR_AMNT = this.convertForeignToFunctionalCurrency(
 						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcCode(),
						glJournalLine.getGlJournal().getGlFunctionalCurrency().getFcName(),
						glJournalLine.getGlJournal().getJrConversionDate(),
						glJournalLine.getGlJournal().getJrConversionRate(),
						glJournalLine.getJlAmount(), AD_CMPNY);
 				
 				if (mdetails.getTiIsArDocument() == EJBCommon.TRUE) {
 					
 					glTaxInterface.setTiIsArDocument(EJBCommon.TRUE);
 					glTaxInterface.setTiTxnReferenceNumber(mdetails.getTiTxnReferenceNumber());
 					
 					if (mdetails.getTiTcName()!= null) {
 						
 						LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(mdetails.getTiTcName(), AD_CMPNY);
 						
 						double NET_AMNT = DR_AMNT / (arTaxCode.getTcRate() / 100);
 						
 						glTaxInterface.setTiNetAmount(NET_AMNT);
 						glTaxInterface.setTiTcCode(arTaxCode.getTcCode());
 						
 					} else {
 						
 						LocalArWithholdingTaxCode arWithholdingTaxCode = arWithholdingTaxCodeHome.findByWtcName(mdetails.getTiWtcName(), AD_CMPNY);
 						
 						double NET_AMNT = DR_AMNT / (arWithholdingTaxCode.getWtcRate() / 100);
 						
 						glTaxInterface.setTiNetAmount(NET_AMNT);
 						glTaxInterface.setTiWtcCode(arWithholdingTaxCode.getWtcCode());
 						
 					}
 					
 					LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(mdetails.getTiSlSubledgerCode(), AD_CMPNY);
 					glTaxInterface.setTiSlCode(arCustomer.getCstCode());
 					glTaxInterface.setTiSlSubledgerCode(arCustomer.getCstCustomerCode());
 					glTaxInterface.setTiSlName(arCustomer.getCstName());
 					glTaxInterface.setTiSlTin(arCustomer.getCstTin());
 					glTaxInterface.setTiSlAddress(arCustomer.getCstAddress());
 					
 				} else {
 					
 					glTaxInterface.setTiIsArDocument(EJBCommon.FALSE);
 					glTaxInterface.setTiTxnReferenceNumber(mdetails.getTiTxnReferenceNumber());
 					
 					if (mdetails.getTiTcName()!= null) {
 						
 						LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(mdetails.getTiTcName(), AD_CMPNY);
 						
 						double NET_AMNT = DR_AMNT / (apTaxCode.getTcRate() / 100);
 						
 						glTaxInterface.setTiNetAmount(NET_AMNT);
 						glTaxInterface.setTiTcCode(apTaxCode.getTcCode());
 						
 					} else {
 						
 						LocalApWithholdingTaxCode apWithholdingTaxCode = apWithholdingTaxCodeHome.findByWtcName(mdetails.getTiWtcName(), AD_CMPNY);
 						
 						double NET_AMNT = DR_AMNT / (apWithholdingTaxCode.getWtcRate() / 100);
 						
 						glTaxInterface.setTiNetAmount(NET_AMNT);
 						glTaxInterface.setTiWtcCode(apWithholdingTaxCode.getWtcCode());
 						
 					}
 					
 					LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(mdetails.getTiSlSubledgerCode(), AD_CMPNY);
 					glTaxInterface.setTiSlCode(apSupplier.getSplCode());
 					glTaxInterface.setTiSlSubledgerCode(apSupplier.getSplSupplierCode());
 					glTaxInterface.setTiSlName(apSupplier.getSplName());
 					glTaxInterface.setTiSlTin(apSupplier.getSplTin());
 					glTaxInterface.setTiSlAddress(apSupplier.getSplAddress());
 					glTaxInterface.setTiTxnReferenceNumber(mdetails.getTiTxnReferenceNumber());
 					
 				}
 				
 			}
 			
 		} catch (GlobalRecordAlreadyDeletedException ex) {
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	/**
 	 * @ejb:interface-method view-type="remote"
 	 * @jboss:method-attributes read-only="true"
 	 **/
 	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean getGlFcPrecisionUnit");
 		
 		LocalAdCompanyHome adCompanyHome = null;
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		try {
 			
 			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			
 			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	// private methods
 	
 	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
 		Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean convertForeignToFunctionalCurrency");
 		
 		
 		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		
 		LocalAdCompany adCompany = null;
 		
 		// Initialize EJB Homes
 		
 		try {
 			
 			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 		// get company and extended precision
 		
 		try {
 			
 			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			
 		} catch (Exception ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}	     
 		
 		
 		// Convert to functional currency if necessary
 		
 		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
 			
 			AMOUNT = AMOUNT * CONVERSION_RATE;
 			
 		} else if (CONVERSION_DATE != null) {
 			
 			try {
 				
 				// Get functional currency rate
 				
 				LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;
 				
 				if (!FC_NM.equals("USD")) {
 					
 					glReceiptFunctionalCurrencyRate = 
 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE, 
 								CONVERSION_DATE, AD_CMPNY);
 					
 					AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();
 					
 				}
 				
 				// Get set of book functional currency rate if necessary
 				
 				if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {
 					
 					LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate = 
 						glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
 								getFcCode(), CONVERSION_DATE, AD_CMPNY);
 					
 					AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();
 					
 				}
 				
 				
 			} catch (Exception ex) {
 				
 				throw new EJBException(ex.getMessage());
 				
 			}       
 			
 		}
 		
 		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
 		
 	}
 	
 	// SessionBean methods
 	
 	/**
 	 * @ejb:create-method view-type="remote"
 	 **/
 	public void ejbCreate() throws CreateException {
 		
 		Debug.print("GlTaxInterfaceMaintenanceControllerBean ejbCreate");
 		
 	}
 	
 	
 }