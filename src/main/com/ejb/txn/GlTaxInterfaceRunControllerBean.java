package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApAppliedVoucher;
import com.ejb.ap.LocalApCheck;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApVoucher;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApWithholdingTaxCode;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlTaxInterface;
import com.ejb.gl.LocalGlTaxInterfaceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlTaxInterfaceDetails;

/**
 * @ejb:bean name="GlTaxInterFaceRunControllerEJB"
 *           display-name="used for tax interface run"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlTaxInterfaceRunControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlTaxInterfaceRunController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlTaxInterfaceRunControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 *
*/

public class GlTaxInterfaceRunControllerBean extends AbstractSessionBean {


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public void runTaxInterface(String DOC_TYP, Date DT_FRM, Date DT_TO, Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("GlTaxInterfaceRunControllerBean arTaxInterface");

    	try {

    		if(DOC_TYP.equals("AR INVOICE") || DOC_TYP.equals("AR CREDIT MEMO") || DOC_TYP.equals("AR RECEIPT")) {

    			this.runArTaxCode(DOC_TYP, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
    			this.runArWithholdingTaxCode(DOC_TYP, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);

    		} else if(DOC_TYP.equals("AP VOUCHER") || DOC_TYP.equals("AP DEBIT MEMO")
    			|| DOC_TYP.equals("AP CHECK") || DOC_TYP.equals("AP RECEIVING ITEM")) {

    			this.runApTaxCode(DOC_TYP, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
    			this.runApWithholdingTaxCode(DOC_TYP, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);

    		} else {

    			this.runArTaxCode(DOC_TYP, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
    			this.runArWithholdingTaxCode(DOC_TYP, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
    			this.runApTaxCode(DOC_TYP, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);
    			this.runApWithholdingTaxCode(DOC_TYP, DT_FRM, DT_TO,  AD_BRNCH, AD_CMPNY);

    		}

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    //  private methods
    private short getGlFcPrecisionUnit(Integer AD_CMPNY) {

	    Debug.print("GlTaxInterfaceRunControllerBean getGlFcPrecisionUnit");

	    LocalAdCompanyHome adCompanyHome = null;


	    // Initialize EJB Home

	    try {

	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

	      return  adCompany.getGlFunctionalCurrency().getFcPrecision();

	    } catch (Exception ex) {

	    	 Debug.printStackTrace(ex);
	      throw new EJBException(ex.getMessage());

	    }

	}

    private void runArTaxCode(String DOC_TYP, Date DT_FRM, Date DT_TO, Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("GlTaxInterfaceRunControllerBean arTaxCodeInteraface");

    	LocalArTaxCodeHome arTaxCodeHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalArInvoiceHome arInvoiceHome = null;
    	LocalArReceiptHome arReceiptHome = null;

    	// Initialize EJB Home

    	try {

    		arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
    		arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
    		arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
    				lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	// ar tax
    	short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);

    	try {

    		if(DOC_TYP.equals("AR INVOICE") || DOC_TYP.equals("")) {
        		System.out.println("AR INVOICE");

        		Collection arInvoices = arInvoiceHome.findPostedTaxInvByInvDateRange(
        				EJBCommon.FALSE, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);


        		Iterator i = arInvoices.iterator();

        		while(i.hasNext()) {

        			LocalArInvoice arInvoice = (LocalArInvoice)i.next();

        			if (arInvoice.getArTaxCode().getTcInterimAccount() != null ) continue;

        			removeExisting("AR INVOICE", arInvoice.getInvCode(), AD_CMPNY);

        			// create tax interface

    				GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();


    				Collection apDrs = arInvoice.getArDistributionRecords();

    				double SALES_AMOUNT = 0d;
    				Integer DR_CODE = 0;
    				Iterator x = apDrs.iterator();
    				while(x.hasNext()){

    					LocalArDistributionRecord apDistributionRecord = (LocalArDistributionRecord)x.next();
    					if(apDistributionRecord.getDrClass().equals("RECEIVABLE")){
    						SALES_AMOUNT += apDistributionRecord.getDrAmount();
    					}
    					
    					if(apDistributionRecord.getDrClass().equals("TAX")){
    						DR_CODE = apDistributionRecord.getDrCode();
    					}

    				}

    				System.out.println("SALES_AMOUNT="+SALES_AMOUNT);
    				double NET_AMNT = EJBCommon.roundIt(SALES_AMOUNT / ((arInvoice.getArTaxCode().getTcRate()/100) + 1), PRECISION_UNIT);
    				details.setTiDocumentType("AR INVOICE");
    				details.setTiSource("AR");
    				details.setTiNetAmount(NET_AMNT);
    				details.setTiTaxAmount(EJBCommon.roundIt(SALES_AMOUNT - NET_AMNT,PRECISION_UNIT));
    				details.setTiSalesAmount(EJBCommon.roundIt(SALES_AMOUNT, PRECISION_UNIT));
    				details.setTiServicesAmount(0d);
    				details.setTiCapitalGoodsAmount(0d);
    				details.setTiOtherCapitalGoodsAmount(0d);
    				details.setTiTxnCode(arInvoice.getInvCode());
    				details.setTiTxnDate(arInvoice.getInvDate());
    				details.setTiTxnDocumentNumber(arInvoice.getInvNumber());
    				details.setTiTxnReferenceNumber(arInvoice.getInvReferenceNumber());
    				details.setTiTaxExempt(EJBCommon.roundIt(arInvoice.getArTaxCode().getTcName().equals("EXEMPT") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTaxZeroRated(EJBCommon.roundIt(arInvoice.getArTaxCode().getTcName().equals("ZERO-RATED") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTxlCode(DR_CODE);
    				details.setTiTxlCoaCode(arInvoice.getArTaxCode().getGlChartOfAccount() != null ?
    						arInvoice.getArTaxCode().getGlChartOfAccount().getCoaCode() : null);
    				details.setTiTcCode(arInvoice.getArTaxCode().getTcCode());
    				details.setTiWtcCode(null);
    				details.setTiSlCode(arInvoice.getArCustomer().getCstCode());
    				details.setTiSlTin(arInvoice.getArCustomer().getCstTin());
    				details.setTiSlSubledgerCode(arInvoice.getArCustomer().getCstCustomerCode());
    				details.setTiSlName(arInvoice.getArCustomer().getCstName());
    				details.setTiSlAddress(arInvoice.getArCustomer().getCstAddress());
    				details.setTiSlAddress2(arInvoice.getArCustomer().getCstCity() +" "+arInvoice.getArCustomer().getCstStateProvince());


    				/*if (apVoucher.getApSupplier2() != null) {

    					details.setTiSlTin(apVoucher.getVouSplSupplier2Tin());
    					details.setTiSlSubledgerCode(apVoucher.getApSupplier2().getSplSupplierCode());
    					details.setTiSlName(apVoucher.getApSupplier2().getSplName());
    					details.setTiSlAddress(apVoucher.getVouSplSupplier2Address1());
    					details.setTiSlAddress2(apVoucher.getVouSplSupplier2Address2());

    				}*/


    				details.setTiIsArDocument(EJBCommon.TRUE);
    				details.setTiAdBranch(AD_BRNCH);
    				details.setTiAdCompany(AD_CMPNY);



    				this.createTaxInterface(details);



        		}



    		}




    		if(DOC_TYP.equals("AR RECEIPT") || DOC_TYP.equals("")) {
        		System.out.println("AR RECEIPT");

        		Collection arReceipts = arReceiptHome.findPostedTaxRctByRctDateRange(
        				DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);

        		Iterator i = arReceipts.iterator();

        		while(i.hasNext()) {

        			LocalArReceipt arReceipt = (LocalArReceipt)i.next();

        			removeExisting("AR RECEIPT", arReceipt.getRctCode(), AD_CMPNY);

        			// create tax interface

    				GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();


    				Collection apDrs = arReceipt.getArDistributionRecords();

    				double SALES_AMOUNT = 0d;
    				Integer DR_CODE = 0;
    				Iterator x = apDrs.iterator();
    				while(x.hasNext()){

    					LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)x.next();
    					
    					System.out.println("get tax and sales 1 in code " + arDistributionRecord.getDrCode());
    					if(arDistributionRecord.getDrClass().equals("RECEIVABLE")){
    						SALES_AMOUNT += arDistributionRecord.getDrAmount();
    					}
    					
    					if(arDistributionRecord.getDrClass().equals("TAX")){
    						System.out.println("get code");
    						DR_CODE = arDistributionRecord.getDrCode();
    						
    					}

    				}


    				ArrayList arAppliedInvoiceList = new ArrayList(arReceipt.getArAppliedInvoices());
    				LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)arAppliedInvoiceList.get(0);
    				LocalArInvoice arInvoice = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice();


    				System.out.println("SALES_AMOUNT="+SALES_AMOUNT);
    				double NET_AMNT = EJBCommon.roundIt(SALES_AMOUNT / ((arInvoice.getArTaxCode().getTcRate()/100) + 1), PRECISION_UNIT);
    				details.setTiDocumentType("AR RECEIPT");
    				details.setTiSource("AR");
    				details.setTiNetAmount(NET_AMNT);
    				details.setTiTaxAmount(EJBCommon.roundIt(SALES_AMOUNT - NET_AMNT,PRECISION_UNIT));
    				details.setTiSalesAmount(EJBCommon.roundIt(SALES_AMOUNT, PRECISION_UNIT));
    				details.setTiServicesAmount(0d);
    				details.setTiCapitalGoodsAmount(0d);
    				details.setTiOtherCapitalGoodsAmount(0d);
    				details.setTiTxnCode(arReceipt.getRctCode());
    				details.setTiTxnDate(arReceipt.getRctDate());
    				details.setTiTxnDocumentNumber(arReceipt.getRctNumber());
    				details.setTiTxnReferenceNumber(arReceipt.getRctReferenceNumber());
    				details.setTiTaxExempt(EJBCommon.roundIt(arInvoice.getArTaxCode().getTcName().equals("EXEMPT") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTaxZeroRated(EJBCommon.roundIt(arInvoice.getArTaxCode().getTcName().equals("ZERO-RATED") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTxlCode(DR_CODE);
    				details.setTiTxlCoaCode(arInvoice.getArTaxCode().getGlChartOfAccount() != null ?
    						arInvoice.getArTaxCode().getGlChartOfAccount().getCoaCode() : null);
    				details.setTiTcCode(arInvoice.getArTaxCode().getTcCode());
    				details.setTiWtcCode(null);
    				details.setTiSlCode(arInvoice.getArCustomer().getCstCode());
    				details.setTiSlTin(arInvoice.getArCustomer().getCstTin());
    				details.setTiSlSubledgerCode(arInvoice.getArCustomer().getCstCustomerCode());
    				details.setTiSlName(arInvoice.getArCustomer().getCstName());
    				details.setTiSlAddress(arInvoice.getArCustomer().getCstAddress());
    				details.setTiSlAddress2(arInvoice.getArCustomer().getCstCity() +" "+arInvoice.getArCustomer().getCstStateProvince());


    				/*if (apVoucher.getApSupplier2() != null) {

    					details.setTiSlTin(apVoucher.getVouSplSupplier2Tin());
    					details.setTiSlSubledgerCode(apVoucher.getApSupplier2().getSplSupplierCode());
    					details.setTiSlName(apVoucher.getApSupplier2().getSplName());
    					details.setTiSlAddress(apVoucher.getVouSplSupplier2Address1());
    					details.setTiSlAddress2(apVoucher.getVouSplSupplier2Address2());

    				}*/


    				details.setTiIsArDocument(EJBCommon.TRUE);
    				details.setTiAdBranch(AD_BRNCH);
    				details.setTiAdCompany(AD_CMPNY);



    				this.createTaxInterface(details);



        		}



    		}


    		if(DOC_TYP.equals("AR RECEIPT") || DOC_TYP.equals("")) {
        		System.out.println("AR MISC RECEIPT");

        		Collection arReceipts = arReceiptHome.findPostedTaxMiscRctByRctDateRange(
        				DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);

        		Iterator i = arReceipts.iterator();

        		while(i.hasNext()) {

        			LocalArReceipt arReceipt = (LocalArReceipt)i.next();

        			removeExisting("AR RECEIPT", arReceipt.getRctCode(), AD_CMPNY);

        			// create tax interface

    				GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();


    				Collection apDrs = arReceipt.getArDistributionRecords();

    				double SALES_AMOUNT = 0d;
    				Integer DR_CODE = 0;
    				Iterator x = apDrs.iterator();
    				while(x.hasNext()){
    				
    					LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)x.next();
    					
    					System.out.println("get tax and sales 2 in code " + arDistributionRecord.getDrCode());
    					if(arDistributionRecord.getDrClass().equals("CASH")){
    						SALES_AMOUNT += arDistributionRecord.getDrAmount();
    					}

    					if(arDistributionRecord.getDrClass().equals("TAX")){
    						
    						System.out.println("get code");
    						DR_CODE = arDistributionRecord.getDrCode();
    					}
    				}


    				System.out.println("SALES_AMOUNT="+SALES_AMOUNT);
    				double NET_AMNT = EJBCommon.roundIt(SALES_AMOUNT / ((arReceipt.getArTaxCode().getTcRate()/100) + 1), PRECISION_UNIT);
    				details.setTiDocumentType("AR RECEIPT");
    				details.setTiSource("AR");
    				details.setTiNetAmount(NET_AMNT);
    				details.setTiTaxAmount(EJBCommon.roundIt(SALES_AMOUNT - NET_AMNT,PRECISION_UNIT));
    				details.setTiSalesAmount(EJBCommon.roundIt(SALES_AMOUNT, PRECISION_UNIT));
    				details.setTiServicesAmount(0d);
    				details.setTiCapitalGoodsAmount(0d);
    				details.setTiOtherCapitalGoodsAmount(0d);
    				details.setTiTxnCode(arReceipt.getRctCode());
    				details.setTiTxnDate(arReceipt.getRctDate());
    				details.setTiTxnDocumentNumber(arReceipt.getRctNumber());
    				details.setTiTxnReferenceNumber(arReceipt.getRctReferenceNumber());
    				details.setTiTaxExempt(EJBCommon.roundIt(arReceipt.getArTaxCode().getTcName().equals("EXEMPT") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTaxZeroRated(EJBCommon.roundIt(arReceipt.getArTaxCode().getTcName().equals("ZERO-RATED") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTxlCode(DR_CODE);
    				details.setTiTxlCoaCode(arReceipt.getArTaxCode().getGlChartOfAccount() != null ?
    						arReceipt.getArTaxCode().getGlChartOfAccount().getCoaCode() : null);
    				details.setTiTcCode(arReceipt.getArTaxCode().getTcCode());
    				details.setTiWtcCode(null);
    				details.setTiSlCode(arReceipt.getArCustomer().getCstCode());
    				details.setTiSlTin(arReceipt.getArCustomer().getCstTin());
    				details.setTiSlSubledgerCode(arReceipt.getArCustomer().getCstCustomerCode());
    				details.setTiSlName(arReceipt.getArCustomer().getCstName());
    				details.setTiSlAddress(arReceipt.getArCustomer().getCstAddress());
    				details.setTiSlAddress2(arReceipt.getArCustomer().getCstCity() +" "+arReceipt.getArCustomer().getCstStateProvince());


    				details.setTiIsArDocument(EJBCommon.TRUE);
    				details.setTiAdBranch(AD_BRNCH);
    				details.setTiAdCompany(AD_CMPNY);



    				this.createTaxInterface(details);



        		}



    		}








    		/*Collection arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);

    		Iterator i = arTaxCodes.iterator();

    		while(i.hasNext()) {

    			LocalArTaxCode arTaxCode = (LocalArTaxCode)i.next();
    			LocalGlChartOfAccount glChartOfAccount = arTaxCode.getGlChartOfAccount();

    			if(glChartOfAccount != null ){

    				// AR INVOICE

    				if(DOC_TYP.equals("AR INVOICE") || DOC_TYP.equals("")) {

    					Collection arDistributionRecords = arDistributionRecordHome.findInvByDateRangeAndCoaAccountNumber(
    						EJBCommon.FALSE, DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    					Iterator drItr = arDistributionRecords.iterator();



    					while(drItr.hasNext()) {

    						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drItr.next();

    						Integer invoiceTcCode = null;
    						if(arDistributionRecord.getArInvoice().getArTaxCode() != null) {

    							invoiceTcCode = arDistributionRecord.getArInvoice().getArTaxCode().getTcCode();

    						}
    						System.out.println("invoice to generate: " + arDistributionRecord.getArInvoice().getInvNumber() + "---------------------------");
    						if((invoiceTcCode != null && invoiceTcCode.equals(arTaxCode.getTcCode()))) {

    							removeExisting("AR INVOICE", arDistributionRecord.getDrCode(), AD_CMPNY);

    							double DR_AMNT = this.convertForeignToFunctionalCurrency(
    								arDistributionRecord.getArInvoice().getGlFunctionalCurrency().getFcCode(),
    								arDistributionRecord.getArInvoice().getGlFunctionalCurrency().getFcName(),
    								arDistributionRecord.getArInvoice().getInvConversionDate(),
    								arDistributionRecord.getArInvoice().getInvConversionRate(),
    								arDistributionRecord.getDrAmount(), AD_CMPNY);

    							//if (arTaxCode.getTcRate() == 0) continue;

    							Collection arInvoices = arDistributionRecord.getArInvoice().getArDistributionRecords();

    							double SALES_AMOUNT = 0d;
    							Iterator x = arInvoices.iterator();
    							while(x.hasNext()){

    								LocalArDistributionRecord arDistributionRecordInvoice = (LocalArDistributionRecord)x.next();
    								if(arDistributionRecordInvoice.getDrClass().equals("RECEIVABLE")){
	    								SALES_AMOUNT += arDistributionRecordInvoice.getDrAmount();
    								}
    							}


    							double NET_AMNT = EJBCommon.roundIt(SALES_AMOUNT / ((arTaxCode.getTcRate()/100) + 1), PRECISION_UNIT);
    							GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    							details.setTiDocumentType("AR INVOICE");
    							details.setTiSource("AR");
    							details.setTiNetAmount(NET_AMNT);
    							details.setTiTaxAmount(EJBCommon.roundIt(SALES_AMOUNT - NET_AMNT,PRECISION_UNIT));
    							details.setTiSalesAmount(EJBCommon.roundIt(SALES_AMOUNT, PRECISION_UNIT));

    							details.setTiTxnCode(arDistributionRecord.getArInvoice().getInvCode());
    							details.setTiTxnDate(arDistributionRecord.getArInvoice().getInvDate());
    							details.setTiTxnDocumentNumber(arDistributionRecord.getArInvoice().getInvNumber());
    							details.setTiTxnReferenceNumber(arDistributionRecord.getArInvoice().getInvReferenceNumber());
    							details.setTiTxlCode(arDistributionRecord.getDrCode());
    							details.setTiTxlCoaCode(arDistributionRecord.getGlChartOfAccount().getCoaCode());
    							details.setTiTcCode(invoiceTcCode);
    							details.setTiWtcCode(null);
    							details.setTiSlCode(arDistributionRecord.getArInvoice().getArCustomer().getCstCode());
    							details.setTiSlTin(arDistributionRecord.getArInvoice().getArCustomer().getCstTin());
    							details.setTiSlSubledgerCode(arDistributionRecord.getArInvoice().getArCustomer().getCstCustomerCode());
    							details.setTiSlName(arDistributionRecord.getArInvoice().getArCustomer().getCstName());
    							details.setTiSlAddress(arDistributionRecord.getArInvoice().getArCustomer().getCstAddress());
    							details.setTiSlAddress2(arDistributionRecord.getArInvoice().getArCustomer().getCstCity() + " "+ arDistributionRecord.getArInvoice().getArCustomer().getCstStateProvince());
    							details.setTiIsArDocument(EJBCommon.TRUE);
    							details.setTiAdBranch(AD_BRNCH);
    							details.setTiAdCompany(AD_CMPNY);

    							this.createTaxInterface(details);

    						}

    					}

    				}

    				// AR CREDIT MEMO

    				if(DOC_TYP.equals("AR CREDIT MEMO") || DOC_TYP.equals("")) {

    					Collection arDistributionRecords = arDistributionRecordHome.findInvByDateRangeAndCoaAccountNumber(
    						EJBCommon.TRUE, DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    					Iterator drItr = arDistributionRecords.iterator();

    					while(drItr.hasNext()) {

    						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drItr.next();
    						LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
    								arDistributionRecord.getArInvoice().getInvCmInvoiceNumber(),
									EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);



    						Integer creditmemoTcCode = null;
    						if(arInvoice.getArTaxCode() != null) {

    							creditmemoTcCode = arInvoice.getArTaxCode().getTcCode();

    						}

    						if((creditmemoTcCode != null && creditmemoTcCode.equals(arTaxCode.getTcCode()))) {

    							removeExisting("AR CREDIT MEMO", arDistributionRecord.getDrCode(), AD_CMPNY);

    							double DR_AMNT = this.convertForeignToFunctionalCurrency(
    								arInvoice.getGlFunctionalCurrency().getFcCode(),
    								arInvoice.getGlFunctionalCurrency().getFcName(),
    								arInvoice.getInvConversionDate(),
    								arInvoice.getInvConversionRate(),
    								arDistributionRecord.getDrAmount(), AD_CMPNY);

    							if (arTaxCode.getTcRate() == 0) continue;

    							double NET_AMNT = DR_AMNT / (arTaxCode.getTcRate()/100);

    							GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    							details.setTiDocumentType("AR CREDIT MEMO");
    							details.setTiSource("AR");
    							details.setTiNetAmount(NET_AMNT);

    							details.setTiSalesAmount(EJBCommon.roundIt(NET_AMNT + DR_AMNT, PRECISION_UNIT));
    							details.setTiTxnCode(arDistributionRecord.getArInvoice().getInvCode());
    							details.setTiTxnDate(arDistributionRecord.getArInvoice().getInvDate());
    							details.setTiTxnDocumentNumber(arDistributionRecord.getArInvoice().getInvNumber());
    							details.setTiTxnReferenceNumber(arDistributionRecord.getArInvoice().getInvCmInvoiceNumber());
    							details.setTiTxlCode(arDistributionRecord.getDrCode());
    							details.setTiTxlCoaCode(arDistributionRecord.getGlChartOfAccount().getCoaCode());
    							details.setTiTcCode(creditmemoTcCode);
    							details.setTiWtcCode(null);
    							details.setTiSlCode(arDistributionRecord.getArInvoice().getArCustomer().getCstCode());
    							details.setTiSlTin(arDistributionRecord.getArInvoice().getArCustomer().getCstTin());
    							details.setTiSlSubledgerCode(arDistributionRecord.getArInvoice().getArCustomer().getCstCustomerCode());
    							details.setTiSlName(arDistributionRecord.getArInvoice().getArCustomer().getCstName());
    							details.setTiSlAddress(arDistributionRecord.getArInvoice().getArCustomer().getCstAddress());
    							details.setTiSlAddress2(arDistributionRecord.getArInvoice().getArCustomer().getCstCity() + " "+ arDistributionRecord.getArInvoice().getArCustomer().getCstStateProvince());
    							details.setTiIsArDocument(EJBCommon.TRUE);
    							details.setTiAdBranch(AD_BRNCH);
    							details.setTiAdCompany(AD_CMPNY);

    							this.createTaxInterface(details);

    						}

    					}

    				}

    				// AR RECEIPT

    				if(DOC_TYP.equals("AR RECEIPT") || DOC_TYP.equals("")) {

    					Collection arDistributionRecords = arDistributionRecordHome.findRctByDateRangeAndCoaAccountNumber(
    						DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    					Iterator drItr = arDistributionRecords.iterator();

    					while(drItr.hasNext()) {

    						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drItr.next();



    						Integer receiptTcCode = null;
    						if(arDistributionRecord.getArReceipt().getArTaxCode() != null) {

    							receiptTcCode = arDistributionRecord.getArReceipt().getArTaxCode().getTcCode();

    						}

    						Integer appliedTcCode = null;
    						if(arDistributionRecord.getArAppliedInvoice() != null) {

    							appliedTcCode = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice().getArTaxCode().getTcCode();

    						}

    						if(((receiptTcCode != null && receiptTcCode.equals(arTaxCode.getTcCode())) ||
    								(appliedTcCode != null && appliedTcCode.equals(arTaxCode.getTcCode())))) {

    							removeExisting("AR RECEIPT", arDistributionRecord.getDrCode(), AD_CMPNY);

    							LocalArAppliedInvoice arAppliedInvoice = arDistributionRecord.getArAppliedInvoice();
    							LocalArInvoice arInvoice = null;

    							double DR_AMNT = 0d;
    							if (arAppliedInvoice == null) {

    								DR_AMNT = this.convertForeignToFunctionalCurrency(
    									arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcCode(),
										arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcName(),
										arDistributionRecord.getArReceipt().getRctConversionDate(),
										arDistributionRecord.getArReceipt().getRctConversionRate(),
										arDistributionRecord.getDrAmount(), AD_CMPNY);

    							} else {

    								arInvoice = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice();

    								DR_AMNT = this.convertForeignToFunctionalCurrency(
    									arInvoice.getGlFunctionalCurrency().getFcCode(),
    									arInvoice.getGlFunctionalCurrency().getFcName(),
    									arInvoice.getInvConversionDate(),
    									arInvoice.getInvConversionRate(),
										arDistributionRecord.getDrAmount(), AD_CMPNY);

    							}

    							if (arTaxCode.getTcRate() == 0) continue;

    							Collection arReceipts = arDistributionRecord.getArReceipt().getArDistributionRecords();

    							double SALES_AMOUNT = 0d;
    							Iterator x = arReceipts.iterator();
    							while(x.hasNext()){

    								LocalArDistributionRecord arDistributionRecordReceipt = (LocalArDistributionRecord)x.next();
    								if(arDistributionRecordReceipt.getDrClass().equals("CASH")){
	    								SALES_AMOUNT += arDistributionRecordReceipt.getDrAmount();
    								}
    							}
    							double NET_AMNT = EJBCommon.roundIt(SALES_AMOUNT / ((arTaxCode.getTcRate()/100) + 1), PRECISION_UNIT);

    							GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    							details.setTiDocumentType("AR RECEIPT");
    							details.setTiSource("AR");
    							details.setTiNetAmount(NET_AMNT);
    							details.setTiTaxAmount(EJBCommon.roundIt(SALES_AMOUNT - NET_AMNT,PRECISION_UNIT));
    							details.setTiSalesAmount(EJBCommon.roundIt(SALES_AMOUNT, PRECISION_UNIT));
    							details.setTiTxnCode(arDistributionRecord.getArReceipt().getRctCode());
    							details.setTiTxnDate(arDistributionRecord.getArReceipt().getRctDate());
    							details.setTiTxnDocumentNumber(arDistributionRecord.getArReceipt().getRctNumber());
    							details.setTiTxnReferenceNumber(arDistributionRecord.getArReceipt().getRctReferenceNumber());
    							details.setTiTxlCode(arDistributionRecord.getDrCode());
    							details.setTiTxlCoaCode(arDistributionRecord.getGlChartOfAccount().getCoaCode());
    							if (arInvoice == null) {

    								details.setTiTcCode(receiptTcCode);

    							} else {

    								details.setTiTcCode(appliedTcCode);

    							}
    							details.setTiWtcCode(null);
    							details.setTiSlCode(arDistributionRecord.getArReceipt().getArCustomer().getCstCode());
    							details.setTiSlTin(arDistributionRecord.getArReceipt().getArCustomer().getCstTin());
    							details.setTiSlSubledgerCode(arDistributionRecord.getArReceipt().getArCustomer().getCstCustomerCode());
    							details.setTiSlName(arDistributionRecord.getArReceipt().getArCustomer().getCstName());
    							details.setTiSlAddress(arDistributionRecord.getArReceipt().getArCustomer().getCstAddress());
    							details.setTiSlAddress2(arDistributionRecord.getArReceipt().getArCustomer().getCstCity() + " "+ arDistributionRecord.getArReceipt().getArCustomer().getCstStateProvince());
    							details.setTiIsArDocument(EJBCommon.TRUE);
    							details.setTiAdBranch(AD_BRNCH);
    							details.setTiAdCompany(AD_CMPNY);

    							this.createTaxInterface(details);

    						}

    					}

    				}

    				// GL JOURNAL

    				Collection glJournalLines = glJournalLineHome.findManualJrByEffectiveDateRangeAndCoaCode(
    					DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    				Iterator jlItr = glJournalLines.iterator();

    				while(jlItr.hasNext()) {

    					LocalGlJournalLine glJournalLine = (LocalGlJournalLine)jlItr.next();

    					removeExisting("GL JOURNAL", glJournalLine.getJlCode(), AD_CMPNY);

    					//if(!existing) {

    						GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    						details.setTiDocumentType("GL JOURNAL");
    						details.setTiSource("GL");
    						details.setTiNetAmount(0d);
    						details.setTiTxnCode(glJournalLine.getGlJournal().getJrCode());
    						details.setTiTxnDate(glJournalLine.getGlJournal().getJrEffectiveDate());
    						details.setTiTxnDocumentNumber(glJournalLine.getGlJournal().getJrDocumentNumber());
    						details.setTiTxnReferenceNumber(null);
    						details.setTiTxlCode(glJournalLine.getJlCode());
    						details.setTiTxlCoaCode(glJournalLine.getGlChartOfAccount().getCoaCode());
    						details.setTiTcCode(null);
    						details.setTiWtcCode(null);
    						details.setTiSlCode(null);
    						details.setTiSlTin(null);
    						details.setTiSlSubledgerCode(null);
    						details.setTiSlName(null);
    						details.setTiSlAddress(null);
    						details.setTiIsArDocument(EJBCommon.FALSE);
    						details.setTiAdBranch(AD_BRNCH);
    						details.setTiAdCompany(AD_CMPNY);

    						this.createTaxInterface(details);

    					//}

    				}

    			}

    		}*/

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void runArWithholdingTaxCode(String DOC_TYP, Date DT_FRM, Date DT_TO, Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("GlTaxInterfaceRunControllerBean arWithholdingTaxCodeInteraface");

    	LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalArDistributionRecordHome arDistributionRecordHome = null;
    	LocalArInvoiceHome arInvoiceHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;

    	// Initialize EJB Home

    	try {

    		arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
    		arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	// ar withholding tax
    	short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);

    	try {

    		Collection arWithholdingTaxCodes = arWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);

    		Iterator i = arWithholdingTaxCodes.iterator();

    		while(i.hasNext()) {

    			LocalArWithholdingTaxCode arWithholdingTaxCode = (LocalArWithholdingTaxCode)i.next();

    			LocalGlChartOfAccount glChartOfAccount = arWithholdingTaxCode.getGlChartOfAccount();

    			if(glChartOfAccount != null ){

    				// AR INVOICE

    				if(DOC_TYP.equals("AR INVOICE") || DOC_TYP.equals("")) {

    					Collection arDistributionRecords = arDistributionRecordHome.findInvByDateRangeAndCoaAccountNumber(
    						EJBCommon.FALSE, DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    					Iterator drItr = arDistributionRecords.iterator();

    					while(drItr.hasNext()) {

    						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drItr.next();



    						Integer invoiceWtcCode = null;
    						if(arDistributionRecord.getArInvoice().getArWithholdingTaxCode() != null) {

    							invoiceWtcCode = arDistributionRecord.getArInvoice().getArWithholdingTaxCode().getWtcCode();

    						}

    						if((invoiceWtcCode!= null && invoiceWtcCode.equals(arWithholdingTaxCode.getWtcCode()))) {

    							removeExisting("AR INVOICE", arDistributionRecord.getDrCode(), AD_CMPNY);

    							double DR_AMNT = this.convertForeignToFunctionalCurrency(
    								arDistributionRecord.getArInvoice().getGlFunctionalCurrency().getFcCode(),
    								arDistributionRecord.getArInvoice().getGlFunctionalCurrency().getFcName(),
    								arDistributionRecord.getArInvoice().getInvConversionDate(),
    								arDistributionRecord.getArInvoice().getInvConversionRate(),
    								arDistributionRecord.getDrAmount(), AD_CMPNY);

    							if (arWithholdingTaxCode.getWtcRate() == 0) continue;

    							double NET_AMNT = DR_AMNT / (arWithholdingTaxCode.getWtcRate()/100);

    							GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    							details.setTiDocumentType("AR INVOICE");
    							details.setTiSource("AR");
    							details.setTiNetAmount(NET_AMNT);
    							details.setTiSalesAmount(EJBCommon.roundIt(NET_AMNT + DR_AMNT, PRECISION_UNIT));
    							details.setTiTxnCode(arDistributionRecord.getArInvoice().getInvCode());
    							details.setTiTxnDate(arDistributionRecord.getArInvoice().getInvDate());
    							details.setTiTxnDocumentNumber(arDistributionRecord.getArInvoice().getInvNumber());
    							details.setTiTxnReferenceNumber(arDistributionRecord.getArInvoice().getInvReferenceNumber());
    							details.setTiTxlCode(arDistributionRecord.getDrCode());
    							details.setTiTxlCoaCode(arDistributionRecord.getGlChartOfAccount().getCoaCode());
    							details.setTiTcCode(null);
    							details.setTiWtcCode(invoiceWtcCode);
    							details.setTiSlCode(arDistributionRecord.getArInvoice().getArCustomer().getCstCode());
    							details.setTiSlTin(arDistributionRecord.getArInvoice().getArCustomer().getCstTin());
    							details.setTiSlSubledgerCode(arDistributionRecord.getArInvoice().getArCustomer().getCstCustomerCode());
    							details.setTiSlName(arDistributionRecord.getArInvoice().getArCustomer().getCstName());
    							details.setTiSlAddress(arDistributionRecord.getArInvoice().getArCustomer().getCstAddress());
    							details.setTiIsArDocument(EJBCommon.TRUE);
    							details.setTiAdBranch(AD_BRNCH);
    							details.setTiAdCompany(AD_CMPNY);

    							this.createTaxInterface(details);

    						}

    					}

    				}

    				// AR CREDIT MEMO

    				if(DOC_TYP.equals("AR CREDIT MEMO") || DOC_TYP.equals("")) {

    					Collection arDistributionRecords = arDistributionRecordHome.findInvByDateRangeAndCoaAccountNumber(
    							EJBCommon.TRUE, DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    					Iterator drItr = arDistributionRecords.iterator();

    					while(drItr.hasNext()) {

    						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drItr.next();
    						LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
    								arDistributionRecord.getArInvoice().getInvCmInvoiceNumber(),
									EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);



    						Integer creditmemoWtcCode = null;
    						if(arInvoice.getArWithholdingTaxCode()!= null) {

    							creditmemoWtcCode = arInvoice.getArWithholdingTaxCode().getWtcCode();

    						}

    						if((creditmemoWtcCode != null && creditmemoWtcCode.equals(arWithholdingTaxCode.getWtcCode()))) {
    							removeExisting("AR CREDIT MEMO", arDistributionRecord.getDrCode(), AD_CMPNY);
    							double DR_AMNT = this.convertForeignToFunctionalCurrency(
    								arInvoice.getGlFunctionalCurrency().getFcCode(),
    								arInvoice.getGlFunctionalCurrency().getFcName(),
    								arInvoice.getInvConversionDate(),
    								arInvoice.getInvConversionRate(),
    								arDistributionRecord.getDrAmount(), AD_CMPNY);

    							if (arWithholdingTaxCode.getWtcRate() == 0) continue;

    							double NET_AMNT = DR_AMNT / (arWithholdingTaxCode.getWtcRate()/100);

    							GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    							details.setTiDocumentType("AR CREDIT MEMO");
    							details.setTiSource("AR");
    							details.setTiNetAmount(NET_AMNT);
    							details.setTiSalesAmount(EJBCommon.roundIt(NET_AMNT + DR_AMNT, PRECISION_UNIT));
    							details.setTiTxnCode(arDistributionRecord.getArInvoice().getInvCode());
    							details.setTiTxnDate(arDistributionRecord.getArInvoice().getInvDate());
    							details.setTiTxnDocumentNumber(arDistributionRecord.getArInvoice().getInvNumber());
    							details.setTiTxnReferenceNumber(arDistributionRecord.getArInvoice().getInvCmInvoiceNumber());
    							details.setTiTxlCode(arDistributionRecord.getDrCode());
    							details.setTiTxlCoaCode(arDistributionRecord.getGlChartOfAccount().getCoaCode());
    							details.setTiTcCode(null);
    							details.setTiWtcCode(creditmemoWtcCode);
    							details.setTiSlCode(arDistributionRecord.getArInvoice().getArCustomer().getCstCode());
    							details.setTiSlTin(arDistributionRecord.getArInvoice().getArCustomer().getCstTin());
    							details.setTiSlSubledgerCode(arDistributionRecord.getArInvoice().getArCustomer().getCstCustomerCode());
    							details.setTiSlName(arDistributionRecord.getArInvoice().getArCustomer().getCstName());
    							details.setTiSlAddress(arDistributionRecord.getArInvoice().getArCustomer().getCstAddress());
    							details.setTiIsArDocument(EJBCommon.TRUE);
    							details.setTiAdBranch(AD_BRNCH);
    							details.setTiAdCompany(AD_CMPNY);

    							this.createTaxInterface(details);

    						}

    					}

    				}

    				// AR RECEIPT

    				if(DOC_TYP.equals("AR RECEIPT") || DOC_TYP.equals("")) {

    					Collection arDistributionRecords = arDistributionRecordHome.findRctByDateRangeAndCoaAccountNumber(
    							DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    					Iterator drItr = arDistributionRecords.iterator();

    					while(drItr.hasNext()) {

    						LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drItr.next();



    						Integer receiptWtcCode = null;
    						if(arDistributionRecord.getArReceipt().getArWithholdingTaxCode() != null) {

    							receiptWtcCode = arDistributionRecord.getArReceipt().getArWithholdingTaxCode().getWtcCode();

    						}

    						Integer appliedWtcCode = null;
    						if(arDistributionRecord.getArAppliedInvoice() != null) {

    							double appliedWtcRate = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice().getArWithholdingTaxCode().getWtcRate();

    							if(appliedWtcRate == 0d) {

    								LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    								appliedWtcCode = adPreference.getArWithholdingTaxCode().getWtcCode();

    							} else {

    								appliedWtcCode = arDistributionRecord.getArAppliedInvoice().getArInvoicePaymentSchedule().getArInvoice().getArWithholdingTaxCode().getWtcCode();

    							}

    						}

    						if(((receiptWtcCode != null && receiptWtcCode.equals(arWithholdingTaxCode.getWtcCode())) ||
    								(appliedWtcCode != null && appliedWtcCode.equals(arWithholdingTaxCode.getWtcCode())))) {
    							removeExisting("AR RECEIPT", arDistributionRecord.getDrCode(), AD_CMPNY);
    							LocalArAppliedInvoice arAppliedInvoice = arDistributionRecord.getArAppliedInvoice();
    							LocalArInvoice arInvoice = null;

    							double DR_AMNT = 0d;
    							if (arAppliedInvoice == null) {

    								DR_AMNT = this.convertForeignToFunctionalCurrency(
    									arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcCode(),
										arDistributionRecord.getArReceipt().getGlFunctionalCurrency().getFcName(),
										arDistributionRecord.getArReceipt().getRctConversionDate(),
										arDistributionRecord.getArReceipt().getRctConversionRate(),
										arDistributionRecord.getDrAmount(), AD_CMPNY);

    							} else {

    								arInvoice = arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice();

    								DR_AMNT = this.convertForeignToFunctionalCurrency(
    									arInvoice.getGlFunctionalCurrency().getFcCode(),
    									arInvoice.getGlFunctionalCurrency().getFcName(),
    									arInvoice.getInvConversionDate(),
    									arInvoice.getInvConversionRate(),
										arDistributionRecord.getDrAmount(), AD_CMPNY);

    							}

    							if (arWithholdingTaxCode.getWtcRate() == 0) continue;

    							double NET_AMNT = DR_AMNT / (arWithholdingTaxCode.getWtcRate()/100);

    							GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    							details.setTiDocumentType("AR RECEIPT");
    							details.setTiSource("AR");
    							details.setTiNetAmount(NET_AMNT);
    							details.setTiSalesAmount(EJBCommon.roundIt(NET_AMNT + DR_AMNT, PRECISION_UNIT));
    							details.setTiTxnCode(arDistributionRecord.getArReceipt().getRctCode());
    							details.setTiTxnDate(arDistributionRecord.getArReceipt().getRctDate());
    							details.setTiTxnDocumentNumber(arDistributionRecord.getArReceipt().getRctNumber());
    							details.setTiTxnReferenceNumber(arDistributionRecord.getArReceipt().getRctReferenceNumber());
    							details.setTiTxlCode(arDistributionRecord.getDrCode());
    							details.setTiTxlCoaCode(arDistributionRecord.getGlChartOfAccount().getCoaCode());
    							details.setTiTcCode(null);
    							if (arInvoice == null) {

    								details.setTiWtcCode(receiptWtcCode);

    							} else {

    								details.setTiWtcCode(appliedWtcCode);

    							}
    							details.setTiSlCode(arDistributionRecord.getArReceipt().getArCustomer().getCstCode());
    							details.setTiSlTin(arDistributionRecord.getArReceipt().getArCustomer().getCstTin());
    							details.setTiSlSubledgerCode(arDistributionRecord.getArReceipt().getArCustomer().getCstCustomerCode());
    							details.setTiSlName(arDistributionRecord.getArReceipt().getArCustomer().getCstName());
    							details.setTiSlAddress(arDistributionRecord.getArReceipt().getArCustomer().getCstAddress());
    							details.setTiIsArDocument(EJBCommon.TRUE);
    							details.setTiAdBranch(AD_BRNCH);
    							details.setTiAdCompany(AD_CMPNY);

    							this.createTaxInterface(details);

    						}

    					}

    				}

    				// GL JOURNAL

    				Collection glJournalLines = glJournalLineHome.findManualJrByEffectiveDateRangeAndCoaCode(
    					DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    				Iterator jlItr = glJournalLines.iterator();

    				while(jlItr.hasNext()) {

    					LocalGlJournalLine glJournalLine = (LocalGlJournalLine)jlItr.next();

    					removeExisting("GL JOURNAL", glJournalLine.getJlCode(), AD_CMPNY );

    					//if(!existing) {

    						GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    						details.setTiDocumentType("GL JOURNAL");
    						details.setTiSource("GL");
    						details.setTiNetAmount(0d);
    						details.setTiTxnCode(glJournalLine.getGlJournal().getJrCode());
    						details.setTiTxnDate(glJournalLine.getGlJournal().getJrEffectiveDate());
    						details.setTiTxnDocumentNumber(glJournalLine.getGlJournal().getJrDocumentNumber());
    						details.setTiTxnReferenceNumber(null);
    						details.setTiTxlCode(glJournalLine.getJlCode());
    						details.setTiTxlCoaCode(glJournalLine.getGlChartOfAccount().getCoaCode());
    						details.setTiTcCode(null);
    						details.setTiWtcCode(null);
    						details.setTiSlCode(null);
    						details.setTiSlTin(null);
    						details.setTiSlSubledgerCode(null);
    						details.setTiSlName(null);
    						details.setTiSlAddress(null);
    						details.setTiIsArDocument(EJBCommon.FALSE);
    						details.setTiAdBranch(AD_BRNCH);
    						details.setTiAdCompany(AD_CMPNY);

    						this.createTaxInterface(details);

    					//}

    				}

    			}

    		}

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void runApTaxCode(String DOC_TYP, Date DT_FRM, Date DT_TO, Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("GlTaxInterfaceRunControllerBean apTaxCodeInterface");

    	LocalApTaxCodeHome apTaxCodeHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalApDistributionRecordHome apDistributionRecordHome = null;
    	LocalApVoucherHome apVoucherHome = null;
    	LocalApCheckHome apCheckHome = null;
    	LocalApPurchaseOrderHome apReceivingItemHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;


    	// Initialize EJB Home

    	try {

    		apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
    		apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
    		apCheckHome = (LocalApCheckHome)EJBHomeFactory.
    				lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
    		apReceivingItemHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
    				lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
    				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	// ap tax
    	short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);

    	try {

    		if(DOC_TYP.equals("AP VOUCHER") || DOC_TYP.equals("")) {
    			System.out.println("AP VOUCHER");


			Collection apVouchers = apVoucherHome.findPostedTaxVouByVouDateRange(
    				EJBCommon.FALSE, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);

			Iterator j = apVouchers.iterator();


			while(j.hasNext()) {

				LocalApVoucher apVoucher = (LocalApVoucher)j.next();

    			removeExisting("AP VOUCHER", apVoucher.getVouCode(), AD_CMPNY);


				// create tax interface

				GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();


				Collection apDrs = apVoucher.getApDistributionRecords();

				double SALES_AMOUNT = 0d;
				double SERVICES = 0d;
				double CAPITAL_GOODS = 0d;
				double OTHER_CAPITAL_GOODS = 0d;

				Iterator x = apDrs.iterator();
				while(x.hasNext()){

					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)x.next();
					if(apDistributionRecord.getDrClass().equals("PAYABLE")){
						SALES_AMOUNT += apDistributionRecord.getDrAmount();
					}

					if(apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("SERVICES")){
						SERVICES += apDistributionRecord.getDrAmount();
					}

					if(apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("CAPITAL GOODS")){
						CAPITAL_GOODS += apDistributionRecord.getDrAmount();
					}

					if(apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("OTHER CAPITAL GOODS")){
						OTHER_CAPITAL_GOODS += apDistributionRecord.getDrAmount();
					}

				}

				System.out.println("SALES_AMOUNT="+SALES_AMOUNT);
				double NET_AMNT = EJBCommon.roundIt(SALES_AMOUNT / ((apVoucher.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT);
				details.setTiDocumentType("AP VOUCHER");
				details.setTiSource("AP");
				details.setTiNetAmount(NET_AMNT);
				details.setTiTaxAmount(EJBCommon.roundIt(SALES_AMOUNT - NET_AMNT,PRECISION_UNIT));
				details.setTiSalesAmount(EJBCommon.roundIt(SALES_AMOUNT, PRECISION_UNIT));
				details.setTiServicesAmount(EJBCommon.roundIt(SERVICES * ((apVoucher.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT));
				details.setTiCapitalGoodsAmount(EJBCommon.roundIt(CAPITAL_GOODS * ((apVoucher.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT));
				details.setTiOtherCapitalGoodsAmount(EJBCommon.roundIt(OTHER_CAPITAL_GOODS * ((apVoucher.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT));
				details.setTiTxnCode(apVoucher.getVouCode());
				details.setTiTxnDate(apVoucher.getVouDate());
				details.setTiTxnDocumentNumber(apVoucher.getVouDocumentNumber());
				details.setTiTxnReferenceNumber(apVoucher.getVouReferenceNumber());
				details.setTiTaxExempt(EJBCommon.roundIt(apVoucher.getApTaxCode().getTcName().equals("EXEMPT") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
				details.setTiTaxZeroRated(EJBCommon.roundIt(apVoucher.getApTaxCode().getTcName().equals("ZERO-RATED") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
				details.setTiTxlCode(0);
				details.setTiTxlCoaCode(apVoucher.getApTaxCode().getGlChartOfAccount() != null ?
						apVoucher.getApTaxCode().getGlChartOfAccount().getCoaCode() : null);
				details.setTiTcCode(apVoucher.getApTaxCode().getTcCode());
				details.setTiWtcCode(null);
				details.setTiSlCode(apVoucher.getApSupplier().getSplCode());
				details.setTiSlTin(apVoucher.getApSupplier().getSplTin());
				details.setTiSlSubledgerCode(apVoucher.getApSupplier().getSplSupplierCode());
				details.setTiSlName(apVoucher.getApSupplier().getSplName());
				details.setTiSlAddress(apVoucher.getApSupplier().getSplAddress());
				details.setTiSlAddress2(apVoucher.getApSupplier().getSplCity() +" "+apVoucher.getApSupplier().getSplStateProvince());

				details.setTiIsArDocument(EJBCommon.FALSE);
				details.setTiAdBranch(AD_BRNCH);
				details.setTiAdCompany(AD_CMPNY);



				this.createTaxInterface(details);


			}



    		// Create tax interface for Officer and Employees voucher per line items

    		Collection apVoucherOAEs = apVoucherHome.findPostedTaxVouByVouOaeDateRange(
    				EJBCommon.FALSE, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);


    		Iterator i = apVoucherOAEs.iterator();

    		while(i.hasNext()) {

    			LocalApVoucher apVoucher = (LocalApVoucher)i.next();

    			removeExisting("AP VOUCHER", apVoucher.getVouCode(), AD_CMPNY);


				if(apVoucher.getApVoucherLineItems().size() > 0){

					Collection apVlis = apVoucher.getApVoucherLineItems();

					Iterator x = apVlis.iterator();

					while(x.hasNext()){

						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)x.next();

						// create tax interface

						GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();

						if(apVoucherLineItem.getVliTax() == EJBCommon.FALSE) continue;

						double SALES_AMOUNT = EJBCommon.roundIt(apVoucherLineItem.getVliAmount() + apVoucherLineItem.getVliTaxAmount(), PRECISION_UNIT);
						double NET_AMNT = EJBCommon.roundIt(SALES_AMOUNT / ((apVoucher.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT);
						double TAX_AMNT = EJBCommon.roundIt(SALES_AMOUNT - NET_AMNT,PRECISION_UNIT);

						details.setTiDocumentType("AP VOUCHER");
						details.setTiSource("AP");
						details.setTiNetAmount(NET_AMNT);
						details.setTiTaxAmount(TAX_AMNT);
						details.setTiSalesAmount(SALES_AMOUNT);

						LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCode(apVoucherLineItem.getInvItemLocation().getIlGlCoaInventoryAccount(), AD_CMPNY);

						double SERVICES = glChartOfAccount.getCoaTaxType().equals("SERVICES") ? TAX_AMNT : 0d;
						double CAPITAL_GOODS = glChartOfAccount.getCoaTaxType().equals("CAPITAL GOODS") ? TAX_AMNT : 0d;
						double OTHER_CAPITAL_GOODS = glChartOfAccount.getCoaTaxType().equals("OTHER CAPITAL GOODS") ? TAX_AMNT : 0d;

						details.setTiServicesAmount(SERVICES);
						details.setTiCapitalGoodsAmount(CAPITAL_GOODS);
						details.setTiOtherCapitalGoodsAmount(OTHER_CAPITAL_GOODS);

						details.setTiTxnCode(apVoucher.getVouCode());
						details.setTiTxnDate(apVoucher.getVouDate());
						details.setTiTxnDocumentNumber(apVoucher.getVouDocumentNumber());
						details.setTiTxnReferenceNumber(apVoucher.getVouReferenceNumber());
						details.setTiTaxExempt(EJBCommon.roundIt(apVoucher.getApTaxCode().getTcName().equals("EXEMPT") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
						details.setTiTaxZeroRated(EJBCommon.roundIt(apVoucher.getApTaxCode().getTcName().equals("ZERO-RATED") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
						details.setTiTxlCode(0);
						details.setTiTxlCoaCode(apVoucher.getApTaxCode().getGlChartOfAccount() != null ?
								apVoucher.getApTaxCode().getGlChartOfAccount().getCoaCode() : null);
						details.setTiTcCode(apVoucher.getApTaxCode().getTcCode());
						details.setTiWtcCode(null);

						details.setTiSlTin(apVoucher.getApSupplier().getSplTin());
						details.setTiSlSubledgerCode(apVoucher.getApSupplier().getSplSupplierCode());

						details.setTiSlCode(null);
						details.setTiSlSubledgerCode(apVoucherLineItem.getVliSplName());
						details.setTiSlName(apVoucherLineItem.getVliSplName());
						details.setTiSlTin(apVoucherLineItem.getVliSplTin());
						details.setTiSlAddress2(apVoucherLineItem.getVliSplAddress());

						details.setTiIsArDocument(EJBCommon.FALSE);
						details.setTiAdBranch(AD_BRNCH);
						details.setTiAdCompany(AD_CMPNY);

						this.createTaxInterface(details);

					}


				}



    		}


    		}



    		if(DOC_TYP.equals("AP DEBIT MEMO") || DOC_TYP.equals("")) {
    			System.out.println("AP DEBIT MEMO");

    			Collection apDebitMemos = apVoucherHome.findPostedTaxVouByVouDateRange(
        				EJBCommon.TRUE, DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);


        		Iterator i = apDebitMemos.iterator();

        		while(i.hasNext()) {

        			LocalApVoucher apVoucher = (LocalApVoucher)i.next();

        			removeExisting("AP DEBIT MEMO", apVoucher.getVouCode(), AD_CMPNY);

        			// create tax interface

    				GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();


    				Collection apDrs = apVoucher.getApDistributionRecords();

    				double SALES_AMOUNT = 0d;
    				double SERVICES = 0d;
    				double CAPITAL_GOODS = 0d;
    				double OTHER_CAPITAL_GOODS = 0d;

    				Iterator x = apDrs.iterator();
    				while(x.hasNext()){

    					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)x.next();
    					if(apDistributionRecord.getDrClass().equals("PAYABLE")){
    						SALES_AMOUNT += apDistributionRecord.getDrAmount();
    					}

    					if(apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("SERVICES")){
    						SERVICES = apDistributionRecord.getDrAmount();
    					}

    					if(apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("CAPITAL GOODS")){
    						CAPITAL_GOODS = apDistributionRecord.getDrAmount();
    					}

    					if(apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("OTHER CAPITAL GOODS")){
    						OTHER_CAPITAL_GOODS = apDistributionRecord.getDrAmount();
    					}

    				}

    				System.out.println("SALES_AMOUNT="+SALES_AMOUNT);
    				double NET_AMNT = EJBCommon.roundIt(SALES_AMOUNT / ((apVoucher.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT);
    				details.setTiDocumentType("AP DEBIT MEMO");
    				details.setTiSource("AP");
    				details.setTiNetAmount(NET_AMNT);
    				details.setTiTaxAmount(EJBCommon.roundIt(SALES_AMOUNT - NET_AMNT,PRECISION_UNIT));
    				details.setTiSalesAmount(EJBCommon.roundIt(SALES_AMOUNT, PRECISION_UNIT));
    				details.setTiServicesAmount(EJBCommon.roundIt(SERVICES * ((apVoucher.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT));
    				details.setTiCapitalGoodsAmount(EJBCommon.roundIt(CAPITAL_GOODS * ((apVoucher.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT));
    				details.setTiOtherCapitalGoodsAmount(EJBCommon.roundIt(OTHER_CAPITAL_GOODS * ((apVoucher.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT));
    				details.setTiTxnCode(apVoucher.getVouCode());
    				details.setTiTxnDate(apVoucher.getVouDate());
    				details.setTiTxnDocumentNumber(apVoucher.getVouDocumentNumber());
    				details.setTiTxnReferenceNumber(apVoucher.getVouReferenceNumber());
    				details.setTiTaxExempt(EJBCommon.roundIt(apVoucher.getApTaxCode().getTcName().equals("EXEMPT") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTaxZeroRated(EJBCommon.roundIt(apVoucher.getApTaxCode().getTcName().equals("ZERO-RATED") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTxlCode(0);
    				details.setTiTxlCoaCode(apVoucher.getApTaxCode().getGlChartOfAccount() != null ?
    						apVoucher.getApTaxCode().getGlChartOfAccount().getCoaCode() : null);
    				details.setTiTcCode(apVoucher.getApTaxCode().getTcCode());
    				details.setTiWtcCode(null);
    				details.setTiSlCode(apVoucher.getApSupplier().getSplCode());
    				details.setTiSlTin(apVoucher.getApSupplier().getSplTin());
    				details.setTiSlSubledgerCode(apVoucher.getApSupplier().getSplSupplierCode());
    				details.setTiSlName(apVoucher.getApSupplier().getSplName());
    				details.setTiSlAddress(apVoucher.getApSupplier().getSplAddress());
    				details.setTiSlAddress2(apVoucher.getApSupplier().getSplCity() +" "+apVoucher.getApSupplier().getSplStateProvince());

    				details.setTiIsArDocument(EJBCommon.FALSE);
    				details.setTiAdBranch(AD_BRNCH);
    				details.setTiAdCompany(AD_CMPNY);



    				this.createTaxInterface(details);



        		}


        		}


    		if(DOC_TYP.equals("AP CHECK") || DOC_TYP.equals("")) {
    			System.out.println("AP CHECK");

        		Collection apChecks = apCheckHome.findPostedTaxDirectChkByChkDateRange(
        				DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);

        		Iterator i = apChecks.iterator();

        		while(i.hasNext()) {

        			LocalApCheck apCheck = (LocalApCheck)i.next();

        			removeExisting("AP CHECK", apCheck.getChkCode(), AD_CMPNY);

        			// create tax interface

    				GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();


    				Collection apDrs = apCheck.getApDistributionRecords();

    				double SALES_AMOUNT = 0d;
    				double SERVICES = 0d;
    				double CAPITAL_GOODS = 0d;
    				double OTHER_CAPITAL_GOODS = 0d;

    				Iterator x = apDrs.iterator();
    				while(x.hasNext()){

    					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)x.next();

    					if(apDistributionRecord.getDrClass().equals("CASH")){
    						SALES_AMOUNT += apDistributionRecord.getDrAmount();
    					}

    					if(apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("SERVICES")){
    						SERVICES += apDistributionRecord.getDrAmount();
    					}

    					if(apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("CAPITAL GOODS")){
    						CAPITAL_GOODS += apDistributionRecord.getDrAmount();
    					}

    					if(apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("OTHER CAPITAL GOODS")){
    						OTHER_CAPITAL_GOODS += apDistributionRecord.getDrAmount();
    					}

    				}

    				System.out.println("SALES_AMOUNT="+SALES_AMOUNT);
    				System.out.println("SERVICES="+SERVICES);
    				System.out.println("CAPITAL_GOODS="+CAPITAL_GOODS);
    				System.out.println("OTHER_CAPITAL_GOODS="+OTHER_CAPITAL_GOODS);

    				double NET_AMNT = EJBCommon.roundIt(SALES_AMOUNT / ((apCheck.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT);
    				details.setTiDocumentType("AP CHECK");
    				details.setTiSource("AP");
    				details.setTiNetAmount(NET_AMNT);
    				details.setTiTaxAmount(EJBCommon.roundIt(SALES_AMOUNT - NET_AMNT,PRECISION_UNIT));
    				details.setTiSalesAmount(EJBCommon.roundIt(SALES_AMOUNT, PRECISION_UNIT));
    				details.setTiServicesAmount(EJBCommon.roundIt(SERVICES * ((apCheck.getApTaxCode().getTcRate()/100) + 1 ), PRECISION_UNIT));
    				details.setTiCapitalGoodsAmount(EJBCommon.roundIt(CAPITAL_GOODS * ((apCheck.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT));
    				details.setTiOtherCapitalGoodsAmount(EJBCommon.roundIt(OTHER_CAPITAL_GOODS * ((apCheck.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT));
    				details.setTiTxnCode(apCheck.getChkCode());
    				details.setTiTxnDate(apCheck.getChkDate());
    				details.setTiTxnDocumentNumber(apCheck.getChkDocumentNumber());
    				details.setTiTxnReferenceNumber(apCheck.getChkReferenceNumber());
    				details.setTiTaxExempt(EJBCommon.roundIt(apCheck.getApTaxCode().getTcName().equals("EXEMPT") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTaxZeroRated(EJBCommon.roundIt(apCheck.getApTaxCode().getTcName().equals("ZERO-RATED") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTxlCode(0);
    				details.setTiTxlCoaCode(apCheck.getApTaxCode().getGlChartOfAccount() != null ?
    						apCheck.getApTaxCode().getGlChartOfAccount().getCoaCode() : null);
    				details.setTiTcCode(apCheck.getApTaxCode().getTcCode());
    				details.setTiWtcCode(null);
    				details.setTiSlCode(apCheck.getApSupplier().getSplCode());
    				details.setTiSlTin(apCheck.getApSupplier().getSplTin());
    				details.setTiSlSubledgerCode(apCheck.getApSupplier().getSplSupplierCode());
    				details.setTiSlName(apCheck.getApSupplier().getSplName());
    				details.setTiSlAddress(apCheck.getApSupplier().getSplAddress());
    				details.setTiSlAddress2(apCheck.getApSupplier().getSplCity() +" "+apCheck.getApSupplier().getSplStateProvince());


    				details.setTiIsArDocument(EJBCommon.FALSE);
    				details.setTiAdBranch(AD_BRNCH);
    				details.setTiAdCompany(AD_CMPNY);



    				this.createTaxInterface(details);



        		}


        		}


    		if(DOC_TYP.equals("AP RECEIVING ITEM") || DOC_TYP.equals("")) {
    			System.out.println("AP RECEIVING ITEM");
    			Collection apReceivings = apReceivingItemHome.findPostedTaxPoByVouDateRange(DT_FRM, DT_TO, AD_BRNCH, AD_CMPNY);

        		Iterator i = apReceivings.iterator();

        		while(i.hasNext()) {

        			LocalApPurchaseOrder apReceivingItem = (LocalApPurchaseOrder)i.next();

        			removeExisting("AP RECEIVING ITEM", apReceivingItem.getPoCode(), AD_CMPNY);

        			// create tax interface

    				GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();


    				Collection apDrs = apReceivingItem.getApDistributionRecords();

    				double SALES_AMOUNT = 0d;
    				double SERVICES = 0d;
    				double CAPITAL_GOODS = 0d;
    				double OTHER_CAPITAL_GOODS = 0d;

    				Iterator x = apDrs.iterator();
    				while(x.hasNext()){

    					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)x.next();
    					System.out.println("COA="+apDistributionRecord.getGlChartOfAccount().getCoaAccountNumber());
    					System.out.println("apDistributionRecord.getDrClass()="+apDistributionRecord.getDrClass());
    					if(apDistributionRecord.getDrClass().equals("ACC INV")){
    						SALES_AMOUNT += apDistributionRecord.getDrAmount();
    					}

    					if(apDistributionRecord.getDrClass().equals("INVENTORY") && apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("SERVICES")){
    						SERVICES += apDistributionRecord.getDrAmount();
    					}

    					if(apDistributionRecord.getDrClass().equals("INVENTORY") && apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("CAPITAL GOODS")){
    						CAPITAL_GOODS += apDistributionRecord.getDrAmount();
    					}

    					if(apDistributionRecord.getDrClass().equals("INVENTORY") && apDistributionRecord.getGlChartOfAccount().getCoaTaxType().equals("OTHER CAPITAL GOODS")){
    						OTHER_CAPITAL_GOODS += apDistributionRecord.getDrAmount();
    					}

    				}

    				System.out.println("SALES_AMOUNT="+SALES_AMOUNT);
    				double NET_AMNT = EJBCommon.roundIt(SALES_AMOUNT / ((apReceivingItem.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT);
    				details.setTiDocumentType("AP RECEIVING ITEM");
    				details.setTiSource("AP");
    				details.setTiNetAmount(NET_AMNT);
    				details.setTiTaxAmount(EJBCommon.roundIt(SALES_AMOUNT - NET_AMNT,PRECISION_UNIT));
    				details.setTiSalesAmount(EJBCommon.roundIt(SALES_AMOUNT, PRECISION_UNIT));
    				details.setTiServicesAmount(EJBCommon.roundIt(SERVICES * ((apReceivingItem.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT));
    				details.setTiCapitalGoodsAmount(EJBCommon.roundIt(CAPITAL_GOODS * ((apReceivingItem.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT));
    				details.setTiOtherCapitalGoodsAmount(EJBCommon.roundIt(OTHER_CAPITAL_GOODS * ((apReceivingItem.getApTaxCode().getTcRate()/100) + 1), PRECISION_UNIT));
    				details.setTiTxnCode(apReceivingItem.getPoCode());
    				details.setTiTxnDate(apReceivingItem.getPoDate());
    				details.setTiTxnDocumentNumber(apReceivingItem.getPoDocumentNumber());
    				details.setTiTxnReferenceNumber(apReceivingItem.getPoReferenceNumber());
    				details.setTiTaxExempt(EJBCommon.roundIt(apReceivingItem.getApTaxCode().getTcName().equals("EXEMPT") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTaxZeroRated(EJBCommon.roundIt(apReceivingItem.getApTaxCode().getTcName().equals("ZERO-RATED") ? SALES_AMOUNT : 0d, PRECISION_UNIT));
    				details.setTiTxlCode(0);
    				details.setTiTxlCoaCode(apReceivingItem.getApTaxCode().getGlChartOfAccount() != null ?
    						apReceivingItem.getApTaxCode().getGlChartOfAccount().getCoaCode() : null);
    				details.setTiTcCode(apReceivingItem.getApTaxCode().getTcCode());
    				details.setTiWtcCode(null);
    				details.setTiSlCode(apReceivingItem.getApSupplier().getSplCode());
    				details.setTiSlTin(apReceivingItem.getApSupplier().getSplTin());
    				details.setTiSlSubledgerCode(apReceivingItem.getApSupplier().getSplSupplierCode());
    				details.setTiSlName(apReceivingItem.getApSupplier().getSplName());
    				details.setTiSlAddress(apReceivingItem.getApSupplier().getSplAddress());
    				details.setTiSlAddress2(apReceivingItem.getApSupplier().getSplCity() +" "+apReceivingItem.getApSupplier().getSplStateProvince());

    				details.setTiIsArDocument(EJBCommon.FALSE);
    				details.setTiAdBranch(AD_BRNCH);
    				details.setTiAdCompany(AD_CMPNY);



    				this.createTaxInterface(details);



        		}


        		}




    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void runApWithholdingTaxCode(String DOC_TYP, Date DT_FRM, Date DT_TO, Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("GlTaxInterfaceRunControllerBean apWithholdingTaxCodeInteraface");

    	LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalApDistributionRecordHome apDistributionRecordHome = null;
    	LocalApVoucherHome apVoucherHome = null;

    	// Initialize EJB Home

    	try {

    		apWithholdingTaxCodeHome = (LocalApWithholdingTaxCodeHome)EJBHomeFactory.
				lookUpLocalHome(LocalApWithholdingTaxCodeHome.JNDI_NAME, LocalApWithholdingTaxCodeHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
    		apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	// ap withholding tax
    	short PRECISION_UNIT = this.getGlFcPrecisionUnit(AD_CMPNY);

    	try {

    		Collection apWithholdingTaxCodes = apWithholdingTaxCodeHome.findEnabledWtcAll(AD_CMPNY);

    		Iterator i = apWithholdingTaxCodes.iterator();

    		while(i.hasNext()) {

    			LocalApWithholdingTaxCode apWithholdingTaxCode = (LocalApWithholdingTaxCode)i.next();
    			LocalGlChartOfAccount glChartOfAccount = apWithholdingTaxCode.getGlChartOfAccount();

    			if(glChartOfAccount != null) {

    				// AP VOUCHER

    				if(DOC_TYP.equals("AP VOUCHER") || DOC_TYP.equals("")) {

    					Collection apDistributionRecords = apDistributionRecordHome.findVouByDateRangeAndCoaAccountNumber(
    						EJBCommon.FALSE, DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    					Iterator drItr = apDistributionRecords.iterator();

    					while(drItr.hasNext()) {

    						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)drItr.next();



    						Integer voucherWtcCode = null;
    						if(apDistributionRecord.getApVoucher().getApWithholdingTaxCode()!= null) {

    							voucherWtcCode = apDistributionRecord.getApVoucher().getApWithholdingTaxCode().getWtcCode();

    						}

    						if((voucherWtcCode!= null && voucherWtcCode.equals(apWithholdingTaxCode.getWtcCode()))) {
    							removeExisting("AP VOUCHER", apDistributionRecord.getDrCode(), AD_CMPNY);
    							double DR_AMNT = this.convertForeignToFunctionalCurrency(
    								apDistributionRecord.getApVoucher().getGlFunctionalCurrency().getFcCode(),
    								apDistributionRecord.getApVoucher().getGlFunctionalCurrency().getFcName(),
    								apDistributionRecord.getApVoucher().getVouConversionDate(),
    								apDistributionRecord.getApVoucher().getVouConversionRate(),
    								apDistributionRecord.getDrAmount(), AD_CMPNY);

    							if (apWithholdingTaxCode.getWtcRate() == 0) continue;

    							double NET_AMNT = DR_AMNT / (apWithholdingTaxCode.getWtcRate()/100);

    							GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    							details.setTiDocumentType("AP VOUCHER");
    							details.setTiSource("AP");
    							details.setTiNetAmount(NET_AMNT);
    							details.setTiSalesAmount(EJBCommon.roundIt(NET_AMNT + DR_AMNT, PRECISION_UNIT));
    							details.setTiTxnCode(apDistributionRecord.getApVoucher().getVouCode());
    							details.setTiTxnDate(apDistributionRecord.getApVoucher().getVouDate());
    							details.setTiTxnDocumentNumber(apDistributionRecord.getApVoucher().getVouDocumentNumber());
    							details.setTiTxnReferenceNumber(apDistributionRecord.getApVoucher().getVouReferenceNumber());
    							details.setTiTxlCode(apDistributionRecord.getDrCode());
    							details.setTiTxlCoaCode(apDistributionRecord.getGlChartOfAccount().getCoaCode());
    							details.setTiTcCode(null);
    							details.setTiWtcCode(voucherWtcCode);
    							details.setTiSlCode(apDistributionRecord.getApVoucher().getApSupplier().getSplCode());
    							details.setTiSlTin(apDistributionRecord.getApVoucher().getApSupplier().getSplTin());
    							details.setTiSlSubledgerCode(apDistributionRecord.getApVoucher().getApSupplier().getSplSupplierCode());
    							details.setTiSlName(apDistributionRecord.getApVoucher().getApSupplier().getSplName());
    							details.setTiSlAddress(apDistributionRecord.getApVoucher().getApSupplier().getSplAddress());
    							details.setTiIsArDocument(EJBCommon.FALSE);
    							details.setTiAdBranch(AD_BRNCH);
    							details.setTiAdCompany(AD_CMPNY);

    							this.createTaxInterface(details);

    						}

    					}

    				}

    				// AP DEBIT MEMO

    				if(DOC_TYP.equals("AP DEBIT MEMO") || DOC_TYP.equals("")) {

    					Collection apDistributionRecords = apDistributionRecordHome.findVouByDateRangeAndCoaAccountNumber(
    						EJBCommon.TRUE, DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    					Iterator drItr = apDistributionRecords.iterator();

    					while(drItr.hasNext()) {

    						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)drItr.next();
    						LocalApVoucher apVoucher = apVoucherHome.findByVouDocumentNumberAndVouDebitMemoAndBrCode(
    								apDistributionRecord.getApVoucher().getVouDmVoucherNumber(),
									EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);



    						Integer debitmemoWtcCode = null;
    						if(apVoucher.getApWithholdingTaxCode()!= null) {

    							debitmemoWtcCode = apVoucher.getApWithholdingTaxCode().getWtcCode();

    						}

    						if((debitmemoWtcCode!= null && debitmemoWtcCode.equals(apWithholdingTaxCode.getWtcCode()))) {
    							removeExisting("AP DEBIT MEMO", apDistributionRecord.getDrCode(), AD_CMPNY);
    							double DR_AMNT = this.convertForeignToFunctionalCurrency(
    								apVoucher.getGlFunctionalCurrency().getFcCode(),
    								apVoucher.getGlFunctionalCurrency().getFcName(),
    								apVoucher.getVouConversionDate(),
									apVoucher.getVouConversionRate(),
    								apDistributionRecord.getDrAmount(), AD_CMPNY);

    							if (apWithholdingTaxCode.getWtcRate() == 0) continue;

    							double NET_AMNT = DR_AMNT / (apWithholdingTaxCode.getWtcRate()/100);

    							GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    							details.setTiDocumentType("AP DEBIT MEMO");
    							details.setTiSource("AP");
    							details.setTiNetAmount(NET_AMNT);
    							details.setTiSalesAmount(EJBCommon.roundIt(NET_AMNT + DR_AMNT, PRECISION_UNIT));
    							details.setTiTxnCode(apDistributionRecord.getApVoucher().getVouCode());
    							details.setTiTxnDate(apDistributionRecord.getApVoucher().getVouDate());
    							details.setTiTxnDocumentNumber(apDistributionRecord.getApVoucher().getVouDocumentNumber());
    							details.setTiTxnReferenceNumber(apDistributionRecord.getApVoucher().getVouDmVoucherNumber());
    							details.setTiTxlCode(apDistributionRecord.getDrCode());
    							details.setTiTxlCoaCode(apDistributionRecord.getGlChartOfAccount().getCoaCode());
    							details.setTiTcCode(null);
    							details.setTiWtcCode(debitmemoWtcCode);
    							details.setTiSlCode(apDistributionRecord.getApVoucher().getApSupplier().getSplCode());
    							details.setTiSlTin(apDistributionRecord.getApVoucher().getApSupplier().getSplTin());
    							details.setTiSlSubledgerCode(apDistributionRecord.getApVoucher().getApSupplier().getSplSupplierCode());
    							details.setTiSlName(apDistributionRecord.getApVoucher().getApSupplier().getSplName());
    							details.setTiSlAddress(apDistributionRecord.getApVoucher().getApSupplier().getSplAddress());
    							details.setTiIsArDocument(EJBCommon.FALSE);
    							details.setTiAdBranch(AD_BRNCH);
    							details.setTiAdCompany(AD_CMPNY);

    							this.createTaxInterface(details);

    						}

    					}

    				}

    				// AP CHECK

    				if(DOC_TYP.equals("AP CHECK") || DOC_TYP.equals("")) {

    					Collection apDistributionRecords = apDistributionRecordHome.findChkByDateRangeAndCoaAccountNumber(
    						DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    					Iterator drItr = apDistributionRecords.iterator();

    					while(drItr.hasNext()) {

    						LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)drItr.next();



    						Integer checkWtcCode = null;
    						if(apDistributionRecord.getApCheck().getApWithholdingTaxCode()!= null) {

    							checkWtcCode = apDistributionRecord.getApCheck().getApWithholdingTaxCode().getWtcCode();

    						}

    						Integer appliedWtcCode = null;
    						if(apDistributionRecord.getApAppliedVoucher() != null) {

    							appliedWtcCode = apDistributionRecord.getApAppliedVoucher().getApVoucherPaymentSchedule().getApVoucher().getApWithholdingTaxCode().getWtcCode();

    						}

    						if(((checkWtcCode!= null && checkWtcCode.equals(apWithholdingTaxCode.getWtcCode()))) ||
    								(appliedWtcCode!= null && appliedWtcCode.equals(apWithholdingTaxCode.getWtcCode()))){
    							removeExisting("AP CHECK", apDistributionRecord.getDrCode(), AD_CMPNY);
    							LocalApAppliedVoucher apAppliedVoucher = apDistributionRecord.getApAppliedVoucher();
    							LocalApVoucher apVoucher = null;

    							double DR_AMNT = 0d;
    							if (apAppliedVoucher == null) {

    								DR_AMNT = this.convertForeignToFunctionalCurrency(
    									apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcCode(),
    									apDistributionRecord.getApCheck().getGlFunctionalCurrency().getFcName(),
    									apDistributionRecord.getApCheck().getChkConversionDate(),
    									apDistributionRecord.getApCheck().getChkConversionRate(),
    									apDistributionRecord.getDrAmount(), AD_CMPNY);

    							} else {

    								apVoucher = apAppliedVoucher.getApVoucherPaymentSchedule().getApVoucher();

    								DR_AMNT = this.convertForeignToFunctionalCurrency(
        								apVoucher.getGlFunctionalCurrency().getFcCode(),
        								apVoucher.getGlFunctionalCurrency().getFcName(),
        								apVoucher.getVouConversionDate(),
        								apVoucher.getVouConversionRate(),
        								apDistributionRecord.getDrAmount(), AD_CMPNY);

    							}

    							if (apWithholdingTaxCode.getWtcRate() == 0) continue;

    							double NET_AMNT = DR_AMNT / (apWithholdingTaxCode.getWtcRate()/100);

    							GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    							details.setTiDocumentType("AP CHECK");
    							details.setTiSource("AP");
    							details.setTiNetAmount(NET_AMNT);
    							details.setTiSalesAmount(EJBCommon.roundIt(NET_AMNT + DR_AMNT, PRECISION_UNIT));
    							details.setTiTxnCode(apDistributionRecord.getApCheck().getChkCode());
    							details.setTiTxnDate(apDistributionRecord.getApCheck().getChkDate());
    							details.setTiTxnDocumentNumber(apDistributionRecord.getApCheck().getChkDocumentNumber());
    							details.setTiTxnReferenceNumber(apDistributionRecord.getApCheck().getChkReferenceNumber());
    							details.setTiTxlCode(apDistributionRecord.getDrCode());
    							details.setTiTxlCoaCode(apDistributionRecord.getGlChartOfAccount().getCoaCode());
    							details.setTiTcCode(null);
    							if (apVoucher == null) {

    								details.setTiWtcCode(checkWtcCode);

    							} else {

    								details.setTiWtcCode(appliedWtcCode);

    							}
    							details.setTiSlCode(apDistributionRecord.getApCheck().getApSupplier().getSplCode());
    							details.setTiSlTin(apDistributionRecord.getApCheck().getApSupplier().getSplTin());
    							details.setTiSlSubledgerCode(apDistributionRecord.getApCheck().getApSupplier().getSplSupplierCode());
    							details.setTiSlName(apDistributionRecord.getApCheck().getApSupplier().getSplName());
    							details.setTiSlAddress(apDistributionRecord.getApCheck().getApSupplier().getSplAddress());
    							details.setTiIsArDocument(EJBCommon.FALSE);
    							details.setTiAdBranch(AD_BRNCH);
    							details.setTiAdCompany(AD_CMPNY);

    							this.createTaxInterface(details);

    						}

    					}

    				}

    				// GL JOURNAL

    				Collection glJournalLines = glJournalLineHome.findManualJrByEffectiveDateRangeAndCoaCode(
    					DT_FRM, DT_TO, glChartOfAccount.getCoaCode(), AD_CMPNY);

    				Iterator jlItr = glJournalLines.iterator();

    				while(jlItr.hasNext()) {

    					LocalGlJournalLine glJournalLine = (LocalGlJournalLine)jlItr.next();

    					removeExisting("GL JOURNAL", glJournalLine.getJlCode(), AD_CMPNY);

    					//if(!existing) {

    						GlTaxInterfaceDetails details = new GlTaxInterfaceDetails();
    						details.setTiDocumentType("GL JOURNAL");
    						details.setTiSource("GL");
    						details.setTiNetAmount(0d);
    						details.setTiTxnCode(glJournalLine.getGlJournal().getJrCode());
    						details.setTiTxnDate(glJournalLine.getGlJournal().getJrEffectiveDate());
    						details.setTiTxnDocumentNumber(glJournalLine.getGlJournal().getJrDocumentNumber());
    						details.setTiTxnReferenceNumber(null);
    						details.setTiTxlCode(glJournalLine.getJlCode());
    						details.setTiTxlCoaCode(glJournalLine.getGlChartOfAccount().getCoaCode());
    						details.setTiTcCode(null);
    						details.setTiWtcCode(null);
    						details.setTiSlCode(null);
    						details.setTiSlTin(null);
    						details.setTiSlSubledgerCode(null);
    						details.setTiSlName(null);
    						details.setTiSlAddress(null);
    						details.setTiIsArDocument(EJBCommon.FALSE);
    						details.setTiAdBranch(AD_BRNCH);
    						details.setTiAdCompany(AD_CMPNY);

    						this.createTaxInterface(details);

    					//}

    				}

    			}

    		}

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private boolean checkExistence(String DOC_TYP, Integer TXL_CODE, Integer AD_CMPNY) {

    	Debug.print("GlTaxInterfaceRunControllerBean checkExistence");

    	LocalGlTaxInterfaceHome glTaxInterfaceHome = null;
    	boolean existing = true;

    	// Initialize EJB Home

    	try {

    		glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		glTaxInterfaceHome.findByTiDocumentTypeAndTxlCode(DOC_TYP, TXL_CODE, AD_CMPNY);

    	} catch(Exception e) {

    		existing = false;

    	}

    	return existing;


    }

    private void removeExisting(String DOC_TYP, Integer TXN_CODE, Integer AD_CMPNY) {

    	Debug.print("GlTaxInterfaceRunControllerBean removeExisting");

    	LocalGlTaxInterfaceHome glTaxInterfaceHome = null;

    	// Initialize EJB Home

    	try {

    		glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		Collection glTaxInterfaces = glTaxInterfaceHome.findAllByTiDocumentTypeAndTxnCode(DOC_TYP, TXN_CODE, AD_CMPNY);

    		Iterator i = glTaxInterfaces.iterator();

    		while(i.hasNext()){
    			LocalGlTaxInterface glTaxInterface = (LocalGlTaxInterface)i.next();
    			glTaxInterface.remove();

    		}

    	} catch(Exception e) {





    	}


    }

    private void createTaxInterface(GlTaxInterfaceDetails details) {

    	Debug.print("GlTaxInterfaceRunControllerBean createTaxInteraface");

    	LocalGlTaxInterfaceHome glTaxInterfaceHome = null;

    	try {

    		glTaxInterfaceHome = (LocalGlTaxInterfaceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlTaxInterfaceHome.JNDI_NAME, LocalGlTaxInterfaceHome.class);

    	} catch (NamingException ex) {

    		System.out.println("Something wnet wrong 1.");
    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		System.out.println("txlcode is: " + details.getTiTxlCode());
    		LocalGlTaxInterface glTaxInterface = glTaxInterfaceHome.create(details.getTiDocumentType(), details.getTiSource(), details.getTiNetAmount(), details.getTiTaxAmount(),
    				details.getTiSalesAmount(), details.getTiServicesAmount(), details.getTiCapitalGoodsAmount(), details.getTiOtherCapitalGoodsAmount(),
    				details.getTiTxnCode(), details.getTiTxnDate(), details.getTiTxnDocumentNumber(), details.getTiTxnReferenceNumber(),
    				details.getTiTaxExempt(), details.getTiTaxZeroRated(),
    				details.getTiTxlCode(), details.getTiTxlCoaCode(), details.getTiTcCode(), details.getTiWtcCode(),
    				details.getTiSlCode(), details.getTiSlTin(), details.getTiSlSubledgerCode(), details.getTiSlName(),
    				details.getTiSlAddress(), details.getTiSlAddress2(), details.getTiIsArDocument(), details.getTiAdBranch(), details.getTiAdCompany());

    		System.out.println("Create at Code: " + glTaxInterface.getTiCode());


    	} catch (Exception ex) {
    		System.out.println("Something wnet wrong 2.");
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
    		Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

    	Debug.print("GlTaxInterfaceRunControllerBean convertForeignToFunctionalCurrency");


    	LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

    	LocalAdCompany adCompany = null;

    	// Initialize EJB Homes

    	try {

    		glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	// get company and extended precision

    	try {

    		adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    	} catch (Exception ex) {

    		throw new EJBException(ex.getMessage());

    	}


    	// Convert to functional currency if necessary

    	if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

    		AMOUNT = AMOUNT * CONVERSION_RATE;

    	} else if (CONVERSION_DATE != null) {

    		try {

    			// Get functional currency rate

    			LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;

    			if (!FC_NM.equals("USD")) {

    				glReceiptFunctionalCurrencyRate =
    					glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE,
    							CONVERSION_DATE, AD_CMPNY);

    				AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();

    			}

    			// Get set of book functional currency rate if necessary

    			if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

    				LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
    					glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
    							getFcCode(), CONVERSION_DATE, AD_CMPNY);

    				AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();

    			}


    		} catch (Exception ex) {

    			throw new EJBException(ex.getMessage());

    		}

    	}

    	return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

    }

 	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("GlCoaGeneratorControllerBean ejbCreate");

    }


}