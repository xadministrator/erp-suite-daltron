package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.exception.GlTCNoTransactionCalendarFoundException;
import com.ejb.exception.GlTCVNoTransactionCalendarValueFoundException;
import com.ejb.exception.GlTCVTransactionCalendarValueAlreadyDeletedException;
import com.ejb.gl.LocalGlTransactionCalendar;
import com.ejb.gl.LocalGlTransactionCalendarHome;
import com.ejb.gl.LocalGlTransactionCalendarValue;
import com.ejb.gl.LocalGlTransactionCalendarValueHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlTransactionCalendarDetails;
import com.util.GlTransactionCalendarValueDetails;

/**
 * @ejb:bean name="GlTransactionCalendarValueControllerEJB"
 *           display-name="Used for maintenance of transaction calendar details"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlTransactionCalendarValueControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlTransactionCalendarValueController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlTransactionCalendarValueControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/

public class GlTransactionCalendarValueControllerBean extends AbstractSessionBean {


   /**************************************************************************************************
      Business methods:

      (1) getGlTcAll - returns an ArrayList of all TCs with only name 
      		       in it

      (2) getGlTcDescriptionByGlTcName - returns the TC description of
      				         the TC name

      (3) getGlTcvByGlTcName - returns an ArrayList of all TCVs of a
      			       particular TC

      (4) loadGlTcvDefault - loads the default business day specified
      			     by the 7 boolean parameter and populates
			     one calendar year

      (5) updateGlTcvEntry - updates one row of a transaction calendar,
           		     only the boolean business day can be
			     modified

      Private methods:

      (1) addGlTransactionCalendarValueToGlTransactionCalendar - adds a transaction calendar value
      								 to a transaction calendar

   ***************************************************************************************************/

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlTcAll(Integer AD_CMPNY)
      throws GlTCNoTransactionCalendarFoundException {

      Debug.print("GlTransactionCalendarControllerValueBean getGlTcvAll");

      /*************************************************************
         Returns an ArrayList of all TCs
      *************************************************************/

      ArrayList tcAllList = new ArrayList();
      Collection glTransactionCalendars = null;
      LocalGlTransactionCalendarHome glTransactionCalendarHome = null;
      
	  // Initialize EJB Home
	    
	  try {
	        
	       glTransactionCalendarHome = (LocalGlTransactionCalendarHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlTransactionCalendarHome.JNDI_NAME, LocalGlTransactionCalendarHome.class);	           
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try {
         glTransactionCalendars = glTransactionCalendarHome.findTcAll(AD_CMPNY);
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      if (glTransactionCalendars.size() == 0)
         throw new GlTCNoTransactionCalendarFoundException();

      Iterator i = glTransactionCalendars.iterator();
      while (i.hasNext()) {
         LocalGlTransactionCalendar glTransactionCalendar =
	    (LocalGlTransactionCalendar) i.next();
	 GlTransactionCalendarDetails details = new GlTransactionCalendarDetails(
	    glTransactionCalendar.getTcName());
	 tcAllList.add(details);
      }

      return tcAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public GlTransactionCalendarDetails getGlTcDescriptionByGlTcName(String TC_NM, Integer AD_CMPNY)
      throws GlTCNoTransactionCalendarFoundException {

      Debug.print("GlTransactionCalendarControllerValueBean getGlTcDescriptionByGlTcName");

      /****************************************************************
         returns the description of the TC name passed
       ***************************************************************/

      LocalGlTransactionCalendar glTransactionCalendar = null;
      LocalGlTransactionCalendarHome glTransactionCalendarHome = null;
      
	  // Initialize EJB Home
	    
	  try {
	        
	       glTransactionCalendarHome = (LocalGlTransactionCalendarHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlTransactionCalendarHome.JNDI_NAME, LocalGlTransactionCalendarHome.class);	           
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try {
         glTransactionCalendar = glTransactionCalendarHome.findByTcName(TC_NM, AD_CMPNY);
      } catch (FinderException ex) {
         throw new GlTCNoTransactionCalendarFoundException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      return new GlTransactionCalendarDetails(glTransactionCalendar.getTcName(),
         glTransactionCalendar.getTcDescription());

   }

   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getGlTcvByGlTcName(String TC_NM, Integer AD_CMPNY)
      throws GlTCVNoTransactionCalendarValueFoundException,
      GlTCNoTransactionCalendarFoundException {

      Debug.print("GlTransactionCalendarControllerValueBean getGlTcvByGlTcName");

      /*****************************************************************
         return an ArrayList of all TCV of a specified TC
       ****************************************************************/

      LocalGlTransactionCalendar glTransactionCalendar = null;

      ArrayList tcvAllList = new ArrayList();
      Collection glTransactionCalendarValues = null;
      LocalGlTransactionCalendarHome glTransactionCalendarHome = null;
      
	  // Initialize EJB Home
	    
	  try {
	        
	       glTransactionCalendarHome = (LocalGlTransactionCalendarHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlTransactionCalendarHome.JNDI_NAME, LocalGlTransactionCalendarHome.class);	           
	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try {
         glTransactionCalendar = glTransactionCalendarHome.findByTcName(TC_NM, AD_CMPNY); 
      } catch (FinderException ex) {
         throw new GlTCNoTransactionCalendarFoundException();
      } catch (Exception ex) {
         throw new EJBException(ex.getMessage());
      }

      try {
         glTransactionCalendarValues =
	    glTransactionCalendar.getGlTransactionCalendarValues();
      } catch (Exception ex) {
          throw new EJBException(ex.getMessage());
      }

      if (glTransactionCalendarValues.size() == 0) 
         throw new GlTCVNoTransactionCalendarValueFoundException();
 
      Iterator i = glTransactionCalendarValues.iterator();
      while (i.hasNext()) {
         LocalGlTransactionCalendarValue glTransactionCalendarValue =
	    (LocalGlTransactionCalendarValue) i.next();

	 GlTransactionCalendarValueDetails details = new GlTransactionCalendarValueDetails(
	    glTransactionCalendarValue.getTcvCode(), glTransactionCalendarValue.getTcvDate(),
	    glTransactionCalendarValue.getTcvDayOfWeek(), glTransactionCalendarValue.getTcvBusinessDay());

	 tcvAllList.add(details);
      }

      return tcvAllList;
   }

   /**
   * @ejb:interface-method view-type="remote"
   **/
   public void updateGlTcvEntry(GlTransactionCalendarValueDetails details, Integer AD_CMPNY)
      throws GlTCVTransactionCalendarValueAlreadyDeletedException {

      Debug.print("GlTransactionCalendarValueControllerBean updateGlTcvEntry");

      /**************************************************
         Updates a particular TCV, only the boolean
	 business day can be modified
       *************************************************/

      LocalGlTransactionCalendarValue glTransactionCalendarValue = null;
      LocalGlTransactionCalendarValueHome glTransactionCalendarValueHome = null;
       
	  // Initialize EJB Home
	    
	  try {
	        
	       glTransactionCalendarValueHome = (LocalGlTransactionCalendarValueHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlTransactionCalendarValueHome.JNDI_NAME, LocalGlTransactionCalendarValueHome.class);	           

	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      try {
         glTransactionCalendarValue = glTransactionCalendarValueHome.findByPrimaryKey(
	    details.getTcvCode());
      } catch (Exception ex) {
         throw new GlTCVTransactionCalendarValueAlreadyDeletedException();
      }

      try {
         glTransactionCalendarValue.setTcvBusinessDay(details.getTcvBusinessDay());
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      }

   }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlTransactionCalendarControllerValueBean ejbCreate");

   }

   // private methods

   private void addGlTransactionCalendarValueToGlTransactionCalendar(Integer TCV_CODE, 
      Integer TC_CODE, Integer AD_CMPNY) {

      Debug.print("GlTransactionCalendarValueControllerBean addGlTransactionCalendarValueToGlTransactionCalendar");

      LocalGlTransactionCalendarHome glTransactionCalendarHome = null;
      LocalGlTransactionCalendarValueHome glTransactionCalendarValueHome = null;
       
	  // Initialize EJB Home
	    
	  try {
	        
	       glTransactionCalendarHome = (LocalGlTransactionCalendarHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlTransactionCalendarHome.JNDI_NAME, LocalGlTransactionCalendarHome.class);	           
	       glTransactionCalendarValueHome = (LocalGlTransactionCalendarValueHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlTransactionCalendarValueHome.JNDI_NAME, LocalGlTransactionCalendarValueHome.class);	           

	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }

      /*****************************************************
         Add relationship of transaction calendar value
	 to transaction calendar
       ****************************************************/

      try {
         LocalGlTransactionCalendarValue glTransactionCalendarValue =
	    glTransactionCalendarValueHome.findByPrimaryKey(TCV_CODE);
         LocalGlTransactionCalendar glTransactionCalendar =
            glTransactionCalendarHome.findByPrimaryKey(TC_CODE);
	 glTransactionCalendar.addGlTransactionCalendarValue(glTransactionCalendarValue);
      } catch (FinderException ex) {
         getSessionContext().setRollbackOnly();
         throw new EJBException(ex.getMessage());
      } catch (Exception ex) {
         getSessionContext().setRollbackOnly();         
         throw new EJBException(ex.getMessage());
      }
   }
}
