
/*
 * GlUserStaticReportControllerBean.java
 *
 * Created on November 21, 2005, 8:46 AM
 * 
 * @author  Farrah S. Garing
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.gl.LocalGlStaticReport;
import com.ejb.gl.LocalGlStaticReportHome;
import com.ejb.gl.LocalGlUserStaticReport;
import com.ejb.gl.LocalGlUserStaticReportHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.GlModUserStaticReportDetails;
import com.util.GlStaticReportDetails;

/**
 * @ejb:bean name="GlUserStaticReportControllerEJB"
 *           display-name="Used for entering payment schedules"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlUserStaticReportControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlUserStaticReportController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlUserStaticReportControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="aruser"
 *                        role-link="aruserlink"
 *
 * @ejb:permission role-name="aruser"
 * 
*/

public class GlUserStaticReportControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public GlModUserStaticReportDetails getGlUstrBySrCodeAndUsrCode(Integer SR_CODE, Integer USR_CODE, Integer AD_CMPNY) {
                    
        Debug.print("GlUserStaticReportControllerBean getGlUstrBySrCodeAndUsrCode");

        LocalGlUserStaticReportHome glUserStaticReportHome = null;

        // Initialize EJB Home

        try {
            
            glUserStaticReportHome = (LocalGlUserStaticReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlUserStaticReportHome.JNDI_NAME, LocalGlUserStaticReportHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        try {
            
        	LocalGlUserStaticReport glUserStaticReport = glUserStaticReportHome.findBySrCodeAndUsrCode(
        			SR_CODE, USR_CODE, AD_CMPNY);
        	
        	GlModUserStaticReportDetails mdetails = new GlModUserStaticReportDetails();
    		mdetails.setUstrCode(glUserStaticReport.getUstrCode());        		
            mdetails.setUstrUsrCode(glUserStaticReport.getAdUser().getUsrCode());
            mdetails.setUstrUsrName(glUserStaticReport.getAdUser().getUsrName());
            
            return mdetails;
            
        } catch (FinderException ex) {
        	
        	return null;
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }        
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public com.util.GlStaticReportDetails getGlSrBySrCode(Integer SR_CODE) {
                    
        Debug.print("GlUserStaticReportControllerBean getAdRsByRsCode");
        		    	         	   	        	    
        LocalGlStaticReportHome glStaticReportHome = null;
                                       
        // Initialize EJB Home
        
        try {
            
            glStaticReportHome = (LocalGlStaticReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlStaticReportHome.JNDI_NAME, LocalGlStaticReportHome.class);              
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {

        	LocalGlStaticReport glStaticReport = glStaticReportHome.findByPrimaryKey(SR_CODE);
        		GlStaticReportDetails details = new GlStaticReportDetails();
        		details.setSrCode(glStaticReport.getSrCode());        		
        		details.setSrName(glStaticReport.getSrName());
        		details.setSrDescription(glStaticReport.getSrDescription());
        		
        	return details;
        	       	
        } catch (Exception ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
	}      
	
    /**
     * @ejb:interface-method view-type="remote"
     **/    
     public void saveGlUstrEntry(ArrayList list, Integer SR_CODE, Integer AD_CMPNY){

        Debug.print("GlUserStaticReportControllerBean saveGlUstrEntry");
        
        LocalGlUserStaticReportHome glUserStaticReportHome = null;
        LocalGlStaticReportHome glStaticReportHome = null;
        LocalAdUserHome adUserHome = null;
                       
        // Initialize EJB Home
        
        try {
            
            glStaticReportHome = (LocalGlStaticReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlStaticReportHome.JNDI_NAME, LocalGlStaticReportHome.class);
            glUserStaticReportHome = (LocalGlUserStaticReportHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlUserStaticReportHome.JNDI_NAME, LocalGlUserStaticReportHome.class);                
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);                                                      
                
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                                   
        try {
        
            LocalGlUserStaticReport glUserStaticReport = null;

        	// delete all existing user static report  
        	
        	Collection users = glUserStaticReportHome.findBySrCode(SR_CODE, AD_CMPNY);

        	Iterator i = users.iterator();
        	
        	while(i.hasNext()) {

        		glUserStaticReport = (LocalGlUserStaticReport)i.next();
        		i.remove();
        		
        		glUserStaticReport.remove();
        		
        	}

        	// create new user static report
        	
        	i = list.iterator();
        	
        	while(i.hasNext()) {
        	
        	    GlModUserStaticReportDetails mdetails = (GlModUserStaticReportDetails)i.next();
       		    
        		glUserStaticReport = glUserStaticReportHome.create(AD_CMPNY);

                // add user
                
                LocalAdUser adUser = adUserHome.findByPrimaryKey(mdetails.getUstrUsrCode());
                adUser.addGlUserStaticReport(glUserStaticReport);
                
                // add static report
                
                LocalGlStaticReport glStaticReport= glStaticReportHome.findByPrimaryKey(SR_CODE);
                glStaticReport.addGlUserStaticReport(glUserStaticReport);

        	}    	

        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }        
                                    	                                        
    }
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
     public ArrayList getAdUsrAll(Integer AD_CMPNY) {
                     
         Debug.print("GlUserStaticReportControllerBean getAdUsrAll");
         
         LocalAdUserHome adUserHome = null;
         
         Collection adUsers = null;
         
         LocalAdUser adUser = null;
         
         ArrayList list = new ArrayList();
         
         // Initialize EJB Home
         
         try {
             
         	adUserHome = (LocalAdUserHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
             
         } catch (NamingException ex) {
             
             throw new EJBException(ex.getMessage());
             
         }
         
         try {
             
         	adUsers = adUserHome.findUsrAll(AD_CMPNY);
             
         } catch (FinderException ex) {
             
         } catch (Exception ex) {
             
             throw new EJBException(ex.getMessage());
         }
         
         if (adUsers.isEmpty()) {
         	
         	return null;
         	
         }
         
         Iterator i = adUsers.iterator();
                
         while (i.hasNext()) {
         	
         	adUser = (LocalAdUser)i.next();
         	
         	GlModUserStaticReportDetails details = new GlModUserStaticReportDetails();
         	
         	details.setUstrUsrCode(adUser.getUsrCode());
         	details.setUstrUsrName(adUser.getUsrName());
         	
         	list.add(details);
         	
         }
         
         return list;
             
     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public ArrayList getGlUstrBySrCode(Integer SR_CODE, Integer AD_CMPNY) {
                     
         Debug.print("GlUserStaticReportControllerBean getGlUstrBySrCode");

         LocalGlUserStaticReportHome glUserStaticReportHome = null;

         // Initialize EJB Home

         try {
             
             glUserStaticReportHome = (LocalGlUserStaticReportHome)EJBHomeFactory.
                 lookUpLocalHome(LocalGlUserStaticReportHome.JNDI_NAME, LocalGlUserStaticReportHome.class);

         } catch (NamingException ex) {
             
             throw new EJBException(ex.getMessage());
             
         }

         try {
             
         	Collection glUserStaticReports = glUserStaticReportHome.findBySrCode(SR_CODE, AD_CMPNY);
         	Iterator i = glUserStaticReports.iterator();
         	ArrayList list = new ArrayList();
         	
         	while (i.hasNext()){

         		LocalGlUserStaticReport glUserStaticReport = (LocalGlUserStaticReport) i.next();
         		
         		GlModUserStaticReportDetails mdetails = new GlModUserStaticReportDetails();
         		
         		mdetails.setUstrCode(glUserStaticReport.getUstrCode());        		
         		mdetails.setUstrUsrCode(glUserStaticReport.getAdUser().getUsrCode());
         		mdetails.setUstrUsrName(glUserStaticReport.getAdUser().getUsrName());
         		
         		list.add(mdetails);
             
         	}
             
             return list;
             
         } catch (FinderException ex) {
         	
         	return null;
             
         } catch (Exception ex) {
             
             throw new EJBException(ex.getMessage());
         }        
             
     }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
     public void ejbCreate() throws CreateException {

       Debug.print("GlUserStaticReportControllerBean ejbCreate");
      
    }
}
