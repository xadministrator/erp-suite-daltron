package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBankAccountHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCheckHome;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApVoucherHome;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.cm.LocalCmDistributionRecordHome;
import com.ejb.cm.LocalCmFundTransferHome;
import com.ejb.exception.GlDepreciationAlreadyMadeForThePeriodException;
import com.ejb.exception.GlDepreciationPeriodInvalidException;
import com.ejb.exception.GlJLChartOfAccountNotFoundException;
import com.ejb.exception.GlJLChartOfAccountPermissionDeniedException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlJREffectiveDateViolationException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalNotAllTransactionsArePostedException;
import com.ejb.genfld.LocalGenField;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountRange;
import com.ejb.gl.LocalGlAccountingCalendar;
import com.ejb.gl.LocalGlAccountingCalendarHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlInvestorAccountBalance;
import com.ejb.gl.LocalGlInvestorAccountBalanceHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlOrganization;
import com.ejb.gl.LocalGlResponsibility;
import com.ejb.gl.LocalGlResponsibilityHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlTransactionCalendar;
import com.ejb.gl.LocalGlTransactionCalendarHome;
import com.ejb.gl.LocalGlTransactionCalendarValue;
import com.ejb.gl.LocalGlTransactionCalendarValueHome;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAssemblyTransferHome;
import com.ejb.inv.LocalInvBranchStockTransferHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyHome;
import com.ejb.inv.LocalInvDepreciationLedger;
import com.ejb.inv.LocalInvDepreciationLedgerHome;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvOverheadHome;
import com.ejb.inv.LocalInvStockIssuanceHome;
import com.ejb.inv.LocalInvStockTransferHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlAccountingCalendarDetails;
import com.util.GlModAccountingCalendarValueDetails;
import com.util.GlModJournalLineDetails;

/**
 * @ejb:bean name="GlYearEndClosingControllerEJB"
 *           display-name="Used for closing of nominal accounts during year-end closing"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/GlYearEndClosingControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.GlYearEndClosingController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.GlYearEndClosingControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="gluser"
 *                        role-link="gluserlink"
 *
 * @ejb:permission role-name="gluser"
 * 
*/ 

public class GlYearEndClosingControllerBean extends AbstractSessionBean {
  


/**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public com.util.GlAccountingCalendarDetails getGlAcForClosing(Integer AD_CMPNY) throws
      GlobalNoRecordFoundException {

      Debug.print("GlYearEndClosingControllerBean getGlAcForClosing");

      LocalGlSetOfBookHome glSetOfBookHome = null;
            
	  // Initialize EJB Home
	  	    
	  try {
	        
	       glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
	          lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);	           
	          	        	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	  
	  try {
	  	
	  	  Collection glSetOfBooks = glSetOfBookHome.findBySobYearEndClosed(EJBCommon.FALSE, AD_CMPNY);
	  	  
	  	  if (glSetOfBooks.isEmpty()) throw new GlobalNoRecordFoundException();
	  	  
	  	  ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);
	  	  
	  	  LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);
	  	  
	  	  GlAccountingCalendarDetails details = new GlAccountingCalendarDetails(
	  	  	   glSetOfBook.getGlAccountingCalendar().getAcCode(),
	  	  	   glSetOfBook.getGlAccountingCalendar().getAcName(),
	  	  	   glSetOfBook.getGlAccountingCalendar().getAcDescription());
	  	  	   
	  	  return details;
	  	  
	  } catch (GlobalNoRecordFoundException ex) {
	  	
	  	  throw ex;
	  		
	  } catch (Exception ex) {
	  	
	  	  Debug.printStackTrace(ex);
	  	  throw new EJBException(ex.getMessage());
	  	
	  }

   }
  
   /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getGlReportableAcvAll(Integer AD_CMPNY) {

		Debug.print("GlYearEndClosingControllerBean getGlReportableAcvAll");      

		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection glSetOfBooks = glSetOfBookHome.findSobAll(AD_CMPNY);

			Iterator i = glSetOfBooks.iterator();

			while (i.hasNext()) {

				LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)i.next();

				Collection glAccountingCalendarValues = 
					glAccountingCalendarValueHome.findReportableAcvByAcCodeAndAcvStatus(
							glSetOfBook.getGlAccountingCalendar().getAcCode(), 'O', 'C', 'P', AD_CMPNY);

				Iterator j = glAccountingCalendarValues.iterator();

				while (j.hasNext()) {

					LocalGlAccountingCalendarValue glAccountingCalendarValue = 
						(LocalGlAccountingCalendarValue)j.next();

					GlModAccountingCalendarValueDetails mdetails = new GlModAccountingCalendarValueDetails();

					mdetails.setAcvPeriodPrefix(glAccountingCalendarValue.getAcvPeriodPrefix());

					// get year

					GregorianCalendar gc = new GregorianCalendar();
					gc.setTime(glAccountingCalendarValue.getAcvDateTo());

					mdetails.setAcvYear(gc.get(Calendar.YEAR));

					// is current

					gc = EJBCommon.getGcCurrentDateWoTime();

					if ((glAccountingCalendarValue.getAcvDateFrom().before(gc.getTime()) ||
							glAccountingCalendarValue.getAcvDateFrom().equals(gc.getTime())) &&
							(glAccountingCalendarValue.getAcvDateTo().after(gc.getTime()) ||
									glAccountingCalendarValue.getAcvDateTo().equals(gc.getTime()))) {

						mdetails.setAcvCurrent(true);

					}

					list.add(mdetails);

				}

			}

			return list;


		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void executeFixedAssetDepreciation(Integer AC_CODE, String DTB_PRD, int DTB_YR, String userName, 
			Integer resCode, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException,
	GlobalAccountNumberInvalidException,
	GlJREffectiveDatePeriodClosedException,
	GlJREffectiveDateNoPeriodExistException,
	GlDepreciationAlreadyMadeForThePeriodException,
	GlDepreciationPeriodInvalidException {
		
		Debug.print("GlYearEndClosingControllerBean executeFixedAssetDepreciation");      


		LocalGlSetOfBookHome glSetOfBookHome = null;
		LocalInvItemHome invItemHome=null;
		LocalGlTransactionCalendarValueHome glTransactionCalendarValueHome = null;
		LocalGlJournalSourceHome glJournalSourceHome = null;  
		LocalGlJournal glJournal = null;
		LocalGlJournalHome glJournalHome = null;   
		LocalGlJournalCategoryHome glJournalCategoryHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlJournalLineHome glJournalLineHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalCmDistributionRecordHome cmDistributionRecordHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalAdBankAccountHome adBankAccountHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalApVoucherHome apVoucherHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvTagHome invTagHome = null;
		LocalInvDepreciationLedgerHome invDepreciationLedgerHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			glTransactionCalendarValueHome = (LocalGlTransactionCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlTransactionCalendarValueHome.JNDI_NAME, LocalGlTransactionCalendarValueHome.class);              
			glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);  
			glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
			glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);              
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);    
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);    
			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);    
			cmDistributionRecordHome = (LocalCmDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalCmDistributionRecordHome.JNDI_NAME, LocalCmDistributionRecordHome.class);    
			invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);    
			adBankAccountHome = (LocalAdBankAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBankAccountHome.JNDI_NAME, LocalAdBankAccountHome.class);    
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);    
			apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
			invDepreciationLedgerHome = (LocalInvDepreciationLedgerHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvDepreciationLedgerHome.JNDI_NAME, LocalInvDepreciationLedgerHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}
		try {
			short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
			
			Collection glSetOfBooks = glSetOfBookHome.findByAcvPeriodPrefixAndDate(DTB_PRD, EJBCommon.getIntendedDate(DTB_YR), AD_CMPNY);
			ArrayList glSetOfBookList = new ArrayList(glSetOfBooks);            
			LocalGlSetOfBook glSetOfBook = (LocalGlSetOfBook)glSetOfBookList.get(0);

			LocalGlAccountingCalendarValue glAccountingCalendarValue =
				glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodPrefix(
						glSetOfBook.getGlAccountingCalendar().getAcCode(),
						DTB_PRD, AD_CMPNY);

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			// validate if effective date has no period and period is closed

			try {
				System.out.println("details.getJrEffectiveDate(: " +glAccountingCalendarValue.getAcvDateTo());
				System.out.println("AD_CMPNY: " +AD_CMPNY);
				LocalGlSetOfBook glJournalSetOfBook = glSetOfBookHome.findByDate(glAccountingCalendarValue.getAcvDateTo(), AD_CMPNY);

				LocalGlAccountingCalendarValue glAccountingCalendarValueCheck = 
					glAccountingCalendarValueHome.findByAcCodeAndDate(
							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), glAccountingCalendarValue.getAcvDateTo(), AD_CMPNY);


				if ((glAccountingCalendarValue.getAcvStatus() == 'N' ||
						glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P')) {

					throw new GlJREffectiveDatePeriodClosedException();

				}

			} catch (GlJREffectiveDatePeriodClosedException ex) {

				throw ex;

			} catch (FinderException ex) {

				throw new GlJREffectiveDateNoPeriodExistException();

			} catch (Exception ex) {

				Debug.printStackTrace(ex);
				throw new EJBException(ex.getMessage());

			}


			// validate effective date violation

			try {

				LocalGlSetOfBook glJournalSetOfBook = glSetOfBookHome.findByDate(glAccountingCalendarValue.getAcvDateTo(), AD_CMPNY);

				LocalGlTransactionCalendarValue glTransactionCalendarValue = 
					glTransactionCalendarValueHome.findByTcCodeAndTcvDate(glJournalSetOfBook.getGlTransactionCalendar().getTcCode(),
							glAccountingCalendarValue.getAcvDateTo(), AD_CMPNY);

				LocalGlJournalSource glValidateJournalSource = glJournalSourceHome.findByJsName("MANUAL", AD_CMPNY);          

				if (glTransactionCalendarValue.getTcvBusinessDay() == EJBCommon.FALSE &&
						glValidateJournalSource.getJsEffectiveDateRule() == 'F') {

					throw new GlJREffectiveDateViolationException();

				}

			} catch (GlJREffectiveDateViolationException ex) {      	  

				throw ex;

			} catch (Exception ex) {

				Debug.printStackTrace(ex);
				throw new EJBException(ex.getMessage());

			}



			/****LOAD ITEMS*****/
			Collection invItems = invItemHome.findFixedAssetIiAll(AD_CMPNY);
			Iterator i = invItems.iterator();	
			
			System.out.println("1.SIZE="+invItems.size());
			double test;

			while (i.hasNext()) {
				LocalInvItem invItem = (LocalInvItem)i.next();
				
				Calendar cal = Calendar.getInstance();
				//cal.setTime(invItem.getIiDateAcquired());
				System.out.println(cal.getTime());
				cal.add(Calendar.YEAR, (int)invItem.getIiLifeSpan());
				//Validate if referenceNumber exist.
				System.out.println("CALENDAR PLUS LIFESPAN: "+cal.getTime());
				System.out.println("Date From: "+glAccountingCalendarValue.getAcvDateFrom());
				if((glAccountingCalendarValue.getAcvDateFrom().before(cal.getTime()))||
						(cal.getTime().after(glAccountingCalendarValue.getAcvDateFrom()) || 
								cal.getTime().equals(glAccountingCalendarValue.getAcvDateFrom()))){


					LocalGlJournal glJournalCheck = null;
					try{
						glJournalCheck = glJournalHome.findByJrNameAndBrCode("FADepreciation"+" "+invItem.getIiName()+ " " +DTB_PRD + " " +
								DTB_YR, AD_BRNCH, AD_CMPNY);
						if(glJournalCheck!=null){
							throw new GlDepreciationAlreadyMadeForThePeriodException();
						}

					}catch (GlDepreciationAlreadyMadeForThePeriodException ex) {
						//getSessionContext().setRollbackOnly();
						throw ex;
					}catch (FinderException ex) {

					}


					try{
						glJournalCheck = null;
						glJournalCheck = glJournalHome.findByJrReferenceNumberAndBrCode("FADepreciation"+" "+invItem.getIiName()+ " " +DTB_PRD 
								+ " " +DTB_YR,AD_BRNCH, AD_CMPNY);
						if(glJournalCheck!=null){
							throw new GlDepreciationAlreadyMadeForThePeriodException();
						}

					}catch (GlDepreciationAlreadyMadeForThePeriodException ex) {
						//getSessionContext().setRollbackOnly();
						throw ex;
					}catch (FinderException ex) {

					}

					/*if(invItem.getIiDateAcquired().after(glAccountingCalendarValue.getAcvDateTo())){
						throw new GlDepreciationPeriodInvalidException();
					}*/
					
		 		

		 		/*try {

	 				LocalGlJournalBatch glJournalBatch = glJournalBatchHome.findByJbName(JB_NM, AD_BRNCH, AD_CMPNY);
	 				//glJournalBatch.addGlJournal(glJournal);
	 				glJournal.setGlJournalBatch(glJournalBatch);

	 			} catch (FinderException ex) {

	 			}*/
		 		
		 		

		 		
		 		Collection invItemLocations = invItem.getInvItemLocations();
		 		System.out.println("itemlocation SIZE="+invItemLocations.size());
				Iterator x = invItemLocations.iterator();
				
				System.out.println("glAccountingCalendarValue.getAcvDateTo()="+glAccountingCalendarValue.getAcvDateTo());
				while(x.hasNext()){
					LocalInvItemLocation invItemLocation = (LocalInvItemLocation)x.next();
					
					Collection invTags = invTagHome.findByLessThanOrEqualToTransactionDateAndItemLocationAndAdBranchFromPORecevingLine(glAccountingCalendarValue.getAcvDateTo(), invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
					Iterator y = invTags.iterator();

					while(y.hasNext()){
						LocalInvTag invTag = (LocalInvTag)y.next();
						
						
						LocalInvDepreciationLedger existingDepreciationLedger = null;
						try{
							existingDepreciationLedger = invDepreciationLedgerHome.findExistingDLbyDateAndInvTag(glAccountingCalendarValue.getAcvDateTo(), invTag.getTgCode(), AD_CMPNY);
							
						}catch (FinderException ex) {

						}
						
						if (existingDepreciationLedger != null) {
							continue;
						}
						
						System.out.println("SERIAL NUM="+invTag.getTgSerialNumber());
						System.out.println("PROPERTY CODE="+invTag.getTgPropertyCode());
						String serialNumber = invTag.getTgSerialNumber();
						String propertyCode = invTag.getTgPropertyCode();
						String itemName = invTag.getApPurchaseOrderLine().getInvItemLocation().getInvItem().getIiDescription();

						double dlAcquisitionCost = invTag.getApPurchaseOrderLine().getPlUnitCost();
						double dlLifeSpanMonths = invTag.getApPurchaseOrderLine().getInvItemLocation().getInvItem().getIiLifeSpan() * 12;
						double dlMonthlyDepreciationAmount = EJBCommon.roundIt(dlAcquisitionCost / dlLifeSpanMonths,precisionUnit);
						double totalCurrentBalance = 0d;
						double totalDepreciation = 0d;
						
						Collection depreciationLedgers = invTag.getInvDepreciationLedgers();
						Iterator z = depreciationLedgers.iterator();
						while(z.hasNext()){
							LocalInvDepreciationLedger invDepreciationLedger = (LocalInvDepreciationLedger)z.next();

							totalDepreciation += invDepreciationLedger.getDlDepreciationAmount();
							totalCurrentBalance = invDepreciationLedger.getDlCurrentBalance();
							
						}
						System.out.println("dlAcquisitionCost="+dlAcquisitionCost);
						System.out.println("totalDepreciation="+totalDepreciation);
						System.out.println("totalCurrentBalance="+totalCurrentBalance);
						if(totalDepreciation==0){
							System.out.println("1");
							totalCurrentBalance = EJBCommon.roundIt(dlAcquisitionCost - dlMonthlyDepreciationAmount, precisionUnit);
						} else {
							System.out.println("2");
							totalCurrentBalance = EJBCommon.roundIt(totalCurrentBalance - dlMonthlyDepreciationAmount, precisionUnit);
						}
						
						System.out.println("3="+totalCurrentBalance);
						
						
						//add lines on depreciation ledger
						LocalInvDepreciationLedger invDepreciationLedger = 
								invDepreciationLedgerHome.create(glAccountingCalendarValue.getAcvDateTo(),dlAcquisitionCost , dlMonthlyDepreciationAmount, dlLifeSpanMonths, totalCurrentBalance, AD_CMPNY);
						
						invDepreciationLedger.setInvTag(invTag);
						
						
						
						
						
						
						
						
						
						
						// validate if document number is unique document number is automatic then set next sequence
						String docNumber = "";
						String AccumulatedDepreciation = "";
						String Depreciation = "";
						try {

							LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
							LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;


							try {

								adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("GL JOURNAL", AD_CMPNY);

							} catch (FinderException ex) {

							}

							try {

								adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

							} catch (FinderException ex) {

							}

							LocalGlJournal glExistingJournal = null;
							System.out.println("DocNumber1 " + docNumber);
							try {

								glExistingJournal = glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode (
										docNumber, "MANUAL", AD_BRNCH, AD_CMPNY);

							} catch (FinderException ex) { 

							}
							System.out.println("DocNumber2 " + docNumber);
							try{
								System.out.println(glExistingJournal.getJrCode()+" glExistingJournal " + glExistingJournal.getJrDocumentNumber());
							}catch(Exception e){
								System.out.println("Not Null");
							}

							if (glExistingJournal != null) {

								throw new GlobalDocumentNumberNotUniqueException();

							}
							System.out.println("DocNumber3 " + docNumber);
							try{
								System.out.println("adBranchDocumentSequenceAssignment: " + adBranchDocumentSequenceAssignment.getBdsCode());
								System.out.println("adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType()"+adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() ) ;

							}catch(Exception e){
								System.out.println("Not Null");
							}
							if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
									(docNumber == "")) {

								while (true) {

									if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

										try {

											glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
											adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
											//docNumber=(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
											System.out.println("DocNumber4 " + docNumber);
										} catch (FinderException ex) {

											docNumber=(adDocumentSequenceAssignment.getDsaNextSequence());
											System.out.println("DocNumber5 " + docNumber);
											adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
											break;

										}

									} else {

										try {

											glJournalHome.findByJrDocumentNumberAndJsNameAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), "MANUAL", AD_BRNCH, AD_CMPNY);		            		
											adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
											//docNumber=(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

										} catch (FinderException ex) {

											docNumber=(adBranchDocumentSequenceAssignment.getBdsNextSequence());
											adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
											break;

										}

									}

								}		            

							}



						} /*catch (GlobalDocumentNumberNotUniqueException ex) {

			 			getSessionContext().setRollbackOnly();
			 			throw ex;

			 		}*/ catch (Exception ex) {

			 			Debug.printStackTrace(ex);
			 			getSessionContext().setRollbackOnly();
			 			throw new EJBException(ex.getMessage());

			 		}
			 		double depreciationAmount = 0d;


			 		try {
			 			LocalGlChartOfAccount glChartOfAccount = null;
			 			glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(Integer.parseInt(invItem.getGlCoaAccumulatedDepreciationAccount()));
			 			AccumulatedDepreciation = glChartOfAccount.getCoaAccountNumber();

			 		} catch (FinderException ex){

			 			throw new GlobalNoRecordFoundException();

			 		}

			 		try {
			 			LocalGlChartOfAccount glChartOfAccount = null;
			 			glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(Integer.parseInt(invItem.getGlCoaDepreciationAccount()));
			 			Depreciation = glChartOfAccount.getCoaAccountNumber();
			 		} catch (FinderException ex){

			 			throw new GlobalNoRecordFoundException();

			 		}

			 		if(docNumber==""){
			 			throw new Exception();
			 		}
						
						
						glJournal = glJournalHome.create(
				 				"FADepreciation"+" "+itemName+" : "+propertyCode+" : "+ serialNumber+ " " +DTB_PRD + " " +DTB_YR, 
				 				"Fixed Asset Depreciation for item: " + itemName + " for the period of " + DTB_PRD + " " +DTB_YR,
				 				glAccountingCalendarValue.getAcvDateTo(), 0.0d,
				 				null, docNumber,
				 				null, 1.0,
				 				null, null, 'N', EJBCommon.FALSE, EJBCommon.FALSE,
				 				userName, new java.util.Date(),
				 				userName, new java.util.Date(),
				 				null, null, null, null, null, null, EJBCommon.FALSE, 
				 				"FADepreciation"+" "+itemName+ " " +DTB_PRD + " " +DTB_YR, 

				 				AD_BRNCH, AD_CMPNY); 

				 		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("MANUAL", AD_CMPNY);
				 		glJournal.setGlJournalSource(glJournalSource);

				 		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adPreference.getPrfApDefaultPrCurrency(), AD_CMPNY);
				 		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

				 		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("GENERAL", AD_CMPNY);
				 		glJournal.setGlJournalCategory(glJournalCategory);
						
				 		
				 		this.addGlJlEntry(Short.parseShort("1"), Byte.parseByte("1"), dlMonthlyDepreciationAmount, Depreciation, glJournal, 
				 				resCode, AD_BRNCH, AD_CMPNY);
				 		this.addGlJlEntry(Short.parseShort("2"), Byte.parseByte("0"), dlMonthlyDepreciationAmount, AccumulatedDepreciation, glJournal, 
				 				resCode, AD_BRNCH, AD_CMPNY);


					}

					
				}
				
				
				}
			}
		} catch (GlobalNoRecordFoundException ex) {
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}catch (GlJREffectiveDatePeriodClosedException ex) {
			//Debug.printStackTrace(ex);
			//throw new EJBException(ex.getMessage());
			throw ex;

		}catch (GlJREffectiveDateNoPeriodExistException ex) {
			//Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}catch (GlDepreciationAlreadyMadeForThePeriodException ex) {
			throw ex;
			
		}catch (GlDepreciationPeriodInvalidException ex) {
			//getSessionContext().setRollbackOnly();
			throw ex;
			
		}catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}
		
		
	}

// private methods
	
	
	
 	
 	private void addGlJlEntry(short lineNumber, Byte debit, double amount, String coaAccount, LocalGlJournal glJournal, 
 			Integer RES_CODE, Integer AD_BRNCH, Integer AD_CMPNY) 
 	throws 
	GlJLChartOfAccountNotFoundException,
	GlJLChartOfAccountPermissionDeniedException {
 		
 		Debug.print("GlJournalEntryControllerBean addGlJlEntry");
 		
 		LocalGlJournalLineHome glJournalLineHome = null;        
 		LocalGlChartOfAccountHome glChartOfAccountHome = null;
 		LocalGlResponsibilityHome glResponsibilityHome = null;
 		LocalAdCompanyHome adCompanyHome = null;
 		
 		
 		// Initialize EJB Home
 		
 		try {
 			
 			glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);            
 			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
 			glResponsibilityHome = (LocalGlResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlResponsibilityHome.JNDI_NAME, LocalGlResponsibilityHome.class);            
 			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
 			
 			
 		} catch (NamingException ex) {
 			
 			throw new EJBException(ex.getMessage());
 			
 		}            
 		
 		try {
 			
 			// get company
 			
 			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
 			
 			// validate if coa exists
 			
 			LocalGlChartOfAccount glChartOfAccount = null;
 			
 			try {
 				
 				glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(coaAccount, AD_BRNCH, AD_CMPNY);
 				
 				if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
 					throw new GlJLChartOfAccountNotFoundException();
 				
 			} catch (FinderException ex) {
 				
 				throw new GlJLChartOfAccountNotFoundException();
 				
 			}
 			
 			// validate responsibility coa permission
 			
 			LocalGlResponsibility glResponsibility = null; 
 			
 			try {
 				
 				glResponsibility = glResponsibilityHome.findByPrimaryKey(RES_CODE);
 				
 			} catch (FinderException ex) {
 				
 				throw new GlJLChartOfAccountPermissionDeniedException();
 				
 			}
 			
 			if (!this.isPermitted(glResponsibility.getGlOrganization(), glChartOfAccount, adCompany.getGenField(), AD_CMPNY)) {
 				
 				throw new GlJLChartOfAccountPermissionDeniedException();
 				
 			}        	        	
 			
 			// create journal 
 			
 			LocalGlJournalLine glJournalLine = glJournalLineHome.create(
 					lineNumber, 
 					debit, 
					EJBCommon.roundIt(amount, adCompany.getGlFunctionalCurrency().getFcPrecision()), 
					null, AD_CMPNY);
 			
 			//glJournal.addGlJournalLine(glJournalLine);
 			glJournalLine.setGlJournal(glJournal);
 			//glChartOfAccount.addGlJournalLine(glJournalLine);
 			glJournalLine.setGlChartOfAccount(glChartOfAccount);
 			
 			
 		} catch (GlJLChartOfAccountPermissionDeniedException ex) {        	
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;
 			
 		} catch (GlJLChartOfAccountNotFoundException ex) {        	
 			
 			getSessionContext().setRollbackOnly();
 			throw ex;    
 			
 		} catch (Exception ex) {
 			
 			Debug.printStackTrace(ex);
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 			
 		}
 		
 	}
 	
 	private boolean isPermitted(LocalGlOrganization glOrganization,
 			LocalGlChartOfAccount glChartOfAccount, LocalGenField genField, Integer AD_CMPNY) {
 		
 		Debug.print("GlJournalEntryControllerBean isPermitted");
 		
 		/*** Get Generic Field's Segment Separator and Number Of Segments**/
 		
 		String strSeparator = null;
 		short numberOfSegment = 0;
 		short genNumberOfSegment = 0;
 		
 		try {
 			char chrSeparator = genField.getFlSegmentSeparator();
 			genNumberOfSegment = genField.getFlNumberOfSegment();
 			strSeparator = String.valueOf(chrSeparator);
 		} catch (Exception ex) {
 			getSessionContext().setRollbackOnly();
 			throw new EJBException(ex.getMessage());
 		}
 		
 		Collection glAccountRanges = glOrganization.getGlAccountRanges();
 		
 		Iterator i = glAccountRanges.iterator();
 		
 		while (i.hasNext()) {
 			
 			LocalGlAccountRange glAccountRange = (LocalGlAccountRange)i.next(); 
 			
 			String[] chartOfAccountSegmentValue = new String[genNumberOfSegment];      	  
 			String[] accountLowSegmentValue = new String[genNumberOfSegment];
 			String[] accountHighSegmentValue = new String[genNumberOfSegment];
 			
 			// tokenize coa
 			
 			StringTokenizer st = new StringTokenizer(glChartOfAccount.getCoaAccountNumber(), strSeparator);
 			
 			int j = 0;
 			
 			while (st.hasMoreTokens()) {
 				
 				chartOfAccountSegmentValue[j] = st.nextToken();
 				j++;
 				
 			}
 			
 			// tokenize account low 
 			
 			st = new StringTokenizer(glAccountRange.getArAccountLow(), strSeparator);
 			
 			j = 0;
 			
 			while (st.hasMoreTokens()) {
 				
 				accountLowSegmentValue[j] = st.nextToken();
 				j++;
 				
 			}
 			
 			// tokenize account high
 			st = new StringTokenizer(glAccountRange.getArAccountHigh(), strSeparator);
 			
 			j = 0;
 			
 			while (st.hasMoreTokens()) {
 				
 				accountHighSegmentValue[j] = st.nextToken();
 				j++;
 				
 			} 
 			
 			boolean isOverlapped = false;
 			
 			for (int k=0; k<genNumberOfSegment; k++) {
 				
 				if(chartOfAccountSegmentValue[k].compareTo(accountLowSegmentValue[k]) >= 0 &&
 						chartOfAccountSegmentValue[k].compareTo(accountHighSegmentValue[k]) <= 0) {
 					
 					isOverlapped = true;
 					
 				} else {
 					
 					isOverlapped = false;
 					break;
 					
 				}
 			}
 			
 			if (isOverlapped) return true;
 			
 			
 			
 		}
 		
 		return false;
 		
 	}
 	
  /**
   * @ejb:interface-method view-type="remote"
   **/
   public void executeGlYearEndClosing(Integer AC_CODE, Integer AD_CMPNY)
   	  throws GlobalNotAllTransactionsArePostedException,
	  GlobalAccountNumberInvalidException {

      Debug.print("GlYearEndClosingControllerBean executeGlYearEndClosing");

      LocalGlSetOfBookHome glSetOfBookHome = null;
      LocalGlAccountingCalendarHome glAccountingCalendarHome = null;
      LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
      LocalGlTransactionCalendarHome glTransactionCalendarHome = null;
      LocalGlTransactionCalendarValueHome glTransactionCalendarValueHome = null;
      LocalGlChartOfAccountHome glChartOfAccountHome = null;
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      
      LocalApSupplierHome apSupplierHome = null;
      LocalGlInvestorAccountBalanceHome glInvestorAccountBalanceHome = null;
      
      LocalAdCompanyHome adCompanyHome = null;
      LocalGlJournalHome glJournalHome = null;
      LocalArInvoiceHome arInvoiceHome = null;
      LocalArReceiptHome arReceiptHome = null;
      LocalApVoucherHome apVoucherHome = null;
      LocalApCheckHome apCheckHome = null;
      LocalCmAdjustmentHome cmAdjustmentHome = null;
      LocalCmFundTransferHome cmFundTransferHome = null;
      LocalInvAdjustmentHome invAdjustmentHome = null;
      LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
      LocalInvOverheadHome invOverheadHome = null;
      LocalApPurchaseOrderHome apPurchaseOrderHome = null;
      LocalInvStockIssuanceHome invStockIssuanceHome = null;
	  LocalInvAssemblyTransferHome invAssemblyTransferHome = null;
	  LocalInvStockTransferHome invStockTransferHome = null;
	  LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
	  LocalAdPreferenceHome adPreferenceHome = null;
	  
	  // Initialize EJB Home
	  	    
	  try {
	  	
	  	glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
		lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);	           
	  	glAccountingCalendarHome = (LocalGlAccountingCalendarHome)EJBHomeFactory.
		lookUpLocalHome(LocalGlAccountingCalendarHome.JNDI_NAME, LocalGlAccountingCalendarHome.class);	           
	  	glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
		lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);	           
	  	glTransactionCalendarHome = (LocalGlTransactionCalendarHome)EJBHomeFactory.
		lookUpLocalHome(LocalGlTransactionCalendarHome.JNDI_NAME, LocalGlTransactionCalendarHome.class);
	  	glTransactionCalendarValueHome = (LocalGlTransactionCalendarValueHome)EJBHomeFactory.
		lookUpLocalHome(LocalGlTransactionCalendarValueHome.JNDI_NAME, LocalGlTransactionCalendarValueHome.class);
	  	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
		lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	  	glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
		lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
	  	
	  	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
	  			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
	  	glInvestorAccountBalanceHome = (LocalGlInvestorAccountBalanceHome)EJBHomeFactory.
	  			lookUpLocalHome(LocalGlInvestorAccountBalanceHome.JNDI_NAME, LocalGlInvestorAccountBalanceHome.class);
	  		  	
	  	
	  	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
		lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	  	glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
		lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
	  	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
		lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
	  	arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
		lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
	  	apVoucherHome = (LocalApVoucherHome)EJBHomeFactory.
		lookUpLocalHome(LocalApVoucherHome.JNDI_NAME, LocalApVoucherHome.class);
	  	apCheckHome = (LocalApCheckHome)EJBHomeFactory.
		lookUpLocalHome(LocalApCheckHome.JNDI_NAME, LocalApCheckHome.class);
	  	cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
		lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);
	  	cmFundTransferHome = (LocalCmFundTransferHome)EJBHomeFactory.
		lookUpLocalHome(LocalCmFundTransferHome.JNDI_NAME, LocalCmFundTransferHome.class);
	  	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
	  	invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
	  	invOverheadHome = (LocalInvOverheadHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvOverheadHome.JNDI_NAME, LocalInvOverheadHome.class);
	  	apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
		lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
	  	invStockIssuanceHome = (LocalInvStockIssuanceHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvStockIssuanceHome.JNDI_NAME, LocalInvStockIssuanceHome.class);
	  	invAssemblyTransferHome = (LocalInvAssemblyTransferHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvAssemblyTransferHome.JNDI_NAME, LocalInvAssemblyTransferHome.class);
	  	invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);
	  	invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
		lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
	  	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
	  	
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	  
	  try {
	  	
	  	  LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	  	
	  	  LocalGlAccountingCalendar glAccountingCalendar = glAccountingCalendarHome.findByPrimaryKey(AC_CODE);	  	    	  
	  	  LocalGlSetOfBook glSetOfBook = glSetOfBookHome.findByAcCode(glAccountingCalendar.getAcCode(), AD_CMPNY);
	  	  LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	  	  if(adPreference.getPrfGlYearEndCloseRestriction()==1){
	  		// check all transactions if already posted
		  	  
		  	  LocalGlAccountingCalendarValue glAcv1 = 
		  	  	     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
		  	  	     	 glAccountingCalendar.getAcCode(), (short)1, AD_CMPNY);
		  	  	     	 
		  	  LocalGlAccountingCalendarValue glAcv2 = 
		  	  	     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
		  	  	     	 glAccountingCalendar.getAcCode(), (short)glAccountingCalendar.getGlPeriodType().getPtPeriodPerYear(), AD_CMPNY);
		  	  	     	 
			  Collection glJournals = glJournalHome.findUnpostedJrByJrDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection arInvoices = arInvoiceHome.findUnpostedInvByInvDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection arReceipts = arReceiptHome.findUnpostedRctByRctDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection apVouchers = apVoucherHome.findUnpostedVouByVouDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection apChecks = apCheckHome.findUnpostedChkByChkDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection cmAdjustments = cmAdjustmentHome.findUnpostedCmAdjByCmAdjDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection cmFundTransfers = cmFundTransferHome.findUnpostedCmFtByCmFtDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection invAdjustments = invAdjustmentHome.findUnpostedInvAdjByInvAdjDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection invBuildUnbuildAssemblies = invBuildUnbuildAssemblyHome.findUnpostedBuaByBuaDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection invOverheads = invOverheadHome.findUnpostedOhByOhDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection apReceivingItems = apPurchaseOrderHome.findUnpostedPoReceivingByPoReceivingDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection invStockIssuances = invStockIssuanceHome.findUnpostedSiBySiDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection invAssemblyTransfers = invAssemblyTransferHome.findUnpostedAtrByAtrDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection invStockTransfers = invStockTransferHome.findUnpostedStByStDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  Collection invBranchStockTransfers = invBranchStockTransferHome.findUnpostedBstByBstDateRange(glAcv1.getAcvDateFrom(), glAcv2.getAcvDateTo(), AD_CMPNY);
			  
			  if ((glJournals != null && !glJournals.isEmpty()) || (arInvoices != null && !arInvoices.isEmpty()) || (arReceipts != null && !arReceipts.isEmpty()) ||
			  		(apVouchers != null && !apVouchers.isEmpty()) || (apChecks != null && !apChecks.isEmpty()) ||
					(cmAdjustments != null && !cmAdjustments.isEmpty()) || (cmFundTransfers != null && !cmFundTransfers.isEmpty()) ||
					(invAdjustments != null && !invAdjustments.isEmpty()) || (invBuildUnbuildAssemblies != null && !invBuildUnbuildAssemblies.isEmpty()) ||
					(invOverheads != null && !invOverheads.isEmpty()) || (apReceivingItems != null && !apReceivingItems.isEmpty()) ||
					(invStockIssuances != null && !invStockIssuances.isEmpty()) || (invAssemblyTransfers != null && !invAssemblyTransfers.isEmpty()) ||
					(invStockTransfers != null && !invStockTransfers.isEmpty()) || (invBranchStockTransfers != null && !invBranchStockTransfers.isEmpty())) {
			  
			  	throw new GlobalNotAllTransactionsArePostedException();
			  
			  }	 
	  	  }
	  	  
	  	  if (false) throw new GlobalNotAllTransactionsArePostedException();
	  	  	     	 	  	  	     	 	  	  	  	  	  	  	  	 	  	 
	  	  // create new calendar if necessary
	  	  
	  	  LocalGlSetOfBook glSubsequentSetOfBook = null;
	  	  
	  	  try {	  	  
	  	  
	  	  	Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
	  	  	
	  	  	if (!glSubsequentSetOfBooks.isEmpty()) {	  	  	
	  	  	
		  	  	ArrayList glSubsequentSetOfBookList = new ArrayList(glSubsequentSetOfBooks);
		  	  	glSubsequentSetOfBook = (LocalGlSetOfBook)glSubsequentSetOfBookList.get(0);
		  	  	
	  	  	}
	  	      
	  	  } catch (FinderException ex) {
	  	  	
	  	  	    	  	
	  	  }
	  	  
	  	  if (glSubsequentSetOfBook == null) {
	  	  	
	  	  	  LocalGlAccountingCalendarValue glFirstAccountingCalendarValue = null;
      		  LocalGlAccountingCalendarValue glLastAccountingCalendarValue = null;
	  	  	
	  	  	  // get new year
	  	  	  
	  	  	  LocalGlAccountingCalendarValue glYearAccountingCalendarValue = 
	  	  	     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
	  	  	     	 glAccountingCalendar.getAcCode(), glAccountingCalendar.getGlPeriodType().getPtPeriodPerYear(), AD_CMPNY);
	  	  	     	 
	  	  	  GregorianCalendar gc = new GregorianCalendar();
	  	  	  gc.setTime(glYearAccountingCalendarValue.getAcvDateTo());
	  	  	  int NEW_YEAR = gc.get(Calendar.YEAR) + 1;
	  	  	     	   	  	  
	  	  	  // create accounting calendar
	  	  	
	  	  	  LocalGlAccountingCalendar glNewAccountingCalendar = glAccountingCalendarHome.create(
	  	  	  	  "CALENDAR " + NEW_YEAR, "CALENDAR " + NEW_YEAR, new Integer(NEW_YEAR), AD_CMPNY);
	  	  	  	  
	  	  	  glAccountingCalendar.getGlPeriodType().addGlAccountingCalendar(glNewAccountingCalendar);
	  	  	  
	  	  	  // create accounting calendar values
	  	  	  
	  	  	  Collection glAccountingCalendarValues = glAccountingCalendar.getGlAccountingCalendarValues();
	     	 
	     	  Iterator i = glAccountingCalendarValues.iterator();
	     	 
	     	  while (i.hasNext()) {
	     	 	
	     	 	LocalGlAccountingCalendarValue glAccountingCalendarValue = 
			       (LocalGlAccountingCalendarValue) i.next();
			       
			    GregorianCalendar gcFrom = new GregorianCalendar();
			    GregorianCalendar gcTo = new GregorianCalendar();
			    			    			    			    
			    gcFrom.setTime(glAccountingCalendarValue.getAcvDateFrom());
			    gcTo.setTime(glAccountingCalendarValue.getAcvDateTo());
			    
			    gcFrom.add(Calendar.YEAR, 1);
			    gcTo.add(Calendar.YEAR, 1);
			    	    
			    
			    if (gcTo.isLeapYear(gcTo.get(Calendar.YEAR)) && 
			        gcTo.get(Calendar.MONTH) == 1 &&
			        gcTo.get(Calendar.DATE) == 28) {
			    	
			        gcTo.add(Calendar.DATE, 1);	
			    	
			    } else if (!gcTo.isLeapYear(gcTo.get(Calendar.YEAR)) && 
			        gcTo.get(Calendar.MONTH) == 2 &&
			        gcTo.get(Calendar.DATE) == 1) {
			        	
			        gcTo.add(Calendar.DATE, -1);
			        	
			    }
			    
			    LocalGlAccountingCalendarValue glNewAccountingCalendarValue = 
			       glAccountingCalendarValueHome.create(glAccountingCalendarValue.getAcvPeriodPrefix(),
			           glAccountingCalendarValue.getAcvQuarter(),
			           glAccountingCalendarValue.getAcvPeriodNumber(),
			           gcFrom.getTime(), gcTo.getTime(),
			           'N', null, null, null, null, AD_CMPNY);
			           
			    glNewAccountingCalendar.addGlAccountingCalendarValue(glNewAccountingCalendarValue);
			    
			    // check if first to be used in tc generation
					  
			    if (glNewAccountingCalendarValue.getAcvPeriodNumber() == 1) {
			  	
			  	    glFirstAccountingCalendarValue = glNewAccountingCalendarValue;
			  	
			    }
			  
			    // check if last to be used in tc generation
			  
			    if (!i.hasNext()) {
			  	
			  	    glLastAccountingCalendarValue = glNewAccountingCalendarValue;
			  	
			    }
			       	
	     	 }
	     	 
	     	 // generate transaction calendar
	          
		      LocalGlTransactionCalendar glTransactionCalendar = 
		          glTransactionCalendarHome.create(glNewAccountingCalendar.getAcName(),
		              glNewAccountingCalendar.getAcDescription(), AD_CMPNY);
		      
		      gc = new GregorianCalendar();
		      gc.setTime(glFirstAccountingCalendarValue.getAcvDateFrom());
		      
		      while (!gc.getTime().after(glLastAccountingCalendarValue.getAcvDateTo())) {
		      			      	  		      	  
		      	  LocalGlTransactionCalendarValue glTransactionCalendarValue =
		      	      glTransactionCalendarValueHome.create(gc.getTime(),
		      	          (short)gc.get(Calendar.DAY_OF_WEEK),
		      	          EJBCommon.TRUE, AD_CMPNY);
		      	          
		      	 glTransactionCalendar.addGlTransactionCalendarValue(glTransactionCalendarValue);
		      	      
		      	  gc.add(Calendar.DATE, 1);
		      	          
		      	
		      }
		      
		      // generate set of book
		      
		      glSubsequentSetOfBook = glSetOfBookHome.create(EJBCommon.FALSE, AD_CMPNY);
		      
		      glNewAccountingCalendar.addGlSetOfBook(glSubsequentSetOfBook);
		      glTransactionCalendar.addGlSetOfBook(glSubsequentSetOfBook);
		      
		      
		      
		      // generate coa balances
		      
		      Collection glChartOfAccounts = glChartOfAccountHome.findCoaAll(AD_CMPNY);
		      glAccountingCalendarValues = glNewAccountingCalendar.getGlAccountingCalendarValues();
		      
		      Iterator iterCoa = glChartOfAccounts.iterator();
		      
		      while (iterCoa.hasNext()) {
		      	
		      	 LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount) iterCoa.next();
		      	 
		         Iterator iterAcv = glAccountingCalendarValues.iterator();
		         
			     while (iterAcv.hasNext()) {
			     	
			     	LocalGlAccountingCalendarValue glAccountingCalendarValue =
			      	   (LocalGlAccountingCalendarValue) iterAcv.next();
			      	
			      	LocalGlChartOfAccountBalance glChartOfAccountBalance = glChartOfAccountBalanceHome.create(0d,0d,0d,0d, AD_CMPNY);
			      	glAccountingCalendarValue.addGlChartOfAccountBalance(glChartOfAccountBalance);
		      		glChartOfAccount.addGlChartOfAccountBalance(glChartOfAccountBalance);
		      		
		         }
		         
		      }   
		      
		      
		      
		      
		      
		      
		      
		      
		      
		      
		      // generate investor balances
		     
		      Collection glInvestors = apSupplierHome.findAllSplInvestor(AD_CMPNY);
		      glAccountingCalendarValues = glNewAccountingCalendar.getGlAccountingCalendarValues();
		      
		      Iterator iterInvtr = glInvestors.iterator();
		      
		      while (iterInvtr.hasNext()) {
		      	
		    	  LocalApSupplier apSupplier = (LocalApSupplier) iterInvtr.next();
		      	  
		         Iterator iterAcv = glAccountingCalendarValues.iterator();
		         
			     while (iterAcv.hasNext()) {
			     	
			     	LocalGlAccountingCalendarValue glAccountingCalendarValue =
			      	   (LocalGlAccountingCalendarValue) iterAcv.next();
			      	
			     	LocalGlInvestorAccountBalance glInvestorAccountBalance = glInvestorAccountBalanceHome.create(
			     			0d, 0d, (byte)0 , (byte)0, 0d, 0d, 0d, 0d, 0d, 0d, AD_CMPNY);
			     	
			     	glAccountingCalendarValue.addGlInvestorAccountBalance(glInvestorAccountBalance);
			     	apSupplier.addGlInvestorAccountBalance(glInvestorAccountBalance);
			      	
		         }
		         
		      }
		      
		      
	     		  	  	
	  	 }
	  	 
	  	 // carry forward balances to subsequent year
	  	  	  	  	  	 
	  	 // from and to acvs	  	 
	  	 	  	 
	  	 LocalGlAccountingCalendarValue glFromAccountingCalendarValue = 
	  	     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
	  	     	glSetOfBook.getGlAccountingCalendar().getAcCode(),
	  	     	glSetOfBook.getGlAccountingCalendar().getGlPeriodType().getPtPeriodPerYear(), AD_CMPNY);
	  	     	
	  	 LocalGlAccountingCalendarValue glToAccountingCalendarValue = 
	  	     glAccountingCalendarValueHome.findByAcCodeAndAcvPeriodNumber(
	  	     	glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(),
	  	     	(short)1, AD_CMPNY);
	  	 
	  	 
	  	 
	  	 Collection glForwardChartOfAccounts = glChartOfAccountHome.findCoaAll(AD_CMPNY);
	  	 
	  	 Iterator iterFCoa = glForwardChartOfAccounts.iterator();
	  	 
	  	 while (iterFCoa.hasNext()) {
	  	 	
	  	 	 LocalGlChartOfAccount glChartOfAccount = (LocalGlChartOfAccount)iterFCoa.next();
	  	 	 LocalGlChartOfAccount glRetainedEarningsChartOfAccount = null;
	  	 	 
	  	 	 String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	  	 	 
	  	 	 
	  	 	 
	  	 	 // carry forward balances
	  	 	 
	  	 	 LocalGlChartOfAccountBalance glFromChartOfAccountBalance = 
	  	 	     glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
	  	 	     	glFromAccountingCalendarValue.getAcvCode(), 
	  	 	     	glChartOfAccount.getCoaCode(), AD_CMPNY);
	  	 	 
	  	 	System.out.println("glToAccountingCalendarValue.getAcvCode()="+glToAccountingCalendarValue.getAcvCode());
		  	
	  	 	
	  	 	 LocalGlChartOfAccountBalance glToChartOfAccountBalance = 
	  	 	     glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
	  	 	     	glToAccountingCalendarValue.getAcvCode(), 
	  	 	     	glChartOfAccount.getCoaCode(), AD_CMPNY);
	  	 	 
	  	 	System.out.println("glToChartOfAccountBalance.getCoabCode()="+glToChartOfAccountBalance.getCoabCode());
		  	
	  	 	
	  	 	     	
	  	 	 if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
				 ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
				        				    			    					    						    				           		
	           	 glToChartOfAccountBalance.setCoabBeginningBalance(
	           		 glToChartOfAccountBalance.getCoabBeginningBalance() +
	           		 glFromChartOfAccountBalance.getCoabEndingBalance());
	           		
	             glToChartOfAccountBalance.setCoabEndingBalance(
	            	 glToChartOfAccountBalance.getCoabEndingBalance() +
	           		 glFromChartOfAccountBalance.getCoabEndingBalance());
					    
		     } else { // revenue & expense
										
		     	try {
		     		
		     		glRetainedEarningsChartOfAccount = 
				    	   glChartOfAccountHome.findByCoaAccountNumber(adCompany.getCmpRetainedEarnings(), AD_CMPNY);
					    
		     	} catch(FinderException ex) {
		     		
		     		throw new GlobalAccountNumberInvalidException();
		     		
		     	}
			    
			    LocalGlChartOfAccountBalance glRetainedEarningsBalance = 
		    	    glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
				      glToAccountingCalendarValue.getAcvCode(),
		              glRetainedEarningsChartOfAccount.getCoaCode(), AD_CMPNY);
		        
		        if (ACCOUNT_TYPE.equals("REVENUE")) {
		        	
		            glRetainedEarningsBalance.setCoabBeginningBalance(
		        	    glRetainedEarningsBalance.getCoabBeginningBalance() +
		        	    glFromChartOfAccountBalance.getCoabEndingBalance());
		        	    
		            glRetainedEarningsBalance.setCoabEndingBalance(
		            	glRetainedEarningsBalance.getCoabEndingBalance() +
		            	glFromChartOfAccountBalance.getCoabEndingBalance());				           	
		        	    
		        } else { // expense
		        	
		        	glRetainedEarningsBalance.setCoabBeginningBalance(
		        	    glRetainedEarningsBalance.getCoabBeginningBalance() -
		        	    glFromChartOfAccountBalance.getCoabEndingBalance());
		        	    
		        	glRetainedEarningsBalance.setCoabEndingBalance(
		            	glRetainedEarningsBalance.getCoabEndingBalance() -
		            	glFromChartOfAccountBalance.getCoabEndingBalance());
		        	
		        }			            			           					    	   				    
												
		     }
		     
		     // carry forward to subsequent periods of the subsequent year
		     
		     Collection glSubsequentAccountingCalendarValues = 
	               glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
	                   glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(),
	                   (short)1, AD_CMPNY);     
	                   
	         Iterator j = glSubsequentAccountingCalendarValues.iterator();
	         
	         while (j.hasNext()) {
	         	
	         	 LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
	         	       (LocalGlAccountingCalendarValue)j.next();
	         	       
	         	 if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
				 	 ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
				 	 	
				 	 	LocalGlChartOfAccountBalance glChartOfAccountBalance = 
				            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
				               	  glSubsequentAccountingCalendarValue.getAcvCode(),
				               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
				               	  
				        glChartOfAccountBalance.setCoabEndingBalance(
				            glChartOfAccountBalance.getCoabEndingBalance() + 
				            glFromChartOfAccountBalance.getCoabEndingBalance()); 					   
				               	  
				        glChartOfAccountBalance.setCoabBeginningBalance(
				       		glChartOfAccountBalance.getCoabBeginningBalance() + 
				       		glFromChartOfAccountBalance.getCoabEndingBalance());
				 	 	
				 	 	
				 } else { // revenue & expense
				 
				       LocalGlChartOfAccountBalance glChartOfAccountBalance = 
				            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
				               	  glSubsequentAccountingCalendarValue.getAcvCode(),
				               	  glRetainedEarningsChartOfAccount.getCoaCode(), AD_CMPNY);
				               	  
				               	  
				       if (ACCOUNT_TYPE.equals("REVENUE")) {
		        	
				            glChartOfAccountBalance.setCoabEndingBalance(
					            glChartOfAccountBalance.getCoabEndingBalance() + 
					            glFromChartOfAccountBalance.getCoabEndingBalance()); 					   
					               	  
					        glChartOfAccountBalance.setCoabBeginningBalance(
					       		glChartOfAccountBalance.getCoabBeginningBalance() + 
					       		glFromChartOfAccountBalance.getCoabEndingBalance());				           	
				        	    
				       } else { // expense
				        	
				        	glChartOfAccountBalance.setCoabEndingBalance(
					            glChartOfAccountBalance.getCoabEndingBalance() - 
					            glFromChartOfAccountBalance.getCoabEndingBalance()); 					   
					               	  
					        glChartOfAccountBalance.setCoabBeginningBalance(
					       		glChartOfAccountBalance.getCoabBeginningBalance() - 
					       		glFromChartOfAccountBalance.getCoabEndingBalance());
				        	
				       }				               	  
				        				 
				 }
	         		         	
	         }	  	 	     	
	  	 	 
	  	 }
	  	 
	  	 
	  	 

	  	Collection glForwardInvestor = apSupplierHome.findAllSplInvestor(AD_CMPNY);
	  	 
	  	 
	  	Iterator iterInvtr = glForwardInvestor.iterator();
	  	 
	  	 while (iterInvtr.hasNext()) {
	  		 
	  		 
	  		LocalApSupplier apSupplier = (LocalApSupplier)iterInvtr.next();
	  		 
	  		
	  		LocalGlInvestorAccountBalance glFromInvestorBalance = 
	  				glInvestorAccountBalanceHome.findByAcvCodeAndSplCode(
				  	 	     	glFromAccountingCalendarValue.getAcvCode(), 
				  	 	     apSupplier.getSplCode(), AD_CMPNY);
	  		
	  		System.out.println("apSupplier.getSplCode()="+apSupplier.getSplCode());
	  		System.out.println("glToAccountingCalendarValue.getAcvCode()="+glToAccountingCalendarValue.getAcvCode());
	  		
	  		LocalGlInvestorAccountBalance glToInvestorBalance = 
	  				glInvestorAccountBalanceHome.findByAcvCodeAndSplCode(
				  	 	     	glToAccountingCalendarValue.getAcvCode(), 
				  	 	     apSupplier.getSplCode(), AD_CMPNY);
	  		

	  		
		  		glToInvestorBalance.setIrabBeginningBalance(
		  				glToInvestorBalance.getIrabBeginningBalance() +
		           		 glFromInvestorBalance.getIrabEndingBalance());
		           		
		  		glToInvestorBalance.setIrabEndingBalance(
		  				glToInvestorBalance.getIrabEndingBalance() +
		           		 glFromInvestorBalance.getIrabEndingBalance());
	  		
	  	
	  		
	  	// carry forward to subsequent periods of the subsequent year
		     
		     Collection glSubsequentAccountingCalendarValues = 
	               glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
	                   glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(),
	                   (short)1, AD_CMPNY);     
	                   
	         Iterator j = glSubsequentAccountingCalendarValues.iterator();
	         
	         while (j.hasNext()) {
	        	 
	        	 LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
		         	       (LocalGlAccountingCalendarValue)j.next();
	        	 
	        	 
	        	 
	        	 LocalGlInvestorAccountBalance glInvestorAccountBalance = 
	        			 glInvestorAccountBalanceHome.findByAcvCodeAndSplCode(
				               	  glSubsequentAccountingCalendarValue.getAcvCode(),
				               	  apSupplier.getSplCode(), AD_CMPNY);

				               	  
	        	 glInvestorAccountBalance.setIrabEndingBalance(
	        			 glInvestorAccountBalance.getIrabEndingBalance() + 
				            glFromInvestorBalance.getIrabEndingBalance()); 					   
				               	  
	        	 glInvestorAccountBalance.setIrabBeginningBalance(
	        			 glInvestorAccountBalance.getIrabBeginningBalance() + 
				       		glFromInvestorBalance.getIrabEndingBalance());
	        	 
	        	 
	         }
	         	

	  		 
	  	 }
	  	 
	  	 
	  	 
	  	 
	  	 
	  	 
	  	 
	  	 
	  	 
	  	 
	  	 
	  	 
	  	 
	  	 
	  	 // set acvs to closed
	  	 
	  	 Collection glToCloseAccountingCalendarValues = glAccountingCalendar.getGlAccountingCalendarValues();
	  	 
	  	 Iterator iterAcv = glToCloseAccountingCalendarValues.iterator();
	  	 
	  	 while (iterAcv.hasNext()) {
	  	 	
	  	 	 LocalGlAccountingCalendarValue glAccountingCalendarValue = (LocalGlAccountingCalendarValue)iterAcv.next();
	  	 	 
	  	 	 glAccountingCalendarValue.setAcvStatus('C');
	  	 	 
	  	 	 if (glAccountingCalendarValue.getAcvDateClosed() == null) {
	  	 	 	
	  	 	 	  glAccountingCalendarValue.setAcvDateClosed(EJBCommon.getGcCurrentDateWoTime().getTime());
	  	 	 	
	  	 	 }
	  	 	
	  	 }
	  	 
	  	 // set sob to closed
	  	 
	  	 glSetOfBook.setSobYearEndClosed(EJBCommon.TRUE);	  	  
	  	  	  	
	  } catch (GlobalNotAllTransactionsArePostedException ex) {
	  
	  	  throw ex;	  
	  	  
	  } catch (GlobalAccountNumberInvalidException ex) {
		  
		  throw ex;	
	  
	  } catch (Exception ex) {
	  	   
	  	  Debug.printStackTrace(ex);
	  	  getSessionContext().setRollbackOnly();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }

   }
   
   
   // Private Methods
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
       
       Debug.print("GlYearEndClosingControllerBean getGlFcPrecisionUnit");
       
       
       LocalAdCompanyHome adCompanyHome = null;
       
       
       // Initialize EJB Home
       
       try {
           
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
           
       } catch (NamingException ex) {
           
           throw new EJBException(ex.getMessage());
           
       }
       
       try {
           
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           
           return  adCompany.getGlFunctionalCurrency().getFcPrecision();
           
       } catch (Exception ex) {
           
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
           
       }
       
   }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("GlYearEndClosingControllerBean ejbCreate");

   }

}