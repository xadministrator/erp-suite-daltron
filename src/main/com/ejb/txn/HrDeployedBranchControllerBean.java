
/*
 * HrDeployedBranchControllerBean.java
 *
 * Created on Aug 8, 2018, 12:00 AM
 *
 * @author  Clint Arrogante
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.hr.LocalHrDeployedBranch;
import com.ejb.hr.LocalHrDeployedBranchHome;
import com.util.AbstractSessionBean;
import com.util.HrDeployedBranchDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="HrDeployedBranchControllerEJB"
 *           display-name="Used for entering banks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/HrDeployedBranchControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.HrDeployedBranchController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.HrDeployedBranchControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="hruser"
 *                        role-link="hruserlink"
 *
 * @ejb:permission role-name="hruser"
 * 
*/

public class HrDeployedBranchControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getHrDbAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("HrDeployedBranchControllerBean getHrDbAll");
        
        LocalHrDeployedBranchHome hrDeployedBranchHome = null;
        
        Collection hrDeployedBranchs = null;
        
        LocalHrDeployedBranch hrDeployedBranch = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	hrDeployedBranchs = hrDeployedBranchHome.findDbAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (hrDeployedBranchs.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = hrDeployedBranchs.iterator();
               
        while (i.hasNext()) {
        	
        	hrDeployedBranch = (LocalHrDeployedBranch)i.next();
        
                
        	HrDeployedBranchDetails details = new HrDeployedBranchDetails();
	    		/*details.setDbCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());*/
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addHrDbEntry(com.util.HrDeployedBranchDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("HrDeployedBranchControllerBean addHrDbEntry");
        
        LocalHrDeployedBranchHome hrDeployedBranchHome = null;

        LocalHrDeployedBranch hrDeployedBranch = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);
                          
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
        	hrDeployedBranch = hrDeployedBranchHome.findByReferenceID(details.getDbReferenceID(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create new bank
        	
        	hrDeployedBranch = hrDeployedBranchHome.create(
        			details.getDbReferenceID(), details.getDbClientCode(), details.getDbClientName(),
        			details.getDbTin(), details.getDbAddress(), details.getDbManagingBranch(),
        			AD_CMPNY); 	        
                	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateHrDbEntry(com.util.HrDeployedBranchDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("HrDeployedBranchControllerBean updateHrDbEntry");
        
        LocalHrDeployedBranchHome hrDeployedBranchHome = null;

        LocalHrDeployedBranch hrDeployedBranch = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);
                          
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalHrDeployedBranch adExistingDeployedBranch = hrDeployedBranchHome.findByReferenceID(details.getDbReferenceID(), AD_CMPNY);
            
            if (!adExistingDeployedBranch.getDbReferenceID().equals(details.getDbReferenceID())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	hrDeployedBranch = hrDeployedBranchHome.findByPrimaryKey(details.getDbCode());
        	
        	hrDeployedBranch.setDbReferenceID(details.getDbReferenceID());
        	hrDeployedBranch.setDbClientCode(details.getDbClientCode());
        	hrDeployedBranch.setDbClientName(details.getDbClientName());
        	hrDeployedBranch.setDbTin(details.getDbTin());
        	hrDeployedBranch.setDbAddress(details.getDbAddress());
        	hrDeployedBranch.setDbManagingBranch(details.getDbManagingBranch());
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteHrDeployedBranchEntry(Integer PP_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("HrDeployedBranchControllerBean deleteHrDeployedBranchEntry");
        
        LocalHrDeployedBranchHome hrDeployedBranchHome = null;

        
        LocalHrDeployedBranch hrDeployedBranch = null;           
               
        // Initialize EJB Home
        
        try {
            
        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);                      
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
	    	hrDeployedBranch = hrDeployedBranchHome.findByPrimaryKey(PP_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {

	    	hrDeployedBranch.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("HrDeployedBranchControllerBean ejbCreate");
      
    }
}
