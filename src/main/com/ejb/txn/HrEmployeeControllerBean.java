
/*
 * HrEmployeeControllerBean.java
 *
 * Created on Aug 8, 2018, 12:00 AM
 *
 * @author  Clint Arrogante
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.hr.LocalHrDeployedBranch;
import com.ejb.hr.LocalHrDeployedBranchHome;
import com.ejb.hr.LocalHrEmployee;
import com.ejb.hr.LocalHrEmployeeHome;
import com.util.AbstractSessionBean;
import com.util.HrEmployeeDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="HrEmployeeControllerEJB"
 *           display-name="Used for entering banks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/HrEmployeeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.HrEmployeeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.HrEmployeeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="hruser"
 *                        role-link="hruserlink"
 *
 * @ejb:permission role-name="hruser"
 * 
*/

public class HrEmployeeControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getHrEmpAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("HrEmployeeControllerBean getHrEmpAll");
        
        LocalHrEmployeeHome hrEmployeeHome = null;
        
        Collection hrEmployees = null;
        
        LocalHrEmployee hrEmployee = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	hrEmployeeHome = (LocalHrEmployeeHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrEmployeeHome.JNDI_NAME, LocalHrEmployeeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	hrEmployees = hrEmployeeHome.findEmpAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (hrEmployees.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = hrEmployees.iterator();
               
        while (i.hasNext()) {
        	
        	hrEmployee = (LocalHrEmployee)i.next();
        
                
        	HrEmployeeDetails details = new HrEmployeeDetails();
	    		/*details.setDbCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());*/
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addHrEmpEntry(com.util.HrModEmployeeDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("HrEmployeeControllerBean addHrEmpEntry");
        
        LocalHrEmployeeHome hrEmployeeHome = null;
        LocalHrDeployedBranchHome hrDeployedBranchHome = null;

        LocalHrEmployee hrEmployee = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	hrEmployeeHome = (LocalHrEmployeeHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrEmployeeHome.JNDI_NAME, LocalHrEmployeeHome.class);
        	
        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);
                          
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
        	hrEmployee = hrEmployeeHome.findByReferenceID(details.getEmpReferenceID(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create new bank
        	
        	hrEmployee = hrEmployeeHome.create(
        			details.getEmpReferenceID(), details.getEmpEmployeeNumber(), details.getEmpBioNumber(), 
        			details.getEmpFirstName(), details.getEmpMiddleName(), details.getEmpLastName(), details.getEmpCurrentJobPosition(),
        			details.getEmpManagingBranch(), AD_CMPNY);
        	
        	
        	if(details.getHrDeployedBranch() != null) {

        		LocalHrDeployedBranch hrDeployedBranch = hrDeployedBranchHome.findByReferenceID(details.getHrDeployedBranch(), AD_CMPNY);
        		
        		hrEmployee.setHrDeployedBranch(hrDeployedBranch);
        	}
                	       
        	
        	
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateHrEmpEntry(com.util.HrModEmployeeDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("HrEmployeeControllerBean updateHrEmpEntry");
        
        LocalHrEmployeeHome hrEmployeeHome = null;
        LocalHrDeployedBranchHome hrDeployedBranchHome = null;
        
        LocalHrEmployee hrEmployee = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	hrEmployeeHome = (LocalHrEmployeeHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrEmployeeHome.JNDI_NAME, LocalHrEmployeeHome.class);
        	
        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);
            
        	
                          
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalHrEmployee adExistingEmployee = hrEmployeeHome.findByReferenceID(details.getEmpReferenceID(), AD_CMPNY);
            
            if (!adExistingEmployee.getEmpReferenceID().equals(details.getEmpReferenceID())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	hrEmployee = hrEmployeeHome.findByPrimaryKey(details.getEmpCode());
        	
        	hrEmployee.setEmpEmployeeNumber(details.getEmpEmployeeNumber());
        	hrEmployee.setEmpBioNumber(details.getEmpBioNumber());
        	hrEmployee.setEmpFirstName(details.getEmpFirstName());
        	hrEmployee.setEmpMiddleName(details.getEmpMiddleName());
        	hrEmployee.setEmpLastName(details.getEmpLastName());
        	hrEmployee.setEmpCurrentJobPosition(details.getEmpCurrentJobPosition());
        	hrEmployee.setEmpManagingBranch(details.getEmpManagingBranch());
        	
        	if(details.getHrDeployedBranch() != null) {

        		LocalHrDeployedBranch hrDeployedBranch = hrDeployedBranchHome.findByReferenceID(details.getHrDeployedBranch(), AD_CMPNY);
        		
        		hrEmployee.setHrDeployedBranch(hrDeployedBranch);
        	}
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteHrEmployeeEntry(Integer PP_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("HrEmployeeControllerBean deleteHrEmployeeEntry");
        
        LocalHrEmployeeHome hrEmployeeHome = null;

        
        LocalHrEmployee hrEmployee = null;           
               
        // Initialize EJB Home
        
        try {
            
        	hrEmployeeHome = (LocalHrEmployeeHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrEmployeeHome.JNDI_NAME, LocalHrEmployeeHome.class);                      
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
	    	hrEmployee = hrEmployeeHome.findByPrimaryKey(PP_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {

	    	hrEmployee.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("HrEmployeeControllerBean ejbCreate");
      
    }
}
