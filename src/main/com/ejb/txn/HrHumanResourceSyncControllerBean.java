
/*
 * HrHumanResourceSyncControllerBean
 *
 * Created on June 9, 2018, 10:16 PM
 *
 * @author Clint Arrogante
 */

package com.ejb.txn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchSupplier;
import com.ejb.ad.LocalAdBranchSupplierHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.util.ArInvoiceDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.HrModEmployeeDetails;

/**
 * 
 * @ejb:bean name="HrHumanResourceSyncControllerBean"
 * 			 type="Stateless"
 * 			 view-type="service-endpoint"
 * 
 * @wsee:port-component name ="HrHumanResourceSync"
 * 
 * @jboss:port-component uri="omega-ejb/HrHumanResourceSyncWS"
 * 
 * @ejb:interface service-endpoint-class="com.ejb.txn.HrHumanResourceSyncWS"
 * 
 **/

public class HrHumanResourceSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
	
	
	/**
     * @ejb:interface-method
     **/
    public String getCompanyName(String CMP_SHRT_NM) {    	
    
    	Debug.print("HrHumanResourceSyncControllerBean getCompanyName");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);

        	return adCompany.getCmpName();
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArEmployeesDeductionUniform(Integer DPLYD_BRNCH, String DT_TO, String CMP_SHRT_NM) {    	
    
    	Debug.print("HrHumanResourceSyncControllerBean getArEmployeesDeductionUniform");
    	
    	//Param 1 = Deployed Branch
    	//Param 2 = Deduction Type
    	//Param 3 = Date To
    	//Param 4 = Company Name
    	
    	LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArCustomerHome arCustomerHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
        	
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);
        	
        	Date dateTo = new SimpleDateFormat("yyyy-MM-dd").parse(DT_TO);
        	
        	Collection arIPSDeductions = null;
        	
        	if(DPLYD_BRNCH > 0) {
        		
        		arIPSDeductions = arInvoicePaymentScheduleHome.findOpenIpsByInvHrDbReferenceIDAndIbName(DPLYD_BRNCH, "Uniform", dateTo , adCompany.getCmpCode());
	
        	} else {
        		
        		arIPSDeductions = arInvoicePaymentScheduleHome.findOpenIpsByCstIbName("Uniform", dateTo , adCompany.getCmpCode());
        		
        	}
        	
        	Iterator i = arIPSDeductions.iterator();
        	
        	ArrayList list = new ArrayList();
        	
        	while(i.hasNext()) {
        		
        		LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();
        		
        		Collection arInvoiceLineItems = arInvoicePaymentSchedule.getArInvoice().getArInvoiceLineItems();
        		
        		Iterator x = arInvoiceLineItems.iterator();
        		
        		while(x.hasNext()) {
        			LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)x.next();
        			
        			list.add(arInvoiceLineItem);
        			
        		}

        	}
        	
        	
        	String[] results = new String[list.size()];
        	Iterator j = list.iterator();
        	
        	int ctr = 0;
	        while (j.hasNext()) {
	        	
	        	LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)j.next();
	        	
	        	results[ctr] = employeeDeductionUniformRowEncode(arInvoiceLineItem);
	        	ctr++;	        	
	        }
	        
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArEmployeesDeductionMicrofinanceLoan(Integer DPLYD_BRNCH, String DT_TO, String CMP_SHRT_NM) {    	
    
    	Debug.print("HrHumanResourceSyncControllerBean getArEmployeesDeductionMicrofinanceLoan");
    	
    	//Param 1 = Deployed Branch
    	//Param 2 = Deduction Type
    	//Param 3 = Date To
    	//Param 4 = Company Name
    	
    	LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArCustomerHome arCustomerHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
        	
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);
        	
        	Date dateTo = new SimpleDateFormat("yyyy-MM-dd").parse(DT_TO);
        	
        	Collection arIPSMLs = null;
        	
        	if(DPLYD_BRNCH > 0) {
        		
        		arIPSMLs = arInvoicePaymentScheduleHome.findOpenIpsByInvHrDbReferenceIDAndIbName(DPLYD_BRNCH, "Microfinance Loan", dateTo , adCompany.getCmpCode());
	
        	} else {
        		
        		arIPSMLs = arInvoicePaymentScheduleHome.findOpenIpsByCstIbName("Microfinance Loan", dateTo , adCompany.getCmpCode());
        		
        	}
        	
        	Iterator i = arIPSMLs.iterator();
        	
        	ArrayList list = new ArrayList();
        	
        	while(i.hasNext()) {
        		
        		LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();

        		list.add(arInvoicePaymentSchedule);

        	}
        	
        	
        	String[] results = new String[list.size()];
        	Iterator j = list.iterator();
        	
        	int ctr = 0;
	        while (j.hasNext()) {
	        	
	        	LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)j.next();
	        	
	        	results[ctr] = employeeDeductionLoanRowEncode(arInvoicePaymentSchedule);
	        	ctr++;	        	
	        }
	        
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArEmployeesDeductionCashAdvanceLoan(Integer DPLYD_BRNCH, String DT_TO, String CMP_SHRT_NM) {    	
    
    	Debug.print("HrHumanResourceSyncControllerBean getArEmployeesDeductionCashAdvanceLoan");
    	
    	//Param 1 = Deployed Branch
    	//Param 2 = Deduction Type
    	//Param 3 = Date To
    	//Param 4 = Company Name
    	
    	LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArCustomerHome arCustomerHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
        	
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);
        	
        	Date dateTo = new SimpleDateFormat("yyyy-MM-dd").parse(DT_TO);
        	
        	Collection arIPSMLs = null;
        	
        	if(DPLYD_BRNCH > 0) {
        		
        		arIPSMLs = arInvoicePaymentScheduleHome.findOpenIpsByInvHrDbReferenceIDAndIbName(DPLYD_BRNCH, "Cash Advance Loan", dateTo , adCompany.getCmpCode());
	
        	} else {
        		
        		arIPSMLs = arInvoicePaymentScheduleHome.findOpenIpsByCstIbName("Cash Advance Loan", dateTo , adCompany.getCmpCode());
        		
        	}
        	
        	Iterator i = arIPSMLs.iterator();
        	
        	ArrayList list = new ArrayList();
        	
        	while(i.hasNext()) {
        		
        		LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();

        		list.add(arInvoicePaymentSchedule);

        	}
        	
        	
        	String[] results = new String[list.size()];
        	Iterator j = list.iterator();
        	
        	int ctr = 0;
	        while (j.hasNext()) {
	        	
	        	LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)j.next();
	        	
	        	results[ctr] = employeeDeductionLoanRowEncode(arInvoicePaymentSchedule);
	        	ctr++;	        	
	        }
	        
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArEmployeesDeductionSSSLoan(Integer DPLYD_BRNCH, String DT_TO, String CMP_SHRT_NM) {    	
    
    	Debug.print("HrHumanResourceSyncControllerBean getArEmployeesDeductionSSSLoan");
    	
    	//Param 1 = Deployed Branch
    	//Param 2 = Deduction Type
    	//Param 3 = Date To
    	//Param 4 = Company Name
    	
    	LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArCustomerHome arCustomerHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
        	
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);
        	
        	Date dateTo = new SimpleDateFormat("yyyy-MM-dd").parse(DT_TO);
        	
        	Collection arIPSMLs = null;
        	
        	if(DPLYD_BRNCH > 0) {
        		
        		arIPSMLs = arInvoicePaymentScheduleHome.findOpenIpsByInvHrDbReferenceIDAndIbName(DPLYD_BRNCH, "SSS Loan", dateTo , adCompany.getCmpCode());
	
        	} else {
        		
        		arIPSMLs = arInvoicePaymentScheduleHome.findOpenIpsByCstIbName("SSS Loan", dateTo , adCompany.getCmpCode());
        		
        	}
        	
        	Iterator i = arIPSMLs.iterator();
        	
        	ArrayList list = new ArrayList();
        	
        	while(i.hasNext()) {
        		
        		LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();

        		list.add(arInvoicePaymentSchedule);

        	}
        	
        	
        	String[] results = new String[list.size()];
        	Iterator j = list.iterator();
        	
        	int ctr = 0;
	        while (j.hasNext()) {
	        	
	        	LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)j.next();
	        	
	        	results[ctr] = employeeDeductionLoanRowEncode(arInvoicePaymentSchedule);
	        	ctr++;	        	
	        }
	        
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArEmployeesDeductionHDMFLoan(Integer DPLYD_BRNCH, String DT_TO, String CMP_SHRT_NM) {    	
    
    	Debug.print("HrHumanResourceSyncControllerBean getArEmployeesDeductionHDMFLoan");
    	
    	//Param 1 = Deployed Branch
    	//Param 2 = Deduction Type
    	//Param 3 = Date To
    	//Param 4 = Company Name
    	
    	LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArCustomerHome arCustomerHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
        	
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);
        	
        	Date dateTo = new SimpleDateFormat("yyyy-MM-dd").parse(DT_TO);
        	
        	Collection arIPSMLs = null;
        	
        	if(DPLYD_BRNCH > 0) {
        		
        		arIPSMLs = arInvoicePaymentScheduleHome.findOpenIpsByInvHrDbReferenceIDAndIbName(DPLYD_BRNCH, "HDMF Loan", dateTo , adCompany.getCmpCode());
	
        	} else {
        		
        		arIPSMLs = arInvoicePaymentScheduleHome.findOpenIpsByCstIbName("HDMF Loan", dateTo , adCompany.getCmpCode());
        		
        	}
        	
        	Iterator i = arIPSMLs.iterator();
        	
        	ArrayList list = new ArrayList();
        	
        	while(i.hasNext()) {
        		
        		LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();

        		list.add(arInvoicePaymentSchedule);

        	}
        	
        	
        	String[] results = new String[list.size()];
        	Iterator j = list.iterator();
        	
        	int ctr = 0;
	        while (j.hasNext()) {
	        	
	        	LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)j.next();
	        	
	        	results[ctr] = employeeDeductionLoanRowEncode(arInvoicePaymentSchedule);
	        	ctr++;	        	
	        }
	        
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArEmployeesDeductionCashBond(Integer DPLYD_BRNCH, String CMP_SHRT_NM) {    	
    
    	Debug.print("HrHumanResourceSyncControllerBean getArEmployeesDeductionCashBond");
    	
    	//Param 1 = Deployed Branch
    	//Param 2 = Deduction Type
    	//Param 3 = Date To
    	//Param 4 = Company Name
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArCustomerHome arCustomerHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);

        	Collection arCustomers = null;
        	
        	if(DPLYD_BRNCH > 0) {
        		
        		arCustomers = arCustomerHome.findCstHrCashBondByCstHrDbReferenceID(DPLYD_BRNCH, adCompany.getCmpCode());
            	
        	} else {
        		
        		arCustomers = arCustomerHome.findCstHrCashBondByCstHrAll(adCompany.getCmpCode());
        		
        	}
        	
        	Iterator i = arCustomers.iterator();
        	
        	ArrayList list = new ArrayList();
        	
        	while(i.hasNext()) {
        		
        		LocalArCustomer arCustomer = (LocalArCustomer)i.next();

        		list.add(arCustomer);

        	}
        	
        	
        	String[] results = new String[list.size()];
        	Iterator j = list.iterator();
        	
        	int ctr = 0;
	        while (j.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)j.next();
	        	
	        	results[ctr] = employeeDeductionCashBondRowEncode(arCustomer);
	        	ctr++;	        	
	        }
	        
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArEmployeesDeductionInsMisc(Integer DPLYD_BRNCH, String CMP_SHRT_NM) {    	
    
    	Debug.print("HrHumanResourceSyncControllerBean getArEmployeesDeductionInsMisc");
    	
    	//Param 1 = Deployed Branch
    	//Param 2 = Deduction Type
    	//Param 3 = Date To
    	//Param 4 = Company Name
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArCustomerHome arCustomerHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);

        	Collection arCustomers = null;
        	
        	if(DPLYD_BRNCH > 0) {
        		
        		arCustomers = arCustomerHome.findCstHrCashBondByCstHrDbReferenceID(DPLYD_BRNCH, adCompany.getCmpCode());
            	
        	} else {
        		
        		arCustomers = arCustomerHome.findCstHrInsMiscByCstHrAll(adCompany.getCmpCode());
        		
        	}
        	
        	Iterator i = arCustomers.iterator();
        	
        	ArrayList list = new ArrayList();
        	
        	while(i.hasNext()) {
        		
        		LocalArCustomer arCustomer = (LocalArCustomer)i.next();

        		list.add(arCustomer);

        	}
        	
        	
        	String[] results = new String[list.size()];
        	Iterator j = list.iterator();
        	
        	int ctr = 0;
	        while (j.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)j.next();
	        	
	        	results[ctr] = employeeDeductionInsMiscRowEncode(arCustomer);
	        	ctr++;	        	
	        }
	        
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
   
	/**
     * @ejb:interface-method
     **//*
    public int getApEmployeeAllNewLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("HrHumanResourceSyncControllerBean getApEmployeeAllNewLength");
    	
    	LocalApSupplierHome apSupplierHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection apSupplier = apSupplierHome.findSplBySplNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');        	
        	
        	return apSupplier.size();
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    *//**
     * @ejb:interface-method
     **//*
    public int getApEmployeeAllUpdatedLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ApEmployeeSyncControllerBean getApEmployeeAllUpdatedLength");

    	LocalApSupplierHome apSupplierHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
            	lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection apSuppplier = apSupplierHome.findSplBySplNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U','U','X');        	
        	
        	return apSuppplier.size();
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
	
	*//**
     * @ejb:interface-method
     **//*
    public String[] getApEmployeeAllNewAndUpdated(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ApEmployeeSyncControllerBean getApEmployeeAllNewAndUpdated");
    	
    	LocalAdBranchSupplierHome adBranchSupplierHome = null;
    	LocalApSupplierHome apSupplierHome = null;
    	LocalAdBranchSupplier adBranchSupplier = null;
    	LocalAdBranchHome adBranchHome = null;    	
    	
    	// Initialize EJB Home    	
        try {
            
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);        	
        	Collection apSuppliers = apSupplierHome.findSplBySplNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');        	
        	Collection arUpdatedSuppliers = apSupplierHome.findSplBySplNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');
        	        	        	
        	String[] results = new String[apSuppliers.size() + arUpdatedSuppliers.size()];
        	Iterator i = apSuppliers.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	LocalApSupplier apSupplier = (LocalApSupplier)i.next();
	        	results[ctr] = employeeRowEncode(apSupplier);
	        	ctr++;	        	
	        }
	        i = arUpdatedSuppliers.iterator();
	        while (i.hasNext()) {
	        	LocalApSupplier apSupplier = (LocalApSupplier)i.next();
	        	results[ctr] = employeeRowEncode(apSupplier);
	        	ctr++;	        	
	        }
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    *//**
     * @ejb:interface-method
     **//*
    public int setApEmployeeAllNewAndUpdatedSuccessConfirmation(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("ApSupplierSyncControllerBean setApEmployeeAllNewAndUpdatedSuccessConfirmation");
    	
    	LocalAdBranchSupplierHome adBranchSupplierHome = null;
    	LocalAdBranchSupplier adBranchSupplier = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	adBranchSupplierHome = (LocalAdBranchSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchSupplierHome.JNDI_NAME, LocalAdBranchSupplierHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection adBranchSuppliers = adBranchSupplierHome.findSplBySplNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'U', 'X');
        	
        	Iterator i = adBranchSuppliers.iterator();        	
	        while (i.hasNext()) {
	        	 
	        	adBranchSupplier = (LocalAdBranchSupplier)i.next();	        	
	        	adBranchSupplier.setBsplSupplierDownloadStatus('D');
	        	
	        }
        	
        } catch (Exception ex) {
        	
        	ctx.setRollbackOnly();
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
        return 0;
        
    }*/
    
    private String employeeRowEncode(LocalApSupplier apSupplier) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	// Start separator
    	tempResult.append(separator);

    	// Primary Key
    	tempResult.append(apSupplier.getSplCode());
    	tempResult.append(separator);
    	
    	// Suppplier Code
    	tempResult.append(apSupplier.getSplSupplierCode());
    	tempResult.append(separator);
    	
    	// Name
    	tempResult.append(apSupplier.getSplName());
    	tempResult.append(separator);
    	
    	// Tin
    	tempResult.append(apSupplier.getSplTin());
    	tempResult.append(separator);
    	
    	// Address
    	if (apSupplier.getSplAddress().length() < 1) {
    		
    		tempResult.append("not specified");
    		tempResult.append(separator);
    		
    	} else {
    		
    		tempResult.append(apSupplier.getSplAddress());
    		tempResult.append(separator);
    		
    	}    	
    	
    	// Bank Account Name
    	tempResult.append(apSupplier.getAdBankAccount().getBaName());
    	tempResult.append(separator);
    	
    	// Tax Code
    	tempResult.append(apSupplier.getApSupplierClass().getApTaxCode().getTcName());
    	tempResult.append(separator);  	
    	
    	// Withholding Tax Code
    	tempResult.append(apSupplier.getApSupplierClass().getApWithholdingTaxCode().getWtcName());
    	tempResult.append(separator);   	
    	
    	// remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();    	
    	    	
    	encodedResult = encodedResult.replace("'", "\'");    	
    	
    	return encodedResult;
    	
    }
    
    
    
    
    private String employeeDeductionLoanRowEncode(LocalArInvoicePaymentSchedule arInvoicePaymentSchedule) {
    	
    	
    	LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArCustomerHome arCustomerHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
        	
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	// Start separator
    	tempResult.append(separator);
    	
    	// Deployed Branch
    	tempResult.append(arInvoicePaymentSchedule.getArInvoice().getHrDeployedBranch());
    	tempResult.append(separator);
    	
    	// deductionType
    	tempResult.append(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch().getIbName());
    	tempResult.append(separator);
    	
    	// employee ID
    	tempResult.append(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getHrEmployee().getEmpReferenceID());
    	tempResult.append(separator);
    	
    	// employee Number
    	tempResult.append(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getHrEmployee().getEmpEmployeeNumber());
    	tempResult.append(separator);
    	
    	// employee current job postion
    	tempResult.append(arInvoicePaymentSchedule.getArInvoice().getInvHrCurrentJobPosition());
    	tempResult.append(separator);
    	
    	LocalDate date = new LocalDate(arInvoicePaymentSchedule.getIpsDueDate());
    	DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
    	String str = date.toString(fmt);
    			 
    	// date
    	tempResult.append(str);
    	tempResult.append(separator);

    	// Installment Number
    	tempResult.append(arInvoicePaymentSchedule.getIpsNumber() + "of"+arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getAdPaymentSchedules().size());
    	tempResult.append(separator);

    	// omega ips referenceNumber
    	tempResult.append("ips"+ arInvoicePaymentSchedule.getIpsCode()+ "-"+arInvoicePaymentSchedule.getArInvoice().getInvCode());
    	tempResult.append(separator);

    	// amount
    	tempResult.append(String.format("%.2f", arInvoicePaymentSchedule.getIpsAmountDue() - arInvoicePaymentSchedule.getIpsAmountPaid() ));
    	tempResult.append(separator);   	
    	
    	// remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();    	
    	    	
    	encodedResult = encodedResult.replace("'", "\'");    	
    	
    	return encodedResult;
    	
    }
    
    
    
private String employeeDeductionCashBondRowEncode(LocalArCustomer arCustomer) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	// Start separator
    	tempResult.append(separator);
    	
    	// Deployed Branch
    	tempResult.append(arCustomer.getHrDeployedBranch().getDbReferenceID());
    	tempResult.append(separator);
    	
    	// deductionType
    	tempResult.append("Cash Bond");
    	tempResult.append(separator);
    	
    	// employee ID
    	tempResult.append(arCustomer.getHrEmployee().getEmpReferenceID());
    	tempResult.append(separator);
    	
    	// employee Number
    	tempResult.append(arCustomer.getHrEmployee().getEmpEmployeeNumber());
    	tempResult.append(separator);
    	
    	// employee current job position
    	tempResult.append(arCustomer.getHrEmployee().getEmpCurrentJobPosition());
    	tempResult.append(separator);

    	// amount
    	tempResult.append(String.format("%.2f", arCustomer.getCstHrCashBondAmount()));
    	tempResult.append(separator);   	
    	
    	// remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();    	
    	    	
    	encodedResult = encodedResult.replace("'", "\'");    	
    	
    	return encodedResult;
    	
    }
    


private String employeeDeductionInsMiscRowEncode(LocalArCustomer arCustomer) {
	
	char separator = EJBCommon.SEPARATOR;
	StringBuffer tempResult = new StringBuffer();
	String encodedResult = new String();

	// Start separator
	tempResult.append(separator);
	
	// Deployed Branch
	tempResult.append(arCustomer.getHrDeployedBranch().getDbReferenceID());
	tempResult.append(separator);
	
	// deductionType
	tempResult.append("Ins Misc");
	tempResult.append(separator);
	
	// employee ID
	tempResult.append(arCustomer.getHrEmployee().getEmpReferenceID());
	tempResult.append(separator);
	
	// employee Number
	tempResult.append(arCustomer.getHrEmployee().getEmpEmployeeNumber());
	tempResult.append(separator);
	
	// employee current job position
	tempResult.append(arCustomer.getHrEmployee().getEmpCurrentJobPosition());
	tempResult.append(separator);

	// amount
	tempResult.append(String.format("%.2f", arCustomer.getCstHrInsMiscAmount()));
	tempResult.append(separator);   	
	
	// remove unwanted chars from encodedResult;
	encodedResult = tempResult.toString();    	
	    	
	encodedResult = encodedResult.replace("'", "\'");    	
	
	return encodedResult;
	
}
    
    

    
    
    
    private String employeeDeductionUniformRowEncode(LocalArInvoiceLineItem arLocalArInvoiceLineItem) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	Collection arIps = arLocalArInvoiceLineItem.getArInvoice().getArInvoicePaymentSchedules();
    	
    	ArrayList arIpsList = new ArrayList(arIps);
	  	LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)arIpsList.get(0);
	  	
	  	
    	// Start separator
    	tempResult.append(separator);
    	
    	// Deployed Branch
    	tempResult.append(arInvoicePaymentSchedule.getArInvoice().getHrDeployedBranch().getDbReferenceID());
    	tempResult.append(separator);
    	
    	// deductionType
    	tempResult.append(arInvoicePaymentSchedule.getArInvoice().getArInvoiceBatch().getIbName());
    	tempResult.append(separator);
    	
    	// item Desc
    	tempResult.append(arLocalArInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
    	tempResult.append(separator);

    	// employee ID
    	tempResult.append(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getHrEmployee().getEmpReferenceID());
    	tempResult.append(separator);
    	
    	// employee Number
    	tempResult.append(arInvoicePaymentSchedule.getArInvoice().getArCustomer().getHrEmployee().getEmpEmployeeNumber());
    	tempResult.append(separator);
    	
    	// employee current job postion
    	tempResult.append(arInvoicePaymentSchedule.getArInvoice().getInvHrCurrentJobPosition());
    	tempResult.append(separator);
    	
    	
    	LocalDate date = new LocalDate(arInvoicePaymentSchedule.getIpsDueDate());
    	DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
    	String str = date.toString(fmt);
    			 
    	
    	// date
    	tempResult.append(str);
    	tempResult.append(separator);
    	
    	// Installment Number
    	tempResult.append(arInvoicePaymentSchedule.getIpsNumber() + "of"+arInvoicePaymentSchedule.getArInvoice().getAdPaymentTerm().getAdPaymentSchedules().size());
    	tempResult.append(separator);
    	
    	
    	// omega ips referenceNumber
    	tempResult.append("ips"+ arInvoicePaymentSchedule.getIpsCode()+ "-"+arInvoicePaymentSchedule.getArInvoice().getInvCode());
    	tempResult.append(separator);

    	
    	// amount
    	tempResult.append(String.format("%.2f", arLocalArInvoiceLineItem.getIliAmount() ));
    	tempResult.append(separator);   	
    	
    	// remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();    	
    	    	
    	encodedResult = encodedResult.replace("'", "\'");    	
    	
    	return encodedResult;
    	
    }
    
    public void ejbCreate() throws CreateException {

       Debug.print("HrHumanResourceSyncControllerBean ejbCreate");
      
    }
    
    public void ejbRemove() {};

    public void ejbActivate() {}
    public void ejbPassivate() {}

    public void setSessionContext(SessionContext ctx) {}

}