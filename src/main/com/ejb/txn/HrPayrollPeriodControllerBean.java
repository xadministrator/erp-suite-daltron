
/*
 * HrPayrollPeriodControllerBean.java
 *
 * Created on May 13, 2003, 9:13 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.hr.LocalHrPayrollPeriod;
import com.ejb.hr.LocalHrPayrollPeriodHome;
import com.util.AbstractSessionBean;
import com.util.HrPayrollPeriodDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="HrPayrollPeriodControllerEJB"
 *           display-name="Used for entering banks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/HrPayrollPeriodControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.HrPayrollPeriodController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.HrPayrollPeriodControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="hruser"
 *                        role-link="hruserlink"
 *
 * @ejb:permission role-name="hruser"
 * 
*/

public class HrPayrollPeriodControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getHrPpAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("HrPayrollPeriodControllerBean getHrPpAll");
        
        LocalHrPayrollPeriodHome hrPayrollPeriodHome = null;
        
        Collection hrPayrollPeriods = null;
        
        LocalHrPayrollPeriod hrPayrollPeriod = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	hrPayrollPeriodHome = (LocalHrPayrollPeriodHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrPayrollPeriodHome.JNDI_NAME, LocalHrPayrollPeriodHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	hrPayrollPeriods = hrPayrollPeriodHome.findPpAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (hrPayrollPeriods.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = hrPayrollPeriods.iterator();
               
        while (i.hasNext()) {
        	
        	hrPayrollPeriod = (LocalHrPayrollPeriod)i.next();
        
                
        	HrPayrollPeriodDetails details = new HrPayrollPeriodDetails();
	    		/*details.setPpCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());*/
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addAdPpEntry(com.util.HrPayrollPeriodDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("HrPayrollPeriodControllerBean addAdPpEntry");
        
        LocalHrPayrollPeriodHome hrPayrollPeriodHome = null;

        LocalHrPayrollPeriod hrPayrollPeriod = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	hrPayrollPeriodHome = (LocalHrPayrollPeriodHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrPayrollPeriodHome.JNDI_NAME, LocalHrPayrollPeriodHome.class);
                          
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
        	hrPayrollPeriod = hrPayrollPeriodHome.findByReferenceID(details.getPpReferenceID(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create new bank
        	
        	hrPayrollPeriod = hrPayrollPeriodHome.create(
        			details.getPpReferenceID(), details.getPpType(), details.getPpName(), details.getPpDateFrom(),  details.getPpDateTo(), 
        			details.getPpStatus(), AD_CMPNY); 	        
                	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateAdPpEntry(com.util.HrPayrollPeriodDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("HrPayrollPeriodControllerBean updateAdPpEntry");
        
        LocalHrPayrollPeriodHome hrPayrollPeriodHome = null;

        LocalHrPayrollPeriod hrPayrollPeriod = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	hrPayrollPeriodHome = (LocalHrPayrollPeriodHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrPayrollPeriodHome.JNDI_NAME, LocalHrPayrollPeriodHome.class);
                          
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalHrPayrollPeriod adExistingPayrollPeriod = hrPayrollPeriodHome.findByReferenceID(details.getPpReferenceID(), AD_CMPNY);
            
            if (!adExistingPayrollPeriod.getPpReferenceID().equals(details.getPpReferenceID())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	hrPayrollPeriod = hrPayrollPeriodHome.findByPrimaryKey(details.getPpCode());
        	
        	hrPayrollPeriod.setPpReferenceID(details.getPpReferenceID());	
        	hrPayrollPeriod.setPpName(details.getPpName());
        	hrPayrollPeriod.setPpDateFrom(details.getPpDateFrom());
        	hrPayrollPeriod.setPpDateTo(details.getPpDateTo());
        	hrPayrollPeriod.setPpStatus(details.getPpStatus());
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteAdPayrollPeriodEntry(Integer PP_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("HrPayrollPeriodControllerBean deleteAdPayrollPeriodEntry");
        
        LocalHrPayrollPeriodHome hrPayrollPeriodHome = null;

        
        LocalHrPayrollPeriod hrPayrollPeriod = null;           
               
        // Initialize EJB Home
        
        try {
            
        	hrPayrollPeriodHome = (LocalHrPayrollPeriodHome)EJBHomeFactory.
                lookUpLocalHome(LocalHrPayrollPeriodHome.JNDI_NAME, LocalHrPayrollPeriodHome.class);                      
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
	    	hrPayrollPeriod = hrPayrollPeriodHome.findByPrimaryKey(PP_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {

	    	hrPayrollPeriod.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("HrPayrollPeriodControllerBean ejbCreate");
      
    }
}
