package com.ejb.txn;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.cm.LocalCmAdjustment;
import com.ejb.cm.LocalCmAdjustmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.hr.LocalHrDeployedBranch;
import com.ejb.hr.LocalHrDeployedBranchHome;
import com.ejb.hr.LocalHrEmployee;
import com.ejb.hr.LocalHrEmployeeHome;
import com.ejb.hr.LocalHrPayrollPeriod;
import com.ejb.hr.LocalHrPayrollPeriodHome;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.google.gson.Gson;
import com.util.AbstractSessionBean;
import com.util.AdLookUpValueDetails;
import com.util.AdModBranchCustomerDetails;
import com.util.AdModBranchSupplierDetails;
import com.util.ApModSupplierDetails;
import com.util.ArModAppliedInvoiceDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModInvoicePaymentScheduleDetails;
import com.util.ArModReceiptDetails;
import com.util.ArReceiptDetails;
import com.util.CmAdjustmentDetails;
import com.util.CmModAdjustmentDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.HrDeployedBranchDetails;
import com.util.HrEmployeeDetails;
import com.util.HrModAppliedDeductions;
import com.util.HrModDeployedBranchDetails;
import com.util.HrModEmployeeDetails;
import com.util.HrModPayrollPeriodDetails;
import com.util.HrPayrollPeriodDetails;
import com.util.PmModSyncProjectDetails;

import sun.net.www.protocol.http.HttpURLConnection;

/**
 * @ejb:bean name="HrSynchronizerControllerEJB"
 *           display-name="Used for synchronizing data for HRIS"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/HrSynchronizerControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.HrSynchronizerController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.HrSynchronizerControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="hruser"
 *                        role-link="hruserlink"
 *
 * @ejb:permission role-name="hruser"
 *
*/

public class HrSynchronizerControllerBean extends AbstractSessionBean {

	private String ip_adress = "http://localhost";
	// http://dev.jigzen.com/hris/public/api/get-payroll-list
	// http://dev.jigzen.com/hris/public/api/get-clients
	// http://dev.jigzen.com/hris/public/api/get-all-employees
	// http://dev.jigzen.com/hrp/public/api/get-applied-employees-deductions/0

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setHrPayrollPeriod(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("HrSynchronizerControllerBean setHrPayrollPeriod");

        LocalHrPayrollPeriodHome hrPayrollPeriodHome = null;

        HrPayrollPeriodControllerHome homePP = null;
        HrPayrollPeriodController ejbPP = null;


        // Initialize EJB Home

        try {

        	hrPayrollPeriodHome = (LocalHrPayrollPeriodHome)EJBHomeFactory.
                	lookUpLocalHome(LocalHrPayrollPeriodHome.JNDI_NAME, LocalHrPayrollPeriodHome.class);

        	homePP = (HrPayrollPeriodControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/HrPayrollPeriodControllerEJB", HrPayrollPeriodControllerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	ejbPP = homePP.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {

        	URL url = new URL(ip_adress+"/hris/public/api/get-payroll-list");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);


    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/

        	Gson gson = new Gson();
        	HrModPayrollPeriodDetails[] payrollPeriodsArray = gson.fromJson(response, HrModPayrollPeriodDetails[].class);

        	for(int i = 0; i < payrollPeriodsArray.length; i++) {

        		HrModPayrollPeriodDetails payrollPeriod = (HrModPayrollPeriodDetails)payrollPeriodsArray[i];

        		HrPayrollPeriodDetails details = new HrPayrollPeriodDetails();


        		details.setPpCode(null);
        		try {
        			System.out.println("payrollPeriod.getHrPayrollPeriodID()="+payrollPeriod.getHrPayrollPeriodID());
        			LocalHrPayrollPeriod hrPayrollPeriod = hrPayrollPeriodHome.findByReferenceID(payrollPeriod.getHrPayrollPeriodID(), AD_CMPNY);

        			System.out.println("hrPayrollPeriod.getPpCode()="+hrPayrollPeriod.getPpCode());
        			details.setPpCode(hrPayrollPeriod.getPpCode());
        		}catch (Exception e) { }

        		details.setPpReferenceID(payrollPeriod.getHrPayrollPeriodID());
        		details.setPpType(payrollPeriod.getHrType());
				details.setPpName(payrollPeriod.getHrTitle());
				details.setPpDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse(payrollPeriod.getHrStartDate()));
				details.setPpDateTo(new SimpleDateFormat("yyyy-MM-dd").parse(payrollPeriod.getHrEndDate()));
				details.setPpStatus(payrollPeriod.getHrStatus());


            	try {

        		if(details.getPpCode() == null) {

        			ejbPP.addAdPpEntry(details, AD_CMPNY);
        		} else {
        			ejbPP.updateAdPpEntry(details, AD_CMPNY);
        		}


            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setHrDeployedBranch(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("HrSynchronizerControllerBean setHrDeployedBranch");

        LocalHrDeployedBranchHome hrDeployedBranchHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        HrDeployedBranchControllerHome homeDB = null;
        HrDeployedBranchController ejbDB = null;

        // Initialize EJB Home

        try {

        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
                	lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);

        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        	homeDB = (HrDeployedBranchControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/HrDeployedBranchControllerEJB", HrDeployedBranchControllerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	ejbDB = homeDB.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);


        	URL url = new URL(ip_adress+"/hris/public/api/get-clients");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);


    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/

    		Gson gson = new Gson();
        	HrModDeployedBranchDetails[] deployedBranchArray = gson.fromJson(response, HrModDeployedBranchDetails[].class);

        	for(int i = 0; i < deployedBranchArray.length; i++) {

        		HrModDeployedBranchDetails deployedBranch = (HrModDeployedBranchDetails)deployedBranchArray[i];


        		if(!adCompany.getCmpShortName().equalsIgnoreCase(deployedBranch.getHrManagingBranch())) continue;

        		HrDeployedBranchDetails details = new HrDeployedBranchDetails();


        		details.setDbCode(null);
        		try {
        			System.out.println("payrollPeriod.getHrDeployedBranchID()="+deployedBranch.getHrDeployedBranchID());
        			LocalHrDeployedBranch hrDeployedBranch = hrDeployedBranchHome.findByReferenceID(deployedBranch.getHrDeployedBranchID(), AD_CMPNY);

        			System.out.println("hrPayrollPeriod.getDbCode()="+hrDeployedBranch.getDbCode());
        			details.setDbCode(hrDeployedBranch.getDbCode());
        		}catch (Exception e) { }

        		details.setDbReferenceID(deployedBranch.getHrDeployedBranchID());
        		details.setDbClientCode(deployedBranch.getHrClientCode());
				details.setDbClientName(deployedBranch.getHrClientName());
				details.setDbTin(deployedBranch.getHrTin());
				details.setDbAddress(deployedBranch.getHrAddress());
				details.setDbManagingBranch(deployedBranch.getHrManagingBranch());


            	try {

        		if(details.getDbCode() == null) {

        			ejbDB.addHrDbEntry(details, AD_CMPNY);
        		} else {
        			ejbDB.updateHrDbEntry(details, AD_CMPNY);
        		}


            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }




    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setHrEmployees(Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("HrSynchronizerControllerBean setHrEmployees");


        LocalHrEmployeeHome hrEmployeeHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        HrEmployeeControllerHome homeEMP = null;
        HrEmployeeController ejbEMP = null;

        // Initialize EJB Home

        try {

        	hrEmployeeHome = (LocalHrEmployeeHome)EJBHomeFactory.
                	lookUpLocalHome(LocalHrEmployeeHome.JNDI_NAME, LocalHrEmployeeHome.class);

        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


        	homeEMP = (HrEmployeeControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/HrEmployeeControllerEJB", HrEmployeeControllerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	ejbEMP = homeEMP.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        	URL url = new URL(ip_adress+"/hris/public/api/get-all-employees");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);


    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/

    		Gson gson = new Gson();
    		HrModEmployeeDetails[] employeeArray = gson.fromJson(response, HrModEmployeeDetails[].class);

        	for(int i = 0; i < employeeArray.length; i++) {

        		HrModEmployeeDetails employee = (HrModEmployeeDetails)employeeArray[i];

        		HrModEmployeeDetails details = new HrModEmployeeDetails();

        		if(!adCompany.getCmpShortName().equalsIgnoreCase(employee.getHrManagingBranch())) continue;

        		details.setEmpCode(null);
        		try {
        			System.out.println("employee.getHrEmployeeID()="+employee.getHrEmployeeID());
        			LocalHrEmployee hrEmployee = hrEmployeeHome.findByReferenceID(employee.getHrEmployeeID(), AD_CMPNY);

        			System.out.println("hrPayrollPeriod.getDbCode()="+hrEmployee.getEmpCode());
        			details.setEmpCode(hrEmployee.getEmpCode());
        		}catch (Exception e) { }

        		details.setEmpReferenceID(employee.getHrEmployeeID());
        		details.setEmpBioNumber(employee.getHrBioNumber());
				details.setEmpEmployeeNumber(employee.getHrEmployeeNumber());
				details.setEmpFirstName(employee.getHrFirstName()== null ? "NOT SPECIFIED": employee.getHrFirstName().toUpperCase());
				details.setEmpMiddleName(employee.getHrMiddleName()== null ? "NOT SPECIFIED": employee.getHrMiddleName().toUpperCase());
				details.setEmpLastName(employee.getHrLastName()== null ? "NOT SPECIFIED": employee.getHrLastName().toUpperCase());
				details.setEmpManagingBranch(employee.getHrManagingBranch()== null ? "NOT SPECIFIED": employee.getHrManagingBranch().toUpperCase());
				details.setEmpCurrentJobPosition(employee.getHrCurrentJobPosition() == null ? "NOT SPECIFIED": employee.getHrCurrentJobPosition().toUpperCase());
				details.setHrDeployedBranch(employee.getHrDeployedBranch());

            	try {

        		if(details.getEmpCode() == null) {

        			ejbEMP.addHrEmpEntry(details, AD_CMPNY);
        		} else {
        			ejbEMP.updateHrEmpEntry(details, AD_CMPNY);
        		}


            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

































    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setCstSplEmployees(String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("HrSynchronizerControllerBean setCstSplEmployees");


        ApSupplierEntryControllerHome homeSPL = null;
        ApSupplierEntryController ejbSPL = null;

        ArCustomerEntryControllerHome homeCST = null;
        ArCustomerEntryController ejbCST = null;

        LocalArCustomerHome arCustomerHome = null;
        LocalApSupplierHome apSupplierHome = null;
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalApSupplierClassHome apSupplierClassHome = null;

        LocalHrEmployeeHome hrEmployeeHome = null;
        LocalHrDeployedBranchHome hrDeployedBranchHome = null;

        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        // Initialize EJB Home

        try {

        	homeSPL = (ApSupplierEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ApSupplierEntryControllerEJB", ApSupplierEntryControllerHome.class);

        	homeCST = (ArCustomerEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ArCustomerEntryControllerEJB", ArCustomerEntryControllerHome.class);

        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                	lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

        	arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);

        	apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                	lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);

        	hrEmployeeHome = (LocalHrEmployeeHome)EJBHomeFactory.
                	lookUpLocalHome(LocalHrEmployeeHome.JNDI_NAME, LocalHrEmployeeHome.class);

        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
                	lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);




        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	ejbSPL = homeSPL.create();

        	ejbCST = homeCST.create();


		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {

        	LocalArCustomerClass arCustomerClass = arCustomerClassHome.findByCcName("Officers and Employees", AD_CMPNY);
        	LocalApSupplierClass apSupplierClass = apSupplierClassHome.findByScName("Officers and Employees", AD_CMPNY);


        	String RECEIVABLE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaReceivableAccount(), AD_CMPNY).getCoaAccountNumber();
        	String REVENUE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaRevenueAccount(), AD_CMPNY).getCoaAccountNumber();
        	String UNEARNED_INT_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaUnEarnedInterestAccount(), AD_CMPNY).getCoaAccountNumber();
        	String EARNED_INT_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaEarnedInterestAccount(), AD_CMPNY).getCoaAccountNumber();

        	String PAYABLE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(apSupplierClass.getScGlCoaPayableAccount(), AD_CMPNY).getCoaAccountNumber();
        	String EXPENSE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(apSupplierClass.getScGlCoaExpenseAccount(), AD_CMPNY).getCoaAccountNumber();


        	Collection hrEmployees = hrEmployeeHome.findEmpAllEmpNumberAndBioNo(AD_CMPNY);

        	Iterator i = hrEmployees.iterator();

        	while(i.hasNext()) {

        		LocalHrEmployee hrEmployee = (LocalHrEmployee)i.next();
        		System.out.println("hrEmployee.getHrDeployedBranch().getDbClientCode()="+hrEmployee.getHrDeployedBranch().getDbClientCode());
        		System.out.println("hrEmployee.getEmpBioNumber()="+hrEmployee.getEmpBioNumber());
        		System.out.println("hrEmployee.getHrDeployedBranch().getDbCode()="+hrEmployee.getHrDeployedBranch().getDbCode());
        		String HR_BIO_DB = hrEmployee.getEmpEmployeeNumber()+"-"+ hrEmployee.getEmpBioNumber();
        		Integer HR_EMP_ID = hrEmployee.getEmpReferenceID();
        		String HR_EMP_NMBR = hrEmployee.getEmpEmployeeNumber();
        		Integer HR_DPLYD_BRNCH = hrEmployee.getHrDeployedBranch().getDbReferenceID();
        		String NAME = hrEmployee.getEmpFirstName() + " " +
        					(hrEmployee.getEmpMiddleName().equals("NOT SPECIFIED") ? "" : hrEmployee.getEmpMiddleName() ) + " "+
        					(hrEmployee.getEmpLastName().equals("NOT SPECIFIED") ? "" : hrEmployee.getEmpLastName() ) ;



        		ApModSupplierDetails splDetails = new ApModSupplierDetails();

        		splDetails.setSplCode(null);

        		try {
        			System.out.println("HR_BIO_DB="+HR_BIO_DB);
        			LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(HR_BIO_DB, AD_CMPNY);
        			splDetails.setSplCode(apSupplier.getSplCode());

        		}catch (Exception e) { }

        		splDetails.setSplEnable((byte)1);
        		splDetails.setSplSupplierCode(HR_BIO_DB);
        		splDetails.setSplName(NAME);

				ArrayList bSplList = new ArrayList();

				AdModBranchSupplierDetails splmDetails = new AdModBranchSupplierDetails();
				splmDetails.setBSplBranchCode(AD_BRNCH);
				splmDetails.setBSplPayableAccountNumber(PAYABLE_ACCNT);
				splmDetails.setBSplExpenseAccountNumber(EXPENSE_ACCNT);

				bSplList.add(splmDetails);



        		ArModCustomerDetails cstDetails = new ArModCustomerDetails();

        		cstDetails.setCstCode(null);

        		try {
        			LocalArCustomer arCustomer = arCustomerHome.findByCstHrEmpReferenceID(HR_EMP_ID, AD_CMPNY);
        			cstDetails.setCstCode(arCustomer.getCstCode());

        		}catch (Exception e) { }


        		cstDetails.setCstEnable((byte)1);
        		cstDetails.setCstAutoComputeInterest((byte)1);
        		cstDetails.setCstCustomerCode(HR_BIO_DB);
        		cstDetails.setCstName(NAME);
        		cstDetails.setCstEmployeeID(HR_EMP_NMBR);
        		cstDetails.setCstSquareMeter(0d);
        		cstDetails.setCstNumbersParking(0d);
        		cstDetails.setCstMonthlyInterestRate(1d);
        		cstDetails.setCstRealPropertyTaxRate(0d);
        		cstDetails.setCstAssociationDuesRate(0d);

    			cstDetails.setCstCreatedBy(USR_NM);
    			cstDetails.setCstDateCreated(new java.util.Date());
    			cstDetails.setCstLastModifiedBy(USR_NM);
    			cstDetails.setCstDateLastModified(new java.util.Date());


        		ArrayList bCstList = new ArrayList();

            	AdModBranchCustomerDetails cstmDetails = new AdModBranchCustomerDetails();

            	cstmDetails.setBcstBranchCode("HQ");
            	cstmDetails.setBcstBranchName("Head Office");
            	cstmDetails.setBcstReceivableAccountNumber(RECEIVABLE_ACCNT);
            	cstmDetails.setBcstRevenueAccountNumber(REVENUE_ACCNT);
            	cstmDetails.setBcstUnEarnedInterestAccountNumber(UNEARNED_INT_ACCNT);
            	cstmDetails.setBcstEarnedInterestAccountNumber(EARNED_INT_ACCNT);
            	cstmDetails.setBcstUnEarnedPenaltyAccountNumber(null);
            	cstmDetails.setBcstEarnedPenaltyAccountNumber(null);

            	bCstList.add(cstmDetails);

            	cstDetails.setBcstList(bCstList);


            	try {

        		ejbSPL.saveApSplEntry(splDetails, null, "IMMEDIATE", apSupplierClass.getScName(),
    					PAYABLE_ACCNT, EXPENSE_ACCNT,
        				"BDO", "Super", bSplList, null, AD_CMPNY);

        		LocalHrDeployedBranch hrDeployedBranch = hrDeployedBranchHome.findByReferenceID(HR_DPLYD_BRNCH, AD_CMPNY);

        		ejbCST.saveArCstEntry(cstDetails, null, "IMMEDIATE", HR_BIO_DB, arCustomerClass.getCcName(),
        				RECEIVABLE_ACCNT, REVENUE_ACCNT, UNEARNED_INT_ACCNT, EARNED_INT_ACCNT, null, null,
        				"SALARY & WAGES", (int)1, null, null, null,
        				hrDeployedBranch.getDbClientName(), HR_BIO_DB,
        				null,
        				false, AD_BRNCH, AD_CMPNY);

            	}catch (Exception e) {
            		continue;
				}


        	}


	        return 0;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }






    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setCstSplClients(String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("HrSynchronizerControllerBean setCstSplClients");


        ApSupplierEntryControllerHome homeSPL = null;
        ApSupplierEntryController ejbSPL = null;

        ArCustomerEntryControllerHome homeCST = null;
        ArCustomerEntryController ejbCST = null;

        LocalArCustomerHome arCustomerHome = null;
        LocalApSupplierHome apSupplierHome = null;
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalApSupplierClassHome apSupplierClassHome = null;

        LocalHrDeployedBranchHome hrDeployedBranchHome = null;

        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        // Initialize EJB Home

        try {

        	homeSPL = (ApSupplierEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ApSupplierEntryControllerEJB", ApSupplierEntryControllerHome.class);

        	homeCST = (ArCustomerEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ArCustomerEntryControllerEJB", ArCustomerEntryControllerHome.class);

        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                	lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

        	arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);

        	apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                	lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);


        	hrDeployedBranchHome = (LocalHrDeployedBranchHome)EJBHomeFactory.
                	lookUpLocalHome(LocalHrDeployedBranchHome.JNDI_NAME, LocalHrDeployedBranchHome.class);




        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	ejbSPL = homeSPL.create();

        	ejbCST = homeCST.create();


		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {

        	LocalArCustomerClass arCustomerClass = arCustomerClassHome.findByCcName("NCR", AD_CMPNY);
        	LocalApSupplierClass apSupplierClass = apSupplierClassHome.findByScName("NCR", AD_CMPNY);


        	String RECEIVABLE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaReceivableAccount(), AD_CMPNY).getCoaAccountNumber();
        	String REVENUE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaRevenueAccount(), AD_CMPNY).getCoaAccountNumber();
        	String UNEARNED_INT_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaUnEarnedInterestAccount(), AD_CMPNY).getCoaAccountNumber();
        	String EARNED_INT_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaEarnedInterestAccount(), AD_CMPNY).getCoaAccountNumber();

        	String PAYABLE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(apSupplierClass.getScGlCoaPayableAccount(), AD_CMPNY).getCoaAccountNumber();
        	String EXPENSE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(apSupplierClass.getScGlCoaExpenseAccount(), AD_CMPNY).getCoaAccountNumber();


        	Collection hrClients = hrDeployedBranchHome.findDbAll(AD_CMPNY);

        	Iterator i = hrClients.iterator();

        	while(i.hasNext()) {

        		LocalHrDeployedBranch hrDeployedBranch = (LocalHrDeployedBranch)i.next();

        		String HR_DB_CLNT_CODE = hrDeployedBranch.getDbClientCode();
        		Integer HR_DPLYD_BRNCH = hrDeployedBranch.getDbReferenceID();
        		String NAME = hrDeployedBranch.getDbClientName();
        		String TIN = hrDeployedBranch.getDbTin();
        		String ADDRSS = hrDeployedBranch.getDbAddress();

        		System.out.println("HR_DB_CLNT_CODE="+HR_DB_CLNT_CODE);;



        		ApModSupplierDetails splDetails = new ApModSupplierDetails();

        		splDetails.setSplCode(null);

        		try {

        			LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(HR_DB_CLNT_CODE, AD_CMPNY);
        			splDetails.setSplCode(apSupplier.getSplCode());

        		}catch (Exception e) { }

        		splDetails.setSplEnable((byte)1);
        		splDetails.setSplSupplierCode(HR_DB_CLNT_CODE);
        		splDetails.setSplName(NAME);
        		splDetails.setSplTin(TIN);
        		splDetails.setSplAddress(ADDRSS);

				ArrayList bSplList = new ArrayList();

				AdModBranchSupplierDetails splmDetails = new AdModBranchSupplierDetails();
				splmDetails.setBSplBranchCode(AD_BRNCH);
				splmDetails.setBSplPayableAccountNumber(PAYABLE_ACCNT);
				splmDetails.setBSplExpenseAccountNumber(EXPENSE_ACCNT);

				bSplList.add(splmDetails);



        		ArModCustomerDetails cstDetails = new ArModCustomerDetails();

        		cstDetails.setCstCode(null);

        		try {
        			LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(HR_DB_CLNT_CODE, AD_CMPNY);
        			cstDetails.setCstCode(arCustomer.getCstCode());

        		}catch (Exception e) { }


        		cstDetails.setCstEnable((byte)1);
        		cstDetails.setCstAutoComputeInterest((byte)1);
        		cstDetails.setCstCustomerCode(HR_DB_CLNT_CODE);
        		cstDetails.setCstName(NAME);
        		cstDetails.setCstTin(TIN);
        		cstDetails.setCstAddress(ADDRSS);
        		cstDetails.setCstSquareMeter(0d);
        		cstDetails.setCstNumbersParking(0d);
        		cstDetails.setCstMonthlyInterestRate(1d);
        		cstDetails.setCstRealPropertyTaxRate(0d);
        		cstDetails.setCstAssociationDuesRate(0d);

    			cstDetails.setCstCreatedBy(USR_NM);
    			cstDetails.setCstDateCreated(new java.util.Date());
    			cstDetails.setCstLastModifiedBy(USR_NM);
    			cstDetails.setCstDateLastModified(new java.util.Date());


        		ArrayList bCstList = new ArrayList();

            	AdModBranchCustomerDetails cstmDetails = new AdModBranchCustomerDetails();

            	cstmDetails.setBcstBranchCode("HQ");
            	cstmDetails.setBcstBranchName("Head Office");
            	cstmDetails.setBcstReceivableAccountNumber(RECEIVABLE_ACCNT);
            	cstmDetails.setBcstRevenueAccountNumber(REVENUE_ACCNT);
            	cstmDetails.setBcstUnEarnedInterestAccountNumber(UNEARNED_INT_ACCNT);
            	cstmDetails.setBcstEarnedInterestAccountNumber(EARNED_INT_ACCNT);
            	cstmDetails.setBcstUnEarnedPenaltyAccountNumber(null);
            	cstmDetails.setBcstEarnedPenaltyAccountNumber(null);

            	bCstList.add(cstmDetails);

            	cstDetails.setBcstList(bCstList);


            	try {

        		ejbSPL.saveApSplEntry(splDetails, null, "IMMEDIATE", apSupplierClass.getScName(),
    					PAYABLE_ACCNT, EXPENSE_ACCNT,
        				"BDO", "Super", bSplList, null, AD_CMPNY);


        		ejbCST.saveArCstEntry(cstDetails, null, "IMMEDIATE", HR_DB_CLNT_CODE, arCustomerClass.getCcName(),
        				RECEIVABLE_ACCNT, REVENUE_ACCNT, UNEARNED_INT_ACCNT, EARNED_INT_ACCNT, null, null,
        				"SALARY & WAGES", (int)1, null, null, null, hrDeployedBranch.getDbClientName(), null, null,
        				false, AD_BRNCH, AD_CMPNY);

            	}catch (Exception e) {
            		continue;
				}


        	}


	        return 0;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }



    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setHrAppliedDeductions(String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("HrSynchronizerControllerBean setHrAppliedDeductions");

        LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalHrPayrollPeriodHome hrPayrollPeriodHome = null;

        ArReceiptEntryControllerHome homeRCT = null;
        ArReceiptEntryController ejbRCT = null;

        // Initialize EJB Home

        try {

        	arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	hrPayrollPeriodHome = (LocalHrPayrollPeriodHome)EJBHomeFactory.
                	lookUpLocalHome(LocalHrPayrollPeriodHome.JNDI_NAME, LocalHrPayrollPeriodHome.class);


        	homeRCT = (ArReceiptEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ArReceiptEntryControllerEJB", ArReceiptEntryControllerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	ejbRCT = homeRCT.create();


		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {

        	URL url = new URL(ip_adress+"/hrp/public/api/get-applied-employees-deductions/0");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);


    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/

        	Gson gson = new Gson();
        	HrModAppliedDeductions[] appliedDeductionArray = gson.fromJson(response, HrModAppliedDeductions[].class);

        	for(int i = 0; i < appliedDeductionArray.length; i++) {

        		Integer HR_EMP_ID = appliedDeductionArray[i].getHrEmployeeID();
        		Integer HR_PP_RFRNC_ID = appliedDeductionArray[i].getHrPayrollPeriodID();
        		String HR_MNGNG_BRNCH = appliedDeductionArray[i].getHrManagingBranch();
        		String HR_DEDUCTION_TYP = appliedDeductionArray[i].getHrDeductionType().toUpperCase();
        		String HR_OMEGA_IPS = appliedDeductionArray[i].getHrOmegaIPS();
        		Double HR_AMNT = Double.parseDouble(appliedDeductionArray[i].getHrAmount());


        		if(HR_DEDUCTION_TYP.contains("CASHBOND")
        				|| HR_DEDUCTION_TYP.contains("INSURANCE")
                			|| HR_OMEGA_IPS == null) continue;


        		String[] ipsArray = HR_OMEGA_IPS.replace("ips", "").split("-");

        		Integer IPS_CODE = Integer.parseInt(ipsArray[0]);

        		ArrayList ipsList = new ArrayList();
        		ArrayList list = new ArrayList();
        		double totalAmount = 0d;

                try {
                	ipsList = new ArrayList();

                	list = ejbRCT.getArIpsByIpsCode(IPS_CODE, false, AD_CMPNY);
                	Iterator x = list.iterator();


    				while (x.hasNext()) {

    	                ArModInvoicePaymentScheduleDetails mdetails = (ArModInvoicePaymentScheduleDetails)x.next();

    	                ArModAppliedInvoiceDetails mAiDetails = new ArModAppliedInvoiceDetails();

    	                mAiDetails.setAiIpsCode(mdetails.getIpsCode());
    	                mAiDetails.setAiApplyAmount( mdetails.getIpsAiApplyAmount());
    	                mAiDetails.setAiPenaltyApplyAmount(0d);
    	                mAiDetails.setAiCreditableWTax(0d);
    	                mAiDetails.setAiDiscountAmount(0d);
    	                mAiDetails.setAiAllocatedPaymentAmount(0d);
    	                mAiDetails.setAiAppliedDeposit(0d);
    	                mAiDetails.setAiCreditBalancePaid(0d);
    					mAiDetails.setAiRebate(0d );
    					mAiDetails.setAiApplyRebate((byte)0);

    					totalAmount += mdetails.getIpsAiApplyAmount();
    	                ipsList.add(mAiDetails);


    				}

                }catch (Exception e) {
					continue;
				}



				LocalArCustomer arCustomer = arCustomerHome.findByCstHrEmpReferenceID(HR_EMP_ID, AD_CMPNY);
				LocalHrPayrollPeriod hrPayrollPeriod = hrPayrollPeriodHome.findByReferenceID(HR_PP_RFRNC_ID, AD_CMPNY);


				ArReceiptDetails details = new ArReceiptDetails();

                details.setRctCode(null);
                details.setRctDate(hrPayrollPeriod.getPpDateTo());
                details.setRctNumber(null);
                details.setRctReferenceNumber(HR_OMEGA_IPS);
                details.setRctVoid(EJBCommon.FALSE);
                details.setRctDescription("SYNC DATA FROM HRP");
                details.setRctConversionDate(EJBCommon.convertStringToSQLDate(null));
                details.setRctConversionRate(1);
                details.setRctSubjectToCommission(EJBCommon.FALSE);
                details.setRctPaymentMethod("CASH");
                details.setRctCustomerDeposit(EJBCommon.FALSE);
                details.setRctCustomerName(arCustomer.getCstName());
                details.setRctCustomerAddress(null);
                details.setRctInvtrInvestorFund(EJBCommon.FALSE);
                details.setRctInvtrNextRunDate(null);
                details.setRctEnableAdvancePayment((byte)0);
                details.setRctExcessAmount(0d);
                details.setRctAmount(totalAmount);

                details.setRctCreatedBy(USR_NM);
                details.setRctDateCreated(new java.util.Date());
                details.setRctLastModifiedBy(USR_NM);
                details.setRctDateLastModified(new java.util.Date());


            	try {

            		ejbRCT.saveArRctEntry(details, true, "SALARY & WAGES", "PHP", arCustomer.getCstCustomerCode(), "BATCH_NAME", ipsList, true, false, hrPayrollPeriod.getPpName(), AD_BRNCH, AD_CMPNY);

            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }





    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setHrAppliedCashBondInsDeductions(String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("HrSynchronizerControllerBean setHrAppliedCashBondInsDeductions");

        LocalHrPayrollPeriodHome hrPayrollPeriodHome = null;
        LocalCmAdjustmentHome cmAdjustmentHome = null;
        LocalArCustomerHome arCustomerHome = null;


        CmAdjustmentEntryControllerHome homeAE = null;
        CmAdjustmentEntryController ejbAE = null;

        // Initialize EJB Home

        try {

        	hrPayrollPeriodHome = (LocalHrPayrollPeriodHome)EJBHomeFactory.
                	lookUpLocalHome(LocalHrPayrollPeriodHome.JNDI_NAME, LocalHrPayrollPeriodHome.class);

        	cmAdjustmentHome = (LocalCmAdjustmentHome)EJBHomeFactory.
                	lookUpLocalHome(LocalCmAdjustmentHome.JNDI_NAME, LocalCmAdjustmentHome.class);

        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);


        	homeAE = (CmAdjustmentEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/CmAdjustmentEntryControllerEJB", CmAdjustmentEntryControllerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	ejbAE = homeAE.create();


		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {

        	URL url = new URL(ip_adress+"/hrp/public/api/get-applied-employees-deductions/0");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);


    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/

        	Gson gson = new Gson();
        	HrModAppliedDeductions[] appliedDeductionArray = gson.fromJson(response, HrModAppliedDeductions[].class);

        	for(int i = 0; i < appliedDeductionArray.length; i++) {


        		Integer HR_EMP_ID = appliedDeductionArray[i].getHrEmployeeID();
        		Integer HR_PAY_PERIOD = appliedDeductionArray[i].getHrPayrollPeriodID();
        		String HR_MNGNG_BRNCH = appliedDeductionArray[i].getHrManagingBranch();
        		String HR_DEDUCTION_TYP = appliedDeductionArray[i].getHrDeductionType().toUpperCase();
        		Double HR_AMNT = Double.parseDouble(appliedDeductionArray[i].getHrAmount());


        		if(! (HR_DEDUCTION_TYP.equals("CASHBOND") || HR_DEDUCTION_TYP.equals("INSURANCE"))) continue;
        		System.out.println("HR_DEDUCTION_TYP="+HR_DEDUCTION_TYP);


				LocalArCustomer arCustomer = arCustomerHome.findByCstHrEmpReferenceID(HR_EMP_ID, AD_CMPNY);

				LocalHrPayrollPeriod hrPayrollPeriod = null;


        		try {
        			System.out.println("HR_PAY_PERIOD="+HR_PAY_PERIOD);
        			hrPayrollPeriod = hrPayrollPeriodHome.findByReferenceID(HR_PAY_PERIOD, AD_CMPNY);
        		}catch (Exception e) { }

        		String BANK_ACCOUNT = HR_DEDUCTION_TYP.equals("CASHBOND") ? "CASHBOND" : "INS/MISC";

        		try {

        			LocalCmAdjustment cmAdjustment = cmAdjustmentHome.findAdjByAdjDateAndPpCodeAndCstCodeAndBaName(hrPayrollPeriod.getPpDateTo(), HR_PAY_PERIOD,arCustomer.getCstCode(),BANK_ACCOUNT,  AD_CMPNY);
        			System.out.println("CM EXISTING"+cmAdjustment.getAdjDocumentNumber());
        			continue;

        		}catch (Exception e) { }

        			System.out.println("hrPayrollPeriod.getPpCode()="+hrPayrollPeriod.getPpCode());

        		CmAdjustmentDetails details = new CmAdjustmentDetails();

        		details.setAdjCode(null);
	            details.setAdjType("ADVANCE");
	            details.setAdjDate(hrPayrollPeriod.getPpDateTo());
	            details.setAdjDocumentNumber(null);
	            details.setAdjReferenceNumber(null);
	            details.setAdjCheckNumber("");
	            details.setAdjAmount(HR_AMNT);
	            details.setAdjConversionDate(null);
	            details.setAdjConversionRate(1d);
	            details.setAdjMemo(HR_DEDUCTION_TYP);
	            details.setAdjVoid((byte)0);
	            details.setAdjRefund((byte)0);

                details.setAdjCreatedBy(USR_NM);
                details.setAdjDateCreated(new java.util.Date());
                details.setAdjLastModifiedBy(USR_NM);
                details.setAdjDateLastModified(new java.util.Date());


            	try {

            		ejbAE.saveCmAdjEntry(details, BANK_ACCOUNT, arCustomer.getCstCustomerCode(), "",
            				"PHP", true, hrPayrollPeriod.getPpCode() , AD_BRNCH, AD_CMPNY);

            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }



    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("HrSynchronizerControllerBean ejbCreate");

    }

}

