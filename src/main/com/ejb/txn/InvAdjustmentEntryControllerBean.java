package com.ejb.txn;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.ArrayList;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInvTagMissingException;
import com.ejb.exception.GlobalInvTagExistingException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.InvTagSerialNumberNotFoundException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModApprovalQueueDetails;
import com.util.ApModSupplierDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModAdjustmentDetails;
import com.util.InvModAdjustmentLineDetails;
import com.util.InvModItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;
import com.util.InvRepAdjustmentRegisterDetails;

/**
 * @ejb:bean name="InvAdjustmentEntryControllerEJB"
 *           display-name="used for adjusting inventory quantity-on-hand"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvAdjustmentEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvAdjustmentEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvAdjustmentEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
*/

public class InvAdjustmentEntryControllerBean extends AbstractSessionBean {



	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getApSplAll(Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("InvAdjustmentEntryControllerBean getApSplAll");

		LocalApSupplierHome apSupplierHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection apSuppliers = apSupplierHome.findEnabledSplAll(AD_BRNCH, AD_CMPNY);

			Iterator i = apSuppliers.iterator();

			while (i.hasNext()) {

				LocalApSupplier apSupplier = (LocalApSupplier)i.next();

				list.add(apSupplier.getSplSupplierCode());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ApModSupplierDetails getApSplBySplSupplierCode(String SPL_SPPLR_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("InvAdjustmentEntryControllerBean getApSplBySplSupplierCode");

		LocalApSupplierHome apSupplierHome = null;

		// Initialize EJB Home

		try {

			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalApSupplier apSupplier = null;


			try {

				apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			ApModSupplierDetails mdetails = new ApModSupplierDetails();

			mdetails.setSplPytName(apSupplier.getAdPaymentTerm() != null ?
					apSupplier.getAdPaymentTerm().getPytName() : null);
			mdetails.setSplScWtcName(apSupplier.getApSupplierClass().getApWithholdingTaxCode() != null ?
					apSupplier.getApSupplierClass().getApWithholdingTaxCode().getWtcName() : null);
			mdetails.setSplName(apSupplier.getSplName());

        	if (apSupplier.getApSupplierClass().getApTaxCode() != null ){

        		mdetails.setSplScTcName( apSupplier.getApSupplierClass().getApTaxCode().getTcName());
        		mdetails.setSplScTcType( apSupplier.getApSupplierClass().getApTaxCode().getTcType());
        		mdetails.setSplScTcRate( apSupplier.getApSupplierClass().getApTaxCode().getTcRate());

        	}

			return mdetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public byte getAdPrfApUseSupplierPulldown(Integer AD_CMPNY) {

		Debug.print("InvAdjustmentEntryControllerBean getAdPrfApUseSupplierPulldown");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return  adPreference.getPrfApUseSupplierPulldown();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {

        System.out.println("Debug line 1");
    	Debug.print("InvAdjustmentEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            invLocations = invLocationHome.findLocAll(AD_CMPNY);

	        if (invLocations.isEmpty()) {

	        	return null;

	        }

	        Iterator i = invLocations.iterator();

	        while (i.hasNext()) {

	        	LocalInvLocation invLocation = (LocalInvLocation)i.next();
	        	String details = invLocation.getLocName();

	        	list.add(details);

	        }

	        return list;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }



    /**
     * @ejb:interface-method view-type="remote"
      **/
     public void executeInvAdjSubmit(Integer ADJ_CODE, Integer AD_BRNCH, Integer AD_CMPNY) throws
 		GlobalRecordAlreadyDeletedException,
 		GlobalBranchAccountNumberInvalidException,
 		GlobalTransactionAlreadyApprovedException,
 		GlobalTransactionAlreadyPendingException,
 		GlobalTransactionAlreadyPostedException,
 		GlobalNoApprovalRequesterFoundException,
 		GlobalNoApprovalApproverFoundException,
 		GlobalInvItemLocationNotFoundException,
 		GlJREffectiveDateNoPeriodExistException,
 		GlJREffectiveDatePeriodClosedException,
 		GlobalJournalNotBalanceException,
 		GlobalDocumentNumberNotUniqueException,
 		GlobalInventoryDateException,
 		GlobalInvTagMissingException,
 		InvTagSerialNumberNotFoundException,
 		GlobalInvTagExistingException,
 		GlobalAccountNumberInvalidException,
 		AdPRFCoaGlVarianceAccountNotFoundException,
 		GlobalExpiryDateNotFoundException,
 		GlobalMiscInfoIsRequiredException,
 		GlobalRecordInvalidException{

     	Debug.print("InvAdjustmentEntryControllerBean saveInvAdjEntry");

     	LocalInvAdjustmentHome invAdjustmentHome = null;
     	LocalGlChartOfAccountHome glChartOfAccountHome = null;
     	LocalAdApprovalHome adApprovalHome = null;
     	LocalAdAmountLimitHome adAmountLimitHome = null;
     	LocalAdApprovalUserHome adApprovalUserHome = null;
     	LocalAdApprovalQueueHome adApprovalQueueHome = null;
     	LocalInvItemLocationHome invItemLocationHome = null;
     	LocalInvCostingHome invCostingHome = null;
     	LocalAdPreferenceHome adPreferenceHome = null;
     	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
     	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
     	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
     	LocalInvItemHome invItemHome = null;
     	LocalInvTagHome invTagHome = null;
         LocalAdUserHome adUserHome = null;
         LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
         LocalInvLocationHome invLocationHome = null;
         LocalApSupplierHome apSupplierHome = null;

         // Initialize EJB Home

         try {

         	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                 lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
         	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
             	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
         	adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
         		lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
         	adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
         		lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
         	adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
         		lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
         	adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
     			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
         	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
 				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
         	invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
 				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
 			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
 			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
     			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
 			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
 				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
 			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
 				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
 			invItemHome = (LocalInvItemHome)EJBHomeFactory.
             	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
 			invTagHome = (LocalInvTagHome)EJBHomeFactory.
     				lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

             adUserHome = (LocalAdUserHome)EJBHomeFactory.
     				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
             invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
     				lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
             invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
     				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
             apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
     				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);



         } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());
         }

         try {

         	LocalInvAdjustment invAdjustment = null;
         	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);


         	// validate if Adjustment is already deleted

         	try {

         		invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);


         	} catch (FinderException ex) {

         		throw new GlobalRecordAlreadyDeletedException();

         	}

         	// validate if adjustment is already posted, void, approved or pending


 	        	if (invAdjustment.getAdjApprovalStatus() != null) {

 	        		if (invAdjustment.getAdjApprovalStatus().equals("APPROVED") ||
 	        			invAdjustment.getAdjApprovalStatus().equals("N/A")) {

 	        		    throw new GlobalTransactionAlreadyApprovedException();


 	        		} else if (invAdjustment.getAdjApprovalStatus().equals("PENDING")) {

 	        			throw new GlobalTransactionAlreadyPendingException();

 	        		}

 	        	}

         		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

         			throw new GlobalTransactionAlreadyPostedException();

         		}

         		Collection invAdjustments = invAdjustment.getInvAdjustmentLines();
         		Double ABS_AMOUNT =0d;
         		Iterator x = invAdjustments.iterator();

         		while(x.hasNext()){

         			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)x.next();
         			ABS_AMOUNT += invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost();
         		}

         		//Insufficient Stocks
            	if (adPreference.getPrfArCheckInsufficientStock() == EJBCommon.TRUE) {
            		System.out.println("CHECK A");
    	        	boolean hasInsufficientItems = false;
    	        	String insufficientItems = "";

    	        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

    	        	Iterator i = invAdjustmentLines.iterator();

    	        	HashMap cstMap = new HashMap();

    	        	while (i.hasNext()) {

    	        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    	        		if(invAdjustmentLine.getAlAdjustQuantity() < 0 ) {

    	    				LocalInvCosting invCosting = null;
    	    				double CURR_QTY = 0;
    	    				boolean isIlFound = false;

    	    				double ILI_QTY = this.convertByUomAndQuantity( invAdjustmentLine.getInvUnitOfMeasure(),
    	    						invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

    	    				if(cstMap.containsKey(invAdjustmentLine.getInvItemLocation().getIlCode().toString())){

    		  	   	    		isIlFound =  true;
    		  	   	    		CURR_QTY = ((Double)cstMap.get(invAdjustmentLine.getInvItemLocation().getIlCode().toString())).doubleValue();

    	    				}else{

    	    					try {

    	    						invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    	    								invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    	    								invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    	    						CURR_QTY = invCosting.getCstRemainingQuantity();

    	    					} catch (FinderException ex) {

    	    					}
    	    				}

    	    				if(invCosting != null){

    		  	   	    		CURR_QTY = this.convertByUomAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(),
    	        						invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invCosting.getCstRemainingQuantity()), AD_CMPNY);

    		  	   	    	}

    	    				double LOWEST_QTY = this.convertByUomAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(),
    	    						invAdjustmentLine.getInvItemLocation().getInvItem(),1, AD_CMPNY);

    	    				if ((invCosting == null && isIlFound==false)|| CURR_QTY == 0 || CURR_QTY - ILI_QTY <= -LOWEST_QTY) {

    	    					hasInsufficientItems = true;
    	    					insufficientItems += invAdjustmentLine.getInvItemLocation().getInvItem().getIiName() + ", ";
    	    				}

    	    				CURR_QTY -= ILI_QTY;

    		  	   	    	if(isIlFound){
    		  	   	    		cstMap.remove(invAdjustmentLine.getInvItemLocation().getIlCode().toString());
    		  	   	    	}

    		  	   	    	cstMap.put(invAdjustmentLine.getInvItemLocation().getIlCode().toString(), new Double(CURR_QTY));

    	        		}

    	        	}
    	        	if(hasInsufficientItems) {

    	        		throw new GlobalRecordInvalidException(insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
    	        	}
    	        }

  	  	    // generate approval status

  	  	    String INV_APPRVL_STATUS = null;


 		 		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

 		 		// check if ar invoice approval is enabled

 		 		if (adApproval.getAprEnableInvAdjustment() == EJBCommon.FALSE) {

 		 			INV_APPRVL_STATUS = "N/A";

 		 		} else {
 		 		
 		 			INV_APPRVL_STATUS = "N/A";
 		 		}


 		 	invAdjustment.setAdjApprovalStatus(INV_APPRVL_STATUS);

 		 	// set adjustment approval status

         	if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

         		this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

         	}







         } catch (GlobalRecordAlreadyDeletedException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;

         } catch (GlobalBranchAccountNumberInvalidException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;



         } catch (GlobalTransactionAlreadyApprovedException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;

         } catch (GlobalTransactionAlreadyPendingException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;

         } catch (GlobalTransactionAlreadyPostedException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;
/*
          } catch (GlobalNoApprovalRequesterFoundException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;

         } catch (GlobalNoApprovalApproverFoundException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;
*/


         } catch (GlJREffectiveDateNoPeriodExistException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;

         } catch (GlJREffectiveDatePeriodClosedException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;

         } catch (GlobalJournalNotBalanceException ex) {

         	getSessionContext().setRollbackOnly();
         	throw ex;


         } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

             getSessionContext().setRollbackOnly();
             throw ex;

         } catch (GlobalExpiryDateNotFoundException ex){

             getSessionContext().setRollbackOnly();
             throw ex;

         } catch (GlobalMiscInfoIsRequiredException ex){

             getSessionContext().setRollbackOnly();
             throw ex;


         }catch (Exception ex) {

         	Debug.printStackTrace(ex);
         	getSessionContext().setRollbackOnly();
         	throw new EJBException(ex.getMessage());

         }

     }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getInvTraceMisc(String II_NAME, Integer AD_CMPNY) {

        Debug.print("InvAdjustmentEntryControllerBean getInvTraceMisc");

        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isTraceMisc = false;
        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
	        System.out.println("II_NAME="+II_NAME);
	        System.out.println("AD_CMPNY="+AD_CMPNY);
            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            System.out.println("invItem Name="+invItem.getIiName()+"-"+invItem.getIiCode());
            if (invItem.getIiTraceMisc() == 1){
            	System.out.println("true");
            	isTraceMisc = true;
            }
            System.out.println("isTraceMisc="+isTraceMisc);

            return isTraceMisc;
    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModAdjustmentDetails getInvAdjByAdjCode(Integer ADJ_CODE, Integer AD_BRNCH, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException {

        Debug.print("InvAdjustmentEntryControllerBean getInvAdjByAdjCode");

        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalInvTagHome invTagHome = null;
        LocalInvItemHome invItemHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;

        // Initialize EJB Home

        try {

        	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
        	invTagHome = (LocalInvTagHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
            		lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            		lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalInvAdjustment invAdjustment = null;

	        try {

	        	invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

	        } catch (FinderException ex) {

	            throw new GlobalNoRecordFoundException();

	        }
	        String specs = null;
        	String propertyCode = null;
        	String expiryDate = null;
        	String serialNumber = null;
	        ArrayList alList = new ArrayList();
	        ArrayList alList2 = new ArrayList();

	        Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();



	        Iterator i = invAdjustmentLines.iterator();


	        while (i.hasNext()) {

	        	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

	        	InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();
	        	//TODO: getInvAdjByAdjCode
	        	System.out.println(mdetails.getTraceMisc() + "<== getTraceMisc under getInvAdjByAdjCode controllerbean");

	        	mdetails.setTraceMisc(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc());
	        //	ArrayList tagList = new ArrayList();

	        	if (mdetails.getTraceMisc() == 1){

	        		ArrayList tagList = new ArrayList();

	        		tagList = this.getInvTagList(invAdjustmentLine);
	        		mdetails.setAlTagList(tagList);
	        		mdetails.setTraceMisc(mdetails.getTraceMisc());


	        	}



	        	mdetails.setAlCode(invAdjustmentLine.getAlCode());
	        	mdetails.setAlUnitCost(invAdjustmentLine.getAlUnitCost());
	        	mdetails.setAlQcNumber(invAdjustmentLine.getAlQcNumber());
	        	mdetails.setAlQcExpiryDate(invAdjustmentLine.getAlQcExpiryDate());
	        	mdetails.setAlAdjustQuantity(invAdjustmentLine.getAlAdjustQuantity());
	        	mdetails.setAlUomName(invAdjustmentLine.getInvUnitOfMeasure().getUomName());
	        	mdetails.setAlLocName(invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName());
	        	mdetails.setAlIiName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
	        	mdetails.setAlIiDescription(invAdjustmentLine.getInvItemLocation().getInvItem().getIiDescription());
	        	mdetails.setAlMisc(invAdjustmentLine.getAlMisc());

	        	alList.add(mdetails);
	        }

	        /*
	        System.out.println("LABAS ng LOOP");
	        Collection itmsCriteria = invItemHome.findEnabledIiAll(AD_CMPNY);
	        Iterator ia = itmsCriteria.iterator();

	        while (ia.hasNext()) {

	        	LocalInvItem invitms = (LocalInvItem)ia.next();

	        	Collection invAdjustmentLines2 = invAdjustment.getInvAdjustmentLines();
		        Iterator ib = invAdjustmentLines2.iterator();
	        	while (ib.hasNext()) {
	        	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)ib.next();
	        	System.out.println("LABAS ng LOOP itemA ="+invitms.getIiName());
	        	System.out.println("LABAS ng LOOP itemB ="+invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());

	        	if(invitms.getIiName()== invAdjustmentLine.getInvItemLocation().getInvItem().getIiName())
	        		{
	        		InvModAdjustmentLineDetails mdetails3 = new InvModAdjustmentLineDetails();
	        		System.out.println("LABAS ng LOOP item - " +invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
		        	mdetails3.setAlCode(invAdjustmentLine.getAlCode());
		        	mdetails3.setAlUnitCost(invAdjustmentLine.getAlUnitCost());
		        	mdetails3.setAlAdjustQuantity(invAdjustmentLine.getAlAdjustQuantity());
		        	mdetails3.setAlUomName(invAdjustmentLine.getInvUnitOfMeasure().getUomName());
		        	mdetails3.setAlLocName(invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName());
		        	mdetails3.setAlIiName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
		        	mdetails3.setAlIiDescription(invAdjustmentLine.getInvItemLocation().getInvItem().getIiDescription());
		        	mdetails3.setAlMisc(invAdjustmentLine.getAlMisc());

		        	alList2.add(mdetails3);
	        		}
	        	}

	        }

	       */

	        Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeAll("INV ADJUSTMENT", invAdjustment.getAdjCode(), AD_CMPNY);

			System.out.println("adApprovalQueues="+adApprovalQueues.size());
			ArrayList approverlist = new ArrayList();

			i = adApprovalQueues.iterator();

			while (i.hasNext()) {
				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

				AdModApprovalQueueDetails adModApprovalQueueDetails = new AdModApprovalQueueDetails();
				adModApprovalQueueDetails.setAqApprovedDate(adApprovalQueue.getAqApprovedDate());
				adModApprovalQueueDetails.setAqApproverName(adApprovalQueue.getAdUser().getUsrDescription());
				adModApprovalQueueDetails.setAqStatus(adApprovalQueue.getAqApproved() == EJBCommon.TRUE ? adApprovalQueue.getAqLevel()+ " APPROVED" : adApprovalQueue.getAqLevel()+" PENDING");

				approverlist.add(adModApprovalQueueDetails);
			}

	        InvModAdjustmentDetails details = new InvModAdjustmentDetails();
	        details.setAdjCode(invAdjustment.getAdjCode());
	        details.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
	        details.setAdjReferenceNumber(invAdjustment.getAdjReferenceNumber());
	        details.setAdjDescription(invAdjustment.getAdjDescription());
	        details.setAdjDate(invAdjustment.getAdjDate());

	        try{

	        	details.setAdjSplSupplierCode(invAdjustment.getApSupplier().getSplSupplierCode());
	        	details.setAdjSplSupplierName(invAdjustment.getApSupplier().getSplName());

	        } catch (Exception ex){

	        	details.setAdjSplSupplierCode("NO RECORD FOUND");
	        	details.setAdjSplSupplierName("NO RECORD FOUND");

	        }
	        details.setAdjType(invAdjustment.getAdjType());
	        details.setAdjApprovalStatus(invAdjustment.getAdjApprovalStatus());
	        details.setAdjPosted(invAdjustment.getAdjPosted());
	        details.setAdjCreatedBy(invAdjustment.getAdjCreatedBy());
	        details.setAdjDateCreated(invAdjustment.getAdjDateCreated());
	        details.setAdjLastModifiedBy(invAdjustment.getAdjLastModifiedBy());
	        details.setAdjDateLastModified(invAdjustment.getAdjDateLastModified());
	        details.setAdjApprovedRejectedBy(invAdjustment.getAdjApprovedRejectedBy());
	        details.setAdjDateApprovedRejected(invAdjustment.getAdjDateApprovedRejected());
	        details.setAdjPostedBy(invAdjustment.getAdjPostedBy());
	        details.setAdjDatePosted(invAdjustment.getAdjDatePosted());
	        details.setAdjCoaAccountNumber(invAdjustment.getGlChartOfAccount().getCoaAccountNumber());
	        details.setAdjCoaAccountDescription(invAdjustment.getGlChartOfAccount().getCoaAccountDescription());
	        details.setAdjReasonForRejection(invAdjustment.getAdjReasonForRejection());
	        details.setAdjIsCostVariance(invAdjustment.getAdjIsCostVariance());
	        details.setAdjVoid(invAdjustment.getAdjVoid());
	        details.setAdjNotedBy(invAdjustment.getAdjNotedBy());
	        details.setAdjAlList(alList);
	        details.setAdjAPRList(approverlist);

	        return details;

        } catch (GlobalNoRecordFoundException ex) {

        	Debug.printStackTrace(ex);
        	throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

	    /**
	     * @ejb:interface-method view-type="remote"
	     * @jboss:method-attributes read-only="true"
	     **/
	    public ArrayList getAdUsrAll(Integer AD_CMPNY) {

	        Debug.print("ApReceivingItemEntryControllerBean getAdUsrAll");

	        LocalAdUserHome adUserHome = null;

	        LocalAdUser adUser = null;

	        Collection adUsers = null;

	        ArrayList list = new ArrayList();

	        // Initialize EJB Home

	        try {

	            adUserHome = (LocalAdUserHome)EJBHomeFactory.
	                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

	        } catch (NamingException ex) {

	            throw new EJBException(ex.getMessage());

	        }

	        try {

	            adUsers = adUserHome.findUsrAll(AD_CMPNY);

	        } catch (FinderException ex) {

	        } catch (Exception ex) {

	            throw new EJBException(ex.getMessage());
	        }

	        if (adUsers.isEmpty()) {

	            return null;

	        }

	        Iterator i = adUsers.iterator();

	        while (i.hasNext()) {

	            adUser = (LocalAdUser)i.next();

	            list.add(adUser.getUsrName());

	        }

	        return list;

	    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/

    public String getNotedByMe(Integer AD_CMPNY) {

    	System.out.println("Debug line 2");
        Debug.print("InvAdjustmentEntryControllerBean getInvLocAll");

        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdPreference adPreference = null;
        String result="";

        // Initialize EJB Home

        try {

        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	result =  adPreference.getPrfApDefaultChecker();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}
          return result;
    }




    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {

    	Debug.print("InvAdjustmentEntryControllerBean getInvUomByIiName");

        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalInvItemHome invItemHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {

        	LocalInvItem invItem = null;
        	LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

        	invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
        	invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

        	Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
        			invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
        	while (i.hasNext()) {

        		LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();

        		try {
	                LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invUnitOfMeasure.getUomName(), AD_CMPNY);
	                if (invUnitOfMeasureConversion.getUmcBaseUnit() == EJBCommon.FALSE && invUnitOfMeasureConversion.getUmcConversionFactor() == 1) continue;
                } catch (FinderException ex) {
                	continue;
                }

        		InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
        		details.setUomName(invUnitOfMeasure.getUomName());

        		if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

        			details.setDefault(true);

        		}

        		list.add(details);

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
    * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvAdjEntryMobile(com.util.InvModAdjustmentDetails details,
		ArrayList alList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalRecordAlreadyDeletedException,
		GlobalBranchAccountNumberInvalidException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalInvItemLocationNotFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalDocumentNumberNotUniqueException,
		GlobalInventoryDateException,
		GlobalAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException,
		GlobalExpiryDateNotFoundException,
		GlobalMiscInfoIsRequiredException,
		GlobalRecordInvalidException{

    	Debug.print("InvAdjustmentEntryControllerBean saveInvAdjEntryMobile");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdApprovalHome adApprovalHome = null;
    	LocalAdAmountLimitHome adAmountLimitHome = null;
    	LocalAdApprovalUserHome adApprovalUserHome = null;
    	LocalAdApprovalQueueHome adApprovalQueueHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalInvItemHome invItemHome = null;
    	LocalInvTagHome invTagHome = null;
    	LocalAdUserHome adUserHome = null;
    	LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
    	 InvRepItemCostingControllerHome homeRIC = null;
         InvRepItemCostingController ejbRIC = null;
        // Initialize EJB Home

        try {

        	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        	adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
        	adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
        	adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
        	adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
        	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
        	invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
	            	lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
	            	lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
	            	lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
	            	lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
			homeRIC = (InvRepItemCostingControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }
        
        try {
        	ejbRIC = homeRIC.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {

        	LocalInvAdjustment invAdjustment = null;

        	// validate if Adjustment is already deleted

        	try {

        		if (details.getAdjCode() != null) {

        			invAdjustment = invAdjustmentHome.findByPrimaryKey(details.getAdjCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if adjustment is already posted, void, approved or pending

        	if (details.getAdjCode() != null) {


	        	if (invAdjustment.getAdjApprovalStatus() != null) {

	        		if (invAdjustment.getAdjApprovalStatus().equals("APPROVED") ||
	        			invAdjustment.getAdjApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();


	        		} else if (invAdjustment.getAdjApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		}

        	}

        	LocalInvAdjustment invExistingAdjustment = null;

        	try {

        		invExistingAdjustment = invAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);

        	} catch (FinderException ex) {

        	}


        	// 	validate if document number is unique document number is automatic then set next sequence

	        if (details.getAdjCode() == null) {


	        	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		        LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	        	if (invExistingAdjustment != null) {

	        		throw new GlobalDocumentNumberNotUniqueException();

	        	}

	        	try {

	        		adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

	        	} catch (FinderException ex) {

	        	}

	        	try {

	        		adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {

	        	}

	        	if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
	        			(details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {

	        		while (true) {

	        			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

	        				try {

	        					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
	        					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

	        				} catch (FinderException ex) {

	        					details.setAdjDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
	        					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
	        					break;

	        				}

 						} else {

 							try {

 								invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
 								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

 							} catch (FinderException ex) {

 								details.setAdjDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
 								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
 								break;

 							}

 						}

	        		}

	        	}

	        } else {


	        	if (invExistingAdjustment != null &&
	                !invExistingAdjustment.getAdjCode().equals(details.getAdjCode())) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

	            if (invAdjustment.getAdjDocumentNumber() != details.getAdjDocumentNumber() &&
	                (details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {

	                details.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());

	         	}

		    }

        	// used in checking if invoice should re-generate distribution records and re-calculate taxes
	        boolean isRecalculate = true;

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	LocalGlChartOfAccount glChartOfAccount = null;

        	try {

            	if (details.getAdjType().equals("GENERAL")) glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGeneralAdjustmentAccount());
            	else if (details.getAdjType().equals("ISSUANCE")) glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvIssuanceAdjustmentAccount());
            	else if (details.getAdjType().equals("PRODUCTION")) glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvProductionAdjustmentAccount());

        	} catch (FinderException ex) {
        		throw new GlobalAccountNumberInvalidException();

        	}

        	// create adjustment

        	if (details.getAdjCode() == null) {

        		invAdjustment = invAdjustmentHome.create(details.getAdjDocumentNumber(), details.getAdjReferenceNumber(), details.getAdjDescription(),
        				details.getAdjDate(), details.getAdjType(), details.getAdjApprovalStatus(),
						EJBCommon.FALSE, details.getAdjCreatedBy(), details.getAdjDateCreated(),
						details.getAdjLastModifiedBy(), details.getAdjDateLastModified(),
						null,null, null, null, null, null,  EJBCommon.FALSE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        	} else {

                // check if critical fields are changed

        		if (!invAdjustment.getGlChartOfAccount().getCoaAccountNumber().equals(glChartOfAccount.getCoaAccountNumber()) ||
					alList.size() != invAdjustment.getInvAdjustmentLines().size() ||
					!(invAdjustment.getAdjDate().equals(details.getAdjDate()))) {

        			isRecalculate = true;

        		} else if (alList.size() == invAdjustment.getInvAdjustmentLines().size()) {

        			Iterator alIter = invAdjustment.getInvAdjustmentLines().iterator();
        			Iterator alListIter = alList.iterator();

        			while (alIter.hasNext()) {

        				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)alIter.next();
        				InvModAdjustmentLineDetails mdetails = (InvModAdjustmentLineDetails)alListIter.next();

        				if (!invAdjustmentLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getAlIiName()) ||
            				!invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getAlLocName()) ||
	        				!invAdjustmentLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getAlUomName()) ||
	        				invAdjustmentLine.getAlAdjustQuantity() != mdetails.getAlAdjustQuantity() ||
	        				invAdjustmentLine.getAlUnitCost() != mdetails.getAlUnitCost() ||
	        				invAdjustmentLine.getAlMisc() != mdetails.getAlMisc()) {

        					isRecalculate = true;
        					break;

        				}

        			//	isRecalculate = false;

        			}

        		} else {

        		//	isRecalculate = false;

        		}

        		invAdjustment.setAdjDocumentNumber(details.getAdjDocumentNumber());
        		invAdjustment.setAdjReferenceNumber(details.getAdjReferenceNumber());
        		invAdjustment.setAdjDescription(details.getAdjDescription());
        		invAdjustment.setAdjDate(details.getAdjDate());
        		invAdjustment.setAdjType(details.getAdjType());
        		invAdjustment.setAdjApprovalStatus(details.getAdjApprovalStatus());
        		invAdjustment.setAdjLastModifiedBy(details.getAdjLastModifiedBy());
        		invAdjustment.setAdjDateLastModified(details.getAdjDateLastModified());
        		invAdjustment.setAdjReasonForRejection(null);

        	}

        	invAdjustment.setGlChartOfAccount(glChartOfAccount);

        	double ABS_TOTAL_AMOUNT = 0d;

        	if (isRecalculate) {

	        	// remove all adjustment lines

	        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

	        	Iterator i = invAdjustmentLines.iterator();

	        	while (i.hasNext()) {

	        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

	        		if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

	        		    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

	        			invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

	        		}

	        		i.remove();

	        		invAdjustmentLine.remove();

	        	}

	        	// remove all distribution records

	 	  	    Collection arDistributionRecords = invAdjustment.getInvDistributionRecords();

	 	  	    i = arDistributionRecords.iterator();

	 	  	    while (i.hasNext()) {

	 	  	   	    LocalInvDistributionRecord arDistributionRecord = (LocalInvDistributionRecord)i.next();

	 	  	  	    i.remove();

	 	  	  	    arDistributionRecord.remove();

	 	  	    }

	 	  	    LocalInvLocationHome invLocationHome = null;
	 	        try {

	 	            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
	 	            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME,
	 	                    LocalInvLocationHome.class);

	 	        } catch (NamingException ex) {

	 	            throw new EJBException(ex.getMessage());

	 	        }

	 	  	    // add new adjustment lines and distribution record

	 	  	    double TOTAL_AMOUNT = 0d;

	 	  	    byte DEBIT = 0;

	 	  	    i = alList.iterator();

	 	  	    while (i.hasNext()) {

	 	  	    	InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();

	 	  	    	LocalInvItemLocation invItemLocation = null;
	 	  	    	LocalInvItem invItem = null;

	 	  	    	try {

	      	  	    	invItem = invItemHome.findByPartNumber(mdetails.getAlPartNumber(), AD_CMPNY);
	      	  	    	LocalInvLocation invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
	      	  	    	invItemLocation = invItemLocationHome.findByLocNameAndIiName(invLocation.getLocName(), invItem.getIiName(), AD_CMPNY);
	      	  	    	mdetails.setAlIiName(invItem.getIiName());

	      	  	    	if (details.getAdjPurchaseUnit()==true) {
	      	  	    		Iterator iter = null;
	      	  	    		try {
	      	  	    			iter = invUnitOfMeasureHome.findByUomAdLvClass(invItem.getInvUnitOfMeasure().getUomAdLvClass(), AD_CMPNY).iterator();
	      	  	    		}
	      	  	    		catch (FinderException ex) {}
	      	  	    		while (iter.hasNext()) {
	      	  	    			LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure)iter.next();

	      	  	    			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invUnitOfMeasure.getUomName(), AD_CMPNY);
	      	  	    			if (invUnitOfMeasureConversion.getUmcBaseUnit()==1) {
	      	  	    				mdetails.setAlUomName(invUnitOfMeasure.getUomName());
	      	  	    				break;
	      	  	    			}
	      	  	    		}
	      	  	    	}
	      	  	    	else mdetails.setAlUomName(invItem.getInvUnitOfMeasure().getUomName());

	      	  	    	mdetails.setAlUnitCost(invItem.getIiUnitCost());
	      	  	    	mdetails.setAlLocName(invLocation.getLocName());

	 	  	    	} catch (FinderException ex) {
	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));

	 	  	    	}

	 	  	    	//	start date validation
	 	  	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		    				invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		    			if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
	 	  	    	}

	 	  	    	LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(mdetails, invAdjustment, EJBCommon.FALSE, AD_CMPNY);

	 	  	    	// add physical inventory distribution

	 	  	    	double AMOUNT = 0d;

	 	  	    	if (invAdjustmentLine.getAlAdjustQuantity() > 0) {

		 	  	    	AMOUNT = EJBCommon.roundIt(
		 	  	    			invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(),
								this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  	    	//AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(), adPreference.getPrfInvCostPrecisionUnit());
		 	  	    	DEBIT = EJBCommon.TRUE;

	 	  	    	} else {
	 	  	    	
	 	  	    		
		 	  	    	AMOUNT = EJBCommon.roundIt(
			 	  	    			invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(),
									this.getGlFcPrecisionUnit(AD_CMPNY));

						invAdjustmentLine.setAlUnitCost(AMOUNT);

						DEBIT = EJBCommon.FALSE;
						
						
						/*
						 * Removing due must get unit cost instead of cost of sales when quantity is negative
	 	  	    		double COST = 0d;

	 	  	    		try {

		 	  	    		LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(details.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
		 	  	    		    invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);


		 	  	    		if(invCosting.getCstRemainingQuantity() <= 0 && invCosting.getCstRemainingValue() <=0){
	                        	System.out.println("RE CALC");
	                        	HashMap criteria = new HashMap();
	                            criteria.put("itemName", invItemLocation.getInvItem().getIiName());
	                            criteria.put("location", invItemLocation.getInvLocation().getLocName());

	                            ArrayList branchList = new ArrayList();

	                            AdBranchDetails mdetailsb = new AdBranchDetails();
	                            mdetailsb.setBrCode(AD_BRNCH);
	        					branchList.add(mdetailsb);

	                            ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

	                            invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(details.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
	    		 	  	    		    invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		 	  	    		}

		 	  	    		//test if AVERAGE or FIFO
		 	  	    		if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
		          			{
		 	  	    			
		 	  	    			if(invCosting.getCstRemainingQuantity()<= 0) {
		 	  	    					COST = invItemLocation.getInvItem().getIiUnitCost();
		 	  	    			}else {
		 	  	    			


		 	  	    			//	COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), adPreference.getPrfInvCostPrecisionUnit());
			          				 COST = invCosting.getCstRemainingQuantity() <= 0 ? COST :
                                    Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());


									if(COST<=0){
		                               COST = invItemLocation.getInvItem().getIiUnitCost();
		                            }	
		 	  	    			}
		 	  	    			
		          				//COST = invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity();
		          			}
		          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
		          			{
		          				COST = this.getInvFifoCost(invAdjustment.getAdjDate(), invItemLocation.getIlCode(), invAdjustmentLine.getAlAdjustQuantity(),
		          						invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
		         			}
		          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
		          			{
		          				COST = invItemLocation.getInvItem().getIiUnitCost();
		         			}

		 	  	        } catch (FinderException ex) {

		 	  	            COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();

		 	  	        }

		 	  	        COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
		 	  	        		invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
								COST, true, AD_CMPNY);

		 	  	        invAdjustmentLine.setAlUnitCost(COST);

	 	  	    		AMOUNT = EJBCommon.roundIt(
		 	  	    			invAdjustmentLine.getAlAdjustQuantity() * COST,
		 	  	    			adPreference.getPrfInvCostPrecisionUnit());
						//AMOUNT = invAdjustmentLine.getAlAdjustQuantity() * COST;
						System.out.println("AMOUNT>>: " + AMOUNT);
		 	  	    	DEBIT = EJBCommon.FALSE;
						*/
	 	  	    	}

	 	  	    	// check for branch mapping

	 	  	    	LocalAdBranchItemLocation adBranchItemLocation = null;

	 	  	    	try{
	 	  	    		adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
	 	  	    				invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

	 	  	    	} catch (FinderException ex) {

	 	  	    	}

	 	  	    	LocalGlChartOfAccount glInventoryChartOfAccount = null;

	 	  	    	if (adBranchItemLocation == null) {
	 	  	    		glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
	 	  	    				invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
	 	  	    	} else {
	 	  	    		glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
	 	  	    				adBranchItemLocation.getBilCoaGlInventoryAccount());

	 	  	    	}

	 	  	    	this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(AMOUNT), EJBCommon.FALSE,
	 	  	    			glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

	 	  	    	TOTAL_AMOUNT += AMOUNT;
	 	  	    	ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);

	 	  	    	// add adjust quantity to item location committed quantity if negative

	 	  	    	if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

	 	  	    		double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

	 	  	    		invItemLocation = invAdjustmentLine.getInvItemLocation();

	 	  	    		invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

	 	  	    	}

	      	  	    try{
	      	  	    	System.out.println("aabot?");
	      	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
		      	  	    Iterator t = mdetails.getAlTagList().iterator();

		      	  	    LocalInvTag invTag  = null;
		      	  	    System.out.println("umabot?");

		      	  	    if (invItem.getIiPartNumber().charAt(2)=='1') {

			      	  	    while (t.hasNext()){
			      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
			      	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
			      	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
			      	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
			      	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
			      	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

			      	  	    	if (tgLstDetails.getTgCode()==null){
			      	  	    		System.out.println("ngcreate ba?");
				      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
				      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
				      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
				      	  	    			tgLstDetails.getTgType());

				      	  	    	invTag.setInvAdjustmentLine(invAdjustmentLine);
				      	  	    	LocalAdUser adUser = null;
				      	  	    	try {
				      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
				      	  	    	}catch(FinderException ex){

				      	  	    	}
				      	  	    	invTag.setAdUser(adUser);
				      	  	    	System.out.println("ngcreate ba?");
			      	  	    	}

			      	  	    }

		      	  	    }

	      	  	    }catch(Exception ex){

	      	  	    }

	 	  	    }

	 	  	    // add variance or transfer/debit distribution

	 	  	    DEBIT = TOTAL_AMOUNT > 0 ? EJBCommon.FALSE : EJBCommon.TRUE;

	 	  	    this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_AMOUNT), EJBCommon.FALSE,
	 	  	    		invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

        	} else {

        		Iterator i = alList.iterator();

	 	  	    while (i.hasNext()) {

	 	  	    	InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();

	 	  	    	LocalInvItemLocation invItemLocation = null;

	 	  	    	try {

	 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(), mdetails.getAlIiName(), AD_CMPNY);

	 	  	    	} catch (FinderException ex) {

	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));

	 	  	    	}

	 	  	    	//	start date validation

	 	  	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		    				invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		    			if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
	 	  	    	}

	 	  	    }

        		Collection invAdjDistributionRecords = invAdjustment.getInvDistributionRecords();

        		i = invAdjDistributionRecords.iterator();

        		while(i.hasNext()) {

        			LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();

        			if(distributionRecord.getDrDebit() == 1) {

        				ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();

        			}

        		}

        	}

        	//Insufficient Stocks
        	if (adPreference.getPrfArCheckInsufficientStock() == EJBCommon.TRUE && !isDraft) {
        		System.out.println("CHECK A");
	        	boolean hasInsufficientItems = false;
	        	String insufficientItems = "";

	        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

	        	Iterator i = invAdjustmentLines.iterator();

	        	HashMap cstMap = new HashMap();

	        	while (i.hasNext()) {

	        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

	        		if(invAdjustmentLine.getAlAdjustQuantity() < 0 ) {

	    				LocalInvCosting invCosting = null;
	    				double CURR_QTY = 0;
	    				boolean isIlFound = false;

	    				double ILI_QTY = this.convertByUomAndQuantity( invAdjustmentLine.getInvUnitOfMeasure(),
	    						invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

	    				if(cstMap.containsKey(invAdjustmentLine.getInvItemLocation().getIlCode().toString())){

		  	   	    		isIlFound =  true;
		  	   	    		CURR_QTY = ((Double)cstMap.get(invAdjustmentLine.getInvItemLocation().getIlCode().toString())).doubleValue();

	    				}else{

	    					try {

	    						invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
	    								invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
	    								invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	    						CURR_QTY = invCosting.getCstRemainingQuantity();

	    					} catch (FinderException ex) {

	    					}
	    				}

	    				if(invCosting != null){

		  	   	    		CURR_QTY = this.convertByUomAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(),
	        						invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invCosting.getCstRemainingQuantity()), AD_CMPNY);

		  	   	    	}

	    				double LOWEST_QTY = this.convertByUomAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(),
	    						invAdjustmentLine.getInvItemLocation().getInvItem(),1, AD_CMPNY);

	    				if ((invCosting == null && isIlFound==false)|| CURR_QTY == 0 || CURR_QTY - ILI_QTY <= -LOWEST_QTY) {

	    					hasInsufficientItems = true;
	    					insufficientItems += invAdjustmentLine.getInvItemLocation().getInvItem().getIiName() + ", ";
	    				}

	    				CURR_QTY -= ILI_QTY;

		  	   	    	if(isIlFound){
		  	   	    		cstMap.remove(invAdjustmentLine.getInvItemLocation().getIlCode().toString());
		  	   	    	}

		  	   	    	cstMap.put(invAdjustmentLine.getInvItemLocation().getIlCode().toString(), new Double(CURR_QTY));

	        		}

	        	}
	        	if(hasInsufficientItems) {

	        		throw new GlobalRecordInvalidException(insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
	        	}
	        }
 	  	    // generate approval status

 	  	    String INV_APPRVL_STATUS = null;

		 	if (!isDraft) {

		 		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

		 		// check if ar invoice approval is enabled

		 		if (adApproval.getAprEnableInvAdjustment() == EJBCommon.FALSE) {

		 			INV_APPRVL_STATUS = "N/A";

		 		} else {	
		 				
		 				
		 		//	LocalAdUser adUser = adUserHome.findByUsrName(details.getAdjLastModifiedBy(), AD_CMPNY);

		INV_APPRVL_STATUS = "N/A";
				//	INV_APPRVL_STATUS = ejbIA.getApprovalStatus(adUser.getUsrDept(), invAdjustment.getAdjLastModifiedBy(), adUser.getUsrDescription(), "INV ADJUSTMENT", invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(), AD_BRNCH, AD_CMPNY);

				}
		 	}

        	if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

        		this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}

		 	// set adjustment approval status

		 	invAdjustment.setAdjApprovalStatus(INV_APPRVL_STATUS);

		 	return invAdjustment.getAdjCode();

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

            	getSessionContext().setRollbackOnly();
            	throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;
			/*
         } catch (GlobalNoApprovalRequesterFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;
		*/
        } catch (GlobalInvItemLocationNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalInventoryDateException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalExpiryDateNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalMiscInfoIsRequiredException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        }catch (GlobalRecordInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        }catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public String getInvPrfDefaultAdjustmentAccount(String ADJ_TYP, Integer AD_CMPNY, String USR_NM) {

    	Debug.print("InvAdjustmentEntryControllerBean getInvPrfDefaultAdjustmentAccount");

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdUserHome adUserHome = null;

        // Initialize EJB Home

        try {
        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        	adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        String COA_ACCNT_NMBR = "";

        try {
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalGlChartOfAccount glChartOfAccount = null;

        	if (ADJ_TYP.equals("GENERAL")) glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGeneralAdjustmentAccount());
        	else if (ADJ_TYP.equals("WASTAGE")) glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvWastageAdjustmentAccount());
        	else if (ADJ_TYP.equals("VARIANCE")) glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
        	else if (ADJ_TYP.equals("ISSUANCE")) glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvIssuanceAdjustmentAccount());
        	else if (ADJ_TYP.equals("PRODUCTION")) glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvProductionAdjustmentAccount());

        	LocalAdUser adUser = adUserHome.findByUsrName(USR_NM, AD_CMPNY);
        	LocalGlChartOfAccount glChartOfAccount2 = glChartOfAccountHome.findByCoaAccountNumber(glChartOfAccount.getCoaAccountNumber().substring(0, glChartOfAccount.getCoaAccountNumber().indexOf("-"))+"-"+adUser.getUsrDept(), AD_CMPNY);

        	//COA_ACCNT_NMBR = glChartOfAccount.getCoaAccountNumber();

        	COA_ACCNT_NMBR = glChartOfAccount.getCoaAccountNumber().substring(0, glChartOfAccount.getCoaAccountNumber().indexOf("-"))+"-"+adUser.getUsrDept();

        }
        catch (FinderException ex) {
        }
    	return COA_ACCNT_NMBR;
    }

    /**
    * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvAdjEntry(com.util.InvAdjustmentDetails details,
    		String COA_ACCOUNT_NUMBER,String SPL_SPPLR_CODE,
		ArrayList alList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalRecordAlreadyDeletedException,
		GlobalBranchAccountNumberInvalidException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalInvItemLocationNotFoundException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalDocumentNumberNotUniqueException,
		GlobalInventoryDateException,
		GlobalInvTagMissingException,
		InvTagSerialNumberNotFoundException,
		GlobalInvTagExistingException,
		GlobalAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException,
		GlobalExpiryDateNotFoundException,
		GlobalMiscInfoIsRequiredException,
		GlobalRecordInvalidException{

    	Debug.print("InvAdjustmentEntryControllerBean saveInvAdjEntry");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdApprovalHome adApprovalHome = null;
    	LocalAdAmountLimitHome adAmountLimitHome = null;
    	LocalAdApprovalUserHome adApprovalUserHome = null;
    	LocalAdApprovalQueueHome adApprovalQueueHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalInvItemHome invItemHome = null;
    	LocalInvTagHome invTagHome = null;
        LocalAdUserHome adUserHome = null;
        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalApSupplierHome apSupplierHome = null;
        InvRepItemCostingControllerHome homeRIC = null;
        InvRepItemCostingController ejbRIC = null;
        
        InvApprovalControllerHome homeIA = null;
        InvApprovalController ejbIA = null;
        // Initialize EJB Home

        try {

        	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        	adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
        	adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
        	adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
        	adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
        	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
        	invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
    				lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
    				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

            homeRIC = (InvRepItemCostingControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);
                    
             homeIA = (InvApprovalControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvApprovalControllerEJB", InvApprovalControllerHome.class);



        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {
        	ejbRIC = homeRIC.create();
        	ejbIA = homeIA.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {

        	LocalInvAdjustment invAdjustment = null;

        	// validate if Adjustment is already deleted

        	try {

        		if (details.getAdjCode() != null) {

        			invAdjustment = invAdjustmentHome.findByPrimaryKey(details.getAdjCode());

        		}

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if adjustment is already posted, void, approved or pending

        	if (details.getAdjCode() != null && details.getAdjVoid() == EJBCommon.FALSE) {

				System.out.println("posted with void = false");
	        	if (invAdjustment.getAdjApprovalStatus() != null) {

	        		if (invAdjustment.getAdjApprovalStatus().equals("APPROVED") ||
	        			invAdjustment.getAdjApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();


	        		} else if (invAdjustment.getAdjApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		}

        	}
        	
        	
        	
        	
        	// validate if receipt is already posted, void, approved or pending

	        if (details.getAdjCode() != null && details.getAdjVoid() == EJBCommon.FALSE) {

				System.out.println("posted with void = true");
        		if (invAdjustment.getAdjApprovalStatus() != null) {

	        		if (invAdjustment.getAdjApprovalStatus().equals("APPROVED") ||
	        		    invAdjustment.getAdjApprovalStatus().equals("N/A")) {

	        		    throw new GlobalTransactionAlreadyApprovedException();

	        		} else if (invAdjustment.getAdjApprovalStatus().equals("PENDING")) {

	        			throw new GlobalTransactionAlreadyPendingException();

	        		}

	        	}

        		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyPostedException();

        		} else if (invAdjustment.getAdjVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

        	}

        	
        	
        	
        	
        	
        	if (details.getAdjCode() != null && details.getAdjVoid() == EJBCommon.TRUE) {
        	
        		if (invAdjustment.getAdjVoid() == EJBCommon.TRUE) {

        			throw new GlobalTransactionAlreadyVoidException();

        		}

    		

        		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {
        			 // generate approval status

		    		String RCT_APPRVL_STATUS = null;
		    		
		    		this.executeVoidInvAdjustment(invAdjustment, AD_BRNCH, AD_CMPNY);
        		
        		}
        	
        	
        	
        		invAdjustment.setAdjVoid(EJBCommon.TRUE);
		        invAdjustment.setAdjLastModifiedBy(details.getAdjLastModifiedBy());
        		invAdjustment.setAdjDateLastModified(details.getAdjDateLastModified());

	    	    return invAdjustment.getAdjCode();
        	
        	
        	}
        	
        	
        	
        	
        	
        	
        	

        	LocalInvAdjustment invExistingAdjustment = null;

        	try {

        		invExistingAdjustment = invAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);

        	} catch (FinderException ex) {

        	}


        	// 	validate if document number is unique document number is automatic then set next sequence

	        if (details.getAdjCode() == null) {


	        	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		        LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

	        	if (invExistingAdjustment != null) {

	        		throw new GlobalDocumentNumberNotUniqueException();

	        	}

	        	try {

	        		adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

	        	} catch (FinderException ex) {

	        	}

	        	try {

	        		adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

	        	} catch (FinderException ex) {

	        	}

	        	if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
	        			(details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {

	        		while (true) {

	        			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

	        				try {

	        					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
	        					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

	        				} catch (FinderException ex) {

	        					details.setAdjDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
	        					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
	        					break;

	        				}

 						} else {

 							try {

 								invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
 								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

 							} catch (FinderException ex) {

 								details.setAdjDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
 								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
 								break;

 							}

 						}

	        		}

	        	}

	        } else {


	        	if (invExistingAdjustment != null &&
	                !invExistingAdjustment.getAdjCode().equals(details.getAdjCode())) {

	            	throw new GlobalDocumentNumberNotUniqueException();

	            }

	            if (invAdjustment.getAdjDocumentNumber() != details.getAdjDocumentNumber() &&
	                (details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {

	                details.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());

	         	}

		    }

        	// used in checking if invoice should re-generate distribution records and re-calculate taxes
	        boolean isRecalculate = true;


        	// create adjustment

        	if (details.getAdjCode() == null) {

        		invAdjustment = invAdjustmentHome.create(details.getAdjDocumentNumber(), details.getAdjReferenceNumber(), details.getAdjDescription(),
        				details.getAdjDate(), details.getAdjType(), details.getAdjApprovalStatus(),
						EJBCommon.FALSE, details.getAdjCreatedBy(), details.getAdjDateCreated(),
						details.getAdjLastModifiedBy(), details.getAdjDateLastModified(),
						null, null, null, null, details.getAdjNotedBy(),null, EJBCommon.FALSE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

        	} else {

                // check if critical fields are changed

        		if (!invAdjustment.getGlChartOfAccount().getCoaAccountNumber().equals(COA_ACCOUNT_NUMBER) ||
					alList.size() != invAdjustment.getInvAdjustmentLines().size() ||
					!(invAdjustment.getAdjDate().equals(details.getAdjDate()))) {

        			isRecalculate = true;

        		} else if (alList.size() == invAdjustment.getInvAdjustmentLines().size()) {

        			Iterator alIter = invAdjustment.getInvAdjustmentLines().iterator();
        			Iterator alListIter = alList.iterator();

        			while (alIter.hasNext()) {

        				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)alIter.next();
        				InvModAdjustmentLineDetails mdetails = (InvModAdjustmentLineDetails)alListIter.next();

        				if (!invAdjustmentLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getAlIiName()) ||
            				!invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getAlLocName()) ||
	        				!invAdjustmentLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getAlUomName()) ||
	        				invAdjustmentLine.getAlAdjustQuantity() != mdetails.getAlAdjustQuantity() ||
	        				invAdjustmentLine.getAlUnitCost() != mdetails.getAlUnitCost() ||
	        				invAdjustmentLine.getAlQcNumber() != mdetails.getAlQcNumber() ||
	        				invAdjustmentLine.getAlQcExpiryDate() != mdetails.getAlQcExpiryDate() ||

	        				invAdjustmentLine.getAlMisc() != mdetails.getAlMisc()) {

        					isRecalculate = true;
        					break;

        				}

        			//	isRecalculate = false;

        			}

        		} else {

        		//	isRecalculate = false;

        		}

        		invAdjustment.setAdjDocumentNumber(details.getAdjDocumentNumber());
        		invAdjustment.setAdjReferenceNumber(details.getAdjReferenceNumber());
        		invAdjustment.setAdjDescription(details.getAdjDescription());
        		invAdjustment.setAdjDate(details.getAdjDate());
        		invAdjustment.setAdjType(details.getAdjType());
        		invAdjustment.setAdjApprovalStatus(details.getAdjApprovalStatus());
        		invAdjustment.setAdjLastModifiedBy(details.getAdjLastModifiedBy());
        		invAdjustment.setAdjDateLastModified(details.getAdjDateLastModified());
        		invAdjustment.setAdjReasonForRejection(null);
        		System.out.println("y");
        		invAdjustment.setAdjNotedBy(details.getAdjNotedBy());

        	}

        	try{

        		LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
    			invAdjustment.setApSupplier(apSupplier);


        	} catch (Exception ex){

        	}

        	if (COA_ACCOUNT_NUMBER!=null && COA_ACCOUNT_NUMBER.equals("UPLOAD")) COA_ACCOUNT_NUMBER = getInvPrfDefaultAdjustmentAccount(details.getAdjType(), AD_CMPNY, details.getAdjCreatedBy());

        	try {

        		LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(COA_ACCOUNT_NUMBER, AD_BRNCH, AD_CMPNY);
            	//glChartOfAccount.addInvAdjustment(invAdjustment);
            	invAdjustment.setGlChartOfAccount(glChartOfAccount);

        	} catch (FinderException ex) {

        		throw new GlobalAccountNumberInvalidException();

        	}

        	double ABS_TOTAL_AMOUNT = 0d;

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);


        	if (isRecalculate) {

	        	// remove all adjustment lines

	        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

	        	Iterator i = invAdjustmentLines.iterator();

	        	while (i.hasNext()) {

	        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

	        		if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

	        		    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

	        			invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

	        		}
	        		//remove all inv tag inside adjustment line
		  	   	    Collection invTags = invAdjustmentLine.getInvTags();

		  	   	    Iterator x = invTags.iterator();

		  	   	    while (x.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();

		  	   	    	x.remove();

		  	   	    	invTag.remove();
		  	   	    }
	        		i.remove();

	        		invAdjustmentLine.remove();

	        	}

	        	// remove all distribution records

	 	  	    Collection arDistributionRecords = invAdjustment.getInvDistributionRecords();

	 	  	    i = arDistributionRecords.iterator();

	 	  	    while (i.hasNext()) {

	 	  	   	    LocalInvDistributionRecord arDistributionRecord = (LocalInvDistributionRecord)i.next();

	 	  	  	    i.remove();

	 	  	  	    arDistributionRecord.remove();

	 	  	    }

	 	  	    // add new adjustment lines and distribution record

	 	  	    double TOTAL_AMOUNT = 0d;

	 	  	    byte DEBIT = 0;

	 	  	    i = alList.iterator();

	 	  	    while (i.hasNext()) {

	 	  	    	InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();

	 	  	    	LocalInvItemLocation invItemLocation = null;

	 	  	    	try {
	 	  	    		if (mdetails.getAlLocName().equals("UPLOAD")) mdetails.setAlLocName(invLocationHome.findByPrimaryKey(invItemHome.findByIiName(mdetails.getAlIiName(),AD_CMPNY).getIiDefaultLocation()).getLocName());
	 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(), mdetails.getAlIiName(), AD_CMPNY);

	 	  	    	} catch (FinderException ex) {

	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));

	 	  	    	}

	 	  	    	if (mdetails.getAlUnitCost()==0) mdetails.setAlUnitCost(invItemLocation.getInvItem().getIiUnitCost());

	 	  	    	//	start date validation
	 	  	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		    				invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		    			if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
	 	  	    	}

	 	  	    	LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(mdetails, invAdjustment, EJBCommon.FALSE, AD_CMPNY);

	 	  	    	// add physical inventory distribution

	 	  	    	double AMOUNT = 0d;

					




	 	  	    	if (invAdjustmentLine.getAlAdjustQuantity() > 0 && !invAdjustmentLine.getInvAdjustment().getAdjType().equals("ISSUANCE")) {
					//if (!invAdjustmentLine.getInvAdjustment().getAdjType().equals("ISSUANCE")) {

		 	  	    	AMOUNT = EJBCommon.roundIt(
		 	  	    			invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(),
								this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  	    	//AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(), adPreference.getPrfInvCostPrecisionUnit());
		 	  	    	DEBIT = EJBCommon.TRUE;

	 	  	    	} else {

						
						
						
						invAdjustmentLine.setAlUnitCost(Math.abs(invAdjustmentLine.getAlUnitCost()));

	 	  	    		AMOUNT = EJBCommon.roundIt(
		 	  	    			invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(),
		 	  	    			adPreference.getPrfInvCostPrecisionUnit());
						//AMOUNT = invAdjustmentLine.getAlAdjustQuantity() * COST;
						System.out.println("AMOUNT>>: " + AMOUNT);
		 	  	    	DEBIT = EJBCommon.FALSE;
						
						
						/*
	 	  	    		double COST = invItemLocation.getInvItem().getIiUnitCost();

	 	  	    		try {

		 	  	    		LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(details.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
		 	  	    		    invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);


		 	  	    		if(invCosting.getCstRemainingQuantity() <= 0 && invCosting.getCstRemainingValue() <=0){
	                        	System.out.println("RE CALC");
	                        	HashMap criteria = new HashMap();
	                            criteria.put("itemName", invItemLocation.getInvItem().getIiName());
	                            criteria.put("location", invItemLocation.getInvLocation().getLocName());

	                            ArrayList branchList = new ArrayList();

	                            AdBranchDetails mdetailsb = new AdBranchDetails();
	                            mdetailsb.setBrCode(AD_BRNCH);
	        					branchList.add(mdetailsb);

	                            ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

	                            invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(details.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
	    		 	  	    		    invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		 	  	    		}

		 	  	    		//test if AVERAGE or FIFO
		 	  	    		if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
		          			{
		 	  	    			
		 	  	    			if(invCosting.getCstRemainingQuantity()<= 0 ){
		 	  	    				COST = invItemLocation.getInvItem().getIiUnitCost();

		 	  	    			
		 	  	    			}else {
		 	  	    			
		 	  	    				 COST = invItemLocation.getInvItem().getIiUnitCost();
		 	  	    				 
		 	  	    				// COST = invCosting.getCstRemainingQuantity() <= 0 ? COST :
                                 //   Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
									

									if(COST<=0){
		                               COST = invItemLocation.getInvItem().getIiUnitCost();
		                            }	
		 	  	    			}
		          					//COST = invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity();
		          			}
		          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
		          			{
		          				//COST = invAdjustmentLine.getAlUnitCost();
		          				COST = this.getInvFifoCost(invAdjustment.getAdjDate(), invItemLocation.getIlCode(), invAdjustmentLine.getAlAdjustQuantity(),
		          						invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
		         			}
		          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
		          			{
		          				COST = invItemLocation.getInvItem().getIiUnitCost();
		         			}

		 	  	        } catch (FinderException ex) {

		 	  	            COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();

		 	  	        }

		 	  	        COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
		 	  	        		invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
								COST, true, AD_CMPNY);

		 	  	        invAdjustmentLine.setAlUnitCost(Math.abs(COST));

	 	  	    		AMOUNT = EJBCommon.roundIt(
		 	  	    			invAdjustmentLine.getAlAdjustQuantity() * COST,
		 	  	    			adPreference.getPrfInvCostPrecisionUnit());
						//AMOUNT = invAdjustmentLine.getAlAdjustQuantity() * COST;
						System.out.println("AMOUNT>>: " + AMOUNT);
		 	  	    	DEBIT = EJBCommon.FALSE;
						*/
	 	  	    	}

	 	  	    	// check for branch mapping

	 	  	    	LocalAdBranchItemLocation adBranchItemLocation = null;

	 	  	    	try{

	 	  	    		adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
	 	  	    				invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

	 	  	    	} catch (FinderException ex) {

	 	  	    	}

	 	  	    	LocalGlChartOfAccount glInventoryChartOfAccount = null;

	 	  	    	if (adBranchItemLocation == null) {

	 	  	    		glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
	 	  	    				invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
	 	  	    	} else {

	 	  	    		glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
	 	  	    				adBranchItemLocation.getBilCoaGlInventoryAccount());

	 	  	    	}

	 	  	    	this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(AMOUNT), EJBCommon.FALSE,
	 	  	    			glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

	 	  	    	TOTAL_AMOUNT += AMOUNT;
	 	  	    	ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);

	 	  	    	// add adjust quantity to item location committed quantity if negative

	 	  	    	if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

	 	  	    		double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

	 	  	    		invItemLocation = invAdjustmentLine.getInvItemLocation();

	 	  	    		invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

	 	  	    	}

	 	  	    	//TODO: add new inv Tag
	 	  	    	/*if (invItemLocation.getInvItem().getIiDineInCharge()==1 && mdetails.getAlTagList() != null){
	      	  	    	throw new GlobalInvTagExistingException (invItemLocation.getInvItem().getIiName());
	      	  	    }*/
	 	  	    	if (mdetails.getTraceMisc() == 1 && !details.getAdjType().equals("VARIANCE")){

		      	  	    if (invItemLocation.getInvItem().getIiNonInventoriable()==0 && mdetails.getAlTagList() != null
			      	  			&& mdetails.getAlTagList().size() < mdetails.getAlAdjustQuantity()){

		      	  	    	throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiName());
		      	  	    }
		      	  	    try{
		      	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
			      	  	    Iterator t = mdetails.getAlTagList().iterator();
			      	  	    LocalInvItem invItem = invItemHome.findByIiName(mdetails.getAlIiName(), AD_CMPNY);
			      	  	    LocalInvTag invTag  = null;
			      	  	    while (t.hasNext()){
			      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();

				      	  	    if (invItemLocation.getInvItem().getIiNonInventoriable()==0 && tgLstDetails.getTgCustodian().equals("")
			      	  	    			&&tgLstDetails.getTgSpecs().equals("")&&tgLstDetails.getTgPropertyCode().equals("")
			      	  	    			&&tgLstDetails.getTgExpiryDate() == null && tgLstDetails.getTgSerialNumber().equals("")){
			      	  	    		throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiDescription());
			      	  	    	}
			      	  	    	if (tgLstDetails.getTgCode()==null){

			      	  	    		if (details.getAdjType().equals("ISSUANCE")) {
				      	  	    		try {
				      	  	    			Collection invTg = invTagHome.findAllInByTgSerialNumberAndAdBranchAndAdCompany(tgLstDetails.getTgSerialNumber(),AD_BRNCH,AD_CMPNY);
				      	  	    			if (invTg.isEmpty()) {
				      	  	    				throw new InvTagSerialNumberNotFoundException(tgLstDetails.getTgSerialNumber());
				      	  	    			}
				      	  	    			else {
						      	  	    		try {
						      	  	    			invTg = invTagHome.findAllOutByTgSerialNumberAndAdBranchAndAdCompany(tgLstDetails.getTgSerialNumber(),AD_BRNCH,AD_CMPNY);
						      	  	    			if (!invTg.isEmpty()) {
						      	  	    				throw new InvTagSerialNumberNotFoundException(tgLstDetails.getTgSerialNumber());
						      	  	    			}
						      	  	    		}
						      	  	    		catch (FinderException ex) {}
				      	  	    			}
				      	  	    		}
				      	  	    		catch (FinderException ex) {}
			      	  	    		}
				      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
				      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
				      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
				      	  	    			tgLstDetails.getTgType());

				      	  	    	invTag.setInvAdjustmentLine(invAdjustmentLine);
				      	  	    	LocalAdUser adUser = null;
				      	  	    	try {
				      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
				      	  	    	}catch(FinderException ex){

				      	  	    	}
					      	  	    if(tgLstDetails.getTgDocumentNumber().equals("")){
					      	  	    	if (invItemLocation.getInvItem().getIiNonInventoriable()==1){
					      	  	    		invTag.setTgDocumentNumber(adPreference.getPrfInvNextCustodianNumber2());
				      	  	    			adPreference.setPrfInvNextCustodianNumber2(EJBCommon.incrementStringNumber(adPreference.getPrfInvNextCustodianNumber2()));
					      	  	    	}else{
					      	  	    		invTag.setTgDocumentNumber(adPreference.getPrfInvNextCustodianNumber1());
					      	  	    		adPreference.setPrfInvNextCustodianNumber1(EJBCommon.incrementStringNumber(adPreference.getPrfInvNextCustodianNumber1()));
			      	  	    			}
				      	  	    	}else{
				      	  	    		invTag.setTgDocumentNumber(tgLstDetails.getTgDocumentNumber());
				      	  	    	}
				      	  	    	invTag.setAdUser(adUser);

			      	  	    	}

			      	  	    }

		      	  	    }
		      	  	    catch(InvTagSerialNumberNotFoundException ex) {
		      	  	    	throw new InvTagSerialNumberNotFoundException(ex.getMessage());
		      	  	    }
		      	  	    catch(Exception ex){
			      	  	    if (invItemLocation.getInvItem().getIiNonInventoriable()==0 ){
		      	  	    		throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiName());
		      	  	    	}
		      	  	    }
	      	  	    }


	 	  	    }

	 	  	    // add variance or transfer/debit distribution

	 	  	    DEBIT = (TOTAL_AMOUNT >= 0 && !invAdjustment.getAdjType().equals("ISSUANCE")) ? EJBCommon.FALSE : EJBCommon.TRUE;

	 	  	    this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_AMOUNT), EJBCommon.FALSE,
	 	  	    		invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

        	} else {

        		Iterator i = alList.iterator();

	 	  	    while (i.hasNext()) {

	 	  	    	InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();

	 	  	    	LocalInvItemLocation invItemLocation = null;

	 	  	    	try {

	 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(), mdetails.getAlIiName(), AD_CMPNY);

	 	  	    	} catch (FinderException ex) {

	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));

	 	  	    	}

	 	  	    	//	start date validation

	 	  	    	LocalInvAdjustmentLine invAdjustmentLine = invAdjustmentLineHome.findByPrimaryKey(mdetails.getAlCode());

		  	   	    //remove all inv tag inside PO line
		  	   	    Collection invTags = invAdjustmentLine.getInvTags();

		  	   	    Iterator x = invTags.iterator();

		  	   	    while (x.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();

		  	   	    	x.remove();

		  	   	    	invTag.remove();
		  	   	    }

	      	  	    /*if (invItemLocation.getInvItem().getIiDineInCharge()==1 && mdetails.getAlTagList() != null){
	      	  	    	throw new GlobalInvTagExistingException (invItemLocation.getInvItem().getIiName());
	      	  	    }*/
		      	  	if (mdetails.getTraceMisc() == 1){

			      	  	if (invItemLocation.getInvItem().getIiNonInventoriable()==0 && mdetails.getAlTagList() != null
			      	  			&& mdetails.getAlTagList().size() < mdetails.getAlAdjustQuantity()){

		      	  	    	throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiName());
		      	  	    }
			      	  	try{
			      	  	    Iterator t = mdetails.getAlTagList().iterator();
			      	  	    while (t.hasNext()){
			      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();

			      	  	    	if (invItemLocation.getInvItem().getIiNonInventoriable()==0 && tgLstDetails.getTgCustodian().equals("")
			      	  	    			&&tgLstDetails.getTgSpecs().equals("")&&tgLstDetails.getTgPropertyCode().equals("")
			      	  	    			&&tgLstDetails.getTgExpiryDate() == null && tgLstDetails.getTgSerialNumber().equals("")){
			      	  	    		throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiDescription());
			      	  	    	}
		      	  	    	    LocalInvTag invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
			      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
			      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
			      	  	    			tgLstDetails.getTgType());

			      	  	    	invTag.setInvAdjustmentLine(invAdjustmentLine);
			      	  	    	LocalAdUser adUser = null;
			      	  	    	try {
			      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
			      	  	    	}catch(FinderException ex){

			      	  	    	}
				      	  	    if(tgLstDetails.getTgDocumentNumber().equals("")){
				      	  	    	System.out.println("tg document number empty?");
				      	  	    	if (invItemLocation.getInvItem().getIiNonInventoriable()==1){
				      	  	    		invTag.setTgDocumentNumber(adPreference.getPrfInvNextCustodianNumber2());
			      	  	    			adPreference.setPrfInvNextCustodianNumber2(EJBCommon.incrementStringNumber(adPreference.getPrfInvNextCustodianNumber2()));
				      	  	    	}else{
				      	  	    		invTag.setTgDocumentNumber(adPreference.getPrfInvNextCustodianNumber1());
				      	  	    		adPreference.setPrfInvNextCustodianNumber1(EJBCommon.incrementStringNumber(adPreference.getPrfInvNextCustodianNumber1()));
		      	  	    			}
			      	  	    	}else{
			      	  	    		System.out.println("tg document number exist?");
			      	  	    		invTag.setTgDocumentNumber(tgLstDetails.getTgDocumentNumber());
			      	  	    	}
			      	  	    	invTag.setAdUser(adUser);
			      	  	    	System.out.println("ngcreate ba?");


			      	  	    }
			      	  	}catch (Exception ex){
				      	  	if (invItemLocation.getInvItem().getIiNonInventoriable()==0 ){
		      	  	    		throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiName());
		      	  	    	}
			      	  	}
		      	  	}
	 	  	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		    				invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		    			if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
	 	  	    	}

	 	  	    }

        		Collection invAdjDistributionRecords = invAdjustment.getInvDistributionRecords();

        		i = invAdjDistributionRecords.iterator();

        		while(i.hasNext()) {

        			LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();

        			if(distributionRecord.getDrDebit() == 1) {

        				ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();

        			}

        		}

        	}

        	//Insufficient Stocks
        	if (adPreference.getPrfArCheckInsufficientStock() == EJBCommon.TRUE && !isDraft) {
        		boolean hasInsufficientItems = false;
	        	String insufficientItems = "";

	        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

	        	Iterator i = invAdjustmentLines.iterator();

	        	HashMap cstMap = new HashMap();

	        	while (i.hasNext()) {

	        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
	        	//	if(invAdjustmentLine.getAlAdjustQuantity()<0){
	        			if (invAdjustmentLine.getInvItemLocation().getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE && invAdjustmentLine.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {
	        				Collection invBillOfMaterials = invAdjustmentLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();

	        				Iterator j = invBillOfMaterials.iterator();
	        				Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N01");
	        				while (j.hasNext()) {

	        					LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

	        					LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

	        					LocalInvCosting invBomCosting = null;

	        					double ILI_QTY = this.convertByUomFromAndItemAndQuantity(
	        							invAdjustmentLine.getInvUnitOfMeasure(),
	        							invAdjustmentLine.getInvItemLocation().getInvItem(),
	        							Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
	        					Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N02");
	        					double CURR_QTY = 0;
	        					boolean isIlFound = false;

	        					if(cstMap.containsKey(invItemLocation.getIlCode().toString())){

	        						isIlFound =  true;
	        						CURR_QTY = ((Double)cstMap.get(invItemLocation.getIlCode().toString())).doubleValue();

	        					}else{
	        						try {

		        						invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
		        								invAdjustment.getAdjDate(), invBillOfMaterial.getBomIiName(),
		        								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

		        					} catch (FinderException ex) {

		        					}
	        					}


	        					LocalInvItem bomItm = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(),AD_CMPNY);


	        					double NEEDED_QTY = this.convertByUomFromAndItemAndQuantity(invBillOfMaterial.getInvUnitOfMeasure(), bomItm,
	        							invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY);
	        					Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N03");



	        					try{
	        						if(invBomCosting != null){
	        							CURR_QTY = this.convertByUomAndQuantity(invBomCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure(),
	        									invBomCosting.getInvItemLocation().getInvItem(),
	        									invBomCosting.getCstRemainingQuantity(), AD_CMPNY);
	        						}
	        					}catch (Exception e) {

	        					}


	        					if ((invBomCosting == null && isIlFound==false) || CURR_QTY == 0 || CURR_QTY < (NEEDED_QTY * ILI_QTY)) {

	        						hasInsufficientItems = true;

	        						insufficientItems += invAdjustmentLine.getInvItemLocation().getInvItem().getIiName()
	        						+ "-" + invBillOfMaterial.getBomIiName() + ", ";
	        					}

	        					CURR_QTY -= (NEEDED_QTY * ILI_QTY);

	 		                    if(!isIlFound){
	 		                    	cstMap.remove(invItemLocation.getIlCode().toString());
	 		                    }

	 		                    cstMap.put(invItemLocation.getIlCode().toString(), new Double(CURR_QTY));

	        				}
	        			} 
	        			/*
	        			 else {
	        				LocalInvCosting invCosting = null;
	        				double CURR_QTY = 0;
	        				boolean isIlFound = false;

	        				double ILI_QTY = this.convertByUomAndQuantity(
	        						invAdjustmentLine.getInvUnitOfMeasure(),
	        						invAdjustmentLine.getInvItemLocation().getInvItem(),
	        						Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);


	        				if(cstMap.containsKey(invAdjustmentLine.getInvItemLocation().getIlCode().toString())){
	 		  	   	    		isIlFound =  true;
	 		  	   	    		CURR_QTY = ((Double)cstMap.get(invAdjustmentLine.getInvItemLocation().getIlCode().toString())).doubleValue();

	        				}else{
	        					try {

	        						invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
	        								invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
	        								invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	        						CURR_QTY = invCosting.getCstRemainingQuantity();


	        					} catch (FinderException ex) {

	        					}
	        				}

	        				if(invCosting != null){

	 		  	   	    		CURR_QTY = this.convertByUomAndQuantity(
		        						invAdjustmentLine.getInvUnitOfMeasure(),
		        						invAdjustmentLine.getInvItemLocation().getInvItem(),
		        						Math.abs(invCosting.getCstRemainingQuantity()), AD_CMPNY);


	 		  	   	    	}

	        				double LOWEST_QTY = this.convertByUomAndQuantity(
	        						invAdjustmentLine.getInvUnitOfMeasure(),
	        						invAdjustmentLine.getInvItemLocation().getInvItem(),
	        						1, AD_CMPNY);
	        				if ((invCosting == null && isIlFound==false)|| CURR_QTY == 0 || CURR_QTY - ILI_QTY <= -LOWEST_QTY) {

	        					hasInsufficientItems = true;
	        					insufficientItems += invAdjustmentLine.getInvItemLocation().getInvItem().getIiName() + ", ";
	        				}

	        				CURR_QTY -= ILI_QTY;

	 		  	   	    	if(isIlFound){
	 		  	   	    		cstMap.remove(invAdjustmentLine.getInvItemLocation().getIlCode().toString());
	 		  	   	    	}

	 		  	   	    	cstMap.put(invAdjustmentLine.getInvItemLocation().getIlCode().toString(), new Double(CURR_QTY));
	        			}
	        			
	        			*/
	        	//	}

	        	}
	        	if(hasInsufficientItems) {

	        		throw new GlobalRecordInvalidException(insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
	        	}
	        }

        	LocalAdUser adUser = adUserHome.findByUsrName(invAdjustment.getAdjCreatedBy(), AD_CMPNY);

 	  	    // generate approval status

 	  	    String INV_APPRVL_STATUS = null;

		 	if (!isDraft) {

		 		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

		 		// check if ar invoice approval is enabled

		 		if (adApproval.getAprEnableInvAdjustment() == EJBCommon.FALSE) {

		 			INV_APPRVL_STATUS = "N/A";

		 		} else {
		 		
		 			LocalAdUser adUser2 = adUserHome.findByUsrName(details.getAdjLastModifiedBy(), AD_CMPNY);


				//	INV_APPRVL_STATUS = ejbIA.getApprovalStatus(adUser2.getUsrDept(), invAdjustment.getAdjLastModifiedBy(), adUser2.getUsrDescription(), "INV ADJUSTMENT", invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(), AD_BRNCH, AD_CMPNY);
					INV_APPRVL_STATUS = ejbIA.getApprovalStatus(adUser.getUsrDept(), invAdjustment.getAdjLastModifiedBy(), adUser.getUsrDescription(), "INV ADJUSTMENT", invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(), AD_BRNCH, AD_CMPNY);

		 		
		 		}


		 	invAdjustment.setAdjApprovalStatus(INV_APPRVL_STATUS);

		 	// set adjustment approval status

        	if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		System.out.println("executeInvAdjPost<------");
        		this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

        	}



		 	}

		 	return invAdjustment.getAdjCode();

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

            	getSessionContext().setRollbackOnly();
            	throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

         } catch (GlobalNoApprovalRequesterFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalNoApprovalApproverFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalInvItemLocationNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        }/* catch (GlobalInvTagExistingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        }*/ catch (GlobalInvTagMissingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (InvTagSerialNumberNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalInventoryDateException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalAccountNumberInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalExpiryDateNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalMiscInfoIsRequiredException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        }catch (GlobalRecordInvalidException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        }catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }






    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteInvAdjEntry(Integer ADJ_CODE, String AD_USR, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException {

        Debug.print("InvAdjustmentEntryControllerBean deleteInvAdjEntry");

        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home

        try {

            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalInvAdjustment invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

        	Iterator j = invAdjustmentLines.iterator();

        	while (j.hasNext()) {

        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) j.next();

        		if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

        		    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

        			invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

            	}

        	}

        	if (invAdjustment.getAdjApprovalStatus() != null && invAdjustment.getAdjApprovalStatus().equals("PENDING")) {

        		Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("INV ADJUSTMENT", invAdjustment.getAdjCode(), AD_CMPNY);

        		Iterator i = adApprovalQueues.iterator();

        		while(i.hasNext()) {

        			LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

        			adApprovalQueue.remove();

        		}

        	}

        	adDeleteAuditTrailHome.create("INV ADJUSTMENT", invAdjustment.getAdjDate(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjReferenceNumber(),
 					0d, AD_USR, new Date(), AD_CMPNY);

        	invAdjustment.remove();

        } catch (FinderException ex) {

            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("InvAdjustmentEntryControllerBean getGlFcPrecisionUnit");


        LocalAdCompanyHome adCompanyHome = null;

        // Initialize EJB Home

        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

          return  adCompany.getGlFunctionalCurrency().getFcPrecision();

        } catch (Exception ex) {

        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());

        }

     }

     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

         Debug.print("InvAdjustmentEntryControllerBean getInvGpQuantityPrecisionUnit");

         LocalAdPreferenceHome adPreferenceHome = null;

          // Initialize EJB Home

          try {

          	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

          } catch (NamingException ex) {

             throw new EJBException(ex.getMessage());

          }


          try {

             LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

             return adPreference.getPrfInvQuantityPrecisionUnit();

          } catch (Exception ex) {

             Debug.printStackTrace(ex);
             throw new EJBException(ex.getMessage());

          }

      }


      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/
       public short getInvGpCostPrecisionUnit(Integer AD_CMPNY) {

          Debug.print("InvAdjustmentEntryControllerBean getInvGpCostPrecisionUnit");

          LocalAdPreferenceHome adPreferenceHome = null;

           // Initialize EJB Home

           try {

           	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                  lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

           } catch (NamingException ex) {

              throw new EJBException(ex.getMessage());

           }


           try {

              LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

              return adPreference.getPrfInvCostPrecisionUnit();

           } catch (Exception ex) {

              Debug.printStackTrace(ex);
              throw new EJBException(ex.getMessage());

           }

       }


     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getInvGpInventoryLineNumber(Integer AD_CMPNY) {

         Debug.print("InvAdjustmentEntryControllerBean getInvGpInventoryLineNumber");

         LocalAdPreferenceHome adPreferenceHome = null;


         // Initialize EJB Home

         try {

         	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

         } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

         }


         try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfInvInventoryLineNumber();

         } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

         }

      }

      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/
       public ArrayList getAdApprovalNotifiedUsersByAdjCode(Integer ADJ_CODE, Integer AD_CMPNY) {

          Debug.print("InvAdjustmentEntryControllerBean getAdApprovalNotifiedUsersByAdjCode");


          LocalAdApprovalQueueHome adApprovalQueueHome = null;
          LocalInvAdjustmentHome invAdjustmentHome = null;

          ArrayList list = new ArrayList();


          // Initialize EJB Home

          try {

              adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
              invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
              	lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);

          } catch (NamingException ex) {

              throw new EJBException(ex.getMessage());

          }

          try {

          	LocalInvAdjustment invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

   	        if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

   	           list.add("DOCUMENT POSTED");
   	           return list;

   	        }

            Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("INV ADJUSTMENT", ADJ_CODE, AD_CMPNY);

            Iterator i = adApprovalQueues.iterator();

            while(i.hasNext()) {

            	LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

            	list.add(adApprovalQueue.getAqLevel() + " APPROVER - "+ adApprovalQueue.getAdUser().getUsrDescription());

            }

            return list;

          } catch (Exception ex) {

          	 Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

          }

     }

   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public double getInvIiUnitCostByIiNameAndUomNameAndLocNameAndDateAndQty(String II_NM, String UOM_NM, String LOC_NM, Date ADJ_DT, double quantity, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("InvAdjustmentEntryControllerBean getInvIiUnitCostByIiNameAndUomNameAndLocNameAndDateAndQty");

        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
      	LocalInvCostingHome invCostingHome = null;
      	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
      	LocalAdPreferenceHome adPreferenceHome = null;
      	 InvRepItemCostingControllerHome homeRIC = null;
         InvRepItemCostingController ejbRIC = null;
        // Initialize EJB Home

        try {

        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
        	invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
        	invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
        	homeRIC = (InvRepItemCostingControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }
        try {
        	ejbRIC = homeRIC.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {

        	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

        	double COST = invItem.getIiUnitCost();

        	// get ave cost

        	if(LOC_NM != null && ADJ_DT != null) {

        		try {

          			LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);

          			LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
          					ADJ_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);



          			if(invCosting.getCstRemainingQuantity() <= 0 && invCosting.getCstRemainingValue() <=0){
                    	System.out.println("RE CALC");
                    	HashMap criteria = new HashMap();
                        criteria.put("itemName", invItemLocation.getInvItem().getIiName());
                        criteria.put("location", invItemLocation.getInvLocation().getLocName());

                        ArrayList branchList = new ArrayList();

                        AdBranchDetails mdetails = new AdBranchDetails();
    					mdetails.setBrCode(AD_BRNCH);
    					branchList.add(mdetails);

                        ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

                        invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
              					ADJ_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
          			}






          			if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
          			{
          				System.out.println("remaining value : "+ Math.abs(invCosting.getCstRemainingValue() ));
          				
          				System.out.println("remaining value : "+ invCosting.getCstRemainingQuantity() );
          				
          				if( invCosting.getCstRemainingQuantity()<=0) {
          					COST = EJBCommon.roundIt(invItemLocation.getInvItem().getIiUnitCost(), adPreference.getPrfInvCostPrecisionUnit());
          				}else {
          					 COST = invCosting.getCstRemainingQuantity() <= 0 ? COST :
                            Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());


							if(COST<=0){
                               COST = invItemLocation.getInvItem().getIiUnitCost();
                            }	
          				}
          				
          				
          			
          				
          				
          				System.out.println("cost is  value : "+ COST);
          			}
          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
          			{
          				System.out.println("invCosting.getCstAdjustQuantity()" + quantity);
          				COST = this.getInvFifoCost(ADJ_DT, invItemLocation.getIlCode(), quantity,
          						invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
         			}
          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
          			{
          				COST = invItemLocation.getInvItem().getIiUnitCost();
         			}

          		} catch (FinderException ex) {

          		}

        	}

      		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvCostPrecisionUnit());

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }

     // private methods

     private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST,
    		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
 	 {

 		 LocalInvCostingHome invCostingHome = null;
 	  	 LocalInvItemLocationHome invItemLocationHome = null;
 	  	 LocalAdPreferenceHome adPreferenceHome = null;

 	     // Initialize EJB Home

 	     try {

 	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
 	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
 	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
 	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
 	    	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
 	     }
 	     catch (NamingException ex) {

 	    	 throw new EJBException(ex.getMessage());
 	     }

 		try {

 			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode2(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
 			System.out.println("TEST " + CST_DT + " " + IL_CODE + " " + CST_QTY + " " + CST_COST + " " + AD_BRNCH + " " + AD_CMPNY);
 			System.out.println("invFifoCostings size " + invFifoCostings.size());
 			if (invFifoCostings.size() > 0) {
 				double fifoCost = 0;
 			    double runningQty = Math.abs(CST_QTY);
 			   System.out.println("Start ----:" + CST_QTY );
 				Iterator x = invFifoCostings.iterator();
 				while (x.hasNext()) {

 					LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
 	  				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
 	  				double newRemainingLifoQuantity = 0;
 	  				if (invFifoCosting.getCstRemainingLifoQuantity() <= runningQty) {
 	  					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
 	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * invFifoCosting.getCstRemainingLifoQuantity();
 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
 		 	  				fifoCost  += invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold() * invFifoCosting.getCstRemainingLifoQuantity();
 	 	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
 	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * invFifoCosting.getCstRemainingLifoQuantity();
 	 	 	  			} else {
 	 	 	  				fifoCost += invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity() * invFifoCosting.getCstRemainingLifoQuantity();
 	 	 	  			}
 	  					runningQty = runningQty - invFifoCosting.getCstRemainingLifoQuantity();
 	  					//System.out.println("runningQty ----:" + runningQty );
 	  					double xxxxx=Math.abs(CST_QTY)-runningQty;
 	  					System.out.println("Unit ----:" + xxxxx );
 	  					System.out.println("Cost ---:" + fifoCost);
 	  				} else {
 	  					System.out.println("CostForward ---:" + fifoCost);
 	  					double fifoCost2=fifoCost;
 	  					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
 	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * runningQty;
 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
 		 	  				fifoCost  += invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold() * runningQty;
 	 	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
 	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * runningQty;
 	 	 	  			} else {
 	 	 	  				fifoCost += invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity() * runningQty;
 	 	 	  			}
 	  					newRemainingLifoQuantity = invFifoCosting.getCstRemainingLifoQuantity() - runningQty;

 	  					fifoCost2=fifoCost -fifoCost2;
 	  					System.out.println("Unit ----:" + runningQty);
 	  					System.out.println("Cost ---:" + fifoCost2);
 	  					runningQty = 0;
 	  				}
 	 	  			if (isAdjustFifo) invFifoCosting.setCstRemainingLifoQuantity(newRemainingLifoQuantity);
 					if (runningQty <=0) break;
 				}
 				System.out.println("fifoCost" + fifoCost);
 				System.out.println("CST_QTY" + CST_QTY);
 				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
 				return EJBCommon.roundIt(fifoCost / CST_QTY, adPreference.getPrfInvCostPrecisionUnit());

 			}
 			else {

 				//most applicable in 1st entries of data
 				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
 				return invItemLocation.getInvItem().getIiUnitCost();
 			}

 		}
 		catch (Exception ex) {
 			Debug.printStackTrace(ex);
 		    throw new EJBException(ex.getMessage());
 		}
 	}



     private LocalInvAdjustmentLine addInvAlEntry(InvModAdjustmentLineDetails mdetails,
    	LocalInvAdjustment invAdjustment, byte AL_VD, Integer AD_CMPNY) throws
    	GlobalMiscInfoIsRequiredException{

		Debug.print("InvAdjustmentEntryControllerBean addInvAlEntry");

		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;

	    // Initialize EJB Home

	    try {

	    	invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
	    	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
	    	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	    	LocalInvAdjustmentLine invAdjustmentLine = invAdjustmentLineHome.create(mdetails.getAlUnitCost(),
	    			mdetails.getAlQcNumber(), mdetails.getAlQcExpiryDate(),mdetails.getAlAdjustQuantity(),0, AL_VD, AD_CMPNY);

	    	//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
	    	invAdjustmentLine.setInvAdjustment(invAdjustment);
	    	System.out.println("UON Name="+mdetails.getAlUomName());
	    	LocalInvUnitOfMeasure invUnitOfMeasure =
	    		invUnitOfMeasureHome.findByUomName(mdetails.getAlUomName(), AD_CMPNY);
	    	//invUnitOfMeasure.addInvAdjustmentLine(invAdjustmentLine);
	    	invAdjustmentLine.setInvUnitOfMeasure(invUnitOfMeasure);

	    	LocalInvItemLocation invItemLocation =
	    		invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(),
	    			mdetails.getAlIiName(), AD_CMPNY);
	    	//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
	    	invAdjustmentLine.setInvItemLocation(invItemLocation);

//	    	validate misc


	    	if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()==1){


	    		this.createInvTagList(invAdjustmentLine, mdetails.getAlTagList(), AD_CMPNY);




	    	}





	    	/*
	    	if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()==1){

	    		if(mdetails.getAlMisc()==null || mdetails.getAlMisc()==""){


	    			throw new GlobalMiscInfoIsRequiredException();

	    		}else{
	    			int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(mdetails.getAlMisc()));

	    			String miscList2Prpgt = this.checkExpiryDates(mdetails.getAlMisc(), qty2Prpgt, "False");
	    			if(miscList2Prpgt!="Error"){
	    				invAdjustmentLine.setAlMisc(mdetails.getAlMisc());
	    			}else{
	    				throw new GlobalMiscInfoIsRequiredException();
	    			}

	    		}
	    	}else{
	    		invAdjustmentLine.setAlMisc(mdetails.getAlMisc());
	    	}

	    	try{

	    	}catch (GlobalMiscInfoIsRequiredException ex){
	    		if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()==1){
	    			if(mdetails.getAlMisc()!=""){
	    				invAdjustmentLine.setAlMisc(mdetails.getAlMisc());
	    			}

		    	}
	    		//getSessionContext().setRollbackOnly();
		    	throw ex;
	    	}*/


            System.out.println("mdetails.getPlMisc() : "+mdetails.getAlMisc());

	     	return invAdjustmentLine;

	    }/*catch (GlobalMiscInfoIsRequiredException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } */catch (Exception ex) {

	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());

	    }

	}

    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)

    throws GlobalBranchAccountNumberInvalidException {

		Debug.print("InvAdjustmentEntryControllerBean addInvDrEntry");

		LocalAdCompanyHome adCompanyHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;

        // Initialize EJB Home

        try {

        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// get company

        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		// validate coa

    		LocalGlChartOfAccount glChartOfAccount = null;

    		try {

    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);


    		} catch(FinderException ex) {

    			throw new GlobalBranchAccountNumberInvalidException ();

    		}

        	// create distribution record

		    LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
			    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    DR_RVRSL, EJBCommon.FALSE, AD_CMPNY);

		    //invAdjustment.addInvDistributionRecord(invDistributionRecord);
		    invDistributionRecord.setInvAdjustment(invAdjustment);
			//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
			invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

        } catch(GlobalBranchAccountNumberInvalidException ex) {

			throw new GlobalBranchAccountNumberInvalidException ();


        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

    }

    private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

    	Debug.print("InvAdjustmentEntryControllerBean convertByUomFromAndUomToAndQuantity");

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

    		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
    		LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

    		return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvCostPrecisionUnit());

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }


	private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

		Debug.print("InvAdjustmentEntryControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvCostPrecisionUnit());

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

	 private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
		GlobalTransactionAlreadyPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException,
		GlobalExpiryDateNotFoundException,
		GlobalMiscInfoIsRequiredException{

        Debug.print("InvAdjustmentEntryControllerBean executeInvAdjPost");

        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;


        // Initialize EJB Home

        try {

            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	// validate if adjustment is already deleted

        	LocalInvAdjustment invAdjustment = null;

        	try {

        		invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

        	} catch (FinderException ex) {

        		throw new GlobalRecordAlreadyDeletedException();

        	}

        	// validate if adjustment is already posted or void

        	if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
        		throw new GlobalTransactionAlreadyPostedException();

            }else if (invAdjustment.getAdjVoid() == EJBCommon.TRUE) {

    			throw new GlobalTransactionAlreadyVoidException();

    		}
        

			 if (invAdjustment.getAdjVoid() == EJBCommon.FALSE && invAdjustment.getAdjPosted() == EJBCommon.FALSE) {
			 
			 
			 
			 
			 }else if (invAdjustment.getAdjVoid() == EJBCommon.TRUE && invAdjustment.getAdjPosted() == EJBCommon.FALSE) {// void adj
			 
			 
			 	this.regenerateInventoryDr(invAdjustment, AD_BRNCH, AD_CMPNY);
			 
		
			 
			 
			 }








        	// regenerate inventory dr

    

    		Collection invAdjustmentLines = null;

    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE){
    			System.out.println("get not voided lines");
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		}
    		else{
    			System.out.println("get voided lines");
    		//	invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);
    			
    			invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			
    			
    		}
    	System.out.println("adj lines : " + invAdjustmentLines.size());
            Iterator c = invAdjustmentLines.iterator();

			while(c.hasNext()) {
				
				System.out.println("void line exec");
				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) c.next();

				String II_NM = invAdjustmentLine.getInvItemLocation().getInvItem().getIiName();
				String LOC_NM = invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName();


				double ADJUST_QTY = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(),
				  invAdjustmentLine.getInvItemLocation().getInvItem(), invAdjustmentLine.getAlAdjustQuantity(), AD_CMPNY);

				LocalInvCosting invCosting = null;

				try {

					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invAdjustment.getAdjDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

				}

				double COST = invAdjustmentLine.getAlUnitCost();

				if (invCosting == null ) {

					if(invAdjustment.getAdjVoid() == EJBCommon.FALSE){
		    			this.postToInv(invAdjustmentLine, invAdjustment.getAdjDate(),
								ADJUST_QTY, COST * ADJUST_QTY,
								ADJUST_QTY, COST * ADJUST_QTY,
								0d, null, AD_BRNCH, AD_CMPNY);
		    		}
		    		else{
		    			this.postToInv(invAdjustmentLine, invAdjustment.getAdjDate(),
								-ADJUST_QTY, -COST * ADJUST_QTY,
								-ADJUST_QTY, -COST * ADJUST_QTY,
								0d, null, AD_BRNCH, AD_CMPNY);
		    		}



					this.postToInv(invAdjustmentLine, invAdjustment.getAdjDate(),
								ADJUST_QTY, COST * ADJUST_QTY,
								ADJUST_QTY, COST * ADJUST_QTY,
								0d, null, AD_BRNCH, AD_CMPNY);

				} else {

				
					if(invAdjustment.getAdjVoid() == EJBCommon.FALSE){
					
						
		    			this.postToInv(invAdjustmentLine, invAdjustment.getAdjDate(),
							ADJUST_QTY, COST* ADJUST_QTY,
							invCosting.getCstRemainingQuantity() + ADJUST_QTY, invCosting.getCstRemainingValue() + (COST * ADJUST_QTY),
							0d, null, AD_BRNCH, AD_CMPNY);
		    		}
		    		else{
		    		System.out.println("void costing ");
		    			this.postToInv(invAdjustmentLine, invAdjustment.getAdjDate(),
							-ADJUST_QTY, -COST* ADJUST_QTY,
							invCosting.getCstRemainingQuantity() - ADJUST_QTY, invCosting.getCstRemainingValue() - (COST * ADJUST_QTY),
							0d, null, AD_BRNCH, AD_CMPNY);
		    		}


				}


			}


           invAdjustment.setAdjPosted(EJBCommon.TRUE);
           invAdjustment.setAdjPostedBy(USR_NM);
           invAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

           // post to gl if necessary

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

           if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL") || invAdjustment.getAdjIsCostVariance() == EJBCommon.TRUE) {

           	   // validate if date has no period and period is closed

           	   LocalGlSetOfBook glJournalSetOfBook = null;

           	   try {

           	   	   glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

           	   } catch (FinderException ex) {

           	       throw new GlJREffectiveDateNoPeriodExistException();

           	   }

			   LocalGlAccountingCalendarValue glAccountingCalendarValue =
			        glAccountingCalendarValueHome.findByAcCodeAndDate(
			        	glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);


			   if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
			        glAccountingCalendarValue.getAcvStatus() == 'C' ||
			        glAccountingCalendarValue.getAcvStatus() == 'P') {

			        throw new GlJREffectiveDatePeriodClosedException();

			   }

			   // check if invoice is balance if not check suspense posting

	           LocalGlJournalLine glOffsetJournalLine = null;

   			Collection invDistributionRecords = null;

   			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

   				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
   						invAdjustment.getAdjCode(), AD_CMPNY);

   			} else {

   				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
   						invAdjustment.getAdjCode(), AD_CMPNY);

   			}


	           Iterator j = invDistributionRecords.iterator();

	           double TOTAL_DEBIT = 0d;
	           double TOTAL_CREDIT = 0d;

	           while (j.hasNext()) {

	        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

	        		double DR_AMNT = 0d;

	        		DR_AMNT = invDistributionRecord.getDrAmount();

	        		if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
	        			System.out.println("DEBIT= "+invDistributionRecord.getDrAmount());
	        			TOTAL_DEBIT += DR_AMNT;

	        		} else {
	        			System.out.println("CREDIT= "+invDistributionRecord.getDrAmount());
	        			TOTAL_CREDIT += DR_AMNT;

	        		}

	        	}
	           System.out.println("1 TOTAL_DEBIT="+TOTAL_DEBIT);
			    System.out.println("1 TOTAL_CREDIT="+TOTAL_CREDIT);

	        	TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
	        	TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
	        	    System.out.println("(Math.abs(EJBCommon.roundIt(TOTAL_DEBIT - TOTAL_CREDIT,adCompany.getGlFunctionalCurrency().getFcPrecision()))="+Math.abs(EJBCommon.roundIt(TOTAL_DEBIT - TOTAL_CREDIT,adCompany.getGlFunctionalCurrency().getFcPrecision())));
	        	if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
	        	    TOTAL_DEBIT != TOTAL_CREDIT) {

	        	    LocalGlSuspenseAccount glSuspenseAccount = null;

	        	    try {

	        	        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS", AD_CMPNY);

	        	    } catch (FinderException ex) {

	        	    	throw new GlobalJournalNotBalanceException();

	        	    }

	        	    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

	        	    	glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE,
	        	    	    TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

	        	    } else {

	        	    	glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
	        	    	    TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

	        	    }

	        	    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
	        	    //glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
	        	    glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


				} else if (Math.abs(EJBCommon.roundIt(TOTAL_DEBIT - TOTAL_CREDIT,adCompany.getGlFunctionalCurrency().getFcPrecision())) == 0.01 ||
						(adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
						    TOTAL_DEBIT != TOTAL_CREDIT)) {
				    System.out.println("2 TOTAL_DEBIT="+TOTAL_DEBIT);
				    System.out.println("2 TOTAL_CREDIT="+TOTAL_CREDIT);
					throw new GlobalJournalNotBalanceException();

				}

		       // create journal batch if necessary

		       LocalGlJournalBatch glJournalBatch = null;
		       java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

		       try {

   	   	   			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

		       } catch (FinderException ex) {

		       }

		       if (glJournalBatch == null) {

	     		    glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

		       }

			   // create journal entry

        	   LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
        		    invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
        		    0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
        		    'N', EJBCommon.TRUE, EJBCommon.FALSE,
        		    USR_NM, new Date(),
        		    USR_NM, new Date(),
        		    null, null,
        		    USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
        		    null, null, EJBCommon.FALSE, null,
        		    AD_BRNCH, AD_CMPNY);

        	   LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
	           glJournal.setGlJournalSource(glJournalSource);

	           LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
	           glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

	           LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
	           glJournal.setGlJournalCategory(glJournalCategory);

        	   if (glJournalBatch != null) {

        		   glJournal.setGlJournalBatch(glJournalBatch);

        	   }
               // create journal lines

               j = invDistributionRecords.iterator();

               while (j.hasNext()) {

            		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

            		double DR_AMNT = 0d;

            		DR_AMNT = invDistributionRecord.getDrAmount();

            		LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),
            			invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

                    //invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
                    glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());

            	    //glJournal.addGlJournalLine(glJournalLine);
            	    glJournalLine.setGlJournal(glJournal);

            	    invDistributionRecord.setDrImported(EJBCommon.TRUE);


               }

               if (glOffsetJournalLine != null) {

            	   //glJournal.addGlJournalLine(glOffsetJournalLine);
            	   glOffsetJournalLine.setGlJournal(glJournal);

               }

               // post journal to gl

               Collection glJournalLines = glJournal.getGlJournalLines();

		       Iterator i = glJournalLines.iterator();

		       while (i.hasNext()) {

		           LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

		           // post current to current acv

		           this.postToGl(glAccountingCalendarValue,
		               glJournalLine.getGlChartOfAccount(),
		               true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


		           // post to subsequent acvs (propagate)

		           Collection glSubsequentAccountingCalendarValues =
		               glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
		                   glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
		                   glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

		           Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

		           while (acvsIter.hasNext()) {

		         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
		         	       (LocalGlAccountingCalendarValue)acvsIter.next();

		         	   this.postToGl(glSubsequentAccountingCalendarValue,
		         	       glJournalLine.getGlChartOfAccount(),
		         	       false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

		           }

		           // post to subsequent years if necessary

		           Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

			  	  	if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

				  	  	adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
				  	  	LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY);

				  	  	Iterator sobIter = glSubsequentSetOfBooks.iterator();

				  	  	while (sobIter.hasNext()) {

				  	  		LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

				  	  		String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

				  	  		// post to subsequent acvs of subsequent set of book(propagate)

				           Collection glAccountingCalendarValues =
				               glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

				           Iterator acvIter = glAccountingCalendarValues.iterator();

				           while (acvIter.hasNext()) {

				         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
				         	       (LocalGlAccountingCalendarValue)acvIter.next();

				         	    if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
					 				ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

						         	this.postToGl(glSubsequentAccountingCalendarValue,
						         	     glJournalLine.getGlChartOfAccount(),
						         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

						        } else { // revenue & expense

						             this.postToGl(glSubsequentAccountingCalendarValue,
						         	     glRetainedEarningsAccount,
						         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

						        }

				           }

				  	  	   if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

				  	  	}

			  	  	}

	           }

	        }

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch(GlobalBranchAccountNumberInvalidException ex) {

			throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (GlobalExpiryDateNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void postToInv(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY, double CST_ADJST_CST,
    		double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH,Integer AD_CMPNY) throws
    		AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {

    	Debug.print("InvAdjustmentEntryControllerBean postToInv");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

        // Initialize EJB Home

        try {

          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	          lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

        } catch (NamingException ex) {

          throw new EJBException(ex.getMessage());

        }

        try {

           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
           int CST_LN_NMBR = 0;

           CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adPreference.getPrfInvCostPrecisionUnit());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adPreference.getPrfInvCostPrecisionUnit());

           if (CST_ADJST_QTY < 0) {

           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

           }


           try {
        	   System.out.println("generate line number");
           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
               System.out.println("generate line number: " + CST_LN_NMBR);
           } catch (FinderException ex) {
        	   System.out.println("generate line number CATCH");
           	   CST_LN_NMBR = 1;

           }

           if(CST_VRNC_VL != 0) {

            //void subsequent cost variance adjustments
            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
            Iterator i = invAdjustmentLines.iterator();

            while (i.hasNext()){

            	LocalInvAdjustmentLine invAdjustmentLineTemp = (LocalInvAdjustmentLine) i.next();
            	this.voidInvAdjustment(invAdjustmentLineTemp.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

            }

           }

           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           try {
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   System.out.println(prevCst.getCstCode()+"   "+ prevCst.getCstRemainingQuantity());
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();
        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }
        	   System.out.println("prevExpiryDates: " + prevExpiryDates);
        	   System.out.println("qtyPrpgt: " + qtyPrpgt);
           }catch (Exception ex){
        	   System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
        	   prevExpiryDates="";
           }

           //create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
           invCosting.setCstQCNumber(invAdjustmentLine.getAlQcNumber());
           invCosting.setCstQCExpiryDate(invAdjustmentLine.getAlQcExpiryDate());
           //invItemLocation.addInvCosting(invCosting);
           invCosting.setInvItemLocation(invItemLocation);
           invCosting.setInvAdjustmentLine(invAdjustmentLine);

           //         Get Latest Expiry Dates

      
           


			// if cost variance is not 0, generate cost variance for the transaction
			if(CST_VRNC_VL != 0) {

				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"INVADJ" + invAdjustmentLine.getInvAdjustment().getAdjDocumentNumber(),
						invAdjustmentLine.getInvAdjustment().getAdjDescription(),
						invAdjustmentLine.getInvAdjustment().getAdjDate(), USR_NM, AD_BRNCH, AD_CMPNY);

			}

           // propagate balance if necessary
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

           Iterator i = invCostings.iterator();

           String miscList = "";
           ArrayList miscList2 = null;
           //double qty = 0d;


           System.out.println("miscList Propagate:" + miscList);
           String propagateMisc ="";
		   String ret = "";


           while (i.hasNext()) {
        	   String Checker = "";
        	   String Checker2 = "";

        	   LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

        	   invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
        	   invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
        	   if (CST_ADJST_QTY > 0) {
        		   invPropagatedCosting.setCstRemainingLifoQuantity(invPropagatedCosting.getCstRemainingLifoQuantity() + CST_ADJST_QTY);
        	   }
        	   System.out.println("invPropagatedCosting.getCstExpiryDate() : " + invPropagatedCosting.getCstExpiryDate());
        	   if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
        		   /*
        		   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
        			   double qty = Double.parseDouble(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
        			   //invPropagatedCosting.getInvAdjustmentLine().getAlMisc();
        			   miscList = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), qty, "False");
        			   miscList2 = this.expiryDates(invAdjustmentLine.getAlMisc(), qty);
        			   System.out.println("invAdjustmentLine.getAlMisc(): "+invAdjustmentLine.getAlMisc());
        			   System.out.println("getAlAdjustQuantity(): "+invAdjustmentLine.getAlAdjustQuantity());

        			   if(invAdjustmentLine.getAlAdjustQuantity()<0){
        				   Iterator mi = miscList2.iterator();

        				   propagateMisc = invPropagatedCosting.getCstExpiryDate();
        				   ret = invPropagatedCosting.getCstExpiryDate();
        				   while(mi.hasNext()){
        					   String miscStr = (String)mi.next();

        					   Integer qTest = this.checkExpiryDates(ret+"fin$");
        					   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

        					   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
        					   System.out.println("ret: " + ret);
        					   Iterator m2 = miscList3.iterator();
        					   ret = "";
        					   String ret2 = "false";
        					   int a = 0;
        					   while(m2.hasNext()){
        						   String miscStr2 = (String)m2.next();

        						   if(ret2=="1st"){
        							   ret2 = "false";
        						   }
        						   System.out.println("2 miscStr: "+miscStr);
        						   System.out.println("2 miscStr2: "+miscStr2);
        						   if(miscStr2.equals(miscStr)){
        							   if(a==0){
        								   a = 1;
        								   ret2 = "1st";
        								   Checker2 = "true";
        							   }else{
        								   a = a+1;
        								   ret2 = "true";
        							   }
        						   }
        						   System.out.println("Checker: "+Checker2);
        						   if(!miscStr2.equals(miscStr) || a>1){
        							   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
        								   if (miscStr2!=""){
        									   miscStr2 = "$" + miscStr2;
        									   ret = ret + miscStr2;
        									   ret2 = "false";
        								   }
        							   }
        						   }

        					   }
        					   if(Checker2!="true"){
        						   throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
        					   }else{
        						   System.out.println("TAE");
        					   }

        					   ret = ret + "$";
        					   qtyPrpgt= qtyPrpgt -1;
        				   }
        			   }

        		   }

        		   System.out.println("getAlAdjustQuantity(): "+invAdjustmentLine.getAlAdjustQuantity());
        		   if(invAdjustmentLine.getAlAdjustQuantity()<0 && miscList2 != null){

        			   
        			   
        			   Iterator mi = miscList2.iterator();

        			   propagateMisc = invPropagatedCosting.getCstExpiryDate();
        			   ret = propagateMisc;
        			   while(mi.hasNext()){
        				   String miscStr = (String)mi.next();

        				   Integer qTest = this.checkExpiryDates(ret+"fin$");
        				   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

        				   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
        				   System.out.println("ret: " + ret);
        				   Iterator m2 = miscList3.iterator();
        				   ret = "";
        				   String ret2 = "false";
        				   int a = 0;
        				   while(m2.hasNext()){
        					   String miscStr2 = (String)m2.next();

        					   if(ret2=="1st"){
        						   ret2 = "false";
        					   }
        					   System.out.println("2 miscStr: "+miscStr);
        					   System.out.println("2 miscStr2: "+miscStr2);
        					   if(miscStr2.equals(miscStr)){
        						   if(a==0){
        							   a = 1;
        							   ret2 = "1st";
        							   Checker = "true";
        						   }else{
        							   a = a+1;
        							   ret2 = "true";
        						   }
        					   }
        					   System.out.println("Checker: "+Checker);
        					   if(!miscStr2.equals(miscStr) || a>1){
        						   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
        							   if (miscStr2!=""){
        								   miscStr2 = "$" + miscStr2;
        								   ret = ret + miscStr2;
        								   ret2 = "false";
        							   }
        						   }
        					   }

        				   }
        				   ret = ret + "$";
        				   qtyPrpgt= qtyPrpgt -1;
        			   }
        			   propagateMisc = ret;
        		   }else{
        			   propagateMisc = miscList+invPropagatedCosting.getCstExpiryDate().substring(1, invPropagatedCosting.getCstExpiryDate().length());
        		   }

        		   invPropagatedCosting.setCstExpiryDate(propagateMisc);
        		   */
        	   }


           }

           // regenerate cost variance
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

           	throw ex;

       // } catch (GlobalExpiryDateNotFoundException ex){

     //      	throw ex;

        } catch (Exception ex) {

      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());

        }

    }

    public static int checkExpiryDates(String misc) throws Exception {

		String separator ="$";

		// Remove first $ character
		misc = misc.substring(1);
		//System.out.println("misc: " + misc);
		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;
		int numberExpry=0;
		String miscList = new String();
		String miscList2 = "";
		String g= "";
		try{
			while(g!="fin"){
				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				g= misc.substring(start, start + length);
				if(g.length()!=0){
					if(g!=null || g!="" || g!="null"){
						if(g.contains("null")){
							miscList2 = "Error";
						}else{
							miscList = miscList + "$" + g;
							numberExpry++;
						}
					}else{
						miscList2 = "Error";
					}

				}else{
					miscList2 = "Error";
				}
			}
		}catch(Exception e){

		}

		if(miscList2==""){
			miscList = miscList+"$";
		}else{
			miscList = miscList2;
		}

		return (numberExpry);
	}

    public String getQuantityExpiryDates(String qntty){
    	String separator = "$";

    	// Remove first $ character
    	qntty = qntty.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = qntty.indexOf(separator, start);
    	int length = nextIndex - start;
    	String y;
    	y = (qntty.substring(start, start + length));
    	System.out.println("Y " + y);

    	return y;
    }

    private ArrayList expiryDates(String misc, double qty) throws Exception{
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	String separator ="$";


    	// Remove first $ character
    	misc = misc.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;

    	System.out.println("qty" + qty);
    	ArrayList miscList = new ArrayList();

    	for(int x=0; x<qty; x++) {

    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;

    		String checker = misc.substring(start, start + length);
    		if(checker.length()!=0 || checker!="null"){
    			miscList.add(checker);
    		}else{
    			miscList.add("null");
    			qty++;
    		}
    	}

    	System.out.println("miscList :" + miscList);
    	return miscList;
    }

    public String propagateExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);

    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}

    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;

    	String miscList = new String();

    	for(int x=0; x<qty; x++) {

    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		String g= misc.substring(start, start + length);
    		System.out.println("g: " + g);
    		System.out.println("g length: " + g.length());
    		if(g.length()!=0){
    			miscList = miscList + "$" + g;
    			System.out.println("miscList G: " + miscList);
    		}
    	}

    	miscList = miscList+"$";
    	System.out.println("miscList :" + miscList);
    	return (miscList);
    }

    public String checkExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);

    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}

    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;

    	String miscList = new String();
    	String miscList2 = "";

    	for(int x=0; x<qty; x++) {

    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		String g= misc.substring(start, start + length);
    		System.out.println("g: " + g);
    		System.out.println("g length: " + g.length());
    		if(g.length()!=0){
    			if(g!=null || g!="" || g!="null"){
    				if(g.contains("null")){
    					miscList2 = "Error";
    				}else{
    					miscList = miscList + "$" + g;
    				}
    			}else{
    				miscList2 = "Error";
    			}

    			System.out.println("miscList G: " + miscList);
    		}else{
    			System.out.println("KABOOM");
    			miscList2 = "Error";
    		}
    	}
    	System.out.println("miscList2 :" + miscList2);
    	if(miscList2==""){
    		miscList = miscList+"$";
    	}else{
    		miscList = miscList2;
    	}

    	System.out.println("miscList :" + miscList);
    	return (miscList);
    }

    private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
    		LocalGlChartOfAccount glChartOfAccount,
			boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

    	Debug.print("InvAdjustmentEntryControllerBean postToGl");

    	LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;


    	// Initialize EJB Home

    	try {

    		glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		LocalGlChartOfAccountBalance glChartOfAccountBalance =
    			glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
    					glAccountingCalendarValue.getAcvCode(),
						glChartOfAccount.getCoaCode(), AD_CMPNY);

    		String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
    		short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcExtendedPrecision();



    		if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
    				isDebit == EJBCommon.TRUE) ||
					(!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
							isDebit == EJBCommon.FALSE)) {

    			glChartOfAccountBalance.setCoabEndingBalance(
    					EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

    			if (!isCurrentAcv) {

    				glChartOfAccountBalance.setCoabBeginningBalance(
    						EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

    			}


    		} else {

    			glChartOfAccountBalance.setCoabEndingBalance(
    					EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

    			if (!isCurrentAcv) {

    				glChartOfAccountBalance.setCoabBeginningBalance(
    						EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

    			}

    		}

    		if (isCurrentAcv) {

    			if (isDebit == EJBCommon.TRUE) {

    				glChartOfAccountBalance.setCoabTotalDebit(
    						EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

    			} else {

    				glChartOfAccountBalance.setCoabTotalCredit(
    						EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
    			}

    		}

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}


    }



    private void regenerateInventoryDr(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException {

    	Debug.print("InvAdjustmentEntryControllerBean regenerateInventoryDr");

    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	InvRepItemCostingControllerHome homeRIC = null;
        InvRepItemCostingController ejbRIC = null;
    	// Initialize EJB Home

    	try {

    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			homeRIC = (InvRepItemCostingControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {
        	ejbRIC = homeRIC.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {

        	// regenerate inventory distribution records

            // remove all inventory distribution

    		Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByAdjCode(
    			invAdjustment.getAdjCode(), AD_CMPNY);

    		Iterator i = invDistributionRecords.iterator();

    		while (i.hasNext()) {

    			LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    			i.remove();
    			invDistributionRecord.remove();

    		}

        	// remove all adjustment lines committed qty

    		Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

        	i = invAdjustmentLines.iterator();

        	while (i.hasNext()) {

        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

        		if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

        		    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

        			invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

        		}

        	}

    		invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

    		if(invAdjustmentLines != null && !invAdjustmentLines.isEmpty()) {

    			byte DEBIT = 0;
    			double TOTAL_AMOUNT = 0d;

    			i = invAdjustmentLines.iterator();

    			while(i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
    				LocalInvItemLocation invItemLocation=invAdjustmentLine.getInvItemLocation();

    				// start date validation
    				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    				if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	    				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	    					invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	    				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
    				}

    				// add physical inventory distribution

    				double AMOUNT = 0d;

    				if (invAdjustmentLine.getAlAdjustQuantity() > 0 && !invAdjustmentLine.getInvAdjustment().getAdjType().equals("ISSUANCE")) {

    					AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() *
    							invAdjustmentLine.getAlUnitCost(), adPreference.getPrfInvCostPrecisionUnit());
    					DEBIT = EJBCommon.TRUE;

    				} else {


						AMOUNT = EJBCommon.roundIt(
			 	  	    			invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(),
									this.getGlFcPrecisionUnit(AD_CMPNY));

						invAdjustmentLine.setAlUnitCost(AMOUNT);

						DEBIT = EJBCommon.FALSE;
						/*
        				double COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();

        				try {

        					LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
        						invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);


						    if(invCosting.getCstRemainingQuantity() <= 0 && invCosting.getCstRemainingValue() <=0){
	                        	System.out.println("RE CALC");
	                        	HashMap criteria = new HashMap();
	                            criteria.put("itemName", invItemLocation.getInvItem().getIiName());
	                            criteria.put("location", invItemLocation.getInvLocation().getLocName());

	                            ArrayList branchList = new ArrayList();

	                            AdBranchDetails mdetails = new AdBranchDetails();
	        					mdetails.setBrCode(AD_BRNCH);
	        					branchList.add(mdetails);

	                            ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

	                            invCosting  =invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
	            						invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						    }

	                        
        					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

     	  	    				COST = invCosting.getCstRemainingQuantity() <= 0 ? COST :
     	  	    					Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

								if(COST<=0){
                               		COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
                           		 }	

     	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

     	  	    				COST =  invCosting.getCstRemainingQuantity() == 0 ? COST :
     	  	    					Math.abs(this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
     	  	    						invAdjustmentLine.getAlAdjustQuantity(), invAdjustmentLine.getAlUnitCost(), false, AD_BRNCH, AD_CMPNY));

     	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))

     	  	    				COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

        				} catch (FinderException ex) { }

    					COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
		 	  	        		invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
								COST, true, AD_CMPNY);

    					AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() * COST,
    							adPreference.getPrfInvCostPrecisionUnit());
    					//AMOUNT = invAdjustmentLine.getAlAdjustQuantity() * COST;
    					DEBIT = EJBCommon.FALSE;
						*/
    				}

    				// check for branch mapping

    				LocalAdBranchItemLocation adBranchItemLocation = null;

    				try{

	 	  	    		adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
	 	  	    				invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

	 	  	    	} catch (FinderException ex) {

	 	  	    		throw new GlobalBranchAccountNumberInvalidException ();

	 	  	    	}

    				LocalGlChartOfAccount glInventoryChartOfAccount = null;

    				if (adBranchItemLocation == null) {

    					glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    							invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    				} else {

    					glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    							adBranchItemLocation.getBilCoaGlInventoryAccount());

    				}

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(AMOUNT), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

    				TOTAL_AMOUNT += AMOUNT;
    				//ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);

    				// add adjust quantity to item location committed
    				// quantity if negative

    				if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

    					double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
    							invAdjustmentLine.getInvUnitOfMeasure(),
								invAdjustmentLine.getInvItemLocation().getInvItem(),
								Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

    					invItemLocation = invAdjustmentLine.getInvItemLocation();

    					invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() +
    							convertedQuantity);

    				}

    			}

    			// add variance or transfer/debit distribution

    			DEBIT = (TOTAL_AMOUNT >= 0 && !invAdjustment.getAdjType().equals("ISSUANCE")) ? EJBCommon.FALSE : EJBCommon.TRUE;

    			this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_AMOUNT), EJBCommon.FALSE,
    					invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

    		}

    	} catch (GlobalInventoryDateException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch(GlobalBranchAccountNumberInvalidException ex) {

			throw new GlobalBranchAccountNumberInvalidException ();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {

		Debug.print("InvAdjustmentEntryControllerBean convertCostByUom");

		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

	    // Initialize EJB Home

	    try {

	        invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
	        invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

	    } catch (NamingException ex) {

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	    	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

	        if (isFromDefault) {

	        	return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();

	        } else {

	        	return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();

	        }



	    } catch (Exception ex) {

	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());

	    }

	}



	private void executeVoidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ApVoucherPostController voidInvAdjustment");

    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();

    			Iterator i = invDistributionRecords.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				list.add(invDistributionRecord);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

    			}

 				invAdjustment.setAdjVoid(EJBCommon.TRUE);

    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);
    			
    			

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }










    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ApVoucherPostController voidInvAdjustment");

    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();

    			Iterator i = invDistributionRecords.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				list.add(invDistributionRecord);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

    			}

    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				list.add(invAdjustmentLine);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				InvModAdjustmentLineDetails details = new InvModAdjustmentLineDetails();

    	    		details.setAlAdjustQuantity(0);
    	    		details.setAlUnitCost(invAdjustmentLine.getAlUnitCost() * - 1);
    	    		details.setAlIiName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
    	    		details.setAlLocName(invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName());
    	    		details.setAlUomName(invAdjustmentLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());

    				this.addInvAlEntry(details, invAdjustment, EJBCommon.TRUE, AD_CMPNY);

    			}

    			invAdjustment.setAdjVoid(EJBCommon.TRUE);

    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH,
			Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ApVoucherPostController generateCostVariance");

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;

    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		try{

    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);

    		} catch (FinderException ex) {

    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		}

    		InvModAdjustmentLineDetails details = new InvModAdjustmentLineDetails();

    		details.setAlAdjustQuantity(0);
    		details.setAlUnitCost(CST_VRNC_VL);
    		details.setAlIiName(invItemLocation.getInvItem().getIiName());
    		details.setAlLocName(invItemLocation.getInvLocation().getLocName());
    		details.setAlUomName(invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName());

    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(details, newInvAdjustment, EJBCommon.TRUE, AD_CMPNY);

    		// check for branch mapping

    		LocalAdBranchItemLocation adBranchItemLocation = null;

    		try{

    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		LocalGlChartOfAccount glInventoryChartOfAccount = null;

    		if (adBranchItemLocation == null) {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());

    		}


    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;

    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE",
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance");

    	try {

    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			if(prevInvCosting.getCstRemainingQuantity() < 0) {

    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();

    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);

    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();

    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();

    					}

    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();

   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){

   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   							ADJ_RFRNC_NMBR = "ARCM" +
							invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();

   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   							ADJ_RFRNC_NMBR = "ARMR" +
							invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   						}

    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" +
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();

    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();

    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {

    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);

    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();

    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);

    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}

    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" +
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();

    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" +
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();

    				} else {

    					prevInvCosting = invPropagatedCosting;
    					continue;

    				}

    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;

    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());

    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);

    			}

    			// set previous costing
    			prevInvCosting = invPropagatedCosting;

    		}

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}
    	*/
    }


    private void createInvTagList(LocalInvAdjustmentLine invAdjustmentLine, ArrayList list, Integer AD_CMPNY) throws Exception {

    	Debug.print("InvAdjustmentEntryControllerBean createInvTagList");

    	LocalAdUserHome adUserHome = null;
    	LocalInvTagHome invTagHome = null;

    	// Initialize EJB Home

    	try{
    		adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
    		invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}



    	try {
			System.out.println("aabot?");
	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
  	  	    Iterator t = list.iterator();

  	  	    LocalInvTag invTag  = null;
  	  	    System.out.println("umabot?");
  	  	    while (t.hasNext()){
  	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
  	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
  	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
  	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
  	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
  	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

  	  	    	if (tgLstDetails.getTgCode()==null){
  	  	    		System.out.println("ngcreate ba?");
      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
      	  	    			tgLstDetails.getTgType());

      	  	    	invTag.setInvAdjustmentLine(invAdjustmentLine);
      	  	    	invTag.setInvItemLocation(invAdjustmentLine.getInvItemLocation());
      	  	    	LocalAdUser adUser = null;
      	  	    	try {
      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
      	  	    	}catch(FinderException ex){

      	  	    	}
      	  	    	invTag.setAdUser(adUser);
      	  	    	System.out.println("ngcreate ba?");
  	  	    	}

  	  	    }




		}catch(Exception ex) {
			throw ex;
		}
    }


    private ArrayList getInvTagList(LocalInvAdjustmentLine arInvAdjustmentLine) {

    	ArrayList list = new ArrayList();

    	Collection invTags = arInvAdjustmentLine.getInvTags();
		Iterator x = invTags.iterator();
		while (x.hasNext()) {
			LocalInvTag invTag = (LocalInvTag) x.next();
			InvModTagListDetails tgLstDetails = new InvModTagListDetails();
			tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
			tgLstDetails.setTgSpecs(invTag.getTgSpecs());
			tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
			tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
			try{

				tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
			}
			catch(Exception ex){
				tgLstDetails.setTgCustodian("");
			}

			list.add(tgLstDetails);

			System.out.println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
			System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
			System.out.println(list+ "<== taglist inside controllerbean ");

		}

    	return list;

    }



    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM,String NT_BY, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("ApVoucherPostController saveInvAdjustment");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home

    	try{

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;

    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

    		try {

    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		try {

    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		while (true) {

    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));
    					break;

    				}

    			} else {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					break;

    				}

    			}

    		}

    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

    		return invAdjustment;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}


    }


private byte sendEmail(LocalAdApprovalQueue adApprovalQueue, Integer AD_CMPNY) {

		Debug.print("InvApprovalControllerBean sendEmail");


		LocalInvAdjustmentHome invAdjustmentHome = null;


		// Initialize EJB Home

		try {

			invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		StringBuilder composedEmail = new StringBuilder();
		LocalInvAdjustment invAdjustment = null;

		try {

			invAdjustment = invAdjustmentHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());

			HashMap hm = new HashMap();

			/*Iterator i = apPurchaseRequisition.getApPurchaseRequisitionLines().iterator();

			while(i.hasNext()) {

				LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)i.next();

				Collection apCanvasses = apCanvassHome.findByPrlCodeAndCnvPo(
						apPurchaseRequisitionLine.getPrlCode(), EJBCommon.TRUE, AD_CMPNY);

				Iterator j = apCanvasses.iterator();


				while(j.hasNext()) {

					LocalApCanvass apCanvass = (LocalApCanvass)j.next();


					if (hm.containsKey(apCanvass.getApSupplier().getSplSupplierCode())){
						AdModApprovalQueueDetails adModApprovalQueueExistingDetails = (AdModApprovalQueueDetails)
		        				 hm.get(apCanvass.getApSupplier().getSplSupplierCode());

						adModApprovalQueueExistingDetails.setAqSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
						adModApprovalQueueExistingDetails.setAqSupplierName(apCanvass.getApSupplier().getSplName());
						adModApprovalQueueExistingDetails.setAqAmount(adModApprovalQueueExistingDetails.getAqAmount() +  apCanvass.getCnvAmount());

					} else {

						AdModApprovalQueueDetails adModApprovalQueueNewDetails = new AdModApprovalQueueDetails();

						adModApprovalQueueNewDetails.setAqSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
						adModApprovalQueueNewDetails.setAqSupplierName(apCanvass.getApSupplier().getSplName());
						adModApprovalQueueNewDetails.setAqAmount(apCanvass.getCnvAmount());

						hm.put(apCanvass.getApSupplier().getSplSupplierCode(), adModApprovalQueueNewDetails);
					}
				}
			}


			Set set = hm.entrySet();

			composedEmail.append("<table border='1' style='width:100%'>");

			composedEmail.append("<tr>");
			composedEmail.append("<th> VENDOR </th>");
			composedEmail.append("<th> AMOUNT </th>");
			composedEmail.append("</tr>");

			Iterator x = set.iterator();

			while(x.hasNext()) {


				Map.Entry me = (Map.Entry)x.next();

				AdModApprovalQueueDetails adModApprovalQueueDetails = (AdModApprovalQueueDetails)
							me.getValue();

				composedEmail.append("<tr>");
				composedEmail.append("<td>");
				composedEmail.append(adModApprovalQueueDetails.getAqSupplierName());
				composedEmail.append("</td>");
				composedEmail.append("<td>");
				composedEmail.append(adModApprovalQueueDetails.getAqAmount());
				composedEmail.append("</td>");
				composedEmail.append("</tr>");

			}

			composedEmail.append("</table></body></html>");*/




		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}


		String emailTo = adApprovalQueue.getAdUser().getUsrEmailAddress();
		Properties props = new Properties();
		/*
		 * GMAIL SETTINGS
		 * props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
		new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("chrisr@daltron.net.pg","Sh0wc453");
			}
		});*/


		props.put("mail.smtp.host", "180.150.253.101");
		props.put("mail.smtp.socketFactory.port", "25");
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.port", "25");

		Session session = Session.getDefaultInstance(props, null);

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ofs-notifcation@daltron.net.pg"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailTo));

			message.setRecipients(Message.RecipientType.BCC,
					InternetAddress.parse("cromero@wrcpng.com"));

			message.setSubject("DALTRON - OFS - INV ADJUSTMENT APPROVAL IA #:"+invAdjustment.getAdjDocumentNumber());
			/*message.setText("Dear Mr/Mrs," +
					"\n\n No spam to my email, please!");*/

			/*message.setContent(
		              "<h1>This is actual message embedded in HTML tags</h1>",
		             "text/html");*/
			System.out.println("adApprovalQueue.getAqRequesterName()="+adApprovalQueue.getAqRequesterName());
			System.out.println("adApprovalQueue.getAqDocumentNumber()="+adApprovalQueue.getAqDocumentNumber());
			System.out.println("adApprovalQueue.getAdUser().getUsrDescription()="+adApprovalQueue.getAdUser().getUsrDescription());

			message.setContent(
		              "Dear "+adApprovalQueue.getAdUser().getUsrDescription()+",<br><br>"+
		              "A stocktake adjusment request was raised by "+adApprovalQueue.getAqRequesterName()+" for your approval.<br>"+
		              "Adj Number: "+adApprovalQueue.getAqDocumentNumber()+".<br>"+
		              "Description: "+invAdjustment.getAdjDescription()+".<br>"+
		              composedEmail.toString() +

		              "Please click the link <a href=\"http://180.150.253.99:8080/daltron\">http://180.150.253.99:8080/daltron</a>.<br><br><br>"+

		              "This is an automated message and was sent from a notification-only email address.<br><br>"+
		              "Please do not reply to this message.<br><br>",
		             "text/html");


			Transport.send(message);
			adApprovalQueue.setAqEmailSent(EJBCommon.TRUE);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

		return 0;
	}

    // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

   	Debug.print("InvAdjustmentEntryControllerBean ejbCreate");

   }

}