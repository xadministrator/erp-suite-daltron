
/*
 * InvAdjustmentPostControllerBean.java
 *
 * Created on August 09 2004, 11:14 AM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModAdjustmentDetails;
import com.util.InvModAdjustmentLineDetails;

/**
 * @ejb:bean name="InvAdjustmentPostControllerEJB"
 *           display-name="Used for posting inventory adjustments"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvAdjustmentPostControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvAdjustmentPostController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvAdjustmentPostControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvAdjustmentPostControllerBean extends AbstractSessionBean {
	double CostA=0d;
	double CostB=0d;
	double UnitA=0d;
	double UnitB=0d;	
	double [ ] CostAmount = new double [10];
	double [ ] UnitValue = new double [10];
	
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvAdjPostableByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvAdjustmentPostControllerBean getInvAdjPostableByCriteria");
        
        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(adj) FROM InvAdjustment adj ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		criteriaSize--;
        		
        	}
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");	
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("adj.adjReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
        		
        	} 
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("adj.adjDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        		
        	} 
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("adj.adjDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	} 
        	
        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("adj.adjDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("adj.adjDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (criteria.containsKey("approvalStatus")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("adj.adjApprovalStatus=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("approvalStatus");
        		ctr++;
        		
        	} else {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("(adj.adjApprovalStatus='APPROVED' OR adj.adjApprovalStatus='N/A') ");
        		
        	}
        	
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("adj.adjPosted = 0 AND adj.adjAdBranch=" + AD_BRNCH + " AND adj.adjAdCompany=" + AD_CMPNY + " ");
        	
        	String orderBy = null;
        	
        	if (ORDER_BY.equals("REFERENCE NUMBER")) {	          
        		
        		orderBy = "adj.adjReferenceNumber";
        		
        	} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
        		
        		orderBy = "adj.adjDocumentNumber";
        		
        	}
        	
        	if (orderBy != null) {
        		
        		jbossQl.append("ORDER BY " + orderBy + ", adj.adjDate");
        		
        	} else {
        		
        		jbossQl.append("ORDER BY adj.adjDate, adj.adjReferenceNumber, adj.adjDocumentNumber");
        		
        	}
        	
        	jbossQl.append(" OFFSET ?" + (ctr + 1));	        
        	obj[ctr] = OFFSET;	      
        	ctr++;
        	
        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;		      
        	
        	Collection invAdjustments = invAdjustmentHome.getAdjByCriteria(jbossQl.toString(), obj);	         
        	
        	if (invAdjustments.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
        	Iterator i = invAdjustments.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvAdjustment invAdjustment = (LocalInvAdjustment)i.next(); 
        		
        		InvModAdjustmentDetails mdetails = new InvModAdjustmentDetails();
        		mdetails.setAdjCode(invAdjustment.getAdjCode());
        		mdetails.setAdjDate(invAdjustment.getAdjDate());
        		mdetails.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
        		mdetails.setAdjReferenceNumber(invAdjustment.getAdjReferenceNumber());
        		
        		LocalInvDistributionRecord invDistributionRecord = null;
        		
        		try {
        			invDistributionRecord = invDistributionRecordHome.findByDrClassAndAdjCode("ADJUSTMENT", invAdjustment.getAdjCode(), AD_CMPNY);
        			mdetails.setAdjAmount(invDistributionRecord.getDrAmount());
        		} catch (FinderException ex) {
        			
        		}
        		
        		
        		
        		list.add(mdetails);
        		
        	}
        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException,
		GlobalAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException,
		GlobalExpiryDateNotFoundException,
		GlobalRecordInvalidException {
                    
        Debug.print("InvAdjustmentPostControllerBean executeInvAdjPost");
        
        LocalInvAdjustmentHome invAdjustmentHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvItemHome invItemHome = null;
                
        // Initialize EJB Home
        
        try {
            
            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);        
    		invItemHome = (LocalInvItemHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	// validate if adjustment is already deleted
        	
        	LocalInvAdjustment invAdjustment = null;
        	
        	try {
        		
        		invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if adjustment is already posted or void
        	
        	if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
        		throw new GlobalTransactionAlreadyPostedException();
        		
        	}
        	
        	// regenerate inventory dr
        	
        	this.regenerateInventoryDr(invAdjustment, AD_BRNCH, AD_CMPNY);
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	
        	boolean hasInsufficientItems = false;
    		String insufficientItems = "";
    		
    		Collection invAdjustmentLines = null;
    		
    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

        	
    		
        	Iterator c2 = invAdjustmentLines.iterator();
        	
        	int line =1;
        	
        	while(c2.hasNext()) {
        	
        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) c2.next();

        		if(invAdjustmentLine.getInvAdjustment().getAdjType().equals("ISSUANCE")) {
        			
        		LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(), invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_CMPNY);
        		
        		double zxc = this.getInvFifoCost2(invAdjustment.getAdjDate(), invItemLocation.getIlCode(), invAdjustmentLine.getAlAdjustQuantity(),
 						 false, AD_BRNCH, AD_CMPNY);
        		
        			System.out.print("ISSUANCE");
        		if(zxc<=1){
        			
                System.out.print("NO LAYER");
                System.out.print("SAVE REQUEST2");
            	   
                } else {
                	
                	zxc--;
                	System.out.print("IS LAYER");
                    System.out.print("SAVE REQUEST2");

                    
                	   invAdjustmentLine.setAlAdjustQuantity(UnitValue[0]*-1);
                	   invAdjustmentLine.setAlUnitCost(CostAmount[0]/UnitValue[0]);

                	   //System.out.print("BRCODE "+BR_BRNCH_CODE);
                	   //System.out.print("COMCODE "+AD_CMPNY);

                	   int indexValue=1;   
                	   while(zxc>0)
                	   {
	                    System.out.print("SAVE REQUEST2");
	                    LocalInvAdjustmentLine invAdjustmentLine2 = invAdjustmentLineHome.create(CostAmount[indexValue]/UnitValue[indexValue], invAdjustmentLine.getAlQcNumber(),invAdjustmentLine.getAlQcExpiryDate(), UnitValue[indexValue]*-1,0, (byte)0, AD_CMPNY);
	                    invAdjustmentLine2.setInvAdjustment(invAdjustmentLine.getInvAdjustment());
	                    invAdjustmentLine2.setInvItemLocation(invAdjustmentLine.getInvItemLocation());
	                    invAdjustmentLine2.setInvUnitOfMeasure(invAdjustmentLine.getInvUnitOfMeasure());
	                    //invAdjustmentLine = invAdjustmentLineHome.create(CostB/UnitB, UnitB, 0, 0, AD_CMPNY);
	                    zxc--;
	                    indexValue++;
	                    
                	   }
                	
                	}
        		
        		
        		
        		}
        		else
        			System.out.print("NOT ISSUANCE");
        	}
        	Collection invAdjustmentLines2 = null;
    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines2 = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines2 = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);
    		System.out.println("*********************************************invAdjustmentLines2.size(): " + invAdjustmentLines2.size());
        	Iterator c = invAdjustmentLines2.iterator();
        	while(c.hasNext()) {
        		
        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) c.next();
        		
        		//INSUFFICIENT STOCKS
        		
        		if(invAdjustmentLine.getAlAdjustQuantity() < 0 && adPreference.getPrfArCheckInsufficientStock() == EJBCommon.TRUE){
        
      
        				
        				double CURR_QTY = 0; 
        				double ILI_QTY = this.convertByUomAndQuantity(
        						invAdjustmentLine.getInvUnitOfMeasure(), 
        						invAdjustmentLine.getInvItemLocation().getInvItem(), 
        						Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

        				LocalInvCosting invCosting = null;
        				
        				try {
        					
        					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
        							invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(), 
        							invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        	
        	
        				} catch (FinderException ex) {
        	
        				}

        				double LOWEST_QTY = this.convertByUomAndQuantity(
        						invAdjustmentLine.getInvUnitOfMeasure(),
        						invAdjustmentLine.getInvItemLocation().getInvItem(),
        						1, AD_CMPNY);
        			

        				if(invCosting != null){

 		  	   	    		CURR_QTY = this.convertByUomAndQuantity( invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), 
	        						Math.abs(invCosting.getCstRemainingQuantity()), AD_CMPNY);

 		  	   	    		
 		  	   	    	}
        				
        				
        				if (invCosting == null || CURR_QTY == 0 || 
        						CURR_QTY - ILI_QTY <= -LOWEST_QTY) {

        					hasInsufficientItems = true;
        				
        					insufficientItems += invAdjustmentLine.getInvItemLocation().getInvItem().getIiName() + ", ";
        				}
        			
        		}
        		
        		
        		if(hasInsufficientItems) {

	        		throw new GlobalRecordInvalidException(insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
	        	}
        		
        		String II_NM = invAdjustmentLine.getInvItemLocation().getInvItem().getIiName();
        		String LOC_NM = invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName();
        		
        		double ADJUST_QTY = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), 
        				invAdjustmentLine.getInvItemLocation().getInvItem(), invAdjustmentLine.getAlAdjustQuantity(), AD_CMPNY);
        		
        		
        		LocalInvCosting invCosting = null;

        		try {
	        			
        			invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invAdjustment.getAdjDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
        			
        		} catch (FinderException ex) {
        			
        		}
        		
        		
        		double COST = invAdjustmentLine.getAlUnitCost();
				
				if (invCosting == null ) {
	
						this.postToInv(invAdjustmentLine, invAdjustment.getAdjDate(), 
								ADJUST_QTY, COST * ADJUST_QTY, 
								ADJUST_QTY, COST * ADJUST_QTY, 
								0d, null, AD_BRNCH, AD_CMPNY);

        		} else {

        			this.postToInv(invAdjustmentLine, invAdjustment.getAdjDate(), 
        					ADJUST_QTY, COST * ADJUST_QTY,
							invCosting.getCstRemainingQuantity() + ADJUST_QTY, invCosting.getCstRemainingValue() + (COST * ADJUST_QTY), 
							0d, null, AD_BRNCH, AD_CMPNY);
				
	        	}
        	}
        	
        	// post adjustment
        	
        	
        	invAdjustment.setAdjPosted(EJBCommon.TRUE);
        	invAdjustment.setAdjPostedBy(USR_NM);
        	invAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        	
        	// post to gl if necessary
        	
        	//LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	if (adPreference.getPrfInvGlPostingType().equals("USE SL POSTING") || invAdjustment.getAdjIsCostVariance() == EJBCommon.TRUE) {
        		
        		// validate if date has no period and period is closed
        		
        		LocalGlSetOfBook glJournalSetOfBook = null;
        		
        		try {
        			
        			glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);
        			
        		} catch (FinderException ex) {
        			
        			throw new GlJREffectiveDateNoPeriodExistException();
        			
        		}
        		
        		LocalGlAccountingCalendarValue glAccountingCalendarValue = 
        			glAccountingCalendarValueHome.findByAcCodeAndDate(
        					glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);
        		
        		
        		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
        				glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
        			
        			throw new GlJREffectiveDatePeriodClosedException();
        			
        		}
        		
        		// check if invoice is balance if not check suspense posting
        		
        		LocalGlJournalLine glOffsetJournalLine = null;
        		
    			Collection invDistributionRecords = null;
    			
    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			} else {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			}

        		
        		Iterator j = invDistributionRecords.iterator();
        		
        		double TOTAL_DEBIT = 0d;
        		double TOTAL_CREDIT = 0d;
        		
        		while (j.hasNext()) {
        			
        			LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
        			
        			double DR_AMNT = 0d;
        			
        			DR_AMNT = invDistributionRecord.getDrAmount();
        			
        			if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
        				
        				TOTAL_DEBIT += DR_AMNT;
        				
        			} else {
        				
        				TOTAL_CREDIT += DR_AMNT;
        				System.out.println(DR_AMNT+"-------------CRDT");
        			}
        			
        		}
        		
        		System.out.println("TOTAL_DEBIT------------>"+TOTAL_DEBIT);
        		System.out.println("TOTAL_CREDIT------------>"+TOTAL_CREDIT);
        		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        		
        		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
        			
        			LocalGlSuspenseAccount glSuspenseAccount = null;
        			
        			try { 	
        				
        				glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS", AD_CMPNY);
        				
        			} catch (FinderException ex) {
        				System.out.println("here: ");
        				throw new GlobalJournalNotBalanceException();
        				
        			}
        			
        			if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
        				
        				glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
								EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
        				
        			} else {
        				
        				glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
								EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
        				
        			}
        			
        			LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
      
        			glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
        			
        			
        		} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
        				TOTAL_DEBIT != TOTAL_CREDIT) {
      
        			throw new GlobalJournalNotBalanceException();		    	
        			
        		}
        		
        		// create journal batch if necessary
        		
        		LocalGlJournalBatch glJournalBatch = null;
        		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        		
        		try {
        			
        			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
        			
        		} catch (FinderException ex) {
        			
        		}
        		
        		if (glJournalBatch == null) {
        			
        			glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
        			
        		}
        		
        		// create journal entry			            	
        		
        		LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
        				invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null, 
						AD_BRNCH, AD_CMPNY);
        		
        		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
        		glJournal.setGlJournalSource(glJournalSource);
        		
        		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
        		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);     		
        		
        		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);        		
        		glJournal.setGlJournalCategory(glJournalCategory);
        		
        		if (glJournalBatch != null) {

        			glJournal.setGlJournalBatch(glJournalBatch);
        		}           		    
        		
        		// create journal lines
        		
        		j = invDistributionRecords.iterator();
        		
        		while (j.hasNext()) {
        			
        			LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
        			
        			double DR_AMNT = 0d;
        			
        			DR_AMNT = invDistributionRecord.getDrAmount();
        			
        			LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
							invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
        			
        			//invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
        			glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());
        			
        			//glJournal.addGlJournalLine(glJournalLine);
        			glJournalLine.setGlJournal(glJournal);
        			
        			invDistributionRecord.setDrImported(EJBCommon.TRUE);
        			
        			
        		}
        		
        		if (glOffsetJournalLine != null) {
        			
        			//glJournal.addGlJournalLine(glOffsetJournalLine);
        			glOffsetJournalLine.setGlJournal(glJournal);
        			
        		}		
        		
        		// post journal to gl
        		
        		Collection glJournalLines = glJournal.getGlJournalLines();
        		
        		Iterator i = glJournalLines.iterator();
        		
        		while (i.hasNext()) {
        			
        			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
        			
        			// post current to current acv
        			
        			this.postToGl(glAccountingCalendarValue,
        					glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        			
        			
        			// post to subsequent acvs (propagate)
        			
        			Collection glSubsequentAccountingCalendarValues = 
        				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
        						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
        			
        			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
        			
        			while (acvsIter.hasNext()) {
        				
        				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        					(LocalGlAccountingCalendarValue)acvsIter.next();
        				
        				this.postToGl(glSubsequentAccountingCalendarValue,
        						glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        				
        			}
        			
        			// post to subsequent years if necessary
        			
        			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
        			
        			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
        				
        				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        				LocalGlChartOfAccount glRetainedEarningsAccount = null;
        				
        				try{
        					
        					glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY);
        				
        				} catch (FinderException ex) {
        					
        					throw new GlobalAccountNumberInvalidException();
							
        				}
        				
        				Iterator sobIter = glSubsequentSetOfBooks.iterator();
        				
        				while (sobIter.hasNext()) {
        					
        					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
        					
        					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
        					
        					// post to subsequent acvs of subsequent set of book(propagate)
        					
        					Collection glAccountingCalendarValues = 
        						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
        					
        					Iterator acvIter = glAccountingCalendarValues.iterator();
        					
        					while (acvIter.hasNext()) {
        						
        						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
        							(LocalGlAccountingCalendarValue)acvIter.next();
        						
        						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
        								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
        							
        						} else { // revenue & expense
        							
        							this.postToGl(glSubsequentAccountingCalendarValue,
        									glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
        							
        						}
        						
        					}
        					
        					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
        					
        				}
        				
        			}
        			
        		}
        		
        	} 			   
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalInventoryDateException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch(GlobalBranchAccountNumberInvalidException ex) {
			
        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (GlobalAccountNumberInvalidException ex) {
			
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
    	} catch (GlobalExpiryDateNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
    	} catch (GlobalRecordInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
            
        }catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }


   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("InvAdjustmentPostControllerBean getGlFcPrecisionUnit");

       	
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    // private methods
    
    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, 
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {
	  	 
		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;
	  	 LocalAdPreferenceHome adPreferenceHome = null;
	       
	     // Initialize EJB Home
	        
	     try {
	         
	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	    	 adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class); 
	     } 
	     catch (NamingException ex) {
	            
	    	 throw new EJBException(ex.getMessage());
	     }
	    	
		try {
 			
 			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
 			
 			if (invFifoCostings.size() > 0) {
 				double fifoCost = 0;
 			    double runningQty = Math.abs(CST_QTY);
 				Iterator x = invFifoCostings.iterator();
 				while (x.hasNext()) {
 					
 					LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
 	  				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
 	  				double newRemainingLifoQuantity = 0;
 	  				if (invFifoCosting.getCstRemainingLifoQuantity() <= runningQty) {
 	  					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
 	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * invFifoCosting.getCstRemainingLifoQuantity();
 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
 		 	  				fifoCost  += invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold() * invFifoCosting.getCstRemainingLifoQuantity();
 	 	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
 	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * invFifoCosting.getCstRemainingLifoQuantity();
 	 	 	  			} else {
 	 	 	  				fifoCost += invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity() * invFifoCosting.getCstRemainingLifoQuantity();
 	 	 	  			}
 	  					runningQty = runningQty - invFifoCosting.getCstRemainingLifoQuantity();
 	  				} else {
 	  					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
 	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * runningQty;
 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
 		 	  				fifoCost  += invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold() * runningQty;
 	 	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
 	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * runningQty;
 	 	 	  			} else {
 	 	 	  				fifoCost += invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity() * runningQty;
 	 	 	  			}
 	  					newRemainingLifoQuantity = invFifoCosting.getCstRemainingLifoQuantity() - runningQty;
 	  					runningQty = 0;
 	  				}
 	 	  			if (isAdjustFifo) invFifoCosting.setCstRemainingLifoQuantity(newRemainingLifoQuantity);
 					if (runningQty <=0) break;
 				}
 				System.out.println("fifoCost" + fifoCost);
 				System.out.println("CST_QTY" + CST_QTY);
 				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
 				return EJBCommon.roundIt(fifoCost / CST_QTY, adPreference.getPrfInvCostPrecisionUnit());
 				
 			} 
 			else {
 				
 				//most applicable in 1st entries of data
 				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
 				return invItemLocation.getInvItem().getIiUnitCost();
 			}
 				
 		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}     

    
    
    
    private void postToInv(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY, double CST_ADJST_CST,
    		double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
    		AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {
    
    	Debug.print("InvAdjustmentPostControllerBean post");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
      	      lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              
            
        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
           int CST_LN_NMBR = 0;
           System.out.println(">>CST_ADJST_CST1: " + CST_ADJST_CST);
           CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adPreference.getPrfInvCostPrecisionUnit());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adPreference.getPrfInvCostPrecisionUnit());
           System.out.println(">>CST_ADJST_CST: " + CST_ADJST_CST);
                      
           if (CST_ADJST_QTY < 0) {
                      
           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
           
           }

           try {
        	   System.out.println("generate line number");
        	   System.out.println(invAdjustmentLine.getInvAdjustment().getAdjDate().getTime());
        	   System.out.println(CST_DT.getTime());
        	   System.out.println(invItemLocation.getInvItem().getIiName());
           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), 
            		   invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
               System.out.println("generate line number: " +CST_LN_NMBR);
           } catch (FinderException ex) {
        	   System.out.println("generate line number CATCH");
        	   try{
        		   /*LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
        				   CST_DT, invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(), 
							invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        		   */
        		   LocalInvCosting invCosting  = invCostingHome.findByCstDateAndIiNameAndLocNameLimit1(CST_DT, 
        				   invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(), invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), 
        				   AD_BRNCH, AD_CMPNY);
        		   System.out.println(invCosting.getCstCode());
        		   System.out.println(invCosting.getCstLineNumber());
        		   CST_LN_NMBR = invCosting.getCstLineNumber()+1;
        	   }catch(Exception e){
        		   CST_LN_NMBR = 1;
        	   }
           	   
           
           }
           /*
           if(CST_ADJST_QTY != 0) {
           	
            //void subsequent cost variance adjustments
            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
            Iterator i = invAdjustmentLines.iterator();
            
            while (i.hasNext()){
            	
            	LocalInvAdjustmentLine invAdjustmentLineTemp = (LocalInvAdjustmentLine) i.next();
            	this.voidInvAdjustment(invAdjustmentLineTemp.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
            	
            }
           	
           }*/
           
           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           try {           
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   System.out.println(prevCst.getCstCode()+"   "+ prevCst.getCstRemainingQuantity());
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();
        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }
        	   System.out.println("prevExpiryDates: " + prevExpiryDates);
        	   System.out.println("qtyPrpgt: " + qtyPrpgt);
           }catch (Exception ex){
        	   System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
        	   prevExpiryDates="";
           }
           
           // create costing
           
           //CST_ADJST_CST = invAdjustmentLine.getAlUnitCost() * CST_ADJST_QTY;
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, 
    			   CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
           //invItemLocation.addInvCosting(invCosting);
           invCosting.setCstQCNumber(invAdjustmentLine.getAlQcNumber());
           invCosting.setCstQCExpiryDate(invAdjustmentLine.getAlQcExpiryDate());
           invCosting.setInvItemLocation(invItemLocation);
           invCosting.setInvAdjustmentLine(invAdjustmentLine);
           
           System.out.println("CST_ADJST_QTY>: " + CST_ADJST_QTY);
           System.out.println("CST_ADJST_CST>: " + CST_ADJST_CST);
//         Get Latest Expiry Dates           
           if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
        	   if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0 ){

        		   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
        			   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));

        			   String miscList2Prpgt = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), qty2Prpgt, "False");
        			   ArrayList miscList = this.expiryDates(invAdjustmentLine.getAlMisc(), qty2Prpgt);
        			   String propagateMiscPrpgt = "";
        			   String ret = "";
        			   String exp = "";
        			   String Checker = "";

        			   //ArrayList miscList2 = null;
        			   if(CST_ADJST_QTY>0){
        				   prevExpiryDates = prevExpiryDates.substring(1);
        				   propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
        			   }else{
        				   Iterator mi = miscList.iterator();

        				   propagateMiscPrpgt = prevExpiryDates;
        				   ret = propagateMiscPrpgt;
        				   while(mi.hasNext()){
        					   String miscStr = (String)mi.next();

        					   Integer qTest = this.checkExpiryDates(ret+"fin$");
        					   ArrayList miscList2 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

        					   //ArrayList miscList2 = this.expiryDates("$" + ret, qtyPrpgt);
        					   Iterator m2 = miscList2.iterator();
        					   ret = "";
        					   String ret2 = "false";
        					   int a = 0;
        					   while(m2.hasNext()){
        						   String miscStr2 = (String)m2.next();

        						   if(ret2=="1st"){
        							   ret2 = "false";
        						   }
        						   System.out.println("miscStr1: "+miscStr.toUpperCase());
        						   System.out.println("miscStr2: "+miscStr2);

        						   if(miscStr2.toUpperCase().trim().equals(miscStr.toUpperCase().trim())){
        							   if(a==0){
        								   a = 1;
        								   ret2 = "1st";
        								   Checker = "true";
        							   }else{
        								   a = a+1;
        								   ret2 = "true";
        							   }
        						   }
        						   System.out.println("Checker: "+Checker);
        						   System.out.println("ret2: "+ret2);
        						   if(!miscStr2.toUpperCase().trim().equals(miscStr.toUpperCase().trim()) || a>1){
        							   if((ret2!="1st")&&((ret2=="false")||(ret2=="true"))){
        								   if (miscStr2!=""){
        									   miscStr2 = "$" + miscStr2;
        									   ret = ret + miscStr2;
        									   System.out.println("ret " + ret);
        									   ret2 = "false";
        								   }
        							   }
        						   }

        					   }
        					   if(ret!=""){
        						   ret = ret + "$";
        					   }
        					   System.out.println("ret una: "+ret);
        					   exp = exp + ret;
        					   qtyPrpgt= qtyPrpgt -1;
        				   }
        				   System.out.println("ret fin " + ret);
        				   System.out.println("exp fin " + exp);
        				   //propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
        				   propagateMiscPrpgt = ret;
        				   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
        				   if(Checker=="true"){
        					   //invCosting.setCstExpiryDate(propagateMiscPrpgt);
        				   }else{
        					   System.out.println("Exp Not Found");
        					   throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
        				   }
        			   }
        			   invCosting.setCstExpiryDate(propagateMiscPrpgt);

        		   }else{
        			   invCosting.setCstExpiryDate(prevExpiryDates);
        		   }

        	   }else{
        		   System.out.println("invAdjustmentLine ETO NA: "+ invAdjustmentLine.getAlAdjustQuantity());
        		   if(invAdjustmentLine.getAlAdjustQuantity()>0){
        			   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
        				   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
        				   String initialPrpgt = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), initialQty, "False");

        				   invCosting.setCstExpiryDate(initialPrpgt);
        			   }else{
        				   invCosting.setCstExpiryDate(prevExpiryDates);
        			   }
        		   }else{                    	
        			   //throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
        			   invCosting.setCstExpiryDate(prevExpiryDates);
        		   }

        	   }
           }
           

           // if cost variance is not 0, generate cost variance for the transaction 
           /*
			if(CST_VRNC_VL != 0) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"INVADJ" + invAdjustmentLine.getInvAdjustment().getAdjDocumentNumber(),
						invAdjustmentLine.getInvAdjustment().getAdjDescription(),
						invAdjustmentLine.getInvAdjustment().getAdjDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				
			}*/

           // propagate balance if necessary           
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
           
           String miscList  ="";
           ArrayList miscList2 = null;
           Iterator i = invCostings.iterator();
           

           String propagateMisc ="";
		   String ret = "";
		   //String Checker = "";
           
           double PREV_QTY =  invCosting.getCstRemainingQuantity();
           double PREV_RMNG_VL = invCosting.getCstRemainingValue();
           double PREV_CST = 0;
           if (invCosting.getCstRemainingQuantity() != 0)
              PREV_CST = invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity(); 
           
           while (i.hasNext()) {
        	   String Checker = "";
        	   String Checker2= "";
        	   
               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
               
               if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
            	   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
        	           double qty = Double.parseDouble(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
        	           //invPropagatedCosting.getInvAdjustmentLine().getAlMisc();
        	           miscList = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), qty, "False");
        	           miscList2 = this.expiryDates(invAdjustmentLine.getAlMisc(), qty);
        	           System.out.println("invAdjustmentLine.getAlMisc(): "+invAdjustmentLine.getAlMisc());
        	           System.out.println("getAlAdjustQuantity(): "+invAdjustmentLine.getAlAdjustQuantity());

        	           if(invAdjustmentLine.getAlAdjustQuantity()<0){
        	        	   Iterator mi = miscList2.iterator();
        	        	   propagateMisc = invPropagatedCosting.getCstExpiryDate();
        	        	   ret = invPropagatedCosting.getCstExpiryDate();
        	        	   while(mi.hasNext()){
        	        		   String miscStr = (String)mi.next();

        	        		   Integer qTest = this.checkExpiryDates(ret+"fin$");
        	        		   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

        	        		   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
        	        		   System.out.println("ret: " + ret);
        	        		   Iterator m2 = miscList3.iterator();
        	        		   ret = "";
        	        		   String ret2 = "false";
        	        		   int a = 0;
        	        		   while(m2.hasNext()){
        	        			   String miscStr2 = (String)m2.next();

        	        			   if(ret2=="1st"){
        	        				   ret2 = "false";
        	        			   }
        	        			   System.out.println("2 miscStr: "+miscStr);
        	        			   System.out.println("2 miscStr2: "+miscStr2);
        	        			   if(miscStr2.equals(miscStr)){
        	        				   if(a==0){
        	        					   a = 1;
        	        					   ret2 = "1st";
        	        					   Checker2 = "true";
        	        				   }else{
        	        					   a = a+1;
        	        					   ret2 = "true";
        	        				   }
        	        			   }
        	        			   System.out.println("Checker: "+Checker2);
        	        			   if(!miscStr2.equals(miscStr) || a>1){
        	        				   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
        	        					   if (miscStr2!=""){
        	        						   miscStr2 = "$" + miscStr2;
        	        						   ret = ret + miscStr2;
        	        						   ret2 = "false";
        	        					   }
        	        				   }
        	        			   }

        	        		   }
        	        		   if(Checker2!="true"){
        	        			   //throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
        	        		   }else{

        	        		   }

        	        		   ret = ret + "$";
        	        		   qtyPrpgt= qtyPrpgt -1;
        	        	   }
        	           }
        	           
                   }
               }
               
               
               System.out.println("2invPropagatedCosting.getCstRemainingQuantity(): " + invPropagatedCosting.getCstRemainingQuantity());
   			System.out.println("2CST_ADJST_QTY(): " + CST_ADJST_QTY);
               
               if (CST_ADJST_QTY < 0) {               
	               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
	               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
               } else {
            	   
            	   invPropagatedCosting.setCstRemainingLifoQuantity(invPropagatedCosting.getCstRemainingLifoQuantity() + CST_ADJST_QTY);
            	   
            	   invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
                   // fix ave cost computation for negative inventory and cost of sales only
                   //CST_ASSMBLY_CST = previous remaining value - new ave cost total 
                   
                   if(invPropagatedCosting.getCstAdjustQuantity() < 0){
                	   double prevTxnCost = invPropagatedCosting.getCstAdjustCost();
                	   invPropagatedCosting.setCstAdjustCost(PREV_CST * invPropagatedCosting.getCstAdjustQuantity());
                	   //invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingQuantity() * PREV_CST);
                	   invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST
                			   - Math.abs(prevTxnCost - invPropagatedCosting.getCstAdjustCost()));
                	   
                   }else if(invPropagatedCosting.getCstQuantitySold() > 0){
                	   double prevTxnCost = invPropagatedCosting.getCstCostOfSales();
                	   invPropagatedCosting.setCstCostOfSales(PREV_CST * invPropagatedCosting.getCstQuantitySold());
                	   //invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingQuantity() * PREV_CST);
                	   invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST
                			   - Math.abs(prevTxnCost - invPropagatedCosting.getCstCostOfSales()));
                	   
                   }else if(invPropagatedCosting.getCstAssemblyQuantity() < 0){
                	   double prevTxnCost = invPropagatedCosting.getCstAssemblyCost();            	   
                	   invPropagatedCosting.setCstAssemblyCost(PREV_CST * invPropagatedCosting.getCstAssemblyQuantity());
                	   invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST
                			   - Math.abs(Math.abs(prevTxnCost) - invPropagatedCosting.getCstAssemblyCost()));
                	   
                   }else{
                	   invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
                	   
                   }
                   
                   PREV_QTY = invPropagatedCosting.getCstRemainingQuantity();
                   PREV_RMNG_VL = invPropagatedCosting.getCstRemainingValue();
                   if(PREV_QTY != 0)
                	   PREV_CST = invPropagatedCosting.getCstRemainingValue() / invPropagatedCosting.getCstRemainingQuantity(); 
               }
               
               if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
            	   System.out.println("getAlAdjustQuantity(): "+invAdjustmentLine.getAlAdjustQuantity());
            	   if(invAdjustmentLine.getAlAdjustQuantity()<0 && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc()!=null && !invAdjustmentLine.getAlMisc().equals("")){

            		   Iterator mi = miscList2.iterator();

            		   propagateMisc = invPropagatedCosting.getCstExpiryDate();
            		   ret = propagateMisc;
            		   while(mi.hasNext()){
            			   String miscStr = (String)mi.next();

            			   Integer qTest = this.checkExpiryDates(ret+"fin$");
            			   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

            			   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
            			   System.out.println("ret: " + ret);
            			   Iterator m2 = miscList3.iterator();
            			   ret = "";
            			   String ret2 = "false";
            			   int a = 0;
            			   while(m2.hasNext()){
            				   String miscStr2 = (String)m2.next();

            				   if(ret2=="1st"){
            					   ret2 = "false";
            				   }
            				   System.out.println("2 miscStr: "+miscStr);
            				   System.out.println("2 miscStr2: "+miscStr2);
            				   if(miscStr2.equals(miscStr)){
            					   if(a==0){
            						   a = 1;
            						   ret2 = "1st";
            						   Checker = "true";
            					   }else{
            						   a = a+1;
            						   ret2 = "true";
            					   }
            				   }
            				   System.out.println("Checker: "+Checker);
            				   if(!miscStr2.equals(miscStr) || a>1){
            					   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
            						   if (miscStr2!=""){
            							   miscStr2 = "$" + miscStr2;
            							   ret = ret + miscStr2;
            							   ret2 = "false";
            						   }
            					   }
            				   }

            			   }
            			   ret = ret + "$";
            			   qtyPrpgt= qtyPrpgt -1;
            		   }
            		   propagateMisc = ret;
            	   }else{
            		   try{
            			   propagateMisc = miscList+invPropagatedCosting.getCstExpiryDate().substring(1, invPropagatedCosting.getCstExpiryDate().length());
            		   }catch(Exception e){
            			   propagateMisc = miscList;
            		   }
            	   }

            	   invPropagatedCosting.setCstExpiryDate(propagateMisc);
               }
               
               
           }                  
           
           // regenerate cost variance
          //this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
          if (false) {
        	throw new AdPRFCoaGlVarianceAccountNotFoundException();     	 
          }
           
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
        	
        	throw ex;
        	
        } catch (GlobalExpiryDateNotFoundException ex){
        	
        	throw ex;
        	
        } catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }

    }
   
    public static int checkExpiryDates(String misc) throws Exception {

		String separator ="$";

		// Remove first $ character
		misc = misc.substring(1);
		//System.out.println("misc: " + misc);
		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;	
		int numberExpry=0;
		String miscList = new String();
		String miscList2 = "";
		String g= "";
		try{
			while(g!="fin"){
				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				g= misc.substring(start, start + length);
				if(g.length()!=0){
					if(g!=null || g!="" || g!="null"){
						if(g.contains("null")){
							miscList2 = "Error";
						}else{
							miscList = miscList + "$" + g;
							numberExpry++;
						}
					}else{
						miscList2 = "Error";
					}

				}else{
					miscList2 = "Error";
				}
			}	
		}catch(Exception e){

		}

		if(miscList2==""){
			miscList = miscList+"$";
		}else{
			miscList = miscList2;
		}

		return (numberExpry);
	}
    
    public String getQuantityExpiryDates(String qntty){
    	String separator = "$";

    	// Remove first $ character
    	qntty = qntty.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = qntty.indexOf(separator, start);
    	int length = nextIndex - start;	
    	String y;
    	y = (qntty.substring(start, start + length));
    	
    	return y;
    }
    
    private ArrayList expiryDates(String misc, double qty) throws Exception{
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	String separator ="$";
    	

    	// Remove first $ character
    	misc = misc.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	

    	ArrayList miscList = new ArrayList();
		
    	for(int x=0; x<qty; x++) {

    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
      	
    		try{
    			String checker = misc.substring(start, start + length);
        		if(checker.length()!=0 || checker!="null"){
        			miscList.add(checker);
        		}else{
        			miscList.add("null");
        			qty++;
        		}
    		}catch(Exception e){
    			break;
    		}
    		
    	}	
		
		return miscList;
    }
    
    public String propagateExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	
    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}
 
    	// Remove first $ character
    	misc = misc.substring(1);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	
    	
    	String miscList = new String();
		
		for(int x=0; x<qty; x++) {
			
			// Date
			start = nextIndex + 1;
			nextIndex = misc.indexOf(separator, start);
			length = nextIndex - start;
			String g= misc.substring(start, start + length);
				if(g.length()!=0){
					miscList = miscList + "$" + g;	
				}
		}	
		
		miscList = miscList+"$";
		return (miscList);
    }
    
   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
      LocalGlChartOfAccount glChartOfAccount, 
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
      	
      Debug.print("InvAdjustmentPostControllerBean postToGl");
      
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
              
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {          
               
               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
       	   
	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance = 
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
	               	  
	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcExtendedPrecision();
	           
	           
	               	  
	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {				    
				    			
 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
				    
		
			  } else {
	
				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
	
		 	  }
		 	  
		 	  if (isCurrentAcv) { 
		 	 
			 	 if (isDebit == EJBCommon.TRUE) {
			 	 	
		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 				
		 		 } else {
		 		 	
		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 		 }       	   
		 		 
		 	}
       	
       } catch (Exception ex) {
       	
       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());
       	
       }
      
      
    }
   
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
			
		Debug.print("InvAdjustmentPostControllerBean convertByUomFromAndItemAndQuantity");		        
        
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
		// Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
                   
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
        	System.out.println("invItem.getIiName()-" + invItem.getIiName());
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
                        
        	return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvCostPrecisionUnit());
        						    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}	
      
    private void regenerateInventoryDr(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvAdjustmentPostControllerBean regenerateInventoryDr");		        
    	
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalInvItemHome invItemHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null; 
    	LocalAdPreferenceHome adPreferenceHome = null;
    	InvRepItemCostingControllerHome homeRIC = null;
        InvRepItemCostingController ejbRIC = null;
    	// Initialize EJB Home
    	
    	try {
    		
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class); 
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class); 
    		invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);    
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			homeRIC = (InvRepItemCostingControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
        try {
        	ejbRIC = homeRIC.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		
    		// regenerate inventory distribution records

            // remove all inventory distribution
            
    		Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByAdjCode(
    			invAdjustment.getAdjCode(), AD_CMPNY);
    		
    		Iterator i = invDistributionRecords.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    			i.remove();
    			invDistributionRecord.remove();
    			
    		}
    		
        	// remove all adjustment lines committed qty
        	
        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
        	
        	i = invAdjustmentLines.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
        		
        		if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
        		
        		    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
        		
        			invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);
        			
        		}
        		
        	}
    		
    		invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    		
    		if(invAdjustmentLines != null && !invAdjustmentLines.isEmpty()) {
    			
    			byte DEBIT = 0;
    			double TOTAL_AMOUNT = 0d; 
    			
    			i = invAdjustmentLines.iterator();
    			
    			while(i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
    				LocalInvItemLocation invItemLocation=invAdjustmentLine.getInvItemLocation();
    				
    				// start date validation
    				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    				if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {    				
	    				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	    					invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	    				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
    				}
    				
    				// add physical inventory distribution
    				
    				double AMOUNT = 0d;
    				
    				if (invAdjustmentLine.getAlAdjustQuantity() > 0 && !invAdjustmentLine.getInvAdjustment().getAdjType().equals("ISSUANCE")) {
    					
    					AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() *
    							invAdjustmentLine.getAlUnitCost(), adPreference.getPrfInvQuantityPrecisionUnit());
    					DEBIT = EJBCommon.TRUE;
    					
    				} else {
    					
                    double COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();
        				
        				try {
        					
        					LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
        						invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        					
        					if(invCosting.getCstRemainingQuantity() <= 0 && invCosting.getCstRemainingValue() <=0){
                                System.out.println("RE CALC");
                                HashMap criteria = new HashMap();
                                criteria.put("itemName", invItemLocation.getInvItem().getIiName());
                                criteria.put("location", invItemLocation.getInvLocation().getLocName());

                                ArrayList branchList = new ArrayList();

                                AdBranchDetails mdetails = new AdBranchDetails();
                                mdetails.setBrCode(AD_BRNCH);
                                branchList.add(mdetails);

                                ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

                                invCosting  =invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                						invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            			   }

        					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
     	  	    				
     	  	    				COST = invCosting.getCstRemainingQuantity() <= 0 ? COST :
     	  	    					Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
     	  	    			
                                if(COST<=0){
                                   COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
                                }   

     	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
     	  	    				
     	  	    				COST =  invCosting.getCstRemainingQuantity() == 0 ? COST :
     	  	    					Math.abs(this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
     	  	    						invAdjustmentLine.getAlAdjustQuantity(), invAdjustmentLine.getAlUnitCost(), false, AD_BRNCH, AD_CMPNY));
        					
     	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
     	  	    				
     	  	    				COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
    	
        				} catch (FinderException ex) { }
    					
    					COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
		 	  	        		invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
								COST, true, AD_CMPNY);
    					
    					AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() * COST,
    							adPreference.getPrfInvCostPrecisionUnit());
    					
    					DEBIT = EJBCommon.FALSE;
    					
    				}

    				// check for branch mapping
    				
	 	  	    	LocalAdBranchItemLocation adBranchItemLocation = null;
	 	  	    	
	 	  	    	try{
	 	  	    	
	 	  	    		adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
	 	  	    				invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {

	 	  	    	}

    				LocalGlChartOfAccount glInventoryChartOfAccount = null;

    				if (adBranchItemLocation == null) {
    					
    					glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    							invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    				} else {
    					
    					glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    							adBranchItemLocation.getBilCoaGlInventoryAccount());
    					
    				}

	 	  	    	this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(AMOUNT), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    				TOTAL_AMOUNT += AMOUNT;
    				//ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);
    				
    				// add adjust quantity to item location committed quantity if negative
    				
    				if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
    					
    					double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
    							invAdjustmentLine.getInvUnitOfMeasure(),
								invAdjustmentLine.getInvItemLocation().getInvItem(),
								Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
    					
    					invItemLocation = invAdjustmentLine.getInvItemLocation();
    					
    					invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() +
    							convertedQuantity);
    					
    				}
    				
    			}
    			
    			// add variance or transfer/debit distribution
    			
    			DEBIT = (TOTAL_AMOUNT > 0 && !invAdjustment.getAdjType().equals("ISSUANCE")) ? EJBCommon.FALSE : EJBCommon.TRUE;
    			
    			this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_AMOUNT), EJBCommon.FALSE,
    					invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    			
    		}

    	} catch (GlobalInventoryDateException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch(GlobalBranchAccountNumberInvalidException ex) {
			
			throw new GlobalBranchAccountNumberInvalidException ();

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
    	LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    	throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvAdjustmentPostControllerBean addInvDrEntry");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;           
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {        
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    		
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    		
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
    		
    		// create distribution record        
    		
    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
    				DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
    				DR_RVRSL, EJBCommon.FALSE, AD_CMPNY);
    		
    		//invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvAdjustment(invAdjustment);    		
    		
    		//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvChartOfAccount(glChartOfAccount);
    		
    		
		} catch(GlobalBranchAccountNumberInvalidException ex) {
			
			throw new GlobalBranchAccountNumberInvalidException ();

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }		
    
    private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

    	Debug.print("ArInvoicePostControllerBean convertByUomFromAndUomToAndQuantity");		        

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
    		invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	

    		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
    		LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

    		return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvCostPrecisionUnit());       	        		        		       	

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }	 
    
    private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {
		
		Debug.print("InvAdjustmentPostControllerBean convertCostByUom");		        
	    
		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		        
	    // Initialize EJB Home
	    
	    try {
	        
	        invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
	        invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
	           
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	            
	    try {
	    
	    	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
	                    
	        if (isFromDefault) {	        	
	        
	        	return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();
	    	
	        } else {
	        	
	        	return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();
	        	
	        }
	    	
	    	
	    						    		        		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
		
	}

    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ApVoucherPostController voidInvAdjustment");
    	
    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();
    			
    			Iterator i = invDistributionRecords.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    				
    				list.add(invDistributionRecord);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();    	    			    			

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(), 
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    			}	    
    			
    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();
    			
    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
    				
    				list.add(invAdjustmentLine);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();    	    			    			
    				
    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}
    			
    			invAdjustment.setAdjVoid(EJBCommon.TRUE);
    			System.out.println("AdjCode: " + invAdjustment.getAdjCode() + "MdfdBy: " + invAdjustment.getAdjLastModifiedBy() + "Branch: " + AD_BRNCH + "Company: " + AD_CMPNY);
    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH,
			Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	
    	/*Debug.print("ApVoucherPostController generateCostVariance");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try{ 

    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;

    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    		
    		try{
    			
    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
    			
    		} catch (FinderException ex) {
    			
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    			
    		}
    		
    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment,
    				CST_VRNC_VL, EJBCommon.FALSE, AD_CMPNY);
    		
    		// check for branch mapping
    		
    		LocalAdBranchItemLocation adBranchItemLocation = null;
    		
    		try{
    			
    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		LocalGlChartOfAccount glInventoryChartOfAccount = null;
    		
    		if (adBranchItemLocation == null) {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());
    			
    		}
    		
    		
    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;
    		
    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", 
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		this.executePrivInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}*/
    	
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	
    	/*Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance");
    	
    	try {
    		
    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			if(prevInvCosting.getCstRemainingQuantity() < 0) {
    				
    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
    					
    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
    					
    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
    					
    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
    						
    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
    						
    					}
    					
    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					
   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
   						
   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   							ADJ_RFRNC_NMBR = "ARCM" + 
							invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
    					
   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   							ADJ_RFRNC_NMBR = "ARMR" + 
							invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   						}
    					
    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" + 
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
    					
    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){
    					
    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
    					
    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
    						
    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() * 
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
    						
    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){
    					
    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
							
    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}
    					
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" + 
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();
    					
    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){
    					
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" + 
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){
    					
    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
    					
    				} else {
    					
    					prevInvCosting = invPropagatedCosting;
    					continue;
    					
    				}
    				
    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;
    				
    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());
    				
    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// set previous costing
    			prevInvCosting = invPropagatedCosting;
    			
    		}
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}*/        	
    }
    
    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte Al_VD, Integer AD_CMPNY) {
    	
    	Debug.print("ArMiscReceiptEntryControllerBean addInvAlEntry");
    	
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {
    		
    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL, null, null, 0,0, Al_VD, AD_CMPNY);
    		
    		// map adjustment, unit of measure, item location
    		//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvAdjustment(invAdjustment);
    		//invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
    		//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvItemLocation(invItemLocation);
    		
    		return invAdjustmentLine;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ApVoucherPostController saveInvAdjustment");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home    	
    	
    	try{
    	
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try{
    		
    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;
    		
    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
    		
    		try {
    			
    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		try {
    			
    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		while (true) {
    			
    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);		            		
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					break;
    					
    				}
    				
    			} else {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
    					break;
    					
    				}
    				
    			}
    			
    		}		            
    		
    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
    		
    		return invAdjustment;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	
    }
    
    private void executePrivInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,		
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvAdjustmentPosControllerBean executePrivInvAdjPost");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;        
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try {
    		
    		// validate if adjustment is already deleted
    		
    		LocalInvAdjustment invAdjustment = null;
    		
    		try {
    			
    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlobalRecordAlreadyDeletedException();
    			
    		}
    		
    		// validate if adjustment is already posted or void
    		
    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();
    			
    		}
    		
    		Collection invAdjustmentLines = null;
    		
    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);
    		
    		Iterator i = invAdjustmentLines.iterator();
    		
    		while(i.hasNext()) {
    			
    			
    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    			
				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);
    			
    		}			
    		
    		// post to gl if necessary
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    			// validate if date has no period and period is closed
    			
    			LocalGlSetOfBook glJournalSetOfBook = null;
    			
    			try {
    				
    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    				throw new GlJREffectiveDateNoPeriodExistException();
    				
    			}
    			
    			LocalGlAccountingCalendarValue glAccountingCalendarValue = 
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);
    			
    			
    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
    				
    				throw new GlJREffectiveDatePeriodClosedException();
    				
    			}
    			
    			// check if invoice is balance if not check suspense posting
    			
    			LocalGlJournalLine glOffsetJournalLine = null;
    			
    			Collection invDistributionRecords = null;
    			
    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			} else {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			}

    			
    			Iterator j = invDistributionRecords.iterator();
    			
    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    					
    					TOTAL_DEBIT += DR_AMNT;
    					
    				} else {
    					
    					TOTAL_CREDIT += DR_AMNT;
    					
    				}
    				
    			}
    			
    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			
    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				LocalGlSuspenseAccount glSuspenseAccount = null;
    				
    				try { 	
    					
    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);
    					
    				} catch (FinderException ex) {
    					System.out.println("here3: ");
    					throw new GlobalJournalNotBalanceException();
    					
    				}
    				
    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE, 
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
    					
    				} else {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
    					
    				}
    				
    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
    				
    				
    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				System.out.println("here4: ");
    				System.out.println("dbt: " + TOTAL_DEBIT + "crdt: " +  TOTAL_CREDIT);
    				throw new GlobalJournalNotBalanceException();		    	
    				
    			}
    			
    			// create journal batch if necessary
    			
    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
    			
    			try {
    				
    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			if (glJournalBatch == null) {
    				
    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// create journal entry			            	
    			
    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null, 
						AD_BRNCH, AD_CMPNY);
    			
    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);
    			
    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
    			
    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);
    			
    			if (glJournalBatch != null) {
    				
    				glJournal.setGlJournalBatch(glJournalBatch);
    				
    			}
    			// create journal lines
    			
    			j = invDistributionRecords.iterator();
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
    				
    				//invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());
    				
    				//glJournal.addGlJournalLine(glJournalLine);
    				glJournalLine.setGlJournal(glJournal);
    				
    				invDistributionRecord.setDrImported(EJBCommon.TRUE);
    				
    				
    			}
    			
    			if (glOffsetJournalLine != null) {
    				
    				//glJournal.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlJournal(glJournal);
    				
    			}		
    			
    			// post journal to gl
    			
    			Collection glJournalLines = glJournal.getGlJournalLines();
    			
    			i = glJournalLines.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
    				
    				// post current to current acv
    				
    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    				
    				
    				// post to subsequent acvs (propagate)
    				
    				Collection glSubsequentAccountingCalendarValues = 
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
    				
    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
    				
    				while (acvsIter.hasNext()) {
    					
    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    						(LocalGlAccountingCalendarValue)acvsIter.next();
    					
    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    					
    				}
    				
    				// post to subsequent years if necessary
    				
    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
    				
    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
    					
    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY); 	  	
    					
    					Iterator sobIter = glSubsequentSetOfBooks.iterator();
    					
    					while (sobIter.hasNext()) {
    						
    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
    						
    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
    						
    						// post to subsequent acvs of subsequent set of book(propagate)
    						
    						Collection glAccountingCalendarValues = 
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
    						
    						Iterator acvIter = glAccountingCalendarValues.iterator();
    						
    						while (acvIter.hasNext()) {
    							
    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    								(LocalGlAccountingCalendarValue)acvIter.next();
    							
    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    								
    							} else { // revenue & expense
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
    								
    							}
    							
    						}
    						
    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
    						
    					}
    					
    				}
    				
    			}
			
			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlJREffectiveDatePeriodClosedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalJournalNotBalanceException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalRecordAlreadyDeletedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalTransactionAlreadyPostedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {
    	
    	Debug.print("InvAdjustmentPostControllerBean postInvAdjustmentToInventory");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
    		
    	} catch (NamingException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;
    		
    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adPreference.getPrfInvCostPrecisionUnit());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adPreference.getPrfInvCostPrecisionUnit());
    		
    		if (CST_ADJST_QTY < 0) {
    			
    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
    			
    		}
    		
    		String prevExpiryDates = "";
            String miscListPrpgt ="";
            double qtyPrpgt = 0;
            try {           
         	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
         			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
         	   
         	   prevExpiryDates = prevCst.getCstExpiryDate();
         	   qtyPrpgt = prevCst.getCstRemainingQuantity();
         	   if (prevExpiryDates==null){
         		   prevExpiryDates="";
         	   }

            }catch (Exception ex){
         	   
            }
            
    		// create costing
    		
    		try {
    			
    			// generate line number
    			
    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
    			
    		} catch (FinderException ex) {
    			
    			CST_LN_NMBR = 1;
    			
    		}
    		
    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		//invItemLocation.addInvCosting(invCosting);
    		invCosting.setCstQCNumber(invAdjustmentLine.getAlQcNumber());
    		invCosting.setCstQCExpiryDate(invAdjustmentLine.getAlQcExpiryDate());
    		invCosting.setInvItemLocation(invItemLocation);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);
    		
//          Get Latest Expiry Dates           

            if(prevExpiryDates!=""){

 			   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
 				   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
 				   
 				   String miscList2Prpgt = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), qty2Prpgt, "False");
 				   ArrayList miscList = this.expiryDates(invAdjustmentLine.getAlMisc(), qty2Prpgt);
 				   String propagateMiscPrpgt = "";
 				   String ret = "";
				   String Checker = "";
 				  //ArrayList miscList2 = null;
 				   if(CST_ADJST_QTY>0){
 					  prevExpiryDates = prevExpiryDates.substring(1);
 					   propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
 				   }else{
 					   Iterator mi = miscList.iterator();
 					   
 					   propagateMiscPrpgt = prevExpiryDates;
 					  ret = propagateMiscPrpgt;
 					  while(mi.hasNext()){
 						  String miscStr = (String)mi.next();
 						  /*propagateMiscPrpgt = propagateMiscPrpgt.replace("$", " ");
 						   propagateMiscPrpgt = propagateMiscPrpgt.replaceFirst(miscStr, " ");
 						   System.out.println("propagateMiscPrpgt MINUS= " + propagateMiscPrpgt);*/
 						  ArrayList miscList2 = this.expiryDates("$" + ret, qtyPrpgt);
 						  Iterator m2 = miscList2.iterator();
 						  ret = "";
 						  String ret2 = "false";
 						  int a = 0;
 						  while(m2.hasNext()){
 							  String miscStr2 = (String)m2.next();

 							  if(ret2=="1st"){
 								 ret2 = "false";
 							  }
 							  
 							  if(miscStr2.equals(miscStr)){
 								  if(a==0){
 									  a = 1;
 									  ret2 = "1st";
 									  Checker = "true";
 								  }else{
 									  a = a+1;
 									  ret2 = "true";
 								  }
 							  }
 							  
 							  if(!miscStr2.equals(miscStr) || a>1){
 								  if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
 									  if (miscStr2!=""){
 										  miscStr2 = "$" + miscStr2;
 										  ret = ret + miscStr2;
 										  ret2 = "false";
 									  }
 								  }
 							  }
 							  
 						  }
 						  ret = ret + "$";
 						  qtyPrpgt= qtyPrpgt -1;
 					  }
 					   //propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
 					  propagateMiscPrpgt = ret;
 					   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
 				   }

 				  if(Checker=="true"){
 					  invCosting.setCstExpiryDate(propagateMiscPrpgt);
 				   }else{
 					   throw new GlobalExpiryDateNotFoundException();
 				   }
 				   
 			   }else{
 				   invCosting.setCstExpiryDate(prevExpiryDates);
 			   }
 			   
            }else{
         	   
         	   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
 			   String initialPrpgt = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), initialQty, "False");
 			   
         	   invCosting.setCstExpiryDate(initialPrpgt);
            }
    		
    		// propagate balance if necessary           
    		
    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    		
    		Iterator i = invCostings.iterator();
    		
    		double qty = Double.parseDouble(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
    		String miscList = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), qty, "False");

    		String propagateMisc ="";
    		String ret = "";
    		String Checker = "";
    		ArrayList miscList2 = this.expiryDates(invAdjustmentLine.getAlMisc(), qty);
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			System.out.println("invPropagatedCosting.getCstRemainingQuantity(): " + invPropagatedCosting.getCstRemainingQuantity());
    			System.out.println("CST_ADJST_QTY(): " + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
    			
    			if(CST_ADJST_QTY>0){
    				miscList = miscList.substring(1);
    				propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
    			}else{
    				Iterator mi = miscList2.iterator();

    				propagateMisc = prevExpiryDates;
    				ret = propagateMisc;
    				while(mi.hasNext()){
    					String miscStr = (String)mi.next();
    					ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
    					Iterator m2 = miscList3.iterator();
    					ret = "";
    					String ret2 = "false";
    					int a = 0;
    					while(m2.hasNext()){
    						String miscStr2 = (String)m2.next();

    						if(ret2=="1st"){
    							ret2 = "false";
    						}

    						if(miscStr2.equals(miscStr)){
    							if(a==0){
    								a = 1;
    								ret2 = "1st";
    								Checker = "true";
    							}else{
    								a = a+1;
    								ret2 = "true";
    							}
    						}

    						if(!miscStr2.equals(miscStr) || a>1){
    							if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
    								if (miscStr2!=""){
    									miscStr2 = "$" + miscStr2;
    									ret = ret + miscStr2;
    									ret2 = "false";
    								}
    							}
    						}

    					}
    					ret = ret + "$";
    					qtyPrpgt= qtyPrpgt -1;
    				}
    				propagateMisc = ret;
    			}
    			if(Checker=="true"){
    				invPropagatedCosting.setCstExpiryDate(propagateMisc);
    			}else{
    				throw new GlobalExpiryDateNotFoundException();
    			}
    			
    		}                           
    		
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	
    }
    private double getInvFifoCost2(Date CST_DT, Integer IL_CODE, double CST_QTY, 
      		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
   	 {
   	  	 
   		 LocalInvCostingHome invCostingHome = null;
   	  	 LocalInvItemLocationHome invItemLocationHome = null;
   	  	 LocalAdPreferenceHome adPreferenceHome = null;
   	       int isNotSingle=0;
   	     // Initialize EJB Home
   	        
   	     try {
   	         
   	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
   	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
   	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
   	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
   	    	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
   	    	 	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
   	     } 
   	     catch (NamingException ex) {
   	            
   	    	 throw new EJBException(ex.getMessage());
   	     }
   	    	
   		try {
   			
   			 CostA=0d;
   			 CostB=0d;
   			 UnitA=0d;
   			 UnitB=0d;
   			CostAmount[0]=0d;
   			CostAmount[1]=0d;
   			CostAmount[2]=0d;
   			CostAmount[3]=0d;
   			CostAmount[4]=0d;
   			CostAmount[5]=0d;
   			CostAmount[6]=0d;
   			CostAmount[7]=0d;
   			CostAmount[8]=0d;
   			CostAmount[9]=0d;
   			UnitValue[0]=0d;
   			UnitValue[1]=0d;
   			UnitValue[2]=0d;
   			UnitValue[3]=0d;
   			UnitValue[4]=0d;
   			UnitValue[5]=0d;
   			UnitValue[6]=0d;
   			UnitValue[7]=0d;
   			UnitValue[8]=0d;
   			UnitValue[9]=0d;
   			
   			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
   			System.out.println("TEST " + CST_DT + " " + IL_CODE + " " + CST_QTY + " " + AD_BRNCH + " " + AD_CMPNY);
   			if (invFifoCostings.size() > 0) {
   				double fifoCost = 0;
   			    double runningQty = Math.abs(CST_QTY);
   			    double xStart=runningQty;
   			    double value=runningQty;
   			    double costvalue=0d;
   			    double xStartCost=0d;
   			    double totalUnitCommited=0d;
   			   System.out.println("Start ----:" + CST_QTY );
   				Iterator x = invFifoCostings.iterator();
   				while (x.hasNext()) {
   					
   					LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
   	  				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
   	  				double newRemainingLifoQuantity = 0;
   	  				if (invFifoCosting.getCstRemainingLifoQuantity() <= runningQty) {
   	  					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
   	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * invFifoCosting.getCstRemainingLifoQuantity();
   		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
   		 	  				fifoCost  += invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold() * invFifoCosting.getCstRemainingLifoQuantity();
   	 	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
   	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * invFifoCosting.getCstRemainingLifoQuantity();
   	 	 	  			} else {
   	 	 	  				fifoCost += invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity() * invFifoCosting.getCstRemainingLifoQuantity();
   	 	 	  			}
   	  					
   	  					runningQty = runningQty - invFifoCosting.getCstRemainingLifoQuantity();
   	  					//System.out.println("runningQty ----:" + runningQty );
   	  					double xxxxx=Math.abs(CST_QTY)-runningQty;
   	  					costvalue=fifoCost-xStartCost;
   	  					System.out.println("cost ----:" + costvalue );
   	  					xStartCost+=costvalue;
   	  					System.out.println("costvalue ----:" + xStartCost );
   	  					value=xStart-runningQty;
   	  					//System.out.println("value ----:" + value );
   	  					xStart=runningQty;
   	  					totalUnitCommited+=value;
   	  					//System.out.println("xStart ----:" + xStart );
   	  					//System.out.println("runningQty ----:" + runningQty );
   	  					//System.out.println("xxxxx ----:" + xxxxx );
   	  					//System.out.println("Unit ----:" + xxxxx );
   	  					//System.out.println("Cost ---:" + fifoCost);
   	  					CostA=fifoCost;
   	  					UnitA=xxxxx;

   	  					CostAmount[isNotSingle]=costvalue;
   	  					UnitValue[isNotSingle]=value;
   	  					invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity()-value);
   	  					System.out.println("invFifoCosting.getCstRemainingLifoQuantity() ----:" + invFifoCosting.getCstRemainingLifoQuantity() );
   	  					System.out.println("commited() ----:" + value );	
   	  					
   	  					System.out.println("Unit ----:" + UnitValue[isNotSingle] );
   	  					System.out.println("Cost ---:" + CostAmount[isNotSingle]);
   	  					System.out.println("isNotSingle" + isNotSingle);
   	  					isNotSingle++;
   	  				} else {
   	  					System.out.println("CostForward ---:" + fifoCost);
   	  					double fifoCost2=0d;
   	  					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
   	  					fifoCost2 += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * runningQty;
   		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
   		 	  			fifoCost2  += invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold() * runningQty;
   	 	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
   	 	 	  			fifoCost2 += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * runningQty;
   	 	 	  			} else {
   	 	 	  			fifoCost2 += invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity() * runningQty;
   	 	 	  			}
   	  					newRemainingLifoQuantity = invFifoCosting.getCstRemainingLifoQuantity() - runningQty;
   	  					
   	  					//fifoCost2=fifoCost -fifoCost2;
   	  					//System.out.println("Unit ----:" + runningQty);
   	  					//System.out.println("Cost ---:" + fifoCost2);
   	  					CostB=fifoCost2;
   	  					System.out.println("fifoCost2 ----:" + fifoCost2 );
   	  					UnitB=runningQty;
   	  					invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity()-runningQty);
   	  					System.out.println("invFifoCosting.getCstRemainingLifoQuantity() ----:" + invFifoCosting.getCstRemainingLifoQuantity() );
	  					System.out.println("commited() ----:" + runningQty );
	  					
	  				
	  					
	  					
   	  					if(isNotSingle==0)
	  					{
	  						CostAmount[isNotSingle]=fifoCost2;
	  						UnitValue[isNotSingle]=runningQty;
	  					}
	  					else
	  					{
	  					CostAmount[isNotSingle]=fifoCost2;
	  					UnitValue[isNotSingle]=runningQty;
	  					}
   	  						totalUnitCommited+=runningQty;
   	  					System.out.println("UnitA ----:" + UnitValue[isNotSingle] );
	  					System.out.println("CostA---:" + CostAmount[isNotSingle]);
	  					System.out.println("isNotSingle" + isNotSingle);
   	  					runningQty = 0;
   	  					isNotSingle++;
   	  				}
   	  				
   	 	  			if (isAdjustFifo) 
   	 	  				{
   	 	  				//invFifoCosting.setCstRemainingLifoQuantity(newRemainingLifoQuantity);
   	 	  			System.out.println("Commited value" + totalUnitCommited);
   	 	  				}
   					if (runningQty <=0)
   						
   						break;
   						
   				}
   				//System.out.println("fifoCost" + fifoCost);
   				//System.out.println("CST_QTY" + CST_QTY);
   				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
   				System.out.println("---------------------->isNotSingle" + isNotSingle);
   				return isNotSingle;
   				
   			} 
   			else {
   				
   				//most applicable in 1st entries of data
   				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
   				System.out.println("---------------------->isNotSingle" + isNotSingle);
   				return isNotSingle;
   			}
   				
   		}
   		catch (Exception ex) {
   			Debug.printStackTrace(ex);
   		    throw new EJBException(ex.getMessage());
   		}
   	}    
// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvAdjustmentPostControllerBean ejbCreate");
      
    }
}
