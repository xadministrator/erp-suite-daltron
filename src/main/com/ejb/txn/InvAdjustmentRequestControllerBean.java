package com.ejb.txn;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
//import com.struts.util.UserRes;
//import com.struts.util.Common;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvAdjustmentDetails;
import com.util.InvModAdjustmentDetails;
import com.util.InvModAdjustmentLineDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="InvAdjustmentRequestControllerEJB"
 *           display-name="used for adjusting inventory quantity-on-hand"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvAdjustmentRequestControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvAdjustmentRequestController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvAdjustmentRequestControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvAdjustmentRequestControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModAdjustmentDetails getInvPrfDefaultAdjustmentRequestAccount(Integer AD_CMPNY, Integer USR_CODE) {
    	
    	Debug.print("InvAdjustmentRequestControllerBean getInvPrfDefaultAdjustmentRequestAccount");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdUserHome adUserHome = null;
    	
        // Initialize EJB Home
        
        try {
        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        	adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
    	
        InvModAdjustmentDetails details = new InvModAdjustmentDetails();
        
        try {
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvPosAdjustmentAccount());
        	LocalAdUser adUser = adUserHome.findByPrimaryKey(USR_CODE);        	
        	LocalGlChartOfAccount glChartOfAccount2 = glChartOfAccountHome.findByCoaAccountNumber(glChartOfAccount.getCoaAccountNumber().substring(0, glChartOfAccount.getCoaAccountNumber().indexOf("-"))+"-"+adUser.getUsrDept(), AD_CMPNY);
        	details.setAdjCoaAccountNumber(glChartOfAccount2.getCoaAccountNumber());
        	details.setAdjCoaAccountDescription(glChartOfAccount2.getCoaAccountDescription());
        }
        catch (FinderException ex) {
        }
    	return details;
    }
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {
    	  System.out.print("printC1");         
        Debug.print("InvAdjustmentRequestControllerBean getInvLocAll");
        
        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
	               
            invLocations = invLocationHome.findLocAll(AD_CMPNY);            
        
	        if (invLocations.isEmpty()) {
	        	
	        	return null;
	        	
	        }
	        
	        Iterator i = invLocations.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalInvLocation invLocation = (LocalInvLocation)i.next();	
	        	String details = invLocation.getLocName();
	        	
	        	list.add(details);
	        	
	        }
	        
	        return list;
        
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
  public double getQuantityByIiNameAndUomName(String II_NM, String LOC_NM, String UOM_NM, Date PI_DT, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("InvAdjustmentRequestControllerBean getQuantityByIiNameAndUomName");
        
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
        System.out.print("printC2");
        // Initialize EJB Home
        double x=0;
        try {
        	invAdjustmentLineHome=(LocalInvAdjustmentLineHome)EJBHomeFactory.
        	lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);       
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);        	  
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);  
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);        	  
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        //double actualQuantity = 0d;
        try {
            
 

           double actualQuantity = 0d;
            
            LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(II_NM, LOC_NM, AD_CMPNY);
            
            try {
            	System.out.println("old PI_DT : " + PI_DT);
            	PI_DT = new java.util.Date();
                LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(PI_DT, 
                        invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                System.out.println("actualPI_DT : " + PI_DT);
                actualQuantity = invCosting.getCstRemainingQuantity();
                System.out.println("actual : " + actualQuantity);
                
                
            } catch (FinderException ex) {
                
            }
            
        	//LocalInvAdjustmentLine adjustmentRequestLine= invAdjustmentLineHome.findByAlCode(mAlDetails.getAlCode(),user.getCmpCode());
          	
   
          	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);            
            System.out.println("actual : " + actualQuantity);
            
            /*
           	double hasUnposted = this.getUnpostedItem(invItemLocation.getInvItem().getIiCode(),AD_BRNCH,AD_CMPNY);
          	System.out.print("hasUnposted = "+ hasUnposted);
          	hasUnposted = hasUnposted*-1;
          	double val = actualQuantity-hasUnposted;
          	
          	*/
          	x = EJBCommon.roundIt(actualQuantity * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor(), this.getInvGpQuantityPrecisionUnit(AD_CMPNY));
          	System.out.print("x = "+ x);
        } catch (Exception ex) {
        	System.out.println("s");
          //  Debug.printStackTrace(ex);
          //  throw new EJBException(ex.getMessage());
            
        }
        return x;
        //return EJBCommon.roundIt(actualQuantity * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor(), this.getInvGpQuantityPrecisionUnit(AD_CMPNY));
    }
  
    /** 
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/   
   
    public InvModAdjustmentDetails getInvAdjByAdjCode(Integer ADJ_CODE, Integer AD_BRNCH, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException {
    	  System.out.print("printC3");
        Debug.print("InvAdjustmentRequestControllerBean getInvAdjByAdjCode");
        
        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalInvTagHome invTagHome = null;
     
        // Initialize EJB Home
        
        try {
            
        	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
        	invTagHome = (LocalInvTagHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalInvAdjustment invAdjustment = null;
        
	        try {
		               
	        	invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
	            
	        } catch (FinderException ex) {
	            
	            throw new GlobalNoRecordFoundException();
	            
	        }
	        
	        String specs = null;
        	String propertyCode = null;
        	String expiryDate = null;
        	String serialNumber = null;
	        ArrayList alList = new ArrayList();
	        
	        Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
	        
	        Iterator i = invAdjustmentLines.iterator();
	       
	        while (i.hasNext()) {
	            System.out.print("1");
	        	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
	        	
	        	InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();
	        	ArrayList tagList = new ArrayList();
	        	mdetails.setAlCode(invAdjustmentLine.getAlCode());
	        	mdetails.setAlUnitCost(invAdjustmentLine.getAlUnitCost());
	        	Collection invTags = invTagHome.findByAlCode(invAdjustmentLine.getAlCode(),AD_CMPNY);   
    			Iterator x = invTags.iterator();
    			
    			while (x.hasNext()) {
    				LocalInvTag invTag = (LocalInvTag) x.next();
    				InvModTagListDetails tgLstDetails = new InvModTagListDetails();
    				tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
    				tgLstDetails.setTgSpecs(invTag.getTgSpecs());
    				tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
    				tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
    				try{
    					
    					tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
    				}
    				catch(Exception ex){
    					tgLstDetails.setTgCustodian("");
    				}
    				
    				tagList.add(tgLstDetails);
    				
    				System.out.println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean getApPoByPoCode");
    				System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean getApPoByPoCode");
    				System.out.println(tagList+ "<== taglist inside controllerbean getApPoByPoCode");
    			
    			}
    			
	        	
	        	System.out.print(invAdjustmentLine.getAlAdjustQuantity()+"Controller Adjust");
	        	//double adjQuantity=invAdjustmentLine.getAlAdjustQuantity()+invAdjustmentLine.getAlServed();
	        	//mdetails.setAlAdjustQuantity(adjQuantity);
	        	
	        	System.out.print(invAdjustmentLine.getAlAdjustQuantity()+"Controller Adjust2");
	        	mdetails.setAlUomName(invAdjustmentLine.getInvUnitOfMeasure().getUomName());
	        	mdetails.setAlLocName(invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName());
	        	mdetails.setAlIiName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
	        	mdetails.setAlIiDescription(invAdjustmentLine.getInvItemLocation().getInvItem().getIiDescription());
	        	System.out.print("herer 1"+invAdjustmentLine.getInvItemLocation().getInvItem().getIiName()+" <-IM "+  invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName()+"<-LOC"+ invAdjustmentLine.getInvUnitOfMeasure().getUomName()+"<-GU"+invAdjustment.getAdjDate()+"<-GD"+AD_BRNCH+"<-BR"+AD_CMPNY+"<-US");     
	        	if (invAdjustmentLine.getInvItemLocation().getInvItem().getIiName()!=null && invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName()!=null && invAdjustmentLine.getInvUnitOfMeasure().getUomName()!=null && invAdjustment.getAdjDate()!=null && AD_BRNCH!=null && AD_CMPNY!=null) 
	        	{  
	        			Date today = Calendar.getInstance().getTime();		
	           		 String dateNow2= EJBCommon.convertSQLDateToString(today);
	           		 System.out.println("Now the date is : =>  " + dateNow2);
	           		
	        		double actual1=this.getQuantityByIiNameAndUomName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(), invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(),invAdjustmentLine.getInvUnitOfMeasure().getUomName(), EJBCommon.convertStringToSQLDate(dateNow2), AD_BRNCH, AD_CMPNY);
	        
	     	    System.out.print(actual1);
	        	 //Double actual2=actual1-invAdjustmentLine.getAlAdjustQuantity();
	        	//	 if(actual2>0)
	     	    mdetails.setAlServed(invAdjustmentLine.getAlServed());
	     	    mdetails.setAlActualQuantity(actual1);
	     	    mdetails.setAlActualQuantityFind(actual1);}
	        	mdetails.setAlAdjustQuantity(invAdjustmentLine.getAlAdjustQuantity()+mdetails.getAlServed());
	        	// 	 }
	        	//  	 else
	        	//   	 {mdetails.setAlActualQuantity(actual1);}
	        	mdetails.setAlMisc(invAdjustmentLine.getAlMisc());
	        	mdetails.setAlTagList(tagList);
	        	alList.add(mdetails);
	        }
	        
	        InvModAdjustmentDetails details = new InvModAdjustmentDetails();
	        details.setAdjCode(invAdjustment.getAdjCode());
	        details.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
	        details.setAdjReferenceNumber(invAdjustment.getAdjReferenceNumber());
	        details.setAdjDescription(invAdjustment.getAdjDescription());
	        details.setAdjDate(invAdjustment.getAdjDate());
	        details.setAdjType(invAdjustment.getAdjType());
	        details.setAdjApprovalStatus(invAdjustment.getAdjApprovalStatus());
	        details.setAdjPosted(invAdjustment.getAdjPosted());
	        details.setAdjCreatedBy(invAdjustment.getAdjCreatedBy());
	        details.setAdjDateCreated(invAdjustment.getAdjDateCreated());
	        details.setAdjLastModifiedBy(invAdjustment.getAdjLastModifiedBy());
	        details.setAdjDateLastModified(invAdjustment.getAdjDateLastModified());
	        details.setAdjApprovedRejectedBy(invAdjustment.getAdjApprovedRejectedBy());
	        details.setAdjDateApprovedRejected(invAdjustment.getAdjDateApprovedRejected());
	        details.setAdjPostedBy(invAdjustment.getAdjPostedBy());
	        details.setAdjDatePosted(invAdjustment.getAdjDatePosted());
	        details.setAdjCoaAccountNumber(invAdjustment.getGlChartOfAccount().getCoaAccountNumber());
	        details.setAdjCoaAccountDescription(invAdjustment.getGlChartOfAccount().getCoaAccountDescription());			    
	        details.setAdjReasonForRejection(invAdjustment.getAdjReasonForRejection());
	        details.setAdjIsCostVariance(invAdjustment.getAdjIsCostVariance());
	        details.setAdjVoid(invAdjustment.getAdjVoid());
	        details.setAdjAlList(alList);
	        
	        return details;
	      
	  
        } catch (GlobalNoRecordFoundException ex) {
        	
        	Debug.printStackTrace(ex);
        	throw ex;
        	
    	} catch (Exception ex) {
        	
    		Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {
                    
        Debug.print("ApReceivingItemEntryControllerBean getAdUsrAll");
        
        LocalAdUserHome adUserHome = null;
                
        LocalAdUser adUser = null;
        
        Collection adUsers = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adUsers = adUserHome.findUsrAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adUsers.isEmpty()) {
        	
            return null;
        	
        }
        
        Iterator i = adUsers.iterator();
               
        while (i.hasNext()) {
        	
            adUser = (LocalAdUser)i.next();
        	
            list.add(adUser.getUsrName());
        	
        }
        
        return list;
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public double getInvAdjusmentByReferenceNumber(int item,String ADJ_RFRNC_NMBR,Integer AD_BRNCH,Integer AD_CMPNY) {
                    
        Debug.print("InvAdjustmentRequestControllerBean getInvAdjusmentByReferenceNumber");
        double hasUnposted = 0;    
        LocalAdUserHome adUserHome = null;
                
        LocalAdUser adUser = null;
        
        Collection InvAdjustments = null;
        LocalInvAdjustment InvAdjustment = null;
        Collection InvAdjustmentLines = null;
        
        ArrayList list = new ArrayList();
        
        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
        LocalInvTagHome invTagHome = null;
     
        // Initialize EJB Home
        
        try {
            
        	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
        	invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
        	invTagHome = (LocalInvTagHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        System.out.print("itemLocation = "+ item);
        try {
           
        	
        	InvAdjustments = invAdjustmentHome.findByAdjReferenceNumber(ADJ_RFRNC_NMBR, AD_BRNCH, AD_CMPNY);
        	
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        

        
        Iterator i = InvAdjustments.iterator();
           
        while (i.hasNext()) {
        	
        	LocalInvAdjustment invAdjstmnet = (LocalInvAdjustment)i.next();
        	String adjCode = invAdjstmnet.getAdjDocumentNumber();
        	System.out.print("adjCode = "+ adjCode);
        	try {
        	InvAdjustmentLines = invAdjustmentLineHome.findUnpostedReference(adjCode, AD_BRNCH, AD_CMPNY);
        	 } 
        	catch (FinderException ex) {}
        	
        	Iterator x = InvAdjustmentLines.iterator();
        	
        	while (x.hasNext()) 
        	{
        		
        	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)x.next();
        	
        	if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiCode()==item)
        		{
        		System.out.print("invAdjustmentLine = "+ invAdjustmentLine.getInvAdjustment().getAdjDocumentNumber());
        		System.out.print("getAlAdjustQuantity = "+ invAdjustmentLine.getAlAdjustQuantity());
        		hasUnposted+=invAdjustmentLine.getAlAdjustQuantity();
        		}
        	}
        	
        }
        
        return hasUnposted;
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public double getUnpostedItem(Integer item,Integer AD_BRNCH,Integer AD_CMPNY) {
                    
        Debug.print("InvAdjustmentRequestControllerBean getInvAdjusmentByReferenceNumber");
        double hasUnposted = 0;    
        LocalAdUserHome adUserHome = null;
                
        LocalAdUser adUser = null;
        
      
       
        Collection InvAdjustmentLines = null;
        
        ArrayList list = new ArrayList();
        
        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
  
     
        // Initialize EJB Home
        
        try {
            

        	invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
        	

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

        System.out.print("itemLocation = "+ item);
        try {
           
        	
        	InvAdjustmentLines = invAdjustmentLineHome.findUnpostedByItem(item, AD_BRNCH, AD_CMPNY);
        	
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        

        
        Iterator x = InvAdjustmentLines.iterator();
           
        while (x.hasNext()) {
        		
        	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)x.next();
        	
        	if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiCode()==item)
        		{
        		System.out.print("invAdjustmentLineDocNum = "+ invAdjustmentLine.getInvAdjustment().getAdjDocumentNumber());
        		System.out.print("getAlAdjustQuantity = "+ invAdjustmentLine.getAlAdjustQuantity());
        		hasUnposted+=invAdjustmentLine.getAlAdjustQuantity();
        		}
        	}
        return hasUnposted;
        }
        
        
            
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {
    	
    	Debug.print("InvAdjustmentRequestControllerBean getInvUomByIiName");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemHome invItemHome = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        System.out.print("printC5");
        try {
            
        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
        	
        	LocalInvItem invItem = null;
        	LocalInvUnitOfMeasure invItemUnitOfMeasure = null;
        	
        	invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);        	
        	invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();
        	
        	Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
        			invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
        	while (i.hasNext()) {
        		
        		LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
        		InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
        		details.setUomName(invUnitOfMeasure.getUomName());
        		   System.out.print("printC5");
        		if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {
        			
        			details.setDefault(true);
        			
        		}
        		
        		list.add(details);
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
	
    }

    /**
    * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvAdjEntryMobile(com.util.InvAdjustmentDetails details, String COA_ACCOUNT_NUMBER,
		ArrayList alList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalRecordAlreadyDeletedException,
		GlobalBranchAccountNumberInvalidException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalInvItemLocationNotFoundException,		
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalDocumentNumberNotUniqueException,
		GlobalInventoryDateException,
		GlobalAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException,
		GlobalExpiryDateNotFoundException,
		GlobalMiscInfoIsRequiredException,
		GlobalRecordInvalidException{
    	
    	Debug.print("InvAdjustmentRequestControllerBean saveInvAdjEntry");
    	  System.out.print("printC7");
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdApprovalHome adApprovalHome = null;
    	LocalAdAmountLimitHome adAmountLimitHome = null;
    	LocalAdApprovalUserHome adApprovalUserHome = null;
    	LocalAdApprovalQueueHome adApprovalQueueHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	//LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdUserHome adUserHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null; 
    	LocalInvItemHome invItemHome = null;
    	LocalInvTagHome invTagHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	LocalInvLocationHome invLocationHome = null;
    	
        // Initialize EJB Home
        
        try {
            
        	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        	adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
        	adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
        	adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
        	adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
        	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);        	
        	//invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			//	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
        	
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);	
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class); 
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
	            	lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
	            	lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
	            	lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
        	
        	LocalInvAdjustment invAdjustment = null;
        	
        	// validate if Adjustment is already deleted
        	
        	try {
        		
        		if (details.getAdjCode() != null) {
        			
        			invAdjustment = invAdjustmentHome.findByPrimaryKey(details.getAdjCode());
        			
        		}
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if adjustment is already posted, void, approved or pending
        	
        	if (details.getAdjCode() != null) {
	        	
	        	
	      	if (invAdjustment.getAdjApprovalStatus() != null) {
        		
	        	
	        		
	        		
	        		if (invAdjustment.getAdjApprovalStatus().equals("APPROVED") ||
	        			invAdjustment.getAdjApprovalStatus().equals("N/A")) {         		    	
	        		    throw new GlobalTransactionAlreadyApprovedException(); 
	        		    	
	        		    	
	       		} else if (invAdjustment.getAdjApprovalStatus().equals("PENDING")) {
	        			
	        			throw new GlobalTransactionAlreadyPendingException();
	        			
	       		}
	       		
	       	}
        			
        	if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {
        			
        			throw new GlobalTransactionAlreadyPostedException();
        			
        		}
        		
        	}
        	
        	LocalInvAdjustment invExistingAdjustment = null;

        	try {
        		
        		invExistingAdjustment = invAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);
        		
        	} catch (FinderException ex) { 
        		
        	}
        	
        	
        	// 	validate if document number is unique document number is automatic then set next sequence

	        if (details.getAdjCode() == null) {
	        	
	        	
	        	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		        LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
	        	
	        	if (invExistingAdjustment != null) {
	        		
	        		throw new GlobalDocumentNumberNotUniqueException();
	        		
	        	}
	        	
	        	try {
	        		
	        		adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT REQUEST", AD_CMPNY);
	        		
	        	} catch (FinderException ex) {
	        		
	        	}
	        	
	        	try {
	        		
	        		adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
	        		
	        	} catch (FinderException ex) {
	        		
	        	}

	        	if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
	        			(details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {
	        		
	        		while (true) {
	        			
	        			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
	        				
	        				try {

	        					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);		            		
	        					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
	        					
	        				} catch (FinderException ex) {
	        					
	        					details.setAdjDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
	        					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
	        					break;
	        					
	        				}
	        				
 						} else {
 							
 							try {
 								
 								invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
 								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
 								
 							} catch (FinderException ex) {
 								
 								details.setAdjDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
 								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
 								break;
 								
 							}
 							
 						}

	        		}		            
	        		
	        	}
	        	
	        } else {
		    			    			    	

	        	if (invExistingAdjustment != null && 
	                !invExistingAdjustment.getAdjCode().equals(details.getAdjCode())) {
	            	
	            	throw new GlobalDocumentNumberNotUniqueException();
	            	
	            }
	            
	            if (invAdjustment.getAdjDocumentNumber() != details.getAdjDocumentNumber() &&
	                (details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {
	                	
	                details.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
	                	
	         	}
		    	
		    }
        	
        	// used in checking if invoice should re-generate distribution records and re-calculate taxes	        	                	
	        boolean isRecalculate = true;
	        
        	
        	// create adjustment
        	
        	if (details.getAdjCode() == null) {
        		
        		invAdjustment = invAdjustmentHome.create(details.getAdjDocumentNumber(), details.getAdjReferenceNumber(), details.getAdjDescription(), 
        				details.getAdjDate(), details.getAdjType(), details.getAdjApprovalStatus(),
						EJBCommon.FALSE, details.getAdjCreatedBy(), details.getAdjDateCreated(),
						details.getAdjLastModifiedBy(), details.getAdjDateLastModified(),
						null, null, null, null, null, null, EJBCommon.FALSE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
        		
        	} else {
        		
                // check if critical fields are changed
        		
        		if (!invAdjustment.getGlChartOfAccount().getCoaAccountNumber().equals(COA_ACCOUNT_NUMBER) ||
					alList.size() != invAdjustment.getInvAdjustmentLines().size() ||
					!(invAdjustment.getAdjDate().equals(details.getAdjDate()))) {
        			
        			isRecalculate = true;
        			
        		} else if (alList.size() == invAdjustment.getInvAdjustmentLines().size()) {
        			
        			Iterator alIter = invAdjustment.getInvAdjustmentLines().iterator();
        			Iterator alListIter = alList.iterator();
        			
        			while (alIter.hasNext()) {
        				
        				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)alIter.next();
        				InvModAdjustmentLineDetails mdetails = (InvModAdjustmentLineDetails)alListIter.next();
        				      System.out.print("controller"+mdetails.getAlAdjustQuantity());  				
        				if (!invAdjustmentLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getAlIiName()) ||
            				!invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getAlLocName()) ||
	        				!invAdjustmentLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getAlUomName()) ||
	        				invAdjustmentLine.getAlAdjustQuantity() != mdetails.getAlAdjustQuantity() ||
	        				invAdjustmentLine.getAlUnitCost() != mdetails.getAlUnitCost() || 
	        				invAdjustmentLine.getAlMisc() != mdetails.getAlMisc()) {
        					
        					isRecalculate = true;
        					break;
        					
        				}
        				
        				isRecalculate = false;
        				
        			}
        			
        		} else {
        			
        			isRecalculate = false;
        			
        		}
        		
        		invAdjustment.setAdjDocumentNumber(details.getAdjDocumentNumber());
        		invAdjustment.setAdjReferenceNumber(details.getAdjReferenceNumber());
        		invAdjustment.setAdjDescription(details.getAdjDescription());
        		invAdjustment.setAdjDate(details.getAdjDate());
        		invAdjustment.setAdjType(details.getAdjType());
        		invAdjustment.setAdjApprovalStatus(details.getAdjApprovalStatus());
        		invAdjustment.setAdjLastModifiedBy(details.getAdjLastModifiedBy());
        		invAdjustment.setAdjDateLastModified(details.getAdjDateLastModified());
        		
        		invAdjustment.setAdjReasonForRejection(null);
        		
        	}
        	
        	try {
        		        		
        		LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(COA_ACCOUNT_NUMBER, AD_BRNCH, AD_CMPNY);
            	glChartOfAccount.addInvAdjustment(invAdjustment);
            	invAdjustment.setGlChartOfAccount(glChartOfAccount);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalAccountNumberInvalidException();
        		
        	}
        	//he
        	double ABS_TOTAL_AMOUNT = 0d;
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	
        	
        	if (isRecalculate) {
        	    System.out.println("isRecalculated");    	
	        	// remove all adjustment lines
	        	
	        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
	        	
	        	Iterator i = invAdjustmentLines.iterator();
	        	
	        	while (i.hasNext()) {
	        		
	        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
	        		
	        		
	        		
	        		if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
	        		
	        		    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
	        		
	        			invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);
	        			
	        		}
	        		//remove all inv tag inside adjustment line
		  	   	    Collection invTags = invAdjustmentLine.getInvTags();
		  	   	    
		  	   	    Iterator x = invTags.iterator();
		  	   	    
		  	   	    while (x.hasNext()){
		  	   	    	
		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();
		  	   	    	
		  	   	    	x.remove();
		  	   	    	
		  	   	    	invTag.remove();
		  	   	    }
	        		
	        		i.remove();
	        		
	        		invAdjustmentLine.remove();
	        		
	        	}
	        	
	        	// remove all distribution records
	       	   
	 	  	    Collection arDistributionRecords = invAdjustment.getInvDistributionRecords();
	 	  	  
	 	  	    i = arDistributionRecords.iterator();     	  
	 	  	  
	 	  	    while (i.hasNext()) {
	 	  	  	
	 	  	   	    LocalInvDistributionRecord arDistributionRecord = (LocalInvDistributionRecord)i.next();
	 	  	  	   
	 	  	  	    i.remove();
	 	  	  	    
	 	  	  	    arDistributionRecord.remove();
	 	  	  	
	 	  	    }	  
	 	  	    
	 	  	    // add new adjustment lines and distribution record
	 	  	    
	 	  	    double TOTAL_AMOUNT = 0d; 
	 	  	    
	 	  	    byte DEBIT = 0;
	 	  	    
	 	  	    i = alList.iterator();
	 	  	    
	 	  	    while (i.hasNext()) {
	 	  	    	
	 	  	    	InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();
	 	  	    	
	 	  	    	LocalInvItemLocation invItemLocation = null;
		      	    LocalInvItem item = null;
		      	    LocalInvLocation location = null;
		      	    
	 	  	    	try {
	 	  	    	
	      	  	    	item = invItemHome.findByIiName(mdetails.getAlIiName(), AD_CMPNY);
	      	  	    	location = invLocationHome.findByPrimaryKey(item.getIiDefaultLocation());
	 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(location.getLocName(), mdetails.getAlIiName(), AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {
	 	  	    	
	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));
	 	  	    		
	 	  	    	}
	 	  	    	
	      	  	    //supply missing entries for the data collector
	 	  	    	mdetails.setAlUnitCost(item.getIiUnitCost());
	 	  	    	mdetails.setAlLocName(location.getLocName());
	 	  	    	mdetails.setAlUomName(item.getInvUnitOfMeasure().getUomName());
	 	  	    	mdetails.setAlActualQuantity(getQuantityByIiNameAndUomName(mdetails.getAlIiName(), mdetails.getAlLocName(), mdetails.getAlUomName(), details.getAdjDate(), AD_BRNCH, AD_CMPNY));
	 	  	    	
	 	  	    	//	start date validation
	 	  	    	/*if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		    				invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		    			if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
	 	  	    	}*/
	 	  	    	
	 	  	    	
	 	  	    	LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(mdetails, invAdjustment, EJBCommon.FALSE, AD_CMPNY);
	 	  	    	 	  	    	 	  	    	
	 	  	    	// add physical inventory distribution
	 	  	    	
	 	  	    	double AMOUNT = 0d; 	  	    	
	 	  	    	
	 	  	    	if (invAdjustmentLine.getAlAdjustQuantity() > 0) {
	 	  	    		  	    	 	  	    	
		 	  	    	AMOUNT = EJBCommon.roundIt(
		 	  	    			invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(),
								this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  	    	//AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(), adPreference.getPrfInvCostPrecisionUnit());
		 	  	    	DEBIT = EJBCommon.TRUE;
		 	  	    	System.out.print(invAdjustmentLine.getAlAdjustQuantity()+"Controller Adjust3");
	 	  	    	} else {
	 	  	    		
	 	  	    		double COST = 0d;
	 	  	    		
	 	  	    	/*	try {
		 	  	    		
		 	  	    	/*	LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(details.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(), 
		 	  	    		    invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);	 	  	    		    	 	  	    		

//test if AVERAGE or FIFO 		 	  	    				 	  	    				 	  	    		
		 	  	    		if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))   
		          			{
		          				COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), adPreference.getPrfInvCostPrecisionUnit());
		          				//COST = invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity();
		          			}
		          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
		          			{
		          				COST = this.getInvFifoCost(invAdjustment.getAdjDate(), invItemLocation.getIlCode(), invCosting.getCstAdjustQuantity(),
		          						invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
		         			}
		          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
		          			{
		          				COST = invItemLocation.getInvItem().getIiUnitCost();		          						
		         			}*/
		 	  	    				
		 	  	    /*    } catch (FinderException ex) {	 */
		 	  	        
		 	  	            //COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();	  	        	 	  	        	
		 	  	        
		 	  	      //  }
		 	  	        	 	  	        
		 	  	      COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
		 	  	        		invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
								COST, true, AD_CMPNY);
		 	  	        
		 	  	        invAdjustmentLine.setAlUnitCost(COST);
		 	  	        
	 	  	    		AMOUNT = EJBCommon.roundIt(
		 	  	    			invAdjustmentLine.getAlAdjustQuantity() * COST,
		 	  	    			adPreference.getPrfInvCostPrecisionUnit());
						//AMOUNT = invAdjustmentLine.getAlAdjustQuantity() * COST;
						System.out.println("AMOUNT>>: " + AMOUNT);
		 	  	    	DEBIT = EJBCommon.FALSE;
	 	  	    	
	 	  	    	}
	 	  	    	
	 	  	    	// check for branch mapping
	 	  	    	
	 	  	    	LocalAdBranchItemLocation adBranchItemLocation = null;
	 	  	    	
	 	  	    	try{
	 	  	    	
	 	  	    		adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
	 	  	    				invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {

	 	  	    	}
	 	  	    	
	 	  	    	LocalGlChartOfAccount glInventoryChartOfAccount = null;
	 	  	    	
	 	  	    	if (adBranchItemLocation == null) {
	 	  	    		
	 	  	    		glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
	 	  	    				invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
	 	  	    	} else {
	 	  	    		
	 	  	    		glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
	 	  	    				adBranchItemLocation.getBilCoaGlInventoryAccount());
	 	  	    		
	 	  	    	}
	 	  	    	
	 	  	    	this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(AMOUNT), EJBCommon.FALSE,
	 	  	    			glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
	 	  	    	
	 	  	    	TOTAL_AMOUNT += AMOUNT;
	 	  	    	ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);
	 	  	    	
	 	  	    	// add adjust quantity to item location committed quantity if negative
	 	  	    	
	 	  	    	if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
	 	  	    	
	 	  	    		double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
	
	 	  	    		invItemLocation = invAdjustmentLine.getInvItemLocation();
	 	  	    		
	 	  	    		invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);
	 	  	    		
	 	  	    	}
	 	  	    	
	 	  	    	
	 	  	    }
	 	  	    
	 	  	    // add variance or transfer/debit distribution
	 	  	    
	 	  	    DEBIT = TOTAL_AMOUNT > 0 ? EJBCommon.FALSE : EJBCommon.TRUE;
	 	  	    
	 	  	    this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_AMOUNT), EJBCommon.FALSE,
	 	  	    		invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
	 	  	    
        	} else {
        	System.out.println("is not recalculated");
        		Iterator i = alList.iterator();
	 	  	    
	 	  	    while (i.hasNext()) {
	 	  	    	
	 	  	    	InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();
	 	  	    	
	 	  	    	LocalInvItemLocation invItemLocation = null;
	 	  	    	
	 	  	    	try {
	 	  	    	
	 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(), mdetails.getAlIiName(), AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {
	 	  	    	
	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));
	 	  	    		
	 	  	    	}
	 	  	    	LocalInvAdjustmentLine invAdjustmentLine = invAdjustmentLineHome.findByPrimaryKey(mdetails.getAlCode());
		  	   	    
		  	   	    
	 	  	    	
	 	  	    	//	start date validation
	 	  	    	
	 	  	    /*	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		    				invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		    			if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
	 	  	    	}*/
	 	  	    
	 	  	    }
	 	  	    	
        		Collection invAdjDistributionRecords = invAdjustment.getInvDistributionRecords();
        		
        		i = invAdjDistributionRecords.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();
        			
        			if(distributionRecord.getDrDebit() == 1) {
        			
        				ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();
        			
        			}
        			
        		}

        	}

        	//Insufficient Stocks
        	if (adPreference.getPrfArCheckInsufficientStock() == EJBCommon.TRUE && !isDraft) {
        		System.out.println("CHECK A");
	        	boolean hasInsufficientItems = false;
	        	String insufficientItems = "";

	        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

	        	Iterator i = invAdjustmentLines.iterator();  
	        	
	        	HashMap cstMap = new HashMap();
	        	
	        	while (i.hasNext()) {

	        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
	        		System.out.println("CHECK A.1");
	        		if(invAdjustmentLine.getAlAdjustQuantity()<0){
	        			if (invAdjustmentLine.getInvItemLocation().getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE && invAdjustmentLine.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {
	        				System.out.println("CHECK B-1");
	        				Collection invBillOfMaterials = invAdjustmentLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();

	        				Iterator j = invBillOfMaterials.iterator();
	        				Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N01");
	        				while (j.hasNext()) {

	        					LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

	        					LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

	        					//LocalInvCosting invBomCosting = null;

	        					double ILI_QTY = this.convertByUomFromAndItemAndQuantity(
	        							invAdjustmentLine.getInvUnitOfMeasure(), 
	        							invAdjustmentLine.getInvItemLocation().getInvItem(), 
	        							Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
	        					Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N02");
	        					double CURR_QTY = 0;
	        					boolean isIlFound = false;
	        					
	        					if(cstMap.containsKey(invItemLocation.getIlCode().toString())){

	        						isIlFound =  true; 		                    	
	        						CURR_QTY = ((Double)cstMap.get(invItemLocation.getIlCode().toString())).doubleValue();

	        					}else{
	        						/*try {

		        						invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
		        								invAdjustment.getAdjDate(), invBillOfMaterial.getBomIiName(), 
		        								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

		        					} catch (FinderException ex) {

		        					}*/
	        					}
	        					

	        					LocalInvItem bomItm = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(),AD_CMPNY);


	        					double NEEDED_QTY = this.convertByUomFromAndItemAndQuantity(invBillOfMaterial.getInvUnitOfMeasure(), bomItm,
	        							invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY);
	        					Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N03");
	        					


	        					/*try{
	        						if(invBomCosting != null){
	        							CURR_QTY = this.convertByUomAndQuantity(invBomCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure(), 
	        									invBomCosting.getInvItemLocation().getInvItem(),
	        									invBomCosting.getCstRemainingQuantity(), AD_CMPNY);
	        						}
	        					}catch (Exception e) {

	        					}*/


	        					/*if ((invBomCosting == null && isIlFound==false) || CURR_QTY == 0 || CURR_QTY < (NEEDED_QTY * ILI_QTY)) {

	        						hasInsufficientItems = true;

	        						insufficientItems += invAdjustmentLine.getInvItemLocation().getInvItem().getIiName()
	        						+ "-" + invBillOfMaterial.getBomIiName() + ", ";
	        					}*/
	        					
	        					CURR_QTY -= (NEEDED_QTY * ILI_QTY);
	 		                    
	 		                    if(!isIlFound){
	 		                    	cstMap.remove(invItemLocation.getIlCode().toString()); 		                    	
	 		                    } 	
	 		                    
	 		                    cstMap.put(invItemLocation.getIlCode().toString(), new Double(CURR_QTY));
	 							
	        				}
	        			}  else {
	        				System.out.println("CHECK B-2");
	        			//	LocalInvCosting invCosting = null;
	        				double CURR_QTY = 0; 
	        				boolean isIlFound = false;
	        				
	        				double ILI_QTY = this.convertByUomAndQuantity(
	        						invAdjustmentLine.getInvUnitOfMeasure(), 
	        						invAdjustmentLine.getInvItemLocation().getInvItem(), 
	        						Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

	        				
	        				if(cstMap.containsKey(invAdjustmentLine.getInvItemLocation().getIlCode().toString())){
	 		  	   	    		Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04 - A");
	 		  	   	    		isIlFound =  true; 		                    	
	 		  	   	    		CURR_QTY = ((Double)cstMap.get(invAdjustmentLine.getInvItemLocation().getIlCode().toString())).doubleValue();
	 		  	   	    		
	        				}else{
	        					//try {

	        						/*invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
	        								invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(), 
	        								invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	        						CURR_QTY = invCosting.getCstRemainingQuantity();
	        						Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N04");
	        						System.out.println("CHECK C");*/
	        				//	} catch (FinderException ex) {
	        						/*System.out.println("CHECK D");*/
	        					//}
	        				}
	        				
	        				/*if(invCosting != null){

	 		  	   	    		CURR_QTY = this.convertByUomAndQuantity(
		        						invAdjustmentLine.getInvUnitOfMeasure(), 
		        						invAdjustmentLine.getInvItemLocation().getInvItem(), 
		        						Math.abs(invCosting.getCstRemainingQuantity()), AD_CMPNY);

	 		  	   	    		Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L05");
	 		  	   	    	}*/
	        				
	        				double LOWEST_QTY = this.convertByUomAndQuantity(
	        						invAdjustmentLine.getInvUnitOfMeasure(),
	        						invAdjustmentLine.getInvItemLocation().getInvItem(),
	        						1, AD_CMPNY);
	        				System.out.println("CHECK E");
	        				System.out.println(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
	        				System.out.println("ILI_QTY: " + ILI_QTY);
	        				System.out.println("LOWEST_QTY: " + LOWEST_QTY);
	        				//System.out.println("invCosting " + invCosting);
	        				System.out.println("CURR_QTY: " + CURR_QTY);
	        			/*	if ((invCosting == null && isIlFound==false)|| CURR_QTY == 0 || CURR_QTY - ILI_QTY <= -LOWEST_QTY) {

	        					hasInsufficientItems = true;
	        					System.out.println("CHECK F");
	        					insufficientItems += invAdjustmentLine.getInvItemLocation().getInvItem().getIiName() + ", ";
	        				}*/
	        				
	        				CURR_QTY -= ILI_QTY;
	 		  	   	    	
	 		  	   	    	if(isIlFound){
	 		  	   	    		cstMap.remove(invAdjustmentLine.getInvItemLocation().getIlCode().toString()); 		  	   	    		
	 		  	   	    	}
	 		  	   	    	
	 		  	   	    	cstMap.put(invAdjustmentLine.getInvItemLocation().getIlCode().toString(), new Double(CURR_QTY));
	        			}
	        		}
	        		
	        	}
	        	if(hasInsufficientItems) {

	        		throw new GlobalRecordInvalidException(insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
	        	}
	        }
 	  	    // generate approval status
 	  	    
 	  	    String INV_APPRVL_STATUS = null;
 		
 	  		if(!isDraft) {
				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
				
				// check if ap voucher approval is enabled
				if (adApproval.getAprEnableInvAdjustmentRequest() == EJBCommon.FALSE) {
					INV_APPRVL_STATUS = "N/A";
					
				} else {
					// for approval, create approval queue
					
					// get user who is requesting details.getPrLastModifiedBy() adUser
					
					LocalAdUser adUser = adUserHome.findByUsrName(details.getAdjLastModifiedBy(), AD_CMPNY);
				
					Collection adUsers = adUserHome.findUsrByDepartmentHead(adUser.getUsrDept(), (byte)1,AD_CMPNY);
					
					if (adUsers.isEmpty()) {
			
						throw new GlobalNoApprovalApproverFoundException();
						
					} else {
						
						Iterator j = adUsers.iterator();
						while (j.hasNext()) {		
							LocalAdUser adUserHead = (LocalAdUser)j.next();
							LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV ADJUSTMENT REQUEST", invAdjustment.getAdjCode(), 
									invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(), "OR", (byte) 1, AD_BRNCH, AD_CMPNY);
							adUserHead.addAdApprovalQueue(adApprovalQueue);	      				 	
						}    				 
					}
					INV_APPRVL_STATUS = "PENDING";
				} 
					
				   
				
				invAdjustment.setAdjApprovalStatus(INV_APPRVL_STATUS);
				
				// set post purchase order
				
				if(INV_APPRVL_STATUS.equals("N/A")) {
				
					invAdjustment.setAdjPosted(EJBCommon.TRUE);
					invAdjustment.setAdjPosted(EJBCommon.TRUE);
					invAdjustment.setAdjPostedBy(invAdjustment.getAdjLastModifiedBy());
					invAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
				 
				 }
				
				
				
			}
    		
 	  	    return invAdjustment.getAdjCode();
        } catch (GlobalRecordAlreadyDeletedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;	
			
		} catch (GlobalNoApprovalApproverFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (GlobalDocumentNumberNotUniqueException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (GlobalTransactionAlreadyPendingException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
		
		} catch (GlobalTransactionAlreadyPostedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		
		} catch (GlobalInvItemLocationNotFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;

		
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
		
		
	}
    
    /**
    * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvAdjEntry(com.util.InvAdjustmentDetails details, String COA_ACCOUNT_NUMBER,
		ArrayList alList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalRecordAlreadyDeletedException,
		GlobalBranchAccountNumberInvalidException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalNoApprovalRequesterFoundException,
		GlobalNoApprovalApproverFoundException,
		GlobalInvItemLocationNotFoundException,		
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalDocumentNumberNotUniqueException,
		GlobalInventoryDateException,
		GlobalAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException,
		GlobalExpiryDateNotFoundException,
		GlobalMiscInfoIsRequiredException,
		GlobalRecordInvalidException{
    	
    	Debug.print("InvAdjustmentRequestControllerBean saveInvAdjEntry");
    	  System.out.print("printC7");
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdApprovalHome adApprovalHome = null;
    	LocalAdAmountLimitHome adAmountLimitHome = null;
    	LocalAdApprovalUserHome adApprovalUserHome = null;
    	LocalAdApprovalQueueHome adApprovalQueueHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdUserHome adUserHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null; 
    	LocalInvItemHome invItemHome = null;
    	LocalInvTagHome invTagHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	 InvRepItemCostingControllerHome homeRIC = null;
         InvRepItemCostingController ejbRIC = null;

        // Initialize EJB Home
        
        try {
            
        	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        	adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
        	adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
        	adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
        	adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
        	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);        	
        	invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
        	
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);	
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class); 
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
	            	lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
	            	lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			homeRIC = (InvRepItemCostingControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
        	ejbRIC = homeRIC.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        LocalInvAdjustment adj = null;
        
        //check if generating issuance
        if (details.getAdjType().equals("ISSUANCE")) {
        	
        	try {
        		adj = invAdjustmentHome.findByPrimaryKey(details.getAdjCode());
        		details.setAdjCode(null);
        		adj.setAdjGenerated(EJBCommon.TRUE);
        	}
        	catch (FinderException ex) {Debug.printStackTrace(ex);}
        }
        
        
        try {
        	
        	LocalInvAdjustment invAdjustment = null;
        	
        	// validate if Adjustment is already deleted
        	
        	try {
        		
        		if (details.getAdjCode() != null) {
        			
        			invAdjustment = invAdjustmentHome.findByPrimaryKey(details.getAdjCode());
        			
        		}
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if adjustment is already posted, void, approved or pending
        	
        	if (details.getAdjCode() != null) {
	        	
	        	
	      	if (invAdjustment.getAdjApprovalStatus() != null) {
        		
	        	
	        		
	        		
	        		if ((invAdjustment.getAdjApprovalStatus().equals("APPROVED") ||
	        			invAdjustment.getAdjApprovalStatus().equals("N/A")) && details.getAdjType().equals("REQUEST")) {
	        			
	        		    throw new GlobalTransactionAlreadyApprovedException(); 
	        		    	
	        		    	
	       		} else if (invAdjustment.getAdjApprovalStatus().equals("PENDING")) {
	        			
	        			throw new GlobalTransactionAlreadyPendingException();
	        			
	       		}
	       		
	       	}
        			
        	if (invAdjustment.getAdjPosted() == EJBCommon.TRUE && details.getAdjType().equals("REQUEST")) {
        			
        			throw new GlobalTransactionAlreadyPostedException();
        			
        		}
        		
        	}
        	
        	LocalInvAdjustment invExistingAdjustment = null;

        	try {
        		
        		invExistingAdjustment = invAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);
        		
        	} catch (FinderException ex) { 
        		
        	}
        	
        	
        	// 	validate if document number is unique document number is automatic then set next sequence

	        if (details.getAdjCode() == null) {
	        	
	        	
	        	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		        LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
	        	
	        	if (invExistingAdjustment != null) {
	        		
	        		throw new GlobalDocumentNumberNotUniqueException();
	        		
	        	}
	        	
	        	if (details.getAdjType().equals("ISSUANCE")) {
		        	try {
		        		
		        		adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);
		        		
		        	} catch (FinderException ex) {
		        		
		        	}
	        	}
	        	else {
		        	try {
		        		
		        		adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT REQUEST", AD_CMPNY);
		        		
		        	} catch (FinderException ex) {
		        		
		        	}
	        	}
	        	
	        	try {
	        		
	        		adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
	        		
	        	} catch (FinderException ex) {
	        		
	        	}

	        	if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
	        			(details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {
	        		
	        		while (true) {
	        			
	        			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
	        				
	        				try {

	        					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);		            		
	        					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
	        					
	        				} catch (FinderException ex) {
	        					
	        					details.setAdjDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
	        					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
	        					break;
	        					
	        				}
	        				
 						} else {
 							
 							try {
 								
 								invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
 								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
 								
 							} catch (FinderException ex) {
 								
 								details.setAdjDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
 								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
 								break;
 								
 							}
 							
 						}

	        		}		            
	        		
	        	}
	        	
	        } else {
		    			    			    	

	        	if (invExistingAdjustment != null && 
	                !invExistingAdjustment.getAdjCode().equals(details.getAdjCode())) {
	            	
	            	throw new GlobalDocumentNumberNotUniqueException();
	            	
	            }
	            
	            if (invAdjustment.getAdjDocumentNumber() != details.getAdjDocumentNumber() &&
	                (details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {
	                	
	                details.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
	                	
	         	}
		    	
		    }
        	
        	// used in checking if invoice should re-generate distribution records and re-calculate taxes	        	                	
	        boolean isRecalculate = true;
	        
        	
        	// create adjustment
        	
        	if (details.getAdjCode() == null) {
        		
        		invAdjustment = invAdjustmentHome.create(details.getAdjDocumentNumber(), details.getAdjReferenceNumber(), details.getAdjDescription(), 
        				details.getAdjDate(), details.getAdjType(), details.getAdjApprovalStatus(),
        				EJBCommon.FALSE, EJBCommon.FALSE, details.getAdjCreatedBy(), details.getAdjDateCreated(),
						details.getAdjLastModifiedBy(), details.getAdjDateLastModified(),
						null, null, null, null, null, null, EJBCommon.FALSE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
        		
        	} else {
        		
                // check if critical fields are changed
        		
        		if (!invAdjustment.getGlChartOfAccount().getCoaAccountNumber().equals(COA_ACCOUNT_NUMBER) ||
					alList.size() != invAdjustment.getInvAdjustmentLines().size() ||
					!(invAdjustment.getAdjDate().equals(details.getAdjDate()))) {
        			
        			isRecalculate = true;
        			
        		} else if (alList.size() == invAdjustment.getInvAdjustmentLines().size()) {
        			
        			Iterator alIter = invAdjustment.getInvAdjustmentLines().iterator();
        			Iterator alListIter = alList.iterator();
        			
        			while (alIter.hasNext()) {
        				
        				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)alIter.next();
        				InvModAdjustmentLineDetails mdetails = (InvModAdjustmentLineDetails)alListIter.next();
        				      System.out.print("controller"+mdetails.getAlAdjustQuantity());  				
        				if (!invAdjustmentLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getAlIiName()) ||
            				!invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getAlLocName()) ||
	        				!invAdjustmentLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getAlUomName()) ||
	        				invAdjustmentLine.getAlAdjustQuantity() != mdetails.getAlAdjustQuantity() ||
	        				invAdjustmentLine.getAlUnitCost() != mdetails.getAlUnitCost() || 
	        				invAdjustmentLine.getAlMisc() != mdetails.getAlMisc()) {
        					
        					isRecalculate = true;
        					break;
        					
        				}
        				
        				isRecalculate = false;
        				
        			}
        			
        		} else {
        			
        			isRecalculate = false;
        			
        		}
        		
        		invAdjustment.setAdjDocumentNumber(details.getAdjDocumentNumber());
        		invAdjustment.setAdjReferenceNumber(details.getAdjReferenceNumber());
        		invAdjustment.setAdjDescription(details.getAdjDescription());
        		invAdjustment.setAdjDate(details.getAdjDate());
        		invAdjustment.setAdjType(details.getAdjType());
        		invAdjustment.setAdjApprovalStatus(details.getAdjApprovalStatus());
        		invAdjustment.setAdjLastModifiedBy(details.getAdjLastModifiedBy());
        		invAdjustment.setAdjDateLastModified(details.getAdjDateLastModified());
        		
        		invAdjustment.setAdjReasonForRejection(null);
        		
        	}
        	
        	try {
        		        		
        		LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(COA_ACCOUNT_NUMBER, AD_BRNCH, AD_CMPNY);
            	glChartOfAccount.addInvAdjustment(invAdjustment);
            	invAdjustment.setGlChartOfAccount(glChartOfAccount);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalAccountNumberInvalidException();
        		
        	}
        	//he
        	double ABS_TOTAL_AMOUNT = 0d;
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	
        	
        	if (isRecalculate) {
        	    System.out.println("isRecalculated");    	
	        	// remove all adjustment lines
	        	
	        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
	        	
	        	Iterator i = invAdjustmentLines.iterator();
	        	
	        	while (i.hasNext()) {
	        		
	        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
	        		
	        		
	        		
	        		if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
	        		
	        		    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
	        		
	        			invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);
	        			
	        		}
	        		//remove all inv tag inside adjustment line
		  	   	    Collection invTags = invAdjustmentLine.getInvTags();
		  	   	    
		  	   	    Iterator x = invTags.iterator();
		  	   	    
		  	   	    while (x.hasNext()){
		  	   	    	
		  	   	    	LocalInvTag invTag = (LocalInvTag)x.next();
		  	   	    	
		  	   	    	x.remove();
		  	   	    	
		  	   	    	invTag.remove();
		  	   	    }
	        		
	        		i.remove();
	        		
	        		invAdjustmentLine.remove();
	        		
	        	}
	        	
	        	// remove all distribution records
	       	   
	 	  	    Collection arDistributionRecords = invAdjustment.getInvDistributionRecords();
	 	  	  
	 	  	    i = arDistributionRecords.iterator();     	  
	 	  	  
	 	  	    while (i.hasNext()) {
	 	  	  	
	 	  	   	    LocalInvDistributionRecord arDistributionRecord = (LocalInvDistributionRecord)i.next();
	 	  	  	   
	 	  	  	    i.remove();
	 	  	  	    
	 	  	  	    arDistributionRecord.remove();
	 	  	  	
	 	  	    }
 	  	    	
 	  	    	//set al_served for issuance generation
	 	  	    Collection adjLines = null;
	 	  	    Iterator j = null;
 	  	    	if (details.getAdjType().equals("ISSUANCE")) {
 	  	    		
 	  	    		adjLines = adj.getInvAdjustmentLines();
 	  	    		j = adjLines.iterator();
 	  	    		
 	  	    	}
	 	  	    
	 	  	    // add new adjustment lines and distribution record
	 	  	    
	 	  	    double TOTAL_AMOUNT = 0d; 
	 	  	    
	 	  	    byte DEBIT = 0;
	 	  	    
	 	  	    i = alList.iterator();
	 	  	    
	 	  	    while (i.hasNext()) {
	 	  	    	
	 	  	    	InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();
	 	  	    	
	 	  	    	LocalInvItemLocation invItemLocation = null;
	 	  	    	
	 	  	    	try {
	 	  	    	
	 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(), mdetails.getAlIiName(), AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {
	 	  	    	
	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));
	 	  	    		
	 	  	    	}
	 	  	    	
	 	  	    	//	start date validation
	 	  	    	/*if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		    				invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		    			if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
	 	  	    	}*/
	 	  	    	
	 	  	    	if (details.getAdjType().equals("REQUEST")) {
	 	  	    		
	 	  	    		try {
		 	  	    		
	 	  	    		LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(details.getAdjDate(), mdetails.getAlIiName(), 
	 	  	    		    mdetails.getAlLocName(), AD_BRNCH, AD_CMPNY);	 	  	    		    	 	  	    		


                        if(invCosting.getCstRemainingQuantity() <= 0 && invCosting.getCstRemainingValue() <=0){
                            System.out.println("RE CALC");
                            HashMap criteria = new HashMap();
                            criteria.put("itemName", invItemLocation.getInvItem().getIiName());
                            criteria.put("location", invItemLocation.getInvLocation().getLocName());

                            ArrayList branchList = new ArrayList();

                            AdBranchDetails mdetailsb = new AdBranchDetails();
                            mdetailsb.setBrCode(AD_BRNCH);
                            branchList.add(mdetailsb);

                            ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

                            invCosting  =  invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(details.getAdjDate(), mdetails.getAlIiName(), 
        	 	  	    		    mdetails.getAlLocName(), AD_BRNCH, AD_CMPNY);	 	  	    		    	 	  	    		
                        }

	 	  	    		//test if AVERAGE or FIFO 		 	  	    				 	  	    				 	  	    		
	 	  	    		if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))   
	          			{
	          				 double COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();

                             COST = invCosting.getCstRemainingQuantity() <= 0 ? COST :
                                    Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                            if(COST<=0){
                                COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
                             }  


                            mdetails.setAlUnitCost(COST);
	          				//COST = invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity();
	          			}
	          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
	          			{
	          				//COST = this.getInvFifoCost(invAdjustment.getAdjDate(), invItemLocation.getIlCode(), invCosting.getCstAdjustQuantity(),
	          				//		invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
	         			}
	          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
	          			{
	          				//COST = invItemLocation.getInvItem().getIiUnitCost();		          						
	         			}
	 	  	    				
	 	  	        } catch (FinderException ex) {
	 	  	        }
	 	  	    		
	 	  	    	}
	 	  	    	LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(mdetails, invAdjustment, EJBCommon.FALSE, AD_CMPNY);
	 	  	    	
	 	  	    	if (details.getAdjType().equals("ISSUANCE")) {
	 	  	    		double COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();
	 	  	    		LocalInvAdjustmentLine  adjLine = (LocalInvAdjustmentLine) j.next();
	 	  	    		invAdjustmentLine.setAlServed(adjLine.getAlServed()-adjLine.getAlAdjustQuantity());
	 	  	    		
		 	  	    		try {
	 	  	    		
		 	  	    		LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(details.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(), 
		 	  	    		    invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);	 	  	    		    	 	  	    		

		 	  	    		//test if AVERAGE or FIFO 		 	  	    				 	  	    				 	  	    		
		 	  	    		if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))   
		          			{
		          			//	COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), adPreference.getPrfInvCostPrecisionUnit());
		          				
                                 COST = invCosting.getCstRemainingQuantity() <= 0 ? COST :
                                    Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                                if(COST<=0){
                                    COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
                                 }  





                                invAdjustmentLine.setAlUnitCost(COST);
		          				//COST = invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity();
		          			}
		          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
		          			{
		          				//COST = this.getInvFifoCost(invAdjustment.getAdjDate(), invItemLocation.getIlCode(), invCosting.getCstAdjustQuantity(),
		          				//		invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
		         			}
		          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
		          			{
		          				COST = invItemLocation.getInvItem().getIiUnitCost();		          						
		         			}
		 	  	    				
		 	  	        } catch (FinderException ex) {
		 	  	        
		 	  	            COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();	  	        	 	  	        	
		 	  	        
		 	  	        }
	 	  	    		
	 	  	    	}
	 	  	    	
	 	  	    	// add physical inventory distribution
	 	  	    	
	 	  	    	double AMOUNT = 0d; 	  	    	
	 	  	    	
	 	  	    	if (invAdjustmentLine.getAlAdjustQuantity() > 0) {
	 	  	    		  	    	 	  	    	
		 	  	    	AMOUNT = EJBCommon.roundIt(
		 	  	    			invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(),
								this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  	    	//AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(), adPreference.getPrfInvCostPrecisionUnit());
		 	  	    	DEBIT = EJBCommon.TRUE;
		 	  	    	System.out.print(invAdjustmentLine.getAlAdjustQuantity()+"Controller Adjust3");
	 	  	    	} else {
	 	  	    		
	 	  	    		double COST = 0d;
	 	  	    		
	 	  	    	/*	try {
		 	  	    		
		 	  	    	/*	LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(details.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(), 
		 	  	    		    invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);	 	  	    		    	 	  	    		

//test if AVERAGE or FIFO 		 	  	    				 	  	    				 	  	    		
		 	  	    		if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))   
		          			{
		          				COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), adPreference.getPrfInvCostPrecisionUnit());
		          				//COST = invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity();
		          			}
		          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
		          			{
		          				COST = this.getInvFifoCost(invAdjustment.getAdjDate(), invItemLocation.getIlCode(), invCosting.getCstAdjustQuantity(),
		          						invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
		         			}
		          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
		          			{
		          				COST = invItemLocation.getInvItem().getIiUnitCost();		          						
		         			}*/
		 	  	    				
		 	  	    /*    } catch (FinderException ex) {	 */
		 	  	        
		 	  	            //COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();	  	        	 	  	        	
		 	  	        
		 	  	      //  }
		 	  	        	 	  	        
		 	  	      COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
		 	  	        		invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
								COST, true, AD_CMPNY);
		 	  	        
		 	  	        invAdjustmentLine.setAlUnitCost(COST);
		 	  	        
	 	  	    		AMOUNT = EJBCommon.roundIt(
		 	  	    			invAdjustmentLine.getAlAdjustQuantity() * COST,
		 	  	    			adPreference.getPrfInvCostPrecisionUnit());
						//AMOUNT = invAdjustmentLine.getAlAdjustQuantity() * COST;
						System.out.println("AMOUNT>>: " + AMOUNT);
		 	  	    	DEBIT = EJBCommon.FALSE;
	 	  	    	
	 	  	    	}
	 	  	    	
	 	  	    	// check for branch mapping
	 	  	    	
	 	  	    	LocalAdBranchItemLocation adBranchItemLocation = null;
	 	  	    	
	 	  	    	try{
	 	  	    	
	 	  	    		adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
	 	  	    				invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {

	 	  	    	}
	 	  	    	
	 	  	    	LocalGlChartOfAccount glInventoryChartOfAccount = null;
	 	  	    	
	 	  	    	if (adBranchItemLocation == null) {
	 	  	    		
	 	  	    		glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
	 	  	    				invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
	 	  	    	} else {
	 	  	    		
	 	  	    		glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
	 	  	    				adBranchItemLocation.getBilCoaGlInventoryAccount());
	 	  	    		
	 	  	    	}
	 	  	    	
	 	  	    	this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(AMOUNT), EJBCommon.FALSE,
	 	  	    			glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
	 	  	    	
	 	  	    	TOTAL_AMOUNT += AMOUNT;
	 	  	    	ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);
	 	  	    	
	 	  	    	// add adjust quantity to item location committed quantity if negative
	 	  	    	
	 	  	    	if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
	 	  	    	
	 	  	    		double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
	
	 	  	    		invItemLocation = invAdjustmentLine.getInvItemLocation();
	 	  	    		
	 	  	    		invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);
	 	  	    		
	 	  	    	}
	 	  	    	
	 	  	    	
	 	  	    }
	 	  	    
	 	  	    // add variance or transfer/debit distribution
	 	  	    
	 	  	    DEBIT = TOTAL_AMOUNT > 0 ? EJBCommon.FALSE : EJBCommon.TRUE;
	 	  	    
	 	  	    this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_AMOUNT), EJBCommon.FALSE,
	 	  	    		invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
	 	  	    
        	} else {
        	System.out.println("is not recalculated");
        		Iterator i = alList.iterator();
	 	  	    
	 	  	    while (i.hasNext()) {
	 	  	    	
	 	  	    	InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();
	 	  	    	
	 	  	    	LocalInvItemLocation invItemLocation = null;
	 	  	    	
	 	  	    	try {
	 	  	    	
	 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(), mdetails.getAlIiName(), AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {
	 	  	    	
	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));
	 	  	    		
	 	  	    	}
	 	  	    	LocalInvAdjustmentLine invAdjustmentLine = invAdjustmentLineHome.findByPrimaryKey(mdetails.getAlCode());
		  	   	    
		  	   	    
	 	  	    	
	 	  	    	//	start date validation
	 	  	    	
	 	  	    /*	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
		 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		    				invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		    			if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
	 	  	    	}*/
	 	  	    
	 	  	    }
	 	  	    	
        		Collection invAdjDistributionRecords = invAdjustment.getInvDistributionRecords();
        		
        		i = invAdjDistributionRecords.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();
        			
        			if(distributionRecord.getDrDebit() == 1) {
        			
        				ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();
        			
        			}
        			
        		}

        	}

        	//Insufficient Stocks
        	if (adPreference.getPrfArCheckInsufficientStock() == EJBCommon.TRUE && !isDraft) {
        		System.out.println("CHECK A");
	        	boolean hasInsufficientItems = false;
	        	String insufficientItems = "";

	        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

	        	Iterator i = invAdjustmentLines.iterator();  
	        	
	        	HashMap cstMap = new HashMap();
	        	
	        	while (i.hasNext()) {

	        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
	        		System.out.println("CHECK A.1");
	        		if(invAdjustmentLine.getAlAdjustQuantity()<0){
	        			if (invAdjustmentLine.getInvItemLocation().getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE && invAdjustmentLine.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {
	        				System.out.println("CHECK B-1");
	        				Collection invBillOfMaterials = invAdjustmentLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();

	        				Iterator j = invBillOfMaterials.iterator();
	        				Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N01");
	        				while (j.hasNext()) {

	        					LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

	        					LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

	        					//LocalInvCosting invBomCosting = null;

	        					double ILI_QTY = this.convertByUomFromAndItemAndQuantity(
	        							invAdjustmentLine.getInvUnitOfMeasure(), 
	        							invAdjustmentLine.getInvItemLocation().getInvItem(), 
	        							Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
	        					Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N02");
	        					double CURR_QTY = 0;
	        					boolean isIlFound = false;
	        					
	        					if(cstMap.containsKey(invItemLocation.getIlCode().toString())){

	        						isIlFound =  true; 		                    	
	        						CURR_QTY = ((Double)cstMap.get(invItemLocation.getIlCode().toString())).doubleValue();

	        					}else{
	        						/*try {

		        						invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
		        								invAdjustment.getAdjDate(), invBillOfMaterial.getBomIiName(), 
		        								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

		        					} catch (FinderException ex) {

		        					}*/
	        					}
	        					

	        					LocalInvItem bomItm = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(),AD_CMPNY);


	        					double NEEDED_QTY = this.convertByUomFromAndItemAndQuantity(invBillOfMaterial.getInvUnitOfMeasure(), bomItm,
	        							invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY);
	        					Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N03");
	        					


	        					/*try{
	        						if(invBomCosting != null){
	        							CURR_QTY = this.convertByUomAndQuantity(invBomCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure(), 
	        									invBomCosting.getInvItemLocation().getInvItem(),
	        									invBomCosting.getCstRemainingQuantity(), AD_CMPNY);
	        						}
	        					}catch (Exception e) {

	        					}*/


	        					/*if ((invBomCosting == null && isIlFound==false) || CURR_QTY == 0 || CURR_QTY < (NEEDED_QTY * ILI_QTY)) {

	        						hasInsufficientItems = true;

	        						insufficientItems += invAdjustmentLine.getInvItemLocation().getInvItem().getIiName()
	        						+ "-" + invBillOfMaterial.getBomIiName() + ", ";
	        					}*/
	        					
	        					CURR_QTY -= (NEEDED_QTY * ILI_QTY);
	 		                    
	 		                    if(!isIlFound){
	 		                    	cstMap.remove(invItemLocation.getIlCode().toString()); 		                    	
	 		                    } 	
	 		                    
	 		                    cstMap.put(invItemLocation.getIlCode().toString(), new Double(CURR_QTY));
	 							
	        				}
	        			}  else {
	        				System.out.println("CHECK B-2");
	        			//	LocalInvCosting invCosting = null;
	        				double CURR_QTY = 0; 
	        				boolean isIlFound = false;
	        				
	        				double ILI_QTY = this.convertByUomAndQuantity(
	        						invAdjustmentLine.getInvUnitOfMeasure(), 
	        						invAdjustmentLine.getInvItemLocation().getInvItem(), 
	        						Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

	        				
	        				if(cstMap.containsKey(invAdjustmentLine.getInvItemLocation().getIlCode().toString())){
	 		  	   	    		Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04 - A");
	 		  	   	    		isIlFound =  true; 		                    	
	 		  	   	    		CURR_QTY = ((Double)cstMap.get(invAdjustmentLine.getInvItemLocation().getIlCode().toString())).doubleValue();
	 		  	   	    		
	        				}else{
	        					//try {

	        						/*invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
	        								invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(), 
	        								invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	        						CURR_QTY = invCosting.getCstRemainingQuantity();
	        						Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N04");
	        						System.out.println("CHECK C");*/
	        				//	} catch (FinderException ex) {
	        						/*System.out.println("CHECK D");*/
	        					//}
	        				}
	        				
	        				/*if(invCosting != null){

	 		  	   	    		CURR_QTY = this.convertByUomAndQuantity(
		        						invAdjustmentLine.getInvUnitOfMeasure(), 
		        						invAdjustmentLine.getInvItemLocation().getInvItem(), 
		        						Math.abs(invCosting.getCstRemainingQuantity()), AD_CMPNY);

	 		  	   	    		Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L05");
	 		  	   	    	}*/
	        				
	        				double LOWEST_QTY = this.convertByUomAndQuantity(
	        						invAdjustmentLine.getInvUnitOfMeasure(),
	        						invAdjustmentLine.getInvItemLocation().getInvItem(),
	        						1, AD_CMPNY);
	        				System.out.println("CHECK E");
	        				System.out.println(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
	        				System.out.println("ILI_QTY: " + ILI_QTY);
	        				System.out.println("LOWEST_QTY: " + LOWEST_QTY);
	        				//System.out.println("invCosting " + invCosting);
	        				System.out.println("CURR_QTY: " + CURR_QTY);
	        			/*	if ((invCosting == null && isIlFound==false)|| CURR_QTY == 0 || CURR_QTY - ILI_QTY <= -LOWEST_QTY) {

	        					hasInsufficientItems = true;
	        					System.out.println("CHECK F");
	        					insufficientItems += invAdjustmentLine.getInvItemLocation().getInvItem().getIiName() + ", ";
	        				}*/
	        				
	        				CURR_QTY -= ILI_QTY;
	 		  	   	    	
	 		  	   	    	if(isIlFound){
	 		  	   	    		cstMap.remove(invAdjustmentLine.getInvItemLocation().getIlCode().toString()); 		  	   	    		
	 		  	   	    	}
	 		  	   	    	
	 		  	   	    	cstMap.put(invAdjustmentLine.getInvItemLocation().getIlCode().toString(), new Double(CURR_QTY));
	        			}
	        		}
	        		
	        	}
	        	if(hasInsufficientItems) {

	        		throw new GlobalRecordInvalidException(insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
	        	}
	        }
 	  	    // generate approval status
 	  	    
 	  	    String INV_APPRVL_STATUS = null;
 		
 	  		if(!isDraft) {
				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
				
				// check if ap voucher approval is enabled
				if (adApproval.getAprEnableInvAdjustmentRequest() == EJBCommon.FALSE) {
					INV_APPRVL_STATUS = "N/A";
					
				} else {
					// for approval, create approval queue
					
					// get user who is requesting details.getPrLastModifiedBy() adUser
					
					LocalAdUser adUser = adUserHome.findByUsrName(details.getAdjLastModifiedBy(), AD_CMPNY);
				
					Collection adUsers = adUserHome.findUsrByDepartmentHead(adUser.getUsrDept(), (byte)1,AD_CMPNY);
					
					if (adUsers.isEmpty()) {
			
						throw new GlobalNoApprovalApproverFoundException();
						
					} else {
						
						Iterator j = adUsers.iterator();
						while (j.hasNext()) {		
							LocalAdUser adUserHead = (LocalAdUser)j.next();
							LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV ADJUSTMENT REQUEST", invAdjustment.getAdjCode(), 
									invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(), "OR", (byte) 1, AD_BRNCH, AD_CMPNY);
							adUserHead.addAdApprovalQueue(adApprovalQueue);	      				 	
						}    				 
					}
					INV_APPRVL_STATUS = "PENDING";
				} 
					
				   
				
				invAdjustment.setAdjApprovalStatus(INV_APPRVL_STATUS);
				
				// set post purchase order
				
				if(INV_APPRVL_STATUS.equals("N/A")) {
				
					invAdjustment.setAdjPosted(EJBCommon.TRUE);
					invAdjustment.setAdjPosted(EJBCommon.TRUE);
					invAdjustment.setAdjPostedBy(invAdjustment.getAdjLastModifiedBy());
					invAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
				 
				 }
				
				
				
			}
    		
 	  	    return invAdjustment.getAdjCode();
        } catch (GlobalRecordAlreadyDeletedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;	
			
		} catch (GlobalNoApprovalApproverFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (GlobalDocumentNumberNotUniqueException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (GlobalTransactionAlreadyPendingException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
		
		} catch (GlobalTransactionAlreadyPostedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		
		} catch (GlobalInvItemLocationNotFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;

		
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
		
		
	}
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteInvAdjEntry(Integer ADJ_CODE, String AD_USR, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException {
                    
        Debug.print("InvAdjustmentRequestControllerBean deleteInvAdjEntry");
        
        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home
        
        try {
            
            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);  

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvAdjustment invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
        	
        	Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
        	
        	Iterator j = invAdjustmentLines.iterator();
        	
        	while (j.hasNext()) {
        		
        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) j.next();
        		
        		if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
            		
        		    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
        		
        			invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);
            			
            	}
        		
        	}
        	        	        	        	
        	if (invAdjustment.getAdjApprovalStatus() != null && invAdjustment.getAdjApprovalStatus().equals("PENDING")) {
        	
        		Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("INV ADJUSTMENT REQUEST", invAdjustment.getAdjCode(), AD_CMPNY);
        		
        		Iterator i = adApprovalQueues.iterator();
        		
        		while(i.hasNext()) {
        		
        			LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
        			
        			adApprovalQueue.remove();
        			
        		}
        	
        	}
        	
        	adDeleteAuditTrailHome.create("INV ADJUSTMENT REQUEST", invAdjustment.getAdjDate(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjReferenceNumber(),
 					0d, AD_USR, new Date(), AD_CMPNY);
        	
        	invAdjustment.remove();
        	
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("InvAdjustmentRequestControllerBean getGlFcPrecisionUnit");

       
        LocalAdCompanyHome adCompanyHome = null;
             
        // Initialize EJB Home
         
        try {
             
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
             
        } catch (NamingException ex) {
             
            throw new EJBException(ex.getMessage());
             
        }

        try {
        	
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             
          return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                      
        } catch (Exception ex) {
        	 
        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
          
        }

     }
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

         Debug.print("InvAdjustmentRequestControllerBean getInvGpQuantityPrecisionUnit");
        
         LocalAdPreferenceHome adPreferenceHome = null;         
         
          // Initialize EJB Home
           
          try {
               
          	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
               
          } catch (NamingException ex) {
               
             throw new EJBException(ex.getMessage());
               
          }
         

          try {
         	
             LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
             return adPreference.getPrfInvQuantityPrecisionUnit();
            
          } catch (Exception ex) {
           	 
             Debug.printStackTrace(ex);
             throw new EJBException(ex.getMessage());
            
          }

      }
      
      
      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/
       public short getInvGpCostPrecisionUnit(Integer AD_CMPNY) {

          Debug.print("InvAdjustmentRequestControllerBean getInvGpCostPrecisionUnit");
         
          LocalAdPreferenceHome adPreferenceHome = null;         
          
           // Initialize EJB Home
            
           try {
                
           	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                  lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
                
           } catch (NamingException ex) {
                
              throw new EJBException(ex.getMessage());
                
           }
          

           try {
          	
              LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
             
              return adPreference.getPrfInvCostPrecisionUnit();
             
           } catch (Exception ex) {
            	 
              Debug.printStackTrace(ex);
              throw new EJBException(ex.getMessage());
             
           }

       }
       
    
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getInvGpInventoryLineNumber(Integer AD_CMPNY) {

         Debug.print("InvAdjustmentRequestControllerBean getInvGpInventoryLineNumber");
                    
         LocalAdPreferenceHome adPreferenceHome = null;
        
        
         // Initialize EJB Home
          
         try {
              
         	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
        	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           
            return adPreference.getPrfInvInventoryLineNumber();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }
        
      }
      
      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/
       public ArrayList getAdApprovalNotifiedUsersByAdjCode(Integer ADJ_CODE, Integer AD_CMPNY) {

          Debug.print("InvAdjustmentRequestControllerBean getAdApprovalNotifiedUsersByAdjCode");

         
          LocalAdApprovalQueueHome adApprovalQueueHome = null;
          LocalInvAdjustmentHome invAdjustmentHome = null;
          
          ArrayList list = new ArrayList();
         
        
          // Initialize EJB Home
           
          try {
               
              adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);  
              invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
              	lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);   
               
          } catch (NamingException ex) {
               
              throw new EJBException(ex.getMessage());
               
          }

          try {
          	
          	LocalInvAdjustment invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
            
   	        if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {
   	         
   	           list.add("DOCUMENT POSTED");
   	           return list;
   	         	         
   	        }
          	
            Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("INV ADJUSTMENT REQUEST", ADJ_CODE, AD_CMPNY);
                     
            Iterator i = adApprovalQueues.iterator();
            
            while(i.hasNext()) {
            	
            	LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
            	
            	list.add(adApprovalQueue.getAdUser().getUsrDescription());
            	
            }
            
            return list;
            
          } catch (Exception ex) {
          	 
          	 Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
          }

     }
       
   /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/ 
     public double getInvIiUnitCostByIiNameAndUomNameAndLocNameAndDate(String II_NM, String UOM_NM, String LOC_NM, Date ADJ_DT, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("InvAdjustmentRequestControllerBean getInvIiUnitCostByIiNameAndUomNameAndLocNameAndDate");
                   
        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
      	LocalInvCostingHome invCostingHome = null;      	
      	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
      	LocalAdPreferenceHome adPreferenceHome = null;
        
        // Initialize EJB Home
         
        try {
             
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
        	invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
        	invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class); 
        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);  
     
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }

        try {
        	
        	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	
        	double COST = invItem.getIiUnitCost();
        	
        	// get ave cost 
			
     	if(LOC_NM != null && ADJ_DT != null) {
        		
        	/*	try { 
        			 
          			LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);
          		
          			LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(
          					ADJ_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
          			          			
          			if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))   
          			{
          				COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), adPreference.getPrfInvCostPrecisionUnit());
          			}
          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
          			{
          				COST = this.getInvFifoCost(ADJ_DT, invItemLocation.getIlCode(), invCosting.getCstAdjustQuantity(),
          						invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
         			}
          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
          			{
          				COST = invItemLocation.getInvItem().getIiUnitCost();
         			}
          			
          		} catch (FinderException ex) {
          			
          		}*/
          		
        	}
       		
      		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
        	
        	return EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvCostPrecisionUnit());      
        	
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
     
     // private methods
     
  /*   private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, 
    		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
 	 {
 	  	 
 		 LocalInvCostingHome invCostingHome = null;
 	  	 LocalInvItemLocationHome invItemLocationHome = null;
 	  	 LocalAdPreferenceHome adPreferenceHome = null;
 	       
 	     // Initialize EJB Home
 	        
 	     try {
 	         
 	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
 	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
 	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
 	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
 	    	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
 	     } 
 	     catch (NamingException ex) {
 	            
 	    	 throw new EJBException(ex.getMessage());
 	     }
 	    	
 		/*try {
 			
 			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
 			
 			if (invFifoCostings.size() > 0) {
 				
 				Iterator x = invFifoCostings.iterator();
 			
 	  			if (isAdjustFifo) {
 	  				
 	  				//executed during POST transaction
 	  				
 	  				double totalCost = 0d;
 	  				double cost;
 	  				
 	  				if(CST_QTY < 0) {
 	  					
 	  					//for negative quantities
 	 	  				double neededQty = -(CST_QTY);
 	 	  				
 	 	  				while(x.hasNext() && neededQty != 0) {
 	 	 	  				
 	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
 	
 	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {  				
 	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();  				
 	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
 	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
 	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {  				
 	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
 	 		 	  			} else {
 	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
 	 		 	  			}
 	
 	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {
 	 	  						
 	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
 	 			  				totalCost += (neededQty * cost);
 	 			  				neededQty = 0d;	 			  				 
 	 	  					} else {
 	 	  						
 	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
 	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
 	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
 	 	  					}
 	 	  				}
 	 	  				
 	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
 	 	  				if(neededQty != 0) {
 	 	  					
 	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
 	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
 	 	  				}
 	 	  				
 	 	  				cost = totalCost / -CST_QTY;
 	  				} 
 	  				
 	  				else {
 	  					
 	  					//for positive quantities
 	  					cost = CST_COST;
 	  				}
 	  				return cost;
 	  			}
 	  			
 	  			else {
 	  				
 	  				//executed during ENTRY transaction
 	  				
 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
 	  				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
 	  				
 	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
 	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), adPreference.getPrfInvCostPrecisionUnit());
	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), adPreference.getPrfInvCostPrecisionUnit());
 	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
 	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), adPreference.getPrfInvCostPrecisionUnit());
 	 	  			} else {
 	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), adPreference.getPrfInvCostPrecisionUnit());
 	 	  			}
 	  			}
 			} 
 			else {
 				
 				//most applicable in 1st entries of data
 				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
 				return invItemLocation.getInvItem().getIiUnitCost();
 			}
 				
 		}
 		catch (Exception ex) {
 			Debug.printStackTrace(ex);
 		    throw new EJBException(ex.getMessage());
 		}
 	}     */
     
     
     
     private LocalInvAdjustmentLine addInvAlEntry(InvModAdjustmentLineDetails mdetails, 
    	LocalInvAdjustment invAdjustment, byte AL_VD, Integer AD_CMPNY) throws
    	GlobalMiscInfoIsRequiredException{
		
		Debug.print("InvAdjustmentRequestControllerBean addInvAlEntry");
		
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
	       	          
	    // Initialize EJB Home
	    
	    try {
	        
	    	invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
	    	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class); 
	    	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
	                    
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }            
	            
	    try {
	    	
	    	LocalInvAdjustmentLine invAdjustmentLine = invAdjustmentLineHome.create(mdetails.getAlUnitCost(), mdetails.getAlQcNumber(), mdetails.getAlQcExpiryDate(),
	    			mdetails.getAlAdjustQuantity(),mdetails.getAlServed(), AL_VD, AD_CMPNY);
	    	
	    	System.out.println("al_code "+mdetails.getAlCode());
	    	//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
	    	invAdjustmentLine.setInvAdjustment(invAdjustment);
	    	LocalInvUnitOfMeasure invUnitOfMeasure = 
	    		invUnitOfMeasureHome.findByUomName(mdetails.getAlUomName(), AD_CMPNY);
	    	//invUnitOfMeasure.addInvAdjustmentLine(invAdjustmentLine);
	    	invAdjustmentLine.setInvUnitOfMeasure(invUnitOfMeasure);

	    	LocalInvItemLocation invItemLocation =
	    		invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(),
	    			mdetails.getAlIiName(), AD_CMPNY);
	    	//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
	    	invAdjustmentLine.setInvItemLocation(invItemLocation);

//	    	validate misc
	    	/*
	    	if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()==1){
	    		
	    		if(mdetails.getAlMisc()==null || mdetails.getAlMisc()==""){
	    			

	    			throw new GlobalMiscInfoIsRequiredException();
	    			
	    		}else{
	    			int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(mdetails.getAlMisc()));
	    			
	    			String miscList2Prpgt = this.checkExpiryDates(mdetails.getAlMisc(), qty2Prpgt, "False");
	    			if(miscList2Prpgt!="Error"){
	    				invAdjustmentLine.setAlMisc(mdetails.getAlMisc());
	    			}else{
	    				throw new GlobalMiscInfoIsRequiredException();
	    			}
	    			
	    		}
	    	}else{
	    		invAdjustmentLine.setAlMisc(mdetails.getAlMisc());
	    	}
	    	*/
	    	/*try{
	    		
	    	}catch (GlobalMiscInfoIsRequiredException ex){
	    		if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()==1){
	    			if(mdetails.getAlMisc()!=""){
	    				invAdjustmentLine.setAlMisc(mdetails.getAlMisc());
	    			}
		    		
		    	}
	    		//getSessionContext().setRollbackOnly();
		    	throw ex;
	    	}*/
	    	
	    	
            System.out.println("mdetails.getPlMisc() : "+mdetails.getAlMisc());
        
	     	return invAdjustmentLine;
	    							    		        		
	    }catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
		
	}	
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
			
		Debug.print("InvAdjustmentRequestControllerBean addInvDrEntry");
		
		LocalAdCompanyHome adCompanyHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;        
		LocalGlChartOfAccountHome glChartOfAccountHome = null;           
                
        // Initialize EJB Home
        
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
        	invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
             
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
                
        try {        
        	    
        	// get company
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {

    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		
    		} catch(FinderException ex) {

    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
        	        	        	
        	// create distribution record        
		    
		    LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
			    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    DR_RVRSL, EJBCommon.FALSE, AD_CMPNY);
			    
		    //invAdjustment.addInvDistributionRecord(invDistributionRecord);
		    invDistributionRecord.setInvAdjustment(invAdjustment);
			//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
			invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

        } catch(GlobalBranchAccountNumberInvalidException ex) {
			
			throw new GlobalBranchAccountNumberInvalidException ();
			

        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
    }		

    private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

    	Debug.print("ArInvoicePostControllerBean convertByUomFromAndUomToAndQuantity");		        

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
    		invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	

    		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
    		LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

    		return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvCostPrecisionUnit());       	        		        		       	

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }	  
    
	
	private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
			
		Debug.print("InvAdjustmentRequestControllerBean convertByUomFromAndItemAndQuantity");		        
        
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
		// Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
                   
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
        	        	
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
                        
        	return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvCostPrecisionUnit());
        						    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}		
	
	 private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException,
		GlobalExpiryDateNotFoundException,
		GlobalMiscInfoIsRequiredException{
                    
        Debug.print("InvAdjustmentRequestControllerBean executeInvAdjPost");
        
        LocalInvAdjustmentHome invAdjustmentHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
                        
                
        // Initialize EJB Home
        
        try {
            
            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	        	
        	// validate if adjustment is already deleted
        	
        	LocalInvAdjustment invAdjustment = null;
        	        	
        	try {
        		        			
        		invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
        			        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if adjustment is already posted or void
        	
        	if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
        		throw new GlobalTransactionAlreadyPostedException();
        		
            }
        	
        	// regenerate inventory dr

        	this.regenerateInventoryDr(invAdjustment, AD_BRNCH, AD_CMPNY);
        	
    		Collection invAdjustmentLines = null;
    		
    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

            
            Iterator c = invAdjustmentLines.iterator();
            
			/*while(c.hasNext()) {
				
				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) c.next();
				
				String II_NM = invAdjustmentLine.getInvItemLocation().getInvItem().getIiName();
				String LOC_NM = invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName();
				double ADJUST_COST = invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost();
				double ADJUST_QTY = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), 
				  invAdjustmentLine.getInvItemLocation().getInvItem(), invAdjustmentLine.getAlAdjustQuantity(), AD_CMPNY);
				
			//	LocalInvCosting invCosting = null;
				
				/*try {
					
					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(invAdjustment.getAdjDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
					
				} catch (FinderException ex) {
																											
				}*/
				
				/*if (invCosting == null && invAdjustmentLine.getInvItemLocation().getInvCostings().size() == 0) {
					
						this.post(invAdjustmentLine, invAdjustment.getAdjDate(), ADJUST_QTY, ADJUST_COST, ADJUST_QTY, ADJUST_COST, 0d,
								null, AD_BRNCH, AD_CMPNY);
								  				
				} else {
					
					double COST = 0d;
					
					if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
					{
						
						LocalInvCosting invLastCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invAdjustment.getAdjDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
						
						if (ADJUST_QTY >= 0) {
							
							//compute cost variance   
							double CST_VRNC_VL = 0d;
							
							if(invLastCosting.getCstRemainingQuantity() < 0)
								CST_VRNC_VL = ADJUST_QTY == 0 ? 0 : (invLastCosting.getCstRemainingQuantity() * (ADJUST_COST/ADJUST_QTY) -
										invLastCosting.getCstRemainingValue());
							
							this.post(invAdjustmentLine, invAdjustment.getAdjDate(), ADJUST_QTY, ADJUST_COST,
									invLastCosting.getCstRemainingQuantity() + ADJUST_QTY,
									invLastCosting.getCstRemainingValue() + ADJUST_COST, CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);
							
						} else {
							
							
							
							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
							
							this.post(invAdjustmentLine, invAdjustment.getAdjDate(), ADJUST_QTY, ADJUST_QTY * COST,
									invLastCosting.getCstRemainingQuantity() + ADJUST_QTY,
									invLastCosting.getCstRemainingValue() + (ADJUST_QTY * COST), 0d, null, AD_BRNCH, AD_CMPNY);
							
							//	set adjustment line unit cost
							
							COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
									invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
									COST, true, AD_CMPNY);
							
							invAdjustmentLine.setAlUnitCost(COST);
						}
						
					}
					else*/// if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
					{
						System.out.println("HELLO!!! This is FIFO!!!");
						
					/*	LocalInvCosting invFifoCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invAdjustment.getAdjDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);;
						
						double fifoCost = this.getInvFifoCost(invFifoCosting.getCstDate(), invFifoCosting.getInvItemLocation().getIlCode(), 
								invAdjustmentLine.getAlAdjustQuantity(), invAdjustmentLine.getAlUnitCost(), true, AD_BRNCH, AD_CMPNY);
						
						//post entries to database        			
						this.post(invAdjustmentLine, invAdjustmentLine.getInvAdjustment().getAdjDate(), ADJUST_QTY, fifoCost * ADJUST_QTY, 
								invFifoCosting.getCstRemainingQuantity() + ADJUST_QTY, 
								invFifoCosting.getCstRemainingValue() + (fifoCost * ADJUST_QTY), 0d, null, AD_BRNCH, AD_CMPNY);
						
						// set adjustment line unit cost
						COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
								invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
								fifoCost, true, AD_CMPNY);
						invAdjustmentLine.setAlUnitCost(COST);
						
					}
					else if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
          			{	
						LocalInvCosting invStandardCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invAdjustment.getAdjDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);;
						
						double standardCost = invStandardCosting.getInvItemLocation().getInvItem().getIiUnitCost();
						
						//post entries to database        			
						this.post(invAdjustmentLine, invAdjustmentLine.getInvAdjustment().getAdjDate(), ADJUST_QTY, standardCost * ADJUST_QTY, 
								invStandardCosting.getCstRemainingQuantity() + ADJUST_QTY, 
								invStandardCosting.getCstRemainingValue() + (standardCost * ADJUST_QTY), 0d, null, AD_BRNCH, AD_CMPNY);
						
						// set adjustment line unit cost
						COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
								invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
								standardCost, true, AD_CMPNY);
						invAdjustmentLine.setAlUnitCost(COST);
						
					}*/
					
					
				}
				
            				
		//	}			
        		
        	// post adjustment
        	
    	    // get credited invoice
    	    
    		// decrease invoice and ips amounts and release lock        		
    		        				                	
        	// set invoice post status
           
           invAdjustment.setAdjPosted(EJBCommon.TRUE);
           invAdjustment.setAdjPostedBy(USR_NM);
           invAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
                      
           // post to gl if necessary
           
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           
           if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL") || invAdjustment.getAdjIsCostVariance() == EJBCommon.TRUE) {
           	
           	   // validate if date has no period and period is closed
           	   
           	   LocalGlSetOfBook glJournalSetOfBook = null;
           	  
           	   try {
           	    
           	   	   glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);
           	   	   
           	   } catch (FinderException ex) {
           	   
           	       throw new GlJREffectiveDateNoPeriodExistException();
           	   
           	   }
			
			   LocalGlAccountingCalendarValue glAccountingCalendarValue = 
			        glAccountingCalendarValueHome.findByAcCodeAndDate(
			        	glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);
			        	
			        	
			   if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
			        glAccountingCalendarValue.getAcvStatus() == 'C' ||
			        glAccountingCalendarValue.getAcvStatus() == 'P') {
			        	
			        throw new GlJREffectiveDatePeriodClosedException();
			        
			   }
			   
			   // check if invoice is balance if not check suspense posting
		            	
	           LocalGlJournalLine glOffsetJournalLine = null;
	        	
   			Collection invDistributionRecords = null;
			
   			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {
   				
   				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
   						invAdjustment.getAdjCode(), AD_CMPNY);
   				
   			} else {
   				
   				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
   						invAdjustment.getAdjCode(), AD_CMPNY);
   				
   			}

	            
	           Iterator j = invDistributionRecords.iterator();
	        	
	           double TOTAL_DEBIT = 0d;
	           double TOTAL_CREDIT = 0d;
	        	
	           while (j.hasNext()) {
	        		
	        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
	        		
	        		double DR_AMNT = 0d;
	        		
	        		DR_AMNT = invDistributionRecord.getDrAmount();
	        		
	        		if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
	        			
	        			TOTAL_DEBIT += DR_AMNT;
	        			            			
	        		} else {
	        			
	        			TOTAL_CREDIT += DR_AMNT;
	        			
	        		}
	        		
	        	}
	        	
	        	TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
	        	TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
	        	            	            	            	
	        	if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
	        	    TOTAL_DEBIT != TOTAL_CREDIT) {
	        	    	
	        	    LocalGlSuspenseAccount glSuspenseAccount = null;
	        	    	
	        	    try { 	
	        	    	
	        	        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS", AD_CMPNY);
	        	        
	        	    } catch (FinderException ex) {
	        	    	
	        	    	throw new GlobalJournalNotBalanceException();
	        	    	
	        	    }
	        	               	    
	        	    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
	        	    	
	        	    	glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE, 
	        	    	    TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
	        	    	              	        
	        	    } else {
	        	    	
	        	    	glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
	        	    	    TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
	        	    	
	        	    }
	        	    
	        	    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
	        	    //glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
	        	    glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
	        	    
	        	    	
				} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
				    TOTAL_DEBIT != TOTAL_CREDIT) {
				    
					throw new GlobalJournalNotBalanceException();		    	
				    	
				}
			   		       
		       // create journal batch if necessary
		       
		       LocalGlJournalBatch glJournalBatch = null;
		       java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
		       
		       try {
		           
   	   	   			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
		       	   	
		       } catch (FinderException ex) {
		       
		       }
				     	
		       if (glJournalBatch == null) {
		     		
	     		    glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
		     		
		       }
		     				     		
			   // create journal entry			            	
		            		            	
        	   LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
        		    invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
        		    0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
        		    'N', EJBCommon.TRUE, EJBCommon.FALSE,
        		    USR_NM, new Date(),
        		    USR_NM, new Date(),
        		    null, null,
        		    USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
        		    null, null, EJBCommon.FALSE, null, 
        		    AD_BRNCH, AD_CMPNY);
        		    
        	   LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
	           glJournal.setGlJournalSource(glJournalSource);
	        	
	           LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
	           glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
	        	
	           LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
	           glJournal.setGlJournalCategory(glJournalCategory);
        		
        	   if (glJournalBatch != null) {

        		   glJournal.setGlJournalBatch(glJournalBatch);
        			
        	   }            	       	            		            
               // create journal lines
            	          	
               j = invDistributionRecords.iterator();
            	
               while (j.hasNext()) {
            		
            		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
            		
            		double DR_AMNT = 0d;
	        		
            		DR_AMNT = invDistributionRecord.getDrAmount();
            		            		
            		LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
            			invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
            			
                    //invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
                    glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());
            	    
            	    //glJournal.addGlJournalLine(glJournalLine);
            	    glJournalLine.setGlJournal(glJournal);
            	    
            	    invDistributionRecord.setDrImported(EJBCommon.TRUE);
            	    
            		
               }
            	
               if (glOffsetJournalLine != null) {
            		
            	   //glJournal.addGlJournalLine(glOffsetJournalLine);
            	   glOffsetJournalLine.setGlJournal(glJournal);
            		
               }		
               
               // post journal to gl
               
               Collection glJournalLines = glJournal.getGlJournalLines();
	       
		       Iterator i = glJournalLines.iterator();
		       
		       while (i.hasNext()) {
		       	
		           LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
		           		           
		           // post current to current acv
		             
		           this.postToGl(glAccountingCalendarValue,
		               glJournalLine.getGlChartOfAccount(),
		               true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
			 	 
		         
		           // post to subsequent acvs (propagate)
		         
		           Collection glSubsequentAccountingCalendarValues = 
		               glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
		                   glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
		                   glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
		                 
		           Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
		         
		           while (acvsIter.hasNext()) {
		         	
		         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
		         	       (LocalGlAccountingCalendarValue)acvsIter.next();
		         	       
		         	   this.postToGl(glSubsequentAccountingCalendarValue,
		         	       glJournalLine.getGlChartOfAccount(),
		         	       false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
		         		         	
		           }
		           
		           // post to subsequent years if necessary
	           
		           Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
			  	  	
			  	  	if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
			  	  	
				  	  	adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
				  	  	LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY); 	  	
			  	  	
				  	  	Iterator sobIter = glSubsequentSetOfBooks.iterator();
				  	  	
				  	  	while (sobIter.hasNext()) {
				  	  		
				  	  		LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
				  	  		
				  	  		String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
				  	  		
				  	  		// post to subsequent acvs of subsequent set of book(propagate)
		         
				           Collection glAccountingCalendarValues = 
				               glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
				                 
				           Iterator acvIter = glAccountingCalendarValues.iterator();
				         
				           while (acvIter.hasNext()) {
				         	
				         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
				         	       (LocalGlAccountingCalendarValue)acvIter.next();
				         	       
				         	    if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
					 				ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
				         	       
						         	this.postToGl(glSubsequentAccountingCalendarValue,
						         	     glJournalLine.getGlChartOfAccount(),
						         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
						         	     
						        } else { // revenue & expense
						        					             					             
						             this.postToGl(glSubsequentAccountingCalendarValue,
						         	     glRetainedEarningsAccount,
						         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
						        
						        }
				         		         	
				           }
				  	  		
				  	  	   if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
				  	  		
				  	  	}
				  	  	
			  	  	}
		       	   	       	   	       	
	           }
	           
	        } 			   
           	           	           	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch(GlobalBranchAccountNumberInvalidException ex) {
			
			throw ex;
        	
      // } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        //	getSessionContext().setRollbackOnly();
        //	throw ex;
        	
    	//}catch (GlobalExpiryDateNotFoundException ex) {
        	
        	//getSessionContext().setRollbackOnly();
        	//throw ex;
        	
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
 /*   private void post(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY, double CST_ADJST_CST,
    		double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH,Integer AD_CMPNY) throws
    		AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {
    
    	Debug.print("InvAdjustmentRequestControllerBean post");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	          lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
          
        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
           int CST_LN_NMBR = 0;
           
           CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adPreference.getPrfInvCostPrecisionUnit());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adPreference.getPrfInvCostPrecisionUnit());
                      
           if (CST_ADJST_QTY < 0) {
                      
           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
           
           }

         /*  try {
        	   System.out.println("generate line number");
           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
               System.out.println("generate line number: " + CST_LN_NMBR);
           } catch (FinderException ex) {
        	   System.out.println("generate line number CATCH");
           	   CST_LN_NMBR = 1;
           
           }*/
           
          /* if(CST_VRNC_VL != 0) {
           	
            //void subsequent cost variance adjustments
           /* Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
            Iterator i = invAdjustmentLines.iterator();
            
            while (i.hasNext()){
            	
            	LocalInvAdjustmentLine invAdjustmentLineTemp = (LocalInvAdjustmentLine) i.next();
            	this.voidInvAdjustment(invAdjustmentLineTemp.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
            	
            }
           	
           }
           
           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
         /*  try {           
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   System.out.println(prevCst.getCstCode()+"   "+ prevCst.getCstRemainingQuantity());
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();
        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }
        	   System.out.println("prevExpiryDates: " + prevExpiryDates);
        	   System.out.println("qtyPrpgt: " + qtyPrpgt);
           }catch (Exception ex){
        	   System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
        	   prevExpiryDates="";
           }

           //create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
           //invItemLocation.addInvCosting(invCosting);
           invCosting.setInvItemLocation(invItemLocation);
           invCosting.setInvAdjustmentLine(invAdjustmentLine);
           */
           //         Get Latest Expiry Dates           
          /* if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
        	   if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){

        		   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
        			   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));

        			   String miscList2Prpgt = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), qty2Prpgt, "False");
        			   ArrayList miscList = this.expiryDates(invAdjustmentLine.getAlMisc(), qty2Prpgt);
        			   String propagateMiscPrpgt = "";
        			   String ret = "";
        			   String exp = "";
        			   String Checker = "";

        			   //ArrayList miscList2 = null;
        			   if(CST_ADJST_QTY>0){
        				   prevExpiryDates = prevExpiryDates.substring(1);
        				   propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
        			   }else{
        				   Iterator mi = miscList.iterator();

        				   propagateMiscPrpgt = prevExpiryDates;
        				   ret = propagateMiscPrpgt;
        				   while(mi.hasNext()){
        					   String miscStr = (String)mi.next();

        					   Integer qTest = this.checkExpiryDates(ret+"fin$");
        					   ArrayList miscList2 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

        					   //ArrayList miscList2 = this.expiryDates("$" + ret, qtyPrpgt);
        					   Iterator m2 = miscList2.iterator();
        					   ret = "";
        					   String ret2 = "false";
        					   int a = 0;
        					   while(m2.hasNext()){
        						   String miscStr2 = (String)m2.next();

        						   if(ret2=="1st"){
        							   ret2 = "false";
        						   }
        						   System.out.println("miscStr1: "+miscStr);
        						   System.out.println("miscStr2: "+miscStr2);

        						   if(miscStr2.trim().equals(miscStr.trim())){
        							   if(a==0){
        								   a = 1;
        								   ret2 = "1st";
        								   Checker = "true";
        							   }else{
        								   a = a+1;
        								   ret2 = "true";
        							   }
        						   }
        						   System.out.println("Checker: "+Checker);
        						   System.out.println("ret2: "+ret2);
        						   if(!miscStr2.equals(miscStr) || a>1){
        							   if((ret2!="1st")&&((ret2=="false")||(ret2=="true"))){
        								   if (miscStr2!=""){
        									   miscStr2 = "$" + miscStr2;
        									   ret = ret + miscStr2;
        									   System.out.println("ret " + ret);
        									   ret2 = "false";
        								   }
        							   }
        						   }

        					   }
        					   if(ret!=""){
        						   ret = ret + "$";
        					   }
        					   System.out.println("ret una: "+ret);
        					   exp = exp + ret;
        					   qtyPrpgt= qtyPrpgt -1;
        				   }
        				   System.out.println("ret fin " + ret);
        				   System.out.println("exp fin " + exp);
        				   //propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
        				   propagateMiscPrpgt = ret;
        				   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
        				   if(Checker=="true"){
        					   //invCosting.setCstExpiryDate(propagateMiscPrpgt);
        				   }else{
        					   System.out.println("Exp Not Found");
        					   throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
        				   }
        			   }
        			  // invCosting.setCstExpiryDate(propagateMiscPrpgt);

        		   }else{
        			 //  invCosting.setCstExpiryDate(prevExpiryDates);
        		   }

        	   }else{
        		   System.out.println("invAdjustmentLine ETO NA: "+ invAdjustmentLine.getAlAdjustQuantity());
        		   if(invAdjustmentLine.getAlAdjustQuantity()>0){
        			   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
        				   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
        				   String initialPrpgt = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), initialQty, "False");

        				   //invCosting.setCstExpiryDate(initialPrpgt);
        			   }else{
        				 //  invCosting.setCstExpiryDate(prevExpiryDates);
        			   }
        		   }else{                    	
        			   throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
        		   }

        	   }
           }
           
           
			// if cost variance is not 0, generate cost variance for the transaction 
			/*if(CST_VRNC_VL != 0) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"INVADJ" + invAdjustmentLine.getInvAdjustment().getAdjDocumentNumber(),
						invAdjustmentLine.getInvAdjustment().getAdjDescription(),
						invAdjustmentLine.getInvAdjustment().getAdjDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				
			}*/

           // propagate balance if necessary           
        //   Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        	   
         // Iterator i = invCostings.iterator();

        	   /* String miscList = "";
           ArrayList miscList2 = null;
           //double qty = 0d;
                 

           System.out.println("miscList Propagate:" + miscList);
           String propagateMisc ="";
		   String ret = "";
		   

           while (i.hasNext()) {
        	   String Checker = "";
        	   String Checker2 = "";
        	   
        	  /* LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

        	   invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
        	   invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
        	   System.out.println("invPropagatedCosting.getCstExpiryDate() : " + invPropagatedCosting.getCstExpiryDate());
        	   if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
        		   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
        			   double qty = Double.parseDouble(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
        			   //invPropagatedCosting.getInvAdjustmentLine().getAlMisc();
        			   miscList = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), qty, "False");
        			   miscList2 = this.expiryDates(invAdjustmentLine.getAlMisc(), qty);
        			   System.out.println("invAdjustmentLine.getAlMisc(): "+invAdjustmentLine.getAlMisc());
        			   System.out.println("getAlAdjustQuantity(): "+invAdjustmentLine.getAlAdjustQuantity());

        			   if(invAdjustmentLine.getAlAdjustQuantity()<0){
        				   Iterator mi = miscList2.iterator();

        				   propagateMisc = invPropagatedCosting.getCstExpiryDate();
        				   ret = invPropagatedCosting.getCstExpiryDate();
        				   while(mi.hasNext()){
        					   String miscStr = (String)mi.next();

        					   Integer qTest = this.checkExpiryDates(ret+"fin$");
        					   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

        					   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
        					   System.out.println("ret: " + ret);
        					   Iterator m2 = miscList3.iterator();
        					   ret = "";
        					   String ret2 = "false";
        					   int a = 0;
        					   while(m2.hasNext()){
        						   String miscStr2 = (String)m2.next();

        						   if(ret2=="1st"){
        							   ret2 = "false";
        						   }
        						   System.out.println("2 miscStr: "+miscStr);
        						   System.out.println("2 miscStr2: "+miscStr2);
        						   if(miscStr2.equals(miscStr)){
        							   if(a==0){
        								   a = 1;
        								   ret2 = "1st";
        								   Checker2 = "true";
        							   }else{
        								   a = a+1;
        								   ret2 = "true";
        							   }
        						   }
        						   System.out.println("Checker: "+Checker2);
        						   if(!miscStr2.equals(miscStr) || a>1){
        							   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
        								   if (miscStr2!=""){
        									   miscStr2 = "$" + miscStr2;
        									   ret = ret + miscStr2;
        									   ret2 = "false";
        								   }
        							   }
        						   }

        					   }
        					   if(Checker2!="true"){
        						   throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
        					   }else{
        						   System.out.println("TAE");
        					   }

        					   ret = ret + "$";
        					   qtyPrpgt= qtyPrpgt -1;
        				   }
        			   }

        		   }*/

        		/*   System.out.println("getAlAdjustQuantity(): "+invAdjustmentLine.getAlAdjustQuantity());
        		   if(invAdjustmentLine.getAlAdjustQuantity()<0){

        			   Iterator mi = miscList2.iterator();

        			  // propagateMisc = invPropagatedCosting.getCstExpiryDate();
        			   ret = propagateMisc;
        			   while(mi.hasNext()){
        				   String miscStr = (String)mi.next();

        				   Integer qTest = this.checkExpiryDates(ret+"fin$");
        				   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

        				   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
        				   System.out.println("ret: " + ret);
        				   Iterator m2 = miscList3.iterator();
        				   ret = "";
        				   String ret2 = "false";
        				   int a = 0;
        				   while(m2.hasNext()){
        					   String miscStr2 = (String)m2.next();

        					   if(ret2=="1st"){
        						   ret2 = "false";
        					   }
        					   System.out.println("2 miscStr: "+miscStr);
        					   System.out.println("2 miscStr2: "+miscStr2);
        					   if(miscStr2.equals(miscStr)){
        						   if(a==0){
        							   a = 1;
        							   ret2 = "1st";
        							   Checker = "true";
        						   }else{
        							   a = a+1;
        							   ret2 = "true";
        						   }
        					   }
        					   System.out.println("Checker: "+Checker);
        					   if(!miscStr2.equals(miscStr) || a>1){
        						   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
        							   if (miscStr2!=""){
        								   miscStr2 = "$" + miscStr2;
        								   ret = ret + miscStr2;
        								   ret2 = "false";
        							   }
        						   }
        					   }

        				   }
        				   ret = ret + "$";
        				   qtyPrpgt= qtyPrpgt -1;
        			   }
        			   propagateMisc = ret;
        		   }else{
        			   //propagateMisc = miscList+invPropagatedCosting.getCstExpiryDate().substring(1, invPropagatedCosting.getCstExpiryDate().length());
        		   }

        		  // invPropagatedCosting.setCstExpiryDate(propagateMisc);
        	   }
        	   

           }
           
           // regenerate cost variance
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
           
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
           	
           	throw ex;

        } catch (GlobalExpiryDateNotFoundException ex){
           	
           	throw ex;

        } catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }

    }*/
    
    public static int checkExpiryDates(String misc) throws Exception {

		String separator ="$";

		// Remove first $ character
		misc = misc.substring(1);
		//System.out.println("misc: " + misc);
		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;	
		int numberExpry=0;
		String miscList = new String();
		String miscList2 = "";
		String g= "";
		try{
			while(g!="fin"){
				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				g= misc.substring(start, start + length);
				if(g.length()!=0){
					if(g!=null || g!="" || g!="null"){
						if(g.contains("null")){
							miscList2 = "Error";
						}else{
							miscList = miscList + "$" + g;
							numberExpry++;
						}
					}else{
						miscList2 = "Error";
					}

				}else{
					miscList2 = "Error";
				}
			}	
		}catch(Exception e){

		}

		if(miscList2==""){
			miscList = miscList+"$";
		}else{
			miscList = miscList2;
		}

		return (numberExpry);
	}
    
    public String getQuantityExpiryDates(String qntty){
    	String separator = "$";
    	
    	// Remove first $ character
    	qntty = qntty.substring(1);
    	
    	// Counter
    	int start = 0;
    	int nextIndex = qntty.indexOf(separator, start);
    	int length = nextIndex - start;	
    	String y;
    	y = (qntty.substring(start, start + length));
    	System.out.println("Y " + y);
    	
    	return y;
    }
        
    private ArrayList expiryDates(String misc, double qty) throws Exception{
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	String separator ="$";
    	
    	
    	// Remove first $ character
    	misc = misc.substring(1);
    	
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	
    	
    	System.out.println("qty" + qty);
    	ArrayList miscList = new ArrayList();
    	
    	for(int x=0; x<qty; x++) {
    		
    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		
    		String checker = misc.substring(start, start + length);
    		if(checker.length()!=0 || checker!="null"){
    			miscList.add(checker);
    		}else{
    			miscList.add("null");
    			qty++;
    		}
    	}	
    	
    	System.out.println("miscList :" + miscList);
    	return miscList;
    }
        
    public String propagateExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	
    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}
    	
    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	
    	
    	String miscList = new String();
    	
    	for(int x=0; x<qty; x++) {
    		
    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		String g= misc.substring(start, start + length);
    		System.out.println("g: " + g);
    		System.out.println("g length: " + g.length());
    		if(g.length()!=0){
    			miscList = miscList + "$" + g;	
    			System.out.println("miscList G: " + miscList);
    		}
    	}	
    	
    	miscList = miscList+"$";
    	System.out.println("miscList :" + miscList);
    	return (miscList);
    }
    
    public String checkExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	
    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}
    	
    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	
    	
    	String miscList = new String();
    	String miscList2 = "";
    	
    	for(int x=0; x<qty; x++) {
    		
    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		String g= misc.substring(start, start + length);
    		System.out.println("g: " + g);
    		System.out.println("g length: " + g.length());
    		if(g.length()!=0){
    			if(g!=null || g!="" || g!="null"){
    				if(g.contains("null")){
    					miscList2 = "Error";
    				}else{
    					miscList = miscList + "$" + g;
    				}
    			}else{
    				miscList2 = "Error";
    			}
    				
    			System.out.println("miscList G: " + miscList);
    		}else{
    			System.out.println("KABOOM");
    			miscList2 = "Error";
    		}
    	}	
    	System.out.println("miscList2 :" + miscList2);
    	if(miscList2==""){
    		miscList = miscList+"$";
    	}else{
    		miscList = miscList2;
    	}
    	
    	System.out.println("miscList :" + miscList);
    	return (miscList);
    }
    
    private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
    		LocalGlChartOfAccount glChartOfAccount, 
			boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
    	
    	Debug.print("InvAdjustmentRequestControllerBean postToGl");
    	
    	LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {          
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
    		
    		LocalGlChartOfAccountBalance glChartOfAccountBalance = 
    			glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
    					glAccountingCalendarValue.getAcvCode(),
						glChartOfAccount.getCoaCode(), AD_CMPNY);
    		
    		String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
    		short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcExtendedPrecision();
    		
    		
    		
    		if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
    				isDebit == EJBCommon.TRUE) ||
					(!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
							isDebit == EJBCommon.FALSE)) {				    
    			
    			glChartOfAccountBalance.setCoabEndingBalance(
    					EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
    			
    			if (!isCurrentAcv) {
    				
    				glChartOfAccountBalance.setCoabBeginningBalance(
    						EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
    				
    			}
    			
    			
    		} else {
    			
    			glChartOfAccountBalance.setCoabEndingBalance(
    					EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
    			
    			if (!isCurrentAcv) {
    				
    				glChartOfAccountBalance.setCoabBeginningBalance(
    						EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
    				
    			}
    			
    		}
    		
    		if (isCurrentAcv) { 
    			
    			if (isDebit == EJBCommon.TRUE) {
    				
    				glChartOfAccountBalance.setCoabTotalDebit(
    						EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
    				
    			} else {
    				
    				glChartOfAccountBalance.setCoabTotalCredit(
    						EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
    			}       	   
    			
    		}
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    }
    
    
    
    private void regenerateInventoryDr(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvAdjustmentRequestControllerBean regenerateInventoryDr");		        
    	
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class); 
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class); 
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {

        	// regenerate inventory distribution records

            // remove all inventory distribution
            
    		Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByAdjCode(
    			invAdjustment.getAdjCode(), AD_CMPNY);
    		
    		Iterator i = invDistributionRecords.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    			i.remove();
    			invDistributionRecord.remove();
    			
    		}
    		
        	// remove all adjustment lines committed qty
        	
    		Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
        	
        	i = invAdjustmentLines.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
        		
        		if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
        		
        		    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
        		
        			invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);
        			
        		}
        		
        	}
    		
    		invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    		
    		if(invAdjustmentLines != null && !invAdjustmentLines.isEmpty()) {
    			
    			byte DEBIT = 0;
    			double TOTAL_AMOUNT = 0d; 
    			
    			i = invAdjustmentLines.iterator();
    			
    			while(i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
    				LocalInvItemLocation invItemLocation=invAdjustmentLine.getInvItemLocation();
    				
    				// start date validation
    				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    				if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	    				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	    					invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	    				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
    				}
    				
    				// add physical inventory distribution
    				
    				double AMOUNT = 0d;
    				
    				if (invAdjustmentLine.getAlAdjustQuantity() > 0) {
    					
    					AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() *
    							invAdjustmentLine.getAlUnitCost(), adPreference.getPrfInvQuantityPrecisionUnit());
    					DEBIT = EJBCommon.TRUE;
    					
    				} else {
    					
    					double COST = 0d;
    					
    					/*try {
    						
    						LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(
    								invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
									invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    						
    						if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))   
    	          			{
    	          				COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), adPreference.getPrfInvQuantityPrecisionUnit());
    	          			}
    	          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
    	          			{
    	          				COST = this.getInvFifoCost(invAdjustment.getAdjDate(), invItemLocation.getIlCode(), invCosting.getCstAdjustQuantity(),
    	          						invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
    	         			}
    	          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
    	          			{
    	          				COST = invItemLocation.getInvItem().getIiUnitCost();
    	         			}
    						
    					} catch (FinderException ex) {
    						
    						COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();
    						
    					}*/
    					
    					/*COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
		 	  	        		invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
								COST, true, AD_CMPNY);
    					
    					/*AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() * COST,
    							adPreference.getPrfInvQuantityPrecisionUnit());*/
    					/*AMOUNT = invAdjustmentLine.getAlAdjustQuantity() * COST;
    					DEBIT = EJBCommon.FALSE; */
    					
    				}
    				
    				// check for branch mapping
    				
    				LocalAdBranchItemLocation adBranchItemLocation = null;

    				try{
	 	  	    	
	 	  	    		adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
	 	  	    				invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {

	 	  	    		throw new GlobalBranchAccountNumberInvalidException ();
	 	  	    		
	 	  	    	}

    				LocalGlChartOfAccount glInventoryChartOfAccount = null;

    				if (adBranchItemLocation == null) {
    					
    					glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    							invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    				} else {
    					
    					glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    							adBranchItemLocation.getBilCoaGlAccruedInventoryAccount());
    					
    				}
	 	  	    	
    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY REQUEST", DEBIT, Math.abs(AMOUNT), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    				TOTAL_AMOUNT += AMOUNT;
    				//ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);
    				
    				// add adjust quantity to item location committed
    				// quantity if negative
    				
    				if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
    					
    					double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
    							invAdjustmentLine.getInvUnitOfMeasure(),
								invAdjustmentLine.getInvItemLocation().getInvItem(),
								Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
    					
    					invItemLocation = invAdjustmentLine.getInvItemLocation();
    					
    					invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() +
    							convertedQuantity);
    					
    				}
    				
    			}
    			
    			// add variance or transfer/debit distribution
    			
    			DEBIT = TOTAL_AMOUNT > 0 ? EJBCommon.FALSE : EJBCommon.TRUE;
    			
    			this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_AMOUNT), EJBCommon.FALSE,
    					invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    			
    		}
    		
    	} catch (GlobalInventoryDateException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch(GlobalBranchAccountNumberInvalidException ex) {
			
			throw new GlobalBranchAccountNumberInvalidException ();
			
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {
		
		Debug.print("InvAdjustmentRequestControllerBean convertCostByUom");		        
	    
		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		        
	    // Initialize EJB Home
	    
	    try {
	        
	        invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
	        invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
	           
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	            
	    try {
	    
	    	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
	                    
	        if (isFromDefault) {	        	
	        
	        	return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();
	    	
	        } else {
	        	
	        	return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();
	        	
	        }
	    	
	    	
	    						    		        		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
		
	}

    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ApVoucherPostController voidInvAdjustment");
    	
    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();
    			
    			Iterator i = invDistributionRecords.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    				
    				list.add(invDistributionRecord);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();    	    			    			

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(), 
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    			}	    

    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
    				
    				list.add(invAdjustmentLine);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();    	    			    			
    				
    				InvModAdjustmentLineDetails details = new InvModAdjustmentLineDetails();
    				
    	    		details.setAlAdjustQuantity(0);
    	    		details.setAlUnitCost(invAdjustmentLine.getAlUnitCost() * - 1);
    	    		details.setAlIiName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
    	    		details.setAlLocName(invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName());
    	    		details.setAlUomName(invAdjustmentLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
    				
    				this.addInvAlEntry(details, invAdjustment, EJBCommon.TRUE, AD_CMPNY);

    			}
    			
    			invAdjustment.setAdjVoid(EJBCommon.TRUE);
    			
    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
      **/
    public Integer saveInvAdjEntry1(ArrayList arList,Integer ADJ_CODE, String CRTD_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
	
	{
	
	Debug.print("InvAdjustmentRequestControllerBean saveInvAdjGenerate");
	  System.out.print("printC7");
	LocalInvAdjustmentHome invAdjustmentHome = null;
	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
	LocalGlChartOfAccountHome glChartOfAccountHome = null;
	LocalAdApprovalHome adApprovalHome = null;
	LocalAdAmountLimitHome adAmountLimitHome = null;
	LocalAdApprovalUserHome adApprovalUserHome = null;
	LocalAdApprovalQueueHome adApprovalQueueHome = null;
	LocalInvItemLocationHome invItemLocationHome = null;
	LocalInvCostingHome invCostingHome = null;
	LocalAdPreferenceHome adPreferenceHome = null;
	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
	LocalAdBranchItemLocationHome adBranchItemLocationHome = null; 
	LocalInvItemHome invItemHome = null;
	
    // Initialize EJB Home
    
    try {
        
    	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    	invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
        lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
    	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
        	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    	adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
    	adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
    	adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
    	adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
    	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);        	
    	invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);	
		adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class); 
		adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 
		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
		invItemHome = (LocalInvItemHome)EJBHomeFactory.
        	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
		
    } catch (NamingException ex) {
        
        throw new EJBException(ex.getMessage());
    }
    
    try {
    	
    	LocalInvAdjustment invAdjustment = null;
    	
    	// validate if Adjustment is already deleted
    	
    	
    	
    	// validate if adjustment is already posted, void, approved or pending
    	
    
        	
        	
        //	if (invAdjustment.getAdjApprovalStatus() != null) {
    		
        	
        		
        		
        	//	if (invAdjustment.getAdjApprovalStatus().equals("APPROVED") ||
        //			invAdjustment.getAdjApprovalStatus().equals("N/A")) {         		    	
        	//	    throw new GlobalTransactionAlreadyApprovedException(); 
        		    	
        		    	
        //		} else if (invAdjustment.getAdjApprovalStatus().equals("PENDING")) {
        			
        	//		throw new GlobalTransactionAlreadyPendingException();
        			
       // 		}
        //		
        //	}
    			
    	//	if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {
    			
    	//		throw new GlobalTransactionAlreadyPostedException();
    			
    	//	}
    		
    
    	
    	LocalInvAdjustment invAdjustmentRequest = null;
    	LocalInvAdjustmentLine invAdjustmentRequestLine = null;
    	try {
    		
    		invAdjustmentRequest = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
    		
    	} catch (FinderException ex) { 
    		
    	}
   //   try {
    		
    		//invAdjustmentRequestLine = invAdjustmentLineHome.findByPrimaryKey(ADJ_CODE);
    		
    	//} catch (FinderException ex) { 
    		
    	//}
    	
    	
    	// 	validate if document number is unique document number is automatic then set next sequence

        
        	
        	
        	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	        LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
        	
        	
        	
        	try {
        		
        		adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT REQUEST", AD_CMPNY);
        		
        	} catch (FinderException ex) {
        		
        	}
        	
        	try {
        		
        		adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
        		
        	} catch (FinderException ex) {
        		
        	}
        	String ADJ_NMBR = null;
        	if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' ) {
        		
        		while (true) {
        			
        			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
        				
        				try {

        					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);		            		
        					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
        					
        				} catch (FinderException ex) {
        					
        					ADJ_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
        					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
        					break;
        					
        				}
        				
						} else {
							
							try {
								
								invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
								
							} catch (FinderException ex) {
								
								ADJ_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
								break;
								
							}
							
						}

        		}		            
        		
        
        	
        } 
    	
    	// used in checking if invoice should re-generate distribution records and re-calculate taxes	        	                	
        //boolean isRecalculate = true;
        
    	
    	// create adjustment
    	
        	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
          	String ADJ_DESC = "GENERATED ADJUSTMENT REQUEST " + formatter.format(new java.util.Date());
          	Date CURR_DT = EJBCommon.getGcCurrentDateWoTime().getTime();
    		invAdjustment = invAdjustmentHome.create(ADJ_NMBR,invAdjustmentRequest.getAdjDocumentNumber(), ADJ_DESC, 
    				CURR_DT, "ISSUANCE", null,
					EJBCommon.FALSE, CRTD_BY, CURR_DT,
					CRTD_BY, CURR_DT,
					null, null, null, null, invAdjustmentRequest.getAdjNotedBy() , null, EJBCommon.FALSE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
    		
    		//invAdjustmentRequest.setAdjApprovalStatus(null);
    		//invAdjustmentRequest.setAdjPosted(EJBCommon.FALSE);
    		
    	
            // check if critical fields are changed
    		/*
    		if (!invAdjustment.getGlChartOfAccount().getCoaAccountNumber().equals(COA_ACCOUNT_NUMBER) ||
				alList.size() != invAdjustment.getInvAdjustmentLines().size() ||
				!(invAdjustment.getAdjDate().equals(details.getAdjDate()))) {
    			
    			isRecalculate = true;
    			
    		} else if (alList.size() == invAdjustment.getInvAdjustmentLines().size()) {
    			
    			Iterator alIter = invAdjustment.getInvAdjustmentLines().iterator();
    			Iterator alListIter = alList.iterator();
    			
    			while (alIter.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)alIter.next();
    				InvModAdjustmentLineDetails mdetails = (InvModAdjustmentLineDetails)alListIter.next();
    				      System.out.print("controller"+mdetails.getAlAdjustQuantity());  				
    				if (!invAdjustmentLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getAlIiName()) ||
        				!invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getAlLocName()) ||
        				!invAdjustmentLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getAlUomName()) ||
        				invAdjustmentLine.getAlAdjustQuantity() != mdetails.getAlAdjustQuantity() ||
        				invAdjustmentLine.getAlUnitCost() != mdetails.getAlUnitCost() || 
        				invAdjustmentLine.getAlMisc() != mdetails.getAlMisc()) {
    					
    					isRecalculate = true;
    					break;
    					
    				}
    				
    				isRecalculate = false;
    				
    			}
    			
    		} else {
    			
    			isRecalculate = false;
    			
    		}
    		
    		invAdjustment.setAdjDocumentNumber(details.getAdjDocumentNumber());
    		invAdjustment.setAdjReferenceNumber(details.getAdjReferenceNumber());
    		invAdjustment.setAdjDescription(details.getAdjDescription());
    		invAdjustment.setAdjDate(details.getAdjDate());
    		invAdjustment.setAdjType(details.getAdjType());
    		invAdjustment.setAdjApprovalStatus(details.getAdjApprovalStatus());
    		invAdjustment.setAdjLastModifiedBy(details.getAdjLastModifiedBy());
    		invAdjustment.setAdjDateLastModified(details.getAdjDateLastModified());
    		
    		invAdjustment.setAdjReasonForRejection(null);
    		
    	
    	*/
    	try {
    		        		
    		LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(invAdjustmentRequest.getGlChartOfAccount().getCoaAccountNumber(), AD_BRNCH, AD_CMPNY);
        	//glChartOfAccount.addInvAdjustment(invAdjustment);
        	invAdjustment.setGlChartOfAccount(glChartOfAccount);
    		
    	} catch (FinderException ex) {
    		
    		throw new GlobalAccountNumberInvalidException();
    		
    	}
    	
    	double ABS_TOTAL_AMOUNT = 0d;
    	
    	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    	
    	/*
    	if (isRecalculate) {
    	        	
        	// remove all adjustment lines
        	*/
        	Collection invAdjustmentLines = invAdjustmentRequest.getInvAdjustmentLines();
        	
        	Iterator i = invAdjustmentLines.iterator();
        /*	
        	while (i.hasNext()) {
        		
        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
        		
        		if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
        		
        		    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
        		
        			invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);
        			
        		}
        		
        		i.remove();
        		
        		invAdjustmentLine.remove();
        		
        	}*/
        	
        	// remove all distribution records
       	   
 	  	    Collection arDistributionRecords = invAdjustmentRequest.getInvDistributionRecords();
 	  	    /*
 	  	    i = arDistributionRecords.iterator();     	  
 	  	  
 	  	    while (i.hasNext()) {
 	  	  	
 	  	   	    LocalInvDistributionRecord arDistributionRecord = (LocalInvDistributionRecord)i.next();
 	  	  	   
 	  	  	    i.remove();
 	  	  	    
 	  	  	    arDistributionRecord.remove();
 	  	  	
 	  	    }	  
 	  	    */
 	  	    // add new adjustment lines and distribution record
 	  	    
 	  	    double TOTAL_AMOUNT = 0d; 
 	  	    
 	  	    byte DEBIT = 0;
 	  	    
 	  	    i = arList.iterator();
 	  	    
 	  	    while (i.hasNext()) {
 	  	    	
 	  	    	InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();
 	  	    	System.out.println("hey"+mdetails.getAlCode());
 	  	    	System.out.println("hey"+mdetails.getAlIiName());
 	  	    	System.out.println("hey"+mdetails.getAlLocName());
 	  	    	LocalInvItemLocation invItemLocation = null;
 	  	    	// try to save adjustment request "served"
// 	  	    	invAdjustmentRequestLine.setAlServed(mdetails.getAlServed());
 	  	    	try{
 	  	    		
 	  	    		
 	  	    		LocalInvAdjustmentLine adjustmentRequestLine= invAdjustmentLineHome.findByAlCode(mdetails.getAlCode(),AD_CMPNY);
 	  	    		adjustmentRequestLine.setAlServed(mdetails.getAlServed());
 	  	    		} catch (FinderException ex) {
	  	    	
 	  	    			throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));
	    		
 	  	    	   }
 	  	    		
 	  	 
 	  	    	try {
 	  	    	
 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(), mdetails.getAlIiName(), AD_CMPNY);
 	  	    		
 	  	    	} catch (FinderException ex) {
 	  	    	
 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));
 	  	    		
 	  	    	}
 	  	    	/*
 	  	    	//	start date validation
 	  	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	    				invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
						invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	    			if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
 	  	    	}
 	  	    	*/
 	  	    	LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(mdetails, invAdjustment, EJBCommon.FALSE, AD_CMPNY);
 	  	    	 	    	 	  	    	
 	  	    	// add physical inventory distribution
 	  	    	
 	  	    	double AMOUNT = 0d; 	  	    	
 	  	    	
 	  	    	
 	  	    	
 	  	    	// check for branch mapping
 	  	    	
 	  	    	LocalAdBranchItemLocation adBranchItemLocation = null;
 	  	    	
 	  	    	try{
 	  	    	
 	  	    		adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
 	  	    				invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
 	  	    		
 	  	    	} catch (FinderException ex) {

 	  	    	}
 	  	    	
 	  	    	LocalGlChartOfAccount glInventoryChartOfAccount = null;
 	  	    	
 	  	    	if (adBranchItemLocation == null) {
 	  	    		
 	  	    		glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
 	  	    				invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
 	  	    	} else {
 	  	    		
 	  	    		glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
 	  	    				adBranchItemLocation.getBilCoaGlInventoryAccount());
 	  	    		
 	  	    	}
 	  	    	
 	  	    	this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(AMOUNT), EJBCommon.FALSE,
 	  	    			glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
 	  	    	
 	  	    	TOTAL_AMOUNT += AMOUNT;
 	  	    	ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);
 	  	    	
 	  	    	// add adjust quantity to item location committed quantity if negative
 	  	    	
 	  	    	if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
 	  	    	
 	  	    		double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

 	  	    		invItemLocation = invAdjustmentLine.getInvItemLocation();
 	  	    		
 	  	    		invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);
 	  	    		
 	  	    	}
 	  	    	
 	  	    }
 	  	    
 	  	    // add variance or transfer/debit distribution
 	  	    
 	  	    DEBIT = TOTAL_AMOUNT > 0 ? EJBCommon.FALSE : EJBCommon.TRUE;
 	  	    /*
 	  	    this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_AMOUNT), EJBCommon.FALSE,
 	  	    		invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
 	  	    
    	/*} else {*/
    		
    		i = arList.iterator();
 	  	    
 	  	    while (i.hasNext()) {
 	  	    	
 	  	    	InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();
 	  	    	
 	  	    	LocalInvItemLocation invItemLocation = null;
 	  	    	
 	  	    	try {
 	  	    	
 	  	    		invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(), mdetails.getAlIiName(), AD_CMPNY);
 	  	    		
 	  	    	} catch (FinderException ex) {
 	  	    	
 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));
 	  	    		
 	  	    	}
 	  	    	
 	  	    	//	start date validation
 	  	    	
 	  	    	/*if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	    				invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
						invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	    			if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
 	  	    	}*/
 	  	    
 	  	    }
 	  	    	
    		Collection invAdjDistributionRecords = invAdjustment.getInvDistributionRecords();
    		
    		i = invAdjDistributionRecords.iterator();
    		
    		while(i.hasNext()) {
    			
    			LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();
    			
    			if(distributionRecord.getDrDebit() == 1) {
    			
    				ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();
    			
    			}
    			
    		}

    	//}

    	/*
	  	    // generate approval status
	  	    
	  	    String INV_APPRVL_STATUS = null;
		
	 //	if (!isDraft) {
	 		
	 		LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
	 		
	 		// check if ar invoice approval is enabled
	 		
	 		if (adApproval.getAprEnableInvAdjustment() == EJBCommon.FALSE) {
	 			        			        			
	 			INV_APPRVL_STATUS = "N/A";
	 			
	 		} else {
	 			
	 			// check if invoice is self approved
	 			
	 			LocalAdAmountLimit adAmountLimit = null;
	 			
	 			try {
	 				
	 				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("INV ADJUSTMENT REQUEST", "REQUESTER", details.getAdjLastModifiedBy(), AD_CMPNY);       			
	 				
	 			} catch (FinderException ex) {
	 				
	 				throw new GlobalNoApprovalRequesterFoundException();
	 				
	 			}

	 			if (ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit()) {
	 				
	 				INV_APPRVL_STATUS = "N/A";
	 				
	 			} else {
	 				
	 				// for approval, create approval queue
	 				
	 				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("INV ADJUSTMENT REQUEST", adAmountLimit.getCalAmountLimit(), AD_CMPNY);
	 				 
	 				 if (adAmountLimits.isEmpty()) {
	 				 	
	 				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);
	 				 	
	 				 	if (adApprovalUsers.isEmpty()) {
	 				 		
	 				 		throw new GlobalNoApprovalApproverFoundException();
	 				 		
	 				 	}
	 				 	        				 	
	 				 	Iterator j = adApprovalUsers.iterator();
	 				 	
	 				 	while (j.hasNext()) {
	 				 		
	 				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
	 				 		
	 				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create("INV ADJUSTMENT REQUEST", 
	 				 				invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(),
									adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
	 				 		
	 				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
	 				 		
	 				 	}        				 	
	 				 	        				 	
	 				 } else {
	 				 	
	 				 	boolean isApprovalUsersFound = false;
	 				 	Iterator i = adAmountLimits.iterator();
	 				 	        				 	
	 				 	while (i.hasNext()) {
	 				 		
	 				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();        				 		
	 				 		        				 		        				 		        				 		
	 				 		if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {
	 				 			
	 				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", 
	 				 					adAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
	 				 			
	 				 			Iterator j = adApprovalUsers.iterator();
		        				 	
		        				 	while (j.hasNext()) {
		        				 		
		        				 		isApprovalUsersFound = true;
		        				 		
		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
		        				 		
		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create("INV ADJUSTMENT REQUEST", 
		        				 				invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(), 
												adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
		        				 		
		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
		        				 		
		        				 	}        				 			
	 				 			        				 			        				 			       				 			
	 				 			break;
		        				 	        				 			
	 				 		} else if (!i.hasNext()) {
	 				 			
	 				 			Collection adApprovalUsers = 
	 				 				adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
	 				 			
	 				 			Iterator j = adApprovalUsers.iterator();
		        				 	
		        				 	while (j.hasNext()) {
		        				 		
		        				 		isApprovalUsersFound = true;
		        				 		
		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
		        				 		
		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create("INV ADJUSTMENT REQUEST", 
		        				 				invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(),
												adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
		        				 		
		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
		        				 		
		        				 	}        				 			
	 				 			        				 			        				 			       				 			
	 				 			break;
	 				 			
	 				 		}        				 		
	 				 		        				 		
	 				 		adAmountLimit = adNextAmountLimit;
	 				 		
	 				 	}
	 				 	
	 				 	if (!isApprovalUsersFound) {
	 				 		
	 				 		throw new GlobalNoApprovalApproverFoundException();
	 				 		
	 				 	}        				 
	 				 	
	 			    }
	 			    
	 			    INV_APPRVL_STATUS = "PENDING";
	 			}        			        			        			
	 		}        		        		        		
	// 	} 
    	
    	if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
    		
    		this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);
    		
    	} 
    			 	
	 	// set adjustment approval status
	 	
	 	invAdjustment.setAdjApprovalStatus(INV_APPRVL_STATUS);
	 	*/
	 	return invAdjustment.getAdjCode();
	 	
    } catch (Exception ex) {
    	
    	Debug.printStackTrace(ex);
    	getSessionContext().setRollbackOnly();
    	throw new EJBException(ex.getMessage());
    	
    }
	
}


        

    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH,
			Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ApVoucherPostController generateCostVariance");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try{ 

    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;

    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    		
    		try{
    			
    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
    			
    		} catch (FinderException ex) {
    			
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    			
    		}
    		
    		InvModAdjustmentLineDetails details = new InvModAdjustmentLineDetails();
    		
    		details.setAlAdjustQuantity(0);
    		details.setAlUnitCost(CST_VRNC_VL);
    		details.setAlIiName(invItemLocation.getInvItem().getIiName());
    		details.setAlLocName(invItemLocation.getInvLocation().getLocName());
    		details.setAlUomName(invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName());

    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(details, newInvAdjustment, EJBCommon.TRUE, AD_CMPNY);
    		
    		// check for branch mapping
    		
    		LocalAdBranchItemLocation adBranchItemLocation = null;
    		
    		try{
    			
    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		LocalGlChartOfAccount glInventoryChartOfAccount = null;
    		
    		if (adBranchItemLocation == null) {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());
    			
    		}
    		
    		
    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;
    		
    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", 
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	*/
    }
    
    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArMiscReceiptEntryControllerBean regenerateCostVariance");
    	
    	try {
    		
    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			if(prevInvCosting.getCstRemainingQuantity() < 0) {
    				
    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
    					
    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
    					
    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
    					
    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
    						
    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
    						
    					}
    					
    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					
   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
   						
   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   							ADJ_RFRNC_NMBR = "ARCM" + 
							invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
    					
   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   							ADJ_RFRNC_NMBR = "ARMR" + 
							invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   						}
    					
    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" + 
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
    					
    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){
    					
    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
    					
    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
    						
    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() * 
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
    						
    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){
    					
    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
							
    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}
    					
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" + 
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();
    					
    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){
    					
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" + 
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){
    					
    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
    					
    				} else {
    					
    					prevInvCosting = invPropagatedCosting;
    					continue;
    					
    				}
    				
    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;
    				
    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());
    				
    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// set previous costing
    			prevInvCosting = invPropagatedCosting;
    			
    		}
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}    
    	*/
    }

    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ApVoucherPostController saveInvAdjustment");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home    	
    	
    	try{
    	
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try{
    		
    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;
    		
    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
    		
    		try {
    			
    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT REQUEST", AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		try {
    			
    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		while (true) {
    			
    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);		            		
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					break;
    					
    				}
    				
    			} else {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
    					break;
    					
    				}
    				
    			}
    			
    		}		            
    		
    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
    		
    		return invAdjustment;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	
    }
    
    // SessionBean methods
   
   /**
    * @ejb:create-method view-type="remote"
    **/
 
  
    
   public void ejbCreate() throws CreateException {
   	
   	Debug.print("InvAdjustmentRequestControllerBean ejbCreate");
   	
   }
   
}