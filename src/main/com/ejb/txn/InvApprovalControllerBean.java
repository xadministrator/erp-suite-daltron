
/*
 * InvApprovalControllerBean.java
 *
 * Created on Aug 04, 2004, 3:42 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApCanvass;
import com.ejb.ap.LocalApCanvassHome;
import com.ejb.ap.LocalApPurchaseRequisition;
import com.ejb.ap.LocalApPurchaseRequisitionHome;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvCSTRemainingQuantityIsLessThanZeroException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.ejb.inv.LocalInvBranchStockTransferHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBuildUnbuildAssembly;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLine;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvStockTransfer;
import com.ejb.inv.LocalInvStockTransferHome;
import com.ejb.inv.LocalInvStockTransferLine;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModApprovalQueueDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModBranchStockTransferDetails;

/**
 * @ejb:bean name="InvApprovalControllerEJB"
 *           display-name="Used for approving inv documents"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvApprovalControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvApprovalController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvApprovalControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="cmuser"
 *                        role-link="cmuserlink"
 *
 * @ejb:permission role-name="cmuser"
 * 
*/

public class InvApprovalControllerBean extends AbstractSessionBean {   



	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
		public String getApprovalStatus(String USR_DEPT, String USR_NM, String USR_DESC, String AQ_DCMNT, Integer AQ_DCMNT_CODE, String AQ_DCMNT_NMBR, Date AQ_DT, Integer AQ_AD_BRNCH, Integer AQ_AD_CMPNY) throws 
	 GlobalNoApprovalRequesterFoundException,
	 GlobalNoApprovalApproverFoundException{
	
	
		Debug.print("InvApprovalControllerBean getApprovalStatus");
		
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalInvAdjustmentHome invAdjustmentHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		
		ArrayList list = new ArrayList();
		
		
		// Initialize EJB Home
		
		try {
			
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class); 
			invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);      
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);     
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class); 
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);     
					
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		
		String PR_APPRVL_STATUS = null;
		Double ABS_TOTAL_AMOUNT = 0d;
		
		try{
			LocalAdAmountLimit adAmountLimit = null;
			
			try {

				adAmountLimit = adAmountLimitHome.findByCalDeptAdcTypeAndAuTypeAndUsrName(USR_DEPT, AQ_DCMNT, "REQUESTER", USR_NM, AQ_AD_CMPNY);

			} catch (FinderException ex) {

				throw new GlobalNoApprovalRequesterFoundException();

			}
			
			
			if (!(ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit())) {

				PR_APPRVL_STATUS = "N/A";

			} else {
				
				// for approval, create approval queue
				Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalDeptAndCalAmountLimit(USR_DEPT,AQ_DCMNT, adAmountLimit.getCalAmountLimit(), AQ_AD_CMPNY);

				
				if (adAmountLimits.isEmpty()) {

					Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(USR_DEPT,"APPROVER", adAmountLimit.getCalCode(), AQ_AD_CMPNY);

					if (adApprovalUsers.isEmpty()) {

						throw new GlobalNoApprovalApproverFoundException();

					}

					Iterator j = adApprovalUsers.iterator();

					while (j.hasNext()) {

						LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

						LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(USR_DEPT,adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()),AQ_DCMNT, AQ_DCMNT_CODE,
								AQ_DCMNT_NMBR, AQ_DT, null, adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, USR_DESC,
										AQ_AD_BRNCH, AQ_AD_CMPNY);

						adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

						if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
							adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
							
							/*
							 *
							 email for future use
							if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE)) {
								
								
								this.sendEmail(adApprovalQueue, AD_CMPNY);
							}
							*/

						}

					}

				} else {
					

					boolean isApprovalUsersFound = false;

					Iterator i = adAmountLimits.iterator();

					while (i.hasNext()) {

						LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

						if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {

							Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(USR_DEPT,"APPROVER", adAmountLimit.getCalCode(), AQ_AD_CMPNY);

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								isApprovalUsersFound = true;

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(USR_DEPT, adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()), AQ_DCMNT, AQ_DCMNT_CODE,
										AQ_DCMNT_NMBR, AQ_DT , null, adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, USR_DESC,
												AQ_AD_BRNCH, AQ_AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

								if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
									adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
									
									/*
									 *
									 email for future use
									if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE))  {
										
										this.sendEmail(adApprovalQueue, AD_CMPNY);
									}
									
									*/

								}
							}

							break;

						} else if (!i.hasNext()) {

							Collection adApprovalUsers = adApprovalUserHome.findByCalDeptAndAuTypeAndCalCode(USR_DEPT,"APPROVER", adNextAmountLimit.getCalCode(), AQ_AD_CMPNY);

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								isApprovalUsersFound = true;

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(USR_DEPT, adApprovalUser.getAuLevel(), EJBCommon.FALSE, EJBCommon.incrementStringNumber(adApprovalUser.getAuLevel()) ,AQ_DCMNT, AQ_DCMNT_CODE,
										AQ_DCMNT_NMBR, AQ_DT, null, adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), EJBCommon.FALSE, USR_DESC,
												AQ_AD_BRNCH, AQ_AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

								if(adApprovalUser.getAuLevel().equals("LEVEL 1")) {
									adApprovalQueue.setAqForApproval(EJBCommon.TRUE);
									/*
									if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE)) {

											this.sendEmail(adApprovalQueue, AD_CMPNY);
									
										
									}
									*/

								}
							}

							break;

						}

						adAmountLimit = adNextAmountLimit;

					}

					if (!isApprovalUsersFound) {

						throw new GlobalNoApprovalApproverFoundException();

					}

				
				}
	
				PR_APPRVL_STATUS = "PENDING";
			
			}
			
			
			
			
			
			return PR_APPRVL_STATUS;
		}catch(GlobalNoApprovalApproverFoundException ex){
				throw new GlobalNoApprovalApproverFoundException();
		
			
			
		}catch(GlobalNoApprovalRequesterFoundException ex){
				throw new GlobalNoApprovalRequesterFoundException();
		
		
		
		}catch(Exception ex){
				Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
		
		}
	
	
	}



	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/	
	public ArrayList getInvApproverList(String AQ_DCMNT, Integer AQ_DCMNT_CODE, Integer AQ_AD_CMPNY){
	
		Debug.print("InvApprovalControllerBean getApApproverList");
		
		
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalInvAdjustmentHome invAdjustmentHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		
		ArrayList list = new ArrayList();
		
		
		// Initialize EJB Home
		
		try {
			
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class); 
			invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);      
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);     
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class); 
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);     
					
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		try{

			Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeAll(AQ_DCMNT, AQ_DCMNT_CODE, AQ_AD_CMPNY);
	
			System.out.println("adApprovalQueues="+adApprovalQueues.size());
	
	
			Iterator i = adApprovalQueues.iterator();
	
			while (i.hasNext()) {
				LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
	
				AdModApprovalQueueDetails adModApprovalQueueDetails = new AdModApprovalQueueDetails();
				adModApprovalQueueDetails.setAqApprovedDate(adApprovalQueue.getAqApprovedDate());
				adModApprovalQueueDetails.setAqApproverName(adApprovalQueue.getAdUser().getUsrDescription());
				adModApprovalQueueDetails.setAqStatus(adApprovalQueue.getAqApproved() == EJBCommon.TRUE ? adApprovalQueue.getAqLevel()+ " APPROVED" : adApprovalQueue.getAqLevel()+" PENDING");
	
				list.add(adModApprovalQueueDetails);
			}
			
			return list;
		
		}catch(Exception ex){
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
		
		}

	}
	














	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdApprovalNotifiedUsersByAqCode(Integer AQ_CODE, Integer AD_CMPNY) {
		
		Debug.print("InvApprovalControllerBean getAdApprovalNotifiedUsersByAqCode");
		
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalInvAdjustmentHome invAdjustmentHome = null;
		LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
		LocalInvStockTransferHome invStockTransferHome = null;
		LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;

		
		LocalAdPreferenceHome adPreferenceHome = null;
		
		ArrayList list = new ArrayList();
		
		
		// Initialize EJB Home
		
		try {
			
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class); 
			invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);      
			invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class); 
			invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class); 
			invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class); 
			
			
			
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);      
					
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdApprovalQueue adApprovalQueue = null;
			try {
				
				adApprovalQueue = adApprovalQueueHome.findByPrimaryKey(AQ_CODE);
				
				System.out.println(">approval queue code is: "  + AQ_CODE);
				adApprovalQueue = adApprovalQueueHome.findByPrimaryKey(AQ_CODE);
				
				
				if(adApprovalQueue.getAqApproved()==EJBCommon.TRUE) {
					System.out.println("approved succesfull 1");
					list.add("DOCUMENT POSTED");
				}

			}catch (Exception e) {
				list.add("DOCUMENT REJECTED");
				return list;
			}
			
			if(adApprovalQueue.getAqNextLevel()==null) {
				return list;
			}
			Integer aqDocumentCode = adApprovalQueue.getAqDocumentCode();
			
			
			String approvalDocument = adApprovalQueue.getAqDocument();
			
			
			
			if(approvalDocument.equals("INV ADJUSTMENT") || approvalDocument.equals("INV ADJUSTMENT REQUEST") ){
			
				LocalInvAdjustment invAdjustment = invAdjustmentHome.findByPrimaryKey(aqDocumentCode);
				if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {
	
	                list.add("DOCUMENT POSTED");
	                return list;
	
	            }
			}
			
			
			if(approvalDocument.equals("INV BRANCH STOCK TRANSFER ORDER") || approvalDocument.equals("INV BRANCH STOCK TRANSFER-OUT")  || approvalDocument.equals("INV BRANCH STOCK TRANSFER-IN") ){
			
				LocalInvBranchStockTransfer invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(aqDocumentCode);
				if (invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE) {
	
	                list.add("DOCUMENT POSTED");
	                return list;
	
	            }
			}
			
			
			if(approvalDocument.equals("INV STOCK TRANSFER")){
			
				LocalInvStockTransfer invStockTransfer = invStockTransferHome.findByPrimaryKey(aqDocumentCode);
				if (invStockTransfer.getStPosted() == EJBCommon.TRUE) {
	
	                list.add("DOCUMENT POSTED");
	                return list;
	
	            }
			}
			
			if(approvalDocument.equals("INV BUILD ASSEMBLY") || approvalDocument.equals("INV BUILD ASSEMBLY ORDER")){
			
				LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(aqDocumentCode);
				if (invBuildUnbuildAssembly.getBuaPosted() == EJBCommon.TRUE) {
	
	                list.add("DOCUMENT POSTED");
	                return list;
	
	            }
			}
			
			
			String messageUser = "";
			LocalAdApprovalQueue adNextApprovalQueue = adApprovalQueueHome.findByAqDeptAndAqLevelAndAqDocumentCode(
					adApprovalQueue.getAqDepartment(), adApprovalQueue.getAqNextLevel(), adApprovalQueue.getAqDocumentCode(), AD_CMPNY);
		//	list.add(adNextApprovalQueue.getAqLevel() +" APPROVER - "+ adNextApprovalQueue.getAdUser().getUsrDescription());
			
			messageUser = adNextApprovalQueue.getAqLevel() +" APPROVER - "+ adNextApprovalQueue.getAdUser().getUsrDescription();
			System.out.println("adNextApprovalQueue.getAdUser().getUsrDescription()="+adNextApprovalQueue.getAdUser().getUsrDescription());
			
			
			
			try {
				
				
				adNextApprovalQueue.setAqForApproval(EJBCommon.TRUE);
				/*
				if(adPreference.getPrfAdEnableEmailNotification()==EJBCommon.TRUE) {
					if((!adApprovalQueue.getAdUser().getUsrEmailAddress().equals("")) && (adPreference.getPrfAdEnableEmailNotification() == EJBCommon.TRUE)) {
						this.sendEmail(adNextApprovalQueue, AD_CMPNY);
						
						messageUser += " [Email Notification Sent.]";
					}
				}
				*/
				
			} catch (Exception e) {
				messageUser += " [Email Notification Not Sent. Cannot connect host or no internet connection.]";
			}
			
			list.add(messageUser);

			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdAqByAqDocumentAndUserName(HashMap criteria,
        String USR_NM, Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
    	
    	Debug.print("InvApprovalControllerBean getAdAqByAqDocumentAndUserName");
    	
    	LocalAdApprovalQueueHome adApprovalQueueHome = null;
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
    	LocalInvStockTransferHome invStockTransferHome = null;
    	LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	ArrayList list = new ArrayList();
    	
    	//initialized EJB Home
    	
    	try {
    		
    		adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
    		invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);
    		invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
    		
    		adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

    	} catch (NamingException ex) 	{
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		StringBuffer jbossQl = new StringBuffer();
    		jbossQl.append("SELECT OBJECT(aq) FROM AdApprovalQueue aq ");
    		
    		boolean firstArgument = true;
    		short ctr = 0;
    		int criteriaSize = criteria.size() + 2;	      
    		
    		Object obj[];	      
    		
    		// Allocate the size of the object parameter
    		
    		obj = new Object[criteriaSize];
    		
    		if (criteria.containsKey("document")) {
    			
    			if (!firstArgument) {
    				jbossQl.append("AND ");
    			} else {
    				firstArgument = false;
    				jbossQl.append("WHERE ");
    			}
    			jbossQl.append("aq.aqDocument=?" + (ctr+1) + " ");
    			obj[ctr] = (String)criteria.get("document");
    			ctr++;
    		}
    		
    		if (criteria.containsKey("dateFrom")) {
    			
    			if (!firstArgument) {
    				jbossQl.append("AND ");
    			} else {
    				firstArgument = false;
    				jbossQl.append("WHERE ");
    			}
    			jbossQl.append("aq.aqDate>=?" + (ctr+1) + " ");
    			obj[ctr] = (Date)criteria.get("dateFrom");
    			ctr++;
    		}  
    		
    		if (criteria.containsKey("dateTo")) {
    			
    			if (!firstArgument) {
    				jbossQl.append("AND ");
    			} else {
    				firstArgument = false;
    				jbossQl.append("WHERE ");
    			}
    			jbossQl.append("aq.aqDate<=?" + (ctr+1) + " ");
    			obj[ctr] = (Date)criteria.get("dateTo");
    			ctr++;
    			
    		}    
    		
    		if (criteria.containsKey("documentNumberFrom")) {
    			
    			if (!firstArgument) {
    				jbossQl.append("AND ");
    			} else {
    				firstArgument = false;
    				jbossQl.append("WHERE ");
    			}
    			jbossQl.append("aq.aqDocumentNumber>=?" + (ctr+1) + " ");
    			obj[ctr] = (String)criteria.get("documentNumberFrom");
    			ctr++;
    		}  
    		
    		if (criteria.containsKey("documentNumberTo")) {
    			
    			if (!firstArgument) {
    				jbossQl.append("AND ");
    			} else {
    				firstArgument = false;
    				jbossQl.append("WHERE ");
    			}
    			jbossQl.append("aq.aqDocumentNumber<=?" + (ctr+1) + " ");
    			obj[ctr] = (String)criteria.get("documentNumberTo");
    			ctr++;
    			
    		}
    		
    		if (!firstArgument) {
    			
    			jbossQl.append("AND ");
    			
    		} else {
    			
    			firstArgument = false;
    			jbossQl.append("WHERE ");
    			
    		}
    		
    		//jbossQl.append("aq.aqAdBranch=" + AD_BRNCH + " AND aq.aqAdCompany=" + AD_CMPNY + " " + "AND aq.adUser.usrName='" + USR_NM + "'");
    		jbossQl.append("aq.aqForApproval = 1 AND aq.aqApproved = 0 AND aq.aqAdCompany=" + AD_CMPNY + " " + "AND aq.adUser.usrName='" + USR_NM + "'");
    		
    		String orderBy = null;
    		
    		if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
    			
    			orderBy = "aq.aqDocumentNumber";
    			
    		} else if (ORDER_BY.equals("DATE")) {
    			
    			orderBy = "aq.aqDate";
    			
    		}
    		
    		if (orderBy != null) {
    			
    			jbossQl.append("ORDER BY " + orderBy + ", aq.aqDate");
    			
    		} else {
    			
    			jbossQl.append("ORDER BY aq.aqDate");
    			
    		}
    		
    		jbossQl.append(" OFFSET ?" + (ctr + 1));	        
    		obj[ctr] = OFFSET;	      
    		ctr++;
    		
    		jbossQl.append(" LIMIT ?" + (ctr + 1));
    		obj[ctr] = LIMIT;
    		ctr++;
    		
    		Collection adApprovalQueues = adApprovalQueueHome.getAqByCriteria(jbossQl.toString(), obj);
    		
    		String AQ_DCMNT = (String)criteria.get("document"); 
    		
    		// Include Draft BST IN
    		if (AQ_DCMNT.equals("INV BRANCH STOCK TRANSFER")) {
    			
				HashMap bstCriteria = new HashMap();
				
				if (criteria.containsKey("documentNumberFrom")) {
					bstCriteria.put("documentNumberFrom", (String)criteria.get("documentNumberFrom"));	    			
	    		}
				if (criteria.containsKey("documentNumberTo")) {	    			
					bstCriteria.put("documentNumberTo", (String)criteria.get("documentNumberTo"));	    			
	    		}
				if (criteria.containsKey("dateFrom")) {	    			
					bstCriteria.put("dateFrom", (Date)criteria.get("dateFrom"));    			
	    		}
				if (criteria.containsKey("dateTo")) {	    			
					bstCriteria.put("dateTo", (Date)criteria.get("dateTo"));	    			
	    		}
				
    			ArrayList bstInList = this.getInvBstByCriteria(bstCriteria, OFFSET, LIMIT, "",AD_BRNCH, AD_CMPNY);
    			

				
				Iterator bstInIter = bstInList.iterator();
				
				while(bstInIter.hasNext()){
					
					InvModBranchStockTransferDetails bstInDetail = (InvModBranchStockTransferDetails)bstInIter.next();
					
					AdModApprovalQueueDetails details = new AdModApprovalQueueDetails();
					
					details.setAqDocument("INV BRANCH STOCK TRANSFER");
					details.setAqDocumentCode(bstInDetail.getBstCode());
					details.setAqBrCode(bstInDetail.getBstAdBranch());
							
					details.setAqDate(bstInDetail.getBstDate());
					details.setAqReferenceNumber(bstInDetail.getBstTransferOutNumber() == null ? "" :
						bstInDetail.getBstTransferOutNumber());
					details.setAqDocumentNumber(bstInDetail.getBstNumber());
					LocalAdBranch adBranchCode = adBranchHome.findByPrimaryKey(bstInDetail.getBstAdBranch());
	        		
					details.setAqAdBranchCode(adBranchCode.getBrBranchCode());
					
					
					
					// get lines
					
					double TTL_ST_AMNT = 0d;	
					
					Collection invDrSt  = invBranchStockTransferHome.findByPrimaryKey(bstInDetail.getBstCode()).getInvDistributionRecords();
					
					Iterator stIter = invDrSt.iterator();
					
					while(stIter.hasNext()) {
						
						LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)stIter.next();
						
						if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
							
							TTL_ST_AMNT += invDistributionRecord.getDrAmount();
							
						}
						
					}
					
					details.setAqAmount(TTL_ST_AMNT);					
					list.add(details);
				}			
				
			}
    		
    		if (adApprovalQueues.size() == 0 && list.size() == 0)
    			throw new GlobalNoRecordFoundException();
    		
    		Iterator i = adApprovalQueues.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
    			
    			AdModApprovalQueueDetails details = new AdModApprovalQueueDetails();
    			
    			details.setAqCode(adApprovalQueue.getAqCode());
        		details.setAqDocument(adApprovalQueue.getAqDocument());
        		details.setAqDocumentCode(adApprovalQueue.getAqDocumentCode());
        		details.setAqDepartment(adApprovalQueue.getAqDepartment());
    			
    			
    			if (AQ_DCMNT.equals("INV ADJUSTMENT")|| AQ_DCMNT.equals("INV ADJUSTMENT REQUEST")) {
    				
    				LocalInvAdjustment invAdjustment = invAdjustmentHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
    				
    				details.setAqDate(invAdjustment.getAdjDate());
    				details.setAqReferenceNumber(invAdjustment.getAdjReferenceNumber());
    				details.setAqDocumentNumber(invAdjustment.getAdjDocumentNumber());
    				LocalAdBranch adBranchCode = adBranchHome.findByPrimaryKey(invAdjustment.getAdjAdBranch());
    				details.setAqBrCode(invAdjustment.getAdjAdBranch());
					details.setAqAdBranchCode(adBranchCode.getBrBranchCode());
					
    				// get lines
    				
    				double TTL_ADJ_AMNT = 0d;
    				
    				Collection invDrAdj  = invAdjustment.getInvDistributionRecords();
    				
    				Iterator ilIter = invDrAdj.iterator();
    				
    				while(ilIter.hasNext()) {
    					
    					LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)ilIter.next();
    					
    					if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    						
    						TTL_ADJ_AMNT += invDistributionRecord.getDrAmount();		    				
    						
    					}			        	
    					
    				}
    				
    				details.setAqAmount(TTL_ADJ_AMNT);
    			}else 	if (AQ_DCMNT.equals("INV ADJUSTMENT REQUEST")) {
    				
    				LocalInvAdjustment invAdjustment = invAdjustmentHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
    				
    				details.setAqDate(invAdjustment.getAdjDate());
    				details.setAqReferenceNumber(invAdjustment.getAdjReferenceNumber());
    				details.setAqDocumentNumber(invAdjustment.getAdjDocumentNumber());
    				details.setAqBrCode(invAdjustment.getAdjAdBranch());
    				LocalAdBranch adBranchCode = adBranchHome.findByPrimaryKey(invAdjustment.getAdjAdBranch());
	        		
					details.setAqAdBranchCode(adBranchCode.getBrBranchCode());
					
    				// get lines
    				
    				double TTL_ADJ_AMNT = 0d;
    				
    				Collection invDrAdj  = invAdjustment.getInvDistributionRecords();
    				
    				Iterator ilIter = invDrAdj.iterator();
    				
    				while(ilIter.hasNext()) {
    					
    					LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)ilIter.next();
    					
    					if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    						
    						TTL_ADJ_AMNT += invDistributionRecord.getDrAmount();		    				
    						
    					}			        	
    					
    				}
    				
    				details.setAqAmount(TTL_ADJ_AMNT);	
    				
    			} else if (AQ_DCMNT.equals("INV BUILD ASSEMBLY")) {
    				
    				LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
    				
    				details.setAqDate(invBuildUnbuildAssembly.getBuaDate());
    				details.setAqReferenceNumber(invBuildUnbuildAssembly.getBuaReferenceNumber());
    				details.setAqDocumentNumber(invBuildUnbuildAssembly.getBuaDocumentNumber());
    				details.setAqBrCode(invBuildUnbuildAssembly.getBuaAdBranch());
    				LocalAdBranch adBranchCode = adBranchHome.findByPrimaryKey(invBuildUnbuildAssembly.getBuaAdBranch());
	        		
					details.setAqAdBranchCode(adBranchCode.getBrBranchCode());
					
    				// get lines
    				
    				double TTL_BUA_AMNT = 0d;
    				
    				Collection invDrBua  = invBuildUnbuildAssembly.getInvDistributionRecords();
    				
    				Iterator buaIter = invDrBua.iterator();
    				
    				while(buaIter.hasNext()) {
    					
    					LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)buaIter.next();
    					
    					if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    						
    						TTL_BUA_AMNT += invDistributionRecord.getDrAmount();
    						
    					}
    					
    				}
    				
    				details.setAqAmount(TTL_BUA_AMNT);
    				
    			} else if (AQ_DCMNT.equals("INV STOCK TRANSFER") ) {
    				
    				LocalInvStockTransfer invStockTransfer = invStockTransferHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
    				
    				details.setAqDate(invStockTransfer.getStDate());
    				details.setAqReferenceNumber(invStockTransfer.getStReferenceNumber());
    				details.setAqDocumentNumber(invStockTransfer.getStDocumentNumber());
    				details.setAqBrCode(invStockTransfer.getStAdBranch());
    				LocalAdBranch adBranchCode = adBranchHome.findByPrimaryKey(invStockTransfer.getStAdBranch());
	        		
					details.setAqAdBranchCode(adBranchCode.getBrBranchCode());
					
    				// get lines
    				
    				double TTL_ST_AMNT = 0d;
    				
    				Collection invDrSt  = invStockTransfer.getInvDistributionRecords();
    				
    				Iterator stIter = invDrSt.iterator();
    				
    				while(stIter.hasNext()) {
    					
    					LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)stIter.next();
    					
    					if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    						
    						TTL_ST_AMNT += invDistributionRecord.getDrAmount();
    						
    					}
    					
    				}
    				
    				details.setAqAmount(TTL_ST_AMNT);
    				
    			} else if (AQ_DCMNT.equals("INV BRANCH STOCK TRANSFER")) {
    				
    				LocalInvBranchStockTransfer invBranchStockTransfer =
    					invBranchStockTransferHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
    				
    				details.setAqDate(invBranchStockTransfer.getBstDate());
    				details.setAqReferenceNumber(invBranchStockTransfer.getBstTransferOutNumber() == null ? "" :
    					invBranchStockTransfer.getBstTransferOutNumber());
    				details.setAqDocumentNumber(invBranchStockTransfer.getBstNumber());
    				LocalAdBranch adBranchCode = adBranchHome.findByPrimaryKey(invBranchStockTransfer.getBstAdBranch());
	        		
					details.setAqAdBranchCode(adBranchCode.getBrBranchCode());
					details.setAqBrCode(invBranchStockTransfer.getBstAdBranch());
    				// get lines
    				
    				double TTL_ST_AMNT = 0d;
    				
    				Collection invDrSt  = invBranchStockTransfer.getInvDistributionRecords();
    				
    				Iterator stIter = invDrSt.iterator();
    				
    				while(stIter.hasNext()) {
    					
    					LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)stIter.next();
    					
    					if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    						
    						TTL_ST_AMNT += invDistributionRecord.getDrAmount();
    						
    					}
    					
    				}
    				
    				details.setAqAmount(TTL_ST_AMNT);
    				
    			} else if (AQ_DCMNT.equals("INV BRANCH STOCK TRANSFER ORDER")) {
    				
    				LocalInvBranchStockTransfer invBranchStockTransfer =
    					invBranchStockTransferHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
    				
    				details.setAqDate(invBranchStockTransfer.getBstDate());
    				details.setAqReferenceNumber(invBranchStockTransfer.getBstTransferOutNumber() == null ? "" :
    					invBranchStockTransfer.getBstTransferOutNumber());
    				details.setAqDocumentNumber(invBranchStockTransfer.getBstNumber());
    				LocalAdBranch adBranchCode = adBranchHome.findByPrimaryKey(invBranchStockTransfer.getBstAdBranch());
	        		
					details.setAqAdBranchCode(adBranchCode.getBrBranchCode());
					details.setAqBrCode(invBranchStockTransfer.getBstAdBranch());
					
    				// get lines
    				
    				double TTL_ST_AMNT = 0d;
    				/*
    				Collection invDrSt  = invBranchStockTransfer.getInvDistributionRecords();
    				
    				Iterator stIter = invDrSt.iterator();
    				
    				while(stIter.hasNext()) {
    					
    					LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)stIter.next();
    					
    					if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    						
    						TTL_ST_AMNT += invDistributionRecord.getDrAmount();
    						
    					}
    					
    				}
    				*/
    				Collection invBranchStockTransferLines = invBranchStockTransfer.getInvBranchStockTransferLines();
    				Iterator bsos = invBranchStockTransferLines.iterator();
    				while (bsos.hasNext()) {
        				LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) bsos.next();
        				TTL_ST_AMNT += invBranchStockTransferLine.getBslAmount();
        			}
    				details.setAqAmount(0.00);
    				
    			}
    			
    			list.add(details);
    			
    		}
    		
    		return list;
    		
    	} catch (GlobalNoRecordFoundException ex) {
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		
    		ex.printStackTrace();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void executeInvApproval(String AQ_DCMNT, Integer AQ_DCMNT_CODE, String USR_NM, boolean isApproved, 
    	String RSN_FR_RJCTN, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,
        GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException {
                    
        Debug.print("InvApprovalControllerBean executeInvApproval");
        
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvStockTransferHome invStockTransferHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
                
        LocalAdApprovalQueue adApprovalQueue = null;
        
        //initialized EJB Home
        
        try {
            
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
        
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	
        	// validate if approval queue is already deleted
        	
        	
        	// validate if approval queue is already deleted
        	try {       			
        		adApprovalQueue = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeAndUsrName(AQ_DCMNT, AQ_DCMNT_CODE, USR_NM, AD_CMPNY);	        		
        	} catch (FinderException ex) {
        		throw new GlobalRecordAlreadyDeletedException();
        	}   
        	
        	// approve/reject
        	
        	Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        	Collection adApprovalQueuesDesc = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeLessThanDesc(AQ_DCMNT, AQ_DCMNT_CODE, adApprovalQueue.getAqCode(), AD_CMPNY);
        	Collection adApprovalQueuesAsc = adApprovalQueueHome.findByAqDocumentAndAqDocumentCodeGreaterThanAsc(AQ_DCMNT, AQ_DCMNT_CODE, adApprovalQueue.getAqCode(), AD_CMPNY);
        	
        	Collection adAllApprovalQueues = adApprovalQueueHome.findAllByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	
        
       

        	
        	if (AQ_DCMNT.equals("INV ADJUSTMENT")) {        		
        		
        		LocalInvAdjustment invAdjustment = invAdjustmentHome.findByPrimaryKey(AQ_DCMNT_CODE);

        		if (isApproved) {
        			        			
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				
        				if (adApprovalQueues.size() == 1) {
        					        					
        					invAdjustment.setAdjApprovalStatus("APPROVED");
        					invAdjustment.setAdjApprovedRejectedBy(USR_NM);
        					invAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					
        					/*invAdjustment.setAdjPosted(EJBCommon.TRUE);
        					invAdjustment.setAdjPostedBy(invAdjustment.getAdjLastModifiedBy());
        					invAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        					*/
        					adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());
        					
        					if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

			    		   		this.executeInvAdjPost(invAdjustment.getAdjCode(), USR_NM, AD_BRNCH, AD_CMPNY);
			    		
			    	    	}

        				} else {
        					
        					// looping up
        					Iterator i = adApprovalQueuesDesc.iterator();
            				
            				while (i.hasNext()) {
            					
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            				
            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            						adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        						adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        							i.remove();
        						} else {
        							break;
        						}
            						
            				}
            				
            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {
            					boolean first = true;
            					i = adApprovalQueuesAsc.iterator();
                				while (i.hasNext()) {
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							i.remove();
            							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        								adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                    					if(first)
                    						first = false;
            						} else {
            							break;
            						}
                						
                				}
                				
            				}
            				
            				//adApprovalQueue.remove();
            				adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());
        					
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
            	        	
        					if(adRemoveApprovalQueues.size() == 0) {
            	        		
        						invAdjustment.setAdjApprovalStatus("APPROVED");
            					invAdjustment.setAdjApprovedRejectedBy(USR_NM);
            					invAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
            					/*invAdjustment.setAdjPosted(EJBCommon.TRUE);
            					invAdjustment.setAdjPostedBy(invAdjustment.getAdjLastModifiedBy());
            					invAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
            					*/
            					if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

    			    		   		this.executeInvAdjPost(invAdjustment.getAdjCode(), USR_NM, AD_BRNCH, AD_CMPNY);
    			    		
    			    	    	}
            	        	}
        				}        				        				
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
        				
        				invAdjustment.setAdjApprovalStatus("APPROVED");
        				invAdjustment.setAdjApprovedRejectedBy(USR_NM);
        				invAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				
        				Iterator i = adApprovalQueues.iterator();
        				
        				while (i.hasNext()) {
        					
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					
        					i.remove();
        					//adRemoveApprovalQueue.remove();
        					adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());

        				}
        				/*invAdjustment.setAdjPosted(EJBCommon.TRUE);
        				invAdjustment.setAdjPostedBy(invAdjustment.getAdjLastModifiedBy());
        				invAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        				*/
        				if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

		    		   		this.executeInvAdjPost(invAdjustment.getAdjCode(), USR_NM, AD_BRNCH, AD_CMPNY);
		    		
		    	    	}

        			}
        			
        		} else if (!isApproved) {
        			
        			invAdjustment.setAdjApprovalStatus(null);
        			invAdjustment.setAdjApprovedRejectedBy(USR_NM);
        			invAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        			invAdjustment.setAdjReasonForRejection(RSN_FR_RJCTN);
    				
    				/*Iterator i = adApprovalQueues.iterator();
    				
    				while (i.hasNext()) {
    					
    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
    					
    					i.remove();
    					adRemoveApprovalQueue.remove();
    					
    				}*/
        			
        			Iterator x = adAllApprovalQueues.iterator();
        			
        			while (x.hasNext()) {
        				LocalAdApprovalQueue adRemoveAllApprovalQueue = (LocalAdApprovalQueue)x.next();
        				x.remove();
        				adRemoveAllApprovalQueue.remove();
        				
        			}
        			        			
        		}
        		
        	}else if (AQ_DCMNT.equals("INV ADJUSTMENT REQUEST")) {        		
            		
            		LocalInvAdjustment invAdjustment = invAdjustmentHome.findByPrimaryKey(AQ_DCMNT_CODE);
            		     
         	  	    Collection invAdjustmentLineItems = invAdjustment.getInvAdjustmentLines();
            		
            		Iterator i = invAdjustmentLineItems.iterator();
            		
            		while (i.hasNext()) {

            			LocalInvAdjustmentLine invAdjustmentLineItem = (LocalInvAdjustmentLine) i.next(); 
            			
            			// start date validation
            			
        				if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
    		  	    		Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
    		  	    			invAdjustment.getAdjDate(), invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName(),
    		  	    			invAdjustmentLineItem.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    		  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName());
        				}
            			
            		}  
            		
            		if (isApproved) {
            			        			
            			if (adApprovalQueue.getAqAndOr().equals("AND")) {
            				
            				if (adApprovalQueues.size() == 1) {
            					        					
            					invAdjustment.setAdjApprovalStatus("APPROVED");
            					invAdjustment.setAdjApprovedRejectedBy(USR_NM);
            					invAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
            					adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					    adApprovalQueue.setAqApprovedDate(new java.util.Date());
            					if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
            		        		
            		        		this.executeInvAdjPost(invAdjustment.getAdjCode(), USR_NM, AD_BRNCH, AD_CMPNY);
            		        		
            		        	}
            					
            					//adApprovalQueue.remove();
            					
            				} else {
            					
            					// looping up
            					i = adApprovalQueuesDesc.iterator();
                				
                				while (i.hasNext()) {
                					
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                				
                					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							
            							i.remove();
            							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					    		adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                    				//	adRemoveApprovalQueue.remove();
                    					
            						} else {
            							
            							break;
            							
            						}
                						
                				}
                				
                				// looping down
                				if(adApprovalQueue.getAqUserOr() == (byte)1) {
                					
                					boolean first = true;
                					
                					i = adApprovalQueuesAsc.iterator();
                    				
                    				while (i.hasNext()) {
                    					
                    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                    					
                    					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
                							
                							i.remove();
                							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					    			adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                        					//adRemoveApprovalQueue.remove();
                        					
                        					if(first)
                        						first = false;
                        					
                						} else {
                							
                							break;
                							
                						}
                    						
                    				}
                    				
                				}
                				
                				adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					    adApprovalQueue.setAqApprovedDate(new java.util.Date());
                				adApprovalQueue.remove();
            					
            					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
                	        	
            					if(adRemoveApprovalQueues.size() == 0) {
                	        		
            						invAdjustment.setAdjApprovalStatus("APPROVED");
                					invAdjustment.setAdjApprovedRejectedBy(USR_NM);
                					invAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
                					
                					if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
                		        		
                		        		this.executeInvAdjPost(invAdjustment.getAdjCode(), USR_NM, AD_BRNCH, AD_CMPNY);
                		        		
                		        	}
                					
                	        	}
                				
            				
            				}
            				        				        				
            			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
            				
            				invAdjustment.setAdjApprovalStatus("APPROVED");
            				invAdjustment.setAdjApprovedRejectedBy(USR_NM);
            				invAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
            				
            				i = adApprovalQueues.iterator();
            				
            				while (i.hasNext()) {
            					
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            					adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					    adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
            					i.remove();
            					//adRemoveApprovalQueue.remove();
            					
            				}
            				
            				if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		
            				 //  invAdjustment.setAdjPosted(EJBCommon.TRUE);
    		    		   		//this.executeInvAdjPost(invAdjustment.getAdjCode(), USR_NM, AD_BRNCH, AD_CMPNY);
    		    		
    		    	    	}
            				
            				
            			}
            			
            		} else if (!isApproved) {
            			
            			invAdjustment.setAdjApprovalStatus(null);
            			invAdjustment.setAdjApprovedRejectedBy(USR_NM);
            			invAdjustment.setAdjDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
            			invAdjustment.setAdjReasonForRejection(RSN_FR_RJCTN);
        				
        				i = adAllApprovalQueues.iterator();
        				
        				while (i.hasNext()) {
        					
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					
        					i.remove();
        					adRemoveApprovalQueue.remove();
        					
        				}
            			        			
            		}	
        	} else if (AQ_DCMNT.equals("INV BUILD ASSEMBLY")) {
        		
        		LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(AQ_DCMNT_CODE);
        		        		       
        		Collection invBuildUnbuildAssemblyLineItems = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
        		
        		Iterator i = invBuildUnbuildAssemblyLineItems.iterator();
        		
        		while (i.hasNext()) {

        			LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLineItem = (LocalInvBuildUnbuildAssemblyLine) i.next(); 
        			
        			// start date validation
        			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
        						invBuildUnbuildAssembly.getBuaDate(), invBuildUnbuildAssemblyLineItem.getInvItemLocation().getInvItem().getIiName(),
        						invBuildUnbuildAssemblyLineItem.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invBuildUnbuildAssemblyLineItem.getInvItemLocation().getInvItem().getIiName());
        			}
	  	    		Collection invBillOfMaterials = invBuildUnbuildAssemblyLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();
	  	    		
	  	    		Iterator j = invBillOfMaterials.iterator();
	  	    		
	  	    		while(j.hasNext()) {
	  	    			
	  	    			LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)j.next();
	  	    			
	  	    			LocalInvItemLocation invIlRawMaterial = null;
	  	    			
	  	    			try {
	  	    				
	  	    				invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
	  	    						invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(),AD_CMPNY);
	  	    				
	  	    			} catch (FinderException ex) {
	  	    				
	  	    				throw new GlobalInvItemLocationNotFoundException(
	  	    						invBuildUnbuildAssemblyLineItem.getInvItemLocation().getInvItem().getIiName() +
	  	    						" - Raw Mat. (" + invBillOfMaterial.getBomIiName() + ")");
	  	    				
	  	    			}
	  	    			
	  	    			// start date validation
	  	    			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	  	    				Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	  	    						invBuildUnbuildAssembly.getBuaDate(), invIlRawMaterial.getInvItem().getIiName(),
	  	    						invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	  	    				if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
	  	    						invBuildUnbuildAssemblyLineItem.getInvItemLocation().getInvItem().getIiName() + 
	  	    						" - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
	  	    			}
	  	    		}
	  	    		
        		} 
        		
        		if (isApproved) {
        			        			
        			if (adApprovalQueue.getAqAndOr().equals("AND")) {
        				
        				if (adApprovalQueues.size() == 1) {
        					
        					invBuildUnbuildAssembly.setBuaApprovalStatus("APPROVED");
        					invBuildUnbuildAssembly.setBuaApprovedRejectedBy(USR_NM);
        					invBuildUnbuildAssembly.setBuaDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        					
        					if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
    		
			    		   		this.executeInvBuaPost(invBuildUnbuildAssembly.getBuaCode(), USR_NM, AD_BRNCH, AD_CMPNY);
			    		
			    	    	}
        					adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());
        					//adApprovalQueue.remove();
        					
        				} else {
        					
        					// looping up
        					i = adApprovalQueuesDesc.iterator();
            				
            				while (i.hasNext()) {
            					
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            				
            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							
        							i.remove();
        							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        							adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                					//adRemoveApprovalQueue.remove();
                					
        						} else {
        							
        							break;
        							
        						}
            						
            				}
            				
            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {
            					
            					boolean first = true;
            					
            					i = adApprovalQueuesAsc.iterator();
                				
                				while (i.hasNext()) {
                					
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                					
                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							
            							i.remove();
            							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        								adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                    					//adRemoveApprovalQueue.remove();
                    					
                    					if(first)
                    						first = false;
                    					
            						} else {
            							
            							break;
            							
            						}
                						
                				}
                				
            				}
            				
            				adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());
            				//adApprovalQueue.remove();
        					
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
            	        	
        					if(adRemoveApprovalQueues.size() == 0) {
            	        		
        						invBuildUnbuildAssembly.setBuaApprovalStatus("APPROVED");
            					invBuildUnbuildAssembly.setBuaApprovedRejectedBy(USR_NM);
            					invBuildUnbuildAssembly.setBuaDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
            					
            					if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
        		
    			    		   		this.executeInvBuaPost(invBuildUnbuildAssembly.getBuaCode(), USR_NM, AD_BRNCH, AD_CMPNY);
    			    		
    			    	    	}
            					
            	        	}
            				
        				}
        				        				        				
        			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
		
        				invBuildUnbuildAssembly.setBuaApprovalStatus("APPROVED");
        				invBuildUnbuildAssembly.setBuaApprovedRejectedBy(USR_NM);
        				invBuildUnbuildAssembly.setBuaDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        				
        				i = adApprovalQueues.iterator();
        				
        				while (i.hasNext()) {
        					
        					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        					
        					i.remove();
        					adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
        				//	adRemoveApprovalQueue.remove();
        					
        				}
        				
        				if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
    		
		    		   		this.executeInvBuaPost(invBuildUnbuildAssembly.getBuaCode(), USR_NM, AD_BRNCH, AD_CMPNY);
		    		
		    	    	}

        			}
    			
        		} else if (!isApproved) {
        					
        			invBuildUnbuildAssembly.setBuaApprovalStatus(null);
        			invBuildUnbuildAssembly.setBuaApprovedRejectedBy(USR_NM);
        			invBuildUnbuildAssembly.setBuaDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        			invBuildUnbuildAssembly.setBuaReasonForRejection(RSN_FR_RJCTN);
        			
    				i = adAllApprovalQueues.iterator();
    				
    				while (i.hasNext()) {
    					
    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
    					
    					i.remove();
    					adRemoveApprovalQueue.remove();
    					
    				}
        			        			
        		}
        		
        	} else if (AQ_DCMNT.equals("INV STOCK TRANSFER")) {
        	            		
	    		LocalInvStockTransfer invStockTransfer = invStockTransferHome.findByPrimaryKey(AQ_DCMNT_CODE);
	    		        		  
	    		Collection invStockTransferLines = invStockTransfer.getInvStockTransferLines();
	    		
	    		Iterator i = invStockTransferLines.iterator();
	    		
	    		while(i.hasNext()) {
	    			
	    			LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine)i.next();
	    			LocalInvLocation invLocFrom = null;	                
	                LocalInvLocation invLocTo = null;
	                
	                try {
		 	  	    	
	                	invLocFrom = invLocationHome.findByPrimaryKey(invStockTransferLine.getStlLocationFrom());
	 	  	    		invLocTo = invLocationHome.findByPrimaryKey(invStockTransferLine.getStlLocationTo());
	 	  	    		
	 	  	    	} catch (FinderException ex) {
	 	  	    	
	 	  	    	}
	    			
	    			// start date validation
	 	  	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	 	  	    		Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	 	  	    				invStockTransfer.getStDate(), invStockTransferLine.getInvItem().getIiName(),
	 	  	    				invLocFrom.getLocName(), AD_BRNCH, AD_CMPNY);
	 	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invStockTransferLine.getInvItem().getIiName());
	 	  	    		
	 	  	    		invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	 	  	    				invStockTransfer.getStDate(), invStockTransferLine.getInvItem().getIiName(),
	 	  	    				invLocTo.getLocName(), AD_BRNCH, AD_CMPNY);
	 	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invStockTransferLine.getInvItem().getIiName());
	 	  	    	}
	    		}
	    		
	    		if (isApproved) {
	    			        			
	    			if (adApprovalQueue.getAqAndOr().equals("AND")) {
	    				
	    				if (adApprovalQueues.size() == 1) {
	    					
	    				    invStockTransfer.setStApprovalStatus("APPROVED");
	    				    invStockTransfer.setStApprovedRejectedBy(USR_NM);
	    				    invStockTransfer.setStDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());	    						    					
			
		    		   		this.executeInvStPost(invStockTransfer.getStCode(), USR_NM, AD_BRNCH, AD_CMPNY);
			    					
			    			adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());		
		    		  // 		adApprovalQueue.remove();
		    		   		
	    				} else {
	    					        					
        					// looping up
        					i = adApprovalQueuesDesc.iterator();
            				
            				while (i.hasNext()) {
            					
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            				
            					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
        							
        							i.remove();
        							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        							adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                					//adRemoveApprovalQueue.remove();
                					
        						} else {
        							
        							break;
        							
        						}
            						
            				}
            				
            				// looping down
            				if(adApprovalQueue.getAqUserOr() == (byte)1) {
            					
            					boolean first = true;
            					
            					i = adApprovalQueuesAsc.iterator();
                				
                				while (i.hasNext()) {
                					
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                					
                					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							
            							i.remove();
            							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        								adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                    					//adRemoveApprovalQueue.remove();
                    					
                    					if(first)
                    						first = false;
                    					
            						} else {
            							
            							break;
            							
            						}
                						
                				}
                				
            				}
            				
            				adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adApprovalQueue.setAqApprovedDate(new java.util.Date());
            				//adApprovalQueue.remove();
        					
        					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
            	        	
        					if(adRemoveApprovalQueues.size() == 0) {
            	        		
        						invStockTransfer.setStApprovalStatus("APPROVED");
    	    				    invStockTransfer.setStApprovedRejectedBy(USR_NM);
    	    				    invStockTransfer.setStDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());	    						    					
    			
    		    		   		this.executeInvStPost(invStockTransfer.getStCode(), USR_NM, AD_BRNCH, AD_CMPNY);
    			 				
            	        	}
            			
	    				}
	    				        				        				
	    			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
		
	    			    invStockTransfer.setStApprovalStatus("APPROVED");
	    			    invStockTransfer.setStApprovedRejectedBy(USR_NM);
	    			    invStockTransfer.setStDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
	    				
	    				i = adApprovalQueues.iterator();
	    				
	    				while (i.hasNext()) {
	    					
	    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
	    					
	    					i.remove();
	    					adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        					adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
	    				//	adRemoveApprovalQueue.remove();
	    					
	    				}	    					    			
			
	    		   		this.executeInvStPost(invStockTransfer.getStCode(), USR_NM, AD_BRNCH, AD_CMPNY);
		    		
	
	    			}
				
	    		} else if (!isApproved) {
	    					
	    		    invStockTransfer.setStApprovalStatus(null);
	    		    invStockTransfer.setStApprovedRejectedBy(USR_NM);
	    		    invStockTransfer.setStDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
	    		    invStockTransfer.setStReasonForRejection(RSN_FR_RJCTN);
	    			
					i = adAllApprovalQueues.iterator();
					
					while (i.hasNext()) {
						
						LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
						
						i.remove();
						adRemoveApprovalQueue.remove();
						
					}
	    			        			
	    		}
        		        	

        	} else if (AQ_DCMNT.equals("INV BRANCH STOCK TRANSFER")|| AQ_DCMNT.equals("INV BRANCH STOCK TRANSFER ORDER")) {

        		
        		LocalInvBranchStockTransfer invBranchStockTransfer =
        			invBranchStockTransferHome.findByPrimaryKey(AQ_DCMNT_CODE);
        		
        		/*Collection invStockTransferLines = invBranchStockTransfer.getInvBranchStockTransferLines();
        		
        		Iterator i = invStockTransferLines.iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)i.next();
        			
        			//Start date validation
        			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
        				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
        						invBranchStockTransfer.getBstDate(),
        						invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
        						invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(),
        						AD_BRNCH, AD_CMPNY);
        				
        				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
        						invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
        			}
        			
        		}*/
        		Iterator i = null;
        		
        		if (isApproved) {
        			System.out.println("STATUS: " + invBranchStockTransfer.getBstApprovalStatus());
        			if(invBranchStockTransfer.getBstApprovalStatus()==null){
        				
        				System.out.println("invBranchStockTransfer.getBstCode(): " + invBranchStockTransfer.getBstCode());
        				this.executeInvBstPost(invBranchStockTransfer.getBstCode(), USR_NM, AD_BRNCH, AD_CMPNY);        				
        				
        			}else{

            			
            			if (adApprovalQueue.getAqAndOr().equals("AND")) {
            				
            				if (adApprovalQueues.size() == 1) {
            					
            					invBranchStockTransfer.setBstApprovalStatus("APPROVED");
            					invBranchStockTransfer.setBstApprovedRejectedBy(USR_NM);
            					invBranchStockTransfer.setBstDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());	    						    					
            					
            					this.executeInvBstPost(invBranchStockTransfer.getBstCode(), USR_NM, AD_BRNCH, AD_CMPNY);
            					
            					adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        						adApprovalQueue.setAqApprovedDate(new java.util.Date());
            				//	adApprovalQueue.remove();
            					
            				} else {
            					        					
            					// looping up
            					i = adApprovalQueuesDesc.iterator();
                				
                				while (i.hasNext()) {
                					
                					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                				
                					if (adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
            							
            							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        								adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
            							i.remove();
            							
                    				//	adRemoveApprovalQueue.remove();
                    					
            						} else {
            							
            							break;
            							
            						}
                						
                				}
                				
                				// looping down
                				if(adApprovalQueue.getAqUserOr() == (byte)1) {
                					
                					boolean first = true;
                					
                					i = adApprovalQueuesAsc.iterator();
                    				
                    				while (i.hasNext()) {
                    					
                    					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
                    					
                    					if (first || adRemoveApprovalQueue.getAqUserOr() == (byte)1) {
                							
                							
                							i.remove();
                							adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        									adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
                        				//	adRemoveApprovalQueue.remove();
                        					
                        					if(first)
                        						first = false;
                        					
                						} else {
                							
                							break;
                							
                						}
                    						
                    				}
                    				
                				}
                				
                				adApprovalQueue.setAqApproved(EJBCommon.TRUE);
        						adApprovalQueue.setAqApprovedDate(new java.util.Date());
                			//	adApprovalQueue.remove();
            					
            					Collection adRemoveApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode(AQ_DCMNT, AQ_DCMNT_CODE, AD_CMPNY);
                	        	
            					if(adRemoveApprovalQueues.size() == 0) {
                	        		
            						invBranchStockTransfer.setBstApprovalStatus("APPROVED");
                					invBranchStockTransfer.setBstApprovedRejectedBy(USR_NM);
                					invBranchStockTransfer.setBstDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());	    						    					
                					
                					this.executeInvBstPost(invBranchStockTransfer.getBstCode(), USR_NM, AD_BRNCH, AD_CMPNY);
                									
                	        	}
                				
            				}
            				
            			} else if (adApprovalQueue.getAqAndOr().equals("OR")) {
            				
            				invBranchStockTransfer.setBstApprovalStatus("APPROVED");
            				invBranchStockTransfer.setBstApprovedRejectedBy(USR_NM);
            				invBranchStockTransfer.setBstDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
            				
            				i = adApprovalQueues.iterator();
            				
            				while (i.hasNext()) {
            					
            					LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
            					
            					i.remove();
            					adRemoveApprovalQueue.setAqApproved(EJBCommon.TRUE);
        						adRemoveApprovalQueue.setAqApprovedDate(new java.util.Date());
            				//	adRemoveApprovalQueue.remove();
            					
            				}	    					    			
            				
            				this.executeInvBstPost(invBranchStockTransfer.getBstCode(), USR_NM, AD_BRNCH, AD_CMPNY);
            				
            				
            			}
            			
            		
        			}
        			
        		} else if (!isApproved) {
        			
        			invBranchStockTransfer.setBstApprovalStatus(null);
        			invBranchStockTransfer.setBstApprovedRejectedBy(USR_NM);
        			invBranchStockTransfer.setBstDateApprovedRejected(EJBCommon.getGcCurrentDateWoTime().getTime());
        			invBranchStockTransfer.setBstReasonForRejection(RSN_FR_RJCTN);
        			
        			i = adAllApprovalQueues.iterator();
        			
        			while (i.hasNext()) {
        				
        				LocalAdApprovalQueue adRemoveApprovalQueue = (LocalAdApprovalQueue)i.next();
        				
        				i.remove();
        				adRemoveApprovalQueue.remove();
        				
        			}
        			
        		}
        		
        	}
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	throw ex;
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalInventoryDateException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;         	

        } catch (GlobalBranchAccountNumberInvalidException ex ) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
        	
            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }


   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("InvApprovalControllerBean getGlFcPrecisionUnit");

       	
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    // private methods
    // Draft BST IN Only
    private ArrayList getInvBstByCriteria(HashMap criteria,
			Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvApprovalControllerBean getInvBstByCriteria");
		
		LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(bst) FROM InvBranchStockTransfer bst WHERE ");
			
			
			short ctr = 0;
			int criteriaSize = criteria.size();
			
			Object obj[];
			
			// Allocate the size of the object parameter
			
			String type = new String();
						
			obj = new Object[criteriaSize];
			
			jbossQl.append(" bst.bstType='IN' ");
			jbossQl.append(" AND bst.bstApprovalStatus IS NULL ");
			jbossQl.append(" AND bst.bstPosted=0 ");
			jbossQl.append(" AND bst.bstVoid=0 ");
			jbossQl.append(" AND bst.bstAdBranch=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
			
			if (criteria.containsKey("documentNumberFrom")) {
								
				jbossQl.append(" AND bst.bstNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberFrom");
				ctr++;
				
			}
			
			if (criteria.containsKey("documentNumberTo")) {
								
				jbossQl.append(" AND bst.bstNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberTo");
				ctr++;
				
			}		  		  
			
			if (criteria.containsKey("dateFrom")) {
								
				jbossQl.append(" AND bst.bstDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
								
				jbossQl.append(" AND bst.bstDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}    
			
			String orderBy = null;
			
			if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
				
				orderBy = "bst.bstNumber";	  
				
			}
			
			if (orderBy != null) {
				
				jbossQl.append("ORDER BY " + orderBy + ", bst.bstDate");
				
			} else {
				
				jbossQl.append("ORDER BY bst.bstDate");
				
			}
			
			jbossQl.append(" OFFSET " + OFFSET);
			jbossQl.append(" LIMIT " + LIMIT);
			
			for(int i=0;i<criteriaSize;i++)
			{
				System.out.println("#" + i + " " + obj[i]); 
			}
			
			System.out.println("jbossQl.toString(): " + jbossQl.toString());
			
			Collection invBranchStockTransfers = invBranchStockTransferHome.getBstByCriteria(jbossQl.toString(), obj);	         
			if (invBranchStockTransfers.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			Iterator i = invBranchStockTransfers.iterator();
			
			while (i.hasNext()) {
				
				LocalInvBranchStockTransfer invBranchStockTransfer = (LocalInvBranchStockTransfer)i.next(); 
				
				InvModBranchStockTransferDetails mdetails = new InvModBranchStockTransferDetails();
				mdetails.setBstCode(invBranchStockTransfer.getBstCode());
				mdetails.setBstType(invBranchStockTransfer.getBstType());
				mdetails.setBstDate(invBranchStockTransfer.getBstDate());
				mdetails.setBstNumber(invBranchStockTransfer.getBstNumber());
				mdetails.setBstTransferOutNumber(invBranchStockTransfer.getBstTransferOutNumber());
				
				mdetails.setBstBranchFrom(invBranchStockTransfer.getAdBranch().getBrName());
            	mdetails.setBstBranchTo(this.getAdBrNameByBrCode(invBranchStockTransfer.getBstAdBranch()));
				mdetails.setBstAdBranch(invBranchStockTransfer.getBstAdBranch());
				list.add(mdetails);
				
			}
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
    
    private String getAdBrNameByBrCode(Integer BR_CODE) {
    	
    	Debug.print("InvFindBranchStockTransferControllerBean getAdBrNameByBrCode");
    	
    	LocalAdBranchHome adBranchHome = null;
    	
    	try {
    		
    		adBranchHome = (LocalAdBranchHome)EJBHomeFactory.lookUpLocalHome(
    				LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
    		
    	} catch(NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		return adBranchHome.findByPrimaryKey(BR_CODE).getBrName();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    }
    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {
    	
    	Debug.print("InvBranchStockTransferInEntryControllerBean postInvAdjustmentToInventory");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
    		
    	} catch (NamingException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;
    		
    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		
    		if (CST_ADJST_QTY < 0) {
    			
    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
    			
    		}
    		
    		// create costing
    		
    		try {
    			
    			// generate line number
    			
    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
    					AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
    			
    		} catch (FinderException ex) {
    			
    			CST_LN_NMBR = 1;
    			
    		}
    		
    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		//invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvItemLocation(invItemLocation);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);
    		
    		// propagate balance if necessary           
    		
    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    		
    		Iterator i = invCostings.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
    			
    		}                           
    		
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	
    }

    private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

    	Debug.print("ArInvoicePostControllerBean convertByUomFromAndUomToAndQuantity");		        

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
    		invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	

    		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
    		LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

    		return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvCostPrecisionUnit());       	        		        		       	

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }
    
    /*private double getInvFifoCost2(Date CST_DT, Integer IL_CODE, double CST_QTY, 
     		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
  	 {
  	  	 
  		 LocalInvCostingHome invCostingHome = null;
  	  	 LocalInvItemLocationHome invItemLocationHome = null;
  	  	 LocalAdPreferenceHome adPreferenceHome = null;
  	       int isNotSingle=0;
  	     // Initialize EJB Home
  	        
  	     try {
  	         
  	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
  	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
  	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
  	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
  	    	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
  	    	 	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
  	     } 
  	     catch (NamingException ex) {
  	            
  	    	 throw new EJBException(ex.getMessage());
  	     }
  	    	
  		try {
  			
  			 CostA=0d;
  			 CostB=0d;
  			 UnitA=0d;
  			 UnitB=0d;
  			CostAmount[0]=0d;
  			CostAmount[1]=0d;
  			CostAmount[2]=0d;
  			CostAmount[3]=0d;
  			CostAmount[4]=0d;
  			CostAmount[5]=0d;
  			CostAmount[6]=0d;
  			CostAmount[7]=0d;
  			CostAmount[8]=0d;
  			CostAmount[9]=0d;
  			UnitValue[0]=0d;
  			UnitValue[1]=0d;
  			UnitValue[2]=0d;
  			UnitValue[3]=0d;
  			UnitValue[4]=0d;
  			UnitValue[5]=0d;
  			UnitValue[6]=0d;
  			UnitValue[7]=0d;
  			UnitValue[8]=0d;
  			UnitValue[9]=0d;
  			
  			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
  			System.out.println("TEST " + CST_DT + " " + IL_CODE + " " + CST_QTY + " " + AD_BRNCH + " " + AD_CMPNY);
  			if (invFifoCostings.size() > 0) {
  				double fifoCost = 0;
  			    double runningQty = Math.abs(CST_QTY);
  			    double xStart=runningQty;
  			    double value=runningQty;
  			    double costvalue=0d;
  			    double xStartCost=0d;
  			    double totalUnitCommited=0d;
  			   System.out.println("Start ----:" + CST_QTY );
  				Iterator x = invFifoCostings.iterator();
  				while (x.hasNext()) {
  					
  					LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
  	  				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
  	  				double newRemainingLifoQuantity = 0;
  	  				if (invFifoCosting.getCstRemainingLifoQuantity() <= runningQty) {
  	  					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
  	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * invFifoCosting.getCstRemainingLifoQuantity();
  		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
  		 	  				fifoCost  += invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold() * invFifoCosting.getCstRemainingLifoQuantity();
  	 	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
  	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * invFifoCosting.getCstRemainingLifoQuantity();
  	 	 	  			} else {
  	 	 	  				fifoCost += invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity() * invFifoCosting.getCstRemainingLifoQuantity();
  	 	 	  			}
  	  					
  	  					runningQty = runningQty - invFifoCosting.getCstRemainingLifoQuantity();
  	  					//System.out.println("runningQty ----:" + runningQty );
  	  					double xxxxx=Math.abs(CST_QTY)-runningQty;
  	  					costvalue=fifoCost-xStartCost;
  	  					System.out.println("cost ----:" + costvalue );
  	  					xStartCost+=costvalue;
  	  					System.out.println("costvalue ----:" + xStartCost );
  	  					value=xStart-runningQty;
  	  					//System.out.println("value ----:" + value );
  	  					xStart=runningQty;
  	  					totalUnitCommited+=value;
  	  					//System.out.println("xStart ----:" + xStart );
  	  					//System.out.println("runningQty ----:" + runningQty );
  	  					//System.out.println("xxxxx ----:" + xxxxx );
  	  					//System.out.println("Unit ----:" + xxxxx );
  	  					//System.out.println("Cost ---:" + fifoCost);
  	  					CostA=fifoCost;
  	  					UnitA=xxxxx;

  	  					CostAmount[isNotSingle]=costvalue;
  	  					UnitValue[isNotSingle]=value;
  	  					invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity()-value);
  	  					System.out.println("invFifoCosting.getCstRemainingLifoQuantity() ----:" + invFifoCosting.getCstRemainingLifoQuantity() );
  	  					System.out.println("commited() ----:" + value );	
  	  					
  	  					System.out.println("Unit ----:" + UnitValue[isNotSingle] );
  	  					System.out.println("Cost ---:" + CostAmount[isNotSingle]);
  	  					System.out.println("isNotSingle" + isNotSingle);
  	  					isNotSingle++;
  	  				} else {
  	  					System.out.println("CostForward ---:" + fifoCost);
  	  					double fifoCost2=0d;
  	  					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
  	  					fifoCost2 += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * runningQty;
  		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
  		 	  			fifoCost2  += invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold() * runningQty;
  	 	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
  	 	 	  			fifoCost2 += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * runningQty;
  	 	 	  			} else {
  	 	 	  			fifoCost2 += invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity() * runningQty;
  	 	 	  			}
  	  					newRemainingLifoQuantity = invFifoCosting.getCstRemainingLifoQuantity() - runningQty;
  	  					
  	  					//fifoCost2=fifoCost -fifoCost2;
  	  					//System.out.println("Unit ----:" + runningQty);
  	  					//System.out.println("Cost ---:" + fifoCost2);
  	  					CostB=fifoCost2;
  	  					System.out.println("fifoCost2 ----:" + fifoCost2 );
  	  					UnitB=runningQty;
  	  					invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity()-runningQty);
  	  					System.out.println("invFifoCosting.getCstRemainingLifoQuantity() ----:" + invFifoCosting.getCstRemainingLifoQuantity() );
	  					System.out.println("commited() ----:" + runningQty );
	  					
	  				
	  					
	  					
  	  					if(isNotSingle==0)
	  					{
	  						CostAmount[isNotSingle]=fifoCost2;
	  						UnitValue[isNotSingle]=runningQty;
	  					}
	  					else
	  					{
	  					CostAmount[isNotSingle]=fifoCost2;
	  					UnitValue[isNotSingle]=runningQty;
	  					}
  	  						totalUnitCommited+=runningQty;
  	  					System.out.println("UnitA ----:" + UnitValue[isNotSingle] );
	  					System.out.println("CostA---:" + CostAmount[isNotSingle]);
	  					System.out.println("isNotSingle" + isNotSingle);
  	  					runningQty = 0;
  	  					isNotSingle++;
  	  				}
  	  				
  	 	  			if (isAdjustFifo) 
  	 	  				{
  	 	  				//invFifoCosting.setCstRemainingLifoQuantity(newRemainingLifoQuantity);
  	 	  			System.out.println("Commited value" + totalUnitCommited);
  	 	  				}
  					if (runningQty <=0)
  						
  						break;
  						
  				}
  				//System.out.println("fifoCost" + fifoCost);
  				//System.out.println("CST_QTY" + CST_QTY);
  				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
  				System.out.println("---------------------->isNotSingle" + isNotSingle);
  				return isNotSingle;
  				
  			} 
  			else {
  				
  				//most applicable in 1st entries of data
  				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
  				System.out.println("---------------------->isNotSingle" + isNotSingle);
  				return isNotSingle;
  			}
  				
  		}
  		catch (Exception ex) {
  			Debug.printStackTrace(ex);
  		    throw new EJBException(ex.getMessage());
  		}
  	}  */  
//SessionBean methods

    
    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException,
		GlobalAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException,
		GlobalExpiryDateNotFoundException {
                    
        Debug.print("InvAdjustmentPostControllerBean executeInvAdjPost");
        
        LocalInvAdjustmentHome invAdjustmentHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvItemHome invItemHome = null;
                
        // Initialize EJB Home
        
        try {
            
            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);        
    		invItemHome = (LocalInvItemHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	
        	// validate if adjustment is already deleted
        	
        	LocalInvAdjustment invAdjustment = null;
        	
        	try {
        		
        		invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if adjustment is already posted or void
        	
        	if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
        		throw new GlobalTransactionAlreadyPostedException();
        		
        	}
        	
        	// regenerate inventory dr
        	
        	this.regenerateInventoryDr(invAdjustment, AD_BRNCH, AD_CMPNY);
        	
        	
Collection invAdjustmentLines = null;
    		
    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

            
            Iterator c = invAdjustmentLines.iterator();
            
			while(c.hasNext()) {
				
				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) c.next();
				
				String II_NM = invAdjustmentLine.getInvItemLocation().getInvItem().getIiName();
				String LOC_NM = invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName();
				
				
				double ADJUST_QTY = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), 
				  invAdjustmentLine.getInvItemLocation().getInvItem(), invAdjustmentLine.getAlAdjustQuantity(), AD_CMPNY);
				
				LocalInvCosting invCosting = null;
				
				try {
					
					invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invAdjustment.getAdjDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
					
				} catch (FinderException ex) {
																											
				}
				
				double COST = invAdjustmentLine.getAlUnitCost();
				
				if (invCosting == null ) {
	
						this.postToInv(invAdjustmentLine, invAdjustment.getAdjDate(), 
								ADJUST_QTY, COST * ADJUST_QTY,
								ADJUST_QTY, COST * ADJUST_QTY, 
								0d, null, AD_BRNCH, AD_CMPNY);
								  				
				} else {

					this.postToInv(invAdjustmentLine, invAdjustment.getAdjDate(), 
							ADJUST_QTY, COST* ADJUST_QTY,
							invCosting.getCstRemainingQuantity() + ADJUST_QTY, invCosting.getCstRemainingValue() + (COST * ADJUST_QTY), 
							0d, null, AD_BRNCH, AD_CMPNY);
				
				}
				
            				
			}
        	
        	// set invoice post status
        	
        	invAdjustment.setAdjPosted(EJBCommon.TRUE);
        	invAdjustment.setAdjPostedBy(USR_NM);
        	invAdjustment.setAdjDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
        	
        	// post to gl if necessary
        	
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
        	
           	
        	   // validate if date has no period and period is closed
        	   
        	   LocalGlSetOfBook glJournalSetOfBook = null;
        	  
        	   try {
        	    
        	   	   glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);
        	   	   
        	   } catch (FinderException ex) {
        	   
        	       throw new GlJREffectiveDateNoPeriodExistException();
        	   
        	   }
			
			   LocalGlAccountingCalendarValue glAccountingCalendarValue = 
			        glAccountingCalendarValueHome.findByAcCodeAndDate(
			        	glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);
			        	
			        	
			   if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
			        glAccountingCalendarValue.getAcvStatus() == 'C' ||
			        glAccountingCalendarValue.getAcvStatus() == 'P') {
			        	
			        throw new GlJREffectiveDatePeriodClosedException();
			        
			   }
			   
			   // check if invoice is balance if not check suspense posting
		            	
	           LocalGlJournalLine glOffsetJournalLine = null;
	        	
			Collection invDistributionRecords = null;
			
			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {
				
				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
						invAdjustment.getAdjCode(), AD_CMPNY);
				
			} else {
				
				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
						invAdjustment.getAdjCode(), AD_CMPNY);
				
			}

	            
	           Iterator j = invDistributionRecords.iterator();
	        	
	           double TOTAL_DEBIT = 0d;
	           double TOTAL_CREDIT = 0d;
	        	
	           while (j.hasNext()) {
	        		
	        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
	        		
	        		double DR_AMNT = 0d;
	        		
	        		DR_AMNT = invDistributionRecord.getDrAmount();
	        		
	        		if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
	        			
	        			TOTAL_DEBIT += DR_AMNT;
	        			            			
	        		} else {
	        			
	        			TOTAL_CREDIT += DR_AMNT;
	        			
	        		}
	        		
	        	}
	        	
	        	TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
	        	TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
	        	            	            	            	
	        	if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE && TOTAL_DEBIT != TOTAL_CREDIT) {
	        	    	
	        	    LocalGlSuspenseAccount glSuspenseAccount = null;
	        	    	
	        	    try { 	
	        	    	
	        	        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS", AD_CMPNY);
	        	        
	        	    } catch (FinderException ex) {
	        	    	
	        	    	throw new GlobalJournalNotBalanceException();
	        	    	
	        	    }
	        	               	    
	        	    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
	        	    	
	        	    	glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE, 
	        	    	    TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
	        	    	              	        
	        	    } else {
	        	    	
	        	    	glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
	        	    	    TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
	        	    	
	        	    }
	        	    
	        	    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
	        	    //glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
	        	    glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
	        	    
	        	    	
				} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
				    TOTAL_DEBIT != TOTAL_CREDIT) {
				    
					throw new GlobalJournalNotBalanceException();		    	
				    	
				}
			   		       
		       // create journal batch if necessary
		       
		       LocalGlJournalBatch glJournalBatch = null;
		       java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
		       
		       try {
		           
	   	   			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
		       	   	
		       } catch (FinderException ex) {
		       
		       }
				     	
		       if (glJournalBatch == null) {
		     		
	     		    glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
		     		
		       }
		     				     		
			   // create journal entry			            	
		            		            	
     	   LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
     		    invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
     		    0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
     		    'N', EJBCommon.TRUE, EJBCommon.FALSE,
     		    USR_NM, new Date(),
     		    USR_NM, new Date(),
     		    null, null,
     		    USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
     		    null, null, EJBCommon.FALSE, null, 

     		    AD_BRNCH, AD_CMPNY);
     		    
     	   		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
	           glJournal.setGlJournalSource(glJournalSource);
	        	
	           LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
	           glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
	        	
	           LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
	           glJournal.setGlJournalCategory(glJournalCategory);
     		
     	   if (glJournalBatch != null) {

     		   glJournal.setGlJournalBatch(glJournalBatch);
     			
     	   }            	       	            		            
            // create journal lines
         	          	
            j = invDistributionRecords.iterator();
         	
            while (j.hasNext()) {
         		
         		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
         		
         		double DR_AMNT = 0d;
	        		
         		DR_AMNT = invDistributionRecord.getDrAmount();
         		            		
         		LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
         			invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
         			
                 //invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
                 glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());
         	    
         	    //glJournal.addGlJournalLine(glJournalLine);
         	    glJournalLine.setGlJournal(glJournal);
         	    
         	    invDistributionRecord.setDrImported(EJBCommon.TRUE);
         	    
         		
            }
         	
            if (glOffsetJournalLine != null) {
         		
         	   //glJournal.addGlJournalLine(glOffsetJournalLine);
         	   glOffsetJournalLine.setGlJournal(glJournal);
         		
            }		
            
            // post journal to gl
            
            Collection glJournalLines = glJournal.getGlJournalLines();
	       
		       Iterator i = glJournalLines.iterator();
		       
		       while (i.hasNext()) {
		       	
		           LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
		           		           
		           // post current to current acv
		             
		           this.postToGl(glAccountingCalendarValue,
		               glJournalLine.getGlChartOfAccount(),
		               true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
			 	 
		         
		           // post to subsequent acvs (propagate)
		         
		           Collection glSubsequentAccountingCalendarValues = 
		               glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
		                   glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
		                   glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
		                 
		           Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
		         
		           while (acvsIter.hasNext()) {
		         	
		         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
		         	       (LocalGlAccountingCalendarValue)acvsIter.next();
		         	       
		         	   this.postToGl(glSubsequentAccountingCalendarValue,
		         	       glJournalLine.getGlChartOfAccount(),
		         	       false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
		         		         	
		           }
		           
		           // post to subsequent years if necessary
	           
		           Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
			  	  	
			  	  	if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
			  	  	
				  	  	adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);	
				  	  	
				  	  LocalGlChartOfAccount glRetainedEarningsAccount = null;
      				
	      				try{
	      					
	      					glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY);
	      				
	      				} catch (FinderException ex) {
	      					
	      					throw new GlobalAccountNumberInvalidException();
								
	      				}
				  	  	Iterator sobIter = glSubsequentSetOfBooks.iterator();
				  	  	
				  	  	while (sobIter.hasNext()) {
				  	  		
				  	  		LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
				  	  		
				  	  		String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
				  	  		
				  	  		// post to subsequent acvs of subsequent set of book(propagate)
		         
				           Collection glAccountingCalendarValues = 
				               glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
				                 
				           Iterator acvIter = glAccountingCalendarValues.iterator();
				         
				           while (acvIter.hasNext()) {
				         	
				         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
				         	       (LocalGlAccountingCalendarValue)acvIter.next();
				         	       
				         	    if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
					 				ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
				         	       
						         	this.postToGl(glSubsequentAccountingCalendarValue,
						         	     glJournalLine.getGlChartOfAccount(),
						         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
						         	     
						        } else { // revenue & expense
						        					             					             
						             this.postToGl(glSubsequentAccountingCalendarValue,
						         	     glRetainedEarningsAccount,
						         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
						        
						        }
				         		         	
				           }
				  	  		
				  	  	   if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
				  	  		
				  	  	}
				  	  	
			  	  	}
		       	   	       	   	       	
	           }
	           
	        
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalInventoryDateException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch(GlobalBranchAccountNumberInvalidException ex) {
			
        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch (GlobalAccountNumberInvalidException ex) {
			
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
    	} /*catch (GlobalExpiryDateNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
    	} catch (GlobalRecordInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
            
        }*/catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    
    
    private void executeInvBuaPost(Integer BUA_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		AdPRFCoaGlVarianceAccountNotFoundException {
    	
    	Debug.print("InvApprovalControllerBean executeInvBuaPost");
    	
    	LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;        
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalInvItemHome invItemHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try {
    		
    		// validate if build/unbuild assembly is already deleted
    		
    		LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = null;
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		try {
    			
    			invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(BUA_CODE);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlobalRecordAlreadyDeletedException();
    			
    		}
    		
    		// validate if build/unbuild assembly is already posted or void
    		
    		if (invBuildUnbuildAssembly.getBuaPosted() == EJBCommon.TRUE) {
    			
    			throw new GlobalTransactionAlreadyPostedException();
    			
    		}
    		
    		// regenerate inventory dr
    		
    		this.regenerateInventoryDr(invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
            
    		Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
            
            Iterator c = invBuildUnbuildAssemblyLines.iterator();
            
            while(c.hasNext()) {
                
                LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine) c.next();
                
                String II_NM = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName();
                String LOC_NM = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName();
                double BUILD_QTY = invBuildUnbuildAssemblyLine.getBlBuildQuantity();
                
                double TOTAL_AMOUNT = 0d;
                
                Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                
                Iterator j = invBillOfMaterials.iterator();
                
                while (j.hasNext()) {
                    
                    LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)j.next();
                    
                    LocalInvCosting invBomCosting = null;
                    
                    double BOM_QTY_NDD = 0d;
                    double COST = 0d;
                    double BOM_AMOUNT = 0d;
                    
                    LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);
                    
                    // post bill of materials
                    
                    try {
                        
                        invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                invBuildUnbuildAssembly.getBuaDate(), invBillOfMaterial.getBomIiName(), invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                    }

                    // bom conversion		    						
                    BOM_QTY_NDD = this.convertByUomFromAndItemAndQuantity(
                            invBillOfMaterial.getInvUnitOfMeasure(), invItem,
                            EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() *  Math.abs(BUILD_QTY),
                            this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);
                    
                    if (invBomCosting == null) {
                        
                        BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * invItem.getIiUnitCost(), adCompany.getGlFunctionalCurrency().getFcPrecision());
                        
                        TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);
                        
                        if (BUILD_QTY >=0) {
                            
                            this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), -BOM_QTY_NDD, -BOM_AMOUNT,
                            		-BOM_QTY_NDD, -BOM_AMOUNT, invBillOfMaterial.getBomIiName(), invBillOfMaterial.getBomLocName(),
									0d, null, AD_BRNCH, AD_CMPNY);    						
                            
                        } else {
                            
                            this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BOM_QTY_NDD, BOM_AMOUNT,
                            		BOM_QTY_NDD, BOM_AMOUNT, invBillOfMaterial.getBomIiName(), invBillOfMaterial.getBomLocName(),
									0d, null, AD_BRNCH, AD_CMPNY);
                            
                        }
                        
                    } else {
                        
                       
                        COST = Math.abs(invBomCosting.getCstRemainingValue() / invBomCosting.getCstRemainingQuantity());
                        
                        BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * COST, adCompany.getGlFunctionalCurrency().getFcPrecision());
                        
                        TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);
                        
                        if (BUILD_QTY >= 0) {
                            
                            this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), -BOM_QTY_NDD, -BOM_AMOUNT,
                            		invBomCosting.getCstRemainingQuantity() - BOM_QTY_NDD,
                            		invBomCosting.getCstRemainingValue() - BOM_AMOUNT, invBillOfMaterial.getBomIiName(),
									invBillOfMaterial.getBomLocName(), 0d, null, AD_BRNCH, AD_CMPNY);
                            
                        } else {
                            
        			        //compute cost variance   
        					double CST_VRNC_VL = 0d;

        					if(invBomCosting.getCstRemainingQuantity() < 0)
        						CST_VRNC_VL = (invBomCosting.getCstRemainingQuantity() * (BOM_AMOUNT/BOM_QTY_NDD) -
        								invBomCosting.getCstRemainingValue());

                            this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BOM_QTY_NDD,  BOM_AMOUNT,
                            		invBomCosting.getCstRemainingQuantity() + BOM_QTY_NDD,
                            		invBomCosting.getCstRemainingValue() + (BOM_AMOUNT), invBillOfMaterial.getBomIiName(),
									invBillOfMaterial.getBomLocName(), CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);							
                            
                        }
                        
                    }
                    
                }

                // post assembly item
                
                LocalInvCosting invLastCosting = null;
                
                try {
                    
                    invLastCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invBuildUnbuildAssembly.getBuaDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                if (invLastCosting == null) {
                    
                    if (BUILD_QTY >= 0) {
                        
                        this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, TOTAL_AMOUNT,
                        		BUILD_QTY, TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
                        
                    } else {

                        this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, -TOTAL_AMOUNT,
                        		BUILD_QTY, -TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
                        
                    }			
                    
                } else {
                    
                    if (BUILD_QTY >= 0) {

    			        //compute cost variance   
    					double CST_VRNC_VL = 0d;

    					if(invLastCosting.getCstRemainingQuantity() < 0)
    						CST_VRNC_VL = (invLastCosting.getCstRemainingQuantity() * (TOTAL_AMOUNT/BUILD_QTY) -
    								invLastCosting.getCstRemainingValue());

                        this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, TOTAL_AMOUNT,
                        		invLastCosting.getCstRemainingQuantity() + BUILD_QTY,
								invLastCosting.getCstRemainingValue() + TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
                        
                    } else {
                        
                        this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, -TOTAL_AMOUNT,
                        		invLastCosting.getCstRemainingQuantity() + BUILD_QTY,
								invLastCosting.getCstRemainingValue() - TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
                        
                    }
                    
                }
                
            }
    		
            // post build/unbuild assembly
            
            // set build/unbuild assembly post status
            
    		invBuildUnbuildAssembly.setBuaPosted(EJBCommon.TRUE);
    		invBuildUnbuildAssembly.setBuaPostedBy(USR_NM);
    		invBuildUnbuildAssembly.setBuaDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
    		
    		// post to gl if necessary
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		//LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		if (adPreference.getPrfInvGlPostingType().equals("USE SL POSTING")) {
    			
    			// validate if date has no period and period is closed
    			
    			LocalGlSetOfBook glJournalSetOfBook = null;
    			
    			try {
    				
    				glJournalSetOfBook = glSetOfBookHome.findByDate(invBuildUnbuildAssembly.getBuaDate(), AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    				throw new GlJREffectiveDateNoPeriodExistException();
    				
    			}
    			
    			LocalGlAccountingCalendarValue glAccountingCalendarValue = 
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invBuildUnbuildAssembly.getBuaDate(), AD_CMPNY);
    			
    			
    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
    				
    				throw new GlJREffectiveDatePeriodClosedException();
    				
    			}
    			
    			// check if invoice is balance if not check suspense posting
    			
    			LocalGlJournalLine glOffsetJournalLine = null;
    			
    			Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByBuaCode(invBuildUnbuildAssembly.getBuaCode(), AD_CMPNY);
    			
    			Iterator j = invDistributionRecords.iterator();
    			
    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    					
    					TOTAL_DEBIT += DR_AMNT;
    					
    				} else {
    					
    					TOTAL_CREDIT += DR_AMNT;
    					
    				}
    				
    			}
    			
    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			
    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				LocalGlSuspenseAccount glSuspenseAccount = null;
    				
    				try { 	
    					
    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ASSEMBLIES", AD_CMPNY);
    					
    				} catch (FinderException ex) {
    					
    					throw new GlobalJournalNotBalanceException();
    					
    				}
    				
    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
								EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
    					
    				} else {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
								EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
    					
    				}
    				
    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				
    				
    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				throw new GlobalJournalNotBalanceException();		    	
    				
    			}
    			
    			// create journal batch if necessary
    			
    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
    			
    			try {
    				
    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ASSEMBLIES", AD_BRNCH, AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			if (glJournalBatch == null) {
    				
    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ASSEMBLIES", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// create journal entry			            	
    			
    			LocalGlJournal glJournal = glJournalHome.create(invBuildUnbuildAssembly.getBuaReferenceNumber(),
    					invBuildUnbuildAssembly.getBuaDescription(), invBuildUnbuildAssembly.getBuaDate(),
						0.0d, null, invBuildUnbuildAssembly.getBuaDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null, 
						AD_BRNCH, AD_CMPNY);
    			
    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);
    			
    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
    			
    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ASSEMBLIES", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);
    			
    			if (glJournalBatch != null) {
    				
    				glJournal.setGlJournalBatch(glJournalBatch);
    				
    			}           		    
    			
    			// create journal lines
    			
    			j = invDistributionRecords.iterator();
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
							invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
    				
    				invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				
    				glJournal.addGlJournalLine(glJournalLine);
    				
    				invDistributionRecord.setDrImported(EJBCommon.TRUE);
    				
    				
    			}
    			
    			if (glOffsetJournalLine != null) {
    				
    				glJournal.addGlJournalLine(glOffsetJournalLine);
    				
    			}		
    			
    			// post journal to gl
    			
    			Collection glJournalLines = glJournal.getGlJournalLines();
    			
    			Iterator i = glJournalLines.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
    				
    				// post current to current acv
    				
    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    				
    				
    				// post to subsequent acvs (propagate)
    				
    				Collection glSubsequentAccountingCalendarValues = 
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
    				
    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
    				
    				while (acvsIter.hasNext()) {
    					
    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    						(LocalGlAccountingCalendarValue)acvsIter.next();
    					
    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    					
    				}
    				
    				// post to subsequent years if necessary
    				
    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
    				
    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
    					
    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
    					LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY); 	  	
    					
    					Iterator sobIter = glSubsequentSetOfBooks.iterator();
    					
    					while (sobIter.hasNext()) {
    						
    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
    						
    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
    						
    						// post to subsequent acvs of subsequent set of book(propagate)
    						
    						Collection glAccountingCalendarValues = 
    							glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
    						
    						Iterator acvIter = glAccountingCalendarValues.iterator();
    						
    						while (acvIter.hasNext()) {
    							
    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    								(LocalGlAccountingCalendarValue)acvIter.next();
    							
    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    								
    							} else { // revenue & expense
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
    								
    							}
    							
    						}
    						
    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
    						
    					}
    					
    				}
    				
    			}
    			
    		}		   
    		
    	} catch (GlJREffectiveDateNoPeriodExistException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlJREffectiveDatePeriodClosedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalJournalNotBalanceException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalRecordAlreadyDeletedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalTransactionAlreadyPostedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
        	
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void executeInvStPost(Integer ST_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	    GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		AdPRFCoaGlVarianceAccountNotFoundException {
	    
	    Debug.print("InvApprovalControllerBean executeInvStPost");
	    
	    LocalInvStockTransferHome invStockTransferHome = null;        
	    LocalInvItemLocationHome invItemLocationHome = null;
	    LocalInvCostingHome invCostingHome = null;
	    LocalGlSetOfBookHome glSetOfBookHome = null;
	    LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
	    LocalInvDistributionRecordHome invDistributionRecordHome = null;
	    LocalAdCompanyHome adCompanyHome = null;
	    LocalGlJournalBatchHome glJournalBatchHome = null;
	    LocalGlJournalHome glJournalHome = null;
	    LocalGlJournalSourceHome glJournalSourceHome = null;
	    LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
	    LocalGlJournalCategoryHome glJournalCategoryHome = null;
	    LocalGlJournalLineHome glJournalLineHome = null;
	    LocalGlChartOfAccountHome glChartOfAccountHome = null;
	    LocalAdPreferenceHome adPreferenceHome = null;
	    LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
	    LocalInvLocationHome invLocationHome = null;
	    
	    // Initialize EJB Home
	    
	    try {
	        
	        invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.lookUpLocalHome(
	                LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);            
	        invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.lookUpLocalHome(
	                LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	        invCostingHome = (LocalInvCostingHome)EJBHomeFactory.lookUpLocalHome(
	                LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	        glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.lookUpLocalHome(
	                LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
	        glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.lookUpLocalHome(
	                LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
	        invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.lookUpLocalHome(
	                LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.lookUpLocalHome(
	                LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.lookUpLocalHome(
	                LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
	        glJournalHome = (LocalGlJournalHome)EJBHomeFactory.lookUpLocalHome(
	                LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
	        glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.lookUpLocalHome(
	                LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
	        glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.lookUpLocalHome(
	                LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
	        glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.lookUpLocalHome(
	                LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
	        glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.lookUpLocalHome(
	                LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
	        glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.lookUpLocalHome(
	                LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
	        adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.lookUpLocalHome(
	                LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
	        glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.lookUpLocalHome(
	                LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
	        invLocationHome = (LocalInvLocationHome)EJBHomeFactory.lookUpLocalHome(
	                LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
	        
	    } catch(NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	                 
	    }
	    
	    try {
	        
	        // validate if adjustment is already deleted
	        
	        LocalInvStockTransfer invStockTransfer = null;
	        
	        try {
	            
	            invStockTransfer = invStockTransferHome.findByPrimaryKey(ST_CODE);
	            
	        } catch (FinderException ex) {
	            
	            throw new GlobalRecordAlreadyDeletedException();
	        }
	        
	        // validate if adjustment is already posted or void
	        
	        if (invStockTransfer.getStPosted() == EJBCommon.TRUE) {
	            
	            throw new GlobalTransactionAlreadyPostedException();
	            
	        }
	        
            // regenearte inventory dr
            
            this.regenerateInventoryDr(invStockTransfer, AD_BRNCH, AD_CMPNY);

	        Iterator i = invStockTransfer.getInvStockTransferLines().iterator();
	        
	        while (i.hasNext()) {
	            
	            LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) i.next();                               
	            
	            String LOC_NM_FRM = invLocationHome.findByPrimaryKey(
	                    invStockTransferLine.getStlLocationFrom()).getLocName();                
	            String LOC_NM_TO = invLocationHome.findByPrimaryKey(
	                    invStockTransferLine.getStlLocationTo()).getLocName();
	            
	            LocalInvItemLocation invItemLocationFrom = invItemLocationHome.findByLocNameAndIiName(
	                    LOC_NM_FRM, invStockTransferLine.getInvItem().getIiName(),AD_CMPNY);                
	            LocalInvItemLocation invItemLocationTo = invItemLocationHome.findByLocNameAndIiName(
	                    LOC_NM_TO, invStockTransferLine.getInvItem().getIiName(),AD_CMPNY);                                
	            
	            double ST_COST = invStockTransferLine.getStlQuantityDelivered() * invStockTransferLine.getStlUnitCost();
	            
	            double ST_QTY = this.convertByUomFromAndItemAndQuantity(
	                    invStockTransferLine.getInvUnitOfMeasure(), 
	                    invStockTransferLine.getInvItem(), 
	                    invStockTransferLine.getStlQuantityDelivered(),
	                    AD_CMPNY);
	            
	            String II_NM = invStockTransferLine.getInvItem().getIiName();
	                            
	            LocalInvCosting invCostingFrom = null;
                LocalInvCosting invCostingTo = null;
                
                try {                    
                    invCostingFrom = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                            			invStockTransfer.getStDate(), II_NM, LOC_NM_FRM, AD_BRNCH, AD_CMPNY);                                                          
                } catch (FinderException ex) {
                    
                }                              
                
                try {                    
                    invCostingTo = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    									invStockTransfer.getStDate(), II_NM, LOC_NM_TO, AD_BRNCH, AD_CMPNY);                    
                } catch (FinderException ex) {
                                        
                }
                
                if (invCostingFrom == null) {
                    
                    this.post(invStockTransferLine, invItemLocationFrom, invStockTransfer.getStDate(), -ST_QTY, -ST_COST, -ST_QTY,
                    		-ST_COST, 0d, null, AD_BRNCH, AD_CMPNY);
                    
                    this.post(invStockTransferLine, invItemLocationTo, invStockTransfer.getStDate(), ST_QTY, ST_COST, ST_QTY, ST_COST,
                    		0d, null, AD_BRNCH, AD_CMPNY);
                    
                } else {
                    
                	if(invCostingFrom.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
                	{
                		
	                    LocalInvCosting invLastCostingFrom = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invStockTransfer.getStDate(), II_NM, LOC_NM_FRM, AD_BRNCH, AD_CMPNY);

	                    double COST = Math.abs(invCostingFrom.getCstRemainingValue() / invCostingFrom.getCstRemainingQuantity());
	                    
	                    this.post(invStockTransferLine, invItemLocationFrom, invStockTransfer.getStDate(), -ST_QTY, -ST_QTY * COST, 
	                    		invLastCostingFrom.getCstRemainingQuantity() - ST_QTY, 
	                    		invLastCostingFrom.getCstRemainingValue() - (ST_QTY * COST), 0d, null, AD_BRNCH, AD_CMPNY);
                    
	                    LocalInvCosting invLastCostingTo = null;
	                    
	                    
	                    try {
	                    	
	                    	 invLastCostingTo = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invStockTransfer.getStDate(), II_NM, LOC_NM_TO, AD_BRNCH, AD_CMPNY);
		                    
	                    } catch (FinderException ex) {
                            
	                    }
	                    
	                    
	                    if (invLastCostingTo == null) {
	                    	
	                    	this.post(invStockTransferLine, invItemLocationTo, invStockTransfer.getStDate(), ST_QTY, ST_COST, ST_QTY, ST_COST,
	                        		0d, null, AD_BRNCH, AD_CMPNY);
	                    	
	                    	
	                    } else {
	                    	
	                    	//compute cost variance   
							double CST_VRNC_VL = 0d;
		
							if(invLastCostingTo.getCstRemainingQuantity() < 0)
								CST_VRNC_VL = (invLastCostingTo.getCstRemainingQuantity() * (COST) -
										invLastCostingTo.getCstRemainingValue());
		
							this.post(invStockTransferLine, invItemLocationTo, invStockTransfer.getStDate(), ST_QTY, ST_QTY * COST, 
									invLastCostingTo.getCstRemainingQuantity() + ST_QTY, 
									invLastCostingTo.getCstRemainingValue() + (ST_QTY * COST), CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);
							
	                    }
	                    
                	} else if(invCostingFrom.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {
                		
	        			LocalInvCosting invFifoCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invStockTransfer.getStDate(), II_NM, LOC_NM_FRM, AD_BRNCH, AD_CMPNY);;
	        			
	        			double fifoCost = this.getInvFifoCost(invFifoCosting.getCstDate(), invFifoCosting.getInvItemLocation().getIlCode(), 
	        					-ST_QTY, invStockTransferLine.getStlUnitCost(), true, AD_BRNCH, AD_CMPNY);
	        			
	        			//post entries to database        			
	        			this.post(invStockTransferLine, invItemLocationFrom, invStockTransfer.getStDate(), -ST_QTY, fifoCost * -ST_QTY, 
	        					invFifoCosting.getCstRemainingQuantity() - ST_QTY, 
	        					invFifoCosting.getCstRemainingValue() - (fifoCost * ST_QTY), 0d, null, AD_BRNCH, AD_CMPNY);

	        			invFifoCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invStockTransfer.getStDate(), II_NM, LOC_NM_TO, AD_BRNCH, AD_CMPNY);
	        			
	                    //compute cost variance   
						double CST_VRNC_VL = 0d;
	
						if(invFifoCosting.getCstRemainingQuantity() < 0)
							CST_VRNC_VL = (invFifoCosting.getCstRemainingQuantity() * (fifoCost) -
									invFifoCosting.getCstRemainingValue());
	
						this.post(invStockTransferLine, invItemLocationTo, invStockTransfer.getStDate(), ST_QTY, ST_QTY * fifoCost, 
								invFifoCosting.getCstRemainingQuantity() + ST_QTY, 
								invFifoCosting.getCstRemainingValue() + (ST_QTY * fifoCost), CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);
                	}
                	else if(invCostingFrom.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
                	{
	        			LocalInvCosting invStandardCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invStockTransfer.getStDate(), II_NM, LOC_NM_FRM, AD_BRNCH, AD_CMPNY);;
	        			
	        			double standardCost = invStandardCosting.getInvItemLocation().getInvItem().getIiUnitCost();
	        			
	        			//post entries to database        			
	        			this.post(invStockTransferLine, invItemLocationFrom, invStockTransfer.getStDate(), -ST_QTY, standardCost * -ST_QTY, 
	        					invStandardCosting.getCstRemainingQuantity() - ST_QTY, 
	        					invStandardCosting.getCstRemainingValue() - (standardCost * ST_QTY), 0d, null, AD_BRNCH, AD_CMPNY);

	        			invStandardCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invStockTransfer.getStDate(), II_NM, LOC_NM_TO, AD_BRNCH, AD_CMPNY);
	        			
	                    //compute cost variance   
						double CST_VRNC_VL = 0d;
	
						if(invStandardCosting.getCstRemainingQuantity() < 0)
							CST_VRNC_VL = (invStandardCosting.getCstRemainingQuantity() * (standardCost) -
									invStandardCosting.getCstRemainingValue());
	
						this.post(invStockTransferLine, invItemLocationTo, invStockTransfer.getStDate(), ST_QTY, ST_QTY * standardCost, 
								invStandardCosting.getCstRemainingQuantity() + ST_QTY, 
								invStandardCosting.getCstRemainingValue() + (ST_QTY * standardCost), CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);
                	}
                    
                }
	        }
	        
	        // set invoice post status
	        
	        invStockTransfer.setStPosted(EJBCommon.TRUE);
	        invStockTransfer.setStPostedBy(USR_NM);
	        invStockTransfer.setStDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
	        
	        // post to GL
	        LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	        LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	        
	        //  validate if date has no period and period is closed
			
			LocalGlSetOfBook glJournalSetOfBook = null;
			
			try {
				
				glJournalSetOfBook = glSetOfBookHome.findByDate(invStockTransfer.getStDate(), AD_CMPNY);
				
			} catch (FinderException ex) {
				
				throw new GlJREffectiveDateNoPeriodExistException();
				
			}
			
			LocalGlAccountingCalendarValue glAccountingCalendarValue = 
				glAccountingCalendarValueHome.findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invStockTransfer.getStDate(), AD_CMPNY);
			
			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
					glAccountingCalendarValue.getAcvStatus() == 'C' ||
					glAccountingCalendarValue.getAcvStatus() == 'P') {
				
				throw new GlJREffectiveDatePeriodClosedException();
				
			}
			
			// check if debit and credit is balance
			
			LocalGlJournalLine glOffsetJournalLine = null;
			
			Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByStCode(invStockTransfer.getStCode(), AD_CMPNY);
			
			i = invDistributionRecords.iterator();
			
			double TOTAL_DEBIT = 0d;
			double TOTAL_CREDIT = 0d;
			
			while (i.hasNext()) {
				
				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
				
				double DR_AMNT = 0d;
				
				DR_AMNT = invDistributionRecord.getDrAmount();
				
				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
					
					TOTAL_DEBIT += DR_AMNT;
					
				} else {
					
					TOTAL_CREDIT += DR_AMNT;
					
				}
			}
			
			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());    		    		
			
			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
	    	    TOTAL_DEBIT != TOTAL_CREDIT) {
	    	    	
	    	    LocalGlSuspenseAccount glSuspenseAccount = null;
	    	    	
	    	    try { 	
	    	    	
	    	        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "STOCK TRANSFERS", AD_CMPNY);
	    	        
	    	    } catch (FinderException ex) {
	    	    	
	    	    	throw new GlobalJournalNotBalanceException();
	    	    	
	    	    }
	    	               	    
	    	    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
	    	    	
	    	    	glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
	    	    	    EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
	    	    	              	        
	    	    } else {
	    	    	
	    	    	glOffsetJournalLine = glJournalLineHome.create( (short)(invDistributionRecords.size() + 1),
	    	    	    EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
	    	    	
	    	    }
	    	    
	    	    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
	    	    glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
	    	    
	    	    	
			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
			    TOTAL_DEBIT != TOTAL_CREDIT) {
			    
				throw new GlobalJournalNotBalanceException();		    	
			    	
			}
			
			// create journal batch if necessary
			
			LocalGlJournalBatch glJournalBatch = null;
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
			
			try {
				
				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " STOCK TRANSFERS", AD_BRNCH, AD_CMPNY);
				
			} catch (FinderException ex) {
				
			}
			
			if (glJournalBatch == null) {
				
				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " STOCK TRANSFERS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
				
			}
			
			// create journal entry			            	
			
			LocalGlJournal glJournal = glJournalHome.create(invStockTransfer.getStReferenceNumber(),
					invStockTransfer.getStDescription(), invStockTransfer.getStDate(),
					0.0d, null, invStockTransfer.getStDocumentNumber(), null, 1d, "N/A", null,
					'N', EJBCommon.TRUE, EJBCommon.FALSE, USR_NM, new Date(), USR_NM, new Date(), null, null,
					USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, EJBCommon.FALSE,
					null, 
					AD_BRNCH, AD_CMPNY);
			
			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
			glJournal.setGlJournalSource(glJournalSource);
			
			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
			
			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("STOCK TRANSFERS", AD_CMPNY);
			glJournal.setGlJournalCategory(glJournalCategory);
			
			if (glJournalBatch != null) {
				
				glJournal.setGlJournalBatch(glJournalBatch);
				
			}           		    
			
			// create journal lines
			
			i = invDistributionRecords.iterator();
			
			while (i.hasNext()) {
				
				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
				
				double DR_AMNT = 0d;
				
				DR_AMNT = invDistributionRecord.getDrAmount();
				
				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
				
				invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
				
				glJournal.addGlJournalLine(glJournalLine);
				
				invDistributionRecord.setDrImported(EJBCommon.TRUE);
				
			}
			
			if (glOffsetJournalLine != null) {
				
				glJournal.addGlJournalLine(glOffsetJournalLine);
				
			}		
			
			// post journal to gl
			
			Collection glJournalLines = glJournal.getGlJournalLines();
			
			i = glJournalLines.iterator();
			
			while (i.hasNext()) {
				
				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
				
				// post current to current acv
				
				this.postToGl(glAccountingCalendarValue,
						glJournalLine.getGlChartOfAccount(),
						true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
				    			
				// post to subsequent acvs (propagate)
				
				Collection glSubsequentAccountingCalendarValues = 
					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
							glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
				
				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
				
				while (acvsIter.hasNext()) {
					
					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
						(LocalGlAccountingCalendarValue)acvsIter.next();
					
					this.postToGl(glSubsequentAccountingCalendarValue,
							glJournalLine.getGlChartOfAccount(),
							false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
					
				}
				
				// post to subsequent years if necessary
				
				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
				
				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
					
					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
					LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY); 	  	
					
					Iterator sobIter = glSubsequentSetOfBooks.iterator();
					
					while (sobIter.hasNext()) {
						
						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
						
						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
						
						// post to subsequent acvs of subsequent set of book(propagate)
						
						Collection glAccountingCalendarValues = 
							glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
						
						Iterator acvIter = glAccountingCalendarValues.iterator();
						
						while (acvIter.hasNext()) {
							
							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
								(LocalGlAccountingCalendarValue)acvIter.next();
							
							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
								
								this.postToGl(glSubsequentAccountingCalendarValue,
										glJournalLine.getGlChartOfAccount(),
										false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
								
							} else { 
								// revenue & expense
								
								this.postToGl(glSubsequentAccountingCalendarValue,
										glRetainedEarningsAccount,
										false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
								
							}
						}
						
						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
						
					}
				}
			}	
	         
	    } catch (GlJREffectiveDateNoPeriodExistException ex) {
	    	
	    	getSessionContext().setRollbackOnly();
	    	throw ex;
	    	
	    } catch (GlJREffectiveDatePeriodClosedException ex) {
	    	
	    	getSessionContext().setRollbackOnly();
	    	throw ex;
	    	
	    } catch (GlobalJournalNotBalanceException ex) {
	    	
	    	getSessionContext().setRollbackOnly();
	    	throw ex;
	    
	    } catch (GlobalRecordAlreadyDeletedException ex) {
	    	
	    	getSessionContext().setRollbackOnly();
	    	throw ex;
	    	        	
	    } catch (GlobalTransactionAlreadyPostedException ex) {
	    	
	    	getSessionContext().setRollbackOnly();
	    	throw ex;
        	
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
	    
	}   
    
    private double convertByUomAndQuantityBst(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
        
        Debug.print("InvApprovalControllerBean convertByUomFromAndUomToAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
       
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    private double getBstInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(String II_NM, String LOC_FRM, String UOM_NM, Date ST_DT, String BR_NM, Integer AD_CMPNY) {
        
        Debug.print("InvApprovalControllerBean getInvIiUnitCostByIiNameAndUomName");
        
        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalAdBranchHome adBranchHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);  
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);  
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrName(BR_NM, AD_CMPNY);
        	
        	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        	double COST = invItem.getIiUnitCost();
        	
        	LocalInvItemLocation invItemLocation = null;
        	
        	
        	try { 
        		
        		invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_FRM, II_NM, AD_CMPNY);
  
      			LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
      					ST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), adBranch.getBrCode(), AD_CMPNY);
    		
      			if(invItemLocation.getInvItem().getIiCostMethod().equals("Average"))
      			{
      				COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), this.getGlFcPrecisionUnit(AD_CMPNY));
      			}
      			else if(invItemLocation.getInvItem().getIiCostMethod().equals("FIFO"))
      			{
      				COST = this.getInvFifoCost(ST_DT, invItemLocation.getIlCode(), invCosting.getCstAdjustQuantity(),
      						invCosting.getCstAdjustCost(), false, adBranch.getBrCode(), AD_CMPNY);
      			}
      			else if(invItemLocation.getInvItem().getIiCostMethod().equals("Standard"))
      			{
      				COST = invItemLocation.getInvItem().getIiUnitCost();
      			}
        		
        	} catch (FinderException ex) {
        		
        	}
        	
        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
        	
    		return EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
        	
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private void executeInvBstPost(Integer BST_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) 
    throws
    GlobalRecordAlreadyDeletedException,		
    GlobalTransactionAlreadyPostedException,
    GlJREffectiveDateNoPeriodExistException,
    GlJREffectiveDatePeriodClosedException,
    GlobalJournalNotBalanceException,
	AdPRFCoaGlVarianceAccountNotFoundException, 
	GlobalExpiryDateNotFoundException,
	GlobalBranchAccountNumberInvalidException{

        
        Debug.print("InvApprovalControllerBean executeInvBstPost");
        
        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;        
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalAdBranchHome adBranchHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);            
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.lookUpLocalHome(
                    LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.lookUpLocalHome(
                    LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.lookUpLocalHome(
            		LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch(NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            // validate if branch stock transfer is already deleted
            
            LocalInvBranchStockTransfer invBranchStockTransfer = null;
            
            try {
                
                invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(BST_CODE);
                
            } catch (FinderException ex) {
                
                throw new GlobalRecordAlreadyDeletedException();
            }
            
            // validate if branch stock transfer is already posted or void
            
            if (invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE) {
                
                throw new GlobalTransactionAlreadyPostedException();
                
            }
            
            // regenerate inventory dr            
            
            Integer BRNCH_FRM = invBranchStockTransfer.getAdBranch().getBrCode();
            
            Iterator i = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();
            Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost A");
            while (i.hasNext()) {
                
                LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) i.next();                               
                
                String locName = invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName();
                String invItemName = invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName();
                String transitLocationName = invBranchStockTransfer.getInvLocation().getLocName();
                
                LocalInvItemLocation invItemLocationFrom = invItemLocationHome.findByLocNameAndIiName(
                        locName, invItemName, AD_CMPNY);
                
                LocalInvItemLocation invItemTransitLocation = invItemLocationHome.findByLocNameAndIiName(
                        transitLocationName, invItemName, AD_CMPNY);
                
	                double BST_COST = invBranchStockTransferLine.getBslQuantityReceived() * invBranchStockTransferLine.getBslUnitCost();
	                
	                LocalAdBranch adBranchFrom = invBranchStockTransfer.getAdBranch();
	                
	                double TRANSIT_COST = this.getBstInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(
	                		invItemName, transitLocationName, invBranchStockTransferLine.getInvUnitOfMeasure().getUomName(), 
	                		invBranchStockTransfer.getBstDate(), adBranchFrom.getBrName(), AD_CMPNY) *
	                		invBranchStockTransferLine.getBslQuantityReceived();
                
                double BST_QTY = this.convertByUomAndQuantityBst(
                        invBranchStockTransferLine.getInvUnitOfMeasure(), 
                        invBranchStockTransferLine.getInvItemLocation().getInvItem(), 
                        invBranchStockTransferLine.getBslQuantityReceived(), AD_CMPNY);
                Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost A01");
                
                LocalInvCosting invCosting = null;
                LocalInvCosting invTransitCosting = null;
                
                try {
                    invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                            invBranchStockTransfer.getBstDate(), invItemName, locName, AD_BRNCH, AD_CMPNY); 
                    Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost A02");
                } catch (FinderException ex) {
                    
                }

                LocalInvCosting invLastCosting = null;
                
                Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost B");
                if (invCosting == null) {
                    
                    this.postBstl(invBranchStockTransferLine, invItemLocationFrom, invBranchStockTransfer.getBstDate(), BST_QTY, BST_COST,
                    		BST_QTY, BST_COST, 0d, null, AD_BRNCH, AD_CMPNY);
                    Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost B01");
                } else if (invCosting != null) {
                	
                	invLastCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invBranchStockTransfer.getBstDate(), invItemName, locName, AD_BRNCH, AD_CMPNY);
                	Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost B02");
                	//compute cost variance   
					double CST_VRNC_VL = 0d;

					if(invLastCosting.getCstRemainingQuantity() < 0 && invBranchStockTransferLine.getBslQuantityReceived() > 0)
						CST_VRNC_VL = (invLastCosting.getCstRemainingQuantity() * (BST_COST/invBranchStockTransferLine.getBslQuantityReceived()) -
								invLastCosting.getCstRemainingValue());

	        		if(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
	        		{
	        			
	        			this.postBstl(invBranchStockTransferLine, invItemLocationFrom, invBranchStockTransfer.getBstDate(), BST_QTY,
	        					BST_COST, invLastCosting.getCstRemainingQuantity() + BST_QTY, 
	        					invLastCosting.getCstRemainingValue() + BST_COST, CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);
	        			Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost B03");
	        			
	        		}
	        		
	        		else if(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
	        		{
	        			
	        			LocalInvCosting invFifoCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
	        					invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate(), 
	        					invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
	        					invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(),
	        					AD_BRNCH, AD_CMPNY);
	        			
	        			double fifoCost = this.getInvFifoCost(invFifoCosting.getCstDate(), invFifoCosting.getInvItemLocation().getIlCode(), 
	        					BST_QTY, invBranchStockTransferLine.getBslUnitCost(), true, AD_BRNCH, AD_CMPNY);
	        			
	        			//post entries to database        			
	        			this.postBstl(invBranchStockTransferLine, invItemTransitLocation, invBranchStockTransfer.getBstDate(), 
	        					BST_QTY, fifoCost * BST_QTY, invFifoCosting.getCstRemainingQuantity() + BST_QTY, 
	        					invFifoCosting.getCstRemainingValue() + (fifoCost * BST_QTY), CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);


	        		}
	        		else if(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
	        		{
	        			
	        			LocalInvCosting invStandardCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
	        					invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate(), 
	        					invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
	        					invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(),
	        					AD_BRNCH, AD_CMPNY);
	        			
	        			double standardCost = invStandardCosting.getInvItemLocation().getInvItem().getIiUnitCost();
	        			
	        			//post entries to database        			
	        			this.postBstl(invBranchStockTransferLine, invItemTransitLocation, invBranchStockTransfer.getBstDate(), 
	        					BST_QTY, standardCost * BST_QTY, invStandardCosting.getCstRemainingQuantity() + BST_QTY, 
	        					invStandardCosting.getCstRemainingValue() + (standardCost * BST_QTY), CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);


	        		}
                	
                }
                Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost C");
                try {
                    invTransitCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                            invBranchStockTransfer.getBstDate(), invItemName, transitLocationName, BRNCH_FRM, AD_CMPNY);     
                    Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost C01");
                } catch (FinderException ex) {
                    
                }
                
                if (invTransitCosting == null) {
                    
                    this.postBstl(invBranchStockTransferLine, invItemTransitLocation, invBranchStockTransfer.getBstDate(), -BST_QTY,
                            -TRANSIT_COST, -BST_QTY, -TRANSIT_COST, 0d, null, BRNCH_FRM, AD_CMPNY);
                    Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost C02");
                } else if (invTransitCosting != null) {
                	
	        		if(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
	        		{
	        			
	        			invLastCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invBranchStockTransfer.getBstDate(), invItemName, transitLocationName, BRNCH_FRM, AD_CMPNY);
	        			
	        			this.postBstl(invBranchStockTransferLine, invItemTransitLocation, invBranchStockTransfer.getBstDate(), 
	        					-BST_QTY, -TRANSIT_COST, 
	        					invLastCosting.getCstRemainingQuantity() - BST_QTY, 
	        					invLastCosting.getCstRemainingValue() - TRANSIT_COST, 0d, null, BRNCH_FRM, AD_CMPNY);
	        			Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost C03");
	        			
	        		}
	        		else if(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
	        		{
	        			LocalInvCosting invFifoCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
	        					invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate(), 
	        					invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
	        					invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(),
	        					AD_BRNCH, AD_CMPNY);
	        			
	        			double fifoCost = this.getInvFifoCost(invFifoCosting.getCstDate(), invFifoCosting.getInvItemLocation().getIlCode(), 
	        					-BST_QTY, invBranchStockTransferLine.getBslUnitCost(), true, AD_BRNCH, AD_CMPNY);
	        			
	        			//post entries to database        			
	        			this.postBstl(invBranchStockTransferLine, invItemTransitLocation, invBranchStockTransfer.getBstDate(), 
	        					-BST_QTY, fifoCost * -BST_QTY, invFifoCosting.getCstRemainingQuantity() + -BST_QTY, 
	        					invFifoCosting.getCstRemainingValue() + (fifoCost * -BST_QTY), 0d, null, BRNCH_FRM, AD_CMPNY);
	        			
	        			System.out.println("2");
	        			System.out.println("CST Code : " + invFifoCosting.getCstCode());
	        			System.out.println("CST Remaining Qty : " + invFifoCosting.getCstRemainingQuantity());
	        			System.out.println("BST Qty : " + (-BST_QTY));
	        			System.out.println("CST Remaining Value : " + invFifoCosting.getCstRemainingValue());
	        			System.out.println("BST Value : " + (fifoCost + -BST_QTY));
	        			
	        		}
	        		else if(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
	        		{
	        			LocalInvCosting invStandardCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
	        					invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate(), 
	        					invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
	        					invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(),
	        					AD_BRNCH, AD_CMPNY);
	        			
	        			double standardCost = invStandardCosting.getInvItemLocation().getInvItem().getIiUnitCost();
	        			
	        			//post entries to database        			
	        			this.postBstl(invBranchStockTransferLine, invItemTransitLocation, invBranchStockTransfer.getBstDate(), 
	        					-BST_QTY, standardCost * -BST_QTY, invStandardCosting.getCstRemainingQuantity() + -BST_QTY, 
	        					invStandardCosting.getCstRemainingValue() + (standardCost * -BST_QTY), 0d, null, BRNCH_FRM, AD_CMPNY);
	        			
	        		}
                    
                }
                
            }
            
            // set branch stock transfer post status
            
            invBranchStockTransfer.setBstPosted(EJBCommon.TRUE);
            invBranchStockTransfer.setBstPostedBy(USR_NM);
            invBranchStockTransfer.setBstDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

            // post to GL
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            //  validate if date has no period and period is closed
            
            LocalGlSetOfBook glJournalSetOfBook = null;
            
            try {
                
                glJournalSetOfBook = glSetOfBookHome.findByDate(invBranchStockTransfer.getBstDate(), AD_CMPNY);
                
            } catch (FinderException ex) {
                
                throw new GlJREffectiveDateNoPeriodExistException();
                
            }
            
            LocalGlAccountingCalendarValue glAccountingCalendarValue = 
                glAccountingCalendarValueHome.findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invBranchStockTransfer.getBstDate(), AD_CMPNY);
            
            if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
                    glAccountingCalendarValue.getAcvStatus() == 'C' ||
                    glAccountingCalendarValue.getAcvStatus() == 'P') {
                
                throw new GlJREffectiveDatePeriodClosedException();
                
            }
            
            // check if debit and credit is balance
            
            LocalGlJournalLine glOffsetJournalLine = null;
            
            Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByBstCode(invBranchStockTransfer.getBstCode(), AD_CMPNY);
            
            i = invDistributionRecords.iterator();
            
            double TOTAL_DEBIT = 0d;
            double TOTAL_CREDIT = 0d;
            
            while (i.hasNext()) {
                
                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
                
                double DR_AMNT = 0d;
                
                DR_AMNT = invDistributionRecord.getDrAmount();
                
                if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
                    
                    TOTAL_DEBIT += DR_AMNT;
                    
                } else {
                    
                    TOTAL_CREDIT += DR_AMNT;
                    
                }
            }
            
            TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
            TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());    		    		
            Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost D");
            if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
                    TOTAL_DEBIT != TOTAL_CREDIT) {
                
                LocalGlSuspenseAccount glSuspenseAccount = null;
                
                try { 	
                    
                    glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "BRANCH STOCK TRANSFERS", AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                    throw new GlobalJournalNotBalanceException();
                    
                }
                
                if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
                    
                    glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
                            EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
                    
                } else {
                    
                    glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
                            EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
                    
                }
                
                LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
                //glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
                glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
                
                
            } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
                    TOTAL_DEBIT != TOTAL_CREDIT) {
                
                throw new GlobalJournalNotBalanceException();		    	
                
            }
            
            // create journal batch if necessary
            
            LocalGlJournalBatch glJournalBatch = null;
            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
            Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost E");
            try {
                
                glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " BRANCH STOCK TRANSFERS", AD_BRNCH, AD_CMPNY);
                
            } catch (FinderException ex) {
                
            }
            
            if (glJournalBatch == null) {
                
                glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " BRANCH STOCK TRANSFERS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
                
            }
            
            // create journal entry			            	
            
            LocalGlJournal glJournal = glJournalHome.create(invBranchStockTransfer.getBstNumber(),
                    invBranchStockTransfer.getBstDescription(), invBranchStockTransfer.getBstDate(),
                    0.0d, null, invBranchStockTransfer.getBstNumber(), null, 1d, "N/A", null,
                    'N', EJBCommon.TRUE, EJBCommon.FALSE, USR_NM, new Date(), USR_NM, new Date(), null, null,
                    USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, EJBCommon.FALSE,
                    null, 

                    AD_BRNCH, AD_CMPNY);
            
            LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
            glJournal.setGlJournalSource(glJournalSource);
            
            LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
            glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
            
            LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("BRANCH STOCK TRANSFERS", AD_CMPNY);
            glJournal.setGlJournalCategory(glJournalCategory);
            
            if (glJournalBatch != null) {

                glJournal.setGlJournalBatch(glJournalBatch);
                
            }           		    
            
            // create journal lines
            Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost E");
            i = invDistributionRecords.iterator();
            
            while (i.hasNext()) {
                
                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
                
                double DR_AMNT = 0d;
                
                DR_AMNT = invDistributionRecord.getDrAmount();
                
                LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
                        invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
                
                //invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
                glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());
                
                //glJournal.addGlJournalLine(glJournalLine);
                glJournalLine.setGlJournal(glJournal);
                
                invDistributionRecord.setDrImported(EJBCommon.TRUE);
                
            }
            Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost E");
            if (glOffsetJournalLine != null) {
                
                //glJournal.addGlJournalLine(glOffsetJournalLine);
                glOffsetJournalLine.setGlJournal(glJournal);
                
            }		
            
            // post journal to gl
            
            Collection glJournalLines = glJournal.getGlJournalLines();
            
            i = glJournalLines.iterator();
            
            while (i.hasNext()) {
                
                LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
                
                // post current to current acv
                
                this.postToGl(glAccountingCalendarValue,
                        glJournalLine.getGlChartOfAccount(),
                        true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
                Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost E01");
                // post to subsequent acvs (propagate)
                
                Collection glSubsequentAccountingCalendarValues = 
                    glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
                            glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                            glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
                
                Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
                
                while (acvsIter.hasNext()) {
                    
                    LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
                        (LocalGlAccountingCalendarValue)acvsIter.next();
                    
                    this.postToGl(glSubsequentAccountingCalendarValue,
                            glJournalLine.getGlChartOfAccount(),
                            false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
                    Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost E02");
                    
                }
                
                // post to subsequent years if necessary
                
                Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
                
                if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
                    
                    adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
                    LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY); 	  	
                    
                    Iterator sobIter = glSubsequentSetOfBooks.iterator();
                    
                    while (sobIter.hasNext()) {
                        
                        LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
                        
                        String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
                        
                        // post to subsequent acvs of subsequent set of book(propagate)
                        
                        Collection glAccountingCalendarValues = 
                            glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
                        
                        Iterator acvIter = glAccountingCalendarValues.iterator();
                        
                        while (acvIter.hasNext()) {
                            
                            LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
                                (LocalGlAccountingCalendarValue)acvIter.next();
                            
                            if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
                                    ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
                                
                                this.postToGl(glSubsequentAccountingCalendarValue,
                                        glJournalLine.getGlChartOfAccount(),
                                        false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
                                
                            } else { 
                                // revenue & expense
                                
                                this.postToGl(glSubsequentAccountingCalendarValue,
                                        glRetainedEarningsAccount,
                                        false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
                                
                            }
                        }
                        
                        if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
                        
                    }
                }
            }	
            Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstPost E Done");
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlJREffectiveDatePeriodClosedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalJournalNotBalanceException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalRecordAlreadyDeletedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyPostedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
        	
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    
    }
    
    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, 
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {
	  	 
		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;
	       
	     // Initialize EJB Home
	        
	     try {
	         
	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	     } 
	     catch (NamingException ex) {
	            
	    	 throw new EJBException(ex.getMessage());
	     }
	    	
		try {
			
			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
			
			if (invFifoCostings.size() > 0) {
				
				Iterator x = invFifoCostings.iterator();
			
	  			if (isAdjustFifo) {
	  				
	  				//executed during POST transaction
	  				
	  				double totalCost = 0d;
	  				double cost;
	  				
	  				if(CST_QTY < 0) {
	  					
	  					//for negative quantities
	 	  				double neededQty = -(CST_QTY);
	 	  				
	 	  				while(x.hasNext() && neededQty != 0) {
	 	 	  				
	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	
	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {  				
	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();  				
	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {  				
	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
	 		 	  			} else {
	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
	 		 	  			}
	
	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {
	 	  						
	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
	 			  				totalCost += (neededQty * cost);
	 			  				neededQty = 0d;	 			  				 
	 	  					} else {
	 	  						
	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
	 	  					}
	 	  				}
	 	  				
	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
	 	  				if(neededQty != 0) {
	 	  					
	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
	 	  				}
	 	  				
	 	  				cost = totalCost / -CST_QTY;
	  				} 
	  				
	  				else {
	  					
	  					//for positive quantities
	  					cost = CST_COST;
	  				}
	  				return cost;
	  			}
	  			
	  			else {
	  				
	  				//executed during ENTRY transaction
	  				
	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	  				
	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			}
	  			}
			} 
			else {
				
				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}
				
		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}     

    
    
    private void post(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY, double CST_ADJST_CST,
    		double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    
    	Debug.print("InvApprovalControllerBean post");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	          lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              
            
        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
           int CST_LN_NMBR = 0;
           
           CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
                      
           if (CST_ADJST_QTY < 0) {
                      
           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
           
           }

           try {
           
           	   // generate line number
           
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
           
           } catch (FinderException ex) {
           
           	   CST_LN_NMBR = 1;
           
           }

           if (CST_ADJST_QTY != 0){
           	
            //void subsequent cost variance adjustments
            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
            Iterator i = invAdjustmentLines.iterator();
            
            while (i.hasNext()){
            	
            	LocalInvAdjustmentLine invAdjustmentLineTemp = (LocalInvAdjustmentLine) i.next();
            	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
            	
            }
           	
           }
           
           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           System.out.println("CST_ADJST_QTY: " + CST_ADJST_QTY);
           try {           
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   System.out.println(prevCst.getCstCode()+"   "+ prevCst.getCstRemainingQuantity());
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();
        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }
        	   System.out.println("prevExpiryDates: " + prevExpiryDates);
        	   System.out.println("qtyPrpgt: " + qtyPrpgt);
           }catch (Exception ex){
        	   System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
        	   prevExpiryDates="";
           }
           
           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
           invItemLocation.addInvCosting(invCosting);
           invCosting.setCstQCNumber(invAdjustmentLine.getAlQcNumber());
           invCosting.setCstQCExpiryDate(invAdjustmentLine.getAlQcExpiryDate());
           
           invCosting.setInvAdjustmentLine(invAdjustmentLine);
           
//         Get Latest Expiry Dates           
           if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
               if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){

     			   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
     				   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
     				   
     				   String miscList2Prpgt = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), qty2Prpgt, "False");
     				   ArrayList miscList = this.expiryDates(invAdjustmentLine.getAlMisc(), qty2Prpgt);
     				   String propagateMiscPrpgt = "";
     				   String ret = "";
     				  String exp = "";
     				   String Checker = "";
     				  
     				  //ArrayList miscList2 = null;
     				   if(CST_ADJST_QTY>0){
     					  prevExpiryDates = prevExpiryDates.substring(1);
     					   propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
     				   }else{
     					   Iterator mi = miscList.iterator();
     					   
     					   propagateMiscPrpgt = prevExpiryDates;
     					  ret = propagateMiscPrpgt;
     					  while(mi.hasNext()){
     						  String miscStr = (String)mi.next();
     						  
     						 Integer qTest = this.checkExpiryDates(ret+"fin$");
     						 ArrayList miscList2 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));
                 			
     						  //ArrayList miscList2 = this.expiryDates("$" + ret, qtyPrpgt);
     						  Iterator m2 = miscList2.iterator();
     						  ret = "";
     						  String ret2 = "false";
     						  int a = 0;
     						  while(m2.hasNext()){
     							  String miscStr2 = (String)m2.next();

     							  if(ret2=="1st"){
     								 ret2 = "false";
     							  }
     							  System.out.println("miscStr: "+miscStr);
     							 System.out.println("miscStr2: "+miscStr2);
    							  
     							  if(miscStr2.equals(miscStr)){
     								  if(a==0){
     									  a = 1;
     									  ret2 = "1st";
     									  Checker = "true";
     								  }else{
     									  a = a+1;
     									  ret2 = "true";
     								  }
     							  }
     							 System.out.println("Checker: "+Checker);
     							System.out.println("ret2: "+ret2);
     							  if(!miscStr2.equals(miscStr) || a>1){
     								  if((ret2!="1st")&&((ret2=="false")||(ret2=="true"))){
     									  if (miscStr2!=""){
     										  miscStr2 = "$" + miscStr2;
     										  ret = ret + miscStr2;
     										  System.out.println("ret " + ret);
     										  ret2 = "false";
     									  }
     								  }
     							  }
     							  
     						  }
     						  if(ret!=""){
     							 ret = ret + "$";
     						  }
     						 System.out.println("ret una: "+ret);
     						exp = exp + ret;
     						  qtyPrpgt= qtyPrpgt -1;
     					  }
     					  System.out.println("ret fin " + ret);
     					 System.out.println("exp fin " + exp);
     					   //propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
     						propagateMiscPrpgt = ret;
     					   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
     					  if(Checker=="true"){
     	 					  //invCosting.setCstExpiryDate(propagateMiscPrpgt);
     	 				   }else{
     	 					   System.out.println("Exp Not Found");
     	 					   throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
     	 				   }
     				   }
     				  invCosting.setCstExpiryDate(propagateMiscPrpgt);
     				   
     			   }else{
     				   invCosting.setCstExpiryDate(prevExpiryDates);
     			   }
     			   
                }else{
                	System.out.println("invAdjustmentLine ETO NA: "+ invAdjustmentLine.getAlAdjustQuantity());
                	if(invAdjustmentLine.getAlAdjustQuantity()>0){
                		if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
                   		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
               			   String initialPrpgt = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), initialQty, "False");
               			   
                       	   invCosting.setCstExpiryDate(initialPrpgt);
                   	   }else{
                   		   invCosting.setCstExpiryDate(prevExpiryDates);
                   	   }
                	}else{                    	
                		//throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
                	}
                	 
                }
           }

    		
           
			// if cost variance is not 0, generate cost variance for the transaction 
			if(CST_VRNC_VL != 0) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"INVADJ" + invAdjustmentLine.getInvAdjustment().getAdjDocumentNumber(),
						invAdjustmentLine.getInvAdjustment().getAdjDescription(),
						invAdjustmentLine.getInvAdjustment().getAdjDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				
			}

           // propagate balance if necessary           
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
           String miscList  ="";
           ArrayList miscList2 = null;
           Iterator i = invCostings.iterator();


           System.out.println("miscList Propagate:" + miscList);
           String propagateMisc ="";
		   String ret = "";
		   
           
           while (i.hasNext()) {
        	   String Checker = "";
        	   String Checker2 = "";
        	   
               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
               
               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
           
               if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
            	   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
            		   double qty = Double.parseDouble(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
            		   //invPropagatedCosting.getInvAdjustmentLine().getAlMisc();
            		   miscList = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), qty, "False");
            		   miscList2 = this.expiryDates(invAdjustmentLine.getAlMisc(), qty);
            		   System.out.println("invAdjustmentLine.getAlMisc(): "+invAdjustmentLine.getAlMisc());
            		   System.out.println("getAlAdjustQuantity(): "+invAdjustmentLine.getAlAdjustQuantity());

            		   if(invAdjustmentLine.getAlAdjustQuantity()<0){
            			   Iterator mi = miscList2.iterator();

            			   propagateMisc = invPropagatedCosting.getCstExpiryDate();
            			   ret = invPropagatedCosting.getCstExpiryDate();
            			   while(mi.hasNext()){
            				   String miscStr = (String)mi.next();

            				   Integer qTest = this.checkExpiryDates(ret+"fin$");
            				   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

            				   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
            				   System.out.println("ret: " + ret);
            				   Iterator m2 = miscList3.iterator();
            				   ret = "";
            				   String ret2 = "false";
            				   int a = 0;
            				   while(m2.hasNext()){
            					   String miscStr2 = (String)m2.next();

            					   if(ret2=="1st"){
            						   ret2 = "false";
            					   }
            					   System.out.println("2 miscStr: "+miscStr);
            					   System.out.println("2 miscStr2: "+miscStr2);
            					   if(miscStr2.equals(miscStr)){
            						   if(a==0){
            							   a = 1;
            							   ret2 = "1st";
            							   Checker2 = "true";
            						   }else{
            							   a = a+1;
            							   ret2 = "true";
            						   }
            					   }
            					   System.out.println("Checker: "+Checker2);
            					   if(!miscStr2.equals(miscStr) || a>1){
            						   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
            							   if (miscStr2!=""){
            								   miscStr2 = "$" + miscStr2;
            								   ret = ret + miscStr2;
            								   ret2 = "false";
            							   }
            						   }
            					   }

            				   }
            				   if(Checker2!="true"){
            					   throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
            				   }else{
            					   System.out.println("TAE");
            				   }

            				   ret = ret + "$";
            				   qtyPrpgt= qtyPrpgt -1;
            			   }
            		   }

            	   }

            	   System.out.println("getAlAdjustQuantity(): "+invAdjustmentLine.getAlAdjustQuantity());
            	   if(invAdjustmentLine.getAlAdjustQuantity()<0){

            		   Iterator mi = miscList2.iterator();

            		   propagateMisc = invPropagatedCosting.getCstExpiryDate();
            		   ret = propagateMisc;
            		   while(mi.hasNext()){
            			   String miscStr = (String)mi.next();

            			   Integer qTest = this.checkExpiryDates(ret+"fin$");
            			   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

            			   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
            			   System.out.println("ret: " + ret);
            			   Iterator m2 = miscList3.iterator();
            			   ret = "";
            			   String ret2 = "false";
            			   int a = 0;
            			   while(m2.hasNext()){
            				   String miscStr2 = (String)m2.next();

            				   if(ret2=="1st"){
            					   ret2 = "false";
            				   }
            				   System.out.println("2 miscStr: "+miscStr);
            				   System.out.println("2 miscStr2: "+miscStr2);
            				   if(miscStr2.equals(miscStr)){
            					   if(a==0){
            						   a = 1;
            						   ret2 = "1st";
            						   Checker = "true";
            					   }else{
            						   a = a+1;
            						   ret2 = "true";
            					   }
            				   }
            				   System.out.println("Checker: "+Checker);
            				   if(!miscStr2.equals(miscStr) || a>1){
            					   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
            						   if (miscStr2!=""){
            							   miscStr2 = "$" + miscStr2;
            							   ret = ret + miscStr2;
            							   ret2 = "false";
            						   }
            					   }
            				   }

            			   }
            			   ret = ret + "$";
            			   qtyPrpgt= qtyPrpgt -1;
            		   }
            		   propagateMisc = ret;
            	   }else{
            		   propagateMisc = miscList+invPropagatedCosting.getCstExpiryDate().substring(1, invPropagatedCosting.getCstExpiryDate().length());
            	   }

            	   invPropagatedCosting.setCstExpiryDate(propagateMisc);
               }
           }                           
           

           // regenerate cost varaince
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }
    
    	
    
    }
    
    public static int checkExpiryDates(String misc) throws Exception {

		String separator ="$";

		// Remove first $ character
		misc = misc.substring(1);
		//System.out.println("misc: " + misc);
		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;	
		int numberExpry=0;
		String miscList = new String();
		String miscList2 = "";
		String g= "";
		try{
			while(g!="fin"){
				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				g= misc.substring(start, start + length);
				if(g.length()!=0){
					if(g!=null || g!="" || g!="null"){
						if(g.contains("null")){
							miscList2 = "Error";
						}else{
							miscList = miscList + "$" + g;
							numberExpry++;
						}
					}else{
						miscList2 = "Error";
					}

				}else{
					miscList2 = "Error";
				}
			}	
		}catch(Exception e){

		}

		if(miscList2==""){
			miscList = miscList+"$";
		}else{
			miscList = miscList2;
		}

		return (numberExpry);
	}
        
    public String getQuantityExpiryDates(String qntty){
    	String separator = "$";

    	// Remove first $ character
    	qntty = qntty.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = qntty.indexOf(separator, start);
    	int length = nextIndex - start;	
    	String y;
    	y = (qntty.substring(start, start + length));
    	System.out.println("Y " + y);
    	
    	return y;
    }
    
    private ArrayList expiryDates(String misc, double qty) throws Exception{
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	String separator ="$";
    	

    	// Remove first $ character
    	misc = misc.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	

    	System.out.println("qty" + qty);
    	ArrayList miscList = new ArrayList();
		
    	for(int x=0; x<qty; x++) {

    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
      	
    		String checker = misc.substring(start, start + length);
    		if(checker.length()!=0 || checker!="null"){
    			miscList.add(checker);
    		}else{
    			miscList.add("null");
    			qty++;
    		}
    	}	
		
		System.out.println("miscList :" + miscList);
		return miscList;
    }
    
    public String propagateExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	
    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}
 
    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	
    	
    	String miscList = new String();
		
		for(int x=0; x<qty; x++) {
			
			// Date
			start = nextIndex + 1;
			nextIndex = misc.indexOf(separator, start);
			length = nextIndex - start;
			String g= misc.substring(start, start + length);
			System.out.println("g: " + g);
			System.out.println("g length: " + g.length());
				if(g.length()!=0){
					miscList = miscList + "$" + g;	
					System.out.println("miscList G: " + miscList);
				}
		}	
		
		miscList = miscList+"$";
		System.out.println("miscList :" + miscList);
		return (miscList);
    }
    
    private void post(LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine, Date CST_DT, double CST_ASSMBLY_QTY,
    		double CST_ASSMBLY_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, String II_NM, String LOC_NM, double CST_VRNC_VL,
			String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
        
    	Debug.print("InvApprovalControllerBean post");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
          invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	          lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              
            
        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           
           LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);
           int CST_LN_NMBR = 0;
           
           CST_ASSMBLY_QTY = EJBCommon.roundIt(CST_ASSMBLY_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ASSMBLY_CST = EJBCommon.roundIt(CST_ASSMBLY_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
                      
           if (CST_ASSMBLY_QTY < 0) {
                      
           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ASSMBLY_QTY));
           
           }

           try {
           
           	   // generate line number
           
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
           
           } catch (FinderException ex) {
           
           	   CST_LN_NMBR = 1;
           
           }

           //void subsequent cost variance adjustments
           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
           Iterator i = invAdjustmentLines.iterator();
           
           while (i.hasNext()){
           	
           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
           	
           }

           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           try {           
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();
        	   System.out.println("prevExpiryDates");
        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }

           }catch (Exception ex){
        	   
           }
           
           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, CST_ASSMBLY_QTY, CST_ASSMBLY_CST, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ASSMBLY_QTY > 0 ? CST_ASSMBLY_QTY : 0, AD_BRNCH, AD_CMPNY);
           invItemLocation.addInvCosting(invCosting);
           invCosting.setInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
           
           if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
 			   System.out.println("apPurchaseOrderLine.getPlMisc(): "+invBuildUnbuildAssemblyLine.getBlMisc().length());

 			   if(invBuildUnbuildAssemblyLine.getBlMisc()!=null && invBuildUnbuildAssemblyLine.getBlMisc()!="" && invBuildUnbuildAssemblyLine.getBlMisc().length()!=0){
 				   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc()));
 				   
 				   String miscList2Prpgt = this.propagateExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc(), qty2Prpgt, "False");
 				   ArrayList miscList = this.expiryDates(invBuildUnbuildAssemblyLine.getBlMisc(), qty2Prpgt);
 				   String propagateMiscPrpgt = "";
 				   String ret = "";
 				   String Checker = "";
				   
 				  //ArrayList miscList2 = null;
 				   if(CST_ASSMBLY_QTY>0){
 					  prevExpiryDates = prevExpiryDates.substring(1);
 					   propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
 				   }else{
 					   Iterator mi = miscList.iterator();
 					   
 					   propagateMiscPrpgt = prevExpiryDates;
 					  ret = propagateMiscPrpgt;
 					  while(mi.hasNext()){
 						  String miscStr = (String)mi.next();
 						  
 						  ArrayList miscList2 = this.expiryDates("$" + ret, qtyPrpgt);
 						  Iterator m2 = miscList2.iterator();
 						  ret = "";
 						  String ret2 = "false";
 						  int a = 0;
 						  while(m2.hasNext()){
 							  String miscStr2 = (String)m2.next();

 							  if(ret2=="1st"){
 								 ret2 = "false";
 							  }
 							  
 							  if(miscStr2.equals(miscStr)){
 								  if(a==0){
 									  a = 1;
 									  ret2 = "1st";
 									  Checker = "true";
 								  }else{
 									  a = a+1;
 									  ret2 = "true";
 								  }
 							  }

 							  if(!miscStr2.equals(miscStr) || a>1){
 								  if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
 									  if (miscStr2!=""){
 										  miscStr2 = "$" + miscStr2;
 										  ret = ret + miscStr2;
 										  ret2 = "false";
 									  }
 								  }
 							  }
 							  
 						  }
 						  ret = ret + "$";
 						  qtyPrpgt= qtyPrpgt -1;
 					  }
 					   //propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
 					  propagateMiscPrpgt = ret;
 					   System.out.println("propagateMiscPrpgt: " + propagateMiscPrpgt);
 					   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
 				   }
 				   if(Checker=="true"){
 					  invCosting.setCstExpiryDate(propagateMiscPrpgt);
 				   }else{
 					   throw new GlobalExpiryDateNotFoundException();
 				   }
 				   
 			   }else{
 				   invCosting.setCstExpiryDate(prevExpiryDates);
 				   System.out.println("prevExpiryDates");
 			   }
 			   
            }else{
            	if(invBuildUnbuildAssemblyLine.getBlMisc()!=null && invBuildUnbuildAssemblyLine.getBlMisc()!="" && invBuildUnbuildAssemblyLine.getBlMisc().length()!=0){
           		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc()));
       			   String initialPrpgt = this.propagateExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc(), initialQty, "False");
       			   
               	   invCosting.setCstExpiryDate(initialPrpgt);
           	   }else{
           		   invCosting.setCstExpiryDate(prevExpiryDates);
           	   }
            }
           
			// if cost variance is not 0, generate cost variance for the transaction 
			if(CST_VRNC_VL != 0) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"INVBUA" + invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDocumentNumber(),
						invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDescription(),
						invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				
			}

           // propagate balance if necessary           
                      
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
           
           i = invCostings.iterator();
           String miscList = "";
           ArrayList miscList2 = null;
           if(invBuildUnbuildAssemblyLine.getBlMisc()!=null && invBuildUnbuildAssemblyLine.getBlMisc()!="" && invBuildUnbuildAssemblyLine.getBlMisc().length()!=0){
        	   double qty = Double.parseDouble(this.getQuantityExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc()));
               miscList = this.propagateExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc(), qty, "False");
               System.out.println("miscList Propagate:" + miscList);
               miscList2 = this.expiryDates(invBuildUnbuildAssemblyLine.getBlMisc(), qty);
           }
           String propagateMisc ="";
		   String ret = "";
		   String Checker = "";
           
           while (i.hasNext()) {
           
               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
               
               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ASSMBLY_QTY);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ASSMBLY_CST);
           
               System.out.println("invPropagatedCosting.getCstExpiryDate() : " + invPropagatedCosting.getCstExpiryDate());
			   if(invBuildUnbuildAssemblyLine.getBlMisc()!=null && invBuildUnbuildAssemblyLine.getBlMisc()!="" && invBuildUnbuildAssemblyLine.getBlMisc().length()!=0){
				   if(CST_ASSMBLY_QTY>0){
					   miscList = miscList.substring(1);
					   propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
					   System.out.println("propagateMiscPrpgt : "+propagateMisc);
				   }else{
					   Iterator mi = miscList2.iterator();

					   propagateMisc = prevExpiryDates;
					   ret = propagateMisc;
					   while(mi.hasNext()){
						   String miscStr = (String)mi.next();
						   ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
						   Iterator m2 = miscList3.iterator();
						   ret = "";
						   String ret2 = "false";
						   int a = 0;
						   while(m2.hasNext()){
							   String miscStr2 = (String)m2.next();

							   if(ret2=="1st"){
								   ret2 = "false";
							   }

							   if(miscStr2.equals(miscStr)){
								   if(a==0){
									   a = 1;
									   ret2 = "1st";
									   Checker = "true";
								   }else{
									   a = a+1;
									   ret2 = "true";
								   }
							   }

							   if(!miscStr2.equals(miscStr) || a>1){
								   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
									   if (miscStr2!=""){
										   miscStr2 = "$" + miscStr2;
										   ret = ret + miscStr2;
										   ret2 = "false";
									   }
								   }
							   }

						   }
						   ret = ret + "$";
						   qtyPrpgt= qtyPrpgt -1;
					   }
					   propagateMisc = ret;
					   System.out.println("propagateMiscPrpgt: " + propagateMisc);
				   }

				   if(Checker=="true"){
					   
				   }else{
					   throw new GlobalExpiryDateNotFoundException();
				   }
				   
				   invPropagatedCosting.setCstExpiryDate(propagateMisc);

			   }else{
				   invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
				   System.out.println("prevExpiryDates");
			   }
			   
           }                           
           

           // regenerate cost varaince
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }
    
    	
    
    }
    
    private void post(LocalInvStockTransferLine invStockTransferLine,  LocalInvItemLocation invItemLocation, Date CST_DT,
    		double CST_ST_QTY, double CST_ST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM,
			Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
        
    	Debug.print("InvApprovalControllerBean post");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	          lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
          
        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);           
           int CST_LN_NMBR = 0;
           
           CST_ST_QTY = EJBCommon.roundIt(CST_ST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ST_CST = EJBCommon.roundIt(CST_ST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
                      
           if (CST_ST_QTY < 0) {
                      
           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ST_QTY));
           
           }

           try {
           
           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
           
           } catch (FinderException ex) {
           
           	   CST_LN_NMBR = 1;
           
           }

           //void subsequent cost variance adjustments
           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
           Iterator i = invAdjustmentLines.iterator();
           
           while (i.hasNext()){
           	
           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
           	
           }

           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           try {           
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();

        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }

           }catch (Exception ex){
        	   
           }
           
           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, CST_ST_QTY, CST_ST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ST_QTY > 0 ? CST_ST_QTY : 0, AD_BRNCH, AD_CMPNY);
           invItemLocation.addInvCosting(invCosting);
           invCosting.setInvStockTransferLine(invStockTransferLine);
           
           if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
        	   System.out.println("apPurchaseOrderLine.getPlMisc(): "+invStockTransferLine.getStlMisc().length());
        	   
        	   if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
        		   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invStockTransferLine.getStlMisc()));
        		   
        		   String miscList2Prpgt = this.propagateExpiryDates(invStockTransferLine.getStlMisc(), qty2Prpgt, "False");
        		   ArrayList miscList = this.expiryDates(invStockTransferLine.getStlMisc(), qty2Prpgt);
        		   String propagateMiscPrpgt = "";
        		   String ret = "";
        		   String check="";
        		   System.out.println("CST_ST_QTY Before Trans: " + CST_ST_QTY);
        		   //ArrayList miscList2 = null;
        		   if(CST_ST_QTY>0){
        			   prevExpiryDates = prevExpiryDates.substring(1);
        			   propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
        		   }else{
        			   Iterator mi = miscList.iterator();
        			   propagateMiscPrpgt = prevExpiryDates;
        			   ret = propagateMiscPrpgt;
        			   String Checker = "";
        			   while(mi.hasNext()){
        				   String miscStr = (String)mi.next();
        				   
        				   ArrayList miscList2 = this.expiryDates("$"+ret, qtyPrpgt);
        				   Iterator m2 = miscList2.iterator();
        				   ret = "";
        				   String ret2 = "false";
        				   int a = 0;
        				   
        				   while(m2.hasNext()){
        					   String miscStr2 = (String)m2.next();
        					   
        					   if(ret2=="1st"){
        						   ret2 = "false";
        					   }
        					   
        					   if(miscStr2.equals(miscStr)){
        						   if(a==0){
        							   a = 1;
        							   ret2 = "1st";
        							   Checker = "true";
        						   }else{
        							   a = a+1;
        							   ret2 = "true";
        						   }
        					   }
        					   
        					   if(!miscStr2.equals(miscStr) || a>1){
        						   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
        							   if (miscStr2!=""){
        								   miscStr2 = "$" + miscStr2;
        								   ret = ret + miscStr2;
        								   ret2 = "false";
        							   }
        						   }
        					   }
        					   
        				   }
        				   ret = ret + "$";
        				   qtyPrpgt= qtyPrpgt -1;
        			   }
        			   //propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
        			   propagateMiscPrpgt = ret;
        			   System.out.println("propagateMiscPrpgt: " + propagateMiscPrpgt);
        			   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
        			   if(Checker=="true"){
        				   //invCosting.setCstExpiryDate(ret);
        				   System.out.println("check: " + check);
        			   }else{
        				   throw new GlobalExpiryDateNotFoundException();
        			   }
        			   
        		   }
        		   invCosting.setCstExpiryDate(propagateMiscPrpgt);
        		   
        	   }else{
        		   invCosting.setCstExpiryDate(prevExpiryDates);
        		   System.out.println("prevExpiryDates");
        	   }
           }else{
        	   if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
        		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(invStockTransferLine.getStlMisc()));
     			   String initialPrpgt = this.propagateExpiryDates(invStockTransferLine.getStlMisc(), initialQty, "False");
     			   
             	   invCosting.setCstExpiryDate(initialPrpgt);
        	   }else{
        		   invCosting.setCstExpiryDate(prevExpiryDates);
 				   System.out.println("prevExpiryDates");
        	   }
         	   
            }
           
			// if cost variance is not 0, generate cost variance for the transaction 
			if(CST_VRNC_VL != 0) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"INVST" + invStockTransferLine.getInvStockTransfer().getStDocumentNumber(),
						invStockTransferLine.getInvStockTransfer().getStDescription(),
						invStockTransferLine.getInvStockTransfer().getStDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				
			}

           // propagate balance if necessary           
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
           
           System.out.println("IL_CODE : " + invItemLocation.getIlCode());
           System.out.println("CST_DT : " + CST_DT);
           System.out.println("II_NM : " + invItemLocation.getInvItem().getIiName());
           System.out.println("LOC_NM : " + invItemLocation.getInvLocation().getLocName());
           System.out.println("(3853) size : " + invCostings.size());
           
           i = invCostings.iterator();
           
           String miscList ="";
           ArrayList miscList2 = null;
           if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
        	   double qty = Double.parseDouble(this.getQuantityExpiryDates(invStockTransferLine.getStlMisc()));
               miscList = this.propagateExpiryDates(invStockTransferLine.getStlMisc(), qty, "False");
               miscList2 = this.expiryDates(invStockTransferLine.getStlMisc(), qty);
           }
           
           System.out.println("miscList Propagate:" + miscList);
           String propagateMisc ="";
           String ret = "";
           String Checker = "";

           
           while (i.hasNext()) {
           
               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
               
               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ST_QTY);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ST_CST);
               
               if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
            	   if(CST_ST_QTY>0){
                	   miscList = miscList.substring(1);
                	   propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
                	   System.out.println("propagateMiscPrpgt : "+propagateMisc);
                   }else{
                	   Iterator mi = miscList2.iterator();

                	   propagateMisc = prevExpiryDates;
                	   ret = propagateMisc;
                	   while(mi.hasNext()){
                		   String miscStr = (String)mi.next();
                		   ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
                		   Iterator m2 = miscList3.iterator();
                		   ret = "";
                		   String ret2 = "false";
                		   int a = 0;
                		   while(m2.hasNext()){
                			   String miscStr2 = (String)m2.next();

                			   if(ret2=="1st"){
                				   ret2 = "false";
                			   }

                			   if(miscStr2.equals(miscStr)){
                				   if(a==0){
                					   a = 1;
                					   ret2 = "1st";
                					   Checker = "true";
                				   }else{
                					   a = a+1;
                					   ret2 = "true";
                				   }
                			   }

                			   if(!miscStr2.equals(miscStr) || a>1){
                				   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
                					   if (miscStr2!=""){
                						   miscStr2 = "$" + miscStr2;
                						   ret = ret + miscStr2;
                						   ret2 = "false";
                					   }
                				   }
                			   }

                		   }
                		   ret = ret + "$";
                		   qtyPrpgt= qtyPrpgt -1;
                	   }
                	   propagateMisc = ret;
                	   System.out.println("propagateMiscPrpgt: " + propagateMisc);

                	   if(Checker=="true"){
                		   //invPropagatedCosting.setCstExpiryDate(propagateMisc);
                	   }else{
                		   throw new GlobalExpiryDateNotFoundException();
                	   }
               }

                  	invPropagatedCosting.setCstExpiryDate(propagateMisc);
           	}else{
           		invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
				   System.out.println("prevExpiryDates");
    	   }

           
           }                           

           // regenerate cost varaince
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }
    
    	
    
    }
    
    private void postBstl(LocalInvBranchStockTransferLine invBranchStockTransferLine, LocalInvItemLocation invItemLocation, Date CST_DT,
    		double CST_ST_QTY, double CST_ST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM,
			Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("InvBranchStockTransferInEntryControllerBean post");
        
        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
            invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	      	lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);           
            int CST_LN_NMBR = 0;
            
            CST_ST_QTY = EJBCommon.roundIt(CST_ST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_ST_CST = EJBCommon.roundIt(CST_ST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
            CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
            
            if (CST_ST_QTY < 0) {
                
                invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ST_QTY));
                
            }

            try {
                
                // generate line number
                
                LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
                
            } catch (FinderException ex) {
                
                CST_LN_NMBR = 1;
                
            }

            //void subsequent cost variance adjustments
            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
            Iterator i = invAdjustmentLines.iterator();
            
            while (i.hasNext()){
            	
            	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
            	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
            	
            }

            String prevExpiryDates = "";
            String miscListPrpgt ="";
            double qtyPrpgt = 0;
            double qtyPrpgt2 = 0;
            try {           
         	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
         			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
         	   System.out.println(prevCst.getCstCode());
         	   System.out.println("ITEM "+ prevCst.getInvItemLocation().getInvItem().getIiName() +" "+ prevCst.getCstExpiryDate());
         	   
         	   prevExpiryDates = prevCst.getCstExpiryDate();
         	  
         	   qtyPrpgt = prevCst.getCstRemainingQuantity();

         	   if (prevExpiryDates==null){
         		   prevExpiryDates="";
         	   }

            }catch (Exception ex){
         	   
            }
            
            // create costing
            LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, CST_ST_QTY, CST_ST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ST_QTY > 0 ? CST_ST_QTY : 0, AD_BRNCH, AD_CMPNY);
            //invItemLocation.addInvCosting(invCosting);
            invCosting.setInvItemLocation(invItemLocation);
            invCosting.setInvBranchStockTransferLine(invBranchStockTransferLine);
            
            // Get Latest Expiry Dates
   			   
			// if cost variance is not 0, generate cost variance for the transaction 
			if(CST_VRNC_VL != 0) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"INVBST" + invBranchStockTransferLine.getInvBranchStockTransfer().getBstNumber(),
						invBranchStockTransferLine.getInvBranchStockTransfer().getBstDescription(),
						invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				
			}

            // propagate balance if necessary           
            Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            i = invCostings.iterator();

            String miscList = "";
            ArrayList miscList2 = null;
            if(invBranchStockTransferLine.getBslMisc()!=null && invBranchStockTransferLine.getBslMisc()!="" && invBranchStockTransferLine.getBslMisc().length()!=0){
            	double qty = Double.parseDouble(this.getQuantityExpiryDates(invBranchStockTransferLine.getBslMisc()));
                miscList = this.propagateExpiryDates(invBranchStockTransferLine.getBslMisc(), qty, "False");
                miscList2 = this.expiryDates(invBranchStockTransferLine.getBslMisc(), qty);
            }
           
            System.out.println("miscList Propagate:" + miscList);
            String propagateMisc ="";
 		   	String ret = "";
 		   String Checker = "";
            
            while (i.hasNext()) {

            	LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

            	invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ST_QTY);
            	invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ST_QTY);

            }
                      

            // regenerate cost varaince
            this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

            
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
        
        
    }

   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
      LocalGlChartOfAccount glChartOfAccount, 
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
      	
      Debug.print("InvApprovalControllerBean postToGl");
      
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
              
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {          
               
               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
       	   
	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance = 
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
	               	  
	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
	           
	           
	               	  
	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {				    
				    			
 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
				    
		
			  } else {
	
				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
	
		 	  }
		 	  
		 	  if (isCurrentAcv) { 
		 	 
			 	 if (isDebit == EJBCommon.TRUE) {
			 	 	
		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 				
		 		 } else {
		 		 	
		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 		 }       	   
		 		 
		 	}
       	
       } catch (Exception ex) {
       	
       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());
       	
       }
      
      
    }
    
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
			
		Debug.print("InvApprovalControllerBean convertByUomFromAndItemAndQuantity");		        
        
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
                
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
        	        	
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
                        
        	return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
        						    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}	
        
    private void regenerateInventoryDr(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY) 
    throws GlobalInventoryDateException,
	GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvApprovalControllerBean regenerateInventoryDr");		        
    	
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalInvItemHome invItemHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	 InvRepItemCostingControllerHome homeRIC = null;
        InvRepItemCostingController ejbRIC = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class); 
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class); 
    		invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);    
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		homeRIC = (InvRepItemCostingControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
        try {
          	ejbRIC = homeRIC.create();
  		} catch (RemoteException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (CreateException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
      	
    	
    	try {
    		
    		// regenerate inventory distribution records

            // remove all inventory distribution
            
    		Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByAdjCode(
    				invAdjustment.getAdjCode(), AD_CMPNY);
    		
    		Iterator i = invDistributionRecords.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    			i.remove();
    			invDistributionRecord.remove();
    			
    		}
    		
        	// remove all adjustment lines committed qty
        	
    		Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
        	
        	i = invAdjustmentLines.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
        		
        		if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
        		
        		    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
        		
        			invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);
        			
        		}
        		
        	}
    		
    		invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    		
    		if(invAdjustmentLines != null && !invAdjustmentLines.isEmpty()) {
    			
    			byte DEBIT = 0;
    			double TOTAL_AMOUNT = 0d; 
    			
    			i = invAdjustmentLines.iterator();
    			
    			while(i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
    				LocalInvItemLocation invItemLocation=invAdjustmentLine.getInvItemLocation();
    				
    				// start date validation
    				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    				if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	    				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	    						invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	    				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
    				}	
    				// add physical inventory distribution
    				
    				double AMOUNT = 0d;
    				
    				if (invAdjustmentLine.getAlAdjustQuantity() > 0 && !invAdjustmentLine.getInvAdjustment().getAdjType().equals("ISSUANCE")) {
    					
    					AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() *
    							invAdjustmentLine.getAlUnitCost(), adPreference.getPrfInvCostPrecisionUnit());
    					DEBIT = EJBCommon.TRUE;
    					
    				} else {
    					
    					double COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();
        				
        				try {
        					
        					LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
        						invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        					
        					if(invCosting.getCstRemainingQuantity() <= 0 && invCosting.getCstRemainingValue() <=0){
                                System.out.println("RE CALC");
                                HashMap criteria = new HashMap();
                                criteria.put("itemName", invItemLocation.getInvItem().getIiName());
                                criteria.put("location", invItemLocation.getInvLocation().getLocName());

                                ArrayList branchList = new ArrayList();

                                AdBranchDetails mdetails = new AdBranchDetails();
                                mdetails.setBrCode(AD_BRNCH);
                                branchList.add(mdetails);

                                ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

                                invCosting  = invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                						invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            					
                            }

        					if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
     	  	    				
     	  	    				COST = invCosting.getCstRemainingQuantity() <= 0 ? COST :
                                    Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
                            
                                if(COST<=0){
                                   COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
                                }   

     	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
     	  	    				
     	  	    				COST =  invCosting.getCstRemainingQuantity() == 0 ? COST :
     	  	    					Math.abs(this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(),
     	  	    						invAdjustmentLine.getAlAdjustQuantity(), invAdjustmentLine.getAlUnitCost(), false, AD_BRNCH, AD_CMPNY));
        					
     	  	    			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
     	  	    				
     	  	    				COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
    	
        				} catch (FinderException ex) { }
    					
    					COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
		 	  	        		invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
								COST, true, AD_CMPNY);
    					
    					AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() * COST,
    							this.getGlFcPrecisionUnit(AD_CMPNY));
    					
    					DEBIT = EJBCommon.FALSE;
    					
    				}
    				
    				// check for branch mapping
    				
    				LocalAdBranchItemLocation adBranchItemLocation = null;
    				
    				try{
    					
    					adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    							invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
    					
    				} catch (FinderException ex) {
    					
    				}
    				
    				LocalGlChartOfAccount glInventoryChartOfAccount = null;
    				
    				if (adBranchItemLocation == null) {
    					
    					glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    							invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    				} else {
    					
    					glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    							adBranchItemLocation.getBilCoaGlInventoryAccount());
    					
    				}
    				
    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(AMOUNT), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    				TOTAL_AMOUNT += AMOUNT;
    				//ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);
    				
    				// add adjust quantity to item location committed
    				// quantity if negative
    				
    				if (invAdjustmentLine.getAlAdjustQuantity() < 0) {
    					
    					double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
    							invAdjustmentLine.getInvUnitOfMeasure(),
								invAdjustmentLine.getInvItemLocation().getInvItem(),
								Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);
    					
    					invItemLocation = invAdjustmentLine.getInvItemLocation();
    					
    					invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() +
    							convertedQuantity);
    					
    				}
    				
    			}
    			
    			// add variance or transfer/debit distribution
    			
    			DEBIT = (TOTAL_AMOUNT >= 0 && !invAdjustment.getAdjType().equals("ISSUANCE")) ? EJBCommon.FALSE : EJBCommon.TRUE;
	 	  	    
    			this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_AMOUNT), EJBCommon.FALSE, 
    					invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    			
    		}
    		
    	} catch (GlobalInventoryDateException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch(GlobalBranchAccountNumberInvalidException ex) {
    		
    		throw new GlobalBranchAccountNumberInvalidException ();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }    
    
    private void regenerateInventoryDr(LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly, Integer AD_BRNCH, Integer AD_CMPNY) 
    throws GlobalInventoryDateException,
	GlobalInvItemLocationNotFoundException, 
	GlobalInvCSTRemainingQuantityIsLessThanZeroException,
	GlobalAccountNumberInvalidException,
	GlobalBranchAccountNumberInvalidException {
        
        Debug.print("InvApprovalControllerBean regenerateInventoryDr");		        
        
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;    
        LocalAdPreferenceHome adPreferenceHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class); 
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class); 
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);    
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            // regenerate inventory distribution records
            
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	
            // remove all inventory distribution
            
            Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByBuaCode(
                    invBuildUnbuildAssembly.getBuaCode(), AD_CMPNY);
            
            Iterator i = invDistributionRecords.iterator();
            
            while (i.hasNext()) {
                
                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
                
                if(invDistributionRecord.getDrClass().equals("INVENTORY")){
                    
                    i.remove();
                    invDistributionRecord.remove();
                    
                }
                
            }

            // remove all build unbuild assembly lines
            
            Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
            
            i = invBuildUnbuildAssemblyLines.iterator();
            
            while (i.hasNext()) {
                
                LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)i.next();
                
                if (invBuildUnbuildAssemblyLine.getBlBuildQuantity() < 0) {
                    
                    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invBuildUnbuildAssemblyLine.getInvUnitOfMeasure(), invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem(), Math.abs(invBuildUnbuildAssemblyLine.getBlBuildQuantity()), AD_CMPNY);
                    
                    invBuildUnbuildAssemblyLine.getInvItemLocation().setIlCommittedQuantity(invBuildUnbuildAssemblyLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);
                    
                } else {
                    
                    Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                    
                    Iterator j = invBillOfMaterials.iterator();
                    
                    while (j.hasNext()) {
                        
                        LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
                        
                        LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
                        
                        // bom conversion
                        double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                                invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
                                EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * invBuildUnbuildAssemblyLine.getBlBuildQuantity(),
                                this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);
                        
                        invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);
                        
                    }
                    
                }
               
            }
            
            // add inventory distribution
            
            invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
            
            if(invBuildUnbuildAssemblyLines != null && !invBuildUnbuildAssemblyLines.isEmpty()) {
                
                i = invBuildUnbuildAssemblyLines.iterator();
                
                while(i.hasNext()) {
                    
                    LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)i.next();
                    LocalInvItemLocation invItemLocation = invBuildUnbuildAssemblyLine.getInvItemLocation();
                    
                    // start date validation
                    if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    			invBuildUnbuildAssembly.getBuaDate(), invItemLocation.getInvItem().getIiName(),
                    			invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                    	
                    	if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
                    }
                    // check for branch mapping
                    
                    LocalAdBranchItemLocation adBranchItemLocation = null;
                    
                    try {
                        
                        adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                invBuildUnbuildAssemblyLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                    }
                    
                    LocalGlChartOfAccount glInventoryChartOfAccount = null;
                    
                    if (adBranchItemLocation == null){
                        
                        glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                                invBuildUnbuildAssemblyLine.getInvItemLocation().getIlGlCoaInventoryAccount());
                        
                        
                    } else {
                        
                        glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                                adBranchItemLocation.getBilCoaGlInventoryAccount());
                        
                    }
                    
                    byte DEBIT = 0;
                    double TOTAL_AMOUNT = 0d;
                    
                    if (invBuildUnbuildAssemblyLine.getBlBuildQuantity() > 0) {
                        
                        // build assembly
                        
                        DEBIT = EJBCommon.TRUE;
                        
                        Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                        
                        Iterator j = invBillOfMaterials.iterator();
                        
                        while (j.hasNext()) {
                            
                            // bill of materials
                            
                            LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

                            // get raw material
                            LocalInvItemLocation invIlRawMaterial = null;
                            
                            try {
                                
                                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                                        invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(),AD_CMPNY);
                                
                            } catch (FinderException ex) {
                                
                                throw new GlobalInvItemLocationNotFoundException(
                                        String.valueOf(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName()) +
                                        " - Raw Mat. (" + invBillOfMaterial.getBomIiName() + ")");
                                
                            }
                            
                            // bom conversion
                            double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                                    invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
									EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * invBuildUnbuildAssemblyLine.getBlBuildQuantity(),
									this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);
                            
                            // start date validation
                            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                            	Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                            			invBuildUnbuildAssembly.getBuaDate(), invIlRawMaterial.getInvItem().getIiName(),
                            			invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                            	
                            	if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
                            			invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
                            }
                            // add bill of material quantity needed to item location committed quantity
                            invIlRawMaterial.setIlCommittedQuantity(invIlRawMaterial.getIlCommittedQuantity() + convertedQuantity);

                            // check for branch mapping
                            
                            LocalAdBranchItemLocation adBranchIlRawMaterial = null;
                            
                            try{
                                
                                adBranchIlRawMaterial = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                        invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);
                                
                            } catch (FinderException ex) {
                                
                            }
                            
                            LocalGlChartOfAccount glCoaRawMaterial = null;
                            
                            if (adBranchIlRawMaterial == null) {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        invIlRawMaterial.getIlGlCoaInventoryAccount());
                                
                            } else {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        adBranchIlRawMaterial.getBilCoaGlInventoryAccount());
                                
                            }
                            
                            LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

                            double COST = 0d;
                            
                            try {
                                
                                LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                        invBuildUnbuildAssembly.getBuaDate(), invItem.getIiName(), 
                                        invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);
                                
                                COST = Math.abs(invCosting.getCstRemainingValue() /
                                        invCosting.getCstRemainingQuantity());
                                
                            } catch (FinderException ex) {
                                
                                COST = invItem.getIiUnitCost();
                                
                            }
                            
                            double BOM_AMOUNT = EJBCommon.roundIt(convertedQuantity * COST, this.getGlFcPrecisionUnit(AD_CMPNY));
                            
                            TOTAL_AMOUNT += BOM_AMOUNT;
                            //ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);
                            
                            this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                                    Math.abs(BOM_AMOUNT), glCoaRawMaterial.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
                            
                        }
                        
                    } else {
                        
                        // unbuild assembly
                        
                        try {
                            
                            LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(
                                    invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName(),
                                    invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName(),
                                    AD_BRNCH, AD_CMPNY);
                            
                            if ((invCosting.getCstRemainingQuantity()- Math.abs(invBuildUnbuildAssemblyLine.getBlBuildQuantity())) < 0) {
                                
                                throw new GlobalInvCSTRemainingQuantityIsLessThanZeroException(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName());
                                
                            }
                            
                        } catch (FinderException ex) {
                            
                        }
                        
                        invItemLocation = invBuildUnbuildAssemblyLine.getInvItemLocation();
                        
                        // add build quantity to item location committed quantity
                        invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + Math.abs(invBuildUnbuildAssemblyLine.getBlBuildQuantity()));
                        
                        DEBIT = EJBCommon.FALSE;
                        
                        Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                        
                        Iterator j = invBillOfMaterials.iterator();
                        
                        while (j.hasNext()) {
                            
                            // bill of materials
                            
                            LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
                            
                            // get raw material
                            LocalInvItemLocation invIlRawMaterial = null;
                            
                            try {
                                
                                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                                        invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
                                
                            } catch(FinderException ex) {
                                
                                throw new GlobalInvItemLocationNotFoundException(String.valueOf(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName()) +
                                        " - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");
                                
                            }
                            
                            // bom conversion
                            double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                                    invBillOfMaterial.getInvUnitOfMeasure(),invIlRawMaterial.getInvItem(),
                                    EJBCommon.roundIt(invBillOfMaterial.getBomQuantityNeeded() * invBuildUnbuildAssemblyLine.getBlBuildQuantity(),
                                    this.getInvGpQuantityPrecisionUnit(AD_CMPNY)),AD_CMPNY);
                            
                            // start date validation
                            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                            	Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                            			invBuildUnbuildAssembly.getBuaDate(), invIlRawMaterial.getInvItem().getIiName(),
                            			invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                            	
                            	if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
                            			invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
                            }
                            // check for branch mapping
                            
                            LocalAdBranchItemLocation adBranchIlRawMaterial =  null;
                            
                            try{
                                
                                adBranchIlRawMaterial = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                        invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);
                                
                            } catch (FinderException ex) {
                                
                            }
                            
                            LocalGlChartOfAccount glCoaRawMaterial = null;
                            
                            if (adBranchIlRawMaterial == null) {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        invIlRawMaterial.getIlGlCoaInventoryAccount());
                                
                            } else {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        adBranchIlRawMaterial.getBilCoaGlInventoryAccount());
                                
                            }
                            
                            LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

                            double COST = 0d;
                            
                            try {
                                
                                LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                        invBuildUnbuildAssembly.getBuaDate(), invItem.getIiName(), 
                                        invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);
                                
                                COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
                                
                            } catch (FinderException ex) {
                                
                                COST = invItem.getIiUnitCost();
                                
                            }
                            
                            double BOM_AMOUNT = EJBCommon.roundIt(convertedQuantity * COST, this.getGlFcPrecisionUnit(AD_CMPNY));
                            
                            TOTAL_AMOUNT += BOM_AMOUNT;
                            //ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);
                            
                            this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                                    Math.abs(BOM_AMOUNT), glCoaRawMaterial.getCoaCode(), invBuildUnbuildAssembly,
                                    AD_BRNCH, AD_CMPNY);
                            
                        }
                        
                    }
                    
                    this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", DEBIT,
                            Math.abs(TOTAL_AMOUNT), glInventoryChartOfAccount.getCoaCode(), invBuildUnbuildAssembly,
                            AD_BRNCH, AD_CMPNY);
                    
                }
                
            }
            
        } catch (GlobalInventoryDateException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInvItemLocationNotFoundException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInvCSTRemainingQuantityIsLessThanZeroException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalBranchAccountNumberInvalidException ex) {
            
            throw new GlobalBranchAccountNumberInvalidException ();    		
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private void regenerateInventoryDr(LocalInvStockTransfer invStockTransfer, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalInventoryDateException,
	GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvApprovalControllerBean regenerateInventoryDr");		        
    	
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalInvLocationHome invLocationHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class); 
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class); 
    		invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class); 
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		// regenerate inventory distribution records
    		
    		Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByStCode(
    				invStockTransfer.getStCode(), AD_CMPNY);
    		
    		Iterator i = invDistributionRecords.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    			
    			if(invDistributionRecord.getDrClass().equals("INVENTORY")){
    				
    				i.remove();
    				invDistributionRecord.remove();
    				
    			}
    			
    		}
    		
    		Collection invStockTransferLines = invStockTransfer.getInvStockTransferLines();
    		
    		i = invStockTransferLines.iterator();
    		
    		while(i.hasNext()) {
    			
    			LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) i.next();	                	                
    			
    			LocalInvLocation invLocFrom = null;
    			LocalInvLocation invLocTo = null;
    			
    			try {
    				
    				invLocFrom = invLocationHome.findByPrimaryKey(invStockTransferLine.getStlLocationFrom());
    				invLocTo = invLocationHome.findByPrimaryKey(invStockTransferLine.getStlLocationTo());
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			LocalInvItemLocation invItemLocFrom = null;	                
    			LocalInvItemLocation invItemLocTo = null;
    			
    			try {
    				
    				invItemLocFrom = invItemLocationHome.findByLocNameAndIiName(
    						invLocFrom.getLocName(), invStockTransferLine.getInvItem().getIiName(), AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    				throw new GlobalInvItemLocationNotFoundException(invStockTransferLine.getInvItem().getIiName() + " - " + invLocFrom.getLocName());
    				
    			}
    			
    			try {
    				
    				invItemLocTo = invItemLocationHome.findByLocNameAndIiName(
    						invLocTo.getLocName(), invStockTransferLine.getInvItem().getIiName(), AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    				throw new GlobalInvItemLocationNotFoundException(invStockTransferLine.getInvItem().getIiName() + " - " + invLocTo.getLocName());
    				
    			}
    			
    			//	start date validation	                	              
    			if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
    				Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
    						invStockTransfer.getStDate(), invItemLocFrom.getInvItem().getIiName(),
    						invItemLocFrom.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocFrom.getInvItem().getIiName());
    				
    				invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
    						invStockTransfer.getStDate(), invItemLocTo.getInvItem().getIiName(),
    						invItemLocTo.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    				if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocTo.getInvItem().getIiName());
    			}
    			// add physical inventory distribution
    			
    			double COST = this.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(invItemLocFrom.getInvItem().getIiName(), invItemLocFrom.getInvLocation().getLocName(),
 	  	    			invStockTransferLine.getInvUnitOfMeasure().getUomName(), invStockTransfer.getStDate(),
						AD_BRNCH, AD_CMPNY);
    			
    			double AMOUNT = 0d;
    			
    			AMOUNT = EJBCommon.roundIt(invStockTransferLine.getStlQuantityDelivered() * COST, 
    					this.getGlFcPrecisionUnit(AD_CMPNY));
    			
    			// dr to locationTo
    			
    			// check branch mapping
    			
    			LocalAdBranchItemLocation adBranchItemLocation = null;
    			
    			try{
    				
    				adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    						invItemLocTo.getIlCode(), AD_BRNCH, AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			LocalGlChartOfAccount glChartOfAccountTo = null;
    			
    			if (adBranchItemLocation == null) {
    				
    				glChartOfAccountTo = glChartOfAccountHome.findByPrimaryKey(
    						invItemLocTo.getIlGlCoaInventoryAccount());
    				
    			} else {
    				
    				glChartOfAccountTo = glChartOfAccountHome.findByPrimaryKey(
    						adBranchItemLocation.getBilCoaGlInventoryAccount());
    				
    			}
    			
    			this.addInvDrEntry(invStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE, 
    					Math.abs(AMOUNT), glChartOfAccountTo.getCoaCode(), invStockTransfer, AD_BRNCH, AD_CMPNY);
    			
    			// cr to locationFrom
    			
    			// check branch mapping
    			
    			try{
    				
    				adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    						invItemLocFrom.getIlCode(), AD_BRNCH, AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			LocalGlChartOfAccount glChartOfAccountFrom = null;
    			
    			if (adBranchItemLocation == null) {
    				
    				glChartOfAccountFrom = glChartOfAccountHome.findByPrimaryKey(
    						invItemLocFrom.getIlGlCoaInventoryAccount());
    				
    			} else {
    				
    				glChartOfAccountFrom = glChartOfAccountHome.findByPrimaryKey(
    						adBranchItemLocation.getBilCoaGlInventoryAccount());
    				
    			}
    			
    			this.addInvDrEntry(invStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, 
    					Math.abs(AMOUNT), glChartOfAccountFrom.getCoaCode(), invStockTransfer, AD_BRNCH, AD_CMPNY);
    			
    		}
    		
    		
    	} catch (GlobalInventoryDateException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch(GlobalBranchAccountNumberInvalidException ex) {
    		
    		throw new GlobalBranchAccountNumberInvalidException ();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
}
    
    private void regenerateInventoryDr(LocalInvBranchStockTransfer invBranchStockTransfer, Integer AD_BRNCH, Integer AD_CMPNY) throws
    GlobalInventoryDateException,
    GlobalBranchAccountNumberInvalidException {
        
        Debug.print("InvBranchStockTransferInEntryControllerBean regenerateInventoryDr");		        
        
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        InvRepItemCostingControllerHome homeRIC = null;
        InvRepItemCostingController ejbRIC = null;
        
        // Initialize EJB Home
        
        try {
            
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class); 
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class); 
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class); 
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		    lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            homeRIC = (InvRepItemCostingControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	ejbRIC = homeRIC.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        try {
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            // regenerate inventory distribution records
            
            Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByBstCode(
                    invBranchStockTransfer.getBstCode(), AD_CMPNY);
            
            Iterator i = invDistributionRecords.iterator();
            
            while (i.hasNext()) {
                
                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
                
                if(invDistributionRecord.getDrClass().equals("INVENTORY")){
                    
                    i.remove();
                    invDistributionRecord.remove();
                    
                }
                
            }
            
            Collection invBranchStockTransferLines = invBranchStockTransfer.getInvBranchStockTransferLines();
            
            i = invBranchStockTransferLines.iterator();
            
            while(i.hasNext()) {
                
                LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) i.next();	                	                
                
                LocalInvItemLocation invItemLocation = null;
                
                String locName = invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName();
                String invItemName = invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName();
                
                try {
                    
                    invItemLocation = invItemLocationHome.findByLocNameAndIiName(locName, invItemName, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                    throw new GlobalInvItemLocationNotFoundException(invItemName + " - " + locName);
                    
                }
                
                LocalInvItemLocation invItemTransitLocation = null;
                
                try {
                    
                    invItemTransitLocation = invItemLocationHome.findByLocNameAndIiName(
                            invBranchStockTransfer.getInvLocation().getLocName(), 
                            invItemName, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                    throw new GlobalInvItemLocationNotFoundException("Transit Location " + String.valueOf(invBranchStockTransfer.getInvLocation().getLocName()));
                    
                }
                
                if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	                //	start date validation	                	              
	                
	                Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	                        invBranchStockTransfer.getBstDate(), invItemName, locName, AD_BRNCH, AD_CMPNY);
	                
	                if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemName);
                }
                
                
                // add physical inventory distribution
                
                double COST = 0d;
                
                try {
                    
                    LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                            invBranchStockTransfer.getBstDate(), invItemName, locName, AD_BRNCH, AD_CMPNY);
                    

                    if(invCosting.getCstRemainingQuantity() <= 0 && invCosting.getCstRemainingValue() <=0){
                        System.out.println("RE CALC");
                        HashMap criteria = new HashMap();
                        criteria.put("itemName", invItemLocation.getInvItem().getIiName());
                        criteria.put("location", invItemLocation.getInvLocation().getLocName());

                        ArrayList branchList = new ArrayList();

                        AdBranchDetails mdetails = new AdBranchDetails();
                        mdetails.setBrCode(AD_BRNCH);
                        branchList.add(mdetails);

                        ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

                        invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                invBranchStockTransfer.getBstDate(), invItemName, locName, AD_BRNCH, AD_CMPNY);
                   }

                    
                    if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))   
          			{
          				COST = invCosting.getCstRemainingQuantity() <= 0 ? COST :
                            Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
                    
                        if(COST<=0){
                           COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
                        }   
          			}
          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
          			{
          				COST = this.getInvFifoCost(invBranchStockTransfer.getBstDate(), invItemLocation.getIlCode(), invCosting.getCstAdjustQuantity(),
          						invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
         			}
          			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
          			{
          				COST = invItemLocation.getInvItem().getIiUnitCost();
         			}
                    
                } catch (FinderException ex) {
                    
                    COST = invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiUnitCost();
                }
                
                double AMOUNT = 0d;
                
                AMOUNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantityReceived() * COST, 
                        this.getGlFcPrecisionUnit(AD_CMPNY));
                
                
                // check branch mapping
                
                LocalAdBranchItemLocation adBranchItemLocation = null;
                
                try{
                    
                    adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                            invBranchStockTransferLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                LocalGlChartOfAccount glChartOfAccount = null;
                
                if (adBranchItemLocation == null) {
                    
                    glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                            invBranchStockTransferLine.getInvItemLocation().getIlGlCoaInventoryAccount());
                    
                } else {
                    
                    glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                            adBranchItemLocation.getBilCoaGlInventoryAccount());
                    
                }
                
                this.addInvDrEntry(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE, 
                        Math.abs(AMOUNT), glChartOfAccount.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);
                
                
                // check branch mapping for transit location
                
                LocalAdBranchItemLocation adBranchItemTransitLocation = null;
                
                try{
                    
                    adBranchItemTransitLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                            invItemTransitLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
                    
                    
                } catch (FinderException ex) {
                    
                }
                
                LocalGlChartOfAccount glChartOfAccountTransit = null;
                
                if (adBranchItemTransitLocation == null) {
                    
                    glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
                            invItemTransitLocation.getIlGlCoaInventoryAccount());
                    
                } else {
                    
                    glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
                            adBranchItemTransitLocation.getBilCoaGlInventoryAccount());
                    
                }
                
                // add dr for inventory transit location
                
                this.addInvDrEntry(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, 
                        Math.abs(AMOUNT), glChartOfAccountTransit.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);
                
            }
            
        } catch (GlobalInventoryDateException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch(GlobalBranchAccountNumberInvalidException ex) {
            
            throw new GlobalBranchAccountNumberInvalidException ();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }    
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvApprovalControllerBean addInvDrEntry");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;           
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {        
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    			
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
    		
    		// create distribution record        
    		
    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					AD_CMPNY);

    		invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		glChartOfAccount.addInvDistributionRecord(invDistributionRecord);		   
    		
    	} catch(GlobalBranchAccountNumberInvalidException ex) {
    		
    		throw new GlobalBranchAccountNumberInvalidException ();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE,
    		LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly, Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalAccountNumberInvalidException,
			GlobalBranchAccountNumberInvalidException {
		
		Debug.print("InvApprovalControllerBean addInvDrEntry");
		
		LocalAdCompanyHome adCompanyHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;        
		LocalGlChartOfAccountHome glChartOfAccountHome = null;           
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
			invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
			
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}            
		
		try {        
			
			// get company
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    		
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    		
    		} catch(FinderException ex) {
    			
    			throw new GlobalAccountNumberInvalidException ();
    			
    		}
			
			if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
				throw new GlobalBranchAccountNumberInvalidException();
			
			// create distribution record        
			
			LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
			
			invBuildUnbuildAssembly.addInvDistributionRecord(invDistributionRecord);
			glChartOfAccount.addInvDistributionRecord(invDistributionRecord);		   

		} catch(GlobalAccountNumberInvalidException ex) {
			
			throw new GlobalAccountNumberInvalidException ();
			
		} catch(GlobalBranchAccountNumberInvalidException ex) {
			
			throw new GlobalBranchAccountNumberInvalidException ();

		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS,
    		byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalInvStockTransfer invStockTransfer, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvApprovalControllerBean addInvDrEntry");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;           
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {        
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    			
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
    		
    		// create distribution record        
    		
    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
    				DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
    		
    		invStockTransfer.addInvDistributionRecord(invDistributionRecord);
    		glChartOfAccount.addInvDistributionRecord(invDistributionRecord);		   
    		
    	} catch (GlobalBranchAccountNumberInvalidException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, 
    		Integer COA_CODE, LocalInvBranchStockTransfer invBranchStockTransfer, 
			Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvApprovalControllerBean addInvDrEntry");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;           
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {        
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    			
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
    		
    		// create distribution record        
    		
    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
    				DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
    		
    		invBranchStockTransfer.addInvDistributionRecord(invDistributionRecord);
    		glChartOfAccount.addInvDistributionRecord(invDistributionRecord);		   
    		
    	} catch (GlobalBranchAccountNumberInvalidException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    private short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
    	
    	Debug.print("InvApprovalControllerBean getInvGpQuantityPrecisionUnit");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;         
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		return adPreference.getPrfInvQuantityPrecisionUnit();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {
		
		Debug.print("InvApprovalControllerBean convertCostByUom");		        
	    
		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		        
	    // Initialize EJB Home
	    
	    try {
	        
	        invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
	        invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
	           
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	            
	    try {
	    
	    	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
	        LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
	                    
	        if (isFromDefault) {	        	
	        
	        	return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();
	    	
	        } else {
	        	
	        	return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();
	        	
	        }
	    	
	    	
	    						    		        		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
		
	}
    
    private double getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(String II_NM, String LOC_FRM, String UOM_NM, Date ST_DT, Integer AD_BRNCH, Integer AD_CMPNY) throws
    GlobalInvItemLocationNotFoundException {
    	
    	Debug.print("InvApprovalControllerBean getInvIiUnitCostByIiNameAndUomName");
    	
    	LocalInvItemHome invItemHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalInvCostingHome invCostingHome = null;      	
    	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
       
    	// Initialize EJB Home
    	
    	try {
    		
    		invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
  			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);  
  
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalInvItemLocation invItemLocation = null;
    	
    		try { 
    			
    			invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_FRM, II_NM, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlobalInvItemLocationNotFoundException(String.valueOf(LOC_FRM + " for item " + II_NM));
    			
    		}
    		
    		double COST = 0d;
    		
    		try {
    			
    			LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    					ST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    			
    			COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), this.getGlFcPrecisionUnit(AD_CMPNY));
    			
    		} catch (FinderException ex) {
    			
    			COST = invItemLocation.getInvItem().getIiUnitCost();
    			
    		}
    		
    		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
      	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);
      	
      	return EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
      	
    	} catch (GlobalInvItemLocationNotFoundException ex) {
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("InvApprovalControllerBean voidInvAdjustment");
    	
    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();
    			
    			Iterator i = invDistributionRecords.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    				
    				list.add(invDistributionRecord);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();    	    			    			
    				
    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(), 
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    			}	    
    			
    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
    				
    				list.add(invAdjustmentLine);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();    	    			    			
    				
    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}
    			
    			invAdjustment.setAdjVoid(EJBCommon.TRUE);
    			
    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("InvApprovalControllerBean generateCostVariance");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try{ 
    		
    		
    		
    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;
    		
    		
    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    		
    		try{
    			
    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
    			
    		} catch (FinderException ex) {
    			
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    			
    		}
    		
    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment,
    				CST_VRNC_VL, EJBCommon.TRUE, AD_CMPNY);
    		
    		// check for branch mapping
    		
    		LocalAdBranchItemLocation adBranchItemLocation = null;
    		
    		try{
    			
    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		LocalGlChartOfAccount glInventoryChartOfAccount = null;
    		
    		if (adBranchItemLocation == null) {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());
    			
    		}
    		
    		
    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;
    		
    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", 
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("InvApprovalControllerBean regenerateCostVariance");
    	
    	try {
    		
    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			if(prevInvCosting.getCstRemainingQuantity() < 0) {
    				
    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
    					
    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
    					
    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
    					
    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
    						
    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
    						
    					}
    					
    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
   						
   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
   	   						
   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
   	    					
   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}
   	   						
    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" + 
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
    					
    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
    					
    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
    						
    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() * 
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
    						
    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();
    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
							
    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}
    					
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" + 
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();
    					
    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" + 
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {
    					
    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
    					
    				} else {
    					
    					prevInvCosting = invPropagatedCosting;
    					continue;
    					
    				}
    				
    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;
    				
    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());
    				
    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// set previous costing
    			prevInvCosting = invPropagatedCosting;
    			
    		}
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}      */  	
    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {
    	
    	Debug.print("InvApprovalControllerBean addInvAlEntry");
    	
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {
    		
    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL,null,null, 0,0, AL_VD, AD_CMPNY);
    		
    		// map adjustment, unit of measure, item location
    		//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvAdjustment(invAdjustment);
    		//invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
    		//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvItemLocation(invItemLocation);
    		
    		return invAdjustmentLine;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    
    private void postToInv(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY, double CST_ADJST_CST,
    		double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH,Integer AD_CMPNY) throws
    		AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {
    
    	Debug.print("InvAdjustmentEntryControllerBean postToInv");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	          lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
          
        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
           LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
           int CST_LN_NMBR = 0;
           
           CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adPreference.getPrfInvCostPrecisionUnit());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adPreference.getPrfInvCostPrecisionUnit());
                      
           if (CST_ADJST_QTY < 0) {
                      
           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
           
           }
           
          
           try {
        	   System.out.println("generate line number");
           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
               System.out.println("generate line number: " + CST_LN_NMBR);
           } catch (FinderException ex) {
        	   System.out.println("generate line number CATCH");
           	   CST_LN_NMBR = 1;
           
           }
           
           if(CST_VRNC_VL != 0) {
           	
            //void subsequent cost variance adjustments
            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
            Iterator i = invAdjustmentLines.iterator();
            
            while (i.hasNext()){
            	
            	LocalInvAdjustmentLine invAdjustmentLineTemp = (LocalInvAdjustmentLine) i.next();
            	this.voidInvAdjustment(invAdjustmentLineTemp.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
            	
            }
           	
           }
           
           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           try {           
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   System.out.println(prevCst.getCstCode()+"   "+ prevCst.getCstRemainingQuantity());
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();
        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }
        	   System.out.println("prevExpiryDates: " + prevExpiryDates);
        	   System.out.println("qtyPrpgt: " + qtyPrpgt);
           }catch (Exception ex){
        	   System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
        	   prevExpiryDates="";
           }

           //create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
           invCosting.setCstQCNumber(invAdjustmentLine.getAlQcNumber());
           invCosting.setCstQCExpiryDate(invAdjustmentLine.getAlQcExpiryDate());
           //invItemLocation.addInvCosting(invCosting);
           invCosting.setInvItemLocation(invItemLocation);
           invCosting.setInvAdjustmentLine(invAdjustmentLine);

           //         Get Latest Expiry Dates           
           if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
        	   if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){

        		   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
        			   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));

        			   String miscList2Prpgt = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), qty2Prpgt, "False");
        			   ArrayList miscList = this.expiryDates(invAdjustmentLine.getAlMisc(), qty2Prpgt);
        			   String propagateMiscPrpgt = "";
        			   String ret = "";
        			   String exp = "";
        			   String Checker = "";

        			   //ArrayList miscList2 = null;
        			   if(CST_ADJST_QTY>0){
        				   prevExpiryDates = prevExpiryDates.substring(1);
        				   propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
        			   }else{
        				   Iterator mi = miscList.iterator();

        				   propagateMiscPrpgt = prevExpiryDates;
        				   ret = propagateMiscPrpgt;
        				   while(mi.hasNext()){
        					   String miscStr = (String)mi.next();

        					   Integer qTest = this.checkExpiryDates(ret+"fin$");
        					   ArrayList miscList2 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

        					   //ArrayList miscList2 = this.expiryDates("$" + ret, qtyPrpgt);
        					   Iterator m2 = miscList2.iterator();
        					   ret = "";
        					   String ret2 = "false";
        					   int a = 0;
        					   while(m2.hasNext()){
        						   String miscStr2 = (String)m2.next();

        						   if(ret2=="1st"){
        							   ret2 = "false";
        						   }
        						   System.out.println("miscStr1: "+miscStr);
        						   System.out.println("miscStr2: "+miscStr2);

        						   if(miscStr2.trim().equals(miscStr.trim())){
        							   if(a==0){
        								   a = 1;
        								   ret2 = "1st";
        								   Checker = "true";
        							   }else{
        								   a = a+1;
        								   ret2 = "true";
        							   }
        						   }
        						   System.out.println("Checker: "+Checker);
        						   System.out.println("ret2: "+ret2);
        						   if(!miscStr2.equals(miscStr) || a>1){
        							   if((ret2!="1st")&&((ret2=="false")||(ret2=="true"))){
        								   if (miscStr2!=""){
        									   miscStr2 = "$" + miscStr2;
        									   ret = ret + miscStr2;
        									   System.out.println("ret " + ret);
        									   ret2 = "false";
        								   }
        							   }
        						   }

        					   }
        					   if(ret!=""){
        						   ret = ret + "$";
        					   }
        					   System.out.println("ret una: "+ret);
        					   exp = exp + ret;
        					   qtyPrpgt= qtyPrpgt -1;
        				   }
        				   System.out.println("ret fin " + ret);
        				   System.out.println("exp fin " + exp);
        				   //propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
        				   propagateMiscPrpgt = ret;
        				   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
        				   if(Checker=="true"){
        					   //invCosting.setCstExpiryDate(propagateMiscPrpgt);
        				   }else{
        					   System.out.println("Exp Not Found");
        					   throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
        				   }
        			   }
        			   invCosting.setCstExpiryDate(propagateMiscPrpgt);

        		   }else{
        			   invCosting.setCstExpiryDate(prevExpiryDates);
        		   }

        	   }else{
        		   System.out.println("invAdjustmentLine ETO NA: "+ invAdjustmentLine.getAlAdjustQuantity());
        		   if(invAdjustmentLine.getAlAdjustQuantity()>0){
        			   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
        				   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
        				   String initialPrpgt = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), initialQty, "False");

        				   invCosting.setCstExpiryDate(initialPrpgt);
        			   }else{
        				   invCosting.setCstExpiryDate(prevExpiryDates);
        			   }
        		   }else{                    	
        			   //throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
        		   }

        	   }
           }
           
           
			// if cost variance is not 0, generate cost variance for the transaction 
			if(CST_VRNC_VL != 0) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"INVADJ" + invAdjustmentLine.getInvAdjustment().getAdjDocumentNumber(),
						invAdjustmentLine.getInvAdjustment().getAdjDescription(),
						invAdjustmentLine.getInvAdjustment().getAdjDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				
			}

           // propagate balance if necessary           
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
           
           Iterator i = invCostings.iterator();

           String miscList = "";
           ArrayList miscList2 = null;
           //double qty = 0d;
                 

           System.out.println("miscList Propagate:" + miscList);
           String propagateMisc ="";
		   String ret = "";
		   

           while (i.hasNext()) {
        	   String Checker = "";
        	   String Checker2 = "";
        	   
        	   LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

        	   invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
        	   invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
        	   if (CST_ADJST_QTY > 0) {
        		   invPropagatedCosting.setCstRemainingLifoQuantity(invPropagatedCosting.getCstRemainingLifoQuantity() + CST_ADJST_QTY);
        	   }
        	   System.out.println("invPropagatedCosting.getCstExpiryDate() : " + invPropagatedCosting.getCstExpiryDate());
        	   if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
        		   if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
        			   double qty = Double.parseDouble(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
        			   //invPropagatedCosting.getInvAdjustmentLine().getAlMisc();
        			   miscList = this.propagateExpiryDates(invAdjustmentLine.getAlMisc(), qty, "False");
        			   miscList2 = this.expiryDates(invAdjustmentLine.getAlMisc(), qty);
        			   System.out.println("invAdjustmentLine.getAlMisc(): "+invAdjustmentLine.getAlMisc());
        			   System.out.println("getAlAdjustQuantity(): "+invAdjustmentLine.getAlAdjustQuantity());

        			   if(invAdjustmentLine.getAlAdjustQuantity()<0){
        				   Iterator mi = miscList2.iterator();

        				   propagateMisc = invPropagatedCosting.getCstExpiryDate();
        				   ret = invPropagatedCosting.getCstExpiryDate();
        				   while(mi.hasNext()){
        					   String miscStr = (String)mi.next();

        					   Integer qTest = this.checkExpiryDates(ret+"fin$");
        					   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

        					   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
        					   System.out.println("ret: " + ret);
        					   Iterator m2 = miscList3.iterator();
        					   ret = "";
        					   String ret2 = "false";
        					   int a = 0;
        					   while(m2.hasNext()){
        						   String miscStr2 = (String)m2.next();

        						   if(ret2=="1st"){
        							   ret2 = "false";
        						   }
        						   System.out.println("2 miscStr: "+miscStr);
        						   System.out.println("2 miscStr2: "+miscStr2);
        						   if(miscStr2.equals(miscStr)){
        							   if(a==0){
        								   a = 1;
        								   ret2 = "1st";
        								   Checker2 = "true";
        							   }else{
        								   a = a+1;
        								   ret2 = "true";
        							   }
        						   }
        						   System.out.println("Checker: "+Checker2);
        						   if(!miscStr2.equals(miscStr) || a>1){
        							   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
        								   if (miscStr2!=""){
        									   miscStr2 = "$" + miscStr2;
        									   ret = ret + miscStr2;
        									   ret2 = "false";
        								   }
        							   }
        						   }

        					   }
        					   if(Checker2!="true"){
        						   throw new GlobalExpiryDateNotFoundException(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
        					   }else{
        						   System.out.println("TAE");
        					   }

        					   ret = ret + "$";
        					   qtyPrpgt= qtyPrpgt -1;
        				   }
        			   }

        		   }

        		   System.out.println("getAlAdjustQuantity(): "+invAdjustmentLine.getAlAdjustQuantity());
        		   if(invAdjustmentLine.getAlAdjustQuantity()<0){

        			   Iterator mi = miscList2.iterator();

        			   propagateMisc = invPropagatedCosting.getCstExpiryDate();
        			   ret = propagateMisc;
        			   while(mi.hasNext()){
        				   String miscStr = (String)mi.next();

        				   Integer qTest = this.checkExpiryDates(ret+"fin$");
        				   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

        				   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
        				   System.out.println("ret: " + ret);
        				   Iterator m2 = miscList3.iterator();
        				   ret = "";
        				   String ret2 = "false";
        				   int a = 0;
        				   while(m2.hasNext()){
        					   String miscStr2 = (String)m2.next();

        					   if(ret2=="1st"){
        						   ret2 = "false";
        					   }
        					   System.out.println("2 miscStr: "+miscStr);
        					   System.out.println("2 miscStr2: "+miscStr2);
        					   if(miscStr2.equals(miscStr)){
        						   if(a==0){
        							   a = 1;
        							   ret2 = "1st";
        							   Checker = "true";
        						   }else{
        							   a = a+1;
        							   ret2 = "true";
        						   }
        					   }
        					   System.out.println("Checker: "+Checker);
        					   if(!miscStr2.equals(miscStr) || a>1){
        						   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
        							   if (miscStr2!=""){
        								   miscStr2 = "$" + miscStr2;
        								   ret = ret + miscStr2;
        								   ret2 = "false";
        							   }
        						   }
        					   }

        				   }
        				   ret = ret + "$";
        				   qtyPrpgt= qtyPrpgt -1;
        			   }
        			   propagateMisc = ret;
        		   }else{
        			   propagateMisc = miscList+invPropagatedCosting.getCstExpiryDate().substring(1, invPropagatedCosting.getCstExpiryDate().length());
        		   }

        		   invPropagatedCosting.setCstExpiryDate(propagateMisc);
        	   }
        	   

           }
           
           // regenerate cost variance
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);
           
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
           	
           	throw ex;

        } catch (GlobalExpiryDateNotFoundException ex){
           	
           	throw ex;

        } catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }

    }
    
    
    
private byte sendEmail(LocalAdApprovalQueue adApprovalQueue, Integer AD_CMPNY) {
		
		Debug.print("InvApprovalControllerBean sendEmail");
		
		
		LocalInvAdjustmentHome invAdjustmentHome = null;
		
		
		// Initialize EJB Home
		
		try {

			invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		StringBuilder composedEmail = new StringBuilder();
		LocalInvAdjustment invAdjustment = null;

		try {

			invAdjustment = invAdjustmentHome.findByPrimaryKey(adApprovalQueue.getAqDocumentCode());
		
			HashMap hm = new HashMap();
			
			/*Iterator i = apPurchaseRequisition.getApPurchaseRequisitionLines().iterator();
			
			while(i.hasNext()) {
				
				LocalApPurchaseRequisitionLine apPurchaseRequisitionLine = (LocalApPurchaseRequisitionLine)i.next();
				
				Collection apCanvasses = apCanvassHome.findByPrlCodeAndCnvPo(
						apPurchaseRequisitionLine.getPrlCode(), EJBCommon.TRUE, AD_CMPNY);
				
				Iterator j = apCanvasses.iterator();

				
				while(j.hasNext()) {
					
					LocalApCanvass apCanvass = (LocalApCanvass)j.next();
					
					
					if (hm.containsKey(apCanvass.getApSupplier().getSplSupplierCode())){
						AdModApprovalQueueDetails adModApprovalQueueExistingDetails = (AdModApprovalQueueDetails)
		        				 hm.get(apCanvass.getApSupplier().getSplSupplierCode());
						
						adModApprovalQueueExistingDetails.setAqSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
						adModApprovalQueueExistingDetails.setAqSupplierName(apCanvass.getApSupplier().getSplName());
						adModApprovalQueueExistingDetails.setAqAmount(adModApprovalQueueExistingDetails.getAqAmount() +  apCanvass.getCnvAmount());

					} else {
						
						AdModApprovalQueueDetails adModApprovalQueueNewDetails = new AdModApprovalQueueDetails();

						adModApprovalQueueNewDetails.setAqSupplierCode(apCanvass.getApSupplier().getSplSupplierCode());
						adModApprovalQueueNewDetails.setAqSupplierName(apCanvass.getApSupplier().getSplName());
						adModApprovalQueueNewDetails.setAqAmount(apCanvass.getCnvAmount());

						hm.put(apCanvass.getApSupplier().getSplSupplierCode(), adModApprovalQueueNewDetails);	
					}
				}
			}
			
			
			Set set = hm.entrySet();
			
			composedEmail.append("<table border='1' style='width:100%'>");

			composedEmail.append("<tr>");
			composedEmail.append("<th> VENDOR </th>");
			composedEmail.append("<th> AMOUNT </th>");
			composedEmail.append("</tr>");

			Iterator x = set.iterator();

			while(x.hasNext()) {
				
			
				Map.Entry me = (Map.Entry)x.next();

				AdModApprovalQueueDetails adModApprovalQueueDetails = (AdModApprovalQueueDetails)
							me.getValue();
				
				composedEmail.append("<tr>");
				composedEmail.append("<td>");
				composedEmail.append(adModApprovalQueueDetails.getAqSupplierName());
				composedEmail.append("</td>");
				composedEmail.append("<td>");
				composedEmail.append(adModApprovalQueueDetails.getAqAmount());
				composedEmail.append("</td>");
				composedEmail.append("</tr>");

			}
			
			composedEmail.append("</table></body></html>");*/
			
			
			
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
			
		String emailTo = adApprovalQueue.getAdUser().getUsrEmailAddress();
		Properties props = new Properties();
		/*
		 * GMAIL SETTINGS
		 * props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		
		Session session = Session.getDefaultInstance(props,
		new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("chrisr@daltron.net.pg","Sh0wc453");
			}
		});*/
		
		
		props.put("mail.smtp.host", "180.150.253.101");
		props.put("mail.smtp.socketFactory.port", "25");
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.port", "25");

		Session session = Session.getDefaultInstance(props, null);

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("ofs-notifcation@daltron.net.pg"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailTo));
			
			message.setRecipients(Message.RecipientType.BCC,
					InternetAddress.parse("cromero@wrcpng.com"));

			message.setSubject("DALTRON - OFS - INV ADJUSTMENT APPROVAL IA #:"+invAdjustment.getAdjDocumentNumber());
			/*message.setText("Dear Mr/Mrs," +
					"\n\n No spam to my email, please!");*/

			/*message.setContent(
		              "<h1>This is actual message embedded in HTML tags</h1>",
		             "text/html");*/
			System.out.println("adApprovalQueue.getAqRequesterName()="+adApprovalQueue.getAqRequesterName());
			System.out.println("adApprovalQueue.getAqDocumentNumber()="+adApprovalQueue.getAqDocumentNumber());
			System.out.println("adApprovalQueue.getAdUser().getUsrDescription()="+adApprovalQueue.getAdUser().getUsrDescription());

			message.setContent(
		              "Dear "+adApprovalQueue.getAdUser().getUsrDescription()+",<br><br>"+
		              "A stocktake adjusment request was raised by "+adApprovalQueue.getAqRequesterName()+" for your approval.<br>"+
		              "Adj Number: "+adApprovalQueue.getAqDocumentNumber()+".<br>"+
		              "Description: "+invAdjustment.getAdjDescription()+".<br>"+
		              composedEmail.toString() +
		              
		              "Please click the link <a href=\"http://180.150.253.99:8080/daltron\">http://180.150.253.99:8080/daltron</a>.<br><br><br>"+
		              
		              "This is an automated message and was sent from a notification-only email address.<br><br>"+
		              "Please do not reply to this message.<br><br>",
		             "text/html");
			

			Transport.send(message);
			adApprovalQueue.setAqEmailSent(EJBCommon.TRUE);
			
			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		
		return 0;
	}
    
    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM,String NT_BY, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("InvApprovalControllerBean saveInvAdjustment");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home    	
    	
    	try{
    	
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try{
    		
    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;
    		
    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
    		
    		try {
    			
    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		try {
    			
    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		while (true) {
    			
    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);		            		
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					break;
    					
    				}
    				
    			} else {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
    					break;
    					
    				}
    				
    			}
    			
    		}		            
    		
    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE,NT_BY ,null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
    		
    		return invAdjustment;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	
    }
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvApprovalControllerBean ejbCreate");
      
    }
}
