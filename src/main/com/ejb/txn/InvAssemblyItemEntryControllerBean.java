
/*
 * InvAssemblyItemEntryControllerBean.java
 *
 * Created on April 8, 2005, 3:07 PM
 *
 * @author  Jolly Martin
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.InvFixedCostAccountNotFoundException;
import com.ejb.exception.InvFreightCostAccountNotFoundException;
import com.ejb.exception.InvLaborCostAccountNotFoundException;
import com.ejb.exception.InvOverHeadCostAccountNotFoundException;
import com.ejb.exception.InvPowerCostAccountNotFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvBuildOrderStock;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModBillOfMaterialDetails;
import com.util.InvModItemDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="InvAssemblyItemEntryControllerEJB"
 *           display-name="Used for entering assembly items"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvAssemblyItemEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvAssemblyItemEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvAssemblyItemEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 */

public class InvAssemblyItemEntryControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomAll(Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getInvUomAll");

		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvUnitOfMeasure invUnitOfMeasure = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection invUnitOfMeasures = invUnitOfMeasureHome.findEnabledUomAll(AD_CMPNY);

			Iterator i = invUnitOfMeasures.iterator();

			while (i.hasNext()) {

				invUnitOfMeasure = (LocalInvUnitOfMeasure)i.next();

				list.add(invUnitOfMeasure.getUomName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	 /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {

    	Debug.print("InvAssemblyItemEntryControllerBean getInvUomByIiName");

        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemHome invItemHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {

        	LocalInvItem invItem = null;
        	LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

        	invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
        	invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

        	Collection invUnitOfMeasures = null;
        	Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
        			invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
        	while (i.hasNext()) {

        		LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
        		InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
        		details.setUomName(invUnitOfMeasure.getUomName());

        		if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

        			details.setDefault(true);

        		}

        		list.add(details);

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArTcAll(Integer AD_CMPNY) {

        Debug.print("ArInvoiceEntryControllerBean getArTcAll");

        LocalArTaxCodeHome arTaxCodeHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
            lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);

            Iterator i = arTaxCodes.iterator();

            while (i.hasNext()) {

                LocalArTaxCode arTaxCode = (LocalArTaxCode)i.next();

                list.add(arTaxCode.getTcName());

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvIiAll(Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getInvIiAll");

		LocalInvItemHome invItemHome = null;
		LocalInvItem invItem = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection invItems = invItemHome.findEnabledIiAll(AD_CMPNY);

			Iterator i = invItems.iterator();

			while (i.hasNext()) {

				invItem = (LocalInvItem)i.next();

				list.add(invItem.getIiName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public Integer saveInvIiEntry(com.util.InvItemDetails details, String UOM_NM, String SPL_SPPLR_CD, String II_RETAIL_UOM_NM, String II_DFLT_LCTN, ArrayList bomList, Integer AD_CMPNY)
	throws GlobalInvItemLocationNotFoundException,
		   GlobalRecordAlreadyExistException,
		   GlobalRecordAlreadyAssignedException,
		   InvLaborCostAccountNotFoundException,
		   InvPowerCostAccountNotFoundException,
		   InvOverHeadCostAccountNotFoundException,
		   InvFreightCostAccountNotFoundException,
		   InvFixedCostAccountNotFoundException,

		   GlobalRecordInvalidException{

		Debug.print("InvAssemblyItemEntryControllerBean saveInvIiEntry");

		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalAdLookUpValueHome adLookUpValueHome = null;
		LocalInvPriceLevelHome invPriceLevelHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

		LocalInvItem invItem = null;
		LocalAdBranchItemLocation adBranchItemLocation = null;

		LocalGlChartOfAccount glChartOfAccount = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;



		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if item already exists

			try {

				invItem = invItemHome.findByIiName(details.getIiName(), AD_CMPNY);

				if(details.getIiCode() == null || details.getIiCode() != null &&
						!invItem.getIiCode().equals(details.getIiCode())) {

					throw new GlobalRecordAlreadyExistException("Assembly Item Name already exists.");

				}

			} catch (GlobalRecordAlreadyExistException ex) {

				throw ex;

			} catch (FinderException ex) {

			}

			LocalGlChartOfAccount glLaborCostAccount = null;
			LocalGlChartOfAccount glPowerCostAccount = null;
			LocalGlChartOfAccount glOverHeadCostAccount = null;
			LocalGlChartOfAccount glFreightCostAccount = null;
			LocalGlChartOfAccount glFixedCostAccount = null;

			/*
    		try {

    			glLaborCostAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getGlCoaLaborCostAccount(), AD_CMPNY);

            } catch (FinderException ex) {

            	throw new InvLaborCostAccountNotFoundException();

            }

    		try {

    			glPowerCostAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getGlCoaPowerCostAccount(), AD_CMPNY);

            } catch (FinderException ex) {

            	throw new InvPowerCostAccountNotFoundException();

            }

    		try {

    			glOverHeadCostAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getGlCoaOverHeadCostAccount(), AD_CMPNY);

            } catch (FinderException ex) {

            	throw new InvOverHeadCostAccountNotFoundException();

            }

    		try {

    			glFreightCostAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getGlCoaFreightCostAccount(), AD_CMPNY);

            } catch (FinderException ex) {

            	throw new InvFreightCostAccountNotFoundException();

            }

    		try {

    			glFixedCostAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getGlCoaFixedCostAccount(), AD_CMPNY);

            } catch (FinderException ex) {

            	throw new InvFixedCostAccountNotFoundException();

            }

			 */

			 Integer retailUom = null;
			 if (II_RETAIL_UOM_NM != null && !II_RETAIL_UOM_NM.equals("")) {
            	 LocalInvUnitOfMeasure invRetailUom = invUnitOfMeasureHome.findByUomName(II_RETAIL_UOM_NM, AD_CMPNY);
            	 retailUom = invRetailUom.getUomCode();
             }

			 Integer defaultLocation = null;
            if (II_DFLT_LCTN != null && !II_DFLT_LCTN.equals("")) {
            	LocalInvLocation invLocation = invLocationHome.findByLocName(II_DFLT_LCTN, AD_CMPNY);
            	defaultLocation = invLocation.getLocCode();
            }

			boolean isRecalculate = false;

			// create new item

			if (details.getIiCode() == null) {

				invItem = invItemHome.create(details.getIiName(), details.getIiDescription(),
						details.getIiPartNumber(), details.getIiShortName(), details.getIiBarCode1(), details.getIiBarCode2(), details.getIiBarCode3(), details.getIiBrand(), details.getIiClass(), details.getIiAdLvCategory(),
						details.getIiCostMethod(), details.getIiUnitCost(), details.getIiSalesPrice(),
						details.getIiEnable(), details.getIiVirtualStore(), details.getIiEnableAutoBuild(), details.getIiDoneness(),
						details.getIiSidings(), details.getIiRemarks(), details.getIiServiceCharge(),
						details.getIiNonInventoriable(), details.getIiServices(), details.getIiJobServices(),  details.getIiIsVatRelief(), details.getIiIsTax(), details.getIiIsProject(),
						details.getIiPercentMarkup(), details.getIiShippingCost(),details.getIiSpecificGravity(), details.getIiStandardFillSize(), details.getIiYield(),
						details.getIiLaborCost(), details.getIiPowerCost(), details.getIiOverHeadCost(), details.getIiFreightCost(), details.getIiFixedCost(),

						glLaborCostAccount != null ? glLaborCostAccount.getCoaCode().toString() : null,
						glPowerCostAccount  != null ? glPowerCostAccount .getCoaCode().toString() : null,
						glOverHeadCostAccount  != null ? glOverHeadCostAccount .getCoaCode().toString() : null,
						glFreightCostAccount  != null ? glFreightCostAccount .getCoaCode().toString() : null,
						glFixedCostAccount  != null ? glFixedCostAccount .getCoaCode().toString() : null,

						details.getIiLossPercentage(),
						details.getIiMarkupValue(),
						details.getIiMarket(), details.getIiEnablePo(), details.getIiPoCycle(),
						details.getIiUmcPackaging(), retailUom, details.getIiOpenProduct(),
						EJBCommon.FALSE, null, defaultLocation, details.getIiTaxCode(),
						details.getIiScSunday(), details.getIiScMonday(), details.getIiScTuesday(), details.getIiScWednesday(), details.getIiScThursday(), details.getIiScFriday(), details.getIiScSaturday(),
						AD_CMPNY);
				invItem.setIiTraceMisc((byte)1);

				// price levels
				Collection adLookUpValues = adLookUpValueHome.findByLuName("INV PRICE LEVEL", AD_CMPNY);

				Iterator i = adLookUpValues.iterator();

				while(i.hasNext()) {

					LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

					LocalInvPriceLevel invPriceLevel = invPriceLevelHome.create(invItem.getIiSalesPrice(), 0d, invItem.getIiPercentMarkup(), invItem.getIiShippingCost(), adLookUpValue.getLvName(), 'N', AD_CMPNY);

					//invItem.addInvPriceLevel(invPriceLevel);
					invPriceLevel.setInvItem(invItem);

				}

				isRecalculate = true;

			} else {

				// update item
				invItem = invItemHome.findByPrimaryKey(details.getIiCode());

				// validate if already assigned
                if(!invItem.getInvUnitOfMeasure().getUomName().equals(UOM_NM)) {

                	Collection invItemLocations = invItem.getInvItemLocations();

                    Iterator locIter = invItemLocations.iterator();

                    while(locIter.hasNext()) {

                        LocalInvItemLocation invItemLocation = (LocalInvItemLocation)locIter.next();

                        if(!invItemLocation.getApVoucherLineItems().isEmpty() || !invItemLocation.getArInvoiceLineItems().isEmpty() ||
                                !invItemLocation.getInvAdjustmentLines().isEmpty() || !invItemLocation.getInvBuildOrderLines().isEmpty() ||
                                !invItemLocation.getInvBuildUnbuildAssemblyLines().isEmpty() || !invItemLocation.getInvCostings().isEmpty() ||
                                !invItemLocation.getInvPhysicalInventoryLines().isEmpty() ||
    							!invItemLocation.getInvBranchStockTransferLines().isEmpty() || !invItemLocation.getInvStockIssuanceLines().isEmpty() ||
								!invItem.getInvBuildOrderStocks().isEmpty() ||
								!invItem.getInvStockTransferLines().isEmpty()) {

                            throw new GlobalRecordAlreadyAssignedException();

                        }

                    }

                }

                // validate if bom already assigned
                Iterator bomItr = bomList.iterator();

                while (bomItr.hasNext()) {

                	InvModBillOfMaterialDetails bomDetails = (InvModBillOfMaterialDetails) bomItr.next();

                	if(bomDetails.getBomIiName().equals(invItem.getIiName()))
                		throw new GlobalRecordInvalidException(bomDetails.getBomIiName());

                    try {

                        LocalInvBillOfMaterial invBillOfMaterial =  invBillOfMaterialHome.findByBomIiNameAndAssemblyItem(bomDetails.getBomIiName(), invItem.getIiName(), AD_CMPNY);

                        LocalInvItem bomItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);

                        if(!invBillOfMaterial.getInvUnitOfMeasure().getUomName().equals(bomDetails.getBomUomName())) {

                            if(!bomItem.getInvBuildOrderStocks().isEmpty()) {

                                Iterator bosItr = bomItem.getInvBuildOrderStocks().iterator();

                                while (bosItr.hasNext()) {

                                    LocalInvBuildOrderStock invBuildOrderStock = (LocalInvBuildOrderStock) bosItr.next();

                                    if(invBuildOrderStock.getInvBuildOrderLine().getBolQuantityRequired() > invBuildOrderStock.getInvBuildOrderLine().getBolQuantityAssembled())
                                        throw new GlobalRecordAlreadyAssignedException();

                                }

                            }

                        }

                    } catch(FinderException ex) {

                    }

                }

                if(!invItem.getInvUnitOfMeasure().getUomName().equals(UOM_NM))
					isRecalculate = true;

				if(invItem.getInvUnitOfMeasureConversions().isEmpty())
					isRecalculate = true;

				double oldUnitCost = invItem.getIiUnitCost();

                // update bill of materials
                Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invItem.getIiName(), AD_CMPNY);
                Iterator bomIter = invBillOfMaterials.iterator();

                while (bomIter.hasNext()) {

                	LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) bomIter.next();
                	invBillOfMaterial.setBomIiName(details.getIiName());

                }

				invItem.setIiName(details.getIiName());
				invItem.setIiDescription(details.getIiDescription());
				invItem.setIiPartNumber(details.getIiPartNumber());
				invItem.setIiShortName(details.getIiShortName());
				invItem.setIiBarCode1(details.getIiBarCode1());
				invItem.setIiBarCode2(details.getIiBarCode2());
				invItem.setIiBarCode3(details.getIiBarCode3());
				invItem.setIiClass(details.getIiClass());
				invItem.setIiAdLvCategory(details.getIiAdLvCategory());
				invItem.setIiCostMethod(details.getIiCostMethod());
				invItem.setIiUnitCost(details.getIiUnitCost());
				invItem.setIiSalesPrice(details.getIiSalesPrice());
				invItem.setIiEnable(details.getIiEnable());
				invItem.setIiVirtualStore(details.getIiVirtualStore());
				invItem.setIiEnableAutoBuild(details.getIiEnableAutoBuild());
				invItem.setIiDoneness(details.getIiDoneness());
				invItem.setIiSidings(details.getIiSidings());
				invItem.setIiRemarks(details.getIiRemarks());
				invItem.setIiServiceCharge(details.getIiServiceCharge());
				invItem.setIiNonInventoriable(details.getIiNonInventoriable());
				invItem.setIiServices(details.getIiServices());
				invItem.setIiJobServices(details.getIiJobServices());
				invItem.setIiIsVatRelief(details.getIiIsVatRelief());
				invItem.setIiPercentMarkup(details.getIiPercentMarkup());
				invItem.setIiShippingCost(details.getIiShippingCost());
				invItem.setIiSpecificGravity(details.getIiSpecificGravity());
				invItem.setIiStandardFillSize(details.getIiStandardFillSize());
				invItem.setIiYield(details.getIiYield());

				invItem.setIiLaborCost(details.getIiLaborCost());
				invItem.setIiPowerCost(details.getIiPowerCost());
				invItem.setIiOverHeadCost(details.getIiOverHeadCost());
				invItem.setIiFreightCost(details.getIiFreightCost());
				invItem.setIiFixedCost(details.getIiFixedCost());

				invItem.setGlCoaLaborCostAccount(glLaborCostAccount != null ? glLaborCostAccount.getCoaCode().toString() : null);
				invItem.setGlCoaPowerCostAccount(glPowerCostAccount  != null ? glPowerCostAccount .getCoaCode().toString() : null);
				invItem.setGlCoaOverHeadCostAccount(glOverHeadCostAccount  != null ? glOverHeadCostAccount .getCoaCode().toString() : null);
				invItem.setGlCoaFreightCostAccount(glFreightCostAccount  != null ? glFreightCostAccount .getCoaCode().toString() : null);
				invItem.setGlCoaFixedCostAccount(glFixedCostAccount  != null ? glFixedCostAccount .getCoaCode().toString() : null);


				invItem.setIiLossPercentage(details.getIiLossPercentage());
				invItem.setIiMarket(details.getIiMarket());
				invItem.setIiEnablePo(details.getIiEnablePo());
				invItem.setIiPoCycle(details.getIiPoCycle());
				invItem.setIiUmcPackaging(details.getIiUmcPackaging());
				invItem.setIiRetailUom(retailUom);
				invItem.setIiOpenProduct(details.getIiOpenProduct());
				invItem.setIiDefaultLocation(defaultLocation);
				invItem.setIiTaxCode(details.getIiTaxCode());

				invItem.setIiScSunday(details.getIiScSunday());
                invItem.setIiScMonday(details.getIiScMonday());
                invItem.setIiScTuesday(details.getIiScTuesday());
                invItem.setIiScWednesday(details.getIiScWednesday());
                invItem.setIiScThursday(details.getIiScThursday());
                invItem.setIiScFriday(details.getIiScFriday());
                invItem.setIiScSaturday(details.getIiScSaturday());

				this.updateAssemblyIiUnitCost(invItem.getIiName(), oldUnitCost, invItem.getIiUnitCost(), AD_CMPNY);

				try{
		            LocalInvItemLocation invItemLocation = null;

		            Collection invItemLocations = invItemLocationHome.findByIiName(details.getIiName(), AD_CMPNY);

		            Iterator iterIl = invItemLocations.iterator();

		            while(iterIl.hasNext()){

		            	invItemLocation = (LocalInvItemLocation)iterIl.next();

		            	Collection adBranchItemLocations = adBranchItemLocationHome.findByInvIlAll(invItemLocation.getIlCode(), AD_CMPNY);

		            	Iterator iterBil = adBranchItemLocations.iterator();
		            	while(iterBil.hasNext()){
		            		adBranchItemLocation = (LocalAdBranchItemLocation)iterBil.next();

		            		if (adBranchItemLocation.getBilItemDownloadStatus()=='N'){
		            			adBranchItemLocation.setBilItemDownloadStatus('N');
		            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='D'){
		            			adBranchItemLocation.setBilItemDownloadStatus('X');
		            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='U'){
		            			adBranchItemLocation.setBilItemDownloadStatus('U');
		            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='X'){
		            			adBranchItemLocation.setBilItemDownloadStatus('X');
		            		}

		            	}

		            }
                }catch (FinderException ex){

                }

			}
			System.out.println("1");
			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);
			invUnitOfMeasure.addInvItem(invItem);
			System.out.println("2");

			try {

				LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CD, AD_CMPNY);
				apSupplier.addInvItem(invItem);

			} catch (FinderException ex) {

				if (invItem.getApSupplier() != null) invItem.getApSupplier().dropInvItem(invItem);

			}

			//>>>>------->
//			 set download status
			InvModBillOfMaterialDetails bomDetails=null;

			Iterator bomIter = bomList.iterator();

			while (bomIter.hasNext()){

				bomDetails = (InvModBillOfMaterialDetails)bomIter.next();

				LocalInvBillOfMaterial invBillOfMaterial=null;

				try{
					invBillOfMaterial = invBillOfMaterialHome.findByBomIiNameAndAssemblyItem(bomDetails.getBomIiName(),invItem.getIiName(),AD_CMPNY);
				} catch (FinderException ex){

				}

				if(invBillOfMaterial==null){
					System.out.println("YES");
					bomDetails.setBomDownloadStatus('N');
				}else {
					System.out.println("NO");
					if (invBillOfMaterial.getBomDownloadStatus()=='N'){
						bomDetails.setBomDownloadStatus('N');
					}else if (invBillOfMaterial.getBomDownloadStatus()=='D'){
						bomDetails.setBomDownloadStatus('X');
					}else if (invBillOfMaterial.getBomDownloadStatus()=='U'){
						bomDetails.setBomDownloadStatus('U');
					}else if (invBillOfMaterial.getBomDownloadStatus()=='X'){
						bomDetails.setBomDownloadStatus('X');
					}
					System.out.println("3");

				}

			}


			// remove all bill of materials

			Collection billOfMaterials = invItem.getInvBillOfMaterials();

			Iterator i = billOfMaterials.iterator();

			while (i.hasNext()) {

				LocalInvBillOfMaterial billOfMaterial = (LocalInvBillOfMaterial)i.next();

				i.remove();

				billOfMaterial.remove();

			}

			// add new bill of materials

			i = bomList.iterator();

			while (i.hasNext()) {

				//InvModBillOfMaterialDetails
				bomDetails = (InvModBillOfMaterialDetails) i.next();

				try {

					LocalInvItemLocation invItemLocation =  invItemLocationHome.findByLocNameAndIiName(bomDetails.getBomLocName(),bomDetails.getBomIiName(), AD_CMPNY);

				} catch(FinderException ex) {

					throw new GlobalInvItemLocationNotFoundException(bomDetails.getBomIiName() + " - " + bomDetails.getBomLocName());

				}

				this.addInvBomEntry(bomDetails, invItem, AD_CMPNY);

			}


			if(isRecalculate) {

				// remove all umc by item

				Collection unitOfMeasureConversions = invItem.getInvUnitOfMeasureConversions();

				i = unitOfMeasureConversions.iterator();

				while (i.hasNext()) {

					LocalInvUnitOfMeasureConversion unitOfMeasureConversion = (LocalInvUnitOfMeasureConversion)i.next();

					i.remove();

					unitOfMeasureConversion.remove();

				}

				// add new umc

				Collection invUnitOfMeasures = invUnitOfMeasureHome.findByUomAdLvClass(invUnitOfMeasure.getUomAdLvClass(), AD_CMPNY);

				i = invUnitOfMeasures.iterator();

				while(i.hasNext()) {

					LocalInvUnitOfMeasure unitOfMeasure = (LocalInvUnitOfMeasure) i.next();

					this.addInvUmcEntry(invItem, unitOfMeasure.getUomName(), unitOfMeasure.getUomAdLvClass(), unitOfMeasure.getUomConversionFactor(), unitOfMeasure.getUomBaseUnit(), AD_CMPNY);

				}

			}

			return invItem.getIiCode();

		} catch	(GlobalInvItemLocationNotFoundException ex){

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyAssignedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordInvalidException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteInvIiEntry(Integer II_CODE, Integer AD_CMPNY)
	throws GlobalRecordAlreadyDeletedException,
	GlobalRecordAlreadyAssignedException {

		Debug.print("InvAssemblyItemEntryControllerBean deleteInvIiEntry");

		LocalInvItemHome invItemHome = null;
		LocalInvBillOfMaterialHome invBillOfMaterialHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

			invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalInvItem invItem = null;

			try {

				invItem = invItemHome.findByPrimaryKey(II_CODE);

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// if already in used in buildorder stock & stock transfers
			if (!invItem.getInvBuildOrderStocks().isEmpty()||
			        !invItem.getInvStockTransferLines().isEmpty()) {

			    throw new GlobalRecordAlreadyAssignedException();

			}

			// if already in used for assembly items

			Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invItem.getIiName(), AD_CMPNY);

			if(!invBillOfMaterials.isEmpty()) throw new GlobalRecordAlreadyAssignedException();


			// if item location is already assigned
			if (!invItem.getInvItemLocations().isEmpty()) {

				Collection itemLocations = invItem.getInvItemLocations();

				Iterator locIter = itemLocations.iterator();

				while(locIter.hasNext()) {

					LocalInvItemLocation itemLocation = (LocalInvItemLocation)locIter.next();

					if(!itemLocation.getApVoucherLineItems().isEmpty() ||
                            !itemLocation.getArInvoiceLineItems().isEmpty() ||
                            !itemLocation.getInvAdjustmentLines().isEmpty() ||
                            !itemLocation.getInvBuildOrderLines().isEmpty() ||
                            !itemLocation.getInvBuildUnbuildAssemblyLines().isEmpty() ||
                            !itemLocation.getInvCostings().isEmpty() ||
                            !itemLocation.getInvPhysicalInventoryLines().isEmpty() ||
							!itemLocation.getInvBranchStockTransferLines().isEmpty() ||
							!itemLocation.getInvStockIssuanceLines().isEmpty() ||
							!itemLocation.getApPurchaseOrderLines().isEmpty() ||
							!itemLocation.getApPurchaseRequisitionLines().isEmpty()||
                        	!itemLocation.getAdBranchItemLocations().isEmpty()||
                        	!itemLocation.getArSalesOrderLines().isEmpty()||
                        	!itemLocation.getInvLineItems().isEmpty()) {

						throw new GlobalRecordAlreadyAssignedException();

					}

                    // branch item locations
                    Collection adBranchItemLocations = itemLocation.getAdBranchItemLocations();

                    Iterator bilIter = adBranchItemLocations.iterator();

                    while(bilIter.hasNext()) {

                        LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)bilIter.next();

                        // if already downloaded to pos
            	        if (adBranchItemLocation.getBilItemDownloadStatus()=='D' || adBranchItemLocation.getBilItemDownloadStatus()=='X' ||
            	                adBranchItemLocation.getBilItemLocationDownloadStatus()=='D' || adBranchItemLocation.getBilItemLocationDownloadStatus()=='X' ||
            	                adBranchItemLocation.getBilLocationDownloadStatus()=='D' || adBranchItemLocation.getBilLocationDownloadStatus()=='X'){

            	            throw new GlobalRecordAlreadyAssignedException();

            	        }

                    }

					// remove item location
					locIter.remove();
					itemLocation.remove();

				}
			}

			// remove all bill of materials

			Collection billOfMaterials = invItem.getInvBillOfMaterials();
			Iterator j = billOfMaterials.iterator();

			while (j.hasNext()) {

				LocalInvBillOfMaterial billOfMaterial = (LocalInvBillOfMaterial)j.next();
				j.remove();
				billOfMaterial.remove();

			}

			invItem.remove();

		} catch (GlobalRecordAlreadyDeletedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyAssignedException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public InvModItemDetails getInvIiByIiCode(Integer II_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException  {

		Debug.print("InvAssemblyItemEntryControllerBean getInvIiByIiCode");

		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvLocationHome invLocationHome = null;

		LocalInvItem invItem = null;

		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalGlChartOfAccount glChartOfAccount = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
					lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			try {

				System.out.println("II_CODE="+II_CODE);

				invItem = invItemHome.findByPrimaryKey(II_CODE);

			} catch (FinderException ex) {
				System.out.println("1");
				throw new GlobalNoRecordFoundException();

			}

			ArrayList iiBomList = new ArrayList();

			// get bill of materials

			Collection billOfMaterials = invItem.getInvBillOfMaterials();
			System.out.println("2="+billOfMaterials.size());
			Iterator i = billOfMaterials.iterator();

			while (i.hasNext()) {

				LocalInvBillOfMaterial billOfMaterial = (LocalInvBillOfMaterial)i.next();

				InvModBillOfMaterialDetails bomDetails = new InvModBillOfMaterialDetails();
				LocalInvItem invBOM = null;
				try {
					System.out.println("AD_CMPNY="+AD_CMPNY);
					System.out.println("3="+billOfMaterial.getBomIiName());
					invBOM = invItemHome.findByIiName(billOfMaterial.getBomIiName(), AD_CMPNY);

				} catch (FinderException ex) {
					System.out.println("4");
					throw new GlobalNoRecordFoundException();

				}
				bomDetails.setBomIiItemCategory(invBOM.getIiAdLvCategory());
				bomDetails.setBomCode(billOfMaterial.getBomCode());
				bomDetails.setBomIiName(billOfMaterial.getBomIiName());
				bomDetails.setBomLocName(billOfMaterial.getBomLocName());
				bomDetails.setBomQuantityNeeded(billOfMaterial.getBomQuantityNeeded());
				System.out.println("QNN="+billOfMaterial.getBomQuantityNeeded());
				bomDetails.setBomUomName(billOfMaterial.getInvUnitOfMeasure().getUomName());

				iiBomList.add(bomDetails);

			}

			InvModItemDetails iiDetails = new InvModItemDetails();
			iiDetails.setIiCode(invItem.getIiCode());
			iiDetails.setIiName(invItem.getIiName());
			iiDetails.setIiDescription(invItem.getIiDescription());
			iiDetails.setIiPartNumber(invItem.getIiPartNumber());
			iiDetails.setIiShortName(invItem.getIiShortName());
			iiDetails.setIiBarCode1(invItem.getIiBarCode1());
			iiDetails.setIiBarCode2(invItem.getIiBarCode2());
			iiDetails.setIiBarCode3(invItem.getIiBarCode3());
			iiDetails.setIiClass(invItem.getIiClass());
			iiDetails.setIiAdLvCategory(invItem.getIiAdLvCategory());
			iiDetails.setIiCostMethod(invItem.getIiCostMethod());
			iiDetails.setIiPercentMarkup(invItem.getIiPercentMarkup());
			iiDetails.setIiShippingCost(invItem.getIiShippingCost());
			iiDetails.setIiSpecificGravity(invItem.getIiSpecificGravity());
			iiDetails.setIiStandardFillSize(invItem.getIiStandardFillSize());
			iiDetails.setIiYield(invItem.getIiYield());

			iiDetails.setIiLaborCost(invItem.getIiLaborCost());
			iiDetails.setIiPowerCost(invItem.getIiPowerCost());
			iiDetails.setIiOverHeadCost(invItem.getIiOverHeadCost());
			iiDetails.setIiFreightCost(invItem.getIiFreightCost());
			iiDetails.setIiFixedCost(invItem.getIiFixedCost());

			if(invItem.getGlCoaLaborCostAccount()!=null){
				try {

					glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(Integer.parseInt(invItem.getGlCoaLaborCostAccount()));

				} catch (FinderException ex){

					throw new GlobalNoRecordFoundException();

				}

				iiDetails.setLaborCostAccount(glChartOfAccount.getCoaAccountNumber());
				iiDetails.setLaborCostAccountDescription(glChartOfAccount.getCoaAccountDescription());
			}


			if(invItem.getGlCoaPowerCostAccount()!=null){
				try {

					glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(Integer.parseInt(invItem.getGlCoaPowerCostAccount()));

				} catch (FinderException ex){

					throw new GlobalNoRecordFoundException();

				}

				iiDetails.setPowerCostAccount(glChartOfAccount.getCoaAccountNumber());
				iiDetails.setPowerCostAccountDescription(glChartOfAccount.getCoaAccountDescription());
			}

			if(invItem.getGlCoaOverHeadCostAccount()!=null){
				try {

					glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(Integer.parseInt(invItem.getGlCoaOverHeadCostAccount()));

				} catch (FinderException ex){

					throw new GlobalNoRecordFoundException();

				}

				iiDetails.setOverHeadCostAccount(glChartOfAccount.getCoaAccountNumber());
				iiDetails.setOverHeadCostAccountDescription(glChartOfAccount.getCoaAccountDescription());
			}

			if(invItem.getGlCoaFreightCostAccount()!=null){
				try {

					glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(Integer.parseInt(invItem.getGlCoaFreightCostAccount()));

				} catch (FinderException ex){

					throw new GlobalNoRecordFoundException();

				}

				iiDetails.setFreightCostAccount(glChartOfAccount.getCoaAccountNumber());
				iiDetails.setFreightCostAccountDescription(glChartOfAccount.getCoaAccountDescription());
			}

			if(invItem.getGlCoaFixedCostAccount()!=null){
				try {

					glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(Integer.parseInt(invItem.getGlCoaFixedCostAccount()));

				} catch (FinderException ex){

					throw new GlobalNoRecordFoundException();

				}

				iiDetails.setFixedCostAccount(glChartOfAccount.getCoaAccountNumber());
				iiDetails.setFixedCostAccountDescription(glChartOfAccount.getCoaAccountDescription());
			}


			iiDetails.setIiLossPercentage(invItem.getIiLossPercentage());
			iiDetails.setIiUnitCost(invItem.getIiUnitCost());
			iiDetails.setIiAveCost(this.getIiAveCost(invItem, new Date(), AD_CMPNY));
			iiDetails.setIiSalesPrice(invItem.getIiSalesPrice());
			iiDetails.setIiEnable(invItem.getIiEnable());
			iiDetails.setIiEnableAutoBuild(invItem.getIiEnableAutoBuild());
			iiDetails.setIiBomList(iiBomList);

			iiDetails.setIiUomName(invItem.getInvUnitOfMeasure() != null ?
					invItem.getInvUnitOfMeasure().getUomName() :
						null);

			iiDetails.setIiDoneness(invItem.getIiDoneness());
			iiDetails.setIiSidings(invItem.getIiSidings());
			iiDetails.setIiRemarks(invItem.getIiRemarks());
			iiDetails.setIiServiceCharge(invItem.getIiServiceCharge());
			iiDetails.setIiNonInventoriable(invItem.getIiNonInventoriable());
			iiDetails.setIiServices(invItem.getIiServices());
			iiDetails.setIiJobServices(invItem.getIiJobServices());
			iiDetails.setIiIsVatRelief(invItem.getIiIsVatRelief());
			iiDetails.setIiMarket(invItem.getIiMarket());
			iiDetails.setIiEnablePo(invItem.getIiEnablePo());
			iiDetails.setIiPoCycle(invItem.getIiPoCycle());
			iiDetails.setIiUmcPackaging(invItem.getIiUmcPackaging());
			iiDetails.setIiOpenProduct(invItem.getIiOpenProduct());
			iiDetails.setIiTaxCode(invItem.getIiTaxCode());

			iiDetails.setIiScSunday(invItem.getIiScSunday());
  			iiDetails.setIiScMonday(invItem.getIiScMonday());
  			iiDetails.setIiScTuesday(invItem.getIiScTuesday());
  			iiDetails.setIiScWednesday(invItem.getIiScWednesday());
  			iiDetails.setIiScThursday(invItem.getIiScThursday());
  			iiDetails.setIiScFriday(invItem.getIiScFriday());
  			iiDetails.setIiScSaturday(invItem.getIiScSaturday());
  			iiDetails.setIiSplSpplrCode(invItem.getApSupplier() != null ? invItem.getApSupplier().getSplSupplierCode() : null);

			String retailUom = null;
			if (invItem.getIiRetailUom() != null) {
				LocalInvUnitOfMeasure invRetailUom = invUnitOfMeasureHome.findByPrimaryKey(invItem.getIiRetailUom());
				retailUom = invRetailUom.getUomName();
			}
			iiDetails.setIiRetailUomName(retailUom);

			String defaultLocation = null;
			if (invItem.getIiDefaultLocation() != null) {
				LocalInvLocation invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
				defaultLocation = invLocation.getLocName();
			}
			iiDetails.setIiDefaultLocationName(defaultLocation);

			return iiDetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfInvInventoryLineNumber(Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getAdPrfInvInventoryLineNumber");

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvInventoryLineNumber();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfInvQuantityPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getAdPrfInvQuantityPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvQuantityPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfInvCostPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getAdPrfInvCostPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvCostPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getAdLvInvItemCategoryAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public double getInvIiUnitCostByIiNameAndUomName(String II_NM, String UOM_NM, Integer AD_CMPNY) {

        Debug.print("InvAssemblyItemEntryControllerBean getInvIiUnitCostByIiNameAndUomName");

        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

        }

        try {

        	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(invItem.getIiUnitCost() * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

        } catch (Exception ex) {

           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());

        }

     }

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getIiUnitCostByIiName(String II_NM, Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getIiUnitCostByIiName");

		LocalInvItemHome invItemHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

			return invItem.getIiUnitCost();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getGlFcPrecisionUnit");

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	  * @ejb:interface-method view-type="remote"
	  * @jboss:method-attributes read-only="true"
	  **/
	public void updateAssemblyIiUnitCost(String itemName, double oldIiUnitCost, double iiUnitCost, Integer AD_CMPNY) {

	 	Debug.print("InvItemEntryControllerBean updateAssemblyIiUnitCost");

	 	LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
	 	LocalInvItemHome invItemHome = null;
	 	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

	 	LocalInvBillOfMaterial invBillOfMaterial = null;

	 	// Initialize EJB Home

	 	try {

	 		invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
	 		invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
	 		invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

	 	} catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

	 	}

	 	try {

	 		short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

	 		LocalInvItem invBomItem = invItemHome.findByIiName(itemName, AD_CMPNY);

			Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invBomItem.getIiName(), AD_CMPNY);

	 		if(!invBillOfMaterials.isEmpty()) {

	 			Iterator i = invBillOfMaterials.iterator();

	 			while (i.hasNext()) {

	 				invBillOfMaterial = (LocalInvBillOfMaterial)i.next();

	 				double quantityNeeded = invBillOfMaterial.getBomQuantityNeeded();
	 				double totalUnitCost = invBillOfMaterial.getInvItem().getIiUnitCost();
	 				double oldTotalUnitCost = totalUnitCost;

	 				LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(
	 						invBomItem.getIiName(), invBillOfMaterial.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
	 	        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(
	 	        			invBomItem.getIiName(), invBomItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

	 	        	oldIiUnitCost = EJBCommon.roundIt(oldIiUnitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	        	iiUnitCost = EJBCommon.roundIt(iiUnitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

	 	        	totalUnitCost = EJBCommon.roundIt(totalUnitCost - (oldIiUnitCost * quantityNeeded), precisionUnit);
	 	        	totalUnitCost = EJBCommon.roundIt(totalUnitCost + (iiUnitCost * quantityNeeded), precisionUnit);

	 				invBillOfMaterial.getInvItem().setIiUnitCost(totalUnitCost);

	 				this.updateAssemblyIiUnitCost(invBillOfMaterial.getInvItem().getIiName(), oldTotalUnitCost, invBillOfMaterial.getInvItem().getIiUnitCost(), AD_CMPNY);

	 			}

	 		}

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	 }

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public String getIiUnitMeasureByIiName(String II_NM, Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getIiUnitMeasureByIiName");

		LocalInvItemHome invItemHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

			return invItem.getInvUnitOfMeasure().getUomName();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public String getIiDescriptionByIiName(String II_NM, Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getIiDescriptionByIiName");

		LocalInvItemHome invItemHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

			return invItem.getIiDescription();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getInvLocAll");

		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			invLocations = invLocationHome.findLocAll(AD_CMPNY);

			if (invLocations.isEmpty()) {

				return null;

			}

			Iterator i = invLocations.iterator();

			while (i.hasNext()) {

				LocalInvLocation invLocation = (LocalInvLocation)i.next();
				String details = invLocation.getLocName();

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
	  * @ejb:interface-method view-type="remote"
	  * @jboss:method-attributes read-only="true"
	  **/
	 public ArrayList getApSplAll(Integer AD_CMPNY) {

	 	Debug.print("InvAssemblyItemEntryControllerBean getApSplAll");

	 	LocalApSupplierHome apSupplierHome = null;

	 	ArrayList list = new ArrayList();

	 	// Initialize EJB Home

	 	try {

	 		apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

	 	} catch (NamingException ex) {

	 		throw new EJBException(ex.getMessage());

	 	}

	 	try {

	 		Collection apSuppliers = apSupplierHome.findEnabledSplAllOrderBySplSupplierCode(AD_CMPNY);

	 		Iterator i = apSuppliers.iterator();

	 		while (i.hasNext()) {

	 			LocalApSupplier apSupplier = (LocalApSupplier)i.next();

	 			list.add(apSupplier.getSplSupplierCode());

	 		}

	 		return list;

	 	} catch (Exception ex) {

	 		Debug.printStackTrace(ex);
	 		throw new EJBException(ex.getMessage());

	 	}

	 }


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomAllByUnitMeasureClass(String UOM_NM, Integer AD_CMPNY) {

		Debug.print("InvItemEntryControllerBean getInvUomAllByUnitMeasureClass");

		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

		    LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);

		    Collection invUnitOfMeasures = invUnitOfMeasureHome.findByUomAdLvClass(invUnitOfMeasure.getUomAdLvClass(), AD_CMPNY);

			Iterator i = invUnitOfMeasures.iterator();

			while(i.hasNext()) {

				LocalInvUnitOfMeasure unitOfMeasure = (LocalInvUnitOfMeasure) i.next();

				list.add(unitOfMeasure.getUomName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	  * @ejb:interface-method view-type="remote"
	  * @jboss:method-attributes read-only="true"
	  **/
	 public ArrayList getAdLvInvDonenessAll(Integer AD_CMPNY) {

	 	Debug.print("InvItemEntryControllerBean getAdLvInvDonenessAll");

	 	LocalAdLookUpValueHome adLookUpValueHome = null;

	 	ArrayList list = new ArrayList();

	 	// Initialize EJB Home

	 	try {

	 		adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

	 	} catch (NamingException ex) {

	 		throw new EJBException(ex.getMessage());

	 	}

	 	try {

	 		Collection adLookUpValues = adLookUpValueHome.findByLuName("INV DONENESS", AD_CMPNY);

	 		Iterator i = adLookUpValues.iterator();

	 		while (i.hasNext()) {

	 			LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

	 			list.add(adLookUpValue.getLvName());

	 		}

	 		return list;

	 	} catch (Exception ex) {

	 		Debug.printStackTrace(ex);
	 		throw new EJBException(ex.getMessage());

	 	}

	 }

	 // private

	 private double getIiAveCost(LocalInvItem invItem, Date currentDate, Integer AD_CMPNY) {

		Debug.print("InvAssemblyItemEntryControllerBean getIiMarkupCost");

		LocalInvCostingHome invCostingHome = null;

		try {

			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			double TOTAL_COST = 0;
			int ctr = 0;

			// get all branch

			ArrayList branchList = this.getAdBrAll(AD_CMPNY);

			// get all item locations

			Collection invItemLocations = invItem.getInvItemLocations();

			if (invItemLocations.size() == 0) return invItem.getIiUnitCost();

			Iterator ilItr = invItemLocations.iterator();

			while (ilItr.hasNext()) {

				LocalInvItemLocation invItemLocation = (LocalInvItemLocation)ilItr.next();

				// get all item costings

				Iterator brItr = branchList.iterator();

				while (brItr.hasNext()) {

					Integer BR_CODE = (Integer)brItr.next();

					double COST = 0;

					try {

						LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
								currentDate, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), BR_CODE, AD_CMPNY);

						COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

					} catch (FinderException ex) {

						//COST = invItemLocation.getInvItem().getIiUnitCost();

					}

					TOTAL_COST += COST;
					if (COST != 0) ctr++;

				}

			}

			if (ctr != 0)
			    return TOTAL_COST / ctr; else return 0;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	 private ArrayList getAdBrAll(Integer AD_CMPNY) {

        Debug.print("InvAssemblyItemEntryControllerBean getAdBrAll");

        LocalAdBranchHome adBranchHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection adBranches = adBranchHome.findBrAll(AD_CMPNY);

        	Iterator i = adBranches.iterator();

        	while (i.hasNext()) {

        		LocalAdBranch adBranch = (LocalAdBranch)i.next();

        		list.add(adBranch.getBrCode());

        	}

        	return list;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());

        }

    }

	private void addInvBomEntry(InvModBillOfMaterialDetails details, LocalInvItem invItem, Integer AD_CMPNY) throws GlobalRecordAlreadyExistException {

		Debug.print("InvAssemblyItemEntryControllerBean addInvBomEntry");

		LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

		// Initialize EJB Home

		try {

			invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

		    LocalInvBillOfMaterial existInvBillOfMaterial = null;

		    try {

		        existInvBillOfMaterial = invBillOfMaterialHome.findByBomIiNameAndAssemblyItem(details.getBomIiName(), invItem.getIiName(), AD_CMPNY);

		    } catch (FinderException ex) {

		    }

		    if(existInvBillOfMaterial != null)
		        throw new GlobalRecordAlreadyExistException("BOM Item Name "+ details.getBomIiName() +" already exists.");

		    // create bill of material

			LocalInvBillOfMaterial invBillOfMaterial = invBillOfMaterialHome.create(
					details.getBomIiName(), details.getBomLocName(), details.getBomQuantityNeeded(), details.getBomDownloadStatus(), AD_CMPNY);

			invItem.addInvBillOfMaterial(invBillOfMaterial);

			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(details.getBomUomName(),
					AD_CMPNY);

			invUnitOfMeasure.addInvBillOfMaterial(invBillOfMaterial);

		} catch (GlobalRecordAlreadyExistException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}


	private void addInvUmcEntry(LocalInvItem invItem,  String uomName, String uomAdLvClass, double conversionFactor, byte umcBaseUnit, Integer AD_CMPNY) throws GlobalNoRecordFoundException {

	    Debug.print("InvUnitOfMeasureConversionControllerBean addInvUmcEntry");

	    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
	    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
	    LocalInvItemHome invItemHome = null;

	    // initialize EJB

	    try{

	        invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
	        lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
	        invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
	        lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
	        invItemHome = (LocalInvItemHome)EJBHomeFactory.
	        lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

	    } catch (NamingException ex){

	        throw new EJBException(ex.getMessage());

	    }

	    try {

	        // create umc
	        LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.create(conversionFactor, umcBaseUnit, AD_CMPNY);

	        try {

	            // map uom
	            LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomNameAndUomAdLvClass(uomName, uomAdLvClass, AD_CMPNY);
	            invUnitOfMeasureConversion.setInvUnitOfMeasure(invUnitOfMeasure);

	            // invUnitOfMeasure.addInvUnitOfMeasureConversion(invUnitOfMeasureConversion);

	        } catch (FinderException ex) {

	            throw new GlobalNoRecordFoundException();

	        }

	        // map item
	        invItem.addInvUnitOfMeasureConversion(invUnitOfMeasureConversion);

	    } catch (GlobalNoRecordFoundException ex) {

	        getSessionContext().setRollbackOnly();
	        throw ex;

	    } catch (Exception ex) {

	        Debug.printStackTrace(ex);
	        throw new EJBException(ex.getMessage());

	    }

	}

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("InvAssemblyItemEntryControllerBean ejbCreate");

	}

}