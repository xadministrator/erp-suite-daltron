package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.InvATRAssemblyQtyGreaterThanAvailableQtyException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvAssemblyTransfer;
import com.ejb.inv.LocalInvAssemblyTransferHome;
import com.ejb.inv.LocalInvAssemblyTransferLine;
import com.ejb.inv.LocalInvAssemblyTransferLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvBuildOrderLine;
import com.ejb.inv.LocalInvBuildOrderLineHome;
import com.ejb.inv.LocalInvBuildOrderStock;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvStockIssuanceLine;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModAssemblyTransferDetails;
import com.util.InvModAssemblyTransferLineDetails;
import com.util.InvModBuildOrderLineDetails;


/**
 * @ejb:bean name="InvAssemblyTransferEntryControllerEJB"
 *           display-name="used to add assembly transfers"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvAssemblyTransferEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvAssemblyTransferEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvAssemblyTransferEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvAssemblyTransferEntryControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModAssemblyTransferDetails getInvAtrByAtrCode(Integer ATR_CODE, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
        
        Debug.print("InvAssemblyTransferEntryControllerBean getInvAtrByAtrCode");
        
        LocalInvAssemblyTransferHome invAssemblyTransferHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invAssemblyTransferHome = (LocalInvAssemblyTransferHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAssemblyTransferHome.JNDI_NAME, LocalInvAssemblyTransferHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvAssemblyTransfer invAssemblyTransfer = null;
            
            try {
                
                invAssemblyTransfer = invAssemblyTransferHome.findByPrimaryKey(ATR_CODE);
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
                
            }
            
            ArrayList atlList = new ArrayList();
            
            Collection invAssemblyTransferLines = invAssemblyTransfer.getInvAssemblyTransferLines();
            
            Iterator i = invAssemblyTransferLines.iterator();
            
            while (i.hasNext()) {
                
                LocalInvAssemblyTransferLine invAssemblyTransferLine = (LocalInvAssemblyTransferLine)i.next();
                
                InvModAssemblyTransferLineDetails mdetails = new InvModAssemblyTransferLineDetails();
                
                mdetails.setAtlCode(invAssemblyTransferLine.getAtlCode());
                mdetails.setAtlAssembleQuantity(invAssemblyTransferLine.getAtlAssembleQuantity());
                mdetails.setAtlBolQtyRequired(invAssemblyTransferLine.getInvBuildOrderLine().getBolQuantityRequired());
                mdetails.setAtlBolQtyAssembled(invAssemblyTransferLine.getInvBuildOrderLine().getBolQuantityAssembled());
                mdetails.setAtlBolIlIiName(invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvItem().getIiName());
                mdetails.setAtlBolIlLocName(invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvLocation().getLocName());
                mdetails.setAtlBolIlIiUomName(invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
                mdetails.setAtlBolBorDcmntNumber(invAssemblyTransferLine.getInvBuildOrderLine().getInvBuildOrder().getBorDocumentNumber());
                mdetails.setAtlAssembleCost(invAssemblyTransferLine.getAtlAssembleCost());
                
                atlList.add(mdetails);
                
            }
            
            InvModAssemblyTransferDetails details = new InvModAssemblyTransferDetails();
            details.setAtrCode(invAssemblyTransfer.getAtrCode());
            details.setAtrDate(invAssemblyTransfer.getAtrDate());
            details.setAtrDocumentNumber(invAssemblyTransfer.getAtrDocumentNumber());
            details.setAtrReferenceNumber(invAssemblyTransfer.getAtrReferenceNumber());
            details.setAtrDescription(invAssemblyTransfer.getAtrDescription());
            details.setAtrVoid(invAssemblyTransfer.getAtrVoid());
            details.setAtrApprovalStatus(invAssemblyTransfer.getAtrApprovalStatus());
            details.setAtrPosted(invAssemblyTransfer.getAtrPosted());
            details.setAtrCreatedBy(invAssemblyTransfer.getAtrCreatedBy());
            details.setAtrDateCreated(invAssemblyTransfer.getAtrDateCreated());
            details.setAtrLastModifiedBy(invAssemblyTransfer.getAtrLastModifiedBy());
            details.setAtrDateLastModified(invAssemblyTransfer.getAtrDateLastModified());
            details.setAtrApprovedRejectedBy(invAssemblyTransfer.getAtrApprovedRejectedBy());
            details.setAtrDateApprovedRejected(invAssemblyTransfer.getAtrDateApprovedRejected());
            details.setAtrPostedBy(invAssemblyTransfer.getAtrPostedBy());
            details.setAtrDatePosted(invAssemblyTransfer.getAtrDatePosted());
            details.setAtrReasonForRejection(invAssemblyTransfer.getAtrReasonForRejection());
            details.setAtrAtlList(atlList);
            
            return details;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            Debug.printStackTrace(ex);
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        } 
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvCompleteBol(Integer AD_BRNCH, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
        
        Debug.print("InvAssemblyTransferEntryControllerBean getInvCompleteBol");
        
        LocalInvBuildOrderLineHome invBuildOrderLineHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invBuildOrderLineHome = (LocalInvBuildOrderLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildOrderLineHome.JNDI_NAME, LocalInvBuildOrderLineHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection invBuildOrderLines = null;
            
            try {
                
                invBuildOrderLines = invBuildOrderLineHome.findCompleteBor(AD_BRNCH, AD_CMPNY);
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
                
            }
            
            ArrayList bolList = new ArrayList();
            
            Iterator i = invBuildOrderLines.iterator();
            
            while (i.hasNext()) {
                
                LocalInvBuildOrderLine invBuildOrderLine = (LocalInvBuildOrderLine)i.next();
                
                InvModBuildOrderLineDetails mdetails = new InvModBuildOrderLineDetails();
                
                mdetails.setBolQuantityRequired(invBuildOrderLine.getBolQuantityRequired());
                mdetails.setBolQuantityAssembled(invBuildOrderLine.getBolQuantityAssembled());
                mdetails.setBolIlIiName(invBuildOrderLine.getInvItemLocation().getInvItem().getIiName());
                mdetails.setBolIlLocName(invBuildOrderLine.getInvItemLocation().getInvLocation().getLocName());
                mdetails.setBolIlIiUomName(invBuildOrderLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
                mdetails.setBolBorDocumentNumber(invBuildOrderLine.getInvBuildOrder().getBorDocumentNumber());
                
                bolList.add(mdetails);
            }
            
            return bolList;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            Debug.printStackTrace(ex);
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvAtrEntry(com.util.InvAssemblyTransferDetails details, ArrayList atlList, 
            boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
            GlobalRecordAlreadyDeletedException,
            GlobalDocumentNumberNotUniqueException,
            GlobalTransactionAlreadyApprovedException,
            GlobalTransactionAlreadyPendingException,
            GlobalTransactionAlreadyPostedException,
            GlobalTransactionAlreadyVoidException,
            GlJREffectiveDateNoPeriodExistException,
            GlJREffectiveDatePeriodClosedException,
            InvATRAssemblyQtyGreaterThanAvailableQtyException,
            GlobalInventoryDateException,
            GlobalBranchAccountNumberInvalidException,
    		AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("InvAssemblyTransferEntryControllerBean saveInvAtrEntry");
        
        LocalInvAssemblyTransferHome invAssemblyTransferHome = null;
        LocalInvAssemblyTransferLineHome invAssemblyTransferLineHome = null;    
        LocalInvBuildOrderLineHome invBuildOrderLineHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome =  null; 
        
        // Initialize EJB Home
        
        try {
            
            invAssemblyTransferHome = (LocalInvAssemblyTransferHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAssemblyTransferHome.JNDI_NAME, LocalInvAssemblyTransferHome.class);
            invAssemblyTransferLineHome = (LocalInvAssemblyTransferLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAssemblyTransferLineHome.JNDI_NAME, LocalInvAssemblyTransferLineHome.class);
            invBuildOrderLineHome = (LocalInvBuildOrderLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildOrderLineHome.JNDI_NAME, LocalInvBuildOrderLineHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvAssemblyTransfer invAssemblyTransfer = null;
            
            // validate if assembly transfer is already deleted
            
            try{
                
                if(details.getAtrCode() != null) {
                    
                    invAssemblyTransfer = invAssemblyTransferHome.findByPrimaryKey(details.getAtrCode());
                    
                }  	
            } catch(FinderException ex) {
                
                throw new GlobalRecordAlreadyDeletedException();
                
            }
            
            // validate if assembly transfer already posted, void, approved or pending
            
            if (details.getAtrCode() != null) {
                
                if (invAssemblyTransfer.getAtrApprovalStatus() != null) {
                    
                    if (invAssemblyTransfer.getAtrApprovalStatus().equals("APPROVED") ||
                            invAssemblyTransfer.getAtrApprovalStatus().equals("N/A")) {         		    	
                        
                        throw new GlobalTransactionAlreadyApprovedException(); 
                        
                    } else if (invAssemblyTransfer.getAtrApprovalStatus().equals("PENDING")) {
                        
                        throw new GlobalTransactionAlreadyPendingException();
                        
                    }
                }
                
                if (invAssemblyTransfer.getAtrPosted() == EJBCommon.TRUE) {
                    
                    throw new GlobalTransactionAlreadyPostedException();
                    
                } else if (invAssemblyTransfer.getAtrVoid() == EJBCommon.TRUE) {
                    
                    throw new GlobalTransactionAlreadyVoidException();
                    
                }
            }
            
            if (details.getAtrCode() != null && details.getAtrVoid() == EJBCommon.TRUE &&
                    invAssemblyTransfer.getAtrPosted() == EJBCommon.FALSE) {
                
                invAssemblyTransfer.setAtrVoid(EJBCommon.TRUE);
                invAssemblyTransfer.setAtrLastModifiedBy(details.getAtrLastModifiedBy());
                invAssemblyTransfer.setAtrDateLastModified(details.getAtrDateLastModified());
                
                return invAssemblyTransfer.getAtrCode();
                
            }
            
            // validate if document number is unique, if document number is automatic then set next sequence
            
            LocalInvAssemblyTransfer invExistingAssemblyTransfer = null;
            
            try {
                
                invExistingAssemblyTransfer = invAssemblyTransferHome.findByAtrDocumentNumberAndBrCode(details.getAtrDocumentNumber(), AD_BRNCH, AD_CMPNY);
                
            } catch (FinderException ex) {
                
            }	    	
            
            if (details.getAtrCode() == null) {
                
                LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
                LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
                
                if (invExistingAssemblyTransfer != null) {
                    
                    throw new GlobalDocumentNumberNotUniqueException();
                    
                }
                
                // set next sequence
                
                try {
                    
                    adDocumentSequenceAssignment = 
                        adDocumentSequenceAssignmentHome.findByDcName("INV ASSEMBLY TRANSFER", AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                try {
                    
                    adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
                        (details.getAtrDocumentNumber() == null || details.getAtrDocumentNumber().trim().length() == 0)) {
                    
                    while (true) {
                        
                        if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {	        			
                            
                            try {
                                // check if next document number exists
                                
                                invAssemblyTransferHome.findByAtrDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);		            		
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
                                
                            } catch (FinderException ex) {
                                
                                details.setAtrDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
                                break;
                                
                            }
                            
                        } else {
                            
                            try {
                                
                                invAssemblyTransferHome.findByAtrDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
                                
                            } catch (FinderException ex) {
                                
                                details.setAtrDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
                                break;
                                
                            }
                            
                        }
                        
                    }		            	            		            	
                }
                
            } else {
                
                if (invExistingAssemblyTransfer != null && 
                        !invExistingAssemblyTransfer.getAtrCode().equals(details.getAtrCode())) {
                    
                    throw new GlobalDocumentNumberNotUniqueException();
                    
                }
                
                // set the correct document number
                
                if (invAssemblyTransfer.getAtrDocumentNumber() != details.getAtrDocumentNumber() &&
                        (details.getAtrDocumentNumber() == null || details.getAtrDocumentNumber().trim().length() == 0)) {
                    
                    details.setAtrDocumentNumber(invAssemblyTransfer.getAtrDocumentNumber());
                    
                }
            }
            
            boolean isRecalculate = true;
            
            if(details.getAtrCode() == null) {
                
                invAssemblyTransfer = invAssemblyTransferHome.create( details.getAtrDate(), 
                        details.getAtrDocumentNumber(), details.getAtrReferenceNumber(), details.getAtrDescription(), 
                        EJBCommon.FALSE, details.getAtrApprovalStatus(), EJBCommon.FALSE,
                        details.getAtrCreatedBy(), details.getAtrDateCreated(), details.getAtrLastModifiedBy(), 
                        details.getAtrDateLastModified(), null, null, null, null, null, AD_BRNCH, AD_CMPNY);
                
            } else {
                
                if (atlList.size() == invAssemblyTransfer.getInvAssemblyTransferLines().size()) {
                    
                    Iterator atlListIter = atlList.iterator();
                    Iterator atlIter = invAssemblyTransfer.getInvAssemblyTransferLines().iterator();
                    
                    while (atlListIter.hasNext()) {
                        
                        LocalInvAssemblyTransferLine invAssemblyTransferLine = (LocalInvAssemblyTransferLine)atlIter.next();
                        InvModAssemblyTransferLineDetails mdetails = (InvModAssemblyTransferLineDetails)atlListIter.next();
                        
                        if (!invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getAtlBolIlLocName()) ||        					
                                !invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvItem().getIiName().equals(mdetails.getAtlBolIlIiName()) ||	
                                !invAssemblyTransferLine.getInvBuildOrderLine().getInvBuildOrder().getBorDocumentNumber().equals(mdetails.getAtlBolBorDcmntNumber()) ||
                                invAssemblyTransferLine.getAtlAssembleQuantity() != mdetails.getAtlAssembleQuantity()) {
                            
                            isRecalculate = true;
                            break;
                            
                        }
                        
                        isRecalculate = false;
                        
                    }
                } else {
                    
                    isRecalculate = true;
                    
                }
                
                invAssemblyTransfer.setAtrDate(details.getAtrDate());
                invAssemblyTransfer.setAtrDocumentNumber(details.getAtrDocumentNumber());
                invAssemblyTransfer.setAtrReferenceNumber(details.getAtrReferenceNumber());
                invAssemblyTransfer.setAtrDescription(details.getAtrDescription());
                invAssemblyTransfer.setAtrLastModifiedBy(details.getAtrLastModifiedBy());
                invAssemblyTransfer.setAtrDateLastModified(details.getAtrDateLastModified());
                
            }
            
            if (isRecalculate) {
                // remove all assembly transfer lines
                
                Collection invAssemblyTransferLines = invAssemblyTransfer.getInvAssemblyTransferLines();
                
                Iterator i = invAssemblyTransferLines.iterator();
                
                while(i.hasNext()) {
                    
                    LocalInvAssemblyTransferLine invAssemblyTransferLine = (LocalInvAssemblyTransferLine)i.next();
                    invAssemblyTransferLine.getInvBuildOrderLine().setBolLock(EJBCommon.FALSE);
                    
                    i.remove();
                    invAssemblyTransferLine.remove();
                    
                }
                
                // remove all distribution records
                
                Collection invDistributionRecords = invAssemblyTransfer.getInvDistributionRecords();
                
                i = invDistributionRecords.iterator();     	  
                
                while (i.hasNext()) {
                    
                    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
                    
                    i.remove();
                    invDistributionRecord.remove();
                    
                }
                
                // create assembly transfer lines and distribution records (line)
                
                int line = 0;
                
                i = atlList.iterator();
                
                while(i.hasNext()) {
                    
                    line++;
                    
                    InvModAssemblyTransferLineDetails mdetails = (InvModAssemblyTransferLineDetails)i.next();
                    
                    //	start date validation
                    
                    LocalInvItemLocation invItemLocation = null;
                    
                    try {
                        
                        invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                                mdetails.getAtlBolIlLocName(),mdetails.getAtlBolIlIiName(),AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                        throw new GlobalInvItemLocationNotFoundException();
                        
                    }
                    
                    LocalInvCosting invLastCostingItem = null;
                    
                    try {
                        
                        invLastCostingItem = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(
                                invItemLocation.getInvItem().getIiName(),
                                invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                        
                    } catch (FinderException ex) { }
                    
                    if (invLastCostingItem != null &&
                            (invLastCostingItem.getCstDate().after(invAssemblyTransfer.getAtrDate()) || 
                                    EJBCommon.getGcCurrentDateWoTime().getTime().before(invAssemblyTransfer.getAtrDate()))) {
                        
                        throw new GlobalInventoryDateException(invLastCostingItem.getInvItemLocation().getInvItem().getIiName());
                        
                    }
                    
                    LocalInvAssemblyTransferLine invAssemblyTransferLine = invAssemblyTransferLineHome.create(mdetails.getAtlCode(),
                            mdetails.getAtlAssembleQuantity(), mdetails.getAtlAssembleCost(), AD_CMPNY);
                    
                    LocalInvBuildOrderLine invBuildOrderLine =  invBuildOrderLineHome.findPostedBorByIiNameAndLocNameAndBorDocumentNumber(mdetails.getAtlBolIlIiName(), 
                            mdetails.getAtlBolIlLocName(), mdetails.getAtlBolBorDcmntNumber(), AD_CMPNY);
                    
                    if (mdetails.getAtlAssembleQuantity() > invBuildOrderLine.getBolQuantityAvailable()) 
                        throw new InvATRAssemblyQtyGreaterThanAvailableQtyException(String.valueOf(line));
                    
                    invBuildOrderLine.setBolLock(EJBCommon.TRUE);
                    
                    invBuildOrderLine.addInvAssemblyTransferLine(invAssemblyTransferLine);
                    invAssemblyTransfer.addInvAssemblyTransferLine(invAssemblyTransferLine);
                    
                    double totalWipCost = 0;
                    short qtyPrecision = this.getInvGpQuantityPrecisionUnit(AD_CMPNY);
                    
                    LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
                    short precision = adCompany.getGlFunctionalCurrency().getFcPrecision();
                    
                    Iterator bosIter = invBuildOrderLine.getInvBuildOrderStocks().iterator();
                    
                    while (bosIter.hasNext()) {
                        
                        LocalInvBuildOrderStock invBuildOrderStock = (LocalInvBuildOrderStock)bosIter.next();
                        
                        // divide bos qty required to get unit qty required
                        double qtyBom = invBuildOrderStock.getBosQuantityRequired() / invBuildOrderLine.getBolQuantityRequired();
                        if (qtyBom >=1 && (qtyBom % Math.floor(qtyBom)) > 0) {
                            
                            qtyBom = qtyBom / Math.floor(qtyBom);
                            
                        } else if (qtyBom >=1 && (qtyBom % Math.floor(qtyBom)) == 0) {
                            
                            qtyBom = 1;
                            
                        }	    		    	
                        
                        double qtyRequired = (invBuildOrderStock.getBosQuantityRequired() / invBuildOrderLine.getBolQuantityRequired())
                        * invAssemblyTransferLine.getAtlAssembleQuantity();
                        
                        Iterator silIter = invBuildOrderStock.getInvStockIssuanceLines().iterator();
                        
                        while (silIter.hasNext()) {
                            
                            LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine)silIter.next();
                            
                            double wipCost = 0d;
                            
                            // double issueQty = invStockIssuanceLine.getSilIssueQuantity();
                            
                            double convertedIssueQuantity = this.convertByUomFromAndBomItemAndQuantity(
                                    invStockIssuanceLine.getInvUnitOfMeasure(), 
                                    invStockIssuanceLine.getInvItemLocation().getInvItem(), 
                                    invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvItem(),
                                    Math.abs(invStockIssuanceLine.getSilIssueQuantity()), true, AD_CMPNY);
                            
                            double unitCost = invStockIssuanceLine.getSilIssueCost() / convertedIssueQuantity * qtyBom;	    		    		
                            
                            while (invStockIssuanceLine.getSilIssueCost() > (invStockIssuanceLine.getSilAssemblyCost() + wipCost) 
                                    && convertedIssueQuantity > 0 && qtyRequired > 0) {
                                
                                wipCost = wipCost + unitCost;
                                qtyRequired = EJBCommon.roundIt(qtyRequired - qtyBom, qtyPrecision);
                                convertedIssueQuantity =  EJBCommon.roundIt(convertedIssueQuantity - qtyBom, qtyPrecision);
                                
                            }
                            
                            wipCost = EJBCommon.roundIt(wipCost, precision);
                            
                            if (wipCost > 0) {
                                
                                // check for branch mapping
                                
                                LocalAdBranchItemLocation adBranchItemLocation = null;
                                
                                try {
                                    
                                    adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                            invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
                                    
                                } catch (FinderException ex) {
                                    
                                }
                                
                                if (adBranchItemLocation == null) {
                                    
                                    this.addInvDrEntry(invAssemblyTransfer.getInvDrNextLine(), "WIP", EJBCommon.FALSE,
                                            wipCost, invItemLocation.getIlGlCoaWipAccount(),
                                            invAssemblyTransfer, AD_BRNCH, AD_CMPNY);
                                    
                                } else {
                                    
                                    this.addInvDrEntry(invAssemblyTransfer.getInvDrNextLine(), "WIP", EJBCommon.FALSE,
                                            wipCost, adBranchItemLocation.getBilCoaGlWipAccount(),
                                            invAssemblyTransfer, AD_BRNCH, AD_CMPNY);
                                    
                                }
                                
                                totalWipCost += wipCost;
                                
                            }
                            
                        }
                         
                    }
                    
                    // check for branch mapping
                    
                    LocalAdBranchItemLocation adBranchItemLocation = null;
                    
                    try {
                        
                        adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(invItemLocation.getIlCode(),
                                AD_BRNCH, AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                    }
                    
                    if (adBranchItemLocation == null) {
                        
                        this.addInvDrEntry(invAssemblyTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                                totalWipCost, invBuildOrderLine.getInvItemLocation().getIlGlCoaInventoryAccount(),
                                invAssemblyTransfer, AD_BRNCH, AD_CMPNY);
                        
                    } else {
                        
                        this.addInvDrEntry(invAssemblyTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                                totalWipCost, adBranchItemLocation.getBilCoaGlInventoryAccount(),
                                invAssemblyTransfer, AD_BRNCH, AD_CMPNY);
                        
                    }
                    
                }
            }
            
            if(!isDraft) {
                
                this.executeInvAtrPost(invAssemblyTransfer.getAtrCode(), invAssemblyTransfer.getAtrLastModifiedBy(), AD_BRNCH, AD_CMPNY);
                
                invAssemblyTransfer.setAtrApprovalStatus("N/A");
                
            }
            
            return invAssemblyTransfer.getAtrCode();
            
        } catch (GlobalRecordAlreadyDeletedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalDocumentNumberNotUniqueException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyApprovedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyPendingException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyPostedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyVoidException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex; 
            
        } catch (GlJREffectiveDatePeriodClosedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex; 
            
        } catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInventoryDateException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch(GlobalBranchAccountNumberInvalidException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
        	
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteInvAtrEntry(Integer ATR_CODE, Integer AD_CMPNY) throws 
    GlobalRecordAlreadyDeletedException {
        
        Debug.print("InvAssemblyTransferEntryControllerBean deleteInvAtrEntry");
        
        LocalInvAssemblyTransferHome invAssemblyTransferHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invAssemblyTransferHome = (LocalInvAssemblyTransferHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAssemblyTransferHome.JNDI_NAME, LocalInvAssemblyTransferHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            LocalInvAssemblyTransfer invAssemblyTransfer = invAssemblyTransferHome.findByPrimaryKey(ATR_CODE);
            
            Collection invAssemblyTransferLines = invAssemblyTransfer.getInvAssemblyTransferLines();
            
            Iterator i = invAssemblyTransferLines.iterator();
            
            while(i.hasNext()) {
                
                LocalInvAssemblyTransferLine invAssemblyTransferLine = (LocalInvAssemblyTransferLine) i.next();
                LocalInvBuildOrderLine invBuildOrderLine = invAssemblyTransferLine.getInvBuildOrderLine();
                
                invBuildOrderLine.setBolLock(EJBCommon.FALSE);
                
            }
            
            invAssemblyTransfer.remove();
            
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
        
        Debug.print("InvAssemblyTransferEntryControllerBean getGlFcPrecisionUnit");
        
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            return  adCompany.getGlFunctionalCurrency().getFcPrecision();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
        
        Debug.print("InvAssemblyTransferEntryControllerBean getInvGpQuantityPrecisionUnit");
        
        LocalAdPreferenceHome adPreferenceHome = null;         
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            return adPreference.getPrfInvQuantityPrecisionUnit();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public double getAssembleCostByBorDocumentNumberAndIiNameAndLocNameAndAssembleQty(String BOR_DCMNT_NMBR, String II_NM, String LOC_NM, double ASSMBL_QTY, Integer AD_CMPNY) 
    throws InvATRAssemblyQtyGreaterThanAvailableQtyException {
        
        Debug.print("InvAssemblyTransferEntryControllerBean convertByUomFromAndItemAndQuantity");		        
        
        LocalInvBuildOrderLineHome invBuildOrderLineHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invBuildOrderLineHome = (LocalInvBuildOrderLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildOrderLineHome.JNDI_NAME, LocalInvBuildOrderLineHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            short precision = this.getGlFcPrecisionUnit(AD_CMPNY);
            short qtyPrecision = this.getInvGpQuantityPrecisionUnit(AD_CMPNY);
            
            LocalInvBuildOrderLine invBuildOrderLine =  invBuildOrderLineHome.findPostedBorByIiNameAndLocNameAndBorDocumentNumber(
                    II_NM, LOC_NM, BOR_DCMNT_NMBR, AD_CMPNY);
            
            if (ASSMBL_QTY > invBuildOrderLine.getBolQuantityAvailable()) 
                throw new InvATRAssemblyQtyGreaterThanAvailableQtyException();
            
            double totalWipCost = 0;
            
            Iterator bosIter = invBuildOrderLine.getInvBuildOrderStocks().iterator();
            
            while (bosIter.hasNext()) {
                
                LocalInvBuildOrderStock invBuildOrderStock = (LocalInvBuildOrderStock)bosIter.next();
                
                // divide bos qty required to get unit qty required
                double qtyBom = invBuildOrderStock.getBosQuantityRequired() / invBuildOrderLine.getBolQuantityRequired();
                if (qtyBom >=1 && (qtyBom % Math.floor(qtyBom)) > 0) {
                    
                    qtyBom = qtyBom / Math.floor(qtyBom);
                    
                } else if (qtyBom >=1 && (qtyBom % Math.floor(qtyBom)) == 0) {
                    
                    qtyBom = 1;
                    
                }	    		    	
                
                double qtyRequired = (invBuildOrderStock.getBosQuantityRequired() / invBuildOrderLine.getBolQuantityRequired())
                * ASSMBL_QTY;
                
                Iterator silIter = invBuildOrderStock.getInvStockIssuanceLines().iterator();
                
                while (silIter.hasNext()) {
                    
                    LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine)silIter.next();
                    
                    double wipCost = 0d;
                    
                    // double issueQty = invStockIssuanceLine.getSilIssueQuantity();
                    
                    double convertedIssueQuantity = this.convertByUomFromAndBomItemAndQuantity(
                            invStockIssuanceLine.getInvUnitOfMeasure(), 
                            invStockIssuanceLine.getInvItemLocation().getInvItem(), 
                            invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvItem(),
                            Math.abs(invStockIssuanceLine.getSilIssueQuantity()), true, AD_CMPNY);
                    
                    double unitCost = invStockIssuanceLine.getSilIssueCost() / convertedIssueQuantity * qtyBom;	    		    		
                    
                    while (invStockIssuanceLine.getSilIssueCost() > (invStockIssuanceLine.getSilAssemblyCost() + wipCost) 
                            && convertedIssueQuantity > 0 && qtyRequired > 0) {
                        
                        wipCost = wipCost + unitCost;
                        qtyRequired = EJBCommon.roundIt(qtyRequired - qtyBom, qtyPrecision);
                        convertedIssueQuantity =  EJBCommon.roundIt(convertedIssueQuantity - qtyBom, qtyPrecision);
                        
                    }
                    
                    wipCost = EJBCommon.roundIt(wipCost, precision);
                    
                    if (wipCost > 0) {
                        
                        totalWipCost += wipCost;
                        
                    }
                    
                }
                
            }
            
            return totalWipCost;
            
        } catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    // private methods
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, 
            double DR_AMNT, Integer COA_CODE, LocalInvAssemblyTransfer invAssemblyTransfer, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
        
        Debug.print("InvAssemblyTransferEntryControllerBean addInvDrEntry");
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;        
        LocalGlChartOfAccountHome glChartOfAccountHome = null;           
        
        // Initialize EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
        
        try {        
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            // validate coa
            
            LocalGlChartOfAccount glChartOfAccount = null;
            
            try {
                
                glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
                
            } catch(FinderException ex) {
                
                throw new GlobalBranchAccountNumberInvalidException ();
                
            }
            
            // create distribution record        
            
            LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
                    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
                    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
            
            invAssemblyTransfer.addInvDistributionRecord(invDistributionRecord);
            glChartOfAccount.addInvDistributionRecord(invDistributionRecord);		   
            
        } catch(GlobalBranchAccountNumberInvalidException ex) {
            
            throw new GlobalBranchAccountNumberInvalidException ();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
    }	
    
    private void executeInvAtrPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
    GlobalRecordAlreadyDeletedException,		
    GlobalTransactionAlreadyPostedException,
    GlJREffectiveDateNoPeriodExistException,
    GlJREffectiveDatePeriodClosedException,
    GlobalJournalNotBalanceException,
	AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("InvAssemblyTransferEntryControllerBean executeInvAtrPost");
        
        LocalInvAssemblyTransferHome invAssemblyTransferHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invAssemblyTransferHome = (LocalInvAssemblyTransferHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAssemblyTransferHome.JNDI_NAME, LocalInvAssemblyTransferHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
            // validate if assembly transfer is already deleted
            
            LocalInvAssemblyTransfer invAssemblyTransfer = null;
            
            try {
                
                invAssemblyTransfer = invAssemblyTransferHome.findByPrimaryKey(ADJ_CODE);
                
            } catch (FinderException ex) {
                
                throw new GlobalRecordAlreadyDeletedException();
                
            }
            
            // validate if assembly transfer is already posted or void
            
            if (invAssemblyTransfer.getAtrPosted() == EJBCommon.TRUE) {
                
                throw new GlobalTransactionAlreadyPostedException();
                
            }
            
            Collection invAssemblyTransferLines = invAssemblyTransfer.getInvAssemblyTransferLines();
            
            Iterator i = invAssemblyTransferLines.iterator();
            
            while(i.hasNext()) {
                
                LocalInvAssemblyTransferLine invAssemblyTransferLine = (LocalInvAssemblyTransferLine) i.next();
                LocalInvBuildOrderLine invBuildOrderLine = invAssemblyTransferLine.getInvBuildOrderLine();
                
                invBuildOrderLine.setBolLock(EJBCommon.FALSE);
                invBuildOrderLine.setBolQuantityAssembled(invBuildOrderLine.getBolQuantityAssembled() + invAssemblyTransferLine.getAtlAssembleQuantity());
                invBuildOrderLine.setBolQuantityAvailable(invBuildOrderLine.getBolQuantityAvailable() - invAssemblyTransferLine.getAtlAssembleQuantity());
                
                String II_NM = invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvItem().getIiName();
                String LOC_NM = invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvLocation().getLocName();
                
                
                // get assembly cost
                
                double totalWipCost = 0;
                short qtyPrecision = this.getInvGpQuantityPrecisionUnit(AD_CMPNY);
                short precision = (adCompanyHome.findByPrimaryKey(AD_CMPNY)).getGlFunctionalCurrency().getFcPrecision();
                
                Iterator bosIter = invBuildOrderLine.getInvBuildOrderStocks().iterator();
                
                while (bosIter.hasNext()) {
                    
                    LocalInvBuildOrderStock invBuildOrderStock = (LocalInvBuildOrderStock)bosIter.next();
                    
                    double qtyBom = invBuildOrderStock.getBosQuantityRequired() / invBuildOrderLine.getBolQuantityRequired();
                    if (qtyBom >=1 && (qtyBom % Math.floor(qtyBom)) > 0) {
                        
                        qtyBom = qtyBom / Math.floor(qtyBom);
                        
                    } else if (qtyBom >=1 && (qtyBom % Math.floor(qtyBom)) == 0) {
                        
                        qtyBom = 1;
                        
                    }
                    
                    
                    double qtyRequired = invBuildOrderStock.getBosQuantityRequired() / invBuildOrderLine.getBolQuantityRequired() * invAssemblyTransferLine.getAtlAssembleQuantity();
                    
                    Iterator silIter = invBuildOrderStock.getInvStockIssuanceLines().iterator();
                    
                    while (silIter.hasNext()) {
                        
                        LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine)silIter.next();
                        
                        // double convertedIssueQuantity = invStockIssuanceLine.getSilIssueQuantity();
                        
                        double convertedIssueQuantity = this.convertByUomFromAndBomItemAndQuantity(
                                invStockIssuanceLine.getInvUnitOfMeasure(), 
                                invStockIssuanceLine.getInvItemLocation().getInvItem(),
                                invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvItem(),
                                Math.abs(invStockIssuanceLine.getSilIssueQuantity()), true, AD_CMPNY);
                        
                        double wipCost = 0d;
                        
                        double unitCost = invStockIssuanceLine.getSilIssueCost() / convertedIssueQuantity * qtyBom;
                        
                        while (invStockIssuanceLine.getSilIssueCost() > (invStockIssuanceLine.getSilAssemblyCost() + wipCost) && convertedIssueQuantity > 0 && qtyRequired > 0) {
                            
                            wipCost = wipCost + unitCost;
                            qtyRequired = EJBCommon.roundIt(qtyRequired - qtyBom, qtyPrecision);
                            convertedIssueQuantity = EJBCommon.roundIt(convertedIssueQuantity - qtyBom, qtyPrecision);
                            
                        }
                        
                        wipCost = EJBCommon.roundIt(wipCost, precision);
                        
                        if (wipCost > 0) {
                            
                            invStockIssuanceLine.setSilAssemblyCost(EJBCommon.roundIt(invStockIssuanceLine.getSilAssemblyCost() + wipCost, precision));
                            
                            totalWipCost += wipCost;
                            
                        }
                        
                    }
                }
                
                double ASSEMBLE_COST = totalWipCost;
                
                double ASSEMBLE_QTY = invAssemblyTransferLine.getAtlAssembleQuantity();
                
                LocalInvCosting invCosting = null;
                
                try {
                    
                    invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invAssemblyTransfer.getAtrDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                if (invCosting == null) {
                    
                    this.postToCosting(invAssemblyTransferLine, invAssemblyTransfer.getAtrDate(), ASSEMBLE_QTY, ASSEMBLE_COST,
                    		ASSEMBLE_QTY, ASSEMBLE_COST, 0d, null, AD_BRNCH, AD_CMPNY);			
                    
                } else {

                	//compute cost variance   
					double CST_VRNC_VL = 0d;

					if(invCosting.getCstRemainingQuantity() < 0)
						CST_VRNC_VL = (invCosting.getCstRemainingQuantity() * (ASSEMBLE_COST/ASSEMBLE_QTY) -
								invCosting.getCstRemainingValue());

                    this.postToCosting(invAssemblyTransferLine, invAssemblyTransfer.getAtrDate(), ASSEMBLE_QTY, ASSEMBLE_COST,
                    		invCosting.getCstRemainingQuantity() + ASSEMBLE_QTY, invCosting.getCstRemainingValue() + ASSEMBLE_COST,
                    		CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);
                    
                }
            }			
            
            invAssemblyTransfer.setAtrPosted(EJBCommon.TRUE);
            invAssemblyTransfer.setAtrPostedBy(USR_NM);
            invAssemblyTransfer.setAtrDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            // validate if date has no period and period is closed
            
            LocalGlSetOfBook glJournalSetOfBook = null;
            
            try {
                
                glJournalSetOfBook = glSetOfBookHome.findByDate(invAssemblyTransfer.getAtrDate(), AD_CMPNY);
                
            } catch (FinderException ex) {
                
                throw new GlJREffectiveDateNoPeriodExistException();
                
            }
            
            LocalGlAccountingCalendarValue glAccountingCalendarValue = 
                glAccountingCalendarValueHome.findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAssemblyTransfer.getAtrDate(), AD_CMPNY);
            
            if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
                    glAccountingCalendarValue.getAcvStatus() == 'C' ||
                    glAccountingCalendarValue.getAcvStatus() == 'P') {
                
                throw new GlJREffectiveDatePeriodClosedException();
                
            }
            
            // check if debit and credit is balance
            
            LocalGlJournalLine glOffsetJournalLine = null;
            
            Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByAtrCode(invAssemblyTransfer.getAtrCode(), AD_CMPNY);
            
            i = invDistributionRecords.iterator();
            
            double TOTAL_DEBIT = 0d;
            double TOTAL_CREDIT = 0d;
            
            while (i.hasNext()) {
                
                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
                
                double DR_AMNT = 0d;
                
                DR_AMNT = invDistributionRecord.getDrAmount();
                
                if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
                    
                    TOTAL_DEBIT += DR_AMNT;
                    
                } else {
                    
                    TOTAL_CREDIT += DR_AMNT;
                    
                }
            }
            
            TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
            TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
            
            if (TOTAL_DEBIT != TOTAL_CREDIT) {
                
                throw new GlobalJournalNotBalanceException();		    	
                
            }
            
            // create journal batch
            
            LocalGlJournalBatch glJournalBatch = null;
            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
            
            try {
                
                glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " ASSEMBLY TRANSFERS", AD_BRNCH, AD_CMPNY);
                
            } catch (FinderException ex) {
                
            }
            
            if (glJournalBatch == null) {
                
                glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " ASSEMBLY TRANSFERS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
                
            }
            
            // create journal entry			            	
            
            LocalGlJournal glJournal = glJournalHome.create(invAssemblyTransfer.getAtrReferenceNumber(),
                    invAssemblyTransfer.getAtrDescription(), invAssemblyTransfer.getAtrDate(),
                    0.0d, null, invAssemblyTransfer.getAtrDocumentNumber(), null, 1d, "N/A", null,
                    'N', EJBCommon.TRUE, EJBCommon.FALSE, USR_NM, new Date(), USR_NM, new Date(), null, null,
                    USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, EJBCommon.FALSE,
                    null, 
                    AD_BRNCH, AD_CMPNY);
            
            LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
            glJournal.setGlJournalSource(glJournalSource);
            
            LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
            glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
            
            LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("ASSEMBLY TRANSFERS", AD_CMPNY);
            glJournal.setGlJournalCategory(glJournalCategory);
            
            if (glJournalBatch != null) {
                
            	glJournal.setGlJournalBatch(glJournalBatch);
                
            }           		    
            
            // create journal lines
            
            i = invDistributionRecords.iterator();
            
            while (i.hasNext()) {
                
                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
                
                double DR_AMNT = 0d;
                
                DR_AMNT = invDistributionRecord.getDrAmount();
                
                LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
                        invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
                
                invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
                
                glJournal.addGlJournalLine(glJournalLine);
                
                invDistributionRecord.setDrImported(EJBCommon.TRUE);
                
            }
            
            if (glOffsetJournalLine != null) {
                
                glJournal.addGlJournalLine(glOffsetJournalLine);
                
            }		
            
            Collection glJournalLines = glJournal.getGlJournalLines();
            
            i = glJournalLines.iterator();
            
            while (i.hasNext()) {
                
                LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
                
                // post current to current acv
                
                this.postToGl(glAccountingCalendarValue,
                        glJournalLine.getGlChartOfAccount(),
                        true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
                
                // post to subsequent acvs (propagate)
                
                Collection glSubsequentAccountingCalendarValues = 
                    glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
                            glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                            glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
                
                Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
                
                while (acvsIter.hasNext()) {
                    
                    LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
                        (LocalGlAccountingCalendarValue)acvsIter.next();
                    
                    this.postToGl(glSubsequentAccountingCalendarValue,
                            glJournalLine.getGlChartOfAccount(),
                            false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
                    
                }
                
                // post to subsequent years if necessary
                
                Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
                
                if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
                    
                    adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
                    LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY); 	  	
                    
                    Iterator sobIter = glSubsequentSetOfBooks.iterator();
                    
                    while (sobIter.hasNext()) {
                        
                        LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
                        
                        String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
                        
                        // post to subsequent acvs of subsequent set of book(propagate)
                        
                        Collection glAccountingCalendarValues = 
                            glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
                        
                        Iterator acvIter = glAccountingCalendarValues.iterator();
                        
                        while (acvIter.hasNext()) {
                            
                            LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
                                (LocalGlAccountingCalendarValue)acvIter.next();
                            
                            if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
                                    ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
                                
                                this.postToGl(glSubsequentAccountingCalendarValue,
                                        glJournalLine.getGlChartOfAccount(),
                                        false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
                                
                            } else { 
                                // revenue & expense
                                
                                this.postToGl(glSubsequentAccountingCalendarValue,
                                        glRetainedEarningsAccount,
                                        false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
                                
                            }
                        }
                        
                        if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
                        
                    }
                }
            }			   
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlJREffectiveDatePeriodClosedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalJournalNotBalanceException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalRecordAlreadyDeletedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyPostedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
        	
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
    }
    
    private void postToCosting(LocalInvAssemblyTransferLine invAssemblyTransferLine, Date CST_DT, double CST_ASSMBL_QTY,
    		double CST_ASSMBL_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM, Integer AD_BRNCH,
			Integer AD_CMPNY)throws
			AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("InvAssemblyTransferEntryControllerBean postToCosting");
        
        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
            invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            LocalInvItemLocation invItemLocation = invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation();
            int CST_LN_NMBR = 0;
            
            CST_ASSMBL_QTY = EJBCommon.roundIt(CST_ASSMBL_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_ASSMBL_CST = EJBCommon.roundIt(CST_ASSMBL_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
            CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
            
            if (CST_ASSMBL_QTY < 0) {
                
                invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ASSMBL_QTY));
                
            }
            
            try {
                
                LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
                
            } catch (FinderException ex) {
                
                CST_LN_NMBR = 1;
                
            }

            //void subsequent cost variance adjustments
            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
            Iterator i = invAdjustmentLines.iterator();
            
            while (i.hasNext()){
            	
            	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
            	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
            	
            }

            // create costing
            LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, CST_ASSMBL_QTY, CST_ASSMBL_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ASSMBL_QTY > 0 ? CST_ASSMBL_QTY : 0, AD_BRNCH, AD_CMPNY);
            invItemLocation.addInvCosting(invCosting);
            invCosting.setInvAssemblyTransferLine(invAssemblyTransferLine);
            
			// if cost variance is not 0, generate cost variance for the transaction 
			if(CST_VRNC_VL != 0) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"INVAT" + invAssemblyTransferLine.getInvAssemblyTransfer().getAtrDocumentNumber(),
						invAssemblyTransferLine.getInvAssemblyTransfer().getAtrDescription(),
						invAssemblyTransferLine.getInvAssemblyTransfer().getAtrDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				
			}

           // propagate balance if necessary           
            Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            
            i = invCostings.iterator();
            
            while (i.hasNext()) {
                
                LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
                
                invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ASSMBL_QTY);
                invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ASSMBL_CST);
                
            }  

            // regenerate cost varaince
            this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
    }
    
    private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
            LocalGlChartOfAccount glChartOfAccount, 
            boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
        
        Debug.print("InvAssemblyTransferEntryControllerBean postToGl");
        
        LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);    
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {          
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
            
            LocalGlChartOfAccountBalance glChartOfAccountBalance = glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
                    glAccountingCalendarValue.getAcvCode(), glChartOfAccount.getCoaCode(), AD_CMPNY);
            
            String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
            short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
            
            if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
                    isDebit == EJBCommon.TRUE) || (!ACCOUNT_TYPE.equals("ASSET") && 
                            !ACCOUNT_TYPE.equals("EXPENSE") && isDebit == EJBCommon.FALSE)) {				    
                
                glChartOfAccountBalance.setCoabEndingBalance(
                        EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
                
                if (!isCurrentAcv) {
                    
                    glChartOfAccountBalance.setCoabBeginningBalance(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
                    
                }
            } else {
                
                glChartOfAccountBalance.setCoabEndingBalance(
                        EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
                
                if (!isCurrentAcv) {
                    
                    glChartOfAccountBalance.setCoabBeginningBalance(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
                    
                }
            }
            
            if (isCurrentAcv) { 
                
                if (isDebit == EJBCommon.TRUE) {
                    
                    glChartOfAccountBalance.setCoabTotalDebit(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
                    
                } else {
                    
                    glChartOfAccountBalance.setCoabTotalCredit(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
                }       	   
            }
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
    }
    
    private double convertByUomFromAndBomItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, LocalInvItem invAssembly, double ISSUE_QTY, boolean isFromBom, Integer AD_CMPNY) {
        
        Debug.print("InvAssemblyTransferEntryControllerBean convertByUomFromAndItemAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvBillOfMaterial invBillOfMaterial = invBillOfMaterialHome.findByBomIiNameAndAssemblyItem(
                    invItem.getIiName(), invAssembly.getIiName(), AD_CMPNY);
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invBomUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invBillOfMaterial.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            if (isFromBom) {	        	
                
                return EJBCommon.roundIt(ISSUE_QTY * invBomUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
                
            } else {
                
                return EJBCommon.roundIt(ISSUE_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
                
            }
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("InvAssemblyTransferEntryControllerBean voidInvAdjustment");
    	
    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();
    			
    			Iterator i = invDistributionRecords.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    				
    				list.add(invDistributionRecord);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();    	    			    			
    				
    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(), 
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    			}	    
    			
    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
    				
    				list.add(invAdjustmentLine);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();    	    			    			
    				
    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}

    			
    			invAdjustment.setAdjVoid(EJBCommon.TRUE);
    			
    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	
    	Debug.print("InvAssemblyTransferEntryControllerBean generateCostVariance");
    	/*
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try{ 
    		
    		
    		
    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;
    		
    		
    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    		
    		try{
    			
    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			
    		} catch (FinderException ex) {
    			
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    			
    		}
    		
    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment,
    				CST_VRNC_VL, EJBCommon.FALSE, AD_CMPNY);
    		
    		// check for branch mapping
    		
    		LocalAdBranchItemLocation adBranchItemLocation = null;
    		
    		try{
    			
    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		LocalGlChartOfAccount glInventoryChartOfAccount = null;
    		
    		if (adBranchItemLocation == null) {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());
    			
    		}
    		
    		
    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;
    		
    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", 
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	
    	Debug.print("InvAssemblyTransferEntryControllerBean regenerateCostVariance");
    	/*
    	try {
    		
    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			if(prevInvCosting.getCstRemainingQuantity() < 0) {
    				
    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
    					
    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
    					
    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
    					
    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
    						
    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
    						
    					}
    					
    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
   						
   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
   	   						
   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
   	    					
   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}
   	   						
    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" + 
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
    					
    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
    					
    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
    						
    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() * 
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
    						
    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();
    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
							
    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}
    					
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" + 
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();
    					
    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" + 
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
    					
    				} else {
    					
    					prevInvCosting = invPropagatedCosting;
    					continue;
    					
    				}
    				
    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;
    				
    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());
    				
    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// set previous costing
    			prevInvCosting = invPropagatedCosting;
    			
    		}
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}     */   	
    }
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE, 
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvAssemblyTransferEntryControllerBean addInvDrEntry");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;           
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {        
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    			
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
    		
    		// create distribution record        
    		
    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					AD_CMPNY);
    		
    		invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		glChartOfAccount.addInvDistributionRecord(invDistributionRecord);		   
    		
    	} catch(GlobalBranchAccountNumberInvalidException ex) {
    		
    		throw new GlobalBranchAccountNumberInvalidException ();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,		
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvAssemblyTransferEntryControllerBean executeInvAdjPost");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;        
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try {
    		
    		// validate if adjustment is already deleted
    		
    		LocalInvAdjustment invAdjustment = null;
    		
    		try {
    			
    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlobalRecordAlreadyDeletedException();
    			
    		}
    		
    		// validate if adjustment is already posted or void
    		
    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();
    			
    		}
    		
    		Collection invAdjustmentLines = null;
    		
    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

    		
    		Iterator i = invAdjustmentLines.iterator();
    		
    		while(i.hasNext()) {
    			
    			
    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    			
				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);
    			
    		}			
    		
    		// post to gl if necessary
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    			// validate if date has no period and period is closed
    			
    			LocalGlSetOfBook glJournalSetOfBook = null;
    			
    			try {
    				
    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    				throw new GlJREffectiveDateNoPeriodExistException();
    				
    			}
    			
    			LocalGlAccountingCalendarValue glAccountingCalendarValue = 
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);
    			
    			
    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
    				
    				throw new GlJREffectiveDatePeriodClosedException();
    				
    			}
    			
    			// check if invoice is balance if not check suspense posting
    			
    			LocalGlJournalLine glOffsetJournalLine = null;
    			
    			Collection invDistributionRecords = null;
    			
    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			} else {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			}

    			
    			Iterator j = invDistributionRecords.iterator();
    			
    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    					
    					TOTAL_DEBIT += DR_AMNT;
    					
    				} else {
    					
    					TOTAL_CREDIT += DR_AMNT;
    					
    				}
    				
    			}
    			
    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			
    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				LocalGlSuspenseAccount glSuspenseAccount = null;
    				
    				try { 	
    					
    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);
    					
    				} catch (FinderException ex) {
    					
    					throw new GlobalJournalNotBalanceException();
    					
    				}
    				
    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE, 
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
    					
    				} else {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
    					
    				}
    				
    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				
    				
    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				throw new GlobalJournalNotBalanceException();		    	
    				
    			}
    			
    			// create journal batch if necessary
    			
    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
    			
    			try {
    				
    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			if (glJournalBatch == null) {
    				
    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// create journal entry			            	
    			
    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null, 

						AD_BRNCH, AD_CMPNY);
    			
    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournalSource.addGlJournal(glJournal);
    			glJournal.setGlJournalSource(glJournalSource);
    			
    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
    			
    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);
    			
    			if (glJournalBatch != null) {
    				
    				glJournal.setGlJournalBatch(glJournalBatch);
    				
    			}           		    
    			
    			// create journal lines
    			
    			j = invDistributionRecords.iterator();
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
    				
    				invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				
    				glJournal.addGlJournalLine(glJournalLine);
    				
    				invDistributionRecord.setDrImported(EJBCommon.TRUE);
    				
    				
    			}
    			
    			if (glOffsetJournalLine != null) {
    				
    				glJournal.addGlJournalLine(glOffsetJournalLine);
    				
    			}		
    			
    			// post journal to gl
    			
    			Collection glJournalLines = glJournal.getGlJournalLines();
    			
    			i = glJournalLines.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
    				
    				// post current to current acv
    				
    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    				
    				
    				// post to subsequent acvs (propagate)
    				
    				Collection glSubsequentAccountingCalendarValues = 
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
    				
    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
    				
    				while (acvsIter.hasNext()) {
    					
    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    						(LocalGlAccountingCalendarValue)acvsIter.next();
    					
    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    					
    				}
    				
    				// post to subsequent years if necessary
    				
    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
    				
    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
    					
    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY); 	  	
    					
    					Iterator sobIter = glSubsequentSetOfBooks.iterator();
    					
    					while (sobIter.hasNext()) {
    						
    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
    						
    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
    						
    						// post to subsequent acvs of subsequent set of book(propagate)
    						
    						Collection glAccountingCalendarValues = 
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
    						
    						Iterator acvIter = glAccountingCalendarValues.iterator();
    						
    						while (acvIter.hasNext()) {
    							
    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    								(LocalGlAccountingCalendarValue)acvIter.next();
    							
    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    								
    							} else { // revenue & expense
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
    								
    							}
    							
    						}
    						
    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
    						
    					}
    					
    				}
    				
    			}
			
			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlJREffectiveDatePeriodClosedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalJournalNotBalanceException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalRecordAlreadyDeletedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalTransactionAlreadyPostedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {
    	
    	Debug.print("InvAssemblyTransferEntryControllerBean addInvAlEntry");
    	
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {
    		
    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL,null,null, 0,0, AL_VD, AD_CMPNY);
    		
    		// map adjustment, unit of measure, item location
    		invAdjustment.addInvAdjustmentLine(invAdjustmentLine);	
    		invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		
    		return invAdjustmentLine;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    } 
    
    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("InvAssemblyTransferEntryControllerBean saveInvAdjustment");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home    	
    	
    	try{
    	
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try{
    		
    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;
    		
    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
    		
    		try {
    			
    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		try {
    			
    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		while (true) {
    			
    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);		            		
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					break;
    					
    				}
    				
    			} else {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
    					break;
    					
    				}
    				
    			}
    			
    		}		            
    		
    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
    		
    		return invAdjustment;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	
    }
    
    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {
    	
    	Debug.print("InvAssemblyTransferEntryControllerBean postInvAdjustmentToInventory");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
    		
    	} catch (NamingException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;
    		
    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		
    		if (CST_ADJST_QTY < 0) {
    			
    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
    			
    		}
    		
    		// create costing
    		
    		try {
    			
    			// generate line number
    			
    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
    			
    		} catch (FinderException ex) {
    			
    			CST_LN_NMBR = 1;
    			
    		}
    		
    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);
    		
    		// propagate balance if necessary           
    		
    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    		
    		Iterator i = invCostings.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
    			
    		}                           
    		
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	
    }
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
		
		Debug.print("InvAssemblyTransferEntryControllerBean convertByUomFromAndUomToAndQuantity");		        
	    
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
                
	    // Initialize EJB Home
	    
	    try {
	        
	        adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
	             lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
	        invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
                    
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	            
	    try {
	    
	        LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
	        
	        LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
                        
        	return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
	    						    		        		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
		
	}	
    
    // SessionBean methods
    
    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {
        
        Debug.print("InvAssemblyTransferEntryControllerBean ejbCreate");
        
    }
    
}