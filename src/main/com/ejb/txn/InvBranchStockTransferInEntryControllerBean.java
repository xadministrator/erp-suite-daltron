package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInvTagMissingException;
import com.ejb.exception.GlobalInvTagExistingException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.InvATRAssemblyQtyGreaterThanAvailableQtyException;
import com.ejb.exception.InvTagSerialNumberNotFoundException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.ejb.inv.LocalInvBranchStockTransferHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModBranchStockTransferDetails;
import com.util.InvModBranchStockTransferLineDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 *  @author Kenji Gella
 *
 *
 *
 *
 * @ejb:bean name="InvBranchStockTransferInEntryControllerEJB"
 *           display-name="Used for transferring stocks from one branch to another"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvBranchStockTransferInEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvBranchStockTransferInEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvBranchStockTransferInEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
*/


public class InvBranchStockTransferInEntryControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {

        Debug.print("InvBranchStockTransferInEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            invLocations = invLocationHome.findLocAll(AD_CMPNY);

            if (invLocations.isEmpty()) {

                return null;

            }

            Iterator i = invLocations.iterator();

            while (i.hasNext()) {

                LocalInvLocation invLocation = (LocalInvLocation)i.next();
                String details = invLocation.getLocName();

                list.add(details);

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getInvTraceMisc(String II_NAME, Integer AD_CMPNY) {

        Debug.print("InvAdjustmentEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isTraceMisc = false;
        // Initialize EJB Home

        try {

        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            if (invItem.getIiTraceMisc() == 1){
            	isTraceMisc = true;
            }
            return isTraceMisc;
    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());

    	}

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {

        Debug.print("ApReceivingItemEntryControllerBean getAdUsrAll");

        LocalAdUserHome adUserHome = null;

        LocalAdUser adUser = null;

        Collection adUsers = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adUsers = adUserHome.findUsrAll(AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adUsers.isEmpty()) {

            return null;

        }

        Iterator i = adUsers.iterator();

        while (i.hasNext()) {

            adUser = (LocalAdUser)i.next();

            list.add(adUser.getUsrName());

        }

        return list;

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModBranchStockTransferDetails getInvBstByBstCode(Date BSTI_DT, Integer BST_CODE, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalNoRecordFoundException {

        Debug.print("InvBranchStockTransferInEntryControllerBean getInvBstByBstCode");

        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
        LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
        LocalAdBranchHome adBranchHome = null;
        LocalInvTagHome invTagHome = null;
        // Initialize EJB Home

        try {

            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);

            invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);

            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            invTagHome = (LocalInvTagHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
        } catch(NamingException ex) {

            throw(new EJBException(ex.getMessage()));

        }


        try {

            LocalInvBranchStockTransfer invBranchStockTransfer = null;

            try {

                invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(BST_CODE);

            } catch(FinderException ex) {

                throw new GlobalNoRecordFoundException();
            }

            String specs = null;
        	String propertyCode = null;
        	String expiryDate = null;
        	String serialNumber = null;
            ArrayList bslList = new ArrayList();

             Iterator i = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();

            // branch stock transfer lines
            while(i.hasNext()) {

            	LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) i.next();

            	InvModBranchStockTransferLineDetails mdetails = new InvModBranchStockTransferLineDetails();


            	ArrayList tagList = new ArrayList();
            	mdetails.setTraceMisc(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiTraceMisc());
	        	System.out.println(mdetails.getTraceMisc() + "<== getTraceMisc under getInvAdjByAdjCode controllerbean");
	        	if (mdetails.getTraceMisc() == 1){

	        		tagList = this.getInvTagList(invBranchStockTransferLine);

	        		mdetails.setBslTagList(tagList);

	        	}






            	mdetails.setBslCode(invBranchStockTransferLine.getBslCode());
            	mdetails.setBslIiName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
            	mdetails.setBslLocationName(invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
            	mdetails.setBslQuantity(invBranchStockTransferLine.getBslQuantity());
            	mdetails.setBslQuantityReceived(invBranchStockTransferLine.getBslQuantityReceived());
            	mdetails.setBslUomName(invBranchStockTransferLine.getInvUnitOfMeasure().getUomName());
            	mdetails.setBslIiDescription(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiDescription());
            	mdetails.setBslMisc(invBranchStockTransferLine.getBslMisc());
            	// if bst out, unit cost based on transit location
            	if(invBranchStockTransfer.getBstType().equals("OUT")) {

            		String BR_OUT_NM = this.getAdBrNameByBrCode(invBranchStockTransfer.getBstAdBranch());

            		double unitCost = this.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(
            				invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
            				invBranchStockTransfer.getInvLocation().getLocName(),
            				invBranchStockTransferLine.getInvUnitOfMeasure().getUomName(),
            				BSTI_DT, BR_OUT_NM, AD_CMPNY);
            		double shippingCost = 0d;

            		shippingCost = this.getInvIiShippingCostByIiUnitCostAndAdBranch(unitCost, AD_BRNCH, AD_CMPNY);

            		mdetails.setBslShippingCost(shippingCost);
                	mdetails.setBslUnitCost(unitCost + shippingCost);

                	double AMOUNT = 0d;

                	AMOUNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantity() * mdetails.getBslUnitCost(), this.getGlFcPrecisionUnit(AD_CMPNY));

                	mdetails.setBslAmount(AMOUNT);


            	} else {

            		mdetails.setBslUnitCost(invBranchStockTransferLine.getBslUnitCost());
            		mdetails.setBslAmount(invBranchStockTransferLine.getBslAmount());

            	}
            	//mdetails.setBslMisc(invBranchStockTransferLine.getBslMisc());
            	//System.out.println("invBranchStockTransferLine.getBslMisc() : " + invBranchStockTransferLine.getBslMisc());

            	bslList.add(mdetails);
            }


            InvModBranchStockTransferDetails details = new InvModBranchStockTransferDetails();

            details.setBstCode(invBranchStockTransfer.getBstCode());
            details.setBstNumber(invBranchStockTransfer.getBstNumber());
            details.setBstDescription(invBranchStockTransfer.getBstDescription());
            details.setBstDate(invBranchStockTransfer.getBstDate());
            details.setBstTransferOutNumber(invBranchStockTransfer.getBstTransferOutNumber());
            details.setBstBranchFrom(invBranchStockTransfer.getAdBranch().getBrName());

            try{
            	String BR_OUT_NM = this.getAdBrNameByBrCode(invBranchStockTransfer.getBstAdBranch());
            	this.getInvBstOutByBstNumberAndBstBranch(invBranchStockTransfer.getBstTransferOutNumber(),
            			BR_OUT_NM, AD_BRNCH, AD_CMPNY);
            	details.setBstType("BST-OUT MATCHED");

            }catch(GlobalNoRecordFoundException ex){

            	details.setBstType("ITEMS");

            }

            details.setBstApprovalStatus(invBranchStockTransfer.getBstApprovalStatus());
            details.setBstPosted(invBranchStockTransfer.getBstPosted());
            details.setBstCreatedBy(invBranchStockTransfer.getBstCreatedBy());
            details.setBstDateCreated(invBranchStockTransfer.getBstDateCreated());
            details.setBstLastModifiedBy(invBranchStockTransfer.getBstLastModifiedBy());
            details.setBstDateLastModified(invBranchStockTransfer.getBstDateLastModified());
            details.setBstApprovedRejectedBy(invBranchStockTransfer.getBstApprovedRejectedBy());
            details.setBstDateApprovedRejected(invBranchStockTransfer.getBstDateApprovedRejected());
            details.setBstPostedBy(invBranchStockTransfer.getBstPostedBy());
            details.setBstDatePosted(invBranchStockTransfer.getBstDatePosted());
            details.setBstReasonForRejection(invBranchStockTransfer.getBstReasonForRejection());

            if(invBranchStockTransfer.getBstType().equals("IN")) {

            	details.setBstBranchFrom(invBranchStockTransfer.getAdBranch().getBrName());
                details.setBstTransitLocation(invBranchStockTransfer.getInvLocation().getLocName());

            } else {

            	details.setBstBranchFrom(this.getAdBrNameByBrCode(invBranchStockTransfer.getBstAdBranch()));
                details.setBstTransitLocation(invBranchStockTransfer.getInvLocation().getLocName());

            }

            details.setBstBtlList(bslList);

            return details;

        } catch (GlobalNoRecordFoundException ex) {

            Debug.printStackTrace(ex);
            throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }
    }



    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModBranchStockTransferDetails getInvBstOutByBstCode(Integer BST_OUT_CODE, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalNoRecordFoundException {

        Debug.print("InvBranchStockTransferInEntryControllerBean getInvBstOutByBstCode");

        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
        LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;

        // Initialize EJB Home

        try {

            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);

            invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);

        } catch(NamingException ex) {

            throw(new EJBException(ex.getMessage()));

        }


        try {

            LocalInvBranchStockTransfer invBranchStockTransferOut = null;
            LocalAdBranch adBranch = null;

            try {

            	invBranchStockTransferOut = invBranchStockTransferHome.findByPrimaryKey(BST_OUT_CODE);

            } catch(FinderException ex) {

                throw new GlobalNoRecordFoundException();
            }

            ArrayList bslList = new ArrayList();



            Iterator i = invBranchStockTransferOut.getInvBranchStockTransferLines().iterator();

            // branch stock transfer lines
            while(i.hasNext()) {

                LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) i.next();

                InvModBranchStockTransferLineDetails mdetails = new InvModBranchStockTransferLineDetails();

                System.out.println("invBranchStockTransferOut.getBstTransferOutNumber()="+invBranchStockTransferOut.getBstTransferOutNumber());
                System.out.println("invBranchStockTransferOut.getBstNumber()="+invBranchStockTransferOut.getBstNumber());
                System.out.println("invBranchStockTransferLine.getInvItemLocation().getIlCode()="+invBranchStockTransferLine.getInvItemLocation().getIlCode());
                System.out.println("AD_BRNCH="+AD_BRNCH);
                System.out.println("AD_CMPNY="+AD_CMPNY);

                Collection invAllBstReceives = invBranchStockTransferLineHome.findInBslByBstTransferOutNumberAndItemLocAndAdBranchAndAdCompany(
                		invBranchStockTransferOut.getBstNumber(),
                		invBranchStockTransferLine.getInvItemLocation().getIlCode(),
                		AD_BRNCH,
                		AD_CMPNY);

                System.out.println("invAllBstReceives.size()="+invAllBstReceives.size());
                Iterator x = invAllBstReceives.iterator();
                double totalQuantityReceived = 0d;

                while(x.hasNext()) {
                	LocalInvBranchStockTransferLine invBranchStockTransferLine2 = (LocalInvBranchStockTransferLine)x.next();

                	totalQuantityReceived += invBranchStockTransferLine2.getBslQuantityReceived();


                }

                double totalRemaining = invBranchStockTransferLine.getBslQuantity() - totalQuantityReceived;

                System.out.println("totalRemaining="+totalRemaining);


                mdetails.setBslCode(invBranchStockTransferLine.getBslCode());
                mdetails.setBslIiName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
            	mdetails.setBslLocationName(invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
                mdetails.setBslQuantity(totalRemaining);
                mdetails.setBslQuantityReceived(totalRemaining);
                mdetails.setBslUomName(invBranchStockTransferLine.getInvUnitOfMeasure().getUomName());
                mdetails.setBslIiDescription(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiDescription());
                mdetails.setBslUnitCost(invBranchStockTransferLine.getBslUnitCost());
                //mdetails.setBslMisc(invBranchStockTransferLine.getBslMisc());
                System.out.println("invBranchStockTransferLine.getBslMisc(): " + invBranchStockTransferLine.getBslMisc());


            	ArrayList tagList = new ArrayList();
            	mdetails.setTraceMisc(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiTraceMisc());
	        	System.out.println(mdetails.getTraceMisc() + "<== getTraceMisc under getInvAdjByAdjCode controllerbean");
	        	if (mdetails.getTraceMisc() == 1){

	        		tagList = this.getInvTagList(invBranchStockTransferLine);

	        		mdetails.setBslTagList(tagList);

	        	}








                bslList.add(mdetails);
            }


            /*Collection invAllBstReceives = invBranchStockTransferLineHome.findPostedIncomingBslByBstTransferOutNumberAndBstAdBranchAndBstAdCompany(
        			invBranchStockTransfer.getBstNumber(), invBranchStockTransfer.getAdBranch().getBrCode(), AD_CMPNY);


        	Iterator x = invAllBstReceives.iterator();
        	double totalQuantityReceived = 0d;
        	while(x.hasNext()){
        		LocalInvBranchStockTransferLine invBranchStockTransferLine2 = (LocalInvBranchStockTransferLine)x.next();
        		totalQuantityReceived += invBranchStockTransferLine2.getBslQuantityReceived();
        	}

        	double totalRemaining = invBranchStockTransferLine.getBslQuantity() - totalQuantityReceived;

        	System.out.println("totalRemaining="+totalRemaining);*/

            InvModBranchStockTransferDetails details = new InvModBranchStockTransferDetails();

            details.setBstCode(invBranchStockTransferOut.getBstCode());
            details.setBstNumber(invBranchStockTransferOut.getBstNumber());
            details.setBstBranchFrom(invBranchStockTransferOut.getAdBranch().getBrName());
            details.setBstTransitLocation(invBranchStockTransferOut.getInvLocation().getLocName());
            details.setBstTransferOutNumber(invBranchStockTransferOut.getBstTransferOutNumber());
            details.setBstBranchFrom(this.getAdBrNameByBrCode(invBranchStockTransferOut.getBstAdBranch()));
            details.setBstTransitLocation(invBranchStockTransferOut.getInvLocation().getLocName());

            details.setBstBtlList(bslList);

            return details;

        } catch (GlobalNoRecordFoundException ex) {

            Debug.printStackTrace(ex);
            throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvBranchUomByIiName(String II_NM, Integer AD_CMPNY) {

        Debug.print("InvBranchStockTransferInEntryControllerBean getInvBranchUomByIiName");

        LocalInvUnitOfMeasureHome invBranchUnitOfMeasureHome = null;
        LocalInvItemHome invBranchItemHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invBranchUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            invBranchItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {

            LocalInvItem invBranchItem = null;
            LocalInvUnitOfMeasure invBranchItemUnitOfMeasure = null;

            invBranchItem = invBranchItemHome.findByIiName(II_NM, AD_CMPNY);
            invBranchItemUnitOfMeasure = invBranchItem.getInvUnitOfMeasure();

            Iterator i = invBranchUnitOfMeasureHome.findByUomAdLvClass(invBranchItemUnitOfMeasure.getUomAdLvClass(),
                    AD_CMPNY).iterator();

            while (i.hasNext()) {

                LocalInvUnitOfMeasure invBranchUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
                InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
                details.setUomName(invBranchUnitOfMeasure.getUomName());

                if (invBranchUnitOfMeasure.getUomName().equals(invBranchItemUnitOfMeasure.getUomName())) {

                    details.setDefault(true);

                }

                list.add(details);

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvBstEntry(com.util.InvModBranchStockTransferDetails details, ArrayList bslList,
            boolean isDraft, String type, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalRecordAlreadyDeletedException,
    GlobalTransactionAlreadyApprovedException,
    GlobalTransactionAlreadyPendingException,
    GlobalTransactionAlreadyPostedException,
    GlobalInvItemLocationNotFoundException,
    GlobalInvTagMissingException,
	GlobalInvTagExistingException,
    GlJREffectiveDateNoPeriodExistException,
    GlJREffectiveDatePeriodClosedException,
    GlobalJournalNotBalanceException,
    GlobalDocumentNumberNotUniqueException,
    GlobalInventoryDateException,
    GlobalBranchAccountNumberInvalidException,
    InvATRAssemblyQtyGreaterThanAvailableQtyException,
    //GlobalRecordAlreadyAssignedException,
	AdPRFCoaGlVarianceAccountNotFoundException,
	GlobalMiscInfoIsRequiredException, GlobalExpiryDateNotFoundException, InvTagSerialNumberNotFoundException {

        Debug.print("InvBranchStockTransferInEntryControllerBean saveInvBstEntry");

        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalAdBranchHome adBranchHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvTagHome invTagHome = null;
        LocalAdUserHome adUserHome = null;
        LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
        LocalInvItemHome invItemHome = null;

        try {

            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		    	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invTagHome = (LocalInvTagHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
            invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        Date txnStartDate = new Date();

        try {
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvBranchStockTransfer invBranchStockTransfer = null;

            //Validate if branch stock transfer is already deleted
            try {

                if (details.getBstCode() != null) {

                    invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(details.getBstCode());

                }

            } catch (FinderException ex) {

                throw new GlobalRecordAlreadyDeletedException();

            }

            //Validate if branch stock transfer is already posted, void, approved or pending
            if (details.getBstCode() != null) {

                if (invBranchStockTransfer.getBstApprovalStatus() != null) {

                    if (invBranchStockTransfer.getBstApprovalStatus().equals("APPROVED") ||
                            invBranchStockTransfer.getBstApprovalStatus().equals("N/A")) {

                        throw new GlobalTransactionAlreadyApprovedException();


                    } else if (invBranchStockTransfer.getBstApprovalStatus().equals("PENDING")) {

                        throw new GlobalTransactionAlreadyPendingException();

                    }

                }

                if (invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE) {

                    throw new GlobalTransactionAlreadyPostedException();

                }

            }

            // invoice void

            if (details.getBstCode() != null && details.getBstVoid() == EJBCommon.TRUE &&
                    invBranchStockTransfer.getBstPosted() == EJBCommon.FALSE) {

            	invBranchStockTransfer.setBstVoid(EJBCommon.TRUE);
            	invBranchStockTransfer.setBstLastModifiedBy(details.getBstLastModifiedBy());
            	invBranchStockTransfer.setBstDateLastModified(details.getBstDateLastModified());

                return invBranchStockTransfer.getBstCode();

            }


           /* LocalInvBranchStockTransfer invBranchExistingStockTransfer = null;
            Debug.print("InvBranchStockTransferInEntryControllerBean saveInvBstEntry A");
            try {

                invBranchExistingStockTransfer = invBranchStockTransferHome.findByBstNumberAndBrCode(details.getBstNumber(), AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }*/

            // validate if document number is unique and if document number is automatic then set next sequence\

            if (details.getBstCode() == null) {

                LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
                LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;


                try {

                    adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV BRANCH STOCK TRANSFER-IN", AD_CMPNY);

                } catch (FinderException ex) {

                }

                try {

                    adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

                } catch (FinderException ex) {

                }


                LocalInvBranchStockTransfer invBranchExistingStockTransfer = null;

                try {


                    invBranchExistingStockTransfer = invBranchStockTransferHome.findByBstNumberAndBrCode(
                    		details.getBstNumber(), AD_BRNCH, AD_CMPNY);


                } catch (FinderException ex) {

                }

                if (invBranchExistingStockTransfer != null) {

                    throw new GlobalDocumentNumberNotUniqueException();

                }


                if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
                        (details.getBstNumber() == null || details.getBstNumber().trim().length() == 0)) {

                    while (true) {

                        if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

                            try {

                                invBranchStockTransferHome.findByBstNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

                            } catch (FinderException ex) {

                                details.setBstNumber(adDocumentSequenceAssignment.getDsaNextSequence());
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
                                break;

                            }

                        } else {

                            try {

                                invBranchStockTransferHome.findByBstNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

                            } catch (FinderException ex) {

                                details.setBstNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
                                break;

                            }

                        }

                    }

                }

            } else {


            	LocalInvBranchStockTransfer invBranchExistingStockTransfer = null;

                try {

                	invBranchExistingStockTransfer = invBranchStockTransferHome.findByBstNumberAndBrCode(
                    		details.getBstNumber(), AD_BRNCH, AD_CMPNY);

                } catch (FinderException ex) {
                }



                if (invBranchExistingStockTransfer != null &&
                        !invBranchExistingStockTransfer.getBstCode().equals(details.getBstCode())) {

                    throw new GlobalDocumentNumberNotUniqueException();

                }

                if (invBranchStockTransfer.getBstNumber() != details.getBstNumber() &&
                        (details.getBstNumber() == null || details.getBstNumber().trim().length() == 0)) {

                    details.setBstNumber(invBranchStockTransfer.getBstNumber());

                }

            }


            /*// check if transfer-out already has transfer in
            try {

            	LocalInvBranchStockTransfer invExistingBranchStockTransferIn = invBranchStockTransferHome.findInByBstOutNumberAndBrCode(
            			details.getBstTransferOutNumber(), AD_BRNCH, AD_CMPNY);

            	if ((details.getBstCode() != null && !invExistingBranchStockTransferIn.getBstCode().equals(details.getBstCode())) ||
            			details.getBstCode() == null) {

            		throw new GlobalRecordAlreadyAssignedException();

            	}

            } catch (FinderException ex) {

            }*/


            // lock branch stock out

            LocalInvBranchStockTransfer invBranchExistingStockTransfer = null;

            try {

            	System.out.println("LOCK ---------------->");

            	invBranchExistingStockTransfer = invBranchStockTransferHome.findByBstNumberAndBrCode(
                		details.getBstNumber(), AD_BRNCH, AD_CMPNY);


                if ((invBranchStockTransfer == null || (invBranchStockTransfer != null && invBranchStockTransfer.getBstNumber() != null
                		&& (!invBranchStockTransfer.getBstNumber().equals(details.getBstNumber()))))) {
                	System.out.println("details.getBstNumber()="+details.getBstNumber());
                	System.out.println("1 LOCK ---------------->");

                	System.out.println("invBranchExistingStockTransfer="+invBranchExistingStockTransfer.getBstNumber());
                    if (invBranchExistingStockTransfer.getBstLock() == EJBCommon.TRUE) {

                        throw new GlobalTransactionAlreadyLockedException();

                    }

                    invBranchExistingStockTransfer.setBstLock(EJBCommon.TRUE);

                }
                System.out.println("FALSE LOCK ---------------->");

            } catch (Exception ex) {

            }


            // used in checking if branch stock transfer should re-generate distribution records

            boolean isRecalculate = true;

            // create branch stock transfer

            if (details.getBstCode() == null) {

                invBranchStockTransfer = invBranchStockTransferHome.create(details.getBstDate(),
                        "IN", details.getBstNumber(), details.getBstTransferOutNumber(), null,
                        details.getBstDescription(), details.getBstApprovalStatus(), details.getBstPosted(),
                        details.getBstReasonForRejection(), details.getBstCreatedBy(), details.getBstDateCreated(),
                        details.getBstLastModifiedBy(), details.getBstDateLastModified(), details.getBstApprovedRejectedBy(),
                        details.getBstDateApprovedRejected(), details.getBstPostedBy(), details.getBstDatePosted(),
                        EJBCommon.FALSE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

            } else {

                if (bslList.size() != invBranchStockTransfer.getInvBranchStockTransferLines().size() ||
                        !(invBranchStockTransfer.getBstDate().equals(details.getBstDate()))) {

                    isRecalculate = true;

                } else if (bslList.size() == invBranchStockTransfer.getInvBranchStockTransferLines().size()) {

                    Iterator bslIter = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();
                    Iterator bslIterList = bslList.iterator();

                    while(bslIter.hasNext()) {

                        LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) bslIter.next();
                        InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) bslIterList.next();


                        if (!invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getBslLocationName()) ||
                             invBranchStockTransferLine.getBslQuantityReceived() != mdetails.getBslQuantityReceived() ||
                             invBranchStockTransferLine.getBslMisc() != mdetails.getBslMisc()) {

                            isRecalculate = true;
                            break;

                        }

                 //       isRecalculate = false;

                    }

                } else {

               //     isRecalculate = true;

                }

                invBranchStockTransfer.setBstType("IN");
                invBranchStockTransfer.setBstNumber(details.getBstNumber());
                invBranchStockTransfer.setBstDescription(details.getBstDescription());
                invBranchStockTransfer.setBstDate(details.getBstDate());
                invBranchStockTransfer.setBstTransferOutNumber(details.getBstTransferOutNumber());
                invBranchStockTransfer.setBstApprovalStatus(details.getBstApprovalStatus());
                invBranchStockTransfer.setBstLastModifiedBy(details.getBstLastModifiedBy());
                invBranchStockTransfer.setBstDateLastModified(details.getBstDateLastModified());
                invBranchStockTransfer.setBstReasonForRejection(null);

            }
            // get transfer out branch

            LocalInvBranchStockTransfer invBranchStockTransferOut = null;

        	if (details.getBstBranchFrom().equals("")) {
        		details.setBstBranchFrom(adBranchHome.findByPrimaryKey(invBranchStockTransferHome.findBstByBstNumberAndAdBranchAndAdCompany(details.getBstTransferOutNumber(), AD_BRNCH, AD_CMPNY).getBstAdBranch()).getBrName());
        	}

            LocalAdBranch adBranchFrom = adBranchHome.findByBrName(details.getBstBranchFrom(), AD_CMPNY);
        	adBranchFrom.addInvBranchStockTransfer(invBranchStockTransfer);

        	if (details.getBstDescription().equals("")) {
        //		invBranchStockTransfer.setBstDescription(invBranchStockTransferHome.findBstByBstNumberAndAdBranchAndAdCompany(details.getBstTransferOutNumber(), AD_BRNCH, AD_CMPNY).getBstDescription());
        	}


            LocalInvLocation invLocation = invLocationHome.findByLocName(details.getBstTransitLocation(), AD_CMPNY);
            invLocation.addInvBranchStockTransfer(invBranchStockTransfer);
            invBranchStockTransfer.setInvLocation(invLocation);
            double ABS_TOTAL_AMOUNT = 0d;

            if (isRecalculate) {

                // remove all branch stock transfer lines

                Iterator i = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();

                short LINE_NUMBER = 0;

                while(i.hasNext()) {

                    LINE_NUMBER++;

                    LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) i.next();

                    LocalInvItemLocation invItemLocation = null;

                    try {

                        invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                                invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(),
                                invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);

                    } catch(FinderException ex) {

                        throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(LINE_NUMBER) + " - " + invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());

                    }

                    double convertedQuantity = this.convertByUomAndQuantity(
                            invBranchStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
                            invBranchStockTransferLine.getBslQuantityReceived(), AD_CMPNY);

                    invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);

                    //remove all inv tag inside branch stock transfer line
		  	   	    Collection invTags = invBranchStockTransferLine.getInvTags();

		  	   	    Iterator y = invTags.iterator();

		  	   	    while (y.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)y.next();

		  	   	    	y.remove();

		  	   	    	invTag.remove();
		  	   	    }

                    i.remove();

                    invBranchStockTransferLine.remove();

                }

                // remove all distribution records

                i = invBranchStockTransfer.getInvDistributionRecords().iterator();

                while(i.hasNext()) {

                    LocalInvDistributionRecord arDistributionRecord = (LocalInvDistributionRecord) i.next();

                    i.remove();

                    arDistributionRecord.remove();

                }


        
        /*if (type.equalsIgnoreCase("BST-OUT MATCHED")){

            	// lock corresponding transfer out
	            invBranchStockTransferOut =
	            	invBranchStockTransferHome.findByBstNumberAndBrCode(details.getBstTransferOutNumber(),
	            			invBranchStockTransfer.getAdBranch().getBrCode(), AD_CMPNY);
	            invBranchStockTransferOut.setBstLock(EJBCommon.TRUE);

            }  */

                // remove all inv branch stock transfer lines

                /*Collection invBranchStockTransferLines = invBranchStockTransfer.getInvBranchStockTransferLines();


                i = invBranchStockTransferLines.iterator();
                System.out.println("invBranchStockTransferLines.size()="+invBranchStockTransferLines.size());
                while (i.hasNext()) {

                		LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)i.next();

                    //release previous sales order
                    if (!invBranchExistingStockTransfer.getBstNumber().equals(
                    		invBranchStockTransferLine.getInvBranchStockTransfer().getBstNumber()))
                    	invBranchStockTransferLine.getInvBranchStockTransfer().setBstLock(EJBCommon.FALSE);

                    i.remove();

                    invBranchStockTransferLine.remove();

                }*/

                // add new branch stock transfer entry lines and distribution record

                byte DEBIT = 0;

                i = bslList.iterator();
                while(i.hasNext()) {

                    InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) i.next();

                    LocalInvItemLocation invItemLocation = null;

                    try {

                    	if (mdetails.getBslLocationName().equals("")) {
                    		mdetails.setBslLocationName(invLocationHome.findByPrimaryKey(invItemHome.findByIiName(mdetails.getBslIiName(), AD_CMPNY).getIiDefaultLocation()).getLocName());
                    	}

                        invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                                mdetails.getBslLocationName(),
                                mdetails.getBslIiName(), AD_CMPNY);

                    } catch (FinderException ex) {

                        throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(mdetails.getBslLineNumber() + " - " + mdetails.getBslLocationName()));

                    }

                    if (mdetails.getBslUnitCost()==0) {
                    	mdetails.setBslUnitCost(invItemHome.findByIiName(mdetails.getBslIiName(), AD_CMPNY).getIiUnitCost());
                    }
                    if (mdetails.getBslAmount()==0) {
                    	mdetails.setBslAmount(mdetails.getBslQuantityReceived()*mdetails.getBslUnitCost());
                    }

                    if (mdetails.getBslQuantity()==mdetails.getBslQuantityReceived()) {

                        Iterator j = invBranchStockTransferHome.findBstByBstNumberAndAdBranchAndAdCompany(details.getBstTransferOutNumber(), AD_BRNCH, AD_CMPNY).getInvBranchStockTransferLines().iterator();
                        while (j.hasNext()) {
                        	LocalInvBranchStockTransferLine line = (LocalInvBranchStockTransferLine) j.next();

                        	// && mdetails.getBslQuantity()== line.getBslQuantity() && mdetails.getBslUomName().equals(line.getInvUnitOfMeasure().getUomName())
                        	if (mdetails.getBslIiName().equals(line.getInvItemLocation().getInvItem().getIiName()) && mdetails.getBslUomName().equals(line.getInvUnitOfMeasure().getUomName())  ) {

                        		mdetails.setBslQuantity(line.getBslQuantity());

                        	}
                        }
                    }

                    LocalInvItemLocation invItemTransitLocation = null;

                    try {

                        invItemTransitLocation = invItemLocationHome.findByLocNameAndIiName(
                                details.getBstTransitLocation(),
                                mdetails.getBslIiName(), AD_CMPNY);

                    } catch (FinderException ex) {

                        throw new GlobalInvItemLocationNotFoundException("Transit Location " + String.valueOf(details.getBstTransitLocation()));

                    }

                    
                    if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	                    //Start date validation
	                    Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	                            invBranchStockTransfer.getBstDate(), invItemLocation.getInvItem().getIiName(),
	                            invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

	                    if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
                    }


                    LocalInvBranchStockTransferLine invBranchStockTransferLine = this.addInvBslEntry(mdetails, invBranchStockTransfer, AD_CMPNY);


                    // add physical inventory distribution

                    double COST = invBranchStockTransferLine.getBslUnitCost();

                    double SHIPPING = mdetails.getBslShippingCost();

                    double AMOUNT = 0d;

                    AMOUNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantityReceived() * COST, this.getGlFcPrecisionUnit(AD_CMPNY));

                    double SHIPPING_AMNT = 0d;

                    SHIPPING_AMNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantityReceived() * SHIPPING, this.getGlFcPrecisionUnit(AD_CMPNY));

                    // check branch mapping

                    LocalAdBranchItemLocation adBranchItemLocation = null;

                    try{

                        adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);


                    } catch (FinderException ex) {

                    }

                    LocalGlChartOfAccount glChartOfAccount = null;

                    if (adBranchItemLocation == null) {

                        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                                invItemLocation.getIlGlCoaInventoryAccount());

                    } else {

                        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                                adBranchItemLocation.getBilCoaGlInventoryAccount());

                    }

                    // add dr for inventory

                    this.addInvDrEntry(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                            Math.abs(AMOUNT), glChartOfAccount.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);


                    // check branch mapping for transit location

                    LocalAdBranchItemLocation adBranchItemTransitLocation = null;

                    try{

	                        adBranchItemTransitLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
	                                invItemTransitLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

                    } catch (FinderException ex) {

                    }

                    LocalGlChartOfAccount glChartOfAccountTransit = null;

                    if (adBranchItemTransitLocation == null) {

                        glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
                                invItemTransitLocation.getIlGlCoaInventoryAccount());

                    } else {

                        glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
                                adBranchItemTransitLocation.getBilCoaGlWipAccount());

                    }

                    // add dr for inventory transit location

                    this.addInvDrEntry(invBranchStockTransfer.getInvDrNextLine(), "IN TRANSIT", EJBCommon.FALSE,
                            Math.abs(AMOUNT - SHIPPING_AMNT), glChartOfAccountTransit.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);

                    // add dr for shipping if shipping amount > 0

                    if(SHIPPING_AMNT > 0d) {

                    	LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(AD_BRNCH);
                    	LocalGlChartOfAccount glChartOfAccountShipping = adBranch.getGlChartOfAccount();

                    	this.addInvDrEntry(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                                Math.abs(SHIPPING_AMNT), glChartOfAccountShipping.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);

                    }

                    ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);

                    // set ilCommittedQuantity

                    double convertedQuantity = this.convertByUomAndQuantity(
                            invBranchStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
                            invBranchStockTransferLine.getBslQuantityReceived(), AD_CMPNY);

                    invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

                    //TODO: add new inv Tag
                    // validate inventoriable and non-inventoriable items
                    /*if (invItemLocation.getInvItem().getIiDineInCharge()==1 && mdetails.getBslTagList() != null){
	      	  	    	throw new GlobalInvTagExistingException (invItemLocation.getInvItem().getIiName());
	      	  	    }*/
                    if (mdetails.getTraceMisc() == 1){
			      	  	if (invItemLocation.getInvItem().getIiNonInventoriable()==0 && mdetails.getBslTagList() != null
			      	  			&& mdetails.getBslTagList().size() < mdetails.getBslQuantityReceived()){

		      	  	    	throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiName());
		      	  	    }

		      	  	    boolean isSerialNumberFound = false;
		      	  	    String serialNumber="";
		      	  	    try{
			      	  	    Iterator t = mdetails.getBslTagList().iterator();
			      	  	    LocalInvTag invTag  = null;


			      	  	    while (t.hasNext()){
			      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();

			      	  	    	if (invBranchStockTransferOut!=null) {
			      	  	    		Iterator s = invBranchStockTransferOut.getInvBranchStockTransferLines().iterator();
					      	  	    //check if serial number exists in the transfer out transaction
					      	  	    isSerialNumberFound = false;
					      	  	    serialNumber = tgLstDetails.getTgSerialNumber();
					      	  	    while (s.hasNext()) {
					      	  	    	LocalInvBranchStockTransferLine tl = (LocalInvBranchStockTransferLine)s.next();
					      	  	    	Iterator iter = tl.getInvTags().iterator();
					      	  	    	while (iter.hasNext()) {
					      	  	    		LocalInvTag iT = (LocalInvTag)iter.next();
					      	  	    		if (tgLstDetails.getTgSerialNumber().equals(iT.getTgSerialNumber())) {
					      	  	    			isSerialNumberFound=true;
					      	  	    			break;
					      	  	    		}
					      	  	    	}
					      	  	    	if (isSerialNumberFound) break;
					      	  	    }
			      	  	    	}
				      	  	    if (invItemLocation.getInvItem().getIiNonInventoriable()==0 && tgLstDetails.getTgCustodian().equals("")
			      	  	    			&&tgLstDetails.getTgSpecs().equals("")&&tgLstDetails.getTgPropertyCode().equals("")
			      	  	    			&&tgLstDetails.getTgExpiryDate() == null && tgLstDetails.getTgSerialNumber().equals("")){
			      	  	    		throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiDescription());

			      	  	    	}
				      	  	    if ((!isSerialNumberFound)&&(invBranchStockTransferOut!=null)) {
			      	  	    		throw new InvTagSerialNumberNotFoundException(serialNumber);
			      	  	    	}
			      	  	    	if (tgLstDetails.getTgCode()==null){

				      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
				      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
				      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
				      	  	    			tgLstDetails.getTgType());

				      	  	    	invTag.setInvBranchStockTransferLine(invBranchStockTransferLine);
				      	  	    	LocalAdUser adUser = null;
				      	  	    	try {
				      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
				      	  	    	}catch(FinderException ex){

				      	  	    	}
				      	  	    	invTag.setAdUser(adUser);

			      	  	    	}

			      	  	    }

		      	  	    }catch(GlobalInvTagMissingException ex){
			      	  	    if (invItemLocation.getInvItem().getIiNonInventoriable()==0 ){
		      	  	    		throw new GlobalInvTagMissingException (mdetails.getBslIiName());
		      	  	    	}
		      	  	    }
		      	  	    catch (InvTagSerialNumberNotFoundException e) {
			      	  	    if (!isSerialNumberFound) {
		      	  	    		throw new InvTagSerialNumberNotFoundException(serialNumber);
		      	  	    	}
						}
                    }
                }

            } else {

                Iterator i = bslList.iterator();

                while(i.hasNext()) {

                    InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) i.next();

                    LocalInvItemLocation invItemLocation = null;

                    try {

                        invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                                mdetails.getBslLocationName(),
                                mdetails.getBslIiName(), AD_CMPNY);

                    } catch (FinderException ex) {

                        throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(mdetails.getBslLineNumber() + " - " + mdetails.getBslLocationName()));

                    }

                    if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	                    //	start date validation

	                    Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	                            invBranchStockTransfer.getBstDate(), invItemLocation.getInvItem().getIiName(),
	                            invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

	                    if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
                    }


                    i = invBranchStockTransfer.getInvDistributionRecords().iterator();

                    while(i.hasNext()) {

                        LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();

                        if(distributionRecord.getDrDebit() == 1) {

                            ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();

                        }

                    }
                    LocalInvBranchStockTransferLine invBranchStockTransferLine = invBranchStockTransferLineHome.findByPrimaryKey(mdetails.getBslCode());
                    //remove all inv tag inside PO line
		  	   	    Collection invTags = invBranchStockTransferLine.getInvTags();

		  	   	    Iterator y = invTags.iterator();

		  	   	    while (y.hasNext()){

		  	   	    	LocalInvTag invTag = (LocalInvTag)y.next();

		  	   	    	y.remove();

		  	   	    	invTag.remove();
		  	   	    }

		  	   	    if (mdetails.getTraceMisc() == 1){


			      	  	if (invItemLocation.getInvItem().getIiNonInventoriable()==0 && mdetails.getBslTagList() != null
			      	  			&& mdetails.getBslTagList().size() < mdetails.getBslQuantityReceived()){

		      	  	    	throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiName());
		      	  	    }
		      	  	    try{

			      	  	    Iterator t = mdetails.getBslTagList().iterator();
			      	  	    while (t.hasNext()){
			      	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();

			      	  	    	if (invItemLocation.getInvItem().getIiNonInventoriable()==0 && tgLstDetails.getTgCustodian().equals("")
			      	  	    			&&tgLstDetails.getTgSpecs().equals("")&&tgLstDetails.getTgPropertyCode().equals("")
			      	  	    			&&tgLstDetails.getTgExpiryDate() == null && tgLstDetails.getTgSerialNumber().equals("")){
			      	  	    		throw new GlobalInvTagMissingException (invItemLocation.getInvItem().getIiDescription());
			      	  	    	}
		      	  	    	    LocalInvTag invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
			      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
			      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
			      	  	    			tgLstDetails.getTgType());

		      	  	    	    invTag.setInvBranchStockTransferLine(invBranchStockTransferLine);
			      	  	    	LocalAdUser adUser = null;
			      	  	    	try {
			      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
			      	  	    	}catch(FinderException ex){

			      	  	    	}
			      	  	    	invTag.setAdUser(adUser);


			      	  	    }
		      	  	    }catch(Exception ex){
			      	  	    if (invItemLocation.getInvItem().getIiNonInventoriable()==0 ){
		      	  	    		throw new GlobalInvTagMissingException (mdetails.getBslIiName());
		      	  	    	}
		      	  	    }
		  	   	    }

                }

            }

            //	generate approval status

            String INV_APPRVL_STATUS = !isDraft ?  "N/A" : null;

            if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfArGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

                this.executeInvBstInPost(invBranchStockTransfer.getBstCode(), invBranchStockTransfer.getBstLastModifiedBy(),
                        adBranchFrom.getBrCode(), AD_BRNCH, AD_CMPNY);


                // Set SO Lock if Fully Served
                System.out.println("details.getBstTransferOutNumber()="+details.getBstTransferOutNumber());

                invBranchExistingStockTransfer = invBranchStockTransferHome.findByBstNumberAndAdCompany(details.getBstTransferOutNumber(), AD_CMPNY);


                Iterator stOter = invBranchExistingStockTransfer.getInvBranchStockTransferLines().iterator();
                System.out.println("invBranchExistingStockTransfer.getBstNumber()="+invBranchExistingStockTransfer.getBstNumber());
    			boolean isOpenBST = false;

                while(stOter.hasNext()) {


                	LocalInvBranchStockTransferLine invBranchStockTransferLineOut = (LocalInvBranchStockTransferLine) stOter.next();


                	Collection invAllBstReceives = invBranchStockTransferLineHome.findInBslByBstTransferOutNumberAndItemLocAndAdBranchAndAdCompany(
                			details.getBstTransferOutNumber(),
                			invBranchStockTransferLineOut.getInvItemLocation().getIlCode(),
                    		AD_BRNCH,
                    		AD_CMPNY);


                	Iterator stIter = invAllBstReceives.iterator();

                    double QUANTITY_RCVD = 0d;

                    while(stIter.hasNext()) {

                    	LocalInvBranchStockTransferLine invBranchStockTransferLineIn = (LocalInvBranchStockTransferLine)stIter.next();

                        if (invBranchStockTransferLineIn.getInvBranchStockTransfer().getBstPosted() == EJBCommon.TRUE) {

                        	QUANTITY_RCVD += invBranchStockTransferLineIn.getBslQuantityReceived();

                        }

                    }

                    double TOTAL_REMAINING_QTY = invBranchStockTransferLineOut.getBslQuantity() - QUANTITY_RCVD;

                    System.out.println("TOTAL_REMAINING_QTY="+TOTAL_REMAINING_QTY);

                    if(TOTAL_REMAINING_QTY > 0) {
                    	isOpenBST = true;
                    	break;
                    }

                }

                if(isOpenBST)
                	invBranchExistingStockTransfer.setBstLock(EJBCommon.FALSE);
                else
                	invBranchExistingStockTransfer.setBstLock(EJBCommon.TRUE);



            }

            // set stock transfer approval status

            invBranchStockTransfer.setBstApprovalStatus(INV_APPRVL_STATUS);
            Debug.print("InvBranchStockTransferInEntryControllerBean saveInvBstEntry " + txnStartDate);
            return invBranchStockTransfer.getBstCode();

        } catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalTransactionAlreadyApprovedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalTransactionAlreadyPendingException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalInvItemLocationNotFoundException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        }/*catch (GlobalInvTagExistingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        }*/ catch (GlobalInvTagMissingException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalInventoryDateException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch(GlobalBranchAccountNumberInvalidException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

       /* } catch(GlobalRecordAlreadyAssignedException ex) {

        	getSessionContext().setRollbackOnly();
            throw ex; */

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalMiscInfoIsRequiredException ex){

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalExpiryDateNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (InvTagSerialNumberNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        }catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }
    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteInvBstEntry(Integer BST_CODE, String type, String AD_USR, Integer AD_CMPNY) throws
    GlobalRecordAlreadyDeletedException {

        Debug.print("InvBranchStockTransferInEntryControllerBean deleteInvBstEntry");

        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home

        try {

            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
           		lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvBranchStockTransfer invBranchStockTransfer =  invBranchStockTransferHome.findByPrimaryKey(BST_CODE);

            Iterator i = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();

            while(i.hasNext()) {

                LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)i.next();

                LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                        invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(),
                        invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);

                double convertedQuantity = this.convertByUomAndQuantity(
                        invBranchStockTransferLine.getInvUnitOfMeasure(),
                        invItemLocation.getInvItem(),
                        Math.abs(invBranchStockTransferLine.getBslQuantityReceived()), AD_CMPNY);

                invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);
            }

            if (invBranchStockTransfer.getBstApprovalStatus() != null &&
            		invBranchStockTransfer.getBstApprovalStatus().equals("PENDING")) {

                Collection adApprovalQueues =
                	adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("INV BRANCH STOCK TRANSFER",
                		invBranchStockTransfer.getBstCode(), AD_CMPNY);

                Iterator j = adApprovalQueues.iterator();

                while(j.hasNext()) {

                    LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)j.next();

                    adApprovalQueue.remove();

                }

            }


            // unlock correponding transfer out

            LocalInvBranchStockTransfer invBranchExistingStockTransfer = null;

            try {

            	System.out.println("LOCK ---------------->");

            	invBranchExistingStockTransfer = invBranchStockTransferHome.findByBstNumberAndAdCompany(
            			invBranchStockTransfer.getBstTransferOutNumber(), AD_CMPNY);

            	invBranchExistingStockTransfer.setBstLock(EJBCommon.FALSE);

            } catch (Exception ex){

            }

            adDeleteAuditTrailHome.create("INV BRANCH STOCK TRANSFER IN", invBranchStockTransfer.getBstDate(), invBranchStockTransfer.getBstNumber(), invBranchStockTransfer.getBstTransferOutNumber(),
 					0d, AD_USR, new Date(), AD_CMPNY);

            invBranchStockTransfer.remove();

        } catch (FinderException ex) {

            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("InvBranchStockTransferInEntryControllerBean getGlFcPrecisionUnit");

        LocalAdCompanyHome adCompanyHome = null;

        // Initialize EJB Home

        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            return  adCompany.getGlFunctionalCurrency().getFcPrecision();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("InvBranchStockTransferInEntryControllerBean getInvGpQuantityPrecisionUnit");

        LocalAdPreferenceHome adPreferenceHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfInvQuantityPrecisionUnit();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getInvGpInventoryLineNumber(Integer AD_CMPNY) {

        Debug.print("InvBranchStockTransferInEntryControllerBean getInvGpInventoryLineNumber");

        LocalAdPreferenceHome adPreferenceHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfInvInventoryLineNumber();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdApprovalNotifiedUsersByBstCode(Integer BST_CODE, Integer AD_CMPNY) {

        Debug.print("InvBranchStockTransferInEntryControllerBean getAdApprovalNotifiedUsersByBstCode");


        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;

        ArrayList list = new ArrayList();


        // Initialize EJB Home

        try {

            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvBranchStockTransfer invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(BST_CODE);

            if (invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE) {

                list.add("DOCUMENT POSTED");
                return list;

            }

            Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("INV BRANCH STOCK TRANSFER", BST_CODE, AD_CMPNY);

            Iterator i = adApprovalQueues.iterator();

            while(i.hasNext()) {

                LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();

                list.add(adApprovalQueue.getAdUser().getUsrDescription());

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public double getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(String II_NM, String LOC_FRM, String UOM_NM, Date ST_DT, String BR_NM, Integer AD_CMPNY) {

        Debug.print("InvBranchStockTransferOutEntryControllerBean getInvIiUnitCostByIiNameAndUomName");

        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalAdBranchHome adBranchHome = null;

        // Initialize EJB Home

        try {

            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalAdBranch adBranch = adBranchHome.findByBrName(BR_NM, AD_CMPNY);
        	System.out.println(AD_CMPNY+":AD_CMPNY **** II_NM: " + II_NM);
        	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        	double COST = invItem.getIiUnitCost();

        	LocalInvItemLocation invItemLocation = null;


        	try {

        		invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_FRM, II_NM, AD_CMPNY);

      			LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
      					ST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), adBranch.getBrCode(), AD_CMPNY);

      			if(invItemLocation.getInvItem().getIiCostMethod().equals("Average"))
      			{
      				
      				if(invCosting.getCstRemainingQuantity()<=0) {
      					COST = EJBCommon.roundIt(Math.abs(invItemLocation.getInvItem().getIiUnitCost()), this.getGlFcPrecisionUnit(AD_CMPNY));
      				}else {
      					COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), this.getGlFcPrecisionUnit(AD_CMPNY));
      				    if(COST<=0){
                           COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
                        }  

                    }
      				
      			}
      			else if(invItemLocation.getInvItem().getIiCostMethod().equals("FIFO"))
      			{
      				COST = this.getInvFifoCost(ST_DT, invItemLocation.getIlCode(), invCosting.getCstAdjustQuantity(),
      						invCosting.getCstAdjustCost(), false, adBranch.getBrCode(), AD_CMPNY);
      			}
      			else if(invItemLocation.getInvItem().getIiCostMethod().equals("Standard"))
      			{
      				COST = invItemLocation.getInvItem().getIiUnitCost();
      			}

        	} catch (FinderException ex) {

        	}

        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

    		return EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

        	/*LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);

            return EJBCommon.roundIt(COST * invUnitOfMeasure.getUomConversionFactor() / (invItemLocation != null ?
            	invItemLocation.getInvItem().getInvUnitOfMeasure().getUomConversionFactor() :
            		invItem.getInvUnitOfMeasure().getUomConversionFactor()), this.getGlFcPrecisionUnit(AD_CMPNY));
            */

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public LocalInvBranchStockTransfer getInvBstOutByBstNumberAndBstBranch(String BST_NMBR, String BR_NM, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalNoRecordFoundException {

        Debug.print("InvBranchStockTransferInEntryControllerBean getInvBstOutByBstNumber");

        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
        LocalAdBranchHome adBranchHome = null;

        // Initialize EJB Home

        try {

            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME,
                    LocalInvBranchStockTransferHome.class);

            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch(NamingException ex) {

            throw(new EJBException(ex.getMessage()));

        }


        try {


            LocalInvBranchStockTransfer invBranchStockTransfer = null;

            try {

                LocalAdBranch adBranch = adBranchHome.findByBrName(BR_NM, AD_CMPNY);

                invBranchStockTransfer = invBranchStockTransferHome.findByBstNumberAndBrCode(BST_NMBR, adBranch.getBrCode(), AD_CMPNY);

            } catch(FinderException ex) {

                throw new GlobalNoRecordFoundException();
            }

            return invBranchStockTransfer;


        } catch (GlobalNoRecordFoundException ex) {

            Debug.printStackTrace(ex);
            throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrAll(Integer AD_CMPNY) {

        Debug.print("InvBranchStockTransferOutEntryControllerBean getInvLocAll");

        LocalAdBranchHome AdBranchHome = null;
        Collection AdBranches = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            AdBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            AdBranches = AdBranchHome.findBrAll(AD_CMPNY);

            if (AdBranches.isEmpty()) {

                return null;

            }

            Iterator i = AdBranches.iterator();

            while (i.hasNext()) {

                LocalAdBranch AdBranch = (LocalAdBranch)i.next();
                String details = AdBranch.getBrName();

                list.add(details);

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public double getInvIiShippingCostByIiUnitCostAndAdBranch(double II_UNT_CST, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("InvBranchStockTransferOutEntryControllerBean getInvIiMarkupUnitCostByIiUnitCostAndAdBranch");

        LocalAdBranchHome adBranchHome = null;

        // Initialize EJB Home

        try {

            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(AD_BRNCH);

        	double BSL_SHPPNG_CST = 0d;

        	if(adBranch.getBrApplyShipping() == (byte)EJBCommon.TRUE) {

        		BSL_SHPPNG_CST = EJBCommon.roundIt(II_UNT_CST *
    					(adBranch.getBrPercentMarkup() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

        	}

        	return BSL_SHPPNG_CST;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    // private methods

    private String getInvLocNameByLocCode(Integer LOC_CODE) {

        Debug.print("InvBranchStockTransferInEntryControllerBean getInvLocNameByLocCode");

        LocalInvLocationHome invLocationHome = null;

        try {

            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

        } catch(NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            return invLocationHome.findByPrimaryKey(LOC_CODE).getLocName();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }
    }

    private String getAdBrNameByBrCode(Integer BR_CODE) {

        Debug.print("InvBranchStockTransferInEntryControllerBean getAdBrNameByBrCode");

        LocalAdBranchHome adBranchHome = null;

        try {

        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.lookUpLocalHome(
            		LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch(NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            return adBranchHome.findByPrimaryKey(BR_CODE).getBrName();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }
    }

    private LocalInvBranchStockTransferLine addInvBslEntry(InvModBranchStockTransferLineDetails mdetails,
            LocalInvBranchStockTransfer invBranchStockTransfer, Integer AD_CMPNY)
    	throws InvATRAssemblyQtyGreaterThanAvailableQtyException, GlobalMiscInfoIsRequiredException {

        Debug.print("InvBranchStockTransferInEntryControllerBean addInvBslEntry");

        LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvItemLocationHome invItemLocationHome= null;
        LocalInvItemHome invItemHome= null;
        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;

        // Initialize EJB Home

        try {

        	invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);

            invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	System.out.println("mdetails.getBslQuantity()="+mdetails.getBslQuantity());
        	System.out.println("mdetails.getBslQuantityReceived()="+mdetails.getBslQuantityReceived());
        	if(mdetails.getBslQuantity() < mdetails.getBslQuantityReceived())
        		throw new InvATRAssemblyQtyGreaterThanAvailableQtyException();

            LocalInvBranchStockTransferLine invBranchStockTransferLine =
                invBranchStockTransferLineHome.create(mdetails.getBslQuantity(),mdetails.getBslQuantityReceived(),
                        mdetails.getBslUnitCost(),mdetails.getBslAmount(), AD_CMPNY);

            //invBranchStockTransfer.addInvBranchStockTransferLine(invBranchStockTransferLine);
            invBranchStockTransferLine.setInvBranchStockTransfer(invBranchStockTransfer);

            if (mdetails.getBslUomName().equals("")) {
            	mdetails.setBslUomName(invItemHome.findByPartNumber(mdetails.getBslIiName(), AD_CMPNY).getInvUnitOfMeasure().getUomName());
            }

            LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
                    mdetails.getBslUomName(), AD_CMPNY);

            //invUnitOfMeasure.addInvBranchStockTransferLine(invBranchStockTransferLine);
            invBranchStockTransferLine.setInvUnitOfMeasure(invUnitOfMeasure);


            LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
                    mdetails.getBslIiName(), mdetails.getBslLocationName(), AD_CMPNY);

            //invItemLocation.addInvBranchStockTransferLine(invBranchStockTransferLine);
            invBranchStockTransferLine.setInvItemLocation(invItemLocation);


            if (invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiTraceMisc() == 1){
				//TODO: add new inv Tag

				this.createInvTagList(invBranchStockTransferLine,  mdetails.getBslTagList(), AD_CMPNY);

            }
//	    	validate misc
            /*if(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiTraceMisc()==1){
            	if(mdetails.getBslMisc()==null || mdetails.getBslMisc()==""){


	    			throw new GlobalMiscInfoIsRequiredException();

	    		}else{
	    			int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(mdetails.getBslMisc()));

	    			String miscList2Prpgt = this.checkExpiryDates(mdetails.getBslMisc(), qty2Prpgt, "False");
	    			if(miscList2Prpgt!="Error"){
	    				invBranchStockTransferLine.setBslMisc(mdetails.getBslMisc());
	    			}else{
	    				throw new GlobalMiscInfoIsRequiredException();
	    			}

	    		}
            }else{
            	invBranchStockTransferLine.setBslMisc(mdetails.getBslMisc());
            }

            System.out.println("mdetails.getBslMisc() : "+mdetails.getBslMisc());*/


          LocalInvBranchStockTransfer invBranchExistingStockTransfer = null;

          try {

          	System.out.println("LOCK ---------------->");

          	invBranchExistingStockTransfer = invBranchStockTransferHome.findByBstNumberAndAdCompany(
          			invBranchStockTransferLine.getInvBranchStockTransfer().getBstTransferOutNumber(), AD_CMPNY);
          	invBranchExistingStockTransfer.setBstLock(EJBCommon.TRUE);

          } catch (Exception ex){

          }



            return invBranchStockTransferLine;

        }/*catch (GlobalMiscInfoIsRequiredException ex){

            getSessionContext().setRollbackOnly();
            throw ex;


        } */catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    public String checkExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);

    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}

    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;

    	String miscList = new String();
    	String miscList2 = "";

    	for(int x=0; x<qty; x++) {

    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		String g= misc.substring(start, start + length);
    		System.out.println("g: " + g);
    		System.out.println("g length: " + g.length());
    		if(g.length()!=0){
    			if(g!=null || g!="" || g!="null"){
    				if(g.contains("null")){
    					miscList2 = "Error";
    				}else{
    					miscList = miscList + "$" + g;
    				}
    			}else{
    				miscList2 = "Error";
    			}

    			System.out.println("miscList G: " + miscList);
    		}else{
    			System.out.println("KABOOM");
    			miscList2 = "Error";
    		}
    	}
    	System.out.println("miscList2 :" + miscList2);
    	if(miscList2==""){
    		miscList = miscList+"$";
    	}else{
    		miscList = miscList2;
    	}

    	System.out.println("miscList :" + miscList);
    	return (miscList);
    }

    private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

        Debug.print("InvBranchStockTransferOutEntryControllerBean convertByUomFromAndUomToAndQuantity");

        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT,
            Integer COA_CODE, LocalInvBranchStockTransfer invBranchStockTransfer,
            Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalBranchAccountNumberInvalidException {

        Debug.print("InvBranchStockTransferInEntryControllerBean addInvDrEntry");

        LocalAdCompanyHome adCompanyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        // Initialize EJB Home

        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            // get company

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            // validate coa

            LocalGlChartOfAccount glChartOfAccount = null;
            System.out.println("COA_CODE: " + COA_CODE);
            System.out.println("AD_BRNCH: " + AD_BRNCH);
            try {

                glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

            } catch(FinderException ex) {

                throw new GlobalBranchAccountNumberInvalidException ();

            }

            // create distribution record

            LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
                    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
                    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

            //invBranchStockTransfer.addInvDistributionRecord(invDistributionRecord);
            invDistributionRecord.setInvBranchStockTransfer(invBranchStockTransfer);
            invDistributionRecord.setInvBranchStockTransfer(invBranchStockTransfer);

            //glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
            invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	System.out.println("invalid coa: "+ COA_CODE);
        	
            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private void executeInvBstInPost(Integer BST_CODE, String USR_NM, Integer BRNCH_FRM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
    GlobalTransactionAlreadyPostedException,
    GlJREffectiveDateNoPeriodExistException,
    GlJREffectiveDatePeriodClosedException,
    GlobalJournalNotBalanceException,
	AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {

        Debug.print("InvBranchStockTransferInEntryControllerBean executeInvBstInPost");

        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalAdBranchHome adBranchHome = null;

        // Initialize EJB Home

        try {

            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.lookUpLocalHome(
                    LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.lookUpLocalHome(
                    LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.lookUpLocalHome(
            		LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch(NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            // validate if branch stock transfer is already deleted

            LocalInvBranchStockTransfer invBranchStockTransfer = null;

            try {

                invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(BST_CODE);

            } catch (FinderException ex) {

                throw new GlobalRecordAlreadyDeletedException();
            }

            // validate if branch stock transfer is already posted or void

            if (invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE) {

                throw new GlobalTransactionAlreadyPostedException();

            }

            // regenerate inventory dr

            this.regenerateInventoryDr(invBranchStockTransfer, AD_BRNCH, AD_CMPNY);

            Iterator i = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();

            while (i.hasNext()) {

                LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) i.next();

                String locName = invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName();
                String invItemName = invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName();

                String transitLocationName = invBranchStockTransfer.getInvLocation().getLocName();

                LocalInvItemLocation invItemLocationFrom = invItemLocationHome.findByLocNameAndIiName(
                        locName, invItemName, AD_CMPNY);

                LocalInvItemLocation invItemTransitLocation = invItemLocationHome.findByLocNameAndIiName(
                        transitLocationName, invItemName, AD_CMPNY);


                //GET QUANTITY RECEIVED
                double BST_QTY = this.convertByUomAndQuantity(invBranchStockTransferLine.getInvUnitOfMeasure(),
                        invBranchStockTransferLine.getInvItemLocation().getInvItem(),
                        invBranchStockTransferLine.getBslQuantityReceived(), AD_CMPNY);

                
                LocalInvCosting invCosting = null;


                //GET INV COSTING BY DATE IF EXIST
                try {
            
                    invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                            invBranchStockTransfer.getBstDate(), invItemName, locName, AD_BRNCH, AD_CMPNY);

                } catch (FinderException ex) { }


                //GET COST
                double BST_COST = invBranchStockTransferLine.getBslUnitCost();

              
                if (invCosting == null) {
                      //IF THE ITEM HAVE NOT YET INV COSTING . . .
                    this.postToInv(invBranchStockTransferLine, invItemLocationFrom, invBranchStockTransfer.getBstDate(),
                    		BST_QTY, BST_COST * BST_QTY,
                    		BST_QTY, BST_COST * BST_QTY,
                    		0d, null, AD_BRNCH, AD_CMPNY);


                } else {
                    //IF INV COSTING EXIST
                	this.postToInv(
                	       invBranchStockTransferLine, //Branch stock transfer line entity
                	       invItemLocationFrom,        //Inv Location Entity
                	       invBranchStockTransfer.getBstDate(),    //BSTI DATE
                		   BST_QTY,                  //Stock transfer quantity
                		   BST_COST * BST_QTY,       //stock transfer cost * quantity
                		   invCosting.getCstRemainingQuantity() + BST_QTY,  //quantity received from bsto
                		   invCosting.getCstRemainingValue() + (BST_QTY * BST_COST),    //remaining value  which is qty * cost
                		   0d,   // Variance Value
                		   USR_NM, 
                		   AD_BRNCH, 
                		   AD_CMPNY
                		   );


                }

                /*LocalInvCosting invTransitCosting = null;
                try {

                    invTransitCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                            invBranchStockTransfer.getBstDate(), invItemName, transitLocationName, BRNCH_FRM, AD_CMPNY);

                } catch (FinderException ex) { }

                double COST = invBranchStockTransferLine.getBslUnitCost();

                if (invTransitCosting == null) {

                    this.postToInv(invBranchStockTransferLine, invItemTransitLocation, invBranchStockTransfer.getBstDate(),
                    		-BST_QTY, -COST * BST_QTY,
                    		-BST_QTY, -COST * BST_QTY,
                    		0d, null, BRNCH_FRM, AD_CMPNY);


                } else {

                	this.postToInv(invBranchStockTransferLine, invItemTransitLocation, invBranchStockTransfer.getBstDate(),
        					-BST_QTY, -COST * BST_QTY,
        					invTransitCosting.getCstRemainingQuantity() - BST_QTY, invTransitCosting.getCstRemainingValue() - (COST * BST_QTY),
        					0d, null, BRNCH_FRM, AD_CMPNY);


                }*/

            }

            // set branch stock transfer post status

            invBranchStockTransfer.setBstPosted(EJBCommon.TRUE);
            invBranchStockTransfer.setBstPostedBy(USR_NM);
            invBranchStockTransfer.setBstDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

            // post to GL
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            //  validate if date has no period and period is closed

            LocalGlSetOfBook glJournalSetOfBook = null;

            try {

                glJournalSetOfBook = glSetOfBookHome.findByDate(invBranchStockTransfer.getBstDate(), AD_CMPNY);

            } catch (FinderException ex) {

                throw new GlJREffectiveDateNoPeriodExistException();

            }

            LocalGlAccountingCalendarValue glAccountingCalendarValue =
                glAccountingCalendarValueHome.findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invBranchStockTransfer.getBstDate(), AD_CMPNY);

            if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
                    glAccountingCalendarValue.getAcvStatus() == 'C' ||
                    glAccountingCalendarValue.getAcvStatus() == 'P') {

                throw new GlJREffectiveDatePeriodClosedException();

            }

            // check if debit and credit is balance

            LocalGlJournalLine glOffsetJournalLine = null;

            Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByBstCode(invBranchStockTransfer.getBstCode(), AD_CMPNY);

            i = invDistributionRecords.iterator();

            double TOTAL_DEBIT = 0d;
            double TOTAL_CREDIT = 0d;

            while (i.hasNext()) {

                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

                double DR_AMNT = 0d;

                DR_AMNT = invDistributionRecord.getDrAmount();

                if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

                    TOTAL_DEBIT += DR_AMNT;

                } else {

                    TOTAL_CREDIT += DR_AMNT;

                }
            }

            TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
            TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

            if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
                    TOTAL_DEBIT != TOTAL_CREDIT) {

                LocalGlSuspenseAccount glSuspenseAccount = null;

                try {

                    glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "BRANCH STOCK TRANSFERS", AD_CMPNY);

                } catch (FinderException ex) {

                    throw new GlobalJournalNotBalanceException();

                }

                if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

                    glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
                            EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

                } else {

                    glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
                            EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

                }

                LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
                //glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
                glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


            } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
                    TOTAL_DEBIT != TOTAL_CREDIT) {

                throw new GlobalJournalNotBalanceException();

            }

            // create journal batch if necessary

            LocalGlJournalBatch glJournalBatch = null;
            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

            try {

                glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " BRANCH STOCK TRANSFERS", AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) {

            }

            if (glJournalBatch == null) {

                glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " BRANCH STOCK TRANSFERS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);

            }

            // create journal entry

            LocalGlJournal glJournal = glJournalHome.create(invBranchStockTransfer.getBstNumber(),
                    invBranchStockTransfer.getBstDescription(), invBranchStockTransfer.getBstDate(),
                    0.0d, null, invBranchStockTransfer.getBstNumber(), null, 1d, "N/A", null,
                    'N', EJBCommon.TRUE, EJBCommon.FALSE, USR_NM, new Date(), USR_NM, new Date(), null, null,
                    USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, EJBCommon.FALSE,
                    null,
                    AD_BRNCH, AD_CMPNY);

            LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
            glJournal.setGlJournalSource(glJournalSource);

            LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
            glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

            LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("BRANCH STOCK TRANSFERS", AD_CMPNY);
            glJournal.setGlJournalCategory(glJournalCategory);

            if (glJournalBatch != null) {

                glJournal.setGlJournalBatch(glJournalBatch);

            }

            // create journal lines

            i = invDistributionRecords.iterator();

            while (i.hasNext()) {

                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

                double DR_AMNT = 0d;

                DR_AMNT = invDistributionRecord.getDrAmount();

                LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),
                        invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

                //invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
                glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());

                //glJournal.addGlJournalLine(glJournalLine);
                glJournalLine.setGlJournal(glJournal);

                invDistributionRecord.setDrImported(EJBCommon.TRUE);

            }

            if (glOffsetJournalLine != null) {

                //glJournal.addGlJournalLine(glOffsetJournalLine);
                glOffsetJournalLine.setGlJournal(glJournal);

            }

            // post journal to gl

            Collection glJournalLines = glJournal.getGlJournalLines();

            i = glJournalLines.iterator();

            while (i.hasNext()) {

                LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

                // post current to current acv

                this.postToGl(glAccountingCalendarValue,
                        glJournalLine.getGlChartOfAccount(),
                        true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                // post to subsequent acvs (propagate)

                Collection glSubsequentAccountingCalendarValues =
                    glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                            glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                            glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

                Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

                while (acvsIter.hasNext()) {

                    LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                        (LocalGlAccountingCalendarValue)acvsIter.next();

                    this.postToGl(glSubsequentAccountingCalendarValue,
                            glJournalLine.getGlChartOfAccount(),
                            false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


                }

                // post to subsequent years if necessary

                Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

                if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

                    adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
                    LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY);

                    Iterator sobIter = glSubsequentSetOfBooks.iterator();

                    while (sobIter.hasNext()) {

                        LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

                        String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

                        // post to subsequent acvs of subsequent set of book(propagate)

                        Collection glAccountingCalendarValues =
                            glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

                        Iterator acvIter = glAccountingCalendarValues.iterator();

                        while (acvIter.hasNext()) {

                            LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
                                (LocalGlAccountingCalendarValue)acvIter.next();

                            if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
                                    ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

                                this.postToGl(glSubsequentAccountingCalendarValue,
                                        glJournalLine.getGlChartOfAccount(),
                                        false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                            } else {
                                // revenue & expense

                                this.postToGl(glSubsequentAccountingCalendarValue,
                                        glRetainedEarningsAccount,
                                        false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

                            }
                        }

                        if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

                    }
                }
            }

        } catch (GlJREffectiveDateNoPeriodExistException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalRecordAlreadyDeletedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalTransactionAlreadyPostedException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        } catch (GlobalExpiryDateNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        }catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST,
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {

		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;

	     // Initialize EJB Home

	     try {

	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	     }
	     catch (NamingException ex) {

	    	 throw new EJBException(ex.getMessage());
	     }

		try {

			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);

			if (invFifoCostings.size() > 0) {

				Iterator x = invFifoCostings.iterator();

	  			if (isAdjustFifo) {

	  				//executed during POST transaction

	  				double totalCost = 0d;
	  				double cost;

	  				if(CST_QTY < 0) {

	  					//for negative quantities
	 	  				double neededQty = -(CST_QTY);

	 	  				while(x.hasNext() && neededQty != 0) {

	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();
	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
	 		 	  			} else {
	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
	 		 	  			}

	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {

	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
	 			  				totalCost += (neededQty * cost);
	 			  				neededQty = 0d;
	 	  					} else {

	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
	 	  					}
	 	  				}

	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
	 	  				if(neededQty != 0) {

	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
	 	  				}

	 	  				cost = totalCost / -CST_QTY;
	  				}

	  				else {

	  					//for positive quantities
	  					cost = CST_COST;
	  				}
	  				return cost;
	  			}

	  			else {

	  				//executed during ENTRY transaction

	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();

	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			}
	  			}
			}
			else {

				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}

		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}




    private void postToInv(LocalInvBranchStockTransferLine invBranchStockTransferLine, LocalInvItemLocation invItemLocation, Date CST_DT,
    		double CST_ST_QTY, double CST_ST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM,
			Integer AD_BRNCH, Integer AD_CMPNY)throws
			AdPRFCoaGlVarianceAccountNotFoundException, GlobalExpiryDateNotFoundException {

        Debug.print("InvBranchStockTransferInEntryControllerBean post");

        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

        // Initialize EJB Home

        try {

            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	      	lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            int CST_LN_NMBR = 0;

            CST_ST_QTY = EJBCommon.roundIt(CST_ST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_ST_CST = EJBCommon.roundIt(CST_ST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
            CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

            if (CST_ST_QTY < 0) {

                invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ST_QTY));

            }

            try {

                // generate line number

                LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

            } catch (FinderException ex) {

                CST_LN_NMBR = 1;

            }

            //void subsequent cost variance adjustments
            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
            Iterator i = invAdjustmentLines.iterator();

            while (i.hasNext()){

            	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
            	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);

            }

            String prevExpiryDates = "";
            String miscListPrpgt ="";
            double qtyPrpgt = 0;
            double qtyPrpgt2 = 0;
            try {
            
            
         	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
         			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
         	   System.out.println(prevCst.getCstCode());
         	   System.out.println("ITEM "+ prevCst.getInvItemLocation().getInvItem().getIiName() +" "+ prevCst.getCstExpiryDate());

         	   prevExpiryDates = prevCst.getCstExpiryDate();

         	   qtyPrpgt = prevCst.getCstRemainingQuantity();

         	   if (prevExpiryDates==null){
         		   prevExpiryDates="";
         	   }

            }catch (Exception ex){
            	System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
         	   prevExpiryDates="";
            }

            // create costing
            LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, CST_ST_QTY, CST_ST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
            //invItemLocation.addInvCosting(invCosting);
            invCosting.setInvItemLocation(invItemLocation);
            invCosting.setInvBranchStockTransferLine(invBranchStockTransferLine);

            // Get Latest Expiry Dates

            if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0 && invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){

            	String CST_EXPRY_DT = prevExpiryDates;

            	if(invBranchStockTransferLine.getBslMisc()!=null && invBranchStockTransferLine.getBslMisc()!="" && invBranchStockTransferLine.getBslMisc().length()!=0){

            		String TXN_MISC = invBranchStockTransferLine.getBslMisc();

            		TXN_MISC = TXN_MISC.substring(1);
            		TXN_MISC = TXN_MISC.substring(TXN_MISC.indexOf(EJBCommon.DELIMETER));

            		Iterator txnMiscsIter = EJBCommon.miscList(TXN_MISC).iterator();
            		ArrayList txnMiscsList = EJBCommon.miscList(CST_EXPRY_DT);
            		System.out.println(CST_ST_QTY+" :CST_EXPRY_DT: " + CST_EXPRY_DT);
            		StringBuffer str = new StringBuffer(CST_EXPRY_DT);

            		if(CST_ST_QTY>0){
            			while(txnMiscsIter.hasNext())
                        {
                        	String misc = (String)txnMiscsIter.next();
                        	str.append(misc);
                            str.append(EJBCommon.DELIMETER);
                        }
            			/*
            			TXN_MISC = TXN_MISC.substring(1);
            			CST_EXPRY_DT = prevExpiryDates + TXN_MISC;*/

            		}else{

            			//Iterator outMiscIter = txnMiscsList.iterator();

            			while(txnMiscsIter.hasNext()){

            				String misc = (String)txnMiscsIter.next();
                            System.out.println("misc: " + misc);

                            if (txnMiscsList.contains(misc))
                            {
                            	txnMiscsList.remove(misc);
                            }
                            else
                            {
                            	throw new GlobalExpiryDateNotFoundException(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
                            }
                            /*
                        	misc = EJBCommon.DELIMETER + misc + EJBCommon.DELIMETER;

                            if (CST_EXPRY_DT.contains(misc)){
                            	CST_EXPRY_DT.replace(misc, EJBCommon.DELIMETER);
                            }else{
                            	throw new GlobalExpiryDateNotFoundException(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
                            }*/
                            System.out.println("CST_EXPRY_DT: " + CST_EXPRY_DT);

            			}

            			if (txnMiscsList.size() > 0)
                            str = new StringBuffer(EJBCommon.DELIMETER);
                        else
                            str = new StringBuffer();

                        Iterator cstMiscsIter = txnMiscsList.iterator();
                        while(cstMiscsIter.hasNext()){
                        	String misc = (String)cstMiscsIter.next();
                        	str.append(misc);
                            str.append(EJBCommon.DELIMETER);
                        }

            		}
            		CST_EXPRY_DT = str.toString();
            		System.out.println("FIN CST_EXPRY_DT: " + CST_EXPRY_DT);
            		invCosting.setCstExpiryDate(CST_EXPRY_DT);

            	}else{
            		invCosting.setCstExpiryDate(prevExpiryDates);
            		System.out.println("prevExpiryDates");
            	}
            }else{

            	invCosting.setCstExpiryDate("");
            }

			// if cost variance is not 0, generate cost variance for the transaction
			if(CST_VRNC_VL != 0) {

				/*this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"INVBST" + invBranchStockTransferLine.getInvBranchStockTransfer().getBstNumber(),
						invBranchStockTransferLine.getInvBranchStockTransfer().getBstDescription(),
						invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate(), USR_NM, AD_BRNCH, AD_CMPNY);*/

			}

            // propagate balance if necessary
            Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

            i = invCostings.iterator();

            String miscList = "";
            ArrayList miscList2 = null;


            //System.out.println("miscList Propagate:" + miscList);
            String propagateMisc ="";
 		   	String ret = "";

 		   //String CST_EXPRY_DT  = "";
 		   	while (i.hasNext()) {
 		   		String Checker = "";
 		   		String Checker2 = "";

 		   		LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

 		   		if(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
 		   			if(invBranchStockTransferLine.getBslMisc()!=null && invBranchStockTransferLine.getBslMisc()!="" && invBranchStockTransferLine.getBslMisc().length()!=0
 		   					&& invPropagatedCosting.getCstExpiryDate()!=null && invPropagatedCosting.getCstExpiryDate()!=""){

 		   				System.out.println("invBranchStockTransferLine.getBslMisc(): "+invBranchStockTransferLine.getBslMisc());
 		   				System.out.println("getAlAdjustQuantity(): "+invBranchStockTransferLine.getBslQuantity());


 		   				String CST_EXPRY_DT  = invPropagatedCosting.getCstExpiryDate();

 		   				System.out.println("invPropagatedCosting.getCstCode(): " + invPropagatedCosting.getCstCode());
 		   				System.out.println("CST_EXPRY_DT: " + CST_EXPRY_DT);

 		   				String TXN_MISC = invBranchStockTransferLine.getBslMisc();

 		   				TXN_MISC = TXN_MISC.substring(1);
 		   				TXN_MISC = TXN_MISC.substring(TXN_MISC.indexOf(EJBCommon.DELIMETER));

 		   				Iterator txnMiscsIter = EJBCommon.miscList(TXN_MISC).iterator();
 		   				ArrayList txnMiscsList = EJBCommon.miscList(CST_EXPRY_DT);

 		   				StringBuffer str = new StringBuffer(CST_EXPRY_DT);

 		   				if(CST_ST_QTY>0){
 		   					while(txnMiscsIter.hasNext())
 		   					{
 		   						String misc = (String)txnMiscsIter.next();
 		   						str.append(misc);
 		   						str.append(EJBCommon.DELIMETER);
 		   					}
 		   					/*
            			TXN_MISC = TXN_MISC.substring(1);
            			CST_EXPRY_DT = prevExpiryDates + TXN_MISC;
 		   					 */
 		   				}else{

 		   					//Iterator outMiscIter = txnMiscsList.iterator();

 		   					while(txnMiscsIter.hasNext()){

 		   						String misc = (String)txnMiscsIter.next();
 		   						System.out.println("misc: " + misc);

 		   						if (txnMiscsList.contains(misc))
 		   						{
 		   							txnMiscsList.remove(misc);
 		   						}
 		   						else
 		   						{
 		   							throw new GlobalExpiryDateNotFoundException(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
 		   						}
 		   						/*
            				String misc = (String)outMiscIter.next();

            				misc = EJBCommon.DELIMETER + misc + EJBCommon.DELIMETER;

            				if (CST_EXPRY_DT.contains(misc)){
                            	CST_EXPRY_DT.replace(misc, EJBCommon.DELIMETER);
                            }else{
                            	throw new GlobalExpiryDateNotFoundException(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
                            }*/
 		   					}

 		   					if (txnMiscsList.size() > 0)
 		   						str = new StringBuffer(EJBCommon.DELIMETER);
 		   					else
 		   						str = new StringBuffer();

 		   					Iterator cstMiscsIter = txnMiscsList.iterator();
 		   					while(cstMiscsIter.hasNext()){
 		   						String misc = (String)cstMiscsIter.next();
 		   						str.append(misc);
 		   						str.append(EJBCommon.DELIMETER);
 		   					}
 		   				}
 		   				System.out.println("str.toString(): " + str.toString());
 		   				if(invPropagatedCosting.getCstExpiryDate()!=null && invPropagatedCosting.getCstExpiryDate()!=""){
 		   					CST_EXPRY_DT = str.toString();
 		   					invPropagatedCosting.setCstExpiryDate(CST_EXPRY_DT);
 		   				}
 		   			}else{
 		   				invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
 		   				System.out.println("prevExpiryDates");
 		   			}
 		   		}


 		   		invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ST_QTY);
 		   		invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ST_QTY);

 		   	}


            // regenerate cost varaince
            //this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);


        } /*catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	getSessionContext().setRollbackOnly();
        	throw ex;

        }*/catch (GlobalExpiryDateNotFoundException ex){
        	System.out.println("Huli Ka");
        	throw ex;
        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }



    }

    public String getQuantityExpiryDates(String qntty){
    	String separator = "$";

    	// Remove first $ character
    	qntty = qntty.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = qntty.indexOf(separator, start);
    	int length = nextIndex - start;
    	String y;
    	y = (qntty.substring(start, start + length));
    	System.out.println("Y " + y);

    	return y;
    }

    private ArrayList expiryDates(String misc, double qty) throws Exception{
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	String separator ="$";
    	int check =0;


    	// Remove first $ character
    	misc = misc.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;

    	System.out.println("qty" + qty);
    	ArrayList miscList = new ArrayList();

    	for(int x=0; x<=qty; x++) {

    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		try{
    			String checker = misc.substring(start, start + length);
        		if(checker.length()!=0 || checker!="null"){
        			miscList.add(checker);
        		}else{
        			miscList.add("null");
        		}
    		}catch(Exception e){
    			check=1;

    		}
    		if(check==1)
    			break;
    	}

    	System.out.println("miscList :" + miscList);
    	return miscList;
    }

    public static int checkExpiryDates(String misc) throws Exception {

		String separator ="$";

		// Remove first $ character
		misc = misc.substring(1);
		//System.out.println("misc: " + misc);
		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;
		int numberExpry=0;
		String miscList = new String();
		String miscList2 = "";
		String g= "";
		try{
			while(g!="fin"){
				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				g= misc.substring(start, start + length);
				if(g.length()!=0){
					if(g!=null || g!="" || g!="null"){
						if(g.contains("null")){
							miscList2 = "Error";
						}else{
							miscList = miscList + "$" + g;
							numberExpry++;
						}
					}else{
						miscList2 = "Error";
					}

				}else{
					miscList2 = "Error";
				}
			}
		}catch(Exception e){

		}

		if(miscList2==""){
			miscList = miscList+"$";
		}else{
			miscList = miscList2;
		}

		return (numberExpry);
	}

    public String propagateExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("InvBranchStockTransferOutControllerBean getExpiryDates");
    	//System.out.println("misc: " + misc);

    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}

    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;

    	String miscList = new String();

		for(int x=0; x<qty; x++) {


			// Date
			start = nextIndex + 1;
			nextIndex = misc.indexOf(separator, start);
			length = nextIndex - start;
			String g= misc.substring(start, start + length);
			System.out.println("g: " + g);
			System.out.println("g length: " + g.length());
				if(g.length()!=0){
					miscList = miscList + "$" + g;
					System.out.println("miscList G: " + miscList);
				}
		}

		miscList = miscList+"$";
		System.out.println("miscList :" + miscList);
		return (miscList);
    }

    private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue,
            LocalGlChartOfAccount glChartOfAccount,
            boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {

        Debug.print("InvBranchStockTransferInEntryControllerBean postToGl");

        LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        // Initialize EJB Home

        try {

            glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            LocalGlChartOfAccountBalance glChartOfAccountBalance = glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
                    glAccountingCalendarValue.getAcvCode(), glChartOfAccount.getCoaCode(), AD_CMPNY);

            String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
            short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();

            if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
                    isDebit == EJBCommon.TRUE) || (!ACCOUNT_TYPE.equals("ASSET") &&
                            !ACCOUNT_TYPE.equals("EXPENSE") && isDebit == EJBCommon.FALSE)) {

                glChartOfAccountBalance.setCoabEndingBalance(
                        EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

                if (!isCurrentAcv) {

                    glChartOfAccountBalance.setCoabBeginningBalance(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN));

                }
            } else {

                glChartOfAccountBalance.setCoabEndingBalance(
                        EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

                if (!isCurrentAcv) {

                    glChartOfAccountBalance.setCoabBeginningBalance(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN));

                }
            }

            if (isCurrentAcv) {

                if (isDebit == EJBCommon.TRUE) {

                    glChartOfAccountBalance.setCoabTotalDebit(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));

                } else {

                    glChartOfAccountBalance.setCoabTotalCredit(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));
                }
            }
        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }
    }

    private void regenerateInventoryDr(LocalInvBranchStockTransfer invBranchStockTransfer, Integer AD_BRNCH, Integer AD_CMPNY) throws
    GlobalInventoryDateException,
    GlobalBranchAccountNumberInvalidException {

        Debug.print("InvBranchStockTransferInEntryControllerBean regenerateInventoryDr");

        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;

        // Initialize EJB Home

        try {

            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
		    lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            // regenerate inventory distribution records

            Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByBstCode(
                    invBranchStockTransfer.getBstCode(), AD_CMPNY);

            Iterator i = invDistributionRecords.iterator();

            while (i.hasNext()) {

                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

                i.remove();
                invDistributionRecord.remove();

            }

            Collection invBranchStockTransferLines = invBranchStockTransfer.getInvBranchStockTransferLines();

            i = invBranchStockTransferLines.iterator();

            while(i.hasNext()) {

                LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) i.next();

                LocalInvItemLocation invItemLocation = null;

                String locName = invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName();
                String invItemName = invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName();

                try {

                    invItemLocation = invItemLocationHome.findByLocNameAndIiName(locName, invItemName, AD_CMPNY);

                } catch (FinderException ex) {

                    throw new GlobalInvItemLocationNotFoundException(invItemName + " - " + locName);

                }

                LocalInvItemLocation invItemTransitLocation = null;

                try {

                    invItemTransitLocation = invItemLocationHome.findByLocNameAndIiName(
                            invBranchStockTransfer.getInvLocation().getLocName(),
                            invItemName, AD_CMPNY);

                } catch (FinderException ex) {

                    throw new GlobalInvItemLocationNotFoundException("Transit Location " + String.valueOf(invBranchStockTransfer.getInvLocation().getLocName()));

                }

                if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	                //	start date validation

	                Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	                        invBranchStockTransfer.getBstDate(), invItemName, locName, AD_BRNCH, AD_CMPNY);

	                if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemName);
                }


                // add physical inventory distribution

                double COST = invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiUnitCost();

                try {

                    LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                    		invBranchStockTransfer.getBstDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                    if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

                    	COST = invCosting.getCstRemainingQuantity() <= 0 ? COST:
                    	Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                        if(COST<=0){
                           COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
                        }  



                    else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))

                    	COST = invCosting.getCstRemainingQuantity() <= 0 ? COST:
            						this.getInvFifoCost(invBranchStockTransfer.getBstDate(), invItemLocation.getIlCode(), invBranchStockTransferLine.getBslQuantity(),
            								invBranchStockTransferLine.getBslUnitCost(), false, AD_BRNCH, AD_CMPNY);

                    else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))

                    	COST = invItemLocation.getInvItem().getIiUnitCost();

                } catch (FinderException ex) { }


                double AMOUNT = 0d;

                AMOUNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantityReceived() * COST,
                        this.getGlFcPrecisionUnit(AD_CMPNY));


                // check branch mapping

                LocalAdBranchItemLocation adBranchItemLocation = null;

                try{

                    adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                            invBranchStockTransferLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

                } catch (FinderException ex) {

                }

                LocalGlChartOfAccount glChartOfAccount = null;

                if (adBranchItemLocation == null) {

                    glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                            invBranchStockTransferLine.getInvItemLocation().getIlGlCoaInventoryAccount());

                } else {

                    glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                            adBranchItemLocation.getBilCoaGlInventoryAccount());

                }

                this.addInvDrEntry(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                        Math.abs(AMOUNT), glChartOfAccount.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);


                // check branch mapping for transit location

                LocalAdBranchItemLocation adBranchItemTransitLocation = null;

                try{

                    adBranchItemTransitLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                            invItemTransitLocation.getIlCode(), AD_BRNCH, AD_CMPNY);


                } catch (FinderException ex) {

                }

                LocalGlChartOfAccount glChartOfAccountTransit = null;

                if (adBranchItemTransitLocation == null) {

                    glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
                            invItemTransitLocation.getIlGlCoaWipAccount());

                } else {

                    glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
                            adBranchItemTransitLocation.getBilCoaGlWipAccount());

                }

                // add dr for inventory transit location

                this.addInvDrEntry(invBranchStockTransfer.getInvDrNextLine(), "IN TRANSIT", EJBCommon.FALSE,
                        Math.abs(AMOUNT), glChartOfAccountTransit.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);

            }

        } catch (GlobalInventoryDateException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch(GlobalBranchAccountNumberInvalidException ex) {

            throw new GlobalBranchAccountNumberInvalidException ();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("InvBranchStockTransferInEntryControllerBean voidInvAdjustment");

    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();

    			Iterator i = invDistributionRecords.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				list.add(invDistributionRecord);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();

    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(),
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

    			}

    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				list.add(invAdjustmentLine);

    			}

    			i = list.iterator();

    			while (i.hasNext()) {

    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}

    			invAdjustment.setAdjVoid(EJBCommon.TRUE);

    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("InvBranchStockTransferInEntryControllerBean generateCostVariance");

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

    	// Initialize EJB Home

    	try {

    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{



    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;


    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		try{

    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);

    		} catch (FinderException ex) {

    			throw new AdPRFCoaGlVarianceAccountNotFoundException();

    		}

    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment,
    				CST_VRNC_VL, EJBCommon.TRUE, AD_CMPNY);

    		// check for branch mapping

    		LocalAdBranchItemLocation adBranchItemLocation = null;

    		try{

    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		LocalGlChartOfAccount glInventoryChartOfAccount = null;

    		if (adBranchItemLocation == null) {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {

    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());

    		}


    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;

    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE",
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);

    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance");

    	try {

    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			if(prevInvCosting.getCstRemainingQuantity() < 0) {

    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance A");
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
						Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance B");
    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();

    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
						Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance C");
    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();

    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {

    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();

    					}

    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(),
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
   						Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance D");
   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" +
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();

   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" +
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}

    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);
    					Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance E");
    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" +
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();

    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();

    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {

    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() *
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
    						Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance F");
    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();
    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();

    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
							Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance G");
    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(),
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
							Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance H");
    					}

    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" +
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();

    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" +
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
						Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance I");
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();

    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(),
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
						Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance J");
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();

    				} else {

    					prevInvCosting = invPropagatedCosting;
    					continue;

    				}

    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;

    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());

    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
    				Debug.print("InvBranchStockTransferInEntryControllerBean regenerateCostVariance K");
    			}

    			// set previous costing
    			prevInvCosting = invPropagatedCosting;

    		}

    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}     */
    }

    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)

    throws GlobalBranchAccountNumberInvalidException {

    	Debug.print("InvBranchStockTransferInEntryControllerBean addInvDrEntry");

    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;

    	// Initialize EJB Home

    	try {

    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// get company

    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    		// validate coa

    		LocalGlChartOfAccount glChartOfAccount = null;

    		try {

    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

    		} catch(FinderException ex) {

    			throw new GlobalBranchAccountNumberInvalidException ();

    		}

    		// create distribution record

    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					AD_CMPNY);

    		//invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvAdjustment(invAdjustment);
    		//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

    	} catch(GlobalBranchAccountNumberInvalidException ex) {

    		throw new GlobalBranchAccountNumberInvalidException ();

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {

    	Debug.print("InvBranchStockTransferInEntryControllerBean executeInvAdjPost");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    	// Initialize EJB Home

    	try {

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// validate if adjustment is already deleted

    		LocalInvAdjustment invAdjustment = null;

    		try {

    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);

    		} catch (FinderException ex) {

    			throw new GlobalRecordAlreadyDeletedException();

    		}

    		// validate if adjustment is already posted or void

    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();

    		}

    		Collection invAdjustmentLines = null;

    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

    		Iterator i = invAdjustmentLines.iterator();

    		while(i.hasNext()) {


    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);

    		}

    		// post to gl if necessary

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

    			// validate if date has no period and period is closed

    			LocalGlSetOfBook glJournalSetOfBook = null;

    			try {

    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);

    			} catch (FinderException ex) {

    				throw new GlJREffectiveDateNoPeriodExistException();

    			}

    			LocalGlAccountingCalendarValue glAccountingCalendarValue =
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);


    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {

    				throw new GlJREffectiveDatePeriodClosedException();

    			}

    			// check if invoice is balance if not check suspense posting

    			LocalGlJournalLine glOffsetJournalLine = null;

    			Collection invDistributionRecords = null;

    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			} else {

    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);

    			}


    			Iterator j = invDistributionRecords.iterator();

    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {

    					TOTAL_DEBIT += DR_AMNT;

    				} else {

    					TOTAL_CREDIT += DR_AMNT;

    				}

    			}

    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());

    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				LocalGlSuspenseAccount glSuspenseAccount = null;

    				try {

    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);

    				} catch (FinderException ex) {

    					throw new GlobalJournalNotBalanceException();

    				}

    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE,
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);

    				} else {

    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);

    				}

    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);


    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {

    				throw new GlobalJournalNotBalanceException();

    			}

    			// create journal batch if necessary

    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

    			try {

    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);

    			} catch (FinderException ex) {

    			}

    			if (glJournalBatch == null) {

    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);

    			}

    			// create journal entry

    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(),
						null, null, EJBCommon.FALSE, null,

						AD_BRNCH, AD_CMPNY);

    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);


    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);

    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("BRANCH STOCK TRANSFERS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);


    			if (glJournalBatch != null) {

    				glJournal.setGlJournalBatch(glJournalBatch);

    			}

    			// create journal lines

    			j = invDistributionRecords.iterator();

    			while (j.hasNext()) {

    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();

    				double DR_AMNT = 0d;

    				DR_AMNT = invDistributionRecord.getDrAmount();

    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);

    				//invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());

    				//glJournal.addGlJournalLine(glJournalLine);
    				glJournalLine.setGlJournal(glJournal);

    				invDistributionRecord.setDrImported(EJBCommon.TRUE);


    			}

    			if (glOffsetJournalLine != null) {

    				//glJournal.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlJournal(glJournal);

    			}

    			// post journal to gl

    			Collection glJournalLines = glJournal.getGlJournalLines();

    			i = glJournalLines.iterator();

    			while (i.hasNext()) {

    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();

    				// post current to current acv

    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);


    				// post to subsequent acvs (propagate)

    				Collection glSubsequentAccountingCalendarValues =
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);

    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();

    				while (acvsIter.hasNext()) {

    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    						(LocalGlAccountingCalendarValue)acvsIter.next();

    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    				}

    				// post to subsequent years if necessary

    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);

    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {

    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY);

    					Iterator sobIter = glSubsequentSetOfBooks.iterator();

    					while (sobIter.hasNext()) {

    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();

    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();

    						// post to subsequent acvs of subsequent set of book(propagate)

    						Collection glAccountingCalendarValues =
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);

    						Iterator acvIter = glAccountingCalendarValues.iterator();

    						while (acvIter.hasNext()) {

    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue =
    								(LocalGlAccountingCalendarValue)acvIter.next();

    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							} else { // revenue & expense

    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);

    							}

    						}

    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;

    					}

    				}

    			}

			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlJREffectiveDatePeriodClosedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalJournalNotBalanceException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalRecordAlreadyDeletedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (GlobalTransactionAlreadyPostedException ex) {

    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {

    	Debug.print("InvBranchStockTransferInEntryControllerBean addInvAlEntry");

    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;

    	// Initialize EJB Home

    	try {

    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL,null,null, 0,0, AL_VD, AD_CMPNY);

    		// map adjustment, unit of measure, item location
    		//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvAdjustment(invAdjustment);
    		//invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
    		//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvItemLocation(invItemLocation);

    		return invAdjustmentLine;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    }

    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){

    	Debug.print("InvBranchStockTransferInEntryControllerBean saveInvAdjustment");

    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home

    	try{

    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}

    	try{

    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;

    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

    		try {

    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		try {

    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

    		} catch (FinderException ex) {

    		}

    		while (true) {

    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));
    					break;

    				}

    			} else {

    				try {

    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));

    				} catch (FinderException ex) {

    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					break;

    				}

    			}

    		}

    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

    		return invAdjustment;

    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}


    }

    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {

    	Debug.print("InvBranchStockTransferInEntryControllerBean postInvAdjustmentToInventory");

    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;

    	// Initialize EJB Home

    	try {

    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    	} catch (NamingException ex) {

    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}

    	try {

    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;

    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());

    		if (CST_ADJST_QTY < 0) {

    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));

    		}

    		// create costing

    		try {

    			// generate line number

    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;

    		} catch (FinderException ex) {

    			CST_LN_NMBR = 1;

    		}

    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		//invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvItemLocation(invItemLocation);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);

    		// propagate balance if necessary

    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

    		Iterator i = invCostings.iterator();

    		while (i.hasNext()) {

    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();

    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);

    		}


    	} catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}



    }


    private void createInvTagList(LocalInvBranchStockTransferLine invBranchStockTransferLine, ArrayList list, Integer AD_CMPNY) throws Exception {

    	Debug.print("InvBranchStockTransferOrderInControllerBean createInvTagList");

    	LocalAdUserHome adUserHome = null;
    	LocalInvTagHome invTagHome = null;

    	// Initialize EJB Home

    	try{
    		adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
    		invTagHome = (LocalInvTagHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

    	} catch (NamingException ex) {

    		throw new EJBException(ex.getMessage());

    	}



    	try {
			System.out.println("aabot?");
	  	    	//Iterator t = apPurchaseOrderLine.getInvTag().iterator();
  	  	    Iterator t = list.iterator();

  	  	    LocalInvTag invTag  = null;
  	  	    System.out.println("umabot?");
  	  	    while (t.hasNext()){
  	  	    	InvModTagListDetails tgLstDetails = (InvModTagListDetails)t.next();
  	  	    	System.out.println(tgLstDetails.getTgCustodian() + "<== custodian");
  	  	    	System.out.println(tgLstDetails.getTgSpecs() + "<== specs");
  	  	    	System.out.println(tgLstDetails.getTgPropertyCode() + "<== propertyCode");
  	  	    	System.out.println(tgLstDetails.getTgExpiryDate() + "<== expiryDate");
  	  	    	System.out.println(tgLstDetails.getTgSerialNumber() + "<== serial number");

  	  	    	if (tgLstDetails.getTgCode()==null){
  	  	    		System.out.println("ngcreate ba?");
      	  	    	invTag = invTagHome.create(tgLstDetails.getTgPropertyCode(),
      	  	    			tgLstDetails.getTgSerialNumber(),null,tgLstDetails.getTgExpiryDate(),
      	  	    			tgLstDetails.getTgSpecs(), AD_CMPNY, tgLstDetails.getTgTransactionDate(),
      	  	    			tgLstDetails.getTgType());

      	  	    	invTag.setInvBranchStockTransferLine(invBranchStockTransferLine);
      	  	    	invTag.setInvItemLocation(invBranchStockTransferLine.getInvItemLocation());
      	  	    	LocalAdUser adUser = null;
      	  	    	try {
      	  	    		adUser = adUserHome.findByUsrName(tgLstDetails.getTgCustodian(), AD_CMPNY);
      	  	    	}catch(FinderException ex){

      	  	    	}
      	  	    	invTag.setAdUser(adUser);
      	  	    	System.out.println("ngcreate ba?");
  	  	    	}

  	  	    }




		}catch(Exception ex) {
			throw ex;
		}
    }


    private ArrayList getInvTagList(LocalInvBranchStockTransferLine arInvBranchStockTransferLine) {

    	ArrayList list = new ArrayList();

    	Collection invTags = arInvBranchStockTransferLine.getInvTags();
		Iterator x = invTags.iterator();
		while (x.hasNext()) {
			LocalInvTag invTag = (LocalInvTag) x.next();
			InvModTagListDetails tgLstDetails = new InvModTagListDetails();
			tgLstDetails.setTgPropertyCode(invTag.getTgPropertyCode());
			tgLstDetails.setTgSpecs(invTag.getTgSpecs());
			tgLstDetails.setTgExpiryDate(invTag.getTgExpiryDate());
			tgLstDetails.setTgSerialNumber(invTag.getTgSerialNumber());
			try{

				tgLstDetails.setTgCustodian(invTag.getAdUser().getUsrName());
			}
			catch(Exception ex){
				tgLstDetails.setTgCustodian("");
			}

			list.add(tgLstDetails);

			System.out.println(tgLstDetails.getTgPropertyCode() + "<== property code inside controllerbean ");
			System.out.println(tgLstDetails.getTgSpecs() + "<== specs inside controllerbean ");
			System.out.println(list+ "<== taglist inside controllerbean ");

		}

    	return list;

    }


    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

		Debug.print("InvBranchStockTransferInEntryControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("InvBranchStockTransferInEntryControllerBean ejbCreate");

    }

}
