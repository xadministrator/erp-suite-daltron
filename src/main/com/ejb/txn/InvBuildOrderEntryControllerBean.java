package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArSalesOrder;
import com.ejb.ar.LocalArSalesOrderHome;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.ar.LocalArSalesOrderLineHome;
import com.ejb.exception.ArINVNoSalesOrderLinesFoundException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalOverapplicationNotAllowedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvBuildOrder;
import com.ejb.inv.LocalInvBuildOrderHome;
import com.ejb.inv.LocalInvBuildOrderLine;
import com.ejb.inv.LocalInvBuildOrderLineHome;
import com.ejb.inv.LocalInvBuildOrderStock;
import com.ejb.inv.LocalInvBuildOrderStockHome;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.ArModSalesOrderDetails;
import com.util.ArModSalesOrderLineDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModBuildOrderDetails;
import com.util.InvModBuildOrderLineDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="InvBuildOrderEntryControllerEJB"
 *           display-name="used for building order entry"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvBuildOrderEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvBuildOrderEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvBuildOrderEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvBuildOrderEntryControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {
                    
        Debug.print("InvBuildOrderEntryControllerBean getInvLocAll");
        
        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
	               
            invLocations = invLocationHome.findLocAll(AD_CMPNY);            
        
	        if (invLocations.isEmpty()) {
	        	
	        	return null;
	        	
	        }
	        
	        Iterator i = invLocations.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalInvLocation invLocation = (LocalInvLocation)i.next();	
	        	String details = invLocation.getLocName();
	        	
	        	list.add(details);
	        	
	        }
	        
	        return list;
        
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
            
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModBuildOrderDetails getInvBorByBorCode(Integer BOR_CODE, String TYPE, Integer AD_BRNCH, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException {
                    
        Debug.print("InvBuildOrderEntryControllerBean getInvBorByBorCode");
        
        LocalInvBuildOrderHome invBuildOrderHome = null;
        LocalArSalesOrderHome arSalesOrderHome = null;
     
        // Initialize EJB Home
        
        try {
            
        	invBuildOrderHome = (LocalInvBuildOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildOrderHome.JNDI_NAME, LocalInvBuildOrderHome.class);
        	arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
        		lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalInvBuildOrder invBuildOrder = null;
        	LocalArSalesOrder arSalesOrder = null;
        
	        try {
		               
	        	invBuildOrder = invBuildOrderHome.findByPrimaryKey(BOR_CODE);
	            
	        } catch (FinderException ex) {
	            
	            throw new GlobalNoRecordFoundException();
	            
	        }
	        
	        
	        ArrayList bolList = new ArrayList();
	        String soNumber = "";
	        String customerName = "";
	        
	        if(TYPE.equals("ITEMS")) {
		        
		        Collection invBuildOrderLines = invBuildOrder.getInvBuildOrderLines();
		        
		        Iterator i = invBuildOrderLines.iterator();
		        
		        while (i.hasNext()) {
		        	
		        	LocalInvBuildOrderLine invBuildOrderLine = (LocalInvBuildOrderLine)i.next();
		        	
		        	InvModBuildOrderLineDetails mdetails = new InvModBuildOrderLineDetails();
		        	
		        	mdetails.setBolCode(invBuildOrderLine.getBolCode());
		        	mdetails.setBolQuantityRequired(invBuildOrderLine.getBolQuantityRequired());
		        	mdetails.setBolIlIiUomName(invBuildOrderLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
		        	mdetails.setBolIlLocName(invBuildOrderLine.getInvItemLocation().getInvLocation().getLocName());
		        	mdetails.setBolIlIiName(invBuildOrderLine.getInvItemLocation().getInvItem().getIiName());
		        	mdetails.setBolIlIiDescription(invBuildOrderLine.getInvItemLocation().getInvItem().getIiDescription());
		        	
		        	mdetails.setBolRemaining(0d);
		        	//mdetails.setBolUnitCost(0d);
		        	//mdetails.setBolTotalCost(0d);
		        	mdetails.setBolTotalDiscount(0d);
		        	mdetails.setBolLineNumber(null);
		        	mdetails.setBolIssue(false);
		        	mdetails.setBolMisc(invBuildOrderLine.getBolMisc());
		        	System.out.println("invBuildOrderLine.getBolMisc(): "+invBuildOrderLine.getBolMisc());
		        	bolList.add(mdetails);
		        }
	        } else if(TYPE.equals("SO MATCHED")) {

	        	arSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndBrCode(
	        			invBuildOrder.getBorSoNumber(), AD_BRNCH, AD_CMPNY);
	        	
	        	soNumber = arSalesOrder.getSoDocumentNumber();
	        	customerName = arSalesOrder.getArCustomer().getCstName();

	        	Collection invBuildOrderLines = invBuildOrder.getInvBuildOrderLines();
	        	
	        	Iterator i = invBuildOrderLines.iterator();
	        	
	        	while(i.hasNext()) {
	        		
	        		LocalInvBuildOrderLine invBuildOrderLine = (LocalInvBuildOrderLine)i.next();
	        		
	        		InvModBuildOrderLineDetails mdetails = new InvModBuildOrderLineDetails();
	        		
	        		mdetails.setBolCode(invBuildOrderLine.getArSalesOrderLine().getSolCode());
	        		mdetails.setBolQuantityRequired(invBuildOrderLine.getBolQuantityRequired());
	        		mdetails.setBolIlIiUomName(invBuildOrderLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
	        		mdetails.setBolIlLocName(invBuildOrderLine.getInvItemLocation().getInvLocation().getLocName());
	        		mdetails.setBolIlIiName(invBuildOrderLine.getInvItemLocation().getInvItem().getIiName());
	        		mdetails.setBolIlIiDescription(invBuildOrderLine.getInvItemLocation().getInvItem().getIiDescription());
	        		
	        		mdetails.setBolRemaining(invBuildOrderLine.getArSalesOrderLine().getSolQuantity() - 
	        				invBuildOrderLine.getArSalesOrderLine().getSolRequestQuantity());
	        		mdetails.setBolUnitCost(invBuildOrderLine.getArSalesOrderLine().getSolUnitPrice());
	        		mdetails.setBolTotalCost(invBuildOrderLine.getArSalesOrderLine().getSolAmount());
	        		mdetails.setBolLineNumber(new Short(invBuildOrderLine.getArSalesOrderLine().getSolLine()).toString());
	        		mdetails.setBolIssue(true);
	        		
	        		bolList.add(mdetails);
	        	}
	        }
        
      
	        InvModBuildOrderDetails details = new InvModBuildOrderDetails();
	        details.setBorCode(invBuildOrder.getBorCode());
	        details.setBorDocumentNumber(invBuildOrder.getBorDocumentNumber());
	        details.setBorReferenceNumber(invBuildOrder.getBorReferenceNumber());
	        details.setBorDescription(invBuildOrder.getBorDescription());
	        details.setBorDate(invBuildOrder.getBorDate());
	        details.setBorApprovalStatus(invBuildOrder.getBorApprovalStatus());
	        details.setBorPosted(invBuildOrder.getBorPosted());
	        details.setBorCreatedBy(invBuildOrder.getBorCreatedBy());
	        details.setBorDateCreated(invBuildOrder.getBorDateCreated());
	        details.setBorLastModifiedBy(invBuildOrder.getBorLastModifiedBy());
	        details.setBorDateLastModified(invBuildOrder.getBorDateLastModified());
	        details.setBorVoid(invBuildOrder.getBorVoid());
	        details.setBorApprovedRejectedBy(invBuildOrder.getBorApprovedRejectedBy());
	        details.setBorDateApprovedRejected(invBuildOrder.getBorDateApprovedRejected());
	        details.setBorPostedBy(invBuildOrder.getBorPostedBy());
	        details.setBorDatePosted(invBuildOrder.getBorDatePosted());
	        details.setBorReasonForRejection(invBuildOrder.getBorReasonForRejection());
	        
	        if(TYPE.equals("SO MATCHED")) {
	        	details.setBorSoNumber(soNumber);
	        	details.setBorCustomerName(customerName);
	        }
	        
	        details.setBorBolList(bolList);
	        
	        return details;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	Debug.printStackTrace(ex);
        	throw ex;
        	
    	} catch (Exception ex) {
        	
    		Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public String getInvUomByIiName(String II_NM, Integer AD_CMPNY) {
    	
    	Debug.print("InvBuildOrderEntryControllerBean getInvUomByIiName");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemHome invItemHome = null;
        
        
        // Initialize EJB Home
        
        try {
            
        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
        	
        	LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY); 
        	
        	return invItem.getInvUnitOfMeasure().getUomName();                		
        		
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getSolUomByIiName(String II_NM, Integer AD_CMPNY) {
        
        Debug.print("InvBuildOrderEntryControllerBean getInvUomByIiName");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemHome invItemHome = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
            
            LocalInvItem invItem = null;
            LocalInvUnitOfMeasure invItemUnitOfMeasure = null;
            
            invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);        	
            invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();
            
            Collection invUnitOfMeasures = null;
            Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
                    invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
            while (i.hasNext()) {
                
                LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
                InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
                details.setUomName(invUnitOfMeasure.getUomName());
                
                if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {
                    
                    details.setDefault(true);
                    
                }
                
                list.add(details);
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArModSalesOrderDetails getArSoBySoDocumentNumberAndAdBranch(
            String SO_DCMNT_NMBR, Integer AD_BRNCH, Integer AD_CMPNY) throws 
    GlobalNoRecordFoundException,
    ArINVNoSalesOrderLinesFoundException{	
        
        Debug.print("InvBuildOrderEntryControllerBean getArSoBySoDocumentNumberAndAdBranch");
        
        LocalArSalesOrderHome arSalesOrderHome = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
            lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        LocalArSalesOrder arSalesOrder = null;
        
        try {                        
            
        	arSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndBrCode(SO_DCMNT_NMBR, AD_BRNCH, AD_CMPNY);
        	
            if(arSalesOrder.getSoPosted() != EJBCommon.TRUE) {
                
                throw new GlobalNoRecordFoundException();
                
            }
            
            // get sales order line
            
            Collection arSalesOrderLines = arSalesOrder.getArSalesOrderLines();
            
            Iterator i = arSalesOrderLines.iterator();
            
            while(i.hasNext()) {                                
                
                LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) i.next();
                
                ArModSalesOrderLineDetails solDetails = new ArModSalesOrderLineDetails();
                
                solDetails.setSolCode(arSalesOrderLine.getSolCode());
                solDetails.setSolLine(arSalesOrderLine.getSolLine());
                solDetails.setSolUnitPrice(arSalesOrderLine.getSolUnitPrice());
                solDetails.setSolIiName(arSalesOrderLine.getInvItemLocation().getInvItem().getIiName());
                solDetails.setSolLocName(arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName());
                solDetails.setSolUomName(arSalesOrderLine.getInvUnitOfMeasure().getUomName());
                solDetails.setSolIiDescription(arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription());                              
                
                double TOTAL_REMAINING_QTY = arSalesOrderLine.getSolQuantity() - arSalesOrderLine.getSolRequestQuantity();
                
                if(TOTAL_REMAINING_QTY == 0) 
                	continue;
                
                solDetails.setSolRemaining(TOTAL_REMAINING_QTY);
                solDetails.setSolQuantity(TOTAL_REMAINING_QTY);
                solDetails.setSolIssue(false);                        
                solDetails.setSolDiscount1(arSalesOrderLine.getSolDiscount1());
                solDetails.setSolDiscount2(arSalesOrderLine.getSolDiscount2());
                solDetails.setSolDiscount3(arSalesOrderLine.getSolDiscount3());
                solDetails.setSolDiscount4(arSalesOrderLine.getSolDiscount4()); 
                solDetails.setSolTotalDiscount(arSalesOrderLine.getSolTotalDiscount());
                solDetails.setSolAmount(arSalesOrderLine.getSolAmount());
                
                list.add(solDetails);
                
            }
            
            if (list == null || list.size() == 0) {
                
                throw new ArINVNoSalesOrderLinesFoundException();
                
            }
            
            ArModSalesOrderDetails mdetails = new ArModSalesOrderDetails();
            
            mdetails.setSoCode(arSalesOrder.getSoCode());
            mdetails.setSoTcName(arSalesOrder.getArTaxCode().getTcName());
            mdetails.setSoTcType(arSalesOrder.getArTaxCode().getTcType());
            mdetails.setSoTcRate(arSalesOrder.getArTaxCode().getTcRate());
            mdetails.setSoFcName(arSalesOrder.getGlFunctionalCurrency().getFcName());
            mdetails.setSoPytName(arSalesOrder.getAdPaymentTerm().getPytName());
            mdetails.setSoSlpSalespersonCode(arSalesOrder.getArSalesperson() != null ?
                    arSalesOrder.getArSalesperson().getSlpSalespersonCode() : null);
    		mdetails.setSoConversionDate(arSalesOrder.getSoConversionDate());
    		mdetails.setSoConversionRate(arSalesOrder.getSoConversionRate());
    		mdetails.setSoCstName(arSalesOrder.getArCustomer().getCstName());
    		mdetails.setSoDescription(arSalesOrder.getSoDescription());
    		mdetails.setSoDocumentNumber(arSalesOrder.getSoDocumentNumber());
            mdetails.setSoSolList(list);
            
            return mdetails;
            
        } catch (FinderException ex) {
            
            throw new GlobalNoRecordFoundException();
            
        } catch(GlobalNoRecordFoundException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch(ArINVNoSalesOrderLinesFoundException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }        
        
    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvBorEntry(com.util.InvBuildOrderDetails details,
		ArrayList bolList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalDocumentNumberNotUniqueException,
		GlobalRecordAlreadyDeletedException,
		GlobalTransactionAlreadyApprovedException,
		GlobalTransactionAlreadyPendingException,
		GlobalTransactionAlreadyPostedException,
		GlobalTransactionAlreadyVoidException,
		GlobalInvItemLocationNotFoundException,
		GlobalInventoryDateException {
    	
    	Debug.print("InvBuildOrderEntryControllerBean saveInvBorEntry");
    	
    	LocalInvBuildOrderHome invBuildOrderHome = null;
    	LocalInvBuildOrderLineHome invBuildOrderLineHome = null;
    	LocalInvBuildOrderStockHome invBuildOrderStockHome = null;
    	LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalAdApprovalHome adApprovalHome = null;
    	LocalAdApprovalUserHome adApprovalUserHome = null;
    	LocalAdApprovalQueueHome adApprovalQueueHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalInvItemHome invItemHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        
        // Initialize EJB Home
        
        try {
            
        	invBuildOrderHome = (LocalInvBuildOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildOrderHome.JNDI_NAME, LocalInvBuildOrderHome.class);
        	invBuildOrderLineHome = (LocalInvBuildOrderLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvBuildOrderLineHome.JNDI_NAME, LocalInvBuildOrderLineHome.class);
        	invBuildOrderStockHome = (LocalInvBuildOrderStockHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvBuildOrderStockHome.JNDI_NAME, LocalInvBuildOrderStockHome.class);
        	invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
        	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
        	adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
        	adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
        	adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
    			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
        	adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 
        	
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
    
		try {
			
			   LocalInvBuildOrder invBuildOrder = null;
			   
			   	//	validate if build order is already deleted
	        	
	        	try {
	        		
	        		if (details.getBorCode() != null) {
	        			
	        			invBuildOrder = invBuildOrderHome.findByPrimaryKey(details.getBorCode());
	        			
	        		}
	        		
	        	} catch (FinderException ex) {
	        		
	        		throw new GlobalRecordAlreadyDeletedException();
	        		
	        	}
	        	
	        	// validate if buid order is already approved, pending, posted or void
	        	
	        	if (details.getBorCode() != null) {
		        	
		        	
		        	if (invBuildOrder.getBorApprovalStatus() != null) {
	        		
		        		if (invBuildOrder.getBorApprovalStatus().equals("APPROVED") ||
		        			invBuildOrder.getBorApprovalStatus().equals("N/A")) {         		    	
		        		 
		        		    throw new GlobalTransactionAlreadyApprovedException(); 
		        		    	
		        		    	
		        		} else if (invBuildOrder.getBorApprovalStatus().equals("PENDING")) {
		        			
		        			throw new GlobalTransactionAlreadyPendingException();
		        			
		        		}
		        		
		        	}
	        			
	        		if (invBuildOrder.getBorPosted() == EJBCommon.TRUE) {
	        			
	        			throw new GlobalTransactionAlreadyPostedException();
	        			
	        		} else if (invBuildOrder.getBorVoid() == EJBCommon.TRUE) {
	        			
	        			throw new GlobalTransactionAlreadyVoidException();
	        			
	        		}
	        		
	        	}
	        	//	build order void
	        	
		    	if (details.getBorCode() != null && details.getBorVoid() == EJBCommon.TRUE &&
		    	    invBuildOrder.getBorPosted() == EJBCommon.FALSE) {
		    			    		        		
		    		invBuildOrder.setBorVoid(EJBCommon.TRUE);
		    		invBuildOrder.setBorLastModifiedBy(details.getBorLastModifiedBy());
		    		invBuildOrder.setBorDateLastModified(details.getBorDateLastModified());
	        		
	        		return invBuildOrder.getBorCode();
		    		
		    	}
	        	
		    	
		    	
	        	// validate if document number is unique document number is automatic then set next sequence
	        	
	    		LocalInvBuildOrder invExistingBuildOrder = null;
	    		
	    		try {
	    			
	    			invExistingBuildOrder = invBuildOrderHome.findByBorDocumentNumberAndBrCode(
	    					details.getBorDocumentNumber(), AD_BRNCH, AD_CMPNY);
	    			
	    		} catch (FinderException ex) {
	    			
	    		}

		    	if (details.getBorCode() == null) {

			        LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
			        LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;

		            if (invExistingBuildOrder != null) {
		            	
		            	throw new GlobalDocumentNumberNotUniqueException();
		            	
		            }
		    	
		        	try {
		        		
		        		adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV BUILD ORDER", AD_CMPNY);
		        		
		        	} catch (FinderException ex) {
		        		
		        	}
		        	
		        	try {
		        		
		        		adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
		        		
		        	} catch (FinderException ex) {
		        		
		        	}
			        	    
			        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
			        		(details.getBorDocumentNumber() == null || details.getBorDocumentNumber().trim().length() == 0)) {
			        	
			        	while (true) {
			        		
			        		if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
			        			
			        			try {
			        				
			        				invBuildOrderHome.findByBorDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);		            		
			        				adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
			        				
			        			} catch (FinderException ex) {
			        				
			        				details.setBorDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
			        				adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
			        				break;
			        				
			        			}
			        			
			        		} else {
			        			
			        			try {
			        				
			        				invBuildOrderHome.findByBorDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
			        				adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
			        				
			        			} catch (FinderException ex) {
			        				
			        				details.setBorDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
			        				adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
			        				break;
			        				
			        			}
			        			
			        		}
			        		
			        	}		            
			        	
			        }
			        
			    } else {

		        	if (invExistingBuildOrder != null && 
		                !invExistingBuildOrder.getBorCode().equals(details.getBorCode())) {
		            	
		            	throw new GlobalDocumentNumberNotUniqueException();
		            	
		            }
		            
		            if (invBuildOrder.getBorDocumentNumber() != details.getBorDocumentNumber() &&
		                (details.getBorDocumentNumber() == null || details.getBorDocumentNumber().trim().length() == 0)) {
		                	
		                details.setBorDocumentNumber(invBuildOrder.getBorDocumentNumber());
		                	
		         	}
			    	
			    }
			    
		    	
		    	
			   if (details.getBorCode() == null) {
			        
			   // create build order
			   	   
					invBuildOrder = invBuildOrderHome.create(details.getBorDate(), details.getBorDocumentNumber(), 
							details.getBorReferenceNumber(), details.getBorDescription(), EJBCommon.FALSE, details.getBorApprovalStatus(),
							EJBCommon.FALSE, details.getBorCreatedBy(), details.getBorDateCreated(), details.getBorLastModifiedBy(), 
							details.getBorDateLastModified(), null, null, null, null, null, null, AD_BRNCH, AD_CMPNY);
					
			   } else {
			   	
			   // update build order
			   				   	     
			   	     invBuildOrder.setBorDate(details.getBorDate());
			   	     invBuildOrder.setBorDocumentNumber(details.getBorDocumentNumber());
			   	     invBuildOrder.setBorReferenceNumber(details.getBorReferenceNumber());
			   	     invBuildOrder.setBorDescription(details.getBorDescription());
			   	     invBuildOrder.setBorVoid(details.getBorVoid());
			   	     invBuildOrder.setBorApprovalStatus(details.getBorApprovalStatus());
			   	     invBuildOrder.setBorLastModifiedBy(details.getBorLastModifiedBy());
			   	     invBuildOrder.setBorDateLastModified(details.getBorDateLastModified());
			   	  
			   	
			   }
			   
			   // delete build order lines
			   	 
			     
			    Collection invBuildOrderLines = invBuildOrder.getInvBuildOrderLines();
		        	
	        	Iterator i = invBuildOrderLines.iterator();
	        	
	        	while (i.hasNext()) {
	        		
	        		LocalInvBuildOrderLine invBuildOrderLine = (LocalInvBuildOrderLine)i.next();
	        		
	        		i.remove();
	        		
	        		invBuildOrderLine.remove();
	        		
	        	}
				
			   // create build order lines
			   
				i = bolList.iterator();
		
				while(i.hasNext()) {
		    
					InvModBuildOrderLineDetails mdetails = (InvModBuildOrderLineDetails)i.next();
															 	 	  	    					
				    LocalInvBuildOrderLine invBuildOrderLine = invBuildOrderLineHome.create(mdetails.getBolQuantityRequired(),
				    	0d, 0d, AD_CMPNY);
					
				    invBuildOrder.addInvBuildOrderLine(invBuildOrderLine);
				    
				    LocalInvItemLocation invItemLocation = null;
				    
				    try {
				    
				        invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getBolIlLocName(), mdetails.getBolIlIiName(), AD_CMPNY);
				    	
				    	invItemLocation.addInvBuildOrderLine(invBuildOrderLine);
				    	
				    } catch (FinderException ex) {
				    	
				    	throw new GlobalInvItemLocationNotFoundException();
				    	
				    }
					
				    // start date validation

	 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	 	  	    			invBuildOrder.getBorDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	 	  	    	if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
						
				}
				
				if (!isDraft) {
					
					invBuildOrder.setBorApprovalStatus("N/A");
					invBuildOrder.setBorPosted(EJBCommon.TRUE);
					invBuildOrder.setBorPostedBy(details.getBorLastModifiedBy());
					invBuildOrder.setBorDatePosted(details.getBorDateLastModified());
					
					// generate build order stocks
					
					invBuildOrderLines = invBuildOrder.getInvBuildOrderLines();
					
					i = invBuildOrderLines.iterator();
	        	
					while (i.hasNext()) {
	        		
						LocalInvBuildOrderLine invBuildOrderLine = (LocalInvBuildOrderLine)i.next();
	        		   
	        		 //  LocalInvItemLocation invItemLocation = null;
						 		 	  	    						
						Collection invBillofMaterials = invBuildOrderLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
							        		
						Iterator j = invBillofMaterials.iterator();
	        		
						while (j.hasNext()) {
		        		
							LocalInvBillOfMaterial invBillofMaterial = (LocalInvBillOfMaterial)j.next();
							
							LocalInvItem invItem = invItemHome.findByIiName(invBillofMaterial.getBomIiName(), AD_CMPNY);
		        		
							LocalInvBuildOrderStock invBuildOrderStock = invBuildOrderStockHome.create(invBillofMaterial.getBomQuantityNeeded() * invBuildOrderLine.getBolQuantityRequired(),
						    	0d, EJBCommon.FALSE, AD_CMPNY);
		        		
		        		    invItem.addInvBuildOrderStock(invBuildOrderStock);
							invBuildOrderLine.addInvBuildOrderStock(invBuildOrderStock);
										        	
						}
	        		
					}
					
					
				}
				
	            return invBuildOrder.getBorCode();
	            
		} catch (GlobalRecordAlreadyDeletedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (GlobalTransactionAlreadyApprovedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
		
		} catch (GlobalTransactionAlreadyPendingException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
		
		} catch (GlobalTransactionAlreadyPostedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (GlobalTransactionAlreadyVoidException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (GlobalDocumentNumberNotUniqueException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;	
			
		} catch (GlobalInvItemLocationNotFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;	
			
		} catch (GlobalInventoryDateException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;	
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}

    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvBorSolEntry(com.util.InvBuildOrderDetails details,
    		ArrayList solList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws 
    		GlobalRecordAlreadyDeletedException,
    		GlobalDocumentNumberNotUniqueException,
			GlobalTransactionAlreadyApprovedException,
			GlobalTransactionAlreadyPendingException,
			GlobalTransactionAlreadyPostedException,
			GlobalTransactionAlreadyVoidException,
			GlobalInvItemLocationNotFoundException,
			GlobalInventoryDateException,
		    GlobalTransactionAlreadyLockedException,
		    GlobalOverapplicationNotAllowedException{
        
    	
	    	Debug.print("InvBuildOrderEntryControllerBean saveInvBorSolEntry");
	        
	        LocalArSalesOrderHome arSalesOrderHome = null;
	        LocalArSalesOrderLineHome arSalesOrderLineHome = null;
	        LocalInvBuildOrderHome invBuildOrderHome = null;
	    	LocalInvBuildOrderLineHome invBuildOrderLineHome = null;
	    	LocalInvBuildOrderStockHome invBuildOrderStockHome = null;
	    	LocalInvItemLocationHome invItemLocationHome = null;
	    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
	    	LocalInvItemHome invItemHome = null;
	    	LocalInvCostingHome invCostingHome = null;
	    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
	        
	        try {
	            
	            arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
	            lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
	            arSalesOrderLineHome = (LocalArSalesOrderLineHome)EJBHomeFactory.
	            lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
	            invBuildOrderHome = (LocalInvBuildOrderHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvBuildOrderHome.JNDI_NAME, LocalInvBuildOrderHome.class);
	            invBuildOrderLineHome = (LocalInvBuildOrderLineHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvBuildOrderLineHome.JNDI_NAME, LocalInvBuildOrderLineHome.class);
	            invBuildOrderStockHome = (LocalInvBuildOrderStockHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvBuildOrderStockHome.JNDI_NAME, LocalInvBuildOrderStockHome.class);
	            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
	            lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
	            invItemHome = (LocalInvItemHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
	            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
	            lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
	            
	        } catch (NamingException ex) {
	        	
	        	throw new EJBException(ex.getMessage());
	        }
	        
	        try {
	        	
	        	LocalInvBuildOrder invBuildOrder = null;
	        	
	        	// validate if invoice is already deleted
	        	
	        	try {
	        		
	        		if (details.getBorCode() != null) {
                    
	        			invBuildOrder = invBuildOrderHome.findByPrimaryKey(details.getBorCode());
	        		}
                
	        	} catch (FinderException ex) {
                
	        		throw new GlobalRecordAlreadyDeletedException();
	        	}        	        	
            
	        	// validate if invoice is already posted, void, arproved or pending
            
	        	if (details.getBorCode() != null) {
                
	        		if (invBuildOrder.getBorApprovalStatus() != null) {
                    
	        			if (invBuildOrder.getBorApprovalStatus().equals("APPROVED") || 
	        					invBuildOrder.getBorApprovalStatus().equals("N/A")) {
	        				
	        				throw new GlobalTransactionAlreadyApprovedException(); 
                        
	        			} else if (invBuildOrder.getBorApprovalStatus().equals("PENDING")) {
                        
	        				throw new GlobalTransactionAlreadyPendingException();
	        			}
	        		}
                
	        		if (invBuildOrder.getBorPosted() == EJBCommon.TRUE) {
                    
	        			throw new GlobalTransactionAlreadyPostedException();
                    
	        		} else if (invBuildOrder.getBorVoid() == EJBCommon.TRUE) {
                    
	        			throw new GlobalTransactionAlreadyVoidException();
	        		}
	        	}
            
	        	// invoice void
            
	        	if (details.getBorCode() != null && details.getBorVoid() == EJBCommon.TRUE && 
	        			invBuildOrder.getBorPosted() == EJBCommon.FALSE) {
                
	            	invBuildOrder.setBorVoid(EJBCommon.TRUE);
	            	invBuildOrder.setBorLastModifiedBy(details.getBorLastModifiedBy());
	            	invBuildOrder.setBorDateLastModified(details.getBorDateLastModified());        		        		        		        		
	                
	                return invBuildOrder.getBorCode();
	        	}

	            // validate if document number is unique document number is automatic then set next sequence
	            
	            LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
	            LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
	            
	            if (details.getBorCode() == null) {
	                
	                try {
	                    
	                    adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV BUILD ORDER", AD_CMPNY);
	                    
	                } catch (FinderException ex) {
	                }
	                
	                try {
	                    
	                    adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
	                    
	                } catch (FinderException ex) {
	                }
	                
	                LocalInvBuildOrder invExistingBuildOrder = null;
	                
	                try {
	                	
	                	invExistingBuildOrder = invBuildOrderHome.findByBorDocumentNumberAndBrCode(
		    					details.getBorDocumentNumber(), AD_BRNCH, AD_CMPNY);
		    					
	                } catch (FinderException ex) { 
	                }
	                
	                if (invExistingBuildOrder != null) {
	                    
	                    throw new GlobalDocumentNumberNotUniqueException();
	                }
	                
	                if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
	                        (details.getBorDocumentNumber() == null || details.getBorDocumentNumber().trim().length() == 0)) {
	                    
	                    while (true) {
	                        
	                        if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
	                            
	                            try {
	                                
	                                invBuildOrderHome.findByBorDocumentNumberAndBrCode(
	                                		adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
	                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
	                                
	                            } catch (FinderException ex) {
	                                
	                                details.setBorDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
	                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
	                                break;
	                            }
	                            
	                        } else {
	                            
	                            try {
	                                
	                            	invBuildOrderHome.findByBorDocumentNumberAndBrCode(
	                            			adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
	                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
	                                
	                            } catch (FinderException ex) {
	                                
	                                details.setBorDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
	                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
	                                break;
	                            }
	                        }
	                    }		            
	                }
	                
	            } else {
	                
	                LocalInvBuildOrder invExistingBuildOrder = null;
	                
	                try {
	                    
	                	invExistingBuildOrder = invBuildOrderHome.findByBorDocumentNumberAndBrCode(
	                			details.getBorDocumentNumber(), AD_BRNCH, AD_CMPNY);
	                    
	                } catch (FinderException ex) { 
	                }
	                
	                if (invExistingBuildOrder != null && 
	                        !invExistingBuildOrder.getBorCode().equals(details.getBorCode())) {
	                    
	                    throw new GlobalDocumentNumberNotUniqueException();
	                }
	                
	                if (invBuildOrder.getBorDocumentNumber() != details.getBorDocumentNumber() &&
	                        (details.getBorDocumentNumber() == null || details.getBorDocumentNumber().trim().length() == 0)) {
	                    
	                    details.setBorDocumentNumber(invBuildOrder.getBorDocumentNumber());
	                }
	            }
	        	

	        	// lock sales order
            
	        	LocalArSalesOrder arExistingSalesOrder = null;	        	        
            
	        	try {
	        		
	        		arExistingSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndBrCode(
	        				details.getBorSoNumber(), AD_BRNCH, AD_CMPNY);        		    
                
	        		if ((invBuildOrder == null || (invBuildOrder != null && invBuildOrder.getBorSoNumber() != null 
	        				&& (!invBuildOrder.getBorSoNumber().equals(details.getBorSoNumber()))))) {
                    
	        			if (arExistingSalesOrder.getSoBoLock() == EJBCommon.TRUE) {
                        
	        				throw new GlobalTransactionAlreadyLockedException();
	        			}
                    
	        			arExistingSalesOrder.setSoBoLock(EJBCommon.TRUE);
	        		}
	        	} catch (FinderException ex) {
	        	}
            
	        	if (details.getBorCode() == null) {

		        	// create build order
	                invBuildOrder = invBuildOrderHome.create(details.getBorDate(), details.getBorDocumentNumber(), 
	                		details.getBorReferenceNumber(), details.getBorDescription(), EJBCommon.FALSE, 
	                		details.getBorApprovalStatus(), EJBCommon.FALSE, details.getBorCreatedBy(),
	                		details.getBorDateCreated(), details.getBorLastModifiedBy(), details.getBorDateLastModified(), 
	                		details.getBorApprovedRejectedBy(), details.getBorDateApprovedRejected(), details.getBorPostedBy(), 
	                		details.getBorDatePosted(), details.getBorReasonForRejection(), details.getBorSoNumber(), 
	                		AD_BRNCH, AD_CMPNY);
                		
	        	} else {
			   	
	        		// update build order
				   	invBuildOrder.setBorDate(details.getBorDate());
				   	invBuildOrder.setBorDocumentNumber(details.getBorDocumentNumber());
				   	invBuildOrder.setBorReferenceNumber(details.getBorReferenceNumber());
			   	    invBuildOrder.setBorDescription(details.getBorDescription());
			   	    invBuildOrder.setBorVoid(details.getBorVoid());
			   	    invBuildOrder.setBorApprovalStatus(details.getBorApprovalStatus());
			   	    invBuildOrder.setBorLastModifiedBy(details.getBorLastModifiedBy());
			   	    invBuildOrder.setBorDateLastModified(details.getBorDateLastModified());
	            }
	        	
	        	// delete build order lines
			    Collection invBuildOrderLines = invBuildOrder.getInvBuildOrderLines();
		        	
	        	Iterator i = invBuildOrderLines.iterator();
	        	
	        	while (i.hasNext()) {
	        		
	        		LocalInvBuildOrderLine invBuildOrderLine = (LocalInvBuildOrderLine)i.next();
	        		
	        		i.remove();
	        		
	        		invBuildOrderLine.remove();
	        	}

		        // create build order lines
				i = solList.iterator();
				
				while(i.hasNext()) {
		    
					InvModBuildOrderLineDetails mdetails = (InvModBuildOrderLineDetails)i.next();
															 	 	  	    					
				    LocalInvBuildOrderLine invBuildOrderLine = invBuildOrderLineHome.create(
				    		mdetails.getBolQuantityRequired(), 0d, 0d, AD_CMPNY);
				    		
				    invBuildOrder.addInvBuildOrderLine(invBuildOrderLine);
				    
				    LocalInvItemLocation invItemLocation = null;
				    
				    try {
				    
				        invItemLocation = invItemLocationHome.findByLocNameAndIiName(
				        		mdetails.getBolIlLocName(), mdetails.getBolIlIiName(), AD_CMPNY);
				    	
				    	invItemLocation.addInvBuildOrderLine(invBuildOrderLine);
				    	
				    } catch (FinderException ex) {
				    	
				    	throw new GlobalInvItemLocationNotFoundException();
				    }

					
				    //update sales order line rqst_qty
				    LocalArSalesOrder arSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndBrCode(
				    		details.getBorSoNumber(), AD_BRNCH, AD_CMPNY);
				    
				    Iterator solIter = arSalesOrder.getArSalesOrderLines().iterator();
				    
				    while(solIter.hasNext()) {
				    	
				    	LocalArSalesOrderLine salesOrderLine = (LocalArSalesOrderLine)solIter.next();
				    	
				    	//if(new Short(salesOrderLine.getSolLine()).toString().equals(mdetails.getBolLineNumber())) {
				    		
				    	if(salesOrderLine.getInvItemLocation().equals(invBuildOrderLine.getInvItemLocation())) {
				    		salesOrderLine.addInvBuildOrderLine(invBuildOrderLine);
				    		
				    		if(!isDraft) {
				    			
				    			double qty = salesOrderLine.getSolRequestQuantity() + invBuildOrderLine.getBolQuantityRequired();
					    		
				    			if(salesOrderLine.getSolQuantity() >= qty)
				    				salesOrderLine.setSolRequestQuantity(qty);
				    			else
				    				throw new GlobalOverapplicationNotAllowedException();
				    		}
						    break;
				    	}
				    }
				    
				    // start date validation
	 	  	    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	 	  	    			invBuildOrder.getBorDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	 	  	    	if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
						
				}
				
				
				if (!isDraft) {
					
					invBuildOrder.setBorApprovalStatus("N/A");
					invBuildOrder.setBorPosted(EJBCommon.TRUE);
					invBuildOrder.setBorPostedBy(details.getBorLastModifiedBy());
					invBuildOrder.setBorDatePosted(details.getBorDateLastModified());
					
					// generate build order stocks
					
					invBuildOrderLines = invBuildOrder.getInvBuildOrderLines();
					
					i = invBuildOrderLines.iterator();
	        	
					while (i.hasNext()) {
	        		
						LocalInvBuildOrderLine invBuildOrderLine = (LocalInvBuildOrderLine)i.next();
	        		   
						Collection invBillofMaterials = invBuildOrderLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
							        		
						Iterator j = invBillofMaterials.iterator();
	        		
						while (j.hasNext()) {
		        		
							LocalInvBillOfMaterial invBillofMaterial = (LocalInvBillOfMaterial)j.next();
							
							LocalInvItem invItem = invItemHome.findByIiName(invBillofMaterial.getBomIiName(), AD_CMPNY);
		        		
							LocalInvBuildOrderStock invBuildOrderStock = invBuildOrderStockHome.create(invBillofMaterial.getBomQuantityNeeded() * invBuildOrderLine.getBolQuantityRequired(),
						    	0d, EJBCommon.FALSE, AD_CMPNY);
		        		
		        		    invItem.addInvBuildOrderStock(invBuildOrderStock);
							invBuildOrderLine.addInvBuildOrderStock(invBuildOrderStock);
						}
					}
					
					//unlock SO build order
				    LocalArSalesOrder arSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndBrCode(
				    		invBuildOrder.getBorSoNumber(), AD_BRNCH, AD_CMPNY);
				    
				    arSalesOrder.setSoBoLock(EJBCommon.FALSE);

				}
				
	            return invBuildOrder.getBorCode();
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	            
	            getSessionContext().setRollbackOnly();
	            throw ex;
	            
	        } catch (GlobalDocumentNumberNotUniqueException ex) {
	            
	            getSessionContext().setRollbackOnly();
	            throw ex;
	            
	        } catch (GlobalTransactionAlreadyApprovedException ex) {
	            
	            getSessionContext().setRollbackOnly();
	            throw ex;
	            
	        } catch (GlobalTransactionAlreadyPendingException ex) {
	            
	            getSessionContext().setRollbackOnly();
	            throw ex;
	            
	        } catch (GlobalTransactionAlreadyPostedException ex) {
	            
	            getSessionContext().setRollbackOnly();
	            throw ex;
	            
	        } catch (GlobalTransactionAlreadyVoidException ex) {
	            
	            getSessionContext().setRollbackOnly();
	            throw ex;
	            
	        } catch (GlobalInvItemLocationNotFoundException ex) {
	            
	            getSessionContext().setRollbackOnly();
	            throw ex;
	            
	        } catch (GlobalTransactionAlreadyLockedException ex) {
	            
	            getSessionContext().setRollbackOnly();
	            throw ex;
	            
	        } catch (GlobalInventoryDateException ex) {
	            
	            getSessionContext().setRollbackOnly();
	            throw ex;
	            
	        } catch (GlobalOverapplicationNotAllowedException ex) {
	            
	            getSessionContext().setRollbackOnly();
	            throw ex;
	            
	        } catch (Exception ex) {
	            
	            Debug.printStackTrace(ex);
	            getSessionContext().setRollbackOnly();
	            throw new EJBException(ex.getMessage());
	        }
	    }

    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteInvBorEntry(Integer BOR_CODE, String AD_USR, Integer AD_BRNCH, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException {
                    
        Debug.print("InvBuildOrderEntryControllerBean deleteInvBorEntry");
        
        LocalInvBuildOrderHome invBuildOrderHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;
        LocalArSalesOrderHome arSalesOrderHome = null;

        // Initialize EJB Home
        
        try {
            
            invBuildOrderHome = (LocalInvBuildOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildOrderHome.JNDI_NAME, LocalInvBuildOrderHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);  
            arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);  

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvBuildOrder invBuildOrder = invBuildOrderHome.findByPrimaryKey(BOR_CODE);

        	adDeleteAuditTrailHome.create("INV BUILD ORDER", invBuildOrder.getBorDate(), invBuildOrder.getBorDocumentNumber(), invBuildOrder.getBorReferenceNumber(),
 				0d, AD_USR, new Date(), AD_CMPNY);
        	
        	//SO MATHCED UNLOCK SO
        	if(invBuildOrder.getBorSoNumber() != null) {
        		
        		LocalArSalesOrder arSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndBrCode(
        				invBuildOrder.getBorSoNumber(), AD_BRNCH, AD_CMPNY);
        		
        		arSalesOrder.setSoBoLock(EJBCommon.FALSE);
        	}
        	
        	invBuildOrder.remove();
        	
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/ 
    public ArrayList getArNotIssuedSolBySoNumber(Integer BOR_CODE, ArrayList issuedSolList, Integer AD_BRNCH, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
        
        Debug.print("InvBuildOrderEntryControllerBean getArNotIssuedSolBySoNumber");
        
        LocalArSalesOrderHome arSalesOrderHome = null;
        LocalInvBuildOrderHome invBuildOrderHome = null;

        try {
        	
        	invBuildOrderHome = (LocalInvBuildOrderHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildOrderHome.JNDI_NAME, LocalInvBuildOrderHome.class);
            arSalesOrderHome = (LocalArSalesOrderHome)EJBHomeFactory.
            lookUpLocalHome(LocalArSalesOrderHome.JNDI_NAME, LocalArSalesOrderHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {

        	LocalInvBuildOrder invBuildOrder = null;
            LocalArSalesOrder arSalesOrder = null;
            
	        try {
	               
	        	invBuildOrder = invBuildOrderHome.findByPrimaryKey(BOR_CODE);
	            
	        } catch (FinderException ex) {
	            
	            throw new GlobalNoRecordFoundException();
	            
	        }

	        try{
                
                arSalesOrder = arSalesOrderHome.findBySoDocumentNumberAndBrCode(invBuildOrder.getBorSoNumber(), AD_BRNCH, AD_CMPNY);
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
            }

            ArrayList solList = new ArrayList();
            
            Collection arSalesOrderLines = arSalesOrder.getArSalesOrderLines();
            
            Iterator i = arSalesOrderLines.iterator();
            
            while(i.hasNext()) {
                
                LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) i.next();
                
                boolean issued = false;
                
                Iterator j = issuedSolList.iterator();
                
                while(j.hasNext()) {
                    
                    Integer issuedSolCode = (Integer)j.next();
                    
                    if (arSalesOrderLine.getSolCode().equals(issuedSolCode)) {
                        
                        issued = true;
                        break;
                    }
                }

        		if(!issued) {
        			
	        		InvModBuildOrderLineDetails mdetails = new InvModBuildOrderLineDetails();
	        		
	        		mdetails.setBolCode(arSalesOrderLine.getSolCode());
	        		mdetails.setBolQuantityRequired(arSalesOrderLine.getSolQuantity() - 
	        				arSalesOrderLine.getSolRequestQuantity());
	        		mdetails.setBolIlIiUomName(arSalesOrderLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
	        		mdetails.setBolIlLocName(arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName());
	        		mdetails.setBolIlIiName(arSalesOrderLine.getInvItemLocation().getInvItem().getIiName());
	        		mdetails.setBolIlIiDescription(arSalesOrderLine.getInvItemLocation().getInvItem().getIiDescription());
	        		
	        		mdetails.setBolRemaining(arSalesOrderLine.getSolQuantity() - 
	        				arSalesOrderLine.getSolRequestQuantity());
	        		mdetails.setBolUnitCost(arSalesOrderLine.getSolUnitPrice());
	        		mdetails.setBolTotalCost(arSalesOrderLine.getSolAmount());
	        		mdetails.setBolLineNumber(new Short(arSalesOrderLine.getSolLine()).toString());
	        		mdetails.setBolIssue(false);
	        		
	        		
	        		solList.add(mdetails);
        		}
        	}
            
            return solList;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            Debug.printStackTrace(ex);
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("InvBuildOrderEntryControllerBean getInvGpQuantityPrecisionUnit");
       
        LocalAdPreferenceHome adPreferenceHome = null;         
        
         // Initialize EJB Home
          
         try {
              
         	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
        	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           
            return adPreference.getPrfInvQuantityPrecisionUnit();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }

     }
   
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getInvGpInventoryLineNumber(Integer AD_CMPNY) {

        Debug.print("InvBuildOrderEntryControllerBean getInvGpInventoryLineNumber");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfInvInventoryLineNumber();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

         Debug.print("InvBuildOrderEntryControllerBean getGlFcPrecisionUnit");

        
         LocalAdCompanyHome adCompanyHome = null;
              
         // Initialize EJB Home
          
         try {
              
             adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
              
         } catch (NamingException ex) {
              
             throw new EJBException(ex.getMessage());
              
         }

         try {
         	
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
              
           return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                       
         } catch (Exception ex) {
         	 
         	 Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
           
         }

      }
     
      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/
      public double getInvIiUnitCostByIiNameAndUomName(String II_NM, String UOM_NM, Integer AD_CMPNY) {
          
          Debug.print("InvBuildOrderEntryControllerBean getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate");
          
          LocalInvItemHome invItemHome = null;
          LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
          
          // Initialize EJB Home
          
          try {
              
              invItemHome = (LocalInvItemHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
              invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);  
              
          } catch (NamingException ex) {
              
              throw new EJBException(ex.getMessage());
              
          }
          
          try {
              
              LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
              
              LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
              LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
              
              return EJBCommon.roundIt(invItem.getIiUnitCost() * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
              
          } catch (Exception ex) {
              
              Debug.printStackTrace(ex);
              throw new EJBException(ex.getMessage());
              
          }
          
      }
     
}   
   

