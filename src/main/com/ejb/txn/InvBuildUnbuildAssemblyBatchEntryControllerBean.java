
/*
 * InvBuildUnbuildAssemblyBatchEntryControllerBean.java
 *
 * Created on May 20, 2004, 3:52 PM
 *
 * @author  Neil Andrew M. Ajero
 */
 
package com.ejb.txn;

import java.util.Collection;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.inv.LocalInvBuildUnbuildAssemblyBatch;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyBatchHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.inv.LocalInvBuildUnbuildAssembly;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyBatch;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyHome;
import com.util.AbstractSessionBean;
import com.util.InvBuildUnbuildAssemblyBatchDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;


/**
 * @ejb:bean name="InvBuildUnbuildAssemblyBatchEntryControllerEJB"
 *           display-name="used for entering invoice batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvBuildUnbuildAssemblyBatchEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvBuildUnbuildAssemblyBatchEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvBuildUnbuildAssemblyBatchEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvBuildUnbuildAssemblyBatchEntryControllerBean extends AbstractSessionBean {

    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.InvBuildUnbuildAssemblyBatchDetails getArBbByBbCode(Integer BB_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvBuildUnbuildAssemblyBatchEntryControllerBean getArBbByBbCode");
        
        LocalInvBuildUnbuildAssemblyBatchHome invBuildUnbuildAssemblyBatchHome = null;        
                
        // Initialize EJB Home
        
        try {
            
            invBuildUnbuildAssemblyBatchHome = (LocalInvBuildUnbuildAssemblyBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyBatchHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvBuildUnbuildAssemblyBatch invBuildUnbuildAssemblyBatch = null;
        	
        	
        	try {
        		
        		invBuildUnbuildAssemblyBatch = invBuildUnbuildAssemblyBatchHome.findByPrimaryKey(BB_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	InvBuildUnbuildAssemblyBatchDetails details = new InvBuildUnbuildAssemblyBatchDetails();
        	details.setBbCode(invBuildUnbuildAssemblyBatch.getBbCode());
        	details.setBbName(invBuildUnbuildAssemblyBatch.getBbName());
        	details.setBbDescription(invBuildUnbuildAssemblyBatch.getBbDescription());
        	details.setBbStatus(invBuildUnbuildAssemblyBatch.getBbStatus());
        	details.setBbType(invBuildUnbuildAssemblyBatch.getBbType());
        	details.setBbDateCreated(invBuildUnbuildAssemblyBatch.getBbDateCreated());
        	details.setBbCreatedBy(invBuildUnbuildAssemblyBatch.getBbCreatedBy());
        	
        	return details;
        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveArBbEntry(com.util.InvBuildUnbuildAssemblyBatchDetails details, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyExistException,
		/*GlobalTransactionBatchCloseException,
		GlobalRecordAlreadyAssignedException,*/
		GlobalRecordAlreadyDeletedException{
                    
        Debug.print("InvBuildUnbuildAssemblyBatchEntryControllerBean saveArBbEntry");
        
        LocalInvBuildUnbuildAssemblyBatchHome invBuildUnbuildAssemblyBatchHome = null;  
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;       
        
        LocalInvBuildUnbuildAssemblyBatch invBuildUnbuildAssemblyBatch = null;
        LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = null;
         
                
        // Initialize EJB Home
        
        try {
            
            invBuildUnbuildAssemblyBatchHome = (LocalInvBuildUnbuildAssemblyBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyBatchHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyBatchHome.class);     
            invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);       
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        } 
        
        try {
 
                		
			// validate if invoice batch is already deleted
			
			try {
				
				if (details.getBbCode() != null) {
					
					invBuildUnbuildAssemblyBatch = invBuildUnbuildAssemblyBatchHome.findByPrimaryKey(details.getBbCode());
					
				}
				
		 	} catch (FinderException ex) {
		 		
		 		throw new GlobalRecordAlreadyDeletedException();	 		
				
			} 
	        
	        // validate if invoice batch exists
	        		
			try {
				
			    LocalInvBuildUnbuildAssemblyBatch arExistingInvoiceBatch = invBuildUnbuildAssemblyBatchHome.findByBbName(details.getBbName(), AD_BRNCH, AD_CMPNY);
			
			    if (details.getBbCode() == null ||
			        details.getBbCode() != null && !arExistingInvoiceBatch.getBbCode().equals(details.getBbCode())) {
			    	
			        throw new GlobalRecordAlreadyExistException();
			        
			    }
			 
			} catch (GlobalRecordAlreadyExistException ex) {
				
				throw ex;
			
			} catch (FinderException ex) {
			    
			} 	
			
			/*// validate if invoice batch closing
			
			if (details.getBbStatus().equals("CLOSED")) {
				
				Collection arInvoices = invBuildUnbuildAssemblyHome.find.findByInvPostedAndInvVoidAndBbName(EJBCommon.FALSE, EJBCommon.FALSE, details.getBbName(), AD_CMPNY);
				
				Collection invBuildUnbuildAssemblies = 
				if (!arInvoices.isEmpty()) {
					
					throw new GlobalTransactionBatchCloseException();
					
				}
				
			}
			
			// validate if invoice already assigned
			
			if (details.getBbCode() != null) {
				
				Collection arInvoices = invBuildUnbuildAssemblyBatch.getArInvoices();
				
				if (!invBuildUnbuildAssemblyBatch.getBbType().equals(details.getBbType()) &&
				    !arInvoices.isEmpty()) {
				    	
				    throw new GlobalRecordAlreadyAssignedException();
				    	
				}
				
			}											
			*/
			
			if (details.getBbCode() == null) {
				
				invBuildUnbuildAssemblyBatch = invBuildUnbuildAssemblyBatchHome.create(details.getBbName(),
				   details.getBbDescription(), details.getBbStatus(), details.getBbType(),
				   details.getBbDateCreated(), details.getBbCreatedBy(), AD_BRNCH, AD_CMPNY);
				
			} else {
				
				invBuildUnbuildAssemblyBatch.setBbName(details.getBbName());
				invBuildUnbuildAssemblyBatch.setBbDescription(details.getBbDescription());
				invBuildUnbuildAssemblyBatch.setBbStatus(details.getBbStatus());
				invBuildUnbuildAssemblyBatch.setBbType(details.getBbType());
				
			}
			
			return invBuildUnbuildAssemblyBatch.getBbCode();				
		      	            	
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	 
        	getSessionContext().setRollbackOnly();      	  
      	    throw ex;
        
        } catch (GlobalRecordAlreadyExistException ex) {	 	
        
            getSessionContext().setRollbackOnly();      	  
      	    throw ex;
      	    
    /*  	} catch (GlobalTransactionBatchCloseException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;*/
                	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }     
            
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteArBbEntry(Integer BB_CODE, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException,
        GlobalRecordAlreadyAssignedException {
                    
        Debug.print("InvBuildUnbuildAssemblyBatchEntryControllerBean deleteArBbEntry");
        
        LocalInvBuildUnbuildAssemblyBatchHome invBuildUnbuildAssemblyBatchHome = null;  
        // Initialize EJB Home
        
        try {
            
            invBuildUnbuildAssemblyBatchHome = (LocalInvBuildUnbuildAssemblyBatchHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyBatchHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyBatchHome.class);     

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvBuildUnbuildAssemblyBatch invBuildUnbuildAssemblyBatch = invBuildUnbuildAssemblyBatchHome.findByPrimaryKey(BB_CODE);
        	
        	Collection invBuildUnbuildAssemblies = invBuildUnbuildAssemblyBatch.getInvBuildUnbuildAssemblies();
        	
        	if (!invBuildUnbuildAssemblies.isEmpty()) {
        		
        		throw new GlobalRecordAlreadyAssignedException();
        		
        	}
        	
        	invBuildUnbuildAssemblyBatch.remove();
        	
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw ex; 
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    
              
	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvBuildUnbuildAssemblyBatchEntryControllerBean ejbCreate");
      
    }
    
   // private methods

   

}