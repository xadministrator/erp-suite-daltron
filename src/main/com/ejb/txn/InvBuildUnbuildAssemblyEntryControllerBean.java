package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ApRINoPurchaseOrderLinesFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvCSTRemainingQuantityIsLessThanZeroException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInvItemNotAssemblyException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBuildUnbuildAssembly;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyBatch;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyBatchHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLine;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.ApModPurchaseOrderDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ArModCustomerDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModBuildUnbuildAssemblyDetails;
import com.util.InvModBuildUnbuildAssemblyLineDetails;
import com.util.InvModItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="InvBuildUnbuildAssemblyEntryControllerEJB"
 *           display-name="used for building and unbuilding assembly items"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvBuildUnbuildAssemblyEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvBuildUnbuildAssemblyEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvBuildUnbuildAssemblyEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
 */

public class InvBuildUnbuildAssemblyEntryControllerBean extends AbstractSessionBean {
    
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModItemDetails getInvDetailsByIiName(String II_NM, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvDetailsByIiName");
        
        LocalInvItemHome invItemHome = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
            
            LocalInvItem invItem = null;

            invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);        	

            
            InvModItemDetails details = new InvModItemDetails();
            
            details.setIiSpecificGravity(invItem.getIiSpecificGravity());
            
            return details;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvLocAll");
        
        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            invLocations = invLocationHome.findLocAll(AD_CMPNY);            
            
            if (invLocations.isEmpty()) {
                
                return null;
                
            }
            
            Iterator i = invLocations.iterator();
            
            while (i.hasNext()) {
                
                LocalInvLocation invLocation = (LocalInvLocation)i.next();	
                String details = invLocation.getLocName();
                
                list.add(details);
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getArCstAll");
        
        LocalArCustomerHome arCustomerHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);
            
            Iterator i = arCustomers.iterator();
            
            while (i.hasNext()) {
                
                LocalArCustomer arCustomer = (LocalArCustomer)i.next();

                list.add(arCustomer.getCstCustomerCode());
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModBuildUnbuildAssemblyDetails getInvBuaByBuaCode(Integer BUA_CODE, Integer AD_BRNCH, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvBuaByBuaCode");
        
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;        
        // Initialize EJB Home
        
        try {
            
            invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
            
            invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = null;
            
            try {
                
                invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(BUA_CODE);
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
                
            }
            
            ArrayList blList = new ArrayList();
            
            Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
            
            Iterator i = invBuildUnbuildAssemblyLines.iterator();
            
            while (i.hasNext()) {
                
                LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)i.next();
                
                InvModBuildUnbuildAssemblyLineDetails bdetails = new InvModBuildUnbuildAssemblyLineDetails();
                bdetails.setBlCode(invBuildUnbuildAssemblyLine.getBlCode());
                //mdetails.setBlLineNumber(invBuildUnbuildAssemblyLine.get);
                
                //plDetails.setPlCode(apPurchaseOrderLine.getPlCode());
    			//plDetails.setPlLine(apPurchaseOrderLine.getPlLine());
    			

    			if (invBuildUnbuildAssemblyLine.getBlBlCode() != null) {
    				
    				double PL_RCVD = 0d;
	    			double PL_RMNNG = 0d;

	    			Collection invBuildAssemblyReceivingLines = invBuildUnbuildAssemblyLineHome.findByBlBlCode(invBuildUnbuildAssemblyLine.getBlBlCode(), AD_CMPNY);
	    	    			
	    			Iterator j = invBuildAssemblyReceivingLines.iterator();
	    			
	    			while (j.hasNext()) {
	    
	    				LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyReceivingLine = (LocalInvBuildUnbuildAssemblyLine) j.next();
	    				
	    				System.out.println("DOC NUM="+invBuildUnbuildAssemblyReceivingLine.getInvBuildUnbuildAssembly().getBuaDocumentNumber());
	    				PL_RCVD += invBuildUnbuildAssemblyReceivingLine.getBlBuildQuantity();
	    				
	    				/*if (invBuildUnbuildAssemblyReceivingLine.getInvBuildUnbuildAssembly().getBuaPosted() == EJBCommon.TRUE) {
	    					
	    					PL_RCVD += invBuildUnbuildAssemblyReceivingLine.getBlBuildQuantity();
	    				
	    				}*/
	    			
	    			}	    			
	    			
	    			try {

	    				LocalInvBuildUnbuildAssemblyLine invBlLine = invBuildUnbuildAssemblyLineHome.findByPrimaryKey(invBuildUnbuildAssemblyLine.getBlBlCode());
	    				
	    				PL_RMNNG = invBlLine.getBlBuildQuantity() - PL_RCVD;
	    				
	    				bdetails.setBlRemaining(PL_RMNNG < 0 ? 0 : PL_RMNNG);
		    			
	    				
	    			} catch (FinderException ex) {
	    				
	    			}	 
	    			
	    			
	    		}
    			
    			
    			bdetails.setBlBlCode(invBuildUnbuildAssemblyLine.getBlBlCode());
    			bdetails.setBlBuildSpecificGravity(invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity());
    			bdetails.setBlBuildStandardFillSize(invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize());
    			bdetails.setBlBuildQuantity(invBuildUnbuildAssemblyLine.getBlBuildQuantity());
    			bdetails.setBlUomName(invBuildUnbuildAssemblyLine.getInvUnitOfMeasure().getUomName());
    			bdetails.setBlLocName(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName());
    			bdetails.setBlIiName(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName());
    			bdetails.setBlIiDescription(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiDescription());
    			bdetails.setBlCstQuantity(this.getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(bdetails.getBlIiName(), bdetails.getBlLocName(), bdetails.getBlUomName(), AD_BRNCH, AD_CMPNY));
    			bdetails.setBlMisc(invBuildUnbuildAssemblyLine.getBlMisc());
                blList.add(bdetails);
                
            }
            
            InvModBuildUnbuildAssemblyDetails details = new InvModBuildUnbuildAssemblyDetails();
            
            details.setBuaCode(invBuildUnbuildAssembly.getBuaCode());
            details.setBuaType(invBuildUnbuildAssembly.getBuaType());
            details.setBuaBbName(invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyBatch() != null ? invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyBatch().getBbName() : null);
            details.setBuaDocumentNumber(invBuildUnbuildAssembly.getBuaDocumentNumber());
            details.setBuaReferenceNumber(invBuildUnbuildAssembly.getBuaReferenceNumber());
            details.setBuaReceiveBoNumber(invBuildUnbuildAssembly.getBuaReceiveBoNumber());
            details.setBuaDescription(invBuildUnbuildAssembly.getBuaDescription());
            details.setBuaVoid(invBuildUnbuildAssembly.getBuaVoid());
            details.setBuaDate(invBuildUnbuildAssembly.getBuaDate());
            details.setBuaDueDate(invBuildUnbuildAssembly.getBuaDueDate());
            details.setBuaApprovalStatus(invBuildUnbuildAssembly.getBuaApprovalStatus());
            details.setBuaPosted(invBuildUnbuildAssembly.getBuaPosted());
            details.setBuaCreatedBy(invBuildUnbuildAssembly.getBuaCreatedBy());
            details.setBuaDateCreated(invBuildUnbuildAssembly.getBuaDateCreated());
            details.setBuaLastModifiedBy(invBuildUnbuildAssembly.getBuaLastModifiedBy());
            details.setBuaDateLastModified(invBuildUnbuildAssembly.getBuaDateLastModified());
            details.setBuaApprovedRejectedBy(invBuildUnbuildAssembly.getBuaApprovedRejectedBy());
            details.setBuaDateApprovedRejected(invBuildUnbuildAssembly.getBuaDateApprovedRejected());
            details.setBuaPostedBy(invBuildUnbuildAssembly.getBuaPostedBy());
            details.setBuaDatePosted(invBuildUnbuildAssembly.getBuaDatePosted());
            details.setBuaReasonForRejection(invBuildUnbuildAssembly.getBuaReasonForRejection());
            details.setBuaCstCustomerCode(invBuildUnbuildAssembly.getArCustomer().getCstCustomerCode());
            details.setBuaCstName(invBuildUnbuildAssembly.getArCustomer().getCstName());
  

            details.setBuaBlList(blList);
            
            return details;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            Debug.printStackTrace(ex);
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvUomByIiName");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemHome invItemHome = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
            
            LocalInvItem invItem = null;
            LocalInvUnitOfMeasure invItemUnitOfMeasure = null;
            
            invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);        	
            invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();
            
            //Collection invUnitOfMeasures = null;
            Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
                    invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
            while (i.hasNext()) {
                
                LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
                InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
                details.setUomName(invUnitOfMeasure.getUomName());
                
                if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {
                    
                    details.setDefault(true);
                    
                }
                
                list.add(details);
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfEnableInvBuildUnbuildAssemblyBatch(Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getAdPrfEnableInvBuildUnbuildAssemblyBatch");
        
        LocalAdPreferenceHome adPreferenceHome = null;
        
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            return adPreference.getPrfEnableInvBUABatch();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvOpenBbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvOpenBbAll");
        
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;
        LocalInvBuildUnbuildAssemblyBatchHome invBuildUnbuildAssemblyBatchHome = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invBuildUnbuildAssemblyBatchHome = (LocalInvBuildUnbuildAssemblyBatchHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildUnbuildAssemblyBatchHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {

            Collection invBuildUnbuildAssemblyBatches = invBuildUnbuildAssemblyBatchHome.findOpenBbByBbType("BUA", AD_BRNCH, AD_CMPNY);
            
            Iterator i = invBuildUnbuildAssemblyBatches.iterator();
            
            while (i.hasNext()) {
                
            	 
            	 LocalInvBuildUnbuildAssemblyBatch invBuildUnbuildAssemblyBatch = (LocalInvBuildUnbuildAssemblyBatch)i.next();
                
                list.add(invBuildUnbuildAssemblyBatch.getBbName());
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvBuaEntry(com.util.InvBuildUnbuildAssemblyDetails details,
            ArrayList blList, boolean isDraft, String CST_CSTMR_CODE, String BB_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
            GlobalRecordAlreadyDeletedException,
            GlobalAccountNumberInvalidException,
            GlobalTransactionAlreadyApprovedException,
            GlobalTransactionAlreadyPendingException,
            GlobalTransactionAlreadyPostedException,
            GlobalNoApprovalRequesterFoundException,
            GlobalNoApprovalApproverFoundException,
            GlobalInvItemLocationNotFoundException, 
            GlobalInvItemNotAssemblyException,
            GlobalInvCSTRemainingQuantityIsLessThanZeroException,
            GlJREffectiveDateNoPeriodExistException,
            GlJREffectiveDatePeriodClosedException,
            GlobalJournalNotBalanceException,
            GlobalDocumentNumberNotUniqueException, 
            GlobalInventoryDateException,
            GlobalBranchAccountNumberInvalidException,
    		AdPRFCoaGlVarianceAccountNotFoundException,
    		GlobalRecordInvalidException,
    		GlobalMiscInfoIsRequiredException{
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean saveInvBuaEntry");
        
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalInvBuildUnbuildAssemblyBatchHome invBuildUnbuildAssemblyBatchHome = null;
        LocalInvTagHome invTagHome = null;
        
        // Initialize EJB Home
        Date txnStartDate = new Date();
        try {
            
            invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                    lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            invBuildUnbuildAssemblyBatchHome = (LocalInvBuildUnbuildAssemblyBatchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvBuildUnbuildAssemblyBatchHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyBatchHome.class);
            
            invTagHome = (LocalInvTagHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class); 
                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
            
            LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = null;
            
            // validate if BuildUnbuildAssembly is already deleted
            
            try {
                
                if (details.getBuaCode() != null) {
                    
                    invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(details.getBuaCode());
                    
                }
                
            } catch (FinderException ex) {
                
                throw new GlobalRecordAlreadyDeletedException();
                
            }
            
            // validate if build unbuild assembly is already posted, void, approved or pending
            
            if (details.getBuaCode() != null) {
                
                
                if (invBuildUnbuildAssembly.getBuaApprovalStatus() != null) {
                    
                    if (invBuildUnbuildAssembly.getBuaApprovalStatus().equals("APPROVED") ||
                            invBuildUnbuildAssembly.getBuaApprovalStatus().equals("N/A")) {         		    	
                        
                        throw new GlobalTransactionAlreadyApprovedException(); 
                        
                        
                    } else if (invBuildUnbuildAssembly.getBuaApprovalStatus().equals("PENDING")) {
                        
                        throw new GlobalTransactionAlreadyPendingException();
                        
                    }
                    
                }
                
                if (invBuildUnbuildAssembly.getBuaPosted() == EJBCommon.TRUE) {
                    
                    throw new GlobalTransactionAlreadyPostedException();
                    
                }
                
            }
            
            // validate if document number is unique document number is automatic then set next sequence
          
            LocalInvBuildUnbuildAssembly invExistingBuildUnbuildAssemblyReceiving = null;
            
            try {
                
            	invExistingBuildUnbuildAssemblyReceiving = invBuildUnbuildAssemblyHome.findByBuaDocumentNumberAndBuaReceivingBrCode(details.getBuaDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
                
            } catch (FinderException ex) {
                
            }
            
            if (details.getBuaCode() == null) {
                
                LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
                LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
                
                if (invExistingBuildUnbuildAssemblyReceiving != null) {
                    
                    throw new GlobalDocumentNumberNotUniqueException();
                    
                }
                
                try {
                    
                    adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV BUILD ASSEMBLY", AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                try {
                    
                    adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
                        (details.getBuaDocumentNumber() == null || details.getBuaDocumentNumber().trim().length() == 0)) {
                    
                    while (true) {
                        
                        if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {		            	
                            
                            try {
                                
                                invBuildUnbuildAssemblyHome.findByBuaDocumentNumberAndBuaReceivingBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);		            		
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
                                
                            } catch (FinderException ex) {
                                
                                details.setBuaDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
                                break;
                                
                            }
                            
                        } else {
                            
                            try {
                                
                                invBuildUnbuildAssemblyHome.findByBuaDocumentNumberAndBuaReceivingBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.TRUE,  AD_BRNCH, AD_CMPNY);		            		
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
                                
                            } catch (FinderException ex) {
                                
                                details.setBuaDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
                                break;
                                
                            }
                            
                        }
                        
                    }		            
                    
                }
                
            } else {
                
                if (invExistingBuildUnbuildAssemblyReceiving != null && 
                        !invExistingBuildUnbuildAssemblyReceiving.getBuaCode().equals(details.getBuaCode())) {
                    
                    throw new GlobalDocumentNumberNotUniqueException();
                    
                }
                
                if (invBuildUnbuildAssembly.getBuaDocumentNumber() != details.getBuaDocumentNumber() &&
                        (details.getBuaDocumentNumber() == null || details.getBuaDocumentNumber().trim().length() == 0)) {
                    
                    details.setBuaDocumentNumber(invBuildUnbuildAssembly.getBuaDocumentNumber());
                    
                }
                
            }
            
            // lock purchase order
	        
	        LocalInvBuildUnbuildAssembly invExistingBuildUnbuildAssemblyOrder = null;
	        Debug.print("ApReceivingItemEntryControllerBean saveApPoEntry C");
	        if (details.getBuaType().equals("BO MATCHED")) {
	        	
	        	try {
	        		invExistingBuildUnbuildAssemblyOrder = invBuildUnbuildAssemblyHome.findByBuaDocumentNumberAndBuaReceivingBrCode(
	        				details.getBuaReceiveBoNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
	        				
	    		    
		        	if (invBuildUnbuildAssembly == null || (invBuildUnbuildAssembly != null 
		        			&& !invBuildUnbuildAssembly.getBuaReceiveBoNumber().equals(details.getBuaReceiveBoNumber()))) {
			        				        				        				        				    		  
			    		    if (invExistingBuildUnbuildAssemblyOrder.getBuaLock() == EJBCommon.TRUE) {
			    		    	
			    		    	throw new GlobalTransactionAlreadyLockedException();
			    		    	
			    		    }
			    		    
			    		    invExistingBuildUnbuildAssemblyOrder.setBuaLock(EJBCommon.TRUE);
			    		    
			    		    if (invBuildUnbuildAssembly != null && invBuildUnbuildAssembly.getBuaReceiveBoNumber() != null){
			    		    
			    		    	LocalInvBuildUnbuildAssembly invPreviousBO = null;
			    		    	
			    		    	invPreviousBO = invBuildUnbuildAssemblyHome.findByBuaDocumentNumberAndBuaReceivingBrCode(
			    		    			invBuildUnbuildAssembly.getBuaReceiveBoNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
			    		    	
			    		    	invPreviousBO.setBuaLock(EJBCommon.FALSE);
		
			    		    
			    		    }
			    		    
			    		    if (invExistingBuildUnbuildAssemblyOrder.getBuaVoid() == EJBCommon.TRUE) {
			        			
			        			throw new GlobalTransactionAlreadyVoidPostedException();
			        			
			        		}
				    		    		        		        	
		        	}
	        	
	        	} catch (FinderException ex) {
	        		
	        	}
	        	
	        }
            
            // used in checking if invoice should re-generate distribution records and re-calculate taxes	        	                	
            boolean isRecalculate = true;
            
            // create build unbuild assembly
            
           
            if (details.getBuaCode() == null) {
                
                invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.create(EJBCommon.TRUE, details.getBuaType(), details.getBuaDocumentNumber(), details.getBuaReferenceNumber(), details.getBuaReceiveBoNumber(),  EJBCommon.FALSE,
                        details.getBuaDate(), details.getBuaDueDate(), details.getBuaDescription(), details.getBuaVoid(), details.getBuaApprovalStatus(),
                        EJBCommon.FALSE, details.getBuaCreatedBy(), details.getBuaDateCreated(),
                        details.getBuaLastModifiedBy(), details.getBuaDateLastModified(),
                        null, null, null, null, null, AD_BRNCH, AD_CMPNY);
                
            } else {
                
                // check if critical fields are changed
                
                if (blList.size() != invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines().size() || 
                        !(invBuildUnbuildAssembly.getBuaDate().equals(details.getBuaDate()))) {
                    
                    isRecalculate = true;
                    
                } else if (blList.size() == invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines().size()) {
                    
                    Iterator blIter = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines().iterator();
                    Iterator blListIter = blList.iterator();
                    
                    while (blIter.hasNext()) {
                        
                        LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)blIter.next();
                        InvModBuildUnbuildAssemblyLineDetails mdetails = (InvModBuildUnbuildAssemblyLineDetails)blListIter.next();
                        if (!invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getBlIiName()) ||
                                !invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getBlLocName()) ||
                                !invBuildUnbuildAssemblyLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getBlUomName()) ||
                                invBuildUnbuildAssemblyLine.getBlBuildQuantity() != mdetails.getBlBuildQuantity() ||
                                		invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize() != mdetails.getBlBuildStandardFillSize() ||
                                invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity() != mdetails.getBlBuildSpecificGravity()
                        		) {
                            
                            isRecalculate = true;
                            break;
                            
                        }
                        
                        isRecalculate = false;
                        
                    }
                    
                } else {
                    
                    isRecalculate = false;
                    
                }
                
                invBuildUnbuildAssembly.setBuaReceiving(EJBCommon.TRUE);
                invBuildUnbuildAssembly.setBuaType(details.getBuaType());
                invBuildUnbuildAssembly.setBuaDocumentNumber(details.getBuaDocumentNumber());
                invBuildUnbuildAssembly.setBuaReferenceNumber(details.getBuaReferenceNumber());
                invBuildUnbuildAssembly.setBuaReceiveBoNumber(details.getBuaReceiveBoNumber());
                invBuildUnbuildAssembly.setBuaDate(details.getBuaDate());
                invBuildUnbuildAssembly.setBuaDueDate(details.getBuaDueDate());
                invBuildUnbuildAssembly.setBuaDescription(details.getBuaDescription());
                invBuildUnbuildAssembly.setBuaVoid(details.getBuaVoid());
                invBuildUnbuildAssembly.setBuaApprovalStatus(details.getBuaApprovalStatus());
                invBuildUnbuildAssembly.setBuaLastModifiedBy(details.getBuaLastModifiedBy());
                invBuildUnbuildAssembly.setBuaDateLastModified(details.getBuaDateLastModified());
                invBuildUnbuildAssembly.setBuaReasonForRejection(null);
                
            }
            
            LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
            //arCustomer.addArInvoice(arInvoice);
            invBuildUnbuildAssembly.setArCustomer(arCustomer);
            
            try {
                
            	LocalInvBuildUnbuildAssemblyBatch invBuildUnbuildAssemblyBatch = invBuildUnbuildAssemblyBatchHome.findByBbName(BB_NM, AD_BRNCH, AD_CMPNY);
                //arInvoiceBatch.addArInvoice(arInvoice);
            	invBuildUnbuildAssembly.setInvBuildUnbuildAssemblyBatch(invBuildUnbuildAssemblyBatch);
                
            } catch (FinderException ex) {
                
            }
            
            
            double ABS_TOTAL_AMOUNT = 0d;
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            if (isRecalculate) {		
                
                // remove all distribution records
                
                Collection invDistributionRecords = invBuildUnbuildAssembly.getInvDistributionRecords();
                
                Iterator i = invDistributionRecords.iterator();     	  
                
                while (i.hasNext()) {
                    
                    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
                    
                    i.remove();
                    
                    invDistributionRecord.remove();
                    
                }
                
                // remove all build unbuild assembly lines
                
                Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
                
                i = invBuildUnbuildAssemblyLines.iterator();
                
                while (i.hasNext()) {
                    
                    LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)i.next();
                
                    // build qty conversion
                    
                   
                    double buildQuantity = this.convertByUomFromAndItemAndQuantity(
                    		invBuildUnbuildAssemblyLine.getInvUnitOfMeasure(), 
                    		invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem(),
                    		Math.abs(invBuildUnbuildAssemblyLine.getBlBuildQuantity()), AD_CMPNY);
                    
                    
                    
                    if (invBuildUnbuildAssemblyLine.getBlBuildQuantity() < 0) {
                        
                        invBuildUnbuildAssemblyLine.getInvItemLocation().setIlCommittedQuantity(invBuildUnbuildAssemblyLine.getInvItemLocation().getIlCommittedQuantity() - buildQuantity);
                        
                    } else {
                        
                        Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                        
                        Iterator j = invBillOfMaterials.iterator();
                        
                        while (j.hasNext()) {
                            
                            LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
                            
                            LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
                            
                            double specificGravity = invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity();
                            double standardFillSize = invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize();
                            
                            // bom conversion
                            double convertedQuantity = 
                            		invItemLocation.getInvItem().getIiAdLvCategory().contains("RAWMAT") && !invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiAdLvCategory().contains("Bulk") ?
                                            buildQuantity * (invBillOfMaterial.getBomQuantityNeeded()/ 100) * (standardFillSize/1000) * specificGravity
                                    :
                                    	this.convertByUomFromAndItemAndQuantity(
                                                invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
                                                EJBCommon.roundIt(buildQuantity * (invBillOfMaterial.getBomQuantityNeeded() / 100),
                                                this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY)
                                    
                                    ;
                            
                            
                            
                            invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity );
                            
                        }
                        
                        Collection invBillOfMaterialsTags = invBuildUnbuildAssemblyLine.getInvTags();
                        
                        Iterator k = invBillOfMaterialsTags.iterator();
                        
                        while(k.hasNext()){
                        	
                        	LocalInvTag invTag = (LocalInvTag)k.next();
                        	
                        	
                        	k.remove();
                        	invTag.remove();
                        	
                        	
                        }
                        
                        
                        
                        
                    }
                    
                    i.remove();
                    
                    invBuildUnbuildAssemblyLine.remove();
                    
                }				  
                
                // add build/unbuild assembly lines and distribution record
                
                byte DEBIT = 0;
                
                i = blList.iterator();
                
                while (i.hasNext()) {
                    
                    InvModBuildUnbuildAssemblyLineDetails  mdetails = (InvModBuildUnbuildAssemblyLineDetails) i.next();
                    
                    
                    LocalInvItemLocation invItemLocation = null;
                    
                    try {
                        
                        invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getBlLocName(), mdetails.getBlIiName(), AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                        throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getBlLineNumber()));
                        
                    }
                    
                    try {
                        
                        LocalInvItem invItem = invItemHome.findByIiName(mdetails.getBlIiName(), AD_CMPNY);
                        
                        if (invItem.getIiClass().equals("Stock")) {
                            
                            throw new GlobalInvItemNotAssemblyException(String.valueOf(mdetails.getBlLineNumber()));
                            
                        }
                        
                    } catch (FinderException ex) {
                        
                    }
                    
                    //	start date validation
                    if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    			invBuildUnbuildAssembly.getBuaDate(), invItemLocation.getInvItem().getIiName(),
                    			invItemLocation.getInvLocation().getLocName(), AD_BRNCH,	AD_CMPNY);
                    	Debug.print("InvBuildUnbuildAssemblyEntryControllerBean saveInvBuaEntry C03");
                    	if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
                    }
                    LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = this.addInvBlEntry(mdetails, invBuildUnbuildAssembly, AD_CMPNY);
                    
                    // check for branch mapping
                    
                    LocalAdBranchItemLocation adBranchItemLocation = null;
                    
                    try{
                        
                        adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                invBuildUnbuildAssemblyLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                    }
                    
                    LocalGlChartOfAccount glInventoryChartOfAccount = null;
                    
                    if (adBranchItemLocation == null) {
                        
                        glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                                invBuildUnbuildAssemblyLine.getInvItemLocation().getIlGlCoaInventoryAccount());
                        
                    } else {
                        
                        glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                                adBranchItemLocation.getBilCoaGlInventoryAccount());
                        
                    }
                    
                    double TOTAL_AMOUNT = 0d;
                    
                    
                    // build qty conversion
                    double buildQuantity = this.convertByUomFromAndItemAndQuantity(
                    		invBuildUnbuildAssemblyLine.getInvUnitOfMeasure(), 
                    		invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem(),
                    		Math.abs(invBuildUnbuildAssemblyLine.getBlBuildQuantity()   ), AD_CMPNY);
                    
                    if (buildQuantity > 0) {
                        System.out.println("buildQuantity > 0---------------------->");
                        // build assembly
                        
                        DEBIT = EJBCommon.TRUE;
                        
                        Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                        
                        
                        Iterator j = invBillOfMaterials.iterator();
                        
                        while (j.hasNext()) {
                            
                            // bill of materials
                            
                            LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
                            
                            // get raw material
                            LocalInvItemLocation invIlRawMaterial = null;
                            
                            try {
                                
                                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                                        invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(),AD_CMPNY);
                               
                            } catch (FinderException ex) {
                                
                                throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getBlLineNumber()) +
                                        " - Raw Mat. (" + invBillOfMaterial.getBomIiName() + ")");
                                
                            }
                            // create tags for Raw Materials
                            //LocalInvTag invTag = invTagHome.create(null, null, null, null, null, AD_CMPNY, invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDate(), "OUT");
                            //invTag.setInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
                            //invTag.setInvItemLocation(invIlRawMaterial);
                            
                            double specificGravity = invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity();
                            double standardFillSize = invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize();
                            
                            System.out.println(invBillOfMaterial.getBomIiName()+" QNN = "+invBillOfMaterial.getBomQuantityNeeded());
                            // bom conversion
                            double convertedQuantity = 
                            		invIlRawMaterial.getInvItem().getIiAdLvCategory().contains("RAWMAT") && !invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiAdLvCategory().contains("Bulk") ? 
                                    buildQuantity * (invBillOfMaterial.getBomQuantityNeeded()/ 100) * (standardFillSize/1000) * specificGravity
                                    :
                                    	this.convertByUomFromAndItemAndQuantity(
                                                invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                                                EJBCommon.roundIt( buildQuantity * (invBillOfMaterial.getBomQuantityNeeded() / 100),
                                                this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY)
                                    
                                    ;
                                    
                            
                            
                            // start date validation
                                    
                            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                            	Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                            			invBuildUnbuildAssembly.getBuaDate(), invIlRawMaterial.getInvItem().getIiName(),
                            			invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH,	AD_CMPNY);
                            	
                            	if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
                            			invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
                            }
                            // add bill of material quantity needed to item location committed quantity
                            invIlRawMaterial.setIlCommittedQuantity(invIlRawMaterial.getIlCommittedQuantity() + convertedQuantity );
                            
                            // check for branch mapping
                            
                            LocalAdBranchItemLocation adBranchIlRawMaterial = null;
                            
                            try{
                                
                                adBranchIlRawMaterial = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                        invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);
                                
                            } catch (FinderException ex) {
                                
                            }
                            
                            LocalGlChartOfAccount glCoaRawMaterial = null;
                           
                            
                            if (adBranchIlRawMaterial == null) {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        invIlRawMaterial.getIlGlCoaInventoryAccount());
                                
                             
                            } else {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        adBranchIlRawMaterial.getBilCoaGlInventoryAccount());
                              
                                
                            }
                            
                            LocalInvItem invItemRaw = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);												
                            
                            double COST = 0d;

                            try {
                                
                                LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                        invBuildUnbuildAssembly.getBuaDate(), invItemRaw.getIiName(),
                                        invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);
                                
                                COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                                
                            } catch (FinderException ex) {	 
                                
                                COST = invItemRaw.getIiUnitCost();	  	        	 	  	        	
                                
                                
                            }
                            
                             
                            double BOM_AMOUNT = EJBCommon.roundIt(convertedQuantity * COST, this.getGlFcPrecisionUnit(AD_CMPNY));
                            

                            TOTAL_AMOUNT += BOM_AMOUNT;
                            ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);
                            
                            this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(BOM_AMOUNT),
                                    glCoaRawMaterial.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
                            
                            //System.out.println(invIlRawMaterial.getInvItem().getIiName()+" - "+invIlRawMaterial.getInvItem().getIiAdLvCategory() + " - " + convertedQuantity);
                            
                            //System.out.println("BOM_AMOUNT-------->"+BOM_AMOUNT);
                            
                            
                        }
                        
                    } else {

                        // unbuild assembly
                        
                        try {	 	  	    		
                            
                            LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(
                                    invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName(), 
                                    invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName(), 
                                    AD_BRNCH, AD_CMPNY);
                            if ((invCosting.getCstRemainingQuantity() - Math.abs(buildQuantity)) < 0) {
                                    
                                throw new GlobalInvCSTRemainingQuantityIsLessThanZeroException(String.valueOf(mdetails.getBlLineNumber()));	 	  	    		    
                                
                            }
                            
                        } catch (FinderException ex) {
                            
                        }
                        
                        invItemLocation = invBuildUnbuildAssemblyLine.getInvItemLocation();
                        
                        // add build quantity to item location committed quantity
                        invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + Math.abs(buildQuantity));
                        
                        DEBIT = EJBCommon.FALSE;
                        
                        Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                        
                        Iterator j = invBillOfMaterials.iterator();
                        
                        while (j.hasNext()) {
                            
                            // bill of materials
                            
                            LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
                            
                            // get raw material
                            
                            LocalInvItemLocation invIlRawMaterial = null;
                            
                            try {
                                
                                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                                        invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
                                
                            } catch(FinderException ex) {
                                
                                throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getBlLineNumber()) +
                                        " - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");
                                
                            }
                            
                            // create tags for Raw Materials
                            LocalInvTag invTag = invTagHome.create(null, null, null, null, null, AD_CMPNY, invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDate(), "OUT");
                            invTag.setInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
                            invTag.setInvItemLocation(invIlRawMaterial);
                            
                            
                            double specificGravity = invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity();
                            double standardFillSize = invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize();
                            
                            
                            // bom conversion
                            double convertedQuantity = 
                            		invIlRawMaterial.getInvItem().getIiAdLvCategory().contains("RAWMAT") && !invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiAdLvCategory().contains("Bulk") ? 
                                            buildQuantity * (invBillOfMaterial.getBomQuantityNeeded()/ 100) * (standardFillSize/1000) * specificGravity
                                            :
                                    	this.convertByUomFromAndItemAndQuantity(
                                                invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                                                EJBCommon.roundIt( buildQuantity * (invBillOfMaterial.getBomQuantityNeeded() / 100),
                                                this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);
                             
                            
                            // start date validation
                            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                            	Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                            			invBuildUnbuildAssembly.getBuaDate(), invIlRawMaterial.getInvItem().getIiName(),
                            			invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                            	if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
                            			invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" +invIlRawMaterial.getInvItem().getIiName() + ")");
                            }
                            // check for branch mapping
                            
                            LocalAdBranchItemLocation adBranchIlRawMaterial = null;
                            try{
                                
                                adBranchIlRawMaterial = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                        invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);
                                
                            } catch (FinderException ex) {
                                
                            }
                            
                            LocalGlChartOfAccount glCoaRawMaterial = null;
                            
                            if (adBranchIlRawMaterial == null) {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        invIlRawMaterial.getIlGlCoaInventoryAccount());
                                
                            } else {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        adBranchIlRawMaterial.getBilCoaGlInventoryAccount());
                                
                            }
                            
                            LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);												
                            
                            double COST = 0d;
                            
                            try {
                                
                                LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                        invBuildUnbuildAssembly.getBuaDate(), invItem.getIiName(),
                                        invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);
                                
                                COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
                                
                            } catch (FinderException ex) {	 
                                
                                COST = invItem.getIiUnitCost();	  	        	 	  	
                                
                            }								
                            
                            double BOM_AMOUNT = EJBCommon.roundIt(convertedQuantity * COST, this.getGlFcPrecisionUnit(AD_CMPNY));
                           
                            
                            TOTAL_AMOUNT += BOM_AMOUNT;
                            ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);
                            
                            
                            
                            this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE, Math.abs(BOM_AMOUNT),
                                    glCoaRawMaterial.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
                            
                        }
                        
                    }
                    
                 
                    double LABORCOST =0;
                    double POWERCOST = 0d;
                    double OVERHEADCOST =0d;
                    double FREIGHTCOST = 0d;
                    double FIXEDCOST = 0d;

                    
                    
                    if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaLaborCostAccount()!=null) {
                    	LABORCOST = EJBCommon.roundIt(buildQuantity * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiLaborCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                    	LocalGlChartOfAccount glCoaLaborCost = glChartOfAccountHome.findByCoaCode(
                		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaLaborCostAccount()), AD_CMPNY);
                		
                		this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(LABORCOST),
                		glCoaLaborCost.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
                    }
                    if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaPowerCostAccount()!=null) {
                    	POWERCOST = EJBCommon.roundIt(buildQuantity * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiPowerCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                     	LocalGlChartOfAccount glCoaPowerCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaPowerCostAccount()), AD_CMPNY);
                    	this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(POWERCOST),
                    		glCoaPowerCost.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
                    }

					if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaOverHeadCostAccount()!=null) {
						OVERHEADCOST = EJBCommon.roundIt(buildQuantity * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiOverHeadCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                   		LocalGlChartOfAccount glCoaOverHeadCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaOverHeadCostAccount()), AD_CMPNY);
						this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(OVERHEADCOST),
                    		glCoaOverHeadCost.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
					}
					if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFreightCostAccount()!=null) {
					    FREIGHTCOST = EJBCommon.roundIt(buildQuantity * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiFreightCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                    	LocalGlChartOfAccount glCoaFreightCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFreightCostAccount()), AD_CMPNY);	
						this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(FREIGHTCOST),
                    		glCoaFreightCost.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
					}
					if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFixedCostAccount()!=null) {
						 FIXEDCOST = EJBCommon.roundIt(buildQuantity * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiFixedCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
						  LocalGlChartOfAccount glCoaFixedCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFixedCostAccount()), AD_CMPNY);
                         this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(FIXEDCOST),
                    		glCoaFixedCost.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
                    
                    
					}
				
                    
                    
                    
                
                
                    
                    
           
                    
                    
                    
                    this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(TOTAL_AMOUNT+LABORCOST+POWERCOST+OVERHEADCOST+FREIGHTCOST+FIXEDCOST),
                            glInventoryChartOfAccount.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);				
                    
                }
                
            } else {//sperator
                
                Iterator i = blList.iterator();
                while (i.hasNext()) {
                    
                    InvModBuildUnbuildAssemblyLineDetails mdetails = (InvModBuildUnbuildAssemblyLineDetails) i.next();
                    
                    LocalInvItemLocation invItemLocation = null;
                    
                    try {
                        
                        invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getBlLocName(),
                                mdetails.getBlIiName(), AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                        throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getBlLineNumber()));
                        
                    }
                    
                    LocalInvItem invItem = null;
                    
                    try {
                        
                        invItem = invItemHome.findByIiName(mdetails.getBlIiName(), AD_CMPNY);
                        
                        if (invItem.getIiClass().equals("Stock")) {
                            
                            throw new GlobalInvItemNotAssemblyException(String.valueOf(mdetails.getBlLineNumber()));
                            
                        }
                        
                    } catch (FinderException ex) { }
                    
                    //start date validation
                    if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    			invBuildUnbuildAssembly.getBuaDate(), invItemLocation.getInvItem().getIiName(),
                    			invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                    	if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
                    }
                    Collection invBillOfMaterials = invItemLocation.getInvItem().getInvBillOfMaterials();
                    
                    Iterator j = invBillOfMaterials.iterator();
                    
                    while (j.hasNext()) {
                        
                        LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
                       
                        LocalInvItemLocation invIlRawMaterial = null;
                        
                        try {
                            
                            invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                                    invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(),AD_CMPNY);
                            
                        } catch (FinderException ex) {
                            
                            throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getBlLineNumber()) +
                                    " - Raw Mat. (" + invBillOfMaterial.getBomIiName() + ")");
                            
                        }

                        
                        // start date validation
                        if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                        	Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                        			invBuildUnbuildAssembly.getBuaDate(), invIlRawMaterial.getInvItem().getIiName(),
                        			invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                        	if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
                        			invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
                        }
                    }
                    
                }	
                
                Collection invBuaDistributionRecords = invBuildUnbuildAssembly.getInvDistributionRecords();
                
                i = invBuaDistributionRecords.iterator();
                
                while(i.hasNext()) {
                    
                    LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();
                    
                    if(distributionRecord.getDrDebit() == 1) {
                        
                        ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();
                        
                    }
                }
            }
            
            
            //insufficient stock checking
            if (adPreference.getPrfArCheckInsufficientStock() == EJBCommon.TRUE && !isDraft) {        		
            	
            	
            	boolean hasInsufficientItems = false;
            	String insufficientItems = "";
            	
            	Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
            	
            	Iterator i = invBuildUnbuildAssemblyLines.iterator();     	  
            	
            	while (i.hasNext()) {
            		
            		LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)i.next();
            		
            		
            		if(invBuildUnbuildAssemblyLine.getBlBuildQuantity()<0){

            			LocalInvCosting invAssemblyCosting = null;
        				
        				double BL_QTY = this.convertByUomFromAndItemAndQuantity(
        						invBuildUnbuildAssemblyLine.getInvUnitOfMeasure(), 
        						invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem(), 
        						Math.abs(invBuildUnbuildAssemblyLine.getBlBuildQuantity()), AD_CMPNY);
        				
        				try {
        					
        					invAssemblyCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
        							invBuildUnbuildAssembly.getBuaDate(), invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName(), 
        							invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
        					
        				} catch (FinderException ex) {
        					
        				}
            			
            			double CURR_QTY = 0;
        				
        				try{
        					if(invAssemblyCosting != null){
        						CURR_QTY = this.convertByUomAndQuantity(invAssemblyCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure(), 
        								invAssemblyCosting.getInvItemLocation().getInvItem(),
        								invAssemblyCosting.getCstRemainingQuantity(), AD_CMPNY);
        						
        					}
        				}catch (Exception e) {
        					
        				}
        				if (invAssemblyCosting == null || CURR_QTY == 0 || CURR_QTY < BL_QTY) {
        					
        					hasInsufficientItems = true;
        					
        					insufficientItems += invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName() + ", ";        					
        				}
            			
            		} else if(invBuildUnbuildAssemblyLine.getBlBuildQuantity()>0) {

            			Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
            			
            			Iterator j = invBillOfMaterials.iterator();
            			
            			while (j.hasNext()) {
            				
            				LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
            				
            				LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
            				LocalInvCosting invBomCosting = null;
            				
            				double BL_QTY = this.convertByUomFromAndItemAndQuantity(
            						invBuildUnbuildAssemblyLine.getInvUnitOfMeasure(), 
            						invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem(), 
            						Math.abs(invBuildUnbuildAssemblyLine.getBlBuildQuantity()), AD_CMPNY);
            				
            				try {
            					
            					invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
            							invBuildUnbuildAssembly.getBuaDate(), invBillOfMaterial.getBomIiName(), 
            							invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);
            					
            				} catch (FinderException ex) {
            					
            				}
            				
            				LocalInvItem bomItm = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(),AD_CMPNY);
            				
            				
            				double NEEDED_QTY = this.convertByUomFromAndItemAndQuantityNoRounding(invBillOfMaterial.getInvUnitOfMeasure(), bomItm,
            						invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY);
            				double CURR_QTY = 0;
            				
            				try{
            					if(invBomCosting != null){
            						CURR_QTY = this.convertByUomAndQuantity(invBomCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure(), 
            								invBomCosting.getInvItemLocation().getInvItem(),
            								invBomCosting.getCstRemainingQuantity(), AD_CMPNY);
            						
            					}
            				}catch (Exception e) {
            					
            				}
            				
            				if (invBomCosting == null || CURR_QTY == 0 || CURR_QTY < EJBCommon.roundIt(NEEDED_QTY * BL_QTY, (short)3)) {
            					
            					hasInsufficientItems = true;
            					
            					insufficientItems += invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName()
            					+ "-" + invBillOfMaterial.getBomIiName() + ", ";
            				}
            			}
            		}
            	}
            	
            	if(hasInsufficientItems) {
            		
            		throw new GlobalRecordInvalidException(insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
            	}
            	
            }
            
            // generate approval status
            
            String INV_APPRVL_STATUS = null;
            if (!isDraft) {
                
                LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
                
                // check if inv build unbuild assembly approval is enabled
                
                if (adApproval.getAprEnableInvBuild() == EJBCommon.FALSE) {
                    
                    INV_APPRVL_STATUS = "N/A";
                    
                    if (details.getBuaType().equals("BO MATCHED")) {
        	        	invExistingBuildUnbuildAssemblyOrder.setBuaLock(EJBCommon.FALSE);
        	    		    
        	        }
                    
                } else {
                    
                    // check if invoice is self approved
                    
                    LocalAdAmountLimit adAmountLimit = null;
                    
                    try {
                        
                        adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("INV BUILD ASSEMBLY", "REQUESTER", details.getBuaLastModifiedBy(), AD_CMPNY);       			
                        
                    } catch (FinderException ex) {
                        
                        throw new GlobalNoApprovalRequesterFoundException();
                        
                    }
                    
                    if (ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit()) {
                        
                        INV_APPRVL_STATUS = "N/A";
                        
                    } else {
                        
                        // for approval, create approval queue
                        
                        Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("INV BUILD ASSEMBLY", adAmountLimit.getCalAmountLimit(), AD_CMPNY);
                        if (adAmountLimits.isEmpty()) {
                            
                            Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);
                            
                            if (adApprovalUsers.isEmpty()) {
                                
                                throw new GlobalNoApprovalApproverFoundException();
                                
                            }
                            
                            Iterator j = adApprovalUsers.iterator();
                            
                            while (j.hasNext()) {
                                
                                LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
                                
                                LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV BUILD ASSEMBLY", 
                                        invBuildUnbuildAssembly.getBuaCode(), invBuildUnbuildAssembly.getBuaDocumentNumber(),
                                        invBuildUnbuildAssembly.getBuaDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
                                
                                adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
                                
                            }        				 	
                            
                        } else {
                            
                            boolean isApprovalUsersFound = false;
                            
                            Iterator i = adAmountLimits.iterator();
                            
                            while (i.hasNext()) {
                                
                                LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();        				 		
                                
                                if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {
                                    
                                    Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", 
                                            adAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
                                    
                                    Iterator j = adApprovalUsers.iterator();
                                    
                                    while (j.hasNext()) {
                                        
                                        isApprovalUsersFound = true;
                                        
                                        LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
                                        
                                        LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV BUILD ASSEMBLY", 
                                                invBuildUnbuildAssembly.getBuaCode(), invBuildUnbuildAssembly.getBuaDocumentNumber(),
                                                invBuildUnbuildAssembly.getBuaDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
                                        
                                        adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
                                        
                                    }        				 			
                                    
                                    break;
                                    
                                } else if (!i.hasNext()) {
                                    
                                    Collection adApprovalUsers = 
                                        adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
                                    
                                    Iterator j = adApprovalUsers.iterator();
                                    
                                    while (j.hasNext()) {
                                        
                                        isApprovalUsersFound = true;
                                        
                                        LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
                                        
                                        LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV BUILD ASSEMBLY", 
                                                invBuildUnbuildAssembly.getBuaCode(), invBuildUnbuildAssembly.getBuaDocumentNumber(),
                                                invBuildUnbuildAssembly.getBuaDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
                                        
                                        adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
                                        
                                    }        				 			
                                    
                                    break;
                                    
                                }        				 		
                                
                                adAmountLimit = adNextAmountLimit;
                                
                            }
                            
                            if (!isApprovalUsersFound) {
                                
                                throw new GlobalNoApprovalApproverFoundException();
                                
                            }        				 
                            
                        }
                        
                        INV_APPRVL_STATUS = "PENDING";
                    }        			        			        			
                }        		        		        		
            } 
            
            adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
                
            	System.out.println("POSTING");
                this.executeInvBuaPost(invBuildUnbuildAssembly.getBuaCode(), invBuildUnbuildAssembly.getBuaLastModifiedBy(), AD_BRNCH, AD_CMPNY);
                
            } 
            
            // set build unbuild assembly approval status
            
            invBuildUnbuildAssembly.setBuaApprovalStatus(INV_APPRVL_STATUS);
            
            return invBuildUnbuildAssembly.getBuaCode();
            
        } catch (GlobalRecordAlreadyDeletedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalDocumentNumberNotUniqueException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyApprovedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyPendingException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyPostedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalNoApprovalRequesterFoundException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalNoApprovalApproverFoundException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInvItemLocationNotFoundException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInvItemNotAssemblyException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInvCSTRemainingQuantityIsLessThanZeroException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;     
            
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlJREffectiveDatePeriodClosedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalJournalNotBalanceException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;  
            
        } catch (GlobalInventoryDateException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;  
            
        } catch(GlobalBranchAccountNumberInvalidException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;  

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
        	
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalRecordInvalidException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        }catch (GlobalMiscInfoIsRequiredException ex){
        	
            getSessionContext().setRollbackOnly();
            throw ex;
    
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArModCustomerDetails getArCstByCstCustomerCode(String CST_CSTMR_CODE, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getArCstByCstCustomerCode");
        
        LocalArCustomerHome arCustomerHome = null;
        // Initialize EJB Home
     
        try {
        	
        	
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            LocalArCustomer arCustomer = null;
            
            
            try {
                
                arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
                
            }
            
            ArModCustomerDetails mdetails = new ArModCustomerDetails();
            
           
            
            mdetails.setCstCustomerCode(arCustomer.getCstCustomerCode());
           
            mdetails.setCstName(arCustomer.getCstName());
            
            
            return mdetails;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    
    
 
 
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModBuildUnbuildAssemblyDetails getInvBoByBuaRcvBuaNumberAndCstcsutomerCodeAndAdBranch(String BUA_RCV_BUA_NMBR, String CST_CSTMR_CODE, Integer AD_BRNCH, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException,
    	ApRINoPurchaseOrderLinesFoundException {
    	
    	Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvBoByBuaRcvBuaNumberAndCstcsutomerCodeAndAdBranch");
        
        
        
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
        LocalInvTagHome invTagHome = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
        	invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
        	invTagHome = (LocalInvTagHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class); 
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        

        LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = null;
        
        try {
        	System.out.println("BUA_RCV_BUA_NMBR="+BUA_RCV_BUA_NMBR);
        	System.out.println("CST_CSTMR_CODE="+CST_CSTMR_CODE);        	
        	invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByBuaRcvBuaNumberAndBuaReceivingAndCstCustomerCodeAndBrCode(BUA_RCV_BUA_NMBR, EJBCommon.FALSE, CST_CSTMR_CODE, AD_BRNCH, AD_CMPNY);
        	
        	if (invBuildUnbuildAssembly.getBuaPosted() != EJBCommon.TRUE) {
        	
        		throw new GlobalNoRecordFoundException();
        	
        	}
        	
        	System.out.println("DOC NUM="+invBuildUnbuildAssembly.getBuaDocumentNumber());
        	
        	//TODO: get receiving item line
        	
        	Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
        	
        	
        	System.out.println("BUA lines size: " + invBuildUnbuildAssemblyLines.size());
        	String serialNumber = null;
        	
    		Iterator i = invBuildUnbuildAssemblyLines.iterator();
    		
    		while (i.hasNext()) {
    			
    			
    			
    			LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine) i.next();

    			InvModBuildUnbuildAssemblyLineDetails blDetails = new InvModBuildUnbuildAssemblyLineDetails();
    			
	        	blDetails.setBlCode(invBuildUnbuildAssemblyLine.getBlCode());
	        	blDetails.setBlIiName(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName());
	        	blDetails.setBlLocName(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName());
	        	blDetails.setBlUomName(invBuildUnbuildAssemblyLine.getInvUnitOfMeasure().getUomName());
	        	blDetails.setBlIiDescription(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiDescription());
	        	blDetails.setBlMisc(invBuildUnbuildAssemblyLine.getBlMisc());
	        	blDetails.setBlBuildSpecificGravity(invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity());
	        	blDetails.setBlBuildStandardFillSize(invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize());
    			
    			double RCVD_QTY = 0d;
    			double RCVD_DSCNT = 0d;
    			double RCVD_AMNT = 0d;

    			Collection invBuildUnbuildAssemblyLines2 = invBuildUnbuildAssemblyLineHome.findByBlBlCode(invBuildUnbuildAssemblyLine.getBlCode(), AD_CMPNY);
    			
    			Iterator j = invBuildUnbuildAssemblyLines2.iterator();
    			
    			while (j.hasNext()) {
    				
    				LocalInvBuildUnbuildAssemblyLine invBuildUnBuildAssemblyItemLine = (LocalInvBuildUnbuildAssemblyLine) j.next();

    				RCVD_QTY += invBuildUnBuildAssemblyItemLine.getBlBuildQuantity();
    				
    			
    			}

    			double REMAINING_QTY = invBuildUnbuildAssemblyLine.getBlBuildQuantity() - RCVD_QTY;
    			if (REMAINING_QTY <= 0) continue;
    			
    			blDetails.setBlRemaining(REMAINING_QTY < 0 ? 0 : REMAINING_QTY);
    			blDetails.setBlBuildQuantity(REMAINING_QTY < 0 ? 0 : REMAINING_QTY);
    			
    			list.add(blDetails);
   
        		
    		}
    		
    		if (list == null || list.size() == 0) {
    			
    			throw new ApRINoPurchaseOrderLinesFoundException();
    			
    		}

    		InvModBuildUnbuildAssemblyDetails mdetails = new InvModBuildUnbuildAssemblyDetails();
    		

    		mdetails.setBuaCode(invBuildUnbuildAssembly.getBuaCode());
    		mdetails.setBuaCstName(invBuildUnbuildAssembly.getArCustomer().getCstName());
    		mdetails.setBuaDescription(invBuildUnbuildAssembly.getBuaDescription());
    		mdetails.setBuaBlList(list); 
    		System.out.println("mdetails.getBuaBlList()="+mdetails.getBuaBlList());
			
    		return mdetails;
        	
        } catch (FinderException ex) {
    		
    		throw new GlobalNoRecordFoundException();
    		
        } catch(GlobalNoRecordFoundException ex) {
        
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch(ApRINoPurchaseOrderLinesFoundException ex) {
        
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteInvBuaEntry(Integer BUA_CODE, String AD_USR, Integer AD_CMPNY) throws 
    GlobalRecordAlreadyDeletedException {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean deleteInvBuaEntry");
        
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);  
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(BUA_CODE);
            
            // remove all build unbuild assembly lines
            
            Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
            
            Iterator k = invBuildUnbuildAssemblyLines.iterator();
            
            while (k.hasNext()) {
                
                LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)k.next();
                
                Collection bomTags = invBuildUnbuildAssemblyLine.getInvTags();
                
                Iterator l = bomTags.iterator();
                
                while(l.hasNext()){
                	LocalInvTag invTag = (LocalInvTag)l.next();
                	invTag.remove();
                	
                }
                
                // build qty conversion
                double buildQuantity = this.convertByUomFromAndItemAndQuantity(
                		invBuildUnbuildAssemblyLine.getInvUnitOfMeasure(), 
                		invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem(),
                		Math.abs(invBuildUnbuildAssemblyLine.getBlBuildQuantity()), AD_CMPNY);
                
                if (invBuildUnbuildAssemblyLine.getBlBuildQuantity() < 0) {
                    
                    invBuildUnbuildAssemblyLine.getInvItemLocation().setIlCommittedQuantity(invBuildUnbuildAssemblyLine.getInvItemLocation().getIlCommittedQuantity() - buildQuantity);
                    
                } else {
                    
                    Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                    
                    Iterator j = invBillOfMaterials.iterator();
                    
                    while (j.hasNext()) {
                        
                        LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
                        
                        LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
                        
                        double specificGravity = invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity();
                        double standardFillSize = invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize();
                        
                        // bom conversion
                        double convertedQuantity = 
                        				invItemLocation.getInvItem().getIiAdLvCategory().contains("RAWMAT") && !invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiAdLvCategory().contains("Bulk") ? 
                                        buildQuantity * (invBillOfMaterial.getBomQuantityNeeded() / 100) * (standardFillSize/1000) * specificGravity
                                        :
			                        	this.convertByUomFromAndItemAndQuantity(
			                                    invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
			                                    EJBCommon.roundIt( buildQuantity * (invBillOfMaterial.getBomQuantityNeeded() / 100) ,
			                                    this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY)
			                        
			                        ;
                        
                        invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);
                        
                    }
                    
                }
                
            }
            
            if (invBuildUnbuildAssembly.getBuaApprovalStatus() != null && invBuildUnbuildAssembly.getBuaApprovalStatus().equals("PENDING")) {
                
                Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("INV BUILD ASSEMBLY", invBuildUnbuildAssembly.getBuaCode(), AD_CMPNY);
                
                Iterator i = adApprovalQueues.iterator();
                
                while(i.hasNext()) {
                    
                    LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
                    
                    adApprovalQueue.remove();
                    
                }
                
            }
            
            adDeleteAuditTrailHome.create("INV BUILD ASSEMBLY", invBuildUnbuildAssembly.getBuaDate(), invBuildUnbuildAssembly.getBuaDocumentNumber(), invBuildUnbuildAssembly.getBuaReferenceNumber(),
 					0d, AD_USR, new Date(), AD_CMPNY);
            
            invBuildUnbuildAssembly.remove();
            
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getGlFcPrecisionUnit");
        
        
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            return  adCompany.getGlFunctionalCurrency().getFcPrecision();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvGpQuantityPrecisionUnit");
        
        LocalAdPreferenceHome adPreferenceHome = null;         
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            return adPreference.getPrfInvQuantityPrecisionUnit();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getInvGpInventoryLineNumber(Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvGpInventoryLineNumber");
        
        LocalAdPreferenceHome adPreferenceHome = null;
        
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            return adPreference.getPrfInvInventoryLineNumber();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdApprovalNotifiedUsersByBuaCode(Integer BUA_CODE, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getAdApprovalNotifiedUsersByBuaCode");
        
        
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        
        ArrayList list = new ArrayList();
        
        
        // Initialize EJB Home
        
        try {
            
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);       
            invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(BUA_CODE);
            
            if (invBuildUnbuildAssembly.getBuaPosted() == EJBCommon.TRUE) {
                
                list.add("DOCUMENT POSTED");
                return list;
                
            }
            
            Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("INV BUILD ASSEMBLY", BUA_CODE, AD_CMPNY);
            
            Iterator i = adApprovalQueues.iterator();
            
            while(i.hasNext()) {
                
                LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
                
                list.add(adApprovalQueue.getAdUser().getUsrDescription());
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/ 
    public double getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(String II_NM, String LOC_NM, String UOM_NM, Integer AD_BRNCH, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvCstRemainingQuantityByIiNameAndLocNameAndUomName");
        
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        LocalInvItemLocation invItemLocation = null;
        LocalInvCosting invCosting = null;
        
        double QTY = 0d;
        
        try {

        	try{

        		invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);

            	invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

            	QTY = invCosting.getCstRemainingQuantity();

        	}  catch (FinderException ex) {

        		if (invCosting == null && invItemLocation != null && invItemLocation.getIlCommittedQuantity() > 0) {

        			QTY = 0d;

        		} else {

        			return QTY;

        		}

        	}

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	

        	LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);

        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItemLocation.getInvItem().getIiName(), invUnitOfMeasure.getUomName(), AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItemLocation.getInvItem().getIiName(), invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(QTY * invUnitOfMeasureConversion.getUmcConversionFactor()/ invDefaultUomConversion.getUmcConversionFactor() , adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    
    // private methods
    
    private LocalInvBuildUnbuildAssemblyLine addInvBlEntry(InvModBuildUnbuildAssemblyLineDetails mdetails, 
            LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly, Integer AD_CMPNY) throws GlobalMiscInfoIsRequiredException {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean addInvBlEntry");
        
        LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class); 
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class); 
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
        
        try {

        	System.out.println("mdetails.getBlBlCode()------------->"+ mdetails.getBlBlCode());
        	System.out.println("mdetails.getBlBuildSpecificGravity()------------->"+ mdetails.getBlBuildSpecificGravity());
            LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = invBuildUnbuildAssemblyLineHome.create(
                    mdetails.getBlBuildQuantity(), mdetails.getBlBuildSpecificGravity(), mdetails.getBlBuildStandardFillSize(), invBuildUnbuildAssembly.getBuaType().equals("BO MATCHED") ? mdetails.getBlBlCode() : null, AD_CMPNY);
            
            //invBuildUnbuildAssembly.addInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
            invBuildUnbuildAssemblyLine.setInvBuildUnbuildAssembly(invBuildUnbuildAssembly);
            
            LocalInvUnitOfMeasure invUnitOfMeasure = 
                invUnitOfMeasureHome.findByUomName(mdetails.getBlUomName(), AD_CMPNY);
            //invUnitOfMeasure.addInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
            invBuildUnbuildAssemblyLine.setInvUnitOfMeasure(invUnitOfMeasure);
            
            LocalInvItemLocation invItemLocation =
                invItemLocationHome.findByLocNameAndIiName(mdetails.getBlLocName(),
                        mdetails.getBlIiName(), AD_CMPNY);
            //invItemLocation.addInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
            invBuildUnbuildAssemblyLine.setInvItemLocation(invItemLocation);
            
            if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiTraceMisc()==1){
            	/*
            	if(mdetails.getBlMisc()==null || mdetails.getBlMisc()==""){
	    			

	    			throw new GlobalMiscInfoIsRequiredException();
	    			
	    		}else{
	    			int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(mdetails.getBlMisc()));
	    			
	    			String miscList2Prpgt = this.checkExpiryDates(mdetails.getBlMisc(), qty2Prpgt, "False");
	    			if(miscList2Prpgt!="Error"){
	    				invBuildUnbuildAssemblyLine.setBlMisc(mdetails.getBlMisc());
	    			}else{
	    				throw new GlobalMiscInfoIsRequiredException();
	    			}
	    			
	    		}
	    		*/
            }else{
            	invBuildUnbuildAssemblyLine.setBlMisc(mdetails.getBlMisc());
            }
            
            return invBuildUnbuildAssemblyLine;
        /*    
        }catch (GlobalMiscInfoIsRequiredException ex){
        	
            getSessionContext().setRollbackOnly();
            throw ex;
		*/
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }	
    
    public String checkExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	
    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}
    	
    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	
    	
    	String miscList = new String();
    	String miscList2 = "";
    	
    	for(int x=0; x<qty; x++) {
    		
    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		String g= misc.substring(start, start + length);
    		System.out.println("g: " + g);
    		System.out.println("g length: " + g.length());
    		if(g.length()!=0){
    			if(g!=null || g!="" || g!="null"){
    				if(g.contains("null")){
    					miscList2 = "Error";
    				}else{
    					miscList = miscList + "$" + g;
    				}
    			}else{
    				miscList2 = "Error";
    			}
    				
    			System.out.println("miscList G: " + miscList);
    		}else{
    			System.out.println("KABOOM");
    			miscList2 = "Error";
    		}
    	}	
    	System.out.println("miscList2 :" + miscList2);
    	if(miscList2==""){
    		miscList = miscList+"$";
    	}else{
    		miscList = miscList2;
    	}
    	
    	System.out.println("miscList :" + miscList);
    	return (miscList);
    }
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE,
            LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly, Integer AD_BRNCH, Integer AD_CMPNY)throws
            GlobalBranchAccountNumberInvalidException,
            GlobalAccountNumberInvalidException {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean addInvDrEntry");
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;        
        LocalGlChartOfAccountHome glChartOfAccountHome = null;           
        
        // Initialize EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
            
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
        
        try {        
            
            // get company
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            // validate coa
            
            LocalGlChartOfAccount glChartOfAccount = null;
            
            try {
                
                glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
                
            } catch(FinderException ex) {
                
                throw new GlobalBranchAccountNumberInvalidException ();
                
            }
            
            if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
                throw new GlobalAccountNumberInvalidException();
            
            // create distribution record        
            
            LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
                    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
                    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
            
            //invBuildUnbuildAssembly.addInvDistributionRecord(invDistributionRecord);
            invDistributionRecord.setInvBuildUnbuildAssembly(invBuildUnbuildAssembly);
            //glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
            invDistributionRecord.setInvChartOfAccount(glChartOfAccount);
            
        } catch(GlobalAccountNumberInvalidException ex) {
            
            throw new GlobalAccountNumberInvalidException ();
            
        } catch(GlobalBranchAccountNumberInvalidException ex) {
            
            throw new GlobalBranchAccountNumberInvalidException ();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }	
    
    
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean convertByUomFromAndItemAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
             
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }	
    
    
    private double convertByUomFromAndItemAndQuantityNoRounding(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean convertByUomFromAndItemAndQuantityNoRounding");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();
             
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }	
    
    private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean convertByUomAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
       
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }	    

    private void executeInvBuaPost(Integer BUA_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
    GlobalRecordAlreadyDeletedException,		
    GlobalTransactionAlreadyPostedException,
    GlJREffectiveDateNoPeriodExistException,
    GlJREffectiveDatePeriodClosedException,
    GlobalJournalNotBalanceException,
    GlobalInvItemLocationNotFoundException, 
    GlobalInvCSTRemainingQuantityIsLessThanZeroException,
    GlobalBranchAccountNumberInvalidException,
	AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean executeInvBuaPost");
        
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemHome invItemHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
            
            // validate if build/unbuild assembly is already deleted
            
            LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = null;
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            try {
                
                invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(BUA_CODE);
                
            } catch (FinderException ex) {
                
                throw new GlobalRecordAlreadyDeletedException();
                
            }
            
            // validate if build/unbuild assembly is already posted or void
            
            if (invBuildUnbuildAssembly.getBuaPosted() == EJBCommon.TRUE) {
                
                throw new GlobalTransactionAlreadyPostedException();
                
            }
            
            // regenerate inventory dr
            
            this.regenerateInventoryDr(invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
            
            Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
            
            Iterator c = invBuildUnbuildAssemblyLines.iterator();
            
            while(c.hasNext()) {
                
                LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine) c.next();
                
                String II_NM = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName();
                String LOC_NM = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName();
                
                

                // build qty conversion
                double BUILD_QTY = this.convertByUomFromAndItemAndQuantity(
                		invBuildUnbuildAssemblyLine.getInvUnitOfMeasure(), 
                		invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem(),
                		invBuildUnbuildAssemblyLine.getBlBuildQuantity(), AD_CMPNY);
                
                
                
                 double LABORCOST =0;
                    double POWERCOST = 0d;
                    double OVERHEADCOST =0d;
                    double FREIGHTCOST = 0d;
                    double FIXEDCOST = 0d;

                    
                    
                    if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaLaborCostAccount()!=null) {
                    	LABORCOST = EJBCommon.roundIt(invBuildUnbuildAssemblyLine.getBlBuildQuantity() * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiLaborCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                    	LocalGlChartOfAccount glCoaLaborCost = glChartOfAccountHome.findByCoaCode(
                		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaLaborCostAccount()), AD_CMPNY);
                	
                    }
                    if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaPowerCostAccount()!=null) {
                    	POWERCOST = EJBCommon.roundIt(invBuildUnbuildAssemblyLine.getBlBuildQuantity() * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiPowerCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                     	LocalGlChartOfAccount glCoaPowerCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaPowerCostAccount()), AD_CMPNY);
                    	
                    }

					if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaOverHeadCostAccount()!=null) {
						OVERHEADCOST = EJBCommon.roundIt(invBuildUnbuildAssemblyLine.getBlBuildQuantity() * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiOverHeadCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                   		LocalGlChartOfAccount glCoaOverHeadCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaOverHeadCostAccount()), AD_CMPNY);
						
					}
					if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFreightCostAccount()!=null) {
					    FREIGHTCOST = EJBCommon.roundIt(invBuildUnbuildAssemblyLine.getBlBuildQuantity() * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiFreightCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                    	LocalGlChartOfAccount glCoaFreightCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFreightCostAccount()), AD_CMPNY);	
					}
					if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFixedCostAccount()!=null) {
						 FIXEDCOST = EJBCommon.roundIt(invBuildUnbuildAssemblyLine.getBlBuildQuantity() * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiFixedCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
						  LocalGlChartOfAccount glCoaFixedCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFixedCostAccount()), AD_CMPNY);
                       
                    
					}
				
                    
                
                
                
            
                double TOTAL_AMOUNT = LABORCOST + POWERCOST + OVERHEADCOST + FREIGHTCOST + FIXEDCOST;
                
                Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                
                Iterator j = invBillOfMaterials.iterator();
                
                while (j.hasNext()) {
                    
                    LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)j.next();
                    LocalInvCosting invBomCosting = null;
                    
                    
                    String BOM_II_NM = invBillOfMaterial.getBomIiName();
                    String BOM_LOC_NM = invBillOfMaterial.getBomLocName();
                    
                    
                    double BOM_QTY_NDD = 0d;
               
                    double COST = 0d;
                    double BOM_AMOUNT = 0d;
              
                    
                    LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);
                    
                    // post bill of materials
                    
                    try {
                        
                        invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                invBuildUnbuildAssembly.getBuaDate(), invBillOfMaterial.getBomIiName(), invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                    }
                    
                //TODO DO
                    
                    double specificGravity = invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity();
                    double standardFillSize = invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize();
                    
                    
                 // bom conversion		    						
                    BOM_QTY_NDD =
                    		invItem.getIiAdLvCategory().contains("RAWMAT") && !invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiAdLvCategory().contains("Bulk") ? 
                                    BUILD_QTY * (invBillOfMaterial.getBomQuantityNeeded()/ 100) * (standardFillSize/1000) * specificGravity
                    		:
                            	this.convertByUomFromAndItemAndQuantity(
                                        invBillOfMaterial.getInvUnitOfMeasure(), invItem,
                                        EJBCommon.roundIt(   Math.abs(BUILD_QTY) * (invBillOfMaterial.getBomQuantityNeeded() ),
                                        this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);
                
                    System.out.println("BOM="+invItem.getIiName());             
                    System.out.println("specificGravity="+specificGravity);
                    System.out.println("standardFillSize="+standardFillSize);
                    System.out.println("BUILD_QTY="+BUILD_QTY);
                    

                    if (invBomCosting == null) {
  
                        BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * invItem.getIiUnitCost(), adCompany.getGlFunctionalCurrency().getFcPrecision());
                        TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

                       
                        if (BUILD_QTY >=0) {
                            
                            this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), 
                            		-BOM_QTY_NDD, -BOM_AMOUNT,
                            		-BOM_QTY_NDD, -BOM_AMOUNT, 
                            				BOM_II_NM, BOM_LOC_NM, 0d,
									null, AD_BRNCH, AD_CMPNY);    						
                           
                        } else {
                            
                        	 this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), 
                        			 BOM_QTY_NDD, BOM_AMOUNT,
                        			 BOM_QTY_NDD, BOM_AMOUNT, 
                             				BOM_II_NM, BOM_LOC_NM, 0d,
 									null, AD_BRNCH, AD_CMPNY);    						
                            
                        }
                        
                    } else {
                    	
                        System.out.println("ELSE---------------------->");
                        
                           
                        COST = Math.abs(invBomCosting.getCstRemainingValue() / invBomCosting.getCstRemainingQuantity());
                        
                        BOM_AMOUNT = EJBCommon.roundIt(BOM_QTY_NDD * invItem.getIiUnitCost(), adCompany.getGlFunctionalCurrency().getFcPrecision());
  
                        TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);

        			        //compute cost variance   
        					double CST_VRNC_VL = 0d;

        					if(invBomCosting.getCstRemainingQuantity() < 0)
        						CST_VRNC_VL = (invBomCosting.getCstRemainingQuantity() * (BOM_AMOUNT / BOM_QTY_NDD) -
        								invBomCosting.getCstRemainingValue());
        					
        					
        					if(invBomCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {
        	                	
        	                	
        	                	
        	                    if (BUILD_QTY >= 0) {
        	                        
        	                        System.out.println("Assemble");
        	                        
        	                        this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), -BOM_QTY_NDD, -TOTAL_AMOUNT,
        	                        		invBomCosting.getCstRemainingQuantity() - BOM_QTY_NDD,
        	                        		invBomCosting.getCstRemainingValue() - TOTAL_AMOUNT, BOM_II_NM, BOM_LOC_NM, CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);
        	                        
        	                    } else {
        	                    	    System.out.println("Disassemble");
        	                    	this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BOM_QTY_NDD, TOTAL_AMOUNT,
        	                    			invBomCosting.getCstRemainingQuantity() + BOM_QTY_NDD,
        	                    			invBomCosting.getCstRemainingValue() + TOTAL_AMOUNT, BOM_II_NM, BOM_LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
        	                    	
        	                    }
                        	} else if(invBomCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {
                        		
                        		
                        	
        	        			LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(BOM_II_NM, BOM_LOC_NM, AD_CMPNY);
        	        			
        	        			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(invBuildUnbuildAssembly.getBuaDate(), invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY) ;         
        	        			
        	        			Iterator x = invFifoCostings.iterator();
        	        			
        	        			double neededQty = BOM_QTY_NDD;
        	        			double neededAmount = BOM_AMOUNT;
        	        			double remaining = invBomCosting.getCstRemainingQuantity();
        	        			double remainingAmount = invBomCosting.getCstRemainingValue();
        	        			
        	        			while(x.hasNext() && neededQty != 0) {
        	 	 	  				
        	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
        	 	 	  				
	        	 	 	  			int CST_LN_NMBR = 0;
		 			  				try {
		 			  	                
		 			  	                // generate line number
		 			  	                LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(invBuildUnbuildAssembly.getBuaDate().getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
		 			  	                CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
		 			  	                
		 			  	            } catch (FinderException ex) { CST_LN_NMBR = 1;}
		 			  				
        	
        	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {
        	 	  						System.out.println("1.neededQty="+neededQty);
        	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
        	 			  				invFifoCosting.setCstRemainingQuantity(invFifoCosting.getCstRemainingQuantity() - neededQty);
        	 			  				invFifoCosting.setCstRemainingValue(invFifoCosting.getCstRemainingValue() - neededAmount);
        	 			  				
        	 			  				LocalInvCosting invCosting = invCostingHome.create(invBuildUnbuildAssembly.getBuaDate(), invBuildUnbuildAssembly.getBuaDate().getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d , AD_BRNCH, AD_CMPNY);
        	 			  	       
        	 			  	            invCosting.setInvItemLocation(invItemLocation);
        	 			  	            invCosting.setInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
        	 			  	            invCosting.setCstQCNumber(invFifoCosting.getCstQCNumber());
        	 			  	            invCosting.setCstExpiryDate(invFifoCosting.getCstExpiryDate());
        	 			  	            invCosting.setCstRemainingLifoOutQuantity(neededQty);
        	 			  	            invCosting.setCstAssemblyQuantity(neededQty * -1);
        	 			  	            
        	 			  	            remaining -= neededQty;
        	 			  	            remainingAmount -= neededAmount;
        	 			  	            invCosting.setCstRemainingQuantity(remaining);
        	 			  	            invCosting.setCstRemainingValue(remainingAmount);
        	 			  	            remainingAmount = 0d;
        	 			  	            neededQty = 0d;	 
        	 	  					} else {
        	 	  						System.out.println("2.neededQty="+neededQty);
        	 	  						
        	 	  						LocalInvCosting invCosting = invCostingHome.create(invBuildUnbuildAssembly.getBuaDate(), invBuildUnbuildAssembly.getBuaDate().getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d , AD_BRNCH, AD_CMPNY);
             	 			  	       
        	 			  	            invCosting.setInvItemLocation(invItemLocation);
        	 			  	            invCosting.setInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
        	 			  	            invCosting.setCstQCNumber(invFifoCosting.getCstQCNumber());
        	 			  	            invCosting.setCstExpiryDate(invFifoCosting.getCstExpiryDate());
        	 			  	            invCosting.setCstRemainingLifoOutQuantity(invFifoCosting.getCstRemainingLifoQuantity());
        	 			  	            //invCosting.setCstAssemblyQuantity(invFifoCosting.get);
        	 			  	            
        	 			  	            remaining -= neededQty;
        	 			  	            remainingAmount -= neededAmount;
        	 			  	            invCosting.setCstRemainingQuantity(remaining);
        	 			  	            invCosting.setCstRemainingValue(remainingAmount);
        	 			  	            neededAmount -= invFifoCosting.getCstRemainingValue();
        	 			  	            neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
        	 			  	            
        	 			  	            invFifoCosting.setCstRemainingLifoQuantity(0);
     	 	  						
        	 	  						
        	 	  						
        	 	  					}
        	 	  				}
        	        			
        	        			/*invLastBomCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocNameAndFifoRemainingQuantity(
                                        invBuildUnbuildAssembly.getBuaDate(), BOM_II_NM, BOM_LOC_NM, AD_BRNCH, AD_CMPNY); 
                                
        	        			
        	        			
        	        			this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BOM_QTY_NDD, 0d, 
    		        					invLastBomCosting.getCstRemainingQuantity() - BOM_QTY_NDD, 
    		        					0d, BOM_II_NM, BOM_LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
    	                    */
        	        			//TODO
        	        			//post entries to database        			
        	        			/*
        	                    if (BOM_QTY_NDD >= 0) 
        	                    {
        		        			this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BOM_QTY_NDD, fifoCost * BOM_QTY_NDD, 
        		        					invLastBomCosting.getCstRemainingQuantity() + BOM_QTY_NDD, 
        		        					invLastBomCosting.getCstRemainingValue() + (fifoCost * BOM_QTY_NDD), BOM_II_NM, BOM_LOC_NM, CST_VRNC_VL, null, AD_BRNCH, AD_CMPNY);
        	                    } else {
        		        			this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BOM_QTY_NDD, fifoCost * BOM_QTY_NDD, 
        		        					invLastBomCosting.getCstRemainingQuantity() + BOM_QTY_NDD, 
        		        					invLastBomCosting.getCstRemainingValue() + (fifoCost * BOM_QTY_NDD), BOM_II_NM, BOM_LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
        	                    }*/
                        		
                        	} else if(invBomCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {
                        		
        	        			double standardCost = invBomCosting.getInvItemLocation().getInvItem().getIiUnitCost();
        	        			
        	        			//post entries to database        			
        	        			
        	                    if (BUILD_QTY >= 0) 
        	                    {
        		        			this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BOM_QTY_NDD, standardCost * BOM_QTY_NDD, 
        		        					invBomCosting.getCstRemainingQuantity() - BOM_QTY_NDD, 
        		        					invBomCosting.getCstRemainingValue() + (standardCost * BOM_QTY_NDD), BOM_II_NM, BOM_LOC_NM, CST_VRNC_VL, null, AD_BRNCH, AD_CMPNY);
        	                    } else {
        		        			this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BOM_QTY_NDD, standardCost * BOM_QTY_NDD, 
        		        					invBomCosting.getCstRemainingQuantity() - BOM_QTY_NDD, 
        		        					invBomCosting.getCstRemainingValue() + (standardCost * BOM_QTY_NDD), BOM_II_NM, BOM_LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
        	                    }
                        		
                        	}
        					
 
                      
                        
                    }
                    
                }
                
                // post assembly item
                
                LocalInvCosting invLastCosting = null;
                try {
                    
                    invLastCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invBuildUnbuildAssembly.getBuaDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }

                System.out.println("invLastCosting="+invLastCosting);
                if (invLastCosting == null) {
                    
                    if (BUILD_QTY >= 0) {
                        
                        this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, 
                        		TOTAL_AMOUNT, BUILD_QTY, TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
                        
                    } else {
                        
                        this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, 
                                -TOTAL_AMOUNT, BUILD_QTY, -TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
                        
                    }			
                    
                } else {

			        //compute cost variance   
					double CST_VRNC_VL = 0d;

					if(invLastCosting.getCstRemainingQuantity() < 0)
						CST_VRNC_VL = (invLastCosting.getCstRemainingQuantity() * (TOTAL_AMOUNT/BUILD_QTY) -
								invLastCosting.getCstRemainingValue());
					System.out.println("COST METHOD="+invLastCosting.getInvItemLocation().getInvItem().getIiCostMethod());
					
					if(invLastCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {
                	
	                    if (BUILD_QTY >= 0) {
	                        
	                        this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, TOTAL_AMOUNT,
	                        		invLastCosting.getCstRemainingQuantity() + BUILD_QTY,
									invLastCosting.getCstRemainingValue() + TOTAL_AMOUNT, II_NM, LOC_NM, CST_VRNC_VL, USR_NM, AD_BRNCH, AD_CMPNY);
	                        
	                    } else {
	                    	
	                    	this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, -TOTAL_AMOUNT,
	                        		invLastCosting.getCstRemainingQuantity() + BUILD_QTY,
	                        		invLastCosting.getCstRemainingValue() - TOTAL_AMOUNT, II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
	                    	
	                    }
                	} else if(invLastCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {
                		
	        			double fifoCost = this.getInvFifoCost(invLastCosting.getCstDate(), invLastCosting.getInvItemLocation().getIlCode(), 
	        					BUILD_QTY, TOTAL_AMOUNT/BUILD_QTY, true, AD_BRNCH, AD_CMPNY);
	        			
	        			//post entries to database        			
	        			
	                    if (BUILD_QTY >= 0) 
	                    {
		        			this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, fifoCost * BUILD_QTY, 
		        					invLastCosting.getCstRemainingQuantity() + BUILD_QTY, 
		        					invLastCosting.getCstRemainingValue() + (fifoCost * BUILD_QTY), II_NM, LOC_NM, CST_VRNC_VL, null, AD_BRNCH, AD_CMPNY);
	                    }
	                    else
	                    {
		        			this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, fifoCost * BUILD_QTY, 
		        					invLastCosting.getCstRemainingQuantity() + BUILD_QTY, 
		        					invLastCosting.getCstRemainingValue() + (fifoCost * BUILD_QTY), II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
	                    }
                		
                	} else if(invLastCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {
                		
	        			double standardCost = invLastCosting.getInvItemLocation().getInvItem().getIiUnitCost();
	        			
	        			//post entries to database        			
	        			
	                    if (BUILD_QTY >= 0) 
	                    {
		        			this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, standardCost * BUILD_QTY, 
		        					invLastCosting.getCstRemainingQuantity() + BUILD_QTY, 
		        					invLastCosting.getCstRemainingValue() + (standardCost * BUILD_QTY), II_NM, LOC_NM, CST_VRNC_VL, null, AD_BRNCH, AD_CMPNY);
	                    }
	                    else
	                    {
		        			this.post(invBuildUnbuildAssemblyLine, invBuildUnbuildAssembly.getBuaDate(), BUILD_QTY, standardCost * BUILD_QTY, 
		        					invLastCosting.getCstRemainingQuantity() + BUILD_QTY, 
		        					invLastCosting.getCstRemainingValue() + (standardCost * BUILD_QTY), II_NM, LOC_NM, 0d, null, AD_BRNCH, AD_CMPNY);
	                    }
                		
                	}
                    
                }
                
            }
            
            // post build/unbuild assembly
            
            // set build/unbuild assembly post status
            
            invBuildUnbuildAssembly.setBuaPosted(EJBCommon.TRUE);
            invBuildUnbuildAssembly.setBuaPostedBy(USR_NM);
            invBuildUnbuildAssembly.setBuaDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
            
            // post to gl if necessary
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            //LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            if (adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {
                
                // validate if date has no period and period is closed
                
                LocalGlSetOfBook glJournalSetOfBook = null;
                
                try {
                    
                    glJournalSetOfBook = glSetOfBookHome.findByDate(invBuildUnbuildAssembly.getBuaDate(), AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                    throw new GlJREffectiveDateNoPeriodExistException();
                    
                }
                
                LocalGlAccountingCalendarValue glAccountingCalendarValue = 
                    glAccountingCalendarValueHome.findByAcCodeAndDate(
                            glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invBuildUnbuildAssembly.getBuaDate(), AD_CMPNY);
                
                
                if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
                        glAccountingCalendarValue.getAcvStatus() == 'C' ||
                        glAccountingCalendarValue.getAcvStatus() == 'P') {
                    
                    throw new GlJREffectiveDatePeriodClosedException();
                    
                }
                
                // check if invoice is balance if not check suspense posting
                
                LocalGlJournalLine glOffsetJournalLine = null;
                
                Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByBuaCode(invBuildUnbuildAssembly.getBuaCode(), AD_CMPNY);
                
                Iterator j = invDistributionRecords.iterator();
                
                double TOTAL_DEBIT = 0d;
                double TOTAL_CREDIT = 0d;
                
                while (j.hasNext()) {
                    
                    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
                    
                    double DR_AMNT = 0d;
                    
                    DR_AMNT = invDistributionRecord.getDrAmount();
                    
                    if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
                        
                        TOTAL_DEBIT += DR_AMNT;
                        
                    } else {
                        
                        TOTAL_CREDIT += DR_AMNT;
                        
                    }
                    
                }
                
                TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
                TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
                
                if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
                        TOTAL_DEBIT != TOTAL_CREDIT) {
                    
                    LocalGlSuspenseAccount glSuspenseAccount = null;
                    
                    try { 	
                        
                        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ASSEMBLIES", AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                        throw new GlobalJournalNotBalanceException();
                        
                    }
                    
                    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
                        
                        glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
                                EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
                        
                    } else {
                        
                        glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
                                EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
                        
                    }
                    
                    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
                    //glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
                    glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
                    
                    
                } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
                        TOTAL_DEBIT != TOTAL_CREDIT) {
                    
                    throw new GlobalJournalNotBalanceException();		    	
                    
                }
                
                // create journal batch if necessary
                
                LocalGlJournalBatch glJournalBatch = null;
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
                
                try {
                    
                    glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ASSEMBLIES", AD_BRNCH, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                if (glJournalBatch == null) {
                    
                    glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ASSEMBLIES", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
                    
                }
                
                System.out.println("CREATING JOURNALS UPON POSTING");
                // create journal entry			            	
                LocalGlJournal glJournal = glJournalHome.create(invBuildUnbuildAssembly.getBuaReferenceNumber(),
                        invBuildUnbuildAssembly.getBuaDescription(), invBuildUnbuildAssembly.getBuaDate(),
                        0.0d, null, invBuildUnbuildAssembly.getBuaDocumentNumber(), null, 1d, "N/A", null,
                        'N', EJBCommon.TRUE, EJBCommon.FALSE,
                        USR_NM, new Date(),
                        USR_NM, new Date(),
                        null, null,
                        USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
                        null, null, EJBCommon.FALSE, null,
  
                        AD_BRNCH, AD_CMPNY);
                
                LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
                glJournal.setGlJournalSource(glJournalSource);
                
                LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
                glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
                
                LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ASSEMBLIES", AD_CMPNY);
                glJournal.setGlJournalCategory(glJournalCategory);
                
                if (glJournalBatch != null) {

                    glJournal.setGlJournalBatch(glJournalBatch);
                    
                }           		    
                
                // create journal lines
                j = invDistributionRecords.iterator();
                
                while (j.hasNext()) {
                    
                    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
                    
                    double DR_AMNT = 0d;
                    
                    DR_AMNT = invDistributionRecord.getDrAmount();
                    
                    LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
                            invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
                    
                    //invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
                    glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());
                    
                    //glJournal.addGlJournalLine(glJournalLine);
                    glJournalLine.setGlJournal(glJournal);
                    
                    invDistributionRecord.setDrImported(EJBCommon.TRUE);
                    
                    
                }
                if (glOffsetJournalLine != null) {
                    
                    //glJournal.addGlJournalLine(glOffsetJournalLine);
                    glOffsetJournalLine.setGlJournal(glJournal);
                    
                }		
                
                // post journal to gl
                
                Collection glJournalLines = glJournal.getGlJournalLines();
                
                Iterator i = glJournalLines.iterator();
                while (i.hasNext()) {
                    
                    LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
                    
                    // post current to current acv
                    
                    this.postToGl(glAccountingCalendarValue,
                            glJournalLine.getGlChartOfAccount(),
                            true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
                    
                    
                    // post to subsequent acvs (propagate)
                    
                    Collection glSubsequentAccountingCalendarValues = 
                        glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
                                glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                                glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
                    
                    Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
                    
                    while (acvsIter.hasNext()) {
                        
                        LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
                            (LocalGlAccountingCalendarValue)acvsIter.next();
                        
                        this.postToGl(glSubsequentAccountingCalendarValue,
                                glJournalLine.getGlChartOfAccount(),
                                false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
                        
                    }
                    
                    // post to subsequent years if necessary
                    
                    Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
                    
                    if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
                        
                        adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
                        LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY); 	  	
                        
                        Iterator sobIter = glSubsequentSetOfBooks.iterator();
                        
                        while (sobIter.hasNext()) {
                            
                            LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
                            
                            String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
                            
                            // post to subsequent acvs of subsequent set of book(propagate)
                            
                            Collection glAccountingCalendarValues = 
                                glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
                            
                            Iterator acvIter = glAccountingCalendarValues.iterator();
                            
                            while (acvIter.hasNext()) {
                                
                                LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
                                    (LocalGlAccountingCalendarValue)acvIter.next();
                                
                                if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
                                        ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
                                    
                                    this.postToGl(glSubsequentAccountingCalendarValue,
                                            glJournalLine.getGlChartOfAccount(),
                                            false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
                                    
                                } else { // revenue & expense
                                    
                                    this.postToGl(glSubsequentAccountingCalendarValue,
                                            glRetainedEarningsAccount,
                                            false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
                                    
                                }
                                
                            }
                            
                            if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
                            
                        }
                        
                    }
                    
                }
                
            }		   
            
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlJREffectiveDatePeriodClosedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalJournalNotBalanceException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalRecordAlreadyDeletedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyPostedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInvItemLocationNotFoundException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInvCSTRemainingQuantityIsLessThanZeroException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalBranchAccountNumberInvalidException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;    		
        	
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, 
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {
	  	 Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvFifoCost");
		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;
	       
	     // Initialize EJB Home
	        
	     try {
	         
	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	     } 
	     catch (NamingException ex) {
	            
	    	 throw new EJBException(ex.getMessage());
	     }
	    	
		try {
			
			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
			
			if (invFifoCostings.size() > 0) {
				
				Iterator x = invFifoCostings.iterator();
			
	  			if (isAdjustFifo) {
	  				
	  				//executed during POST transaction
	  				
	  				double totalCost = 0d;
	  				double cost;
	  				
	  				if(CST_QTY > 0) {
	  					System.out.println("CST_QTY < 0--------------->="+CST_QTY);
	  					System.out.println("");
	  					//for negative quantities
	 	  				double neededQty = CST_QTY;
	 	  				
	 	  				while(x.hasNext() && neededQty != 0) {
	 	 	  				
	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	
	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {  				
	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();  				
	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {  				
	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
	 		 	  			} else {
	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
	 		 	  			}
	
	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {
	 	  						System.out.println("1.neededQty="+neededQty);
	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
	 			  				totalCost += (neededQty * cost);
	 			  				neededQty = 0d;	 			  				 
	 	  					} else {
	 	  						System.out.println("2.neededQty="+neededQty);
	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
	 	  					}
	 	  				}
	 	  				
	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
	 	  				if(neededQty != 0) {
	 	  					
	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
	 	  				}
	 	  				
	 	  				cost = totalCost / -CST_QTY;
	 	  				
	  				} else {
	  					
	  					//for positive quantities
	  					cost = CST_COST;
	  				}
	  				return cost;
	  				
	  			} else {
	  				System.out.println("ELSE------------------------------>>>>>>>>>>"+CST_QTY);
	  				//executed during ENTRY transaction
	  				
	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	  				
	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			}
	  			}
	  			
			} else {
				
				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}
				
		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}     

    
    
    private void post(LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine, Date CST_DT, double CST_ASSMBLY_QTY,
    		double CST_ASSMBLY_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, String II_NM, String LOC_NM, double CST_VRNC_VL,
			String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean post");
        
        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);              
            invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	        lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);   
            

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);
            int CST_LN_NMBR = 0;
            
            CST_ASSMBLY_QTY = EJBCommon.roundIt(CST_ASSMBLY_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_ASSMBLY_CST = EJBCommon.roundIt(CST_ASSMBLY_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
            CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
            
            if (CST_ASSMBLY_QTY < 0) {
                
                invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ASSMBLY_QTY));
                
            }

            try {
                
                // generate line number
                LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
                
            } catch (FinderException ex) {
                
                CST_LN_NMBR = 1;
                
            }

            //void subsequent cost variance adjustments
            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
            Iterator i = invAdjustmentLines.iterator();
            
            while (i.hasNext()){
            	
            	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
            	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
            	
            }
            
            String prevExpiryDates = "";
            String miscListPrpgt ="";
            double qtyPrpgt = 0;
            try {           
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   //System.out.println(prevCst.getCstCode()+"   "+ prevCst.getCstRemainingQuantity());
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();
        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }
        	   //System.out.println("prevExpiryDates: " + prevExpiryDates);
        	   //System.out.println("qtyPrpgt: " + qtyPrpgt);
           }catch (Exception ex){
        	   //System.out.println("prevExpiryDates CATCH: " + prevExpiryDates);
        	   prevExpiryDates="";
           }
	        
            // create tag for BOM
         	System.out.println("Quantity in BU :" + CST_ASSMBLY_QTY);
            // create costing
            LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, CST_ASSMBLY_QTY, CST_ASSMBLY_CST, 0d, 0d, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ASSMBLY_QTY > 0 ? CST_ASSMBLY_QTY : 0, AD_BRNCH, AD_CMPNY);
            
            //invItemLocation.addInvCosting(invCosting);
            invCosting.setInvItemLocation(invItemLocation);
            invCosting.setInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
            
            // Get Latest Expiry Dates           
            if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
            	if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
                	//System.out.println("apPurchaseOrderLine.getPlMisc(): "+invBuildUnbuildAssemblyLine.getBlMisc().length());
                	
                	if(invBuildUnbuildAssemblyLine.getBlMisc()!=null && invBuildUnbuildAssemblyLine.getBlMisc()!="" &&  invBuildUnbuildAssemblyLine.getBlMisc().length()!=0){
            		   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc()));

            		   String miscList2Prpgt = this.propagateExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc(), qty2Prpgt, "False");
            		   ArrayList miscList = this.expiryDates(invBuildUnbuildAssemblyLine.getBlMisc(), qty2Prpgt);
            		   String propagateMiscPrpgt = "";
            		   String ret = "";
            		   String exp = "";
            		   String Checker = "";

            		   //ArrayList miscList2 = null;
            		   if(CST_ASSMBLY_QTY>0){
            			   prevExpiryDates = prevExpiryDates.substring(1);
            			   propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
            		   }else{
            			   Iterator mi = miscList.iterator();

            			   propagateMiscPrpgt = prevExpiryDates;
            			   ret = propagateMiscPrpgt;
            			   while(mi.hasNext()){
            				   String miscStr = (String)mi.next();

            				   Integer qTest = this.checkExpiryDates(ret+"fin$");
            				   ArrayList miscList2 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

            				   //ArrayList miscList2 = this.expiryDates("$" + ret, qtyPrpgt);
            				   Iterator m2 = miscList2.iterator();
            				   ret = "";
            				   String ret2 = "false";
            				   int a = 0;
            				   while(m2.hasNext()){
            					   String miscStr2 = (String)m2.next();

            					   if(ret2=="1st"){
            						   ret2 = "false";
            					   }
            					   System.out.println("miscStr: "+miscStr);
            					   System.out.println("miscStr2: "+miscStr2);

            					   if(miscStr2.equals(miscStr)){
            						   if(a==0){
            							   a = 1;
            							   ret2 = "1st";
            							   Checker = "true";
            						   }else{
            							   a = a+1;
            							   ret2 = "true";
            						   }
            					   }
            					   System.out.println("Checker: "+Checker);
            					   System.out.println("ret2: "+ret2);
            					   if(!miscStr2.equals(miscStr) || a>1){
            						   if((ret2!="1st")&&((ret2=="false")||(ret2=="true"))){
            							   if (miscStr2!=""){
            								   miscStr2 = "$" + miscStr2;
            								   ret = ret + miscStr2;
            								   System.out.println("ret " + ret);
            								   ret2 = "false";
            							   }
            						   }
            					   }

            				   }
            				   if(ret!=""){
            					   ret = ret + "$";
            				   }
            				   System.out.println("ret una: "+ret);
            				   exp = exp + ret;
            				   qtyPrpgt= qtyPrpgt -1;
            			   }
            			   System.out.println("ret fin " + ret);
            			   System.out.println("exp fin " + exp);
            			   //propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
            			   propagateMiscPrpgt = ret;
            			   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
            			   if(Checker=="true"){
            				   //invCosting.setCstExpiryDate(propagateMiscPrpgt);
            			   }else{
            				   System.out.println("Exp Not Found");
            				   throw new GlobalExpiryDateNotFoundException(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName());
            			   }
            		   }
            		   invCosting.setCstExpiryDate(propagateMiscPrpgt);

            	   }else{
                		invCosting.setCstExpiryDate(prevExpiryDates);
                		System.out.println("prevExpiryDates");
                	}
                	
                }else{
                	
                	if(invBuildUnbuildAssemblyLine.getBlMisc()!=null && invBuildUnbuildAssemblyLine.getBlMisc()!="" && invBuildUnbuildAssemblyLine.getBlMisc().length()!=0){            		
                		int initialQty = Integer.parseInt(this.getQuantityExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc()));
                		String initialPrpgt = this.propagateExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc(), initialQty, "False");            		
                		invCosting.setCstExpiryDate(initialPrpgt);
                	}else{
                		invCosting.setCstExpiryDate(prevExpiryDates);
                	}
                }
            }
            
	           
			// if cost variance is not 0, generate cost variance for the transaction 
			if(CST_VRNC_VL != 0) {
				
				this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
						"INVBUA" + invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDocumentNumber(),
						invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDescription(),
						invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDate(), USR_NM, AD_BRNCH, AD_CMPNY);
				
			}
         
           
            // propagate balance if necessary           
            Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            
            i = invCostings.iterator();
            
            
            String propagateMisc ="";
            String ret = "";
            
            String miscList = "";
            ArrayList miscList2 =null;
            
            System.out.println("miscList Propagate:" + miscList);
            while (i.hasNext()) {
            	String Checker = "";
            	String Checker2 = "";
            	
                LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
                
                invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ASSMBLY_QTY);
                invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ASSMBLY_CST);
                
                if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiTraceMisc()!=0){
                	if(invBuildUnbuildAssemblyLine.getBlMisc()!=null && invBuildUnbuildAssemblyLine.getBlMisc()!="" && invBuildUnbuildAssemblyLine.getBlMisc().length()!=0){

                    	double qty = Double.parseDouble(this.getQuantityExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc()));
                    	miscList = this.propagateExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc(), qty, "False");
                    	miscList2 = this.expiryDates(invBuildUnbuildAssemblyLine.getBlMisc(), qty);
                    	
                    	 if(invBuildUnbuildAssemblyLine.getBlBuildQuantity()<0){
          	        	   Iterator mi = miscList2.iterator();

          	        	   propagateMisc = invPropagatedCosting.getCstExpiryDate();
          	        	   ret = invPropagatedCosting.getCstExpiryDate();
          	        	   while(mi.hasNext()){
          	        		   String miscStr = (String)mi.next();

          	        		   Integer qTest = this.checkExpiryDates(ret+"fin$");
          	        		   ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

          	        		   // ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
          	        		   System.out.println("ret: " + ret);
          	        		   Iterator m2 = miscList3.iterator();
          	        		   ret = "";
          	        		   String ret2 = "false";
          	        		   int a = 0;
          	        		   while(m2.hasNext()){
          	        			   String miscStr2 = (String)m2.next();

          	        			   if(ret2=="1st"){
          	        				   ret2 = "false";
          	        			   }
          	        			   System.out.println("2 miscStr: "+miscStr);
          	        			   System.out.println("2 miscStr2: "+miscStr2);
          	        			   if(miscStr2.equals(miscStr)){
          	        				   if(a==0){
          	        					   a = 1;
          	        					   ret2 = "1st";
          	        					   Checker2 = "true";
          	        				   }else{
          	        					   a = a+1;
          	        					   ret2 = "true";
          	        				   }
          	        			   }
          	        			   System.out.println("Checker: "+Checker2);
          	        			   if(!miscStr2.equals(miscStr) || a>1){
          	        				   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
          	        					   if (miscStr2!=""){
          	        						   miscStr2 = "$" + miscStr2;
          	        						   ret = ret + miscStr2;
          	        						   ret2 = "false";
          	        					   }
          	        				   }
          	        			   }

          	        		   }
          	        		   if(Checker2!="true"){
          	        			   throw new GlobalExpiryDateNotFoundException(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName());
          	        		   }else{
          	        			   System.out.println("TAE");
          	        		   }

          	        		   ret = ret + "$";
          	        		   qtyPrpgt= qtyPrpgt -1;
          	        	   }
          	           }
                    }
                    
                    System.out.println("invPropagatedCosting.getCstExpiryDate() : " + invPropagatedCosting.getCstExpiryDate());
                    System.out.println("CST_ASSMBLY_QTY: " + CST_ASSMBLY_QTY);
                    
                    if(CST_ASSMBLY_QTY>0){
                    	propagateMisc = miscList+invPropagatedCosting.getCstExpiryDate().substring(1, invPropagatedCosting.getCstExpiryDate().length());
                    }else{

                    	Iterator mi = miscList2.iterator();

                    	propagateMisc = invPropagatedCosting.getCstExpiryDate();
                    	ret = propagateMisc;
                    	while(mi.hasNext()){
                    		String miscStr = (String)mi.next();

                    		Integer qTest = this.checkExpiryDates(ret+"fin$");
                    		ArrayList miscList3 = this.expiryDates("$" + ret, Double.parseDouble(qTest.toString()));

                    		// ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
                    		System.out.println("ret: " + ret);
                    		Iterator m2 = miscList3.iterator();
                    		ret = "";
                    		String ret2 = "false";
                    		int a = 0;
                    		while(m2.hasNext()){
                    			String miscStr2 = (String)m2.next();

                    			if(ret2=="1st"){
                    				ret2 = "false";
                    			}
                    			System.out.println("2 miscStr: "+miscStr);
                    			System.out.println("2 miscStr2: "+miscStr2);
                    			if(miscStr2.equals(miscStr)){
                    				if(a==0){
                    					a = 1;
                    					ret2 = "1st";
                    					Checker = "true";
                    				}else{
                    					a = a+1;
                    					ret2 = "true";
                    				}
                    			}
                    			System.out.println("Checker: "+Checker);
                    			if(!miscStr2.equals(miscStr) || a>1){
                    				if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
                    					if (miscStr2!=""){
                    						miscStr2 = "$" + miscStr2;
                    						ret = ret + miscStr2;
                    						ret2 = "false";
                    					}
                    				}
                    			}

                    		}
                    		ret = ret + "$";
                    		qtyPrpgt= qtyPrpgt -1;
                    	}
                    	propagateMisc = ret;
                    }
    /*
                    if(Checker!="true"){
                    	invPropagatedCosting.setCstExpiryDate(propagateMisc);
                    }else{
                    	throw new GlobalExpiryDateNotFoundException();
                    }

    */
             	  
                    invPropagatedCosting.setCstExpiryDate(propagateMisc);
                }
                
            }                           

            // regenerate cost varaince
            this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

         } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
         	
         	getSessionContext().setRollbackOnly();
         	throw ex;
         	
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
        
        
    }
    
    public static int checkExpiryDates(String misc) throws Exception {

		String separator ="$";

		// Remove first $ character
		misc = misc.substring(1);
		//System.out.println("misc: " + misc);
		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;	
		int numberExpry=0;
		String miscList = new String();
		String miscList2 = "";
		String g= "";
		try{
			while(g!="fin"){
				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				g= misc.substring(start, start + length);
				if(g.length()!=0){
					if(g!=null || g!="" || g!="null"){
						if(g.contains("null")){
							miscList2 = "Error";
						}else{
							miscList = miscList + "$" + g;
							numberExpry++;
						}
					}else{
						miscList2 = "Error";
					}

				}else{
					miscList2 = "Error";
				}
			}	
		}catch(Exception e){

		}

		if(miscList2==""){
			miscList = miscList+"$";
		}else{
			miscList = miscList2;
		}

		return (numberExpry);
	}
    
    public String getQuantityExpiryDates(String qntty){
    	String separator = "$";

    	// Remove first $ character
    	qntty = qntty.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = qntty.indexOf(separator, start);
    	int length = nextIndex - start;	
    	String y;
    	y = (qntty.substring(start, start + length));
    	System.out.println("Y " + y);
    	
    	return y;
    }
    
    private ArrayList expiryDates(String misc, double qty) throws Exception{
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	String separator ="$";
    	

    	// Remove first $ character
    	misc = misc.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	

    	System.out.println("qty" + qty);
    	ArrayList miscList = new ArrayList();
		
    	for(int x=0; x<qty; x++) {

    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
      	
    		String checker = misc.substring(start, start + length);
    		if(checker.length()!=0 || checker!="null"){
    			miscList.add(checker);
    		}else{
    			miscList.add("null");
    			qty++;
    		}
    	}	
		
		System.out.println("miscList :" + miscList);
		return miscList;
    }
    
    public String propagateExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	
    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}
 
    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	
    	
    	String miscList = new String();
		
		for(int x=0; x<qty; x++) {
			
			// Date
			start = nextIndex + 1;
			nextIndex = misc.indexOf(separator, start);
			length = nextIndex - start;
			String g= misc.substring(start, start + length);
			System.out.println("g: " + g);
			System.out.println("g length: " + g.length());
				if(g.length()!=0){
					miscList = miscList + "$" + g;	
					System.out.println("miscList G: " + miscList);
				}
		}	
		
		miscList = miscList+"$";
		System.out.println("miscList :" + miscList);
		return (miscList);
    }
   
    
    private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
            Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean convertForeignToFunctionalCurrency");
        
        
        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        LocalAdCompany adCompany = null;
        
        // Initialize EJB Homes
        
        try {
            
            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        // get company and extended precision
        
        try {
            
            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }	     
        
        
        // Convert to functional currency if necessary
        
        if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
            
            AMOUNT = AMOUNT / CONVERSION_RATE;
            
        }
        return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        
    }
    
    private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
            LocalGlChartOfAccount glChartOfAccount, 
            boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean postToGl");
        
        LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        
        // Initialize EJB Home
        
        try {
            
            glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
            
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {          
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
            
            LocalGlChartOfAccountBalance glChartOfAccountBalance = 
                glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
                        glAccountingCalendarValue.getAcvCode(),
                        glChartOfAccount.getCoaCode(), AD_CMPNY);
            
            String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
            short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
            
            
            
            if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
                    isDebit == EJBCommon.TRUE) ||
                    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
                            isDebit == EJBCommon.FALSE)) {				    
                
                glChartOfAccountBalance.setCoabEndingBalance(
                        EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
                
                if (!isCurrentAcv) {
                    
                    glChartOfAccountBalance.setCoabBeginningBalance(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
                    
                }
                
                
            } else {
                
                glChartOfAccountBalance.setCoabEndingBalance(
                        EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
                
                if (!isCurrentAcv) {
                    
                    glChartOfAccountBalance.setCoabBeginningBalance(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
                    
                }
                
            }
            
            if (isCurrentAcv) { 
                
                if (isDebit == EJBCommon.TRUE) {
                    
                    glChartOfAccountBalance.setCoabTotalDebit(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
                    
                } else {
                    
                    glChartOfAccountBalance.setCoabTotalCredit(
                            EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
                }       	   
                
            }
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
        
    }
    
    private void regenerateInventoryDr(LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly, Integer AD_BRNCH, Integer AD_CMPNY) throws GlobalInventoryDateException,
    GlobalInvItemLocationNotFoundException, 
    GlobalInvCSTRemainingQuantityIsLessThanZeroException,
    GlobalBranchAccountNumberInvalidException {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean regenerateInventoryDr");		        
        
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;   
        LocalAdPreferenceHome adPreferenceHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class); 
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class); 
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);    
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            // regenerate inventory distribution records
            
            // remove all inventory distribution
            
            Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByBuaCode(
                    invBuildUnbuildAssembly.getBuaCode(), AD_CMPNY);
            
            Iterator i = invDistributionRecords.iterator();
            
            while (i.hasNext()) {
                
                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
                
                if(invDistributionRecord.getDrClass().equals("INVENTORY")){
                    
                    i.remove();
                    invDistributionRecord.remove();
                    
                }
                
            }
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            // remove all build unbuild assembly lines committed qty
            
            Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
            
            i = invBuildUnbuildAssemblyLines.iterator();
            
            while (i.hasNext()) {
                
                LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)i.next();
                
                // build qty conversion
                double buildQuantity = this.convertByUomFromAndItemAndQuantity(
                		invBuildUnbuildAssemblyLine.getInvUnitOfMeasure(), 
                		invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem(),
                		Math.abs(invBuildUnbuildAssemblyLine.getBlBuildQuantity()), AD_CMPNY);
                
                if (invBuildUnbuildAssemblyLine.getBlBuildQuantity() < 0) {
                    
                	invBuildUnbuildAssemblyLine.getInvItemLocation().setIlCommittedQuantity(invBuildUnbuildAssemblyLine.getInvItemLocation().getIlCommittedQuantity() - buildQuantity);
                    
                } else {
                    
                    Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                    
                    Iterator j = invBillOfMaterials.iterator();
                    
                    while (j.hasNext()) {
                        
                        LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
                        
                        LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
                        
                        // bom conversion
                        
                        double specificGravity = invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity();
                        double standardFillSize = invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize();
                        
                        double convertedQuantity = 
                        		invItemLocation.getInvItem().getIiAdLvCategory().contains("RAWMAT") && !invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiAdLvCategory().contains("Bulk") ? 
                                buildQuantity * (invBillOfMaterial.getBomQuantityNeeded() / 100) * (standardFillSize/1000) * specificGravity
                                :
	                        	this.convertByUomFromAndItemAndQuantity(
	                                    invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
	                                    EJBCommon.roundIt( buildQuantity * (invBillOfMaterial.getBomQuantityNeeded() / 100),
	                                    this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY)
	                        
	                        ;
                        invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);
                        
                    }
                    
                }
                
            }
            
            // add inventory distribution
            
            invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
            
            if(invBuildUnbuildAssemblyLines != null && !invBuildUnbuildAssemblyLines.isEmpty()) {
                
                i = invBuildUnbuildAssemblyLines.iterator();
                
                while(i.hasNext()) {
                    
                    LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)i.next();
                    
                    LocalInvItemLocation invItemLocation = invBuildUnbuildAssemblyLine.getInvItemLocation();
                    
                    
                    
                    // start date validation
                    if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                    	Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                    			invBuildUnbuildAssembly.getBuaDate(), invItemLocation.getInvItem().getIiName(),
                    			invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                    	if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
                    }
                    // check for branch mapping
                    
                    LocalAdBranchItemLocation adBranchItemLocation = null;
                    
                    try {
                        
                        adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                invBuildUnbuildAssemblyLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                    }
                    
                    LocalGlChartOfAccount glInventoryChartOfAccount = null;
                    
                    if (adBranchItemLocation == null){
                        
                        glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                                invBuildUnbuildAssemblyLine.getInvItemLocation().getIlGlCoaInventoryAccount());
                        
                        
                    } else {
                        
                        glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                                adBranchItemLocation.getBilCoaGlInventoryAccount());
                        
                    }
                    
                    byte DEBIT = 0;
                    double TOTAL_AMOUNT = 0d;
                    
                    // build qty conversion
                    double buildQuantity = this.convertByUomFromAndItemAndQuantity(
                    		invBuildUnbuildAssemblyLine.getInvUnitOfMeasure(), 
                    		invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem(),
                    		invBuildUnbuildAssemblyLine.getBlBuildQuantity(), AD_CMPNY);
                    
                    if (invBuildUnbuildAssemblyLine.getBlBuildQuantity() > 0) {
                        System.out.println("invBuildUnbuildAssemblyLine.getBlBuildQuantity() > 0----------->");
                        // build assembly
                        
                        DEBIT = EJBCommon.TRUE;
                        
                        Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                        
                        Iterator j = invBillOfMaterials.iterator();
                        
                        while (j.hasNext()) {
                            
                            // bill of materials
                            
                            LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
                            
                            // get raw material
                            LocalInvItemLocation invIlRawMaterial = null;
                            
                            try {
                                
                                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                                        invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(),AD_CMPNY);
                                
                            } catch (FinderException ex) {
                                
                                throw new GlobalInvItemLocationNotFoundException(
                                        String.valueOf(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName()) +
                                        " - Raw Mat. (" + invBillOfMaterial.getBomIiName() + ")");
                                
                            }

                            
                            Double specificGravity = invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity();
                            Double standardFillSize = invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize();
                            
                            // bom conversion
                            double convertedQuantity = 
                            		invIlRawMaterial.getInvItem().getIiAdLvCategory().contains("RAWMAT") && !invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiAdLvCategory().contains("Bulk") ?
                                            buildQuantity * (invBillOfMaterial.getBomQuantityNeeded()/ 100) * (standardFillSize/1000) * specificGravity
                                    :
                                    	this.convertByUomFromAndItemAndQuantity(
                                                invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                                                EJBCommon.roundIt( buildQuantity * (invBillOfMaterial.getBomQuantityNeeded() / 100),
                                                this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY)
                                    
                                    ;
                            // start date validation
                            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                            	Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                            			invBuildUnbuildAssembly.getBuaDate(), invIlRawMaterial.getInvItem().getIiName(),
                            			invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                            	if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
                            			invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
                            }
                            // add bill of material quantity needed to item location committed quantity
                            invIlRawMaterial.setIlCommittedQuantity(invIlRawMaterial.getIlCommittedQuantity() + convertedQuantity);
                            
                            // check for branch mapping
                            
                            LocalAdBranchItemLocation adBranchIlRawMaterial = null;
                            
                            try{
                                
                                adBranchIlRawMaterial = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                        invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);
                                
                            } catch (FinderException ex) {
                                
                            }
                            
                            LocalGlChartOfAccount glCoaRawMaterial = null;
                            
                            if (adBranchIlRawMaterial == null) {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        invIlRawMaterial.getIlGlCoaInventoryAccount());
                                
                            } else {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        adBranchIlRawMaterial.getBilCoaGlInventoryAccount());
                                
                            }
                            
                            LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);												

                            double COST = 0d;
                            
                            try {
                            								 
                                LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                        invBuildUnbuildAssembly.getBuaDate(), invItem.getIiName(), 
                                        invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);
                                
                                COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
                                
                            } catch (FinderException ex) {
                                
                                COST = invItem.getIiUnitCost();
                                
                            }
                            
                            
                            double BOM_AMOUNT = EJBCommon.roundIt(convertedQuantity * COST, this.getGlFcPrecisionUnit(AD_CMPNY));

                            
                            TOTAL_AMOUNT += BOM_AMOUNT;
                            //ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);
                            
                            this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE,
                                    Math.abs(BOM_AMOUNT), glCoaRawMaterial.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
                            
                            
                            System.out.println(invIlRawMaterial.getInvItem().getIiName()+" - "+invIlRawMaterial.getInvItem().getIiAdLvCategory() + " - " + convertedQuantity);
                            System.out.println("BOM_AMOUNT-------->"+Math.abs(BOM_AMOUNT));

                        }
                        
                    } else {
                        
                        // unbuild assembly
                        
                        try {
                            
                            LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(
                                    invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName(),
                                    invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName(),
                                    AD_BRNCH, AD_CMPNY);
                            
                            if ((invCosting.getCstRemainingQuantity()- buildQuantity) < 0) {
                                
                                throw new GlobalInvCSTRemainingQuantityIsLessThanZeroException(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName());
                                
                            }
                            
                        } catch (FinderException ex) {
                            
                        }
                        
                        invItemLocation = invBuildUnbuildAssemblyLine.getInvItemLocation();
                        
                        // add build quantity to item location committed quantity
                        invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + Math.abs(buildQuantity));
                        
                        DEBIT = EJBCommon.FALSE;
                        
                        Collection invBillOfMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
                        
                        Iterator j = invBillOfMaterials.iterator();
                        
                        while (j.hasNext()) {
                            
                            // bill of materials
                            
                            LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();
                            
                            // get raw material
                            LocalInvItemLocation invIlRawMaterial = null;
                            
                            try {
                                
                                invIlRawMaterial = invItemLocationHome.findByLocNameAndIiName(
                                        invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);
                                
                            } catch(FinderException ex) {
                                
                                throw new GlobalInvItemLocationNotFoundException(String.valueOf(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName()) +
                                        " - Raw Mat. (" +invBillOfMaterial.getBomIiName() + ")");
                                
                            }

                            Double specificGravity = invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity();
                            Double standardFillSize = invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize();
                            
                            // bom conversion
                            double convertedQuantity = 
                            		invIlRawMaterial.getInvItem().getIiAdLvCategory().contains("RAWMAT") && !invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiAdLvCategory().contains("Bulk") ? 
                                            buildQuantity * (invBillOfMaterial.getBomQuantityNeeded()/ 100) * (standardFillSize/1000) * specificGravity
                                    :
                                    	this.convertByUomFromAndItemAndQuantity(
                                                invBillOfMaterial.getInvUnitOfMeasure(), invIlRawMaterial.getInvItem(),
                                                EJBCommon.roundIt(buildQuantity * (invBillOfMaterial.getBomQuantityNeeded() / 100),
                                                this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);
                                    
                            // start date validation
                            if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
                            	Collection invIlRawMatNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                            			invBuildUnbuildAssembly.getBuaDate(), invIlRawMaterial.getInvItem().getIiName(),
                            			invIlRawMaterial.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                            	if(!invIlRawMatNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(
                            			invItemLocation.getInvItem().getIiName() + " - Raw Mat. (" + invIlRawMaterial.getInvItem().getIiName() + ")");
                            }
                            // check for branch mapping
                            
                            LocalAdBranchItemLocation adBranchIlRawMaterial =  null;
                            
                            try{
                                
                                adBranchIlRawMaterial = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                        invIlRawMaterial.getIlCode(), AD_BRNCH, AD_CMPNY);
                                
                            } catch (FinderException ex) {
                                
                            }
                            
                            LocalGlChartOfAccount glCoaRawMaterial = null;
                            
                            if (adBranchIlRawMaterial == null) {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        invIlRawMaterial.getIlGlCoaInventoryAccount());
                                
                            } else {
                                
                                glCoaRawMaterial = glChartOfAccountHome.findByPrimaryKey(
                                        adBranchIlRawMaterial.getBilCoaGlInventoryAccount());
                                
                            }
                            
                            LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);												
                            
                            double COST = 0d;
                            
                            try {
                                
                                LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                        invBuildUnbuildAssembly.getBuaDate(), invItem.getIiName(), 
                                        invBillOfMaterial.getBomLocName(), AD_BRNCH, AD_CMPNY);
                                
                                COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
                                
                            } catch (FinderException ex) {
                                
                                COST = invItem.getIiUnitCost();
                                
                            }
                            
                            double BOM_AMOUNT = EJBCommon.roundIt(convertedQuantity * COST, this.getGlFcPrecisionUnit(AD_CMPNY));
                            
                            
                            TOTAL_AMOUNT += BOM_AMOUNT;
                            //ABS_TOTAL_AMOUNT += Math.abs(BOM_AMOUNT);
                            
                            this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE,
                                    Math.abs(BOM_AMOUNT), glCoaRawMaterial.getCoaCode(), invBuildUnbuildAssembly,
                                    AD_BRNCH, AD_CMPNY);
                            
                        }
                        
                    }
                    
                     double LABORCOST =0;
                    double POWERCOST = 0d;
                    double OVERHEADCOST =0d;
                    double FREIGHTCOST = 0d;
                    double FIXEDCOST = 0d;

                    
                    
                    if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaLaborCostAccount()!=null) {
                    	LABORCOST = EJBCommon.roundIt(buildQuantity * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiLaborCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                    	LocalGlChartOfAccount glCoaLaborCost = glChartOfAccountHome.findByCoaCode(
                		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaLaborCostAccount()), AD_CMPNY);
                		
                		this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(LABORCOST),
                		glCoaLaborCost.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
                    }
                    if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaPowerCostAccount()!=null) {
                    	POWERCOST = EJBCommon.roundIt(buildQuantity * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiPowerCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                     	LocalGlChartOfAccount glCoaPowerCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaPowerCostAccount()), AD_CMPNY);
                    	this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(POWERCOST),
                    		glCoaPowerCost.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
                    }

					if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaOverHeadCostAccount()!=null) {
						OVERHEADCOST = EJBCommon.roundIt(buildQuantity * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiOverHeadCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                   		LocalGlChartOfAccount glCoaOverHeadCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaOverHeadCostAccount()), AD_CMPNY);
						this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(OVERHEADCOST),
                    		glCoaOverHeadCost.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
					}
					if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFreightCostAccount()!=null) {
					    FREIGHTCOST = EJBCommon.roundIt(buildQuantity * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiFreightCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
                    	LocalGlChartOfAccount glCoaFreightCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFreightCostAccount()), AD_CMPNY);	
						this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(FREIGHTCOST),
                    		glCoaFreightCost.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
					}
					if(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFixedCostAccount()!=null) {
						 FIXEDCOST = EJBCommon.roundIt(buildQuantity * invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiFixedCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
						  LocalGlChartOfAccount glCoaFixedCost = glChartOfAccountHome.findByCoaCode(
                    		Integer.parseInt(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getGlCoaFixedCostAccount()), AD_CMPNY);
                         this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, Math.abs(FIXEDCOST),
                    		glCoaFixedCost.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);
                    
                    
					}
				
                    
                    
                    
                    double TOTAL_CREDIT = TOTAL_AMOUNT+LABORCOST+POWERCOST+OVERHEADCOST+FREIGHTCOST+FIXEDCOST;
                    this.addInvDrEntry(invBuildUnbuildAssembly.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(TOTAL_CREDIT),
                            glInventoryChartOfAccount.getCoaCode(), invBuildUnbuildAssembly, AD_BRNCH, AD_CMPNY);

                  System.out.println("TOTAL CREDIT="+TOTAL_CREDIT);
                }
                
            }
            
        } catch (GlobalInventoryDateException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInvItemLocationNotFoundException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInvCSTRemainingQuantityIsLessThanZeroException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalBranchAccountNumberInvalidException ex) {
            
            throw new GlobalBranchAccountNumberInvalidException ();    		
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ArReceiptPostControllerBean voidInvAdjustment");
    	
    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();
    			
    			Iterator i = invDistributionRecords.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    				
    				list.add(invDistributionRecord);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();    	    			    			
    				
    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(), 
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    			}	    
    			
    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
    				
    				list.add(invAdjustmentLine);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();    	    			    			
    				
    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}
    			
    			invAdjustment.setAdjVoid(EJBCommon.TRUE);
    			
    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArReceiptPostControllerBean generateCostVariance");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try{ 
    		
    		
    		
    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;
    		
    		
    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    		
    		try{
    			
    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			//glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			newInvAdjustment.setGlChartOfAccount(glCoaVarianceAccount);
    			
    		} catch (FinderException ex) {
    			
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    			
    		}
    		
    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment,
    				CST_VRNC_VL, EJBCommon.FALSE, AD_CMPNY);
    		
    		// check for branch mapping
    		
    		LocalAdBranchItemLocation adBranchItemLocation = null;
    		
    		try{
    			
    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		LocalGlChartOfAccount glInventoryChartOfAccount = null;
    		
    		if (adBranchItemLocation == null) {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());
    			
    		}
    		
    		
    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;
    		
    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", 
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("ArReceiptPostControllerBean regenerateCostVariance");
    	
    	try {
    		
    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			if(prevInvCosting.getCstRemainingQuantity() < 0) {
    				
    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";
    				Debug.print("ArReceiptPostControllerBean regenerateCostVariance A");
    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
						Debug.print("ArReceiptPostControllerBean regenerateCostVariance B");
    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
    					
    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
						Debug.print("ArReceiptPostControllerBean regenerateCostVariance C");
    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
    						
    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
    						
    					}
    					
    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
   						Debug.print("ArReceiptPostControllerBean regenerateCostVariance D");
   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
   	   						
   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
   	    					
   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}
   	   						
    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);
    					Debug.print("ArReceiptPostControllerBean regenerateCostVariance E");
    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" + 
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
    					
    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
    					
    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
    						
    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() * 
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
    						Debug.print("ArReceiptPostControllerBean regenerateCostVariance F");
    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();
    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
							Debug.print("ArReceiptPostControllerBean regenerateCostVariance G");
    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
							Debug.print("ArReceiptPostControllerBean regenerateCostVariance H");
    					}
    					
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" + 
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();
    					
    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" + 
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
						Debug.print("ArReceiptPostControllerBean regenerateCostVariance I");
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
						Debug.print("ArReceiptPostControllerBean regenerateCostVariance J");
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
    					
    				} else {
    					
    					prevInvCosting = invPropagatedCosting;
    					continue;
    					
    				}
    				
    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;
    				
    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());
    				Debug.print("ArReceiptPostControllerBean regenerateCostVariance K");
    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
    				Debug.print("ArReceiptPostControllerBean regenerateCostVariance L");
    			}
    			
    			// set previous costing
    			prevInvCosting = invPropagatedCosting;
    			
    		}
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}      */  	
    }
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE, 
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("ArReceiptPostControllerBean addInvDrEntry");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;           
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {        
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    			
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
    		
    		// create distribution record        
    		
    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					AD_CMPNY);
    		
    		//invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvAdjustment(invAdjustment);
    		//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
    		invDistributionRecord.setInvChartOfAccount(glChartOfAccount);
    		
    	} catch(GlobalBranchAccountNumberInvalidException ex) {
    		
    		throw new GlobalBranchAccountNumberInvalidException ();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,		
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvBuildUnbuildAssemblyPostControllerBean executeInvAdjPost");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;        
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try {
    		
    		// validate if adjustment is already deleted
    		
    		LocalInvAdjustment invAdjustment = null;
    		
    		try {
    			
    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlobalRecordAlreadyDeletedException();
    			
    		}
    		
    		// validate if adjustment is already posted or void
    		
    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();
    			
    		}
    		
    		Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    		
    		Iterator i = invAdjustmentLines.iterator();
    		
    		while(i.hasNext()) {
    			
    			
    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    			
				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);
    			
    		}			
    		
    		// post to gl if necessary
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    			// validate if date has no period and period is closed
    			
    			LocalGlSetOfBook glJournalSetOfBook = null;
    			
    			try {
    				
    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    				throw new GlJREffectiveDateNoPeriodExistException();
    				
    			}
    			
    			LocalGlAccountingCalendarValue glAccountingCalendarValue = 
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);
    			
    			
    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
    				
    				throw new GlJREffectiveDatePeriodClosedException();
    				
    			}
    			
    			// check if invoice is balance if not check suspense posting
    			
    			LocalGlJournalLine glOffsetJournalLine = null;
    			
    			Collection invDistributionRecords = null;
    			
    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			} else {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			}

    			
    			Iterator j = invDistributionRecords.iterator();
    			
    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    					
    					TOTAL_DEBIT += DR_AMNT;
    					
    				} else {
    					
    					TOTAL_CREDIT += DR_AMNT;
    					
    				}
    				
    			}
    			
    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			
    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				LocalGlSuspenseAccount glSuspenseAccount = null;
    				
    				try { 	
    					
    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);
    					
    				} catch (FinderException ex) {
    					
    					throw new GlobalJournalNotBalanceException();
    					
    				}
    				
    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE, 
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
    					
    				} else {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
    					
    				}
    				
    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				//glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlChartOfAccount(glChartOfAccount);
    				
    				
    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				throw new GlobalJournalNotBalanceException();		    	
    				
    			}
    			
    			// create journal batch if necessary
    			
    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
    			
    			try {
    				
    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			if (glJournalBatch == null) {
    				
    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// create journal entry			            	
    			
    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null,

						AD_BRNCH, AD_CMPNY);
    			
    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);
    			
    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
    			
    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ASSEMBLIES", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);
    			
    			if (glJournalBatch != null) {

    				glJournal.setGlJournalBatch(glJournalBatch);
    				
    			}           		    
    			
    			// create journal lines
    			
    			j = invDistributionRecords.iterator();
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
    				
    				//invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				glJournalLine.setGlChartOfAccount(invDistributionRecord.getInvChartOfAccount());
    				
    				//glJournal.addGlJournalLine(glJournalLine);
    				glJournalLine.setGlJournal(glJournal);
    				
    				invDistributionRecord.setDrImported(EJBCommon.TRUE);
    				
    				
    			}
    			
    			if (glOffsetJournalLine != null) {
    				
    				//glJournal.addGlJournalLine(glOffsetJournalLine);
    				glOffsetJournalLine.setGlJournal(glJournal);
    				
    			}		
    			
    			// post journal to gl
    			
    			Collection glJournalLines = glJournal.getGlJournalLines();
    			
    			i = glJournalLines.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
    				
    				// post current to current acv
    				
    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    				
    				
    				// post to subsequent acvs (propagate)
    				
    				Collection glSubsequentAccountingCalendarValues = 
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
    				
    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
    				
    				while (acvsIter.hasNext()) {
    					
    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    						(LocalGlAccountingCalendarValue)acvsIter.next();
    					
    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    					
    				}
    				
    				// post to subsequent years if necessary
    				
    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
    				
    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
    					
    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY); 	  	
    					
    					Iterator sobIter = glSubsequentSetOfBooks.iterator();
    					
    					while (sobIter.hasNext()) {
    						
    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
    						
    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
    						
    						// post to subsequent acvs of subsequent set of book(propagate)
    						
    						Collection glAccountingCalendarValues = 
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
    						
    						Iterator acvIter = glAccountingCalendarValues.iterator();
    						
    						while (acvIter.hasNext()) {
    							
    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    								(LocalGlAccountingCalendarValue)acvIter.next();
    							
    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    								
    							} else { // revenue & expense
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
    								
    							}
    							
    						}
    						
    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
    						
    					}
    					
    				}
    				
    			}
			
			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlJREffectiveDatePeriodClosedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalJournalNotBalanceException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalRecordAlreadyDeletedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalTransactionAlreadyPostedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {
    	
    	Debug.print("ArReceiptPostControllerBean addInvAlEntry");
    	
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {
    		
    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL,null,null, 0,0, AL_VD, AD_CMPNY);
    	
    		// map adjustment, unit of measure, item location
    		//invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvAdjustment(invAdjustment);
    		//invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvUnitOfMeasure(invItemLocation.getInvItem().getInvUnitOfMeasure());
    		//invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		invAdjustmentLine.setInvItemLocation(invItemLocation);
    		
    		return invAdjustmentLine;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    } 
    
    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("ArReceiptPostControllerBean saveInvAdjustment");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home    	
    	
    	try{
    	
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try{
    		
    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;
    		
    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
    		
    		try {
    			
    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		try {
    			
    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		while (true) {
    			
    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);		            		
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					break;
    					
    				}
    				
    			} else {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
    					break;
    					
    				}
    				
    			}
    			
    		}		            
    		
    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
    		
    		return invAdjustment;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	
    }
    
    
private double getConversionFactorByUomFromAndItem(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, Integer AD_CMPNY) {
		
		Debug.print("ApReceivingItemEntryControllerBean getConversionFactorByUomFromAndItem");		        
        
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
               
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
        	return EJBCommon.roundIt(invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
        	
        	       	        		        		       	
        						    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}
    
    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {
    	
    	Debug.print("ArReceiptPostControllerBean postInvAdjustmentToInventory");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
    		
    	} catch (NamingException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;
    		
    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		
    		if (CST_ADJST_QTY < 0) {
    			
    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
    			
    		}
    		
    		// create costing
    		
    		try {
    			
    			// generate line number
    			
    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
    			
    		} catch (FinderException ex) {
    			
    			CST_LN_NMBR = 1;
    			
    		}
    		
    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		//invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvItemLocation(invItemLocation);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);
    		
    		// propagate balance if necessary           
    		
    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    		
    		Iterator i = invCostings.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
    			
    		}                           
    		
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	
    }

    // SessionBean methods
    
    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean ejbCreate");
        
    }
    
}