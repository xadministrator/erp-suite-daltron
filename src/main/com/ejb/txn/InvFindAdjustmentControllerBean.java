
/*
 * InvFindAdjustmentControllerBean.java
 *
 * Created on June 28, 2004, 09:25 AM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModAdjustmentDetails;

/**
 * @ejb:bean name="InvFindAdjustmentControllerEJB"
 *           display-name="Used for finding adjustment"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvFindAdjustmentControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindAdjustmentController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindAdjustmentControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvFindAdjustmentControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvAdjByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindAdjustmentControllerBean getInvAdjByCriteria");
        Debug.print("Offset " + OFFSET + " Limit " + LIMIT);
        LocalInvAdjustmentHome invAdjustmentHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(adj) FROM InvAdjustment adj ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size() + 2;	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      if (criteria.containsKey("approvalStatus")) {
	      	
	      	 String approvalStatus = (String)criteria.get("approvalStatus");
	      	
	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
	      	 	
	      	 	 criteriaSize--;
	      	 	
	      	 }
	      	
	      }
	      	
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("referenceNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("adj.adjReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

		  }
		  
		  if (criteria.containsKey("documentNumberFrom")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjDocumentNumber>=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("documentNumberFrom");
	       	  ctr++;
	       	  
	      }
		  
		  if (criteria.containsKey("documentNumberTo")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjDocumentNumber<=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("documentNumberTo");
	       	  ctr++;
	       	  
	      }
		  
		  if (criteria.containsKey("type")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjType=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("type");
	       	  ctr++;
	       	  
	      }
		  
		  if (criteria.containsKey("generated")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjGenerated=?" + (ctr+1) + " ");
	       	  obj[ctr] = (Byte)criteria.get("generated");
	       	  Debug.print(obj[ctr].toString());
	       	  
	       	  ctr++;
	       	  
	      }
	      
	      
	       if (criteria.containsKey("void")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjVoid=?" + (ctr+1) + " ");
	       	  obj[ctr] = (Byte)criteria.get("void");
	       	  Debug.print(obj[ctr].toString());
	       	  
	       	  ctr++;
	       	  
	      }

	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("adj.adjDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("adj.adjDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		  
	      if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
	       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("adj.adjApprovalStatus IS NULL ");
	       	  	
	       	  } else {
	      	  	
		      	  jbossQl.append("adj.adjApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
	       	  
	      }
	      	      
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjPosted=?" + (ctr+1) + " ");
	       	  
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }	      
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("adj.adjAdBranch=" + AD_BRNCH + " AND adj.adjAdCompany=" + AD_CMPNY + " ");
      			
		  String orderBy = null;
		  
		  if (ORDER_BY.equals("REFERENCE NUMBER")) {	          
		      	      		
		  	  orderBy = "adj.adjReferenceNumber";

		  } else if (ORDER_BY.equals("TYPE")) {	          
	      		
		  	  orderBy = "adj.adjType";
		  	  
		  } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
      		
	  	  orderBy = "adj.adjDocumentNumber";	  

		  }
		  
	  	  if (orderBy != null) {
		
		  	  jbossQl.append("ORDER BY " + orderBy + ", adj.adjDate");

		  } else {
		  	
		  	  jbossQl.append("ORDER BY adj.adjDate");
		  	
		  }
		  
		  jbossQl.append(" OFFSET ?" + (ctr + 1));	        
		  obj[ctr] = OFFSET;	      
		  ctr++;
		  
		  jbossQl.append(" LIMIT ?" + (ctr + 1));
		  obj[ctr] = LIMIT;
		  ctr++;		
		  
		  System.out.println(jbossQl.toString());
			  	      	
	      Collection invAdjustments = invAdjustmentHome.getAdjByCriteria(jbossQl.toString(), obj);	         
		  
		  if (invAdjustments.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = invAdjustments.iterator();
		
		  while (i.hasNext()) {
		  	
		  	LocalInvAdjustment invAdjustment = (LocalInvAdjustment)i.next(); 
		  	
		  	InvModAdjustmentDetails mdetails = new InvModAdjustmentDetails();
		  	mdetails.setAdjCode(invAdjustment.getAdjCode());
		  	mdetails.setAdjDate(invAdjustment.getAdjDate());
		  	mdetails.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
		  	mdetails.setAdjReferenceNumber(invAdjustment.getAdjReferenceNumber());
		  	mdetails.setAdjType(invAdjustment.getAdjType());
		  	try {
		  		mdetails.setAdjCoaAccountNumber(invAdjustment.getGlChartOfAccount().getCoaAccountNumber());
		  		mdetails.setAdjCoaAccountDescription(invAdjustment.getGlChartOfAccount().getCoaAccountDescription());		  	  
			  	
		  	} catch (Exception e) {
		  		mdetails.setAdjCoaAccountNumber("");
		  		mdetails.setAdjCoaAccountDescription("");
			}
		  	
		  	
		  	list.add(mdetails);
		  	
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getInvAdjSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindAdjustmentControllerBean getInvAdjSizeByCriteria");
        
        LocalInvAdjustmentHome invAdjustmentHome = null;

        //initialized EJB Home
        
        try {
            
        	invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(adj) FROM InvAdjustment adj ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      if (criteria.containsKey("approvalStatus")) {
	      	
	      	 String approvalStatus = (String)criteria.get("approvalStatus");
	      	
	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
	      	 	
	      	 	 criteriaSize--;
	      	 	
	      	 }
	      	
	      }
	      	
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("referenceNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("adj.adjReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

		  }
		  
		  if (criteria.containsKey("documentNumberFrom")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjDocumentNumber>=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("documentNumberFrom");
	       	  ctr++;
	       	  
	      }
		  
		  if (criteria.containsKey("documentNumberTo")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjDocumentNumber<=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("documentNumberTo");
	       	  ctr++;
	       	  
	      }
		  
		  if (criteria.containsKey("type")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjType=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("type");
	       	  ctr++;
	       	  
	      }
		  
		  if (criteria.containsKey("generated")) {
		       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjGenerated=?" + (ctr+1) + " ");
	       	  obj[ctr] = (Byte)criteria.get("generated");
	       	  ctr++;
	       	  
	      }
	      
	          if (criteria.containsKey("void")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjVoid=?" + (ctr+1) + " ");
	       	  obj[ctr] = (Byte)criteria.get("void");
	       	  Debug.print(obj[ctr].toString());
	       	  
	       	  ctr++;
	       	  
	      }

	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("adj.adjDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("adj.adjDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		  
	      if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("adj.adjApprovalStatus IS NULL ");
	       	  	
	       	  } else {
	      	  	
		      	  jbossQl.append("adj.adjApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
	       	  
	      }
	      	      
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("adj.adjPosted=?" + (ctr+1) + " "); 
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }	      
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
	      jbossQl.append("adj.adjAdBranch=" + AD_BRNCH + " AND adj.adjAdCompany=" + AD_CMPNY + " ");      	
	      Collection invAdjustments = invAdjustmentHome.getAdjByCriteria(jbossQl.toString(), obj);	         
		  
		  if (invAdjustments.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  return new Integer(invAdjustments.size());
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvFindAdjustmentControllerBean ejbCreate");
      
    }
}
