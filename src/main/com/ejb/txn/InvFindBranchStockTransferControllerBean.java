package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.ejb.inv.LocalInvBranchStockTransferHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModBranchStockTransferDetails;

/**
 * @ejb:bean name="InvFindBranchStockTransferControllerEJB"
 *           display-name="Used for finding branch stock transfer"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvFindBranchStockTransferControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindBranchStockTransferController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindBranchStockTransferControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvFindBranchStockTransferControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvBstByCriteria(HashMap criteria,
			Integer OFFSET, Integer LIMIT, String ORDER_BY, boolean isBstLookup, Integer AD_BRNCH, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvFindBranchStockTransferControllerBean getInvBstByCriteria");
		
		LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(bst) FROM InvBranchStockTransfer bst ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size() + 2;	      
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			String type = new String();
			String statusType = new String();

			if (criteria.containsKey("statusType"))  {
				
				statusType = (String)criteria.get("statusType");
			
			}
			
			if (criteria.containsKey("type")) {
				
				type = (String)criteria.get("type");
				
			}
			
			if (criteria.containsKey("transferOutNumber")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("transferOrderNumber")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("statusType")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("approvalStatus")) {
				
				String approvalStatus = (String)criteria.get("approvalStatus");
				
				if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
					
					criteriaSize--;
					
				}
				
			}
			
			if (!criteria.containsKey("posted") &&
					(type != null) && type.equals("OUT") && statusType.equalsIgnoreCase("INCOMING")) {
				
				criteriaSize++;
				
			}else if (!criteria.containsKey("posted") &&
					(type != null) && type.equals("ORDER") && statusType.equalsIgnoreCase("INCOMING")) {
				
				criteriaSize++;
				
			}
			
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("type")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}

				jbossQl.append("(bst.bstType=?" + (ctr+1) + " ");
				jbossQl.append(" OR bst.bstType='REGULAR' OR bst.bstType='EMERGENCY') ");
				obj[ctr] = type;				
				ctr++;
				
			}
			
			if (criteria.containsKey("documentNumberFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bst.bstNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberFrom");
				ctr++;
				
			}
			
			if (criteria.containsKey("documentNumberTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bst.bstNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberTo");
				ctr++;
				
			}		  		  
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("bst.bstDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("bst.bstDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}    
			
			if (criteria.containsKey("brVoid")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("bst.bstVoid=?" + (ctr+1) + " ");
				obj[ctr] = ((Byte)criteria.get("brVoid"));
				ctr++;
								
			}  

			if (criteria.containsKey("approvalStatus")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				String approvalStatus = (String)criteria.get("approvalStatus");
				
				if ((type != null) && type.equals("OUT") && statusType.equalsIgnoreCase("INCOMING")) {
					
					if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED") ||
							approvalStatus.equals("PENDING")) {
						
						throw new GlobalNoRecordFoundException();
						
					}
					
				}else if ((type != null) && type.equals("ORDER") && statusType.equalsIgnoreCase("INCOMING")) {
					
					if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED") ||
							approvalStatus.equals("PENDING")) {
						
						throw new GlobalNoRecordFoundException();
						
					}
					
				}
				
				if (approvalStatus.equals("DRAFT")) {
					
					jbossQl.append("bst.bstApprovalStatus IS NULL ");
					
				} else {
					
					jbossQl.append("bst.bstApprovalStatus=?" + (ctr+1) + " ");
					obj[ctr] = approvalStatus;
					ctr++;
					
				}
				
			} else if (!criteria.containsKey("approvalStatus") &&
					(type != null) && type.equals("OUT") && statusType.equalsIgnoreCase("INCOMING")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("(bst.bstApprovalStatus='N/A' OR bst.bstApprovalStatus='APPROVED') AND bst.bstLock=0 ");
				
			}else if (!criteria.containsKey("approvalStatus") &&
					(type != null) && type.equals("ORDER") && statusType.equalsIgnoreCase("INCOMING")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("(bst.bstApprovalStatus='N/A' OR bst.bstApprovalStatus='APPROVED') AND bst.bstLock=0 ");
				
			}
			
			if (criteria.containsKey("posted")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bst.bstPosted=?" + (ctr+1) + " ");
				
				String posted = (String)criteria.get("posted");

				if (posted.equals("YES")) {
					
					obj[ctr] = new Byte(EJBCommon.TRUE);
					
				} else {
					
					obj[ctr] = new Byte(EJBCommon.FALSE);
					
				}       	  
				
				ctr++;
								
			} else if (!criteria.containsKey("posted") &&
					(type != null) && type.equals("OUT") && statusType.equalsIgnoreCase("INCOMING")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bst.bstPosted=?" + (ctr+1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;
								
			}else if (!criteria.containsKey("posted") &&
					(type != null) && type.equals("ORDER") && statusType.equalsIgnoreCase("INCOMING")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bst.bstPosted=?" + (ctr+1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;
								
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			if ((type != null) && (type.equals("IN"))){
				
				if (criteria.containsKey("transferOutNumber")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("bst.bstTransferOutNumber LIKE '%" + (String)criteria.get("transferOutNumber") + "%' ");
					
				}
				
			} 
			if ((type != null) && (type.equals("OUT"))){
				
				if (criteria.containsKey("transferOrderNumber")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("bst.bstTransferOrderNumber LIKE '%" + (String)criteria.get("transferOrderNumber") + "%' ");
					
				}
				
			}

			if ( type.equals("OUT")) {

				if(statusType.equalsIgnoreCase("INCOMING")) {
					
					jbossQl.append("bst.adBranch.brCode=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
					
				} else if (statusType.equalsIgnoreCase("OUTGOING")) {
					
					jbossQl.append("bst.bstAdBranch=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
					
				}
				
			} else if ( type.equals("ORDER")) {
				
				if(statusType.equalsIgnoreCase("INCOMING")) {
					
					jbossQl.append("bst.adBranch.brCode=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
					
				} else {
					
					jbossQl.append("bst.bstAdBranch=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
					
				}
				
				//jbossQl.append("bst.bstAdBranch=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
				
				
			} else {
				
				jbossQl.append("bst.bstAdBranch=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
				
			}
			
			String orderBy = null;
			
			if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
				
				orderBy = "bst.bstNumber";	  
				
			}
			
			if (orderBy != null) {
				
				jbossQl.append("ORDER BY " + orderBy + ", bst.bstDate");
				
			} else {
				
				jbossQl.append("ORDER BY bst.bstDate");
				
			}
			
			if (!isBstLookup) {
				jbossQl.append(" OFFSET ?" + (ctr + 1));	        
				obj[ctr] = OFFSET;	      
				ctr++;
				
				jbossQl.append(" LIMIT ?" + (ctr + 1));
				obj[ctr] = LIMIT;
				ctr++;		
			} else {
				jbossQl.append(" OFFSET ?" + (ctr + 1));	        
				obj[ctr] = 0;	      
				ctr++;
				
				jbossQl.append(" LIMIT ?" + (ctr + 1));
				obj[ctr] = 0;
				ctr++;		
			}
			
			System.out.println(jbossQl.toString());
			
			Collection invBranchStockTransfers = invBranchStockTransferHome.getBstByCriteria(jbossQl.toString(), obj);	         
			
			if (invBranchStockTransfers.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			Iterator i = invBranchStockTransfers.iterator();
			
			while (i.hasNext()) {
				
				LocalInvBranchStockTransfer invBranchStockTransfer = (LocalInvBranchStockTransfer)i.next(); 
				
				System.out.println("isBstLookup="+isBstLookup);
				if (isBstLookup) {
					System.out.println("Find for BS Transfer Out ------------------------>");
					boolean isBstBreak = false;
	        		Collection invBranchStockTransferLines = invBranchStockTransfer.getInvBranchStockTransferLines();
	        		Iterator j = invBranchStockTransferLines.iterator();
	        		while (j.hasNext()) {
	        			
	        			LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)j.next();
	        			Collection invServedBsts = null;
	        			
	        			if (invBranchStockTransfer.getBstType().equals("ORDER") || invBranchStockTransfer.getBstType().equals("REGULAR") || invBranchStockTransfer.getBstType().equals("EMERGENCY")) {
	        				invServedBsts = invBranchStockTransferLineHome.findByBstTransferOrderNumberAndIiNameAndLocNameAndBrCode(
	        						invBranchStockTransfer.getBstNumber(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
	        						invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(), invBranchStockTransfer.getAdBranch().getBrCode(), AD_CMPNY);
	        				System.out.println("1 invServedBsts ORDER");
	        			} else if (invBranchStockTransfer.getBstType().equals("OUT")) {
	        				invServedBsts = invBranchStockTransferLineHome.findByBstTransferOutNumberAndIiNameAndLocNameAndBrCode(
	        						invBranchStockTransfer.getBstNumber(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
	        						invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(), invBranchStockTransfer.getAdBranch().getBrCode(), AD_CMPNY);
	        				System.out.println("2 invServedBsts OUT");
	        			}
	        			
	        			System.out.println("invServedBsts="+invServedBsts.size());
	        			double totalQuantityReceived = 0;
	        			Iterator k = invServedBsts.iterator();
	        			while (k.hasNext()) {
	        				
	        				LocalInvBranchStockTransferLine invServedBst = (LocalInvBranchStockTransferLine)k.next();
	        				totalQuantityReceived += invServedBst.getBslQuantityReceived();
	        				
	        			}
	        			System.out.println("invBranchStockTransferLine.getBslQuantity()="+invBranchStockTransferLine.getBslQuantity());
	        			System.out.println("totalQuantityReceived="+totalQuantityReceived);
	        			if (invServedBsts.size() == 0 || totalQuantityReceived != invBranchStockTransferLine.getBslQuantity()) {
	        				InvModBranchStockTransferDetails mdetails = new InvModBranchStockTransferDetails();
	    					mdetails.setBstCode(invBranchStockTransfer.getBstCode());
	    					mdetails.setBstType(invBranchStockTransfer.getBstType());
	    					mdetails.setBstDate(invBranchStockTransfer.getBstDate());
	    					mdetails.setBstNumber(invBranchStockTransfer.getBstNumber());
	    					mdetails.setBstTransferOutNumber(invBranchStockTransfer.getBstTransferOutNumber());
	    					mdetails.setBstTransferOrderNumber(invBranchStockTransfer.getBstTransferOrderNumber());
	    					
	    					
	    					System.out.println("mdetails.getBstType()="+mdetails.getBstType());
	    					if(mdetails.getBstType().equals("OUT")) {
	    		            	
	    		            	mdetails.setBstBranchFrom(this.getAdBrNameByBrCode(invBranchStockTransfer.getBstAdBranch()));
	    		            	mdetails.setBstBranchTo(invBranchStockTransfer.getAdBranch().getBrName());
	    		            	
	    		            } else {
	    		            	
	    		            	mdetails.setBstBranchFrom(invBranchStockTransfer.getAdBranch().getBrName());
	    		            	mdetails.setBstBranchTo(this.getAdBrNameByBrCode(invBranchStockTransfer.getBstAdBranch()));
	    		            	
	    		            }
	    					
	    					System.out.println("type="+type);
	    					System.out.println("statusType="+statusType);
	    					if(type.equals("IN") || (type.equals("OUT") 
	    						&& mdetails.getBstType().equals("OUT")&&
	    						((!statusType.equalsIgnoreCase("INCOMING") &&
	    						invBranchStockTransfer.getBstAdBranch().equals(AD_BRNCH))|| 
	    						(invBranchStockTransfer.getAdBranch().getBrCode().equals(AD_BRNCH) &&
	    						invBranchStockTransfer.getBstLock() == EJBCommon.FALSE &&
	    						invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE)))) {
	    						System.out.println("---------------------------->1");
	    						list.add(mdetails);
	    					} else if(type.equals("IN") || (type.equals("ORDER") 
	    							&& (mdetails.getBstType().equals("ORDER") || mdetails.getBstType().equals("REGULAR") || mdetails.getBstType().equals("EMERGENCY"))&&
	    							((!statusType.equalsIgnoreCase("INCOMING") &&
	    							invBranchStockTransfer.getBstAdBranch().equals(AD_BRNCH))|| 
	    							(invBranchStockTransfer.getAdBranch().getBrCode().equals(AD_BRNCH) &&
	    							invBranchStockTransfer.getBstLock() == EJBCommon.FALSE &&
	    							invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE)))) {
	    						System.out.println("---------------------------->2");	
	    							list.add(mdetails);
	    					}
	    	        		isBstBreak = true;
	        			}
	        			System.out.println("1--------isBstBreak="+isBstBreak);
	        			if (isBstBreak) break;
	        			
	        		}
	        		System.out.println("2--------isBstBreak="+isBstBreak);
	        		if (isBstBreak) continue;
				
				} else {
					System.out.println("Normal Find ------------------------>");
					InvModBranchStockTransferDetails mdetails = new InvModBranchStockTransferDetails();
					mdetails.setBstCode(invBranchStockTransfer.getBstCode());
					mdetails.setBstType(invBranchStockTransfer.getBstType());
					mdetails.setBstDate(invBranchStockTransfer.getBstDate());
					mdetails.setBstNumber(invBranchStockTransfer.getBstNumber());
					mdetails.setBstTransferOutNumber(invBranchStockTransfer.getBstTransferOutNumber());
					mdetails.setBstTransferOrderNumber(invBranchStockTransfer.getBstTransferOrderNumber());
					
					if(mdetails.getBstType().equals("OUT")) {
						System.out.println("1-------------->"+invBranchStockTransfer.getBstAdBranch());
						System.out.println("2-------------->"+invBranchStockTransfer.getAdBranch().getBrName());
						
		            	mdetails.setBstBranchFrom(this.getAdBrNameByBrCode(invBranchStockTransfer.getBstAdBranch()));
		            	
		            	mdetails.setBstBranchTo(invBranchStockTransfer.getAdBranch().getBrName());
		            	
		            } else {
		            	
		            	mdetails.setBstBranchFrom(invBranchStockTransfer.getAdBranch().getBrName());
		            	mdetails.setBstBranchTo(this.getAdBrNameByBrCode(invBranchStockTransfer.getBstAdBranch()));
		            	
		            }
					if(type.equals("IN") || (type.equals("OUT") && mdetails.getBstType().equals("OUT")&&
						((!statusType.equalsIgnoreCase("INCOMING") && invBranchStockTransfer.getBstAdBranch().equals(AD_BRNCH))|| 
						(invBranchStockTransfer.getAdBranch().getBrCode().equals(AD_BRNCH) &&
						invBranchStockTransfer.getBstLock() == EJBCommon.FALSE &&
						invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE)))) {
						
						list.add(mdetails);
					} else if(type.equals("IN") || (type.equals("ORDER") 
							&& (mdetails.getBstType().equals("ORDER") || mdetails.getBstType().equals("REGULAR") || mdetails.getBstType().equals("EMERGENCY"))&&
							((!statusType.equalsIgnoreCase("INCOMING") &&
							invBranchStockTransfer.getBstAdBranch().equals(AD_BRNCH))|| 
							(invBranchStockTransfer.getAdBranch().getBrCode().equals(AD_BRNCH) &&
							invBranchStockTransfer.getBstLock() == EJBCommon.FALSE &&
							invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE)))) {
							
							list.add(mdetails);
					}
				} 
			}

			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public Integer getInvBstSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvFindBranchStockTransferControllerBean getInvBstSizeByCriteria");
		
		LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(bst) FROM InvBranchStockTransfer bst ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			String type = new String();
			String statusType = new String();

			if (criteria.containsKey("statusType"))  {
				
				statusType = (String)criteria.get("statusType");
			
			}
			
			if (criteria.containsKey("type")) {
				
				type = (String)criteria.get("type");
				
			}
			
			if (criteria.containsKey("transferOutNumber")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("statusType")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("approvalStatus")) {
				
				String approvalStatus = (String)criteria.get("approvalStatus");
				
				if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
					
					criteriaSize--;
					
				}
				
			}
			
			if (!criteria.containsKey("posted") &&
					(type != null) && type.equals("OUT") && statusType.equalsIgnoreCase("INCOMING")) {
				
				criteriaSize++;
				
			}
			if (!criteria.containsKey("posted") &&
					(type != null) && type.equals("ORDER") && statusType.equalsIgnoreCase("INCOMING")) {
				
				criteriaSize++;
				
			}
			
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("type")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
			
				jbossQl.append("bst.bstType=?" + (ctr+1) + " ");
				obj[ctr] = type;				
				ctr++;
				
			}
			
			if (criteria.containsKey("documentNumberFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bst.bstNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberFrom");
				ctr++;
				
			}
			
			if (criteria.containsKey("documentNumberTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bst.bstNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberTo");
				ctr++;
				
			}		  		  
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("bst.bstDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("bst.bstDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}    
			
			if (criteria.containsKey("brVoid")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("bst.bstVoid=?" + (ctr+1) + " ");
				obj[ctr] = ((Byte)criteria.get("brVoid"));
				ctr++;
								
			}  

			if (criteria.containsKey("approvalStatus")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				String approvalStatus = (String)criteria.get("approvalStatus");
				
				if ((type != null) && type.equals("OUT") && statusType.equalsIgnoreCase("INCOMING")) {
					
					if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED") ||
							approvalStatus.equals("PENDING")) {
						
						throw new GlobalNoRecordFoundException();
						
					}
					
				}else if ((type != null) && type.equals("ORDER") && statusType.equalsIgnoreCase("INCOMING")) {
					
					if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED") ||
							approvalStatus.equals("PENDING")) {
						
						throw new GlobalNoRecordFoundException();
						
					}
					
				}
				
				if (approvalStatus.equals("DRAFT")) {
					
					jbossQl.append("bst.bstApprovalStatus IS NULL ");
					
				} else {
					
					jbossQl.append("bst.bstApprovalStatus=?" + (ctr+1) + " ");
					obj[ctr] = approvalStatus;
					ctr++;
					
				}
				
			} else if (!criteria.containsKey("approvalStatus") &&
					(type != null) && type.equals("OUT") && statusType.equalsIgnoreCase("INCOMING")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("(bst.bstApprovalStatus='N/A' OR bst.bstApprovalStatus='APPROVED') AND bst.bstLock=0 ");
				
			} else if (!criteria.containsKey("approvalStatus") &&
					(type != null) && type.equals("ORDER") && statusType.equalsIgnoreCase("INCOMING")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("(bst.bstApprovalStatus='N/A' OR bst.bstApprovalStatus='APPROVED') AND bst.bstLock=0 ");
				
			}
			
			if (criteria.containsKey("posted")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bst.bstPosted=?" + (ctr+1) + " ");
				
				String posted = (String)criteria.get("posted");

				if (posted.equals("YES")) {
					
					obj[ctr] = new Byte(EJBCommon.TRUE);
					
				} else {
					
					obj[ctr] = new Byte(EJBCommon.FALSE);
					
				}       	  
				
				ctr++;
								
			} else if (!criteria.containsKey("posted") &&
					(type != null) && type.equals("OUT") && statusType.equalsIgnoreCase("INCOMING")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bst.bstPosted=?" + (ctr+1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;
								
			} else if (!criteria.containsKey("posted") &&
					(type != null) && type.equals("ORDER") && statusType.equalsIgnoreCase("INCOMING")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bst.bstPosted=?" + (ctr+1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;
								
			}		      
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			if ((type != null) && (type.equals("IN"))){
				
				if (criteria.containsKey("transferOutNumber")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("bst.bstTransferOutNumber LIKE '%" + (String)criteria.get("transferOutNumber") + "%' ");
					
				}
				
			} 
			if ((type != null) && (type.equals("OUT"))){
				
				if (criteria.containsKey("transferOrderNumber")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");	
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("bst.bstTransferOrderNumber LIKE '%" + (String)criteria.get("transferOrderNumber") + "%' ");
					
				}
				
			}

			if ( type.equals("OUT")) {

				if(statusType.equalsIgnoreCase("INCOMING")) {
					
					jbossQl.append("bst.adBranch.brCode=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
					
				} else if (statusType.equalsIgnoreCase("OUTGOING")) {
					
					jbossQl.append("bst.bstAdBranch=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
					
				}
				
			} else if ( type.equals("ORDER")) {
				if(statusType.equalsIgnoreCase("INCOMING")) {
					
					jbossQl.append("bst.adBranch.brCode=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
					
				} else if (statusType.equalsIgnoreCase("OUTGOING")) {
					
					jbossQl.append("bst.bstAdBranch=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
					
				}
				
				//jbossQl.append("bst.bstAdBranch=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
				
				
			} else {
				
				jbossQl.append("bst.bstAdBranch=" + AD_BRNCH + " AND bst.bstAdCompany=" + AD_CMPNY + " ");
				
			}
			
			Collection invBranchStockTransfers = invBranchStockTransferHome.getBstByCriteria(jbossQl.toString(), obj);	         
			
			if (invBranchStockTransfers.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			int SIZE = invBranchStockTransfers.size();
			
			Iterator i = invBranchStockTransfers.iterator();

			while (i.hasNext()) {

				LocalInvBranchStockTransfer invBranchStockTransfer = (LocalInvBranchStockTransfer)i.next();

				if(type.equals("OUT") && ((invBranchStockTransfer.getAdBranch().getBrCode().equals(AD_BRNCH) &&
						invBranchStockTransfer.getBstLock() != EJBCommon.TRUE)))
					SIZE = SIZE -1;
				
			}

			return new Integer(SIZE);
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// private methods
	
	private String getAdBrNameByBrCode(Integer BR_CODE) {
        
        Debug.print("InvFindBranchStockTransferControllerBean getAdBrNameByBrCode");
        
        LocalAdBranchHome adBranchHome = null;
        
        try {
            
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.lookUpLocalHome(
            		LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch(NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            return adBranchHome.findByPrimaryKey(BR_CODE).getBrBranchCode();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
    }
	
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvFindBranchStockTransferControllerBean ejbCreate");
		
	}
}
