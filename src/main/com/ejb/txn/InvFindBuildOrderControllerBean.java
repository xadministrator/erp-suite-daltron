
/*
 * InvFindBuildOrderControllerBean.java
 *
 * Created on March 31, 2005, 11:04 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvBuildOrder;
import com.ejb.inv.LocalInvBuildOrderHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvBuildOrderDetails;

/**
 * @ejb:bean name="InvFindBuildOrderControllerEJB"
 *           display-name="Used for finding build orders"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvFindBuildOrderControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindBuildOrderController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindBuildOrderControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvFindBuildOrderControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvBorByCriteria(HashMap criteria, boolean SO_MATCHED,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindBuildOrderControllerBean getInvBorByCriteria");
        
        LocalInvBuildOrderHome invBuildOrderHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	invBuildOrderHome = (LocalInvBuildOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildOrderHome.JNDI_NAME, LocalInvBuildOrderHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(bor) FROM InvBuildOrder bor ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		criteriaSize--;
        		
        	}
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");	
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("bor.borReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
        		
        	}
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("bor.borDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("bor.borDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	}

        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("bor.borDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("bor.borDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}
        	
    		if (!firstArgument) {
    			jbossQl.append("AND ");
    		} else {
    			firstArgument = false;
    			jbossQl.append("WHERE ");
    		}
        	
        	if(SO_MATCHED)
        		jbossQl.append("bor.borSoNumber IS NOT NULL ");
        	else
        		jbossQl.append("bor.borSoNumber IS NULL ");
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("bor.borAdBranch=" + AD_BRNCH + " AND bor.borAdCompany=" + AD_CMPNY + " ");
        	
        	String orderBy = null;
        	
        	if (ORDER_BY.equals("DATE")) {	          
        		
        		orderBy = "bor.borDate";
        		
        	} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
        		
        		orderBy = "bor.borDocumentNumber";
        		
        	} else if (ORDER_BY.equals("REFERENCE NUMBER")) {	          
        		
        		orderBy = "bor.borReferenceNumber";	  
        		
        	}
        	
        	if (orderBy != null) {
        		
        		jbossQl.append("ORDER BY " + orderBy + ", bor.borDate");
        		
        	} else {
        		
        		jbossQl.append("ORDER BY bor.borDate");
        		
        	}
        	
        	jbossQl.append(" OFFSET ?" + (ctr + 1));	        
        	obj[ctr] = OFFSET;	      
        	ctr++;
        	
        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;		      
        	
        	Collection invBuildOrders = invBuildOrderHome.getBorByCriteria(jbossQl.toString(), obj);	         
        	
        	if (invBuildOrders.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
//        	if(SO_MATCHED) {
        		
        		
        		
  //      	}
        	
        	Iterator i = invBuildOrders.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvBuildOrder invBuildOrder = (LocalInvBuildOrder)i.next(); 
        		
        		InvBuildOrderDetails mdetails = new InvBuildOrderDetails();
        		mdetails.setBorCode(invBuildOrder.getBorCode());
        		mdetails.setBorDate(invBuildOrder.getBorDate());
        		mdetails.setBorDocumentNumber(invBuildOrder.getBorDocumentNumber());
        		mdetails.setBorReferenceNumber(invBuildOrder.getBorReferenceNumber());
        		mdetails.setBorDescription(invBuildOrder.getBorDescription());
        		mdetails.setBorSoNumber(invBuildOrder.getBorSoNumber());
        		
        		list.add(mdetails);
        		
        	}
        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	
        } catch (Exception ex) {

        	ex.printStackTrace();
        	throw new EJBException(ex.getMessage());
        	
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getInvBorSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindBuildOrderControllerBean getInvBorSizeByCriteria");
        
        LocalInvBuildOrderHome invBuildOrderHome = null;

        //initialized EJB Home
        
        try {
            
        	invBuildOrderHome = (LocalInvBuildOrderHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildOrderHome.JNDI_NAME, LocalInvBuildOrderHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(bor) FROM InvBuildOrder bor ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size();	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		criteriaSize--;
        		
        	}
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");	
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("bor.borReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
        		
        	}
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("bor.borDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("bor.borDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	}

        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("bor.borDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("bor.borDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("bor.borAdBranch=" + AD_BRNCH + " AND bor.borAdCompany=" + AD_CMPNY + " ");
        	
        	Collection invBuildOrders = invBuildOrderHome.getBorByCriteria(jbossQl.toString(), obj);	         
        	
        	if (invBuildOrders.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
        	return new Integer(invBuildOrders.size());
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	
        } catch (Exception ex) {

        	ex.printStackTrace();
        	throw new EJBException(ex.getMessage());
        	
        }
        
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvFindBuildOrderControllerBean ejbCreate");
      
    }
}
