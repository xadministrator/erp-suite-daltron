
/*
 * InvFindBuildOrderLineControllerBean.java
 *
 * Created on April 1, 2005 09:41 AM
 *
 * @author  Marvis Fernando C. Salvador
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvBuildOrderLine;
import com.ejb.inv.LocalInvBuildOrderLineHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvModBuildOrderLineDetails;

/**
 * @ejb:bean name="InvFindBuildOrderLineControllerEJB"
 *           display-name="Used for finding build order lines"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvFindBuildOrderLineControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindBuildOrderLineController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindBuildOrderLineControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvFindBuildOrderLineControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvIncompleteBol(Integer OFFSET, Integer LIMIT,Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindBuildOrderLineControllerBean getInvIncompleteBol");
        
        LocalInvBuildOrderLineHome invBuildOrderLineHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	invBuildOrderLineHome = (LocalInvBuildOrderLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildOrderLineHome.JNDI_NAME, LocalInvBuildOrderLineHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT DISTINCT OBJECT(bol) FROM InvBuildOrderLine bol, IN(bol.invBuildOrderStocks) bos WHERE bol.bolQuantityRequired > bol.bolQuantityAssembled AND bos.bosQuantityRequired > bos.bosQuantityIssued AND ");
		  		  		  
		  Object obj[] = null;		      
		       	 
		  jbossQl.append("bol.invBuildOrder.borAdBranch=" + AD_BRNCH + " AND bol.bolAdCompany=" + AD_CMPNY + " ");
		     	      			
		  jbossQl.append("ORDER BY bol.invItemLocation.invItem.iiName");
		  
		  jbossQl.append(" OFFSET " + OFFSET);	        
	      		  
		  jbossQl.append(" LIMIT " + LIMIT);		      
			  	      	
	      Collection invBuildOrderLines = invBuildOrderLineHome.getIncompleteBolByCriteria(jbossQl.toString(), obj);	         
		  
		  if (invBuildOrderLines.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = invBuildOrderLines.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalInvBuildOrderLine invBuildOrderLine = (LocalInvBuildOrderLine)i.next();   	  
		  	  
		  	  InvModBuildOrderLineDetails mdetails = new InvModBuildOrderLineDetails();
		  	  mdetails.setBolCode(invBuildOrderLine.getBolCode());
      	      mdetails.setBolQuantityRequired(invBuildOrderLine.getBolQuantityRequired());
      	      mdetails.setBolQuantityAssembled(invBuildOrderLine.getBolQuantityAssembled());
      	      mdetails.setBolIlIiName(invBuildOrderLine.getInvItemLocation().getInvItem().getIiName());
      	      mdetails.setBolIlLocName(invBuildOrderLine.getInvItemLocation().getInvLocation().getLocName());
      	      mdetails.setBolIlIiDescription(invBuildOrderLine.getInvItemLocation().getInvItem().getIiDescription());
      	      mdetails.setBolBorDocumentNumber(invBuildOrderLine.getInvBuildOrder().getBorDocumentNumber());
			      	  	      	  		     
			  list.add(mdetails);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getInvIncompleteBolSize(Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindBuildOrderLineControllerBean getInvIncompleteBolSize");
        
        LocalInvBuildOrderLineHome invBuildOrderLineHome = null;
        
        //initialized EJB Home
        
        try {
            
        	invBuildOrderLineHome = (LocalInvBuildOrderLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildOrderLineHome.JNDI_NAME, LocalInvBuildOrderLineHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT DISTINCT OBJECT(bol) FROM InvBuildOrderLine bol, IN(bol.invBuildOrderStocks) bos WHERE bol.bolQuantityRequired > bol.bolQuantityAssembled AND bos.bosQuantityRequired > bos.bosQuantityIssued AND ");
		  		  		  
		  Object obj[] = null;		      
		       	 
       	  jbossQl.append("bol.bolAdBranch=" + AD_BRNCH + " AND bol.bolAdCompany=" + AD_CMPNY + " ");
		    	      	
	      Collection invBuildOrderLines = invBuildOrderLineHome.getIncompleteBolByCriteria(jbossQl.toString(), obj);	         
		  
		  if (invBuildOrderLines.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  return new Integer(invBuildOrderLines.size());
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	   /**
	    * @ejb:interface-method view-type="remote"
	    * @jboss:method-attributes read-only="true"
	    **/
	    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

	       Debug.print("InvFindBuildOrderLineControllerBean getGlFcPrecisionUnit");

	      
	       LocalAdCompanyHome adCompanyHome = null;
	      
	     
	       // Initialize EJB Home
	        
	       try {
	            
	           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
	            
	       } catch (NamingException ex) {
	            
	           throw new EJBException(ex.getMessage());
	            
	       }

	       try {
	       	
	         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	            
	         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
	                                     
	       } catch (Exception ex) {
	       	 
	       	 Debug.printStackTrace(ex);
	         throw new EJBException(ex.getMessage());
	         
	       }

	    }	    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvFindBuildOrderLineControllerBean ejbCreate");
      
    }
}
