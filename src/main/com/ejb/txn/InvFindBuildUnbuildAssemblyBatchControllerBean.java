
/*
 * InvFindBuildUnbuildAssemblyBatchControllerBean.java
 *
 * Created on May 20, 2004, 2:36 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.inv.LocalInvBuildUnbuildAssemblyBatch;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyBatchHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvBuildUnbuildAssemblyBatchDetails;

/**
 * @ejb:bean name="InvFindBuildUnbuildAssemblyBatchControllerEJB"
 *           display-name="Used for searching buildunbuild assembly batches"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvFindBuildUnbuildAssemblyBatchControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindBuildUnbuildAssemblyBatchController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindBuildUnbuildAssemblyBatchControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvFindBuildUnbuildAssemblyBatchControllerBean extends AbstractSessionBean {


   
   /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
   public ArrayList getArBbByCriteria(HashMap criteria, String ORDER_BY, Integer OFFSET, Integer LIMIT, Integer AD_BRNCH, Integer AD_CMPNY)
      throws GlobalNoRecordFoundException {

      Debug.print("InvFindBuildUnbuildAssemblyBatchControllerBean getArBbByCriteria");
      
     
      LocalInvBuildUnbuildAssemblyBatchHome invBuildUnbuildAssemblyBatchHome = null;
      
      // Initialize EJB Home
        
      try {
      	
    	  invBuildUnbuildAssemblyBatchHome = (LocalInvBuildUnbuildAssemblyBatchHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvBuildUnbuildAssemblyBatchHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyBatchHome.class);
           
      } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
      }
      
      try {
  
	      ArrayList list = new ArrayList();
	      
	      StringBuffer jbossQl = new StringBuffer();
	      jbossQl.append("SELECT OBJECT(bb) FROM InvBuildUnbuildAssemblyBatch bb ");
	      
	      boolean firstArgument = true;
	      short ctr = 0;
	      Object obj[];
	      
	       // Allocate the size of the object parameter
	
	       
	      if (criteria.containsKey("batchName")) {
	      	
	      	 obj = new Object[(criteria.size()-1) + 2];
	      	 
	      } else {
	      	
	      	 obj = new Object[criteria.size() + 2];
		
	      }
	      
	  
	         
	      
	      if (criteria.containsKey("batchName")) {
	      	
	      	 if (!firstArgument) {
	      	 
	      	    jbossQl.append("AND ");	
	      	 	
	         } else {
	         	
	         	firstArgument = false;
	         	jbossQl.append("WHERE ");
	         	
	         }
	         
	      	 jbossQl.append("bb.bbName LIKE '%" + (String)criteria.get("batchName") + "%' ");
	      	 
	      }
	      	 
	        
	      if (criteria.containsKey("status")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("bb.bbStatus=?" + (ctr+1) + " ");
		  	 obj[ctr] = (String)criteria.get("status");
		  	 ctr++;
		  }  
		  
		  if (criteria.containsKey("dateCreated")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("bb.bbDateCreated=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateCreated");
		  	 ctr++;
		  } 
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("bb.bbAdBranch=" + AD_BRNCH + " AND bb.bbAdCompany=" + AD_CMPNY + " ");
		      
	         
	      String orderBy = null;
		      
		  if (ORDER_BY.equals("BATCH NAME")) {
		
		  	  orderBy = "bb.bbName";
		  	  
		  } else if (ORDER_BY.equals("DATE CREATED")) {
		
		  	  orderBy = "bb.bbDateCreated";
		  	
		  }
		  
		  if (orderBy != null) {
		  
		  	jbossQl.append("ORDER BY " + orderBy);
		  	
		  }
	               
	      jbossQl.append(" OFFSET ?" + (ctr + 1));
	      obj[ctr] = OFFSET;
	      ctr++;
	      
	      jbossQl.append(" LIMIT ?" + (ctr + 1));
	      obj[ctr] = LIMIT;
	      
	      System.out.println("QL + " + jbossQl);

	      Collection arInvoiceBatches = invBuildUnbuildAssemblyBatchHome.getBbByCriteria(jbossQl.toString(), obj);
	         
	      if (arInvoiceBatches.isEmpty())
	         throw new GlobalNoRecordFoundException();
	         
	      Iterator i = arInvoiceBatches.iterator();
	      while (i.hasNext()) {
	      	      	
	         LocalInvBuildUnbuildAssemblyBatch invBuildUnbuildAssemblyBatch = (LocalInvBuildUnbuildAssemblyBatch) i.next();
	         
	         InvBuildUnbuildAssemblyBatchDetails details = new InvBuildUnbuildAssemblyBatchDetails();
	         
	         
	         details.setBbCode(invBuildUnbuildAssemblyBatch.getBbCode());
	         details.setBbName(invBuildUnbuildAssemblyBatch.getBbName());
	         details.setBbDescription(invBuildUnbuildAssemblyBatch.getBbDescription());
	         details.setBbStatus(invBuildUnbuildAssemblyBatch.getBbStatus());
	         details.setBbDateCreated(invBuildUnbuildAssemblyBatch.getBbDateCreated());
	         details.setBbCreatedBy(invBuildUnbuildAssemblyBatch.getBbCreatedBy());
	         
	         list.add(details);
	      	
	      }
	         
	      return list;
  
	   } catch (GlobalNoRecordFoundException ex) {
		  	 
		  	  throw ex;
		  	
	   } catch (Exception ex) {
	  	
	
		  	  ex.printStackTrace();
		  	  throw new EJBException(ex.getMessage());
		  	
	   }
    
   }
   
   
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public Integer getArBbSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
       throws GlobalNoRecordFoundException {

       Debug.print("InvFindBuildUnbuildAssemblyBatchControllerBean getArBbSizeByCriteria");
       
       LocalInvBuildUnbuildAssemblyBatchHome invBuildUnbuildAssemblyBatchHome = null;
       // Initialize EJB Home
         
       try {
       	
           invBuildUnbuildAssemblyBatchHome = (LocalInvBuildUnbuildAssemblyBatchHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvBuildUnbuildAssemblyBatchHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyBatchHome.class);
            
       } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
       }
       
       try {
   
 	      StringBuffer jbossQl = new StringBuffer();
 	      jbossQl.append("SELECT OBJECT(bb) FROM InvBuildUnbuildAssemblyBatch bb ");
 	      
 	      boolean firstArgument = true;
 	      short ctr = 0;
 	      Object obj[];
 	      
 	       // Allocate the size of the object parameter
 	
 	       
 	      if (criteria.containsKey("batchName")) {
 	      	
 	      	 obj = new Object[(criteria.size()-1)];
 	      	 
 	      } else {
 	      	
 	      	 obj = new Object[criteria.size()];
 		
 	      }
 	      
 	      
 	      if (criteria.containsKey("batchName")) {
 	      	
 	      	 if (!firstArgument) {
 	      	 
 	      	    jbossQl.append("AND ");	
 	      	 	
 	         } else {
 	         	
 	         	firstArgument = false;
 	         	jbossQl.append("WHERE ");
 	         	
 	         }
 	         
 	      	 jbossQl.append("bb.bbName LIKE '%" + (String)criteria.get("batchName") + "%' ");
 	      	 
 	      }
 	      	 
 	        
 	      if (criteria.containsKey("status")) {
 		      	
 		  	 if (!firstArgument) {
 		  	 	jbossQl.append("AND ");
 		  	 } else {
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 }
 		  	 jbossQl.append("bb.bbStatus=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (String)criteria.get("status");
 		  	 ctr++;
 		  }  
 		  
 		  if (criteria.containsKey("dateCreated")) {
 		      	
 		  	 if (!firstArgument) {
 		  	 	jbossQl.append("AND ");
 		  	 } else {
 		  	 	firstArgument = false;
 		  	 	jbossQl.append("WHERE ");
 		  	 }
 		  	 jbossQl.append("bb.bbDateCreated=?" + (ctr+1) + " ");
 		  	 obj[ctr] = (Date)criteria.get("dateCreated");
 		  	 ctr++;
 		  } 
 		  
 		  if (!firstArgument) {
 		  	
 		  	jbossQl.append("AND ");
 		  	
 		  } else {
 		  	
 		  	firstArgument = false;
 		  	jbossQl.append("WHERE ");
 		  	
 		  }
        	  
 		  jbossQl.append("bb.bbAdBranch=" + AD_BRNCH + " AND bb.bbAdCompany=" + AD_CMPNY + " ");
 		      
 	      Collection arInvoiceBatches = invBuildUnbuildAssemblyBatchHome.getBbByCriteria(jbossQl.toString(), obj);
 	         
 	      if (arInvoiceBatches.isEmpty())
 	         throw new GlobalNoRecordFoundException();
 	         
 	      return new Integer(arInvoiceBatches.size());
   
 	   } catch (GlobalNoRecordFoundException ex) {
 		  	 
 		  	  throw ex;
 		  	
 	   } catch (Exception ex) {
 	  	
 	
 		  	  ex.printStackTrace();
 		  	  throw new EJBException(ex.getMessage());
 		  	
 	   }
     
    }
   
   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("InvFindBuildUnbuildAssemblyBatchControllerBean ejbCreate");
      
   }

   // private methods

     
}
