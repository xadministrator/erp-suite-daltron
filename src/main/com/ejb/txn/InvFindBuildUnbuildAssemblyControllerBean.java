
/*
 * InvFindBuildUnbuildAssemblyControllerBean.java
 *
 * Created on June 18, 2004, 10:15 AM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdFormFunctionResponsibility;
import com.ejb.ad.LocalAdFormFunctionResponsibilityHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.inv.LocalInvBuildUnbuildAssembly;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyBatch;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyBatchHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLine;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.ApModPurchaseOrderDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvBuildUnbuildAssemblyDetails;
import com.util.InvModBuildUnbuildAssemblyDetails;
import com.util.InvModBuildUnbuildAssemblyOrderLineDetails;

/**
 * @ejb:bean name="InvFindBuildUnbuildAssemblyControllerEJB"
 *           display-name="Used for finding build unbuild assembly"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvFindBuildUnbuildAssemblyControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindBuildUnbuildAssemblyController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindBuildUnbuildAssemblyControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvFindBuildUnbuildAssemblyControllerBean extends AbstractSessionBean {

	
	
	/**
     * @ejb:interface-method view-type="remote"
     **/    
    public int copyInvBuaEntry( com.util.InvBuildUnbuildAssemblyDetails details, int NUM_COPY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws Exception {
                    
        Debug.print("InvFindBuildUnbuildAssemblyControllerBean copyInvBuaEntry");
      
       
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalArCustomerHome arCustomerHome = null;
        LocalInvBuildUnbuildAssemblyBatchHome invBuildUnbuildAssemblyBatchHome = null;
        
        try {
        	
        	invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
                glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
                adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
                adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
                adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
                adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
                invItemHome = (LocalInvItemHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
                invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
                invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
                adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
                adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
                adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
                adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
                
                arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                        lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
                invBuildUnbuildAssemblyBatchHome = (LocalInvBuildUnbuildAssemblyBatchHome)EJBHomeFactory.
                        lookUpLocalHome(LocalInvBuildUnbuildAssemblyBatchHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyBatchHome.class);
                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }   
        
        
        
        
        try {
        	
        	int ctr = 1;
        	while (ctr <= NUM_COPY){
        		
        		
        		LocalInvBuildUnbuildAssembly invBuildUnbuildAssemblyCopy = null;
                
                // validate if BuildUnbuildAssembly is already deleted
                
                try {
                    
                    if (details.getBuaCode() != null) {
                        
                        invBuildUnbuildAssemblyCopy = invBuildUnbuildAssemblyHome.findByPrimaryKey(details.getBuaCode());
                        
                    }
                    
                } catch (FinderException ex) {
                    
                    throw new GlobalRecordAlreadyDeletedException();
                    
                }
                
                
                
                
                LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
                LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null; 
                
           	
                try {
                    
                    adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV BUILD ASSEMBLY ORDER", AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                try {
                    
                    adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                String newDocumentNumber = "";
                if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
                        (details.getBuaDocumentNumber() == null || details.getBuaDocumentNumber().trim().length() == 0)) {
                    
                    while (true) {
                        
                        if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {		            	
                            
                            try {
                                
                                invBuildUnbuildAssemblyHome.findByBuaDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);		            		
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
                                
                            } catch (FinderException ex) {
                                
                            	newDocumentNumber = adDocumentSequenceAssignment.getDsaNextSequence();   
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
                                break;
                                
                            }
                            
                        } else {
                            
                            try {
                                
                                invBuildUnbuildAssemblyHome.findByBuaDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
                                
                            } catch (FinderException ex) {
                                
                            	newDocumentNumber = adDocumentSequenceAssignment.getDsaNextSequence();
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
                                break;
                                
                            }
                            
                        }
                        
                    }		            
                    
                }
                
                
                
                LocalInvBuildUnbuildAssembly newInvBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.create(EJBCommon.FALSE, invBuildUnbuildAssemblyCopy.getBuaType(), newDocumentNumber,
               		 invBuildUnbuildAssemblyCopy.getBuaDocumentNumber(), null, EJBCommon.FALSE,
               		 invBuildUnbuildAssemblyCopy.getBuaDate(), invBuildUnbuildAssemblyCopy.getBuaDueDate(), "SYSTEM GENERATED BUA ORDER REF:" +invBuildUnbuildAssemblyCopy.getBuaDocumentNumber() , 
               		 invBuildUnbuildAssemblyCopy.getBuaVoid(), null,
                        EJBCommon.FALSE, invBuildUnbuildAssemblyCopy.getBuaCreatedBy(), invBuildUnbuildAssemblyCopy.getBuaDateCreated(),
                        invBuildUnbuildAssemblyCopy.getBuaLastModifiedBy(), invBuildUnbuildAssemblyCopy.getBuaDateLastModified(),
                        null, null, null, null, null, AD_BRNCH, AD_CMPNY);
                
                LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(invBuildUnbuildAssemblyCopy.getArCustomer().getCstCustomerCode(), AD_CMPNY);
                newInvBuildUnbuildAssembly.setArCustomer(arCustomer);
           	
           	
                /*try {
                    
                	LocalInvBuildUnbuildAssemblyBatch invBuildUnbuildAssemblyBatch = invBuildUnbuildAssemblyBatchHome.findByBbName(invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyBatch().getBbName(), AD_BRNCH, AD_CMPNY);

                	newInvBuildUnbuildAssembly.setInvBuildUnbuildAssemblyBatch(invBuildUnbuildAssemblyBatch);
                    
                } catch (FinderException ex) {
                    
                }*/
                
                Collection invBuildUnbuildAssemblyLinesCopy = invBuildUnbuildAssemblyCopy.getInvBuildUnbuildAssemblyLines();
                
                Iterator i = invBuildUnbuildAssemblyLinesCopy.iterator();
                
                
                while (i.hasNext()) {
               	 

                    LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLineCopy = (LocalInvBuildUnbuildAssemblyLine) i.next();

                    LocalInvItemLocation invItemLocation = null;
                    
                    try {
                        
                        invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                       		 invBuildUnbuildAssemblyLineCopy.getInvItemLocation().getInvLocation().getLocName(), 
                       		 invBuildUnbuildAssemblyLineCopy.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                        
                    }
                    
                    this.addInvBlEntry(invBuildUnbuildAssemblyLineCopy, newInvBuildUnbuildAssembly, AD_CMPNY);
                     
               	 
                }

        		ctr++;
        		
        	}
        	
        	
        	 return ctr;
        	
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	
	
	
	
	
	
	
	
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvGpQuantityPrecisionUnit");
        
        LocalAdPreferenceHome adPreferenceHome = null;         
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            return adPreference.getPrfInvQuantityPrecisionUnit();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvOpenBbAll(Integer AD_BRNCH, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvOpenBbAll");
        
        LocalArInvoiceBatchHome arInvoiceBatchHome = null;
        LocalInvBuildUnbuildAssemblyBatchHome invBuildUnbuildAssemblyBatchHome = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invBuildUnbuildAssemblyBatchHome = (LocalInvBuildUnbuildAssemblyBatchHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildUnbuildAssemblyBatchHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyBatchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {

            Collection invBuildUnbuildAssemblyBatches = invBuildUnbuildAssemblyBatchHome.findOpenBbByBbType("BUA", AD_BRNCH, AD_CMPNY);
            
            Iterator i = invBuildUnbuildAssemblyBatches.iterator();
            
            while (i.hasNext()) {
                
            	 
            	 LocalInvBuildUnbuildAssemblyBatch invBuildUnbuildAssemblyBatch = (LocalInvBuildUnbuildAssemblyBatch)i.next();
                
                list.add(invBuildUnbuildAssemblyBatch.getBbName());
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getArCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("InvFindBuildUnbuildAssemblyControllerBean getArCstAll");
        
        LocalArCustomerHome arCustomerHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);

	        Iterator i = arCustomers.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)i.next();
	        	
	        	list.add(arCustomer.getCstCustomerCode());
	        	
	        }
	        
	        return list;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvBuaByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, boolean isPoLookup, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindBuildUnbuildAssemblyControllerBean getInvBuaByCriteria");
        
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
        
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
        	invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
                
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size() + 2;	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      if (criteria.containsKey("approvalStatus")) {
	      	
	      	 String approvalStatus = (String)criteria.get("approvalStatus");
	      	
	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
	      	 	
	      	 	 criteriaSize--;
	      	 	
	      	 }
	      	
	      }
	      	
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("referenceNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("bua.buaReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

		  }
		  
		  if (criteria.containsKey("batchName")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("bua.invBuildUnbuildAssemblyBatch.bbName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("batchName");
		   	  ctr++;
	   	  
	      }
		  
		  if (criteria.containsKey("customerCode")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("bua.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerCode");
		   	  ctr++;
	   	  
	      }	
		  
		  if (criteria.containsKey("type")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("bua.buaType=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("type");
		   	  ctr++;
   	  
        }
		  
		  if (!firstArgument) {
			 	jbossQl.append("AND ");
			} else {
			 	firstArgument = false;
			 	jbossQl.append("WHERE ");
			}
			 
		    jbossQl.append("bua.buaReceiving=?" + (ctr+1) + " ");
		    obj[ctr] = (Byte)criteria.get("receiving");
		    ctr++;
		    
		    System.out.println("receiving="+(Byte)criteria.get("receiving"));
		    

	      if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	  } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	  }

	  	  jbossQl.append("bua.buaVoid=?" + (ctr+1) + " ");
	  	  obj[ctr] = (Byte)criteria.get("buaVoid");
	      ctr++;
	      System.out.println("buaVoid="+(Byte)criteria.get("buaVoid"));
		    
	      
	      if (criteria.containsKey("documentNumberFrom")) {
	      	
	      	if (!firstArgument) {
	      		jbossQl.append("AND ");
	      	} else {
	      		firstArgument = false;
	      		jbossQl.append("WHERE ");
	      	}
	      	jbossQl.append("bua.buaDocumentNumber>=?" + (ctr+1) + " ");
	      	obj[ctr] = (String)criteria.get("documentNumberFrom");
	      	ctr++;
	      }
	      
	      if (criteria.containsKey("documentNumberTo")) {
	      	
	      	if (!firstArgument) {
	      		jbossQl.append("AND ");
	      	} else {
	      		firstArgument = false;
	      		jbossQl.append("WHERE ");
	      	}
	      	jbossQl.append("bua.buaDocumentNumber<=?" + (ctr+1) + " ");
	      	obj[ctr] = (String)criteria.get("documentNumberTo");
	      	ctr++;
	      }
	      
	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("bua.buaDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("bua.buaDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		  
	      if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
	       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("bua.buaApprovalStatus IS NULL ");
	       	  	
	       	  } else {
	      	  	
		      	  jbossQl.append("bua.buaApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
	       	  
	      }
	      	      
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("bua.buaPosted=?" + (ctr+1) + " ");
	       	  
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }	   
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("bua.buaAdBranch=" + AD_BRNCH + " AND bua.buaAdCompany=" + AD_CMPNY + " ");
      			
		  String orderBy = null;
		  
		  if (ORDER_BY.equals("REFERENCE NUMBER")) {	          
		      	      		
		  	  orderBy = "bua.buaReferenceNumber";

		  } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
  	      		
		  	 orderBy = "bua.buaDocumentNumber";
		  	
		  }
		  
	  	  if (orderBy != null) {
		
		  	  jbossQl.append("ORDER BY " + orderBy + ", bua.buaDate");

		  } else {
		  	
		  	  jbossQl.append("ORDER BY bua.buaDate");
		  	
		  }
		  
	  	if (!isPoLookup) {
    		
        	jbossQl.append(" OFFSET ?" + (ctr + 1));	        
        	obj[ctr] = OFFSET;	      
        	ctr++;
        	System.out.println("LIMIT------------->"+LIMIT);
        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;		
        	
    	} else {
    		
    		jbossQl.append(" OFFSET ?" + (ctr + 1));	        
        	obj[ctr] = 0;	      
        	ctr++;
        	System.out.println("LIMIT------------->"+LIMIT);
        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = 0;
        	ctr++;	
    	}
	  	
	  	System.out.println("jbossQl="+jbossQl.toString());
	  	Collection invBuildUnbuildAssemblies = invBuildUnbuildAssemblyHome.getBuaByCriteria(jbossQl.toString(), obj);	         
		  
		  if (invBuildUnbuildAssemblies.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = invBuildUnbuildAssemblies.iterator();
		  
		  while (i.hasNext()) {
			  
			  LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = (LocalInvBuildUnbuildAssembly)i.next();   	  
		  	  
			  System.out.println("isPoLookup="+isPoLookup);
			  if (isPoLookup) {
	        		boolean isPoBreak = false;
	        		Collection invBuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
	        		System.out.println("invBuildAssemblyLines.size()="+invBuildAssemblyLines.size());
	        		Iterator j = invBuildAssemblyLines.iterator();
	        		while (j.hasNext()) {
	        			
	        			LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)j.next();
	        			Collection invReceivedBos = invBuildUnbuildAssemblyLineHome.findByBlBlCode(invBuildUnbuildAssemblyLine.getBlCode(), AD_CMPNY);
	        			
	        			double totalReceived = 0;
	        			System.out.println("invReceivedBos.size()="+invReceivedBos.size());
		        		
	        			Iterator k = invReceivedBos.iterator();
	        			while (k.hasNext()) {
	        				
	        				LocalInvBuildUnbuildAssemblyLine invReceivedBo = (LocalInvBuildUnbuildAssemblyLine)k.next();
	        				totalReceived += invReceivedBo.getBlBuildQuantity();
	        				
	        			}
	        			System.out.println("totalReceived="+totalReceived);
	        			if (invReceivedBos.size() == 0 || totalReceived != invBuildUnbuildAssemblyLine.getBlBuildQuantity()) {
	        				
	        				
	        				
	        				InvModBuildUnbuildAssemblyDetails mdetails = new InvModBuildUnbuildAssemblyDetails();
	      			  	  	mdetails.setBuaCode(invBuildUnbuildAssembly.getBuaCode());
	      			  	  	mdetails.setBuaCstCustomerCode(invBuildUnbuildAssembly.getArCustomer().getCstCustomerCode());  
	      			  	  	mdetails.setBuaDocumentNumber(invBuildUnbuildAssembly.getBuaDocumentNumber());
	      			  	  	mdetails.setBuaReferenceNumber(invBuildUnbuildAssembly.getBuaReferenceNumber());
	      			  	  	mdetails.setBuaDescription(invBuildUnbuildAssembly.getBuaDescription());
	      			  	  	mdetails.setBuaDate(invBuildUnbuildAssembly.getBuaDate());
	      			  	  	mdetails.setBuaVoid(invBuildUnbuildAssembly.getBuaVoid());
	      			  	  	mdetails.setBuaReceiving(invBuildUnbuildAssembly.getBuaReceiving());
	      			  	  	System.out.println("invBuildUnbuildAssembly.getBuaDocumentNumber()="+invBuildUnbuildAssembly.getBuaDocumentNumber());
	    	        		list.add(mdetails);
	    	        		isPoBreak = true;
	        			}
	        			if (isPoBreak) break;
	        			
	        		}
	        		
	        		if (isPoBreak) continue;
	        		
				  
			  } else {

				  	System.out.println("invBuildUnbuildAssembly.getBuaDocumentNumber()="+invBuildUnbuildAssembly.getBuaDocumentNumber());
	    			
				  	Collection invBuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
				  	
				  	Iterator j = invBuildAssemblyLines.iterator();
				  	double TOTA_QTY =0d;
	        		while (j.hasNext()) {
	        			LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)j.next();
	        			TOTA_QTY += invBuildUnbuildAssemblyLine.getBlBuildQuantity();
	        		}
				  	
				  	
				  	Collection invBuildUnbuildAssemblyLines2 = invBuildUnbuildAssemblyLineHome.findByBuaRcvBuaNumberAndBuaReceivingAndCstCustomerCodeAndBrCode(
	      					invBuildUnbuildAssembly.getBuaDocumentNumber(), invBuildUnbuildAssembly.getArCustomer().getCstCustomerCode(), invBuildUnbuildAssembly.getBuaAdBranch(), AD_CMPNY);
	      			Iterator y = invBuildUnbuildAssemblyLines2.iterator();
	      			System.out.println("invBuildUnbuildAssemblyLines2.size="+invBuildUnbuildAssemblyLines2.size());
	      			double RCVD_QTY = 0d;
	      			while (y.hasNext()) { 
	      				LocalInvBuildUnbuildAssemblyLine invBuildUnBuildAssemblyItemLine = (LocalInvBuildUnbuildAssemblyLine)y.next();
	
	      				RCVD_QTY += invBuildUnBuildAssemblyItemLine.getBlBuildQuantity();
	
	      			}
	    
					InvModBuildUnbuildAssemblyDetails mdetails = new InvModBuildUnbuildAssemblyDetails();
					mdetails.setBuaQuantity(TOTA_QTY);
					mdetails.setBuaReceived(RCVD_QTY);
			  	  	mdetails.setBuaCode(invBuildUnbuildAssembly.getBuaCode());
			  	  	mdetails.setBuaCstCustomerCode(invBuildUnbuildAssembly.getArCustomer().getCstCustomerCode());  
			  	  	mdetails.setBuaDocumentNumber(invBuildUnbuildAssembly.getBuaDocumentNumber());
			  	  	mdetails.setBuaReferenceNumber(invBuildUnbuildAssembly.getBuaReferenceNumber());
			  	  	mdetails.setBuaDescription(invBuildUnbuildAssembly.getBuaDescription());
			  	  	mdetails.setBuaDate(invBuildUnbuildAssembly.getBuaDate());
			  	  	mdetails.setBuaVoid(invBuildUnbuildAssembly.getBuaVoid());
			  	  	mdetails.setBuaReceiving(invBuildUnbuildAssembly.getBuaReceiving());
			  	  	list.add(mdetails);

				  
			  }
		  	  

			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getInvBuaSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindBuildUnbuildAssemblyControllerBean getInvBuaSizeByCriteria");
        
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        
        //initialized EJB Home
        
        try {
            
        	invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(bua) FROM InvBuildUnbuildAssembly bua ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      if (criteria.containsKey("approvalStatus")) {
	      	
	      	 String approvalStatus = (String)criteria.get("approvalStatus");
	      	
	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
	      	 	
	      	 	 criteriaSize--;
	      	 	
	      	 }
	      	
	      }
	      	
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("referenceNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("bua.buaReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

		  }
		  
		  if (criteria.containsKey("batchName")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("bua.invBuildUnbuildAssemblyBatch.bbName=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("batchName");
		   	  ctr++;
	   	  
	      }
		  
		  if (criteria.containsKey("customerCode")) {
	   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("bua.arCustomer.cstCustomerCode=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("customerCode");
		   	  ctr++;
	   	  
	      }	
		  
		  if (criteria.containsKey("type")) {
			   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("bua.buaType=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("type");
		   	  ctr++;
   	  
        }
		  
		  if (!firstArgument) {
			  jbossQl.append("AND ");
		  } else {
			 	firstArgument = false;
			 	jbossQl.append("WHERE ");
			}
			 
		    jbossQl.append("bua.buaReceiving=?" + (ctr+1) + " ");
		    obj[ctr] = (Byte)criteria.get("receiving");
		    ctr++;

	      if (!firstArgument) {
	  	 	jbossQl.append("AND ");
	  	  } else {
	  	 	firstArgument = false;
	  	 	jbossQl.append("WHERE ");
	  	  }

	  	  jbossQl.append("bua.buaVoid=?" + (ctr+1) + " ");
	  	  obj[ctr] = (Byte)criteria.get("buaVoid");
	      ctr++;
	      
	      if (criteria.containsKey("documentNumberFrom")) {
	      	
	      	if (!firstArgument) {
	      		jbossQl.append("AND ");
	      	} else {
	      		firstArgument = false;
	      		jbossQl.append("WHERE ");
	      	}
	      	jbossQl.append("bua.buaDocumentNumber>=?" + (ctr+1) + " ");
	      	obj[ctr] = (String)criteria.get("documentNumberFrom");
	      	ctr++;
	      }
	      
	      if (criteria.containsKey("documentNumberTo")) {
	      	
	      	if (!firstArgument) {
	      		jbossQl.append("AND ");
	      	} else {
	      		firstArgument = false;
	      		jbossQl.append("WHERE ");
	      	}
	      	jbossQl.append("bua.buaDocumentNumber<=?" + (ctr+1) + " ");
	      	obj[ctr] = (String)criteria.get("documentNumberTo");
	      	ctr++;
	      }
	      
	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("bua.buaDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("bua.buaDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		  
	      if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
	       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("bua.buaApprovalStatus IS NULL ");
	       	  	
	       	  } else {
	      	  	
		      	  jbossQl.append("bua.buaApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
	       	  
	      }
	      	      
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("bua.buaPosted=?" + (ctr+1) + " ");
	       	  
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }	   
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
	      jbossQl.append("bua.buaAdBranch=" + AD_BRNCH + " AND bua.buaAdCompany=" + AD_CMPNY + " ");
      			
		    	      	
	      Collection invBuildUnbuildAssemblies = invBuildUnbuildAssemblyHome.getBuaByCriteria(jbossQl.toString(), obj);	         
		  
		  if (invBuildUnbuildAssemblies.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  return new Integer(invBuildUnbuildAssemblies.size());
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableArInvoiceBatch(Integer AD_CMPNY) {

        Debug.print("ApFindVoucherControllerBean getAdPrfEnableArInvoiceBatch");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfEnableArInvoiceBatch();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
     
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfArUseCustomerPulldown(Integer AD_CMPNY) {
    	
    	Debug.print("InvFindBuildUnbuildAssemblyControllerBean getAdPrfArUseCustomerPulldown");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;         
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		return adPreference.getPrfArUseCustomerPulldown();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    
    
    private void addInvBlEntry(LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLineCopy, 
            LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly, Integer AD_CMPNY) 
            		throws GlobalMiscInfoIsRequiredException {
        
        Debug.print("InvBuildUnbuildAssemblyOrderEntryControllerBean addInvBlEntry");
        
        LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class); 
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class); 
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
        
        try {
            
        	LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = invBuildUnbuildAssemblyLineHome.create(
        			invBuildUnbuildAssemblyLineCopy.getBlBuildQuantity(), invBuildUnbuildAssemblyLineCopy.getBlBuildSpecificGravity(), invBuildUnbuildAssemblyLineCopy.getBlBuildStandardFillSize(), null, AD_CMPNY);
            
            //invBuildUnbuildAssembly.addInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
            invBuildUnbuildAssemblyLine.setInvBuildUnbuildAssembly(invBuildUnbuildAssembly);
            
            LocalInvUnitOfMeasure invUnitOfMeasure = 
                invUnitOfMeasureHome.findByUomName(invBuildUnbuildAssemblyLineCopy.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            //invUnitOfMeasure.addInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
            invBuildUnbuildAssemblyLine.setInvUnitOfMeasure(invUnitOfMeasure);
            
            LocalInvItemLocation invItemLocation =
                invItemLocationHome.findByLocNameAndIiName(
                		invBuildUnbuildAssemblyLineCopy.getInvItemLocation().getInvLocation().getLocName(),
                		invBuildUnbuildAssemblyLineCopy.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);
            //invItemLocation.addInvBuildUnbuildAssemblyLine(invBuildUnbuildAssemblyLine);
            invBuildUnbuildAssemblyLine.setInvItemLocation(invItemLocation);

            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }	
    
    
private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyOrderEntryControllerBean convertByUomFromAndItemAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
             
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }	
    
    public String checkExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	
    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}
    	
    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	
    	
    	String miscList = new String();
    	String miscList2 = "";
    	
    	for(int x=0; x<qty; x++) {
    		
    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		String g= misc.substring(start, start + length);
    		System.out.println("g: " + g);
    		System.out.println("g length: " + g.length());
    		if(g.length()!=0){
    			if(g!=null || g!="" || g!="null"){
    				if(g.contains("null")){
    					miscList2 = "Error";
    				}else{
    					miscList = miscList + "$" + g;
    				}
    			}else{
    				miscList2 = "Error";
    			}
    				
    			System.out.println("miscList G: " + miscList);
    		}else{
    			System.out.println("KABOOM");
    			miscList2 = "Error";
    		}
    	}	
    	System.out.println("miscList2 :" + miscList2);
    	if(miscList2==""){
    		miscList = miscList+"$";
    	}else{
    		miscList = miscList2;
    	}
    	
    	System.out.println("miscList :" + miscList);
    	return (miscList);
    }
    
    
    public String getQuantityExpiryDates(String qntty){
    	String separator = "$";

    	// Remove first $ character
    	qntty = qntty.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = qntty.indexOf(separator, start);
    	int length = nextIndex - start;	
    	String y;
    	y = (qntty.substring(start, start + length));
    	System.out.println("Y " + y);
    	
    	return y;
    }
    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvFindBuildUnbuildAssemblyControllerBean ejbCreate");
      
    }
}
