
/*
 * InvFindItemControllerBean.java
 *
 * Created on June 3, 2004, 10:44 AM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLine;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.ejb.inv.LocalInvStockTransferLine;
import com.ejb.inv.LocalInvStockTransferLineHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModItemDetails;

/**
 * @ejb:bean name="InvFindItemControllerEJB" display-name="Used for finding
 *           items" type="Stateless" view-type="remote"
 *           jndi-name="ejb/InvFindItemControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindItemController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindItemControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser" role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 */

public class InvFindItemControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(String CST_CSTMR_CODE, String II_NM,
			String UOM_NM, Integer AD_CMPNY) {

		Debug.print("InvFindItemControllerBean getInvIiSalesPriceByIiNameAndUomName");

		LocalInvItemHome invItemHome = null;
		LocalInvPriceLevelHome invPriceLevelHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			invPriceLevelHome = (LocalInvPriceLevelHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
			arCustomerHome = (LocalArCustomerHome) EJBHomeFactory.lookUpLocalHome(LocalArCustomerHome.JNDI_NAME,
					LocalArCustomerHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalArCustomer arCustomer = null;

			try {

				arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

			} catch (FinderException ex) {

				// return 0d;

			}

			double unitPrice = 0d;

			LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

			LocalInvPriceLevel invPriceLevel = null;
			if (arCustomer != null) {

				try {

					invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(II_NM, arCustomer.getCstDealPrice(),
							AD_CMPNY);

					if (invPriceLevel.getPlAmount() == 0) {

						unitPrice = invItem.getIiSalesPrice();

					} else {

						unitPrice = invPriceLevel.getPlAmount();

					}

				} catch (FinderException ex) {

					unitPrice = invItem.getIiSalesPrice();
				}

			} else {

				unitPrice = invItem.getIiSalesPrice();
			}

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome
					.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
			LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
					.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

			return EJBCommon.roundIt(unitPrice * invDefaultUomConversion.getUmcConversionFactor()
					/ invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public String getIiDealPriceByInvCstCustomerCodeAndIiNameAndUomName(String CST_CSTMR_CODE, String II_NM,
			String UOM_NM, Integer AD_CMPNY) {

		Debug.print("InvFindItemControllerBean getIiDealPriceByInvCstCustomerCodeAndIiNameAndUomName");

		LocalInvItemHome invItemHome = null;
		LocalInvPriceLevelHome invPriceLevelHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			invPriceLevelHome = (LocalInvPriceLevelHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
			arCustomerHome = (LocalArCustomerHome) EJBHomeFactory.lookUpLocalHome(LocalArCustomerHome.JNDI_NAME,
					LocalArCustomerHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		String dealPrice = "";

		try {

			LocalArCustomer arCustomer = null;

			try {

				arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CODE, AD_CMPNY);

			} catch (FinderException ex) {

				// return 0d;

			}

			LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

			LocalInvPriceLevel invPriceLevel = null;
			if (arCustomer != null) {

				try {

					invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(II_NM, arCustomer.getCstDealPrice(),
							AD_CMPNY);

					dealPrice = arCustomer.getCstDealPrice();

				} catch (FinderException ex) {

				}

			}

			return dealPrice;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public double getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(String II_NM, String LOC_NM, String UOM_NM,
			Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("InvFindItemControllerBean getInvCstRemainingQuantityByIiNameAndLocNameAndUomName");

		LocalInvCostingHome invCostingHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		InvRepItemCostingControllerHome homeRIC = null;
		InvRepItemCostingController ejbRIC = null;
		// Initialize EJB Home

		try {

			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,
					LocalAdPreferenceHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

			homeRIC = (InvRepItemCostingControllerHome) com.util.EJBHomeFactory
					.lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {
			ejbRIC = homeRIC.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LocalInvItemLocation invItemLocation = null;
		LocalInvCosting invCosting = null;

		double QTY = 0d;

		try {

			try {

				invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);

				HashMap criteria = new HashMap();
				criteria.put("itemName", II_NM);
				criteria.put("location", LOC_NM);

				ArrayList branchList = new ArrayList();

				AdBranchDetails mdetails = new AdBranchDetails();
				mdetails.setBrCode(AD_BRNCH);
				branchList.add(mdetails);

				ejbRIC.executeInvFixItemCosting(criteria, branchList, AD_CMPNY);

				invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(II_NM, LOC_NM,
						AD_BRNCH, AD_CMPNY);

				QTY = invCosting.getCstRemainingQuantity();

			} catch (FinderException ex) {

				if (invCosting == null && invItemLocation != null && invItemLocation.getIlCommittedQuantity() > 0) {

					QTY = 0d;

				} else {

					return QTY;

				}

			}

			// double UNPSTD_QTY =
			// this.getShUnpostedQuantity(invItemLocation.getInvItem().getIiName(),invItemLocation.getInvLocation().getLocName(),invItemLocation.getInvLocation().getLocCode(),
			// AD_BRNCH, AD_CMPNY);

			//
			// QTY += UNPSTD_QTY;

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome
					.findUmcByIiNameAndUomName(invItemLocation.getInvItem().getIiName(), invUnitOfMeasure.getUomName(),
							AD_CMPNY);
			LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome
					.findUmcByIiNameAndUomName(invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);
			System.out.println(QTY + " the quantity");
			System.out.println("Final count is : " + QTY * invUnitOfMeasureConversion.getUmcConversionFactor()
					/ invDefaultUomConversion.getUmcConversionFactor());

			return EJBCommon.roundIt(
					QTY * invUnitOfMeasureConversion.getUmcConversionFactor()
							/ invDefaultUomConversion.getUmcConversionFactor(),
					adPreference.getPrfInvQuantityPrecisionUnit());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {

		Debug.print("InvLocationEntryControllerBean getAdLvInvItemCategoryAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvIiByCriteria(HashMap criteria, Integer OFFSET, Integer LIMIT, String ORDER_BY,
			Integer AD_BRNCH, Integer AD_CMPNY) throws GlobalNoRecordFoundException {

		Debug.print("InvFindItemControllerBean getInvIiByCriteria");

		LocalInvItemHome invItemHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalInvLocationHome invLocationHome = null;

		ArrayList list = new ArrayList();

		// initialized EJB Home

		try {

			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invLocationHome = (LocalInvLocationHome) EJBHomeFactory.lookUpLocalHome(LocalInvLocationHome.JNDI_NAME,
					LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			StringBuffer jbossQl = new StringBuffer();
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size() + 2;

			Object obj[] = null;

			/*
			 * if (AD_BRNCH == null){
			 * 
			 * jbossQl.append("SELECT OBJECT(ii) FROM InvItem ii ");
			 * 
			 * } else {
			 * 
			 * jbossQl.
			 * append("SELECT DISTINCT OBJECT(ii) FROM InvItem ii, IN(ii.invItemLocations) AS il,  IN(il.adBranchItemLocations) AS bil "
			 * );
			 * 
			 * }
			 */

			jbossQl.append("SELECT OBJECT(ii) FROM InvItem ii ");

			// Allocate the size of the object parameter

			if (criteria.containsKey("customer")) {

				criteriaSize--;

			}

			if (criteria.containsKey("itemName")) {

				criteriaSize--;

			}

			if (criteria.containsKey("itemClass")) {

				criteriaSize--;

			}

			if (criteria.containsKey("category")) {

				criteriaSize--;

			}

			if (criteria.containsKey("costMethod")) {

				criteriaSize--;

			}

			if (criteria.containsKey("itemDescription")) {

				criteriaSize--;

			}

			obj = new Object[criteriaSize];

			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append(" WHERE ");

				}

				jbossQl.append("ii.iiName LIKE '%" + (String) criteria.get("itemName") + "%' ");

			}

			if (criteria.containsKey("itemClass")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append(" WHERE ");

				}

				jbossQl.append("ii.iiClass LIKE '%" + (String) criteria.get("itemClass") + "%' ");

			}

			if (criteria.containsKey("category")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}

				jbossQl.append("ii.iiAdLvCategory = '" + (String) criteria.get("category") + "' ");

			}

			if (criteria.containsKey("enable") && criteria.containsKey("disable")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}

				Byte enable = (Byte) criteria.get("enable");
				Byte disable = (Byte) criteria.get("disable");

				System.out.println("criteria.get(enable)" + enable);
				System.out.println("criteria.get(disable)" + disable);

				if (enable == EJBCommon.TRUE && disable == EJBCommon.TRUE) {

					jbossQl.append("(ii.iiEnable=?" + (ctr + 1) + " OR ");
					obj[ctr] = new Byte(EJBCommon.TRUE);
					ctr++;

					jbossQl.append("ii.iiEnable=?" + (ctr + 1) + ") ");
					obj[ctr] = new Byte(EJBCommon.FALSE);
					ctr++;

				} else {

					jbossQl.append("(ii.iiEnable=?" + (ctr + 1) + " OR ");
					obj[ctr] = new Byte(EJBCommon.TRUE);
					ctr++;

					jbossQl.append("ii.iiEnable=?" + (ctr + 1) + ") ");
					obj[ctr] = new Byte(EJBCommon.TRUE);
					ctr++;

				}

			} else {

				if (criteria.containsKey("enable")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append(" WHERE ");
					}
					jbossQl.append("ii.iiEnable=?" + (ctr + 1) + " ");
					obj[ctr] = new Byte(EJBCommon.TRUE);
					ctr++;

				}

				if (criteria.containsKey("disable")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append(" WHERE ");
					}
					jbossQl.append("ii.iiEnable=?" + (ctr + 1) + " ");
					obj[ctr] = new Byte(EJBCommon.FALSE);
					ctr++;

				}

			}

			if (criteria.containsKey("ScMonday")) {
				System.out.println("1contains MONDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScMonday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScTuesday")) {
				System.out.println("1contains TUESDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScTuesday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScWednesday")) {
				System.out.println("1contains WEDNESDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScWednesday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScThursday")) {
				System.out.println("1contains THURSDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScThursday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScFriday")) {
				System.out.println("1contains FRIDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScFriday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScSaturday")) {
				System.out.println("1contains SATURDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScSaturday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScSunday")) {
				System.out.println("1contains SUNDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScSunday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("prType")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiNonInventoriable=?" + (ctr + 1) + " ");
				obj[ctr] = (Byte) criteria.get("prType");
				ctr++;

			}

			/*
			 * if (AD_BRNCH != null) {
			 * 
			 * if (!firstArgument) {
			 * 
			 * jbossQl.append("AND ");
			 * 
			 * } else {
			 * 
			 * firstArgument = false; jbossQl.append(" WHERE ");
			 * 
			 * }
			 * 
			 * jbossQl.append("bil.adBranch.brCode=" + AD_BRNCH + " ");
			 * 
			 * }
			 */

			if (criteria.containsKey("itemDescription")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append(" WHERE ");

				}

				jbossQl.append("ii.iiDescription LIKE '%" + (String) criteria.get("itemDescription") + "%' ");

			}

			if (criteria.containsKey("partNumber")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append(" WHERE ");

				}

				jbossQl.append("ii.iiPartNumber=?" + (ctr + 1) + " ");
				obj[ctr] = (String) criteria.get("partNumber");
				ctr++;

			}

			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append(" WHERE ");

			}

			jbossQl.append("ii.iiAdCompany=" + AD_CMPNY + " ");

			String orderBy = null;

			if (ORDER_BY.equals("ITEM CLASS")) {

				orderBy = "ii.iiClass";

			} else if (ORDER_BY.equals("ITEM CATEGORY")) {

				orderBy = "ii.iiAdLvCategory";

			} else if (ORDER_BY.equals("ITEM NAME")) {

				orderBy = "ii.iiName";

			} else {

				orderBy = "ii.iiName";

			}

			jbossQl.append("ORDER BY " + orderBy);

			jbossQl.append(" OFFSET ?" + (ctr + 1));
			obj[ctr] = OFFSET;
			ctr++;

			jbossQl.append(" LIMIT ?" + (ctr + 1));
			obj[ctr] = LIMIT;
			ctr++;
			System.out.println(jbossQl.toString() + "<== query");
			Collection invItems = invItemHome.getIiByCriteria(jbossQl.toString(), obj);

			String customerCode = (String) criteria.get("customer");

			if (invItems.size() == 0)
				throw new GlobalNoRecordFoundException();

			Iterator i = invItems.iterator();

			while (i.hasNext()) {

				LocalInvItem invItem = (LocalInvItem) i.next();

				InvModItemDetails mdetails = new InvModItemDetails();
				mdetails.setIiCode(invItem.getIiCode());
				mdetails.setIiName(invItem.getIiName());
				mdetails.setIiDescription(invItem.getIiDescription());
				mdetails.setIiClass(invItem.getIiClass());
				mdetails.setIiPartNumber(invItem.getIiPartNumber());
				mdetails.setIiShortName(invItem.getIiShortName());
				mdetails.setIiBarCode1(invItem.getIiBarCode1());
				mdetails.setIiBarCode2(invItem.getIiBarCode2());
				mdetails.setIiBarCode3(invItem.getIiBarCode3());
				mdetails.setIiUomName(invItem.getInvUnitOfMeasure().getUomName());
				mdetails.setIiAdLvCategory(invItem.getIiAdLvCategory());
				mdetails.setIiCostMethod(invItem.getIiCostMethod());
				mdetails.setIiUnitCost(invItem.getIiUnitCost());
				mdetails.setIiPercentMarkup(invItem.getIiPercentMarkup());

				mdetails.setIiEnable(invItem.getIiEnable());

				String defaultLocation = null;
				if (invItem.getIiDefaultLocation() != null) {
					LocalInvLocation invLocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
					defaultLocation = invLocation.getLocName();
				} else {
					Collection invLocations = invLocationHome.findLocAll(AD_CMPNY);
					Iterator j = invLocations.iterator();
					if (j.hasNext()) {
						LocalInvLocation invLocation = (LocalInvLocation) j.next();
						defaultLocation = invLocation.getLocName();
					}
				}
				mdetails.setIiDefaultLocationName(defaultLocation);
				double quantityOnHand = 0d;
				try {
					quantityOnHand = this.getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(invItem.getIiName(),
							defaultLocation, invItem.getInvUnitOfMeasure().getUomName(), AD_BRNCH, AD_CMPNY);

				} catch (Exception ex) {
					quantityOnHand = 0d;
				}

				double salesPriceCustomer = this.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(customerCode,
						invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

				mdetails.setIiSalesPrice(salesPriceCustomer);

				String dealPrice = this.getIiDealPriceByInvCstCustomerCodeAndIiNameAndUomName(customerCode,
						invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

				mdetails.setIiDealPrice(dealPrice);

				// double unpostedQty = this.getShUnpostedQuantity(adBranchItemLocation,
				// AS_OF_DT, AD_CMPNY)

				mdetails.setIiQuantityOnHand(quantityOnHand);

				/*
				 * if (criteria.containsKey("branchCode")) {
				 * 
				 * Collection adBranchItemLocations =
				 * adBranchItemLocationHome.findBilByIiCodeAndBrCode(invItem.getIiCode(),(
				 * Integer)criteria.get("branchCode"),AD_CMPNY );
				 * 
				 * if (adBranchItemLocations == null) continue loopItem;
				 * 
				 * }
				 */

				list.add(mdetails);

			}

			return list;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			ex.printStackTrace();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public Integer getInvIiSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY)
			throws GlobalNoRecordFoundException {

		Debug.print("InvFindItemControllerBean getInvIiSizeByCriteria");

		LocalInvItemHome invItemHome = null;

		// initialized EJB Home

		try {

			invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
					LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			StringBuffer jbossQl = new StringBuffer();

			/*
			 * if (AD_BRNCH == null){
			 * 
			 * jbossQl.append("SELECT OBJECT(ii) FROM InvItem ii ");
			 * 
			 * } else {
			 * 
			 * jbossQl.
			 * append("SELECT DISTINCT OBJECT(ii) FROM InvItem ii, IN(ii.invItemLocations) AS il,  IN(il.adBranchItemLocations) AS bil "
			 * );
			 * 
			 * }
			 */

			jbossQl.append("SELECT OBJECT(ii) FROM InvItem ii ");

			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();

			Object obj[] = null;

			// Allocate the size of the object parameter

			if (criteria.containsKey("customer")) {

				criteriaSize--;

			}

			if (criteria.containsKey("itemName")) {

				criteriaSize--;

			}

			if (criteria.containsKey("itemClass")) {

				criteriaSize--;

			}

			if (criteria.containsKey("category")) {

				criteriaSize--;

			}

			if (criteria.containsKey("costMethod")) {

				criteriaSize--;

			}

			if (criteria.containsKey("itemDescription")) {

				criteriaSize--;

			}

			obj = new Object[criteriaSize];

			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}

				jbossQl.append("ii.iiName LIKE '%" + (String) criteria.get("itemName") + "%' ");

			}

			if (criteria.containsKey("itemClass")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append(" WHERE ");

				}

				jbossQl.append("ii.iiClass LIKE '%" + (String) criteria.get("itemClass") + "%' ");

			}

			if (criteria.containsKey("category")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}

				jbossQl.append("ii.iiAdLvCategory = '" + (String) criteria.get("category") + "' ");

			}

			if (criteria.containsKey("enable") && criteria.containsKey("disable")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}

				Byte enable = (Byte) criteria.get("enable");
				Byte disable = (Byte) criteria.get("disable");

				System.out.println("criteria.get(enable)" + enable);
				System.out.println("criteria.get(disable)" + disable);

				if (enable == EJBCommon.TRUE && disable == EJBCommon.TRUE) {

					jbossQl.append("(ii.iiEnable=?" + (ctr + 1) + " OR ");
					obj[ctr] = new Byte(EJBCommon.TRUE);
					ctr++;

					jbossQl.append("ii.iiEnable=?" + (ctr + 1) + ") ");
					obj[ctr] = new Byte(EJBCommon.FALSE);
					ctr++;

				} else {

					jbossQl.append("(ii.iiEnable=?" + (ctr + 1) + " OR ");
					obj[ctr] = new Byte(EJBCommon.TRUE);
					ctr++;

					jbossQl.append("ii.iiEnable=?" + (ctr + 1) + ") ");
					obj[ctr] = new Byte(EJBCommon.TRUE);
					ctr++;

				}

			} else {

				if (criteria.containsKey("enable")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append(" WHERE ");
					}
					jbossQl.append("ii.iiEnable=?" + (ctr + 1) + " ");
					obj[ctr] = new Byte(EJBCommon.TRUE);
					ctr++;

				}

				if (criteria.containsKey("disable")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append(" WHERE ");
					}
					jbossQl.append("ii.iiEnable=?" + (ctr + 1) + " ");
					obj[ctr] = new Byte(EJBCommon.FALSE);
					ctr++;

				}

			}

			if (criteria.containsKey("ScMonday")) {
				System.out.println("1contains MONDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScMonday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScTuesday")) {
				System.out.println("1contains TUESDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScTuesday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScWednesday")) {
				System.out.println("1contains WEDNESDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScWednesday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScThursday")) {
				System.out.println("1contains THURSDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScThursday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScFriday")) {
				System.out.println("1contains FRIDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScFriday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScSaturday")) {
				System.out.println("1contains SATURDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScSaturday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			if (criteria.containsKey("ScSunday")) {
				System.out.println("1contains SUNDAY");
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append(" WHERE ");
				}
				jbossQl.append("ii.iiScSunday=?" + (ctr + 1) + " ");
				obj[ctr] = new Byte(EJBCommon.TRUE);
				ctr++;

			}

			/*
			 * if (AD_BRNCH != null) {
			 * 
			 * if (!firstArgument) {
			 * 
			 * jbossQl.append("AND ");
			 * 
			 * } else {
			 * 
			 * firstArgument = false; jbossQl.append(" WHERE ");
			 * 
			 * }
			 * 
			 * jbossQl.append("bil.adBranch.brCode=" + AD_BRNCH + " ");
			 * 
			 * }
			 */

			if (criteria.containsKey("itemDescription")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append(" WHERE ");

				}

				jbossQl.append("ii.iiDescription LIKE '%" + (String) criteria.get("itemDescription") + "%' ");

			}

			if (criteria.containsKey("partNumber")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append(" WHERE ");

				}

				jbossQl.append("ii.iiPartNumber=?" + (ctr + 1) + " ");
				obj[ctr] = (String) criteria.get("partNumber");
				ctr++;

			}

			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append(" WHERE ");

			}

			jbossQl.append("ii.iiAdCompany=" + AD_CMPNY + " ");

			Collection invItems = invItemHome.getIiByCriteria(jbossQl.toString(), obj);

			if (invItems.size() == 0)
				throw new GlobalNoRecordFoundException();

			return new Integer(invItems.size());

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			ex.printStackTrace();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("InvItemEntryControllerBean getGlFcPrecisionUnit");

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private double getShUnpostedQuantity(String II_NM, String IL_LOC_NM, Integer IL_LOC_CODE, Integer AD_BRNCH,
			Integer AD_CMPNY) {
		Debug.print("InvItemEntryControllerBean getUnpostedQuantity");

		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
		LocalInvStockTransferLineHome invStockTransferLineHome = null;
		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;

		double totalUnpostedQuantity = 0d;

		// Initialize EJB Home
		System.out.println("CheckPoint executeInvRepStockOnHand K");
		try {

			apVoucherLineItemHome = (LocalApVoucherLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
			invStockTransferLineHome = (LocalInvStockTransferLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class);
			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);

		} catch (NamingException ex) {
			throw new EJBException(ex.getMessage());
		}
		System.out.println("CheckPoint executeInvRepStockOnHand L");
		try {
			double unpostedQuantity = 0d;

			String itemName = II_NM;
			String locationName = IL_LOC_NM;
			Integer locationCode = IL_LOC_CODE;
			Integer branchCode = AD_BRNCH;
			Date AS_OF_DT = new Date();

			// (1) AP Voucher Line Item - AP Voucher
			byte debitMemo;

			Collection apVoucherLineItems = apVoucherLineItemHome
					.findUnpostedVouByIiNameAndLocNameAndLessEqualDateAndVouAdBranch(itemName, locationName, AS_OF_DT,
							branchCode, AD_CMPNY);

			Iterator x = apVoucherLineItems.iterator();

			while (x.hasNext()) {

				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) x.next();

				unpostedQuantity = apVoucherLineItem.getVliQuantity();

				System.out.println("(1) Name: " + apVoucherLineItem.getInvItemLocation().getInvItem().getIiName()
						+ "\tUnposted Quantity: " + unpostedQuantity);

				debitMemo = apVoucherLineItem.getApVoucher().getVouDebitMemo();

				if (debitMemo == 0) {
					totalUnpostedQuantity += unpostedQuantity;
				} else {
					totalUnpostedQuantity -= unpostedQuantity;
				}
			}

			// (2) AP Voucher Line Item - AP Check
			apVoucherLineItems = apVoucherLineItemHome.findUnpostedChkByIiNameAndLocNameAndLessEqualDateAndChkAdBranch(
					itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);

			x = apVoucherLineItems.iterator();

			while (x.hasNext()) {

				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) x.next();

				unpostedQuantity = apVoucherLineItem.getVliQuantity();

				System.out.println("(2) Name: " + apVoucherLineItem.getInvItemLocation().getInvItem().getIiName()
						+ "\tUnposted Quantity: " + unpostedQuantity);

				totalUnpostedQuantity += unpostedQuantity;

			}

			// (3) AP PURCHASE ORDER LINE
			Collection apPurchaseOrderLines = apPurchaseOrderLineHome
					.findUnpostedPoByIiNameAndLocNameAndLessEqualDateAndPoAdBranch(itemName, locationName, AS_OF_DT,
							branchCode, AD_CMPNY);

			x = apPurchaseOrderLines.iterator();

			while (x.hasNext()) {

				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine) x.next();

				unpostedQuantity = apPurchaseOrderLine.getPlQuantity();

				System.out.println("(3) Name: " + apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName()
						+ "\tUnposted Quantity: " + unpostedQuantity);

				totalUnpostedQuantity += unpostedQuantity;

			}

			// (4) AR Invoice Line Item - AR Invoice
			byte creditMemo;

			Collection arInvoiceLineItems = arInvoiceLineItemHome
					.findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(itemName, locationName, AS_OF_DT,
							branchCode, AD_CMPNY);

			x = arInvoiceLineItems.iterator();

			while (x.hasNext()) {

				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) x.next();

				unpostedQuantity = arInvoiceLineItem.getIliQuantity();

				System.out.println("(4) Name: " + arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName()
						+ "\tUnposted Quantity: " + unpostedQuantity);

				creditMemo = arInvoiceLineItem.getArInvoice().getInvCreditMemo();

				if (creditMemo == 0) {
					totalUnpostedQuantity -= unpostedQuantity;
				} else {
					totalUnpostedQuantity += unpostedQuantity;
				}
			}

			// (5) AR Invoice Line Item - AR Receipt
			arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedRctByIiNameAndLocNameAndLessEqualDateAndRctAdBranch(
					itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);

			x = arInvoiceLineItems.iterator();

			while (x.hasNext()) {

				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) x.next();

				unpostedQuantity = arInvoiceLineItem.getIliQuantity();

				System.out.println("(5) Name: " + arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName()
						+ "\tUnposted Quantity: " + unpostedQuantity);

				totalUnpostedQuantity -= unpostedQuantity;

			}

			// (6)AR Sales Order Invoice Line
			Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome
					.findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(itemName, locationName, AS_OF_DT,
							branchCode, AD_CMPNY);

			x = arSalesOrderInvoiceLines.iterator();

			while (x.hasNext()) {
				LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine) x.next();

				unpostedQuantity = arSalesOrderInvoiceLine.getSilQuantityDelivered();

				System.out.println("(6) Name: "
						+ arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiName()
						+ "\tUnposted Quantity: " + unpostedQuantity);

				totalUnpostedQuantity -= unpostedQuantity;

			}

			// (7) INV Adjustment Line
			Collection invAdjustmentLines = invAdjustmentLineHome
					.findUnpostedAdjByIiNameAndLocNameAndLessEqualDateAndAdjAdBranch(itemName, locationName, AS_OF_DT,
							branchCode, AD_CMPNY);

			x = invAdjustmentLines.iterator();

			while (x.hasNext()) {

				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) x.next();

				unpostedQuantity = invAdjustmentLine.getAlAdjustQuantity();

				System.out.println("(7) Name: " + invAdjustmentLine.getInvItemLocation().getInvItem().getIiName()
						+ "\tUnposted Quantity: " + unpostedQuantity);

				totalUnpostedQuantity += unpostedQuantity;
			}

			// (8) INV Build-Unbuild Assembly
			Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssemblyLineHome
					.findUnpostedBuaByIiNameAndLocNameAndLessEqualDateAndBuaAdBranch(itemName, locationName, AS_OF_DT,
							branchCode, AD_CMPNY);

			x = invBuildUnbuildAssemblyLines.iterator();

			while (x.hasNext()) {

				LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine) x
						.next();

				unpostedQuantity = invBuildUnbuildAssemblyLine.getBlBuildQuantity();

				System.out.println(
						"(8) Name: " + invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName()
								+ "\tUnposted Quantity: " + unpostedQuantity);

				if (unpostedQuantity >= 0) {
					totalUnpostedQuantity += unpostedQuantity;
				} else if (unpostedQuantity < 0) {
					totalUnpostedQuantity -= unpostedQuantity;
				}
			}

			// (8.1) INV Build-Unbuild Assembly
			Collection invBuildUnbuildAssemblyLines2 = invBuildUnbuildAssemblyLineHome
					.findUnpostedBuaByIiNameAndLocNameAndLessEqualDateAndBuaAdBranch(itemName, locationName, AS_OF_DT,
							branchCode, AD_CMPNY);

			x = invBuildUnbuildAssemblyLines.iterator();

			while (x.hasNext()) {

				LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine) x
						.next();

				Collection invBillofMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem()
						.getInvBillOfMaterials();

				Iterator y = invBillofMaterials.iterator();

				while (y.hasNext()) {

					LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) y.next();

					double qtyNeeded = invBillOfMaterial.getBomQuantityNeeded();

					unpostedQuantity = (invBuildUnbuildAssemblyLine.getBlBuildQuantity()) * qtyNeeded / 100;

					if (unpostedQuantity >= 0) {
						totalUnpostedQuantity += unpostedQuantity;
					} else if (unpostedQuantity < 0) {
						totalUnpostedQuantity -= unpostedQuantity;
					}

				}

			}

			// (9) INV Stock Transfer Line
			// Series of System.out's were executed in order to trace the ADDITION and
			// DEDUCTION of unposted quantities for each item.
			Collection invStockTransferLines = invStockTransferLineHome
					.findUnpostedStlByIiNameAndLocCodeAndLessEqualDateAndStAdBranch(itemName, locationCode, AS_OF_DT,
							branchCode, AD_CMPNY);

			x = invStockTransferLines.iterator();

			while (x.hasNext()) {

				LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) x.next();

				unpostedQuantity = invStockTransferLine.getStlQuantityDelivered();

				System.out.println("(9) Item Name: " + invStockTransferLine.getInvItem().getIiName()
						+ "\tUnposted Quantity: " + unpostedQuantity);
				System.out.println("Location Code" + locationCode);
				System.out.println("Location TO: " + invStockTransferLine.getStlLocationTo());
				System.out.println("Location FROM: " + invStockTransferLine.getStlLocationFrom());

				if (invStockTransferLine.getStlLocationTo().equals(locationCode)) {
					System.out.println("(9) Before Adding: Name: " + invStockTransferLine.getInvItem().getIiName()
							+ "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity"
							+ totalUnpostedQuantity);
					totalUnpostedQuantity += unpostedQuantity;
					System.out.println("(9) After Adding: Name: " + invStockTransferLine.getInvItem().getIiName()
							+ "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity"
							+ totalUnpostedQuantity);
				} else if (invStockTransferLine.getStlLocationFrom().equals(locationCode)) {
					System.out.println("(9) Before Deducting: Name: " + invStockTransferLine.getInvItem().getIiName()
							+ "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity"
							+ totalUnpostedQuantity);
					totalUnpostedQuantity -= unpostedQuantity;
					System.out.println("(9) After Deducting: Name: " + invStockTransferLine.getInvItem().getIiName()
							+ "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity"
							+ totalUnpostedQuantity);
				}
			}

			// (10) INV Branch Stock Transfer Line
			String bstType = "";

			Collection invBranchStockTransferLines = invBranchStockTransferLineHome
					.findUnpostedBstByIiNameAndLocNameAndLessEqualDateAndBstAdBranch(itemName, locationName, AS_OF_DT,
							branchCode, AD_CMPNY);

			x = invBranchStockTransferLines.iterator();

			while (x.hasNext()) {

				LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) x.next();

				unpostedQuantity = invBranchStockTransferLine.getBslQuantity();

				bstType = invBranchStockTransferLine.getInvBranchStockTransfer().getBstType();

				System.out.println(
						"(10) Name: " + invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName()
								+ "\tUnposted Quantity: " + unpostedQuantity);

				if (bstType.equals("IN")) {
					totalUnpostedQuantity += unpostedQuantity;
				} else if (bstType.equals("OUT")) {
					totalUnpostedQuantity -= unpostedQuantity;
				}
			}

		} catch (Exception e) {
			System.out.println(e);
		}

		return totalUnpostedQuantity;

	}

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("InvFindItemControllerBean ejbCreate");

	}
}
