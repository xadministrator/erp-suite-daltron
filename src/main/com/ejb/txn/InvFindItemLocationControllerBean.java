
/*
 * InvFindItemLocationControllerBean.java
 *
 * Created on June 8, 2004 10:26 AM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModBranchItemLocationDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvModItemLocationDetails;

/**
 * @ejb:bean name="InvFindItemLocationControllerEJB"
 *           display-name="Used for finding item locations"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvFindItemLocationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindItemLocationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindItemLocationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
 */

public class InvFindItemLocationControllerBean extends AbstractSessionBean {
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvFindItemLocationControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvIiAll(Integer AD_CMPNY) {
		
		Debug.print("InvFindItemLocationControllerBean getInvIiAll");
		
		LocalInvItemHome invItemHome = null;
		LocalInvItem invItem = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection invItems = invItemHome.findEnabledIiAll(AD_CMPNY);
			
			Iterator i = invItems.iterator();
			
			while (i.hasNext()) {
				
				invItem = (LocalInvItem)i.next();
				
				list.add(invItem.getIiName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvFindItemLocationControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		LocalInvLocation invLocation = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection invLocations = invLocationHome.findLocAll(AD_CMPNY);
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				invLocation = (LocalInvLocation)i.next();
				
				list.add(invLocation.getLocName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}   
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfInvQuantityPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("InvFindItemLocationControllerBean getAdPrfInvQuantityPrecisionUnit");
		
		LocalAdPreferenceHome adPreferenceHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfInvQuantityPrecisionUnit();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}		    
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("InvFindItemLocationControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }

	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvIlByCriteria(HashMap criteria, ArrayList branchList, 
			Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException {
		
		Debug.print("InvFindItemLocationControllerBean getInvIlByCriteria");
		
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;

		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			
			if(branchList.size() > 0) {
				
				jbossQl.append("SELECT DISTINCT OBJECT(il) FROM InvItemLocation il, IN(il.adBranchItemLocations)bil ");
				
			} else {
				
				jbossQl.append("SELECT OBJECT(il) FROM InvItemLocation il ");
				
			}
			
			boolean firstArgument = true;
			short ctr = 0;	      
			int criteriaSize = criteria.size() + 2 + branchList.size();
			
			Object obj[] = null;		
			
			if (criteria.containsKey("category")) {
        		
        		criteriaSize--;
        		
        	}

			if (criteria.containsKey("itemName")) {
        		
        		criteriaSize--;
        		
        	} 
			
			if (criteria.containsKey("itemDescription")) {
        		
        		criteriaSize--;
        		
        	} 
			
			obj = new Object[criteriaSize];
			
			
			if (criteria.containsKey("category")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append(" WHERE ");
        		}
        		
        		jbossQl.append("il.invItem.iiAdLvCategory = '" + (String)criteria.get("category") + "' ");
        		
        		
        	}
			
			if (criteria.containsKey("itemName")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("il.invItem.iiName LIKE '%" + (String)criteria.get("itemName") + "%' ");		
        		
        	}
			
			if (criteria.containsKey("itemDescription")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("il.invItem.iiDescription LIKE '%" + (String)criteria.get("itemDescription") + "%' ");		
        		
        	}
			
			if (criteria.containsKey("locationName")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("il.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("locationName");
				ctr++;
				
			}
			
			if(branchList.size() > 0) {
			
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				AdModBranchItemLocationDetails details = null;
				
				Iterator i = branchList.iterator();
				
				details = (AdModBranchItemLocationDetails)i.next();
				
				jbossQl.append("(bil.adBranch.brCode=?" + (ctr+1) + " ");
				obj[ctr] = (Integer)details.getBilBrCode();
				ctr++;
				
				while(i.hasNext()) {
					
					details = (AdModBranchItemLocationDetails)i.next();
					
					jbossQl.append("OR bil.adBranch.brCode=?" + (ctr+1) + " ");
					obj[ctr] = (Integer)details.getBilBrCode();
					ctr++;
						
				}
				
				jbossQl.append(") ");
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("il.ilAdCompany=" + AD_CMPNY + " ");
			
			String orderBy = null;
			
			if (ORDER_BY.equals("ITEM NAME")) {          
				
				orderBy = "il.invItem.iiName";
				
			} else {
				
				orderBy = "il.invLocation.locName";
				
			} 
			
			jbossQl.append("ORDER BY " + orderBy);
			
			jbossQl.append(" OFFSET ?" + (ctr + 1));	        
			obj[ctr] = OFFSET;	      
			ctr++;
			
			jbossQl.append(" LIMIT ?" + (ctr + 1));
			obj[ctr] = LIMIT;
			ctr++;		      
			
			Collection invItemLocations = invItemLocationHome.getIlByCriteria(jbossQl.toString(), obj);	         
			
			if (invItemLocations.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			Iterator i = invItemLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvItemLocation invItemLocation = (LocalInvItemLocation)i.next();   	  
				
				LocalGlChartOfAccount glSalesAccount = glChartOfAccountHome.findByPrimaryKey(invItemLocation.getIlGlCoaSalesAccount());
				LocalGlChartOfAccount glInventoryAccount = glChartOfAccountHome.findByPrimaryKey(invItemLocation.getIlGlCoaInventoryAccount());
				LocalGlChartOfAccount glCostOfSalesAccount = glChartOfAccountHome.findByPrimaryKey(invItemLocation.getIlGlCoaCostOfSalesAccount());
				
				InvModItemLocationDetails mdetails = new InvModItemLocationDetails();
				mdetails.setIlCode(invItemLocation.getIlCode());
				mdetails.setIlReorderPoint(invItemLocation.getIlReorderPoint());
				mdetails.setIlReorderQuantity(invItemLocation.getIlReorderQuantity());
				mdetails.setIlIiName(invItemLocation.getInvItem().getIiName());
				mdetails.setIlLocName(invItemLocation.getInvLocation().getLocName());
				mdetails.setIlCoaGlSalesAccountNumber(glSalesAccount.getCoaAccountNumber());
				mdetails.setIlCoaGlSalesAccountDescription(glSalesAccount.getCoaAccountDescription());
				mdetails.setIlCoaGlInventoryAccountNumber(glInventoryAccount.getCoaAccountNumber());
				mdetails.setIlCoaGlInventoryAccountDescription(glInventoryAccount.getCoaAccountDescription());
				mdetails.setIlCoaGlCostOfSalesAccountNumber(glCostOfSalesAccount.getCoaAccountNumber());	
				mdetails.setIlCoaGlCostOfSalesAccountDescription(glCostOfSalesAccount.getCoaAccountDescription());						  	  	  	 
				
				list.add(mdetails);
				
			}
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public Integer getInvIlSizeByCriteria(HashMap criteria, ArrayList branchList, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException {
		
		Debug.print("InvFindItemLocationControllerBean getInvIlSizeByCriteria");
		
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;

		//initialized EJB Home
		
		try {
			
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			
			if(branchList.size() > 0) {
				
				jbossQl.append("SELECT DISTINCT OBJECT(il) FROM InvItemLocation il, IN(il.adBranchItemLocations)bil ");
				
			} else {
				
				jbossQl.append("SELECT OBJECT(il) FROM InvItemLocation il ");
				
			}
			
			
			boolean firstArgument = true;
			short ctr = 0;	      
			int criteriaSize = criteria.size() + branchList.size();			
			Object obj[] = null;
			
			if (criteria.containsKey("category")) {
        		
        		criteriaSize--;
        		
        	}

			if (criteria.containsKey("itemName")) {
        		
        		criteriaSize--;
        		
        	} 
			
			if (criteria.containsKey("itemDescription")) {
        		
        		criteriaSize--;
        		
        	} 
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("category")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append(" WHERE ");
        		}
        		
        		jbossQl.append("il.invItem.iiAdLvCategory = '" + (String)criteria.get("category") + "' ");
        		
        		
        	}


			if (criteria.containsKey("itemName")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("il.invItem.iiName LIKE '%" + (String)criteria.get("itemName") + "%' ");		
        		
        	}
			
			if (criteria.containsKey("itemDescription")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("il.invItem.iiDescription LIKE '%" + (String)criteria.get("itemDescription") + "%' ");		
        		
        	}
			
			if (criteria.containsKey("locationName")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("il.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("locationName");
				ctr++;
				
			}
			
			if(branchList.size() > 0) {
			
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				AdModBranchItemLocationDetails details = null;
				
				Iterator i = branchList.iterator();
				
				details = (AdModBranchItemLocationDetails)i.next();
				
				jbossQl.append("(bil.adBranch.brCode=?" + (ctr+1) + " ");
				obj[ctr] = (Integer)details.getBilBrCode();
				ctr++;
				
				while(i.hasNext()) {
					
					details = (AdModBranchItemLocationDetails)i.next();
					
					jbossQl.append("OR bil.adBranch.brCode=?" + (ctr+1) + " ");
					obj[ctr] = (Integer)details.getBilBrCode();
					ctr++;
						
				}
				
				jbossQl.append(") ");
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("il.ilAdCompany=" + AD_CMPNY + " ");
			
			Collection invItemLocations = invItemLocationHome.getIlByCriteria(jbossQl.toString(), obj);	         
			
			if (invItemLocations.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			return new Integer(invItemLocations.size());
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvFindItemLocationControllerBean ejbCreate");
		
	}
}
