
/*
 * InvFindLocationControllerBean.java
 *
 * Created on June 3, 2004 06:08 PM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvLocationDetails;

/**
 * @ejb:bean name="InvFindLocationControllerEJB"
 *           display-name="Used for finding locations"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvFindLocationControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindLocationController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindLocationControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvFindLocationControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvLocationTypeAll(Integer AD_CMPNY) {
		
		Debug.print("InvLocationEntryControllerBean getAdLvInvLocationTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV LOCATION TYPE", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}		
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindLocationControllerBean getInvLocByCriteria");
        
        LocalInvLocationHome invLocationHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(loc) FROM InvLocation loc ");
		  
		  boolean firstArgument = true;
		  short ctr = 0;
		  int criteriaSize = criteria.size() + 2;
		
		  
		  Object obj[] = null;		      
		
		  // Allocate the size of the object parameter
				   
		  if (criteria.containsKey("locationName")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		  
		  if (criteria.containsKey("type")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		  		  
		  if (criteria.containsKey("contactPerson")) {
			
		  	 criteriaSize--;
		  
		  }
		  
		  obj = new Object[criteriaSize];    
		       	      		
	      if (criteria.containsKey("locationName")) {
	  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
	  	 
	   	     jbossQl.append("loc.locName LIKE '%" + (String)criteria.get("locationName") + "%' ");		
	  	 
	      }
	      
		  if (criteria.containsKey("type")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("loc.locLvType LIKE '%" + (String)criteria.get("type") + "%' ");
		  	 
		  }	       
			
		  if (criteria.containsKey("contactPerson")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 
		   	 jbossQl.append("loc.locContactPerson LIKE '%" + (String)criteria.get("contactPerson") + "%' ");		
		  	 
		  }	
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("loc.locAdCompany=" + AD_CMPNY + " ");
		  
		  jbossQl.append("ORDER BY " + "loc.locName");
		  
		  jbossQl.append(" OFFSET ?" + (ctr + 1));	        
		  obj[ctr] = OFFSET;	      
		  ctr++;
		  
		  jbossQl.append(" LIMIT ?" + (ctr + 1));
		  obj[ctr] = LIMIT;
		  ctr++;		      
			  	      	
	      Collection invLocations = invLocationHome.getLocByCriteria(jbossQl.toString(), obj);	         
		  
		  if (invLocations.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = invLocations.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalInvLocation invLocation = (LocalInvLocation)i.next();   	  
		  	  
		  	  InvLocationDetails details = new InvLocationDetails();
		  	  details.setLocCode(invLocation.getLocCode());
		  	  details.setLocName(invLocation.getLocName());
		  	  details.setLocDescription(invLocation.getLocDescription());
		  	  details.setLocLvType(invLocation.getLocLvType());
		  	  details.setLocAddress(invLocation.getLocAddress());
		  	  details.setLocContactPerson(invLocation.getLocContactPerson());
		  	  details.setLocContactNumber(invLocation.getLocContactNumber());
		  	  details.setLocEmailAddress(invLocation.getLocEmailAddress());
			      	  	      	  		     
			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getInvLocSizeByCriteria(HashMap criteria, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindLocationControllerBean getInvLocSizeByCriteria");
        
        LocalInvLocationHome invLocationHome = null;
        
        //initialized EJB Home
        
        try {
            
        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(loc) FROM InvLocation loc ");
		  
		  boolean firstArgument = true;
		  short ctr = 0;
		  int criteriaSize = criteria.size();
		
		  
		  Object obj[] = null;		      
		
		  // Allocate the size of the object parameter
				   
		  if (criteria.containsKey("locationName")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		  
		  if (criteria.containsKey("type")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		  		  
		  if (criteria.containsKey("contactPerson")) {
			
		  	 criteriaSize--;
		  
		  }
		  
		  obj = new Object[criteriaSize];    
		       	      		
	      if (criteria.containsKey("locationName")) {
	  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
	  	 
	   	     jbossQl.append("loc.locName LIKE '%" + (String)criteria.get("locationName") + "%' ");		
	  	 
	      }
	      
		  if (criteria.containsKey("type")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("loc.locLvType LIKE '%" + (String)criteria.get("type") + "%' ");
		  	 
		  }	       
			
		  if (criteria.containsKey("contactPerson")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 
		   	 jbossQl.append("loc.locContactPerson LIKE '%" + (String)criteria.get("contactPerson") + "%' ");		
		  	 
		  }	
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("loc.locAdCompany=" + AD_CMPNY + " ");
		  
		    	      	
	      Collection invLocations = invLocationHome.getLocByCriteria(jbossQl.toString(), obj);	         
		  
		  if (invLocations.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  return new Integer(invLocations.size());
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvFindLocationControllerBean ejbCreate");
      
    }
}
