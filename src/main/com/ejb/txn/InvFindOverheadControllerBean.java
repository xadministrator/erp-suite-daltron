
/*
 * InvFindOverheadControllerBean.java
 *
 * Created on February 07, 2005, 03:18 PM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.inv.LocalInvOverhead;
import com.ejb.inv.LocalInvOverheadHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvOverheadDetails;

/**
 * @ejb:bean name="InvFindOverheadControllerEJB"
 *           display-name="Used for finding overheads"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvFindOverheadControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindOverheadController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindOverheadControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvFindOverheadControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvOverheadCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvFindOverheadControllerBean getAdLvInvOverheadCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV OVERHEAD CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvOhByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindOverheadControllerBean getInvOhByCriteria");
        
        LocalInvOverheadHome invOverheadHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	invOverheadHome = (LocalInvOverheadHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvOverheadHome.JNDI_NAME, LocalInvOverheadHome.class);
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
        	genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
    			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);	

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(oh) FROM InvOverhead oh ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		criteriaSize--;
        		
        	}
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");	
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("oh.ohReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
        		
        	}	
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("oh.ohDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("oh.ohDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	} 
        	
        	if (criteria.containsKey("category")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("oh.ohAdLvCategory=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("category");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("oh.ohDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("oh.ohDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (criteria.containsKey("posted")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("oh.ohPosted=?" + (ctr+1) + " ");
        		
        		String posted = (String)criteria.get("posted");
        		
        		if (posted.equals("YES")) {
        			
        			obj[ctr] = new Byte(EJBCommon.TRUE);
        			
        		} else {
        			
        			obj[ctr] = new Byte(EJBCommon.FALSE);
        			
        		}       	  
        		
        		ctr++;
        		
        	}	    
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("oh.ohAdBranch=" + AD_BRNCH + " AND oh.ohAdCompany=" + AD_CMPNY + " ");
        	
        	String orderBy = null;
        	
        	if (ORDER_BY.equals("REFERENCE NUMBER")) {	          
        		
        		orderBy = "oh.ohReferenceNumber";
        		
        	} else if (ORDER_BY.equals("CATEGORY")) {	          
        		
        		orderBy = "oh.ohAdLvCategory";
        		
        	} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
        		
        		orderBy = "oh.ohDocumentNumber";

        	}
        	
        	if (orderBy != null) {
        		
        		jbossQl.append("ORDER BY " + orderBy + ", oh.ohDate");
        		
        	} else {
        		
        		jbossQl.append("ORDER BY oh.ohDate");
        		
        	}
        	
        	jbossQl.append(" OFFSET ?" + (ctr + 1));	        
        	obj[ctr] = OFFSET;	      
        	ctr++;
        	
        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;		      
        	
        	Collection invOverheads = invOverheadHome.getOhByCriteria(jbossQl.toString(), obj);	         
        	
        	if (invOverheads.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
        	Iterator i = invOverheads.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvOverhead invOverhead = (LocalInvOverhead)i.next(); 
        		
        		InvOverheadDetails details = new InvOverheadDetails();
        		details.setOhCode(invOverhead.getOhCode());
        		details.setOhDate(invOverhead.getOhDate());
        		details.setOhDocumentNumber(invOverhead.getOhDocumentNumber());
        		details.setOhReferenceNumber(invOverhead.getOhReferenceNumber());
        		details.setOhLvShift(invOverhead.getOhLvShift());
        		details.setOhDescription(invOverhead.getOhDescription());
        		
        		list.add(details);
        		
        	}
        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getInvOhSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindOverheadControllerBean getInvOhSizeByCriteria");
        
        LocalInvOverheadHome invOverheadHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;

        //initialized EJB Home
        
        try {
            
        	invOverheadHome = (LocalInvOverheadHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvOverheadHome.JNDI_NAME, LocalInvOverheadHome.class);
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
        	genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
    			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);	

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(oh) FROM InvOverhead oh ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size();	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		criteriaSize--;
        		
        	}
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");	
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("oh.ohReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
        		
        	}	
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("oh.ohDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("oh.ohDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	} 
        	
        	if (criteria.containsKey("category")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("oh.ohAdLvCategory=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("category");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("oh.ohDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("oh.ohDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (criteria.containsKey("posted")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("oh.ohPosted=?" + (ctr+1) + " ");
        		
        		String posted = (String)criteria.get("posted");
        		
        		if (posted.equals("YES")) {
        			
        			obj[ctr] = new Byte(EJBCommon.TRUE);
        			
        		} else {
        			
        			obj[ctr] = new Byte(EJBCommon.FALSE);
        			
        		}       	  
        		
        		ctr++;
        		
        	}	    
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("oh.ohAdBranch=" + AD_BRNCH + " AND oh.ohAdCompany=" + AD_CMPNY + " ");
        	
        	
        	Collection invOverheads = invOverheadHome.getOhByCriteria(jbossQl.toString(), obj);	         
        	
        	if (invOverheads.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
        	return new Integer(invOverheads.size());
        	
        } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvFindOverheadControllerBean ejbCreate");
      
    }
}
