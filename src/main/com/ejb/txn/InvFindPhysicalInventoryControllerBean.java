
/*
* InvFindPhysicalInventoryControllerBean.java
*
* Created on July 09, 2004, 10:08 AM
*
* @author  Enrico C. Yap
*/

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPhysicalInventory;
import com.ejb.inv.LocalInvPhysicalInventoryHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvModPhysicalInventoryDetails;

/**
* @ejb:bean name="InvFindPhysicalInventoryControllerEJB"
*           display-name="Used for finding physical inventory"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/InvFindPhysicalInventoryControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.InvFindPhysicalInventoryController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.InvFindPhysicalInventoryControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class InvFindPhysicalInventoryControllerBean extends AbstractSessionBean {
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvFindPhysicalInventoryControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		LocalInvLocation invLocation = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection invLocations = invLocationHome.findLocAll(AD_CMPNY);
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				invLocation = (LocalInvLocation)i.next();
				
				list.add(invLocation.getLocName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvFindPhysicalInventoryControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
   /**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public ArrayList getInvPiByCriteria(HashMap criteria,
			Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvFindPhysicalInventoryControllerBean getInvPiByCriteria");
		
		LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);	
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(pi) FROM InvPhysicalInventory pi ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size() + 2;	      
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("referenceNumber")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("referenceNumber")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pi.piReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
				
			}		  
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("pi.piDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("pi.piDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}    
			
			if (criteria.containsKey("location")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("pi.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;
				
			}
			
			if (criteria.containsKey("category")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("pi.piAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;
				
			}
			
			if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	} else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	}
	       	  
			jbossQl.append("pi.piAdBranch=" + AD_BRNCH + " AND pi.piAdCompany=" + AD_CMPNY + " ");
			
			String orderBy = null;
			
			if (ORDER_BY.equals("REFERENCE NUMBER")) {	          
				
				orderBy = "pi.piReferenceNumber";
				
			} else if (ORDER_BY.equals("CATEGORY")) {
				
				orderBy = "pi.piAdLvCategory";	
				
			} else if (ORDER_BY.equals("LOCATION")) {
				
				orderBy = "pi.invLocation.locName";

			}
			
			if (orderBy != null) {
				
				jbossQl.append("ORDER BY " + orderBy + ", pi.piDate");
				
			} else {
				
				jbossQl.append("ORDER BY pi.piDate");
				
			}
			
			jbossQl.append(" OFFSET ?" + (ctr + 1));	        
			obj[ctr] = OFFSET;	      
			ctr++;
			
			jbossQl.append(" LIMIT ?" + (ctr + 1));
			obj[ctr] = LIMIT;
			ctr++;		      
			
			Collection invPhysicalInventories = invPhysicalInventoryHome.getPiByCriteria(jbossQl.toString(), obj);	         
			
			if (invPhysicalInventories.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			Iterator i = invPhysicalInventories.iterator();
			
			while (i.hasNext()) {
				
				LocalInvPhysicalInventory invPhysicalInventory = (LocalInvPhysicalInventory)i.next(); 
				
				InvModPhysicalInventoryDetails mdetails = new InvModPhysicalInventoryDetails();
				mdetails.setPiCode(invPhysicalInventory.getPiCode());
				mdetails.setPiDate(invPhysicalInventory.getPiDate());
				mdetails.setPiReferenceNumber(invPhysicalInventory.getPiReferenceNumber());
				mdetails.setPiDescription(invPhysicalInventory.getPiDescription());
				try {
					mdetails.setPiLocName(invPhysicalInventory.getInvLocation().getLocName());
				}catch (Exception e) {
					mdetails.setPiLocName("");
				}
				
				mdetails.setPiAdLvCategory(invPhysicalInventory.getPiAdLvCategory());
	
				list.add(mdetails);
				
			}
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public Integer getInvPiSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException {
		
		Debug.print("InvFindPhysicalInventoryControllerBean getInvPiSizeByCriteria");
		
		LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGenValueSetValueHome genValueSetValueHome = null;
		LocalGenSegmentHome genSegmentHome = null;
		
		//initialized EJB Home
		
		try {
			
			invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
			genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);	
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(pi) FROM InvPhysicalInventory pi ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			Object obj[];	      
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("referenceNumber")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("referenceNumber")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pi.piReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
				
			}		  
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("pi.piDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("pi.piDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}    
			
			if (criteria.containsKey("location")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("pi.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;
				
			}
			
			if (criteria.containsKey("category")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("pi.piAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;
				
			}
			
			if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	} else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	}
	       	  
	       	jbossQl.append("pi.piAdBranch=" + AD_BRNCH + " AND pi.piAdCompany=" + AD_CMPNY + " ");
			
			
			Collection invPhysicalInventories = invPhysicalInventoryHome.getPiByCriteria(jbossQl.toString(), obj);	         
			
			if (invPhysicalInventories.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			return new Integer(invPhysicalInventories.size());
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// SessionBean methods
	
   /**
    * @ejb:create-method view-type="remote"
    **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvFindPhysicalInventoryControllerBean ejbCreate");
		
	}
}
