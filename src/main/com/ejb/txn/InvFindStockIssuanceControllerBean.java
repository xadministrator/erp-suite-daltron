
/*
 * InvFindStockIssuanceControllerBean.java
 *
 * Created on March 31, 2005, 11:04 PM
 *
 * @author  Neil Andrew M. Ajero
 */


package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvStockIssuance;
import com.ejb.inv.LocalInvStockIssuanceHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvStockIssuanceDetails;

/**
 * @ejb:bean name="InvFindStockIssuanceControllerEJB"
 *           display-name="Used for finding stocks issued"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvFindStockIssuanceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindStockIssuanceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindStockIssuanceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvFindStockIssuanceControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvSiByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindStockIssuanceControllerBean getInvSiByCriteria");
        
        LocalInvStockIssuanceHome invStockIssuanceHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	invStockIssuanceHome = (LocalInvStockIssuanceHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvStockIssuanceHome.JNDI_NAME, LocalInvStockIssuanceHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(si) FROM InvStockIssuance si ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		criteriaSize--;
        		
        	}
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");	
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("si.siReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
        		
        	}
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("si.siDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("si.siDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("si.siDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("si.siDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("si.siAdBranch=" + AD_BRNCH + " AND si.siAdCompany=" + AD_CMPNY + " ");
        	
        	String orderBy = null;
        	
        	if (ORDER_BY.equals("DATE")) {	          
        		
        		orderBy = "si.siDate";
        		
        	} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
        		
        		orderBy = "si.siDocumentNumber";
        		
        	} else if (ORDER_BY.equals("REFERENCE NUMBER")) {	          
        		
        		orderBy = "si.siReferenceNumber";	  
        		
        	}
        	
        	if (orderBy != null) {
        		
        		jbossQl.append("ORDER BY " + orderBy + ", si.siDate");
        		
        	} else {
        		
        		jbossQl.append("ORDER BY si.siDate");
        		
        	}
        	
        	jbossQl.append(" OFFSET ?" + (ctr + 1));	        
        	obj[ctr] = OFFSET;	      
        	ctr++;
        	
        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;		      
        	
        	Collection invStockIssuances = invStockIssuanceHome.getSiByCriteria(jbossQl.toString(), obj);	         
        	
        	if (invStockIssuances.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
        	Iterator i = invStockIssuances.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvStockIssuance invStockIssuance = (LocalInvStockIssuance)i.next(); 
        		
        		InvStockIssuanceDetails mdetails = new InvStockIssuanceDetails();
        		mdetails.setSiCode(invStockIssuance.getSiCode());
        		mdetails.setSiDate(invStockIssuance.getSiDate());
        		mdetails.setSiDocumentNumber(invStockIssuance.getSiDocumentNumber());
        		mdetails.setSiReferenceNumber(invStockIssuance.getSiReferenceNumber());
        		mdetails.setSiDescription(invStockIssuance.getSiDescription());		  	  
        		
        		list.add(mdetails);
        		
        	}
        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getInvSiSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindStockIssuanceControllerBean getInvSiSizeByCriteria");
        
        LocalInvStockIssuanceHome invStockIssuanceHome = null;

        //initialized EJB Home
        
        try {
            
        	invStockIssuanceHome = (LocalInvStockIssuanceHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvStockIssuanceHome.JNDI_NAME, LocalInvStockIssuanceHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(si) FROM InvStockIssuance si ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size();	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		criteriaSize--;
        		
        	}
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");	
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("si.siReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
        		
        	}
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("si.siDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("si.siDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("si.siDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("si.siDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("si.siAdBranch=" + AD_BRNCH + " AND si.siAdCompany=" + AD_CMPNY + " ");
        	
        	Collection invStockIssuances = invStockIssuanceHome.getSiByCriteria(jbossQl.toString(), obj);	         
        	
        	if (invStockIssuances.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
        	return new Integer(invStockIssuances.size());
        	
        } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvFindStockIssuanceControllerBean ejbCreate");
      
    }
}
