package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvStockTransfer;
import com.ejb.inv.LocalInvStockTransferHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModStockTransferDetails;

/**
 * @ejb:bean name="InvFindStockTransferControllerEJB"
 *           display-name="Used for finding stock transfer"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvFindStockTransferControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvFindStockTransferController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvFindStockTransferControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvFindStockTransferControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvStByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindStockTransferControllerBean getInvStByCriteria");
        
        LocalInvStockTransferHome invStockTransferHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(st) FROM InvStockTransfer st ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size() + 2;	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("referenceNumber")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      if (criteria.containsKey("approvalStatus")) {
	      	
	      	 String approvalStatus = (String)criteria.get("approvalStatus");
	      	
	      	 if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
	      	 	
	      	 	 criteriaSize--;
	      	 	
	      	 }
	      	
	      }
	      	
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("referenceNumber")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("st.stReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

		  }
		  
		  if (criteria.containsKey("documentNumberFrom")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("st.stDocumentNumber>=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("documentNumberFrom");
	       	  ctr++;
	       	  
	      }
		  
		  if (criteria.containsKey("documentNumberTo")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("st.stDocumentNumber<=?" + (ctr+1) + " ");
	       	  obj[ctr] = (String)criteria.get("documentNumberTo");
	       	  ctr++;
	       	  
	      }		  		  

	      if (criteria.containsKey("dateFrom")) {
		      	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("st.stDate>=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateFrom");
		  	 ctr++;
		  }  
		      
		  if (criteria.containsKey("dateTo")) {
		  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 jbossQl.append("st.stDate<=?" + (ctr+1) + " ");
		  	 obj[ctr] = (Date)criteria.get("dateTo");
		  	 ctr++;
		  	 
		  }    
		  
	      if (criteria.containsKey("approvalStatus")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  String approvalStatus = (String)criteria.get("approvalStatus");
	       	  
	       	  if (approvalStatus.equals("DRAFT")) {
	       	      
		       	  jbossQl.append("st.stApprovalStatus IS NULL ");
	       	  	
	       	  } else {
	      	  	
		      	  jbossQl.append("st.stApprovalStatus=?" + (ctr+1) + " ");
		       	  obj[ctr] = approvalStatus;
		       	  ctr++;
	      	  	
	      	  }
	       	  
	      }
	      	      
	      if (criteria.containsKey("posted")) {
	       	
	       	  if (!firstArgument) {
	       	  	
	       	     jbossQl.append("AND ");
	       	     
	       	  } else {
	       	  	
	       	  	 firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	  }
	       	  
	       	  jbossQl.append("st.stPosted=?" + (ctr+1) + " ");
	       	  
	       	  String posted = (String)criteria.get("posted");
	       	  
	       	  if (posted.equals("YES")) {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.TRUE);
	       	  	
	       	  } else {
	       	  	
	       	  	obj[ctr] = new Byte(EJBCommon.FALSE);
	       	  	
	       	  }       	  
	       	 
	       	  ctr++;
	       	  
	      }	      
	      
	      if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("st.stAdBranch=" + AD_BRNCH + " AND st.stAdCompany=" + AD_CMPNY + " ");
      			
		  String orderBy = null;
		  
		  if (ORDER_BY.equals("REFERENCE NUMBER")) {	          
		      	      		
		  	  orderBy = "st.stReferenceNumber";

		  } else if (ORDER_BY.equals("TYPE")) {	          
	      		
		  	  orderBy = "st.stType";
		  	  
		  } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          
      		
		      orderBy = "st.stDocumentNumber";	  

		  }
		  
	  	  if (orderBy != null) {
		
		  	  jbossQl.append("ORDER BY " + orderBy + ", st.stDate");

		  } else {
		  	
		  	  jbossQl.append("ORDER BY st.stDate");
		  	
		  }
		  
		  jbossQl.append(" OFFSET ?" + (ctr + 1));	        
		  obj[ctr] = OFFSET;	      
		  ctr++;
		  
		  jbossQl.append(" LIMIT ?" + (ctr + 1));
		  obj[ctr] = LIMIT;
		  ctr++;		
		  
		  System.out.println(jbossQl.toString());
			  	      	
	      Collection invStockTransfers = invStockTransferHome.getStByCriteria(jbossQl.toString(), obj);	         
		  
		  if (invStockTransfers.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = invStockTransfers.iterator();
		
		  while (i.hasNext()) {
		  	
		  	LocalInvStockTransfer invStockTransfer = (LocalInvStockTransfer)i.next(); 
		  	
		  	InvModStockTransferDetails mdetails = new InvModStockTransferDetails();
		  	mdetails.setStCode(invStockTransfer.getStCode());
		  	mdetails.setStDate(invStockTransfer.getStDate());
		  	mdetails.setStDocumentNumber(invStockTransfer.getStDocumentNumber());
		  	mdetails.setStReferenceNumber(invStockTransfer.getStReferenceNumber());		  			  			  	 
		  	
		  	list.add(mdetails);
		  	
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getInvStSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvFindStockTransferControllerBean getInvStByCriteria");
        
        LocalInvStockTransferHome invStockTransferHome = null;

        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();
        	jbossQl.append("SELECT OBJECT(st) FROM InvStockTransfer st ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size();	      
        	
        	Object obj[];	      
        	
        	// Allocate the size of the object parameter
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		criteriaSize--;
        		
        	}
        	
        	if (criteria.containsKey("approvalStatus")) {
        		
        		String approvalStatus = (String)criteria.get("approvalStatus");
        		
        		if (approvalStatus.equals("DRAFT") || approvalStatus.equals("REJECTED")) {
        			
        			criteriaSize--;
        			
        		}
        		
        	}
        	
        	obj = new Object[criteriaSize];
        	
        	if (criteria.containsKey("referenceNumber")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");	
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("st.stReferenceNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");
        		
        	}
        	
        	if (criteria.containsKey("documentNumberFrom")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("st.stDocumentNumber>=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberFrom");
        		ctr++;
        		
        	}
        	
        	if (criteria.containsKey("documentNumberTo")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("st.stDocumentNumber<=?" + (ctr+1) + " ");
        		obj[ctr] = (String)criteria.get("documentNumberTo");
        		ctr++;
        		
        	}		  		  
        	
        	if (criteria.containsKey("dateFrom")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("st.stDate>=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateFrom");
        		ctr++;
        	}  
        	
        	if (criteria.containsKey("dateTo")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        		}
        		jbossQl.append("st.stDate<=?" + (ctr+1) + " ");
        		obj[ctr] = (Date)criteria.get("dateTo");
        		ctr++;
        		
        	}    
        	
        	if (criteria.containsKey("approvalStatus")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		String approvalStatus = (String)criteria.get("approvalStatus");
        		
        		if (approvalStatus.equals("DRAFT")) {
        			
        			jbossQl.append("st.stApprovalStatus IS NULL ");
        			
        		} else {
        			
        			jbossQl.append("st.stApprovalStatus=?" + (ctr+1) + " ");
        			obj[ctr] = approvalStatus;
        			ctr++;
        			
        		}
        		
        	}
        	
        	if (criteria.containsKey("posted")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("st.stPosted=?" + (ctr+1) + " ");
        		
        		String posted = (String)criteria.get("posted");
        		
        		if (posted.equals("YES")) {
        			
        			obj[ctr] = new Byte(EJBCommon.TRUE);
        			
        		} else {
        			
        			obj[ctr] = new Byte(EJBCommon.FALSE);
        			
        		}       	  
        		
        		ctr++;
        		
        	}	      
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append("WHERE ");
        		
        	}
        	
        	jbossQl.append("st.stAdBranch=" + AD_BRNCH + " AND st.stAdCompany=" + AD_CMPNY + " ");
        	System.out.println(jbossQl.toString());
        	Collection invStockTransfers = invStockTransferHome.getStByCriteria(jbossQl.toString(), obj);	         
        	
        	if (invStockTransfers.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
        	return new Integer(invStockTransfers.size());
        	
        } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

    
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvFindStockTransferControllerBean ejbCreate");
      
    }
}
