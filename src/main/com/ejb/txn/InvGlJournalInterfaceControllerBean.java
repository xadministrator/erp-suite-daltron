
/*
 * InvGlJournalInterfaceControllerBean.java
 *
 * Created on Aug 12, 2004, 12:59 PM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlJournalInterface;
import com.ejb.gl.LocalGlJournalInterfaceHome;
import com.ejb.gl.LocalGlJournalLineInterface;
import com.ejb.gl.LocalGlJournalLineInterfaceHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvBuildUnbuildAssembly;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="InvGlJournalInterfaceControllerEJB"
 *           display-name="Used for journal interface run"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvGlJournalInterfaceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvGlJournalInterfaceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvGlJournalInterfaceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvGlJournalInterfaceControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public long executeInvGlJriRun(Date JRI_DT_FRM, Date JRI_DT_TO, Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("InvGlJournalInterfaceControllerBean executeInvGlJriRun");
        
        LocalGlJournalInterfaceHome glJournalInterfaceHome = null;
        LocalGlJournalLineInterfaceHome glJournalLineInterfaceHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;        
        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS"); 
        long IMPORTED_JOURNALS = 0L;
        short lineNumber = 0;
                       
                
        // Initialize EJB Home
        
        try {
            
            glJournalInterfaceHome = (LocalGlJournalInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalInterfaceHome.JNDI_NAME, LocalGlJournalInterfaceHome.class);
            glJournalLineInterfaceHome = (LocalGlJournalLineInterfaceHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlJournalLineInterfaceHome.JNDI_NAME, LocalGlJournalLineInterfaceHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);    
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
                
                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {   
      
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            LocalGlFunctionalCurrency glFunctionalCurrency = adCompany.getGlFunctionalCurrency();
                            
            // inventory adjustments
            
            Collection invAdjustments = invAdjustmentHome.findPostedAdjByAdjDateRange(JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
            
            Iterator i = invAdjustments.iterator();
            
            while (i.hasNext()) {
            	
            	LocalInvAdjustment invAdjustment = (LocalInvAdjustment)i.next();
            	
            	Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByAdjCode(invAdjustment.getAdjCode(), AD_CMPNY);
            	            	            	
            	if (!invDistributionRecords.isEmpty()) {
            		
            		LocalGlJournalInterface glJournalInterface = glJournalInterfaceHome.create(
            				invAdjustment.getAdjReferenceNumber(),
            		        invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(), 
							"INVENTORY ADJUSTMENTS", "INVENTORY",
            		         glFunctionalCurrency.getFcName(), null, invAdjustment.getAdjDocumentNumber(), 
            		         null, 1d, 'N', EJBCommon.FALSE, null, null, null, AD_BRNCH, AD_CMPNY);
            		         
            		IMPORTED_JOURNALS++;
            		lineNumber = 0;
            		
            		Iterator j = invDistributionRecords.iterator();
            	
	            	while (j.hasNext()) {
	            		
	            		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
	            	    
	            	    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber,
	        		    	invDistributionRecord.getDrDebit(),
	        		    	invDistributionRecord.getDrAmount(),
	        		    	invDistributionRecord.getInvChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    	
	        		    invDistributionRecord.setDrImported(EJBCommon.TRUE);	        		    	
	            	    		            			            		
	            			            		
	            	}
	            	
            	}
            	
            }            
            
            // build/unbuild assemblies
            
            Collection invBuildUnbuildAssemblies = invBuildUnbuildAssemblyHome.findPostedBuaByBuaDateRange(JRI_DT_FRM, JRI_DT_TO, AD_CMPNY);
            
            i = invBuildUnbuildAssemblies.iterator();
            
            while (i.hasNext()) {
            	
            	LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = (LocalInvBuildUnbuildAssembly)i.next();
            	            	
            	Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByBuaCode(invBuildUnbuildAssembly.getBuaCode(), AD_CMPNY);
            	            	            	
            	if (!invDistributionRecords.isEmpty()) {
            		
            		LocalGlJournalInterface glJournalInterface = glJournalInterfaceHome.create(
            				invBuildUnbuildAssembly.getBuaReferenceNumber(),
            		        invBuildUnbuildAssembly.getBuaDescription(), invBuildUnbuildAssembly.getBuaDate(), 
							"INVENTORY ASSEMBLIES", "INVENTORY",
            		        glFunctionalCurrency.getFcName(), null, invBuildUnbuildAssembly.getBuaDocumentNumber(), 
            		         null, 1d, 'N', EJBCommon.FALSE, null, null, null, AD_BRNCH, AD_CMPNY);
            		         
            		IMPORTED_JOURNALS++;
            		lineNumber = 0;
            		
            		Iterator j = invDistributionRecords.iterator();
            	
	            	while (j.hasNext()) {
	            		
	            		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
	            		
	            	    LocalGlJournalLineInterface glJournalLineInterface = glJournalLineInterfaceHome.create(
	        		    	++lineNumber, 
	        		    	invDistributionRecord.getDrDebit(),        		    	
	        		    	invDistributionRecord.getDrAmount(),
	        		    	invDistributionRecord.getInvChartOfAccount().getCoaAccountNumber(), AD_CMPNY);
	        		    	
	        		    glJournalInterface.addGlJournalLineInterface(glJournalLineInterface);
	        		    	
	        		    invDistributionRecord.setDrImported(EJBCommon.TRUE);	        		    	
	            	    		            			            		
	            			            		
	            	}
	            	
            	}
            	
            }
            
            return IMPORTED_JOURNALS;
                
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

   // SessionBean methods

   /**
    * @ejb:create-method view-type="remote"
    **/
   public void ejbCreate() throws CreateException {

      Debug.print("InvGlJournalInterfaceControllerBean ejbCreate");
      
   }

   // private methods
   	
		   
}
