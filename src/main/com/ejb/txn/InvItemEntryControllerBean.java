/*
 * InvItemEntryControllerBean.java
 *
 * Created on May 24, 2004, 6:29 PM
 *
 * @author Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchCustomerHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.exception.ArCCCoaGlReceivableAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlRevenueAccountNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.InvAccumulatedDepreciationAccountNotFoundException;
import com.ejb.exception.InvDepreciationAccountNotFoundException;
import com.ejb.exception.InvFixedAssetAccountNotFoundException;
import com.ejb.exception.InvFixedAssetInformationNotCompleteException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModItemDetails;


/**
 * @ejb:bean name="InvItemEntryControllerEJB" display-name="Used for entering items"
 *           type="Stateless" view-type="remote" jndi-name="ejb/InvItemEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvItemEntryController" extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvItemEntryControllerHome" extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser" role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 */

public class InvItemEntryControllerBean extends AbstractSessionBean {

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getInvUomAll(
      Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getInvUomAll");

    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    LocalInvUnitOfMeasure invUnitOfMeasure = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection invUnitOfMeasures = invUnitOfMeasureHome.findEnabledUomAll(AD_CMPNY);

      Iterator i = invUnitOfMeasures.iterator();

      while (i.hasNext()) {

        invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();

        list.add(invUnitOfMeasure.getUomName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getInvLocAll(
      Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getInvLocAll");

    LocalInvLocationHome invLocationHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      invLocationHome = (LocalInvLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection invLocations = invLocationHome.findLocAll(AD_CMPNY);

      Iterator i = invLocations.iterator();

      while (i.hasNext()) {

        LocalInvLocation invLocation = (LocalInvLocation) i.next();

        list.add(invLocation.getLocName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getInvIiAll(
      Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getInvIiAll");

    LocalInvItemHome invItemHome = null;
    LocalInvItem invItem = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection invItems = invItemHome.findEnabledIiAll(AD_CMPNY);

      Iterator i = invItems.iterator();

      while (i.hasNext()) {

        invItem = (LocalInvItem) i.next();

        list.add(invItem.getIiName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   **/
  public Integer saveInvIiEntry(
      com.util.InvItemDetails details, String UOM_NM, String SPL_SPPLR_CD, String CST_CSTMR_CD,
      String II_RETAIL_UOM_NM, String II_DFLT_LCTN, String FixedAssetAccount,
      String DepreciationAccount, String AccumulatedDepreciation, boolean trackMisc,
      Integer AD_CMPNY
  ) throws GlobalRecordAlreadyExistException, GlobalRecordAlreadyAssignedException,
      InvFixedAssetAccountNotFoundException, InvDepreciationAccountNotFoundException,
      InvFixedAssetInformationNotCompleteException,
      InvAccumulatedDepreciationAccountNotFoundException {

    Debug.print("InvItemEntryControllerBean saveInvIiEntry");

    LocalInvItemHome invItemHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalInvLocationHome invLocationHome = null;
    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    LocalAdLookUpValueHome adLookUpValueHome = null;
    LocalInvPriceLevelHome invPriceLevelHome = null;
    LocalApSupplierHome apSupplierHome = null;
    LocalArCustomerHome arCustomerHome = null;
    LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
    LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    LocalAdBranchHome adBranchHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalInvItem invItem = null;
    LocalAdBranchItemLocation adBranchItemLocation = null;
    LocalAdBranch adBranch = null;

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      invLocationHome = (LocalInvLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
      invPriceLevelHome = (LocalInvPriceLevelHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
      apSupplierHome = (LocalApSupplierHome) EJBHomeFactory
          .lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

      invBillOfMaterialHome = (LocalInvBillOfMaterialHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
      adBranchItemLocationHome = (LocalAdBranchItemLocationHome) EJBHomeFactory.lookUpLocalHome(
          LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
      adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
          LocalAdBranchHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // validate if item already exists

      try {

        invItem = invItemHome.findByIiName(details.getIiName(), AD_CMPNY);

        if (details.getIiCode() == null
            || details.getIiCode() != null && !invItem.getIiCode().equals(details.getIiCode())) {

          throw new GlobalRecordAlreadyExistException();

        }

      } catch (GlobalRecordAlreadyExistException ex) {

        throw ex;

      } catch (FinderException ex) {

      }
 System.out.println(" 1" );
      LocalGlChartOfAccount glFixedAssetAccount = null;
      LocalGlChartOfAccount glDepreciationAccount = null;
      LocalGlChartOfAccount glAccumulatedDepreciationAccount = null;
      if (details.getIiFixedAsset() == 1) {
        try {

          glFixedAssetAccount =
              glChartOfAccountHome.findByCoaAccountNumber(FixedAssetAccount, AD_CMPNY);

        } catch (FinderException ex) {

          throw new InvFixedAssetAccountNotFoundException();

        }

        try {

          glDepreciationAccount =
              glChartOfAccountHome.findByCoaAccountNumber(DepreciationAccount, AD_CMPNY);

        } catch (FinderException ex) {

          throw new InvDepreciationAccountNotFoundException();

        }

        try {

          glAccumulatedDepreciationAccount =
              glChartOfAccountHome.findByCoaAccountNumber(AccumulatedDepreciation, AD_CMPNY);

        } catch (FinderException ex) {

          throw new InvAccumulatedDepreciationAccountNotFoundException();

        }

      }
      Integer retailUom = null;
      if (II_RETAIL_UOM_NM != null && !II_RETAIL_UOM_NM.equals("")) {
        LocalInvUnitOfMeasure invRetailUom =
            invUnitOfMeasureHome.findByUomName(II_RETAIL_UOM_NM, AD_CMPNY);
        retailUom = invRetailUom.getUomCode();
      }

      Integer defaultLocation = null;
      if (II_DFLT_LCTN != null && !II_DFLT_LCTN.equals("")) {
        LocalInvLocation invLocation = invLocationHome.findByLocName(II_DFLT_LCTN, AD_CMPNY);
        defaultLocation = invLocation.getLocCode();
      }

      boolean isRecalculate = false;

      // create new item

      System.out.println(" 2" );
      if (details.getIiCode() == null) {

        if (details.getIiFixedAsset() == 1 && details.getIiLifeSpan() == 0) {
          throw new InvFixedAssetInformationNotCompleteException();
        }
        invItem = invItemHome.create(details.getIiName(), details.getIiDescription(),
            details.getIiPartNumber(), details.getIiShortName(), details.getIiBarCode1(),
            details.getIiBarCode2(), details.getIiBarCode3(), details.getIiBrand(),
            details.getIiClass(), details.getIiAdLvCategory(), details.getIiCostMethod(),
            details.getIiUnitCost(), details.getIiSalesPrice(), details.getIiEnable(),
            details.getIiVirtualStore(), details.getIiEnableAutoBuild(), details.getIiDoneness(),
            details.getIiSidings(), details.getIiRemarks(), details.getIiServiceCharge(),
            details.getIiNonInventoriable(), details.getIiServices(), details.getIiJobServices(),
            details.getIiIsVatRelief(), details.getIiIsTax(), details.getIiIsProject(),
            details.getIiPercentMarkup(), details.getIiShippingCost(),
            details.getIiStandardFillSize(), details.getIiSpecificGravity(), details.getIiYield(),

            0d, 0d, 0d, 0d, 0d, null, null, null, null, null,

            details.getIiLossPercentage(), details.getIiMarkupValue(), details.getIiMarket(),
            details.getIiEnablePo(), details.getIiPoCycle(), details.getIiUmcPackaging(), retailUom,
            details.getIiOpenProduct(), details.getIiFixedAsset(), details.getIiDateAcquired(),
            defaultLocation, details.getIiTraceMisc(), details.getIiScSunday(),
            details.getIiScMonday(), details.getIiScTuesday(), details.getIiScWednesday(),
            details.getIiScThursday(), details.getIiScFriday(), details.getIiScSaturday(),
            details.getIiAcquisitionCost(), details.getIiLifeSpan(), details.getIiResidualValue(),
            glFixedAssetAccount != null ? glFixedAssetAccount.getCoaCode().toString() : null,
            glDepreciationAccount != null ? glDepreciationAccount.getCoaCode().toString() : null,


            glAccumulatedDepreciationAccount != null
                ? glAccumulatedDepreciationAccount.getCoaCode().toString()
                : null,
            details.getIiCondition(), details.getIiTaxCode(), AD_CMPNY);



        invItem.setIiCreatedBy(details.getIiCreatedBy());

        invItem.setIiDateCreated(details.getIiDateCreated());
        invItem.setIiLastModifiedBy(details.getIiLastModifiedBy());
        invItem.setIiDateLastModified(details.getIiDateLastModified());

        // price levels
        Collection adLookUpValues = adLookUpValueHome.findByLuName("INV PRICE LEVEL", AD_CMPNY);

        Iterator i = adLookUpValues.iterator();

        while (i.hasNext()) {

          LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();

          LocalInvPriceLevel invPriceLevel =
              invPriceLevelHome.create(invItem.getIiSalesPrice(), 0d, invItem.getIiPercentMarkup(),
                  invItem.getIiShippingCost(), adLookUpValue.getLvName(), 'N', AD_CMPNY);

          invPriceLevel.setInvItem(invItem);

          // invItem.addInvPriceLevel(invPriceLevel);

        }


        isRecalculate = true;
              System.out.println(" 3" );
      } else {

        // update item

        invItem = invItemHome.findByPrimaryKey(details.getIiCode());

        if (details.getIiFixedAsset() == 1 && details.getIiLifeSpan() == 0) {
          throw new InvFixedAssetInformationNotCompleteException();
        }

        // validate if already assigned
        if (!invItem.getInvUnitOfMeasure().getUomName().equals(UOM_NM)) {

          // if item location is already assigned
          if (!invItem.getInvItemLocations().isEmpty()) {

            Collection invItemLocations = invItem.getInvItemLocations();

            Iterator locIter = invItemLocations.iterator();

            while (locIter.hasNext()) {

              LocalInvItemLocation invItemLocation = (LocalInvItemLocation) locIter.next();

              if (!invItemLocation.getApVoucherLineItems().isEmpty()
                  || !invItemLocation.getArInvoiceLineItems().isEmpty()
                  || !invItemLocation.getInvAdjustmentLines().isEmpty()
                  || !invItemLocation.getInvBuildOrderLines().isEmpty()
                  || !invItemLocation.getInvBuildUnbuildAssemblyLines().isEmpty()
                  || !invItemLocation.getInvCostings().isEmpty()
                  || !invItemLocation.getInvPhysicalInventoryLines().isEmpty()
                  || !invItemLocation.getInvBranchStockTransferLines().isEmpty()
                  || !invItemLocation.getInvStockIssuanceLines().isEmpty()
                  || !invItem.getInvBuildOrderStocks().isEmpty()
                  || !invItem.getInvStockTransferLines().isEmpty()) {

                throw new GlobalRecordAlreadyAssignedException();

              }

            }
          }

        }

        if (!invItem.getInvUnitOfMeasure().getUomName().equals(UOM_NM))
          isRecalculate = true;

        if (invItem.getInvUnitOfMeasureConversions().isEmpty())
          isRecalculate = true;

        double oldUnitCost = invItem.getIiUnitCost();

        // update bill of materials
        Collection invBillOfMaterials =
            invBillOfMaterialHome.findByBomIiName(invItem.getIiName(), AD_CMPNY);
        Iterator bomIter = invBillOfMaterials.iterator();

        while (bomIter.hasNext()) {

          LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) bomIter.next();
          invBillOfMaterial.setBomIiName(details.getIiName());

        }

        invItem.setIiName(details.getIiName());
        invItem.setIiDescription(details.getIiDescription());
        invItem.setIiPartNumber(details.getIiPartNumber());
        invItem.setIiShortName(details.getIiShortName());
        invItem.setIiBarCode1(details.getIiBarCode1());
        invItem.setIiBarCode2(details.getIiBarCode2());
        invItem.setIiBarCode3(details.getIiBarCode3());
        invItem.setIiBrand(details.getIiBrand());
        invItem.setIiClass(details.getIiClass());
        invItem.setIiAdLvCategory(details.getIiAdLvCategory());
        invItem.setIiCostMethod(details.getIiCostMethod());
        invItem.setIiUnitCost(details.getIiUnitCost());
        invItem.setIiSalesPrice(details.getIiSalesPrice());
        invItem.setIiEnable(details.getIiEnable());
        invItem.setIiVirtualStore(details.getIiVirtualStore());
        invItem.setIiDoneness(details.getIiDoneness());
        invItem.setIiSidings(details.getIiSidings());
        invItem.setIiRemarks(details.getIiRemarks());
        invItem.setIiServiceCharge(details.getIiServiceCharge());
        invItem.setIiNonInventoriable(details.getIiNonInventoriable());
        invItem.setIiServices(details.getIiServices());
        invItem.setIiJobServices(details.getIiJobServices());
        invItem.setIiIsVatRelief(details.getIiIsVatRelief());
        invItem.setIiIsTax(details.getIiIsTax());
        invItem.setIiIsProject(details.getIiIsProject());
        invItem.setIiPercentMarkup(details.getIiPercentMarkup());
        invItem.setIiShippingCost(details.getIiShippingCost());
        invItem.setIiSpecificGravity(details.getIiSpecificGravity());
        invItem.setIiStandardFillSize(details.getIiStandardFillSize());
        invItem.setIiYield(details.getIiYield());
        invItem.setIiLossPercentage(details.getIiLossPercentage());
        invItem.setIiMarket(details.getIiMarket());
        invItem.setIiEnablePo(details.getIiEnablePo());
        invItem.setIiPoCycle(details.getIiPoCycle());
        invItem.setIiUmcPackaging(details.getIiUmcPackaging());
        invItem.setIiRetailUom(retailUom);
        invItem.setIiOpenProduct(details.getIiOpenProduct());
        invItem.setIiFixedAsset(details.getIiFixedAsset());
        invItem.setIiDateAcquired(details.getIiDateAcquired());
        invItem.setIiDefaultLocation(defaultLocation);
        invItem.setIiAcquisitionCost(details.getIiAcquisitionCost());
        invItem.setIiLifeSpan(details.getIiLifeSpan());
        invItem.setIiResidualValue(details.getIiResidualValue());
        System.out.println("glFixedAssetAccount=" + glFixedAssetAccount);
        System.out.println("glDepreciationAccount=" + glDepreciationAccount);
        System.out.println("glAccumulatedDepreciationAccount=" + glAccumulatedDepreciationAccount);
        invItem.setGlCoaFixedAssetAccount(
            glFixedAssetAccount != null ? glFixedAssetAccount.getCoaCode().toString() : null);
        invItem.setGlCoaDepreciationAccount(
            glDepreciationAccount != null ? glDepreciationAccount.getCoaCode().toString() : null);
        invItem.setGlCoaAccumulatedDepreciationAccount(glAccumulatedDepreciationAccount != null
            ? glAccumulatedDepreciationAccount.getCoaCode().toString()
            : null);
        invItem.setIiCondition(details.getIiCondition());
        invItem.setIiTaxCode(details.getIiTaxCode());
        if (trackMisc) {
          invItem.setIiTraceMisc((byte) 1);
        } else {
          invItem.setIiTraceMisc((byte) 0);
        }
        invItem.setIiScSunday(details.getIiScSunday());
        invItem.setIiScMonday(details.getIiScMonday());
        invItem.setIiScTuesday(details.getIiScTuesday());
        invItem.setIiScWednesday(details.getIiScWednesday());
        invItem.setIiScThursday(details.getIiScThursday());
        invItem.setIiScFriday(details.getIiScFriday());
        invItem.setIiScSaturday(details.getIiScSaturday());



        invItem.setIiLastModifiedBy(details.getIiLastModifiedBy());
        invItem.setIiDateLastModified(details.getIiDateLastModified());


        System.out.print("UPDATE UNIT COST-------------");
        this.updateIiUnitCost(invItem.getIiName(), oldUnitCost, invItem.getIiUnitCost(), AD_CMPNY);
        System.out.print("UPDATE UNIT COST");
        // >>>>------->

        try {
          LocalInvItemLocation invItemLocation = null;

          Collection invItemLocations =
              invItemLocationHome.findByIiName(details.getIiName(), AD_CMPNY);

          Iterator iterIl = invItemLocations.iterator();
          System.out.print("INVENTORY LOCATION---------" + invItemLocations.size());

          while (iterIl.hasNext()) {
            System.out.print("INVENTORY LOCATION---------!!!!!!!!!!!!!!!");
            invItemLocation = (LocalInvItemLocation) iterIl.next();

            Collection adBranchItemLocations =
                adBranchItemLocationHome.findByInvIlAll(invItemLocation.getIlCode(), AD_CMPNY);

            Iterator iterBil = adBranchItemLocations.iterator();
            while (iterBil.hasNext()) {
              adBranchItemLocation = (LocalAdBranchItemLocation) iterBil.next();

              if (adBranchItemLocation.getBilItemDownloadStatus() == 'N') {
                adBranchItemLocation.setBilItemDownloadStatus('N');
              } else if (adBranchItemLocation.getBilItemDownloadStatus() == 'D') {
                adBranchItemLocation.setBilItemDownloadStatus('X');
              } else if (adBranchItemLocation.getBilItemDownloadStatus() == 'U') {
                adBranchItemLocation.setBilItemDownloadStatus('U');
              } else if (adBranchItemLocation.getBilItemDownloadStatus() == 'X') {
                adBranchItemLocation.setBilItemDownloadStatus('X');
              }

            }

          }
        } catch (FinderException ex) {

        }



      }

      LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);
      invItem.setInvUnitOfMeasure(invUnitOfMeasure);

      try {

        LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CD, AD_CMPNY);
        invItem.setApSupplier(apSupplier);

      } catch (FinderException ex) {

        if (invItem.getApSupplier() != null)
          invItem.getApSupplier().dropInvItem(invItem);

      }


      try {
        LocalArCustomer arCustomer = arCustomerHome.findByCstCustomerCode(CST_CSTMR_CD, AD_CMPNY);
        invItem.setArCustomer(arCustomer);

      } catch (FinderException ex) {

        if (invItem.getArCustomer() != null)
          invItem.getArCustomer().dropInvItem(invItem);

      }


      if (isRecalculate) {
        // remove all umc by item

        Collection unitOfMeasureConversions = invItem.getInvUnitOfMeasureConversions();

        Iterator i = unitOfMeasureConversions.iterator();

        while (i.hasNext()) {

          LocalInvUnitOfMeasureConversion unitOfMeasureConversion =
              (LocalInvUnitOfMeasureConversion) i.next();

          i.remove();

          unitOfMeasureConversion.remove();

        }

        // add new umc

        Collection invUnitOfMeasures =
            invUnitOfMeasureHome.findByUomAdLvClass(invUnitOfMeasure.getUomAdLvClass(), AD_CMPNY);

        i = invUnitOfMeasures.iterator();

        while (i.hasNext()) {

          LocalInvUnitOfMeasure unitOfMeasure = (LocalInvUnitOfMeasure) i.next();

          this.addInvUmcEntry(invItem, unitOfMeasure.getUomName(), unitOfMeasure.getUomAdLvClass(),
              unitOfMeasure.getUomConversionFactor(), unitOfMeasure.getUomBaseUnit(), AD_CMPNY);

        }

      }

      return invItem.getIiCode();

    } catch (GlobalRecordAlreadyExistException ex) {

      throw ex;

    } catch (GlobalRecordAlreadyAssignedException ex) {

      throw ex;

    } catch (InvFixedAssetAccountNotFoundException ex) {

      throw ex;

    } catch (InvDepreciationAccountNotFoundException ex) {

      throw ex;

    } catch (InvFixedAssetInformationNotCompleteException ex) {

      throw ex;

    } catch (InvAccumulatedDepreciationAccountNotFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   **/
  public void deleteInvIiEntry(
      Integer II_CODE, Integer AD_CMPNY
  ) throws GlobalRecordAlreadyDeletedException, GlobalRecordAlreadyAssignedException {

    Debug.print("InvItemEntryControllerBean deleteInvIiEntry");

    LocalInvItemHome invItemHome = null;
    LocalInvBillOfMaterialHome invBillOfMaterialHome = null;

    // Initialize EJB Home

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

      invBillOfMaterialHome = (LocalInvBillOfMaterialHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalInvItem invItem = null;

      try {

        invItem = invItemHome.findByPrimaryKey(II_CODE);

      } catch (FinderException ex) {

        throw new GlobalRecordAlreadyDeletedException();

      }

      // if already in used in buildorder stock & stock transfers
      if (!invItem.getInvBuildOrderStocks().isEmpty()
          || !invItem.getInvStockTransferLines().isEmpty()) {

        throw new GlobalRecordAlreadyAssignedException();

      }

      // if already in used in assembly items

      Collection invBillOfMaterials =
          invBillOfMaterialHome.findByBomIiName(invItem.getIiName(), AD_CMPNY);

      if (!invBillOfMaterials.isEmpty())
        throw new GlobalRecordAlreadyAssignedException();

      // if item location is already assigned
      if (!invItem.getInvItemLocations().isEmpty()) {

        Collection itemLocations = invItem.getInvItemLocations();

        Iterator locIter = itemLocations.iterator();

        while (locIter.hasNext()) {

          LocalInvItemLocation itemLocation = (LocalInvItemLocation) locIter.next();

          if (!itemLocation.getApVoucherLineItems().isEmpty()
              || !itemLocation.getArInvoiceLineItems().isEmpty()
              || !itemLocation.getInvAdjustmentLines().isEmpty()
              || !itemLocation.getInvBuildOrderLines().isEmpty()
              || !itemLocation.getInvBuildUnbuildAssemblyLines().isEmpty()
              || !itemLocation.getInvCostings().isEmpty()
              || !itemLocation.getInvPhysicalInventoryLines().isEmpty()
              || !itemLocation.getInvBranchStockTransferLines().isEmpty()
              || !itemLocation.getInvStockIssuanceLines().isEmpty()
              || !itemLocation.getApPurchaseOrderLines().isEmpty()
              || !itemLocation.getApPurchaseRequisitionLines().isEmpty()
              || !itemLocation.getAdBranchItemLocations().isEmpty()
              || !itemLocation.getArSalesOrderLines().isEmpty()
              || !itemLocation.getInvLineItems().isEmpty()) {

            throw new GlobalRecordAlreadyAssignedException();

          }

          // branch item locations
          Collection adBranchItemLocations = itemLocation.getAdBranchItemLocations();

          Iterator bilIter = adBranchItemLocations.iterator();

          while (bilIter.hasNext()) {

            LocalAdBranchItemLocation adBranchItemLocation =
                (LocalAdBranchItemLocation) bilIter.next();

            // if already downloaded to pos
            if (adBranchItemLocation.getBilItemDownloadStatus() == 'D'
                || adBranchItemLocation.getBilItemDownloadStatus() == 'X'
                || adBranchItemLocation.getBilItemLocationDownloadStatus() == 'D'
                || adBranchItemLocation.getBilItemLocationDownloadStatus() == 'X'
                || adBranchItemLocation.getBilLocationDownloadStatus() == 'D'
                || adBranchItemLocation.getBilLocationDownloadStatus() == 'X') {

              throw new GlobalRecordAlreadyAssignedException();

            }

          }

          // remove item location
          locIter.remove();
          itemLocation.remove();

        }
      }

      invItem.remove();

    } catch (GlobalRecordAlreadyDeletedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (GlobalRecordAlreadyAssignedException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public InvModItemDetails getInvIiByIiCode(
      Integer II_CODE, Integer AD_BRNCH, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("InvItemEntryControllerBean getInvIiByIiCode");

    LocalInvItemHome invItemHome = null;
    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    LocalInvLocationHome invLocationHome = null;
    LocalGlChartOfAccountHome glChartOfAccountHome = null;
    LocalInvItem invItem = null;
    LocalGlChartOfAccount glChartOfAccount = null;
    // Initialize EJB Home

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
      invLocationHome = (LocalInvLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
      glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
          .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      try {

        invItem = invItemHome.findByPrimaryKey(II_CODE);

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      InvModItemDetails iiDetails = new InvModItemDetails();

      if (invItem.getIiFixedAsset() == 1) {
        if (invItem.getGlCoaFixedAssetAccount() != null) {
          try {

            glChartOfAccount = glChartOfAccountHome
                .findByPrimaryKey(Integer.parseInt(invItem.getGlCoaFixedAssetAccount()));

          } catch (FinderException ex) {

            throw new GlobalNoRecordFoundException();

          }

          iiDetails.setFixedAssetAccount(glChartOfAccount.getCoaAccountNumber());
          iiDetails.setFixedAssetAccountDescription(glChartOfAccount.getCoaAccountDescription());
        }

        if (invItem.getGlCoaDepreciationAccount() != null) {
          try {
            glChartOfAccount = null;
            glChartOfAccount = glChartOfAccountHome
                .findByPrimaryKey(Integer.parseInt(invItem.getGlCoaDepreciationAccount()));

          } catch (FinderException ex) {

            throw new GlobalNoRecordFoundException();

          }

          iiDetails.setDepreciationAccount(glChartOfAccount.getCoaAccountNumber());
          iiDetails.setDepreciationAccountDescription(glChartOfAccount.getCoaAccountDescription());
          System.out.println("Depreciation: " + glChartOfAccount.getCoaAccountDescription());
        }

        if (invItem.getGlCoaAccumulatedDepreciationAccount() != null) {
          try {
            glChartOfAccount = null;
            glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                Integer.parseInt(invItem.getGlCoaAccumulatedDepreciationAccount()));

          } catch (FinderException ex) {

            throw new GlobalNoRecordFoundException();

          }

          iiDetails.setAccumulatedDepreciationAccount(glChartOfAccount.getCoaAccountNumber());
          iiDetails.setAccumulatedDepreciationAccountDescription(
              glChartOfAccount.getCoaAccountDescription());
          System.out.println("Accumulated: " + glChartOfAccount.getCoaAccountDescription());
        }


      }



      iiDetails.setIiCode(invItem.getIiCode());
      System.out.println(invItem.getIiCode());
      iiDetails.setIiName(invItem.getIiName());
      iiDetails.setIiDescription(invItem.getIiDescription());
      iiDetails.setIiPartNumber(invItem.getIiPartNumber());
      iiDetails.setIiShortName(invItem.getIiShortName());
      iiDetails.setIiBarCode1(invItem.getIiBarCode1());
      iiDetails.setIiBarCode2(invItem.getIiBarCode2());
      iiDetails.setIiBarCode3(invItem.getIiBarCode3());
      iiDetails.setIiBrand(invItem.getIiBrand());
      iiDetails.setIiClass(invItem.getIiClass());
      iiDetails.setIiAdLvCategory(invItem.getIiAdLvCategory());
      iiDetails.setIiCostMethod(invItem.getIiCostMethod());
      iiDetails.setIiUnitCost(invItem.getIiUnitCost());
      iiDetails.setIiAveCost(this.getIiAveCost(invItem, new Date(), AD_BRNCH, AD_CMPNY));
      iiDetails.setIiPercentMarkup(invItem.getIiPercentMarkup());
      iiDetails.setIiShippingCost(invItem.getIiShippingCost());
      iiDetails.setIiSalesPrice(invItem.getIiSalesPrice());
      iiDetails.setIiEnable(invItem.getIiEnable());

      iiDetails.setIiUomName(
          invItem.getInvUnitOfMeasure() != null ? invItem.getInvUnitOfMeasure().getUomName()
              : null);

      iiDetails.setIiDoneness(invItem.getIiDoneness());
      iiDetails.setIiSidings(invItem.getIiSidings());
      iiDetails.setIiRemarks(invItem.getIiRemarks());
      iiDetails.setIiServiceCharge(invItem.getIiServiceCharge());
      iiDetails.setIiNonInventoriable(invItem.getIiNonInventoriable());
      iiDetails.setIiServices(invItem.getIiServices());
      iiDetails.setIiJobServices(invItem.getIiJobServices());
      iiDetails.setIiIsVatRelief(invItem.getIiIsVatRelief());
      iiDetails.setIiIsTax(invItem.getIiIsTax());
      iiDetails.setIiIsProject(invItem.getIiIsProject());
      iiDetails.setIiSplSpplrCode(
          invItem.getApSupplier() != null ? invItem.getApSupplier().getSplSupplierCode() : null);
      iiDetails.setIiCstCustomerCode(
          invItem.getArCustomer() != null ? invItem.getArCustomer().getCstCustomerCode() : null);

      iiDetails.setIiMarket(invItem.getIiMarket());
      iiDetails.setIiEnablePo(invItem.getIiEnablePo());
      iiDetails.setIiPoCycle(invItem.getIiPoCycle());
      iiDetails.setIiUmcPackaging(invItem.getIiUmcPackaging());
      iiDetails.setIiOpenProduct(invItem.getIiOpenProduct());

      iiDetails.setIiFixedAsset(invItem.getIiFixedAsset());
      iiDetails.setIiDateAcquired(invItem.getIiDateAcquired());
      iiDetails.setIiTraceMisc(invItem.getIiTraceMisc());

      iiDetails.setIiScSunday(invItem.getIiScSunday());
      iiDetails.setIiScMonday(invItem.getIiScMonday());
      iiDetails.setIiScTuesday(invItem.getIiScTuesday());
      iiDetails.setIiScWednesday(invItem.getIiScWednesday());
      iiDetails.setIiScThursday(invItem.getIiScThursday());
      iiDetails.setIiScFriday(invItem.getIiScFriday());
      iiDetails.setIiScSaturday(invItem.getIiScSaturday());

      iiDetails.setAcquisitionCost(invItem.getIiAcquisitionCost());
      System.out.println("invItem.getIiAcquisitionCost(): " + invItem.getIiAcquisitionCost());
      iiDetails.setLifeSpan(invItem.getIiLifeSpan());
      iiDetails.setResidualValue(invItem.getIiResidualValue());
      iiDetails.setIiCondition(invItem.getIiCondition());
      iiDetails.setIiTaxCode(invItem.getIiTaxCode());

      iiDetails.setIiDateCreated(invItem.getIiDateCreated());
      iiDetails.setIiCreatedBy(invItem.getIiCreatedBy());
      iiDetails.setIiDateLastModified(invItem.getIiDateLastModified());
      iiDetails.setIiLastModifiedBy(invItem.getIiLastModifiedBy());

      System.out.println("invItem.getIiCondition(): " + invItem.getIiCondition());
      System.out.println("invItem.getIiTraceMisc(): " + invItem.getIiTraceMisc());
      System.out.println("invItem.getIiAcquisitionCost(): " + invItem.getIiAcquisitionCost());
      String retailUom = null;
      if (invItem.getIiRetailUom() != null) {
        LocalInvUnitOfMeasure invRetailUom =
            invUnitOfMeasureHome.findByPrimaryKey(invItem.getIiRetailUom());
        retailUom = invRetailUom.getUomName();
      }
      iiDetails.setIiRetailUomName(retailUom);

      String defaultLocation = null;
      if (invItem.getIiDefaultLocation() != null) {
        LocalInvLocation invLocation =
            invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
        defaultLocation = invLocation.getLocName();
      }
      iiDetails.setIiDefaultLocationName(defaultLocation);

      return iiDetails;

    } catch (GlobalNoRecordFoundException ex) {

      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArTcAll(
      Integer AD_CMPNY
  ) {

    Debug.print("ArInvoiceEntryControllerBean getArTcAll");

    LocalArTaxCodeHome arTaxCodeHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arTaxCodeHome = (LocalArTaxCodeHome) EJBHomeFactory
          .lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection arTaxCodes = arTaxCodeHome.findEnabledTcAll(AD_CMPNY);

      Iterator i = arTaxCodes.iterator();

      while (i.hasNext()) {

        LocalArTaxCode arTaxCode = (LocalArTaxCode) i.next();

        list.add(arTaxCode.getTcName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public short getAdPrfInvQuantityPrecisionUnit(
      Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getAdPrfInvQuantityPrecisionUnit");

    LocalAdPreferenceHome adPreferenceHome = null;

    // Initialize EJB Home

    try {

      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      return adPreference.getPrfInvQuantityPrecisionUnit();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getInvUomAllByUnitMeasureClass(
      String UOM_NM, Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getInvUomAllByUnitMeasureClass");

    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);

      Collection invUnitOfMeasures =
          invUnitOfMeasureHome.findByUomAdLvClass(invUnitOfMeasure.getUomAdLvClass(), AD_CMPNY);

      Iterator i = invUnitOfMeasures.iterator();

      while (i.hasNext()) {

        LocalInvUnitOfMeasure unitOfMeasure = (LocalInvUnitOfMeasure) i.next();

        list.add(unitOfMeasure.getUomName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  // private methods

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdLvInvItemCategoryAll(
      Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getAdLvInvItemCategoryAll");

    LocalAdLookUpValueHome adLookUpValueHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();

        list.add(adLookUpValue.getLvName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public double getIiUnitCostByIiName(
      String II_NM, Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getIiUnitCostByIiName");

    LocalInvItemHome invItemHome = null;

    // Initialize EJB Home

    try {

      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

      return invItem.getIiUnitCost();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public short getGlFcPrecisionUnit(
      Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getGlFcPrecisionUnit");

    LocalAdCompanyHome adCompanyHome = null;


    // Initialize EJB Home

    try {

      adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

      return adCompany.getGlFunctionalCurrency().getFcPrecision();

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public void updateIiUnitCost(
      String itemName, double oldIiUnitCost, double iiUnitCost, Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean updateAssemblyIiUnitCost");

    LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
    LocalInvItemHome invItemHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

    LocalInvBillOfMaterial invBillOfMaterial = null;

    // Initialize EJB Home

    try {

      invBillOfMaterialHome = (LocalInvBillOfMaterialHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);
      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {
;
      short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

      LocalInvItem invBomItem = invItemHome.findByIiName(itemName, AD_CMPNY);

      Collection invBillOfMaterials =
          invBillOfMaterialHome.findByBomIiName(invBomItem.getIiName(), AD_CMPNY);


      if (!invBillOfMaterials.isEmpty()) {
       Iterator i = invBillOfMaterials.iterator();

        while (i.hasNext()) {

          invBillOfMaterial = (LocalInvBillOfMaterial) i.next();

          double quantityNeeded = invBillOfMaterial.getBomQuantityNeeded();
          double totalUnitCost = invBillOfMaterial.getInvItem().getIiUnitCost();
          double oldTotalUnitCost = totalUnitCost;

          LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
              invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBomItem.getIiName(),
                  invBillOfMaterial.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
          LocalInvUnitOfMeasureConversion invDefaultUomConversion =
              invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBomItem.getIiName(),
                  invBomItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
  
          oldIiUnitCost = EJBCommon.roundIt(
              oldIiUnitCost * invDefaultUomConversion.getUmcConversionFactor()
                  / invUnitOfMeasureConversion.getUmcConversionFactor(),
              this.getGlFcPrecisionUnit(AD_CMPNY));
  
          iiUnitCost = EJBCommon.roundIt(
              iiUnitCost * invDefaultUomConversion.getUmcConversionFactor()
                  / invUnitOfMeasureConversion.getUmcConversionFactor(),
              this.getGlFcPrecisionUnit(AD_CMPNY));
          System.out.println("YES3");
          totalUnitCost =
              EJBCommon.roundIt(totalUnitCost - (oldIiUnitCost * quantityNeeded), precisionUnit);
          totalUnitCost =
              EJBCommon.roundIt(totalUnitCost + (iiUnitCost * quantityNeeded), precisionUnit);

          invBillOfMaterial.getInvItem().setIiUnitCost(totalUnitCost);

          this.updateIiUnitCost(invBillOfMaterial.getInvItem().getIiName(), oldTotalUnitCost,
              invBillOfMaterial.getInvItem().getIiUnitCost(), AD_CMPNY);

        }

      }

    } catch (Exception ex) {
  
      Debug.printStackTrace(ex);
      getSessionContext().setRollbackOnly();
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getAdLvInvDonenessAll(
      Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getAdLvInvDonenessAll");

    LocalAdLookUpValueHome adLookUpValueHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection adLookUpValues = adLookUpValueHome.findByLuName("INV DONENESS", AD_CMPNY);

      Iterator i = adLookUpValues.iterator();

      while (i.hasNext()) {

        LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();

        list.add(adLookUpValue.getLvName());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getApSplAll(
      Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getApSplAll");

    LocalApSupplierHome apSupplierHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      apSupplierHome = (LocalApSupplierHome) EJBHomeFactory
          .lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection apSuppliers = apSupplierHome.findEnabledSplAllOrderBySplSupplierCode(AD_CMPNY);

      Iterator i = apSuppliers.iterator();

      while (i.hasNext()) {

        LocalApSupplier apSupplier = (LocalApSupplier) i.next();

        list.add(apSupplier.getSplSupplierCode());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }



  /**
   * @ejb:interface-method view-type="remote"
   * @jboss:method-attributes read-only="true"
   **/
  public ArrayList getArCstAll(
      Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getArCstAll");

    LocalArCustomerHome arCustomerHome = null;
    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      arCustomerHome = (LocalArCustomerHome) EJBHomeFactory
          .lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {
      Collection arCustomers = arCustomerHome.findEnabledCstAllOrderByCstName(AD_CMPNY);

      Iterator i = arCustomers.iterator();

      while (i.hasNext()) {

        LocalArCustomer arCustomer = (LocalArCustomer) i.next();

        list.add(arCustomer.getCstCustomerCode());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  private double getIiAveCost(
      LocalInvItem invItem, Date currentDate, Integer AD_BRNCH, Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getIiAveCost");

    LocalInvCostingHome invCostingHome = null;
    LocalInvLocationHome invLocationHome = null;
    LocalAdBranchHome adBranchHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;

    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invLocationHome = (LocalInvLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
      adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
          LocalAdBranchHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
          .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          


    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {
    
      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
     // TODO Default Locations for now
      String defaultLocation = adPreference.getPrfCentralWarehouseLocation();
      LocalInvLocation invLocation = invLocationHome.findByLocName(defaultLocation, AD_CMPNY);
      // TODO Default Branch for now
      String branchName = adPreference.getPrfCentralWarehouseBranch();
       LocalAdBranch adBranch = adBranchHome.findByBrName(branchName, AD_CMPNY);
      double COST = invItem.getIiUnitCost();
    
      
      try{
       
  
  
        LocalInvItemLocation invItemLocation = invItemLocationHome
            .findByIiNameAndLocName(invItem.getIiName(), defaultLocation, AD_CMPNY);

      
      
      

        COST = this.getInvIiUnitCostByIiNameAndUomName(invItemLocation.getInvItem().getIiName(), invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), new Date(), AD_CMPNY);
  



        return COST;
      
      }catch(FinderException ex){
      
      
         return invItem.getIiUnitCost();
      }

  
    

    


    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }
  
   
 /**
 * @throws FinderException 
 * @ejb:interface-method view-type="remote"
 * @jboss:method-attributes read-only="true"
 **/
  public LocalInvCosting getInvCostingByIiNameAndDate(
       Date currentDate, String II_NM ,Integer AD_CMPNY) 
  {
      Debug.print("InvItemEntryControllerBean getInvCostingByIiNameAndDate");
    
     LocalInvCostingHome invCostingHome = null;
     LocalInvLocationHome invLocationHome = null;
     LocalAdBranchHome adBranchHome = null;
     LocalInvItemLocationHome invItemLocationHome = null;
     LocalAdPreferenceHome adPreferenceHome = null;
    
    
    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      invLocationHome = (LocalInvLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
      adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
          LocalAdBranchHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }
  
  
    try{
    
      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      // TODO Default Locations for now
      String defaultLocation = adPreference.getPrfCentralWarehouseLocation();
      LocalInvLocation invLocation = invLocationHome.findByLocName(defaultLocation, AD_CMPNY);
      // TODO Default Branch for now
      String branchName = adPreference.getPrfCentralWarehouseBranch();
      LocalAdBranch adBranch = adBranchHome.findByBrName(branchName, AD_CMPNY);
      
      
      

      LocalInvItemLocation invItemLocation = invItemLocationHome
          .findByIiNameAndLocName(II_NM, defaultLocation, AD_CMPNY);
          
          
      LocalInvCosting invCosting = invCostingHome
           .getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                currentDate, invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), adBranch.getBrCode(), AD_CMPNY); 
    
    
      return invCosting;
    
    
    }catch(FinderException ex){
    
     	return null;
    }catch(Exception ex){
      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());
    
    }
  
  
  
  }
  
  
  
  
  
 /**
 * @ejb:interface-method view-type="remote"
 * @jboss:method-attributes read-only="true"
 **/
  public double getInvIiUnitCostByIiNameAndUomName(
      String II_NM , String UOM_NM , Date currentDate, Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getInvIiUnitCostByIiNameAndUomName");

    LocalInvCostingHome invCostingHome = null;
    LocalInvLocationHome invLocationHome = null;
    LocalAdBranchHome adBranchHome = null;
    LocalInvItemLocationHome invItemLocationHome = null;
    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
    LocalAdPreferenceHome adPreferenceHome = null;
    LocalInvItemHome invItemHome = null;


    try {

      invCostingHome = (LocalInvCostingHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
     invItemHome = (LocalInvItemHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
      invLocationHome = (LocalInvLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
      adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
          LocalAdBranchHome.class);
      invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
      adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

      // TODO Default Locations for now
      String defaultLocation = adPreference.getPrfCentralWarehouseLocation();
      LocalInvLocation invLocation = invLocationHome.findByLocName(defaultLocation, AD_CMPNY);
      // TODO Default Branch for now
      String branchName = adPreference.getPrfCentralWarehouseBranch();
      LocalAdBranch adBranch = adBranchHome.findByBrName(branchName, AD_CMPNY);

     

      LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
      
 
      double COST = invItem.getIiUnitCost();

      try {
      
         LocalInvItemLocation invItemLocation = invItemLocationHome
          .findByIiNameAndLocName(II_NM, defaultLocation, AD_CMPNY);
          

          if (invItemLocation.getInvItem().getIiCostMethod().equals("Average")) {

                // Get average cost.
            LocalInvCosting invCosting = invCostingHome
           .getLatestAverageCost(
                currentDate, invItemLocation.getInvItem().getIiName(),
                invItemLocation.getInvLocation().getLocName(), adBranch.getBrCode(), AD_CMPNY);
                
                 COST = invCosting.getCstRemainingQuantity() == 0 ? COST
            : Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
          
     

          } else if (invItemLocation.getInvItem().getIiCostMethod()
              .equals("Standard")) {

             COST = invItemLocation.getInvItem().getIiUnitCost();
          }else{
              COST = invItemLocation.getInvItem().getIiUnitCost();
          } 
          
         try{
      
              LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
          
              LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);
        
              COST =  EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(),adPreference.getPrfInvCostPrecisionUnit());
    
          }catch(FinderException ex){
          
                
          }

      

      } catch (FinderException ex) {

        
       

        COST = invItem.getIiUnitCost();
        


      }
      
    
       
      return COST;
 

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }
  
  
 

  private ArrayList getAdBrAll(
      Integer AD_CMPNY
  ) {

    Debug.print("InvItemEntryControllerBean getAdBrAll");

    LocalAdBranchHome adBranchHome = null;

    ArrayList list = new ArrayList();

    // Initialize EJB Home

    try {

      adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
          LocalAdBranchHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      Collection adBranches = adBranchHome.findBrAll(AD_CMPNY);

      Iterator i = adBranches.iterator();

      while (i.hasNext()) {

        LocalAdBranch adBranch = (LocalAdBranch) i.next();

        list.add(adBranch.getBrCode());

      }

      return list;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }

  private void addInvUmcEntry(
      LocalInvItem invItem, String uomName, String uomAdLvClass, double conversionFactor,
      byte umcBaseUnit, Integer AD_CMPNY
  ) throws GlobalNoRecordFoundException {

    Debug.print("InvUnitOfMeasureConversionControllerBean addInvUmcEntry");

    LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
    LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    LocalInvItemHome invItemHome = null;

    // initialize EJB

    try {

      invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME,
              LocalInvUnitOfMeasureConversionHome.class);
      invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome) EJBHomeFactory
          .lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
      invItemHome = (LocalInvItemHome) EJBHomeFactory.lookUpLocalHome(LocalInvItemHome.JNDI_NAME,
          LocalInvItemHome.class);

    } catch (NamingException ex) {

      throw new EJBException(ex.getMessage());

    }

    try {

      // create umc
      LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =
          invUnitOfMeasureConversionHome.create(conversionFactor, umcBaseUnit, AD_CMPNY);

      try {

        // map uom
        LocalInvUnitOfMeasure invUnitOfMeasure =
            invUnitOfMeasureHome.findByUomNameAndUomAdLvClass(uomName, uomAdLvClass, AD_CMPNY);
        invUnitOfMeasureConversion.setInvUnitOfMeasure(invUnitOfMeasure);

        // invUnitOfMeasure.addInvUnitOfMeasureConversion(invUnitOfMeasureConversion);

      } catch (FinderException ex) {

        throw new GlobalNoRecordFoundException();

      }

      // map item
      invUnitOfMeasureConversion.setInvItem(invItem);
      // invItem.addInvUnitOfMeasureConversion(invUnitOfMeasureConversion);

    } catch (GlobalNoRecordFoundException ex) {

      getSessionContext().setRollbackOnly();
      throw ex;

    } catch (Exception ex) {

      Debug.printStackTrace(ex);
      throw new EJBException(ex.getMessage());

    }

  }


  // SessionBean methods

  /**
   * @ejb:create-method view-type="remote"
   **/
  public void ejbCreate(
  ) throws CreateException {

    Debug.print("InvItemEntryControllerBean ejbCreate");

  }

}
