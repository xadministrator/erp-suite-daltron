/*
 * InvItemEntryControllerBean.java
 *
 * Created on June 03, 2004, 4:33 PM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArReceipt;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.InvILCoaGlAccruedInventoryAccountNotFoundException;
import com.ejb.exception.InvILCoaGlCostOfSalesAccountNotFoundException;
import com.ejb.exception.InvILCoaGlInventoryAccountNotFoundException;
import com.ejb.exception.InvILCoaGlSalesAccountNotFoundException;
import com.ejb.exception.InvILCoaGlSalesReturnAccountNotFoundException;
import com.ejb.exception.InvILCoaGlWipAccountNotFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModBranchItemLocationDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvItemDetails;
import com.util.InvModItemDetails;
import com.util.InvModItemLocationDetails;


/**
 * @ejb:bean name="InvItemLocationEntryControllerEJB"
 *           display-name="Used for entering items"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvItemLocationEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvItemLocationEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvItemLocationEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
 */

public class InvItemLocationEntryControllerBean extends AbstractSessionBean {


	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {

		Debug.print("InvItemLocationEntryControllerBean getInvLocAll");

		LocalInvLocationHome invLocationHome = null;
		LocalInvLocation invLocation = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection invLocations = invLocationHome.findLocAll(AD_CMPNY);

			Iterator i = invLocations.iterator();

			while (i.hasNext()) {

				invLocation = (LocalInvLocation)i.next();

				list.add(invLocation.getLocName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvIiAll(Integer AD_CMPNY) {

		Debug.print("InvItemLocationEntryControllerBean getInvIiAll");

		LocalInvItemHome invItemHome = null;
		LocalInvItem invItem = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection invItemLocations = invItemHome.findEnabledIiAll(AD_CMPNY);
			System.out.println("---------------->SIZE="+invItemLocations.size());
			Iterator i = invItemLocations.iterator();

			while (i.hasNext()) {

				invItem = (LocalInvItem)i.next();

				InvModItemDetails details = new InvModItemDetails();

				details.setIiName(invItem.getIiName());
				details.setIiDescription(invItem.getIiDescription());
				details.setIiAdLvCategory(invItem.getIiAdLvCategory());

				list.add(details);

			}

			Collections.sort(list,InvModItemDetails.ItemCategoryGroupComparator);

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public InvItemDetails getInvItemByIiName(String II_NM, Integer AD_CMPNY) {

		Debug.print("InvItemLocationEntryControllerBean getInvItemByIiName");

		LocalInvItemHome invItemHome = null;
		LocalInvItem invItem = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			System.out.println("II_NM : " + II_NM);
			System.out.println("AD_CMPNY : " + AD_CMPNY);

			invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

			InvItemDetails details = new InvItemDetails();

			details.setIiName(invItem.getIiName());
			details.setIiDescription(invItem.getIiDescription());


			return details;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfInvItemLocationShowAll(Integer AD_CMPNY) {

        Debug.print("InvItemLocationEntryControllerBean getAdPrfInvItemLocationShowAll");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfInvItemLocationShowAll();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public byte getAdPrfInvItemLocationAddByItemList(Integer AD_CMPNY) {

        Debug.print("InvItemLocationEntryControllerBean getAdPrfInvItemLocationAddItemList");

        LocalAdPreferenceHome adPreferenceHome = null;


        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfInvItemLocationAddByItemList();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }



	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {

		Debug.print("InvItemLocationEntryControllerBean getAdLvInvItemCategoryAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/

    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException{

    	Debug.print("InvItemLocationEntryControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;

    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adBranchResponsibilities.isEmpty()) {

            throw new GlobalNoRecordFoundException();

        }

        try {

	        Iterator i = adBranchResponsibilities.iterator();

	        while(i.hasNext()) {

	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();

		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());

		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrCoaSegment(adBranch.getBrCoaSegment());
		    	details.setBrType(adBranch.getBrType());

		    	list.add(details);

	        }

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        return list;

    }

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void saveInvIlEntry(com.util.InvModItemLocationDetails mdetails, ArrayList itemList, ArrayList locationList,
			ArrayList branchItemLocationList, ArrayList categoryList, String II_CLSS, Integer RS_CODE, Integer AD_CMPNY)
		throws InvILCoaGlSalesAccountNotFoundException,
		InvILCoaGlSalesReturnAccountNotFoundException,
		   InvILCoaGlInventoryAccountNotFoundException,
		   InvILCoaGlCostOfSalesAccountNotFoundException,
		   InvILCoaGlWipAccountNotFoundException,
		   InvILCoaGlAccruedInventoryAccountNotFoundException {

		Debug.print("InvItemLocationEntryControllerBean saveInvIlEntry");

		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvItemHome invItemHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdBranchHome adBranchHome = null;

		try {

			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {


			String locationName = null;

			Iterator i = locationList.iterator();

			while (i.hasNext()) {

				locationName  = (String)i.next();

				Iterator l = itemList.iterator();

				while (l.hasNext()) {

					String itemName = (String)l.next();

					Iterator k = categoryList.iterator();

					while (k.hasNext()) {

						String category = (String)k.next();

						LocalInvItem invItem = null;

						try {

							invItem = invItemHome.findByIiNameAndIiAdLvCategory(itemName, category, AD_CMPNY);

						} catch (FinderException ex) {

							continue;

						}

						LocalGlChartOfAccount glSalesAccount = null;
						LocalGlChartOfAccount glInventoryAccount = null;
						LocalGlChartOfAccount glCostOfSalesAccount = null;
						LocalGlChartOfAccount glWipAccount = null;
						LocalGlChartOfAccount glAccrdInvAccount = null;
						LocalGlChartOfAccount glSalesReturnAccount = null;
						LocalInvItemLocation invItemLocation = null;

						// get sales account, inventory account and cost of sales account to validate accounts

						try {

							glSalesAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getIlCoaGlSalesAccountNumber(), AD_CMPNY);

						} catch (FinderException ex) {

							throw new InvILCoaGlSalesAccountNotFoundException();

						}

						try {

							glSalesReturnAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getIlCoaGlSalesReturnAccountNumber(), AD_CMPNY);

						} catch (FinderException ex) {

							throw new InvILCoaGlSalesReturnAccountNotFoundException();

						}

						try {

							glInventoryAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getIlCoaGlInventoryAccountNumber(), AD_CMPNY);

						} catch (FinderException ex) {

							throw new InvILCoaGlInventoryAccountNotFoundException();

						}

						try {

							glCostOfSalesAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getIlCoaGlCostOfSalesAccountNumber(), AD_CMPNY);

						} catch (FinderException ex) {

							throw new InvILCoaGlCostOfSalesAccountNotFoundException();

						}

						try {

							glAccrdInvAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getIlCoaGlAccruedInventoryAccountNumber(), AD_CMPNY);

						} catch (FinderException ex) {

							throw new InvILCoaGlAccruedInventoryAccountNotFoundException();

						}

						try {

							glWipAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getIlCoaGlWipAccountNumber(), AD_CMPNY);

						} catch (FinderException ex) {

							throw new InvILCoaGlWipAccountNotFoundException();

						}



						if (II_CLSS != null && II_CLSS.length() > 0 && !invItem.getIiClass().equals(II_CLSS)) continue;

						// create new item location

						try {

							invItemLocation = invItemLocationHome.findByLocNameAndIiName(locationName, itemName, AD_CMPNY);

						} catch (FinderException ex) {

						}

						if (invItemLocation == null) {

							invItemLocation = invItemLocationHome.create(mdetails.getIlRack(),mdetails.getIlBin(),
									mdetails.getIlReorderPoint(), mdetails.getIlReorderQuantity(),
							        mdetails.getIlReorderLevel(), 0d, glSalesAccount.getCoaCode(), glInventoryAccount.getCoaCode(),
							        glCostOfSalesAccount.getCoaCode(), glWipAccount.getCoaCode(), glAccrdInvAccount.getCoaCode(),
							        glSalesReturnAccount.getCoaCode(),
							        mdetails.getIlSubjectToCommission(), AD_CMPNY);


							invItemLocation.setInvItem(invItem);

							LocalInvLocation invLocation = invLocationHome.findByLocName(locationName, AD_CMPNY);
							invItemLocation.setInvLocation(invLocation);
							
							
							invItemLocation.getInvItem().setIiLastModifiedBy(mdetails.getIlLastModifiedBy());
							invItemLocation.getInvItem().setIiDateLastModified(mdetails.getIlDateLastModified());
							Iterator brIter = branchItemLocationList.iterator();

							while(brIter.hasNext()) {

								AdModBranchItemLocationDetails details = (AdModBranchItemLocationDetails)brIter.next();

								try {


									glSalesAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlSalesAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlSalesAccountNotFoundException();

								}

								try {


									glSalesReturnAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlSalesReturnAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlSalesReturnAccountNotFoundException();

								}

								try {

									glInventoryAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlInventoryAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlInventoryAccountNotFoundException();

								}

								try {

									glCostOfSalesAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlCostOfSalesAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlCostOfSalesAccountNotFoundException();

								}

								try {

									glAccrdInvAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlAccruedInventoryAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlAccruedInventoryAccountNotFoundException();

								}

								try {

									glWipAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlWipAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlWipAccountNotFoundException();

								}

								//create new branch item location

								LocalAdBranchItemLocation adBranchItemLocation = adBranchItemLocationHome.create(
										details.getBilRack(), details.getBilBin(),
										details.getBilReorderPoint(), details.getBilReorderQuantity(), glSalesAccount.getCoaCode(),
										glInventoryAccount.getCoaCode(), glCostOfSalesAccount.getCoaCode(), glWipAccount.getCoaCode(),glAccrdInvAccount.getCoaCode(), glSalesReturnAccount.getCoaCode(),
										details.getBilSubjectToCommission(), details.getBilHist1Sales(), details.getBilHist2Sales(), details.getBilProjectedSales(),
										details.getBilDeliveryTime(), details.getBilDeliveryBuffer(), details.getBilOrderPerYear(), 'N','N','N', AD_CMPNY);
								
								adBranchItemLocation.setInvItemLocation(invItemLocation);
								LocalAdBranch adBranch = adBranchHome.findByBrName(details.getBilBrName(), AD_CMPNY);
								adBranchItemLocation.setAdBranch(adBranch);

							}

						} else  {

							// update item location

							invItemLocation.setIlRack(mdetails.getIlRack());
							invItemLocation.setIlBin(mdetails.getIlBin());
							invItemLocation.setIlReorderPoint(mdetails.getIlReorderPoint());
							invItemLocation.setIlReorderQuantity(mdetails.getIlReorderQuantity());
							invItemLocation.setIlReorderLevel(mdetails.getIlReorderLevel());
							invItemLocation.setIlGlCoaSalesAccount(glSalesAccount.getCoaCode());
							invItemLocation.setIlGlCoaInventoryAccount(glInventoryAccount.getCoaCode());
							invItemLocation.setIlGlCoaCostOfSalesAccount(glCostOfSalesAccount.getCoaCode());
							invItemLocation.setIlGlCoaWipAccount(glWipAccount.getCoaCode());
							invItemLocation.setIlGlCoaAccruedInventoryAccount(glAccrdInvAccount.getCoaCode());
							invItemLocation.setIlGlCoaSalesReturnAccount(glSalesReturnAccount.getCoaCode());
							invItemLocation.setIlSubjectToCommission(mdetails.getIlSubjectToCommission());
							invItemLocation.getInvItem().setIiLastModifiedBy(mdetails.getIlLastModifiedBy());
							invItemLocation.getInvItem().setIiDateLastModified(mdetails.getIlDateLastModified());
							// set download status

							AdModBranchItemLocationDetails details = null;

							Iterator brIter = branchItemLocationList.iterator();

							while(brIter.hasNext()) {

									details = (AdModBranchItemLocationDetails)brIter.next();

									LocalAdBranch adBranch = adBranchHome.findByBrName(details.getBilBrName(), AD_CMPNY);

									LocalAdBranchItemLocation adBranchItemLocation = null;

									try{

										adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(invItemLocation.getIlCode(), adBranch.getBrCode(), AD_CMPNY);

									} catch (FinderException ex) {

									}

									if(adBranchItemLocation == null) {

										details.setBilItemDownloadStatus('N');
										details.setBilLocationDownloadStatus('N');
										details.setBilItemLocationDownloadStatus('N');

									} else {

										if(adBranchItemLocation.getBilItemLocationDownloadStatus() == 'N'){
											details.setBilItemLocationDownloadStatus('N');
										}else if(adBranchItemLocation.getBilItemLocationDownloadStatus() == 'D'){
											details.setBilItemLocationDownloadStatus('X');
										}else if(adBranchItemLocation.getBilItemLocationDownloadStatus() == 'U'){
											details.setBilItemLocationDownloadStatus('U');
										}else if(adBranchItemLocation.getBilItemLocationDownloadStatus() == 'X'){
											details.setBilItemLocationDownloadStatus('X');
										}
										details.setBilItemDownloadStatus(adBranchItemLocation.getBilItemDownloadStatus());
										details.setBilLocationDownloadStatus(adBranchItemLocation.getBilLocationDownloadStatus());


									}

							}

							// remove all branch item locations

							try {

								Collection adBranchItemLocations = adBranchItemLocationHome.findBilByIlCodeAndRsCode(invItemLocation.getIlCode(), RS_CODE, AD_CMPNY);

								Iterator bilIter = adBranchItemLocations.iterator();

								while(bilIter.hasNext()) {

									LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)bilIter.next();
									invItemLocation.dropAdBranchItemLocation(adBranchItemLocation);

									LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(adBranchItemLocation.getAdBranch().getBrCode());
									adBranch.dropAdBranchItemLocation(adBranchItemLocation);

									adBranchItemLocation.remove();

								}

							} catch(FinderException ex) {


							}

							// add branch item locations

							brIter = branchItemLocationList.iterator();

							while(brIter.hasNext()) {

								//AdModBranchItemLocationDetails
								details = (AdModBranchItemLocationDetails)brIter.next();

								try {

									glSalesAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlSalesAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlSalesAccountNotFoundException();

								}

								try {

									glSalesReturnAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlSalesReturnAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlSalesReturnAccountNotFoundException();

								}

								try {

									glInventoryAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlInventoryAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlInventoryAccountNotFoundException();

								}

								try {

									glCostOfSalesAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlCostOfSalesAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlCostOfSalesAccountNotFoundException();

								}

								try {

									glAccrdInvAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlAccruedInventoryAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlAccruedInventoryAccountNotFoundException();

								}

								try {

									glWipAccount = glChartOfAccountHome.findByCoaAccountNumber(details.getBilCoaGlWipAccountNumber(), AD_CMPNY);

								} catch (FinderException ex) {

									throw new InvILCoaGlWipAccountNotFoundException();

								}

								//create new branch item location
								LocalAdBranchItemLocation adBranchItemLocation = adBranchItemLocationHome.create(
										details.getBilRack(), details.getBilBin(),
										details.getBilReorderPoint(),
										details.getBilReorderQuantity(), glSalesAccount.getCoaCode(), glInventoryAccount.getCoaCode(),
										glCostOfSalesAccount.getCoaCode(), glWipAccount.getCoaCode(), glAccrdInvAccount.getCoaCode(),  glSalesReturnAccount.getCoaCode(),
										details.getBilSubjectToCommission(), details.getBilHist1Sales(), details.getBilHist2Sales(), details.getBilProjectedSales(),
										details.getBilDeliveryTime(), details.getBilDeliveryBuffer(), details.getBilOrderPerYear(),
										details.getBilItemDownloadStatus(),
										details.getBilLocationDownloadStatus(), details.getBilItemLocationDownloadStatus(), AD_CMPNY);

								adBranchItemLocation.setInvItemLocation(invItemLocation);
								LocalAdBranch adBranch = adBranchHome.findByBrName(details.getBilBrName(), AD_CMPNY);
								adBranchItemLocation.setAdBranch(adBranch);
								
								//assign modified item
								adBranchItemLocation.getInvItemLocation().getInvItem().setIiLastModifiedBy(details.getBilLastModifiedBy());
								adBranchItemLocation.getInvItemLocation().getInvItem().setIiDateLastModified(details.getBilDateLastModified());

								
								

							}

						}
					}

				}
			}

		} catch (InvILCoaGlSalesAccountNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (InvILCoaGlSalesReturnAccountNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (InvILCoaGlInventoryAccountNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (InvILCoaGlCostOfSalesAccountNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (InvILCoaGlWipAccountNotFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}
	}


	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void updateInvIliAveSales(ArrayList itemList, ArrayList locationList,
			ArrayList branchItemLocationList, ArrayList categoryList, String II_CLSS, Integer RS_CODE, Integer AD_CMPNY) {

		Debug.print("InvItemLocationEntryControllerBean updateInvIliAveSales");

		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvItemHome invItemHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdBranchHome adBranchHome = null;

		try {

			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {


			String locationName = null;

			Iterator i = locationList.iterator();

			while (i.hasNext()) {

				locationName  = (String)i.next();

				Iterator l = itemList.iterator();

				while (l.hasNext()) {

					String itemName = (String)l.next();

					Iterator k = categoryList.iterator();

					while (k.hasNext()) {

						String category = (String)k.next();

						LocalInvItem invItem = null;

						try {

							invItem = invItemHome.findByIiNameAndIiAdLvCategory(itemName, category, AD_CMPNY);

						} catch (FinderException ex) {

							continue;

						}


						if (II_CLSS != null && II_CLSS.length() > 0 && !invItem.getIiClass().equals(II_CLSS)) continue;

						// create new item location
						LocalInvItemLocation invItemLocation = null;

						try {

							invItemLocation = invItemLocationHome.findByLocNameAndIiName(locationName, itemName, AD_CMPNY);

						} catch (FinderException ex) {
							continue;
						}

						// update item location


						Iterator brIter = branchItemLocationList.iterator();

						while(brIter.hasNext()) {

							    AdModBranchItemLocationDetails details = (AdModBranchItemLocationDetails)brIter.next();

								LocalAdBranch adBranch = adBranchHome.findByBrName(details.getBilBrName(), AD_CMPNY);

								LocalAdBranchItemLocation adBranchItemLocation = null;

								try{

									adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(invItemLocation.getIlCode(), adBranch.getBrCode(), AD_CMPNY);

								} catch (FinderException ex) {

									continue;

								}

								adBranchItemLocation.setBilHist1Sales(details.getBilHist1Sales());
								adBranchItemLocation.setBilHist2Sales(details.getBilHist1Sales());
								adBranchItemLocation.setBilProjectedSales(details.getBilProjectedSales());
								adBranchItemLocation.setBilDeliveryTime(details.getBilDeliveryTime());
								adBranchItemLocation.setBilDeliveryBuffer(details.getBilDeliveryBuffer());
								adBranchItemLocation.setBilOrderPerYear(details.getBilOrderPerYear());

						}


					}

				}
			}

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteInvIlEntry(ArrayList itemList, ArrayList locationList, ArrayList categoryList, String II_CLSS, Integer AD_CMPNY)
		throws GlobalRecordAlreadyAssignedException {

		Debug.print("InvItemLocationEntryControllerBean deleteInvIlEntry");

		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvItemHome invItemHome = null;

		try {

			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			 boolean isFirstArgument = false;
			 int ctr = 1;
			 Iterator i = itemList.iterator();
			 String item = "";

			 while (i.hasNext()) {

				if (ctr == 1) item = item + "(";

				item = item + "il.invItem.iiName='" + (String)i.next() + "'";

				if (ctr < itemList.size()) item = item + " OR "; else item = item + ")";

				ctr++;
				isFirstArgument = true;
			 }

			 ctr = 1;
			 i = locationList.iterator();
			 String location = "";

			 while (i.hasNext()) {

				 if (ctr == 1 && isFirstArgument) location = location + " AND ("; else if (ctr == 1 && !isFirstArgument) location = location + "(";

				location = location + "il.invLocation.locName='" + (String)i.next() + "'";

				if (ctr < locationList.size()) location = location + " OR "; else location = location + ")";

				ctr++;
				isFirstArgument = true;

			 }

			 ctr = 1;
			 i = categoryList.iterator();
			 String category = "";

			 while (i.hasNext()) {

				 if (ctr == 1 && isFirstArgument) category = category + " AND ("; else if(ctr ==1 && !isFirstArgument) category = category + "(";

				category = category + "il.invItem.iiAdLvCategory='" + (String)i.next() + "'";

				if (ctr < categoryList.size()) category = category + " OR "; else category = category + ")";

				ctr++;
				isFirstArgument = true;

			 }

			 String company = "";
			 if (isFirstArgument) company = " AND il.ilAdCompany=" + AD_CMPNY; else company = "il.ilAdCompany=" + AD_CMPNY;



			 StringBuffer jbossQl = new StringBuffer();
			 jbossQl.append("SELECT OBJECT(il) FROM InvItemLocation il WHERE " + item +  location + category + company);

			 System.out.println(jbossQl);

			 Collection invItemLocations = invItemLocationHome.getIlByCriteria(jbossQl.toString(), new Object[0]);

			 Iterator iter = invItemLocations.iterator();

			 boolean isException = false;

			 while (iter.hasNext()) {

				 LocalInvItemLocation invItemLocation = (LocalInvItemLocation)iter.next();

				 System.out.println("invItemLocation " + invItemLocation.getInvItem().getIiName() + invItemLocation.getInvLocation().getLocName());

				 if(!invItemLocation.getApVoucherLineItems().isEmpty() ||
	                    !invItemLocation.getArInvoiceLineItems().isEmpty() ||
	                    !invItemLocation.getInvAdjustmentLines().isEmpty() ||
	                    !invItemLocation.getInvBuildOrderLines().isEmpty() ||
	                    !invItemLocation.getInvBuildUnbuildAssemblyLines().isEmpty() ||
	                    !invItemLocation.getInvCostings().isEmpty() ||
	                    !invItemLocation.getInvPhysicalInventoryLines().isEmpty() ||

	                    !invItemLocation.getInvBranchStockTransferLines().isEmpty() ||
						!invItemLocation.getInvStockIssuanceLines().isEmpty() ||
						!invItemLocation.getApPurchaseOrderLines().isEmpty() ||
						!invItemLocation.getApPurchaseRequisitionLines().isEmpty()||
	                 	!invItemLocation.getArSalesOrderLines().isEmpty()||
	                 	!invItemLocation.getInvLineItems().isEmpty()) {

	                 isException = true;
	                 continue;

				 }

				 invItemLocation.remove();

			}

			if (isException) throw new GlobalRecordAlreadyAssignedException();


		} catch (GlobalRecordAlreadyAssignedException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public InvModItemLocationDetails getInvIlByIlCode(Integer IL_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException  {

		Debug.print("InvItemLocationEntryControllerBean getInvIlByIlCode");

		LocalInvItemLocationHome invItemLocationHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;

		LocalInvItemLocation invItemLocation = null;

		// Initialize EJB Home

		try {

			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			try {

				invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			InvModItemLocationDetails ilDetails = new InvModItemLocationDetails();
			ilDetails.setIlCode(invItemLocation.getIlCode());
			ilDetails.setIlRack(invItemLocation.getIlRack());
			ilDetails.setIlBin(invItemLocation.getIlBin());
			ilDetails.setIlReorderPoint(invItemLocation.getIlReorderPoint());
			ilDetails.setIlReorderQuantity(invItemLocation.getIlReorderQuantity());
			ilDetails.setIlReorderLevel(invItemLocation.getIlReorderLevel());

			LocalGlChartOfAccount glSalesAccount = glChartOfAccountHome.findByPrimaryKey(invItemLocation.getIlGlCoaSalesAccount());
			LocalGlChartOfAccount glSalesReturnAccount = glChartOfAccountHome.findByPrimaryKey(invItemLocation.getIlGlCoaSalesReturnAccount());

			LocalGlChartOfAccount glInventoryAccount = glChartOfAccountHome.findByPrimaryKey(invItemLocation.getIlGlCoaInventoryAccount());
			LocalGlChartOfAccount glCostOfSalesAccount = glChartOfAccountHome.findByPrimaryKey(invItemLocation.getIlGlCoaCostOfSalesAccount());
			LocalGlChartOfAccount glWipAccount = glChartOfAccountHome.findByPrimaryKey(invItemLocation.getIlGlCoaWipAccount());
			LocalGlChartOfAccount glAccrdInvAccount = glChartOfAccountHome.findByPrimaryKey(invItemLocation.getIlGlCoaAccruedInventoryAccount());

			ilDetails.setIlCoaGlSalesAccountNumber(glSalesAccount.getCoaAccountNumber());
			ilDetails.setIlCoaGlSalesAccountDescription(glSalesAccount.getCoaAccountDescription());
			ilDetails.setIlCoaGlSalesReturnAccountNumber(glSalesReturnAccount.getCoaAccountNumber());
			ilDetails.setIlCoaGlSalesReturnAccountDescription(glSalesReturnAccount.getCoaAccountDescription());
			ilDetails.setIlCoaGlInventoryAccountNumber(glInventoryAccount.getCoaAccountNumber());
			ilDetails.setIlCoaGlInventoryAccountDescription(glInventoryAccount.getCoaAccountDescription());
			ilDetails.setIlCoaGlCostOfSalesAccountNumber(glCostOfSalesAccount.getCoaAccountNumber());
			ilDetails.setIlCoaGlCostOfSalesAccountDescription(glCostOfSalesAccount.getCoaAccountDescription());
			ilDetails.setIlCoaGlWipAccountNumber(glWipAccount.getCoaAccountNumber());
			ilDetails.setIlCoaGlWipAccountDescription(glWipAccount.getCoaAccountDescription());
			ilDetails.setIlCoaGlAccruedInventoryAccountNumber(glAccrdInvAccount.getCoaAccountNumber());
			ilDetails.setIlCoaGlAccruedInventoryAccountDescription(glAccrdInvAccount.getCoaAccountDescription());
			ilDetails.setIlSubjectToCommission(invItemLocation.getIlSubjectToCommission());

			ilDetails.setIlIiName(invItemLocation.getInvItem() != null ?
					invItemLocation.getInvItem().getIiName() :
						null);
			ilDetails.setIlIiDescription(invItemLocation.getInvItem() != null ?
					invItemLocation.getInvItem().getIiDescription() :
						null);
			ilDetails.setIlLocName(invItemLocation.getInvLocation() != null ?
					invItemLocation.getInvLocation().getLocName() :
						null);

			Collection adBranchItemLocations = invItemLocation.getAdBranchItemLocations();

			Iterator i = adBranchItemLocations.iterator();

			while(i.hasNext()) {

				LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)i.next();

				AdModBranchItemLocationDetails bilDetails = new AdModBranchItemLocationDetails();

				bilDetails.setBilBrCode(adBranchItemLocation.getAdBranch().getBrCode());
				bilDetails.setBilBrName(adBranchItemLocation.getAdBranch().getBrName());
				bilDetails.setBilRack(adBranchItemLocation.getBilRack());
				bilDetails.setBilBin(adBranchItemLocation.getBilBin());
				bilDetails.setBilReorderPoint(adBranchItemLocation.getBilReorderPoint());
				bilDetails.setBilReorderQuantity(adBranchItemLocation.getBilReorderQuantity());

				glSalesAccount = glChartOfAccountHome.findByPrimaryKey(adBranchItemLocation.getBilCoaGlSalesAccount());
				glSalesReturnAccount = glChartOfAccountHome.findByPrimaryKey(adBranchItemLocation.getBilCoaGlSalesReturnAccount());

				glInventoryAccount = glChartOfAccountHome.findByPrimaryKey(adBranchItemLocation.getBilCoaGlInventoryAccount());
				glCostOfSalesAccount = glChartOfAccountHome.findByPrimaryKey(adBranchItemLocation.getBilCoaGlCostOfSalesAccount());
				glWipAccount = glChartOfAccountHome.findByPrimaryKey(adBranchItemLocation.getBilCoaGlWipAccount());
				glAccrdInvAccount = glChartOfAccountHome.findByPrimaryKey(adBranchItemLocation.getBilCoaGlAccruedInventoryAccount());

				bilDetails.setBilCoaGlSalesAccountNumber(glSalesAccount.getCoaAccountNumber());
				bilDetails.setBilCoaGlSalesAccountDescription(glSalesAccount.getCoaAccountDescription());
				bilDetails.setBilCoaGlSalesReturnAccountNumber(glSalesReturnAccount.getCoaAccountNumber());
				bilDetails.setBilCoaGlSalesReturnAccountDescription(glSalesReturnAccount.getCoaAccountDescription());

				bilDetails.setBilCoaGlInventoryAccountNumber(glInventoryAccount.getCoaAccountNumber());
				bilDetails.setBilCoaGlInventoryAccountDescription(glInventoryAccount.getCoaAccountDescription());
				bilDetails.setBilCoaGlCostOfSalesAccountNumber(glCostOfSalesAccount.getCoaAccountNumber());
				bilDetails.setBilCoaGlCostOfSalesAccountDescription(glCostOfSalesAccount.getCoaAccountDescription());
				bilDetails.setBilCoaGlWipAccountNumber(glWipAccount.getCoaAccountNumber());
				bilDetails.setBilCoaGlWipAccountDescription(glWipAccount.getCoaAccountDescription());
				bilDetails.setBilCoaGlAccruedInventoryAccountNumber(glAccrdInvAccount.getCoaAccountNumber());
				bilDetails.setBilCoaGlAccruedInventoryAccountDescription(glAccrdInvAccount.getCoaAccountDescription());
				bilDetails.setBilSubjectToCommission(adBranchItemLocation.getBilSubjectToCommission());
				bilDetails.setBilHist1Sales(adBranchItemLocation.getBilHist1Sales());
				bilDetails.setBilHist2Sales(adBranchItemLocation.getBilHist2Sales());
				bilDetails.setBilProjectedSales(adBranchItemLocation.getBilProjectedSales());
				bilDetails.setBilDeliveryTime(adBranchItemLocation.getBilDeliveryTime());
				bilDetails.setBilDeliveryBuffer(adBranchItemLocation.getBilDeliveryBuffer());
				bilDetails.setBilOrderPerYear(adBranchItemLocation.getBilOrderPerYear());


				ilDetails.saveBrIlList(bilDetails);

			}

			return ilDetails;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfInvQuantityPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("InvItemLocationEntryControllerBean getAdPrfInvQuantityPrecisionUnit");

		LocalAdPreferenceHome adPreferenceHome = null;


		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			return adPreference.getPrfInvQuantityPrecisionUnit();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public void saveInvIlReorderPointAndReorderQuantity(ArrayList locationList, ArrayList itemList, Integer RS_CODE, Integer AD_CMPNY) {

		Debug.print("InvItemLocationEntryControllerBean saveInvIlReorderPointAndReorderQuantity");

		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalInvBillOfMaterialHome invBillOfMaterialHome = null;


		// Initialize EJB Home

		try {

		    invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
		    invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
		    adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
		    adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
		    invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
		    invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

		    String locationName = null;

			Iterator l = locationList.iterator();

			while (l.hasNext()) {

				locationName  = (String)l.next();

				Iterator i = itemList.iterator();

				while (i.hasNext()) {

					String itemName = (String)i.next();

					// get inv item location
					LocalInvItemLocation invItemLocation = null;

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(locationName, itemName, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if(invItemLocation == null) continue;


					// get all branch item locations

					try {

						LocalAdBranchItemLocation adHqBranchItemLocation = null;

						double hqAverageSales = 0;

						Collection adBranchItemLocations = adBranchItemLocationHome.findBilByIlCodeAndRsCode(invItemLocation.getIlCode(), RS_CODE, AD_CMPNY);

						Iterator bilIter = adBranchItemLocations.iterator();

						while(bilIter.hasNext()) {

							LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)bilIter.next();

							// set default variables
							double histSales1 = adBranchItemLocation.getBilHist1Sales();
							double histSales2 = adBranchItemLocation.getBilHist2Sales();
							double projectedSales = adBranchItemLocation.getBilProjectedSales();


							// find hist 1 actual sales
							GregorianCalendar gcHistDateFrom = new GregorianCalendar();
							gcHistDateFrom.set(Calendar.MONTH, 0);
							gcHistDateFrom.set(Calendar.DATE, 1);
							gcHistDateFrom.set(Calendar.HOUR, 0);
							gcHistDateFrom.set(Calendar.MINUTE, 0);
							gcHistDateFrom.set(Calendar.SECOND, 0);
							gcHistDateFrom.add(Calendar.YEAR, -1);
							GregorianCalendar gcHistDateTo = new GregorianCalendar();
							gcHistDateTo.set(Calendar.MONTH, 11);
							gcHistDateTo.set(Calendar.DATE, 31);
							gcHistDateTo.set(Calendar.HOUR, 0);
							gcHistDateTo.set(Calendar.MINUTE, 0);
							gcHistDateTo.set(Calendar.SECOND, 0);
							gcHistDateTo.add(Calendar.YEAR, -1);
							double tempHistSales1 = 0;
							Collection invHist1Costing = invCostingHome.findQtySoldByCstDateFromAndCstDateToAndIlCode(gcHistDateFrom.getTime(),
									gcHistDateTo.getTime(), invItemLocation.getIlCode(), adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
							Iterator invHist1CostingIter = invHist1Costing.iterator();
							while (invHist1CostingIter.hasNext()) {

								LocalInvCosting invCosting = (LocalInvCosting)invHist1CostingIter.next();
								if (invCosting.getArSalesOrderInvoiceLine() != null) {

									LocalArInvoice arInvoice = invCosting.getArSalesOrderInvoiceLine().getArInvoice();
									tempHistSales1 += this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
											arInvoice.getGlFunctionalCurrency().getFcName(),
											arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
											invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount(), AD_CMPNY);

								} else if (invCosting.getArInvoiceLineItem() != null) {

									if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {
										LocalArInvoice arInvoice = invCosting.getArInvoiceLineItem().getArInvoice();
										tempHistSales1 += this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
												arInvoice.getGlFunctionalCurrency().getFcName(),
												arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
												invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), AD_CMPNY);
									} else {
										LocalArReceipt arReceipt = invCosting.getArInvoiceLineItem().getArReceipt();
										tempHistSales1 += this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
												arReceipt.getGlFunctionalCurrency().getFcName(),
												arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
												invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), AD_CMPNY);
									}

								}

							}

							//************************** add sales from BOM

							Collection invBillOfMaterials =  invBillOfMaterialHome.findByBomIiNameAndLocNameAndBrCode(invItemLocation.getInvItem().getIiName(),
									invItemLocation.getInvLocation().getLocName(), AD_CMPNY);
							Iterator bomIter = invBillOfMaterials.iterator();

							while (bomIter.hasNext()) {

								LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) bomIter.next();
								Collection invItemLocations = invItemLocationHome.findByIiNameAndAdBranch(invBillOfMaterial.getInvItem().getIiName(),
										adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
								Iterator invIlIter = invItemLocations.iterator();
								while (invIlIter.hasNext()) {

									LocalInvItemLocation invAssemblyItemLocation = (LocalInvItemLocation)invIlIter.next();

									Collection invAssemblyHist1Costing = invCostingHome.findQtySoldByCstDateFromAndCstDateToAndIlCode(gcHistDateFrom.getTime(),
											gcHistDateTo.getTime(), invAssemblyItemLocation.getIlCode(), adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
									Iterator invAssemblyHist1CostingIter = invAssemblyHist1Costing.iterator();
									while (invAssemblyHist1CostingIter.hasNext()) {

										LocalInvCosting invCosting = (LocalInvCosting)invAssemblyHist1CostingIter.next();
										tempHistSales1 += invItemLocation.getInvItem().getIiUnitCost() *
											invCosting.getCstQuantitySold() *
											this.convertByUomFromAndItemAndQuantity(invBillOfMaterial.getInvUnitOfMeasure(),
													invItemLocation.getInvItem(), invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY) *
										 	(1 + (invAssemblyItemLocation.getInvItem().getIiPercentMarkup() / 100));

									}

								}

							}

							if (tempHistSales1 > 0) histSales1 = tempHistSales1;

							// find hist 2 actual sales
							gcHistDateFrom.add(Calendar.YEAR, -1);
							gcHistDateTo.add(Calendar.YEAR, -1);
							double tempHistSales2 = 0;
							Collection invHist2Costing = invCostingHome.findQtySoldByCstDateFromAndCstDateToAndIlCode(gcHistDateFrom.getTime(),
									gcHistDateTo.getTime(), invItemLocation.getIlCode(), adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
							Iterator invHist2CostingIter = invHist1Costing.iterator();
							while (invHist1CostingIter.hasNext()) {

								LocalInvCosting invCosting = (LocalInvCosting)invHist2CostingIter.next();
								if (invCosting.getArSalesOrderInvoiceLine() != null) {

									LocalArInvoice arInvoice = invCosting.getArSalesOrderInvoiceLine().getArInvoice();
									tempHistSales2 += this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
											arInvoice.getGlFunctionalCurrency().getFcName(),
											arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
											invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount(), AD_CMPNY);

								} else if (invCosting.getArInvoiceLineItem() != null) {

									if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {
										LocalArInvoice arInvoice = invCosting.getArInvoiceLineItem().getArInvoice();
										tempHistSales2 += this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
												arInvoice.getGlFunctionalCurrency().getFcName(),
												arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
												invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), AD_CMPNY);
									} else {
										LocalArReceipt arReceipt = invCosting.getArInvoiceLineItem().getArReceipt();
										tempHistSales2 += this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
												arReceipt.getGlFunctionalCurrency().getFcName(),
												arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
												invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), AD_CMPNY);
									}

								}

							}

//							************************** add sales from BOM

							bomIter = invBillOfMaterials.iterator();

							while (bomIter.hasNext()) {

								LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) bomIter.next();
								Collection invItemLocations = invItemLocationHome.findByIiNameAndAdBranch(invBillOfMaterial.getInvItem().getIiName(),
										adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
								Iterator invIlIter = invItemLocations.iterator();
								while (invIlIter.hasNext()) {

									LocalInvItemLocation invAssemblyItemLocation = (LocalInvItemLocation)invIlIter.next();

									Collection invAssemblyHist2Costing = invCostingHome.findQtySoldByCstDateFromAndCstDateToAndIlCode(gcHistDateFrom.getTime(),
											gcHistDateTo.getTime(), invAssemblyItemLocation.getIlCode(), adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
									Iterator invAssemblyHist2CostingIter = invAssemblyHist2Costing.iterator();
									while (invAssemblyHist2CostingIter.hasNext()) {

										LocalInvCosting invCosting = (LocalInvCosting)invAssemblyHist2CostingIter.next();
										tempHistSales2 += invItemLocation.getInvItem().getIiUnitCost() *
											invCosting.getCstQuantitySold() *
											this.convertByUomFromAndItemAndQuantity(invBillOfMaterial.getInvUnitOfMeasure(),
													invItemLocation.getInvItem(), invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY) *
										 	(1 + (invAssemblyItemLocation.getInvItem().getIiPercentMarkup() / 100));

									}

								}

							}

							if (tempHistSales2 > 0) histSales2 = tempHistSales2;


							// get actual sales per month

							GregorianCalendar gcDateFrom = new GregorianCalendar();
							gcDateFrom.set(Calendar.MONTH, 0);
							gcDateFrom.set(Calendar.DATE, 1);
							gcDateFrom.set(Calendar.HOUR, 0);
							gcDateFrom.set(Calendar.MINUTE, 0);
							gcDateFrom.set(Calendar.SECOND, 0);
							GregorianCalendar gcDateTo = new GregorianCalendar();
							gcDateTo.set(Calendar.MONTH, 0);
							gcDateTo.set(Calendar.DATE, 31);
							gcDateTo.set(Calendar.HOUR, 0);
							gcDateTo.set(Calendar.MINUTE, 0);
							gcDateTo.set(Calendar.SECOND, 0);

							double actualSales = 0;
							int projectedSalesMonths = 0;
							int actualSalesMonths = 0;
							boolean isSalesFound = false;
							for (int j=0; j<=12; j++) {

								double runningActual = 0;
								Collection invCostings = invCostingHome.findQtySoldByCstDateFromAndCstDateToAndIlCode(gcDateFrom.getTime(), gcDateTo.getTime(), invItemLocation.getIlCode(), adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
								Iterator invCostingIter = invCostings.iterator();
								while (invCostingIter.hasNext()) {

									LocalInvCosting invCosting = (LocalInvCosting)invCostingIter.next();
									if (invCosting.getArSalesOrderInvoiceLine() != null) {

										LocalArInvoice arInvoice = invCosting.getArSalesOrderInvoiceLine().getArInvoice();
										runningActual += this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
												arInvoice.getGlFunctionalCurrency().getFcName(),
												arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
												invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount(), AD_CMPNY);

									} else if (invCosting.getArInvoiceLineItem() != null) {

										if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {
											LocalArInvoice arInvoice = invCosting.getArInvoiceLineItem().getArInvoice();
											runningActual += this.convertForeignToFunctionalCurrency(arInvoice.getGlFunctionalCurrency().getFcCode(),
													arInvoice.getGlFunctionalCurrency().getFcName(),
													arInvoice.getInvConversionDate(), arInvoice.getInvConversionRate(),
													invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), AD_CMPNY);
										} else {
											LocalArReceipt arReceipt = invCosting.getArInvoiceLineItem().getArReceipt();
											runningActual += this.convertForeignToFunctionalCurrency(arReceipt.getGlFunctionalCurrency().getFcCode(),
													arReceipt.getGlFunctionalCurrency().getFcName(),
													arReceipt.getRctConversionDate(), arReceipt.getRctConversionRate(),
													invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount(), AD_CMPNY);
										}

									}

								}

//								************************** add sales from BOM
								bomIter = invBillOfMaterials.iterator();

								while (bomIter.hasNext()) {

									LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) bomIter.next();
									Collection invItemLocations = invItemLocationHome.findByIiNameAndAdBranch(invBillOfMaterial.getInvItem().getIiName(),
											adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
									Iterator invIlIter = invItemLocations.iterator();
									while (invIlIter.hasNext()) {

										LocalInvItemLocation invAssemblyItemLocation = (LocalInvItemLocation)invIlIter.next();

										Collection invAssemblyActualCosting = invCostingHome.findQtySoldByCstDateFromAndCstDateToAndIlCode(gcDateFrom.getTime(), gcDateTo.getTime(), invAssemblyItemLocation.getIlCode(), adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
										Iterator invAssemblyActualCostingIter = invAssemblyActualCosting.iterator();
										while (invAssemblyActualCostingIter.hasNext()) {

											LocalInvCosting invCosting = (LocalInvCosting)invAssemblyActualCostingIter.next();
											runningActual += invItemLocation.getInvItem().getIiUnitCost() *
												invCosting.getCstQuantitySold() *
												this.convertByUomFromAndItemAndQuantity(invBillOfMaterial.getInvUnitOfMeasure(),
														invItemLocation.getInvItem(), invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY) *
											 	(1 + (invAssemblyItemLocation.getInvItem().getIiPercentMarkup() / 100));

										}

									}

								}

								actualSales += runningActual;
								if (runningActual == 0 && !isSalesFound) {
									projectedSalesMonths++;
								} else {
									isSalesFound = true;
								}
								if (runningActual > 0) {
									actualSalesMonths++;
								}

								gcDateFrom.add(Calendar.MONTH, 1);
								gcDateTo.add(Calendar.MONTH, 1);
								gcDateTo.set(Calendar.DATE, gcDateTo.getActualMaximum(Calendar.DATE));

							}

							if (actualSales > 0) {
								double aveMonthlyProjectedSales = projectedSales / 12;
								aveMonthlyProjectedSales = aveMonthlyProjectedSales * projectedSalesMonths;
								projectedSales = ((actualSales + aveMonthlyProjectedSales) / (projectedSalesMonths + actualSalesMonths) * 12);
							}

							int divisor = 0;

							if (histSales1 != 0) divisor++;
							if (histSales2 != 0) divisor++;
							if (projectedSales != 0) divisor++;
							if (divisor == 0) divisor = 1;

							double averageSales = (histSales1 +
												   histSales2 +
							                       projectedSales) / divisor;

							if (adBranchItemLocation.getAdBranch().getBrHeadQuarter() == EJBCommon.FALSE) {

								double deliveryBuffer = adBranchItemLocation.getBilDeliveryBuffer();
								double deliveryTime = adBranchItemLocation.getBilDeliveryTime();
								double dailyCons = averageSales / 360;
								double minimumInventory = dailyCons * deliveryBuffer;
								double reorderPoint = (dailyCons * deliveryTime) + minimumInventory;
								double reorderQty = averageSales / adBranchItemLocation.getBilOrderPerYear();

								reorderPoint = EJBCommon.roundIt(reorderPoint, (short)3);
								reorderQty = EJBCommon.roundIt(reorderQty, (short)3);

								// set reorder point & quantity
								adBranchItemLocation.setBilReorderPoint(reorderPoint);
								adBranchItemLocation.setBilReorderQuantity(reorderQty);
								hqAverageSales += averageSales;

							} else {

								adHqBranchItemLocation = adBranchItemLocation;

							}

						}

						if (adHqBranchItemLocation != null) {

							double deliveryBuffer = adHqBranchItemLocation.getBilDeliveryBuffer();
							double deliveryTime = adHqBranchItemLocation.getBilDeliveryTime();
							double dailyCons = hqAverageSales / 360;
							double minimumInventory = dailyCons * deliveryBuffer;
							double reorderPoint = (dailyCons * deliveryTime) + minimumInventory;
							double reorderQty = hqAverageSales / adHqBranchItemLocation.getBilOrderPerYear();

							reorderPoint = EJBCommon.roundIt(reorderPoint, (short)3);
							reorderQty = EJBCommon.roundIt(reorderQty, (short)3);

							// set reorder point & quantity
							adHqBranchItemLocation.setBilReorderPoint(reorderPoint);
							adHqBranchItemLocation.setBilReorderQuantity(reorderQty);
							adHqBranchItemLocation = null;
						}

					} catch(FinderException ex) {


					}

				}

			}


		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	// private method
	private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

        Debug.print("InvItemLocationEntryControllerBean convertByUomFromAndItemAndQuantity");

        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM,
            Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {

        Debug.print("InvItemLocationEntryControllerBean convertForeignToFunctionalCurrency");


        LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
        LocalAdCompanyHome adCompanyHome = null;

        LocalAdCompany adCompany = null;

        // Initialize EJB Homes

        try {

            glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        // get company and extended precision

        try {

            adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());

        }


        // Convert to functional currency if necessary

        if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {

            AMOUNT = AMOUNT * CONVERSION_RATE;

        } else if (CONVERSION_DATE != null) {

            try {

                // Get functional currency rate

                LocalGlFunctionalCurrencyRate glReceiptFunctionalCurrencyRate = null;

                if (!FC_NM.equals("USD")) {

                    glReceiptFunctionalCurrencyRate =
                        glFunctionalCurrencyRateHome.findByFcCodeAndDate(FC_CODE,
                                CONVERSION_DATE, AD_CMPNY);

                    AMOUNT = AMOUNT * glReceiptFunctionalCurrencyRate.getFrXToUsd();

                }

                // Get set of book functional currency rate if necessary

                if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")) {

                    LocalGlFunctionalCurrencyRate glCompanyFunctionalCurrencyRate =
                        glFunctionalCurrencyRateHome.findByFcCodeAndDate(adCompany.getGlFunctionalCurrency().
                                getFcCode(), CONVERSION_DATE, AD_CMPNY);

                    AMOUNT = AMOUNT / glCompanyFunctionalCurrencyRate.getFrXToUsd();

                }


            } catch (Exception ex) {

                throw new EJBException(ex.getMessage());

            }

        }

        return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());

    }

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("InvItemLocationEntryControllerBean ejbCreate");

	}
}