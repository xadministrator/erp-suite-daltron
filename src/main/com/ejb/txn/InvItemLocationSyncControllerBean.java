
/*
 * InvItemLocationSyncControllerBean
 *
 * Created on November 9, 2005, 1:01 PM
 *
 * @author  Dann Ryan M. Hilario
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.inv.LocalInvItemLocation;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="InvItemLocationSyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *           
 * @wsee:port-component name="InvItemLocationSync"
 *
 * @jboss:port-component uri="omega-ejb/InvItemLocationSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.InvItemLocationSyncWS"
 * 
*/

public class InvItemLocationSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
	
	/**
     * @ejb:interface-method
     **/
    public int getInvItemLocationAllNewLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("InvItemLocationSyncControllerBean getInvItemsLocationAllNewLength");
    	
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection adBranchItemLocations = adBranchItemLocationHome.findIlByIlNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
        	
        	return adBranchItemLocations.size();       
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public int getInvItemLocationAllUpdatedLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("InvItemLocationSyncControllerBean getInvItemsLocationsAllUpdatedLength");
    	
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection adBranchItemLocations = adBranchItemLocationHome.findIlByIlNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');
        	
        	return adBranchItemLocations.size();       
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
	
	/**
     * @ejb:interface-method
     **/
    public String[] getInvItemLocationAllNewAndUpdated(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("InvItemLocationSyncControllerBean getInvItemsLocationAllNewAndUpdated");
    	
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	System.out.println("Branch : "+BR_BRNCH_CODE);
        	System.out.println("Company : "+AD_CMPNY);
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection adBranchItemLocations = adBranchItemLocationHome.findIlByIlNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'U', 'X');
        	
        	String[] results = new String[adBranchItemLocations.size()];
        	System.out.println(adBranchItemLocations.size());
        	
        	Iterator i = adBranchItemLocations.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	
	        	LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)i.next();
	        	
	        	System.out.println("Loc +"+adBranchItemLocation.getInvItemLocation().getInvLocation().getLocName());
	        	System.out.println("Item +"+adBranchItemLocation.getInvItemLocation().getInvItem().getIiName());
	        	System.out.println("Status +"+adBranchItemLocation.getBilItemLocationDownloadStatus());
	        	
	        	results[ctr] = itemLocationRowEncode(adBranchItemLocation.getInvItemLocation());
	        	System.out.println(results[ctr]);
	        	ctr++;
	        	
	        }

	        return results;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public int setInvItemLocationAllNewAndUpdatedSuccessConfirmation(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("InvItemLocationSyncControllerBean setInvItemLocationsAllNewAndUpdatedSuccessConfirmation");
    	
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {

        	adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection adBranchItemLocations = adBranchItemLocationHome.findIlByIlNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'U', 'X');
        	
        	Iterator i = adBranchItemLocations.iterator();        	
	        while (i.hasNext()) {
	        	
	        	LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)i.next();	    

                       adBranchItemLocation.setBilItemLocationDownloadStatus('D');
	        	
	        }
        	
        } catch (Exception ex) {
        	
        	ctx.setRollbackOnly();
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
        return 0;
    }
    
    private String itemLocationRowEncode(LocalInvItemLocation invItemLocation) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer encodedResult = new StringBuffer();

    	// Start separator
    	encodedResult.append(separator);
    	
    	// Item Code / OPOS: Item Code
    	encodedResult.append(invItemLocation.getInvItem().getIiCode().toString());
    	encodedResult.append(separator);
    	
    	// Location Code / OPOS: Location Code
    	encodedResult.append(invItemLocation.getInvLocation().getLocCode().toString());
    	encodedResult.append(separator);
    	
    	// End separator
    	encodedResult.append(separator);
    	
    	return encodedResult.toString();
    	
    }

    public void ejbCreate() throws CreateException {

       Debug.print("InvItemLocationSyncControllerBean ejbCreate");
      
    }
    
    public void ejbRemove() {};

    public void ejbActivate() {}
    public void ejbPassivate() {}

    public void setSessionContext(SessionContext ctx) {}

}