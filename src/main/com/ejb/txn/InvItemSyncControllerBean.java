/*
 * InvItemSyncControllerBean
 *
 * Created on November 9, 2005, 1:01 PM
 *
 * @author  Dann Ryan M. Hilario
 */

package com.ejb.txn;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalDocument;
import com.ejb.ad.LocalAdApprovalDocumentHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchCustomer;
import com.ejb.ad.LocalAdBranchCustomerHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchStandardMemoLine;
import com.ejb.ad.LocalAdBranchStandardMemoLineHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDiscount;
import com.ejb.ad.LocalAdDiscountHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPaymentSchedule;
import com.ejb.ad.LocalAdPaymentScheduleHome;
import com.ejb.ad.LocalAdPaymentTerm;
import com.ejb.ad.LocalAdPaymentTermHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApDistributionRecord;
import com.ejb.ap.LocalApDistributionRecordHome;
import com.ejb.ap.LocalApPurchaseOrder;
import com.ejb.ap.LocalApPurchaseOrderHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApTaxCode;
import com.ejb.ap.LocalApTaxCodeHome;
import com.ejb.ap.LocalApWithholdingTaxCodeHome;
import com.ejb.ar.LocalArAutoAccountingSegment;
import com.ejb.ar.LocalArAutoAccountingSegmentHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerBalance;
import com.ejb.ar.LocalArCustomerBalanceHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArDistributionRecord;
import com.ejb.ar.LocalArDistributionRecordHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceBatch;
import com.ejb.ar.LocalArInvoiceBatchHome;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLine;
import com.ejb.ar.LocalArInvoiceLineHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoicePaymentSchedule;
import com.ejb.ar.LocalArInvoicePaymentScheduleHome;
import com.ejb.ar.LocalArPdc;
import com.ejb.ar.LocalArPdcHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.ar.LocalArStandardMemoLine;
import com.ejb.ar.LocalArStandardMemoLineHome;
import com.ejb.ar.LocalArTaxCode;
import com.ejb.ar.LocalArTaxCodeHome;
import com.ejb.ar.LocalArWithholdingTaxCode;
import com.ejb.ar.LocalArWithholdingTaxCodeHome;
import com.ejb.exception.AdPRFCoaGlPettyCashAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ArINVAmountExceedsCreditLimitException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalSupplierItemInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.exception.InvATRAssemblyQtyGreaterThanAvailableQtyException;
import com.ejb.exception.InvILCoaGlAccruedInventoryAccountNotFoundException;
import com.ejb.exception.InvILCoaGlCostOfSalesAccountNotFoundException;
import com.ejb.exception.InvILCoaGlInventoryAccountNotFoundException;
import com.ejb.exception.InvILCoaGlSalesAccountNotFoundException;
import com.ejb.exception.InvILCoaGlWipAccountNotFoundException;
import com.ejb.genfld.LocalGenField;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.ejb.inv.LocalInvBranchStockTransferHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvBuildOrder;
import com.ejb.inv.LocalInvBuildOrderHome;
import com.ejb.inv.LocalInvBuildOrderLine;
import com.ejb.inv.LocalInvBuildOrderLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelDate;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AdModBranchItemLocationDetails;
import com.util.ApModPurchaseOrderDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModReceiptDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvAdjustmentDetails;
import com.util.InvModAdjustmentDetails;
import com.util.InvModAdjustmentLineDetails;
import com.util.InvModBranchStockTransferDetails;
import com.util.InvModBranchStockTransferLineDetails;
import com.util.InvModItemDetails;

/**
 * @ejb:bean name="InvItemSyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *
 * @wsee:port-component name="InvItemSync"
 *
 * @jboss:port-component uri="omega-ejb/InvItemSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.InvItemSyncWS"
 *
 */

public class InvItemSyncControllerBean implements SessionBean {

	private SessionContext ctx;
	private String DATE_FORMAT_OUTPUT = "MM/dd/yyyy";
	double CostA=0d;
	double CostB=0d;
	double UnitA=0d;
	double UnitB=0d;
	/**
	 * @ejb:interface-method
	 **/
	public int getInvItemAllNewLength(String BR_BRNCH_CODE, String II_IL_LOCATION, Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean getInvItemAllNewLength");

		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			//Get all New Items
                        System.out.println("NEW ITEM SYNC");
			System.out.println("II_IL_LOCATION-"+II_IL_LOCATION);
			System.out.println("adBranch="+adBranch.getBrCode());
			System.out.println("AD_CMPNY="+AD_CMPNY);


			Collection invItemsNew = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, 'N', 'N', 'N');
			System.out.println("invItemsNew="+invItemsNew.size());

                        Iterator i = invItemsNew.iterator();


                        while(i.hasNext()){
                            LocalInvItem  invItem = (LocalInvItem)i.next();


                            System.out.println(invItem.getIiCode() + " itemCode NEW");

                        }


			return invItemsNew.size();



		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method
	 **/
	public int getInvItemAllNewLengthAnyLoc(String BR_BRNCH_CODE, Integer AD_CMPNY){


		Debug.print("InvItemSyncControllerBean getInvItemAllNewLength");

		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			//Get all New Items
			Collection invItemsNew = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');

			System.out.println("invItemsNew"+invItemsNew.size());
			return invItemsNew.size();



		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}


	}


	/**
	 * @throws RemoteException
	 * @ejb:interface-method
	 **/
	public int generateIssuance(String ADJ_DCMNT_NMBR,Integer BR_BRNCH_CODE, Integer AD_CMPNY,String USERname) throws RemoteException{


		Debug.print("InvItemSyncControllerBean generateIssuance");
		InvAdjustmentRequestControllerHome homeADJ = null;
        InvAdjustmentRequestController ejbADJ = null;
    	InvAdjustmentEntryControllerHome homeADJ2 = null;
    	InvAdjustmentEntryController ejbADJ2 = null;
		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalInvAdjustmentHome invAdjustmentHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		int success=0;
		// Initialize EJB Home

		try {
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			 homeADJ = (InvAdjustmentRequestControllerHome)com.util.EJBHomeFactory.
             lookUpHome("ejb/InvAdjustmentRequestControllerEJB", InvAdjustmentRequestControllerHome.class);
			 homeADJ2 = (InvAdjustmentEntryControllerHome)com.util.EJBHomeFactory.
             lookUpHome("ejb/InvAdjustmentEntryControllerEJB", InvAdjustmentEntryControllerHome.class);
			invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
		    invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
        	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		LocalInvAdjustment invAdjustment = null;

    	// validate if Adjustment is already deleted




    	try {
    		try {

                ejbADJ = homeADJ.create();


             }
    		catch(CreateException ex)
             {

    			System.out.print(ex.toString());



             }
 		try {


                ejbADJ2 = homeADJ2.create();

             }
    		catch(CreateException ex)
             {

    			System.out.print(ex.toString());



             }

    			invAdjustment = invAdjustmentHome.findByAdjDocumentNumberAndBrCode(ADJ_DCMNT_NMBR,BR_BRNCH_CODE,AD_CMPNY);

    			InvAdjustmentDetails details = new InvAdjustmentDetails();

                System.out.print("SAVE REQUEST5");
                details.setAdjCode( invAdjustment.getAdjCode());
                //System.out.println("invAdjustment.getAdjCode(): " + invAdjustment.getAdjCode());
                details.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
                //System.out.println("invAdjustment.getAdjDocumentNumber() : " + invAdjustment.getAdjDocumentNumber());
                details.setAdjReferenceNumber(invAdjustment.getAdjReferenceNumber());
                //System.out.println("invAdjustment.getAdjReferenceNumber() : " + invAdjustment.getAdjReferenceNumber());
                details.setAdjDescription(invAdjustment.getAdjDescription());
                //System.out.println("invAdjustment.getAdjDescription() : " + invAdjustment.getAdjDescription());
                details.setAdjDate(EJBCommon.convertStringToSQLDate(invAdjustment.getAdjDate().toString()));
                //System.out.println("invAdjustment.getAdjDate().toString() : " + invAdjustment.getAdjDate().toString());
                details.setAdjType("REQUEST");
                details.setAdjCreatedBy(invAdjustment.getAdjCreatedBy());
                //System.out.println("invAdjustment.getAdjCreatedBy() : " + invAdjustment.getAdjCreatedBy());
                details.setAdjDateCreated(EJBCommon.convertStringToSQLDate(invAdjustment.getAdjDateCreated().toString()));
                //System.out.println("invAdjustment.getAdjDateCreated().toString() : " + invAdjustment.getAdjDateCreated().toString());
                details.setAdjLastModifiedBy(invAdjustment.getAdjLastModifiedBy());
                details.setAdjDateLastModified(EJBCommon.convertStringToSQLDate(invAdjustment.getAdjDateLastModified().toString()));
                details.setAdjVoid(invAdjustment.getAdjVoid());
                details.setAdjIsCostVariance(invAdjustment.getAdjIsCostVariance());
                details.setAdjPosted(EJBCommon.FALSE);
                details.setAdjNotedBy(invAdjustment.getAdjNotedBy());
                details.setAdjApprovalStatus("DRAFT");
                //System.out.print("adfsdafdsaffsadfdsa");


                ArrayList alList = new ArrayList();
                Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

	        	Iterator i = invAdjustmentLines.iterator();
	        	int line =1;

	        	while (i.hasNext()) {

	        		LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

	        		InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();
	        		LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(), invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_CMPNY);
	        		double actual=0d;
	        		double unposted=0d;
	        		try{
	        			System.out.println("XD");
	        			//System.out.println("invItemLocation.getInvItem().getIiName( : " +invItemLocation.getInvItem().getIiName());
	        			//System.out.println("invItemLocation.getInvLocation().getLocName() : " + invItemLocation.getInvLocation().getLocName());
	        			//System.out.println("invAdjustmentLine.getInvUnitOfMeasure().getUomName() : " + invAdjustmentLine.getInvUnitOfMeasure().getUomName());
	        			//System.out.println("invAdjustment.getAdjDate() : " + invAdjustment.getAdjDate());
	        			//System.out.println("BR_BRNCH_CODE : " + BR_BRNCH_CODE);
	        			//System.out.println("AD_CMPNY : " + AD_CMPNY);




	        			 actual = ejbADJ.getQuantityByIiNameAndUomName(invItemLocation.getInvItem().getIiName(),
     	        		 invItemLocation.getInvLocation().getLocName(), invAdjustmentLine.getInvUnitOfMeasure().getUomName(),invAdjustment.getAdjDate(), BR_BRNCH_CODE, AD_CMPNY);

	        			 System.out.println("A "+invItemLocation.getInvItem().getIiName());
	        				try{
	    	        			Collection UnpostedIssuance=null;
	    	        			UnpostedIssuance = invAdjustmentLineHome.findUnPostedIssuance(AD_CMPNY);
	    	        			Iterator i2 = UnpostedIssuance.iterator();
	    	        			System.out.println("size "+UnpostedIssuance.size());
	    	        			while (i2.hasNext()) {
	    	        				LocalInvAdjustmentLine invAdjustmentLine2 = (LocalInvAdjustmentLine)i2.next();
	    	        				System.out.println("B "+invAdjustmentLine2.getInvItemLocation().getInvItem().getIiName());
	    	        				if(invItemLocation.getInvItem().getIiName()==invAdjustmentLine2.getInvItemLocation().getInvItem().getIiName())
	    	        				unposted+=invAdjustmentLine2.getAlAdjustQuantity();
	    	        				System.out.println("DOC "+invAdjustmentLine2.getInvAdjustment().getAdjDocumentNumber());
	    	        			}


	    	        			}
	    	        			catch(Exception ex)
	    	        			{
	    	        				System.out.print("aw");
	    	        				System.out.print(ex.toString());
	    	        			}

	        			 actual=actual+unposted;
	        			System.out.println("unposted : " + unposted);
	        			System.out.println("actual : " + actual);
	        		}
	        		catch(Exception ex)
	        		{
	        			System.out.print(ex.toString());
	        		}

	        		LocalInvAdjustmentLine adjustmentRequestLine= invAdjustmentLineHome.findByAlCode(invAdjustmentLine.getAlCode(),AD_CMPNY);
	        		double toserved=0d;
	        		double served = adjustmentRequestLine.getAlServed();
	        		double adjust=invAdjustmentLine.getAlAdjustQuantity();
	        		adjust=adjust*-1;
	        		double tobeadjusted= adjust-served;
	        		//System.out.print("served = "+ served);
	            	//System.out.print("adjust1 = "+ tobeadjusted);
	            	adjust=tobeadjusted;
	            	if(actual !=0)
	            	   {
	            		   if(actual<=adjust )
	            		   	{
	            			   	toserved=actual;
	            			    served+=toserved;
	            			    mdetails.setAlServed(served);

	            	   }else
	            	   { // if actual > adjustby, served=adjustby means no balance

	            		    toserved=adjust;
	            		    served+=toserved;

	                		mdetails.setAlServed(served);
	                		//System.out.print("Part2");
	            	   }

	            	//   negates values of adjustby
	                    if(adjust>0)
	                 {
	            	    mdetails.setAlAdjustQuantity(toserved*-1);

	            	   //System.out.println("served2");
	                  }else
	                 {
	                	  mdetails.setAlAdjustQuantity(toserved);

	                   System.out.println("served1 "+toserved);
	                   }


	                    System.out.print("NO LAYER");
	                    System.out.print("SAVE REQUEST2");
	                	   mdetails.setAlUnitCost(invAdjustmentLine.getAlUnitCost());
	                	   System.out.println("invAdjustmentLine.getAlUnitCost() : " + invAdjustmentLine.getAlUnitCost());

	                	   //mdetails.setAlAdjustQuantity(invAdjustmentLine.getAlAdjustQuantity());
	                	   mdetails.setAlUomName(invAdjustmentLine.getInvUnitOfMeasure().getUomName());
	                	   //System.out.println("invAdjustmentLine.getInvUnitOfMeasure().getUomName()) : " + invAdjustmentLine.getInvUnitOfMeasure().getUomName());
	                	   mdetails.setAlLocName(invItemLocation.getInvLocation().getLocName());
	                	   //System.out.println("invItemLocation.getInvLocation().getLocName() : " + invItemLocation.getInvLocation().getLocName());
	                	   mdetails.setAlIiName(invItemLocation.getInvItem().getIiName());
	                	   //System.out.println("invItemLocation.getInvItem().getIiName()) : " + invItemLocation.getInvItem().getIiName());
	                	   mdetails.setAlCode(invAdjustmentLine.getAlCode());
	                	   //System.out.println("invAdjustmentLine.getAlCode() : " + invAdjustmentLine.getAlCode());
	                	   //mdetails.setAlServed(invAdjustmentLine.getAlAdjustQuantity());
	                	   mdetails.setAlLineNumber((short)line);
	                	   mdetails.setAlMisc(invAdjustmentLine.getAlMisc());
	                	   //System.out.print("SAVE REQUEST");
	                	   //System.out.println("invADJList.getMisc()1 : " + invAdjustmentLine.getAlMisc());

	                	   //System.out.print("BRCODE "+BR_BRNCH_CODE);
	                	   //System.out.print("COMCODE "+AD_CMPNY);


	                	   if(toserved==0)
	                	   {

	                	   }
	                	   else
	                	   {
	                	   alList.add(mdetails);
	                	   line++;
	                	   }




                }


    	}
	         	try{
	        		//System.out.print("alist="+alList.size());
	        		int code=0;
	        	if(alList.size()>0)
	        		{
	        		code = ejbADJ.saveInvAdjEntry1(alList, invAdjustment.getAdjCode(),USERname , BR_BRNCH_CODE, AD_CMPNY);

	        		}
	        	//System.out.print("code= "+code);
	        	success=code;

	        	}
	        	catch(Exception ex)
	        	{
	        		System.out.print(ex.toString());
	        	}
    	} catch (FinderException ex) {



    	}


        return success;
	}

	/**
	 * @ejb:interface-method
	 **/
	public int getInvItemAllUpdatedLength(String BR_BRNCH_CODE, String II_IL_LOCATION, Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean getInvItemAllUpdatedLength");

		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;
		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			//Get all New Items
			Collection invItemsNew = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, "U".charAt(0), "U".charAt(0), "X".charAt(0));
			System.out.println("UPDATE ITEM SYNC");
                        System.out.println("II_IL_LOCATION-"+II_IL_LOCATION);
			System.out.println("adBranch="+adBranch.getBrCode());
			System.out.println("AD_CMPNY="+AD_CMPNY);
                        System.out.println("invItemsNew="+invItemsNew.size());

                        Iterator i = invItemsNew.iterator();

                       while(i.hasNext()){
                            LocalInvItem  invItem = (LocalInvItem)i.next();


                            System.out.println(invItem.getIiCode() + " itemCode UPDATE");

                        }

			return invItemsNew.size();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method
	 **/
	public int getInvItemAllUpdatedLengthAnyLoc(String BR_BRNCH_CODE, Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean getInvItemAllUpdatedLength");

		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;
		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			//Get all New Items
			Collection invItemsNew = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');

			return invItemsNew.size();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}


	/**
	 * @ejb:interface-method
	 **/
	public String getInvItemAllDownloadedAnyLoc(String BR_BRNCH_CODE, Integer AD_CMPNY){

		Debug.print("InvItemSyncControllerBean getInvItemAllDownloaded");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer InvItemAllDownloaded = new StringBuffer();
		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			Collection invDownloadedItems = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'D', 'D', 'D');

			Iterator i = invDownloadedItems.iterator();

			while (i.hasNext()) {
				LocalInvItem invItem = (LocalInvItem)i.next();

				InvItemAllDownloaded.append(separator);
				InvItemAllDownloaded.append(invItem.getIiCode());
			}

			InvItemAllDownloaded.append(separator);
			Debug.print(InvItemAllDownloaded.toString());

			return InvItemAllDownloaded.toString();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	/**
	 * @ejb:interface-method
	 **/
	public String getInvItemAllDownloaded(String BR_BRNCH_CODE, String II_IL_LOCATION, Integer AD_CMPNY){

		Debug.print("InvItemSyncControllerBean getInvItemAllDownloaded");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer InvItemAllDownloaded = new StringBuffer();
		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			Collection invDownloadedItems = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, 'D', 'D', 'D');

			Iterator i = invDownloadedItems.iterator();

			while (i.hasNext()) {
				LocalInvItem invItem = (LocalInvItem)i.next();

				InvItemAllDownloaded.append(separator);
				InvItemAllDownloaded.append(invItem.getIiCode());
			}

			InvItemAllDownloaded.append(separator);
			Debug.print(InvItemAllDownloaded.toString());

			return InvItemAllDownloaded.toString();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method
	 **/
	public String getInvItemAllDownloadedWithUnitCost(String BR_BRNCH_CODE, String II_IL_LOCATION, Integer AD_CMPNY){

		Debug.print("InvItemSyncControllerBean getInvItemAllDownloadedWithUnitCost");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer InvItemAllDownloaded = new StringBuffer();
		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			Collection invDownloadedItems = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, 'D', 'D', 'D');

			Iterator i = invDownloadedItems.iterator();

			while (i.hasNext()) {
				LocalInvItem invItem = (LocalInvItem)i.next();

				InvItemAllDownloaded.append(separator);
				InvItemAllDownloaded.append(invItem.getIiCode());
			}

			InvItemAllDownloaded.append(separator);
			Debug.print(InvItemAllDownloaded.toString());

			return InvItemAllDownloaded.toString();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method
	 **/
	public String[] getInvItemAllNewAndUpdatedAnyLoc(String BR_BRNCH_CODE, Integer AD_CMPNY){

		Debug.print("InvItemSyncControllerBean getInvItemAllNewAndUpdated");

		LocalInvItemHome invItemHome = null;
		LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		String II_IL_LOCATION = "";

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);

			Collection invItems = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
			Collection invUpdatedItems = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');

			String[] results = new String[invItems.size() + invUpdatedItems.size()];

			Iterator i = invItems.iterator();
			int ctr = 0;

			while (i.hasNext()) {

				LocalInvItem invItem = (LocalInvItem)i.next();

				String retailUom = null;
				if (invItem.getIiRetailUom() != null) {
					LocalInvUnitOfMeasure invRetailUom = invUnitOfMeasureHome.findByPrimaryKey(invItem.getIiRetailUom());
					retailUom = invRetailUom.getUomName();
				}

				Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invItem.getIiName() , AD_CMPNY);

				if(invBillOfMaterials.size() <=0 ) {

					results[ctr] = itemRowEncode(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.FALSE, 0, 0, "NA", retailUom);

				}else {

					results[ctr] = itemRowEncode(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.TRUE, 0, 0, "NA", retailUom);

				}
				ctr++;
			}

			i = invUpdatedItems.iterator();

			while (i.hasNext()) {

				LocalInvItem invItem = (LocalInvItem)i.next();

				String retailUom = null;
				if (invItem.getIiRetailUom() != null) {
					LocalInvUnitOfMeasure invRetailUom = invUnitOfMeasureHome.findByPrimaryKey(invItem.getIiRetailUom());
					retailUom = invRetailUom.getUomName();
				}

				Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invItem.getIiName() , AD_CMPNY);

				if(invBillOfMaterials.size() <=0 ) {

					results[ctr] = itemRowEncode(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.FALSE, 0, 0, "NA", retailUom);

				}else {

					results[ctr] = itemRowEncode(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.TRUE, 0, 0, "NA", retailUom);

				}
				ctr++;
			}

			System.out.print("results length = "+ results.length);

			return results;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method
	 **/
	public String[] getInvItemAllNewAndUpdated(String BR_BRNCH_CODE, String II_IL_LOCATION, Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean getInvItemAllNewAndUpdated");

		LocalInvItemHome invItemHome = null;
		LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);

			Collection invItems = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, 'N', 'N', 'N');
			Collection invUpdatedItems = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, 'U', 'U', 'X');

			String[] results = new String[invItems.size() + invUpdatedItems.size()];

			Iterator i = invItems.iterator();
			int ctr = 0;

			while (i.hasNext()) {

				LocalInvItem invItem = (LocalInvItem)i.next();

				String retailUom = null;
				if (invItem.getIiRetailUom() != null) {
					LocalInvUnitOfMeasure invRetailUom = invUnitOfMeasureHome.findByPrimaryKey(invItem.getIiRetailUom());
					retailUom = invRetailUom.getUomName();
				}

				Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invItem.getIiName() , AD_CMPNY);

				if(invBillOfMaterials.size() <=0 ) {

					results[ctr] = itemRowEncode(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.FALSE, 0, 0, "NA", retailUom);

				}else {

					results[ctr] = itemRowEncode(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.TRUE, 0, 0, "NA", retailUom);

				}
				ctr++;
			}

			i = invUpdatedItems.iterator();

			while (i.hasNext()) {

				LocalInvItem invItem = (LocalInvItem)i.next();

				String retailUom = null;
				if (invItem.getIiRetailUom() != null) {
					LocalInvUnitOfMeasure invRetailUom = invUnitOfMeasureHome.findByPrimaryKey(invItem.getIiRetailUom());
					retailUom = invRetailUom.getUomName();
				}

				Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invItem.getIiName() , AD_CMPNY);

				if(invBillOfMaterials.size() <=0 ) {

					results[ctr] = itemRowEncode(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.FALSE, 0, 0, "NA", retailUom);

				}else {

					results[ctr] = itemRowEncode(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.TRUE, 0, 0, "NA", retailUom);

				}
				ctr++;
			}

			System.out.print("results length = "+ results.length);

			return results;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}


	/**
	 * @ejb:interface-method
	 **/
	public String[] getInvItemAllNewAndUpdatedPosUs(String BR_BRNCH_CODE, String II_IL_LOCATION, Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean getInvItemAllNewAndUpdated");

		LocalInvItemHome invItemHome = null;
		LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);

			Collection invItems = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, 'N', 'N', 'N');
			Collection invUpdatedItems = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, 'U', 'U', 'X');

			String[] results = new String[invItems.size() + invUpdatedItems.size()];

			Iterator i = invItems.iterator();
			int ctr = 0;

			while (i.hasNext()) {

				LocalInvItem invItem = (LocalInvItem)i.next();

				String retailUom = null;
				if (invItem.getIiRetailUom() != null) {
					LocalInvUnitOfMeasure invRetailUom = invUnitOfMeasureHome.findByPrimaryKey(invItem.getIiRetailUom());
					retailUom = invRetailUom.getUomName();
				}

				Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invItem.getIiName() , AD_CMPNY);
	
				if(invBillOfMaterials.size() <=0 ) {

					results[ctr] = itemRowEncodePosUs(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.FALSE, 0, 0, "NA", retailUom);

				}else {

					results[ctr] = itemRowEncodePosUs(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.TRUE, 0, 0, "NA", retailUom);

				}
				ctr++;
			}

			i = invUpdatedItems.iterator();

			while (i.hasNext()) {

				LocalInvItem invItem = (LocalInvItem)i.next();

				String retailUom = null;
				if (invItem.getIiRetailUom() != null) {
					LocalInvUnitOfMeasure invRetailUom = invUnitOfMeasureHome.findByPrimaryKey(invItem.getIiRetailUom());
					retailUom = invRetailUom.getUomName();
				}

				Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invItem.getIiName() , AD_CMPNY);

				if(invBillOfMaterials.size() <=0 ) {

					results[ctr] = itemRowEncodePosUs(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.FALSE, 0, 0, "NA", retailUom);

				}else {

					results[ctr] = itemRowEncodePosUs(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.TRUE, 0, 0, "NA", retailUom);

				}
				ctr++;
			}

			System.out.print("results length = "+ results.length);

			return results;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}
	/**
	 * @ejb:interface-method
	 **/
	public String[] getInvItemAllNewAndUpdatedWithUnitPrice(String BR_BRNCH_CODE, String II_IL_LOCATION, Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean getInvItemAllNewAndUpdatedWithUnitPrice");

		LocalInvItemHome invItemHome = null;
		LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

		// Initialize EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);

			Collection invItems = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, 'N', 'N', 'N');
			Collection invUpdatedItems = invItemHome.findIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, 'U', 'U', 'X');

			String[] results = new String[invItems.size() + invUpdatedItems.size()];

			Iterator i = invItems.iterator();
			int ctr = 0;

			while (i.hasNext()) {

				LocalInvItem invItem = (LocalInvItem)i.next();

				String retailUom = null;
				if (invItem.getIiRetailUom() != null) {
					LocalInvUnitOfMeasure invRetailUom = invUnitOfMeasureHome.findByPrimaryKey(invItem.getIiRetailUom());
					retailUom = invRetailUom.getUomName();
				}

				Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invItem.getIiName() , AD_CMPNY);

				if(invBillOfMaterials.size() <=0 ) {

					results[ctr] = itemRowEncodeWithUnitPrice(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.FALSE, 0, 0, "NA", retailUom);

				}else {

					results[ctr] = itemRowEncodeWithUnitPrice(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.TRUE, 0, 0, "NA", retailUom);

				}
				ctr++;
			}

			i = invUpdatedItems.iterator();

			while (i.hasNext()) {

				LocalInvItem invItem = (LocalInvItem)i.next();

				String retailUom = null;
				if (invItem.getIiRetailUom() != null) {
					LocalInvUnitOfMeasure invRetailUom = invUnitOfMeasureHome.findByPrimaryKey(invItem.getIiRetailUom());
					retailUom = invRetailUom.getUomName();
				}

				Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invItem.getIiName() , AD_CMPNY);

				if(invBillOfMaterials.size() <=0 ) {

					results[ctr] = itemRowEncodeWithUnitPrice(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.FALSE, 0, 0, "NA", retailUom);

				}else {

					results[ctr] = itemRowEncodeWithUnitPrice(invItem, new Integer(0), II_IL_LOCATION, EJBCommon.TRUE, 0, 0, "NA", retailUom);

				}
				ctr++;
			}

			System.out.print("results length = "+ results.length);

			return results;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method
	 **/
	public int setInvItemAllNewAndUpdatedSuccessConfirmationAnyLoc(String BR_BRNCH_CODE, Integer AD_CMPNY){


		Debug.print("InvItemSyncControllerBean setInvItemAllNewAndUpdatedSuccessConfirmation");

		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdBranchHome adBranchHome = null;

		// Initialize EJB Home

		try {

			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome .JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);

			Collection adBranchItemLocations = adBranchItemLocationHome.findEnabledIiByIiNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'U', 'X');

			Iterator i = adBranchItemLocations.iterator();
			while (i.hasNext()) {

				LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)i.next();
				adBranchItemLocation.setBilItemDownloadStatus('D');

			}

		} catch (Exception ex) {

			ctx.setRollbackOnly();
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

		return 0;

	}
	/**
	 * @ejb:interface-method
	 **/
	public int setInvItemAllNewAndUpdatedSuccessConfirmation(String BR_BRNCH_CODE, String II_IL_LOCATION, Integer AD_CMPNY){

		Debug.print("InvItemSyncControllerBean setInvItemAllNewAndUpdatedSuccessConfirmation");

		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdBranchHome adBranchHome = null;

		// Initialize EJB Home

		try {

			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome .JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			/*
			Collection adBranchItemLocations = adBranchItemLocationHome.findEnabledIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, 'N', 'U', 'X');
                        System.out.println("BRANCH IS: " + BR_BRNCH_CODE);
                        System.out.println("ad branch item location is: " + II_IL_LOCATION);
			*/

			Collection adBranchItemLocations = adBranchItemLocationHome.findAllIiByIiNewAndUpdated(adBranch.getBrCode(), II_IL_LOCATION, AD_CMPNY, 'N', 'U', 'X');
            System.out.println("BRANCH IS: " + BR_BRNCH_CODE);
            System.out.println("ad branch item location is: " + II_IL_LOCATION);


			Iterator i = adBranchItemLocations.iterator();
			while (i.hasNext()) {

				LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)i.next();
				adBranchItemLocation.setBilItemDownloadStatus('D');
                                 System.out.println("updating into D " + adBranchItemLocation.getInvItemLocation().getIlCode());


			}

		} catch (Exception ex) {

			ctx.setRollbackOnly();
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

		return 0;
	}

	private String itemRowEncode(LocalInvItem invItem, Integer INV_LOCATION, String II_IL_LOCATION, byte isItemRaw, int quantity, int unitValue, String baseUnitUOM, String retailUom) {


		char separator = EJBCommon.SEPARATOR;
		StringBuffer encodedResult = new StringBuffer();

		// Start separator
		encodedResult.append(separator);

		// Primary Key
		encodedResult.append(invItem.getIiCode().toString());
		encodedResult.append(separator);

		// Name / OPOS: Item Code
		encodedResult.append(invItem.getIiName());
		encodedResult.append(separator);

		// Part Number
		if (invItem.getIiPartNumber() == null || invItem.getIiPartNumber().length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiPartNumber());
			encodedResult.append(separator);
		}

		// Unit of Measure / OPOS: UOM
		encodedResult.append(invItem.getInvUnitOfMeasure().getUomName());
		//		encodedResult.append(invItem.getInvUnitOfMeasure().getUomCode().toString());
		encodedResult.append(separator);

		// Description / OPOS: Item Description
		encodedResult.append(invItem.getIiDescription());
		encodedResult.append(separator);

		// Sales Price / OPOS: Unit Price
		encodedResult.append(Double.toString(invItem.getIiSalesPrice()));
		encodedResult.append(separator);

		// Location / OPOS: Location
		//		ArrayList tmpItemLocations = new ArrayList(invItem.getInvItemLocations());
		//		/* Get any of the item location (index zero) in this case */
		//		encodedResult.append(((LocalInvItemLocation)tmpItemLocations.get(0)).getInvLocation().getLocName());
		encodedResult.append(II_IL_LOCATION);
		encodedResult.append(separator);

		// Sub Location / OPOS: Location
		encodedResult.append(INV_LOCATION.toString());
		encodedResult.append(separator);


		// Category / OPOS: Product Category
		encodedResult.append(invItem.getIiAdLvCategory());
		encodedResult.append(separator);

		// Doneness
		if (invItem.getIiDoneness()== null || invItem.getIiDoneness().length() < 1) {
			encodedResult.append("NA");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiDoneness());
			encodedResult.append(separator);
		}

		// Remarks
		if (invItem.getIiRemarks() == null || invItem.getIiRemarks().length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiRemarks());
			encodedResult.append(separator);
		}

		// Sidings
		if (invItem.getIiSidings() == null || invItem.getIiSidings().length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiSidings());
			encodedResult.append(separator);
		}

		// Service Charge
		encodedResult.append(invItem.getIiServiceCharge());
		encodedResult.append(separator);

		// Non Inventoriable
		encodedResult.append(invItem.getIiNonInventoriable());
		encodedResult.append(separator);

		// isItemRaw
		if (isItemRaw==EJBCommon.TRUE){
			encodedResult.append("1");
			encodedResult.append(separator);
		}else{
			encodedResult.append("0");
			encodedResult.append(separator);
		}

		// Open Product
		encodedResult.append(invItem.getIiOpenProduct());
		encodedResult.append(separator);

		// Quantity
		encodedResult.append(Integer.toString(quantity));
		encodedResult.append(separator);

		//UnitValue
		encodedResult.append(Integer.toString(unitValue));
		encodedResult.append(separator);

		//BaseUnitUOM
		encodedResult.append(baseUnitUOM);
		encodedResult.append(separator);


		// retail UOM
		if (retailUom == null || retailUom.length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(retailUom);
			encodedResult.append(separator);
		}

		System.out.println("ITEM CODE : " + invItem.getIiName());
		// Unit Cost
		encodedResult.append(invItem.getIiUnitCost());
		encodedResult.append(separator);
		System.out.println("UNIT COST : " + invItem.getIiUnitCost());

		// Shipping Cost
		encodedResult.append(invItem.getIiShippingCost());
		encodedResult.append(separator);
		System.out.println("SHIPPING COST : " + invItem.getIiShippingCost());

		// Track Misc
		encodedResult.append(invItem.getIiTraceMisc());
		encodedResult.append(separator);
		System.out.println("TRACK MISC : " + invItem.getIiTraceMisc());

		// Enable Item
		encodedResult.append(invItem.getIiEnable());
		encodedResult.append(separator);
		System.out.println("ENABLED : " + invItem.getIiEnable());


		// INV_PRC_LVL
		encodedResult.append("INV_PRC_LVL");
		encodedResult.append(separator);




		Collection invPriceLevels = invItem.getInvPriceLevels();
		
		Collection invPriceLevelDates = invItem.getInvPriceLevelsDate();

		Iterator i = invPriceLevels.iterator();

		while (i.hasNext()) {

			LocalInvPriceLevel invPriceLevel = (LocalInvPriceLevel)i.next();
			
			String priceLevelAmount = Double.toString(invPriceLevel.getPlAmount());
			
			Date dateNow =  new Date();
					
			
			Iterator iPd = invPriceLevelDates.iterator();
			
			
			while(iPd.hasNext()){
			
				LocalInvPriceLevelDate invPriceLevelDate = (LocalInvPriceLevelDate)iPd.next();
				
				
				
				if(invPriceLevel.getPlAdLvPriceLevel().equals(invPriceLevelDate.getPdAdLvPriceLevel()) && invPriceLevelDate.getPdStatus().equals("ENABLE")){
					System.out.println("hello");
					if(dateNow.equals(invPriceLevelDate.getPdDateFrom()) || dateNow.equals(invPriceLevelDate.getPdDateTo())){
						priceLevelAmount =  Double.toString(invPriceLevelDate.getPdAmount());
						break;
					}
					
			
					if(dateNow.after(invPriceLevelDate.getPdDateFrom()) && dateNow.before(invPriceLevelDate.getPdDateTo())){
						priceLevelAmount =  Double.toString(invPriceLevelDate.getPdAmount());
						break;
					
					}
				
				}
				
				
				
				
				
			}
			
			// primary key
			encodedResult.append(invPriceLevel.getPlCode().toString());
			encodedResult.append(separator);

			// amount
			encodedResult.append(priceLevelAmount);
			encodedResult.append(separator);

			// price level
			encodedResult.append(invPriceLevel.getPlAdLvPriceLevel());
			encodedResult.append(separator);

			// item
			encodedResult.append(invPriceLevel.getInvItem().getIiCode());
			encodedResult.append(separator);

		}

		// INV_UNT_OF_MSR_CNVRSN
		encodedResult.append("INV_UNT_OF_MSR_CNVRSN");
		encodedResult.append(separator);

		Collection invUnitOfMeasureConversions = invItem.getInvUnitOfMeasureConversions();

		i = invUnitOfMeasureConversions.iterator();

		while (i.hasNext()) {

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = (LocalInvUnitOfMeasureConversion)i.next();

			// base unit
			encodedResult.append(String.valueOf(invUnitOfMeasureConversion.getUmcBaseUnit()));
			encodedResult.append(separator);

			// conversion factor
			encodedResult.append(Double.toString(invUnitOfMeasureConversion.getUmcConversionFactor()));
			encodedResult.append(separator);

			// item
			encodedResult.append(invUnitOfMeasureConversion.getInvItem().getIiCode());
			encodedResult.append(separator);

			// uom
			encodedResult.append(invUnitOfMeasureConversion.getInvUnitOfMeasure().getUomCode());
			encodedResult.append(separator);

		}

		// INV_BLL_OF_MTRL
		encodedResult.append("INV_BLL_OF_MTRL");
		encodedResult.append(separator);

		Collection invBillOfMaterials = invItem.getInvBillOfMaterials();

		i = invBillOfMaterials.iterator();

		while (i.hasNext()) {

			LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)i.next();

			// primary key
			encodedResult.append(invBillOfMaterial.getBomCode());
			encodedResult.append(separator);

			// raw item
			encodedResult.append(invBillOfMaterial.getBomIiName());
			encodedResult.append(separator);

			// qty
			encodedResult.append(invBillOfMaterial.getBomQuantityNeeded());
			encodedResult.append(separator);

			// item
			encodedResult.append(invBillOfMaterial.getInvItem().getIiCode());
			encodedResult.append(separator);

		}

		// item end
		encodedResult.append("ITEM_END");
		encodedResult.append(separator);

		// End separator
		encodedResult.append(separator);

		return encodedResult.toString();


	}
	// End separator
	
	
	
	
	
	
	
	
	
	
	

	private String itemRowEncodePosUs(LocalInvItem invItem, Integer INV_LOCATION, String II_IL_LOCATION, byte isItemRaw, int quantity, int unitValue, String baseUnitUOM, String retailUom) {


		char separator = EJBCommon.SEPARATOR;
		StringBuffer encodedResult = new StringBuffer();

		// Start separator
		encodedResult.append(separator);

		// Primary Key
		encodedResult.append(invItem.getIiCode().toString());
		encodedResult.append(separator);

		// Name / OPOS: Item Code
		encodedResult.append(invItem.getIiName());
		encodedResult.append(separator);

		// Part Number
		if (invItem.getIiPartNumber() == null || invItem.getIiPartNumber().length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiPartNumber());
			encodedResult.append(separator);
		}

		// Unit of Measure / OPOS: UOM
		encodedResult.append(invItem.getInvUnitOfMeasure().getUomName());
		//		encodedResult.append(invItem.getInvUnitOfMeasure().getUomCode().toString());
		encodedResult.append(separator);

		// Description / OPOS: Item Description
		encodedResult.append(invItem.getIiDescription());
		encodedResult.append(separator);

		// Sales Price / OPOS: Unit Price
		encodedResult.append(Double.toString(invItem.getIiSalesPrice()));
		encodedResult.append(separator);

		// Location / OPOS: Location
		//		ArrayList tmpItemLocations = new ArrayList(invItem.getInvItemLocations());
		//		/* Get any of the item location (index zero) in this case */
		//		encodedResult.append(((LocalInvItemLocation)tmpItemLocations.get(0)).getInvLocation().getLocName());
		encodedResult.append(II_IL_LOCATION);
		encodedResult.append(separator);

		// Sub Location / OPOS: Location
		encodedResult.append(INV_LOCATION.toString());
		encodedResult.append(separator);


		// Category / OPOS: Product Category
		encodedResult.append(invItem.getIiAdLvCategory());
		encodedResult.append(separator);

		// Doneness
		if (invItem.getIiDoneness()== null || invItem.getIiDoneness().length() < 1) {
			encodedResult.append("NA");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiDoneness());
			encodedResult.append(separator);
		}

		// Remarks
		if (invItem.getIiRemarks() == null || invItem.getIiRemarks().length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiRemarks());
			encodedResult.append(separator);
		}

		// Sidings
		if (invItem.getIiSidings() == null || invItem.getIiSidings().length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiSidings());
			encodedResult.append(separator);
		}

		// Service Charge
		encodedResult.append(invItem.getIiServiceCharge());
		encodedResult.append(separator);

		// Non Inventoriable
		encodedResult.append(invItem.getIiNonInventoriable());
		encodedResult.append(separator);

		// isItemRaw
		if (isItemRaw==EJBCommon.TRUE){
			encodedResult.append("1");
			encodedResult.append(separator);
		}else{
			encodedResult.append("0");
			encodedResult.append(separator);
		}

		// Open Product
		encodedResult.append(invItem.getIiOpenProduct());
		encodedResult.append(separator);

		// Tax Code
		encodedResult.append(invItem.getIiTaxCode());
		encodedResult.append(separator);

		// Quantity
		encodedResult.append(Integer.toString(quantity));
		encodedResult.append(separator);

		//UnitValue
		encodedResult.append(Integer.toString(unitValue));
		encodedResult.append(separator);

		//BaseUnitUOM
		encodedResult.append(baseUnitUOM);
		encodedResult.append(separator);

		// retail UOM
		if (retailUom == null || retailUom.length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(retailUom);
			encodedResult.append(separator);
		}

		// INV_PRC_LVL
		encodedResult.append("INV_PRC_LVL");
		encodedResult.append(separator);

		Collection invPriceLevels = invItem.getInvPriceLevels();

		Iterator i = invPriceLevels.iterator();

		while (i.hasNext()) {

			LocalInvPriceLevel invPriceLevel = (LocalInvPriceLevel)i.next();

			// primary key
			encodedResult.append(invPriceLevel.getPlCode().toString());
			encodedResult.append(separator);

			// amount
			encodedResult.append(Double.toString(invPriceLevel.getPlAmount()));
			encodedResult.append(separator);

			// price level
			encodedResult.append(invPriceLevel.getPlAdLvPriceLevel());
			encodedResult.append(separator);

			// item
			encodedResult.append(invPriceLevel.getInvItem().getIiCode());
			encodedResult.append(separator);

		}

		// INV_UNT_OF_MSR_CNVRSN
		encodedResult.append("INV_UNT_OF_MSR_CNVRSN");
		encodedResult.append(separator);

		Collection invUnitOfMeasureConversions = invItem.getInvUnitOfMeasureConversions();

		i = invUnitOfMeasureConversions.iterator();

		while (i.hasNext()) {

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = (LocalInvUnitOfMeasureConversion)i.next();

			// base unit
			encodedResult.append(String.valueOf(invUnitOfMeasureConversion.getUmcBaseUnit()));
			encodedResult.append(separator);

			// conversion factor
			encodedResult.append(Double.toString(invUnitOfMeasureConversion.getUmcConversionFactor()));
			encodedResult.append(separator);

			// item
			encodedResult.append(invUnitOfMeasureConversion.getInvItem().getIiCode());
			encodedResult.append(separator);

			// uom
			encodedResult.append(invUnitOfMeasureConversion.getInvUnitOfMeasure().getUomCode());
			encodedResult.append(separator);

		}

		// INV_BLL_OF_MTRL
		encodedResult.append("INV_BLL_OF_MTRL");
		encodedResult.append(separator);

		Collection invBillOfMaterials = invItem.getInvBillOfMaterials();

		i = invBillOfMaterials.iterator();

		while (i.hasNext()) {

			LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)i.next();

			// primary key
			encodedResult.append(invBillOfMaterial.getBomCode());
			encodedResult.append(separator);

			// raw item
			encodedResult.append(invBillOfMaterial.getBomIiName());
			encodedResult.append(separator);

			// qty
			encodedResult.append(invBillOfMaterial.getBomQuantityNeeded());
			encodedResult.append(separator);

			// item
			encodedResult.append(invBillOfMaterial.getInvItem().getIiCode());
			encodedResult.append(separator);

		}

		// item end
		encodedResult.append("ITEM_END");
		encodedResult.append(separator);

		// End separator
		encodedResult.append(separator);

		return encodedResult.toString();


	}


	private String itemRowEncodeWithUnitPrice(LocalInvItem invItem, Integer INV_LOCATION, String II_IL_LOCATION, byte isItemRaw, int quantity, int unitValue, String baseUnitUOM, String retailUom) {


		char separator = EJBCommon.SEPARATOR;
		StringBuffer encodedResult = new StringBuffer();

		// Start separator
		encodedResult.append(separator);

		// Primary Key
		encodedResult.append(invItem.getIiCode().toString());
		encodedResult.append(separator);

		// Name / OPOS: Item Code
		encodedResult.append(invItem.getIiName());
		encodedResult.append(separator);

		// Part Number
		if (invItem.getIiPartNumber() == null || invItem.getIiPartNumber().length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiPartNumber());
			encodedResult.append(separator);
		}

		// Unit of Measure / OPOS: UOM
		encodedResult.append(invItem.getInvUnitOfMeasure().getUomName());
		//		encodedResult.append(invItem.getInvUnitOfMeasure().getUomCode().toString());
		encodedResult.append(separator);

		// Description / OPOS: Item Description
		encodedResult.append(invItem.getIiDescription());
		encodedResult.append(separator);

		// Sales Price / OPOS: Unit Price
		encodedResult.append(Double.toString(invItem.getIiSalesPrice()));
		encodedResult.append(separator);

		// Location / OPOS: Location
		//		ArrayList tmpItemLocations = new ArrayList(invItem.getInvItemLocations());
		//		/* Get any of the item location (index zero) in this case */
		//		encodedResult.append(((LocalInvItemLocation)tmpItemLocations.get(0)).getInvLocation().getLocName());
		encodedResult.append(II_IL_LOCATION);
		encodedResult.append(separator);

		// Sub Location / OPOS: Location
		encodedResult.append(INV_LOCATION.toString());
		encodedResult.append(separator);


		// Category / OPOS: Product Category
		encodedResult.append(invItem.getIiAdLvCategory());
		encodedResult.append(separator);

		// Doneness
		if (invItem.getIiDoneness()== null || invItem.getIiDoneness().length() < 1) {
			encodedResult.append("NA");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiDoneness());
			encodedResult.append(separator);
		}

		// Remarks
		if (invItem.getIiRemarks() == null || invItem.getIiRemarks().length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiRemarks());
			encodedResult.append(separator);
		}

		// Sidings
		if (invItem.getIiSidings() == null || invItem.getIiSidings().length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(invItem.getIiSidings());
			encodedResult.append(separator);
		}

		// Service Charge
		encodedResult.append(invItem.getIiServiceCharge());
		encodedResult.append(separator);

		// Non Inventoriable
		encodedResult.append(invItem.getIiNonInventoriable());
		encodedResult.append(separator);

		// isItemRaw
		if (isItemRaw==EJBCommon.TRUE){
			encodedResult.append("1");
			encodedResult.append(separator);
		}else{
			encodedResult.append("0");
			encodedResult.append(separator);
		}

		// Open Product
		encodedResult.append(invItem.getIiOpenProduct());
		encodedResult.append(separator);

		// Quantity
		encodedResult.append(Integer.toString(quantity));
		encodedResult.append(separator);

		//UnitValue
		encodedResult.append(Integer.toString(unitValue));
		encodedResult.append(separator);

		//BaseUnitUOM
		encodedResult.append(baseUnitUOM);
		encodedResult.append(separator);

		// retail UOM
		if (retailUom == null || retailUom.length() < 1) {
			encodedResult.append("none");
			encodedResult.append(separator);
		} else {
			encodedResult.append(retailUom);
			encodedResult.append(separator);
		}

		// INV_PRC_LVL
		encodedResult.append("INV_PRC_LVL");
		encodedResult.append(separator);

		Collection invPriceLevels = invItem.getInvPriceLevels();

		Iterator i = invPriceLevels.iterator();

		while (i.hasNext()) {

			LocalInvPriceLevel invPriceLevel = (LocalInvPriceLevel)i.next();

			// primary key
			encodedResult.append(invPriceLevel.getPlCode().toString());
			encodedResult.append(separator);

			// amount
			encodedResult.append(Double.toString(invPriceLevel.getPlAmount()));
			encodedResult.append(separator);

			// price level
			encodedResult.append(invPriceLevel.getPlAdLvPriceLevel());
			encodedResult.append(separator);

			// item
			encodedResult.append(invPriceLevel.getInvItem().getIiCode());

		}

		// item end
		encodedResult.append("ITEM_END");
		encodedResult.append(separator);

		// End separator
		encodedResult.append(separator);

		return encodedResult.toString();


	}

	/*
	 *  Offline Inventory Transactions
	 */

	/**
	 * @ejb:interface-method
	 * */
	public String[] getAllMemolineInvoice(String BR_BRNCH_CODE, Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean getAllMemolineInvoice");

		LocalArInvoiceHome arInvoiceHome = null;
		LocalAdBranchHome adBranchHome = null;

		//initialized EJB Home

		try {
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try{

			Integer AD_BRNCH = new Integer(0);
			String dateFrom = "";
			String dateTo = "";

			//Parse Date
			System.out.println("BR_BRNCH_CODE: " +BR_BRNCH_CODE);
			dateFrom = BR_BRNCH_CODE.substring(0,BR_BRNCH_CODE.indexOf("$"));
			BR_BRNCH_CODE = BR_BRNCH_CODE.substring(dateFrom.length() + 1);
			dateTo = BR_BRNCH_CODE.substring(0,BR_BRNCH_CODE.indexOf("$"));
			BR_BRNCH_CODE = BR_BRNCH_CODE.substring(dateTo.length() + 1);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setLenient(false);

			Object obj[] = new Object[2];
			obj[0] = sdf.parse(dateFrom);
			obj[1] = sdf.parse(dateTo);

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();

			String ilSql =  "SELECT OBJECT(inv) FROM ArInvoice inv, IN(inv.arInvoiceLines) il  WHERE inv.invPosted = 1 AND inv.invVoid = 0 AND inv.invCreditMemo=0 AND inv.invDate>=?1 AND inv.invDate<=?2 AND inv.invAdBranch = " + AD_BRNCH + " AND inv.invAdCompany = " + AD_CMPNY ;
			Collection arItemInvoices = arInvoiceHome.getInvByCriteria(ilSql, obj);

			String[] results = new String[arItemInvoices.size()];

			int ctr=0;
			String currInvNumber = "";

			Iterator IiIter = arItemInvoices.iterator();

			while(IiIter.hasNext()){
				LocalArInvoice arInvoice = (LocalArInvoice)IiIter.next();

				if(currInvNumber.equals(arInvoice.getInvNumber()))
					continue;

				currInvNumber = arInvoice.getInvNumber();

				results[ctr] = mmoInvRowEncode(arInvoice);
				ctr++;
			}

			Debug.print("Sending : " + (ctr) + " Invoice(s).....");

			return results;

		}catch(Exception ex){
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
		}

	}

	/**
	 * @ejb:interface-method
	 * */
	public String[] getAllConsolidatedItems(String[] ItemStr, Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean getAllConsolidatedItems");

		LocalInvItemHome invItemHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalAdLookUpValueHome adLookUpValueHome = null;
		LocalInvPriceLevelHome invPriceLevelHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdBranchHome adBranchHome = null;

		//initialized EJB Home

		try {

			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try{

			Integer AD_BRNCH = new Integer(0);
			String dateFrom = "";
			String dateTo = "";

			String[] results = new String[ItemStr.length];

			for (int i = 0; i < ItemStr.length; i++) {

				InvModItemDetails  details = decodeConsolidatedItem(ItemStr[i]);

				try{

					LocalInvItem invItem = null;
					// Try if Item Code is Already Present
					try {

						invItem = invItemHome.findByIiName(details.getIiName(), AD_CMPNY);
						System.out.println("DUPLICATE CODE FOR: " + details.getIiName());
						ctx.setRollbackOnly();
						throw new GlobalRecordAlreadyExistException();
					} catch (FinderException ex) {

					}

					LocalInvLocation invLocation = invLocationHome.findByLocName(details.getIiDefaultLocationName(), AD_CMPNY);

					if(invItem==null){

						LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(details.getIiUomName(), AD_CMPNY);


						// Create Item
						invItem = invItemHome.create(details.getIiName(), details.getIiDescription(),
								details.getIiPartNumber(), details.getIiShortName(), details.getIiBarCode1(), details.getIiBarCode2(), details.getIiBarCode3(), details.getIiBrand(), details.getIiClass(), details.getIiAdLvCategory(),
								details.getIiCostMethod(), details.getIiUnitCost(), details.getIiSalesPrice(),
								details.getIiEnable(), details.getIiVirtualStore(), details.getIiEnableAutoBuild(), details.getIiDoneness(),
								details.getIiSidings(), details.getIiRemarks(), details.getIiServiceCharge(),
								details.getIiNonInventoriable(), details.getIiServices(), details.getIiJobServices(), details.getIiIsVatRelief(), details.getIiIsTax(), details.getIiIsProject(),
								details.getIiPercentMarkup(), details.getIiShippingCost(), details.getIiSpecificGravity(), details.getIiStandardFillSize(), details.getIiYield(),

								 0d, 0d, 0d, 0d, 0d,
									null,null, null, null, null,
								details.getIiLossPercentage(), details.getIiMarkupValue(),
								details.getIiMarket(), details.getIiEnablePo(), details.getIiPoCycle(),
								details.getIiUmcPackaging(), invUnitOfMeasure.getUomCode(), details.getIiOpenProduct(),
								details.getIiFixedAsset(), details.getIiDateAcquired(), invLocation.getLocCode(),  details.getIiTaxCode(),
								details.getIiScSunday(), details.getIiScMonday(), details.getIiScTuesday(), details.getIiScWednesday(), details.getIiScThursday(), details.getIiScFriday(), details.getIiScSaturday(),
								AD_CMPNY);

						//Add UOM and UMC
						invItem.setInvUnitOfMeasure(invUnitOfMeasure);
						this.addInvUmcEntry(invItem, invUnitOfMeasure.getUomName(), invUnitOfMeasure.getUomAdLvClass(), invUnitOfMeasure.getUomConversionFactor(), invUnitOfMeasure.getUomBaseUnit(), AD_CMPNY);

						/*
						 *  Set Item Location and Branch Item Location
						 *  Get Inventory COA's
						 *  Special for CWC Only
						 *
						 */

						LocalInvItem prevItem = (LocalInvItem)invItemHome.findEnabledIiByIiAdLvCategory(invItem.getIiAdLvCategory(), AD_CMPNY).toArray()[0];
						LocalInvItemLocation prevIl = (LocalInvItemLocation)prevItem.getInvItemLocations().toArray()[0];

						LocalInvItemLocation invItemLocation = invItemLocationHome.create(null, null, prevIl.getIlReorderPoint(), prevIl.getIlReorderQuantity(),
								prevIl.getIlReorderLevel(), 0d, prevIl.getIlGlCoaSalesAccount(), prevIl.getIlGlCoaInventoryAccount(),
								prevIl.getIlGlCoaCostOfSalesAccount(), prevIl.getIlGlCoaWipAccount(), prevIl.getIlGlCoaAccruedInventoryAccount(), prevIl.getIlGlCoaSalesReturnAccount(),
								prevIl.getIlSubjectToCommission(), AD_CMPNY);

						invItemLocation.setInvItem(invItem);
						invItemLocation.setInvLocation(invLocation);

						Iterator brIter = adBranchHome.findBrAll(AD_CMPNY).iterator();

						while(brIter.hasNext()){

							LocalAdBranch adBranch = (LocalAdBranch)brIter.next();

							LocalAdBranchItemLocation adBranchItemLocation = adBranchItemLocationHome.create(null,null, prevIl.getIlReorderPoint(), prevIl.getIlReorderQuantity(),
									prevIl.getIlGlCoaSalesAccount(), prevIl.getIlGlCoaInventoryAccount(), prevIl.getIlGlCoaCostOfSalesAccount(), prevIl.getIlGlCoaWipAccount(),
									prevIl.getIlGlCoaAccruedInventoryAccount(),prevIl.getIlGlCoaSalesReturnAccount() , (byte)0, 0d, 0d, 0d, 0d, 0d, 0d, 'D','D','D', AD_CMPNY);

							adBranchItemLocation.setInvItemLocation(invItemLocation);
							adBranchItemLocation.setAdBranch(adBranch);

						}
					}

					results[i] = itemRowEncode(invItem, new Integer(0), invLocation.getLocLvType(), EJBCommon.FALSE, 0, 0, "NA", invItem.getInvUnitOfMeasure().getUomName());

				}catch(Exception e) {
					e.printStackTrace();
					ctx.setRollbackOnly();
					return null;
				}
			}

			return results;

		}catch(Exception ex){
			ex.printStackTrace();
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());
		}

	}

	private InvModItemDetails decodeConsolidatedItem(String itemStr){


		Debug.print("InvItemSyncControllerBean decodeConsolidatedItem");

		String separator = "$";
		InvModItemDetails details = new InvModItemDetails();

		// Remove first $ character
		itemStr = itemStr.substring(1);

		// II_NM
		int start = 0;
		int nextIndex = itemStr.indexOf(separator, start);
		int length = nextIndex - start;
		details.setIiName(itemStr.substring(start, start + length));

		// II_PRT_NMBR
		start = nextIndex + 1;
		nextIndex = itemStr.indexOf(separator, start);
		length = nextIndex - start;
		details.setIiPartNumber(itemStr.substring(start, start + length));

		// II_DESC
		start = nextIndex + 1;
		nextIndex = itemStr.indexOf(separator, start);
		length = nextIndex - start;
		details.setIiDescription(itemStr.substring(start, start + length));

		// II_UOM
		start = nextIndex + 1;
		nextIndex = itemStr.indexOf(separator, start);
		length = nextIndex - start;
		details.setIiUomName(itemStr.substring(start, start + length));


		// II_AD_LVL_CTGRY
		start = nextIndex + 1;
		nextIndex = itemStr.indexOf(separator, start);
		length = nextIndex - start;
		details.setIiAdLvCategory(itemStr.substring(start, start + length));

		// II_DFLT_LCTN
		start = nextIndex + 1;
		nextIndex = itemStr.indexOf(separator, start);
		length = nextIndex - start;
		details.setIiDefaultLocationName(itemStr.substring(start, start + length));

		// II_RMRKS
		start = nextIndex + 1;
		nextIndex = itemStr.indexOf(separator, start);
		length = nextIndex - start;
		details.setIiRemarks(itemStr.substring(start, start + length));

		details.setIiClass("Stock");
		details.setIiCostMethod("FIFO");
		details.setIiUnitCost(0);
		details.setIiSalesPrice(0);
		details.setIiEnable((byte)1);
		details.setIiEnableAutoBuild((byte)0);
		details.setIiDoneness(null);
		details.setIiSidings(null);
		details.setIiServiceCharge((byte)0);
		details.setIiNonInventoriable((byte)0);
		details.setIiPercentMarkup(0);
		details.setIiShippingCost(0);
		details.setIiMarkupValue(0);
		details.setIiMarket(null);
		details.setIiEnablePo((byte)0);
		details.setIiPoCycle((byte)0);
		details.setIiUmcPackaging(null);
		details.setIiOpenProduct((byte)0);
		details.setIiFixedAsset((byte)0);
		details.setIiDateAcquired(null);

		return details;


	}

	private void addInvUmcEntry(LocalInvItem invItem,  String uomName, String uomAdLvClass, double conversionFactor, byte umcBaseUnit, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("InvUnitOfMeasureConversionControllerBean addInvUmcEntry");

		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemHome invItemHome = null;

		// initialize EJB

		try{

			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex){

			throw new EJBException(ex.getMessage());

		}

		try {

			// create umc
			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.create(conversionFactor, umcBaseUnit, AD_CMPNY);

			try {

				// map uom
				LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomNameAndUomAdLvClass(uomName, uomAdLvClass, AD_CMPNY);
				invUnitOfMeasureConversion.setInvUnitOfMeasure(invUnitOfMeasure);

				//invUnitOfMeasure.addInvUnitOfMeasureConversion(invUnitOfMeasureConversion);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			// map item
			invUnitOfMeasureConversion.setInvItem(invItem);
			//invItem.addInvUnitOfMeasureConversion(invUnitOfMeasureConversion);

		} catch (GlobalNoRecordFoundException ex) {
			ex.printStackTrace();
			ctx.setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	/**
	 * @ejb:interface-method
	 * */
	public String[] getAllPostedArSoMatchedInvoice(String BR_BRNCH_CODE, Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean getAllPostedArSoMatchedInvoice");

		LocalArInvoiceHome arInvoiceHome = null;
		LocalAdBranchHome adBranchHome = null;

		//initialized EJB Home

		try {
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try{

			Integer AD_BRNCH = new Integer(0);
			String dateFrom = "";
			String dateTo = "";

			//Parse Date
			System.out.println("BR_BRNCH_CODE: " +BR_BRNCH_CODE);
			dateFrom = BR_BRNCH_CODE.substring(0,BR_BRNCH_CODE.indexOf("$"));
			BR_BRNCH_CODE = BR_BRNCH_CODE.substring(dateFrom.length() + 1);
			dateTo = BR_BRNCH_CODE.substring(0,BR_BRNCH_CODE.indexOf("$"));
			BR_BRNCH_CODE = BR_BRNCH_CODE.substring(dateTo.length() + 1);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.setLenient(false);

			Object obj[] = new Object[2];
			obj[0] = sdf.parse(dateFrom);
			obj[1] = sdf.parse(dateTo);

			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();

			String iliSql =  "SELECT OBJECT(inv) FROM ArInvoice inv, IN(inv.arInvoiceLineItems) ili  WHERE inv.invPosted = 1 AND inv.invVoid = 0 AND inv.invCreditMemo=0 AND inv.invDate>=?1 AND inv.invDate<=?2 AND inv.invAdBranch = " + AD_BRNCH + " AND inv.invAdCompany = " + AD_CMPNY ;
			Collection arItemInvoices = arInvoiceHome.getInvByCriteria(iliSql, obj);

			String solSql =  "SELECT OBJECT(inv) FROM ArInvoice inv, IN(inv.arSalesOrderInvoiceLines) sil  WHERE inv.invPosted = 1 AND inv.invVoid = 0 AND inv.invCreditMemo=0 AND inv.invDate>=?1 AND inv.invDate<=?2 AND inv.invAdBranch = " + AD_BRNCH + " AND inv.invAdCompany = " + AD_CMPNY ;

			Collection arSolInvoices = arInvoiceHome.getInvByCriteria(solSql, obj);

			Debug.print("List Size: " + (arItemInvoices.size() + arSolInvoices.size()));

			String[] results = new String[(arItemInvoices.size() + arSolInvoices.size())];

			int ctr=0;
			String currInvNumber = "";

			Iterator IiIter = arItemInvoices.iterator();

			while(IiIter.hasNext()){
				LocalArInvoice arInvoice = (LocalArInvoice)IiIter.next();

				if(currInvNumber.equals(arInvoice.getInvNumber()))
					continue;

				currInvNumber = arInvoice.getInvNumber();

				results[ctr] = invRowEncode(arInvoice);
				ctr++;
			}

			Iterator SoIter = arSolInvoices.iterator();

			while(SoIter.hasNext()){
				LocalArInvoice arInvoice = (LocalArInvoice)SoIter.next();

				if(currInvNumber.equals(arInvoice.getInvNumber()))
					continue;

				currInvNumber = arInvoice.getInvNumber();

				results[ctr] = invRowEncode(arInvoice);
				ctr++;
			}

			Debug.print("Sending : " + (ctr) + " Invoice(s).....");

			return results;

		}catch(Exception ex){
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
		}

	}
	private String mmoInvRowEncode(LocalArInvoice arInvoice)
	{
		//Debug.print("InvItemSyncControllerBean soRowEncode");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer encodedResult = new StringBuffer();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		// Start separator
		encodedResult.append(separator);

		// INV Code
		encodedResult.append(arInvoice.getInvCode());
		encodedResult.append(separator);

		// Description
		encodedResult.append(arInvoice.getInvDescription());
		encodedResult.append(separator);

		// INV Date
		encodedResult.append(sdf.format(arInvoice.getInvDate()));
		encodedResult.append(separator);

		// INV Number
		encodedResult.append(arInvoice.getInvNumber());
		encodedResult.append(separator);

		// Reference Number
		encodedResult.append(arInvoice.getInvReferenceNumber());
		encodedResult.append(separator);

		// Amount Due
		encodedResult.append(arInvoice.getInvAmountDue());
		encodedResult.append(separator);

		// Amount Paid
		encodedResult.append(arInvoice.getInvAmountPaid());
		encodedResult.append(separator);

		// Conversion Date
		encodedResult.append(arInvoice.getInvConversionDate());
		encodedResult.append(separator);

		// Conversion Rate
		encodedResult.append(arInvoice.getInvConversionRate());
		encodedResult.append(separator);

		//INV_BLL_TO_ADDRSS
		encodedResult.append(arInvoice.getInvBillToAddress());
		encodedResult.append(separator);

		//INV_BLL_TO_CNTCT
		encodedResult.append(arInvoice.getInvBillToContact());
		encodedResult.append(separator);

		//INV_BLL_TO_ALT_CNTCT
		encodedResult.append(arInvoice.getInvBillToAltContact());
		encodedResult.append(separator);

		//INV_BLL_TO_PHN
		encodedResult.append(arInvoice.getInvBillToPhone());
		encodedResult.append(separator);

		//INV_BLLNG_HDR
		encodedResult.append(arInvoice.getInvBillingHeader());
		encodedResult.append(separator);

		//INV_BLLNG_FTR
		encodedResult.append(arInvoice.getInvBillingFooter());
		encodedResult.append(separator);

		//INV_BLLNG_HDR2
		encodedResult.append(arInvoice.getInvBillingHeader2());
		encodedResult.append(separator);

		//INV_BLLNG_FTR2
		encodedResult.append(arInvoice.getInvBillingFooter2());
		encodedResult.append(separator);

		//INV_BLLNG_HDR3
		encodedResult.append(arInvoice.getInvBillingHeader3());
		encodedResult.append(separator);

		//INV_BLLNG_FTR3
		encodedResult.append(arInvoice.getInvBillingFooter3());
		encodedResult.append(separator);

		//INV_BLLNG_SGNTRY
		encodedResult.append(arInvoice.getInvBillingSignatory());
		encodedResult.append(separator);

		//INV_SGNTRY_TTL
		encodedResult.append(arInvoice.getInvSignatoryTitle());
		encodedResult.append(separator);

		//INV_SHP_TO_ADDRSS
		encodedResult.append(arInvoice.getInvShipToAddress());
		encodedResult.append(separator);

		//INV_SHP_TO_CNTCT
		encodedResult.append(arInvoice.getInvShipToContact());
		encodedResult.append(separator);

		//INV_SHP_TO_ALT_CNTCT
		encodedResult.append(arInvoice.getInvShipToAltContact());
		encodedResult.append(separator);

		//INV_SHP_TO_PHN
		encodedResult.append(arInvoice.getInvShipToPhone());
		encodedResult.append(separator);

		//INV_SHP_DT
		encodedResult.append(arInvoice.getInvShipDate());
		encodedResult.append(separator);

		//INV_LV_FRGHT
		encodedResult.append(arInvoice.getInvLvFreight());
		encodedResult.append(separator);

		//INV_LV_SHFT
		encodedResult.append(arInvoice.getInvLvShift());
		encodedResult.append(separator);

		//INV_SO_NMBR
		encodedResult.append(arInvoice.getInvSoNumber());
		encodedResult.append(separator);

		//INV_CLNT_PO
		encodedResult.append(arInvoice.getInvClientPO());
		encodedResult.append(separator);

		//INV_EFFCTVTY_DT
		encodedResult.append(arInvoice.getInvEffectivityDate());
		encodedResult.append(separator);

		//GL_FUNCTIONAL_CURRENCY
		encodedResult.append(arInvoice.getGlFunctionalCurrency().getFcName());
		encodedResult.append(separator);

		//AR_TAX_CODE
		encodedResult.append(arInvoice.getArTaxCode().getTcName());
		encodedResult.append(separator);

		//AR_WITHHOLDING_TAX_CODE
		encodedResult.append(arInvoice.getArWithholdingTaxCode().getWtcName());
		encodedResult.append(separator);

		//AR_CUSTOMER
		encodedResult.append(arInvoice.getArCustomer().getCstCustomerCode());
		encodedResult.append(separator);

		//AD_PAYMENT_TERM
		encodedResult.append(arInvoice.getAdPaymentTerm().getPytName());
		encodedResult.append(separator);

		// end separator
		encodedResult.append(separator);

		String lineSeparator = "~";

		// begin lineSeparator
		encodedResult.append(lineSeparator);

		// Append Invoice Lines
		Iterator solIter = arInvoice.getArInvoiceLines().iterator();

		while (solIter.hasNext()) {

			LocalArInvoiceLine arInvoiceLine = (LocalArInvoiceLine)solIter.next();

			// begin separator
			encodedResult.append(separator);

			// IL_CODE
			encodedResult.append(arInvoiceLine.getIlCode());
			encodedResult.append(separator);

			// IL_DESC
			encodedResult.append(arInvoiceLine.getIlDescription());
			encodedResult.append(separator);

			// IL_QNTTY
			encodedResult.append(arInvoiceLine.getIlQuantity());
			encodedResult.append(separator);

			// IL_UNT_PRC
			encodedResult.append(arInvoiceLine.getIlUnitPrice());
			encodedResult.append(separator);

			// IL_AMNT
			encodedResult.append(arInvoiceLine.getIlAmount());
			encodedResult.append(separator);

			// IL_TX_AMNT
			encodedResult.append(arInvoiceLine.getIlTaxAmount());
			encodedResult.append(separator);

			// IL_TX
			encodedResult.append(arInvoiceLine.getIlTax());
			encodedResult.append(separator);

			// AR_STANDARD_MEMO_LINE
			encodedResult.append(arInvoiceLine.getArStandardMemoLine().getSmlCode());
			encodedResult.append(separator);

			// begin lineSeparator
			encodedResult.append(lineSeparator);
		}

		// Append Distribution Records
		encodedResult.append("$JOURNAL$");

		Iterator drIter = arInvoice.getArDistributionRecords().iterator();

		while (drIter.hasNext()) {

			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drIter.next();

			// begin separator
			encodedResult.append(separator);

			// Dr Code
			encodedResult.append(arDistributionRecord.getDrCode());
			encodedResult.append(separator);

			// Dr Line
			encodedResult.append(arDistributionRecord.getDrLine());
			encodedResult.append(separator);

			// Dr Class
			encodedResult.append(arDistributionRecord.getDrClass());
			encodedResult.append(separator);

			// Dr Debit
			encodedResult.append(arDistributionRecord.getDrDebit());
			encodedResult.append(separator);

			// Dr Amount
			encodedResult.append(arDistributionRecord.getDrAmount());
			encodedResult.append(separator);

			// Dr COA
			encodedResult.append(arDistributionRecord.getGlChartOfAccount().getCoaCode());
			encodedResult.append(separator);

			// Dr Invoice
			encodedResult.append(arInvoice.getInvCode());
			encodedResult.append(separator);

			// begin lineSeparator
			encodedResult.append(lineSeparator);
		}

		return encodedResult.toString();


	}

	private String invRowEncode(LocalArInvoice arInvoice)
	{

		//Debug.print("InvItemSyncControllerBean soRowEncode");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer encodedResult = new StringBuffer();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		// Start separator
		encodedResult.append(separator);

		// INV Code
		encodedResult.append(arInvoice.getInvCode());
		encodedResult.append(separator);

		// Description
		encodedResult.append(arInvoice.getInvDescription());
		encodedResult.append(separator);

		// INV Date
		encodedResult.append(sdf.format(arInvoice.getInvDate()));
		encodedResult.append(separator);

		// INV Number
		encodedResult.append(arInvoice.getInvNumber());
		encodedResult.append(separator);

		// Reference Number
		encodedResult.append(arInvoice.getInvReferenceNumber());
		encodedResult.append(separator);

		// Amount Due
		encodedResult.append(arInvoice.getInvAmountDue());
		encodedResult.append(separator);

		// Amount Paid
		encodedResult.append(arInvoice.getInvAmountPaid());
		encodedResult.append(separator);

		// Conversion Date
		encodedResult.append(arInvoice.getInvConversionDate());
		encodedResult.append(separator);

		// Conversion Rate
		encodedResult.append(arInvoice.getInvConversionRate());
		encodedResult.append(separator);

		//INV_BLL_TO_ADDRSS
		encodedResult.append(arInvoice.getInvBillToAddress());
		encodedResult.append(separator);

		//INV_BLL_TO_CNTCT
		encodedResult.append(arInvoice.getInvBillToContact());
		encodedResult.append(separator);

		//INV_BLL_TO_ALT_CNTCT
		encodedResult.append(arInvoice.getInvBillToAltContact());
		encodedResult.append(separator);

		//INV_BLL_TO_PHN
		encodedResult.append(arInvoice.getInvBillToPhone());
		encodedResult.append(separator);

		//INV_BLLNG_HDR
		encodedResult.append(arInvoice.getInvBillingHeader());
		encodedResult.append(separator);

		//INV_BLLNG_FTR
		encodedResult.append(arInvoice.getInvBillingFooter());
		encodedResult.append(separator);

		//INV_BLLNG_HDR2
		encodedResult.append(arInvoice.getInvBillingHeader2());
		encodedResult.append(separator);

		//INV_BLLNG_FTR2
		encodedResult.append(arInvoice.getInvBillingFooter2());
		encodedResult.append(separator);

		//INV_BLLNG_HDR3
		encodedResult.append(arInvoice.getInvBillingHeader3());
		encodedResult.append(separator);

		//INV_BLLNG_FTR3
		encodedResult.append(arInvoice.getInvBillingFooter3());
		encodedResult.append(separator);

		//INV_BLLNG_SGNTRY
		encodedResult.append(arInvoice.getInvBillingSignatory());
		encodedResult.append(separator);

		//INV_SGNTRY_TTL
		encodedResult.append(arInvoice.getInvSignatoryTitle());
		encodedResult.append(separator);

		//INV_SHP_TO_ADDRSS
		encodedResult.append(arInvoice.getInvShipToAddress());
		encodedResult.append(separator);

		//INV_SHP_TO_CNTCT
		encodedResult.append(arInvoice.getInvShipToContact());
		encodedResult.append(separator);

		//INV_SHP_TO_ALT_CNTCT
		encodedResult.append(arInvoice.getInvShipToAltContact());
		encodedResult.append(separator);

		//INV_SHP_TO_PHN
		encodedResult.append(arInvoice.getInvShipToPhone());
		encodedResult.append(separator);

		//INV_SHP_DT
		encodedResult.append(arInvoice.getInvShipDate());
		encodedResult.append(separator);

		//INV_LV_FRGHT
		encodedResult.append(arInvoice.getInvLvFreight());
		encodedResult.append(separator);

		//INV_LV_SHFT
		encodedResult.append(arInvoice.getInvLvShift());
		encodedResult.append(separator);

		//INV_SO_NMBR
		encodedResult.append(arInvoice.getInvSoNumber());
		encodedResult.append(separator);

		//INV_CLNT_PO
		encodedResult.append(arInvoice.getInvClientPO());
		encodedResult.append(separator);

		//INV_EFFCTVTY_DT
		encodedResult.append(arInvoice.getInvEffectivityDate());
		encodedResult.append(separator);

		//GL_FUNCTIONAL_CURRENCY
		encodedResult.append(arInvoice.getGlFunctionalCurrency().getFcName());
		encodedResult.append(separator);

		//AR_TAX_CODE
		encodedResult.append(arInvoice.getArTaxCode().getTcName());
		encodedResult.append(separator);

		//AR_WITHHOLDING_TAX_CODE
		encodedResult.append(arInvoice.getArWithholdingTaxCode().getWtcName());
		encodedResult.append(separator);

		//AR_CUSTOMER
		encodedResult.append(arInvoice.getArCustomer().getCstCustomerCode());
		encodedResult.append(separator);

		//AD_PAYMENT_TERM
		encodedResult.append(arInvoice.getAdPaymentTerm().getPytName());
		encodedResult.append(separator);

		// end separator
		encodedResult.append(separator);

		String lineSeparator = "~";

		// begin lineSeparator
		encodedResult.append(lineSeparator);

		// Append Sales Order Invoice Lines
		Iterator solIter = arInvoice.getArSalesOrderInvoiceLines().iterator();

		while (solIter.hasNext()) {

			LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)solIter.next();

			// begin separator
			encodedResult.append(separator);

			// Line Code
			encodedResult.append(arSalesOrderInvoiceLine.getSilCode());
			encodedResult.append(separator);

			// Quantity Remaining
			encodedResult.append(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolQuantity());
			encodedResult.append(separator);

			// Quantity Delivered
			encodedResult.append(arSalesOrderInvoiceLine.getSilQuantityDelivered());
			encodedResult.append(separator);

			// Unit Price
			encodedResult.append(arSalesOrderInvoiceLine.getArSalesOrderLine().getSolUnitPrice());
			encodedResult.append(separator);

			// Amount
			encodedResult.append(arSalesOrderInvoiceLine.getSilAmount());
			encodedResult.append(separator);

			// Discount 1
			encodedResult.append(arSalesOrderInvoiceLine.getSilDiscount1());
			encodedResult.append(separator);

			// Discount 2
			encodedResult.append(arSalesOrderInvoiceLine.getSilDiscount2());
			encodedResult.append(separator);

			// Discount 3
			encodedResult.append(arSalesOrderInvoiceLine.getSilDiscount3());
			encodedResult.append(separator);

			// Discount 4
			encodedResult.append(arSalesOrderInvoiceLine.getSilDiscount4());
			encodedResult.append(separator);


			// Total Discount
			encodedResult.append(arSalesOrderInvoiceLine.getSilTotalDiscount());
			encodedResult.append(separator);

			// Unit of Measure
			encodedResult.append(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
			encodedResult.append(separator);

			// Item Name
			encodedResult.append(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiName());
			encodedResult.append(separator);

			// Location Name
			encodedResult.append(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvLocation().getLocName());
			encodedResult.append(separator);

			// begin lineSeparator
			encodedResult.append(lineSeparator);
		}

		// Append Sales Order Invoice Lines
		Iterator iliIter = arInvoice.getArInvoiceLineItems().iterator();

		while (iliIter.hasNext()) {

			LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)iliIter.next();

			// begin separator
			encodedResult.append(separator);

			// Line Code
			encodedResult.append(arInvoiceLineItem.getIliCode());
			encodedResult.append(separator);

			// Quantity Remaining
			encodedResult.append(arInvoiceLineItem.getIliQuantity());
			encodedResult.append(separator);

			// Quantity Delivered
			encodedResult.append("0");
			encodedResult.append(separator);

			// Unit Price
			encodedResult.append(arInvoiceLineItem.getIliUnitPrice());
			encodedResult.append(separator);

			// Amount
			encodedResult.append(arInvoiceLineItem.getIliAmount());
			encodedResult.append(separator);

			// Discount 1
			encodedResult.append(arInvoiceLineItem.getIliDiscount1());
			encodedResult.append(separator);

			// Discount 2
			encodedResult.append(arInvoiceLineItem.getIliDiscount1());
			encodedResult.append(separator);

			// Discount 3
			encodedResult.append(arInvoiceLineItem.getIliDiscount1());
			encodedResult.append(separator);

			// Discount 4
			encodedResult.append(arInvoiceLineItem.getIliDiscount1());
			encodedResult.append(separator);


			// Total Discount
			encodedResult.append(arInvoiceLineItem.getIliTotalDiscount());
			encodedResult.append(separator);

			// Unit of Measure
			encodedResult.append(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
			encodedResult.append(separator);

			// Item Name
			encodedResult.append(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
			encodedResult.append(separator);

			// Location Name
			encodedResult.append(arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName());
			encodedResult.append(separator);

			// begin lineSeparator
			encodedResult.append(lineSeparator);
		}

		// Append Distribution Records
		encodedResult.append("$JOURNAL$");

		Iterator drIter = arInvoice.getArDistributionRecords().iterator();

		while (drIter.hasNext()) {

			LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)drIter.next();

			// begin separator
			encodedResult.append(separator);

			// Dr Code
			encodedResult.append(arDistributionRecord.getDrCode());
			encodedResult.append(separator);

			// Dr Line
			encodedResult.append(arDistributionRecord.getDrLine());
			encodedResult.append(separator);

			// Dr Class
			encodedResult.append(arDistributionRecord.getDrClass());
			encodedResult.append(separator);

			// Dr Debit
			encodedResult.append(arDistributionRecord.getDrDebit());
			encodedResult.append(separator);

			// Dr Amount
			encodedResult.append(arDistributionRecord.getDrAmount());
			encodedResult.append(separator);

			// Dr COA
			encodedResult.append(arDistributionRecord.getGlChartOfAccount().getCoaCode());
			encodedResult.append(separator);

			// Dr Invoice
			encodedResult.append(arInvoice.getInvCode());
			encodedResult.append(separator);

			// begin lineSeparator
			encodedResult.append(lineSeparator);
		}

		return encodedResult.toString();

	}


	/**
	 * @ejb:interface-method
	 * */
	public int setInvAdjustment(String[] ADJ, String BR_BRNCH_CODE, Integer AD_CMPNY)
	{
		LocalAdBranchHome adBranchHome = null;

		try {
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
		} catch(Exception e) {
			return 0;
		}

		Integer AD_BRNCH = new Integer(0);
		try {
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();

		} catch(Exception e) {
			return 0;
		}

		for (int i = 0; i < ADJ.length; i++) {

			InvModAdjustmentDetails  invModAdjustmentDetails = adjRowDecode(ADJ[i]);

			try{
				invModAdjustmentDetails.setAdjVoid(EJBCommon.FALSE);
				invModAdjustmentDetails.setAdjPosted(EJBCommon.FALSE);
				invModAdjustmentDetails.setAdjCode(null);
				System.out.println("SAVING "+invModAdjustmentDetails.getAdjAlList().toString());
				Integer invAdjOrderCode = this.saveInvAdjEntry(invModAdjustmentDetails, invModAdjustmentDetails.getAdjCoaAccountNumber(),
						invModAdjustmentDetails.getAdjAlList(), true, AD_BRNCH, AD_CMPNY);

			}catch(Exception e) {
				e.printStackTrace();
				ctx.setRollbackOnly();
				return 0;
			}
		}
		return 1;

	}


	/**
	 * @ejb:interface-method
	 * */
	public int setArBillingInvoiceAndCreditMemos(String[] cmTxn, String[] invTxn,String BR_BRNCH_CODE, Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean setArMemoLineReceiptAndCreditMemos");

		LocalAdBranchHome adBranchHome = null;

		try {
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
		} catch(Exception e) {
			return 0;
		}

		Integer AD_BRNCH = new Integer(0);
		try {
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();

		} catch(Exception e) {
			return 0;
		}



		HashMap arInvDrLineMap = new HashMap();

		for (int i = 0; i < cmTxn.length; i++) {

			ArModInvoiceDetails  arModInvoiceDetails = cmRowDecode(cmTxn[i]);

			try{

				if(this.saveArCmInvEntry(arModInvoiceDetails, arModInvoiceDetails.getInvCstCustomerCode(),
						arModInvoiceDetails.getInvIbName(), AD_BRNCH, AD_CMPNY) !=1){
					System.out.print("arModInvoiceDetails.getInvCstCustomerCode(): "+arModInvoiceDetails.getInvCstCustomerCode());
					System.out.print("arModInvoiceDetails: "+arModInvoiceDetails);
					throw new Exception("UPLOAD FAILED");


				}else{

					if(arInvDrLineMap.containsKey(arModInvoiceDetails.getInvReferenceNumber())){

						HashMap arMemoLineAmountMap = (HashMap)arInvDrLineMap.get(arModInvoiceDetails.getInvReferenceNumber());
						arInvDrLineMap.remove(arModInvoiceDetails.getInvReferenceNumber());
						arInvDrLineMap.put(arModInvoiceDetails.getInvReferenceNumber(), getArMemoLineAmount(arMemoLineAmountMap,  arModInvoiceDetails.getInvCmInvoiceNumber(), AD_BRNCH, AD_CMPNY));

					}else{

						arInvDrLineMap.put(arModInvoiceDetails.getInvReferenceNumber(), getArMemoLineAmount(new HashMap(),  arModInvoiceDetails.getInvCmInvoiceNumber(), AD_BRNCH, AD_CMPNY));
					}

				}
			}catch(Exception e) {
				e.printStackTrace();
				ctx.setRollbackOnly();
				return 0;
			}
		}



		for (int i = 0; i < invTxn.length; i++) {

			ArModInvoiceDetails  arInvoiceDetails = scRowDecode(invTxn[i]);


			try{

				HashMap arMemoLineAmountMap = (HashMap)arInvDrLineMap.get(arInvoiceDetails.getInvNumber());

				//arInvoiceDetails = scAddLineFromDR(arInvoiceDetails, arMemoLineAmountMap);
				double drTotalAmount = getDRAmount(arMemoLineAmountMap);

				if(this.saveArInvEntry(arInvoiceDetails, arInvoiceDetails.getInvTcName(),
						Integer.parseInt(arInvoiceDetails.getInvCstCustomerCode()), drTotalAmount, AD_BRNCH, AD_CMPNY) !=1){

					throw new Exception("UPLOAD FAILED");

				}else{

				}



			}catch(Exception e) {
				e.printStackTrace();
				ctx.setRollbackOnly();
				return 0;
			}
		}

		return 1;

	}

	private int saveArInvEntry(com.util.ArModInvoiceDetails details, String TC_NM, int CST_CODE, double drTotalAmount, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalDocumentNumberNotUniqueException {

		Debug.print("InvItemSyncControllerBean saveArInvEntry");

		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoiceBatchHome arInvoiceBatchHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalAdPaymentScheduleHome adPaymentScheduleHome = null;
		LocalAdDiscountHome adDiscountHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;
		LocalArWithholdingTaxCodeHome arWithholdingTaxCodeHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalArInvoicePaymentScheduleHome arInvoicePaymentScheduleHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdApprovalDocumentHome adApprovalDocumentHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalArCustomerBalanceHome arCustomerBalanceHome = null;
		LocalArReceiptHome arReceiptHome = null;
		LocalAdBranchCustomerHome adBranchCustomerHome = null;
		LocalArSalespersonHome arSalespersonHome = null;


		LocalArInvoice arInvoice = null;


		// Initialize EJB Home

		try {

			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			adPaymentScheduleHome = (LocalAdPaymentScheduleHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentScheduleHome.JNDI_NAME, LocalAdPaymentScheduleHome.class);
			adDiscountHome = (LocalAdDiscountHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDiscountHome.JNDI_NAME, LocalAdDiscountHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
			arWithholdingTaxCodeHome = (LocalArWithholdingTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArWithholdingTaxCodeHome.JNDI_NAME, LocalArWithholdingTaxCodeHome.class);
			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			arInvoicePaymentScheduleHome = (LocalArInvoicePaymentScheduleHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoicePaymentScheduleHome.JNDI_NAME, LocalArInvoicePaymentScheduleHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adApprovalDocumentHome = (LocalAdApprovalDocumentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalDocumentHome.JNDI_NAME, LocalAdApprovalDocumentHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			arCustomerBalanceHome = (LocalArCustomerBalanceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerBalanceHome.JNDI_NAME, LocalArCustomerBalanceHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			adBranchCustomerHome = (LocalAdBranchCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchCustomerHome.JNDI_NAME, LocalAdBranchCustomerHome.class);
			arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		try {

			// validate if document number is unique document number is automatic then set next sequence

			LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
			Debug.print("ArInvoiceEntryControllerBean saveArInvEntry A");

			LocalArInvoice arExistingInvoice = null;

			try {

				arExistingInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
						details.getInvNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

			} catch (FinderException ex) {
			}

			if (arExistingInvoice != null &&
					!arExistingInvoice.getInvCode().equals(details.getInvCode())) {

				throw new GlobalDocumentNumberNotUniqueException();

			}

			// create invoice

			details.setInvConversionDate(null);
			details.setInvConversionRate(1);

			arInvoice = arInvoiceHome.create("ITEMS",EJBCommon.FALSE,
					details.getInvDescription(), details.getInvDate(),
					details.getInvNumber(), details.getInvReferenceNumber(),details.getInvUploadNumber(),null, null,
					0d,0d, 0d,0d, 0d, 0d, details.getInvConversionDate(), details.getInvConversionRate(), details.getInvMemo(),
					0d, 0d, details.getInvBillToAddress(), details.getInvBillToContact(), details.getInvBillToAltContact(),
					details.getInvBillToPhone(), details.getInvBillingHeader(), details.getInvBillingFooter(),
					details.getInvBillingHeader2(), details.getInvBillingFooter2(), details.getInvBillingHeader3(),
					details.getInvBillingFooter3(), details.getInvBillingSignatory(), details.getInvSignatoryTitle(),
					details.getInvShipToAddress(), details.getInvShipToContact(), details.getInvShipToAltContact(),
					details.getInvShipToPhone(), details.getInvShipDate(), details.getInvLvFreight(),
					null, null,
					EJBCommon.FALSE,
					null, EJBCommon.FALSE, EJBCommon.FALSE,
					EJBCommon.FALSE, EJBCommon.FALSE,
					EJBCommon.FALSE, null, 0d, null, null, null, null,
					details.getInvCreatedBy(), details.getInvDateCreated(),
					details.getInvLastModifiedBy(), details.getInvDateLastModified(),
					null, null, null, null, EJBCommon.FALSE, null, null, null,details.getInvDebitMemo(),
					details.getInvSubjectToCommission(), null, details.getInvDate(), AD_BRNCH, AD_CMPNY);

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalArCustomer arCustomer = arCustomerHome.findByPrimaryKey(new Integer(CST_CODE));
			arInvoice.setArCustomer(arCustomer);

			LocalAdPaymentTerm adPaymentTerm = arCustomer.getAdPaymentTerm();
			arInvoice.setAdPaymentTerm(adPaymentTerm);

			LocalGlFunctionalCurrency glFunctionalCurrency = adCompany.getGlFunctionalCurrency();
			arInvoice.setGlFunctionalCurrency(glFunctionalCurrency);

			LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			arInvoice.setArTaxCode(arTaxCode);

			LocalArWithholdingTaxCode arWithholdingTaxCode = arCustomer.getArCustomerClass().getArWithholdingTaxCode();
			arInvoice.setArWithholdingTaxCode(arWithholdingTaxCode);

			double amountDue = 0;

			// add new invoice lines and distribution record

			double TOTAL_TAX = 0d;
			double TOTAL_LINE = 0d;
			double TOTAL_UNTAXABLE = 0d;

			Iterator i = details.getInvIlList().iterator();

			while (i.hasNext()) {

				ArModInvoiceLineDetails mInvDetails = (ArModInvoiceLineDetails) i.next();

				LocalArInvoiceLine arInvoiceLine = this.addArIlEntry(mInvDetails, arInvoice, AD_CMPNY);

				// add revenue/credit distributions

				this.addArDrEntry(arInvoice.getArDrNextLine(),
						"REVENUE", EJBCommon.FALSE, arInvoiceLine.getIlAmount(),
						this.getArGlCoaRevenueAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), null, arInvoice, AD_BRNCH, AD_CMPNY);

				TOTAL_LINE += arInvoiceLine.getIlAmount();
				TOTAL_TAX += arInvoiceLine.getIlTaxAmount();

				if(arInvoiceLine.getIlTax() == EJBCommon.FALSE)
					TOTAL_UNTAXABLE += arInvoiceLine.getIlAmount();

			}


			// add tax distribution if necessary

			if (!arTaxCode.getTcType().equals("NONE") &&
					!arTaxCode.getTcType().equals("EXEMPT")) {

				if (arTaxCode.getTcInterimAccount() == null) {

					this.addArDrEntry(arInvoice.getArDrNextLine(),
							"TAX", EJBCommon.FALSE, TOTAL_TAX, arTaxCode.getGlChartOfAccount().getCoaCode(),
							null, arInvoice, AD_BRNCH, AD_CMPNY);

				} else {

					this.addArDrEntry(arInvoice.getArDrNextLine(),
							"DEFERRED TAX", EJBCommon.FALSE, TOTAL_TAX, arTaxCode.getTcInterimAccount(),
							null, arInvoice, AD_BRNCH, AD_CMPNY);

				}

			}

			// add wtax distribution if necessary

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			double W_TAX_AMOUNT = 0d;

			if (arWithholdingTaxCode.getWtcRate() != 0 && adPreference.getPrfArWTaxRealization().equals("INVOICE")) {

				W_TAX_AMOUNT = EJBCommon.roundIt((TOTAL_LINE - TOTAL_UNTAXABLE) * (arWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

				this.addArDrEntry(arInvoice.getArDrNextLine(), "W-TAX",
						EJBCommon.TRUE, W_TAX_AMOUNT, arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(),
						null, arInvoice, AD_BRNCH, AD_CMPNY);

			}

			// add payment discount if necessary

			double DISCOUNT_AMT = 0d;

			if (adPaymentTerm.getPytDiscountOnInvoice() == EJBCommon.TRUE) {

				Collection adPaymentSchedules = adPaymentScheduleHome.findByPytCode(adPaymentTerm.getPytCode(),AD_CMPNY);
				ArrayList adPaymentScheduleList = new ArrayList(adPaymentSchedules);
				LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)adPaymentScheduleList.get(0);

				Collection adDiscounts = adDiscountHome.findByPsCode(adPaymentSchedule.getPsCode(), AD_CMPNY);
				ArrayList adDiscountList = new ArrayList(adDiscounts);
				LocalAdDiscount adDiscount = (LocalAdDiscount)adDiscountList.get(0);


				double rate = adDiscount.getDscDiscountPercent();
				DISCOUNT_AMT = (TOTAL_LINE + TOTAL_TAX) * (rate / 100d);

				this.addArDrIliEntry(arInvoice.getArDrNextLine(), "DISCOUNT",
						EJBCommon.TRUE, DISCOUNT_AMT,
						adPaymentTerm.getGlChartOfAccount().getCoaCode(),
						arInvoice, AD_BRNCH, AD_CMPNY);

			}

			// add receivable distribution

			try {

				LocalAdBranchCustomer adBranchCustomer = adBranchCustomerHome.findBcstByCstCodeAndBrCode(arInvoice.getArCustomer().getCstCode(), AD_BRNCH, AD_CMPNY);

				this.addArDrEntry(arInvoice.getArDrNextLine(), "RECEIVABLE",
						EJBCommon.TRUE, drTotalAmount + TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT,
						adBranchCustomer.getBcstGlCoaReceivableAccount(),
						null, arInvoice, AD_BRNCH, AD_CMPNY);

				this.addArDrEntry(arInvoice.getArDrNextLine(), "REVENUE",
						EJBCommon.FALSE, drTotalAmount, adBranchCustomer.getBcstGlCoaReceivableAccount(),
						null, arInvoice, AD_BRNCH, AD_CMPNY);

			} catch(FinderException ex) {

			}

			// compute invoice amount due



			amountDue = TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT - DISCOUNT_AMT;

			//set invoice amount due

			arInvoice.setInvAmountDue(amountDue);

			// remove all payment schedule

			Collection arInvoicePaymentSchedules = arInvoice.getArInvoicePaymentSchedules();

			i = arInvoicePaymentSchedules.iterator();

			while (i.hasNext()) {

				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule = (LocalArInvoicePaymentSchedule)i.next();

				i.remove();

				arInvoicePaymentSchedule.remove();

			}


			// create invoice payment schedule

			short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);
			double TOTAL_PAYMENT_SCHEDULE =  0d;

			GregorianCalendar gcPrevDateDue = new GregorianCalendar();
			GregorianCalendar gcDateDue = new GregorianCalendar();
			gcPrevDateDue.setTime(arInvoice.getInvEffectivityDate());

			Collection adPaymentSchedules = adPaymentTerm.getAdPaymentSchedules();

			i = adPaymentSchedules.iterator();

			while (i.hasNext()) {

				LocalAdPaymentSchedule adPaymentSchedule = (LocalAdPaymentSchedule)i.next();

				// get date due

				if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("DEFAULT")){

					gcDateDue.setTime(arInvoice.getInvEffectivityDate());
					gcDateDue.add(Calendar.DATE, adPaymentSchedule.getPsDueDay());

				} else if (arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("MONTHLY")){

					gcDateDue = gcPrevDateDue;
					gcDateDue.add(Calendar.MONTH, 1);
					gcPrevDateDue = gcDateDue;

				} else if(arInvoice.getAdPaymentTerm().getPytScheduleBasis().equals("BI-MONTHLY")) {

					gcDateDue = gcPrevDateDue;

					if (gcPrevDateDue.get(Calendar.MONTH) != 1) {
						if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 31 && gcPrevDateDue.get(Calendar.DATE) > 15 && gcPrevDateDue.get(Calendar.DATE) < 31){
							gcDateDue.add(Calendar.DATE, 16);
						} else {
							gcDateDue.add(Calendar.DATE, 15);
						}
					} else if (gcPrevDateDue.get(Calendar.MONTH) == 1) {
						if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28 && gcPrevDateDue.get(Calendar.DATE) == 14) {
							gcDateDue.add(Calendar.DATE, 14);
						} else if(gcPrevDateDue.getActualMaximum(Calendar.DATE) == 28 && gcPrevDateDue.get(Calendar.DATE) >= 15 && gcPrevDateDue.get(Calendar.DATE) < 28) {
							gcDateDue.add(Calendar.DATE, 13);
						} else if (gcPrevDateDue.getActualMaximum(Calendar.DATE) == 29 && gcPrevDateDue.get(Calendar.DATE) >= 15 && gcPrevDateDue.get(Calendar.DATE) < 29) {
							gcDateDue.add(Calendar.DATE, 14);
						} else {
							gcDateDue.add(Calendar.DATE, 15);
						}
					}

					gcPrevDateDue = gcDateDue;

				}

				// create a payment schedule

				double PAYMENT_SCHEDULE_AMOUNT = 0;

				// if last payment schedule subtract to avoid rounding difference error

				if (i.hasNext()) {

					PAYMENT_SCHEDULE_AMOUNT = EJBCommon.roundIt((adPaymentSchedule.getPsRelativeAmount() / adPaymentTerm.getPytBaseAmount()) * arInvoice.getInvAmountDue(), precisionUnit);

				} else {

					PAYMENT_SCHEDULE_AMOUNT = arInvoice.getInvAmountDue() - TOTAL_PAYMENT_SCHEDULE;

				}

				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
					arInvoicePaymentScheduleHome.create(gcDateDue.getTime(),
							adPaymentSchedule.getPsLineNumber(),
							PAYMENT_SCHEDULE_AMOUNT,
							0d, EJBCommon.FALSE,
							(short)0, gcDateDue.getTime(), 0d, 0d,
							AD_CMPNY);

				arInvoicePaymentSchedule.setArInvoice(arInvoice);

				TOTAL_PAYMENT_SCHEDULE += PAYMENT_SCHEDULE_AMOUNT;

			}

			// generate approval status


			// set invoice approval status

			arInvoice.setInvApprovalStatus(null);

			return 1;


		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}


	}

	private void addArDrIliEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArInvoiceEntryControllerBean addArDrIliEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

			// create distribution record

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			arDistributionRecord.setArInvoice(arInvoice);
			arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

		} catch (FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addArDrEntry(short DR_LN, String DR_CLSS,
			byte DR_DBT, double DR_AMNT, Integer COA_CODE, Integer SC_COA, LocalArInvoice arInvoice, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArInvoiceEntryControllerBean addArDrEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);


			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

			// create distribution record

			if (DR_AMNT < 0  ){

				DR_AMNT = DR_AMNT * -1;

				if (DR_DBT == 0)
					DR_DBT = 1;
				else if (DR_DBT == 1)
					DR_DBT = 0;

			}

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			arDistributionRecord.setArInvoice(arInvoice);
			arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

			if(DR_CLSS.equals("SC")){
				if(SC_COA == null)
					throw new GlobalBranchAccountNumberInvalidException();
				else
					arDistributionRecord.setDrScAccount(SC_COA);
			}

		} catch(FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private LocalArInvoiceLine addArIlEntry(ArModInvoiceLineDetails mdetails, LocalArInvoice arInvoice, Integer AD_CMPNY) {

		Debug.print("ArInvoiceEntryControllerBean addArIlEntry");

		LocalArInvoiceLineHome arInvoiceLineHome = null;
		LocalArStandardMemoLineHome arStandardMemoLineHome = null;


		// Initialize EJB Home

		try {

			arInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
			arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

			double IL_AMNT = 0d;
			double IL_TAX_AMNT = 0d;

			if (mdetails.getIlTax() == EJBCommon.TRUE) {

				LocalArTaxCode arTaxCode = arInvoice.getArTaxCode();

				// calculate net amount
				IL_AMNT = this.calculateIlNetAmount(mdetails, arTaxCode.getTcRate(), arTaxCode.getTcType(), precisionUnit);

				// calculate tax
				IL_TAX_AMNT = this.calculateIlTaxAmount(mdetails, arTaxCode.getTcRate(), arTaxCode.getTcType(), IL_AMNT, precisionUnit);

			} else {

				IL_AMNT = mdetails.getIlAmount();

			}

			LocalArInvoiceLine arInvoiceLine = arInvoiceLineHome.create(
					mdetails.getIlDescription(), mdetails.getIlQuantity(),
					mdetails.getIlUnitPrice(), IL_AMNT,
					IL_TAX_AMNT, mdetails.getIlTax(), AD_CMPNY);

			arInvoiceLine.setArInvoice(arInvoice);

			LocalArStandardMemoLine arStandardMemoLine = arStandardMemoLineHome.findBySmlName(mdetails.getIlSmlName(), AD_CMPNY);
			arInvoiceLine.setArStandardMemoLine(arStandardMemoLine);

			return arInvoiceLine;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private double calculateIlTaxAmount(ArModInvoiceLineDetails mdetails, double tcRate, String tcType, double amount, short precisionUnit) {

		double taxAmount = 0d;

		if (!tcType.equals("NONE") &&
				!tcType.equals("EXEMPT")) {


			if (tcType.equals("INCLUSIVE")) {

				taxAmount = EJBCommon.roundIt(mdetails.getIlAmount() - amount, precisionUnit);

			} else if (tcType.equals("EXCLUSIVE")) {

				taxAmount = EJBCommon.roundIt(mdetails.getIlAmount() * tcRate / 100, precisionUnit);

			} else {

				// tax none zero-rated or exempt

			}

		}

		return taxAmount;

	}

	private double calculateIlNetAmount(ArModInvoiceLineDetails mdetails, double tcRate, String tcType, short precisionUnit) {

		double amount = 0d;

		if (tcType.equals("INCLUSIVE")) {

			amount = EJBCommon.roundIt(mdetails.getIlAmount() / (1 + (tcRate / 100)), precisionUnit);

		} else {

			// tax exclusive, none, zero rated or exempt

			amount = mdetails.getIlAmount();

		}

		return amount;

	}

	private HashMap getArMemoLineAmount(HashMap arMemoLineAmountMap, String ArInvoiceNumber,  Integer AD_BRNCH, Integer AD_CMPNY){

		LocalArInvoiceHome arInvoiceHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalArStandardMemoLineHome arStandardMemoLineHome = null;

		try {

			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try{

			ArrayList arStandardMemoLineList = new ArrayList();
			ArrayList arStandardMemoLineCoaList = new ArrayList();

			Iterator smlIter = arStandardMemoLineHome.findEnabledSmlAll(AD_BRNCH, AD_CMPNY).iterator();


			while(smlIter.hasNext()){

				LocalArStandardMemoLine arStandardMemoLine = (LocalArStandardMemoLine)smlIter.next();
				arStandardMemoLineList.add(arStandardMemoLine.getSmlName());
				arStandardMemoLineCoaList.add(arStandardMemoLine.getGlChartOfAccount().getCoaCode());

			}


			LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(ArInvoiceNumber,
					EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

			Iterator i = arInvoice.getArInvoiceLineItems().iterator();

			while(i.hasNext()){

				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)i.next();

				LocalAdBranchItemLocation adBranchItemLocation = null;

				try {

					adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

				} catch(FinderException ex) {

				}

				Integer COA_CODE = null;
				String SML_NAME = "";

				if(adBranchItemLocation!=null){

					COA_CODE = adBranchItemLocation.getBilCoaGlSalesAccount();

				}else{

					COA_CODE = arInvoiceLineItem.getInvItemLocation().getIlGlCoaSalesAccount();

				}

				SML_NAME = (arStandardMemoLineList.toArray()[arStandardMemoLineCoaList.indexOf(COA_CODE)]).toString();

				if(arMemoLineAmountMap.containsKey(SML_NAME)){

					double iliAmount = Double.parseDouble(arMemoLineAmountMap.get(SML_NAME).toString());
					arMemoLineAmountMap.remove(SML_NAME);
					arMemoLineAmountMap.put(SML_NAME, arInvoiceLineItem.getIliAmount() + iliAmount);

				}else{

					arMemoLineAmountMap.put(SML_NAME, arInvoiceLineItem.getIliAmount());
				}


			}

			i = arInvoice.getArSalesOrderInvoiceLines().iterator();
			while(i.hasNext()){

				LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)i.next();

				LocalAdBranchItemLocation adBranchItemLocation = null;

				try {

					adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

				} catch(FinderException ex) {

				}

				Integer COA_CODE = null;
				String SML_NAME = null;

				if(adBranchItemLocation!=null){

					COA_CODE = adBranchItemLocation.getBilCoaGlSalesAccount();

				}else{

					COA_CODE = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getIlGlCoaSalesAccount();

				}

				SML_NAME = (arStandardMemoLineList.toArray()[arStandardMemoLineCoaList.indexOf(COA_CODE)]).toString();

				if(arMemoLineAmountMap.containsKey(SML_NAME)){

					double iliAmount = Double.parseDouble(arMemoLineAmountMap.get(SML_NAME).toString());
					arMemoLineAmountMap.remove(SML_NAME);
					arMemoLineAmountMap.put(SML_NAME, arSalesOrderInvoiceLine.getSilAmount() + iliAmount);

				}else{

					arMemoLineAmountMap.put(SML_NAME, arSalesOrderInvoiceLine.getSilAmount());
				}


			}

			return arMemoLineAmountMap;

		}catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private ArModReceiptDetails mrRowDecode(String mrcm)
	{


		Debug.print("InvItemSyncControllerBean mrRowDecode");

		String separator = "$";
		String lineSeparator = "~";
		ArModReceiptDetails mrDetails = new ArModReceiptDetails();

		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_OUTPUT);
		sdf.setLenient(false);

		// Remove first $ character
		mrcm = mrcm.substring(1);

		// RCT_DT
		int start = 0;
		int nextIndex = mrcm.indexOf(separator, start);
		int length = nextIndex - start;
		try {
			mrDetails.setRctDate(sdf.parse(mrcm.substring(start, start + length)));
		} catch (Exception ex) {}

		// RCT_NMBR
		start = nextIndex + 1;
		nextIndex = mrcm.indexOf(separator, start);
		length = nextIndex - start;
		mrDetails.setRctNumber(mrcm.substring(start, start + length));

		// AR_CUSTOMER
		start = nextIndex + 1;
		nextIndex = mrcm.indexOf(separator, start);
		length = nextIndex - start;
		mrDetails.setRctCstCustomerCode(mrcm.substring(start, start + length));

		// TAX_CODE
		start = nextIndex + 1;
		nextIndex = mrcm.indexOf(separator, start);
		length = nextIndex - start;
		mrDetails.setRctTcName(mrcm.substring(start, start + length));

		mrDetails.setInvIlList(new ArrayList());

		while (true) {

			ArModInvoiceLineDetails mInvDetails = new ArModInvoiceLineDetails();

			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = mrcm.indexOf(lineSeparator, start);
			length = nextIndex - start;

			// begin separator
			start = nextIndex + 1;
			nextIndex = mrcm.indexOf(separator, start);
			length = nextIndex - start;

			// IL_DESC
			start = nextIndex + 1;
			nextIndex = mrcm.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println(" IL_DESC: " + mrcm.substring(start, start + length));
			mInvDetails.setIlDescription(mrcm.substring(start, start + length));

			// IL_QNTTY
			start = nextIndex + 1;
			nextIndex = mrcm.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println("IL_QNTTY: " + mrcm.substring(start, start + length));
			mInvDetails.setIlQuantity((double)Double.parseDouble(mrcm.substring(start, start + length)));

			// IL_UNT_PRC
			start = nextIndex + 1;
			nextIndex = mrcm.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println("IL_UNT_PRC: " + mrcm.substring(start, start + length));
			mInvDetails.setIlUnitPrice((double)Double.parseDouble(mrcm.substring(start, start + length)));

			mInvDetails.setIlAmount(mInvDetails.getIlQuantity() * mInvDetails.getIlUnitPrice());

			// AR_STANDARD_MEMO_LINE
			start = nextIndex + 1;
			nextIndex = mrcm.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println(" AR_STANDARD_MEMO_LINE: " + mrcm.substring(start, start + length));
			mInvDetails.setIlSmlName(mrcm.substring(start, start + length));

			mrDetails.saveInvIlList(mInvDetails);

			int tempStart = nextIndex + 1;
			if (mrcm.indexOf(separator, tempStart) == -1) break;

		}

		return mrDetails;
	}

	/*
	 *  Reserved Method for Future Use
	 */
	private ArModInvoiceDetails scAddLineFromDR(ArModInvoiceDetails scDetails, HashMap arMemoLineAmountMap)
	{

		Iterator smlIter = arMemoLineAmountMap.keySet().iterator();
		while(smlIter.hasNext()){
			String smlName = smlIter.next().toString();
			System.out.println("smlCode: " + smlName+ "\t" + " val: " + arMemoLineAmountMap.get(smlName));

			ArModInvoiceLineDetails mInvDetails = new ArModInvoiceLineDetails();

			mInvDetails.setIlDescription("Sales Entry From DR's");
			mInvDetails.setIlQuantity(1);
			mInvDetails.setIlUnitPrice((double)Double.parseDouble(arMemoLineAmountMap.get(smlName).toString()));
			mInvDetails.setIlAmount(mInvDetails.getIlQuantity() * mInvDetails.getIlUnitPrice());
			mInvDetails.setIlSmlName(smlName);

			scDetails.getInvIlList().add(mInvDetails);
		}

		return scDetails;
	}

	private double getDRAmount(HashMap arMemoLineAmountMap)
	{
		double drTotalAmount = 0;

		Iterator smlIter = arMemoLineAmountMap.keySet().iterator();

		while(smlIter.hasNext()){

			String smlName = smlIter.next().toString();
			drTotalAmount += (double)Double.parseDouble(arMemoLineAmountMap.get(smlName).toString());

		}

		return drTotalAmount;
	}

	private ArModInvoiceDetails scRowDecode(String sc)
	{
		Debug.print("InvItemSyncControllerBean scRowDecode");

		String separator = "$";
		String lineSeparator = "~";
		ArModInvoiceDetails scDetails = new ArModInvoiceDetails();

		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_OUTPUT);
		sdf.setLenient(false);

		// Remove first $ character
		sc = sc.substring(1);

		// INV_DATE
		int start = 0;
		int nextIndex = sc.indexOf(separator, start);
		int length = nextIndex - start;
		try {
			scDetails.setInvDate(sdf.parse(sc.substring(start, start + length)));
		} catch (Exception ex) {}

		// INV_NUMBER
		start = nextIndex + 1;
		nextIndex = sc.indexOf(separator, start);
		length = nextIndex - start;
		scDetails.setInvNumber(sc.substring(start, start + length));

		// INV_REFERENCE
		start = nextIndex + 1;
		nextIndex = sc.indexOf(separator, start);
		length = nextIndex - start;
		scDetails.setInvReferenceNumber(sc.substring(start, start + length));

		// INV_DESC
		start = nextIndex + 1;
		nextIndex = sc.indexOf(separator, start);
		length = nextIndex - start;
		scDetails.setInvDescription(sc.substring(start, start + length));

		// AR_CUSTOMER
		start = nextIndex + 1;
		nextIndex = sc.indexOf(separator, start);
		length = nextIndex - start;
		scDetails.setInvCstCustomerCode(sc.substring(start, start + length));

		// TAX_CODE
		start = nextIndex + 1;
		nextIndex = sc.indexOf(separator, start);
		length = nextIndex - start;
		scDetails.setInvTcName(sc.substring(start, start + length));

		scDetails.setInvIlList(new ArrayList());

		while (true) {

			int tempStart = nextIndex + 1;
			if (sc.indexOf(separator, tempStart) == -1) break;

			ArModInvoiceLineDetails mInvDetails = new ArModInvoiceLineDetails();

			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = sc.indexOf(lineSeparator, start);
			length = nextIndex - start;

			// begin separator
			start = nextIndex + 1;
			nextIndex = sc.indexOf(separator, start);
			length = nextIndex - start;

			// IL_DESC
			start = nextIndex + 1;
			nextIndex = sc.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println(" IL_DESC: " + sc.substring(start, start + length));
			mInvDetails.setIlDescription(sc.substring(start, start + length));

			// IL_QNTTY
			start = nextIndex + 1;
			nextIndex = sc.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println("IL_QNTTY: " + sc.substring(start, start + length));
			mInvDetails.setIlQuantity((double)Double.parseDouble(sc.substring(start, start + length)));

			// IL_UNT_PRC
			start = nextIndex + 1;
			nextIndex = sc.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println("IL_UNT_PRC: " + sc.substring(start, start + length));
			mInvDetails.setIlUnitPrice((double)Double.parseDouble(sc.substring(start, start + length)));

			mInvDetails.setIlAmount(mInvDetails.getIlQuantity() * mInvDetails.getIlUnitPrice());

			// AR_STANDARD_MEMO_LINE
			start = nextIndex + 1;
			nextIndex = sc.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println(" AR_STANDARD_MEMO_LINE: " + sc.substring(start, start + length));
			mInvDetails.setIlSmlName(sc.substring(start, start + length));

			scDetails.getInvIlList().add(mInvDetails);

			tempStart = nextIndex + 1;
			if (sc.indexOf(separator, tempStart) == -1) break;
		}

		return scDetails;
	}


	private ArModInvoiceDetails cmRowDecode(String mrcm)
	{

		Debug.print("InvItemSyncControllerBean cmRowDecode");

		String separator = "$";
		ArModInvoiceDetails arCmDetails = new ArModInvoiceDetails();

		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_OUTPUT);
		sdf.setLenient(false);

		// Remove first $ character
		mrcm = mrcm.substring(1);

		// CM DESC
		int start = 0;
		int nextIndex = mrcm.indexOf(separator, start);
		int length = nextIndex - start;
		arCmDetails.setInvDescription(mrcm.substring(start, start + length));

		// CM DATE
		start = nextIndex + 1;
		nextIndex = mrcm.indexOf(separator, start);
		length = nextIndex - start;
		try {

			arCmDetails.setInvDate(sdf.parse(mrcm.substring(start, start + length)));

		} catch (Exception ex) {

			//throw ex;
		}

		// CM NUMBER
		start = nextIndex + 1;
		nextIndex = mrcm.indexOf(separator, start);
		length = nextIndex - start;
		arCmDetails.setInvNumber(mrcm.substring(start, start + length));

		// CM REF NUMBER
		start = nextIndex + 1;
		nextIndex = mrcm.indexOf(separator, start);
		length = nextIndex - start;
		arCmDetails.setInvReferenceNumber(mrcm.substring(start, start + length));

		// CM INVOICE NUMBER
		start = nextIndex + 1;
		nextIndex = mrcm.indexOf(separator, start);
		length = nextIndex - start;
		arCmDetails.setInvCmInvoiceNumber(mrcm.substring(start, start + length));

		// CM AMOUNT DUE
		start = nextIndex + 1;
		nextIndex = mrcm.indexOf(separator, start);
		length = nextIndex - start;
		arCmDetails.setInvAmountDue(Double.parseDouble(mrcm.substring(start, start + length)));

		// CM CREATED BY
		start = nextIndex + 1;
		nextIndex = mrcm.indexOf(separator, start);
		length = nextIndex - start;
		arCmDetails.setInvCreatedBy(mrcm.substring(start, start + length));

		// CM BATCH
		start = nextIndex + 1;
		nextIndex = mrcm.indexOf(separator, start);
		length = nextIndex - start;
		arCmDetails.setInvIbName(mrcm.substring(start, start + length));

		// end separator
		start = nextIndex + 1;
		nextIndex = mrcm.indexOf(separator, start);
		length = nextIndex - start;

		arCmDetails.setInvLastModifiedBy(arCmDetails.getInvCreatedBy());
		arCmDetails.setInvDateCreated(new Date());
		arCmDetails.setInvDateLastModified(arCmDetails.getInvDateCreated());

		return arCmDetails;


	}

	private int saveArRctEntry(ArModReceiptDetails details, String TC_NM,
			int CST_CODE, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean saveArRctEntry");

		LocalArReceiptHome arReceiptHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalArTaxCodeHome arTaxCodeHome = null;

		LocalArCustomerHome arCustomerHome = null;


		LocalArReceipt arReceipt = null;

		// Initialize EJB Home

		try {

			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			arTaxCodeHome = (LocalArTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalArTaxCodeHome.JNDI_NAME, LocalArTaxCodeHome.class);
			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// used in checking if receipt should re-generate distribution records and re-calculate taxes
			boolean isRecalculate = true;

			// create misc receipt

			arReceipt = arReceiptHome.create("MISC", details.getRctDescription(),
					details.getRctDate(), details.getRctNumber(),details.getRctReferenceNumber(), null,null,
					null,null,null,null,null,
					0d, 0d,0d,0d,0d, 0d, 0d,
					details.getRctConversionDate(), details.getRctConversionRate(),
					details.getRctSoldTo(), details.getRctPaymentMethod(), details.getRctCustomerDeposit(), 0d, null, null,
					EJBCommon.FALSE, null, EJBCommon.FALSE, EJBCommon.FALSE,
					EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
				    null, null, null, null, null,
					details.getRctCreatedBy(), details.getRctDateCreated(), details.getRctLastModifiedBy(), details.getRctDateLastModified(),
					null, null, null, null, EJBCommon.FALSE, null, EJBCommon.FALSE, details.getRctSubjectToCommission(), null, null,
					EJBCommon.FALSE, EJBCommon.FALSE, null,
					AD_BRNCH, AD_CMPNY);

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalArCustomer arCustomer = arCustomerHome.findByPrimaryKey(new Integer(CST_CODE));
			arReceipt.setArCustomer(arCustomer);

			LocalArTaxCode arTaxCode = arTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			arReceipt.setArTaxCode(arTaxCode);

			arReceipt.setAdBankAccount(arCustomer.getAdBankAccount());
			arReceipt.setGlFunctionalCurrency(adCompany.getGlFunctionalCurrency());
			arReceipt.setRctCustomerName(arCustomer.getCstName());

			LocalArWithholdingTaxCode arWithholdingTaxCode = arCustomer.getArCustomerClass().getArWithholdingTaxCode();
			arReceipt.setArWithholdingTaxCode(arWithholdingTaxCode);

			arReceipt.setRctConversionRate(1);

			if (isRecalculate) {

				// remove line items

				Collection arInvoiceLineItems = arReceipt.getArInvoiceLineItems();

				Iterator i = arInvoiceLineItems.iterator();

				// remove all distribution records

				double TOTAL_TAX = 0d;
				double TOTAL_LINE = 0d;
				double TOTAL_UNTAXABLE = 0d;


				i = details.getInvIlList().iterator();

				while (i.hasNext()) {

					ArModInvoiceLineDetails mInvDetails = (ArModInvoiceLineDetails) i.next();

					LocalArInvoiceLine arInvoiceLine = null; //this.addArIlEntry(mInvDetails, arReceipt, AD_CMPNY);

					// add revenue/credit distributions

					this.addArRctDrEntry(arReceipt.getArDrNextLine(),
							"REVENUE", EJBCommon.FALSE, arInvoiceLine.getIlAmount(),
							this.getArGlCoaRevenueAccount(arInvoiceLine, AD_BRNCH, AD_CMPNY), EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

					TOTAL_LINE += arInvoiceLine.getIlAmount();
					TOTAL_TAX += arInvoiceLine.getIlTaxAmount();

					if(arInvoiceLine.getIlTax() == EJBCommon.FALSE)
						TOTAL_UNTAXABLE += arInvoiceLine.getIlAmount();

				}


				// add tax distribution if necessary

				if (!arTaxCode.getTcType().equals("NONE") &&
						!arTaxCode.getTcType().equals("EXEMPT")) {

					this.addArRctDrEntry(arReceipt.getArDrNextLine(),
							"TAX", EJBCommon.FALSE, TOTAL_TAX, arTaxCode.getGlChartOfAccount().getCoaCode(),
							EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

				}

				// add wtax distribution if necessary

				double W_TAX_AMOUNT = 0d;

				if (arWithholdingTaxCode.getWtcRate() != 0) {

					W_TAX_AMOUNT = EJBCommon.roundIt((TOTAL_LINE - TOTAL_UNTAXABLE) * (arWithholdingTaxCode.getWtcRate() / 100), this.getGlFcPrecisionUnit(AD_CMPNY));

					this.addArRctDrEntry(arReceipt.getArDrNextLine(), "W-TAX",
							EJBCommon.TRUE, W_TAX_AMOUNT, arWithholdingTaxCode.getGlChartOfAccount().getCoaCode(),
							EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

				}


				// add cash distribution

				this.addArRctDrEntry(arReceipt.getArDrNextLine(), "CASH",
						EJBCommon.TRUE, TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT,
						arReceipt.getAdBankAccount().getBaCoaGlCashAccount(),
						EJBCommon.FALSE, arReceipt, AD_BRNCH, AD_CMPNY);

				// set receipt amount

				arReceipt.setRctAmount(TOTAL_LINE + TOTAL_TAX - W_TAX_AMOUNT);

			}

			// set receipt approval status
			arReceipt.setRctApprovalStatus(null);

			return 1;


		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}


	}

	private LocalArInvoiceLine addArIlEntryx(ArModInvoiceLineDetails mdetails, LocalArReceipt arReceipt, Integer AD_CMPNY) {

		Debug.print("ArMiscReceiptEntryControllerBean addArIlEntry");

		LocalArInvoiceLineHome arInvoiceLineHome = null;
		LocalArStandardMemoLineHome arStandardMemoLineHome = null;


		// Initialize EJB Home

		try {

			arInvoiceLineHome = (LocalArInvoiceLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceLineHome.JNDI_NAME, LocalArInvoiceLineHome.class);
			arStandardMemoLineHome = (LocalArStandardMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArStandardMemoLineHome.JNDI_NAME, LocalArStandardMemoLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

			double IL_AMNT = 0d;
			double IL_TAX_AMNT = 0d;

			if (mdetails.getIlTax() == EJBCommon.TRUE) {

				// calculate net amount

				LocalArTaxCode arTaxCode = arReceipt.getArTaxCode();

				if (arTaxCode.getTcType().equals("INCLUSIVE")) {

					IL_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() / (1 + (arTaxCode.getTcRate() / 100)), precisionUnit);

				} else {

					// tax exclusive, none, zero rated or exempt

					IL_AMNT = mdetails.getIlAmount();

				}

				// calculate tax

				if (!arTaxCode.getTcType().equals("NONE") &&
						!arTaxCode.getTcType().equals("EXEMPT")) {


					if (arTaxCode.getTcType().equals("INCLUSIVE")) {

						IL_TAX_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() - IL_AMNT, precisionUnit);

					} else if (arTaxCode.getTcType().equals("EXCLUSIVE")) {

						IL_TAX_AMNT = EJBCommon.roundIt(mdetails.getIlAmount() * arTaxCode.getTcRate() / 100, precisionUnit);

					} else {

						// tax none zero-rated or exempt

					}

				}

			} else {

				IL_AMNT = mdetails.getIlAmount();

			}

			LocalArInvoiceLine arInvoiceLine = arInvoiceLineHome.create(
					mdetails.getIlDescription(), mdetails.getIlQuantity(),
					mdetails.getIlUnitPrice(), IL_AMNT,
					IL_TAX_AMNT, mdetails.getIlTax(), AD_CMPNY);

			arInvoiceLine.setArReceipt(arReceipt);

			LocalArStandardMemoLine arStandardMemoLine = arStandardMemoLineHome.findBySmlName(mdetails.getIlSmlName(), AD_CMPNY);
			arInvoiceLine.setArStandardMemoLine(arStandardMemoLine);

			return arInvoiceLine;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private Integer getArGlCoaRevenueAccount(LocalArInvoiceLine arInvoiceLine, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("ArInvoiceLineEntryControllerBean getArGlCoaRevenueAccount");


		LocalArAutoAccountingSegmentHome arAutoAccountingSegmentHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdBranchStandardMemoLineHome adBranchStandardMemoLineHome = null;

		// Initialize EJB Home

		try {

			arAutoAccountingSegmentHome = (LocalArAutoAccountingSegmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalArAutoAccountingSegmentHome.JNDI_NAME, LocalArAutoAccountingSegmentHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adBranchStandardMemoLineHome = (LocalAdBranchStandardMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchStandardMemoLineHome.JNDI_NAME, LocalAdBranchStandardMemoLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		// generate revenue account

		try {

			String GL_COA_ACCNT = "";

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGenField genField = adCompany.getGenField();

			String FL_SGMNT_SPRTR = String.valueOf(genField.getFlSegmentSeparator());

			LocalAdBranchStandardMemoLine adBranchStandardMemoLine = null;

			try {

				adBranchStandardMemoLine = adBranchStandardMemoLineHome.findBSMLBySMLCodeAndBrCode(arInvoiceLine.getArStandardMemoLine().getSmlCode(), AD_BRNCH, AD_CMPNY);

			} catch(FinderException ex) {

			}


			Collection arAutoAccountingSegments = arAutoAccountingSegmentHome.findByAaAccountType("REVENUE", AD_CMPNY);

			Iterator i = arAutoAccountingSegments.iterator();

			while (i.hasNext()) {

				LocalArAutoAccountingSegment arAutoAccountingSegment =
					(LocalArAutoAccountingSegment) i.next();

				LocalGlChartOfAccount glChartOfAccount = null;

				if (arAutoAccountingSegment.getAasClassType().equals("AR CUSTOMER")) {

					glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
							arInvoiceLine.getArReceipt().getArCustomer().getCstGlCoaRevenueAccount());

					StringTokenizer st = new StringTokenizer(
							glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

					int ctr = 0;
					while (st.hasMoreTokens()) {

						++ctr;

						if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

							GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR +
							st.nextToken();

							break;

						} else {

							st.nextToken();

						}
					}


				} else if (arAutoAccountingSegment.getAasClassType().equals("AR STANDARD MEMO LINE")) {

					if(adBranchStandardMemoLine != null) {

						try {

							glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlAccount());

						} catch (FinderException ex) {

						}

					} else {

						glChartOfAccount = arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount();

					}

					StringTokenizer st = new StringTokenizer(
							glChartOfAccount.getCoaAccountNumber(), FL_SGMNT_SPRTR);

					int ctr = 0;
					while (st.hasMoreTokens()) {

						++ctr;

						if (ctr == arAutoAccountingSegment.getAasSegmentNumber()) {

							GL_COA_ACCNT = GL_COA_ACCNT + FL_SGMNT_SPRTR +
							st.nextToken();

							break;

						} else {

							st.nextToken();

						}
					}

				}
			}

			GL_COA_ACCNT = GL_COA_ACCNT.substring(1, GL_COA_ACCNT.length());

			try {

				LocalGlChartOfAccount glGeneratedChartOfAccount =
					glChartOfAccountHome.findByCoaAccountNumber(GL_COA_ACCNT, AD_CMPNY);

				return glGeneratedChartOfAccount.getCoaCode();

			} catch (FinderException ex) {

				if(adBranchStandardMemoLine != null) {

					LocalGlChartOfAccount glChartOfAccount = null;

					try {

						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adBranchStandardMemoLine.getBsmlGlAccount());;

					} catch(FinderException e) {

					}

					return glChartOfAccount.getCoaCode();

				} else {

					return arInvoiceLine.getArStandardMemoLine().getGlChartOfAccount().getCoaCode();

				}

			}


		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private void addArRctDrEntry(short DR_LN, String DR_CLSS,
			byte DR_DBT, double DR_AMNT, Integer COA_CODE, byte DR_RVRSD, LocalArReceipt arReceipt, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalBranchAccountNumberInvalidException {

		Debug.print("ArMiscReceiptEntryControllerBean addArDrEntry");

		LocalArDistributionRecordHome arDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company
			System.out.println("COA_CODE: " + COA_CODE);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

			// create distribution record

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, DR_RVRSD, AD_CMPNY);

			arDistributionRecord.setArReceipt(arReceipt);
			arDistributionRecord.setGlChartOfAccount(glChartOfAccount);

		} catch (FinderException ex) {

			throw new GlobalBranchAccountNumberInvalidException(ex.getMessage());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}
	}

	private int saveArCmInvEntry(com.util.ArInvoiceDetails details, String CST_CSTMR_CODE, String IB_NM, Integer AD_BRNCH, Integer AD_CMPNY)  {

		Debug.print("InvItemSyncControllerBean saveArCmInvEntry");

		LocalArInvoiceHome arInvoiceHome = null;
		LocalArInvoice arCreditMemo = null;
		LocalArInvoiceBatchHome arInvoiceBatchHome = null;
		LocalArDistributionRecordHome arDistributionRecordHome = null;

		// Initialize EJB Home
		System.out.println("Checkpoint A");
		try {

			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			arDistributionRecordHome = (LocalArDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalArDistributionRecordHome.JNDI_NAME, LocalArDistributionRecordHome.class);
			arInvoiceBatchHome = (LocalArInvoiceBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceBatchHome.JNDI_NAME, LocalArInvoiceBatchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}
		System.out.println("Checkpoint B");
		try {
			System.out.println("Checkpoint C");

			arCreditMemo = arInvoiceHome.create("ITEMS", EJBCommon.TRUE,
					details.getInvDescription(), details.getInvDate(),
					details.getInvNumber(), details.getInvReferenceNumber(), details.getInvUploadNumber(),details.getInvCmInvoiceNumber(), details.getInvCmReferenceNumber(),
					details.getInvAmountDue(), 0d,0d, 0d, 0d,0d, details.getInvConversionDate(), details.getInvConversionRate(), details.getInvMemo(),
					0d, 0d, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null,
					EJBCommon.FALSE,
					null, EJBCommon.FALSE, EJBCommon.FALSE,
					EJBCommon.FALSE, EJBCommon.FALSE,
					EJBCommon.FALSE, null, 0d, null, null, null, null,
					details.getInvCreatedBy(),
					details.getInvDateCreated(), details.getInvLastModifiedBy(), details.getInvDateLastModified(),
					null, null, null, null, EJBCommon.FALSE, null, null, null, EJBCommon.FALSE, details.getInvSubjectToCommission(),
					details.getInvClientPO(), details.getInvEffectivityDate(), AD_BRNCH, AD_CMPNY);

			LocalArInvoice arCMInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(arCreditMemo.getInvCmInvoiceNumber(),
					EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
			arCreditMemo.setArSalesperson(arCMInvoice.getArSalesperson());
			arCreditMemo.setArCustomer(arCMInvoice.getArCustomer());
			try {
				LocalArInvoiceBatch arInvoiceBatch = arInvoiceBatchHome.findByIbName(IB_NM, AD_BRNCH, AD_CMPNY);
				arCreditMemo.setArInvoiceBatch(arInvoiceBatch);
			} catch (FinderException ex) {}

			// create distribution records

			Iterator i = null; //arCMInvoice.getArDistributionRecords().iterator();

			double ratio = arCMInvoice.getInvAmountDue() / arCreditMemo.getInvAmountDue();

			LocalArDistributionRecord arDistributionRecord = arDistributionRecordHome.findByDrClassAndInvCode("RECEIVABLE",arCMInvoice.getInvCode(),AD_CMPNY);

			LocalArDistributionRecord revenueCmArDistributionRecord = arDistributionRecordHome.create(
					(short)1, "REVENUE", (byte)1,
					arDistributionRecord.getDrAmount()*ratio, EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			revenueCmArDistributionRecord.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());
			arCreditMemo.addArDistributionRecord(revenueCmArDistributionRecord);

			LocalArDistributionRecord receivableCmArDistributionRecord = arDistributionRecordHome.create(
					(short)2, "RECEIVABLE", (byte)0,
					arDistributionRecord.getDrAmount()*ratio, EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			receivableCmArDistributionRecord.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());
			arCreditMemo.addArDistributionRecord(receivableCmArDistributionRecord);
			/*
			while(i.hasNext()){

				LocalArDistributionRecord arDistributionRecord = (LocalArDistributionRecord)i.next();

				double drAmount = EJBCommon.roundIt(arDistributionRecord.getDrAmount() / ratio, this.getGlFcPrecisionUnit(AD_CMPNY));

				LocalArDistributionRecord cmArDistributionRecord = arDistributionRecordHome.create(
						arDistributionRecord.getDrLine(), arDistributionRecord.getDrClass(), (byte)(arDistributionRecord.getDrDebit() == 1 ? 0 : 1),
						drAmount, EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

				System.out.println("Checkpoint M");
				cmArDistributionRecord.setGlChartOfAccount(arDistributionRecord.getGlChartOfAccount());
				System.out.println("Checkpoint N");
				arCreditMemo.addArDistributionRecord(cmArDistributionRecord);
				System.out.println("Checkpoint O");
			}*/

			// create new invoice payment schedule lock

			Collection arInvoicePaymentSchedules = arCMInvoice.getArInvoicePaymentSchedules();

			i = arInvoicePaymentSchedules.iterator();

			while (i.hasNext()) {

				LocalArInvoicePaymentSchedule arInvoicePaymentSchedule =
					(LocalArInvoicePaymentSchedule)i.next();

				arInvoicePaymentSchedule.setIpsLock(EJBCommon.TRUE);

			}

			// Create Transaction Log
			/*
			 *          INV_CRTD_BY
      	     		INV_DT_CRTD
      	     		INV_LST_MDFD_BY
      	     		INV_DT_LST_MDFD
			 */
			return 1;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}


	}


	private Integer saveInvAdjEntry(com.util.InvAdjustmentDetails details, String COA_ACCOUNT_NUMBER,
			ArrayList alList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalRecordAlreadyDeletedException,
			GlobalBranchAccountNumberInvalidException,
			GlobalTransactionAlreadyApprovedException,
			GlobalTransactionAlreadyPendingException,
			GlobalTransactionAlreadyPostedException,
			GlobalNoApprovalRequesterFoundException,
			GlobalNoApprovalApproverFoundException,
			GlobalInvItemLocationNotFoundException,
			GlJREffectiveDateNoPeriodExistException,
			GlJREffectiveDatePeriodClosedException,
			GlobalJournalNotBalanceException,
			GlobalDocumentNumberNotUniqueException,
			GlobalInventoryDateException,
			GlobalAccountNumberInvalidException,
			AdPRFCoaGlVarianceAccountNotFoundException {


		Debug.print("InvItemSyncControllerBean saveInvAdjEntry");

		LocalInvAdjustmentHome invAdjustmentHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

		// Initialize EJB Home

		try {

			invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {

			LocalInvAdjustment invAdjustment = null;

			// validate if Adjustment is already deleted

			try {

				if (details.getAdjCode() != null) {

					invAdjustment = invAdjustmentHome.findByPrimaryKey(details.getAdjCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if adjustment is already posted, void, approved or pending

			if (details.getAdjCode() != null) {


				if (invAdjustment.getAdjApprovalStatus() != null) {

					if (invAdjustment.getAdjApprovalStatus().equals("APPROVED") ||
							invAdjustment.getAdjApprovalStatus().equals("N/A")) {

						throw new GlobalTransactionAlreadyApprovedException();


					} else if (invAdjustment.getAdjApprovalStatus().equals("PENDING")) {

						throw new GlobalTransactionAlreadyPendingException();

					}

				}

				if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyPostedException();

				}

			}

			LocalInvAdjustment invExistingAdjustment = null;

			try {

				invExistingAdjustment = invAdjustmentHome.findByAdjDocumentNumberAndBrCode(details.getAdjDocumentNumber(), AD_BRNCH, AD_CMPNY);

			} catch (FinderException ex) {

			}


			// 	validate if document number is unique document number is automatic then set next sequence

			if (details.getAdjCode() == null) {


				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

				if (invExistingAdjustment != null) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				try {

					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

				} catch (FinderException ex) {

				}

				try {

					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

				}

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
						(details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {

					while (true) {

						if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

							try {

								invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

							} catch (FinderException ex) {

								details.setAdjDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
								break;

							}

						} else {

							try {

								invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

							} catch (FinderException ex) {

								details.setAdjDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
								break;

							}

						}

					}

				}

			} else {


				if (invExistingAdjustment != null &&
						!invExistingAdjustment.getAdjCode().equals(details.getAdjCode())) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (invAdjustment.getAdjDocumentNumber() != details.getAdjDocumentNumber() &&
						(details.getAdjDocumentNumber() == null || details.getAdjDocumentNumber().trim().length() == 0)) {

					details.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());

				}

			}

			// used in checking if invoice should re-generate distribution records and re-calculate taxes
			boolean isRecalculate = true;

			// create adjustment

			if (details.getAdjCode() == null) {

				invAdjustment = invAdjustmentHome.create(details.getAdjDocumentNumber(), details.getAdjReferenceNumber(), details.getAdjDescription(),
						details.getAdjDate(), details.getAdjType(), details.getAdjApprovalStatus(),
						EJBCommon.FALSE, details.getAdjCreatedBy(), details.getAdjDateCreated(),
						details.getAdjLastModifiedBy(), details.getAdjDateLastModified(),
						null, null, null, null, null, null, EJBCommon.FALSE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

			} else {

				// check if critical fields are changed

				if (!invAdjustment.getGlChartOfAccount().getCoaAccountNumber().equals(COA_ACCOUNT_NUMBER) ||
						alList.size() != invAdjustment.getInvAdjustmentLines().size() ||
						!(invAdjustment.getAdjDate().equals(details.getAdjDate()))) {

					isRecalculate = true;

				} else if (alList.size() == invAdjustment.getInvAdjustmentLines().size()) {

					Iterator alIter = invAdjustment.getInvAdjustmentLines().iterator();
					Iterator alListIter = alList.iterator();

					while (alIter.hasNext()) {

						LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)alIter.next();
						InvModAdjustmentLineDetails mdetails = (InvModAdjustmentLineDetails)alListIter.next();

						if (!invAdjustmentLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getAlIiName()) ||
								!invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getAlLocName()) ||
								!invAdjustmentLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getAlUomName()) ||
								invAdjustmentLine.getAlAdjustQuantity() != mdetails.getAlAdjustQuantity() ||
								invAdjustmentLine.getAlUnitCost() != mdetails.getAlUnitCost()) {

							isRecalculate = true;
							break;

						}

						isRecalculate = false;

					}

				} else {

					isRecalculate = false;

				}

				invAdjustment.setAdjDocumentNumber(details.getAdjDocumentNumber());
				invAdjustment.setAdjReferenceNumber(details.getAdjReferenceNumber());
				invAdjustment.setAdjDescription(details.getAdjDescription());
				invAdjustment.setAdjDate(details.getAdjDate());
				invAdjustment.setAdjType(details.getAdjType());
				invAdjustment.setAdjApprovalStatus(details.getAdjApprovalStatus());
				invAdjustment.setAdjLastModifiedBy(details.getAdjLastModifiedBy());
				invAdjustment.setAdjDateLastModified(details.getAdjDateLastModified());
				invAdjustment.setAdjReasonForRejection(null);

			}

			try {
				System.out.println("COA_ACCOUNT_NUMBER: " + COA_ACCOUNT_NUMBER);
				LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(COA_ACCOUNT_NUMBER, AD_BRNCH, AD_CMPNY);
				invAdjustment.setGlChartOfAccount(glChartOfAccount);


			} catch (FinderException ex) {

				throw new GlobalAccountNumberInvalidException();

			}

			double ABS_TOTAL_AMOUNT = 0d;

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			if (isRecalculate) {

				// remove all adjustment lines

				Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();

				Iterator i = invAdjustmentLines.iterator();

				while (i.hasNext()) {

					LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();

					if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

						double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

						invAdjustmentLine.getInvItemLocation().setIlCommittedQuantity(invAdjustmentLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);

					}

					i.remove();

					invAdjustmentLine.remove();

				}

				// remove all distribution records

				Collection arDistributionRecords = invAdjustment.getInvDistributionRecords();

				i = arDistributionRecords.iterator();

				while (i.hasNext()) {

					LocalInvDistributionRecord arDistributionRecord = (LocalInvDistributionRecord)i.next();

					i.remove();

					arDistributionRecord.remove();

				}

				// add new adjustment lines and distribution record

				double TOTAL_AMOUNT = 0d;

				byte DEBIT = 0;

				i = alList.iterator();

				while (i.hasNext()) {

					InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();

					LocalInvItemLocation invItemLocation = null;

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(), mdetails.getAlIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));

					}

					//	start date validation
					if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
					}

					LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(mdetails, invAdjustment, EJBCommon.FALSE, AD_CMPNY);

					// add physical inventory distribution

					double AMOUNT = 0d;

					if (invAdjustmentLine.getAlAdjustQuantity() > 0) {

						AMOUNT = EJBCommon.roundIt(
								invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(),
								this.getGlFcPrecisionUnit(AD_CMPNY));
						DEBIT = EJBCommon.TRUE;

					} else {

						double COST = 0d;

						try {
							System.out.println("invAdjustmentLine.getAlUnitCost(): " + invAdjustmentLine.getAlUnitCost());
							if(invAdjustmentLine.getAlUnitCost()==0 || invAdjustmentLine.getAlUnitCost()<0){
								LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(details.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
										invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

								COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
							}else{
								COST = invAdjustmentLine.getAlUnitCost();
							}

							System.out.println("COST! " + COST);

						} catch (FinderException ex) {

							COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();
							System.out.println("COST2! " + COST);
						}

						COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
								invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
								COST, true, AD_CMPNY);

						System.out.println("COST3! " + COST);
						invAdjustmentLine.setAlUnitCost(COST);

						AMOUNT = EJBCommon.roundIt(
								invAdjustmentLine.getAlAdjustQuantity() * COST,
								this.getGlFcPrecisionUnit(AD_CMPNY));

						DEBIT = EJBCommon.FALSE;

					}

					// check for branch mapping

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try{

						adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
								invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					LocalGlChartOfAccount glInventoryChartOfAccount = null;

					if (adBranchItemLocation == null) {

						glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
								invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
					} else {

						glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
								adBranchItemLocation.getBilCoaGlInventoryAccount());

					}

					this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(AMOUNT), EJBCommon.FALSE,
							glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

					TOTAL_AMOUNT += AMOUNT;
					ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);

					// add adjust quantity to item location committed quantity if negative

					if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

						double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

						invItemLocation = invAdjustmentLine.getInvItemLocation();

						invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

					}

				}

				// add variance or transfer/debit distribution

				DEBIT = TOTAL_AMOUNT > 0 ? EJBCommon.FALSE : EJBCommon.TRUE;

				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_AMOUNT), EJBCommon.FALSE,
						invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

			} else {

				Iterator i = alList.iterator();

				while (i.hasNext()) {

					InvModAdjustmentLineDetails  mdetails = (InvModAdjustmentLineDetails) i.next();

					LocalInvItemLocation invItemLocation = null;

					try {

						System.out.println("mdetails.getAlLocName(): " + mdetails.getAlLocName());
						System.out.println("mdetails.getAlIiName(): " + mdetails.getAlIiName());
						invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(), mdetails.getAlIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getAlLineNumber()));

					}

					//	start date validation

					if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								invAdjustment.getAdjDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
					}

				}

				Collection invAdjDistributionRecords = invAdjustment.getInvDistributionRecords();

				i = invAdjDistributionRecords.iterator();

				while(i.hasNext()) {

					LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();

					if(distributionRecord.getDrDebit() == 1) {

						ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();

					}

				}

			}

			// generate approval status

			String INV_APPRVL_STATUS = null;

			if (!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if ar invoice approval is enabled

				if (adApproval.getAprEnableInvAdjustment() == EJBCommon.FALSE) {

					INV_APPRVL_STATUS = "N/A";

				} else {

					// check if invoice is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("INV ADJUSTMENT", "REQUESTER", details.getAdjLastModifiedBy(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit()) {

						INV_APPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("INV ADJUSTMENT", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV ADJUSTMENT",
										invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(),
										adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

							}

						} else {

							boolean isApprovalUsersFound = false;
							Iterator i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
											adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV ADJUSTMENT",
												invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(),
												adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers =
										adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV ADJUSTMENT",
												invAdjustment.getAdjCode(), invAdjustment.getAdjDocumentNumber(), invAdjustment.getAdjDate(),
												adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						INV_APPRVL_STATUS = "PENDING";
					}
				}
			}

			if (INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A") && adPreference.getPrfInvGlPostingType().equals("AUTO-POST UPON APPROVAL")) {

				//this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

			}

			// set adjustment approval status

			invAdjustment.setAdjApprovalStatus(INV_APPRVL_STATUS);

			return invAdjustment.getAdjCode();

		} catch (GlobalRecordAlreadyDeletedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;

		}/* catch (GlJREffectiveDateNoPeriodExistException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlobalInventoryDateException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlobalAccountNumberInvalidException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            ctx.setRollbackOnly();
            throw ex;

        } */catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	ctx.setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }


	}
	private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean convertCostByUom");

		LocalInvItemHome invItemHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
			LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

			if (isFromDefault) {

				return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();

			} else {

				return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();

			}



		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}
	private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE,
			LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)

	throws GlobalBranchAccountNumberInvalidException {

		Debug.print("InvItemSyncControllerBean addInvDrEntry");

		LocalAdCompanyHome adCompanyHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			// validate coa

			LocalGlChartOfAccount glChartOfAccount = null;

			try {

				glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
				System.out.print("glChartOfAccount: " +glChartOfAccount);
			} catch(FinderException ex) {

				throw new GlobalBranchAccountNumberInvalidException ();

			}

			// create distribution record

			LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					DR_RVRSL, EJBCommon.FALSE, AD_CMPNY);

			invDistributionRecord.setInvAdjustment(invAdjustment);
			invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

		} catch(GlobalBranchAccountNumberInvalidException ex) {

			throw new GlobalBranchAccountNumberInvalidException ();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private LocalInvAdjustmentLine addInvAlEntry(InvModAdjustmentLineDetails mdetails,
			LocalInvAdjustment invAdjustment, byte AL_VD, Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean addInvAlEntry");

		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;

		// Initialize EJB Home

		try {

			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalInvAdjustmentLine invAdjustmentLine = invAdjustmentLineHome.create(mdetails.getAlUnitCost(),null,null,
					mdetails.getAlAdjustQuantity(),0, AL_VD, AD_CMPNY);

			invAdjustmentLine.setInvAdjustment(invAdjustment);

			LocalInvUnitOfMeasure invUnitOfMeasure =
				invUnitOfMeasureHome.findByUomName(mdetails.getAlUomName(), AD_CMPNY);
			invAdjustmentLine.setInvUnitOfMeasure(invUnitOfMeasure);

			LocalInvItemLocation invItemLocation =
				invItemLocationHome.findByLocNameAndIiName(mdetails.getAlLocName(),
						mdetails.getAlIiName(), AD_CMPNY);
			invAdjustmentLine.setInvItemLocation(invItemLocation);

			return invAdjustmentLine;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	/**
     * @ejb:interface-method
     **/
    public String[] convertBulkToLooseUom(Double qty, String II_NM, String BULK_UOM_NM, String LOOSE_UOM_NM, Integer AD_CMPNY) {
    	String[] results = new String[1];;

    	Debug.print("InvItemSyncControllerBean convertBulkToLooseUom");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemHome invItemHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion =  null;
			LocalInvUnitOfMeasureConversion invDefaultUomConversion = null;

			LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			if (BULK_UOM_NM.equals("")) {
				Iterator i = invItem.getInvUnitOfMeasureConversions().iterator();
				while (i.hasNext()) {
					LocalInvUnitOfMeasureConversion cnv = (LocalInvUnitOfMeasureConversion)i.next();
					if (cnv.getUmcBaseUnit()==(byte)1) {
						invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), cnv.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
						BULK_UOM_NM = cnv.getInvUnitOfMeasure().getUomName();
						break;
					}
				}
			}
			if (LOOSE_UOM_NM.equals("")) invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

			Double convertedQty = EJBCommon.roundIt(qty * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
			System.out.println("convertedQty ==== "+convertedQty);
			results[0] = convertedQty.toString();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);

			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

        return results;
    }

	private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean convertByUomFromAndItemAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
			LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

			return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);

			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private InvModAdjustmentDetails adjRowDecode(String adj)
	{

		//Debug.print("InvItemSyncControllerBean adjRowDecode");

		String separator = "$";
		InvModAdjustmentDetails invAdjustmentDetails = new InvModAdjustmentDetails();
		System.out.println(adj);
		// Remove first $ character
		adj = adj.substring(1);

		// ADJ Code
		int start = 0;
		int nextIndex = adj.indexOf(separator, start);
		int length = nextIndex - start;
		invAdjustmentDetails.setAdjCode(new Integer(adj.substring(start, start + length)));

		// ADJ Type
		start = nextIndex + 1;
		nextIndex = adj.indexOf(separator, start);
		length = nextIndex - start;
		invAdjustmentDetails.setAdjType(adj.substring(start, start + length));

		// Document No
		start = nextIndex + 1;
		nextIndex = adj.indexOf(separator, start);
		length = nextIndex - start;
		//invAdjustmentDetails.setPoDocumentNumber(adj.substring(start, start + length));
		invAdjustmentDetails.setAdjDocumentNumber("");

		// Reference No
		start = nextIndex + 1;
		nextIndex = adj.indexOf(separator, start);
		length = nextIndex - start;
		invAdjustmentDetails.setAdjReferenceNumber(adj.substring(start, start + length));

		// PO Date
		start = nextIndex + 1;
		nextIndex = adj.indexOf(separator, start);
		length = nextIndex - start;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		sdf.setLenient(false);
		try {
			invAdjustmentDetails.setAdjDate(sdf.parse(adj.substring(start, start + length)));
			System.out.println("Date-" + invAdjustmentDetails.getAdjDate());
		} catch (Exception ex) {
			System.out.println("Error On PO Date: " + ex.getMessage());
			//throw ex;
		}

		// Description
		start = nextIndex + 1;
		nextIndex = adj.indexOf(separator, start);
		length = nextIndex - start;
		invAdjustmentDetails.setAdjDescription(adj.substring(start, start + length));

		/************************** Misc Details***************************/

		// Created By
		start = nextIndex + 1;
		nextIndex = adj.indexOf(separator, start);
		length = nextIndex - start;
		invAdjustmentDetails.setAdjCreatedBy(adj.substring(start, start + length));

		// Date Created
		start = nextIndex + 1;
		nextIndex = adj.indexOf(separator, start);
		length = nextIndex - start;
		try {
			invAdjustmentDetails.setAdjDateCreated(sdf.parse(adj.substring(start, start + length)));
			System.out.println("Date Created-" + invAdjustmentDetails.getAdjDateCreated());
		} catch (Exception ex) {
			System.out.println("Error On Date Created");
			//throw ex;
		}

		// Last Modified By
		start = nextIndex + 1;
		nextIndex = adj.indexOf(separator, start);
		length = nextIndex - start;
		invAdjustmentDetails.setAdjLastModifiedBy(adj.substring(start, start + length));

		// Date Last Modified
		start = nextIndex + 1;
		nextIndex = adj.indexOf(separator, start);
		length = nextIndex - start;
		try {
			invAdjustmentDetails.setAdjDateLastModified(sdf.parse(adj.substring(start, start + length)));
			System.out.println("Date Last Modified-" + invAdjustmentDetails.getAdjDateLastModified());
		} catch (Exception ex) {
			System.out.println("Error On Date Last Modified");
			//throw ex;
		}

		// COA ACCOUNT NO
		start = nextIndex + 1;
		nextIndex = adj.indexOf(separator, start);
		length = nextIndex - start;
		invAdjustmentDetails.setAdjCoaAccountNumber(adj.substring(start, start + length));

		// end separator
		start = nextIndex + 1;
		nextIndex = adj.indexOf(separator, start);
		length = nextIndex - start;

		String lineSeparator = "~";

		// begin lineSeparator
		start = nextIndex + 1;
		nextIndex = adj.indexOf(lineSeparator, start);
		length = nextIndex - start;

		invAdjustmentDetails.setAdjAlList(new ArrayList());

		while (true) {
			InvModAdjustmentLineDetails invModAdjustmentLineDetails = new InvModAdjustmentLineDetails();

			// begin separator
			start = nextIndex + 1;
			nextIndex = adj.indexOf(separator, start);
			length = nextIndex - start;

			// Unit Cost
			start = nextIndex + 1;
			nextIndex = adj.indexOf(separator, start);
			length = nextIndex - start;
			invModAdjustmentLineDetails.setAlUnitCost((double)Double.parseDouble(adj.substring(start, start + length)));
			System.out.println("Unit Cost: " + invModAdjustmentLineDetails.getAlUnitCost());

			// Adj Quantity
			start = nextIndex + 1;
			nextIndex = adj.indexOf(separator, start);
			length = nextIndex - start;
			invModAdjustmentLineDetails.setAlAdjustQuantity((double)Double.parseDouble(adj.substring(start, start + length)));

			// Unit of Measure
			start = nextIndex + 1;
			nextIndex = adj.indexOf(separator, start);
			length = nextIndex - start;
			invModAdjustmentLineDetails.setAlUomName(adj.substring(start, start + length));

			// Item
			start = nextIndex + 1;
			nextIndex = adj.indexOf(separator, start);
			length = nextIndex - start;
			invModAdjustmentLineDetails.setAlIiName(adj.substring(start, start + length));

			// Location
			start = nextIndex + 1;
			nextIndex = adj.indexOf(separator, start);
			length = nextIndex - start;
			invModAdjustmentLineDetails.setAlLocName(adj.substring(start, start + length));

			System.out.println("item Location: " + invModAdjustmentLineDetails.getAlIiName() + " " + invModAdjustmentLineDetails.getAlLocName());

			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = adj.indexOf(lineSeparator, start);
			length = nextIndex - start;

			invAdjustmentDetails.getAdjAlList().add(invModAdjustmentLineDetails);

			int tempStart = nextIndex + 1;
			if (adj.indexOf(separator, tempStart) == -1) break;

		}

		return invAdjustmentDetails;


	}

	/**
	 * @ejb:interface-method
	 * */
	public int setApReceivingItem(String[] RR, String BR_BRNCH_CODE, Integer AD_CMPNY)
	{
		LocalAdBranchHome adBranchHome = null;
		try {

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch(Exception e) {

			return 0;
		}

		Integer AD_BRNCH = new Integer(0);
		try {
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();

		} catch(Exception e) {

			return 0;
		}

		//LocalApPurchaseOrder
		for (int i = 0; i < RR.length; i++) {

			ApModPurchaseOrderDetails  apModPurchaseOrderDetails = poRowDecode(RR[i]);
			try{

				apModPurchaseOrderDetails.setPoVoid(EJBCommon.FALSE);
				apModPurchaseOrderDetails.setPoPosted(EJBCommon.FALSE);
				apModPurchaseOrderDetails.setPoTcType("ITEMS");
				apModPurchaseOrderDetails.setPoCode(null);

				System.out.println(apModPurchaseOrderDetails.getPoSplSupplierCode());
				Integer purchaseOrderCode = this.saveApPoEntry(apModPurchaseOrderDetails, apModPurchaseOrderDetails.getPoPytName(),
						apModPurchaseOrderDetails.getPoTcName(), apModPurchaseOrderDetails.getPoFcName(), apModPurchaseOrderDetails.getPoSplSupplierCode(), apModPurchaseOrderDetails.getPoPlList(), true,
						AD_BRNCH, AD_CMPNY);

			}catch(Exception e) {
				e.printStackTrace();
				ctx.setRollbackOnly();
				return 0;
			}
		}

		return 1;
	}

	private Integer saveApPoEntry(com.util.ApPurchaseOrderDetails details, String PYT_NM, String TC_NM,
			String FC_NM, String SPL_SPPLR_CODE, ArrayList plList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalRecordAlreadyDeletedException,
			GlobalDocumentNumberNotUniqueException,
			GlobalConversionDateNotExistException,
			GlobalPaymentTermInvalidException,
			GlobalTransactionAlreadyPendingException,
			GlobalTransactionAlreadyPostedException,
			GlobalTransactionAlreadyVoidException,
			GlobalInvItemLocationNotFoundException,
			GlJREffectiveDateNoPeriodExistException,
			GlJREffectiveDatePeriodClosedException,
			GlobalJournalNotBalanceException,
			GlobalTransactionAlreadyLockedException,
			GlobalInventoryDateException,
			GlobalTransactionAlreadyVoidPostedException,
			GlobalBranchAccountNumberInvalidException,
			AdPRFCoaGlVarianceAccountNotFoundException {

		Debug.print("InvItemSyncControllerBean saveApPoVliEntry");

		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

		LocalApPurchaseOrder apPurchaseOrder = null;

		// Initialize EJB Home

		try {

			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if receiving item is already deleted

			try {

				if (details.getPoCode() != null) {

					apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(details.getPoCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if receiving item is already posted, void, approved or pending

			if (details.getPoCode() != null) {


				if (apPurchaseOrder.getPoApprovalStatus() != null) {

					if (apPurchaseOrder.getPoApprovalStatus().equals("APPROVED") ||
							apPurchaseOrder.getPoApprovalStatus().equals("N/A")) {

						throw new GlobalTransactionAlreadyApprovedException();


					} else if (apPurchaseOrder.getPoApprovalStatus().equals("PENDING")) {

						throw new GlobalTransactionAlreadyPendingException();

					}

				}

				if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyPostedException();

				} else if (apPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyVoidException();

				}

			}

			// receiving item void

			if (details.getPoCode() != null && details.getPoVoid() == EJBCommon.TRUE &&
					apPurchaseOrder.getPoPosted() == EJBCommon.FALSE) {

				apPurchaseOrder.setPoVoid(EJBCommon.TRUE);
				apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
				apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());

				return apPurchaseOrder.getPoCode();

			}

			// validate if document number is unique document number is automatic then set next sequence

			try {

				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

				if (details.getPoCode() == null) {

					try {

						adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP RECEIVING ITEM", AD_CMPNY);

					} catch (FinderException ex) { }

					try {

						adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) { }


					LocalApPurchaseOrder apExistingReceivingItem = null;

					try {

						apExistingReceivingItem = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
								details.getPoDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if (apExistingReceivingItem != null) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
							(details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

						while (true) {

							if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									details.setPoDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									details.setPoDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}

					}

				} else {

					LocalApPurchaseOrder apExistingReceivingItem = null;

					try {

						apExistingReceivingItem = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
								details.getPoDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if (apExistingReceivingItem != null &&
							!apExistingReceivingItem.getPoCode().equals(details.getPoCode())) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (apPurchaseOrder.getPoDocumentNumber() != details.getPoDocumentNumber() &&
							(details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

						details.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());

					}

				}

			} catch (GlobalDocumentNumberNotUniqueException ex) {

				ctx.setRollbackOnly();
				throw ex;

			} catch (Exception ex) {

				Debug.printStackTrace(ex);
				ctx.setRollbackOnly();
				throw new EJBException(ex.getMessage());

			}

			// validate if conversion date exists

			try {

				if (details.getPoConversionDate() != null) {

					LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
					LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

					if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
									details.getPoConversionDate(), AD_CMPNY);

					} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(
									adCompany.getGlFunctionalCurrency().getFcCode(), details.getPoConversionDate(), AD_CMPNY);

					}

				}

			} catch (FinderException ex) {

				throw new GlobalConversionDateNotExistException();

			}

			// validate if payment term has at least one payment schedule

			if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

				throw new GlobalPaymentTermInvalidException();

			}

			// lock purchase order

			LocalApPurchaseOrder apExistingPurchaseOrder = null;

			if (details.getPoType().equals("PO MATCHED")) {

				try {

					apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
							details.getPoRcvPoNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

					if (apPurchaseOrder == null || (apPurchaseOrder != null
							&& !apPurchaseOrder.getPoRcvPoNumber().equals(details.getPoRcvPoNumber()))) {

						if (apExistingPurchaseOrder.getPoLock() == EJBCommon.TRUE) {

							throw new GlobalTransactionAlreadyLockedException();

						}

						//apExistingPurchaseOrder.setPoLock(EJBCommon.TRUE);

						if (apPurchaseOrder != null && apPurchaseOrder.getPoRcvPoNumber() != null){

							LocalApPurchaseOrder apPreviousPO = null;

							apPreviousPO = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
									apPurchaseOrder.getPoRcvPoNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

							apPreviousPO.setPoLock(EJBCommon.FALSE);

						}

						if (apExistingPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

							throw new GlobalTransactionAlreadyVoidPostedException();

						}

					}

				} catch (FinderException ex) {

				}

			}

			// used in checking if receiving item should re-generate distribution records and re-calculate taxes
			boolean isRecalculate = true;

			// create receiving item

			boolean newPurchaseOrder = false;

			if (details.getPoCode() == null) {

				apPurchaseOrder = apPurchaseOrderHome.create(EJBCommon.TRUE, details.getPoType(),
						details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(), details.getPoReferenceNumber(),
						details.getPoRcvPoNumber(), details.getPoDescription(), null, null,
						details.getPoConversionDate(), details.getPoConversionRate(), details.getPoTotalAmount(), EJBCommon.FALSE, EJBCommon.FALSE,null,
						null, EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(),
						details.getPoLastModifiedBy(), details.getPoDateLastModified(),
						null, null, null, null, EJBCommon.FALSE,
						0d,0d,0d,0d,0d,
						AD_BRNCH,AD_CMPNY);


				/*.create(EJBCommon.TRUE, details.getPoType(),
						details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(), details.getPoReferenceNumber(),
						details.getPoRcvPoNumber(), details.getPoDescription(), null, null,
						details.getPoConversionDate(), details.getPoConversionRate(), details.getPoTotalAmount(), EJBCommon.FALSE, EJBCommon.FALSE,null,
						null, EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(),
						details.getPoLastModifiedBy(), details.getPoDateLastModified(),
						null, null, null, null, EJBCommon.FALSE,
						0d, null,null,null, null,null,null,0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d,
						null,null,null,null,null,null,null,null,null,
						AD_BRNCH, AD_CMPNY);*/

				newPurchaseOrder = true;

			} else {

				// check if critical fields are changed

				if (!apPurchaseOrder.getApTaxCode().getTcName().equals(TC_NM) ||
						!apPurchaseOrder.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CODE) ||
						!apPurchaseOrder.getAdPaymentTerm().getPytName().equals(PYT_NM) ||
						plList.size() != apPurchaseOrder.getApPurchaseOrderLines().size() ||
						!(apPurchaseOrder.getPoDate().equals(details.getPoDate()))) {

					isRecalculate = true;

				} else if (plList.size() == apPurchaseOrder.getApPurchaseOrderLines().size()) {

					Iterator ilIter = apPurchaseOrder.getApPurchaseOrderLines().iterator();
					Iterator ilListIter = plList.iterator();

					while (ilIter.hasNext()) {

						LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();
						ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)ilListIter.next();

						if (!apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getPlIiName()) ||
								!apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getPlLocName()) ||
								!apPurchaseOrderLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getPlUomName()) ||
								apPurchaseOrderLine.getPlQuantity() != mdetails.getPlQuantity() ||
								apPurchaseOrderLine.getPlUnitCost() != mdetails.getPlUnitCost()||
								apPurchaseOrderLine.getPlTotalDiscount() != mdetails.getPlTotalDiscount()) {

							isRecalculate = true;
							break;

						}

						isRecalculate = false;

					}

				} else {

					isRecalculate = false;

				}

				apPurchaseOrder.setPoReceiving(EJBCommon.TRUE);
				apPurchaseOrder.setPoDescription(details.getPoDescription());
				apPurchaseOrder.setPoRcvPoNumber(details.getPoRcvPoNumber());
				apPurchaseOrder.setPoType(details.getPoType());
				apPurchaseOrder.setPoDate(details.getPoDate());
				apPurchaseOrder.setPoDocumentNumber(details.getPoDocumentNumber());
				apPurchaseOrder.setPoReferenceNumber(details.getPoReferenceNumber());
				apPurchaseOrder.setPoConversionDate(details.getPoConversionDate());
				apPurchaseOrder.setPoConversionRate(details.getPoConversionRate());
				apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
				apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());
				apPurchaseOrder.setPoReasonForRejection(null);

			}

			LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
			apPurchaseOrder.setAdPaymentTerm(adPaymentTerm);

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			apPurchaseOrder.setApTaxCode(apTaxCode);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			apPurchaseOrder.setGlFunctionalCurrency(glFunctionalCurrency);

			System.out.println(SPL_SPPLR_CODE + " "  + AD_CMPNY);
			LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
			apPurchaseOrder.setApSupplier(apSupplier);

			double TOTAL_AMOUNT = 0;

			if (isRecalculate) {

				// remove all receiving item line

				Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

				Iterator i = apPurchaseOrderLines.iterator();

				while (i.hasNext()) {

					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();

					i.remove();

					apPurchaseOrderLine.remove();

				}

				// remove all distribution records

				Collection apDistributionRecords = apPurchaseOrder.getApDistributionRecords();

				i = apDistributionRecords.iterator();

				while (i.hasNext()) {

					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

					i.remove();

					apDistributionRecord.remove();

				}

				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

				// add new receiving item line and distribution record

				double TOTAL_TAX = 0d;
				double TOTAL_LINE = 0d;

				i = plList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) i.next();

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(mPlDetails.getPlLocName(), mPlDetails.getPlIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPlDetails.getPlLine()));

					}

					//	start date validation

					if(mPlDetails.getPlQuantity() > 0) {

						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								apPurchaseOrder.getPoDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

					}


					LocalApPurchaseOrderLine apPurchaseOrderLine = this.addApPlEntry(mPlDetails, apPurchaseOrder, invItemLocation, newPurchaseOrder, AD_CMPNY);

					if(mPlDetails.getPlQuantity() == 0) continue;

					// add inventory distributions

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try {
						adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(apPurchaseOrderLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
					} catch(FinderException ex) {

					}

					if(adBranchItemLocation != null) {

						// Use AdBranchItemLocation Coa Account
						this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
								"INVENTORY", EJBCommon.TRUE, apPurchaseOrderLine.getPlAmount(),
								adBranchItemLocation.getBilCoaGlInventoryAccount(), apPurchaseOrder, AD_BRNCH, AD_CMPNY);



					} else {

						// Use default account
						this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
								"INVENTORY", EJBCommon.TRUE, apPurchaseOrderLine.getPlAmount(),
								apPurchaseOrderLine.getInvItemLocation().getIlGlCoaInventoryAccount(), apPurchaseOrder, AD_BRNCH, AD_CMPNY);

					}

					TOTAL_LINE += apPurchaseOrderLine.getPlAmount();
					TOTAL_TAX += apPurchaseOrderLine.getPlTaxAmount();

				}

				// add tax distribution if necessary

				if (!apTaxCode.getTcType().equals("NONE") &&
						!apTaxCode.getTcType().equals("EXEMPT")) {

					if (adPreference.getPrfApUseAccruedVat() == EJBCommon.FALSE) {

						this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
								"TAX", EJBCommon.TRUE, TOTAL_TAX, apTaxCode.getGlChartOfAccount().getCoaCode(),
								apPurchaseOrder, AD_BRNCH, AD_CMPNY);

					} else {

						this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
								"ACC TAX", EJBCommon.TRUE, TOTAL_TAX, adPreference.getPrfApGlCoaAccruedVatAccount(),
								apPurchaseOrder, AD_BRNCH, AD_CMPNY);

					}

				}

				TOTAL_AMOUNT = TOTAL_LINE + TOTAL_TAX;

			} else {

				Iterator i = plList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) i.next();

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(mPlDetails.getPlLocName(), mPlDetails.getPlIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPlDetails.getPlLine()));

					}

					//	start date validation

					Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
							apPurchaseOrder.getPoDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
					if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

				}

				Collection apDistributionRecords = apPurchaseOrder.getApDistributionRecords();

				i = apDistributionRecords.iterator();

				while(i.hasNext()) {

					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

					if(apDistributionRecord.getDrDebit() == 0) {

						TOTAL_AMOUNT += apDistributionRecord.getDrAmount();

					}
				}

			}

			// generate approval status

			String PO_APPRVL_STATUS = null;

			if (!isDraft) {

				PO_APPRVL_STATUS = "N/A";

				// release purchase order lock

				if (details.getPoType().equals("PO MATCHED")) {

					apExistingPurchaseOrder.setPoLock(EJBCommon.FALSE);

				}

			}

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			if (PO_APPRVL_STATUS != null && PO_APPRVL_STATUS.equals("N/A")) {

				//this.executeApPoPost(apPurchaseOrder.getPoCode(), apPurchaseOrder.getPoLastModifiedBy(), AD_BRNCH, AD_CMPNY);

			}

			// set receiving item approval status

			apPurchaseOrder.setPoApprovalStatus(PO_APPRVL_STATUS);

			return apPurchaseOrder.getPoCode();


		} catch (GlobalRecordAlreadyDeletedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalConversionDateNotExistException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalPaymentTermInvalidException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;

        } catch (GlobalTransactionAlreadyLockedException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlobalInventoryDateException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } /*catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            ctx.setRollbackOnly();
            throw ex;

        } */catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	ctx.setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

	/**
	 * @ejb:interface-method
	 * */
	public int setApReceivingItemPoCondition(String[] RR, String BR_BRNCH_CODE, Integer AD_CMPNY)
	{
		LocalAdBranchHome adBranchHome = null;
		try {

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch(Exception e) {

			return 0;
		}

		Integer AD_BRNCH = new Integer(0);
		try {
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();

		} catch(Exception e) {

			return 0;
		}

		//LocalApPurchaseOrder
		for (int i = 0; i < RR.length; i++) {

			ApModPurchaseOrderDetails  apModPurchaseOrderDetails = poRowDecode(RR[i]);
			try{

				apModPurchaseOrderDetails.setPoVoid(EJBCommon.FALSE);
				apModPurchaseOrderDetails.setPoPosted(EJBCommon.FALSE);
				apModPurchaseOrderDetails.setPoTcType("ITEMS");
				apModPurchaseOrderDetails.setPoCode(null);

				System.out.println(apModPurchaseOrderDetails.getPoSplSupplierCode());
				Integer purchaseOrderCode = this.saveApPoEntry2(apModPurchaseOrderDetails, apModPurchaseOrderDetails.getPoPytName(),
						apModPurchaseOrderDetails.getPoTcName(), apModPurchaseOrderDetails.getPoFcName(), apModPurchaseOrderDetails.getPoSplSupplierCode(), apModPurchaseOrderDetails.getPoPlList(), true,
						AD_BRNCH, AD_CMPNY);

			}catch(Exception e) {
				e.printStackTrace();
				ctx.setRollbackOnly();
				return 0;
			}
		}

		return 1;
	}

	private Integer saveApPoEntry2(com.util.ApPurchaseOrderDetails details, String PYT_NM, String TC_NM,
			String FC_NM, String SPL_SPPLR_CODE, ArrayList plList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
			GlobalRecordAlreadyDeletedException,
			GlobalDocumentNumberNotUniqueException,
			GlobalConversionDateNotExistException,
			GlobalPaymentTermInvalidException,
			GlobalTransactionAlreadyPendingException,
			GlobalTransactionAlreadyPostedException,
			GlobalTransactionAlreadyVoidException,
			GlobalInvItemLocationNotFoundException,
			GlJREffectiveDateNoPeriodExistException,
			GlJREffectiveDatePeriodClosedException,
			GlobalJournalNotBalanceException,
			GlobalTransactionAlreadyLockedException,
			GlobalInventoryDateException,
			GlobalTransactionAlreadyVoidPostedException,
			GlobalBranchAccountNumberInvalidException,
			AdPRFCoaGlVarianceAccountNotFoundException {

		Debug.print("InvItemSyncControllerBean saveApPoVliEntry");

		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalApWithholdingTaxCodeHome apWithholdingTaxCodeHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;

		LocalApPurchaseOrder apPurchaseOrder = null;

		// Initialize EJB Home

		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		// Initialize EJB Home



		try {
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if receiving item is already deleted

			try {

				if (details.getPoCode() != null) {

					apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(details.getPoCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if receiving item is already posted, void, approved or pending

			if (details.getPoCode() != null) {


				if (apPurchaseOrder.getPoApprovalStatus() != null) {

					if (apPurchaseOrder.getPoApprovalStatus().equals("APPROVED") ||
							apPurchaseOrder.getPoApprovalStatus().equals("N/A")) {

						throw new GlobalTransactionAlreadyApprovedException();


					} else if (apPurchaseOrder.getPoApprovalStatus().equals("PENDING")) {

						throw new GlobalTransactionAlreadyPendingException();

					}

				}

				if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyPostedException();

				} else if (apPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyVoidException();

				}

			}

			// receiving item void

			if (details.getPoCode() != null && details.getPoVoid() == EJBCommon.TRUE &&
					apPurchaseOrder.getPoPosted() == EJBCommon.FALSE) {

				apPurchaseOrder.setPoVoid(EJBCommon.TRUE);
				apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
				apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());

				return apPurchaseOrder.getPoCode();

			}

			// validate if document number is unique document number is automatic then set next sequence

			try {

				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

				if (details.getPoCode() == null) {

					try {

						adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP RECEIVING ITEM", AD_CMPNY);

					} catch (FinderException ex) { }

					try {

						adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) { }


					LocalApPurchaseOrder apExistingReceivingItem = null;

					try {

						apExistingReceivingItem = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
								details.getPoDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if (apExistingReceivingItem != null) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
							(details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

						while (true) {

							if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									details.setPoDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									details.setPoDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}

					}

				} else {

					LocalApPurchaseOrder apExistingReceivingItem = null;

					try {

						apExistingReceivingItem = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
								details.getPoDocumentNumber(), EJBCommon.TRUE, AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {

					}

					if (apExistingReceivingItem != null &&
							!apExistingReceivingItem.getPoCode().equals(details.getPoCode())) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (apPurchaseOrder.getPoDocumentNumber() != details.getPoDocumentNumber() &&
							(details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

						details.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());

					}

				}

			} catch (GlobalDocumentNumberNotUniqueException ex) {

				ctx.setRollbackOnly();
				throw ex;

			} catch (Exception ex) {

				Debug.printStackTrace(ex);
				ctx.setRollbackOnly();
				throw new EJBException(ex.getMessage());

			}

			// validate if document number is unique document number is automatic then set next sequence. PO
			String poDocNum = "";
			System.out.println("details.getPoReferenceNumber(): " + details.getPoReferenceNumber());
			System.out.println("getPoDocumentNumber(): " + details.getPoDocumentNumber().trim());
			if(details.getPoReferenceNumber()!="" && details.getPoReferenceNumber()!=null){
				try {

				    LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		 			LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

					if (poDocNum == "") {
					    System.out.println("Enter");
					    try {

					        adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP PURCHASE ORDER", AD_CMPNY);

					    } catch(FinderException ex) { }

					    try {

		 					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

		 				} catch (FinderException ex) { }

						LocalApPurchaseOrder apExistingPurchaseOrder = null;

						try {

							apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(
									poDocNum, AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {
						}

						if (apExistingPurchaseOrder != null) {

							throw new GlobalDocumentNumberNotUniqueException();

						}

						if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
								(poDocNum == null || poDocNum== "")) {

							while (true) {

							    if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

									try {

										apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
										adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

									} catch (FinderException ex) {

										poDocNum=adDocumentSequenceAssignment.getDsaNextSequence();
										adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
										break;

									}

							    } else {

							        try {

										apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
										adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

									} catch (FinderException ex) {

										poDocNum=adBranchDocumentSequenceAssignment.getBdsNextSequence();
										adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
										break;

									}

							    }

							}

						}

					} else {

					    LocalApPurchaseOrder apExistingPurchaseOrder = null;

						try {

							apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(
									details.getPoReferenceNumber().trim(), AD_BRNCH, AD_CMPNY);

						} catch (FinderException ex) {
						}

						if (apExistingPurchaseOrder != null &&
								!apExistingPurchaseOrder.getPoCode().equals(details.getPoCode())) {

							throw new GlobalDocumentNumberNotUniqueException();

						}

						if (apPurchaseOrder.getPoDocumentNumber() != details.getPoReferenceNumber().trim() &&
								(details.getPoReferenceNumber().trim() == null || details.getPoReferenceNumber().trim().length() == 0)) {

							poDocNum=apPurchaseOrder.getPoReferenceNumber();

						}

					}

				} catch (GlobalDocumentNumberNotUniqueException ex) {

					ctx.setRollbackOnly();
					throw ex;

				} catch (Exception ex) {

					Debug.printStackTrace(ex);
					ctx.setRollbackOnly();
					throw new EJBException(ex.getMessage());

				}
			}

			System.out.println("poDocNum: " + poDocNum);
			// validate if conversion date exists

			try {

				if (details.getPoConversionDate() != null) {

					LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
					LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

					if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
									details.getPoConversionDate(), AD_CMPNY);

					} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(
									adCompany.getGlFunctionalCurrency().getFcCode(), details.getPoConversionDate(), AD_CMPNY);

					}

				}

			} catch (FinderException ex) {

				throw new GlobalConversionDateNotExistException();

			}

			// validate if payment term has at least one payment schedule

			if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

				throw new GlobalPaymentTermInvalidException();

			}

			// lock purchase order

			LocalApPurchaseOrder apExistingPurchaseOrder = null;

			if (details.getPoType().equals("PO MATCHED")) {

				try {

					apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
							details.getPoRcvPoNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

					if (apPurchaseOrder == null || (apPurchaseOrder != null
							&& !apPurchaseOrder.getPoRcvPoNumber().equals(details.getPoRcvPoNumber()))) {

						if (apExistingPurchaseOrder.getPoLock() == EJBCommon.TRUE) {

							throw new GlobalTransactionAlreadyLockedException();

						}

						//apExistingPurchaseOrder.setPoLock(EJBCommon.TRUE);

						if (apPurchaseOrder != null && apPurchaseOrder.getPoRcvPoNumber() != null){

							LocalApPurchaseOrder apPreviousPO = null;

							apPreviousPO = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
									apPurchaseOrder.getPoRcvPoNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

							apPreviousPO.setPoLock(EJBCommon.FALSE);

						}

						if (apExistingPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

							throw new GlobalTransactionAlreadyVoidPostedException();

						}

					}

				} catch (FinderException ex) {

				}

				LocalApPurchaseOrder apCheckPurchaseOrder = null;
				System.out.println("HERE 1");
				try {

					apCheckPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndPoReceivingAndBrCode(
							details.getPoReferenceNumber(), EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
					System.out.println("HERE 2");
					if(apCheckPurchaseOrder!=null){
						System.out.println("HERE 3");
						LocalApPurchaseOrder apPurchaseOrderNew = null;
						LocalApPurchaseOrderLine apPurchaseOrderLineNew = null;
						if (plList.size() == apCheckPurchaseOrder.getApPurchaseOrderLines().size()) {
							System.out.println("HERE 4");
							Iterator ilIter = apCheckPurchaseOrder.getApPurchaseOrderLines().iterator();
							Iterator ilListIter = plList.iterator();

							int asd = 1;

							while (ilIter.hasNext()) {
								double quantity=0;
								double quantity2=0;
								LocalInvItemLocation invItemLocationNew = null;
								LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();
								ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)ilListIter.next();

								System.out.println("HERE 5");
								if (!apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getPlIiName()) ||
										!apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getPlLocName()) ||
										!apPurchaseOrderLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getPlUomName()) ||
										apPurchaseOrderLine.getPlQuantity() != mdetails.getPlQuantity()) {

									System.out.println("HERE 6");
									try {

										invItemLocationNew = invItemLocationHome.findByLocNameAndIiName(mdetails.getPlLocName(), mdetails.getPlIiName(), AD_CMPNY);

									} catch (FinderException ex) {

										throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getPlLine()));

									}
									System.out.println("HERE 7");
									if(asd==1){
										System.out.println("poDocNum: " + poDocNum);
										apPurchaseOrderNew = apPurchaseOrderHome.create(EJBCommon.FALSE, details.getPoType(),
												details.getPoDate(), details.getPoDeliveryPeriod(), poDocNum, "Additional PO for "+ details.getPoDocumentNumber(),
												details.getPoRcvPoNumber(), details.getPoDescription(), null, null,
												details.getPoConversionDate(), details.getPoConversionRate(), details.getPoTotalAmount(), EJBCommon.FALSE, EJBCommon.FALSE,null,
												null, EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(),
												details.getPoLastModifiedBy(), details.getPoDateLastModified(),
												null, null, null, null, EJBCommon.FALSE,
												0d,0d,0d,0d,0d,
												AD_BRNCH,AD_CMPNY);

										/*.create(EJBCommon.FALSE, details.getPoType(),
												details.getPoDate(), details.getPoDeliveryPeriod(), poDocNum, "Additional PO for "+ details.getPoDocumentNumber(),
												details.getPoRcvPoNumber(), details.getPoDescription(), null, null,
												details.getPoConversionDate(), details.getPoConversionRate(), details.getPoTotalAmount(), EJBCommon.FALSE, EJBCommon.FALSE,null,
												null, EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(),
												details.getPoLastModifiedBy(), details.getPoDateLastModified(),
												null, null, null, null, EJBCommon.FALSE,
												0d,null,null,null,null,null,null, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d,
												null,null,null,null,null,null,null,null,null,
												AD_BRNCH, AD_CMPNY);*/
										asd++;

										LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
										apPurchaseOrderNew.setAdPaymentTerm(adPaymentTerm);

										LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
										apPurchaseOrderNew.setApTaxCode(apTaxCode);

										LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
										apPurchaseOrderNew.setGlFunctionalCurrency(glFunctionalCurrency);

										System.out.println(SPL_SPPLR_CODE + " "  + AD_CMPNY);
										LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
										apPurchaseOrderNew.setApSupplier(apSupplier);
										System.out.println("ADD HERE");
									}
									System.out.println("HERE 8");

									quantity=mdetails.getPlQuantity()-apPurchaseOrderLine.getPlQuantity();

									quantity2=mdetails.getPlQuantity();
									mdetails.setPlQuantity(quantity);

									apPurchaseOrderLineNew = this.addApPlEntry(mdetails, apPurchaseOrderNew, invItemLocationNew, true, AD_CMPNY);
									mdetails.setPlQuantity(apPurchaseOrderLine.getPlQuantity());
									//if(mdetails.getPlQuantity()!=quantity){
									//	System.out.println("/***/ "+mdetails.getPlQuantity()+quantity);
									//	mdetails.setPlQuantity(mdetails.getPlQuantity()+quantity);
									//}*/


									System.out.println("HERE 9");
								}


							}
						}
					}


				} catch (FinderException ex) {

				}

			}

			// used in checking if receiving item should re-generate distribution records and re-calculate taxes
			boolean isRecalculate = true;

			// create receiving item

			boolean newPurchaseOrder = false;

			if (details.getPoCode() == null) {

				apPurchaseOrder = apPurchaseOrderHome.create(EJBCommon.TRUE, details.getPoType(),
						details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(), details.getPoReferenceNumber(),
						details.getPoRcvPoNumber(), details.getPoDescription(), null, null,
						details.getPoConversionDate(), details.getPoConversionRate(), details.getPoTotalAmount(), EJBCommon.FALSE, EJBCommon.FALSE, null,
						null, EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(),
						details.getPoLastModifiedBy(), details.getPoDateLastModified(),
						null, null, null, null, EJBCommon.FALSE,
						0d,0d,0d,0d,0d,
						AD_BRNCH, AD_CMPNY);


				/*.create(EJBCommon.TRUE, details.getPoType(),
						details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(), details.getPoReferenceNumber(),
						details.getPoRcvPoNumber(), details.getPoDescription(), null, null,
						details.getPoConversionDate(), details.getPoConversionRate(), details.getPoTotalAmount(), EJBCommon.FALSE, EJBCommon.FALSE, null,
						null, EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(),
						details.getPoLastModifiedBy(), details.getPoDateLastModified(),
						null, null, null, null, EJBCommon.FALSE,
						0d,null,null,null,null,null,null, 0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d,
						null,null,null,null,null,null,null,null,null,
						AD_BRNCH, AD_CMPNY);*/

				newPurchaseOrder = true;

			} else {

				// check if critical fields are changed

				if (!apPurchaseOrder.getApTaxCode().getTcName().equals(TC_NM) ||
						!apPurchaseOrder.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CODE) ||
						!apPurchaseOrder.getAdPaymentTerm().getPytName().equals(PYT_NM) ||
						plList.size() != apPurchaseOrder.getApPurchaseOrderLines().size() ||
						!(apPurchaseOrder.getPoDate().equals(details.getPoDate()))) {

					isRecalculate = true;

				} else if (plList.size() == apPurchaseOrder.getApPurchaseOrderLines().size()) {

					Iterator ilIter = apPurchaseOrder.getApPurchaseOrderLines().iterator();
					Iterator ilListIter = plList.iterator();

					while (ilIter.hasNext()) {

						LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();
						ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)ilListIter.next();

						if (!apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getPlIiName()) ||
								!apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getPlLocName()) ||
								!apPurchaseOrderLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getPlUomName()) ||
								apPurchaseOrderLine.getPlQuantity() != mdetails.getPlQuantity() ||
								apPurchaseOrderLine.getPlUnitCost() != mdetails.getPlUnitCost()||
								apPurchaseOrderLine.getPlTotalDiscount() != mdetails.getPlTotalDiscount()) {

							isRecalculate = true;
							break;

						}

						isRecalculate = false;

					}

				} else {

					isRecalculate = false;

				}

				apPurchaseOrder.setPoReceiving(EJBCommon.TRUE);
				apPurchaseOrder.setPoDescription(details.getPoDescription());
				apPurchaseOrder.setPoRcvPoNumber(details.getPoRcvPoNumber());
				apPurchaseOrder.setPoType(details.getPoType());
				apPurchaseOrder.setPoDate(details.getPoDate());
				apPurchaseOrder.setPoDocumentNumber(details.getPoDocumentNumber());
				apPurchaseOrder.setPoReferenceNumber(details.getPoReferenceNumber());
				apPurchaseOrder.setPoConversionDate(details.getPoConversionDate());
				apPurchaseOrder.setPoConversionRate(details.getPoConversionRate());
				apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
				apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());
				apPurchaseOrder.setPoReasonForRejection(null);

			}

			LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
			apPurchaseOrder.setAdPaymentTerm(adPaymentTerm);

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			apPurchaseOrder.setApTaxCode(apTaxCode);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			apPurchaseOrder.setGlFunctionalCurrency(glFunctionalCurrency);

			System.out.println(SPL_SPPLR_CODE + " "  + AD_CMPNY);
			LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
			apPurchaseOrder.setApSupplier(apSupplier);

			double TOTAL_AMOUNT = 0;

			if (isRecalculate) {

				// remove all receiving item line

				Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

				Iterator i = apPurchaseOrderLines.iterator();

				while (i.hasNext()) {

					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();

					i.remove();

					apPurchaseOrderLine.remove();

				}

				// remove all distribution records

				Collection apDistributionRecords = apPurchaseOrder.getApDistributionRecords();

				i = apDistributionRecords.iterator();

				while (i.hasNext()) {

					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

					i.remove();

					apDistributionRecord.remove();

				}

				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

				// add new receiving item line and distribution record

				double TOTAL_TAX = 0d;
				double TOTAL_LINE = 0d;

				i = plList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) i.next();

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(mPlDetails.getPlLocName(), mPlDetails.getPlIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPlDetails.getPlLine()));

					}

					//	start date validation

					if(mPlDetails.getPlQuantity() > 0) {

						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								apPurchaseOrder.getPoDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

					}


					LocalApPurchaseOrderLine apPurchaseOrderLine = this.addApPlEntry(mPlDetails, apPurchaseOrder, invItemLocation, newPurchaseOrder, AD_CMPNY);

					if(mPlDetails.getPlQuantity() == 0) continue;

					// add inventory distributions

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try {
						adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(apPurchaseOrderLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
					} catch(FinderException ex) {

					}

					if(adBranchItemLocation != null) {

						// Use AdBranchItemLocation Coa Account
						this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
								"INVENTORY", EJBCommon.TRUE, apPurchaseOrderLine.getPlAmount(),
								adBranchItemLocation.getBilCoaGlInventoryAccount(), apPurchaseOrder, AD_BRNCH, AD_CMPNY);


					} else {

						// Use default account
						this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
								"INVENTORY", EJBCommon.TRUE, apPurchaseOrderLine.getPlAmount(),
								apPurchaseOrderLine.getInvItemLocation().getIlGlCoaInventoryAccount(), apPurchaseOrder, AD_BRNCH, AD_CMPNY);


					}

					TOTAL_LINE += apPurchaseOrderLine.getPlAmount();
					TOTAL_TAX += apPurchaseOrderLine.getPlTaxAmount();

				}

				// add tax distribution if necessary

				if (!apTaxCode.getTcType().equals("NONE") &&
						!apTaxCode.getTcType().equals("EXEMPT")) {

					if (adPreference.getPrfApUseAccruedVat() == EJBCommon.FALSE) {

						this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
								"TAX", EJBCommon.TRUE, TOTAL_TAX, apTaxCode.getGlChartOfAccount().getCoaCode(),
								apPurchaseOrder, AD_BRNCH, AD_CMPNY);

					} else {

						this.addApDrPlEntry(apPurchaseOrder.getApDrNextLine(),
								"ACC TAX", EJBCommon.TRUE, TOTAL_TAX, adPreference.getPrfApGlCoaAccruedVatAccount(),
								apPurchaseOrder, AD_BRNCH, AD_CMPNY);

					}

				}

				TOTAL_AMOUNT = TOTAL_LINE + TOTAL_TAX;

			} else {

				Iterator i = plList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) i.next();

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(mPlDetails.getPlLocName(), mPlDetails.getPlIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPlDetails.getPlLine()));

					}

					//	start date validation

					Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
							apPurchaseOrder.getPoDate(), invItemLocation.getInvItem().getIiName(),
							invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
					if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());

				}

				Collection apDistributionRecords = apPurchaseOrder.getApDistributionRecords();

				i = apDistributionRecords.iterator();

				while(i.hasNext()) {

					LocalApDistributionRecord apDistributionRecord = (LocalApDistributionRecord)i.next();

					if(apDistributionRecord.getDrDebit() == 0) {

						TOTAL_AMOUNT += apDistributionRecord.getDrAmount();

					}
				}

			}

			// generate approval status

			String PO_APPRVL_STATUS = null;

			if (!isDraft) {

				PO_APPRVL_STATUS = "N/A";

				// release purchase order lock

				if (details.getPoType().equals("PO MATCHED")) {

					apExistingPurchaseOrder.setPoLock(EJBCommon.FALSE);

				}

			}

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			if (PO_APPRVL_STATUS != null && PO_APPRVL_STATUS.equals("N/A")) {

				//this.executeApPoPost(apPurchaseOrder.getPoCode(), apPurchaseOrder.getPoLastModifiedBy(), AD_BRNCH, AD_CMPNY);

			}

			// set receiving item approval status

			apPurchaseOrder.setPoApprovalStatus(PO_APPRVL_STATUS);

			return apPurchaseOrder.getPoCode();


		} catch (GlobalRecordAlreadyDeletedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalConversionDateNotExistException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalPaymentTermInvalidException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;


        } catch (GlobalTransactionAlreadyLockedException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlobalInventoryDateException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlobalTransactionAlreadyVoidPostedException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlobalBranchAccountNumberInvalidException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } /*catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

            ctx.setRollbackOnly();
            throw ex;

        } */catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	ctx.setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }

	}

	/**
	 * @ejb:interface-method
	 * */
	public int setApPurcahseOrder(String[] PO, Boolean isReceiving, String BR_BRNCH_CODE, Integer AD_CMPNY)
	{

		System.out.print("PO length:"+PO.length);
		System.out.print("String PO[0]:"+PO[0]);
		System.out.print("Boolean isReceiving:"+isReceiving);
		System.out.print("String BR_BRNCH_CODE:"+BR_BRNCH_CODE);
		System.out.print("Integer AD_CMPNY:"+AD_CMPNY);
		LocalAdBranchHome adBranchHome = null;
		try {

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch(Exception e) {

			return 0;
		}

		Integer AD_BRNCH = new Integer(0);
		try {
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();

		} catch(Exception e) {

			return 0;
		}

		//LocalApPurchaseOrder
		for (int i = 0; i < PO.length; i++) {

			ApModPurchaseOrderDetails  apModPurchaseOrderDetails = poRowDecode(PO[i]);
			try{

				apModPurchaseOrderDetails.setPoVoid(EJBCommon.FALSE);
				apModPurchaseOrderDetails.setPoPosted(EJBCommon.FALSE);
				apModPurchaseOrderDetails.setPoTcType("ITEMS");
				apModPurchaseOrderDetails.setPoCode(null);
				System.out.println(apModPurchaseOrderDetails.getPoSplSupplierCode());
				if(isReceiving){

					Integer purchaseOrderCode = this.saveApPoEntry(apModPurchaseOrderDetails, apModPurchaseOrderDetails.getPoPytName(),
							apModPurchaseOrderDetails.getPoTcName(), apModPurchaseOrderDetails.getPoFcName(), apModPurchaseOrderDetails.getPoSplSupplierCode(), apModPurchaseOrderDetails.getPoPlList(), true,
							AD_BRNCH, AD_CMPNY);
				}else{
					Integer purchaseOrderCode = this.saveApPoSyncEntry(apModPurchaseOrderDetails, apModPurchaseOrderDetails.getPoPytName(),
							apModPurchaseOrderDetails.getPoTcName(), apModPurchaseOrderDetails.getPoFcName(), apModPurchaseOrderDetails.getPoSplSupplierCode(), apModPurchaseOrderDetails.getPoPlList(), true,
							AD_BRNCH, AD_CMPNY);
				}

			}catch(Exception e) {
				e.printStackTrace();
				ctx.setRollbackOnly();
				return 0;
			}
		}

		return 1;
	}

	private Integer saveApPoSyncEntry(com.util.ApPurchaseOrderDetails details, String PYT_NM, String TC_NM, String FC_NM, String SPL_SPPLR_CODE, ArrayList plList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,
	GlobalDocumentNumberNotUniqueException,
	GlobalConversionDateNotExistException,
	GlobalPaymentTermInvalidException,
	GlobalTransactionAlreadyApprovedException,
	GlobalTransactionAlreadyPendingException,
	GlobalTransactionAlreadyPostedException,
	GlobalTransactionAlreadyVoidException,
	GlobalInvItemLocationNotFoundException,
	GlobalNoApprovalRequesterFoundException,
	GlobalNoApprovalApproverFoundException,
	GlobalSupplierItemInvalidException {

		Debug.print("InvItemSyncControllerBean saveApPoSyncEntry");

		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalApSupplierHome apSupplierHome = null;
		LocalAdPaymentTermHome adPaymentTermHome = null;
		LocalApTaxCodeHome apTaxCodeHome = null;
		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalInvItemHome invItemHome = null;

		LocalApPurchaseOrder apPurchaseOrder = null;

		// Initialize EJB Home

		try {

			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
			lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);
			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// validate if purchase order is already deleted

			try {

				if (details.getPoCode() != null) {

					apPurchaseOrder = apPurchaseOrderHome.findByPrimaryKey(details.getPoCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if purchase order is already posted, void, approved or pending

			if (details.getPoCode() != null && details.getPoVoid() == EJBCommon.FALSE) {

				if (apPurchaseOrder.getPoApprovalStatus() != null) {

					if (apPurchaseOrder.getPoApprovalStatus().equals("APPROVED") ||
							apPurchaseOrder.getPoApprovalStatus().equals("N/A")) {

						throw new GlobalTransactionAlreadyApprovedException();

					} else if (apPurchaseOrder.getPoApprovalStatus().equals("PENDING")) {

						throw new GlobalTransactionAlreadyPendingException();

					}
				}

				if (apPurchaseOrder.getPoPosted() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyPostedException();

				} else if (apPurchaseOrder.getPoVoid() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyVoidException();

				}
			}

			// purchase order void

			if (details.getPoCode() != null && details.getPoVoid() == EJBCommon.TRUE) {

				apPurchaseOrder.setPoVoid(EJBCommon.TRUE);
				apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
				apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());

				return apPurchaseOrder.getPoCode();

			}

			// validate if document number is unique document number is automatic then set next sequence

			try {

				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

				if (details.getPoCode() == null) {

					try {

						adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("AP PURCHASE ORDER", AD_CMPNY);

					} catch(FinderException ex) { }

					try {

						adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) { }

					LocalApPurchaseOrder apExistingPurchaseOrder = null;

					try {

						apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(
								details.getPoDocumentNumber(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingPurchaseOrder != null) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
							(details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

						while (true) {

							if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

								try {

									apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

								} catch (FinderException ex) {

									details.setPoDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
									adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
									break;

								}

							} else {

								try {

									apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

								} catch (FinderException ex) {

									details.setPoDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
									adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
									break;

								}

							}

						}

					}

				} else {

					LocalApPurchaseOrder apExistingPurchaseOrder = null;

					try {

						apExistingPurchaseOrder = apPurchaseOrderHome.findByPoDocumentNumberAndBrCode(
								details.getPoDocumentNumber(), AD_BRNCH, AD_CMPNY);

					} catch (FinderException ex) {
					}

					if (apExistingPurchaseOrder != null &&
							!apExistingPurchaseOrder.getPoCode().equals(details.getPoCode())) {

						throw new GlobalDocumentNumberNotUniqueException();

					}

					if (apPurchaseOrder.getPoDocumentNumber() != details.getPoDocumentNumber() &&
							(details.getPoDocumentNumber() == null || details.getPoDocumentNumber().trim().length() == 0)) {

						details.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());

					}

				}

			} catch (GlobalDocumentNumberNotUniqueException ex) {

				ctx.setRollbackOnly();
				throw ex;

			} catch (Exception ex) {

				Debug.printStackTrace(ex);
				ctx.setRollbackOnly();
				throw new EJBException(ex.getMessage());

			}

			// validate if conversion date exists

			try {

				if (details.getPoConversionDate() != null) {

					LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
					LocalGlFunctionalCurrency glValidateFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);

					if (!glValidateFunctionalCurrency.getFcName().equals("USD")) {

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate = glFunctionalCurrencyRateHome.findByFcCodeAndDate(glValidateFunctionalCurrency.getFcCode(),
								details.getPoConversionDate(), AD_CMPNY);

					} else if (!adCompany.getGlFunctionalCurrency().getFcName().equals("USD")){

						LocalGlFunctionalCurrencyRate glFunctionalCurrencyRate =
							glFunctionalCurrencyRateHome.findByFcCodeAndDate(
									adCompany.getGlFunctionalCurrency().getFcCode(), details.getPoConversionDate(), AD_CMPNY);

					}
				}

			} catch (FinderException ex) {

				throw new GlobalConversionDateNotExistException();

			}

			// validate if payment term has at least one payment schedule

			if (adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY).getAdPaymentSchedules().isEmpty()) {

				throw new GlobalPaymentTermInvalidException();

			}

			boolean isRecalculate = true;

			// create purchase order

			if (details.getPoCode() == null) {

				apPurchaseOrder = apPurchaseOrderHome.create(EJBCommon.FALSE, null, details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(),
						details.getPoReferenceNumber(), null, details.getPoDescription(), details.getPoBillTo(), details.getPoShipTo(),
						details.getPoConversionDate(), details.getPoConversionRate(), details.getPoTotalAmount(), EJBCommon.FALSE, EJBCommon.FALSE, null, null,
						EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(), details.getPoLastModifiedBy(),
						details.getPoDateLastModified(), null, null, null, null, EJBCommon.FALSE,
						0d,0d,0d,0d,0d,
						AD_BRNCH,AD_CMPNY);

				/*.create(EJBCommon.FALSE, null, details.getPoDate(), details.getPoDeliveryPeriod(), details.getPoDocumentNumber(),
						details.getPoReferenceNumber(), null, details.getPoDescription(), details.getPoBillTo(), details.getPoShipTo(),
						details.getPoConversionDate(), details.getPoConversionRate(), details.getPoTotalAmount(), EJBCommon.FALSE, EJBCommon.FALSE, null, null,
						EJBCommon.FALSE, null, details.getPoCreatedBy(), details.getPoDateCreated(), details.getPoLastModifiedBy(),
						details.getPoDateLastModified(), null, null, null, null, EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE,null,null,null,null,null,null, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
						EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE, EJBCommon.FALSE,
						null,null,null,null,null,null,null,null,null,
						AD_BRNCH, AD_CMPNY);*/

			} else {

				// check if critical fields are changed

				if (!apPurchaseOrder.getApTaxCode().getTcName().equals(TC_NM) ||
						!apPurchaseOrder.getApSupplier().getSplSupplierCode().equals(SPL_SPPLR_CODE) ||
						!apPurchaseOrder.getAdPaymentTerm().getPytName().equals(PYT_NM) ||
						plList.size() != apPurchaseOrder.getApPurchaseOrderLines().size()) {

					isRecalculate = true;

				} else if (plList.size() == apPurchaseOrder.getApPurchaseOrderLines().size()) {

					Iterator ilIter = apPurchaseOrder.getApPurchaseOrderLines().iterator();
					Iterator ilListIter = plList.iterator();

					while (ilIter.hasNext()) {

						LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)ilIter.next();
						ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)ilListIter.next();

						if (!apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getPlIiName()) ||
								!apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription().equals(mdetails.getPlIiDescription()) ||
								!apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getPlLocName()) ||
								!apPurchaseOrderLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getPlUomName()) ||
								apPurchaseOrderLine.getPlQuantity() != mdetails.getPlQuantity() ||
								apPurchaseOrderLine.getPlUnitCost() != mdetails.getPlUnitCost() ||
								apPurchaseOrderLine.getPlTotalDiscount() != mdetails.getPlTotalDiscount()) {

							isRecalculate = true;
							break;

						}

						isRecalculate = false;

					}

				} else {

					isRecalculate = false;

				}

				apPurchaseOrder.setPoDate(details.getPoDate());
				apPurchaseOrder.setPoDocumentNumber(details.getPoDocumentNumber());
				apPurchaseOrder.setPoReferenceNumber(details.getPoReferenceNumber());
				apPurchaseOrder.setPoDescription(details.getPoDescription());
				apPurchaseOrder.setPoBillTo(details.getPoBillTo());
				apPurchaseOrder.setPoShipTo(details.getPoShipTo());
				apPurchaseOrder.setPoPrinted(details.getPoPrinted());
				apPurchaseOrder.setPoVoid(details.getPoVoid());
				apPurchaseOrder.setPoConversionDate(details.getPoConversionDate());
				apPurchaseOrder.setPoConversionRate(details.getPoConversionRate());
				apPurchaseOrder.setPoLastModifiedBy(details.getPoLastModifiedBy());
				apPurchaseOrder.setPoDateLastModified(details.getPoDateLastModified());

			}

			LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(SPL_SPPLR_CODE, AD_CMPNY);
			//apSupplier.addApPurchaseOrder(apPurchaseOrder);
			apPurchaseOrder.setApSupplier(apSupplier);

			LocalAdPaymentTerm adPaymentTerm = adPaymentTermHome.findByPytName(PYT_NM, AD_CMPNY);
			//adPaymentTerm.addApPurchaseOrder(apPurchaseOrder);
			apPurchaseOrder.setAdPaymentTerm(adPaymentTerm);

			LocalApTaxCode apTaxCode = apTaxCodeHome.findByTcName(TC_NM, AD_CMPNY);
			//apTaxCode.addApPurchaseOrder(apPurchaseOrder);
			apPurchaseOrder.setApTaxCode(apTaxCode);

			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(FC_NM, AD_CMPNY);
			//glFunctionalCurrency.addApPurchaseOrder(apPurchaseOrder);
			apPurchaseOrder.setGlFunctionalCurrency(glFunctionalCurrency);

			double ABS_TOTAL_AMOUNT = 0d;

			//	 Map Supplier and Item

			Iterator iter = plList.iterator();

			while (iter.hasNext()) {

				ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) iter.next();
				LocalInvItem invItem = invItemHome.findByIiName(mPlDetails.getPlIiName(), AD_CMPNY);

				if(invItem.getApSupplier() != null &&
						!invItem.getApSupplier().getSplSupplierCode().equals(
								apPurchaseOrder.getApSupplier().getSplSupplierCode())) {

					throw new GlobalSupplierItemInvalidException("" + mPlDetails.getPlLine());

				}

			}


			if (isRecalculate) {

				// remove all purchase order line items

				Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

				Iterator i = apPurchaseOrderLines.iterator();

				while (i.hasNext()) {

					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();

					i.remove();

					apPurchaseOrderLine.remove();

				}

				// add new purchase order line item

				i = plList.iterator();

				LocalInvItemLocation invItemLocation = null;

				while (i.hasNext()) {

					ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails) i.next();

					LocalApPurchaseOrderLine apPurchaseOrderLine = apPurchaseOrderLineHome.create(
							mPlDetails.getPlLine(),	mPlDetails.getPlQuantity(), mPlDetails.getPlUnitCost(),
							mPlDetails.getPlAmount(), mPlDetails.getPlQcNumber(),mPlDetails.getPlQcExpiryDate(), mPlDetails.getPlConversionFactor(), 0d, null, mPlDetails.getPlDiscount1(),
							mPlDetails.getPlDiscount2(), mPlDetails.getPlDiscount3(), mPlDetails.getPlDiscount4(), mPlDetails.getPlTotalDiscount(),
							AD_CMPNY);

					//apPurchaseOrder.addApPurchaseOrderLine(apPurchaseOrderLine);
					apPurchaseOrderLine.setApPurchaseOrder(apPurchaseOrder);

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								mPlDetails.getPlLocName(), mPlDetails.getPlIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mPlDetails.getPlLine()));

					}

					ABS_TOTAL_AMOUNT += apPurchaseOrderLine.getPlAmount();

					//invItemLocation.addApPurchaseOrderLine(apPurchaseOrderLine);
					apPurchaseOrderLine.setInvItemLocation(invItemLocation);

					LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
							mPlDetails.getPlUomName(), AD_CMPNY);

					//invUnitOfMeasure.addApPurchaseOrderLine(apPurchaseOrderLine);
					apPurchaseOrderLine.setInvUnitOfMeasure(invUnitOfMeasure);

				}

			} else {

				Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

				Iterator i = apPurchaseOrderLines.iterator();

				while (i.hasNext()) {

					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)i.next();

					ABS_TOTAL_AMOUNT += apPurchaseOrderLine.getPlAmount();

				}
			}

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			// set purchase order approval status

			String PO_APPRVL_STATUS = null;

			if(!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if ap voucher approval is enabled

				if (adApproval.getAprEnableApPurchaseOrder() == EJBCommon.FALSE) {

					PO_APPRVL_STATUS = "N/A";

				} else {

					// check if voucher is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("AP PURCHASE ORDER", "REQUESTER", details.getPoLastModifiedBy(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit()) {

						PO_APPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("AP PURCHASE ORDER", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP PURCHASE ORDER", apPurchaseOrder.getPoCode(),
										apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

							}

						} else {

							boolean isApprovalUsersFound = false;

							Iterator i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP PURCHASE ORDER", apPurchaseOrder.getPoCode(),
												apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "AP PURCHASE ORDER", apPurchaseOrder.getPoCode(),
												apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getPoDate(), adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						PO_APPRVL_STATUS = "PENDING";

					}

				}

				apPurchaseOrder.setPoApprovalStatus(PO_APPRVL_STATUS);

				// set post purchase order

				if(PO_APPRVL_STATUS.equals("N/A")) {

					apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
					apPurchaseOrder.setPoPosted(EJBCommon.TRUE);
					apPurchaseOrder.setPoPostedBy(apPurchaseOrder.getPoLastModifiedBy());
					apPurchaseOrder.setPoDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());

				}

			}

			return apPurchaseOrder.getPoCode();


		} catch (GlobalRecordAlreadyDeletedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalDocumentNumberNotUniqueException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalConversionDateNotExistException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalPaymentTermInvalidException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyVoidException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalSupplierItemInvalidException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}
	private void addApDrPlEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalApPurchaseOrder apPurchaseOrder, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalBranchAccountNumberInvalidException {

		Debug.print("InvItemSyncControllerBean addApDrPlEntry");

		LocalApDistributionRecordHome apDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			apDistributionRecordHome = (LocalApDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalApDistributionRecordHome.JNDI_NAME, LocalApDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			LocalGlChartOfAccount glChartOfAccount = null;

			try {

				glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

				if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE)
					throw new GlobalBranchAccountNumberInvalidException();

			} catch (FinderException ex) {

				throw new GlobalBranchAccountNumberInvalidException();

			}

			// create distribution record

			LocalApDistributionRecord apDistributionRecord = apDistributionRecordHome.create(
					DR_LN, DR_CLSS, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					DR_DBT, EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			//apPurchaseOrder.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setApPurchaseOrder(apPurchaseOrder);
			//glChartOfAccount.addApDistributionRecord(apDistributionRecord);
			apDistributionRecord.setGlChartOfAccount(glChartOfAccount);

		} catch(GlobalBranchAccountNumberInvalidException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private LocalApPurchaseOrderLine addApPlEntry(ApModPurchaseOrderLineDetails mdetails, LocalApPurchaseOrder apPurchaseOrder,
			LocalInvItemLocation invItemLocation, boolean newPurchaseOrder, Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean addApPlEntry");

		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

		// Initialize EJB Home

		try {

			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			short precisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

			double VLI_AMNT = 0d;
			double VLI_TAX_AMNT = 0d;

			// calculate net amount

			LocalApTaxCode apTaxCode = apPurchaseOrder.getApTaxCode();

			if (apTaxCode.getTcType().equals("INCLUSIVE")) {

				VLI_AMNT = EJBCommon.roundIt(mdetails.getPlAmount() / (1 + (apTaxCode.getTcRate() / 100)), precisionUnit);

			} else {

				// tax exclusive, none, zero rated or exempt

				VLI_AMNT = mdetails.getPlAmount();

			}

			// calculate tax

			if (!apTaxCode.getTcType().equals("NONE") &&
					!apTaxCode.getTcType().equals("EXEMPT")) {


				if (apTaxCode.getTcType().equals("INCLUSIVE")) {

					VLI_TAX_AMNT = EJBCommon.roundIt(mdetails.getPlAmount() - VLI_AMNT, precisionUnit);

				} else if (apTaxCode.getTcType().equals("EXCLUSIVE")) {

					VLI_TAX_AMNT = EJBCommon.roundIt(mdetails.getPlAmount() * apTaxCode.getTcRate() / 100, precisionUnit);

				} else {

					// tax none zero-rated or exempt

				}

			}

			LocalApPurchaseOrderLine apPurchaseOrderLine = null;

			apPurchaseOrderLine = apPurchaseOrderLineHome.create(
					mdetails.getPlLine(), mdetails.getPlQuantity(), mdetails.getPlUnitCost(),
					VLI_AMNT, mdetails.getPlQcNumber(), mdetails.getPlQcExpiryDate(), mdetails.getPlConversionFactor(), VLI_TAX_AMNT, apPurchaseOrder.getPoType().equals("PO MATCHED") ? mdetails.getPlPlCode() : null,
							mdetails.getPlDiscount1(), mdetails.getPlDiscount2(), mdetails.getPlDiscount3(),
							mdetails.getPlDiscount4(),mdetails.getPlTotalDiscount(), AD_CMPNY);

			//apPurchaseOrder.addApPurchaseOrderLine(apPurchaseOrderLine);
			apPurchaseOrderLine.setApPurchaseOrder(apPurchaseOrder);

			//invItemLocation.addApPurchaseOrderLine(apPurchaseOrderLine);
			apPurchaseOrderLine.setInvItemLocation(invItemLocation);

			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getPlUomName(), AD_CMPNY);
			//invUnitOfMeasure.addApPurchaseOrderLine(apPurchaseOrderLine);
			apPurchaseOrderLine.setInvUnitOfMeasure(invUnitOfMeasure);

			return apPurchaseOrderLine;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("InvItemSyncControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	/**
	 * @ejb:interface-method
	 * */
	public String setInvBST(String[] BST, String BR_BRNCH_CODE, Integer AD_CMPNY)
	{

		Debug.print("InvItemSyncControllerBean setInvBST");

		LocalAdBranchHome adBranchHome = null;

		try {
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
		} catch(Exception e) {
			return "$ERROR[EJB Error]$";
		}

		Integer AD_BRNCH = new Integer(0);
		LocalAdBranch adBranch = null;
		try {

			adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();

		} catch(Exception e) {
			return "$ERROR[Branch Code Not Found: " + BR_BRNCH_CODE + "]$";
		}

		String returnString = "";

		for (int i = 0; i < BST.length; i++) {

			try{

				// Insert Initial Error
				returnString = "Parse Error";
				InvModBranchStockTransferDetails invModBranchStockTransferDetails = bstRowDecode(BST[i]);

				returnString = invModBranchStockTransferDetails.getBstNumber();

				invModBranchStockTransferDetails.setBstVoid(EJBCommon.FALSE);
				invModBranchStockTransferDetails.setBstPosted(EJBCommon.FALSE);


				if(invModBranchStockTransferDetails.getBstType().equals("IN")){

					String branchFromName = "";
					try{
						branchFromName = adBranchHome.findByBrBranchCode(invModBranchStockTransferDetails.getBstBranchFrom(), AD_CMPNY).getBrName();
					}
					catch(Exception ex){
						throw ex;
					}

					invModBranchStockTransferDetails.setBstBranchFrom(branchFromName);
					invModBranchStockTransferDetails.setBstBranchTo(adBranch.getBrName());

					Integer invBstInCode = this.saveInvBstInEntry(invModBranchStockTransferDetails, invModBranchStockTransferDetails.getBstBtlList(),
							invModBranchStockTransferDetails.getBstType() , AD_BRNCH, AD_CMPNY);

					returnString = "";
				}else if(invModBranchStockTransferDetails.getBstType().equals("OUT")){

					String branchFromName = "";
					Integer branchFromCode;
					try{
						LocalAdBranch brFrom = adBranchHome.findByBrBranchCode(invModBranchStockTransferDetails.getBstBranchFrom(), AD_CMPNY);
						branchFromName = brFrom.getBrName();
						branchFromCode = brFrom.getBrCode();
					}
					catch(Exception ex){
						throw ex;
					}

					invModBranchStockTransferDetails.setBstBranchFrom(adBranch.getBrName());
					invModBranchStockTransferDetails.setBstBranchTo(branchFromName);

					Integer invBstInCode = this.saveInvBstInEntry(invModBranchStockTransferDetails, invModBranchStockTransferDetails.getBstBtlList(),
							invModBranchStockTransferDetails.getBstType() , branchFromCode, AD_CMPNY);

					returnString = "";
					/*String branchFromTo = "";
					try{
						branchFromTo = adBranchHome.findByBrBranchCode(invModBranchStockTransferDetails.getBstBranchTo(), AD_CMPNY).getBrName();
					}
					catch(Exception ex){
						throw ex;
					}

					invModBranchStockTransferDetails.setBstBranchFrom(adBranch.getBrName());
					invModBranchStockTransferDetails.setBstBranchTo(branchFromTo);
					Integer invBstOutCode = this.saveInvBstOutEntry(invModBranchStockTransferDetails, invModBranchStockTransferDetails.getBstBtlList(),
							true, AD_BRNCH, AD_CMPNY);

					returnString = "";*/
				}

			}catch(Exception e) {
				e.printStackTrace();
				ctx.setRollbackOnly();
				break;
			}
		}

		if(returnString.equals("")){

			returnString="$SUCCESS$";

		}else{
			returnString = "$" + returnString + "$";
		}

		return returnString;

	}

	private Integer saveInvBstInEntry(com.util.InvModBranchStockTransferDetails details, ArrayList bslList, String type, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyApprovedException,
	GlobalTransactionAlreadyPendingException,
	GlobalTransactionAlreadyPostedException,
	GlobalInvItemLocationNotFoundException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalDocumentNumberNotUniqueException,
	GlobalInventoryDateException,
	GlobalBranchAccountNumberInvalidException,
	InvATRAssemblyQtyGreaterThanAvailableQtyException,
	GlobalRecordAlreadyAssignedException,
	AdPRFCoaGlVarianceAccountNotFoundException {


		Debug.print("InvItemSyncControllerBean saveInvBstEntry");

		LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;

		try {

			invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		try {
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvBranchStockTransfer invBranchStockTransfer = null;

			//Validate if branch stock transfer is already deleted
			try {

				if (details.getBstCode() != null) {

					invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(details.getBstCode());

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			//Validate if branch stock transfer is already posted, void, approved or pending
			if (details.getBstCode() != null) {

				if (invBranchStockTransfer.getBstApprovalStatus() != null) {

					if (invBranchStockTransfer.getBstApprovalStatus().equals("APPROVED") ||
							invBranchStockTransfer.getBstApprovalStatus().equals("N/A")) {

						throw new GlobalTransactionAlreadyApprovedException();


					} else if (invBranchStockTransfer.getBstApprovalStatus().equals("PENDING")) {

						throw new GlobalTransactionAlreadyPendingException();

					}

				}

				if (invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyPostedException();

				}

			}

			LocalInvBranchStockTransfer invBranchExistingStockTransfer = null;

			try {

				invBranchExistingStockTransfer = invBranchStockTransferHome.findByBstNumberAndBrCode(details.getBstNumber(), AD_BRNCH, AD_CMPNY);

			} catch (FinderException ex) {

			}

			// validate if document number is unique and if document number is automatic then set next sequence\

			if (details.getBstCode() == null) {

				System.out.println("Checkpoint A");

				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

				if (invBranchExistingStockTransfer != null) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				try {

					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV BRANCH STOCK TRANSFER-IN", AD_CMPNY);

				} catch (FinderException ex) {

				}

				try {

					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

				}

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
						(details.getBstNumber() == null || details.getBstNumber().trim().length() == 0)) {

					while (true) {

						if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

							try {

								invBranchStockTransferHome.findByBstNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

							} catch (FinderException ex) {

								details.setBstNumber(adDocumentSequenceAssignment.getDsaNextSequence());
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
								break;

							}

						} else {

							try {

								invBranchStockTransferHome.findByBstNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

							} catch (FinderException ex) {

								details.setBstNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
								break;

							}

						}

					}

				}

			} else {

				System.out.println("Checkpoint B");

				if (invBranchExistingStockTransfer != null &&
						!invBranchExistingStockTransfer.getBstCode().equals(details.getBstCode())) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (invBranchStockTransfer.getBstNumber() != details.getBstNumber() &&
						(details.getBstNumber() == null || details.getBstNumber().trim().length() == 0)) {

					details.setBstNumber(invBranchStockTransfer.getBstNumber());

				}

			}


			// check if transfer-out already has transfer in
			try {

				LocalInvBranchStockTransfer invExistingBranchStockTransferIn = invBranchStockTransferHome.findInByBstOutNumberAndBrCode(
						details.getBstTransferOutNumber(), AD_BRNCH, AD_CMPNY);

				System.out.println("details.getBstCode(): " + details.getBstCode());
				System.out.println("invExistingBranchStockTransferIn.getBstCode(): " + invExistingBranchStockTransferIn.getBstCode());

				if ((details.getBstCode() != null && !invExistingBranchStockTransferIn.getBstCode().equals(details.getBstCode())) ||
						details.getBstCode() == null) {

					throw new GlobalRecordAlreadyAssignedException();

				}

			} catch (FinderException ex) {

			}


			// used in checking if branch stock transfer should re-generate distribution records

			boolean isRecalculate = true;

			// create branch stock transfer

			if (details.getBstCode() == null) {

				invBranchStockTransfer = invBranchStockTransferHome.create(details.getBstDate(),
						"IN", details.getBstNumber(), details.getBstTransferOutNumber(), null,
						details.getBstDescription(), details.getBstApprovalStatus(), details.getBstPosted(),
						details.getBstReasonForRejection(), details.getBstCreatedBy(), details.getBstDateCreated(),
						details.getBstLastModifiedBy(), details.getBstDateLastModified(), details.getBstApprovedRejectedBy(),
						details.getBstDateApprovedRejected(), details.getBstPostedBy(), details.getBstDatePosted(),
						EJBCommon.FALSE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

			} else {

				if (bslList.size() != invBranchStockTransfer.getInvBranchStockTransferLines().size() ||
						!(invBranchStockTransfer.getBstDate().equals(details.getBstDate()))) {

					isRecalculate = true;

				} else if (bslList.size() == invBranchStockTransfer.getInvBranchStockTransferLines().size()) {

					Iterator bslIter = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();
					Iterator bslIterList = bslList.iterator();

					while(bslIter.hasNext()) {

						LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) bslIter.next();
						InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) bslIterList.next();


						if (!invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getBslLocationName()) ||
								invBranchStockTransferLine.getBslQuantityReceived() != mdetails.getBslQuantityReceived()) {

							isRecalculate = true;
							break;

						}

						isRecalculate = false;

					}

				} else {

					isRecalculate = true;

				}

				invBranchStockTransfer.setBstType("IN");
				invBranchStockTransfer.setBstNumber(details.getBstNumber());
				invBranchStockTransfer.setBstDescription(details.getBstDescription());
				invBranchStockTransfer.setBstDate(details.getBstDate());
				invBranchStockTransfer.setBstTransferOutNumber(details.getBstTransferOutNumber());
				invBranchStockTransfer.setBstApprovalStatus(details.getBstApprovalStatus());
				invBranchStockTransfer.setBstLastModifiedBy(details.getBstLastModifiedBy());
				invBranchStockTransfer.setBstDateLastModified(details.getBstDateLastModified());
				invBranchStockTransfer.setBstReasonForRejection(null);

			}

			LocalInvBranchStockTransfer invBranchStockTransferOut = null;

			// get transfer out branch
			LocalAdBranch adBranchFrom = adBranchHome.findByBrName(details.getBstBranchFrom(), AD_CMPNY);
			adBranchFrom.addInvBranchStockTransfer(invBranchStockTransfer);

			if (type.equalsIgnoreCase("BST-OUT MATCHED")){

				// lock corresponding transfer out
				invBranchStockTransferOut =
					invBranchStockTransferHome.findByBstNumberAndBrCode(invBranchStockTransfer.getBstTransferOutNumber(),
							invBranchStockTransfer.getAdBranch().getBrCode(), AD_CMPNY);

				invBranchStockTransferOut.setBstLock(EJBCommon.TRUE);

			}

			LocalInvLocation invLocation = invLocationHome.findByLocName(details.getBstTransitLocation(), AD_CMPNY);
			//invLocation.addInvBranchStockTransfer(invBranchStockTransfer);
			invBranchStockTransfer.setInvLocation(invLocation);

			double ABS_TOTAL_AMOUNT = 0d;

			if (isRecalculate) {

				// remove all branch stock transfer lines

				Iterator i = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();

				short LINE_NUMBER = 0;

				while(i.hasNext()) {

					LINE_NUMBER++;

					LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) i.next();

					LocalInvItemLocation invItemLocation = null;

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(),
								invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);

					} catch(FinderException ex) {

						System.out.println("Line " + String.valueOf(LINE_NUMBER) + " - " + invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());

						throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(LINE_NUMBER) + " - " + invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());

					}

					double convertedQuantity = this.convertByUomAndQuantity(
							invBranchStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
							invBranchStockTransferLine.getBslQuantityReceived(), AD_CMPNY);

					invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);

					i.remove();

					invBranchStockTransferLine.remove();

				}

				// remove all distribution records

				i = invBranchStockTransfer.getInvDistributionRecords().iterator();

				while(i.hasNext()) {

					LocalInvDistributionRecord arDistributionRecord = (LocalInvDistributionRecord) i.next();

					i.remove();

					arDistributionRecord.remove();

				}

				// add new branch stock transfer entry lines and distribution record

				byte DEBIT = 0;

				i = bslList.iterator();

				while(i.hasNext()) {

					InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) i.next();

					LocalInvItemLocation invItemLocation = null;

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								mdetails.getBslLocationName(),
								mdetails.getBslIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						System.out.println("Line " + String.valueOf(mdetails.getBslLineNumber() + " - " + mdetails.getBslLocationName()));
						throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(mdetails.getBslLineNumber() + " - " + mdetails.getBslLocationName()));

					}

					LocalInvItemLocation invItemTransitLocation = null;

					try {

						invItemTransitLocation = invItemLocationHome.findByLocNameAndIiName(
								details.getBstTransitLocation(),
								mdetails.getBslIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						System.out.println("Transit Location " + String.valueOf(details.getBstTransitLocation()));
						throw new GlobalInvItemLocationNotFoundException("Transit Location " + String.valueOf(details.getBstTransitLocation()));

					}

					if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
						//Start date validation
						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								invBranchStockTransfer.getBstDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
					}


					LocalInvBranchStockTransferLine invBranchStockTransferLine = this.addInvBslEntryIn(mdetails, invBranchStockTransfer, AD_CMPNY);


					// add physical inventory distribution

					double COST = invBranchStockTransferLine.getBslUnitCost();

					double SHIPPING = mdetails.getBslShippingCost();

					double AMOUNT = 0d;

					AMOUNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantityReceived() * COST, this.getGlFcPrecisionUnit(AD_CMPNY));

					double SHIPPING_AMNT = 0d;

					SHIPPING_AMNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantityReceived() * SHIPPING, this.getGlFcPrecisionUnit(AD_CMPNY));

					// check branch mapping

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try{

						adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
								invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);


					} catch (FinderException ex) {

					}

					LocalGlChartOfAccount glChartOfAccount = null;

					if (adBranchItemLocation == null) {

						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
								invItemLocation.getIlGlCoaInventoryAccount());

					} else {

						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
								adBranchItemLocation.getBilCoaGlInventoryAccount());

					}

					// add dr for inventory

					this.addInvDrEntryIn(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE,
							Math.abs(AMOUNT), glChartOfAccount.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);


					// check branch mapping for transit location

					LocalAdBranchItemLocation adBranchItemTransitLocation = null;

					try{

						adBranchItemTransitLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
								invItemTransitLocation.getIlCode(), adBranchFrom.getBrCode(), AD_CMPNY);

					} catch (FinderException ex) {

					}

					LocalGlChartOfAccount glChartOfAccountTransit = null;

					if (adBranchItemTransitLocation == null) {

						glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
								invItemTransitLocation.getIlGlCoaInventoryAccount());

					} else {

						glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
								adBranchItemTransitLocation.getBilCoaGlInventoryAccount());

					}

					// add dr for inventory transit location

					this.addInvDrEntryIn(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE,
							Math.abs(AMOUNT - SHIPPING_AMNT), glChartOfAccountTransit.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);

					// add dr for shipping if shipping amount > 0

					if(SHIPPING_AMNT > 0d) {

						LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(AD_BRNCH);
						LocalGlChartOfAccount glChartOfAccountShipping = adBranch.getGlChartOfAccount();

						this.addInvDrEntryIn(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE,
								Math.abs(SHIPPING_AMNT), glChartOfAccountShipping.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);

					}

					ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);

					// set ilCommittedQuantity

					double convertedQuantity = this.convertByUomAndQuantity(
							invBranchStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
							invBranchStockTransferLine.getBslQuantityReceived(), AD_CMPNY);

					invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

				}

			} else {

				Iterator i = bslList.iterator();

				while(i.hasNext()) {

					InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) i.next();

					LocalInvItemLocation invItemLocation = null;

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								mdetails.getBslLocationName(),
								mdetails.getBslIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						System.out.println("Line " + String.valueOf(mdetails.getBslLineNumber() + " - " + mdetails.getBslLocationName()));
						throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(mdetails.getBslLineNumber() + " - " + mdetails.getBslLocationName()));

					}
					if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
						//	start date validation

						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								invBranchStockTransfer.getBstDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
					}


					i = invBranchStockTransfer.getInvDistributionRecords().iterator();

					while(i.hasNext()) {

						LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();

						if(distributionRecord.getDrDebit() == 1) {

							ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();

						}

					}

				}

			}

			// set stock transfer approval status
			invBranchStockTransfer.setBstApprovalStatus(null);

			return invBranchStockTransfer.getBstCode();

		} catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalRecordAlreadyDeletedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} /*catch (GlJREffectiveDateNoPeriodExistException ex) {

    		ctx.setRollbackOnly();
    		throw ex;

    	} catch (GlJREffectiveDatePeriodClosedException ex) {

    		ctx.setRollbackOnly();
    		throw ex;

    	} catch (GlobalJournalNotBalanceException ex) {

    		ctx.setRollbackOnly();
    		throw ex;

    	} */catch (GlobalInventoryDateException ex) {

    		ctx.setRollbackOnly();
    		throw ex;

    	} catch (GlobalDocumentNumberNotUniqueException ex) {

    		ctx.setRollbackOnly();
    		throw ex;

    	} catch(GlobalBranchAccountNumberInvalidException ex) {

    		ctx.setRollbackOnly();
    		throw ex;

    	} catch(GlobalRecordAlreadyAssignedException ex) {

    		ctx.setRollbackOnly();
    		throw ex;

    	} /*catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

    		ctx.setRollbackOnly();
    		throw ex;

    	}	*/catch (Exception ex) {

    		Debug.printStackTrace(ex);
    		ctx.setRollbackOnly();
    		throw new EJBException(ex.getMessage());

    	}


	}

	private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

		Debug.print("InvBranchStockTransferOutEntryControllerBean convertByUomFromAndUomToAndQuantity");

		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

		// Initialize EJB Home

		try {

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
			LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

			return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private LocalInvBranchStockTransferLine addInvBslEntryOut(InvModBranchStockTransferLineDetails mdetails,
			LocalInvBranchStockTransfer invBranchStockTransfer, Integer AD_CMPNY) {

		Debug.print("InvBranchStockTransferOutEntryControllerBean addInvBslEntry");

		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalInvItemLocationHome invItemLocationHome= null;

		// Initialize EJB Home

		try {

			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalInvBranchStockTransferLine invBranchStockTransferLine =
				invBranchStockTransferLineHome.create(mdetails.getBslQuantity(),mdetails.getBslQuantityReceived(),
						mdetails.getBslUnitCost(),mdetails.getBslAmount(), AD_CMPNY);

			invBranchStockTransferLine.setBslMisc("");

			//invBranchStockTransfer.addInvBranchStockTransferLine(invBranchStockTransferLine);
			invBranchStockTransferLine.setInvBranchStockTransfer(invBranchStockTransfer);

			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
					mdetails.getBslUomName(), AD_CMPNY);

			//invUnitOfMeasure.addInvBranchStockTransferLine(invBranchStockTransferLine);
			invBranchStockTransferLine.setInvUnitOfMeasure(invUnitOfMeasure);


			LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
					mdetails.getBslIiName(), mdetails.getBslLocationName(), AD_CMPNY);

			//invItemLocation.addInvBranchStockTransferLine(invBranchStockTransferLine);
			invBranchStockTransferLine.setInvItemLocation(invItemLocation);


			return invBranchStockTransferLine;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}
	private LocalInvBranchStockTransferLine addInvBslEntryIn(InvModBranchStockTransferLineDetails mdetails,
			LocalInvBranchStockTransfer invBranchStockTransfer, Integer AD_CMPNY)
	throws InvATRAssemblyQtyGreaterThanAvailableQtyException {

		Debug.print("InvBranchStockTransferInEntryControllerBean addInvBslEntry");

		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalInvItemLocationHome invItemLocationHome= null;

		// Initialize EJB Home

		try {

			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			if(mdetails.getBslQuantity() < mdetails.getBslQuantityReceived())
				throw new InvATRAssemblyQtyGreaterThanAvailableQtyException();

			LocalInvBranchStockTransferLine invBranchStockTransferLine =
				invBranchStockTransferLineHome.create(mdetails.getBslQuantity(),mdetails.getBslQuantityReceived(),
						mdetails.getBslUnitCost(),mdetails.getBslAmount(), AD_CMPNY);

			invBranchStockTransferLine.setBslMisc("");

			//invBranchStockTransfer.addInvBranchStockTransferLine(invBranchStockTransferLine);
			invBranchStockTransferLine.setInvBranchStockTransfer(invBranchStockTransfer);

			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
					mdetails.getBslUomName(), AD_CMPNY);

			//invUnitOfMeasure.addInvBranchStockTransferLine(invBranchStockTransferLine);
			invBranchStockTransferLine.setInvUnitOfMeasure(invUnitOfMeasure);


			LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
					mdetails.getBslIiName(), mdetails.getBslLocationName(), AD_CMPNY);

			//invItemLocation.addInvBranchStockTransferLine(invBranchStockTransferLine);
			invBranchStockTransferLine.setInvItemLocation(invItemLocation);

			return invBranchStockTransferLine;

		} catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addInvDrEntryOut(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT,
			Integer COA_CODE, LocalInvBranchStockTransfer invBranchStockTransfer,
			Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalBranchAccountNumberInvalidException {

		Debug.print("InvBranchStockTransferOutEntryControllerBean addInvDrEntry");

		LocalAdCompanyHome adCompanyHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			// validate coa

			LocalGlChartOfAccount glChartOfAccount = null;

			try {

				glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

			} catch(FinderException ex) {

				throw new GlobalBranchAccountNumberInvalidException ();

			}

			// create distribution record

			LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			//invBranchStockTransfer.addInvDistributionRecord(invDistributionRecord);
			invDistributionRecord.setInvBranchStockTransfer(invBranchStockTransfer);
			//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
			invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}

	private void addInvDrEntryIn(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT,
			Integer COA_CODE, LocalInvBranchStockTransfer invBranchStockTransfer,
			Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalBranchAccountNumberInvalidException {

		Debug.print("InvBranchStockTransferInEntryControllerBean addInvDrEntry");

		LocalAdCompanyHome adCompanyHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			// get company

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			// validate coa

			LocalGlChartOfAccount glChartOfAccount = null;

			try {

				glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

			} catch(FinderException ex) {

				throw new GlobalBranchAccountNumberInvalidException ();

			}

			// create distribution record

			LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
					DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
					EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

			//invBranchStockTransfer.addInvDistributionRecord(invDistributionRecord);
			invDistributionRecord.setInvBranchStockTransfer(invBranchStockTransfer);
			//glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
			invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

		} catch (GlobalBranchAccountNumberInvalidException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			ctx.setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}

	}
	private InvModBranchStockTransferDetails bstRowDecode(String bst)
	{

		//Debug.print("InvItemSyncControllerBean adjRowDecode");

		String separator = "$";
		InvModBranchStockTransferDetails invBranchStockTransferDetails = new InvModBranchStockTransferDetails();

		// Remove first $ character
		bst = bst.substring(1);

		// bst number
		int start = 0;
		int nextIndex = bst.indexOf(separator, start);
		int length = nextIndex - start;
		invBranchStockTransferDetails.setBstNumber(bst.substring(start, start + length));
		//invBranchStockTransferDetails.setBstCode(new Integer(bst.substring(start, start + length)));

		// BST Date
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		sdf.setLenient(false);
		try {
			invBranchStockTransferDetails.setBstDate(sdf.parse(bst.substring(start, start + length)));
			System.out.println("Date-" + invBranchStockTransferDetails.getBstDate());
		} catch (Exception ex) {
			System.out.println("Error On PO Date: " + ex.getMessage());
			//throw ex;
		}

		// BST Type
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;
		invBranchStockTransferDetails.setBstType(bst.substring(start, start + length));

		// BST Transfer Out Number
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;
		invBranchStockTransferDetails.setBstTransferOutNumber(bst.substring(start, start + length));

		// Description
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;
		invBranchStockTransferDetails.setBstDescription(bst.substring(start, start + length));

		// Location
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;
		invBranchStockTransferDetails.setBstTransitLocation(bst.substring(start, start + length));

		// Branch From
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;
		invBranchStockTransferDetails.setBstBranchFrom(bst.substring(start, start + length));

		// Branch To
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;
		invBranchStockTransferDetails.setBstBranchTo(bst.substring(start, start + length));

		/************************** Misc Details***************************/

		// Created By
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;
		invBranchStockTransferDetails.setBstCreatedBy(bst.substring(start, start + length));

		// Date Created
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;
		try {
			invBranchStockTransferDetails.setBstDateCreated(sdf.parse(bst.substring(start, start + length)));
			System.out.println("Date Created-" + invBranchStockTransferDetails.getBstDateCreated());
		} catch (Exception ex) {
			System.out.println("Error On Date Created");
			//throw ex;
		}

		// Last Modified By
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;
		invBranchStockTransferDetails.setBstLastModifiedBy(bst.substring(start, start + length));

		// Date Last Modified
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;
		try {
			invBranchStockTransferDetails.setBstDateLastModified(sdf.parse(bst.substring(start, start + length)));
			System.out.println("Date Last Modified-" + invBranchStockTransferDetails.getBstDateLastModified());
		} catch (Exception ex) {
			System.out.println("Error On Date Last Modified");
			//throw ex;
		}

		// end separator
		start = nextIndex + 1;
		nextIndex = bst.indexOf(separator, start);
		length = nextIndex - start;

		String lineSeparator = "~";

		// begin lineSeparator
		start = nextIndex + 1;
		nextIndex = bst.indexOf(lineSeparator, start);
		length = nextIndex - start;

		invBranchStockTransferDetails.setBstBtlList(new ArrayList());

		while (true) {
			InvModBranchStockTransferLineDetails invModBranchStockTransferLineDetails = new InvModBranchStockTransferLineDetails();

			// begin separator
			start = nextIndex + 1;
			nextIndex = bst.indexOf(separator, start);
			length = nextIndex - start;

			// bst Quantity
			start = nextIndex + 1;
			nextIndex = bst.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println(" bst Quantity: " + bst.substring(start, start + length));
			invModBranchStockTransferLineDetails.setBslQuantity((double)Double.parseDouble(bst.substring(start, start + length)));


			// bst Quantity Received
			start = nextIndex + 1;
			nextIndex = bst.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println("bst Quantity Received: " + bst.substring(start, start + length));
			invModBranchStockTransferLineDetails.setBslQuantityReceived((double)Double.parseDouble(bst.substring(start, start + length)));

			// Unit Cost
			start = nextIndex + 1;
			nextIndex = bst.indexOf(separator, start);
			length = nextIndex - start;
			System.out.println("Unit Cost: " + bst.substring(start, start + length));
			invModBranchStockTransferLineDetails.setBslUnitCost((double)Double.parseDouble(bst.substring(start, start + length)));

			// Amount
			start = nextIndex + 1;
			nextIndex = bst.indexOf(separator, start);
			length = nextIndex - start;
			invModBranchStockTransferLineDetails.setBslAmount((double)Double.parseDouble(bst.substring(start, start + length)));

			// Item
			start = nextIndex + 1;
			nextIndex = bst.indexOf(separator, start);
			length = nextIndex - start;
			invModBranchStockTransferLineDetails.setBslIiName(bst.substring(start, start + length));

			// Location
			start = nextIndex + 1;
			nextIndex = bst.indexOf(separator, start);
			length = nextIndex - start;
			invModBranchStockTransferLineDetails.setBslLocationName(bst.substring(start, start + length));

			// Unit of Measure
			start = nextIndex + 1;
			nextIndex = bst.indexOf(separator, start);
			length = nextIndex - start;
			invModBranchStockTransferLineDetails.setBslUomName(bst.substring(start, start + length));

			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = bst.indexOf(lineSeparator, start);
			length = nextIndex - start;

			invBranchStockTransferDetails.getBstBtlList().add(invModBranchStockTransferLineDetails);

			int tempStart = nextIndex + 1;
			if (bst.indexOf(separator, tempStart) == -1) break;

		}

		return invBranchStockTransferDetails;


	}

	private Integer saveInvBstOutEntry(com.util.InvModBranchStockTransferDetails details, ArrayList bslList,
			boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY, String type)
	throws GlobalRecordAlreadyDeletedException,
	GlobalTransactionAlreadyApprovedException,
	GlobalTransactionAlreadyPendingException,
	GlobalTransactionAlreadyPostedException,
	GlobalNoApprovalRequesterFoundException,
	GlobalNoApprovalApproverFoundException,
	GlobalInvItemLocationNotFoundException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalDocumentNumberNotUniqueException,
	GlobalInventoryDateException,
	GlobalBranchAccountNumberInvalidException,
	AdPRFCoaGlVarianceAccountNotFoundException,
	GlobalRecordInvalidException,
	GlobalNoRecordFoundException {

		Debug.print("InvBranchStockTransferOutEntryControllerBean saveInvBstEntry");

		LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
		LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		LocalAdApprovalHome adApprovalHome = null;
		LocalAdAmountLimitHome adAmountLimitHome = null;
		LocalAdApprovalUserHome adApprovalUserHome = null;
		LocalAdApprovalQueueHome adApprovalQueueHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;

		try {

			invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
			adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
			adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
			adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}
		//TODO: Start here.
		try {
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalInvBranchStockTransfer invBranchStockTransfer = null;

			// validate if branch stock transfer is already deleted
			try {

				if (details.getBstCode() != null) {

					invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(details.getBstCode());

					if (details.getBstVoid() == EJBCommon.TRUE) {

						invBranchStockTransfer.setBstVoid(EJBCommon.TRUE);
						return invBranchStockTransfer.getBstCode();
					}

				}

			} catch (FinderException ex) {

				throw new GlobalRecordAlreadyDeletedException();

			}

			// validate if branch stock transfer is already posted, void, approved or pending
			if (details.getBstCode() != null) {

				if (invBranchStockTransfer.getBstApprovalStatus() != null) {

					if (invBranchStockTransfer.getBstApprovalStatus().equals("APPROVED") ||
							invBranchStockTransfer.getBstApprovalStatus().equals("N/A")) {

						throw new GlobalTransactionAlreadyApprovedException();


					} else if (invBranchStockTransfer.getBstApprovalStatus().equals("PENDING")) {

						throw new GlobalTransactionAlreadyPendingException();

					}

				}

				if (invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE) {

					throw new GlobalTransactionAlreadyPostedException();

				}

			}

			LocalInvBranchStockTransfer invBranchExistingStockTransfer = null;

			try {

				invBranchExistingStockTransfer = invBranchStockTransferHome.findByBstNumberAndBrCode(details.getBstNumber(), AD_BRNCH, AD_CMPNY);

			} catch (FinderException ex) {

			}

			// validate if document number is unique and if document number is automatic then set next sequence

			if (details.getBstCode() == null) {

				LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
				LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

				if (invBranchExistingStockTransfer != null) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				try {

					adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV BRANCH STOCK TRANSFER-OUT", AD_CMPNY);

				} catch (FinderException ex) {

				}

				try {

					adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

				}

				if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' &&
						(details.getBstNumber() == null || details.getBstNumber().trim().length() == 0)) {

					while (true) {

						if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

							try {

								invBranchStockTransferHome.findByBstNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

							} catch (FinderException ex) {

								details.setBstNumber(adDocumentSequenceAssignment.getDsaNextSequence());
								adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
								break;

							}

						} else {

							try {

								invBranchStockTransferHome.findByBstNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

							} catch (FinderException ex) {

								details.setBstNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
								adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
								break;

							}

						}

					}

				}

			} else {

				if (invBranchExistingStockTransfer != null &&
						!invBranchExistingStockTransfer.getBstCode().equals(details.getBstCode())) {

					throw new GlobalDocumentNumberNotUniqueException();

				}

				if (invBranchStockTransfer.getBstNumber() != details.getBstNumber() &&
						(details.getBstNumber() == null || details.getBstNumber().trim().length() == 0)) {

					details.setBstNumber(invBranchStockTransfer.getBstNumber());

				}

			}

			// used in checking if branch stock transfer should re-generate distribution records

			boolean isRecalculate = true;

			// create branch stock transfer

			if (details.getBstCode() == null) {

				invBranchStockTransfer = invBranchStockTransferHome.create(details.getBstDate(),
						"OUT", details.getBstNumber(), null, details.getBstTransferOrderNumber(), details.getBstDescription(),
						details.getBstApprovalStatus(), details.getBstPosted(), details.getBstReasonForRejection(),
						details.getBstCreatedBy(), details.getBstDateCreated(), details.getBstLastModifiedBy(),
						details.getBstDateLastModified(), details.getBstApprovedRejectedBy(),
						details.getBstDateApprovedRejected(), details.getBstPostedBy(), details.getBstDatePosted(),
						EJBCommon.FALSE, details.getBstVoid(), AD_BRNCH, AD_CMPNY);

			} else {

				if (bslList.size() != invBranchStockTransfer.getInvBranchStockTransferLines().size() ||
						!(invBranchStockTransfer.getBstDate().equals(details.getBstDate()))) {

					isRecalculate = true;

				} else if (bslList.size() == invBranchStockTransfer.getInvBranchStockTransferLines().size()) {

					Iterator bslIter = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();
					Iterator bslIterList = bslList.iterator();

					while(bslIter.hasNext()) {

						LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) bslIter.next();
						InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) bslIterList.next();


						if (!invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getBslLocationName()) ||
								invBranchStockTransferLine.getBslUnitCost() != mdetails.getBslUnitCost() ||
								invBranchStockTransferLine.getBslQuantity() != mdetails.getBslQuantity() ||
								invBranchStockTransferLine.getBslAmount() != mdetails.getBslAmount() ||
								!invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getBslIiName()) ||
								!invBranchStockTransferLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getBslUomName())) {

							isRecalculate = true;
							break;

						}

						// get item cost
						double COST = 0d;

						try {

							LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
									invBranchStockTransfer.getBstDate(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
									invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(),
									AD_BRNCH, AD_CMPNY);

							COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), this.getGlFcPrecisionUnit(AD_CMPNY));

						} catch (FinderException ex) {

							COST = invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiUnitCost();
						}

						LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), invBranchStockTransferLine.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
						LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);

						COST = EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

						double AMOUNT = 0d;

						AMOUNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantity() * COST, this.getGlFcPrecisionUnit(AD_CMPNY));

						if (invBranchStockTransferLine.getBslUnitCost() != COST) {

							mdetails.setBslUnitCost(COST);
							mdetails.setBslAmount(AMOUNT);

							isRecalculate = true;
							break;

						}

						isRecalculate = false;

					}

				} else {

					isRecalculate = true;

				}

				invBranchStockTransfer.setBstType("OUT");
				invBranchStockTransfer.setBstNumber(details.getBstNumber());
				invBranchStockTransfer.setBstDescription(details.getBstDescription());
				invBranchStockTransfer.setBstDate(details.getBstDate());
				invBranchStockTransfer.setBstApprovalStatus(details.getBstApprovalStatus());
				invBranchStockTransfer.setBstLastModifiedBy(details.getBstLastModifiedBy());
				invBranchStockTransfer.setBstDateLastModified(details.getBstDateLastModified());
				invBranchStockTransfer.setBstReasonForRejection(null);

			}

			LocalInvBranchStockTransfer invBranchStockTransferOrder = null;
			LocalAdBranch adBranch = adBranchHome.findByBrName(details.getBstBranchTo(), AD_CMPNY);
			adBranch.addInvBranchStockTransfer(invBranchStockTransfer);
			// get transfer order branch

			if (type.equalsIgnoreCase("BSOS-MATCHED")){

				// lock corresponding transfer order
				invBranchStockTransferOrder =
					invBranchStockTransferHome.findByBstNumberAndBrCode(invBranchStockTransfer.getBstTransferOrderNumber(),
							invBranchStockTransfer.getAdBranch().getBrCode(), AD_CMPNY);

				invBranchStockTransferOrder.setBstLock(EJBCommon.TRUE);

			}

			LocalInvLocation invLocation = invLocationHome.findByLocName(details.getBstTransitLocation(), AD_CMPNY);
			//invLocation.addInvBranchStockTransfer(invBranchStockTransfer);
			invBranchStockTransfer.setInvLocation(invLocation);

			double ABS_TOTAL_AMOUNT = 0d;

			if (isRecalculate) {

				// remove all branch stock transfer lines

				Iterator i = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();

				short LINE_NUMBER = 0;

				while(i.hasNext()) {

					LINE_NUMBER++;

					LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) i.next();

					LocalInvItemLocation invItemLocation = null;

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(),
								invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);

					} catch(FinderException ex) {

						System.out.println(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName() + " - " + invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
						throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(LINE_NUMBER) + " - " + invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());

					}

					double convertedQuantity = this.convertByUomAndQuantity(
							invBranchStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
							invBranchStockTransferLine.getBslQuantity(), AD_CMPNY);

					invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);

					i.remove();

					invBranchStockTransferLine.remove();

				}

				// remove all distribution records

				i = invBranchStockTransfer.getInvDistributionRecords().iterator();

				while(i.hasNext()) {

					LocalInvDistributionRecord arDistributionRecord = (LocalInvDistributionRecord) i.next();

					i.remove();

					arDistributionRecord.remove();

				}

				// add new branch stock transfer entry lines and distribution record

				byte DEBIT = 0;

				i = bslList.iterator();

				while(i.hasNext()) {

					InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) i.next();

					LocalInvItemLocation invItemLocation = null;

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								mdetails.getBslLocationName(),
								mdetails.getBslIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						System.out.println("Line " + mdetails.getBslIiName() + " - " + mdetails.getBslLocationName());
						throw new GlobalInvItemLocationNotFoundException(mdetails.getBslIiName() + " - " + mdetails.getBslLocationName());

					}

					LocalInvItemLocation invItemTransitLocation = null;

					try {

						invItemTransitLocation = invItemLocationHome.findByLocNameAndIiName(
								details.getBstTransitLocation(),
								mdetails.getBslIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						System.out.println(mdetails.getBslIiName() + "-" + String.valueOf(details.getBstTransitLocation()));
						throw new GlobalInvItemLocationNotFoundException("Transit Location " + String.valueOf(details.getBstTransitLocation()));

					}

					if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
						// start date validation
						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								invBranchStockTransfer.getBstDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
					}


					LocalInvBranchStockTransferLine invBranchStockTransferLine = this.addInvBslEntryOut(mdetails, invBranchStockTransfer, AD_CMPNY);


					// add distribution records

					double COST = 0d;

					try {

						LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
								invBranchStockTransfer.getBstDate(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(),
								invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(),
								AD_BRNCH, AD_CMPNY);

						COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), this.getGlFcPrecisionUnit(AD_CMPNY));

					} catch (FinderException ex) {

						COST = invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiUnitCost();
					}

					LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), invBranchStockTransferLine.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
					LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);

					COST = EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

					double AMOUNT = 0d;

					AMOUNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantity() * COST, this.getGlFcPrecisionUnit(AD_CMPNY));

					// check branch mapping

					LocalAdBranchItemLocation adBranchItemLocation = null;

					try{

						adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
								invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);


					} catch (FinderException ex) {

					}

					LocalGlChartOfAccount glChartOfAccount = null;

					if (adBranchItemLocation == null) {

						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
								invItemLocation.getIlGlCoaInventoryAccount());

					} else {

						glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
								adBranchItemLocation.getBilCoaGlInventoryAccount());

					}

					// add dr for inventory

					this.addInvDrEntryOut(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE,
							Math.abs(AMOUNT), glChartOfAccount.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);


					// check branch mapping for transit location

					LocalAdBranchItemLocation adBranchItemTransitLocation = null;

					try{

						adBranchItemTransitLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
								invItemTransitLocation.getIlCode(), AD_BRNCH, AD_CMPNY);


					} catch (FinderException ex) {

					}

					LocalGlChartOfAccount glChartOfAccountTransit = null;

					if (adBranchItemTransitLocation == null) {

						glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
								invItemTransitLocation.getIlGlCoaInventoryAccount());

					} else {

						glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
								adBranchItemTransitLocation.getBilCoaGlInventoryAccount());

					}

					// add dr for inventory transit location

					this.addInvDrEntryOut(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE,
							Math.abs(AMOUNT), glChartOfAccountTransit.getCoaCode(), invBranchStockTransfer, AD_BRNCH, AD_CMPNY);


					ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);

					// set ilCommittedQuantity

					double convertedQuantity = this.convertByUomAndQuantity(
							invBranchStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
							invBranchStockTransferLine.getBslQuantity(), AD_CMPNY);

					invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

					// check branch to mapping

					LocalAdBranchItemLocation adBranchToItemLocation = null;

					try{

						adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
								invItemTransitLocation.getIlCode(), adBranch.getBrCode(), AD_CMPNY);


					} catch (FinderException ex) {

						try{
							adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
									invItemTransitLocation.getIlCode(), AD_BRNCH, AD_CMPNY);

							// create new branch item location
							LocalAdBranchItemLocation newAdBranchItemLocation = adBranchItemLocationHome.create(null, null, adBranchItemLocation.getBilReorderPoint(),
									adBranchItemLocation.getBilReorderQuantity(), adBranchItemLocation.getBilCoaGlSalesAccount(), adBranchItemLocation.getBilCoaGlInventoryAccount(),
									adBranchItemLocation.getBilCoaGlCostOfSalesAccount(), adBranchItemLocation.getBilCoaGlWipAccount(), adBranchItemLocation.getBilCoaGlAccruedInventoryAccount(), adBranchItemLocation.getBilCoaGlSalesReturnAccount(),
									adBranchItemLocation.getBilSubjectToCommission(), adBranchItemLocation.getBilHist1Sales(), adBranchItemLocation.getBilHist2Sales(), adBranchItemLocation.getBilProjectedSales(),
									adBranchItemLocation.getBilDeliveryTime(), adBranchItemLocation.getBilDeliveryBuffer(), adBranchItemLocation.getBilOrderPerYear(),
									adBranchItemLocation.getBilItemDownloadStatus(), adBranchItemLocation.getBilLocationDownloadStatus(), adBranchItemLocation.getBilItemLocationDownloadStatus(), AD_CMPNY);

							//invItemLocation.addAdBranchItemLocation(adBranchItemLocation);
							adBranchItemLocation.setInvItemLocation(invItemLocation);
							//adBranch.addAdBranchItemLocation(adBranchItemLocation);
							adBranchItemLocation.setAdBranch(adBranch);

						}catch (FinderException ex2) {

							System.out.println("AD_BR_IL not found: " + invItemTransitLocation.getInvItem().getIiName() + " - " + invItemTransitLocation.getInvLocation().getLocName());
							throw new GlobalNoRecordFoundException(invItemTransitLocation.getInvItem().getIiName() + " - " + invItemTransitLocation.getInvLocation().getLocName());
						}

					}

				}

			} else {

				Iterator i = bslList.iterator();

				while(i.hasNext()) {

					InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) i.next();

					LocalInvItemLocation invItemLocation = null;

					try {

						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								mdetails.getBslLocationName(),
								mdetails.getBslIiName(), AD_CMPNY);

					} catch (FinderException ex) {

						System.out.println("Line " + mdetails.getBslIiName() + " - " + mdetails.getBslLocationName());
						throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(mdetails.getBslLineNumber() + " - " + mdetails.getBslLocationName()));

					}

					if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
						//	start date validation

						Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
								invBranchStockTransfer.getBstDate(), invItemLocation.getInvItem().getIiName(),
								invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
						if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
					}


					i = invBranchStockTransfer.getInvDistributionRecords().iterator();

					while(i.hasNext()) {

						LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();

						if(distributionRecord.getDrDebit() == 1) {

							ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();

						}

					}

				}

			}

			//	generate approval status

			String INV_APPRVL_STATUS = null;

			if (!isDraft) {

				LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);

				// check if inv stock transfer approval is enabled

				if (adApproval.getAprEnableInvBranchStockTransfer() == EJBCommon.FALSE) {

					INV_APPRVL_STATUS = "N/A";

				} else {

					// check if invoice is self approved

					LocalAdAmountLimit adAmountLimit = null;

					try {

						adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("INV BRANCH STOCK TRANSFER", "REQUESTER", details.getBstLastModifiedBy(), AD_CMPNY);

					} catch (FinderException ex) {

						throw new GlobalNoApprovalRequesterFoundException();

					}

					if (ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit()) {

						INV_APPRVL_STATUS = "N/A";

					} else {

						// for approval, create approval queue

						Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("INV BRANCH STOCK TRANSFER", adAmountLimit.getCalAmountLimit(), AD_CMPNY);

						if (adAmountLimits.isEmpty()) {

							Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);

							if (adApprovalUsers.isEmpty()) {

								throw new GlobalNoApprovalApproverFoundException();

							}

							Iterator j = adApprovalUsers.iterator();

							while (j.hasNext()) {

								LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

								LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV BRANCH STOCK TRANSFER",
										invBranchStockTransfer.getBstCode(), invBranchStockTransfer.getBstNumber(), invBranchStockTransfer.getBstDate(),
										adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

								adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

							}

						} else {

							boolean isApprovalUsersFound = false;

							Iterator i = adAmountLimits.iterator();

							while (i.hasNext()) {

								LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();

								if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {

									Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER",
											adAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV BRANCH STOCK TRANSFER",
												invBranchStockTransfer.getBstCode(), invBranchStockTransfer.getBstNumber(), invBranchStockTransfer.getBstDate(),
												adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								} else if (!i.hasNext()) {

									Collection adApprovalUsers =
										adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);

									Iterator j = adApprovalUsers.iterator();

									while (j.hasNext()) {

										isApprovalUsersFound = true;

										LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();

										LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV BRANCH STOCK TRANSFER",
												invBranchStockTransfer.getBstCode(), invBranchStockTransfer.getBstNumber(), invBranchStockTransfer.getBstDate(),
												adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);

										adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);

									}

									break;

								}

								adAmountLimit = adNextAmountLimit;

							}

							if (!isApprovalUsersFound) {

								throw new GlobalNoApprovalApproverFoundException();

							}

						}

						INV_APPRVL_STATUS = "PENDING";

					}

				}

			}

			if(INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A")) {

				/*this.executeInvBstPost(invBranchStockTransfer.getBstCode(), invBranchStockTransfer.getBstLastModifiedBy(),
                        AD_BRNCH, AD_CMPNY);*/

			}

			// set stock transfer approval status

			invBranchStockTransfer.setBstApprovalStatus(INV_APPRVL_STATUS);

			return invBranchStockTransfer.getBstCode();


		} catch (GlobalRecordAlreadyDeletedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyApprovedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPendingException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalTransactionAlreadyPostedException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalRequesterFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalNoApprovalApproverFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} catch (GlobalInvItemLocationNotFoundException ex) {

			ctx.setRollbackOnly();
			throw ex;

		} /*catch (GlJREffectiveDateNoPeriodExistException ex) {

            ctx.setRollbackOnly();
            throw ex;

        } catch (GlJREffectiveDatePeriodClosedException ex) {

            ctx.setRollbackOnly();
            throw ex;

        } catch (GlobalJournalNotBalanceException ex) {

            ctx.setRollbackOnly();
            throw ex;

        } */catch (GlobalInventoryDateException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlobalDocumentNumberNotUniqueException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch(GlobalBranchAccountNumberInvalidException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } /*catch (AdPRFCoaGlVarianceAccountNotFoundException ex){

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (GlobalRecordInvalidException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } */catch (GlobalNoRecordFoundException ex) {

        	ctx.setRollbackOnly();
        	throw ex;

        } catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	ctx.setRollbackOnly();
        	throw new EJBException(ex.getMessage());

        }
	}


	/**
	 * @ejb:interface-method
	 * */
	public String[] getInvBranchStockTransferAllIncoming(String BR_BRNCH_CODE, Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean getInvBranchStockTransferAllIncoming");

		LocalAdBranchHome adBranchHome = null;
		LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome= null;

		ArrayList AllIncomingBstList= new ArrayList();

		try {

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);

		} catch(Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		Integer AD_BRNCH = new Integer(0);
		try {
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();
		} catch(Exception ex) {
			throw new EJBException(ex.getMessage());
		}

		try {

			Collection invBranchStockTransfers = invBranchStockTransferHome.findPostedIncomingBstByAdBranchAndBstAdCompany(AD_BRNCH, AD_CMPNY);
			Iterator i = invBranchStockTransfers.iterator();

			int ctr =1, size = invBranchStockTransfers.size(), all=0;

			while(i.hasNext()) {
				LocalInvBranchStockTransfer invBranchStockTransfer = (LocalInvBranchStockTransfer)i.next();
				try{
					LocalInvBranchStockTransfer incomingBST = invBranchStockTransferHome.findInByBstOutNumberAndBrCode(invBranchStockTransfer.getBstNumber(),AD_BRNCH,AD_CMPNY);
					all++;
				} catch (Exception ex) {
					AllIncomingBstList.add(invBranchStockTransfer);
				}

				System.out.println("Status: Processing " + (ctr++) + "/" + size);

			}

			System.out.println("Sending " + all + "/" + invBranchStockTransfers.size());

		} catch(Exception ex) {
			throw new EJBException(ex.getMessage());
		}

		String[] results = new String[AllIncomingBstList.size()];
		Iterator bstIter =  AllIncomingBstList.iterator();
		int ctr = 0;

		while(bstIter.hasNext()){
			LocalInvBranchStockTransfer invBranchStockTransfer = (LocalInvBranchStockTransfer)bstIter.next();;
			results[ctr++] = bstRowEncode(invBranchStockTransfer);
		}

		return results;
	}

	private String bstRowEncode(LocalInvBranchStockTransfer invBranchStockTransfer)
	{

		//Debug.print("InvItemSyncControllerBean poRowEncode");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer encodedResult = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_OUTPUT);

		// Start separator
		encodedResult.append(separator);

		// BST Code
		encodedResult.append(invBranchStockTransfer.getBstCode());
		encodedResult.append(separator);

		// BST date
		encodedResult.append(sdf.format(invBranchStockTransfer.getBstDate()));
		encodedResult.append(separator);

		// BST Type
		encodedResult.append(invBranchStockTransfer.getBstType());
		encodedResult.append(separator);

		// Number
		encodedResult.append(invBranchStockTransfer.getBstNumber());
		encodedResult.append(separator);

		// Description
		encodedResult.append(invBranchStockTransfer.getBstDescription());
		encodedResult.append(separator);

		// Location
		encodedResult.append(invBranchStockTransfer.getInvLocation().getLocName());
		encodedResult.append(separator);

		// Branch From
		encodedResult.append(invBranchStockTransfer.getBstAdBranch());
		encodedResult.append(separator);

		// end separator
		encodedResult.append(separator);

		String lineSeparator = "~";

		// begin lineSeparator
		encodedResult.append(lineSeparator);

		Iterator i = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();

		while (i.hasNext()) {

			LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)i.next();

			// begin separator
			encodedResult.append(separator);

			// Line Code
			encodedResult.append(invBranchStockTransferLine.getBslCode());
			encodedResult.append(separator);

			// Quantity
			encodedResult.append(invBranchStockTransferLine.getBslQuantity());
			encodedResult.append(separator);

			// Unit Cost
			encodedResult.append(invBranchStockTransferLine.getBslUnitCost());
			encodedResult.append(separator);

			// Amount
			encodedResult.append(invBranchStockTransferLine.getBslAmount());
			encodedResult.append(separator);

			// Item Location
			encodedResult.append(invBranchStockTransferLine.getInvItemLocation().getIlCode());
			encodedResult.append(separator);

			// Item Name
			encodedResult.append(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
			encodedResult.append(separator);

			// Location Name
			encodedResult.append(invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
			encodedResult.append(separator);

			// Unit of Measure
			encodedResult.append(invBranchStockTransferLine.getInvUnitOfMeasure().getUomName());
			encodedResult.append(separator);

			// BST Code
			encodedResult.append(invBranchStockTransfer.getBstCode());
			encodedResult.append(separator);

			// begin lineSeparator
			encodedResult.append(lineSeparator);
		}

		return encodedResult.toString();

	}

	/**
	 * @ejb:interface-method
	 * */
	public String[] getInvStockOnHand(String BR_BRNCH_CODE,String II_IL_LOCATION, Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean getInvStockOnHand");
		LocalAdBranchItemLocationHome invStockOnHandHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invStockOnHandHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
		}catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		Integer AD_BRNCH = new Integer(0);
		LocalAdCompany adCompany = null;

		try {
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch(Exception ex) {
			throw new EJBException(ex.getMessage());
		}

		Collection invStockOnHands = null;
		String[] results =null;
		Date currentDate = new Date();

		try {

			invStockOnHands = invStockOnHandHome.findEnabledIiByIiNewAndUpdated(AD_BRNCH,II_IL_LOCATION,AD_CMPNY,'U','D','X');

			results = new String[invStockOnHands.size()];

			Iterator i = invStockOnHands.iterator();

			int ctr = 0;

			while (i.hasNext()) {

				LocalAdBranchItemLocation invStockOnHand = (LocalAdBranchItemLocation) i.next();

				char separator = EJBCommon.SEPARATOR;
				StringBuffer encodedResult = new StringBuffer();

				try {
					LocalInvCosting invStockOnHandCost =
						invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
								currentDate, invStockOnHand.getInvItemLocation().getInvItem().getIiName(), invStockOnHand.getInvItemLocation().getInvLocation().getLocName(),
								invStockOnHand.getAdBranch().getBrCode(), AD_CMPNY);

					// Start Separator
					encodedResult.append(separator);

					// Qty
					encodedResult.append(invStockOnHandCost.getCstRemainingQuantity());
					encodedResult.append(separator);

					// Value
					encodedResult.append(invStockOnHandCost.getCstRemainingValue());
					encodedResult.append(separator);

					// Average Cost
					encodedResult.append(invStockOnHandCost.getCstRemainingQuantity() == 0 ? 0 :
						EJBCommon.roundIt(invStockOnHandCost.getCstRemainingValue() / invStockOnHandCost.getCstRemainingQuantity(), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					encodedResult.append(separator);

					// Item
					encodedResult.append(invStockOnHand.getInvItemLocation().getInvItem().getIiCode());
					encodedResult.append(separator);

					// Location
					encodedResult.append(invStockOnHand.getInvItemLocation().getInvLocation().getLocCode());
					encodedResult.append(separator);

				} catch (FinderException ex){
					encodedResult =  new StringBuffer();

					// Start Separator
					encodedResult.append(separator);

					// Qty
					encodedResult.append(0);
					encodedResult.append(separator);

					// Value
					encodedResult.append(0);
					encodedResult.append(separator);

					// Average Cost
					encodedResult.append(0);
					encodedResult.append(separator);

					// Item
					encodedResult.append(invStockOnHand.getInvItemLocation().getInvItem().getIiCode());
					encodedResult.append(separator);

					// Location
					encodedResult.append(invStockOnHand.getInvItemLocation().getInvLocation().getLocCode());
					encodedResult.append(separator);
				}

				results[ctr++] = encodedResult.toString();
				encodedResult =  new StringBuffer();

			}


		} catch (FinderException ex)	{

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

		return results;
	}

	/**
	 * @ejb:interface-method
	 * */
	public String[] getInvStockOnHandWithExpiryDate(String BR_BRNCH_CODE,String II_IL_LOCATION, Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean getInvStockOnHandWithExpiryDate");
		LocalAdBranchItemLocationHome invStockOnHandHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invStockOnHandHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
		}catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		Integer AD_BRNCH = new Integer(0);
		LocalAdCompany adCompany = null;

		try {
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();

			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch(Exception ex) {
			throw new EJBException(ex.getMessage());
		}

		Collection invStockOnHands = null;
		String[] results =null;
		Date currentDate = new Date();

		try {

			invStockOnHands = invStockOnHandHome.findEnabledIiByIiNewAndUpdated(AD_BRNCH,II_IL_LOCATION,AD_CMPNY,'U','D','X');

			results = new String[invStockOnHands.size()];

			Iterator i = invStockOnHands.iterator();

			int ctr = 0;

			while (i.hasNext()) {

				LocalAdBranchItemLocation invStockOnHand = (LocalAdBranchItemLocation) i.next();

				char separator = EJBCommon.SEPARATOR;
				StringBuffer encodedResult = new StringBuffer();

				try {
					LocalInvCosting invStockOnHandCost =
						invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
								currentDate, invStockOnHand.getInvItemLocation().getInvItem().getIiName(), invStockOnHand.getInvItemLocation().getInvLocation().getLocName(),
								invStockOnHand.getAdBranch().getBrCode(), AD_CMPNY);

					// Start Separator
					encodedResult.append(separator);

					// Qty
					encodedResult.append(invStockOnHandCost.getCstRemainingQuantity());
					encodedResult.append(separator);

					// Value
					encodedResult.append(invStockOnHandCost.getCstRemainingValue());
					encodedResult.append(separator);

					// Average Cost
					encodedResult.append(invStockOnHandCost.getCstRemainingQuantity() == 0 ? 0 :
						EJBCommon.roundIt(invStockOnHandCost.getCstRemainingValue() / invStockOnHandCost.getCstRemainingQuantity(), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					encodedResult.append(separator);

					// Item
					encodedResult.append(invStockOnHand.getInvItemLocation().getInvItem().getIiCode());
					encodedResult.append(separator);

					// Location
					encodedResult.append(invStockOnHand.getInvItemLocation().getInvLocation().getLocCode());
					encodedResult.append(separator);

					// Expiry Date
					encodedResult.append("~");
					encodedResult.append(invStockOnHandCost.getCstExpiryDate());
					System.out.println("(invStockOnHandCost.getCstExpiryDate(): " + invStockOnHandCost.getCstExpiryDate());
					encodedResult.append("~");

				} catch (FinderException ex){
					encodedResult =  new StringBuffer();

					// Start Separator
					encodedResult.append(separator);

					// Qty
					encodedResult.append(0);
					encodedResult.append(separator);

					// Value
					encodedResult.append(0);
					encodedResult.append(separator);

					// Average Cost
					encodedResult.append(0);
					encodedResult.append(separator);

					// Item
					encodedResult.append(invStockOnHand.getInvItemLocation().getInvItem().getIiCode());
					encodedResult.append(separator);

					// Location
					encodedResult.append(invStockOnHand.getInvItemLocation().getInvLocation().getLocCode());
					encodedResult.append(separator);

					// Expiry Date
					encodedResult.append("~");
					encodedResult.append("~");
				}

				results[ctr++] = encodedResult.toString();
				encodedResult =  new StringBuffer();

			}


		} catch (FinderException ex)	{

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

		return results;
	}


	/**
	 * @ejb:interface-method
	 * */
	public String[] getInvStockOnHandOnly(String[] invUploadOrig, Integer AD_CMPNY)
	{


		LocalAdBranchItemLocationHome invStockOnHandHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invStockOnHandHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
		}catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		Debug.print("InvItemSyncControllerBean getInvStockOnHandOnly");

		//Debug.print("InvItemSyncControllerBean poRowDecode");
		String[] results =null;
		int ctr = 0;
		char separator = EJBCommon.SEPARATOR;
		int lengthOriginal = invUploadOrig.length;
		results = new String[lengthOriginal];

		for (int i = 0; i < lengthOriginal; i++) {
			// Remove first $ character
			String invUpload = invUploadOrig[i].substring(1);

			// Item
			int start = 0;
			int nextIndex = invUpload.indexOf(separator, start);
			int length = nextIndex - start;
			String Item =(invUpload.substring(start, start + length));

			// Qty
			start = nextIndex + 1;
			nextIndex = invUpload.indexOf(separator, start);
			length = nextIndex - start;
			double qty = Double.parseDouble((invUpload.substring(start, start + length)));

			// Date
			start = nextIndex + 1;
			nextIndex = invUpload.indexOf(separator, start);
			length = nextIndex - start;
			String datetxn = (invUpload.substring(start, start + length));
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date dateTransact = null;
			try
			{
				dateTransact = sdf.parse(datetxn);
				System.out.println("dateTransact = " + sdf.format(dateTransact));
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			// Location
			start = nextIndex + 1;
			nextIndex = invUpload.indexOf(separator, start);
			length = nextIndex - start;
			String location = (invUpload.substring(start, start + length));

			// Branch
			start = nextIndex + 1;
			nextIndex = invUpload.indexOf(separator, start);
			length = nextIndex - start;
			String branch = (invUpload.substring(start, start + length));

			// Open Bottle
			start = nextIndex + 1;
			nextIndex = invUpload.indexOf(separator, start);
			length = nextIndex - start;
			String openBottle = (invUpload.substring(start, start + length));

			// Orig Qty
			start = nextIndex + 1;
			nextIndex = invUpload.indexOf(separator, start);
			length = nextIndex - start;
			double origQty = Double.parseDouble((invUpload.substring(start, start + length)));

			System.out.println("Item: " + Item);
			System.out.println("qty: " + qty);
			System.out.println("origQty: " + origQty);
			System.out.println("datetxn: " + datetxn);
			System.out.println("location: " + location);
			System.out.println("branch: " + branch);

			Integer AD_BRNCH = new Integer(0);
			LocalAdCompany adCompany = null;

			try {
				System.out.println("Check A: " + branch);
				LocalAdBranch adBranch = adBranchHome.findByBrName(branch, AD_CMPNY);
				System.out.println("Check B");
				AD_BRNCH = adBranch.getBrCode();
				System.out.println("AD_BRNCH: " + AD_BRNCH);
				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			} catch(Exception ex) {
				throw new EJBException(ex.getMessage());
			}
			Collection invStockOnHands = null;

			Date currentDate = new Date();

			System.out.println("currentDate: " + currentDate);
			System.out.println("datetxn: " + datetxn);
			System.out.println("dateTransact: " + dateTransact);
			//char separator2 = EJBCommon.SEPARATOR;
			StringBuffer encodedResult = new StringBuffer();
			StringBuffer encodedResult2 = new StringBuffer();
			try {
				LocalInvCosting invStockOnHandCost =
					invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
							dateTransact, Item, location, AD_BRNCH, AD_CMPNY);
				double qtyNew = 0d;
				double qtyNew2 = 0d;
				System.out.println("invStockOnHandCost.getCstRemainingQuantity(): " + invStockOnHandCost.getCstRemainingQuantity());
				System.out.println("qty: " + qty);
				System.out.println("origQty: " + qty);
				System.out.println("**************************************");
				System.out.println("openBottle: " + openBottle);
				System.out.println("**************************************");

				if(origQty!=qty && invStockOnHandCost.getCstRemainingQuantity()>origQty){
					qty = origQty;
				}
				System.out.println("qty: " + qty);
				qtyNew2 = qty;
				if(openBottle=="T"){
					qty = qty *-1;
				}
				if(invStockOnHandCost.getCstRemainingQuantity()<0 && invStockOnHandCost.getCstRemainingQuantity()<qty){
					System.out.println("CHECKPOINT A");
					qtyNew = ((invStockOnHandCost.getCstRemainingQuantity() - qty) * -1) + invStockOnHandCost.getCstRemainingQuantity();
				}else if(invStockOnHandCost.getCstRemainingQuantity()>=0 && invStockOnHandCost.getCstRemainingQuantity()<qty){
					System.out.println("CHECKPOINT B");
					qtyNew = qty - invStockOnHandCost.getCstRemainingQuantity();
				}
				System.out.println("qtyNew: " + qtyNew);

				if(openBottle.equals("T")){
					if(qtyNew!=0){
						qtyNew = qtyNew *-1;
					}else{
						qtyNew = qtyNew2;
					}
				}
				System.out.println("qty: " + qty);
				System.out.println("qtyNew B4 CHeck C Before: " + qtyNew);
				if(openBottle.equals("V") && invStockOnHandCost.getCstRemainingQuantity()<qtyNew){
					qtyNew = qtyNew2;
				}
				System.out.println("qtyNew B4 CHeck C: " + qtyNew);
				if(qtyNew!=0){
					System.out.println("CHECKPOINT C");
					// Start Separator
					encodedResult.append(separator);

					// Qty
					encodedResult.append(qtyNew);
					encodedResult.append(separator);

					// Item
					encodedResult.append(Item);
					encodedResult.append(separator);

					// Date
					encodedResult.append(datetxn);
					encodedResult.append(separator);

					// Location
					encodedResult.append(location);
					encodedResult.append(separator);

					// Branch
					encodedResult.append(branch);
					encodedResult.append(separator);

					// Value
					encodedResult.append(invStockOnHandCost.getCstRemainingValue());
					encodedResult.append(separator);

					// Average Cost
					encodedResult.append(invStockOnHandCost.getCstRemainingQuantity() == 0 ? 0 :
						EJBCommon.roundIt(invStockOnHandCost.getCstRemainingValue() / invStockOnHandCost.getCstRemainingQuantity(), adCompany.getGlFunctionalCurrency().getFcPrecision()));
					encodedResult.append(separator);

					// Unit Cost
					encodedResult.append(invStockOnHandCost.getInvItemLocation().getInvItem().getIiUnitCost());
					encodedResult.append(separator);

					// UOM
					encodedResult.append(invStockOnHandCost.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomShortName());
					encodedResult.append(separator);

					System.out.println(ctr + " CHECKPOINT D: " + encodedResult.toString());
					results[ctr++] = encodedResult.toString();
					encodedResult =  new StringBuffer();
					System.out.println("******************BREAK********************");
				}



				// Expiry Date
				/*encodedResult.append("~");
					encodedResult.append(invStockOnHandCost.getCstExpiryDate());
					System.out.println("(invStockOnHandCost.getCstExpiryDate(): " + invStockOnHandCost.getCstExpiryDate());
					encodedResult.append("~");
				 */
			} catch (FinderException ex){
				Debug.printStackTrace(ex);
				throw new EJBException(ex.getMessage());
			}


		}
		System.out.println(results.length);
		return results;
	}

	/**
	 * @ejb:interface-method
	 * */
	public String[] getLastPo(String invoiceDate, String location, String[] invUploadOrig, String AD_BRNCH, Integer AD_CMPNY)
	{


		LocalAdBranchItemLocationHome invStockOnHandHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		// Initialize EJB Home

		try {
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invStockOnHandHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
		}catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		Debug.print("InvItemSyncControllerBean getInvStockOnHandOnly");

		//Debug.print("InvItemSyncControllerBean poRowDecode");
		String[] results =null;
		int ctr = 0;
		char separator = EJBCommon.SEPARATOR;
		int lengthOriginal = invUploadOrig.length;
		results = new String[lengthOriginal];
		double unitCost =0;
		String Item ="";
		double qty = 0;
		double qty2 = 0;
		int branch = 0;

		for (int i = 0; i < lengthOriginal; i++) {
			// Remove first $ character
			String invUpload = invUploadOrig[i].substring(1);

			// Item
			int start = 0;
			int nextIndex = invUpload.indexOf(separator, start);
			int length = nextIndex - start;
			Item =(invUpload.substring(start, start + length));

			// Qty
			start = nextIndex + 1;
			nextIndex = invUpload.indexOf(separator, start);
			length = nextIndex - start;
			qty = Double.parseDouble((invUpload.substring(start, start + length)));

			System.out.println("Item: " + Item);
			System.out.println("qty: " + qty);
			System.out.println("location: " + location);
			System.out.println("branch: " + AD_BRNCH);
			System.out.println("AD_CMPNY: " + AD_CMPNY);
			LocalAdCompany adCompany = null;

			try {
				System.out.println("Check A: " + AD_BRNCH);
				LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(AD_BRNCH, AD_CMPNY);
				System.out.println("Check B");
				branch = adBranch.getBrCode();
				System.out.println("AD_BRNCH: " + AD_BRNCH);
				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			} catch(Exception ex) {
				throw new EJBException(ex.getMessage());
			}
			Collection invStockOnHands = null;

			Date currentDate = new Date();

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			sdf.setLenient(false);
			Date datetxn = null;
			try {
				datetxn = sdf.parse(invoiceDate);
				System.out.println("Date-" + datetxn);
			} catch (Exception ex) {
				System.out.println("Error On PO Date: " + ex.getMessage());
				//throw ex;
			}

			//char separator2 = EJBCommon.SEPARATOR;
			/*StringBuffer encodedResult = new StringBuffer();
			StringBuffer encodedResult2 = new StringBuffer();*/
			try {
				try{
					LocalApPurchaseOrderLine apPurchaseOrderLine =
						apPurchaseOrderLineHome.getByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPostedAndLessThanEqualPoDate(
								Item, location, EJBCommon.FALSE, EJBCommon.TRUE, datetxn, branch, AD_CMPNY);

					unitCost = unitCost+ (apPurchaseOrderLine.getPlUnitCost() * qty);
				}catch(Exception e){
					System.out.println("CATCH: " + Item);
					LocalInvItem invItem = invItemHome.findByIiName(Item.trim(), AD_CMPNY);

					unitCost = unitCost+ (invItem.getIiUnitCost() * qty);
				}

				System.out.println("CHECKPOINT C");
				qty2 = qty2 + qty;



				//encodedResult =  new StringBuffer();
				System.out.println("******************BREAK********************");

			} catch (FinderException ex){
				Debug.printStackTrace(ex);
				throw new EJBException(ex.getMessage() + ". ITEM NOT FOUND: " + Item);
			}


		}
		StringBuffer encodedResult = new StringBuffer();
		// Start Separator
		encodedResult.append(separator);

		// Date
		encodedResult.append(invoiceDate);
		encodedResult.append(separator);

		// Qty
		encodedResult.append(qty2);
		encodedResult.append(separator);

		// Unit Cost
		encodedResult.append(unitCost);
		encodedResult.append(separator);

		System.out.println(ctr + " CHECKPOINT D: " + encodedResult.toString());
		results[ctr++] = encodedResult.toString();

		System.out.println(results.length);
		return results;
	}

	/**
	 * @ejb:interface-method
	 * */
	public String[] getLastPoPerItem(String invoiceDate, String location, String[] invUploadOrig, String AD_BRNCH, Integer AD_CMPNY)
	{


		LocalAdBranchItemLocationHome invStockOnHandHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		// Initialize EJB Home

		try {
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invStockOnHandHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
		}catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}


		Debug.print("InvItemSyncControllerBean getInvStockOnHandOnly");

		//Debug.print("InvItemSyncControllerBean poRowDecode");
		String[] results =null;
		int ctr = 0;
		char separator = EJBCommon.SEPARATOR;
		int lengthOriginal = invUploadOrig.length;
		results = new String[lengthOriginal];
		double unitCost =0;
		String Item ="";
		String ItemOutput ="";
		String ItemDesc ="";
		double qty = 0;
		double qty2 = 0;
		int branch = 0;

		for (int i = 0; i < lengthOriginal; i++) {
			// Remove first $ character
			String invUpload = invUploadOrig[i].substring(1);

			// Item
			int start = 0;
			int nextIndex = invUpload.indexOf(separator, start);
			int length = nextIndex - start;
			Item =(invUpload.substring(start, start + length));

			// Qty
			start = nextIndex + 1;
			nextIndex = invUpload.indexOf(separator, start);
			length = nextIndex - start;
			qty = Double.parseDouble((invUpload.substring(start, start + length)));

			System.out.println("Item: " + Item);
			System.out.println("qty: " + qty);
			System.out.println("location: " + location);
			System.out.println("branch: " + AD_BRNCH);
			System.out.println("AD_CMPNY: " + AD_CMPNY);
			LocalAdCompany adCompany = null;

			try {
				System.out.println("Check A: " + AD_BRNCH);
				LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(AD_BRNCH, AD_CMPNY);
				System.out.println("Check B");
				branch = adBranch.getBrCode();
				System.out.println("AD_BRNCH: " + AD_BRNCH);
				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			} catch(Exception ex) {
				throw new EJBException("Branch Not Found");
			}
			Collection invStockOnHands = null;

			Date currentDate = new Date();

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			sdf.setLenient(false);
			Date datetxn = null;
			try {
				datetxn = sdf.parse(invoiceDate);
				System.out.println("Date-" + datetxn);
			} catch (Exception ex) {
				System.out.println("Error On PO Date: " + ex.getMessage());
				//throw ex;
			}

			//char separator2 = EJBCommon.SEPARATOR;
			/*StringBuffer encodedResult = new StringBuffer();
			StringBuffer encodedResult2 = new StringBuffer();*/
			try {
				try{
					LocalApPurchaseOrderLine apPurchaseOrderLine =
						apPurchaseOrderLineHome.getByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPostedAndLessThanEqualPoDate(
								Item, location, EJBCommon.FALSE, EJBCommon.TRUE, datetxn, branch, AD_CMPNY);

					unitCost = (apPurchaseOrderLine.getPlUnitCost());
					ItemOutput = apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName();
					ItemDesc = apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription();

				}catch(Exception e){
					System.out.println("CATCH: " + Item);
					LocalInvItem invItem = invItemHome.findByIiName(Item.trim(), AD_CMPNY);

					unitCost = (invItem.getIiUnitCost());
					ItemOutput = invItem.getIiName();
					ItemDesc = invItem.getIiDescription();
				}

				System.out.println("CHECKPOINT C");
				qty2 = qty2 + qty;



				StringBuffer encodedResult = new StringBuffer();
				// Start Separator
				encodedResult.append(separator);

				// Item Name
				encodedResult.append(ItemOutput);
				encodedResult.append(separator);

				// Item Description
				encodedResult.append(ItemDesc);
				encodedResult.append(separator);

				// Date
				encodedResult.append(invoiceDate);
				encodedResult.append(separator);

				// Qty
				encodedResult.append(qty);
				encodedResult.append(separator);
				System.out.println("***qty: " + qty);
				// Unit Cost
				encodedResult.append(unitCost);
				encodedResult.append(separator);

				System.out.println(ctr + " CHECKPOINT D: " + encodedResult.toString());
				results[ctr++] = encodedResult.toString();
				encodedResult =  new StringBuffer();
				System.out.println("******************BREAK********************");

			} catch (FinderException ex){
				/*results = new String[0];
				results[0]="Item not Found: " + Item;*/
				Debug.printStackTrace(ex);
				throw new EJBException("Item Not Found:" +Item);
			}


		}


		System.out.println(results.length);
		return results;
	}

	/**
	 * @ejb:interface-method
	 * */
	public String getCheckInsufficientStocks(String invoiceDateFrom, String invoiceDateTo, String location, String user, String invAdjAccount,
			String transactionType, String AD_BRNCH, Integer AD_CMPNY)
	{


		LocalAdBranchItemLocationHome invStockOnHandHome = null;
		LocalInvItemHome invItemHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalArReceiptHome arReceiptHome = null;
		//LocalArInvoice arInvoice = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalInvAdjustmentHome invAdjustmentHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		// Initialize EJB Home

		try {
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invStockOnHandHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			arReceiptHome = (LocalArReceiptHome)EJBHomeFactory.
			lookUpLocalHome(LocalArReceiptHome.JNDI_NAME, LocalArReceiptHome.class);
		}catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		int count =0;
		double insufficientFix = 0;
		LocalAdCompany adCompany = null;

		int branch = 0;
		try {
			System.out.println("Check A: " + AD_BRNCH);
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(AD_BRNCH, AD_CMPNY);
			System.out.println("Check B");
			branch = adBranch.getBrCode();
			System.out.println("AD_BRNCH: " + AD_BRNCH);
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

		} catch(Exception ex) {
			throw new EJBException(ex.getMessage());
		}

		Debug.print("InvItemSyncControllerBean getCheckInsufficientStocks");

		String [] insufficentItemsToFix = null;
		String results =null;
		//arInvoice = arInvoiceHome.findByPrimaryKey(details.getInvCode());
		try {

			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date dateFrom = null;
			try
			{
				dateFrom = sdf.parse(invoiceDateFrom);
				System.out.println("dateFrom = " + sdf.format(dateFrom));
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			Date dateTo = null;
			try
			{
				dateTo = sdf.parse(invoiceDateTo);
				System.out.println("dateTransact = " + sdf.format(dateTo));
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			System.out.println("transactionType: " + transactionType);

			if(transactionType.equals("Issuance")){
				System.out.println("Issuance");
				Collection invAdjustments = invAdjustmentHome.findUnpostedInvAdjByInvAdjDateRangeByBranch(dateFrom, dateTo, branch, AD_CMPNY);
				HashMap cstMap = null;
				Iterator i = invAdjustments.iterator();
				int runVar = 0;
				while (i.hasNext()) {
					LocalInvAdjustment invAdjustment = (LocalInvAdjustment)i.next();

					System.out.println("*************************CHECKING ISSUANCE INSUFFICIENT STOCKS******************************");

					boolean hasInsufficientItems = false;
					String insufficientItems = "";

					Collection invAdjustmentLineItems = invAdjustment.getInvAdjustmentLines();

					Iterator p = invAdjustmentLineItems.iterator();
					int a =0;
					ArrayList insufficient = new ArrayList();
					ArrayList insufficientQty = new ArrayList();
					ArrayList insufficientUOM = new ArrayList();
					ArrayList insufficientUnitCost = new ArrayList();
					ArrayList itemLocation = new ArrayList();
					runVar++;

					if((invAdjustment.getAdjDate().equals(dateTo) && dateFrom.equals(dateTo)) || runVar==1){
						cstMap = new HashMap();
					}
					while (p.hasNext()) {

						LocalInvAdjustmentLine invAdjustmentLineItem = (LocalInvAdjustmentLine)p.next();
						String II_NM = invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName();
	    				String LOC_NM = invAdjustmentLineItem.getInvItemLocation().getInvLocation().getLocName();
						if(invAdjustmentLineItem.getAlAdjustQuantity()<0){
							//double CURR_QTY = 0;
							if (invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiEnableAutoBuild() == EJBCommon.TRUE && invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

								Collection invBillOfMaterials = invAdjustmentLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

								Iterator j = invBillOfMaterials.iterator();
								Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N01");
								while (j.hasNext()) {
									double CURR_QTY = 0;
									LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

									LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

									LocalInvCosting invBomCosting = null;

									double ILI_QTY = this.convertByUomFromAndItemAndQuantity(
											invAdjustmentLineItem.getInvUnitOfMeasure(),
											invAdjustmentLineItem.getInvItemLocation().getInvItem(),
											Math.abs(invAdjustmentLineItem.getAlAdjustQuantity()), AD_CMPNY);
									Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N02");
									boolean isIlFound = false;


									if(cstMap.containsKey(invItemLocation.getIlCode().toString())){

										isIlFound =  true;
										CURR_QTY = ((Double)cstMap.get(invItemLocation.getIlCode().toString())).doubleValue();

									}else{
										try {

											/*invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(
																arInvoice.getInvDate(), invBillOfMaterial.getBomIiName(),
																invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
											 */
											invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
													invAdjustment.getAdjDate(), invBillOfMaterial.getBomIiName(),
													invItemLocation.getInvLocation().getLocName(), branch, AD_CMPNY);
											CURR_QTY =invBomCosting.getCstRemainingQuantity();
										} catch (FinderException ex) {

										}
									}




									LocalInvItem bomItm = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(),AD_CMPNY);


									double NEEDED_QTY = this.convertByUomFromAndItemAndQuantity(invBillOfMaterial.getInvUnitOfMeasure(), bomItm,
											invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY);
									Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N03");


									try{
										if(invBomCosting != null){
											/*
												CURR_QTY = this.convertByUomAndQuantity(invBomCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure(),
														invBomCosting.getInvItemLocation().getInvItem(),
														invBomCosting.getCstRemainingQuantity(), AD_CMPNY);*/

											CURR_QTY = this.convertByUomAndQuantity(
													invAdjustmentLineItem.getInvUnitOfMeasure(),
													invAdjustmentLineItem.getInvItemLocation().getInvItem(),
													CURR_QTY, AD_CMPNY);
										}
									}catch (Exception e) {

									}





									if ((invBomCosting == null && isIlFound==false)|| CURR_QTY == 0 || CURR_QTY < (NEEDED_QTY * ILI_QTY)) {


										a++;
										hasInsufficientItems = true;
										System.out.println("Doc Number: " + invAdjustment.getAdjDocumentNumber());
										System.out.println("Item: " + invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName());
										System.out.println("Adjust Qty: " + invAdjustmentLineItem.getAlAdjustQuantity());
										double remQty = 0;
										double unitCost = 0;
										try{
											remQty = CURR_QTY;
										}catch(Exception e){
											remQty = 0;
										}
										System.out.println("Remaining Qty: " + remQty);
										//insufficientItems += invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName() + " ";
										insufficientFix = Math.abs(invAdjustmentLineItem.getAlAdjustQuantity()) - CURR_QTY;
										insufficient.add(invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName());
										insufficientQty.add(insufficientFix);
										insufficientUOM.add(invAdjustmentLineItem.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
										insufficientUnitCost.add(invAdjustmentLineItem.getAlUnitCost());
										itemLocation.add(invAdjustmentLineItem.getInvItemLocation().getInvLocation().getLocName());
										System.out.println("insufficientFix: " + Math.abs(insufficientFix));
										CURR_QTY = CURR_QTY + insufficientFix;
										insufficientItems += invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName()
										+ "-" + invBillOfMaterial.getBomIiName() + ", ";
									}

									//CURR_QTY -= (NEEDED_QTY * ILI_QTY);
									CURR_QTY -= ILI_QTY;
		 		                    if(!isIlFound){
		 		                    	cstMap.remove(invItemLocation.getIlCode().toString());
		 		                    }

		 		                    cstMap.put(invItemLocation.getIlCode().toString(), new Double(CURR_QTY));

		 							Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L03");
								}
							}  else {

		 		  	   	  /*********************************************/
								LocalInvCosting invCosting = null;
		 		  	   	    	double ILI_QTY = this.convertByUomAndQuantity(
		 		  	   	    		invAdjustmentLineItem.getInvUnitOfMeasure(),
		 		  	   	    	invAdjustmentLineItem.getInvItemLocation().getInvItem(),
		 		  	   	    			Math.abs(invAdjustmentLineItem.getAlAdjustQuantity()), AD_CMPNY);

		 		  	   	    	double CURR_QTY = 0;
		 		  	   	    	boolean isIlFound = false;

		 		  	   	    	if(cstMap.containsKey(invAdjustmentLineItem.getInvItemLocation().getIlCode().toString())){
		 		  	   	    		Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04 - A");
		 		  	   	    		isIlFound =  true;
		 		  	   	    		CURR_QTY = ((Double)cstMap.get(invAdjustmentLineItem.getInvItemLocation().getIlCode().toString())).doubleValue();

		 		  	   	    	}else{

		 		  	   	    		try {

		 		  	   	    			/*LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIlCode(
		 		  	   	    					arReceipt.getRctDate(), arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
		 		  	   	    			 */
		 		  	   	    			invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
		 		  	   	    					invAdjustment.getAdjDate(), II_NM, LOC_NM, branch, AD_CMPNY);
		 		  	   	    			Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04");
		 		  	   	    			CURR_QTY =invCosting.getCstRemainingQuantity();
		 		  	   	    		} catch (FinderException ex) {
		 		  	   	    			System.out.println("CATCH");
		 		  	   	    		}
		 		  	   	    	}

		 		  	   	    	if(invCosting != null){

		 		  	   	    		/*CURR_QTY = this.convertByUomAndQuantity(arReceiptLineItem.getInvItemLocation().getInvItem().getInvUnitOfMeasure(),
		 		  	   	    			arReceiptLineItem.getInvItemLocation().getInvItem(),
		 		  	   	    				invCosting.getCstRemainingQuantity(), AD_CMPNY);*/

		 		  	   	    		CURR_QTY = this.convertByUomAndQuantity(
		 		  	   	    			invAdjustmentLineItem.getInvUnitOfMeasure(),
		 		  	   	    		invAdjustmentLineItem.getInvItemLocation().getInvItem(),
		 		  	   	    			CURR_QTY, AD_CMPNY);

		 		  	   	    		Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L05");
		 		  	   	    	}

		 		  	   	    	double LOWEST_QTY = this.convertByUomAndQuantity(
		 		  	   	    		invAdjustmentLineItem.getInvUnitOfMeasure(),
		 		  	   	    	invAdjustmentLineItem.getInvItemLocation().getInvItem(),
		 		  	   	    			1, AD_CMPNY);
		 		  	   	    	Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L06");
		 		  	   	    	System.out.println("CURRENT QTY: " + CURR_QTY);
		 		  	   	    	System.out.println("ILI QTY: " + ILI_QTY);
		 		  	   	    	System.out.println("LOWEST_QTY: " + LOWEST_QTY);
		 		  	   	    	System.out.println("invCosting: " + invCosting);
		 		  	   	    	if ((invCosting == null && isIlFound==false) || CURR_QTY == 0 ||
		 		  	   	    		CURR_QTY - ILI_QTY <= -LOWEST_QTY) {
									a++;
									hasInsufficientItems = true;
									System.out.println("Doc Number: " + invAdjustment.getAdjDocumentNumber());
									System.out.println("Item: " + invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName());
									System.out.println("Adjust Qty: " + invAdjustmentLineItem.getAlAdjustQuantity());
									double remQty = 0;
									double unitCost = 0;
									try{
										remQty = CURR_QTY;
									}catch(Exception e){
										remQty = 0;
									}
									System.out.println("Remaining Qty: " + remQty);
									insufficientItems += invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName() + " ";
									insufficientFix = Math.abs(invAdjustmentLineItem.getAlAdjustQuantity()) - CURR_QTY;
									CURR_QTY = CURR_QTY + insufficientFix;
									insufficient.add(invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName());
									insufficientQty.add(insufficientFix);
									insufficientUOM.add(invAdjustmentLineItem.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
									insufficientUnitCost.add(invAdjustmentLineItem.getAlUnitCost());
									itemLocation.add(invAdjustmentLineItem.getInvItemLocation().getInvLocation().getLocName());
									System.out.println("insufficientFix: " + Math.abs(insufficientFix));


								}

		 		  	   	    	CURR_QTY -= ILI_QTY;

		 		  	   	    	if(isIlFound){
		 		  	   	    		cstMap.remove(invAdjustmentLineItem.getInvItemLocation().getIlCode().toString());
		 		  	   	    	}

		 		  	   	    	cstMap.put(invAdjustmentLineItem.getInvItemLocation().getIlCode().toString(), new Double(CURR_QTY));
							}



						}

					}

					String [] insuItem;
					insuItem = new String[a];

					Double [] insuQty;
					insuQty = new Double[a];

					String [] insuUOM;
					insuUOM = new String[a];

					Double [] insuCost;
					insuCost = new Double[a];

					String [] insuItemLoc;
					insuItemLoc = new String[a];

					if(hasInsufficientItems) {
						results = "hasInsufficientItems";
						System.out.println("insufficientItems: " + insufficientItems);
						System.out.println(insufficient.size());

						Iterator mi = insufficient.iterator();
						int b = 0;
						while(mi.hasNext()){

							String miscStr = (String)mi.next();
							System.out.println("Items: " + miscStr);
							insuItem[b] = miscStr;
							System.out.println("insuTest[b]: " + insuItem[b]);
							b++;
						}

						Iterator mi2 = insufficientQty.iterator();
						b = 0;
						while(mi2.hasNext()){

							Double miscStr = (Double)mi2.next();
							insuQty[b] = miscStr;
							System.out.println("insuTest[b]: " + insuQty[b]);
							b++;
						}

						Iterator mi3 = insufficientUOM.iterator();
						b = 0;
						while(mi3.hasNext()){

							String miscStr = (String)mi3.next();
							insuUOM[b] = miscStr;
							System.out.println("insuTest[b]: " + insuUOM[b]);
							b++;
						}

						Iterator mi4 = insufficientUnitCost.iterator();
						b = 0;
						while(mi4.hasNext()){

							Double miscStr = (Double)mi4.next();
							insuCost[b] = miscStr;
							System.out.println("insuItemLoc[b]: " + insuCost[b]);
							b++;
						}

						Iterator mi5 = itemLocation.iterator();
						b = 0;
						while(mi5.hasNext()){

							String miscStr = (String)mi5.next();
							insuItemLoc[b] = miscStr;
							System.out.println("insuCost[b]: " + insuItemLoc[b]);
							b++;
						}

						String[] adjEncode = new String[1];
						//int ctr = 0;
						//for(int x=0; x < a ; x++){
							adjEncode[0] = invAdjRowEncode(invAdjustment.getAdjDate() , user, insuItemLoc, invAdjAccount, insuItem, insuQty,
									insuUOM, insuCost, a, invAdjustment.getAdjDocumentNumber());
						//}
						int Result =0;
						try{
							Result = setInvAdjustment(adjEncode, AD_BRNCH, AD_CMPNY);
							count++;
						}catch(Exception ex){
							ex.printStackTrace();
							throw new EJBException(ex.getMessage());
						}


						System.out.println("Result: " + Result);
						results = Integer.toString(Result);

						//throw new GlobalRecordInvalidException(insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
					}

				}
			}else if(transactionType.equals("Sales")){
				System.out.println("SALES");
				Collection arReceipts = arReceiptHome.findUnpostedRctByRctDateRangeByBranch(dateFrom, dateTo, branch, AD_CMPNY);
				HashMap cstMapBatch = new HashMap();
				Date oldDate = null;
				Iterator i = arReceipts.iterator();
				HashMap cstMap = null;
				int runVar = 0;
				while (i.hasNext()) {

					LocalArReceipt arReceipt = (LocalArReceipt)i.next();

					System.out.println("*************************CHECKING INSUFFICIENT SALES STOCKS******************************");

					boolean hasInsufficientItems = false;
					String insufficientItems = "";

					Collection arReceiptLineItems = arReceipt.getArInvoiceLineItems();

					Iterator p = arReceiptLineItems.iterator();
					int a =0;
					ArrayList insufficient = new ArrayList();
					ArrayList insufficientQty = new ArrayList();
					ArrayList insufficientUOM = new ArrayList();
					ArrayList insufficientUnitCost = new ArrayList();
					ArrayList itemLocation = new ArrayList();
					double totalAmount = 0;
					runVar++;

					if((arReceipt.getRctDate().equals(dateTo) && dateFrom.equals(dateTo)) || runVar==1){
						System.out.println("INSTANTIATE");
						cstMap = new HashMap();
					}
					//HashMap cstMap = new HashMap();
					while (p.hasNext()) {

						LocalArInvoiceLineItem arReceiptLineItem = (LocalArInvoiceLineItem)p.next();
						String II_NM = arReceiptLineItem.getInvItemLocation().getInvItem().getIiName();
	    				String LOC_NM = arReceiptLineItem.getInvItemLocation().getInvLocation().getLocName();

						if(arReceipt.getRctType().equals("MISC")){
							if (arReceiptLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arReceiptLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {



								Collection invBillOfMaterials = arReceiptLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

								Iterator j = invBillOfMaterials.iterator();

								while (j.hasNext()) {

									LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

									LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

									double ILI_QTY = this.convertByUomFromAndItemAndQuantity(
											arReceiptLineItem.getInvUnitOfMeasure(),
											arReceiptLineItem.getInvItemLocation().getInvItem(),
											Math.abs(arReceiptLineItem.getIliQuantity()), AD_CMPNY);
									Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L01");


									LocalInvItem bomItm = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(),AD_CMPNY);

									double NEEDED_QTY = this.convertByUomFromAndItemAndQuantity(invBillOfMaterial.getInvUnitOfMeasure(), bomItm,
											invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY);

									double CURR_QTY = 0;

									boolean isIlFound = false;

									double remQty = 0;

									LocalInvCosting invBomCosting = null;

									if(cstMap.containsKey(invItemLocation.getIlCode().toString())){

										isIlFound =  true;
										CURR_QTY = ((Double)cstMap.get(invItemLocation.getIlCode().toString())).doubleValue();

									}else{

										try {

											invBomCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIlCode(
													arReceipt.getRctDate(), invItemLocation.getIlCode(), branch, AD_CMPNY);

											Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L02");
											CURR_QTY=invBomCosting.getCstRemainingQuantity();
										} catch (FinderException ex) {


										}

									}

									if(invBomCosting != null || cstMap.containsKey(arReceiptLineItem.getInvItemLocation().getIlCode().toString())){

										/*CURR_QTY = this.convertByUomAndQuantity(invBomCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure(),
												invBomCosting.getInvItemLocation().getInvItem(),
												invBomCosting.getCstRemainingQuantity(), AD_CMPNY);*/
										//System.out.println("invBomCosting: " + invBomCosting.getCstCode());

										CURR_QTY = this.convertByUomAndQuantity(
				 		  	   	    			arReceiptLineItem.getInvUnitOfMeasure(),
				 		  	   	    			arReceiptLineItem.getInvItemLocation().getInvItem(),
				 		  	   	    		CURR_QTY, AD_CMPNY);

										Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04");
										try{
											remQty = CURR_QTY;
										}catch(Exception e){
											remQty = 0;
										}
									}
									if (CURR_QTY == 0 || CURR_QTY < (NEEDED_QTY * ILI_QTY)) {


										a++;
										hasInsufficientItems = true;
										try{

											System.out.println("Doc Number: " + arReceipt.getRctNumber());
											System.out.println("Item: " + arReceiptLineItem.getInvItemLocation().getInvItem().getIiName());
											System.out.println("Adjust Qty: " + arReceiptLineItem.getIliQuantity());

										}catch(Exception e){
											System.out.println("Catch");
										}
										double unitCost = 0;

										System.out.println("Remaining Qty: " + remQty);
										//insufficientItems += invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName() + " ";
										insufficientFix = arReceiptLineItem.getIliQuantity() - CURR_QTY;
										//CURR_QTY = insufficientFix;

										insufficient.add(arReceiptLineItem.getInvItemLocation().getInvItem().getIiName());
										insufficientQty.add(insufficientFix);
										insufficientUOM.add(arReceiptLineItem.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
										insufficientUnitCost.add(arReceiptLineItem.getIliUnitPrice());
										itemLocation.add(arReceiptLineItem.getInvItemLocation().getInvLocation().getLocName());
										System.out.println("insufficientFix: " + insufficientFix);
										CURR_QTY = CURR_QTY + insufficientFix;
										insufficientItems += arReceiptLineItem.getInvItemLocation().getInvItem().getIiName()
										+ "-" + invBillOfMaterial.getBomIiName() + ", ";
									}

									//CURR_QTY -= (NEEDED_QTY * ILI_QTY);
									CURR_QTY -= ILI_QTY;

									if(!isIlFound){
										cstMap.remove(invItemLocation.getIlCode().toString());
									}

									cstMap.put(invItemLocation.getIlCode().toString(), new Double(CURR_QTY));

									Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L03");



								}

							}  else {



								LocalInvCosting invCosting = null;
								double ILI_QTY = this.convertByUomAndQuantity(
										arReceiptLineItem.getInvUnitOfMeasure(),
										arReceiptLineItem.getInvItemLocation().getInvItem(),
										Math.abs(arReceiptLineItem.getIliQuantity()), AD_CMPNY);

								double CURR_QTY = 0;
								boolean isIlFound = false;

								if(cstMap.containsKey(arReceiptLineItem.getInvItemLocation().getIlCode().toString())){
									Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04 - A");
									isIlFound =  true;
									CURR_QTY = ((Double)cstMap.get(arReceiptLineItem.getInvItemLocation().getIlCode().toString())).doubleValue();

								}else{

									try {

										/*LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIlCode(
		 		  	   	    					arReceipt.getRctDate(), arInvoiceLineItem.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
										 */
										invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
												arReceipt.getRctDate(), II_NM, LOC_NM, branch, AD_CMPNY);
										Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04");
										CURR_QTY=invCosting.getCstRemainingQuantity();
									} catch (FinderException ex) {
										System.out.println("CATCH");
									}
								}

								if(invCosting != null || cstMap.containsKey(arReceiptLineItem.getInvItemLocation().getIlCode().toString())){

									/*CURR_QTY = this.convertByUomAndQuantity(arReceiptLineItem.getInvItemLocation().getInvItem().getInvUnitOfMeasure(),
		 		  	   	    			arReceiptLineItem.getInvItemLocation().getInvItem(),
		 		  	   	    				invCosting.getCstRemainingQuantity(), AD_CMPNY);*/

									CURR_QTY = this.convertByUomAndQuantity(
											arReceiptLineItem.getInvUnitOfMeasure(),
											arReceiptLineItem.getInvItemLocation().getInvItem(),
											CURR_QTY, AD_CMPNY);

									Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L05");
								}

								double LOWEST_QTY = this.convertByUomAndQuantity(
										arReceiptLineItem.getInvUnitOfMeasure(),
										arReceiptLineItem.getInvItemLocation().getInvItem(),
										1, AD_CMPNY);
								Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L06");
								System.out.println("CURRENT QTY: " + CURR_QTY);
								System.out.println("ILI QTY: " + ILI_QTY);
								System.out.println("LOWEST_QTY: " + LOWEST_QTY);
								//System.out.println("invCosting: " + invCosting);
								if ((invCosting == null && isIlFound==false) || CURR_QTY == 0 ||
										CURR_QTY - ILI_QTY <= -LOWEST_QTY) {
									a++;
									hasInsufficientItems = true;

									double remQty = 0;
									double unitCost = 0;
									try{
										System.out.println("Doc Number: " + arReceipt.getRctNumber());
										System.out.println("Item: " + arReceiptLineItem.getInvItemLocation().getInvItem().getIiName());
										System.out.println("Adjust Qty: " + arReceiptLineItem.getIliQuantity());
										remQty = invCosting.getCstRemainingQuantity();
									}catch(Exception e){
										System.out.println("BOOM!");
										remQty = 0;
									}
									System.out.println("Remaining Qty: " + remQty);
									insufficientItems += arReceiptLineItem.getInvItemLocation().getInvItem().getIiName() + " ";
									insufficientFix = arReceiptLineItem.getIliQuantity() - CURR_QTY;
									CURR_QTY = CURR_QTY + insufficientFix;
									insufficient.add(arReceiptLineItem.getInvItemLocation().getInvItem().getIiName());
									insufficientQty.add(insufficientFix);
									insufficientUOM.add(arReceiptLineItem.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
									insufficientUnitCost.add(arReceiptLineItem.getIliUnitPrice());
									itemLocation.add(arReceiptLineItem.getInvItemLocation().getInvLocation().getLocName());
									System.out.println("insufficientFix: " + insufficientFix);


								}

								CURR_QTY -= ILI_QTY;

								if(isIlFound){
									cstMap.remove(arReceiptLineItem.getInvItemLocation().getIlCode().toString());
								}

								cstMap.put(arReceiptLineItem.getInvItemLocation().getIlCode().toString(), new Double(CURR_QTY));

							}
						}

					}

					String [] insuItem;
					insuItem = new String[a];

					Double [] insuQty;
					insuQty = new Double[a];

					String [] insuUOM;
					insuUOM = new String[a];

					Double [] insuCost;
					insuCost = new Double[a];

					String [] insuLocation;
					insuLocation = new String[a];

					if(hasInsufficientItems) {
						results = "hasInsufficientItems";
						System.out.println("insufficientItems: " + insufficientItems);
						System.out.println(insufficient.size());

						Iterator mi = insufficient.iterator();
						int b = 0;
						while(mi.hasNext()){

							String miscStr = (String)mi.next();
							System.out.println("Items: " + miscStr);
							insuItem[b] = miscStr;
							System.out.println("insuTest[b]: " + insuItem[b]);
							b++;
						}

						Iterator mi2 = insufficientQty.iterator();
						b = 0;
						while(mi2.hasNext()){

							Double miscStr = (Double)mi2.next();
							insuQty[b] = miscStr;
							System.out.println("insuTest[b]: " + insuQty[b]);
							b++;
						}

						Iterator mi3 = insufficientUOM.iterator();
						b = 0;
						while(mi3.hasNext()){

							String miscStr = (String)mi3.next();
							insuUOM[b] = miscStr;
							System.out.println("insuTest[b]: " + insuUOM[b]);
							b++;
						}

						Iterator mi4 = insufficientUnitCost.iterator();
						b = 0;
						while(mi4.hasNext()){

							Double miscStr = (Double)mi4.next();
							insuCost[b] = miscStr;
							System.out.println("insuCost[b]: " + insuCost[b]);
							b++;
						}

						Iterator mi5 = itemLocation.iterator();
						b = 0;
						while(mi5.hasNext()){

							String miscStr = (String)mi5.next();
							insuLocation[b] = miscStr;
							System.out.println("insuCost[b]: " + insuLocation[b]);
							b++;
						}

						String[] adjEncode = new String[1];
						/*int ctr = 0;
						for(int x=0; x < a ; x++){
						System.out.println(arReceipt.getRctNumber());
						System.out.println(ctr);*/
							adjEncode[0] = invAdjRowEncode(arReceipt.getRctDate() , user, insuLocation, invAdjAccount, insuItem, insuQty,
									insuUOM, insuCost, a, arReceipt.getRctNumber());
						//}
						int Result =0;

						try{
							System.out.println("adjEncode: " + adjEncode.length);
							if(adjEncode!=null){
								System.out.println("SAVING");
								Result = setInvAdjustment(adjEncode, AD_BRNCH, AD_CMPNY);
								count++;
							}
						}catch(Exception ex){
							ex.printStackTrace();
							throw new EJBException(ex.getMessage());
						}


						System.out.println("Result: " + Result);
						results = Integer.toString(Result);

						//throw new GlobalRecordInvalidException(insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
					}

				}

				System.out.println("CHECKING INVOICES");
				Collection arInvoices = arInvoiceHome.findUnpostedInvByInvDateRangeByBranch(dateFrom, dateTo, branch, AD_CMPNY);

				Iterator q = arInvoices.iterator();
				HashMap cstMapInvoice = null;
				int runVar2 = 0;
				while (q.hasNext()) {

					LocalArInvoice arInvoice = (LocalArInvoice)q.next();

					System.out.println("*************************CHECKING INSUFFICIENT SALES STOCKS******************************");

					boolean hasInsufficientItems = false;
					String insufficientItems = "";

					Collection arInvoiceLineItems = arInvoice.getArInvoiceLineItems();

					Iterator p = arInvoiceLineItems.iterator();
					int a =0;
					ArrayList insufficient = new ArrayList();
					ArrayList insufficientQty = new ArrayList();
					ArrayList insufficientUOM = new ArrayList();
					ArrayList insufficientUnitCost = new ArrayList();
					ArrayList itemLocation = new ArrayList();
					double totalAmount = 0;
					runVar2++;
					if((arInvoice.getInvDate().equals(dateTo) && dateFrom.equals(dateTo)) || runVar2==1){
						cstMapInvoice = new HashMap();
						cstMap = new HashMap();
					}
					//HashMap cstMap = new HashMap();
					while (p.hasNext()) {

						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)p.next();

						if (arInvoiceLineItem.getIliEnableAutoBuild() == EJBCommon.TRUE && arInvoiceLineItem.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {

							Collection invBillOfMaterials = arInvoiceLineItem.getInvItemLocation().getInvItem().getInvBillOfMaterials();

							Iterator j = invBillOfMaterials.iterator();
							Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N01");
							while (j.hasNext()) {

								LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) j.next();

								LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(invBillOfMaterial.getBomLocName(), invBillOfMaterial.getBomIiName(), AD_CMPNY);

								LocalInvCosting invBomCosting = null;
								double CURR_QTY = 0;
								double ILI_QTY = this.convertByUomFromAndItemAndQuantity(
										arInvoiceLineItem.getInvUnitOfMeasure(),
										arInvoiceLineItem.getInvItemLocation().getInvItem(),
										Math.abs(arInvoiceLineItem.getIliQuantity()), AD_CMPNY);
								Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N02");
								boolean isIlFound = false;
								if(cstMap.containsKey(invItemLocation.getIlCode().toString())){

									isIlFound =  true;
									CURR_QTY = ((Double)cstMap.get(invItemLocation.getIlCode().toString())).doubleValue();

								}else{
									try {

										/*invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(
															arInvoice.getInvDate(), invBillOfMaterial.getBomIiName(),
															invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
										 */
										invBomCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
												arInvoice.getInvDate(), invBillOfMaterial.getBomIiName(), invItemLocation.getInvLocation().getLocName(), branch, AD_CMPNY);
										CURR_QTY = invBomCosting.getCstRemainingQuantity();
									} catch (FinderException ex) {

									}
								}


								LocalInvItem bomItm = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(),AD_CMPNY);


								double NEEDED_QTY = this.convertByUomFromAndItemAndQuantity(invBillOfMaterial.getInvUnitOfMeasure(), bomItm,
										invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY);
								Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N03");


								try{
									if(invBomCosting != null || cstMap.containsKey(arInvoiceLineItem.getInvItemLocation().getIlCode().toString())){

										CURR_QTY = this.convertByUomAndQuantity(
												arInvoiceLineItem.getInvUnitOfMeasure(),
												arInvoiceLineItem.getInvItemLocation().getInvItem(),
				 		  	   	    		CURR_QTY, AD_CMPNY);
									}
								}catch (Exception e) {

								}
								if (invBomCosting == null || CURR_QTY == 0 || CURR_QTY < (NEEDED_QTY * ILI_QTY)) {


									a++;
									hasInsufficientItems = true;
									System.out.println("Doc Number: " + arInvoice.getInvNumber());
									System.out.println("Item: " + arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
									System.out.println("Adjust Qty: " + arInvoiceLineItem.getIliQuantity());
									double remQty = 0;
									double unitCost = 0;
									try{
										remQty = invBomCosting.getCstRemainingQuantity();
									}catch(Exception e){
										remQty = 0;
									}
									System.out.println("Remaining Qty: " + CURR_QTY);
									//insufficientItems += invAdjustmentLineItem.getInvItemLocation().getInvItem().getIiName() + " ";
									insufficientFix = arInvoiceLineItem.getIliQuantity() - CURR_QTY;
									CURR_QTY = CURR_QTY + insufficientFix;
									insufficient.add(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
									insufficientQty.add(insufficientFix);
									insufficientUOM.add(arInvoiceLineItem.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
									insufficientUnitCost.add(arInvoiceLineItem.getIliUnitPrice());
									System.out.println("insufficientFix: " + insufficientFix);
									itemLocation.add(arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName());
									insufficientItems += arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName()
									+ "-" + invBillOfMaterial.getBomIiName() + ", ";
								}

								CURR_QTY -= ILI_QTY;

								if(isIlFound){
									cstMap.remove(arInvoiceLineItem.getInvItemLocation().getIlCode().toString());
								}

								cstMap.put(arInvoiceLineItem.getInvItemLocation().getIlCode().toString(), new Double(CURR_QTY));
							}
						}  else {
							double CURR_QTY = 0;
							LocalInvCosting invCosting = null;

							double ILI_QTY = this.convertByUomAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									Math.abs(arInvoiceLineItem.getIliQuantity()), AD_CMPNY);

							boolean isIlFound = false;

							if(cstMap.containsKey(arInvoiceLineItem.getInvItemLocation().getIlCode().toString())){
								Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L04 - A");
								isIlFound =  true;
								CURR_QTY = ((Double)cstMap.get(arInvoiceLineItem.getInvItemLocation().getIlCode().toString())).doubleValue();

							}else{
								try {

									/*invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(
													arInvoice.getInvDate(), arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(),
													arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);*/

									invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											arInvoice.getInvDate(), arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName()
											, arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), branch, AD_CMPNY);
									Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N04");
									CURR_QTY = invCosting.getCstRemainingQuantity();
								} catch (FinderException ex) {

								}
							}


							if(invCosting != null || cstMap.containsKey(arInvoiceLineItem.getInvItemLocation().getIlCode().toString())){

	 		  	   	    		/*CURR_QTY = this.convertByUomAndQuantity(arReceiptLineItem.getInvItemLocation().getInvItem().getInvUnitOfMeasure(),
	 		  	   	    			arReceiptLineItem.getInvItemLocation().getInvItem(),
	 		  	   	    				invCosting.getCstRemainingQuantity(), AD_CMPNY);*/

								CURR_QTY = this.convertByUomAndQuantity(
										arInvoiceLineItem.getInvUnitOfMeasure(),
										arInvoiceLineItem.getInvItemLocation().getInvItem(),
										CURR_QTY, AD_CMPNY);

	 		  	   	    		Debug.print("ArMiscReceiptEntryControllerBean saveArRctIliEntry  L05");
	 		  	   	    	}

							double LOWEST_QTY = this.convertByUomAndQuantity(
									arInvoiceLineItem.getInvUnitOfMeasure(),
									arInvoiceLineItem.getInvItemLocation().getInvItem(),
									1, AD_CMPNY);

							Debug.print("ArInvoiceEntryControllerBean saveArInvIliEntry N05");

							if ((invCosting == null && isIlFound==false) || CURR_QTY == 0 ||
									CURR_QTY - ILI_QTY <= -LOWEST_QTY) {
								a++;
								hasInsufficientItems = true;
								System.out.println("Doc Number: " + arInvoice.getInvNumber());
								System.out.println("Item: " + arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
								System.out.println("Adjust Qty: " + arInvoiceLineItem.getIliQuantity());
								double remQty = 0;
								double unitCost = 0;
								try{
									remQty = CURR_QTY;
								}catch(Exception e){
									remQty = 0;
								}
								System.out.println("Remaining Qty: " + remQty);
								insufficientItems += arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName() + " ";
								insufficientFix = arInvoiceLineItem.getIliQuantity() - remQty;
								insufficient.add(arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName());
								insufficientQty.add(insufficientFix);
								insufficientUOM.add(arInvoiceLineItem.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
								insufficientUnitCost.add(arInvoiceLineItem.getIliUnitPrice());
								System.out.println("insufficientFix: " + insufficientFix);
								itemLocation.add(arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName());

							}

							CURR_QTY -= ILI_QTY;

							if(isIlFound){
								cstMap.remove(arInvoiceLineItem.getInvItemLocation().getIlCode().toString());
							}

							cstMap.put(arInvoiceLineItem.getInvItemLocation().getIlCode().toString(), new Double(CURR_QTY));
						}

					}

					String [] insuItem;
					insuItem = new String[a];

					Double [] insuQty;
					insuQty = new Double[a];

					String [] insuUOM;
					insuUOM = new String[a];

					Double [] insuCost;
					insuCost = new Double[a];

					String [] insuLocation;
					insuLocation = new String[a];

					if(hasInsufficientItems) {
						results = "hasInsufficientItems";
						System.out.println("insufficientItems: " + insufficientItems);
						System.out.println(insufficient.size());

						Iterator mi = insufficient.iterator();
						int b = 0;
						while(mi.hasNext()){

							String miscStr = (String)mi.next();
							System.out.println("Items: " + miscStr);
							insuItem[b] = miscStr;
							System.out.println("insuTest[b]: " + insuItem[b]);
							b++;
						}

						Iterator mi2 = insufficientQty.iterator();
						b = 0;
						while(mi2.hasNext()){

							Double miscStr = (Double)mi2.next();
							insuQty[b] = miscStr;
							System.out.println("insuTest[b]: " + insuQty[b]);
							b++;
						}

						Iterator mi3 = insufficientUOM.iterator();
						b = 0;
						while(mi3.hasNext()){

							String miscStr = (String)mi3.next();
							insuUOM[b] = miscStr;
							System.out.println("insuTest[b]: " + insuUOM[b]);
							b++;
						}

						Iterator mi4 = insufficientUnitCost.iterator();
						b = 0;
						while(mi4.hasNext()){

							Double miscStr = (Double)mi4.next();
							insuCost[b] = miscStr;
							System.out.println("insuCost[b]: " + insuCost[b]);
							b++;
						}

						Iterator mi5 = itemLocation.iterator();
						b = 0;
						while(mi5.hasNext()){

							String miscStr = (String)mi5.next();
							insuLocation[b] = miscStr;
							System.out.println("insuTest[b]: " + insuLocation[b]);
							b++;
						}

						String[] adjEncode = new String[1];
						/*int ctr = 0;
						for(int x=0; x < a ; x++){
						System.out.println(arReceipt.getRctNumber());
						System.out.println(ctr);*/
							adjEncode[0] = invAdjRowEncode(arInvoice.getInvDate() , user, insuLocation, invAdjAccount, insuItem, insuQty,
									insuUOM, insuCost, a, arInvoice.getInvNumber());
						//}
						int Result =0;

						try{
							System.out.println("adjEncode: " + adjEncode.length);
							if(adjEncode!=null){
								System.out.println("SAVING");
								Result = setInvAdjustment(adjEncode, AD_BRNCH, AD_CMPNY);
								count++;
							}
						}catch(Exception ex){
							ex.printStackTrace();
							throw new EJBException(ex.getMessage());
						}


						System.out.println("Result: " + Result);
						results = Integer.toString(Result);

						//throw new GlobalRecordInvalidException(insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
					}

				}
			}


		} catch (Exception ex) {


			ex.printStackTrace();
			throw new EJBException(ex.getMessage());

		}

		System.out.println("*************************INSUFFICIENT STOCKS Fixed******************************");
		System.out.println("Number of Insufficiency: " + count);
		return results;
	}

	private String invAdjRowEncode(Date invAdjDate, String User, String [] location,
			String adjustmentAccount, String [] insuItem, Double [] insuQty, String [] insuUOM, Double [] insuCost, int arrayCount,
			String documentNumber)
	{

		//Debug.print("InvItemSyncControllerBean poRowEncode");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer encodedResult = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_OUTPUT);

		// Start separator
		encodedResult.append(separator);

		// Adj Code
		encodedResult.append("0");
		encodedResult.append(separator);

		// Adj Type
		encodedResult.append("GENERAL");
		encodedResult.append(separator);

		// Document Number
		encodedResult.append("");
		encodedResult.append(separator);

		// Reference Number
		encodedResult.append("Exception");
		encodedResult.append(separator);

		// Date
		encodedResult.append(sdf.format(invAdjDate));
		encodedResult.append(separator);

		//Description
		encodedResult.append("Insufficient Stock Fix for " + documentNumber);
		encodedResult.append(separator);


		// Created By
        encodedResult.append(User); // Temporarily Admin
        encodedResult.append(separator);

        Date date = new Date();

        // Date Created
        encodedResult.append(sdf.format(date));
        encodedResult.append(separator);

        // Last Modified By
        encodedResult.append(User); // Temporarily Admin
        encodedResult.append(separator);

        // Date Last Modified
        encodedResult.append(sdf.format(date));
        encodedResult.append(separator);

        // COA ACCOUNT NO
        encodedResult.append(adjustmentAccount);
        encodedResult.append(separator);

        // End Separator heading
        encodedResult.append(separator);

		String lineSeparator = "~";

		// begin lineSeparator
		encodedResult.append(lineSeparator);

		for(int x=0; x < arrayCount ; x++){
			 // Start separator
            encodedResult.append(separator);

            System.out.println("insuCost[x]: " + insuCost[x]);
            // Unit Cost
            encodedResult.append(insuCost[x]);
            encodedResult.append(separator);

            // Adjusted Quantity
            encodedResult.append(insuQty[x]);
            encodedResult.append(separator);
            //MessageBox.Show("ADJ QUANTITY : " + lineDr["INV_USED_QTY"]);

            // Unit of Measure
            encodedResult.append(insuUOM[x]);
            encodedResult.append(separator);

            // Item Name
            encodedResult.append(insuItem[x]);
            encodedResult.append(separator);

            // Location Name
            encodedResult.append(location[x]);
            encodedResult.append(separator);

            encodedResult.append(lineSeparator);

		}

		return encodedResult.toString();

	}

	/**
	 * @ejb:interface-method
	 * */
	public String[] getSoMatchedInvBOAllPosted(String DateFrom, String DateTo, String BR_BRNCH_CODE, Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean getSoMatchedInvBOAllPosted");

		LocalAdBranchHome adBranchHome = null;
		LocalInvBuildOrderHome invBuildOrderHome = null;
		LocalInvBuildOrderLineHome invBuildOrderLineHome = null;

		ArrayList AllPoUnreceivedAndPosted = new ArrayList();

		try {

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			invBuildOrderHome = (LocalInvBuildOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBuildOrderHome.JNDI_NAME, LocalInvBuildOrderHome.class);
			invBuildOrderLineHome = (LocalInvBuildOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBuildOrderLineHome.JNDI_NAME, LocalInvBuildOrderLineHome.class);

		} catch(Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		Integer AD_BRNCH = new Integer(0);
		try {
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();
		} catch(Exception ex) {
			throw new EJBException(ex.getMessage());
		}

		Object obj[] = new Object[2];

		StringBuffer jbossQl = new StringBuffer();

		jbossQl.append("SELECT OBJECT(bor) FROM InvBuildOrder bor WHERE ");
		jbossQl.append("bor.borDate>=?1 AND ");
		jbossQl.append("bor.borDate<=?2 AND ");
		jbossQl.append("bor.borPosted=1 AND ");
		jbossQl.append("bor.borAdBranch=" + AD_BRNCH + " AND bor.borAdCompany=" + AD_CMPNY + " ");

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		sdf.setLenient(false);
		try {
			obj[0]=sdf.parse(DateFrom);
			obj[1]=sdf.parse(DateTo);
		} catch (Exception ex) {
			System.out.println("Error On BO Date: " + ex.getMessage());
		}

		String[] results = null;

		try{

			Collection invBuildOrders = invBuildOrderHome.getBorByCriteria(jbossQl.toString(), obj);

			Iterator i = invBuildOrders.iterator();
			results = new String[invBuildOrders.size()];
			int ctr = 0;
			while(i.hasNext()){
				LocalInvBuildOrder bo = (LocalInvBuildOrder)i.next();
				results[ctr] = this.borRowEncode(bo);
				System.out.println(results[ctr]);
				ctr++;
			}

			System.out.println(invBuildOrders.size());


		}catch(Exception ex){
			ex.printStackTrace();
		}

		return results;

	}

	private String borRowEncode(LocalInvBuildOrder bo)
	{

		Debug.print("InvItemSyncControllerBean borRowEncode");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer encodedResult = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_OUTPUT);

		// Start separator
		encodedResult.append(separator);

		// BOR Code
		encodedResult.append(bo.getBorCode());
		encodedResult.append(separator);

		// BOR DATE
		encodedResult.append(sdf.format(bo.getBorDate()));
		encodedResult.append(separator);

		// BOR Document Number
		encodedResult.append(bo.getBorDocumentNumber());
		encodedResult.append(separator);

		// BOR Reference Number
		encodedResult.append(bo.getBorReferenceNumber());
		encodedResult.append(separator);

		// BOR Approval Status
		encodedResult.append(bo.getBorApprovalStatus());
		encodedResult.append(separator);

		// BOR Description
		encodedResult.append(bo.getBorDescription());
		encodedResult.append(separator);

		// BOR Ad Branch
		encodedResult.append(bo.getBorAdBranch());
		encodedResult.append(separator);

		/* ************************* Misc Details************************** */

		// BOR Created By
		encodedResult.append(bo.getBorCreatedBy());
		encodedResult.append(separator);

		// BOR Date Created
		encodedResult.append(sdf.format(bo.getBorDateCreated()));
		encodedResult.append(separator);

		// BOR Last Modified By
		encodedResult.append(bo.getBorLastModifiedBy());
		encodedResult.append(separator);

		// BOR Date Last Modified
		encodedResult.append(sdf.format(bo.getBorDateLastModified()));
		encodedResult.append(separator);

		// end separator
		encodedResult.append(separator);

		String lineSeparator = "~";

		// begin lineSeparator
		encodedResult.append(lineSeparator);

		Iterator i = bo.getInvBuildOrderLines().iterator();

		while (i.hasNext()) {

			LocalInvBuildOrderLine bol = (LocalInvBuildOrderLine)i.next();

			// begin separator
			encodedResult.append(separator);

			// BOL Line Code
			encodedResult.append(bol.getBolCode());
			encodedResult.append(separator);

			// BOL Qty Required
			encodedResult.append(bol.getBolQuantityRequired());
			encodedResult.append(separator);

			// BOL Qty Assembled
			encodedResult.append(bol.getBolQuantityAssembled());
			encodedResult.append(separator);

			// BOL Qty Available
			encodedResult.append(bol.getBolQuantityAvailable());
			encodedResult.append(separator);

			// BOR CODE
			encodedResult.append(bo.getBorCode());
			encodedResult.append(separator);

			// INV Item Location
			encodedResult.append(0);
			encodedResult.append(separator);

			// INV Item
			encodedResult.append(bol.getInvItemLocation().getInvItem().getIiName());
			encodedResult.append(separator);

			// INV Location
			encodedResult.append(bol.getInvItemLocation().getInvLocation().getLocName());
			encodedResult.append(separator);

			// begin lineSeparator
			encodedResult.append(lineSeparator);
		}

		return encodedResult.toString();
	}

	/**
	 * @ejb:interface-method
	 * */
	public String[] getApPOAllPostedAndUnreceived(String BR_BRNCH_CODE, Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean getApPOAllPostedAndUnreceived");

		LocalAdBranchHome adBranchHome = null;
		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

		ArrayList AllPoUnreceivedAndPosted = new ArrayList();

		try {

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

		} catch(Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		Integer AD_BRNCH = new Integer(0);
		try {
			LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
			AD_BRNCH = adBranch.getBrCode();
		} catch(Exception ex) {
			throw new EJBException(ex.getMessage());
		}

		try {

			Collection apPurchaseOrders = apPurchaseOrderHome.findPostedPoAll(AD_BRNCH, AD_CMPNY);
			Iterator i = apPurchaseOrders.iterator();

			int ctr =1, size = apPurchaseOrders.size();

			while(i.hasNext()) {
				LocalApPurchaseOrder apPurchaseOrder = (LocalApPurchaseOrder)i.next();
				try{
					ApModPurchaseOrderDetails receivingPo = this.getApPoByPoRcvPoNumberAndSplSupplierCodeAndAdBranch(apPurchaseOrder.getPoDocumentNumber(), apPurchaseOrder.getApSupplier().getSplSupplierCode(), AD_BRNCH, AD_CMPNY);
					AllPoUnreceivedAndPosted.add(apPurchaseOrder);
				} catch (Exception ex) {}

				System.out.println("Status: Processing " + (ctr++) + "/" + size);

			}

			System.out.println("Sending " + AllPoUnreceivedAndPosted.size() + "/" + apPurchaseOrders.size());

		} catch(Exception ex) {
			throw new EJBException(ex.getMessage());
		}

		String[] results = new String[AllPoUnreceivedAndPosted.size()];
		Iterator poIter =  AllPoUnreceivedAndPosted.iterator();
		int ctr = 0;

		while(poIter.hasNext()){

			ApModPurchaseOrderDetails apModPurchaseOrderDetails = new ApModPurchaseOrderDetails();
			LocalApPurchaseOrder apPurchaseOrder = (LocalApPurchaseOrder)poIter.next();

			apModPurchaseOrderDetails.setPoCode(apPurchaseOrder.getPoCode());
			apModPurchaseOrderDetails.setPoType(apPurchaseOrder.getPoType());
			apModPurchaseOrderDetails.setPoFcName(apPurchaseOrder.getGlFunctionalCurrency().getFcName());
			apModPurchaseOrderDetails.setPoTcName(apPurchaseOrder.getApTaxCode().getTcName());
			apModPurchaseOrderDetails.setPoSplSupplierCode(apPurchaseOrder.getApSupplier().getSplSupplierCode());
			apModPurchaseOrderDetails.setPoPytName(apPurchaseOrder.getAdPaymentTerm().getPytName());
			apModPurchaseOrderDetails.setPoDocumentNumber(apPurchaseOrder.getPoDocumentNumber());
			apModPurchaseOrderDetails.setPoReferenceNumber(apPurchaseOrder.getPoReferenceNumber());
			apModPurchaseOrderDetails.setPoDate(apPurchaseOrder.getPoDate());
			apModPurchaseOrderDetails.setPoDescription(apPurchaseOrder.getPoDescription());
			apModPurchaseOrderDetails.setPoConversionRate(apPurchaseOrder.getPoConversionRate());
			apModPurchaseOrderDetails.setPoConversionDate(apPurchaseOrder.getPoConversionDate());

			apModPurchaseOrderDetails.setPoCreatedBy(apPurchaseOrder.getPoCreatedBy());
			apModPurchaseOrderDetails.setPoDateCreated(apPurchaseOrder.getPoDateCreated());
			apModPurchaseOrderDetails.setPoLastModifiedBy(apPurchaseOrder.getPoLastModifiedBy());
			apModPurchaseOrderDetails.setPoDateLastModified(apPurchaseOrder.getPoDateLastModified());

			// Add lines
			Iterator lineIter = apPurchaseOrder.getApPurchaseOrderLines().iterator();
			ArrayList lines = new ArrayList();

			while (lineIter.hasNext()) {

				ApModPurchaseOrderLineDetails apModPurchaseOrderLineDetails = new ApModPurchaseOrderLineDetails();
				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)lineIter.next();

				apModPurchaseOrderLineDetails.setPlCode(apPurchaseOrderLine.getPlCode());
				apModPurchaseOrderLineDetails.setPlLine(apPurchaseOrderLine.getPlLine());
				apModPurchaseOrderLineDetails.setPlIiName(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName());
				apModPurchaseOrderLineDetails.setPlLocName(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName());
				apModPurchaseOrderLineDetails.setPlQuantity(apPurchaseOrderLine.getPlQuantity());
				apModPurchaseOrderLineDetails.setPlUomName(apPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
				apModPurchaseOrderLineDetails.setPlUnitCost(apPurchaseOrderLine.getPlUnitCost());
				apModPurchaseOrderLineDetails.setPlAmount(apPurchaseOrderLine.getPlAmount());
				apModPurchaseOrderLineDetails.setPlPlCode(apPurchaseOrderLine.getPlPlCode());
				apModPurchaseOrderLineDetails.setPlDiscount1(apPurchaseOrderLine.getPlDiscount1());
				apModPurchaseOrderLineDetails.setPlDiscount2(apPurchaseOrderLine.getPlDiscount2());
				apModPurchaseOrderLineDetails.setPlDiscount3(apPurchaseOrderLine.getPlDiscount3());
				apModPurchaseOrderLineDetails.setPlDiscount4(apPurchaseOrderLine.getPlDiscount4());
				apModPurchaseOrderLineDetails.setPlTotalDiscount(apPurchaseOrderLine.getPlTotalDiscount());

				lines.add(apModPurchaseOrderLineDetails);

			}

			apModPurchaseOrderDetails.setPoPlList(lines);

			results[ctr++] = poRowEncode(apModPurchaseOrderDetails);
		}

		return results;

	}

	private ApModPurchaseOrderDetails getApPoByPoRcvPoNumberAndSplSupplierCodeAndAdBranch(String PO_RCV_PO_NMBR, String SPL_SPPLR_CODE, Integer AD_BRNCH, Integer AD_CMPNY)
	{

		LocalApPurchaseOrderHome apPurchaseOrderHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			apPurchaseOrderHome = (LocalApPurchaseOrderHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderHome.JNDI_NAME, LocalApPurchaseOrderHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());
		}

		LocalApPurchaseOrder apPurchaseOrder = null;

		try {

			apPurchaseOrder = apPurchaseOrderHome.findByPoRcvPoNumberAndPoReceivingAndSplSupplierCodeAndBrCode(PO_RCV_PO_NMBR, EJBCommon.FALSE, SPL_SPPLR_CODE, AD_BRNCH, AD_CMPNY);

			// get receiving item line
			Collection apPurchaseOrderLines = apPurchaseOrder.getApPurchaseOrderLines();

			Iterator i = apPurchaseOrderLines.iterator();

			while (i.hasNext()) {

				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine) i.next();

				ApModPurchaseOrderLineDetails plDetails = new ApModPurchaseOrderLineDetails();

				plDetails.setPlCode(apPurchaseOrderLine.getPlCode());
				plDetails.setPlLine(apPurchaseOrderLine.getPlLine());
				plDetails.setPlUnitCost(apPurchaseOrderLine.getPlUnitCost());
				plDetails.setPlIiName(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName());
				plDetails.setPlLocName(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName());
				plDetails.setPlUomName(apPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
				plDetails.setPlIiDescription(apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiDescription());
				plDetails.setPlDiscount1(apPurchaseOrderLine.getPlDiscount1());
				plDetails.setPlDiscount2(apPurchaseOrderLine.getPlDiscount2());
				plDetails.setPlDiscount3(apPurchaseOrderLine.getPlDiscount3());
				plDetails.setPlDiscount4(apPurchaseOrderLine.getPlDiscount4());

				double RCVD_QTY = 0d;
				double RCVD_DSCNT = 0d;
				double RCVD_AMNT = 0d;

				Collection apReceivingItemLines = apPurchaseOrderLineHome.findByPlPlCode(apPurchaseOrderLine.getPlCode(), AD_CMPNY);

				Iterator j = apReceivingItemLines.iterator();

				while (j.hasNext()) {

					LocalApPurchaseOrderLine apReceivingItemLine = (LocalApPurchaseOrderLine) j.next();

					if (apReceivingItemLine.getApPurchaseOrder().getPoPosted() == EJBCommon.TRUE) {

						RCVD_QTY += apReceivingItemLine.getPlQuantity();
						RCVD_DSCNT += apReceivingItemLine.getPlTotalDiscount();
						RCVD_AMNT += apReceivingItemLine.getPlAmount() + apReceivingItemLine.getPlTaxAmount();

					}

				}

				double REMAINING_QTY = apPurchaseOrderLine.getPlQuantity() - RCVD_QTY;
				double REMAINING_DSCNT = apPurchaseOrderLine.getPlTotalDiscount() - RCVD_DSCNT;
				double REMAINING_AMNT = apPurchaseOrderLine.getPlAmount() - RCVD_AMNT;

				if (REMAINING_QTY <= 0) continue;

				plDetails.setPlRemaining(REMAINING_QTY < 0 ? 0 : REMAINING_QTY);
				plDetails.setPlQuantity(REMAINING_QTY < 0 ? 0 : REMAINING_QTY);
				plDetails.setPlTotalDiscount(REMAINING_DSCNT);
				plDetails.setPlAmount(REMAINING_AMNT);

				list.add(plDetails);

			}

			if (list == null || list.size() == 0) {

				return null;

			}

			ApModPurchaseOrderDetails mdetails = new ApModPurchaseOrderDetails();

			mdetails.setPoCode(apPurchaseOrder.getPoCode());
			mdetails.setPoTcName(apPurchaseOrder.getApTaxCode().getTcName());
			mdetails.setPoTcType(apPurchaseOrder.getApTaxCode().getTcType());
			mdetails.setPoTcRate(apPurchaseOrder.getApTaxCode().getTcRate());
			mdetails.setPoFcName(apPurchaseOrder.getGlFunctionalCurrency().getFcName());
			mdetails.setPoPytName(apPurchaseOrder.getAdPaymentTerm().getPytName());
			mdetails.setPoSplName(apPurchaseOrder.getApSupplier().getSplName());
			mdetails.setPoConversionDate(apPurchaseOrder.getPoConversionDate());
			mdetails.setPoConversionRate(apPurchaseOrder.getPoConversionRate());

			mdetails.setPoPlList(list);

			return mdetails;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	private ApModPurchaseOrderDetails poRowDecode(String po)
	{
		Debug.print("InvItemSyncControllerBean poRowDecode");

		String separator = "$";
		ApModPurchaseOrderDetails apModPurchaseOrderDetails = new ApModPurchaseOrderDetails();

		// Remove first $ character
		po = po.substring(1);

		// PO Code
		int start = 0;
		int nextIndex = po.indexOf(separator, start);
		int length = nextIndex - start;
		System.out.print("PO CODE:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoCode(new Integer(po.substring(start, start + length)));

		// PO Type
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO TYPE:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoType(po.substring(start, start + length));

		// Currency
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO CURRENCY:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoFcName(po.substring(start, start + length));

		// Tax Code
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO TAXCODE:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoTcName(po.substring(start, start + length));

		// Supplier Code
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO SPLSUPPLIERCODE:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoSplSupplierCode(po.substring(start, start + length));

		// Payment Term
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO PAYMENTTERM:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoPytName(po.substring(start, start + length));

		// Document No
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO DOC#:"+(po.substring(start, start + length)));
		//apModPurchaseOrderDetails.setPoDocumentNumber(po.substring(start, start + length));
		apModPurchaseOrderDetails.setPoDocumentNumber("");

		// Po Receiving PO No
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO RCVPO#:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoRcvPoNumber(po.substring(start, start + length));

		// Reference No
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO REF#:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoReferenceNumber(po.substring(start, start + length));

		// PO Date
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		sdf.setLenient(false);
		try {
			System.out.print("PO DATE:"+(po.substring(start, start + length)));
			apModPurchaseOrderDetails.setPoDate(sdf.parse(po.substring(start, start + length)));
			System.out.println("Date-" + apModPurchaseOrderDetails.getPoDate());
		} catch (Exception ex) {
			System.out.println("Error On PO Date: " + ex.getMessage());
			//throw ex;
		}

		// Description
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO DESC:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoDescription(po.substring(start, start + length));

		// Conversion Rate
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO CONVRATE:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoConversionRate(Double.parseDouble(po.substring(start, start + length)));

		// Conversion Date
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		try {
			//apModPurchaseOrderDetails.setPoConversionDate(sdf.parse(po.substring(start, start + length)));
			System.out.println("Conversion Date-" + apModPurchaseOrderDetails.getPoConversionDate());
		} catch (Exception ex) {
			System.out.println("Error On Conversion Date");
			//throw ex;
		}


		/************************** Misc Details***************************/

		// Created By
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO CREATEDBY:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoCreatedBy(po.substring(start, start + length));

		// Date Created
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		try {
			System.out.print("PO DATECREATED:"+(po.substring(start, start + length)));
			apModPurchaseOrderDetails.setPoDateCreated(sdf.parse(po.substring(start, start + length)));
			System.out.println("Date Created-" + apModPurchaseOrderDetails.getPoDateCreated());
		} catch (Exception ex) {
			System.out.println("Error On Date Created");
			//throw ex;
		}

		// Last Modified By
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		System.out.print("PO LASTMODIFIEDBY:"+(po.substring(start, start + length)));
		apModPurchaseOrderDetails.setPoLastModifiedBy(po.substring(start, start + length));

		// Date Last Modified
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;
		try {
			System.out.print("PO DATELASTMODIFIED:"+(po.substring(start, start + length)));
			apModPurchaseOrderDetails.setPoDateLastModified(sdf.parse(po.substring(start, start + length)));
			System.out.println("Date Last Modified-" + apModPurchaseOrderDetails.getPoDateLastModified());
		} catch (Exception ex) {
			System.out.println("Error On Date Last Modified");
			//throw ex;
		}

		// end separator
		start = nextIndex + 1;
		nextIndex = po.indexOf(separator, start);
		length = nextIndex - start;

		String lineSeparator = "~";

		// begin lineSeparator
		start = nextIndex + 1;
		nextIndex = po.indexOf(lineSeparator, start);
		length = nextIndex - start;

		apModPurchaseOrderDetails.setPoPlList(new ArrayList());

		while (true) {
			ApModPurchaseOrderLineDetails apModPurchaseOrderLineDetails = new ApModPurchaseOrderLineDetails();

			// begin separator
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;

			// Line Code
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL CODE:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlCode(new Integer(po.substring(start, start + length)));

			// Line Number
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL LINE:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlLine((Short.parseShort(po.substring(start, start + length))));

			// Item Name
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL ITEMNAME:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlIiName(po.substring(start, start + length));

			// Location Name
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL LOCNAME:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlLocName(po.substring(start, start + length));

			// Quantity
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL QTY:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlQuantity((double)Double.parseDouble(po.substring(start, start + length)));

			// Unit of Measure
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL UOMNAME:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlUomName(po.substring(start, start + length));

			// Unit Cost
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL UNITCOST:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlUnitCost((double)Double.parseDouble(po.substring(start, start + length)));

			// Amount
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL AMOUNT:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlAmount((double)Double.parseDouble(po.substring(start, start + length)));

			// PL PL CODE
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL PLCODE:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlPlCode(new Integer(po.substring(start, start + length)));

			// Discount 1
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL DISCOUNT1:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlDiscount1((double)Double.parseDouble(po.substring(start, start + length)));

			// Discount 2
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL DISCOUNT2:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlDiscount2((double)Double.parseDouble(po.substring(start, start + length)));

			// Discount 3
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL DISCOUNT3:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlDiscount3((double)Double.parseDouble(po.substring(start, start + length)));

			// Discount 4
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL DISCOUNT4:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlDiscount4((double)Double.parseDouble(po.substring(start, start + length)));

			// Total Discount
			start = nextIndex + 1;
			nextIndex = po.indexOf(separator, start);
			length = nextIndex - start;
			System.out.print("PL TOTALDISCOUNT:"+(po.substring(start, start + length)));
			apModPurchaseOrderLineDetails.setPlTotalDiscount((double)Double.parseDouble(po.substring(start, start + length)));

			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = po.indexOf(lineSeparator, start);
			length = nextIndex - start;

			apModPurchaseOrderDetails.getPoPlList().add(apModPurchaseOrderLineDetails);

			int tempStart = nextIndex + 1;
			if (po.indexOf(separator, tempStart) == -1) break;

		}

		return apModPurchaseOrderDetails;

	}


	private String poRowEncode(ApModPurchaseOrderDetails apModPurchaseOrderDetails)
	{

		//Debug.print("InvItemSyncControllerBean poRowEncode");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer encodedResult = new StringBuffer();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_OUTPUT);

		// Start separator
		encodedResult.append(separator);

		// PO Code
		encodedResult.append(apModPurchaseOrderDetails.getPoCode());
		encodedResult.append(separator);

		// PO Type
		encodedResult.append(apModPurchaseOrderDetails.getPoType());
		encodedResult.append(separator);

		// Currency
		encodedResult.append(apModPurchaseOrderDetails.getPoFcName());
		encodedResult.append(separator);

		// Tax Code
		encodedResult.append(apModPurchaseOrderDetails.getPoTcName());
		encodedResult.append(separator);

		// Supplier Code
		encodedResult.append(apModPurchaseOrderDetails.getPoSplSupplierCode());
		encodedResult.append(separator);

		// Payment Term
		encodedResult.append(apModPurchaseOrderDetails.getPoPytName());
		encodedResult.append(separator);

		// Document No
		encodedResult.append(apModPurchaseOrderDetails.getPoDocumentNumber());
		encodedResult.append(separator);

		// Reference No
		encodedResult.append(apModPurchaseOrderDetails.getPoReferenceNumber());
		encodedResult.append(separator);

		// PO Date
		encodedResult.append(sdf.format(apModPurchaseOrderDetails.getPoDate()));
		encodedResult.append(separator);

		// Description
		encodedResult.append(apModPurchaseOrderDetails.getPoDescription());
		encodedResult.append(separator);

		// Conversion Rate
		encodedResult.append(apModPurchaseOrderDetails.getPoConversionRate());
		encodedResult.append(separator);

		// Conversion Date
		if(apModPurchaseOrderDetails.getPoConversionDate()!=null){
			encodedResult.append(sdf.format(apModPurchaseOrderDetails.getPoConversionDate()));
			encodedResult.append(separator);
		}else{
			encodedResult.append(sdf.format(apModPurchaseOrderDetails.getPoDate()));
			encodedResult.append(separator);
		}

		/************************** Misc Details***************************/

		// Created By
		encodedResult.append(apModPurchaseOrderDetails.getPoCreatedBy());
		encodedResult.append(separator);

		// Date Created
		encodedResult.append(sdf.format(apModPurchaseOrderDetails.getPoDateCreated()));
		encodedResult.append(separator);

		// Last Modified By
		encodedResult.append(apModPurchaseOrderDetails.getPoFcName());
		encodedResult.append(separator);

		// Date Last Modified
		encodedResult.append(sdf.format(apModPurchaseOrderDetails.getPoDateLastModified()));
		encodedResult.append(separator);

		// end separator
		encodedResult.append(separator);

		String lineSeparator = "~";

		// begin lineSeparator
		encodedResult.append(lineSeparator);

		Iterator i = apModPurchaseOrderDetails.getPoPlList().iterator();

		while (i.hasNext()) {

			ApModPurchaseOrderLineDetails apModPurchaseOrderLineDetails = (ApModPurchaseOrderLineDetails)i.next();

			// begin separator
			encodedResult.append(separator);

			// Line Code
			encodedResult.append(apModPurchaseOrderLineDetails.getPlCode());
			encodedResult.append(separator);

			// Line Number
			encodedResult.append(apModPurchaseOrderLineDetails.getPlLine());
			encodedResult.append(separator);

			// Item Name
			encodedResult.append(apModPurchaseOrderLineDetails.getPlIiName());
			encodedResult.append(separator);

			// Location Name
			encodedResult.append(apModPurchaseOrderLineDetails.getPlLocName());
			encodedResult.append(separator);


			// Quantity
			encodedResult.append(apModPurchaseOrderLineDetails.getPlQuantity());
			encodedResult.append(separator);

			// Unit of Measure
			encodedResult.append(apModPurchaseOrderLineDetails.getPlUomName());
			encodedResult.append(separator);

			// Unit Cost
			encodedResult.append(apModPurchaseOrderLineDetails.getPlUnitCost());
			encodedResult.append(separator);

			// Amount
			encodedResult.append(apModPurchaseOrderLineDetails.getPlAmount());
			encodedResult.append(separator);

			// PL CODE
			encodedResult.append(apModPurchaseOrderDetails.getPoCode());
			encodedResult.append(separator);

			// Discount 1
			encodedResult.append(apModPurchaseOrderLineDetails.getPlDiscount1());
			encodedResult.append(separator);

			// Discount 2
			encodedResult.append(apModPurchaseOrderLineDetails.getPlDiscount2());
			encodedResult.append(separator);

			// Discount 3
			encodedResult.append(apModPurchaseOrderLineDetails.getPlDiscount3());
			encodedResult.append(separator);

			// Discount 4
			encodedResult.append(apModPurchaseOrderLineDetails.getPlDiscount4());
			encodedResult.append(separator);

			// Total Discount
			encodedResult.append(apModPurchaseOrderLineDetails.getPlTotalDiscount());
			encodedResult.append(separator);

			// begin lineSeparator
			encodedResult.append(lineSeparator);
		}

		return encodedResult.toString();

	}

	/**
	 * @ejb:interface-method
	 **/
	public String getPaymentTermsAll(Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean getPaymentTermsAll");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer paymentTermsStr = new StringBuffer();

		LocalAdPaymentTermHome adPaymentTermHome = null;

		// Initialize EJB Home
		try {
			adPaymentTermHome = (LocalAdPaymentTermHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPaymentTermHome.JNDI_NAME, LocalAdPaymentTermHome.class);
		} catch (NamingException ex) {
			return (ex.getMessage());
		}

		try{
			Collection paymentTerms = adPaymentTermHome.findPytAll(AD_CMPNY);


			Iterator i = paymentTerms.iterator();

			while(i.hasNext()) {
				LocalAdPaymentTerm adPaymentTerm = (LocalAdPaymentTerm)i.next();

				paymentTermsStr.append(separator);
				paymentTermsStr.append(adPaymentTerm.getPytName());

			}

			paymentTermsStr.append(separator);

			return paymentTermsStr.toString();

		}catch (Exception ex) {
			return (ex.getMessage());
		}

	}

	/**
	 * @ejb:interface-method
	 **/
	public String getTaxCodesAll(Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean getTaxCodesAll");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer paymentTermsStr = new StringBuffer();

		LocalApTaxCodeHome apTaxCodeHome = null;

		// Initialize EJB Home
		try {

			apTaxCodeHome = (LocalApTaxCodeHome)EJBHomeFactory.
			lookUpLocalHome(LocalApTaxCodeHome.JNDI_NAME, LocalApTaxCodeHome.class);

		} catch (NamingException ex) {

			return (ex.getMessage());

		}

		try{
			Collection apTaxCodes = apTaxCodeHome.findTcAll(AD_CMPNY);


			Iterator i = apTaxCodes.iterator();

			while(i.hasNext()) {
				LocalApTaxCode apTaxCode = (LocalApTaxCode)i.next();

				paymentTermsStr.append(separator);
				paymentTermsStr.append(apTaxCode.getTcName());

			}

			paymentTermsStr.append(separator);

			return paymentTermsStr.toString();

		}catch (Exception ex) {
			return (ex.getMessage());
		}

	}

	/**
	 * @ejb:interface-method
	 **/
	public String getFunctionalCurrenciesAll(Integer AD_CMPNY)
	{
		Debug.print("InvItemSyncControllerBean getFunctionalCurrenciesAll");

		char separator = EJBCommon.SEPARATOR;
		StringBuffer paymentTermsStr = new StringBuffer();

		LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;

		// Initialize EJB Home
		try {

			glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);

		} catch (NamingException ex) {

			return (ex.getMessage());

		}

		try{
			Collection glFunctionalCurrencies = glFunctionalCurrencyHome.findFcAll(AD_CMPNY);


			Iterator i = glFunctionalCurrencies.iterator();

			while(i.hasNext()) {
				LocalGlFunctionalCurrency glFunctionalCurrency = (LocalGlFunctionalCurrency)i.next();

				paymentTermsStr.append(separator);
				paymentTermsStr.append(glFunctionalCurrency.getFcName());

			}

			paymentTermsStr.append(separator);

			return paymentTermsStr.toString();

		}catch (Exception ex) {
			return (ex.getMessage());
		}
	}


	private double getInvFifoCost2(Date CST_DT, Integer IL_CODE, double CST_QTY,
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {

		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;
	  	 LocalAdPreferenceHome adPreferenceHome = null;
	       double isNotSingle=0d;
	     // Initialize EJB Home

	     try {

	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	    	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
	     }
	     catch (NamingException ex) {

	    	 throw new EJBException(ex.getMessage());
	     }

		try {

			 CostA=0d;
			 CostB=0d;
			 UnitA=0d;
			 UnitB=0d;


			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
			System.out.println("TEST " + CST_DT + " " + IL_CODE + " " + CST_QTY + " " + AD_BRNCH + " " + AD_CMPNY);
			if (invFifoCostings.size() > 0) {
				double fifoCost = 0;
			    double runningQty = Math.abs(CST_QTY);
			   System.out.println("Start ----:" + CST_QTY );
				Iterator x = invFifoCostings.iterator();
				while (x.hasNext()) {

					LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	  				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
	  				double newRemainingLifoQuantity = 0;
	  				if (invFifoCosting.getCstRemainingLifoQuantity() <= runningQty) {
	  					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * invFifoCosting.getCstRemainingLifoQuantity();
		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  				fifoCost  += invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold() * invFifoCosting.getCstRemainingLifoQuantity();
	 	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * invFifoCosting.getCstRemainingLifoQuantity();
	 	 	  			} else {
	 	 	  				fifoCost += invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity() * invFifoCosting.getCstRemainingLifoQuantity();
	 	 	  			}
	  					runningQty = runningQty - invFifoCosting.getCstRemainingLifoQuantity();
	  					//System.out.println("runningQty ----:" + runningQty );
	  					double xxxxx=Math.abs(CST_QTY)-runningQty;
	  					System.out.println("Unit ----:" + xxxxx );
	  					System.out.println("Cost ---:" + fifoCost);
	  					CostA=fifoCost;
	  					UnitA=xxxxx;
	  				} else {
	  					System.out.println("CostForward ---:" + fifoCost);
	  					double fifoCost2=fifoCost;
	  					if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * runningQty;
		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  				fifoCost  += invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold() * runningQty;
	 	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	 	  				fifoCost += invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived() * runningQty;
	 	 	  			} else {
	 	 	  				fifoCost += invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity() * runningQty;
	 	 	  			}
	  					newRemainingLifoQuantity = invFifoCosting.getCstRemainingLifoQuantity() - runningQty;

	  					fifoCost2=fifoCost -fifoCost2;
	  					System.out.println("Unit ----:" + runningQty);
	  					System.out.println("Cost ---:" + fifoCost2);
	  					CostB=fifoCost2;
	  					UnitB=runningQty;
	  					runningQty = 0;
	  					isNotSingle=1;
	  				}
	 	  			if (isAdjustFifo) invFifoCosting.setCstRemainingLifoQuantity(newRemainingLifoQuantity);
					if (runningQty <=0) break;
				}
				System.out.println("fifoCost" + fifoCost);
				System.out.println("CST_QTY" + CST_QTY);
				LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
				return isNotSingle;

			}
			else {

				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return isNotSingle;
			}

		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}


	public void ejbCreate() throws CreateException {

		Debug.print("InvItemSyncControllerBean ejbCreate");

	}

	public void ejbRemove() {};

	public void ejbActivate() {}
	public void ejbPassivate() {}

	public void setSessionContext(SessionContext ctx) {}

}