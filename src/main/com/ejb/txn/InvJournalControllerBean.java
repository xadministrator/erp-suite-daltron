
/*
 * InvJournalControllerBean.java
 *
 * Created on August 30, 2004, 11:48 AM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAssemblyTransfer;
import com.ejb.inv.LocalInvAssemblyTransferHome;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.ejb.inv.LocalInvBranchStockTransferHome;
import com.ejb.inv.LocalInvBuildUnbuildAssembly;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvOverhead;
import com.ejb.inv.LocalInvOverheadHome;
import com.ejb.inv.LocalInvStockIssuance;
import com.ejb.inv.LocalInvStockIssuanceHome;
import com.ejb.inv.LocalInvStockTransfer;
import com.ejb.inv.LocalInvStockTransferHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModDistributionRecordDetails;

/**
 * @ejb:bean name="InvJournalControllerEJB"
 *           display-name="used for viewing journals"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvJournalControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvJournalController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvJournalControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvJournalControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvDrByAdjCode(Integer ADJ_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvJournalControllerBean getInvDrByAdjCode");
        
        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
    
        // Initialize EJB Home
        
        try {
            
            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvAdjustment invAdjustment = null;

        	try {
        		
        		invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection invDistributionRecords = invDistributionRecordHome.findByAdjCode(invAdjustment.getAdjCode(), AD_CMPNY);       	            
        	
        	short lineNumber = 1;
        	
        	Iterator i = invDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
        		
        		InvModDistributionRecordDetails mdetails = new InvModDistributionRecordDetails();
        		
        		mdetails.setDrCode(invDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(invDistributionRecord.getDrClass());
        		mdetails.setDrDebit(invDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(invDistributionRecord.getDrAmount());
        		mdetails.setDrCoaAccountNumber(invDistributionRecord.getInvChartOfAccount().getCoaAccountNumber());      		
			    mdetails.setDrCoaAccountDescription(invDistributionRecord.getInvChartOfAccount().getCoaAccountDescription());			    
			    
        		list.add(mdetails);
        		
        		lineNumber++;
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvDrByBuaCode(Integer BUA_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvJournalControllerBean getInvDrByBuaCode");
        
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
      
        // Initialize EJB Home
        
        try {
            
            invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = null;
        	        	
        	try {
        		
        		invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(BUA_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection invDistributionRecords = invDistributionRecordHome.findByBuaCode(invBuildUnbuildAssembly.getBuaCode(), AD_CMPNY);       	            
        	
        	short lineNumber = 1;
        	
        	Iterator i = invDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
        		
        		InvModDistributionRecordDetails mdetails = new InvModDistributionRecordDetails();
        		
        		mdetails.setDrCode(invDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(invDistributionRecord.getDrClass());
        		mdetails.setDrDebit(invDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(invDistributionRecord.getDrAmount());
        		mdetails.setDrCoaAccountNumber(invDistributionRecord.getInvChartOfAccount().getCoaAccountNumber());      		
			    mdetails.setDrCoaAccountDescription(invDistributionRecord.getInvChartOfAccount().getCoaAccountDescription());			    
			    
        		list.add(mdetails);
        		
        		lineNumber++;
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvDrByOhCode(Integer OH_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvJournalControllerBean getInvDrByOhCode");
        
        LocalInvOverheadHome  invOverheadHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
  
        // Initialize EJB Home
        
        try {
            
            invOverheadHome = (LocalInvOverheadHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvOverheadHome.JNDI_NAME, LocalInvOverheadHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
     
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvOverhead invOverhead = null;
        	
        	
        	try {
        		
        		invOverhead = invOverheadHome.findByPrimaryKey(OH_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection invDistributionRecords = invDistributionRecordHome.findByOhCode(invOverhead.getOhCode(), AD_CMPNY);       	            
        	
        	short lineNumber = 1;
        	
        	Iterator i = invDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
        		
        		InvModDistributionRecordDetails mdetails = new InvModDistributionRecordDetails();
        		
        		mdetails.setDrCode(invDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(invDistributionRecord.getDrClass());
        		mdetails.setDrDebit(invDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(invDistributionRecord.getDrAmount());
        		mdetails.setDrCoaAccountNumber(invDistributionRecord.getInvChartOfAccount().getCoaAccountNumber());      		
			    mdetails.setDrCoaAccountDescription(invDistributionRecord.getInvChartOfAccount().getCoaAccountDescription());			    
			    
        		list.add(mdetails);
        		
        		lineNumber++;
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvDrBySiCode(Integer SI_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvJournalControllerBean getInvDrBySiCode");
        
        LocalInvStockIssuanceHome invStockIssuanceHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
  
        // Initialize EJB Home
        
        try {
            
        	invStockIssuanceHome = (LocalInvStockIssuanceHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvStockIssuanceHome.JNDI_NAME, LocalInvStockIssuanceHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
     
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvStockIssuance invStockIssuance = null;
        	
        	
        	try {
        		
        		invStockIssuance = invStockIssuanceHome.findByPrimaryKey(SI_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection invDistributionRecords = invDistributionRecordHome.findBySiCode(invStockIssuance.getSiCode(), AD_CMPNY);       	            
        	
        	short lineNumber = 1;
        	
        	Iterator i = invDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
        		
        		InvModDistributionRecordDetails mdetails = new InvModDistributionRecordDetails();
        		
        		mdetails.setDrCode(invDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(invDistributionRecord.getDrClass());
        		mdetails.setDrDebit(invDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(invDistributionRecord.getDrAmount());
        		mdetails.setDrCoaAccountNumber(invDistributionRecord.getInvChartOfAccount().getCoaAccountNumber());      		
			    mdetails.setDrCoaAccountDescription(invDistributionRecord.getInvChartOfAccount().getCoaAccountDescription());			    
			    
        		list.add(mdetails);
        		
        		lineNumber++;
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvDrByAtrCode(Integer ATR_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvJournalControllerBean getInvDrByAtrCode");
        
        LocalInvAssemblyTransferHome  invAssemblyTransferHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
  
        // Initialize EJB Home
        
        try {
            
        	invAssemblyTransferHome = (LocalInvAssemblyTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAssemblyTransferHome.JNDI_NAME, LocalInvAssemblyTransferHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
     
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvAssemblyTransfer invAssemblyTransfer = null;
        	
        	
        	try {
        		
        		invAssemblyTransfer = invAssemblyTransferHome.findByPrimaryKey(ATR_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection invDistributionRecords = invDistributionRecordHome.findByAtrCode(invAssemblyTransfer.getAtrCode(), AD_CMPNY);       	            
        	
        	short lineNumber = 1;
        	
        	Iterator i = invDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
        		
        		InvModDistributionRecordDetails mdetails = new InvModDistributionRecordDetails();
        		
        		mdetails.setDrCode(invDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(invDistributionRecord.getDrClass());
        		mdetails.setDrDebit(invDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(invDistributionRecord.getDrAmount());
        		mdetails.setDrCoaAccountNumber(invDistributionRecord.getInvChartOfAccount().getCoaAccountNumber());      		
			    mdetails.setDrCoaAccountDescription(invDistributionRecord.getInvChartOfAccount().getCoaAccountDescription());			    
			    
        		list.add(mdetails);
        		
        		lineNumber++;
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvDrByStCode(Integer ST_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvJournalControllerBean getInvDrByStCode");
        
        LocalInvStockTransferHome invStockTransferHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
    
        // Initialize EJB Home
        
        try {
            
            invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvStockTransfer invStockTransfer = null;

        	try {
        		
        	    invStockTransfer = invStockTransferHome.findByPrimaryKey(ST_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection invDistributionRecords = invDistributionRecordHome.findByStCode(invStockTransfer.getStCode(), AD_CMPNY);      	            
        	
        	short lineNumber = 1;
        	
        	Iterator i = invDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
        		
        		InvModDistributionRecordDetails mdetails = new InvModDistributionRecordDetails();
        		
        		mdetails.setDrCode(invDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(invDistributionRecord.getDrClass());
        		mdetails.setDrDebit(invDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(invDistributionRecord.getDrAmount());
        		mdetails.setDrCoaAccountNumber(invDistributionRecord.getInvChartOfAccount().getCoaAccountNumber());      		
			    mdetails.setDrCoaAccountDescription(invDistributionRecord.getInvChartOfAccount().getCoaAccountDescription());			    
			    
        		list.add(mdetails);
        		
        		lineNumber++;
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvDrByBstCode(Integer BST_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvJournalControllerBean getInvDrByBstCode");
        
        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
    
        // Initialize EJB Home
        
        try {
            
            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvBranchStockTransfer invBranchStockTransfer = null;

        	try {
        		
        	    invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(BST_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get distribution records
        	
        	Collection invDistributionRecords = invDistributionRecordHome.findByBstCode(invBranchStockTransfer.getBstCode(), AD_CMPNY);      	            
        	
        	short lineNumber = 1;
        	
        	Iterator i = invDistributionRecords.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
        		
        		InvModDistributionRecordDetails mdetails = new InvModDistributionRecordDetails();
        		
        		mdetails.setDrCode(invDistributionRecord.getDrCode());
        		mdetails.setDrLine(lineNumber);
        		mdetails.setDrClass(invDistributionRecord.getDrClass());
        		mdetails.setDrDebit(invDistributionRecord.getDrDebit());
        		mdetails.setDrAmount(invDistributionRecord.getDrAmount());
        		mdetails.setDrCoaAccountNumber(invDistributionRecord.getInvChartOfAccount().getCoaAccountNumber());      		
			    mdetails.setDrCoaAccountDescription(invDistributionRecord.getInvChartOfAccount().getCoaAccountDescription());			    
			    
        		list.add(mdetails);
        		
        		lineNumber++;
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

       Debug.print("InvJournalControllerBean getGlFcPrecisionUnit");

      
       LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }

       try {
       	
         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                     
       } catch (Exception ex) {
       	 
       	 Debug.printStackTrace(ex);
         throw new EJBException(ex.getMessage());
         
       }

    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getAdPrfInvJournalLineNumber(Integer AD_CMPNY) {

        Debug.print("InvJournalControllerBean getAdPrfInvJournalLineNumber");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       
        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfInvInventoryLineNumber();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void saveInvDrEntry(Integer PRMRY_KEY, ArrayList drList, String transactionType, Integer AD_BRNCH, Integer AD_CMPNY) 
       throws GlobalAccountNumberInvalidException {
       
       Debug.print("InvJournalControllerBean updateInvDrEntry");

       LocalInvDistributionRecordHome invDistributionRecordHome = null;
       LocalGlChartOfAccountHome glChartOfAccountHome = null;
       LocalInvAdjustmentHome invAdjustmentHome = null;
       LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
       LocalInvOverheadHome invOverheadHome = null;
       LocalInvStockIssuanceHome invStockIssuanceHome = null;
       LocalInvAssemblyTransferHome invAssemblyTransferHome = null;
       LocalInvStockTransferHome invStockTransferHome = null;
       LocalInvBranchStockTransferHome invBranchStockTransferHome = null;

       // Initialize EJB Home
      
       try {
            
       		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
               	lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
       		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
               	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
       		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
   	   	   		lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
       		invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
	   	   		lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
       		invOverheadHome = (LocalInvOverheadHome)EJBHomeFactory.
   	   			lookUpLocalHome(LocalInvOverheadHome.JNDI_NAME, LocalInvOverheadHome.class);       		
       		invStockIssuanceHome = (LocalInvStockIssuanceHome)EJBHomeFactory.
	   			lookUpLocalHome(LocalInvStockIssuanceHome.JNDI_NAME, LocalInvStockIssuanceHome.class);       		
       		invAssemblyTransferHome = (LocalInvAssemblyTransferHome)EJBHomeFactory.
	   			lookUpLocalHome(LocalInvAssemblyTransferHome.JNDI_NAME, LocalInvAssemblyTransferHome.class);       		
       		invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
	   			lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);       		
       		invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
	   			lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);       		
        
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
             
       try {
        
 	       LocalInvAdjustment invAdjustment = null;
 	       LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = null;
 	       LocalInvOverhead invOverhead = null;
 	       LocalInvStockIssuance invStockIssuance = null;
 	       LocalInvAssemblyTransfer invAssemblyTransfer = null;
 	       LocalInvStockTransfer invStockTransfer = null;
 	       LocalInvBranchStockTransfer invBranchStockTransfer = null;
 	       
 	       Collection invDistributionRecords = null;
 	       Iterator i;
 	       
 	       if (transactionType.equals("ADJUSTMENT")) {
 	       	
 	       		invAdjustment = invAdjustmentHome.findByPrimaryKey(PRMRY_KEY);
 	       		
 	            // remove all distribution records
 	       		
 	       		invDistributionRecords = invAdjustment.getInvDistributionRecords();
 	       		
 	       	    i = invDistributionRecords.iterator();     	  
 	       	  
 	       	    while (i.hasNext()) {
 	       	  	
 	       	   	    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
 	       	  	   
 	       	  	    i.remove();
 	       	  	    
 	       	  	    invDistributionRecord.remove();
 	       	  	
 	       	    }
 	       	           	    
 	       	    // add new distribution records
 	       	  
 	       	    i = drList.iterator();
 	       	  
 	       	    while (i.hasNext()) {
 	       	  	
 	       	  	    InvModDistributionRecordDetails mDrDetails = (InvModDistributionRecordDetails) i.next();      	  	  
 	       	  	          	  	  
 	       	  	    this.addInvDrEntry(mDrDetails, invAdjustment, null, null, null, null, null, null, AD_BRNCH, AD_CMPNY);
 	       	  	
 	       	    }
 	       			       			       	
 	       } else if (transactionType.equals("BUILD/UNBUILD ASSEMBLY")) {
 	       	
	 	       invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(PRMRY_KEY);
	       		
	            // remove all distribution records
	       		
	       		invDistributionRecords = invBuildUnbuildAssembly.getInvDistributionRecords();
	       		
	       	    i = invDistributionRecords.iterator();     	  
	       	  
	       	    while (i.hasNext()) {
	       	  	
	       	   	    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
	       	  	   
	       	  	    i.remove();
	       	  	    
	       	  	    invDistributionRecord.remove();
	       	  	
	       	    }
	       	           	    
	       	    // add new distribution records
	       	  
	       	    i = drList.iterator();
	       	  
	       	    while (i.hasNext()) {
	       	  	
	       	  	    InvModDistributionRecordDetails mDrDetails = (InvModDistributionRecordDetails) i.next();      	  	  
	       	  	          	  	  
	       	  	    this.addInvDrEntry(mDrDetails, null, invBuildUnbuildAssembly, null, null, null, null, null, AD_BRNCH, AD_CMPNY);
	       	  	
	       	    }
 	       	
 	       } else if (transactionType.equals("OVERHEAD")) {
 	       	
 	       		invOverhead = invOverheadHome.findByPrimaryKey(PRMRY_KEY);
 	       	
	            // remove all distribution records
	       		
	       		invDistributionRecords = invOverhead.getInvDistributionRecords();
	       		
	       	    i = invDistributionRecords.iterator();     	  
	       	  
	       	    while (i.hasNext()) {
	       	  	
	       	   	    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
	       	  	   
	       	  	    i.remove();
	       	  	    
	       	  	    invDistributionRecord.remove();
	       	  	
	       	    }
	       	           	    
	       	    // add new distribution records
	       	  
	       	    i = drList.iterator();
	       	  
	       	    while (i.hasNext()) {
	       	  	
	       	  	    InvModDistributionRecordDetails mDrDetails = (InvModDistributionRecordDetails) i.next();      	  	  
	       	  	          	  	  
	       	  	    this.addInvDrEntry(mDrDetails, null, null, invOverhead, null, null, null, null, AD_BRNCH, AD_CMPNY);
	       	  	
	       	    } 	       	
 	       	
 	       } else if (transactionType.equals("STOCK ISSUANCE")) {
 	       	
	       		invStockIssuance = invStockIssuanceHome.findByPrimaryKey(PRMRY_KEY);
	       		
	            // remove all distribution records
	       		
	       		invDistributionRecords = invStockIssuance.getInvDistributionRecords();
	       		
	       	    i = invDistributionRecords.iterator();     	  
	       	  
	       	    while (i.hasNext()) {
	       	  	
	       	   	    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
	       	  	   
	       	  	    i.remove();
	       	  	    
	       	  	    invDistributionRecord.remove();
	       	  	
	       	    }
	       	           	    
	       	    // add new distribution records
	       	  
	       	    i = drList.iterator();
	       	  
	       	    while (i.hasNext()) {
	       	  	
	       	  	    InvModDistributionRecordDetails mDrDetails = (InvModDistributionRecordDetails) i.next();      	  	  
	       	  	          	  	  
	       	  	    this.addInvDrEntry(mDrDetails, null, null, null, invStockIssuance, null, null, null, AD_BRNCH, AD_CMPNY);
	       	  	
	       	    }
	       			       			       	
 	       } else if (transactionType.equals("ASSEMBLY TRANSFER")) {
 	       	
       		invAssemblyTransfer = invAssemblyTransferHome.findByPrimaryKey(PRMRY_KEY);
       		
            // remove all distribution records
       		
       		invDistributionRecords = invAssemblyTransfer.getInvDistributionRecords();
       		
       	    i = invDistributionRecords.iterator();     	  
       	  
       	    while (i.hasNext()) {
       	  	
       	   	    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
       	  	   
       	  	    i.remove();
       	  	    
       	  	    invDistributionRecord.remove();
       	  	
       	    }
       	           	    
       	    // add new distribution records
       	  
       	    i = drList.iterator();
       	  
       	    while (i.hasNext()) {
       	  	
       	  	    InvModDistributionRecordDetails mDrDetails = (InvModDistributionRecordDetails) i.next();      	  	  
       	  	          	  	  
       	  	    this.addInvDrEntry(mDrDetails, null, null, null, null, invAssemblyTransfer, null, null, AD_BRNCH, AD_CMPNY);
       	  	
       	    }
       			       			       	
	       } else if (transactionType.equals("STOCK TRANSFER")) {
 	       	
       		invStockTransfer = invStockTransferHome.findByPrimaryKey(PRMRY_KEY);
       		
            // remove all distribution records
       		
       		invDistributionRecords = invStockTransfer.getInvDistributionRecords();
       		
       	    i = invDistributionRecords.iterator();     	  
       	  
       	    while (i.hasNext()) {
       	  	
       	   	    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
       	  	   
       	  	    i.remove();
       	  	    
       	  	    invDistributionRecord.remove();
       	  	
       	    }
       	           	    
       	    // add new distribution records
       	  
       	    i = drList.iterator();
       	  
       	    while (i.hasNext()) {
       	  	
       	  	    InvModDistributionRecordDetails mDrDetails = (InvModDistributionRecordDetails) i.next();      	  	  
       	  	          	  	  
       	  	    this.addInvDrEntry(mDrDetails, null, null, null, null, null, invStockTransfer, null, AD_BRNCH, AD_CMPNY);
       	  	
       	    }
       			       			       	
	       } else if (transactionType.equals("BRANCH STOCK TRANSFER OUT") || transactionType.equals("BRANCH STOCK TRANSFER IN")) {
 	       	
       		invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(PRMRY_KEY);
       		
            // remove all distribution records
       		
       		invDistributionRecords = invBranchStockTransfer.getInvDistributionRecords();
       		
       	    i = invDistributionRecords.iterator();     	  
       	  
       	    while (i.hasNext()) {
       	  	
       	   	    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
       	  	   
       	  	    i.remove();
       	  	    
       	  	    invDistributionRecord.remove();
       	  	
       	    }
       	           	    
       	    // add new distribution records
       	  
       	    i = drList.iterator();
       	  
       	    while (i.hasNext()) {
       	  	
       	  	    InvModDistributionRecordDetails mDrDetails = (InvModDistributionRecordDetails) i.next();      	  	  
       	  	          	  	  
       	  	    this.addInvDrEntry(mDrDetails, null, null, null, null, null, null, invBranchStockTransfer, AD_BRNCH, AD_CMPNY);
       	  	
       	    }
       			       			       	
	       }

       }catch ( GlobalAccountNumberInvalidException ex) {
   	    
    	   getSessionContext().setRollbackOnly();
    	   throw ex;

       } catch (Exception ex) {
       
    	  Debug.printStackTrace(ex);
    	  getSessionContext().setRollbackOnly();
    	  throw new EJBException(ex.getMessage());
       }
    }
    
    // private methods
    
    private void addInvDrEntry(InvModDistributionRecordDetails mdetails, LocalInvAdjustment invAdjustment, LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly,
    	LocalInvOverhead invOverhead, LocalInvStockIssuance invStockIssuance, LocalInvAssemblyTransfer invAssemblyTransfer, 
		LocalInvStockTransfer invStockTransfer, LocalInvBranchStockTransfer invBranchStockTransfer, Integer AD_BRNCH, Integer AD_CMPNY) 
		throws GlobalAccountNumberInvalidException {
	
    	Debug.print("ArJournalControllerBean addArDrEntry");

    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
   
        // Initialize EJB Home
    
    	try {
        
	        invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
	        glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);            
	        adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
	         
	                    
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }            
	            
	    try {
	    	
	    	// get company
	    	
	    	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	    	
	    	// validate if coa exists
	    	
	    	LocalGlChartOfAccount glChartOfAccount = null;
	    	
	    	try {
	    	
	    		glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(mdetails.getDrCoaAccountNumber(), AD_BRNCH, AD_CMPNY);
	    		
	    		if (glChartOfAccount.getCoaEnable() == EJBCommon.FALSE) 
	    		    throw new GlobalAccountNumberInvalidException(String.valueOf(mdetails.getDrLine()));
	    		
	    	} catch (FinderException ex) {
	    		
	    		throw new GlobalAccountNumberInvalidException(String.valueOf(mdetails.getDrLine()));
	    		
	    	}
	    	        			    
		    // create distribution record 
		    
		    LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
			    mdetails.getDrLine(), 
			    mdetails.getDrClass(), mdetails.getDrDebit(), 
			    EJBCommon.roundIt(mdetails.getDrAmount(), adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
		    
		    glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
		    
		    if (invAdjustment != null) {
		    	
		    	invAdjustment.addInvDistributionRecord(invDistributionRecord);
		    	
		    } else if (invBuildUnbuildAssembly != null) {
		    	
		    	invBuildUnbuildAssembly.addInvDistributionRecord(invDistributionRecord);
		    	
		    } else if (invOverhead != null) {
		    	
		    	invOverhead.addInvDistributionRecord(invDistributionRecord);
		    	
		    } else if (invStockIssuance != null) {
		    	
		    	invStockIssuance.addInvDistributionRecord(invDistributionRecord);
		    	
		    } else if (invAssemblyTransfer != null) {
		    	
		    	invAssemblyTransfer.addInvDistributionRecord(invDistributionRecord);
		    	
		    } else if (invStockTransfer != null) {
		    	
		    	invStockTransfer.addInvDistributionRecord(invDistributionRecord);
		    	
		    } else if (invBranchStockTransfer != null) {
		    	
		    	invBranchStockTransfer.addInvDistributionRecord(invDistributionRecord);
		    	
		    }
		    		    							    		    
		} catch ( GlobalAccountNumberInvalidException ex) {        	
	    	
	    	getSessionContext().setRollbackOnly();
	    	throw ex;
		    		            		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }

}
    
	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvJournalControllerBean ejbCreate");
      
    }

}