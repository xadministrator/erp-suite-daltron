
/*
 * InvLineItemTemplateEntryControllerBean.java
 *
 * Created on October 30, 2005, 10:10 AM
 *
 * @author  Jolly Martin
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLineItem;
import com.ejb.inv.LocalInvLineItemHome;
import com.ejb.inv.LocalInvLineItemTemplate;
import com.ejb.inv.LocalInvLineItemTemplateHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvLineItemTemplateDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="InvLineItemTemplateControllerEJB"
 *           display-name="Used for creating Line Item templates"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvLineItemTemplateControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvLineItemTemplateController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvLineItemTemplateControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvLineItemTemplateControllerBean extends AbstractSessionBean {
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void addInvLitEntry(com.util.InvLineItemTemplateDetails details, ArrayList liList, Integer AD_CMPNY)
	throws GlobalRecordAlreadyExistException,
	GlobalInvItemLocationNotFoundException {
		
		Debug.print("InvLineItemTemplateControllerBean addInvLitEntry");
		
		LocalInvLineItemTemplateHome invLineItemTemplateHome = null;
		LocalInvLineItemHome invLineItemHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalArCustomerHome arCustomerHome = null;
		
		try {
			
			invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);
			invLineItemHome = (LocalInvLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLineItemHome.JNDI_NAME, LocalInvLineItemHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalInvLineItemTemplate invLineItemTemplate = null;
			
			// validate if template already exists
			
			try {
				
				invLineItemTemplate = invLineItemTemplateHome.findByLitName(details.getLitName(), AD_CMPNY);
				
				if(details.getLitCode() == null || details.getLitCode() != null && 
						!invLineItemTemplate.getLitCode().equals(details.getLitCode())) {          
					
					throw new GlobalRecordAlreadyExistException();
					
				}   
				
			} catch (GlobalRecordAlreadyExistException ex) {
				
				throw ex;
				
			} catch (FinderException ex) {
				
			}
			
			invLineItemTemplate = invLineItemTemplateHome.create(details.getLitCode(), 
					details.getLitName(), details.getLitDescription(), AD_CMPNY);
			
			// add line items
			
			Iterator i = liList.iterator();
			
			while (i.hasNext()) {
				
				InvModLineItemDetails mdetails = (InvModLineItemDetails)i.next();
				
				LocalInvLineItem invLineItem = invLineItemHome.create(mdetails.getLiLine(), AD_CMPNY);
				
				invLineItemTemplate.addInvLineItem(invLineItem);
				
				LocalInvItemLocation invItemLocation = null;
				
				try {
					
					invItemLocation = invItemLocationHome.findByLocNameAndIiName(
							mdetails.getLiLocName(), mdetails.getLiIiName(), AD_CMPNY);
					
				} catch (FinderException ex) {
					
					throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getLiLine()));
					
				}
				
				invItemLocation.addInvLineItem(invLineItem);
				
				LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getLiUomName(), AD_CMPNY);
				invUnitOfMeasure.addInvLineItem(invLineItem);
				
			}
			
		} catch (GlobalRecordAlreadyExistException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;	 		
			
		} catch (GlobalInvItemLocationNotFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}   
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void updateInvLitEntry(com.util.InvLineItemTemplateDetails details, ArrayList liList, Integer AD_CMPNY)
	throws GlobalRecordAlreadyDeletedException,
	GlobalInvItemLocationNotFoundException {
		
		Debug.print("InvLineItemTemplateControllerBean updateInvLitEntry");
		
		LocalInvLineItemTemplateHome invLineItemTemplateHome = null;
		LocalInvLineItemHome invLineItemHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalArCustomerHome arCustomerHome = null;
		
		try {
			
			invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);
			invLineItemHome = (LocalInvLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLineItemHome.JNDI_NAME, LocalInvLineItemHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalInvLineItemTemplate invLineItemTemplate = null;
			
			// validate if template already deleted
			
			try {
        		
        		if (details.getLitCode() != null) {
        			
        			invLineItemTemplate = invLineItemTemplateHome.findByPrimaryKey(details.getLitCode());
        			
        		}
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
			
			boolean isRecalculate = true;
			
			// update line item template 
			
			invLineItemTemplate = invLineItemTemplateHome.findByPrimaryKey(details.getLitCode());
			
			invLineItemTemplate.setLitName(details.getLitName());
			invLineItemTemplate.setLitDescription(details.getLitDescription());
			
			// check if critical fields are changed
			
			if (liList.size() != invLineItemTemplate.getInvLineItems().size()) {
				
				isRecalculate = true;
				
			} else if (liList.size() == invLineItemTemplate.getInvLineItems().size()) {
				
				Iterator liIter = invLineItemTemplate.getInvLineItems().iterator();
				Iterator liListIter = liList.iterator();
				
				while (liIter.hasNext()) {
					
					LocalInvLineItem invLineItem = (LocalInvLineItem)liIter.next();
					InvModLineItemDetails mdetails = (InvModLineItemDetails)liListIter.next();
					
					if (!invLineItem.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getLiIiName()) ||
							!invLineItem.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getLiLocName()) ||
							!invLineItem.getInvUnitOfMeasure().getUomName().equals(mdetails.getLiUomName())) {
						
						isRecalculate = true;
						break;
						
					}
					
					isRecalculate = false;
					
				}
				
			}
			
			if(isRecalculate) {
				
				// remove all line items
				
				Collection invLineItems = invLineItemTemplate.getInvLineItems();
				
				Iterator i = invLineItems.iterator();     	  
				
				while (i.hasNext()) {
					
					LocalInvLineItem invLineItem = (LocalInvLineItem)i.next();
					
					i.remove();
					
					invLineItem.remove();
					
				}
				
				// add line items
				
				i = liList.iterator();
				
				while (i.hasNext()) {
					
					InvModLineItemDetails mdetails = (InvModLineItemDetails)i.next();
					
					LocalInvLineItem invLineItem = invLineItemHome.create(mdetails.getLiLine(), AD_CMPNY);
					
					invLineItemTemplate.addInvLineItem(invLineItem);
					
					LocalInvItemLocation invItemLocation = null;
					
					try {
						
						invItemLocation = invItemLocationHome.findByLocNameAndIiName(
								mdetails.getLiLocName(), mdetails.getLiIiName(), AD_CMPNY);
						
					} catch (FinderException ex) {
						
						throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getLiLine()));
						
					}
					
					invItemLocation.addInvLineItem(invLineItem);
					
					LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getLiUomName(), AD_CMPNY);
					invUnitOfMeasure.addInvLineItem(invLineItem);
					
				}
				
			}
			
		} catch (GlobalRecordAlreadyDeletedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;	 		
			
		} catch (GlobalInvItemLocationNotFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}   
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public InvLineItemTemplateDetails getInvLitByLitCode(Integer LIT_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException  {
		
		Debug.print("InvLineItemTemplateControllerBean getInvLitByLitCode");
		
		LocalInvLineItemTemplateHome invLineItemTemplateHome = null;
		
		LocalInvLineItemTemplate invLineItemTemplate = null;
		
		// Initialize EJB Home
		
		try {
			
			invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			try {
				
				invLineItemTemplate = invLineItemTemplateHome.findByPrimaryKey(LIT_CODE);      
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			InvLineItemTemplateDetails litDetails = new InvLineItemTemplateDetails();
			
			litDetails.setLitName(invLineItemTemplate.getLitName());
			litDetails.setLitDescription(invLineItemTemplate.getLitDescription());
			
			return litDetails;              
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteInvLitEntry(Integer LIT_CODE, Integer AD_CMPNY) 
	throws GlobalRecordAlreadyDeletedException,
	GlobalRecordAlreadyAssignedException {
		
		Debug.print("InvLineItemTemplateControllerBean deleteInvLitEntry");
		
		LocalInvLineItemTemplateHome invLineItemTemplateHome = null;
		
		// Initialize EJB Home
		
		try {
			
			invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);           
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalInvLineItemTemplate invLineItemTemplate = null;                
			
			try {
				
				invLineItemTemplate = invLineItemTemplateHome.findByPrimaryKey(LIT_CODE);
				
			} catch (FinderException ex) {
				
				throw new GlobalRecordAlreadyDeletedException();
				
			}
			
			if (!invLineItemTemplate.getArCustomers().isEmpty() ||
					!invLineItemTemplate.getApSuppliers().isEmpty()) {
				
				throw new GlobalRecordAlreadyAssignedException();
				
			}
			
			Collection invLineItems = invLineItemTemplate.getInvLineItems();
			
			Iterator i = invLineItems.iterator();
			
			while(i.hasNext()) {
				
				LocalInvLineItem invLineItem = (LocalInvLineItem)i.next();
				
				i.remove();
				
				invLineItem.remove();
				
			}
			
			invLineItemTemplate.remove();
			
		} catch (GlobalRecordAlreadyDeletedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (GlobalRecordAlreadyAssignedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLitAll(Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvLineItemTemplateControllerBean getInvLitAll");
		
		LocalInvLineItemTemplateHome invLineItemTemplateHome = null;
		
		ArrayList litList = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection invLineItemTemplates = invLineItemTemplateHome.findLitAll(AD_CMPNY);
			
			if (invLineItemTemplates.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			Iterator litIter = invLineItemTemplates.iterator();
			
			while (litIter.hasNext()) {
				
				LocalInvLineItemTemplate invLineItemTemplate = (LocalInvLineItemTemplate)litIter.next();
				
				InvLineItemTemplateDetails mdetails = new InvLineItemTemplateDetails();
				
				mdetails.setLitCode(invLineItemTemplate.getLitCode());        		
				mdetails.setLitName(invLineItemTemplate.getLitName());                
				mdetails.setLitDescription(invLineItemTemplate.getLitDescription());
				
				litList.add(mdetails);
				
			}              
			
			return litList;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getInvLiAllByLitCode(Integer LIT_CODE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{
		
		Debug.print("InvLineItemTemplateControllerBean getInvLiAllByLitCode");
		
		LocalInvLineItemTemplateHome invLineItemTemplateHome = null;
		
		LocalInvLineItemTemplate invLineItemTemplate = null;
		
		// Initialize EJB Home
		
		try {
			
			invLineItemTemplateHome = (LocalInvLineItemTemplateHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLineItemTemplateHome.JNDI_NAME, LocalInvLineItemTemplateHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			try {
				
				invLineItemTemplate = invLineItemTemplateHome.findByPrimaryKey(LIT_CODE);      
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			ArrayList list = new ArrayList();
			
			// get line items if any
			
			Collection invLineItems = invLineItemTemplate.getInvLineItems();
			
			if (!invLineItems.isEmpty()) {
				
				Iterator i = invLineItems.iterator();
				
				while (i.hasNext()) {
					
					LocalInvLineItem invLineItem = (LocalInvLineItem) i.next();
					
					InvModLineItemDetails liDetails = new InvModLineItemDetails();
					
					liDetails.setLiCode(invLineItem.getLiCode());
					liDetails.setLiLine(invLineItem.getLiLine());
					liDetails.setLiIiName(invLineItem.getInvItemLocation().getInvItem().getIiName());
					liDetails.setLiLocName(invLineItem.getInvItemLocation().getInvLocation().getLocName());
					liDetails.setLiUomName(invLineItem.getInvUnitOfMeasure().getUomName());
					liDetails.setLiIiDescription(invLineItem.getInvItemLocation().getInvItem().getIiDescription());
					
					list.add(liDetails);
					
				}
				
			}
			
			return list;              
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvLineItemTemplateControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {
		
		Debug.print("InvLineItemTemplateControllerBean getInvUomByIiName");
		
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemHome invItemHome = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		try {
			
			LocalInvItem invItem = null;
			LocalInvUnitOfMeasure invItemUnitOfMeasure = null;
			
			invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);        	
			invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();
			
			Collection invUnitOfMeasures = null;
			
			Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
			
			while (i.hasNext()) {
				
				LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
				InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
				
				details.setUomName(invUnitOfMeasure.getUomName());
				
				if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {
					
					details.setDefault(true);
					
				}
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfInvJournalLineNumber(Integer AD_CMPNY) {
		
		Debug.print("InvLineItemTemplateControllerBean getAdPrfInvJournalLineNumber");
		
		LocalAdPreferenceHome adPreferenceHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfInvInventoryLineNumber();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvLineItemTemplateControllerBean ejbCreate");
		
	}
}