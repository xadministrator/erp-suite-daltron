
/*
 * InvLocationEntryControllerBean.java
 *
 * Created on June 01, 2004, 6:16 PM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.InvLocFixedAssetException;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvLocationDetails;

/**
 * @ejb:bean name="InvLocationEntryControllerEJB"
 *           display-name="Used for entering locations"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvLocationEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvLocationEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvLocationEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
 */

public class InvLocationEntryControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvLocationTypeAll(Integer AD_CMPNY) {
		
		Debug.print("InvLocationEntryControllerBean getAdLvInvLocationTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV LOCATION TYPE", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvLocationDepartmentAll(Integer AD_CMPNY) {
		
		Debug.print("InvLocationEntryControllerBean getAdLvInvLocationTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV DEPARTMENT", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvLocationBranchAll(Integer AD_CMPNY) {
		
		Debug.print("InvLocationEntryControllerBean getAdLvInvLocationTypeAll");
		
		LocalAdBranchHome adBranchHome = null;              
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adbranchs = adBranchHome.findBrAll(AD_CMPNY);
			
			Iterator i = adbranchs.iterator();
			
			while (i.hasNext()) {
				
				LocalAdBranch adBranch = (LocalAdBranch)i.next();
				
				list.add(adBranch.getBrBranchCode());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void saveInvLocEntry(InvLocationDetails details, Integer AD_CMPNY)
	throws GlobalRecordAlreadyExistException,
	InvLocFixedAssetException{
		
		Debug.print("InvLocationEntryControllerBean saveInvLocEntry");
		
		LocalInvLocationHome invLocationHome = null;
		LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalAdBranchHome adBranchHome = null;
		
		LocalAdBranchItemLocation adBranchItemLocation = null;
		LocalInvLocation invLocation = null;
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}                
		
		try {
			
			// validate if customer already exists
			
			try {
				
				invLocation = invLocationHome.findByLocName(details.getLocName(), AD_CMPNY);
				
				if(details.getLocCode() == null || details.getLocCode() != null && 
						!invLocation.getLocCode().equals(details.getLocCode())) {          
					
					throw new GlobalRecordAlreadyExistException();
					
				}   
				
			} catch (GlobalRecordAlreadyExistException ex) {
				
				throw ex;
				
			} catch (FinderException ex) {
				
			}
			
			// create new customer
			
			if (details.getLocCode() == null) { 

				System.out.println("details.getLocLvType(): " + details.getLocLvType());
				System.out.println("getLocBranch(): " + details.getLocBranch());
				System.out.println("getLocDepartment(): " + details.getLocDepartment());
				if(details.getLocLvType().trim().equalsIgnoreCase("FIXED ASSETS") && (details.getLocDepartment().trim()=="" 
					&& details.getLocBranch().trim()=="")){
					throw new InvLocFixedAssetException();
				}
				
				invLocation = invLocationHome.create(details.getLocName(), details.getLocDescription(), 
						details.getLocLvType(), details.getLocAddress(), details.getLocContactPerson(), 
						details.getLocContactNumber(), details.getLocEmailAddress(),details.getLocBranch(),
						details.getLocDepartment(), details.getLocPosition(), details.getLocDateHired(),
						details.getLocEmploymentStatus(), AD_CMPNY);
				
			} else {
				System.out.println("details.getLocLvType(): " + details.getLocLvType());
				System.out.println("getLocBranch(): " + details.getLocBranch());
				System.out.println("getLocDepartment(): " + details.getLocDepartment());
				// update item 
				if(details.getLocLvType().trim().equalsIgnoreCase("FIXED ASSETS") && (details.getLocDepartment().trim()=="" 
					&& details.getLocBranch().trim()=="")){
					throw new InvLocFixedAssetException();
				}
				invLocation = invLocationHome.findByPrimaryKey(details.getLocCode());
				
				// update bill of materials
                Collection invBillOfMaterials = invBillOfMaterialHome.findByBomLocName(invLocation.getLocName(), AD_CMPNY);
                Iterator bomIter = invBillOfMaterials.iterator();
                
                while (bomIter.hasNext()) {
                	
                	LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) bomIter.next();
                	invBillOfMaterial.setBomLocName(details.getLocName());
                	
                }
				
				invLocation.setLocName(details.getLocName());
				invLocation.setLocDescription(details.getLocDescription());
				invLocation.setLocLvType(details.getLocLvType());
				invLocation.setLocAddress(details.getLocAddress());
				invLocation.setLocContactPerson(details.getLocContactPerson());
				invLocation.setLocContactNumber(details.getLocContactNumber());
				invLocation.setLocEmailAddress(details.getLocEmailAddress());
				invLocation.setLocDepartment(details.getLocDepartment());
				invLocation.setLocBranch(details.getLocBranch());
				invLocation.setLocPosition(details.getLocPosition());
				invLocation.setLocDateHired(details.getLocDateHired());
				invLocation.setLocEmploymentStatus(details.getLocEmploymentStatus());
				
				
				//>>>>------->
				try{
					LocalInvItemLocation invItemLocation = null;
					
					Collection invItemLocations = invItemLocationHome.findByLocName(details.getLocName(), AD_CMPNY);
					
					Iterator i = invItemLocations.iterator();
					
					while (i.hasNext()){
						
						invItemLocation = (LocalInvItemLocation)i.next();
						
						Collection adBranchItemLocations = adBranchItemLocationHome.findByInvIlAll(invItemLocation.getIlCode(), AD_CMPNY);
						
						Iterator iterBil = adBranchItemLocations.iterator();
						
						while (iterBil.hasNext()){
							
							adBranchItemLocation = (LocalAdBranchItemLocation)iterBil.next();
							
							if (adBranchItemLocation.getBilLocationDownloadStatus()=='N'){
								adBranchItemLocation.setBilLocationDownloadStatus('N');	
							}else if (adBranchItemLocation.getBilLocationDownloadStatus()=='D'){
								adBranchItemLocation.setBilLocationDownloadStatus('X');
							}else if (adBranchItemLocation.getBilLocationDownloadStatus()=='U'){
								adBranchItemLocation.setBilLocationDownloadStatus('U');
							}else if (adBranchItemLocation.getBilLocationDownloadStatus()=='X'){
								adBranchItemLocation.setBilLocationDownloadStatus('X');
							}
							
						}
						
					}
				
				}catch (FinderException ex){
					
				}
				
			}
			
		} catch (GlobalRecordAlreadyExistException ex) {
			
			throw ex;	 		
			
		} catch (InvLocFixedAssetException ex) {
			
			throw ex;	 		
			
		}catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}   
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public InvLocationDetails getInvLocByLocCode(Integer LOC_CODE, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException  {
		
		Debug.print("InvLocationEntryControllerBean getInvLocByLocCode");
		
		LocalInvLocationHome invLocationHome = null;
		
		LocalInvLocation invLocation = null;
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			try {
				
				invLocation = invLocationHome.findByPrimaryKey(LOC_CODE);      
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			InvLocationDetails details = new InvLocationDetails();
			details.setLocCode(invLocation.getLocCode());
			details.setLocName(invLocation.getLocName());
			details.setLocDescription(invLocation.getLocDescription());
			details.setLocLvType(invLocation.getLocLvType());
			details.setLocAddress(invLocation.getLocAddress());
			details.setLocContactPerson(invLocation.getLocContactPerson());
			details.setLocContactNumber(invLocation.getLocContactNumber());
			details.setLocEmailAddress(invLocation.getLocEmailAddress());
			details.setLocDepartment(invLocation.getLocDepartment());
			details.setLocPosition(invLocation.getLocPosition());
			details.setLocDateHired(invLocation.getLocDateHired());
			details.setLocEmploymentStatus(invLocation.getLocEmploymentStatus());
			details.setLocBranch(invLocation.getLocBranch());
			return details;              
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}	 
	
	/**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteInvLocEntry(Integer LOC_CODE, Integer AD_CMPNY) 
    throws GlobalRecordAlreadyDeletedException,
    GlobalRecordAlreadyAssignedException {
        
        Debug.print("InvLocationEntryControllerBean deleteInvLocEntry");
        
        LocalInvLocationHome invLocationHome = null;
        LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
        
        // Initialize EJB Home
        
        try {
            
        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            LocalInvLocation invLocation = null;                
            
            try {
                
                invLocation = invLocationHome.findByPrimaryKey(LOC_CODE);
                
            } catch (FinderException ex) {
                
                throw new GlobalRecordAlreadyDeletedException();
                
            }
            
            // if already in used in buildorder stock & stock transfers
            if (!invLocation.getInvItemLocations().isEmpty() ||
            		!invLocation.getInvPhysicalInventories().isEmpty() ||
            		!invLocation.getInvBranchStockTransfers().isEmpty()) {
                
                throw new GlobalRecordAlreadyAssignedException();
                
            }
            
            // if already in used in assembly items
                
            Collection invBillOfMaterials = invBillOfMaterialHome.findByBomLocName(invLocation.getLocName(), AD_CMPNY);
            
            if(!invBillOfMaterials.isEmpty()) throw new GlobalRecordAlreadyAssignedException();
                
            // if item location is already assigned
            if (!invLocation.getInvItemLocations().isEmpty()) {
                
                Collection itemLocations = invLocation.getInvItemLocations();
                
                Iterator locIter = itemLocations.iterator();
                
                while(locIter.hasNext()) {
                    
                    LocalInvItemLocation itemLocation = (LocalInvItemLocation)locIter.next();
                    
                    if(!itemLocation.getApVoucherLineItems().isEmpty() || 
                            !itemLocation.getArInvoiceLineItems().isEmpty() ||
                            !itemLocation.getInvAdjustmentLines().isEmpty() || 
                            !itemLocation.getInvBuildOrderLines().isEmpty() ||
                            !itemLocation.getInvBuildUnbuildAssemblyLines().isEmpty() || 
                            !itemLocation.getInvCostings().isEmpty() ||
                            !itemLocation.getInvPhysicalInventoryLines().isEmpty() ||
							!itemLocation.getInvBranchStockTransferLines().isEmpty() || 
							!itemLocation.getInvStockIssuanceLines().isEmpty() ||
							!itemLocation.getApPurchaseOrderLines().isEmpty() || 
							!itemLocation.getApPurchaseRequisitionLines().isEmpty()||
                        	!itemLocation.getAdBranchItemLocations().isEmpty()||
                        	!itemLocation.getArSalesOrderLines().isEmpty()||
                        	!itemLocation.getInvLineItems().isEmpty()) {
                        
                        throw new GlobalRecordAlreadyAssignedException();
                        
                    }
                    
                    // branch item locations
                    Collection adBranchItemLocations = itemLocation.getAdBranchItemLocations();
                    
                    Iterator bilIter = adBranchItemLocations.iterator();
                    
                    while(bilIter.hasNext()) {
                        
                        LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)bilIter.next();
                        
                        // if already downloaded to pos
            	        if (adBranchItemLocation.getBilItemDownloadStatus()=='D' || adBranchItemLocation.getBilItemDownloadStatus()=='X' ||
            	                adBranchItemLocation.getBilItemLocationDownloadStatus()=='D' || adBranchItemLocation.getBilItemLocationDownloadStatus()=='X' ||
            	                adBranchItemLocation.getBilLocationDownloadStatus()=='D' || adBranchItemLocation.getBilLocationDownloadStatus()=='X'){
            	            
            	            throw new GlobalRecordAlreadyAssignedException();
            	            
            	        }
            	        	
                    }
                    
                    // remove item location
                    locIter.remove();
                    itemLocation.remove();
                    
                }
            }
            
            invLocation.remove();
            
        } catch (GlobalRecordAlreadyDeletedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalRecordAlreadyAssignedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvLocationEntryControllerBean ejbCreate");
		
	}
}