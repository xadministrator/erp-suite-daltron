
/*
 * InvLocationSyncControllerBean
 *
 * Created on February 2, 2006, 2:44 PM
 *
 * @author  Franco Antonio R. Roig
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="InvLocationSyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *           
 * @wsee:port-component name="InvLocationSync"
 *
 * @jboss:port-component uri="omega-ejb/InvLocationSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.InvLocationSyncWS"
 * 
*/

public class InvLocationSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
	
	/**
     * @ejb:interface-method
     **/
    public int getInvLocationAllNewLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("InvLocationSyncControllerBean getInvLocationAllNewLength");
    	
    	LocalInvLocationHome invLocationHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	    	
    	// Initialize EJB Home
        
        try {
            
        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection invLocations = invLocationHome.findLocByLocNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
        	        	
        	return invLocations.size();
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
   	
    }
        
    
	/**
     * @ejb:interface-method
     **/
    public int getInvLocationAllUpdatedLength(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	Debug.print("InvLocationSyncControllerBean getInvLocationAllUpdatedLength");
    	
    	LocalInvLocationHome invLocationHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection invLocations = invLocationHome.findLocByLocNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');
        	
        	return invLocations.size();
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
   	
    }
    
	
	/**
     * @ejb:interface-method
     **/
    public String[] getInvLocationAllNewAndUpdated(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	System.out.println("InvLocationSyncControllerBean getInvLocationAllNewAndUpdated");
    	
    	LocalInvLocationHome invLocationHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	 try {
            
    		invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
     			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
    	 	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
    	
        try {
        
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        	
        	Collection invLocations = invLocationHome.findLocByLocNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'N', 'N');
        	Collection invUpdatedLocations = invLocationHome.findLocByLocNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'U', 'U', 'X');
        	        	
        	String[] results = new String[invLocations.size() + invUpdatedLocations.size()];
        	
        	Iterator i = invLocations.iterator();
        	
        	int ctr = 0;
        	
        	while(i.hasNext()){
        		
        		LocalInvLocation  invLocation = (LocalInvLocation)i.next();
        		results[ctr] = locationRowEncode(invLocation);
        		ctr++;
        		
        	}
        	
        	i = invUpdatedLocations.iterator();
        	
        	while(i.hasNext()){
        		
        		LocalInvLocation  invLocation = (LocalInvLocation)i.next();
        		results[ctr] = locationRowEncode(invLocation);
        		ctr++;
        		
        	}

        	return results;
        	
        }catch (Exception ex) {

        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
    }
    
    
    /**
     * @ejb:interface-method
     **/
    public int setInvLocationAllNewAndUpdatedSuccessConfirmation(String BR_BRNCH_CODE, Integer AD_CMPNY) {    	
    
    	System.out.println("InvLocationSyncControllerBean setInvLocationAllNewAndUpdatedSuccessConfirmation");    	    
        
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	try {
            
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
    		adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
    	
        try {
        
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY); 
        	
        	Collection adBranchItemLocations = adBranchItemLocationHome.findLocByLocNewAndUpdated(adBranch.getBrCode(), AD_CMPNY, 'N', 'U', 'X');
        	
        	Iterator i = adBranchItemLocations.iterator();
        	
        	while(i.hasNext()){
        		
        		LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)i.next();
        		adBranchItemLocation.setBilLocationDownloadStatus('D');
        		
        	}
        
        }catch (Exception ex) {

        	ctx.setRollbackOnly();
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    	
        return 0;
    }        
    
    
    private String locationRowEncode(LocalInvLocation invLocation) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer encodedResult = new StringBuffer();

    	// Start separator
    	encodedResult.append(separator);
    	
    	// Primary Key / OPOS: LocCode
    	encodedResult.append(invLocation.getLocCode().toString());
    	encodedResult.append(separator);
    	
    	// Location Name / OPOS: LocName
    	encodedResult.append(invLocation.getLocName());
    	encodedResult.append(separator);

    	// Description / OPOS: LocDesc
    	encodedResult.append(invLocation.getLocDescription());
    	encodedResult.append(separator);
    	
    	// Type / OPOS: aka Grocery / Restaurant)
    	encodedResult.append(invLocation.getLocLvType());
    	encodedResult.append(separator);
    	
    	// End separator
    	//encodedResult.append(separator);
    	
    	return encodedResult.toString();
    	
    }
    
    public void ejbCreate() throws CreateException {

       System.out.println("InvLocationSyncControllerBean ejbCreate");
      
    }
    
    public void ejbRemove() {};

    public void ejbActivate() {}
    public void ejbPassivate() {}

    public void setSessionContext(SessionContext ctx) {}

}