package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchOverheadMemoLine;
import com.ejb.ad.LocalAdBranchOverheadMemoLineHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvOverhead;
import com.ejb.inv.LocalInvOverheadHome;
import com.ejb.inv.LocalInvOverheadLine;
import com.ejb.inv.LocalInvOverheadLineHome;
import com.ejb.inv.LocalInvOverheadMemoLine;
import com.ejb.inv.LocalInvOverheadMemoLineHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModOverheadDetails;
import com.util.InvModOverheadLineDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="InvOverheadEntryControllerEJB"
 *           display-name="used for entering overhead transaction"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvOverheadEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvOverheadEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvOverheadEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvOverheadEntryControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvOmlAll(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("InvOverheadEntryControllerBean getInvOmlAll");
        
        LocalInvOverheadMemoLineHome invOverheadMemoLineHome = null;
        Collection invOverheadMemoLines = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invOverheadMemoLineHome = (LocalInvOverheadMemoLineHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvOverheadMemoLineHome.JNDI_NAME, LocalInvOverheadMemoLineHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
	               
        	invOverheadMemoLines = invOverheadMemoLineHome.findOmlByBranch(AD_BRNCH, AD_CMPNY);            
        
	        if (invOverheadMemoLines.isEmpty()) {
	        	
	        	return null;
	        	
	        }
	        
	        Iterator i = invOverheadMemoLines.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalInvOverheadMemoLine invOverheadMemoLine = (LocalInvOverheadMemoLine)i.next();	
	        	String details = invOverheadMemoLine.getOmlName();
	        	
	        	list.add(details);
	        	
	        }
	        
	        return list;
        
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableInvShift(Integer AD_CMPNY) {

        Debug.print("InvOverheadEntryControllerBean getAdPrfEnableInvShift");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfInvEnableShift();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
    
    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvShiftAll(Integer AD_CMPNY) {
		
		Debug.print("InvItemEntryControllerBean getAdLvInvShiftAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV SHIFT", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModOverheadDetails getInvOhByOhCode(Integer OH_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException {
                    
        Debug.print("InvOverheadEntryControllerBean getInvOhByOhCode");
        
        LocalInvOverheadHome invOverheadHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        LocalGenSegmentHome genSegmentHome = null;
                
        // Initialize EJB Home
        
        try {
            
        	invOverheadHome = (LocalInvOverheadHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvOverheadHome.JNDI_NAME, LocalInvOverheadHome.class);
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
	            lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);
	        genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
	            lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalInvOverhead invOverhead = null;
        
	        try {
		               
	        	invOverhead = invOverheadHome.findByPrimaryKey(OH_CODE);
	            
	        } catch (FinderException ex) {
	            
	            throw new GlobalNoRecordFoundException();
	            
	        }
	        
	        ArrayList ohlList = new ArrayList();
	        
	        Collection invOverheadLines = invOverhead.getInvOverheadLines();
	        
	        Iterator i = invOverheadLines.iterator();
	        
	        while (i.hasNext()) {
	        	
	        	LocalInvOverheadLine invOverheadLine = (LocalInvOverheadLine)i.next();
	        	
	        	InvModOverheadLineDetails mOhldetails = new InvModOverheadLineDetails();
	        	
	        	mOhldetails.setOhlCode(invOverheadLine.getOhlCode());
	        	mOhldetails.setOhlUnitCost(invOverheadLine.getOhlUnitCost());
	        	mOhldetails.setOhlQuantity(invOverheadLine.getOhlQuantity());	        		        	
	        	mOhldetails.setOhlAmount(EJBCommon.roundIt(invOverheadLine.getOhlQuantity() * invOverheadLine.getOhlUnitCost(), this.getGlFcPrecisionUnit(AD_CMPNY)));
	        	mOhldetails.setOhlOmlName(invOverheadLine.getInvOverheadMemoLine().getOmlName());
	        	mOhldetails.setOhlUomName(invOverheadLine.getInvUnitOfMeasure().getUomName());
	        	
	        	ohlList.add(mOhldetails);
	        	
	        }
	        
	        InvModOverheadDetails mdetails = new InvModOverheadDetails();
	        
	        mdetails.setOhCode(invOverhead.getOhCode());
	        mdetails.setOhDate(invOverhead.getOhDate());
	        mdetails.setOhDocumentNumber(invOverhead.getOhDocumentNumber());
	        mdetails.setOhReferenceNumber(invOverhead.getOhReferenceNumber());
	        mdetails.setOhDescription(invOverhead.getOhDescription());	        
	        mdetails.setOhLvShift(invOverhead.getOhLvShift());
	        mdetails.setOhPosted(invOverhead.getOhPosted());
	        mdetails.setOhCreatedBy(invOverhead.getOhCreatedBy());
	        mdetails.setOhDateCreated(invOverhead.getOhDateCreated());
	        mdetails.setOhLastModifiedBy(invOverhead.getOhLastModifiedBy());
	        mdetails.setOhDateLastModified(invOverhead.getOhDateLastModified());
	        mdetails.setOhPostedBy(invOverhead.getOhPostedBy());
	        mdetails.setOhDatePosted(invOverhead.getOhDatePosted());
	        mdetails.setOhOhlList(ohlList);
	        
	        return mdetails;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	Debug.printStackTrace(ex);
        	throw ex;
        	
    	} catch (Exception ex) {
        	
    		Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomByOmlName(String OML_NM, Integer AD_CMPNY)
    throws GlobalNoRecordFoundException {
    	
    	Debug.print("InvOverheadEntryControllerBean getInvUomByOmlName");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvOverheadMemoLineHome invOverheadMemoLineHome = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
        	invOverheadMemoLineHome = (LocalInvOverheadMemoLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvOverheadMemoLineHome.JNDI_NAME, LocalInvOverheadMemoLineHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
        	
        	LocalInvOverheadMemoLine invOverheadMemoLine = null;
        	
        	try {
                
        		invOverheadMemoLine = invOverheadMemoLineHome.findByOmlName(OML_NM, AD_CMPNY);
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
                
            }
                    	
            LocalInvUnitOfMeasure invOmlUnitOfMeasure = invOverheadMemoLine.getInvUnitOfMeasure();
        	
        	Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(invOmlUnitOfMeasure.
        					getUomAdLvClass(), AD_CMPNY).iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
        		
        		InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
        		
        		details.setUomName(invUnitOfMeasure.getUomName());
        		
        		if (invUnitOfMeasure.getUomName().equals(invOmlUnitOfMeasure.getUomName())) {
        			
        			details.setDefault(true);
        			
        		}
        		
        		list.add(details);
        		
        	}
        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
            
            throw ex;
            
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvOhEntry(com.util.InvOverheadDetails details,ArrayList ohlList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) 
    	throws GlobalRecordAlreadyDeletedException,
		       GlobalTransactionAlreadyPostedException,
			   GlJREffectiveDateNoPeriodExistException,
			   GlJREffectiveDatePeriodClosedException,
			   GlobalJournalNotBalanceException,
			   GlobalDocumentNumberNotUniqueException,
			   GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvOverheadEntryControllerBean saveInvOhEntry");
    	
    	LocalInvOverheadHome invOverheadHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;    	
    	LocalAdBranchOverheadMemoLineHome adBranchOverheadMemoLineHome = null;
    	
        LocalInvOverhead invOverhead = null;
        
        // Initialize EJB Home
        
        try {
            
        	invOverheadHome = (LocalInvOverheadHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvOverheadHome.JNDI_NAME, LocalInvOverheadHome.class);
        	adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
        		lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 
			adBranchOverheadMemoLineHome = (LocalAdBranchOverheadMemoLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchOverheadMemoLineHome.JNDI_NAME, LocalAdBranchOverheadMemoLineHome.class); 

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
        	
        	// validate if overhead is already deleted
        	
        	try {
        		
        		if (details.getOhCode() != null) {
        			
        			invOverhead = invOverheadHome.findByPrimaryKey(details.getOhCode());
        			
        		}
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if overhead is already posted, void, approved or pending
        	
        	if (details.getOhCode() != null) {
	        		                			
        		if (invOverhead.getOhPosted() == EJBCommon.TRUE) {
        			
        			throw new GlobalTransactionAlreadyPostedException();
        			
        		}
        		
        	}
        	
    		LocalInvOverhead invExistingOverhead = null;
    		
    		try {
    		
    			invExistingOverhead = invOverheadHome.findByOhDocumentNumberAndBrCode(details.getOhDocumentNumber(), AD_BRNCH, AD_CMPNY);
        		    
        	} catch (FinderException ex) {
        		
        	}

        	// validate if document number is unique document number is automatic then set next sequence
        	
        	if (details.getOhCode() == null) {
	    		

	        	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		        LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
	            if (invExistingOverhead != null) {
	            	
	            	throw new GlobalDocumentNumberNotUniqueException();
	            	
	            }
	    	
	        	try {
	        		
	        		adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV OVERHEAD", AD_CMPNY);
	        		
	        	} catch (FinderException ex) {
	        		
	        	}
	        	
	        	try {
	        		
	        		adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
	        		
	        	} catch (FinderException ex) {
	        		
	        	}
		        	    
		        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
		            (details.getOhDocumentNumber() == null || details.getOhDocumentNumber().trim().length() == 0)) {
		            	
		        	while (true) {
		        		
		        		if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
		        			
		        			try {
		        				
		        				invOverheadHome.findByOhDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);		            		
		        				adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
		        				
		        			} catch (FinderException ex) {
		        				
		        				details.setOhDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
		        				adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
		        				break;
		        				
		        			}
		        			
		        		} else {
		        			
		        			try {
		        				
		        				invOverheadHome.findByOhDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
		        				adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
		        				
		        			} catch (FinderException ex) {
		        				
		        				details.setOhDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
		        				adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
		        				break;
		        				
		        			}
		        			
		        		}
		        		
		        	}		            
		            		            	
		        }
		        
		    } else {

		    	if (invExistingOverhead != null && 
	                !invExistingOverhead.getOhCode().equals(details.getOhCode())) {
	            	
	            	throw new GlobalDocumentNumberNotUniqueException();
	            	
	            }
	            
	            if (invOverhead.getOhDocumentNumber() != details.getOhDocumentNumber() &&
	                (details.getOhDocumentNumber() == null || details.getOhDocumentNumber().trim().length() == 0)) {
	                	
	                details.setOhDocumentNumber(invOverhead.getOhDocumentNumber());
	                	
	         	}
		    	
		    }
        	
        	// used in checking if invoice should re-generate distribution records and re-calculate taxes	        	                	
	        boolean isRecalculate = true;
        	
        	// create overhead

        	if (details.getOhCode() == null) {
        		
        		invOverhead = invOverheadHome.create(details.getOhDate(), details.getOhDocumentNumber(), details.getOhReferenceNumber(), 
        				details.getOhDescription(), details.getOhLvShift(), EJBCommon.FALSE, 
						details.getOhCreatedBy(), details.getOhDateCreated(), details.getOhLastModifiedBy(), 
						details.getOhDateLastModified(), null, null, AD_BRNCH, AD_CMPNY);
        		
        	} else {
        		
                // check if critical fields are changed
        		
        		if (ohlList.size() != invOverhead.getInvOverheadLines().size()) {
        			
        			isRecalculate = true;
        			
        		} else if (ohlList.size() == invOverhead.getInvOverheadLines().size()) {
        			
        			Iterator ohlIter = invOverhead.getInvOverheadLines().iterator();
        			Iterator ohlListIter = ohlList.iterator();
        			
        			while (ohlIter.hasNext()) {
        				
        				LocalInvOverheadLine invOverheadLine = (LocalInvOverheadLine)ohlIter.next();
        				
        				InvModOverheadLineDetails mdetails = (InvModOverheadLineDetails)ohlListIter.next();
        				        				
        				if (!invOverheadLine.getInvOverheadMemoLine().getOmlName().equals(mdetails.getOhlOmlName()) ||
	        				!invOverheadLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getOhlUomName()) ||
	        				invOverheadLine.getOhlQuantity() != mdetails.getOhlQuantity() ||
	        				invOverheadLine.getOhlUnitCost() != mdetails.getOhlUnitCost()) {
        					
        					isRecalculate = true;
        					break;
        					
        				}
        				
        				isRecalculate = false;
        				
        			}
        			
        		} else {
        			
        			isRecalculate = false;
        			
        		}
        		
        		invOverhead.setOhDate(details.getOhDate());
        		invOverhead.setOhDocumentNumber(details.getOhDocumentNumber());
        		invOverhead.setOhReferenceNumber(details.getOhReferenceNumber());
        		invOverhead.setOhDescription(details.getOhDescription());        		
        		invOverhead.setOhLastModifiedBy(details.getOhLastModifiedBy());
        		invOverhead.setOhDateLastModified(details.getOhDateLastModified());
        		
        	}
        	        
        	if (isRecalculate) {
        	        	
	        	// remove all overhead lines
	        	
	        	Collection invOverheadLines = invOverhead.getInvOverheadLines();
	        	
	        	Iterator i = invOverheadLines.iterator();
	        	
	        	while (i.hasNext()) {
	        		
	        		LocalInvOverheadLine invOverheadLine = (LocalInvOverheadLine)i.next();
	        			        		
	        		i.remove();
	        		
	        		invOverheadLine.remove();
	        		
	        	}
	        	
	        	// remove all distribution records
	       	   
	 	  	    Collection invDistributionRecords = invOverhead.getInvDistributionRecords();
	 	  	  
	 	  	    i = invDistributionRecords.iterator();     	  
	 	  	  
	 	  	    while (i.hasNext()) {
	 	  	  	
	 	  	   	    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
	 	  	  	   
	 	  	  	    i.remove();
	 	  	  	    
	 	  	  	    invDistributionRecord.remove();
	 	  	  	
	 	  	    }	  
	 	  	    
	 	  	    // add new overhead lines and distribution record
	        	
	 	  	    i = ohlList.iterator();
	 	  	    
	 	  	    while (i.hasNext()) {
	 	  	    	
	 	  	    	InvModOverheadLineDetails  mdetails = (InvModOverheadLineDetails) i.next();
	 	  	    	
	 	  	    	LocalInvOverheadLine invOverheadLine = this.addInvOhlEntry(mdetails, invOverhead, AD_CMPNY);
	 	  	    	 	  	    	 	  	    		 	  	    
	 	  	    	double AMOUNT = 0d;
	 	  	    	
	 	  	    	LocalAdBranchOverheadMemoLine adBranchOverheadMemoLine =
	 	  	    		adBranchOverheadMemoLineHome.findBomlByOmlCodeAndBrCode(invOverheadLine.getInvOverheadMemoLine().getOmlCode(), AD_BRNCH, AD_CMPNY);

	 	  	    	if (invOverheadLine.getOhlQuantity() > 0) {
	 	  	    		  	    	 	  	    	
		 	  	    	AMOUNT = EJBCommon.roundIt(
		 	  	    			invOverheadLine.getOhlQuantity() * invOverheadLine.getOhlUnitCost(),
								this.getGlFcPrecisionUnit(AD_CMPNY));

		 	  	    	// check for branch mapping
		 	  	    	
		 	  	    	if (adBranchOverheadMemoLine == null) {
		 	  	    	
		 	  	    		this.addInvDrEntry(invOverhead.getInvDrNextLine(), "OVERHEAD",  EJBCommon.TRUE,Math.abs(AMOUNT),
		 	  	    			invOverheadLine.getInvOverheadMemoLine().getOmlGlCoaOverheadAccount(), invOverhead, AD_BRNCH, AD_CMPNY);
		 	  	    	
		 	  	    		this.addInvDrEntry(invOverhead.getInvDrNextLine(), "LIABILITY", EJBCommon.FALSE, Math.abs(AMOUNT),
		 	  	    			invOverheadLine.getInvOverheadMemoLine().getOmlGlCoaLiabilityAccount(), invOverhead, AD_BRNCH, AD_CMPNY);
		 	  	    	
		 	  	    	} else {
		 	  	    		
		 	  	    		this.addInvDrEntry(invOverhead.getInvDrNextLine(), "OVERHEAD",  EJBCommon.TRUE,
		 	  	    			Math.abs(AMOUNT), adBranchOverheadMemoLine.getBomlGlCoaOverheadAccount(), invOverhead, AD_BRNCH, AD_CMPNY);
			 	  	    	
		 	  	    		this.addInvDrEntry(invOverhead.getInvDrNextLine(), "LIABILITY", EJBCommon.FALSE,
		 	  	    			Math.abs(AMOUNT), adBranchOverheadMemoLine.getBomlGlCoaLiabilityAccount(), invOverhead, AD_BRNCH, AD_CMPNY);
		 	  	    		
		 	  	    	}
		 	  	    	
	 	  	    	} else {
	 	  	    			    
	 	  	    		AMOUNT = EJBCommon.roundIt(
		 	  	    			invOverheadLine.getOhlQuantity() * invOverheadLine.getOhlUnitCost(),
								this.getGlFcPrecisionUnit(AD_CMPNY));

		 	  	    	// check for branch mapping	 	  	    		
	 	  	    		
	 	  	    		if (adBranchOverheadMemoLine == null) {
	 	  	    			
		 	  	    		this.addInvDrEntry(invOverhead.getInvDrNextLine(), "OVERHEAD",  EJBCommon.FALSE, Math.abs(AMOUNT),
		 	  	    				invOverheadLine.getInvOverheadMemoLine().getOmlGlCoaOverheadAccount(), invOverhead, AD_BRNCH, AD_CMPNY);
			 	  	    	
			 	  	    	this.addInvDrEntry(invOverhead.getInvDrNextLine(), "LIABILITY", EJBCommon.TRUE, Math.abs(AMOUNT),
			 	  	    			invOverheadLine.getInvOverheadMemoLine().getOmlGlCoaLiabilityAccount(), invOverhead, AD_BRNCH, AD_CMPNY);
	 	  	    			
	 	  	    		} else {
	 	  	    			
	 	  	    			this.addInvDrEntry(invOverhead.getInvDrNextLine(), "OVERHEAD",  EJBCommon.TRUE,
	 	  	    					Math.abs(AMOUNT), adBranchOverheadMemoLine.getBomlGlCoaOverheadAccount(), invOverhead, AD_BRNCH, AD_CMPNY);
	 	  	    			
	 	  	    			this.addInvDrEntry(invOverhead.getInvDrNextLine(), "LIABILITY", EJBCommon.FALSE,
	 	  	    					Math.abs(AMOUNT), adBranchOverheadMemoLine.getBomlGlCoaLiabilityAccount(), invOverhead, AD_BRNCH, AD_CMPNY);
	 	  	    			
	 	  	    		}

	 	  	    	}
	 	  	    	
	 	  	    }
	 	  	    	 	  	    
        	}
        	        	 	  	    
 	  	    // post to gl
        	
        	if (!isDraft) {
 	  	            		
        		this.executeInvOhPost(invOverhead.getOhCode(), invOverhead.getOhLastModifiedBy(), AD_BRNCH, AD_CMPNY);
        		
        	}
        		         			 			 	
		 	return invOverhead.getOhCode();
		 	
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex; 
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex; 
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalDocumentNumberNotUniqueException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

		} catch(GlobalBranchAccountNumberInvalidException ex) {
			
			throw new GlobalBranchAccountNumberInvalidException ();

        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
    	
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteInvOhEntry(Integer OH_CODE, String AD_USR, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException {
                    
        Debug.print("InvOverheadEntryControllerBean deleteInvOhEntry");
        
        LocalInvOverheadHome invOverheadHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home
        
        try {
            
            invOverheadHome = (LocalInvOverheadHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvOverheadHome.JNDI_NAME, LocalInvOverheadHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);  

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	LocalInvOverhead invOverhead = invOverheadHome.findByPrimaryKey(OH_CODE);
        	
        	adDeleteAuditTrailHome.create("INV OVERHEAD", invOverhead.getOhDate(), invOverhead.getOhDocumentNumber(), invOverhead.getOhReferenceNumber(),
 					0d, AD_USR, new Date(), AD_CMPNY);
        	
        	invOverhead.remove();
        	
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("InvOverheadEntryControllerBean getGlFcPrecisionUnit");

       
        LocalAdCompanyHome adCompanyHome = null;
             
        // Initialize EJB Home
         
        try {
             
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
             
        } catch (NamingException ex) {
             
            throw new EJBException(ex.getMessage());
             
        }

        try {
        	
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             
          return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                      
        } catch (Exception ex) {
        	 
        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
          
        }

     }
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

         Debug.print("InvOverheadEntryControllerBean getInvGpQuantityPrecisionUnit");
        
         LocalAdPreferenceHome adPreferenceHome = null;         
         
          // Initialize EJB Home
           
          try {
               
          	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
               
          } catch (NamingException ex) {
               
             throw new EJBException(ex.getMessage());
               
          }
         

          try {
         	
             LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
             return adPreference.getPrfInvQuantityPrecisionUnit();
            
          } catch (Exception ex) {
           	 
             Debug.printStackTrace(ex);
             throw new EJBException(ex.getMessage());
            
          }

      }
    
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getInvGpInventoryLineNumber(Integer AD_CMPNY) {

         Debug.print("InvOverheadEntryControllerBean getInvGpInventoryLineNumber");
                    
         LocalAdPreferenceHome adPreferenceHome = null;
        
        
         // Initialize EJB Home
          
         try {
              
         	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
        	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           
            return adPreference.getPrfInvInventoryLineNumber();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }
        
      }
             
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/ 
     public double getInvOmlUnitCostByOmlNameAndUomName(String OML_NM, String UOM_NM, Integer AD_CMPNY) {

        Debug.print("InvOverheadEntryControllerBean getInvOmlUnitCostByOmlNameAndUomName");
                   
        LocalInvOverheadMemoLineHome invOverheadMemoLineHome = null;
       
        // Initialize EJB Home
         
        try {
             
        	invOverheadMemoLineHome = (LocalInvOverheadMemoLineHome)EJBHomeFactory.
               lookUpLocalHome(LocalInvOverheadMemoLineHome.JNDI_NAME, LocalInvOverheadMemoLineHome.class);
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }

        try {
       	
        	LocalInvOverheadMemoLine invOverheadMemoLine = invOverheadMemoLineHome.findByOmlName(OML_NM, AD_CMPNY);        	
        	return invOverheadMemoLine.getOmlUnitCost();
        	
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
     
     // private methods
     
     private LocalInvOverheadLine addInvOhlEntry(InvModOverheadLineDetails mdetails, 
    	LocalInvOverhead invOverhead, Integer AD_CMPNY) {
		
		Debug.print("InvOverheadEntryControllerBean addInvOhlEntry");
		
		LocalInvOverheadLineHome invOverheadLineHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvOverheadMemoLineHome invOverheadMemoLineHome = null;
	       	          
	    // Initialize EJB Home
	    
	    try {
	        
	    	invOverheadLineHome = (LocalInvOverheadLineHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvOverheadLineHome.JNDI_NAME, LocalInvOverheadLineHome.class); 
	    	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class); 
	    	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
	    	invOverheadMemoLineHome = (LocalInvOverheadMemoLineHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvOverheadMemoLineHome.JNDI_NAME, LocalInvOverheadMemoLineHome.class);
	                    
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }            
	            
	    try {
	    	
	    	LocalInvOverheadLine invOverheadLine = invOverheadLineHome.create(
	    		mdetails.getOhlUnitCost(), mdetails.getOhlQuantity(), AD_CMPNY);
	    	
	    	invOverhead.addInvOverheadLine(invOverheadLine);	    		      
	        
	    	LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getOhlUomName(), AD_CMPNY);
	    	invUnitOfMeasure.addInvOverheadLine(invOverheadLine);
	    	
	    	LocalInvOverheadMemoLine invOverheadMemoLine = invOverheadMemoLineHome.findByOmlName(mdetails.getOhlOmlName(), AD_CMPNY);
	    	invOverheadMemoLine.addInvOverheadLine(invOverheadLine);
	    	
	     	return invOverheadLine;
	    							    		        		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
		
	}	
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS,
	    byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalInvOverhead invOverhead, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
			
		Debug.print("InvOverheadEntryControllerBean addInvDrEntry");
		
		LocalAdCompanyHome adCompanyHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;        
		LocalGlChartOfAccountHome glChartOfAccountHome = null;           
                
        // Initialize EJB Home
        
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
        	invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
             
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
                
        try {        
        	    
        	// get company
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    		
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    		
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
        	        	        	
        	// create distribution record        
		    
		    LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
			    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
			    
		    invOverhead.addInvDistributionRecord(invDistributionRecord);
			glChartOfAccount.addInvDistributionRecord(invDistributionRecord);		   

		} catch(GlobalBranchAccountNumberInvalidException ex) {
			
			throw new GlobalBranchAccountNumberInvalidException ();

        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}		
    
    private void executeInvOhPost(Integer OH_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
        GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException {
                    
        Debug.print("InvOverheadEntryControllerBean executeInvOhPost");
        
        LocalInvOverheadHome invOverheadHome = null;        
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
                        
                
        // Initialize EJB Home
        
        try {
            
            invOverheadHome = (LocalInvOverheadHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvOverheadHome.JNDI_NAME, LocalInvOverheadHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
                                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }       
        
        try {
        	        	
        	// validate if overhead is already deleted
        	
        	LocalInvOverhead invOverhead = null;
        	        	
        	try {
        		        			
        		invOverhead = invOverheadHome.findByPrimaryKey(OH_CODE);
        			        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if overhead is already posted or void
        	
        	if (invOverhead.getOhPosted() == EJBCommon.TRUE) {
        		
        		throw new GlobalTransactionAlreadyPostedException();
        		
            }
                    				                	
        	// set invoice post status
           
           invOverhead.setOhPosted(EJBCommon.TRUE);
           invOverhead.setOhPostedBy(USR_NM);
           invOverhead.setOhDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
                      
           // post to gl if necessary
           
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
                      	
       	   // validate if date has no period and period is closed
       	   
       	   LocalGlSetOfBook glJournalSetOfBook = null;
       	  
       	   try {
       	    
       	   	   glJournalSetOfBook = glSetOfBookHome.findByDate(invOverhead.getOhDate(), AD_CMPNY);
       	   	   
       	   } catch (FinderException ex) {
       	   
       	       throw new GlJREffectiveDateNoPeriodExistException();
       	   
       	   }
		
		   LocalGlAccountingCalendarValue glAccountingCalendarValue = 
		        glAccountingCalendarValueHome.findByAcCodeAndDate(
		        	glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invOverhead.getOhDate(), AD_CMPNY);
		        	
		        	
		   if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
		        glAccountingCalendarValue.getAcvStatus() == 'C' ||
		        glAccountingCalendarValue.getAcvStatus() == 'P') {
		        	
		        throw new GlJREffectiveDatePeriodClosedException();
		        
		   }
		   
		   // check if invoice is balance if not check suspense posting
	            	
           LocalGlJournalLine glOffsetJournalLine = null;
        	
           Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByOhCode(invOverhead.getOhCode(), AD_CMPNY);
            
           Iterator j = invDistributionRecords.iterator();
        	
           double TOTAL_DEBIT = 0d;
           double TOTAL_CREDIT = 0d;
        	
           while (j.hasNext()) {
        		
        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
        		
        		double DR_AMNT = 0d;
        		
        		DR_AMNT = invDistributionRecord.getDrAmount();
        		
        		if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
        			
        			TOTAL_DEBIT += DR_AMNT;
        			            			
        		} else {
        			
        			TOTAL_CREDIT += DR_AMNT;
        			
        		}
        		
        	}
        	
        	TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        	TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
        	            	            	            	
        	if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        	    TOTAL_DEBIT != TOTAL_CREDIT) {
        	    	
        	    LocalGlSuspenseAccount glSuspenseAccount = null;
        	    	
        	    try { 	
        	    	
        	        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY OVERHEAD", AD_CMPNY);
        	        
        	    } catch (FinderException ex) {
        	    	
        	    	throw new GlobalJournalNotBalanceException();
        	    	
        	    }
        	               	    
        	    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
        	    	
        	    	glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE,
        	    	    TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
        	    	              	        
        	    } else {
        	    	
        	    	glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
        	    	    TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
        	    	
        	    }
        	    
        	    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        	    glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        	    
        	    	
			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
			    TOTAL_DEBIT != TOTAL_CREDIT) {
			    
				throw new GlobalJournalNotBalanceException();		    	
			    	
			}
		   
	       
	       // create journal batch if necessary
	       
	       LocalGlJournalBatch glJournalBatch = null;
	       java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
	       
	       try {
	           
   	   			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
	       	   	
	       } catch (FinderException ex) {
	       
	       }
			     	
	       if (glJournalBatch == null) {
	     		
     		    glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
	     		
	       }
	     				     		
		   // create journal entry			            	
	            		            	
    	   LocalGlJournal glJournal = glJournalHome.create(invOverhead.getOhReferenceNumber(),
    		    invOverhead.getOhDescription(), invOverhead.getOhDate(),
    		    0.0d, null, invOverhead.getOhDocumentNumber(), null, 1d, "N/A", null,
    		    'N', EJBCommon.TRUE, EJBCommon.FALSE,
    		    USR_NM, new Date(),
    		    USR_NM, new Date(),
    		    null, null,
    		    USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
    		    null, null, EJBCommon.FALSE, null,
    		    AD_BRNCH, AD_CMPNY);
    		    
    	   LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
           glJournal.setGlJournalSource(glJournalSource);
        	
           LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
           glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
        	
           LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
           glJournal.setGlJournalCategory(glJournalCategory);
    		
    	   if (glJournalBatch != null) {
    			
    		   glJournalBatch.addGlJournal(glJournal);
    			
    	   }           		    
        	       	            		            
           // create journal lines
        	          	
           j = invDistributionRecords.iterator();
        	
           while (j.hasNext()) {
        		
        		LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
        		
        		double DR_AMNT = 0d;
        		
        		DR_AMNT = invDistributionRecord.getDrAmount();
        		            		
        		LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
        			invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
        			
                invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
        	    
        	    glJournal.addGlJournalLine(glJournalLine);
        	    
        	    invDistributionRecord.setDrImported(EJBCommon.TRUE);
        	    
        		
           }
        	
           if (glOffsetJournalLine != null) {
        		
        	   glJournal.addGlJournalLine(glOffsetJournalLine);
        		
           }		
           
           // post journal to gl
           
           Collection glJournalLines = glJournal.getGlJournalLines();
       
	       Iterator i = glJournalLines.iterator();
	       
	       while (i.hasNext()) {
	       	
	           LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
	           		           
	           // post current to current acv
	             
	           this.postToGl(glAccountingCalendarValue,
	               glJournalLine.getGlChartOfAccount(),
	               true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
		 	 
	         
	           // post to subsequent acvs (propagate)
	         
	           Collection glSubsequentAccountingCalendarValues = 
	               glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
	                   glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
	                   glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
	                 
	           Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
	         
	           while (acvsIter.hasNext()) {
	         	
	         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
	         	       (LocalGlAccountingCalendarValue)acvsIter.next();
	         	       
	         	   this.postToGl(glSubsequentAccountingCalendarValue,
	         	       glJournalLine.getGlChartOfAccount(),
	         	       false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
	         		         	
	           }
	           
	           // post to subsequent years if necessary
        
	           Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
		  	  	
		  	  	if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
		  	  	
			  	  	adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
			  	  	LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY); 	  	
		  	  	
			  	  	Iterator sobIter = glSubsequentSetOfBooks.iterator();
			  	  	
			  	  	while (sobIter.hasNext()) {
			  	  		
			  	  		LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
			  	  		
			  	  		String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
			  	  		
			  	  		// post to subsequent acvs of subsequent set of book(propagate)
	         
			           Collection glAccountingCalendarValues = 
			               glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
			                 
			           Iterator acvIter = glAccountingCalendarValues.iterator();
			         
			           while (acvIter.hasNext()) {
			         	
			         	   LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
			         	       (LocalGlAccountingCalendarValue)acvIter.next();
			         	       
			         	    if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
				 				ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
			         	       
					         	this.postToGl(glSubsequentAccountingCalendarValue,
					         	     glJournalLine.getGlChartOfAccount(),
					         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
					         	     
					        } else { // revenue & expense
					        					             					             
					             this.postToGl(glSubsequentAccountingCalendarValue,
					         	     glRetainedEarningsAccount,
					         	     false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
					        
					        }
			         		         	
			           }
			  	  		
			  	  	   if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
			  	  		
			  	  	}
			  	  	
		  	  	}
	       	   	       	   	       	
        }
	           	       	              	           	           	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
   private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
      LocalGlChartOfAccount glChartOfAccount, 
      boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
      	
      Debug.print("InvOverheadEntryControllerBean postToGl");
      
      LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
      LocalAdCompanyHome adCompanyHome = null;
      
     
       // Initialize EJB Home
        
       try {
            
           glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
              lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);                   
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
              
            
       } catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
       }
       
       try {          
               
               LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
       	   
	       	   LocalGlChartOfAccountBalance glChartOfAccountBalance = 
		            glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
		               	  glAccountingCalendarValue.getAcvCode(),
		               	  glChartOfAccount.getCoaCode(), AD_CMPNY);
	               	  
	           String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
	           short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
	           
	           
	               	  
	           if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
				    isDebit == EJBCommon.TRUE) ||
				    (!ACCOUNT_TYPE.equals("ASSET") && !ACCOUNT_TYPE.equals("EXPENSE") &&
				    isDebit == EJBCommon.FALSE)) {				    
				    			
 			        glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
				    
		
			  } else {
	
				    glChartOfAccountBalance.setCoabEndingBalance(
				       EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
				       
				    if (!isCurrentAcv) {
				    	
				    	glChartOfAccountBalance.setCoabBeginningBalance(
				       		EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
				    	
					}
	
		 	  }
		 	  
		 	  if (isCurrentAcv) { 
		 	 
			 	 if (isDebit == EJBCommon.TRUE) {
			 	 	
		 			glChartOfAccountBalance.setCoabTotalDebit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 				
		 		 } else {
		 		 	
		 			glChartOfAccountBalance.setCoabTotalCredit(
		 				EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
		 		 }       	   
		 		 
		 	}
       	
       } catch (Exception ex) {
       	
       	   Debug.printStackTrace(ex);
       	   throw new EJBException(ex.getMessage());
       	
       }
      
      
    }
   
            
	// SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvOverheadEntryControllerBean ejbCreate");
      
    }

}