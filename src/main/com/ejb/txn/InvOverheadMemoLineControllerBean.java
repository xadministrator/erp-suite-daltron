package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchOverheadMemoLine;
import com.ejb.ad.LocalAdBranchOverheadMemoLineHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdResponsibility;
import com.ejb.ad.LocalAdResponsibilityHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.InvOMLCoaGlLiabilityAccountNotFoundException;
import com.ejb.exception.InvOMLCoaGlOverheadAccountNotFoundException;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.inv.LocalInvOverheadMemoLine;
import com.ejb.inv.LocalInvOverheadMemoLineHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModBranchOverheadMemoLineDetails;
import com.util.AdResponsibilityDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvModOverheadMemoLineDetails;

/**
 * @ejb:bean name="InvOverheadMemoLineControllerEJB"
 *           display-name="Used for entering overhead memo lines"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvOverheadMemoLineControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvOverheadMemoLineController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvOverheadMemoLineControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
 */

public class InvOverheadMemoLineControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomAll(Integer AD_CMPNY) {
		
		Debug.print("InvOverheadMemoLineControllerBean getInvUomAll");
		
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvUnitOfMeasure invUnitOfMeasure = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection invUnitOfMeasures = invUnitOfMeasureHome.findEnabledUomAll(AD_CMPNY);
			
			Iterator i = invUnitOfMeasures.iterator();
			
			while (i.hasNext()) {
				
				invUnitOfMeasure = (LocalInvUnitOfMeasure)i.next();
				
				list.add(invUnitOfMeasure.getUomName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvOmlAll(Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvOverheadMemoLineControllerBean getInvOmlAll");
		
		LocalInvOverheadMemoLineHome invOverheadMemoLineHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invOverheadMemoLineHome = (LocalInvOverheadMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvOverheadMemoLineHome.JNDI_NAME, LocalInvOverheadMemoLineHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection invOverheadMemoLines = invOverheadMemoLineHome.findOmlAll(AD_CMPNY);
			
			if (invOverheadMemoLines.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			Iterator i = invOverheadMemoLines.iterator();
			
			while (i.hasNext()) {
				
				LocalInvOverheadMemoLine invOverheadMemoLine = (LocalInvOverheadMemoLine)i.next();
				
				InvModOverheadMemoLineDetails mdetails = new InvModOverheadMemoLineDetails();
				
				mdetails.setOmlCode(invOverheadMemoLine.getOmlCode());
				mdetails.setOmlName(invOverheadMemoLine.getOmlName());
				mdetails.setOmlDescription(invOverheadMemoLine.getOmlDescription());	        		
				mdetails.setOmlUomName(invOverheadMemoLine.getInvUnitOfMeasure().getUomName());
				mdetails.setOmlUnitCost(invOverheadMemoLine.getOmlUnitCost());
				
				// get overhead account and account description
				
				LocalGlChartOfAccount glOverHeadAccount = glChartOfAccountHome.findByPrimaryKey(invOverheadMemoLine.getOmlGlCoaOverheadAccount());
				
				mdetails.setOmlCoaOverheadAccount(glOverHeadAccount.getCoaAccountNumber());        				    
				mdetails.setOmlCoaOverheadAccountDescription(glOverHeadAccount.getCoaAccountDescription());		
				
				// get liability account and account description
				
				LocalGlChartOfAccount glLiabilityAccount = glChartOfAccountHome.findByPrimaryKey(invOverheadMemoLine.getOmlGlCoaLiabilityAccount());
				
				mdetails.setOmlCoaLiabilityAccount(glLiabilityAccount.getCoaAccountNumber());
				mdetails.setOmlCoaLiabilityAccountDescription(glLiabilityAccount.getCoaAccountDescription());
				
				list.add(mdetails);
				
			}              
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/    
	public void addInvOmlEntry(com.util.InvModOverheadMemoLineDetails mdetails,
			String UOM_NM, ArrayList branchList, Integer AD_CMPNY) 
	throws GlobalRecordAlreadyExistException,
	InvOMLCoaGlOverheadAccountNotFoundException,
	InvOMLCoaGlLiabilityAccountNotFoundException {
		
		Debug.print("InvOverheadMemoLineControllerBean addInvOmlEntry");
		
		LocalInvOverheadMemoLineHome invOverheadMemoLineHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		
		LocalGlChartOfAccount glOverheadAccount = null;
		LocalGlChartOfAccount glLiabilityAccount = null;
		
    	LocalAdBranchOverheadMemoLineHome adBranchOverheadMemoLineHome = null;
    	LocalAdBranchOverheadMemoLine adBranchOverheadMemoLine = null;
    	
        LocalAdBranchHome adBranchHome = null;
    	LocalAdBranch adBranch = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invOverheadMemoLineHome = (LocalInvOverheadMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvOverheadMemoLineHome.JNDI_NAME, LocalInvOverheadMemoLineHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			adBranchOverheadMemoLineHome = (LocalAdBranchOverheadMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchOverheadMemoLineHome.JNDI_NAME, LocalAdBranchOverheadMemoLineHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}                
		
		try {
			
			LocalInvOverheadMemoLine invOverheadMemoLine = null;
			
			try { 
				
				invOverheadMemoLine = invOverheadMemoLineHome.findByOmlName(mdetails.getOmlName(), AD_CMPNY);
				
				throw new GlobalRecordAlreadyExistException();
				
			} catch (FinderException ex) {
				
			}
			
			// get overhead account and liability account to validate accounts
			
			try {
				
				glOverheadAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getOmlCoaOverheadAccount(), AD_CMPNY);
				
			} catch (FinderException ex) {
				
				throw new InvOMLCoaGlOverheadAccountNotFoundException();
				
			}
			
			try {
				
				glLiabilityAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getOmlCoaLiabilityAccount(), AD_CMPNY);
				
			} catch (FinderException ex) {
				
				throw new InvOMLCoaGlLiabilityAccountNotFoundException();
				
			}
			
			// create new overhead memo line
			
			invOverheadMemoLine = invOverheadMemoLineHome.create(mdetails.getOmlName(),
					mdetails.getOmlDescription(), mdetails.getOmlUnitCost(), glOverheadAccount.getCoaCode(),
					glLiabilityAccount.getCoaCode(), AD_CMPNY);
			
			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);
			invUnitOfMeasure.addInvOverheadMemoLine(invOverheadMemoLine);
			
			// create new Branch Overhead Memo Line
			LocalGlChartOfAccount glOverheadCoa = null;
			LocalGlChartOfAccount glLiabilityCoa = null;
			Iterator i = branchList.iterator();
			
			while(i.hasNext()) {
				
				AdModBranchOverheadMemoLineDetails adBrOmlDetails = (AdModBranchOverheadMemoLineDetails)i.next();
				
				try {
					glOverheadCoa = glChartOfAccountHome.findByCoaAccountNumber( adBrOmlDetails.getBOMLOverheadAccountNumber(), AD_CMPNY);
				} catch (FinderException ex) {
					throw new InvOMLCoaGlOverheadAccountNotFoundException();
				}

				try {
					glLiabilityCoa = glChartOfAccountHome.findByCoaAccountNumber( adBrOmlDetails.getBOMLLiabilityAccountNumber(), AD_CMPNY);				
				} catch (FinderException ex) {
					throw new InvOMLCoaGlLiabilityAccountNotFoundException();
				}
				adBranchOverheadMemoLine = adBranchOverheadMemoLineHome.create( glOverheadCoa.getCoaCode(), glLiabilityCoa.getCoaCode(), AD_CMPNY);
				
				invOverheadMemoLine.addAdBranchOverheadMemoLine(adBranchOverheadMemoLine);
				adBranch = adBranchHome.findByPrimaryKey(adBrOmlDetails.getBOMLBranchCode());
				adBranch.addAdBranchOverheadMemoLine(adBranchOverheadMemoLine);
				
			}
			
			
		} catch (GlobalRecordAlreadyExistException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (InvOMLCoaGlOverheadAccountNotFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (InvOMLCoaGlLiabilityAccountNotFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}    
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void updateInvOmlEntry(com.util.InvModOverheadMemoLineDetails mdetails, String UOM_NM,
			ArrayList branchList, String RS_NM, Integer AD_CMPNY) 
	throws GlobalRecordAlreadyExistException, 
	InvOMLCoaGlOverheadAccountNotFoundException,
	InvOMLCoaGlLiabilityAccountNotFoundException {
		
		Debug.print("InvOverheadMemoLineControllerBean updateInvOmlEntry");
		
		LocalInvOverheadMemoLineHome invOverheadMemoLineHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;        
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		
		LocalGlChartOfAccount glOverheadAccount = null;
		LocalGlChartOfAccount glLiabilityAccount = null;
		
		LocalInvOverheadMemoLine invOverheadMemoLine = null;
		
        LocalAdBranchHome adBranchHome = null;
    	LocalAdBranch adBranch = null;
        
    	LocalAdBranchOverheadMemoLineHome adBranchOverheadMemoLineHome = null;
    	LocalAdBranchOverheadMemoLine adBranchOverheadMemoLine = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invOverheadMemoLineHome = (LocalInvOverheadMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvOverheadMemoLineHome.JNDI_NAME, LocalInvOverheadMemoLineHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                
			adBranchOverheadMemoLineHome = (LocalAdBranchOverheadMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchOverheadMemoLineHome.JNDI_NAME, LocalAdBranchOverheadMemoLineHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalInvOverheadMemoLine invExistingOverheadMemoLine = null;
			
			try {
				
				invExistingOverheadMemoLine = invOverheadMemoLineHome.findByOmlName(mdetails.getOmlName(), AD_CMPNY);
				
				if (!invExistingOverheadMemoLine.getOmlCode().equals(mdetails.getOmlCode())) {
					
					throw new GlobalRecordAlreadyExistException();
					
				}
				
			} catch (FinderException ex) {
				
			}
			
			// get overhead account and liability account to validate accounts
			
			try {
				
				glOverheadAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getOmlCoaOverheadAccount(), AD_CMPNY);
				
			} catch (FinderException ex) {
				
				throw new InvOMLCoaGlOverheadAccountNotFoundException();
				
			}
			
			try {
				
				glLiabilityAccount = glChartOfAccountHome.findByCoaAccountNumber(mdetails.getOmlCoaLiabilityAccount(), AD_CMPNY);
				
			} catch (FinderException ex) {
				
				throw new InvOMLCoaGlLiabilityAccountNotFoundException();
				
			}
			
			// find and update overhead memo line
			
			invOverheadMemoLine = invOverheadMemoLineHome.findByPrimaryKey(mdetails.getOmlCode());
			
			invOverheadMemoLine.setOmlName(mdetails.getOmlName());
			invOverheadMemoLine.setOmlDescription(mdetails.getOmlDescription());
			invOverheadMemoLine.setOmlUnitCost(mdetails.getOmlUnitCost());
			invOverheadMemoLine.setOmlGlCoaOverheadAccount(glOverheadAccount.getCoaCode());
			invOverheadMemoLine.setOmlGlCoaLiabilityAccount(glLiabilityAccount.getCoaCode());
			
			LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);
			invUnitOfMeasure.addInvOverheadMemoLine(invOverheadMemoLine);	
			
            // remove all adBranchOverheadMemoLine lines			
            Collection adBranchOverheadMemoLines = adBranchOverheadMemoLineHome.findBomlByOmlCodeAndRsName(invOverheadMemoLine.getOmlCode(), RS_NM, AD_CMPNY);
			
            Iterator j = adBranchOverheadMemoLines.iterator();
            

            while(j.hasNext()) {
            	
            	adBranchOverheadMemoLine = (LocalAdBranchOverheadMemoLine)j.next();
            	
            	invOverheadMemoLine.dropAdBranchOverheadMemoLine(adBranchOverheadMemoLine);
            	
            	adBranch = adBranchHome.findByPrimaryKey(adBranchOverheadMemoLine.getAdBranch().getBrCode());
            	adBranch.dropAdBranchOverheadMemoLine(adBranchOverheadMemoLine);
            	adBranchOverheadMemoLine.remove();
            }
            
            // create new Branch Standard Memo Line
        	LocalGlChartOfAccount glOverheadCoa = null;
        	LocalGlChartOfAccount glLiabilityCoa = null;
        	Iterator i = branchList.iterator();
        	
			while(i.hasNext()) {
				
				AdModBranchOverheadMemoLineDetails adBrOmlDetails = (AdModBranchOverheadMemoLineDetails)i.next();
				
				try {
					glOverheadCoa = glChartOfAccountHome.findByCoaAccountNumber( adBrOmlDetails.getBOMLOverheadAccountNumber(), AD_CMPNY);
				} catch (FinderException ex) {
					throw new InvOMLCoaGlOverheadAccountNotFoundException();
				}

				try {
					glLiabilityCoa = glChartOfAccountHome.findByCoaAccountNumber( adBrOmlDetails.getBOMLLiabilityAccountNumber(), AD_CMPNY);				
				} catch (FinderException ex) {
					throw new InvOMLCoaGlLiabilityAccountNotFoundException();
				}
				adBranchOverheadMemoLine = adBranchOverheadMemoLineHome.create( glOverheadCoa.getCoaCode(), glLiabilityCoa.getCoaCode(), AD_CMPNY);
				
				invOverheadMemoLine.addAdBranchOverheadMemoLine(adBranchOverheadMemoLine);
				adBranch = adBranchHome.findByPrimaryKey(adBrOmlDetails.getBOMLBranchCode());
				adBranch.addAdBranchOverheadMemoLine(adBranchOverheadMemoLine);
			}
			
		} catch (GlobalRecordAlreadyExistException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (InvOMLCoaGlOverheadAccountNotFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;        	
			
		} catch (InvOMLCoaGlLiabilityAccountNotFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;        	
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}    
	
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void deleteInvOmlEntry(Integer OML_CODE, Integer AD_CMPNY) 
	throws GlobalRecordAlreadyDeletedException,
	GlobalRecordAlreadyAssignedException {
		
		Debug.print("InvOverheadMemoLineControllerBean deleteInvOmlEntry");
		
		LocalInvOverheadMemoLineHome invOverheadMemoLineHome = null;
		
		// Initialize EJB Home
		
		try {
			
			invOverheadMemoLineHome = (LocalInvOverheadMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvOverheadMemoLineHome.JNDI_NAME, LocalInvOverheadMemoLineHome.class);           
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalInvOverheadMemoLine invOverheadMemoLine = null;                
			
			try {
				
				invOverheadMemoLine = invOverheadMemoLineHome.findByPrimaryKey(OML_CODE);
				
			} catch (FinderException ex) {
				
				throw new GlobalRecordAlreadyDeletedException();
				
			}
			
			if (!invOverheadMemoLine.getInvOverheadLines().isEmpty()) {
				
				throw new GlobalRecordAlreadyAssignedException();
				
			}
			
			invOverheadMemoLine.remove();
			
		} catch (GlobalRecordAlreadyDeletedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (GlobalRecordAlreadyAssignedException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}  
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("InvOverheadMemoLineControllerBean getGlFcPrecisionUnit");
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public AdResponsibilityDetails getAdRsByRsCode(Integer RS_CODE) 
	throws GlobalNoRecordFoundException {
		
		LocalAdResponsibilityHome adResHome = null;
		LocalAdResponsibility adRes = null;
		
		try {
			adResHome = (LocalAdResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdResponsibilityHome.JNDI_NAME, LocalAdResponsibilityHome.class);
		} catch (NamingException ex) {
			
		}
		
		try {
			adRes = adResHome.findByPrimaryKey(RS_CODE);    		
		} catch (FinderException ex) {
			
		}
		
		AdResponsibilityDetails details = new AdResponsibilityDetails();
		details.setRsName(adRes.getRsName());
		
		return details;
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdBrAll(Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{
		
		Debug.print("InvOverheadMemoLineControllerBean getBrAll");
		
		LocalAdBranchHome adBranchHome = null;
		LocalAdBranch adBranch = null;
		
		Collection adBranches = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adBranches = adBranchHome.findBrAll(AD_CMPNY);
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (adBranches.isEmpty()) {
			
			throw new GlobalNoRecordFoundException();
			
		}
		
		Iterator i = adBranches.iterator();
		
		while(i.hasNext()) {
			
			adBranch = (LocalAdBranch)i.next();
			
        	AdBranchDetails details = new AdBranchDetails();

        	details.setBrBranchCode(adBranch.getBrBranchCode());
        	details.setBrCode(adBranch.getBrCode());
        	details.setBrName(adBranch.getBrName());
			
			list.add(details);
			
		}
		
		return list;
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdBrOMLAll(Integer BOML_CODE, String RS_NM, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{
		
		Debug.print("InvOverheadMemoLineControllerBean getAdBrSMLAll");
		
		LocalAdBranchOverheadMemoLineHome adBranchOverheadMemoLineHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalGlChartOfAccountHome glChartOfAccountHome = null;
		
		LocalAdBranchOverheadMemoLine adBranchOverheadMemoLine = null;
		LocalAdBranch adBranch = null;
		LocalGlChartOfAccount glOverheadCoa = null;
		LocalGlChartOfAccount glLiabilityCoa = null;
		
		Collection adBranchOverheadMemoLines = null;
		
		ArrayList branchList = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adBranchOverheadMemoLineHome = (LocalAdBranchOverheadMemoLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchOverheadMemoLineHome.JNDI_NAME, LocalAdBranchOverheadMemoLineHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adBranchOverheadMemoLines = adBranchOverheadMemoLineHome.findBomlByOmlCodeAndRsName(BOML_CODE, RS_NM, AD_CMPNY);
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (adBranchOverheadMemoLines.isEmpty()) {
			
			throw new GlobalNoRecordFoundException();
			
		}
		
		try {
			
			Iterator i = adBranchOverheadMemoLines.iterator();
			
			while(i.hasNext()) {
				
				adBranchOverheadMemoLine = (LocalAdBranchOverheadMemoLine)i.next();
				
				adBranch = adBranchHome.findByPrimaryKey(adBranchOverheadMemoLine.getAdBranch().getBrCode());
				glOverheadCoa = glChartOfAccountHome.findByPrimaryKey(adBranchOverheadMemoLine.getBomlGlCoaOverheadAccount());
				glLiabilityCoa = glChartOfAccountHome.findByPrimaryKey(adBranchOverheadMemoLine.getBomlGlCoaLiabilityAccount());
				
				AdModBranchOverheadMemoLineDetails mdetails = new AdModBranchOverheadMemoLineDetails();
				mdetails.setBOMLBranchCode(adBranch.getBrCode());
				mdetails.setBOMLBranchName(adBranch.getBrName());
				mdetails.setBOMLOverheadAccountNumber(glOverheadCoa.getCoaAccountNumber());
				mdetails.setBOMLOverheadAccountDesc(glOverheadCoa.getCoaAccountDescription());
				mdetails.setBOMLLiabilityAccountNumber(glLiabilityCoa.getCoaAccountNumber());
				mdetails.setBOMLLiabilityAccountDesc(glLiabilityCoa.getCoaAccountDescription());
				
				branchList.add(mdetails);
			}
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		return branchList;
		
	}
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvOverheadMemoLineControllerBean ejbCreate");
		
	}
	
}

