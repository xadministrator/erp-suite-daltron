package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.genfld.LocalGenSegmentHome;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPhysicalInventory;
import com.ejb.inv.LocalInvPhysicalInventoryHome;
import com.ejb.inv.LocalInvPhysicalInventoryLine;
import com.ejb.inv.LocalInvPhysicalInventoryLineHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModAdjustmentDetails;
import com.util.InvModPhysicalInventoryDetails;
import com.util.InvModPhysicalInventoryLineDetails;
import com.util.InvModUnitOfMeasureDetails;
import com.util.InvPhysicalInventoryDetails;

/**
 * @ejb:bean name="InvPhysicalInventoryEntryControllerEJB"
 *           display-name="used for entering physical inventory count"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvPhysicalInventoryEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvPhysicalInventoryEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvPhysicalInventoryEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 *
*/

public class InvPhysicalInventoryEntryControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {

        Debug.print("InvPhysicalInventoryEntryControllerBean getInvLocAll");

        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            invLocations = invLocationHome.findLocAll(AD_CMPNY);

            if (invLocations.isEmpty()) {

                return null;

            }

            Iterator i = invLocations.iterator();

            while (i.hasNext()) {

                LocalInvLocation invLocation = (LocalInvLocation)i.next();
                String details = invLocation.getLocName();

                list.add(details);

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {

        Debug.print("InvPhysicalInventoryEntryControllerBean getInvUomByIiName");

        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemHome invItemHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());
        }

        try {

            LocalInvItem invItem = null;
            LocalInvUnitOfMeasure invItemUnitOfMeasure = null;

            invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();

            Collection invUnitOfMeasures = invUnitOfMeasureHome.findByUomAdLvClass(
                	invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY);

        	Iterator i = invUnitOfMeasures.iterator();

        	while (i.hasNext()) {

                LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();

                InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();

                details.setUomName(invUnitOfMeasure.getUomName());

                if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {

                    details.setDefault(true);

                }

                list.add(details);

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public double getQuantityByIiNameAndUomName(String II_NM, String LOC_NM, String UOM_NM, Date PI_DT, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("InvPhysicalInventoryEntryControllerBean getQuantityByIiNameAndUomName");

        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalInvCostingHome invCostingHome = null;

        // Initialize EJB Home

        try {

            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            double actualQuantity = 0d;

            LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(II_NM, LOC_NM, AD_CMPNY);

            try {

                LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(PI_DT,
                        invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                actualQuantity = invCosting.getCstRemainingQuantity();

            } catch (FinderException ex) {

            }

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            return EJBCommon.roundIt(actualQuantity * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor(), this.getInvGpQuantityPrecisionUnit(AD_CMPNY));

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {

        Debug.print("InvPhysicalInventoryEntryControllerBean getAdLvInvItemCategoryAll");

        LocalAdLookUpValueHome adLookUpValueHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);

            Iterator i = adLookUpValues.iterator();

            while (i.hasNext()) {

                LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

                list.add(adLookUpValue.getLvName());

            }

            return list;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdUsrAll(Integer AD_CMPNY) {

        Debug.print("InvPhysicalInventoryEntryControllerBean getAdUsrAll");

        LocalAdUserHome adUserHome = null;

        LocalAdUser adUser = null;

        Collection adUsers = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adUserHome = (LocalAdUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            adUsers = adUserHome.findUsrAll(AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adUsers.isEmpty()) {

            return null;

        }

        Iterator i = adUsers.iterator();

        while (i.hasNext()) {

            adUser = (LocalAdUser)i.next();

            list.add(adUser.getUsrName());

        }

        return list;

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModPhysicalInventoryDetails getInvPiByPiCode(Date PI_DT, Integer PI_CODE, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalNoRecordFoundException {

        Debug.print("InvPhysicalInventoryEntryControllerBean getInvPiByPiCode");

        LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
        LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGenSegmentHome genSegmentHome = null;
        LocalGenValueSetValueHome genValueSetValueHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
            invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            genSegmentHome = (LocalGenSegmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGenSegmentHome.JNDI_NAME, LocalGenSegmentHome.class);
            genValueSetValueHome = (LocalGenValueSetValueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValueHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvPhysicalInventory invPhysicalInventory = null;


            try {

                invPhysicalInventory = invPhysicalInventoryHome.findByPrimaryKey(PI_CODE);

            } catch (FinderException ex) {

                throw new GlobalNoRecordFoundException();

            }

            ArrayList piPilList = new ArrayList();

            Collection invPhysicalInventoryLines = invPhysicalInventoryLineHome.findByPiCode(PI_CODE, AD_CMPNY);

            Iterator i = invPhysicalInventoryLines.iterator();

            while (i.hasNext()) {

                LocalInvPhysicalInventoryLine invPhysicalInventoryLine = (LocalInvPhysicalInventoryLine)i.next();

                InvModPhysicalInventoryLineDetails mdetails = new InvModPhysicalInventoryLineDetails();
                mdetails.setPilCode(invPhysicalInventoryLine.getPilCode());
                mdetails.setPilEndingInventory(invPhysicalInventoryLine.getPilEndingInventory());
                mdetails.setPilWastage(invPhysicalInventoryLine.getPilWastage());
                mdetails.setPilVariance(invPhysicalInventoryLine.getPilVariance());
                mdetails.setPilMisc(invPhysicalInventoryLine.getPilMisc());

                try {

                   /*LocalInvCosting invCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(invPhysicalInventory.getPiDate().getTime(),
                            invPhysicalInventoryLine.getInvItemLocation().getInvItem().getIiName(), invPhysicalInventoryLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                    mdetails.setPilRemainingQuantity(invCosting.getCstRemainingQuantity());
                    //System.out.println("getInvPiByPiCode: "+invCosting.getCstRemainingQuantity());*/
                	/*LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invPhysicalInventory.getPiDate(),
                			invPhysicalInventoryLine.getInvItemLocation().getInvItem().getIiName(), invPhysicalInventoryLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);*/

                	mdetails.setPilRemainingQuantity(this.getQuantityByIiNameAndUomName(invPhysicalInventoryLine.getInvItemLocation().getInvItem().getIiName(),invPhysicalInventoryLine.getInvItemLocation().getInvLocation().getLocName(),
                			invPhysicalInventoryLine.getInvUnitOfMeasure().getUomName(),invPhysicalInventory.getPiDate(), AD_BRNCH,AD_CMPNY));

                } catch (Exception ex) {

                    mdetails.setPilRemainingQuantity(0d);

                }
                mdetails.setPilIlLocName(invPhysicalInventoryLine.getInvItemLocation().getInvLocation().getLocName());
                mdetails.setPilIlIiName(invPhysicalInventoryLine.getInvItemLocation().getInvItem().getIiName());
                mdetails.setPilIlIiDescription(invPhysicalInventoryLine.getInvItemLocation().getInvItem().getIiDescription());
                mdetails.setPilIlIiUnit(invPhysicalInventoryLine.getInvUnitOfMeasure().getUomName());

                piPilList.add(mdetails);

            }

            InvModPhysicalInventoryDetails details = new InvModPhysicalInventoryDetails();

            details.setPiCode(invPhysicalInventory.getPiCode());
            details.setPiDate(invPhysicalInventory.getPiDate());
            details.setPiReferenceNumber(invPhysicalInventory.getPiReferenceNumber());
            details.setPiDescription(invPhysicalInventory.getPiDescription());
            details.setPiCreatedBy(invPhysicalInventory.getPiCreatedBy());
            details.setPiDateCreated(invPhysicalInventory.getPiDateCreated());
            details.setPiLastModifiedBy(invPhysicalInventory.getPiLastModifiedBy());
            details.setPiDateLastModified(invPhysicalInventory.getPiDateLastModified());
            try {
            	details.setPiLocName(invPhysicalInventory.getInvLocation().getLocName());
            }catch (Exception e) {
            	details.setPiLocName("");
			}

            details.setPiAdLvCategory(invPhysicalInventory.getPiAdLvCategory());
            details.setPiVarianceAdjusted(invPhysicalInventory.getPiVarianceAdjusted());
            details.setPiWastageAdjusted(invPhysicalInventory.getPiWastageAdjusted());
            details.setPiPilList(piPilList);
            details.setPiWastageAccount(getInvPrfDefaultWastageAccount(AD_BRNCH, AD_CMPNY).getPiWastageAccount());
            details.setPiWastageAccountDescription(getInvPrfDefaultWastageAccount(AD_BRNCH,AD_CMPNY).getPiWastageAccountDescription());

            return details;

        } catch (GlobalNoRecordFoundException ex) {

            throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUnsavedIlByLocNameAndIiAdLvCategoryAndPiCode(Date PI_DT, String LOC_NM, String II_AD_LV_CTGRY, Integer PI_CODE, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalNoRecordFoundException {

        Debug.print("InvPhysicalInventoryEntryControllerBean getInvUnsavedIlByLocNameAndIiAdLvCategoryAndPiCode");

        LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
        LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
            invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            Collection invItemLocations = invItemLocationHome.findItemByLocNameAndIiAdLvCategory(LOC_NM, II_AD_LV_CTGRY, AD_CMPNY);

            LocalInvPhysicalInventory invPhysicalInventory = invPhysicalInventoryHome.findByPrimaryKey(PI_CODE);

            if (invItemLocations.isEmpty()) {

                throw new GlobalNoRecordFoundException();

            }

            Iterator i = invItemLocations.iterator();

            while (i.hasNext()) {

                LocalInvItemLocation invItemLocation = (LocalInvItemLocation)i.next();

                try {

                    LocalInvPhysicalInventoryLine invPhysicalInventoryLine = invPhysicalInventoryLineHome.findByPiCodeAndIlCode(PI_CODE, invItemLocation.getIlCode(), AD_CMPNY);
                    continue;

                } catch (FinderException ex) {

                }

                InvModPhysicalInventoryLineDetails mPilDetails = new InvModPhysicalInventoryLineDetails();
                mPilDetails.setPilEndingInventory(0d);
                mPilDetails.setPilWastage(0d);
                mPilDetails.setPilVariance(0d);

                try {

                    /*LocalInvCosting invCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(invPhysicalInventory.getPiDate().getTime(),
                            invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                    mPilDetails.setPilRemainingQuantity(invCosting.getCstRemainingQuantity());
                    System.out.println("getInvUnsavedIlByLocNameAndIiAdLvCategoryAndPiCode: "+invCosting.getCstRemainingQuantity());

                	*/

                    LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invPhysicalInventory.getPiDate(),
                            invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                    mPilDetails.setPilRemainingQuantity(invCosting.getCstRemainingQuantity());


                } catch (FinderException ex) {

                    mPilDetails.setPilRemainingQuantity(0d);

                }

                mPilDetails.setPilIlLocName(invItemLocation.getInvLocation().getLocName());
                mPilDetails.setPilIlIiName(invItemLocation.getInvItem().getIiName());
                mPilDetails.setPilIlIiDescription(invItemLocation.getInvItem().getIiDescription());
                mPilDetails.setPilIlIiUnit(invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName());

                list.add(mPilDetails);

            }

            return list;

        } catch (GlobalNoRecordFoundException ex) {

            throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvIlByPiDateInvLocNameAndInvIiAdLvCategory(Date PI_DT, String LOC_NM, String II_AD_LV_CTGRY, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalNoRecordFoundException {

        Debug.print("InvPhysicalInventoryEntryControllerBean getInvIlByPiDateInvLocNameAndInvIiAdLvCategory");

        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Collection invItemLocations = invItemLocationHome.findItemByLocNameAndIiAdLvCategoryAndAdBranch(LOC_NM, II_AD_LV_CTGRY, AD_BRNCH, AD_CMPNY);

            if (invItemLocations.isEmpty()) {

                throw new GlobalNoRecordFoundException();

            }

            Iterator i = invItemLocations.iterator();

            while (i.hasNext()) {

                LocalInvItemLocation invItemLocation = (LocalInvItemLocation)i.next();

                InvModPhysicalInventoryLineDetails mPilDetails = new InvModPhysicalInventoryLineDetails();
                mPilDetails.setPilEndingInventory(0d);
                mPilDetails.setPilWastage(0d);
                mPilDetails.setPilVariance(0d);

                try {

                    LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(PI_DT,
                            invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                    mPilDetails.setPilRemainingQuantity(invCosting.getCstRemainingQuantity());

                } catch (FinderException ex) {

                    mPilDetails.setPilRemainingQuantity(0d);

                }

                mPilDetails.setPilIlIiName(invItemLocation.getInvItem().getIiName());
                mPilDetails.setPilIlLocName(invItemLocation.getInvLocation().getLocName());
                mPilDetails.setPilIlIiDescription(invItemLocation.getInvItem().getIiDescription());
                mPilDetails.setPilIlIiUnit(invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName());

                list.add(mPilDetails);

            }

            return list;

        } catch (GlobalNoRecordFoundException ex) {

            throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }




    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModPhysicalInventoryLineDetails getInvIlByPiDateInvIiName(Date PI_DT, String II_NM, String LOC_NM, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalNoRecordFoundException {

        Debug.print("InvPhysicalInventoryEntryControllerBean getInvIlByPiDateInvIiName");

        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	LocalInvItemLocation invItemLocation = null;

        	try {

        		invItemLocation = invItemLocationHome.findByIiNameAndLocName(II_NM, LOC_NM, AD_CMPNY);

        	}catch (Exception e) {

        		throw new GlobalNoRecordFoundException();
			}




            InvModPhysicalInventoryLineDetails mPilDetails = new InvModPhysicalInventoryLineDetails();
            mPilDetails.setPilEndingInventory(0d);
            mPilDetails.setPilWastage(0d);
            mPilDetails.setPilVariance(0d);

            try {

                LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(PI_DT,
                		invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                mPilDetails.setPilRemainingQuantity(invCosting.getCstRemainingQuantity());

            } catch (FinderException ex) {

                mPilDetails.setPilRemainingQuantity(0d);

            }

            mPilDetails.setPilIlIiName(invItemLocation.getInvItem().getIiName());
            mPilDetails.setPilIlLocName(invItemLocation.getInvLocation().getLocName());
            mPilDetails.setPilIlIiDescription(invItemLocation.getInvItem().getIiDescription());
            mPilDetails.setPilIlIiUnit(invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName());

            return mPilDetails;

        } catch (GlobalNoRecordFoundException ex) {

            throw ex;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }




    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvPiEntry(InvPhysicalInventoryDetails details, String LOC_NM, ArrayList pilList, Integer AD_BRNCH, Integer AD_CMPNY)
    throws 	GlobalRecordAlreadyExistException {

        Debug.print("InvPhysicalInventoryEntryControllerBean saveInvPiEntry");

        LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
        LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
            invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvPhysicalInventory invPhysicalInventory = null;

            try {

                LocalInvPhysicalInventory invExistingPhysicalInventory = invPhysicalInventoryHome.findByPiDateAndLocNameAndCategoryNameAndBrCode(details.getPiDate(), LOC_NM, details.getPiAdLvCategory(), AD_BRNCH, AD_CMPNY);

                if(details.getPiCode() == null || details.getPiCode() != null &&
                        !invExistingPhysicalInventory.getPiCode().equals(details.getPiCode())) {

                    throw new GlobalRecordAlreadyExistException();

                }

            } catch (GlobalRecordAlreadyExistException ex) {

                throw ex;

            } catch (FinderException ex) {

            }

            // create physical inventory

            if (details.getPiCode() == null) {

                invPhysicalInventory = invPhysicalInventoryHome.create(details.getPiDate(), details.getPiReferenceNumber(),
                        details.getPiDescription(), details.getPiCreatedBy(), details.getPiDateCreated(),
                        details.getPiLastModifiedBy(), details.getPiDateLastModified(), details.getPiAdLvCategory(),
                        EJBCommon.FALSE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

            } else {

                invPhysicalInventory = invPhysicalInventoryHome.findByPrimaryKey(details.getPiCode());

                invPhysicalInventory.setPiReferenceNumber(details.getPiReferenceNumber());
                invPhysicalInventory.setPiDate(details.getPiDate());
                invPhysicalInventory.setPiDescription(details.getPiDescription());
                invPhysicalInventory.setPiLastModifiedBy(details.getPiLastModifiedBy());
                invPhysicalInventory.setPiDateLastModified(details.getPiDateLastModified());
                invPhysicalInventory.setPiAdLvCategory(details.getPiAdLvCategory());

            }

            try {

            	LocalInvLocation invLocation = invLocationHome.findByLocName(LOC_NM, AD_CMPNY);
                invPhysicalInventory.setInvLocation(invLocation);

            } catch (FinderException ex) {

            }

            //invLocation.addInvPhysicalInventory(invPhysicalInventory);

            // remove all physical inventory lines

            Collection invPhysicalInventoryLines = invPhysicalInventoryLineHome.findByPiCode(invPhysicalInventory.getPiCode(), AD_CMPNY);

            Iterator i = invPhysicalInventoryLines.iterator();

            while (i.hasNext()) {

                LocalInvPhysicalInventoryLine invPhysicalInventoryLine = (LocalInvPhysicalInventoryLine)i.next();

                i.remove();

                invPhysicalInventoryLine.remove();

            }

            i = pilList.iterator();
            LocalInvItemLocation invItemLocation = null;

            while (i.hasNext()) {

                InvModPhysicalInventoryLineDetails mdetails = (InvModPhysicalInventoryLineDetails)i.next();
                try {
                    System.out.println("mdetails.getPilIlLocName()="+mdetails.getPilIlLocName());
                    System.out.println("mdetails.getPilIlIiName()="+mdetails.getPilIlIiName());

                    invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getPilIlLocName(), mdetails.getPilIlIiName(), AD_CMPNY);

                } catch (FinderException ex) {

                    throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getPilIlIiName()));

                }


                LocalInvPhysicalInventoryLine invPhysicalInventoryLine = this.addInvPilEntry(mdetails, invPhysicalInventory, invItemLocation, AD_CMPNY);

            }

            return invPhysicalInventory.getPiCode();

        } catch (GlobalRecordAlreadyExistException ex) {

            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {

            getSessionContext().setRollbackOnly();
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer executeInvWastageGeneration(Integer PI_CODE, String WSTG_ACCNT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalBranchAccountNumberInvalidException,
    GlobalAccountNumberInvalidException {

        Debug.print("InvPhysicalInventoryEntryControllerBean executeInvWastageGeneration");

        LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
        LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;

        LocalInvPhysicalInventory invPhysicalInventory = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
            invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            // 	auto save physical inventory

            try {

                invPhysicalInventory = invPhysicalInventoryHome.findByPrimaryKey(PI_CODE);

            } catch (FinderException ex) {

            }

            // check if physical inventory has wastage

            Collection invPhysicalInventoryLines = invPhysicalInventory.getInvPhysicalInventoryLines();

            double TOTAL_WASTAGE = 0d;

            Iterator i = invPhysicalInventoryLines.iterator();

            while(i.hasNext()) {

                LocalInvPhysicalInventoryLine invPhysicalInventoryLine = (LocalInvPhysicalInventoryLine)i.next();

                TOTAL_WASTAGE += invPhysicalInventoryLine.getPilWastage();

            }

            if (TOTAL_WASTAGE == 0d) {

                // set wastage adjusted to true

                invPhysicalInventory.setPiWastageAdjusted(EJBCommon.TRUE);

                return null;

            }

            String ADJ_NMBR = null;

            //generate document number

            LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
            LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

            try {

                adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

            } catch(FinderException ex) { }

            try {

                adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) { }

            if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A') {

                while (true) {

                    if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

                        try {

                            invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
                            adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

                        } catch (FinderException ex) {

                            ADJ_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
                            adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
                            break;

                        }

                    } else {

                        try {

                            invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
                            adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

                        } catch (FinderException ex) {

                            ADJ_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
                            adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
                            break;

                        }

                    }

                }

            }


            // generate adjustment as WASTAGE

            LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_NMBR, invPhysicalInventory.getPiReferenceNumber(),
                    invPhysicalInventory.getPiDescription(), invPhysicalInventory.getPiDate(), "WASTAGE", null,
                    EJBCommon.FALSE, USR_NM, invPhysicalInventory.getPiDate(), USR_NM, invPhysicalInventory.getPiDate(), null, null,
					null, null, null, null, EJBCommon.FALSE,  EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

            try {

                LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(WSTG_ACCNT, AD_BRNCH, AD_CMPNY);
                //glChartOfAccount.addInvAdjustment(invAdjustment);
                invAdjustment.setGlChartOfAccount(glChartOfAccount);
            } catch (FinderException ex) {

                throw new GlobalAccountNumberInvalidException();

            }


            double TOTAL_ADJUSTMENT_AMOUNT = 0d;
            byte DEBIT = 0;

            i = invPhysicalInventoryLines.iterator();

            while (i.hasNext()) {

                LocalInvPhysicalInventoryLine invPhysicalInventoryLine = (LocalInvPhysicalInventoryLine)i.next();

                if (invPhysicalInventoryLine.getPilWastage() == 0d) continue;

                // generate inventory adjustment line

                LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invPhysicalInventoryLine, invAdjustment, invPhysicalInventory.getPiWastageAdjusted(), AD_CMPNY);

                // generate inventory adjustment distribution record

                double TOTAL_AMOUNT = 0d;
                double AMOUNT = 0d;
                double COST = 0d;

                try {

                    LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invPhysicalInventory.getPiDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
                            invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                    COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                } catch (FinderException ex) {

                    COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();

                }


                COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
                        invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
                        COST, true, AD_CMPNY);

                AMOUNT = EJBCommon.roundIt(
                        invAdjustmentLine.getAlAdjustQuantity() * COST,
                        this.getGlFcPrecisionUnit(AD_CMPNY));

                DEBIT = AMOUNT > 0 ? EJBCommon.TRUE : EJBCommon.FALSE;

                // check for branch mapping

                LocalAdBranchItemLocation adBranchItemLocation = null;

                try{

                    adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                            invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

                } catch (FinderException ex) {

                }

                LocalGlChartOfAccount glInventoryChartOfAccount = null;

                if (adBranchItemLocation == null){

                    glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                            invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());

                } else {

                    glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                            adBranchItemLocation.getBilCoaGlInventoryAccount());

                }

                this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(AMOUNT),
                        glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

                TOTAL_AMOUNT += AMOUNT;

                // add adjust quantity to item location committed quantity

                if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

                    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

                    LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();

                    invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

                }

                TOTAL_ADJUSTMENT_AMOUNT += TOTAL_AMOUNT;

            }

            // add adjustment distribution

            DEBIT = TOTAL_ADJUSTMENT_AMOUNT > 0 ? EJBCommon.FALSE : EJBCommon.TRUE;

            this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_ADJUSTMENT_AMOUNT),
                    invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

            // set wastage adjusted to true

            invPhysicalInventory.setPiWastageAdjusted(EJBCommon.TRUE);

            return invAdjustment.getAdjCode();

        } catch(GlobalBranchAccountNumberInvalidException ex) {

            throw new GlobalBranchAccountNumberInvalidException ();

        } catch(GlobalAccountNumberInvalidException ex) {

            throw new GlobalAccountNumberInvalidException ();

        } catch (Exception ex) {

            getSessionContext().setRollbackOnly();
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer executeInvVarianceGeneration(Integer PI_CODE, String VRNC_ACCNT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
    GlobalBranchAccountNumberInvalidException {

        Debug.print("InvPhysicalInventoryEntryControllerBean executeInvVarianceGeneration");

        LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
        LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
        LocalInvAdjustmentHome invAdjustmentHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;

        LocalInvPhysicalInventory invPhysicalInventory = null;
        LocalInvCosting invCosting = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
            invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            // 	auto save physical inventory

            try {

                invPhysicalInventory = invPhysicalInventoryHome.findByPrimaryKey(PI_CODE);

            } catch (FinderException ex) {

            }

            // check if physical inventory has variance

            Collection invPhysicalInventoryLines = invPhysicalInventory.getInvPhysicalInventoryLines();

            boolean isVarianceExisting = false;

            Iterator i = invPhysicalInventoryLines.iterator();

            while(i.hasNext()) {

                LocalInvPhysicalInventoryLine invPhysicalInventoryLine = (LocalInvPhysicalInventoryLine)i.next();

                if (invPhysicalInventoryLine.getPilVariance() != 0)
                	isVarianceExisting = true;

            }

            if (!isVarianceExisting) {

                // set variance adjusted to true

                invPhysicalInventory.setPiVarianceAdjusted(EJBCommon.TRUE);

                return null;

            }

            String ADJ_NMBR = null;

            //generate document number

            LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
            LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;

            try {

                adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);

            } catch(FinderException ex) { }

            try {

                adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);

            } catch (FinderException ex) { }

            if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A') {

                while (true) {

                    if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {

                        try {

                            invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
                            adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));

                        } catch (FinderException ex) {

                            ADJ_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
                            adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
                            break;

                        }

                    } else {

                        try {

                            invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);
                            adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));

                        } catch (FinderException ex) {

                            ADJ_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
                            adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
                            break;

                        }

                    }

                }

            }

            // generate adjustment as VARIANCE

            LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_NMBR, invPhysicalInventory.getPiReferenceNumber(),
                    invPhysicalInventory.getPiDescription(), invPhysicalInventory.getPiDate(), "VARIANCE", null, EJBCommon.FALSE,
					USR_NM, invPhysicalInventory.getPiDate(), USR_NM, invPhysicalInventory.getPiDate(), null, null, null, null, null, null,
					EJBCommon.FALSE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);

            try {

                LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(VRNC_ACCNT, AD_BRNCH, AD_CMPNY);
                //glChartOfAccount.addInvAdjustment(invAdjustment);
                invAdjustment.setGlChartOfAccount(glChartOfAccount);
            } catch (FinderException ex) {

            }

            double TOTAL_ADJUSTMENT_AMOUNT = 0d;
            byte DEBIT = 0;

            i = invPhysicalInventoryLines.iterator();

            while (i.hasNext()) {

                LocalInvPhysicalInventoryLine invPhysicalInventoryLine = (LocalInvPhysicalInventoryLine)i.next();

                if (invPhysicalInventoryLine.getPilVariance() == 0d) continue;

                // generate inventory adjustment line

                LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invPhysicalInventoryLine, invAdjustment, invPhysicalInventory.getPiWastageAdjusted(), AD_CMPNY);

                // generate inventory adjustment distribution record

                double TOTAL_AMOUNT = 0d;
                double AMOUNT = 0d;
                double COST = 0d;

                try {

                    invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invPhysicalInventory.getPiDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
                            invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                    COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                } catch (FinderException ex) {

                    COST = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();

                }

                COST = this.convertCostByUom(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
                        invAdjustmentLine.getInvUnitOfMeasure().getUomName(),
                        COST, true, AD_CMPNY);

                AMOUNT = EJBCommon.roundIt(
                        invAdjustmentLine.getAlAdjustQuantity() * COST,
                        this.getGlFcPrecisionUnit(AD_CMPNY));

                DEBIT = AMOUNT > 0 ? EJBCommon.TRUE : EJBCommon.FALSE;

                // check for branch mapping

                LocalAdBranchItemLocation adBranchItemLocation = null;

                try{

                    adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                            invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);

                } catch (FinderException ex) {

                }

                LocalGlChartOfAccount glInventoryChartOfAccount = null;

                if (adBranchItemLocation == null){

                    glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                            invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());

                } else {

                    glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
                            adBranchItemLocation.getBilCoaGlInventoryAccount());

                }


                this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "INVENTORY", DEBIT, Math.abs(AMOUNT),
                        glInventoryChartOfAccount.getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);

                TOTAL_AMOUNT += AMOUNT;

                // add adjust quantity to item location committed quantity

                if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

                    double convertedQuantity = this.convertByUomFromAndItemAndQuantity(invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(), Math.abs(invAdjustmentLine.getAlAdjustQuantity()), AD_CMPNY);

                    LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();

                    invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

                }

                TOTAL_ADJUSTMENT_AMOUNT += TOTAL_AMOUNT;

            }

            // add adjustment distribution

            DEBIT = TOTAL_ADJUSTMENT_AMOUNT > 0 ? EJBCommon.FALSE : EJBCommon.TRUE;

            this.addInvDrEntry(invAdjustment.getInvDrNextLine(), "ADJUSTMENT", DEBIT, Math.abs(TOTAL_ADJUSTMENT_AMOUNT),
                    invAdjustment.getGlChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);


            // set variance adjusted to true

            invPhysicalInventory.setPiVarianceAdjusted(EJBCommon.TRUE);

            return invAdjustment.getAdjCode();

        } catch(GlobalBranchAccountNumberInvalidException ex) {

            throw new GlobalBranchAccountNumberInvalidException ();

        } catch (Exception ex) {

            getSessionContext().setRollbackOnly();
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }


    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteInvPiEntry(Integer PI_CODE, String AD_USR) {

        Debug.print("InvPhysicalInventoryEntryControllerBean deleteInvPiEntry");

        LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home

        try {

        	invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
        	adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
        				lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

           LocalInvPhysicalInventory invPhysicalInventory = invPhysicalInventoryHome.findByPrimaryKey(PI_CODE);
           adDeleteAuditTrailHome.create("INV PHYSICAL INVENTORY", invPhysicalInventory.getPiDate(), invPhysicalInventory.getPiReferenceNumber(), invPhysicalInventory.getPiReferenceNumber(),
					0d, AD_USR, new Date(), invPhysicalInventory.getPiAdCompany());
           invPhysicalInventory.remove();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }


    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModPhysicalInventoryDetails getInvPrfDefaultVarianceAccount(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("InvPhysicalInventoryControllerBean getInvPrfDefaultVarianceAccount");

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdUserHome adUserHome = null;
    	LocalAdBranchHome adBranchHome = null;

        // Initialize EJB Home

        try {
        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        	adUserHome = (LocalAdUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            throw new EJBException(ex.getMessage());
        }

        InvModPhysicalInventoryDetails details = new InvModPhysicalInventoryDetails();
        try {
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
        	String[] accnt = glChartOfAccount.getCoaAccountNumber().split("-");
        //	String vrncAccntNmbr = accnt[0]+"-"+adBranchHome.findByPrimaryKey(AD_BRNCH).getBrCode();
        	String vrncAccntNmbr = glChartOfAccount.getCoaAccountNumber();
        	LocalGlChartOfAccount glVarianceAccount = null;
        	try {

        		System.out.println("acc number: "+ vrncAccntNmbr);
        		glVarianceAccount = glChartOfAccountHome.findByCoaAccountNumber(vrncAccntNmbr, AD_CMPNY);
            	details.setPiVarianceAccount(glVarianceAccount.getCoaAccountNumber());
            	details.setPiVarianceAccountDescription(glVarianceAccount.getCoaAccountDescription());
            	return details;
        	}
        	catch (FinderException ex) {
        		Debug.printStackTrace(ex);
            	details.setPiVarianceAccount("");
            	details.setPiVarianceAccountDescription("");
            	return details;
        	}
        }
        catch (FinderException ex) {
        	Debug.printStackTrace(ex);
        }
    	return details;
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModPhysicalInventoryDetails getInvPrfDefaultWastageAccount(Integer AD_BRNCH, Integer AD_CMPNY) {

    	Debug.print("InvPhysicalInventoryControllerBean getInvPrfDefaultWastageAccount");

    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdUserHome adUserHome = null;
    	LocalAdBranchHome adBranchHome = null;

        // Initialize EJB Home

        try {
        	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
        		lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        	adUserHome = (LocalAdUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            throw new EJBException(ex.getMessage());
        }

        InvModPhysicalInventoryDetails details = new InvModPhysicalInventoryDetails();
        try {
        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
        	LocalGlChartOfAccount glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvWastageAdjustmentAccount());
        	String[] accnt = glChartOfAccount.getCoaAccountNumber().split("-");
        	String wstgAccntNmbr = accnt[0]+"-"+adBranchHome.findByPrimaryKey(AD_BRNCH).getBrBranchCode();
        	LocalGlChartOfAccount glWastageAccount = null;
        	try {
        		glWastageAccount = glChartOfAccountHome.findByCoaAccountNumber(wstgAccntNmbr, AD_CMPNY);
            	details.setPiWastageAccount(glWastageAccount.getCoaAccountNumber());
            	details.setPiWastageAccountDescription(glWastageAccount.getCoaAccountDescription());
            	return details;
        	}
        	catch (FinderException ex) {
        		Debug.printStackTrace(ex);
            	details.setPiWastageAccount("");
            	details.setPiWastageAccountDescription("");
            	return details;
        	}
        }
        catch (FinderException ex) {
        	Debug.printStackTrace(ex);
        }
    	return details;
    }

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("InvPhysicalInventoryEntryControllerBean getInvGpQuantityPrecisionUnit");

        LocalAdPreferenceHome adPreferenceHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }


        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            return adPreference.getPrfInvQuantityPrecisionUnit();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    // private methods

    private LocalInvPhysicalInventoryLine addInvPilEntry(InvModPhysicalInventoryLineDetails mdetails,
            LocalInvPhysicalInventory invPhysicalInventory, LocalInvItemLocation invItemLocation, Integer AD_CMPNY) {

        Debug.print("InvPhysicalInventoryEntryControllerBean addInvPilEntry");

        LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        // Initialize EJB Home

        try {

            invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvPhysicalInventoryLine invPhysicalInventoryLine = invPhysicalInventoryLineHome.create(
                    mdetails.getPilEndingInventory(), mdetails.getPilWastage(), mdetails.getPilVariance(), mdetails.getPilMisc(), AD_CMPNY);

            //invPhysicalInventory.addInvPhysicalInventoryLine(invPhysicalInventoryLine);

            invPhysicalInventoryLine.setInvPhysicalInventory(invPhysicalInventory);

            //LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getPilIlLocName(), mdetails.getPilIlIiName(), AD_CMPNY);
            invPhysicalInventoryLine.setInvItemLocation(invItemLocation);
            //invItemLocation.addInvPhysicalInventoryLine(invPhysicalInventoryLine);

            LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getPilUomName(),AD_CMPNY);
            invPhysicalInventoryLine.setInvUnitOfMeasure(invUnitOfMeasure);
            //invUnitOfMeasure.addInvPhysicalInventoryLine(invPhysicalInventoryLine);

            return invPhysicalInventoryLine;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvPhysicalInventoryLine invPhysicalInventoryLine,
            LocalInvAdjustment invAdjustment, byte PI_WSTG_VRNC_ADJSTD, Integer AD_CMPNY) {

        Debug.print("InvPhysicalInventoryEntryControllerBean addInvAlEntry");

        LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvAdjustmentLine invAdjustmentLine = null;
            System.out.println("ITM_NAME="+invPhysicalInventoryLine.getInvItemLocation().getInvItem().getIiName());
            LocalInvItem invItem = invItemHome.findByIiName(invPhysicalInventoryLine.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);

            double COST = 0d;

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invPhysicalInventoryLine.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            COST = EJBCommon.roundIt(invItem.getIiUnitCost() * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));

            if (PI_WSTG_VRNC_ADJSTD != EJBCommon.TRUE) {

                invAdjustmentLine = invAdjustmentLineHome.create(
                        COST,null,null, invPhysicalInventoryLine.getPilWastage() * 1,0, EJBCommon.FALSE, AD_CMPNY);
                //invAdjustment.addInvAdjustmentLine(invAdjustmentLine);
                invAdjustmentLine.setInvAdjustment(invAdjustment);
            } else {

                invAdjustmentLine = invAdjustmentLineHome.create(
                        COST,null,null, invPhysicalInventoryLine.getPilVariance() * -1,0, EJBCommon.FALSE, AD_CMPNY);
                invAdjustmentLine.setInvAdjustment(invAdjustment);

            }

            LocalInvUnitOfMeasure invUnitOfMeasure = invPhysicalInventoryLine.getInvUnitOfMeasure();
            //invUnitOfMeasure.addInvAdjustmentLine(invAdjustmentLine);
            invAdjustmentLine.setInvUnitOfMeasure(invUnitOfMeasure);

            LocalInvItemLocation invItemLocation = invPhysicalInventoryLine.getInvItemLocation();
            //invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
            invAdjustmentLine.setInvItemLocation(invItemLocation);

            return invAdjustmentLine;

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private void addInvDrEntry(short DR_LN, String DR_CLSS,
            byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)

    throws GlobalBranchAccountNumberInvalidException {

        Debug.print("InvPhysicalInventoryEntryControllerBean addInvDrEntry");

        LocalAdCompanyHome adCompanyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;

        // Initialize EJB Home

        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);


        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            // get company

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            // validate coa

            LocalGlChartOfAccount glChartOfAccount = null;

            try {

                glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);

            } catch(FinderException ex) {

                throw new GlobalBranchAccountNumberInvalidException ();

            }

            // create distribution record

            LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
                    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
                    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);

            //invAdjustment.addInvDistributionRecord(invDistributionRecord);
            invDistributionRecord.setInvAdjustment(invAdjustment);
            //glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
            invDistributionRecord.setInvChartOfAccount(glChartOfAccount);

        } catch(GlobalBranchAccountNumberInvalidException ex) {

            throw new GlobalBranchAccountNumberInvalidException ();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("InvPhysiscalInventoryEntryControllerBean getGlFcPrecisionUnit");


        LocalAdCompanyHome adCompanyHome = null;

        // Initialize EJB Home

        try {

            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            return  adCompany.getGlFunctionalCurrency().getFcPrecision();

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());

        }

    }

    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {

        Debug.print("InvPhysicalInventoryEntryControllerBean convertByUomFromAndItemAndQuantity");

        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }

    private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {

        Debug.print("InvPhysicalInventoryEntryControllerBean convertCostByUom");

        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;

        // Initialize EJB Home

        try {

            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

            if (isFromDefault) {

                return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();

            } else {

                return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();

            }



        } catch (Exception ex) {

            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());

        }

    }


    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("InvPhysicalInventoryEntryControllerBean ejbCreate");

    }

}