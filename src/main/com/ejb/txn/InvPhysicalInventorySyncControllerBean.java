/*
 * InvPhysicalInventorySyncControllerBean
 *
 * Started on March 29, 2008
 *
 * @author  Cybon Castillo
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPhysicalInventory;
import com.ejb.inv.LocalInvPhysicalInventoryHome;
import com.ejb.inv.LocalInvPhysicalInventoryLine;
import com.ejb.inv.LocalInvPhysicalInventoryLineHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModPhysicalInventoryDetails;
import com.util.InvModPhysicalInventoryLineDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="InvPhysicalInventorySyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *
 * @wsee:port-component name="InvPhysicalInventorySyncWS"
 *
 * @jboss:port-component uri="omega-ejb/InvPhysicalInventorySyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.InvPhysicalInventorySyncWS"
 *
 */

public class InvPhysicalInventorySyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
   
    /**
     * @ejb:interface-method
     **/
    public String setNewPhysicalInv(String[] NEW_PI, String BR_BRNCH_CODE, Integer AD_CMPNY) {
    
    	Debug.print("InvPhysicalInventorySyncControllerBean setNewPhysicalInv");    	
    	    	
    	LocalAdBranchHome adBranchHome = null;   
    	LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
        LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
        LocalInvItemHome invItemHome =  null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvCostingHome invCostingHome = null;        
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
    	
    	// Initialize EJB Home
        String itemError="";
        
        try {        	
        	
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
        	invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
            invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException("NamingException" + ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdBranch adBranch = null;
        	
        	try {
        		
        		adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        		
        	} catch (FinderException ef){
        		
        		System.out.println("adBranchHome.findByBrBranchCode " + ef.getMessage() + " : " + BR_BRNCH_CODE);
        		
        	}
        			
        	for (int i=0;i < NEW_PI.length; i++) {
        		
        		InvModPhysicalInventoryDetails details = physicalInventoryDecode(NEW_PI[i]);
        		
        		LocalInvPhysicalInventory invPhysicalInventory = null;
        		
        		details.setPiCreatedBy("admin");
        		details.setPiDateCreated(new Date());
        		details.setPiLastModifiedBy("admin");
        		details.setPiDateLastModified(new Date());
        		
        		InvModPhysicalInventoryLineDetails lineDetails = (InvModPhysicalInventoryLineDetails)details.getPiPilList().toArray()[0];
        		
        		// Check If Item Exist
        		try {
        			
        			LocalInvItem invItem = null;
    				invItem = invItemHome.findByIiName(lineDetails.getPilIlIiName(), AD_CMPNY);
    				
    				// Check If UOM is Valid
    				LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = null;
    				
    				try{
    					
    					invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(lineDetails.getPilIlIiName(), lineDetails.getPilUomName(), AD_CMPNY);
    					
    				} catch (Exception euom){
    					System.out.println("Error On Item/UOM : " + lineDetails.getPilIlIiName() + " /" + lineDetails.getPilUomName());    					
    		        	itemError = lineDetails.getPilIlIiName();
    		        	
    		        	throw new Exception(itemError);
    		        	//return lineDetails.getPilIlIiName();    		        	    		        	
    				}
    				
    				// Set Location
    				try {    		
        				
        				if(invItem.getIiDefaultLocation() == null){
        					
        					details.setPiLocName(invLocationHome.findByLocName("A - Headquarters", AD_CMPNY).getLocName());
        					
        				}else{
        					
        					details.setPiLocName(invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation()).getLocName());        					
        					
        				}
        				
        			} catch (FinderException ef){
        				
        				System.out.println("invLocationHome " + ef.getMessage() + " : " + details.getPiLocName());
        				
        			}
        			
        		} catch (Exception ef){
        			System.out.println("invItemHome.findByIiName " + ef.getMessage() + " : " + lineDetails.getPilIlIiName());
        			
        			throw new Exception(itemError);
        		}        		
        		
        		try{
        			
        			invPhysicalInventory = invPhysicalInventoryHome.findByPiDateAndLocNameAndCategoryNameAndBrCode(details.getPiDate(), details.getPiLocName(), details.getPiAdLvCategory(), adBranch.getBrCode(), AD_CMPNY);
        			
        		} catch(Exception ex){
        			
        			System.out.println("Creating New LocalInvPhysicalInventory");
        			
        		}
        		
        		// Create New
        		if(invPhysicalInventory == null) {	 
        			
        			try {
        				invPhysicalInventory = invPhysicalInventoryHome.create(details.getPiDate(), details.getPiReferenceNumber(), 
            					details.getPiDescription(), details.getPiCreatedBy(), details.getPiDateCreated(),
            					details.getPiLastModifiedBy(), details.getPiDateLastModified(), details.getPiAdLvCategory(),
            					EJBCommon.FALSE, EJBCommon.FALSE, adBranch.getBrCode(), AD_CMPNY);
        				
        				LocalInvLocation invLocation = invLocationHome.findByLocName(details.getPiLocName(), AD_CMPNY);        				
        				//invLocation.addInvPhysicalInventory(invPhysicalInventory);
        				invPhysicalInventory.setInvLocation(invLocation);
        				
        			} catch (Exception ex){
        				System.out.println("invPhysicalInventoryHome.create/invLocationHome.findByLocName " + ex.getMessage() + " : " + details.getPiLocName());
        				
        			}
        			
        		}
        		
    			// Generate Invetory Lines
    			ArrayList list = new ArrayList();
    			
    			try {

    				list = this.getInvUnsavedIlByLocNameAndIiAdLvCategoryAndPiCode(details.getPiLocName(), 
    						details.getPiAdLvCategory(), invPhysicalInventory.getPiCode(), adBranch.getBrCode(), AD_CMPNY);

    			} catch(Exception ex) {
    				
    				System.out.println("getInvIlByPiDateInvLocNameAndInvIiAdLvCategory ERROR: " + ex.getMessage());    				
    				
    			}        		
    			
    			Iterator iter = list.iterator();
    			
    			while (iter.hasNext()) {
    			
    				InvModPhysicalInventoryLineDetails mdetails = (InvModPhysicalInventoryLineDetails)iter.next();	        
    				
    				// Check if Item is Included to Uploaded                    
    				Iterator piLiter = details.getPiPilList().iterator();    				
    				    			
    				while(piLiter.hasNext()){
    			
    					InvModPhysicalInventoryLineDetails piLdetails = (InvModPhysicalInventoryLineDetails)piLiter.next();
    			
    					if(piLdetails.getPilIlIiName().equals(mdetails.getPilIlIiName())){
    			
    						mdetails = piLdetails;
    						break;
    					}
    					
    				}
    			
    				mdetails.setPilRemainingQuantity(0d);                    
    				
    				try {
    					
    					LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    							invPhysicalInventory.getPiDate(), mdetails.getPilIlIiName() , details.getPiLocName(), adBranch.getBrCode(), AD_CMPNY);
    					
    					mdetails.setPilRemainingQuantity(invCosting.getCstRemainingQuantity());
    					mdetails.setPilRemainingQuantity(this.convertByUomFromAndItemAndQuantity(mdetails.getPilUomName(),invItemHome.findByIiName(mdetails.getPilIlIiName(),AD_CMPNY),mdetails.getPilRemainingQuantity(),AD_CMPNY));
    					
    				} catch (FinderException ex) {                        
    					
    					mdetails.setPilRemainingQuantity(0d);    					
    					
    				}
    				
    				
    			
    				mdetails.setPilVariance(mdetails.getPilRemainingQuantity() - mdetails.getPilEndingInventory());
    				    				
    				LocalInvPhysicalInventoryLine invPhysicalInventoryLine = this.addInvPilEntry(mdetails, invPhysicalInventory, details.getPiLocName(), AD_CMPNY);
    				
    				if(invPhysicalInventoryLine==null){
    					
    					return lineDetails.getPilIlIiName();
    				}
    				
    					
    				    			
    			}
    			
        	}
        	return "";   	
        }        
        catch (Exception ex) {        	
        	ex.printStackTrace();
        	ctx.setRollbackOnly();
        	throw new EJBException (itemError);
        	
        }       
     
    }
    
	/**
     * @ejb:interface-method
     **/
    public String[] setPhysicalInvMisc(String piDate, String locName, String category, String[] misc, Integer AD_CMPNY) {    	
    	String[] results = new String[1];;
    	
    	Debug.print("InvPhysicalInventorySyncControllerBean setPhysicalInvMisc");
		
		LocalInvPhysicalInventoryHome invPhysicalInventoryHome;
		
    	// Initialize EJB Home
    	
    	try {
    		
    		invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
		try {
			LocalInvPhysicalInventory invPhysicalInventory = invPhysicalInventoryHome.findByPiDateAndLocNameAndCategoryName(EJBCommon.convertStringToSQLDate(piDate), locName, category, AD_CMPNY);
			Collection invPhysicalInventoryLinesColl = invPhysicalInventory.getInvPhysicalInventoryLines();
			Iterator i = invPhysicalInventoryLinesColl.iterator();
			
			while (i.hasNext()) {
				LocalInvPhysicalInventoryLine invPhysicalInventoryLine = (LocalInvPhysicalInventoryLine)i.next();
				if (invPhysicalInventoryLine.getPilEndingInventory()>0) {
					
					String serial="",blank="";
					for (int j=0;j<misc.length;j++) {
						if (misc[j].substring(0, 10).equals(invPhysicalInventoryLine.getInvItemLocation().getInvItem().getIiName())) {
							if (serial.equals("")) {
								serial=misc[j];
								blank=" ";
							}
							else {
								serial+=","+misc[j];
								blank+=", ";
							}
						}
					}
					invPhysicalInventoryLine.setPilMisc(invPhysicalInventoryLine.getPilEndingInventory() + "_" + blank + "@" + serial + "<" + blank + ">" + blank + ";" + blank + "%" + blank);
					
				}
			}
		}
		catch (FinderException ex) {
			Debug.printStackTrace(ex);
		}
		return results;
    }
	
    private double convertByUomFromAndItemAndQuantity(String fromUnitOfMeasureName, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
    	
    	Debug.print("InvPhysicalInventoryEntryControllerBean convertByUomFromAndItemAndQuantity");		        
    	
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
    		lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
    		
    		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), fromUnitOfMeasureName, AD_CMPNY);
    		LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
    		
    		return EJBCommon.roundIt(ADJST_QTY * invUnitOfMeasureConversion.getUmcConversionFactor()/invDefaultUomConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		ctx.setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    /**
     * @ejb:interface-method
     **/
    public int getInvItemAllLookUpLength(Integer AD_CMPNY) {    	
        
    	Debug.print("InvPhysicalInventorySyncControllerBean getInvItemAllNewAndUpdated");
    	
    	LocalInvItemHome invItemHome = null;   	
    	
    	
    	// Initialize EJB Home
        
        try {
            
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);       	
        	
    	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	        	        	
        	Collection invItems = invItemHome.findEnabledIiAll(AD_CMPNY);        	        	
        	
        	System.out.print("results length = "+ invItems.size());
        	return invItems.size();
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public String[] getInvItemAllLookUp(Integer AD_CMPNY) {    	
        
    	Debug.print("InvPhysicalInventorySyncControllerBean getInvItemAllLookUp");
    	
    	LocalInvItemHome invItemHome = null;
    	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class); 
        	invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class); 
    	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	        	        	
        	Collection invItems = invItemHome.findEnabledIiAll(AD_CMPNY);        	        	
        	        	
        	String[] results = new String[invItems.size()];
        	
        	Iterator i = invItems.iterator();
        	int ctr = 0;
        	
	        while (i.hasNext()) {
	        	
	        	LocalInvItem invItem = (LocalInvItem)i.next();
	        	
	        	Collection invUnitOfMeasureConversions = invItem.getInvUnitOfMeasureConversions();
	        	
	        	Iterator IUomc = invUnitOfMeasureConversions.iterator();
	        	String uomstr="";
	        	
	        	while(IUomc.hasNext()){	        		
	        		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = (LocalInvUnitOfMeasureConversion)IUomc.next();
	        		if (invUnitOfMeasureConversion.getUmcBaseUnit() == 1 || invUnitOfMeasureConversion.getUmcConversionFactor()>1)
	        			uomstr = uomstr + " " + invUnitOfMeasureConversion.getInvUnitOfMeasure().getUomName();	        		
	        	}
	        	results[ctr] = itemRowEncode(invItem)+ uomstr + "$";	        	
	        	System.out.println(ctr + " " + results[ctr]);
	        	ctr++;	        	
	        }
	        
	        	       
	        System.out.print("results length = "+ results.length);
	        	
	        return results;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    private String itemRowEncode(LocalInvItem invItem) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer encodedResult = new StringBuffer();

    	// Start separator
    	encodedResult.append(separator);
    	
    	// Name / OPOS: Item Code
    	encodedResult.append(invItem.getIiName());
    	encodedResult.append(separator);
    	
    	// Description / OPOS: Item Description
    	encodedResult.append(invItem.getIiDescription());
    	encodedResult.append(separator);
    	
    	// Part Number
    	if (invItem.getIiPartNumber() == null || invItem.getIiPartNumber().length() < 1) {
    		encodedResult.append("none");
    		encodedResult.append(separator);
    	} else {
    		encodedResult.append(invItem.getIiPartNumber());
        	encodedResult.append(separator);
    	}
    	
    	// Description / OPOS: UOM
    	encodedResult.append(invItem.getInvUnitOfMeasure().getUomName());
    	encodedResult.append(separator);
    	
    	// Category 
    	encodedResult.append(invItem.getIiAdLvCategory());
    	encodedResult.append(separator);
    	
    	// End separator
    	//encodedResult.append(separator);
    	
    	return encodedResult.toString();
    	
    }
    
    //  private methods
    
    private InvModPhysicalInventoryDetails physicalInventoryDecode(String STR_PHYSCL_INV) {

    	Debug.print("InvPhysicalInventorySyncControllerBean physicalInventoryDecode");
        
		String separator = "$";
		InvModPhysicalInventoryDetails invModPhysicalInventoryDetails = new InvModPhysicalInventoryDetails();		
		
		// Remove first $ character
		STR_PHYSCL_INV = STR_PHYSCL_INV.substring(1);
		
		// Category
		int start = 0;
		int nextIndex = STR_PHYSCL_INV.indexOf(separator, start);
		int length = nextIndex - start;
		invModPhysicalInventoryDetails.setPiAdLvCategory(STR_PHYSCL_INV.substring(start, start + length));		
				
		// Date
		start = nextIndex + 1;
		nextIndex = STR_PHYSCL_INV.indexOf(separator, start);
		length = nextIndex - start;
		invModPhysicalInventoryDetails.setPiDate(EJBCommon.convertStringToSQLDate(STR_PHYSCL_INV.substring(start, start + length)));
        try {
        	System.out.println("Date- " + invModPhysicalInventoryDetails.getPiDate());
        } catch (Exception ex) {
        	
        	System.out.println(ex.getMessage());
        	
        }
		
		// Location
		start = nextIndex + 1;
		nextIndex = STR_PHYSCL_INV.indexOf(separator, start);
		length = nextIndex - start;
		invModPhysicalInventoryDetails.setPiLocName(STR_PHYSCL_INV.substring(start, start + length));
				
		// Wastage Account
		start = nextIndex + 1;
		nextIndex = STR_PHYSCL_INV.indexOf(separator, start);
		length = nextIndex - start;
		
		// end separator
		start = nextIndex + 1;
		nextIndex = STR_PHYSCL_INV.indexOf(separator, start);
		length = nextIndex - start;		
		
		String lineSeparator = "~";
		
		// begin lineSeparator
		start = nextIndex + 1;
		nextIndex = STR_PHYSCL_INV.indexOf(lineSeparator, start);
		length = nextIndex - start;
    	
    	ArrayList piPilList = new ArrayList();
		
		while (true) {
			
			InvModPhysicalInventoryLineDetails invModPhysicalInventoryLineDetails = new InvModPhysicalInventoryLineDetails();
			
			// begin separator
			start = nextIndex + 1;
			nextIndex = STR_PHYSCL_INV.indexOf(separator, start);
			length = nextIndex - start;
			
			// item name
			start = nextIndex + 1;
			nextIndex = STR_PHYSCL_INV.indexOf(separator, start);
			length = nextIndex - start;	
			invModPhysicalInventoryLineDetails.setPilIlIiName(STR_PHYSCL_INV.substring(start, start + length));
	    	
			// item description
			start = nextIndex + 1;
			nextIndex = STR_PHYSCL_INV.indexOf(separator, start);
			length = nextIndex - start;	
			invModPhysicalInventoryLineDetails.setPilIlIiDescription(STR_PHYSCL_INV.substring(start, start + length));
			
			
			// uom
			start = nextIndex + 1;
			nextIndex = STR_PHYSCL_INV.indexOf(separator, start);
			length = nextIndex - start;	
			invModPhysicalInventoryLineDetails.setPilUomName(STR_PHYSCL_INV.substring(start, start + length));
			
			// Stock On Hand
			start = nextIndex + 1;
			nextIndex = STR_PHYSCL_INV.indexOf(separator, start);
			length = nextIndex - start;	
			double soh = Double.parseDouble(STR_PHYSCL_INV.substring(start, start + length));			
			
			// Ending Inventory
			start = nextIndex + 1;
			nextIndex = STR_PHYSCL_INV.indexOf(separator, start);
			length = nextIndex - start;	
			invModPhysicalInventoryLineDetails.setPilEndingInventory(Double.parseDouble(STR_PHYSCL_INV.substring(start, start + length)));
			
			// Set Variance
			//invModPhysicalInventoryLineDetails.setPilVariance(soh - invModPhysicalInventoryLineDetails.getPilEndingInventory());
			
			// Line Number
			//invModPhysicalInventoryLineDetails//
			
			piPilList.add(invModPhysicalInventoryLineDetails);
			
			// begin lineSeparator
			start = nextIndex + 1;
			nextIndex = STR_PHYSCL_INV.indexOf(lineSeparator, start);
			length = nextIndex - start;
			
			int tempStart = nextIndex + 1;
			if (STR_PHYSCL_INV.indexOf(separator, tempStart) == -1) break;
			
		}
		
		invModPhysicalInventoryDetails.setPiPilList(piPilList);
		
		return invModPhysicalInventoryDetails;
    	
    }
        
    private LocalInvPhysicalInventoryLine addInvPilEntry(InvModPhysicalInventoryLineDetails mdetails, 
            LocalInvPhysicalInventory invPhysicalInventory, String LOC_NM, Integer AD_CMPNY) {
        
        Debug.print("InvPhysicalInventorySyncControllerBean addInvPilEntry");
        
        LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        	          
        // Initialize EJB Home
        
        try {
            
            invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class); 
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
        
        try {
            
            LocalInvPhysicalInventoryLine invPhysicalInventoryLine = invPhysicalInventoryLineHome.create(
                    mdetails.getPilEndingInventory(), mdetails.getPilWastage(), mdetails.getPilVariance(), mdetails.getPilMisc(), AD_CMPNY);
            
            //invPhysicalInventory.addInvPhysicalInventoryLine(invPhysicalInventoryLine);
            invPhysicalInventoryLine.setInvPhysicalInventory(invPhysicalInventory);
            
            LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, mdetails.getPilIlIiName(), AD_CMPNY);
            //invItemLocation.addInvPhysicalInventoryLine(invPhysicalInventoryLine);
            invPhysicalInventoryLine.setInvItemLocation(invItemLocation);
            
            LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(mdetails.getPilUomName(),AD_CMPNY);
            //invUnitOfMeasure.addInvPhysicalInventoryLine(invPhysicalInventoryLine);
            invPhysicalInventoryLine.setInvUnitOfMeasure(invUnitOfMeasure);
            
            return invPhysicalInventoryLine;
            
        } catch (Exception ex) {            
        	
            Debug.printStackTrace(ex);
            //getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
   
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    private ArrayList getInvUnsavedIlByLocNameAndIiAdLvCategoryAndPiCode(String LOC_NM, String II_AD_LV_CTGRY, Integer PI_CODE, Integer AD_BRNCH, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException { 
        
        Debug.print("InvPhysicalInventoryEntryControllerBean getInvUnsavedIlByLocNameAndIiAdLvCategoryAndPiCode");
        
        LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
        LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
            invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class); 
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);   
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {	               	        		        	
            
            Collection invItemLocations = invItemLocationHome.findItemByLocNameAndIiAdLvCategory(LOC_NM, II_AD_LV_CTGRY, AD_CMPNY);
            
            LocalInvPhysicalInventory invPhysicalInventory = invPhysicalInventoryHome.findByPrimaryKey(PI_CODE);
            
            if (invItemLocations.isEmpty()) {
                
                throw new GlobalNoRecordFoundException();
                
            }
            
            Iterator i = invItemLocations.iterator();
            
            while (i.hasNext()) {
                
                LocalInvItemLocation invItemLocation = (LocalInvItemLocation)i.next();
                
                try {
                    
                    LocalInvPhysicalInventoryLine invPhysicalInventoryLine = invPhysicalInventoryLineHome.findByPiCodeAndIlCode(PI_CODE, invItemLocation.getIlCode(), AD_CMPNY);
                    continue;
                    
                } catch (FinderException ex) {
                    
                }
                
                InvModPhysicalInventoryLineDetails mPilDetails = new InvModPhysicalInventoryLineDetails();	
                mPilDetails.setPilEndingInventory(0d);
                mPilDetails.setPilWastage(0d);
                mPilDetails.setPilVariance(0d);
                
                try {
                    
                    LocalInvCosting invCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(invPhysicalInventory.getPiDate().getTime(), 
                            invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                    
                    mPilDetails.setPilRemainingQuantity(invCosting.getCstRemainingQuantity());
                    
                } catch (FinderException ex) {
                    
                    mPilDetails.setPilRemainingQuantity(0d);
                    
                }
                
                mPilDetails.setPilIlIiName(invItemLocation.getInvItem().getIiName());
                mPilDetails.setPilIlIiDescription(invItemLocation.getInvItem().getIiDescription());
                mPilDetails.setPilIlIiUnit(invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName());
                mPilDetails.setPilUomName(invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName());
                
                list.add(mPilDetails);
                
            }
            
            return list;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    public void ejbCreate() throws CreateException {

       Debug.print("InvItemSyncControllerBean ejbCreate");
      
    }
    
    public void ejbRemove() {};

    public void ejbActivate() {}
    public void ejbPassivate() {}

    public void setSessionContext(SessionContext ctx) {}

}