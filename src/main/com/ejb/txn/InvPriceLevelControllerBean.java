/*
 * InvPriceLevelControllerBean.java
 *
 * Created on May 08, 2006, 5:30 PM
 *
 * @author  Neville P. Tagle
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvModItemDetails;
import com.util.InvPriceLevelDetails;

/**
 * @ejb:bean name="InvPriceLevelControllerEJB"
 *           display-name="Used for entering price levels"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvPriceLevelControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvPriceLevelController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvPriceLevelControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
 */

public class InvPriceLevelControllerBean extends AbstractSessionBean {
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvPriceLevelsByIiCode(Integer II_CODE, Integer AD_CMPNY) {
		
		Debug.print("InvPriceLevelControllerBean getInvPriceLevelsByIiCode");
		
		LocalInvPriceLevelHome invPriceLevelHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection invPriceLevels = null;
			
			try {
				
				invPriceLevels = invPriceLevelHome.findByIiCode(II_CODE, AD_CMPNY);      
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			Iterator i = invPriceLevels.iterator();
			
			while (i.hasNext()){
				
				LocalInvPriceLevel invPriceLevel = (LocalInvPriceLevel)i.next(); 
				
				InvPriceLevelDetails details = new InvPriceLevelDetails();
				
				details.setPlAmount(invPriceLevel.getPlAmount());
				details.setPlAdLvPriceLevel(invPriceLevel.getPlAdLvPriceLevel());
				details.setPlMargin(invPriceLevel.getPlMargin());
				details.setPlPercentMarkup(invPriceLevel.getPlPercentMarkup());
				details.setPlShippingCost(invPriceLevel.getPlShippingCost());
				
				list.add(details);
				
			}
						
			return list;              
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	
	public void saveInvPl(ArrayList plList, Integer II_CODE, Integer AD_CMPNY) {
		
		Debug.print("InvPriceLevelControllerBean saveInvPl");
		
		LocalInvItemHome invItemHome = null;
		LocalInvPriceLevelHome invPriceLevelHome= null;
		LocalInvItemLocationHome invItemLocationHome= null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		
		try {
			
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalInvItem invItem = null;
			
			try {
				
				invItem = invItemHome.findByPrimaryKey(II_CODE);
				
			} catch (FinderException ex) {
				
			}

			Iterator i = plList.iterator();
			while (i.hasNext()) {
				
				InvPriceLevelDetails details = (InvPriceLevelDetails)i.next(); 
				
				LocalInvPriceLevel invExistingPriceLevel = null;
				
				try {
					
					invExistingPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(invItem.getIiName(), details.getPlAdLvPriceLevel(), AD_CMPNY);
					
				} catch (FinderException ex) {
					
				}
				
				if (invExistingPriceLevel == null) {
					
					LocalInvPriceLevel invPriceLevel = invPriceLevelHome.create(details.getPlAmount(), details.getPlMargin(), details.getPlPercentMarkup(), details.getPlShippingCost(), details.getPlAdLvPriceLevel(), 'N', AD_CMPNY);
				
					invPriceLevel.setInvItem(invItem);
				
				} else {
					
					// update
					invExistingPriceLevel.setPlAmount(details.getPlAmount());
					invExistingPriceLevel.setPlMargin(details.getPlMargin());
					invExistingPriceLevel.setPlPercentMarkup(details.getPlPercentMarkup());
					invExistingPriceLevel.setPlShippingCost(details.getPlShippingCost());
					invExistingPriceLevel.setPlAdLvPriceLevel(details.getPlAdLvPriceLevel());
					invExistingPriceLevel.setPlAdCompany(AD_CMPNY);
					
					if (invExistingPriceLevel.getPlDownloadStatus()=='N'){ 
						invExistingPriceLevel.setPlDownloadStatus('N');
					}else if (invExistingPriceLevel.getPlDownloadStatus()=='D'){ 
						invExistingPriceLevel.setPlDownloadStatus('X');
					}
					
					invExistingPriceLevel.setInvItem(invItem);
				
				}
				
				
			}
						
			try{
	            LocalInvItemLocation invItemLocation = null;
	            
	            Collection invItemLocations = invItemLocationHome.findByIiName(invItem.getIiName(), AD_CMPNY);
	            
	            Iterator iterIl = invItemLocations.iterator();
	            
	            while(iterIl.hasNext()){
	            	
	            	invItemLocation = (LocalInvItemLocation)iterIl.next();
	            	
	            	Collection adBranchItemLocations = adBranchItemLocationHome.findByInvIlAll(invItemLocation.getIlCode(), AD_CMPNY);
	            	
	            	Iterator iterBil = adBranchItemLocations.iterator();
	            	while(iterBil.hasNext()){
	            		LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)iterBil.next();
	            		
	            		if (adBranchItemLocation.getBilItemDownloadStatus()=='N'){
	            			adBranchItemLocation.setBilItemDownloadStatus('N');
	            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='D'){
	            			adBranchItemLocation.setBilItemDownloadStatus('X');
	            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='U'){
	            			adBranchItemLocation.setBilItemDownloadStatus('U');
	            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='X'){
	            			adBranchItemLocation.setBilItemDownloadStatus('X');
	            		}
	            		
	            	}
	            	
	            }
            }catch (FinderException ex){
            	
            }
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("InvPriceLevelControllerBean getGlFcPrecisionUnit");
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public InvModItemDetails getInvIiMarkupValueByIiCode(Integer II_CODE, Integer AD_CMPNY) {
		
		Debug.print("InvPriceLevelControllerBean getInvIiMarkupValueByIiCode");
		
		LocalInvItemHome invItemHome = null;
		
		// Initialize EJB Home
		
		try {
			
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalInvItem invItem = null;
			
			try {
				
				invItem = invItemHome.findByPrimaryKey(II_CODE);      
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			InvModItemDetails mdetails = new InvModItemDetails();
			
			mdetails.setIiUnitCost(invItem.getIiUnitCost());
			mdetails.setIiShippingCost(invItem.getIiShippingCost());
			mdetails.setIiAveCost(this.getIiAveCost(invItem, new Date(), AD_CMPNY));
			mdetails.setIiPercentMarkup(invItem.getIiPercentMarkup());
			mdetails.setIiSalesPrice(invItem.getIiSalesPrice());
			
			return mdetails;              
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	

	 private double getIiAveCost(LocalInvItem invItem, Date currentDate, Integer AD_CMPNY) {
			
			Debug.print("InvPriceLevelEntryControllerBean getIiMarkupCost");		
			
			LocalInvCostingHome invCostingHome = null;
					
			try {		
				
				invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
				
			} catch (NamingException ex) {
				
				throw new EJBException(ex.getMessage());
				
			}
			
			try {
				
				double TOTAL_COST = 0;
				int ctr = 0;
					
				// get all branch
				
				ArrayList branchList = this.getAdBrAll(AD_CMPNY);
				
				// get all item locations
				
				Collection invItemLocations = invItem.getInvItemLocations();
				
				if (invItemLocations.size() == 0) return invItem.getIiUnitCost();
				
				Iterator ilItr = invItemLocations.iterator();
				
				while (ilItr.hasNext()) {
					
					LocalInvItemLocation invItemLocation = (LocalInvItemLocation)ilItr.next();
				
					// get all item costings
					
					Iterator brItr = branchList.iterator();
					
					while (brItr.hasNext()) {
						
						Integer BR_CODE = (Integer)brItr.next();
						
						double COST = 0;
						
						try {
							
							LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
									currentDate, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), BR_CODE, AD_CMPNY);
							
							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
							
						} catch (FinderException ex) {
							
							//COST = invItemLocation.getInvItem().getIiUnitCost();
							
						}
						
						TOTAL_COST += COST;
						if (COST != 0) ctr++;
																
					}
					
				}
				
				if (ctr != 0)
				    return TOTAL_COST / ctr; else return 0;
								
			} catch (Exception ex) {
				
				Debug.printStackTrace(ex);
				throw new EJBException(ex.getMessage());
				
			}
			
		}
	 
	 private ArrayList getAdBrAll(Integer AD_CMPNY) {
         
        Debug.print("InvPriceLevlControllerBean getAdBrAll");
        
        LocalAdBranchHome adBranchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adBranches = adBranchHome.findBrAll(AD_CMPNY);
        	
        	Iterator i = adBranches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdBranch adBranch = (LocalAdBranch)i.next();
        		
        		list.add(adBranch.getBrCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvPriceLevelControllerBean ejbCreate");
		
	}
}