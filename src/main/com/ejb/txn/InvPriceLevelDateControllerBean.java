/*
 * InvPriceLevelDateControllerBean.java
 *
 * Created on May 08, 2006, 5:30 PM
 *
 * @author  Neville P. Tagle
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelDate;
import com.ejb.inv.LocalInvPriceLevelDateHome;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvModItemDetails;
import com.util.InvModPriceLevelDateDetails;
import com.util.InvPriceLevelDateDetails;
import com.util.InvPriceLevelDetails;

/**
 * @ejb:bean name="InvPriceLevelDateControllerEJB"
 *           display-name="Used for entering price levels date"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvPriceLevelDateControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvPriceLevelDateController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvPriceLevelDateControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
 */

public class InvPriceLevelDateControllerBean extends AbstractSessionBean {
	
	
	 /**
    * @ejb:interface-method view-type="remote"
    **/
    public void saveInvPdEntry(ArrayList list,Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException{

		LocalInvPriceLevelDateHome invPriceLevelDateHome = null;
		LocalInvPriceLevelHome invPriceLevelHome = null;
		LocalInvItemHome invItemHome = null;

       String descName = "";
        
        Debug.print("InvPriceLevelDateControllerBean getInvPriceLevelsByIiCode");
        
        // Initialize EJB Home
       
        try {
            invPriceLevelDateHome = (LocalInvPriceLevelDateHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPriceLevelDateHome.JNDI_NAME, LocalInvPriceLevelDateHome.class);
            
            invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
             invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
           
        }catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }   
           
      
        
        try{
           
           
           
            LocalInvPriceLevel invPriceLevel = null;
            LocalInvPriceLevelDate invPriceLevelDate = null;
            
            
           
            
            
            
            Iterator i = list.iterator();
            
            
            
            
                    
            while(i.hasNext()) {
                
                InvModPriceLevelDateDetails  details = (InvModPriceLevelDateDetails) i.next();
                
                LocalInvItem invItem = invItemHome.findByPrimaryKey(details.getIiCode());
                invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(invItem.getIiName(), details.getPdAdLvPriceLevel(), AD_CMPNY);
            
           
                 
                String lineNumber = details.getPdLine();
                
                descName = details.getPdDesc();

                if(details.getPdCode()!=null){
                    
                    //to update
                    try{
                        invPriceLevelDate = invPriceLevelDateHome.findByPrimaryKey(details.getPdCode());
                    }catch(FinderException ex){
                        System.out.println("Price Level Date Not Exist for updating in Line Number " + lineNumber);
                        throw ex;
                    
                    
                    }
	
					invPriceLevelDate.setPdDesc(details.getPdDesc());
					invPriceLevelDate.setPdAmount(details.getPdAmount());
					invPriceLevelDate.setPdAdLvPriceLevel(details.getPdAdLvPriceLevel());
					invPriceLevelDate.setPdMargin(details.getPdMargin());
					invPriceLevelDate.setPdPercentMarkup(details.getPdPercentMarkup());
					invPriceLevelDate.setPdShippingCost(details.getPdShippingCost());
					invPriceLevelDate.setPdDateFrom(details.getPdDateFrom());
					invPriceLevelDate.setPdDateTo(details.getPdDateTo());
                    
                    invPriceLevelDate.setPdStatus(details.getPdStatus());
              
                       
                
                    
                    
                }else{
                    //to add      
                    
                    invItem = invItemHome.findByPrimaryKey(details.getIiCode());
                    
                    
                    try{
                    	
                    	invPriceLevelDateHome.findPdByPdDescription(details.getPdDesc(), invItem.getIiCode(), AD_CMPNY);
                    
                    
                    	throw new GlobalNoRecordFoundException();
                    }catch(FinderException ex){
                    
                    
                    }
                    
                    
                    
                    
                    
                    invPriceLevelDate = invPriceLevelDateHome.create(details.getPdDesc(), details.getPdAmount(),  details.getPdMargin(), details.getPdPercentMarkup(), details.getPdShippingCost(), details.getPdAdLvPriceLevel(), details.getPdDateFrom(), details.getPdDateTo(), details.getPdStatus(), AD_CMPNY);
                   
                  
                  
                    invPriceLevelDate.setInvItem(invItem);
                  
                   
                  
                    
                    
                }
               
               
               
                if (invPriceLevel.getPlDownloadStatus()=='N'){ 
					invPriceLevel.setPlDownloadStatus('N');
				}else if (invPriceLevel.getPlDownloadStatus()=='D'){ 
					invPriceLevel.setPlDownloadStatus('X');
				}
					
           
            }
           
        }catch(GlobalNoRecordFoundException ex){
              
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new GlobalNoRecordFoundException(descName);
               
                          
        }catch(Exception ex){
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        }

    }
    
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public void saveInvPl(ArrayList plList, Integer II_CODE, Integer AD_CMPNY) {
		
		Debug.print("InvPriceLevelDateControllerBean saveInvPl");
		
		LocalInvItemHome invItemHome = null;
		LocalInvPriceLevelHome invPriceLevelHome= null;
		LocalInvItemLocationHome invItemLocationHome= null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		
		try {
			
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalInvItem invItem = null;
			
			try {
				
				invItem = invItemHome.findByPrimaryKey(II_CODE);
				
			} catch (FinderException ex) {
				
			}

			Iterator i = plList.iterator();
			while (i.hasNext()) {
				
				InvPriceLevelDetails details = (InvPriceLevelDetails)i.next(); 
				
				LocalInvPriceLevel invExistingPriceLevel = null;
				
				try {
					
					invExistingPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(invItem.getIiName(), details.getPlAdLvPriceLevel(), AD_CMPNY);
					
				} catch (FinderException ex) {
					
				}
				
				if (invExistingPriceLevel == null) {
					
					LocalInvPriceLevel invPriceLevel = invPriceLevelHome.create(details.getPlAmount(), details.getPlMargin(), details.getPlPercentMarkup(), details.getPlShippingCost(), details.getPlAdLvPriceLevel(), 'N', AD_CMPNY);
				
					invPriceLevel.setInvItem(invItem);
				
				} else {
					
					// update
					invExistingPriceLevel.setPlAmount(details.getPlAmount());
					invExistingPriceLevel.setPlMargin(details.getPlMargin());
					invExistingPriceLevel.setPlPercentMarkup(details.getPlPercentMarkup());
					invExistingPriceLevel.setPlShippingCost(details.getPlShippingCost());
					invExistingPriceLevel.setPlAdLvPriceLevel(details.getPlAdLvPriceLevel());
					invExistingPriceLevel.setPlAdCompany(AD_CMPNY);
					
					if (invExistingPriceLevel.getPlDownloadStatus()=='N'){ 
						invExistingPriceLevel.setPlDownloadStatus('N');
					}else if (invExistingPriceLevel.getPlDownloadStatus()=='D'){ 
						invExistingPriceLevel.setPlDownloadStatus('X');
					}
					
					invExistingPriceLevel.setInvItem(invItem);
				
				}
				
				
			}
						
			try{
	            LocalInvItemLocation invItemLocation = null;
	            
	            Collection invItemLocations = invItemLocationHome.findByIiName(invItem.getIiName(), AD_CMPNY);
	            
	            Iterator iterIl = invItemLocations.iterator();
	            
	            while(iterIl.hasNext()){
	            	
	            	invItemLocation = (LocalInvItemLocation)iterIl.next();
	            	
	            	Collection adBranchItemLocations = adBranchItemLocationHome.findByInvIlAll(invItemLocation.getIlCode(), AD_CMPNY);
	            	
	            	Iterator iterBil = adBranchItemLocations.iterator();
	            	while(iterBil.hasNext()){
	            		LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)iterBil.next();
	            		
	            		if (adBranchItemLocation.getBilItemDownloadStatus()=='N'){
	            			adBranchItemLocation.setBilItemDownloadStatus('N');
	            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='D'){
	            			adBranchItemLocation.setBilItemDownloadStatus('X');
	            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='U'){
	            			adBranchItemLocation.setBilItemDownloadStatus('U');
	            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='X'){
	            			adBranchItemLocation.setBilItemDownloadStatus('X');
	            		}
	            		
	            	}
	            	
	            }
            }catch (FinderException ex){
            	
            }
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvAdPriceLevels(Integer AD_CMPNY) {
	
		Debug.print("InvPriceLevelDateControllerBean getInvAdPriceLevel");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
	
	
	
		try{
		
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV PRICE LEVEL", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while(i.hasNext()){
			
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				
				
				list.add(adLookUpValue.getLvName());
			
			
			}
			
			return list;
			
		
		
		}catch(Exception ex){
		
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	
	
	
	}
	
	
	
	
	
	
	
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvPriceLevelsDateByIiCode(String PD_STATUS, Integer II_CODE, String INV_AD_PRC_LVL, Integer AD_CMPNY) {
		
		Debug.print("InvPriceLevelDateControllerBean getInvPriceLevelsByIiCode");
		
		LocalInvPriceLevelDateHome invPriceLevelDateHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invPriceLevelDateHome = (LocalInvPriceLevelDateHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPriceLevelDateHome.JNDI_NAME, LocalInvPriceLevelDateHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection invPriceLevels = null;
			
			try {
													  
				invPriceLevels = invPriceLevelDateHome.findPdByIiCodeAndPdPriceLevel(PD_STATUS, II_CODE, INV_AD_PRC_LVL, AD_CMPNY);      
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			Iterator i = invPriceLevels.iterator();
			
			while (i.hasNext()){
				
				LocalInvPriceLevelDate invPriceLevelDate = (LocalInvPriceLevelDate)i.next(); 
				
				InvPriceLevelDateDetails details = new InvPriceLevelDateDetails();
				
				
				details.setPdCode(invPriceLevelDate.getPdCode());
				details.setPdDesc(invPriceLevelDate.getPdDesc());
				details.setPdAmount(invPriceLevelDate.getPdAmount());
				details.setPdAdLvPriceLevel(invPriceLevelDate.getPdAdLvPriceLevel());
				details.setPdMargin(invPriceLevelDate.getPdMargin());
				details.setPdPercentMarkup(invPriceLevelDate.getPdPercentMarkup());
				details.setPdShippingCost(invPriceLevelDate.getPdShippingCost());
				details.setPdDateFrom(invPriceLevelDate.getPdDateFrom());
				details.setPdDateTo(invPriceLevelDate.getPdDateTo());
				details.setPdStatus(invPriceLevelDate.getPdStatus());
				
				list.add(details);
				
			}
						
			return list;              
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public InvPriceLevelDateDetails findPriceLevelDateByDescription(String PD_DESC, Integer II_CODE, Integer AD_CMPNY)
	 throws GlobalNoRecordFoundException {
	
		Debug.print("InvPriceLevelDateControllerBean findPriceLevelDateByDescription");
		
		LocalInvPriceLevelDateHome invPriceLevelDateHome = null;
		LocalInvPriceLevelDate invPriceLevelDate = null;
		
		try {
			
			invPriceLevelDateHome = (LocalInvPriceLevelDateHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPriceLevelDateHome.JNDI_NAME, LocalInvPriceLevelDateHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		
		try {
		
			
			try {
				
				invPriceLevelDate = invPriceLevelDateHome.findPdByPdDescription(PD_DESC, II_CODE, AD_CMPNY);      
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			
				
			InvPriceLevelDateDetails details = new InvPriceLevelDateDetails();
				
				
			details.setPdCode(invPriceLevelDate.getPdCode());
			details.setPdDesc(invPriceLevelDate.getPdDesc());
			details.setPdAmount(invPriceLevelDate.getPdAmount());
			details.setPdAdLvPriceLevel(invPriceLevelDate.getPdAdLvPriceLevel());
			details.setPdMargin(invPriceLevelDate.getPdMargin());
			details.setPdPercentMarkup(invPriceLevelDate.getPdPercentMarkup());
			details.setPdShippingCost(invPriceLevelDate.getPdShippingCost());
			details.setPdDateFrom(invPriceLevelDate.getPdDateFrom());
			details.setPdDateTo(invPriceLevelDate.getPdDateTo());
			details.setPdStatus(invPriceLevelDate.getPdStatus());
	
						
			return details;      
	
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}

	
	
	}
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvPriceLevelsDateByIiCode(Integer II_CODE, Integer AD_CMPNY) {
		
		Debug.print("InvPriceLevelDateControllerBean getInvPriceLevelsByIiCode");
		
		LocalInvPriceLevelDateHome invPriceLevelDateHome = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invPriceLevelDateHome = (LocalInvPriceLevelDateHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPriceLevelDateHome.JNDI_NAME, LocalInvPriceLevelDateHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection invPriceLevels = null;
			
			try {
				
				invPriceLevels = invPriceLevelDateHome.findByIiCode(II_CODE, AD_CMPNY);      
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			Iterator i = invPriceLevels.iterator();
			
			while (i.hasNext()){
				
				LocalInvPriceLevelDate invPriceLevelDate = (LocalInvPriceLevelDate)i.next(); 
				
				InvPriceLevelDateDetails details = new InvPriceLevelDateDetails();
				
				
				details.setPdCode(invPriceLevelDate.getPdCode());
				details.setPdDesc(invPriceLevelDate.getPdDesc());
				details.setPdAmount(invPriceLevelDate.getPdAmount());
				details.setPdAdLvPriceLevel(invPriceLevelDate.getPdAdLvPriceLevel());
				details.setPdMargin(invPriceLevelDate.getPdMargin());
				details.setPdPercentMarkup(invPriceLevelDate.getPdPercentMarkup());
				details.setPdShippingCost(invPriceLevelDate.getPdShippingCost());
				details.setPdDateFrom(invPriceLevelDate.getPdDateFrom());
				details.setPdDateTo(invPriceLevelDate.getPdDateTo());
				details.setPdStatus(invPriceLevelDate.getPdStatus());
				list.add(details);
				
			}
						
			return list;              
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	
	 /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteInvPd(int PD_CODE) 
        throws GlobalRecordAlreadyDeletedException{

        LocalInvPriceLevelDateHome invPriceLevelDateHome = null;
        
        
        Debug.print("InvPriceLevelDateControllerBean deleteInvPd");
        
        // Initialize EJB Home
       
        try {
       
           invPriceLevelDateHome = (LocalInvPriceLevelDateHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPriceLevelDateHome.JNDI_NAME, LocalInvPriceLevelDateHome.class);

                      
        }catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }   
            
        try{
            
            
            LocalInvPriceLevelDate invPriceLevelDate = null;
            invPriceLevelDate = invPriceLevelDateHome.findByPrimaryKey(PD_CODE);

       
            if(invPriceLevelDate==null){
                throw new FinderException();
            }else{
                invPriceLevelDate.remove();
            }
        
        }catch(FinderException ex){
          
            throw new GlobalRecordAlreadyDeletedException();
                   
        }catch(Exception ex){
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
        }

    }
    
    
	
	
	
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("InvPriceLevelDateControllerBean getGlFcPrecisionUnit");
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public InvModItemDetails getInvIiMarkupValueByIiCode(Integer II_CODE, Integer AD_CMPNY) {
		
		Debug.print("InvPriceLevelDateControllerBean getInvIiMarkupValueByIiCode");
		
		LocalInvItemHome invItemHome = null;
		
		// Initialize EJB Home
		
		try {
			
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalInvItem invItem = null;
			
			try {
				
				invItem = invItemHome.findByPrimaryKey(II_CODE);      
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			InvModItemDetails mdetails = new InvModItemDetails();
			mdetails.setIiCode(invItem.getIiCode());
			mdetails.setIiName(invItem.getIiName());
			mdetails.setIiDescription(invItem.getIiDescription());
			mdetails.setIiUnitCost(invItem.getIiUnitCost());
			mdetails.setIiShippingCost(invItem.getIiShippingCost());
			mdetails.setIiAveCost(this.getIiAveCost(invItem, new Date(), AD_CMPNY));
			mdetails.setIiPercentMarkup(invItem.getIiPercentMarkup());
			mdetails.setIiSalesPrice(invItem.getIiSalesPrice());
			
			return mdetails;              
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	

	 private double getIiAveCost(LocalInvItem invItem, Date currentDate, Integer AD_CMPNY) {
			
			Debug.print("InvPriceLevelEntryControllerBean getIiMarkupCost");		
			
			LocalInvCostingHome invCostingHome = null;
					
			try {		
				
				invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
				
			} catch (NamingException ex) {
				
				throw new EJBException(ex.getMessage());
				
			}
			
			try {
				
				double TOTAL_COST = 0;
				int ctr = 0;
					
				// get all branch
				
				ArrayList branchList = this.getAdBrAll(AD_CMPNY);
				
				// get all item locations
				
				Collection invItemLocations = invItem.getInvItemLocations();
				
				if (invItemLocations.size() == 0) return invItem.getIiUnitCost();
				
				Iterator ilItr = invItemLocations.iterator();
				
				while (ilItr.hasNext()) {
					
					LocalInvItemLocation invItemLocation = (LocalInvItemLocation)ilItr.next();
				
					// get all item costings
					
					Iterator brItr = branchList.iterator();
					
					while (brItr.hasNext()) {
						
						Integer BR_CODE = (Integer)brItr.next();
						
						double COST = 0;
						
						try {
							
							LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
									currentDate, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), BR_CODE, AD_CMPNY);
							
							COST = Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
							
						} catch (FinderException ex) {
							
							//COST = invItemLocation.getInvItem().getIiUnitCost();
							
						}
						
						TOTAL_COST += COST;
						if (COST != 0) ctr++;
																
					}
					
				}
				
				if (ctr != 0)
				    return TOTAL_COST / ctr; else return 0;
								
			} catch (Exception ex) {
				
				Debug.printStackTrace(ex);
				throw new EJBException(ex.getMessage());
				
			}
			
		}
	 
	 private ArrayList getAdBrAll(Integer AD_CMPNY) {
         
        Debug.print("InvPriceLevlDateControllerBean getAdBrAll");
        
        LocalAdBranchHome adBranchHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Collection adBranches = adBranchHome.findBrAll(AD_CMPNY);
        	
        	Iterator i = adBranches.iterator();
        	
        	while (i.hasNext()) {
        		
        		LocalAdBranch adBranch = (LocalAdBranch)i.next();
        		
        		list.add(adBranch.getBrCode());
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvPriceLevelDateControllerBean ejbCreate");
		
	}
}