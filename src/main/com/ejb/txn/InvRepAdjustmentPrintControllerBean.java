
/*
 * InvRepAdjustmentEntryPrintControllerBean.java
 *
 * Created on September 19,2006 2:58 PM
 *
 * @author  Rey B. Limosenero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArRepCustomerListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvModAdjustmentLineDetails;
import com.util.InvRepAdjustmentPrintDetails;

/**
 * @ejb:bean name="InvRepAdjustmentPrintControllerEJB"
 *           display-name="Use for printing adjustment"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepAdjustmentPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepAdjustmentPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepAdjustmentPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class InvRepAdjustmentPrintControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeInvRepAdjustmentPrint(ArrayList adjCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvRepAdjustmentPrintControllerBean executeInvRepAdjustmentPrint");
        LocalGenValueSetValueHome gvsv=null;
        LocalInvAdjustmentHome invAdjustmentHome = null; 
        LocalInvCostingHome invCostingHome = null;
        LocalAdUserHome adUserHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvTagHome invTagHome = null;
        System.out.print("Actual PPPP");
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
        	gvsv = (LocalGenValueSetValueHome)EJBHomeFactory.
          	lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValue.class);

            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome .JNDI_NAME, LocalAdUserHome .class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	invTagHome = (LocalInvTagHome)EJBHomeFactory.
    				lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = adjCodeList.iterator();
        	
        	while (i.hasNext()) {
        		
        		Integer ADJ_CODE = (Integer) i.next();
        	
        		LocalInvAdjustment invAdjustment = null;	        	
        		
        		try {
        			
        		    invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
        		    
        		} catch (FinderException ex) {
        			
        			continue;
        			
        		}	        	  	         
        		
        		// get adjustment lines 
        		
        		
        	      		
        		
        		
        		Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
        		
        		Iterator adjIter = invAdjustmentLines.iterator();
        		
        		while(adjIter.hasNext()) {
        			double COST = 0d;
        			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)adjIter.next();
        			
        			boolean isService = invAdjustmentLine.getInvItemLocation().getInvItem().getIiAdLvCategory().startsWith("Service");
        			if (!invAdjustmentLine.getInvTags().isEmpty() && invAdjustment.getAdjType().equals("GENERAL")&&
        					invAdjustmentLine.getInvItemLocation().getInvItem().getIiTraceMisc() == 1){
        				
        				Collection invTags = invAdjustmentLine.getInvTags();
    					Iterator tags =  invTags.iterator();
    					while (tags.hasNext()){
    						InvRepAdjustmentPrintDetails details = new InvRepAdjustmentPrintDetails();
    						
    						if (isService){
                				System.out.println(isService + " <== service ba?");
                				details.setApAlIsService(true);
                				System.out.println(details.getApAlIsService() + " <== details.getApAlIsService");
                			}
                			else {
                				System.out.println(isService + " <== o hindi?");
                				details.setApAlIsService(false);
                				System.out.println(details.getApAlIsService() + " <== details.getApAlIsService");
                			}
    						
    						if (invAdjustmentLine.getInvItemLocation().getInvItem().getIiNonInventoriable()==1){
                				details.setApAlIsInventoriable(false);
                			}else{
                				details.setApAlIsInventoriable(true);
                			}
    						details.setApAdjType(invAdjustment.getAdjType());
                			details.setApAdjDate(invAdjustment.getAdjDate());
                			details.setApAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
                			details.setApAdjReferenceNumber(invAdjustment.getAdjReferenceNumber());
                			details.setApAdjDescription(invAdjustment.getAdjDescription());
                			details.setApAdjGlCoaAccount(invAdjustment.getGlChartOfAccount().getCoaAccountNumber());
                			/*String x = invAdjustment.getGlChartOfAccount().getCoaAccountDescription();
                			
                			if (x.contains("-")){
                				details.setApAdjGlCoaAccountDesc(x.substring( 0, x.indexOf( '-' ) ));
                			}else{
                				details.setApAdjGlCoaAccountDesc(x);
                			}
                			*/
                			
                			Collection BrList = gvsv.findByVsName("BRANCH", AD_CMPNY);                			              	
            		 		String sample = invAdjustment.getGlChartOfAccount().getCoaAccountDescription();
            		 		
            		 		 Iterator ic = BrList.iterator();
            		 		while (ic.hasNext()) {
            		 			LocalGenValueSetValue glvsv = (LocalGenValueSetValue)ic.next();
            		 			if(sample.contains(glvsv.getVsvDescription()+"-"))
            		 					{
            		 				System.out.println(glvsv.getVsvDescription());
            		 				sample=glvsv.getVsvDescription();
            		 					break;            		 					
            		 					}
            		 		}

            		 		details.setApAdjGlCoaAccountDesc(sample);
            		 		try{
                    			details.setApAdjSupplierName(invAdjustment.getApSupplier().getSplName());
                    			details.setApAdjSupplierAddress(invAdjustment.getApSupplier().getSplAddress());
                    			} catch (Exception ex) {
                    				details.setApAdjSupplierName("");
                        			details.setApAdjSupplierAddress("");
                        			
                    			}
                            details.setApAlBranchCode(invAdjustment.getAdjAdBranch());
                            details.setApAdjPostedBy(invAdjustment.getAdjPostedBy());
                          // details.setApAlBranchName(invAdjustment.getAdjAdBranch().getBrName());
                            details.setApAdjApprovedBy(invAdjustment.getAdjApprovedRejectedBy());
                            
                            LocalAdUser adUser = adUserHome.findByUsrName(invAdjustment.getAdjCreatedBy(), AD_CMPNY);
                			details.setApAdjCreatedBy(adUser.getUsrDescription());
                			details.setApAdjCreatedByPosition(adUser.getUsrPosition());
                			details.setApAlIiName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
                			details.setApAlIiDescription(invAdjustmentLine.getInvItemLocation().getInvItem().getIiDescription());
                			details.setApAlLocName(invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName());
                			details.setApAlUomName(invAdjustmentLine.getInvUnitOfMeasure().getUomName());
                			details.setApAlAdjustQuantity(invAdjustmentLine.getAlAdjustQuantity());
                			details.setApAlUnitCost(invAdjustmentLine.getAlUnitCost());
                			details.setApAdjNotedBy(invAdjustment.getAdjNotedBy());
                			LocalInvCosting invLastCosting = null;
                			
                			try{
                    			invLastCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIlCode(invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getIlCode(), invAdjustmentLine.getInvAdjustment().getAdjAdBranch(), AD_CMPNY);
                    			COST = Math.abs(invLastCosting.getCstRemainingValue() / invLastCosting.getCstRemainingQuantity());
                			}catch(Exception e){
                				COST =0;
                			}
                			
                			
                			//details.setApAlAveCost(COST);
                			details.setApAlAveCost(invAdjustmentLine.getAlUnitCost());
                			
                			LocalInvTag invTag = (LocalInvTag)tags.next();
    						System.out.println(invTag.getTgDocumentNumber() + "<== tg document number");
    						try {
    							details.setApTgCustodian(invTag.getAdUser().getUsrDescription());
    							details.setApTgCustodianPosition(invTag.getAdUser().getUsrPosition());
    						}catch(Exception ex){
    							details.setApTgCustodian("");
    							details.setApTgCustodianPosition("");
    						}
    						
    						details.setApTgDocumentNumber(invTag.getTgDocumentNumber());
    						details.setApTgExpiryDate(invTag.getTgExpiryDate());
    						details.setApTgPropertyCode(invTag.getTgPropertyCode());
    						details.setApTgSerialNumber(invTag.getTgSerialNumber());
    						details.setApTgSpecs(invTag.getTgSpecs());
    						list.add(details);
    					}
        			}else{
        				InvRepAdjustmentPrintDetails details = new InvRepAdjustmentPrintDetails();
        				details.setApAdjType(invAdjustment.getAdjType());
            			details.setApAdjDate(invAdjustment.getAdjDate());
            			details.setApAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
            			details.setApAdjReferenceNumber(invAdjustment.getAdjReferenceNumber());
            			details.setApAdjDescription(invAdjustment.getAdjDescription());
            			details.setApAdjGlCoaAccount(invAdjustment.getGlChartOfAccount().getCoaAccountNumber());
            			//String x = invAdjustment.getGlChartOfAccount().getCoaAccountDescription();
            			
            			Collection BrList = gvsv.findByVsName("BRANCH", AD_CMPNY);
            			          			
        		 		String sample = invAdjustment.getGlChartOfAccount().getCoaAccountDescription();
        		 		
        		 		 Iterator ic = BrList.iterator();
        		 		while (ic.hasNext()) {
        		 			LocalGenValueSetValue glvsv = (LocalGenValueSetValue)ic.next();
        		 			if(sample.contains(glvsv.getVsvDescription()+"-"))
        		 					{
        		 				System.out.println(glvsv.getVsvDescription());
        		 				sample=glvsv.getVsvDescription();
        		 					break;       		 					
        		 					}
        		 		}
            			
            			/*
            			
            			if (x.contains("-")){
            				details.setApAdjGlCoaAccountDesc(x.substring( 0, x.indexOf( '-' ) ));
            			}else{
            				details.setApAdjGlCoaAccountDesc(x);
            			}
            			*/
        		 		details.setApAdjGlCoaAccountDesc(sample);
            			details.setApAlBranchCode(invAdjustment.getAdjAdBranch());
            			try{
            			details.setApAdjSupplierName(invAdjustment.getApSupplier().getSplName());
            			details.setApAdjSupplierAddress(invAdjustment.getApSupplier().getSplAddress());
            			} catch (Exception ex) {
            				details.setApAdjSupplierName("");
                			details.setApAdjSupplierAddress("");
                			
            			}
                        details.setApAdjPostedBy(invAdjustment.getAdjPostedBy());
                      // details.setApAlBranchName(invAdjustment.getAdjAdBranch().getBrName());
                        details.setApAdjApprovedBy(invAdjustment.getAdjApprovedRejectedBy()); 
                        LocalAdUser adUser = adUserHome.findByUsrName(invAdjustment.getAdjCreatedBy(), AD_CMPNY);
            			details.setApAdjCreatedBy(adUser.getUsrDescription());
            			details.setApAdjCreatedByPosition(adUser.getUsrPosition());
            			details.setApAlIiName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
            			details.setApAlIiDescription(invAdjustmentLine.getInvItemLocation().getInvItem().getIiDescription());
            			details.setApAlLocName(invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName());
            			details.setApAlUomName(invAdjustmentLine.getInvUnitOfMeasure().getUomName());
            			details.setApAlAdjustQuantity(invAdjustmentLine.getAlAdjustQuantity());
            			details.setApAlUnitCost(invAdjustmentLine.getAlUnitCost());
            			details.setApAdjNotedBy(invAdjustment.getAdjNotedBy());
            			
            			LocalInvCosting invLastCosting = null;
            			
            			try{
                			invLastCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIlCode(invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getIlCode(), invAdjustmentLine.getInvAdjustment().getAdjAdBranch(), AD_CMPNY);
                			COST = Math.abs(invLastCosting.getCstRemainingValue() / invLastCosting.getCstRemainingQuantity());
            			}catch(Exception e){
            				COST =0;
            			}
            			
            			
            			//details.setApAlAveCost(COST);
            			details.setApAlAveCost(invAdjustmentLine.getAlUnitCost());
            			
            			list.add(details);
        			}
        			
        		
        		}
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}    
        	
        	Collections.sort(list, InvRepAdjustmentPrintDetails.ItemComparator);
        	
        	return list;        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public boolean getInvTraceMisc(String II_NAME, Integer AD_CMPNY) {
                    
        Debug.print("InvAdjustmentEntryControllerBean getInvLocAll");
        
        LocalInvLocationHome invLocationHome = null;
        LocalInvItemHome invItemHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        boolean isTraceMisc = false;
        // Initialize EJB Home
        
        try {
            
        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
	               
            LocalInvItem invItem = invItemHome.findByIiName(II_NAME, AD_CMPNY);
            if (invItem.getIiTraceMisc() == 1){
            	isTraceMisc = true;
            }
            return isTraceMisc;
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
            
    }
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepAdjustmentPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
		
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvRepAdjustmentPrintControllerBean ejbCreate");
      
    }
}
