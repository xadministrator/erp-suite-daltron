package com.ejb.txn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.gl.LocalGlFunctionalCurrencyRate;
import com.ejb.gl.LocalGlFunctionalCurrencyRateHome;
import com.ejb.gl.LocalGlRecurringJournalHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvCosting;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepAdjustmentRegisterBalanceSheetDetails;
import com.util.InvRepAdjustmentRegisterDetails;
import com.util.InvRepAdjustmentRegisterPostedDeliveriesDetails;
import com.util.InvRepAdjustmentRegisterRmUsedDetails;
import com.util.InvRepAdjustmentRegisterRmVarDetails;
import com.util.InvRepAdjustmentRegisterProdListDetails;
import com.util.InvRepAdjustmentRegisterSummaryOfIssuanceDetails;
import com.util.InvRepItemListDetails;


/**
 * @ejb:bean name="InvRepAdjustmentRegisterControllerEJB"
 *           display-name="Used for finding inv adjustment"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepAdjustmentRegisterControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepAdjustmentRegisterController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepAdjustmentRegisterControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvRepAdjustmentRegisterControllerBean extends AbstractSessionBean {
	
	
	Double forwardedBal =0d;
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	
	
	
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
		
		Debug.print("InvRepAdjustmentRegisterControllerBean getAdCompany");      
		
		LocalAdCompanyHome adCompanyHome = null;
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			AdCompanyDetails details = new AdCompanyDetails();
			details.setCmpName(adCompany.getCmpName());
			
			return details;  	
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}  
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException{
		
		Debug.print("InvRepAdjustmentRegisterControllerBean getAdBrResAll");
		
		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;
		
		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;
		
		Collection adBranchResponsibilities = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
			
		} catch (FinderException ex) {
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		if (adBranchResponsibilities.isEmpty()) {
			
			throw new GlobalNoRecordFoundException();
			
		}
		
		try {
			
			Iterator i = adBranchResponsibilities.iterator();
			
			while(i.hasNext()) {
				
				adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
				
				adBranch = adBranchResponsibility.getAdBranch();
				
				AdBranchDetails details = new AdBranchDetails();
				
				details.setBrCode(adBranch.getBrCode());
				details.setBrBranchCode(adBranch.getBrBranchCode());
				details.setBrName(adBranch.getBrName());
				details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
				
				list.add(details);
				
			}	               
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
		}
		
		return list;
		
	}    
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("ArInvoiceEntryControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @throws FinderException 
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public Double getforwardedBal()
	{
		return forwardedBal;
	}
	
	/**
	 * @throws FinderException 
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeInvRepAdjustmentRegister(HashMap criteria, String ORDER_BY, String GROUP_BY, boolean SHOW_LN_ITEMS,String DateFrom, String DateTo, ArrayList adBrnchList, String REPORT_TYPE, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException, FinderException {
		
		Debug.print("InvRepAdjustmentRegisterControllerBean executeInvRepAdjustmentRegister");
		
		LocalInvAdjustmentHome invAdjustmentHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalInvDistributionRecordHome invDisHome =null;
		LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvLocationHome invLocationHome = null;
        
        LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGenValueSetValueHome gvsv=null;
		ArrayList list = new ArrayList();
		ArrayList listAdj = new ArrayList();
		//initialized EJB Home
		
		try {
			
			  gvsv = (LocalGenValueSetValueHome)EJBHomeFactory.
			          	lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValue.class);

			invDisHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
			invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		    invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		System.out.println("REPORT_TYPE" + REPORT_TYPE);
		
		if(REPORT_TYPE.equals("RM_USED")){
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			  Collection invItems = invItemHome.findStockIiAll(AD_CMPNY);	         

			  if (invItems.size() == 0)
	                throw new GlobalNoRecordFoundException();
	            Date enteredDate = (Date)criteria.get("dateTo");
	            Iterator i = invItems.iterator();
	            
	            while (i.hasNext()) {

	            	LocalInvItem invItem = (LocalInvItem)i.next();
	               
	                InvRepAdjustmentRegisterRmUsedDetails details = new InvRepAdjustmentRegisterRmUsedDetails();
	                details.setIlIiName(invItem.getIiName());
	                details.setIlIiDescription(invItem.getIiDescription());
	                details.setIlIiUomName(invItem.getInvUnitOfMeasure().getUomShortName());
		            for (int loop=1; loop <= 12; loop++) {
		                // get date range for month 12
		                GregorianCalendar gcCurrDate = new GregorianCalendar();
			        	 gcCurrDate.setTime(enteredDate);
			        	 gcCurrDate.add(Calendar.MONTH, (loop - 1) * -1);
			        	 
			        	 // date from
			        	 GregorianCalendar gcDateFrom = new GregorianCalendar();
			        	 gcDateFrom.set(gcCurrDate.get(Calendar.YEAR), gcCurrDate.get(Calendar.MONTH), 0, 0, 0, 0);
			        	 gcDateFrom.set(Calendar.MILLISECOND, 0);
			        	 criteria.put("dateFrom", gcDateFrom.getTime());
			        	 //date to
			        	 GregorianCalendar gcDateTo = new GregorianCalendar();
			        	 gcDateTo.set(gcCurrDate.get(Calendar.YEAR), gcCurrDate.get(Calendar.MONTH), gcCurrDate.getMaximum(Calendar.DATE), 0, 0, 0);
			        	 gcDateTo.set(Calendar.MILLISECOND, 0);
			        	 criteria.put("dateTo", gcDateTo.getTime());
			        	 
		                double qty = 0d;
		                criteria.put("type", "ISSUANCE");
		                criteria.put("itemName", invItem.getIiName());
						Collection invAdjustments = this.getAdjByCriteria(criteria, ORDER_BY, adBrnchList, AD_CMPNY);
						Iterator adjIter = invAdjustments.iterator();
						while (adjIter.hasNext()) {
							LocalInvAdjustment invAdjustment = (LocalInvAdjustment)adjIter.next();
							Iterator alItr = invAdjustment.getInvAdjustmentLines().iterator();
							while (alItr.hasNext()) {
								
								LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)alItr.next();
								//System.out.println(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName()+ "==" +invItem.getIiName());
								if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName().equals(invItem.getIiName())){
									qty += invAdjustmentLine.getAlAdjustQuantity();
								}
								
							} 
						}
						qty = Math.abs(qty);
		                switch(loop) {
		                case 12:
			                details.setIlIiMonth1(qty);

			                break;
		                case 11:
			                details.setIlIiMonth2(qty);
			                break;
		                case 10:
			                details.setIlIiMonth3(qty);
			                break;
		                case 9:
			                details.setIlIiMonth4(qty);
			                break;
		                case 8:
			                details.setIlIiMonth5(qty);
			                break;
		                case 7:
			                details.setIlIiMonth6(qty);
			                break;
		                case 6:
			                details.setIlIiMonth7(qty);
			                break;
		                case 5:
			                details.setIlIiMonth8(qty);
			                break;
		                case 4:
			                details.setIlIiMonth9(qty);
			                break;
		                case 3:
			                details.setIlIiMonth10(qty);
			                break;
		                case 2:
			                details.setIlIiMonth11(qty);
			                break;
		                case 1:
			                details.setIlIiMonth12(qty);
			                break;
		                }
	                }
	                
	                list.add(details);
	                
	                
	            }
	            
	            if (list.size() == 0)
	                throw new GlobalNoRecordFoundException();
	            
	            return list;
		}else if(REPORT_TYPE.equals("PROD_LIST")){
			System.out.println("PROD_LIST");
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			  Collection invItems = invItemHome.findAssemblyIiAll(AD_CMPNY);	         
	            Date enteredDate = (Date)criteria.get("dateTo");
	            if (invItems.size() == 0)
	                throw new GlobalNoRecordFoundException();
	            
	            Iterator i = invItems.iterator();

	             
	            while (i.hasNext()) {
	            	
	                LocalInvItem invItem = (LocalInvItem)i.next();
	                
	                InvRepAdjustmentRegisterProdListDetails details = new InvRepAdjustmentRegisterProdListDetails();
	                details.setIlIiName(invItem.getIiName());
	                details.setIlIiDescription(invItem.getIiDescription());
	                details.setIlIiYield(invItem.getIiYield());
	                details.setIlIiUomName(invItem.getInvUnitOfMeasure().getUomShortName());
	                
	                for (int loop=1; loop <= 12; loop++) {
		                // get date range for month 12
		                GregorianCalendar gcCurrDate = new GregorianCalendar();
			        	 gcCurrDate.setTime(enteredDate);
			        	 gcCurrDate.add(Calendar.MONTH, (loop - 1) * -1);
			        	 
			        	 // date from
			        	 GregorianCalendar gcDateFrom = new GregorianCalendar();
			        	 gcDateFrom.set(gcCurrDate.get(Calendar.YEAR), gcCurrDate.get(Calendar.MONTH), 0, 0, 0, 0);
			        	 gcDateFrom.set(Calendar.MILLISECOND, 0);
			        	 criteria.put("dateFrom", gcDateFrom.getTime());
			        	 //date to
			        	 GregorianCalendar gcDateTo = new GregorianCalendar();
			        	 gcDateTo.set(gcCurrDate.get(Calendar.YEAR), gcCurrDate.get(Calendar.MONTH), gcCurrDate.getMaximum(Calendar.DATE), 0, 0, 0);
			        	 gcDateTo.set(Calendar.MILLISECOND, 0);
			        	 criteria.put("dateTo", gcDateTo.getTime());
			        	 
		                double qty = 0d;
		                criteria.put("type", "PRODUCTION");
		                criteria.put("itemName", invItem.getIiName());
						Collection invAdjustments = this.getAdjByCriteria(criteria, ORDER_BY, adBrnchList, AD_CMPNY);
						Iterator adjIter = invAdjustments.iterator();
						while (adjIter.hasNext()) {
							LocalInvAdjustment invAdjustment = (LocalInvAdjustment)adjIter.next();
							Iterator alItr = invAdjustment.getInvAdjustmentLines().iterator();
							while (alItr.hasNext()) {
								
								LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)alItr.next();
								//System.out.println(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName()+ "==" +invItem.getIiName());
								if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName().equals(invItem.getIiName())){
									qty += invAdjustmentLine.getAlAdjustQuantity();
								}
								
							} 
						}
						qty = Math.abs(qty);
		                switch(loop) {
		                case 12:
			                details.setIlIiMonth1(qty);

			                break;
		                case 11:
			                details.setIlIiMonth2(qty);
			                break;
		                case 10:
			                details.setIlIiMonth3(qty);
			                break;
		                case 9:
			                details.setIlIiMonth4(qty);
			                break;
		                case 8:
			                details.setIlIiMonth5(qty);
			                break;
		                case 7:
			                details.setIlIiMonth6(qty);
			                break;
		                case 6:
			                details.setIlIiMonth7(qty);
			                break;
		                case 5:
			                details.setIlIiMonth8(qty);
			                break;
		                case 4:
			                details.setIlIiMonth9(qty);
			                break;
		                case 3:
			                details.setIlIiMonth10(qty);
			                break;
		                case 2:
			                details.setIlIiMonth11(qty);
			                break;
		                case 1:
			                details.setIlIiMonth12(qty);
			                break;
		                }
	                }
	                list.add(details);
	                
	            }
	            
	            if (list.size() == 0)
	                throw new GlobalNoRecordFoundException();
	            
	            return list;
			
		}else if(REPORT_TYPE.equals("RM_VAR")){
			
			
			
			  Collection invItems = invItemHome.findStockIiAll(AD_CMPNY);	
			 
	            if (invItems.size() == 0)
	                throw new GlobalNoRecordFoundException();
	            
	            Iterator i = invItems.iterator();

	            while (i.hasNext()) {

	            	LocalInvItem invItem = (LocalInvItem)i.next();
	                
	                Double actualQtyUsed= 0d;
	                Double stdQty= 0d;
	                Double varianceQty= 0d;
	                Double actualAmnt= 0d;
	                Double stdAmnt= 0d;
	                Double varianceAmnt= 0d;
	                

	                InvRepAdjustmentRegisterRmVarDetails details = new InvRepAdjustmentRegisterRmVarDetails();
	                details.setIlIiName(invItem.getIiName());
	                details.setIlIiDescription(invItem.getIiDescription());	                
	                details.setIlIiUomName(invItem.getInvUnitOfMeasure().getUomShortName());

	                criteria.put("type", "ISSUANCE");	
	                criteria.put("itemName", invItem.getIiName());
	                Collection invAdjustments = this.getAdjByCriteria(criteria, ORDER_BY, adBrnchList, AD_CMPNY);
	                if (invItem.getIiName().equals("121SUG0002"))
	                System.out.println("invAdjustments-" + invAdjustments.size());
					Iterator adjIter = invAdjustments.iterator();
					while (adjIter.hasNext()) {
						LocalInvAdjustment invAdjustment = (LocalInvAdjustment)adjIter.next();
						Iterator alItr = invAdjustment.getInvAdjustmentLines().iterator();
						while (alItr.hasNext()) {
							
							LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)alItr.next();
							//System.out.println(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName()+ "==" +invItem.getIiName());
							if(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName().equals(invItem.getIiName())){
								actualQtyUsed += Math.abs(invAdjustmentLine.getAlAdjustQuantity());
								actualAmnt += Math.abs(invAdjustmentLine.getAlUnitCost() * invAdjustmentLine.getAlAdjustQuantity());
							}
							
						} 
					}
					if (invItem.getIiName().equals("121SUG0002"))
					System.out.println("actualQtyUsed-" + actualQtyUsed);
	                details.setIlIiActualQtyUsed(actualQtyUsed);
	                details.setIlIiActualAmnt(actualAmnt);
	                
	                
	                Collection invBillOfMaterials = invBillOfMaterialHome.findByBomIiName(invItem.getIiName(), AD_CMPNY);
	                Iterator bomIter = invBillOfMaterials.iterator();
	                while (bomIter.hasNext()) {
	                	LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)bomIter.next();

	                	criteria.put("type", "PRODUCTION");
	                	criteria.put("itemName", invBillOfMaterial.getInvItem().getIiName());
		                Collection invBomAdjustments = this.getAdjByCriteria(criteria, ORDER_BY, adBrnchList, AD_CMPNY);
						Iterator bomAdjIter = invBomAdjustments.iterator();
						while (bomAdjIter.hasNext()) {
							LocalInvAdjustment invBomAdjustment = (LocalInvAdjustment)bomAdjIter.next();
							Iterator bomAlItr = invBomAdjustment.getInvAdjustmentLines().iterator();
							while (bomAlItr.hasNext()) {
								
								LocalInvAdjustmentLine invBomAdjustmentLine = (LocalInvAdjustmentLine)bomAlItr.next();
								//System.out.println(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName()+ "==" +invItem.getIiName());
								if(invBomAdjustmentLine.getInvItemLocation().getInvItem().getIiName().equals(invBillOfMaterial.getInvItem().getIiName())){
									stdQty += EJBCommon.roundIt(invBomAdjustmentLine.getAlAdjustQuantity() * (invBillOfMaterial.getBomQuantityNeeded() / invBillOfMaterial.getInvItem().getIiYield()), (short)2);
									stdAmnt += EJBCommon.roundIt(stdQty * invItem.getIiUnitCost(), (short)2);
								}
								
							} 
						}
	                }
	                details.setIlIiStdQty(stdQty);
	                details.setIlIiStdAmnt(stdAmnt);
	                
	                details.setIlIiVarianceQty(actualQtyUsed - stdQty);
	                details.setIlIiVarianceAmnt(EJBCommon.roundIt(actualAmnt - stdAmnt, (short)2));
	                details.setIlIiStdUnitCost(EJBCommon.roundIt(stdAmnt / stdQty, (short)2));
	                
	                //details.setIlIiQtyUsed(quantityUsed);
	         
	                list.add(details);
	                
	            }
	            
	            if (list.size() == 0)
	                throw new GlobalNoRecordFoundException();
	            
	            return list;
		} 
		else if(REPORT_TYPE.equals("Daily Balance Sheet")){
						
			System.out.println("Daily Balance Sheet");			
			double forwardedBalance= 0d;			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(bil) FROM AdBranchItemLocation bil ");
			short ctr = 0;
			int criteriaSize = 1;	      
			Object obj[] = null;	      
			if (adBrnchList.isEmpty()) {
				forwardedBalance = 0;
			}
			else {
				System.out.println("CheckPoint executeInvRepStockOnHand B");
				jbossQl.append("WHERE bil.adBranch.brCode in (");
				
				boolean firstLoop = true;

				Iterator j = adBrnchList.iterator();

				while(j.hasNext()) {

					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}

					AdBranchDetails mdetails = (AdBranchDetails) j.next();
					jbossQl.append(mdetails.getBrCode());

				}
				jbossQl.append(") ");
			}  
			
				jbossQl.append("AND ");

			jbossQl.append("bil.bilAdCompany = " + AD_CMPNY + " "); 
						
			System.out.println("jbossQl: "+jbossQl.toString());
			Collection adBranchItemLocations = null;
			
			try {
				adBranchItemLocations = adBranchItemLocationHome.getBilByCriteria(jbossQl.toString(), obj);				
			} catch (FinderException ex)	{
				throw new GlobalNoRecordFoundException ();
			} 

			if (adBranchItemLocations.isEmpty())
				forwardedBalance = 0;
			//System.out.println("jbossQl: "+adBranchItemLocations.size());
			Iterator i = adBranchItemLocations.iterator();
			java.util.GregorianCalendar gcCurrDate = new java.util.GregorianCalendar();
       	    gcCurrDate.setTime(EJBCommon.convertStringToSQLDate(DateFrom));
            gcCurrDate.set(gcCurrDate.get(java.util.Calendar.YEAR), gcCurrDate.get(java.util.Calendar.MONTH), gcCurrDate.get(java.util.Calendar.DATE), 0, 0, 0);
            gcCurrDate.set(java.util.Calendar.MILLISECOND, 0);
            gcCurrDate.add(java.util.Calendar.DATE, -1);
       	    Date AS_OF_DT = gcCurrDate.getTime();
       	    
			while (i.hasNext()) {
				
				LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation) i.next();
				
				try{
				LocalInvCosting invCosting = 
					invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
							AS_OF_DT, adBranchItemLocation.getInvItemLocation().getInvItem().getIiName(), 
							adBranchItemLocation.getInvItemLocation().getInvLocation().getLocName(), 
							adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
			
			    forwardedBalance += invCosting.getCstRemainingValue();
				}
				catch(Exception ex)
				{
				}
			
			}
			String DocNumbers="";
			String DocNumbers2="";
			String tempDocs ="";
			String tempDocs2 ="";
			forwardedBal = forwardedBalance;
			//System.out.println("outside: ");
			int xxx=0;
			int Day = 1;
			while (Day<32) {
			
				String newDate = DateFrom.substring(0, DateFrom.indexOf("/"))+"/"+Day+DateFrom.substring(DateFrom.lastIndexOf("/"));
				System.out.println(newDate);
				try{
					xxx = isValidDate(newDate);
				}
				catch(Exception ex)
				{
					System.out.println("ERROR");
				}
				double General =0d;
				double adjCost =0d;
				double Issuance =0d;
				 DocNumbers="";
				 DocNumbers2="";
				try{
					
					Collection invAdjustments2 = invAdjustmentLineHome.findGeneral(AD_CMPNY, EJBCommon.convertStringToSQLDate(newDate));
					
					Iterator ix = invAdjustments2.iterator();
					
					while (ix.hasNext()) {
						LocalInvAdjustmentLine invItem1 = (LocalInvAdjustmentLine)ix.next();
						
						if(invItem1.getInvAdjustment().getGlChartOfAccount().getCoaCode()==1419)
							{							
							General += invItem1.getAlUnitCost()*invItem1.getAlAdjustQuantity();													
							}
						else
						{
							adjCost +=invItem1.getAlUnitCost()*invItem1.getAlAdjustQuantity();
							System.out.println("adjCost xd "+adjCost);
							
							if(tempDocs2==invItem1.getInvAdjustment().getAdjDocumentNumber())
							{
							}
							else{
								tempDocs2=invItem1.getInvAdjustment().getAdjDocumentNumber();
								if(DocNumbers.length()>1)
									{
										DocNumbers2 +="/"+invItem1.getInvAdjustment().getAdjDocumentNumber().substring(4);
									}
									else
										DocNumbers2 = invItem1.getInvAdjustment().getAdjDocumentNumber();
											}
							
							continue;
						}
						
						if(tempDocs==invItem1.getInvAdjustment().getAdjDocumentNumber())
						{
						}
						else{
							tempDocs=invItem1.getInvAdjustment().getAdjDocumentNumber();
							if(DocNumbers.length()>1)
								{
									DocNumbers +="/"+invItem1.getInvAdjustment().getAdjDocumentNumber().substring(4);
								}
								else
									DocNumbers = invItem1.getInvAdjustment().getAdjDocumentNumber();
										}							
						}
					}
				catch (Exception ex) {					
				}
				
				try{

					Collection invAdjustments3 = invAdjustmentLineHome.findPostedIssuance(AD_CMPNY, EJBCommon.convertStringToSQLDate(newDate));
								
					Iterator iy = invAdjustments3.iterator();
					
					while (iy.hasNext()) {
						LocalInvAdjustmentLine invItem2 = (LocalInvAdjustmentLine)iy.next();
						double z=0d;
						z=invItem2.getAlUnitCost()*invItem2.getAlAdjustQuantity();
						Issuance += Math.abs(z);
										}
					}
				catch (Exception ex) {
					
				}
				
				
	                InvRepAdjustmentRegisterBalanceSheetDetails details = new InvRepAdjustmentRegisterBalanceSheetDetails();
	                details.setIlDate(newDate);	                
	                details.setIlForwardedBalance(forwardedBalance);
	                details.setIlDeliveries(General);
	                System.out.println("General - "+General);
	                details.setIlDocNumber(DocNumbers);
	                details.setIlAjustmentCost(Math.abs(adjCost));	              
	                details.setIlReferenceNumber(DocNumbers2);
	                details.setIlWithdrawals(Issuance);
	                System.out.println("Issuance - "+ Issuance);	                
	                forwardedBalance=forwardedBalance+General+Math.abs(adjCost);	                
	                forwardedBalance=forwardedBalance-Math.abs(Issuance);
	                details.setIlEndingBalance(forwardedBalance);
	                if(xxx==1)
	                list.add(details);
	                Day++;	                
			}	            
	            if (list.size() == 0)
	                throw new GlobalNoRecordFoundException();
	            return list;	            
		}
		
		else if(REPORT_TYPE.equals("Posted Deliveries")){
			

			System.out.println("Posted Deliveries");
		
			if(DateFrom.length()>1 && DateTo.length()>1)
			{
				Date dtFrom = EJBCommon.convertStringToSQLDate(DateFrom);
				
				Date dtTo = EJBCommon.convertStringToSQLDate(DateTo);
				System.out.println("S");	
				Collection Generals = invCostingHome.findByCstDateGeneralOnlyByMonth(dtFrom,dtTo,AD_CMPNY);
				System.out.println("GENERAL SIZE = " +Generals.size());	
			
				
				Iterator i = Generals.iterator();
				while (i.hasNext()) {
					LocalInvCosting postedDeliveries = (LocalInvCosting)i.next();
					InvRepAdjustmentRegisterPostedDeliveriesDetails details = new InvRepAdjustmentRegisterPostedDeliveriesDetails();
					details.setIL_DOC_NUM(postedDeliveries.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber());
					System.out.println("DOC_NUM = " + postedDeliveries.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber());	
					details.setIL_ITEM_CODE(postedDeliveries.getInvItemLocation().getInvItem().getIiName());
					System.out.println("ITEM_CODE = " + postedDeliveries.getInvItemLocation().getInvItem().getIiName());
					details.setIL_ITEM_DESC(postedDeliveries.getInvItemLocation().getInvItem().getIiDescription());
					System.out.println("ITEM_DESC = " + postedDeliveries.getInvItemLocation().getInvItem().getIiDescription());
					details.setIL_ITEM_UNIT(postedDeliveries.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
					System.out.println("ITEM_UNIT = " + postedDeliveries.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
					details.setIL_ITEM_QUANTITY(postedDeliveries.getInvAdjustmentLine().getAlAdjustQuantity());
					details.setIL_ITEM_COST(postedDeliveries.getInvAdjustmentLine().getAlUnitCost());
					System.out.println("details.setIL_DOC_NUM = " + postedDeliveries.getInvAdjustmentLine().getAlCode());
					if(postedDeliveries.getInvAdjustmentLine().getInvAdjustment().getGlChartOfAccount().getCoaCode()==1419)					
					list.add(details);
				}
			}
			else
			{
				throw new GlobalNoRecordFoundException();
			}
	            
	            if (list.size() == 0)
	                throw new GlobalNoRecordFoundException();
	            return list;
		}
		
			else if(REPORT_TYPE.equals("Summary of Issuance")){
			
			
			System.out.println("Summary of Issuance");
			
			double forwardedBalance= 0d;
			
			StringBuffer jbossQl2 = new StringBuffer();
			jbossQl2.append("SELECT OBJECT(bil) FROM AdBranchItemLocation bil ");

			short ctr = 0;
			int criteriaSize = 1;	      

			Object obj2[] = null;	      

			if (adBrnchList.isEmpty()) {

				forwardedBalance = 0;

			}
			else {
				System.out.println("CheckPoint executeInvRepStockOnHand B");
				jbossQl2.append("WHERE bil.adBranch.brCode in (");
				
				boolean firstLoop = true;

				Iterator j = adBrnchList.iterator();

				while(j.hasNext()) {

					if(firstLoop == false) { 
						jbossQl2.append(", "); 
					}
					else { 
						firstLoop = false; 
					}

					AdBranchDetails mdetails = (AdBranchDetails) j.next();

					jbossQl2.append(mdetails.getBrCode());

				}

				jbossQl2.append(") ");


			}  
			
		
			if (criteria.containsKey("itemFrom")) {

				

					jbossQl2.append("AND ");	

	

				jbossQl2.append("bil.invItemLocation.invItem.iiName  >= '" + (String)criteria.get("itemFrom") + "' ");

			}
			
			if (criteria.containsKey("itemTo")) {

				

				jbossQl2.append("AND ");	



			jbossQl2.append("bil.invItemLocation.invItem.iiName  <= '" + (String)criteria.get("itemTo") + "' ");

		}

				jbossQl2.append("AND ");

			

			jbossQl2.append("bil.bilAdCompany = " + AD_CMPNY + " ORDER BY bil.invItemLocation.invItem.iiName "); 
			
			
			System.out.println("jbossQl: "+jbossQl2.toString());
			Collection adBranchItemLocations = null;

			
			try {

				adBranchItemLocations = adBranchItemLocationHome.getBilByCriteria(jbossQl2.toString(), obj2);
				
			} catch (FinderException ex)	{

				throw new GlobalNoRecordFoundException ();

			} 

			if (adBranchItemLocations.isEmpty())
				forwardedBalance = 0;
			System.out.println("jbossQl: "+adBranchItemLocations.size());
			Iterator i = adBranchItemLocations.iterator();

			int xxx=0;
			String newDate = DateFrom.substring(0, DateFrom.indexOf("/"))+"/01/"+DateFrom.substring(DateFrom.lastIndexOf("/")+1);
			System.out.println(newDate);
			try{
				xxx = isValidDate(newDate);
				System.out.println(xxx);

			}
			catch(Exception ex)
			{
				System.out.println("ERROR");
			}
			xxx=0;
			
	
			

			String newDate2="";
			String  az = DateFrom.substring(0, DateFrom.indexOf("/"))+"/28/"+DateFrom.substring(DateFrom.lastIndexOf("/")+1);
				try{
					xxx = isValidDate(az);
					System.out.println(xxx);
					if(xxx==1)
						newDate2=az;
						
				}
				catch(Exception ex3)
				{

				}
				
				az = DateFrom.substring(0, DateFrom.indexOf("/"))+"/29/"+DateFrom.substring(DateFrom.lastIndexOf("/")+1);
				try{
					xxx = isValidDate(az);
					System.out.println(xxx);
					if(xxx==1)
						newDate2=az;
				}
				catch(Exception ex2)
				{
					
				}
				
				 az = DateFrom.substring(0, DateFrom.indexOf("/"))+"/30/"+DateFrom.substring(DateFrom.lastIndexOf("/")+1);
				try{
					xxx = isValidDate(az);
					System.out.println(xxx);
					if(xxx==1)
						newDate2=az;
				}
				catch(Exception ex1)
				{
					 
				}
				
				az = DateFrom.substring(0, DateFrom.indexOf("/"))+"/31/"+DateFrom.substring(DateFrom.lastIndexOf("/")+1);
			System.out.println(newDate);
			try{
				xxx = isValidDate(az);
				System.out.println(xxx);
				if(xxx==1)
					newDate2=az;
			}
			catch(Exception ex)
			{
				 
			}
			//System.out.println("newDate2: "+newDate2);
            
			String DocNumbers="";
			String tempDocs ="";
			forwardedBal = forwardedBalance;
			
			Object obj[] = null;	      
			int Day = 1;
			double total=0d;
			while (i.hasNext()) {
				double Issuance =0d;
				try{
					//System.out.println("SQL KABA?");
					//System.out.println("BAKIT?");
					LocalAdBranchItemLocation invItem = (LocalAdBranchItemLocation) i.next();
					StringBuffer jbossQl = new StringBuffer();
					//System.out.println("DATE FROM: "+newDate);
					//System.out.println("DATE TO: "+newDate2);
					obj = new Object[2];
					//Date dtFrom = EJBCommon.convertStringToSQLDate(DateFrom);
					//Date dtTo = EJBCommon.convertStringToSQLDate(DateTo);
					obj[0] = newDate;
					obj[1] = newDate2;
					System.out.println("newDate2="+newDate2);

					Collection invAdjustments4 = invAdjustmentHome.findPostedAdjByAdjDateRange(EJBCommon.convertStringToSQLDate(newDate), EJBCommon.convertStringToSQLDate(newDate2), AD_CMPNY);
					
					System.out.println("BOOM!? size "+invAdjustments4.size() + " name "+invItem.getInvItemLocation().getInvItem().getIiName());
					
					Iterator iy = invAdjustments4.iterator();
					
					while (iy.hasNext()) {
						LocalInvAdjustment adjustment = (LocalInvAdjustment)iy.next();	            	            		            	
						if(adjustment.getAdjType().equals("ISSUANCE"))
						{
							
							Collection invAdjustmentsLines99 =invAdjustmentLineHome.findByAlVoidAndAdjCode((byte)0,adjustment.getAdjCode() , AD_CMPNY);
							Iterator iyx = invAdjustmentsLines99.iterator();
							
							while (iyx.hasNext()) 
							{
								LocalInvAdjustmentLine adjustmentLine999 = (LocalInvAdjustmentLine)iyx.next();
								if(adjustmentLine999.getInvItemLocation().getInvItem().getIiName()==invItem.getInvItemLocation().getInvItem().getIiName())
								{	
									System.out.println("ISSUANCE - "+adjustment.getAdjDocumentNumber());
									System.out.println("BOOM!? item "+invItem.getInvItemLocation().getInvItem().getIiName());
									System.out.println("BOOM!? line "+adjustmentLine999.getAlCode());
									total+=adjustmentLine999.getAlAdjustQuantity()*adjustmentLine999.getAlUnitCost()*-1;
									
									 InvRepAdjustmentRegisterSummaryOfIssuanceDetails details = new InvRepAdjustmentRegisterSummaryOfIssuanceDetails();
									 									 
									 Collection BrList = gvsv.findByVsName("BRANCH", AD_CMPNY);

								 		String sample = adjustment.getGlChartOfAccount().getCoaAccountDescription();
								 		
								 		 Iterator ic = BrList.iterator();
								 		while (ic.hasNext()) {
								 			LocalGenValueSetValue glvsv = (LocalGenValueSetValue)ic.next();
								 			if(sample.contains(glvsv.getVsvDescription()+"-"))
								 					{
								 				System.out.println(glvsv.getVsvDescription());
								 				sample=glvsv.getVsvDescription();
								 					break;								 					
								 					}
								 		}				 
									 
									 
									 
									 
									 details.setIlDepartment(sample);
									 details.setIlDescription(invItem.getInvItemLocation().getInvItem().getIiDescription());
									 details.setIlName(invItem.getInvItemLocation().getInvItem().getIiName());
									 details.setIlQuantity(adjustmentLine999.getAlAdjustQuantity()*-1);
									 details.setIlTotalCost(adjustmentLine999.getAlAdjustQuantity()*adjustmentLine999.getAlUnitCost()*-1);
									 details.setOrderBy(details.getIlName());
									 details.setDATE(adjustment.getAdjDate());
									 details.setIL_REFERENCE(adjustment.getAdjReferenceNumber());
						                list.add(details);
								}
							}
						}
						else
						{
							//System.out.println("GENERAL - ");
						}
						
										}
					}
				catch (Exception ex) {
					System.out.println("Hello" + ex.toString());
					
				}
				
				
			
	
				
			

	               
	                
			}
			System.out.println("Total" + total);
	            if (list.size() == 0)
	                throw new GlobalNoRecordFoundException();
	            
	            Collections.sort(list, InvRepAdjustmentRegisterSummaryOfIssuanceDetails.ItemNameComparator);
	            
	            
	            return list;
	            
	            
		}else {
			try { 
				LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
				StringBuffer jbossQl = new StringBuffer();
				jbossQl.append("SELECT OBJECT(adj) FROM InvAdjustment adj ");
				boolean firstArgument = true;
				short ctr = 0;
				int criteriaSize = criteria.size();	      
				Object obj[];	      
				
				// Allocate the size of the object parameter
				if (criteria.containsKey("includedUnposted")) {
					criteriaSize--;
				}
				obj = new Object[criteriaSize];
				if (criteria.containsKey("type")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("adj.adjType=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("type");
					ctr++;
				}  
					if (criteria.containsKey("accountDescription")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					System.out.print("hhhhhererhere"+(String)criteria.get("accountDescription"));
					jbossQl.append("adj.glChartOfAccount.coaAccountNumber=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("accountDescription");
					ctr++;
				} 
				if (criteria.containsKey("dateFrom")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("adj.adjDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
				}  
				if (criteria.containsKey("dateTo")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("adj.adjDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;
				}    
				if (criteria.containsKey("documentNumberFrom")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("adj.adjDocumentNumber>=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("documentNumberFrom");
					ctr++;
				}  
				if (criteria.containsKey("documentNumberTo")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("adj.adjDocumentNumber<=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("documentNumberTo");
					ctr++;
				}
				if (criteria.containsKey("referenceNumber")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("adj.adjReferenceNumber>=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("referenceNumber");
					ctr++;
				}
				if (((String)criteria.get("includedUnposted")).equals("NO")) {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}	       	  
					jbossQl.append("adj.adjPosted = 1 ");
				} 
				if(adBrnchList.isEmpty()) {
					throw new GlobalNoRecordFoundException();
				} else {
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}	      
					jbossQl.append("adj.adjAdBranch in (");
					boolean firstLoop = true;
					Iterator i = adBrnchList.iterator();
					while(i.hasNext()) {
						if(firstLoop == false) { 
							jbossQl.append(", "); 
						}
						else { 
							firstLoop = false; 
						}
						AdBranchDetails mdetails = (AdBranchDetails) i.next();
						jbossQl.append(mdetails.getBrCode());
					}
					jbossQl.append(") ");
					firstArgument = false;
				}
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("adj.adjAdCompany = " + AD_CMPNY + " ");
				String orderBy = null;
				if (ORDER_BY.equals("DATE")) {
					orderBy = "adj.adjDate";
				} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
					orderBy = "adj.adjDocumentNumber";
				} 
				if (orderBy != null) {
					jbossQl.append("ORDER BY " + orderBy);
				} 
				Collection invAdjustments = null;
				try {
					System.out.println("jbossQl.toString()"+jbossQl.toString());
					invAdjustments = invAdjustmentHome.getAdjByCriteria(jbossQl.toString(), obj);
					System.out.println("invAdjustments="+invAdjustments.size());
				} catch (FinderException ex)	{
				} 	
				if (invAdjustments.size() == 0)
					throw new GlobalNoRecordFoundException();
				Iterator i = invAdjustments.iterator();
				while (i.hasNext()) {
					LocalInvAdjustment invAdjustment = (LocalInvAdjustment)i.next();   	  
					if (SHOW_LN_ITEMS) {
						double AMOUNT = 0d;
						Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
						Iterator alItr = invAdjustmentLines.iterator();
						while (alItr.hasNext()) {
							LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)alItr.next();
							InvRepAdjustmentRegisterDetails details = new InvRepAdjustmentRegisterDetails();
							details.setAdjDate(invAdjustment.getAdjDate());
							details.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
							details.setAdjReferenceNumber(invAdjustment.getAdjReferenceNumber());
							details.setAdjType(invAdjustment.getAdjType());
							details.setAdjDescription(invAdjustment.getAdjDescription());
							details.setAdjItemName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
							details.setAdjItemDesc(invAdjustmentLine.getInvItemLocation().getInvItem().getIiDescription());
							details.setAdjLocation(invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName());
							details.setAdjQuantity(invAdjustmentLine.getAlAdjustQuantity());
							details.setAdjUnit(invAdjustmentLine.getInvUnitOfMeasure().getUomName());
							details.setAdjUnitCost(invAdjustmentLine.getAlUnitCost());
							details.setAdjAccountDescription(invAdjustment.getGlChartOfAccount().getCoaAccountDescription());
						    System.out.print(invAdjustment.getGlChartOfAccount().getCoaAccountDescription()+"here"+invAdjustment.getGlChartOfAccount().getCoaAccountNumber());
							AMOUNT = EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(),  this.getGlFcPrecisionUnit(AD_CMPNY));
							details.setAdjAmount(AMOUNT);
							details.setOrderBy(ORDER_BY);
							System.out.println("1"); 
							list.add(details);
						} 
					} else {
						System.out.println("ELSE-------------------------");
						InvRepAdjustmentRegisterDetails details = new InvRepAdjustmentRegisterDetails();
						details.setAdjDate(invAdjustment.getAdjDate());
						details.setAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
						details.setAdjReferenceNumber(invAdjustment.getAdjReferenceNumber());
						details.setAdjType(invAdjustment.getAdjType());
						details.setAdjDescription(invAdjustment.getAdjDescription());
						details.setAdjAccountDescription(invAdjustment.getGlChartOfAccount().getCoaAccountDescription());
					    System.out.print(invAdjustment.getGlChartOfAccount().getCoaAccountDescription()+"here"+invAdjustment.getGlChartOfAccount().getCoaAccountNumber());
					    double AMOUNT = 0d;
						Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
						Iterator alItr = invAdjustmentLines.iterator();
						while (alItr.hasNext()) {
							LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)alItr.next();
							AMOUNT += EJBCommon.roundIt(invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost(), this.getGlFcPrecisionUnit(AD_CMPNY));
						} 
						details.setAdjAmount(AMOUNT);
						details.setOrderBy(ORDER_BY);
						list.add(details);
					}
				}
				
				// sort
				System.out.println("2");
				if (GROUP_BY.equals("ITEM NAME")) {
					Collections.sort(list, InvRepAdjustmentRegisterDetails.ItemNameComparator);
				} else if (GROUP_BY.equals("DATE")) {
					Collections.sort(list, InvRepAdjustmentRegisterDetails.DateComparator);
				} else {
					Collections.sort(list, InvRepAdjustmentRegisterDetails.NoGroupComparator);
				}
				System.out.println("3");
				return list;
			} catch (GlobalNoRecordFoundException ex) {
				throw ex;
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new EJBException(ex.getMessage());
			}
		}
		
		
	}
	// private methods
	private static int isValidDate(String input) {
        String formatString = "MM/dd/yyyy";
        int x=0;
        try {
            SimpleDateFormat format = new SimpleDateFormat(formatString);
            format.setLenient(false);
            format.parse(input);
        } catch (ParseException e) {
           return x;
        } catch (IllegalArgumentException e) {
            return x;
        }
        x=1;
        return x;
    }

	
	// private methods
	private double convertForeignToFunctionalCurrency(Integer FC_CODE, String FC_NM, 
			Date CONVERSION_DATE, double CONVERSION_RATE, double AMOUNT, Integer AD_CMPNY) {
		
		Debug.print("InvRepAdjustmentRegisterControllerBean convertForeignToFunctionalCurrency");
		
		
		LocalGlFunctionalCurrencyRateHome glFunctionalCurrencyRateHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		LocalAdCompany adCompany = null;
		
		// Initialize EJB Homes
		
		try {
			
			glFunctionalCurrencyRateHome = (LocalGlFunctionalCurrencyRateHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyRateHome.JNDI_NAME, LocalGlFunctionalCurrencyRateHome.class);                         
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);             
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		// get company and extended precision
		
		try {
			
			adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
		} catch (Exception ex) {
			
			throw new EJBException(ex.getMessage());
			
		}	     
		
		
		// Convert to functional currency if necessary
		
		if (CONVERSION_RATE != 1 && CONVERSION_RATE != 0 ) {
			
			AMOUNT = AMOUNT / CONVERSION_RATE;
			
		}
		
		return EJBCommon.roundIt(AMOUNT, adCompany.getGlFunctionalCurrency().getFcPrecision());
		
	}
	

	
	private Collection getAdjByCriteria(HashMap criteria, String ORDER_BY, ArrayList adBrnchList, Integer AD_CMPNY) throws GlobalNoRecordFoundException {
		
		//Debug.print("InvRepAdjustmentRegisterControllerBean getAdjByCriteria");
		
		LocalInvAdjustmentHome invAdjustmentHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		
		LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvLocationHome invLocationHome = null;
        
        LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
		
		ArrayList list = new ArrayList();
		ArrayList listAdj = new ArrayList();
		//initialized EJB Home
		
		try {
			
			invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
		    invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
            invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(adj) FROM InvAdjustment adj ");
			if (criteria.containsKey("itemName")) jbossQl.append(", IN(adj.invAdjustmentLines) al ");
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			Object obj[];	      
			
			// Allocate the size of the object parameter
			if (criteria.containsKey("includedUnposted")) {
				criteriaSize--;
			}
			
			obj = new Object[criteriaSize];
			if (criteria.containsKey("type")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("adj.adjType=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("type");
				ctr++;
			}  
				if (criteria.containsKey("accountDescription")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				System.out.print("hhhhhererhere"+(String)criteria.get("accountDescription"));
				jbossQl.append("adj.glChartOfAccount.coaAccountNumber=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("accountDescription");
				ctr++;
			} 
			if (criteria.containsKey("dateFrom")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("adj.adjDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}  
			if (criteria.containsKey("dateTo")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("adj.adjDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}    
			if (criteria.containsKey("documentNumberFrom")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("adj.adjDocumentNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberFrom");
				ctr++;
			}  
			if (criteria.containsKey("documentNumberTo")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("adj.adjDocumentNumber<=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberTo");
				ctr++;
			}
			if (criteria.containsKey("referenceNumber")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("adj.adjReferenceNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("referenceNumber");
				ctr++;
			}
			
			if (criteria.containsKey("itemName")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("al.invItemLocation.invItem.iiName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemName");
				ctr++;
			}
			if (((String)criteria.get("includedUnposted")).equals("NO")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	       	  
				jbossQl.append("adj.adjPosted = 1 ");
			} 
			if(adBrnchList.isEmpty()) {
				throw new GlobalNoRecordFoundException();
			} else {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}	      
				jbossQl.append("adj.adjAdBranch in (");
				boolean firstLoop = true;
				Iterator i = adBrnchList.iterator();
				while(i.hasNext()) {
					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}
					AdBranchDetails mdetails = (AdBranchDetails) i.next();
					jbossQl.append(mdetails.getBrCode());
				}
				jbossQl.append(") ");
				firstArgument = false;
			}
			if (!firstArgument) {
				jbossQl.append("AND ");
			} else {
				firstArgument = false;
				jbossQl.append("WHERE ");
			}
			jbossQl.append("adj.adjAdCompany = " + AD_CMPNY + " ");
			String orderBy = null;
			if (ORDER_BY.equals("DATE")) {
				orderBy = "adj.adjDate";
			} else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
				orderBy = "adj.adjDocumentNumber";
			} 
			if (orderBy != null) {
				jbossQl.append("ORDER BY " + orderBy);
			} 
			Collection invAdjustments = null;
			try {
				invAdjustments = invAdjustmentHome.getAdjByCriteria(jbossQl.toString(), obj);
			} catch (FinderException ex)	{
			} 	
			if (!criteria.containsKey("itemName")) {
				System.out.println("jbossQl-" + jbossQl.toString());
				System.out.println("criteria-" +(String)criteria.get("type"));
				System.out.println("private-" + invAdjustments.size());
				
			}
			return invAdjustments;
		} catch (GlobalNoRecordFoundException ex) {
			throw ex;
		} catch (EJBException ex) {
			throw new EJBException(ex.getMessage());

		}
		
		
		
		
	}
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvRepAdjustmentRegisterControllerBean ejbCreate");
		
	}
}
