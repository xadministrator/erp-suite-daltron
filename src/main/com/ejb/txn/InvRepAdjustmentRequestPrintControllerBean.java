
/*
 * InvRepAdjustmentRequestPrintControllerBean.java
 *
 * Created on September 19,2006 2:58 PM
 *
 * @author  Rey B. Limosenero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
//import com.struts.util.Common;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvAdjustmentDetails;
import com.util.InvRepAdjustmentRequestPrintDetails;

/**
 * @ejb:bean name="InvRepAdjustmentRequestPrintControllerEJB"
 *           display-name="Use for printing adjustment request"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepAdjustmentRequestPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepAdjustmentRequestPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepAdjustmentRequestPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class InvRepAdjustmentRequestPrintControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
	
	
	public double getQuantityByIiNameAndUomName(String II_NM, String LOC_NM, String UOM_NM, Date PI_DT, Integer AD_BRNCH, Integer AD_CMPNY) {

        Debug.print("InvAdjustmentRequestControllerBean getQuantityByIiNameAndUomName");
        
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalInvCostingHome invCostingHome = null;
        System.out.print("printC2");
        // Initialize EJB Home
        double x=0;
        try {
            
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);        	  
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);  
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);        	  
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        //double actualQuantity = 0d;
        try {
            
           double actualQuantity = 0d;
            
            LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(II_NM, LOC_NM, AD_CMPNY);
            
            try {
                
                LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(PI_DT, 
                        invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                
                actualQuantity = invCosting.getCstRemainingQuantity();
                System.out.println("actual : " + actualQuantity);
            } catch (FinderException ex) {
                
            }
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);            
            System.out.println("actual : " + actualQuantity);
            x= EJBCommon.roundIt(actualQuantity * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor(), this.getInvGpQuantityPrecisionUnit(AD_CMPNY));
            
        } catch (Exception ex) {
        	System.out.println("s");
          //  Debug.printStackTrace(ex);
          //  throw new EJBException(ex.getMessage());
            
        }
        return x;
        //return EJBCommon.roundIt(actualQuantity * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor(), this.getInvGpQuantityPrecisionUnit(AD_CMPNY));
    }
  
    /** 
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/   
	
	
    public ArrayList executeInvRepAdjustmentRequestPrint(ArrayList adjCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvRepAdjustmentRequestPrintControllerBean executeInvRepAdjustmentRequestPrint");
        
        LocalInvAdjustmentHome invAdjustmentHome = null; 
        LocalInvCostingHome invCostingHome = null;
        LocalAdUserHome adUserHome = null;
        
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
            invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome .JNDI_NAME, LocalAdUserHome .class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = adjCodeList.iterator();
        	
        	while (i.hasNext()) {
        		
        		Integer ADJ_CODE = (Integer) i.next();
        	
        		LocalInvAdjustment invAdjustment = null;	        	
        		
        		try {
        			
        		    invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
        			
        		} catch (FinderException ex) {
        			
        			continue;
        			
        		}	        	  	         
        		
        		// get adjustment lines 
        		
        		Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
        		
        		Iterator adjIter = invAdjustmentLines.iterator();
        		
        		while(adjIter.hasNext()) {
        			double COST = 0d;
        			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)adjIter.next();
        		
        			InvRepAdjustmentRequestPrintDetails details = new InvRepAdjustmentRequestPrintDetails();
        			
        			
        			details.setApAdjType(invAdjustment.getAdjType());
        			details.setApAdjDate(invAdjustment.getAdjDate());
        			details.setApAdjDocumentNumber(invAdjustment.getAdjDocumentNumber());
        			details.setApAdjReferenceNumber(invAdjustment.getAdjReferenceNumber());
        			details.setApAdjDescription(invAdjustment.getAdjDescription());
        			details.setApAdjGlCoaAccount(invAdjustment.getGlChartOfAccount().getCoaAccountNumber());
        			details.setApAdjGlCoaAccountDesc(invAdjustment.getGlChartOfAccount().getCoaAccountDescription());
        			LocalAdUser adUser = adUserHome.findByUsrName(invAdjustment.getAdjCreatedBy(), AD_CMPNY);
        			String desc = adUser.getUsrDescription().toString();
                    details.setApAlBranchCode(invAdjustment.getAdjAdBranch());
                   // details.setApAlBranchName(invAdjustment.getAdjAdBranch());
                    details.setApAdjApprovedBy(invAdjustment.getAdjApprovedRejectedBy()); 
        			details.setApAdjCreatedBy(invAdjustment.getAdjCreatedBy());
        			details.setApAlIiName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName());
        			details.setApAlIiDescription(invAdjustmentLine.getInvItemLocation().getInvItem().getIiDescription());
        			details.setApAlLocName(invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName());
        			details.setApAlUomName(invAdjustmentLine.getInvUnitOfMeasure().getUomName());
        			details.setApAlAdjustQuantity(invAdjustmentLine.getAlAdjustQuantity());
        			details.setApAlUnitCost(invAdjustmentLine.getAlUnitCost());
        			System.out.print("herer 1"+invAdjustmentLine.getInvItemLocation().getInvItem().getIiName()+" <-IM "+ invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName()+"<-LOC"+ invAdjustmentLine.getInvUnitOfMeasure().getUomName()+"<-GU"+invAdjustment.getAdjDate()+"<-GD"+new Integer(invAdjustment.getAdjAdBranch())+"<-BR"+new Integer(invAdjustment.getAdjAdCompany())+"<-US");     
        			double actual = this.getQuantityByIiNameAndUomName(invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(),  invAdjustmentLine.getInvUnitOfMeasure().getUomName(),invAdjustment.getAdjDate(), new Integer(invAdjustment.getAdjAdBranch()), invAdjustment.getAdjAdCompany());
        			System.out.print(actual+" actual");
        			details.setApAlActualQuantity(actual);
        			
        			LocalInvCosting invLastCosting = null;
        			
        			try{
            			invLastCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIlCode(invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getIlCode(), invAdjustmentLine.getInvAdjustment().getAdjAdBranch(), AD_CMPNY);
            			COST = Math.abs(invLastCosting.getCstRemainingValue() / invLastCosting.getCstRemainingQuantity());
        			}catch(Exception e){
        				COST =0;
        			}
        			
        			
        			details.setApAlAveCost(COST);
        			
        			list.add(details);
        		
        		}
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}        	       	
        	
        	return list;        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
    public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("InvAdjustmentRequestControllerBean getInvGpQuantityPrecisionUnit");
       
        LocalAdPreferenceHome adPreferenceHome = null;         
        
         // Initialize EJB Home
          
         try {
              
         	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
        	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           
            return adPreference.getPrfInvQuantityPrecisionUnit();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }

     }
     
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepAdjustmentRequestPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
		
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvRepAdjustmentRequestPrintControllerBean ejbCreate");
      
    }
}
