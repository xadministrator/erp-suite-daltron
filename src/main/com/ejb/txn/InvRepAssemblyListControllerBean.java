
/*
 * InvRepAssemblyListControllerBean.java
 *
 * Created on Aug 27, 2004, 11:04 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepAssemblyListDetails;

/**
 * @ejb:bean name="InvRepAssemblyListControllerEJB"
 *           display-name="Used for viewing assembly lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepAssemblyListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepAssemblyListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepAssemblyListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvRepAssemblyListControllerBean extends AbstractSessionBean {

	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepAssemblyListControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	 /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvLocationAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepAssemblyListControllerBean getAdLvInvItemCategoryAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeInvRepAssemblyList(HashMap criteria, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                  
        Debug.print("InvRepAssemblyListControllerBean executeInvRepAssemblyList");
        
        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
           
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(ii) FROM InvItem ii ");
		  
	      boolean firstArgument = true;
	      short ctr = 0;
		  int criteriaSize = criteria.size();	      
	      
	      Object obj[];	      
		
		  // Allocate the size of the object parameter
		
	      if (criteria.containsKey("itemName")) {
	      	
	      	 criteriaSize--;
	      	 
	      }
	      
	      if (criteria.containsKey("location")) {
	          
	          jbossQl.append(", IN(ii.invItemLocations) il ");
	          
	      }
	      
	      obj = new Object[criteriaSize];
		       	      
		  if (criteria.containsKey("itemName")) {
		  	
		  	 if (!firstArgument) {
		  	 
		  	    jbossQl.append("AND ");	
		  	 	
		     } else {
		     	
		     	firstArgument = false;
		     	jbossQl.append("WHERE ");
		     	
		     }
		     
		  	 jbossQl.append("ii.iiName LIKE '%" + (String)criteria.get("itemName") + "%' ");
		  	 
		  }
		  
		  if (criteria.containsKey("itemCategory")) {
		   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("ii.iiAdLvCategory=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("itemCategory");
		   	  ctr++;
	   	  
	      }
		  
		  if (criteria.containsKey("location")) {
		      
		      if (!firstArgument) {		       	  	
		          jbossQl.append("AND ");		       	     
		      } else {		       	  	
		          firstArgument = false;
		          jbossQl.append("WHERE ");		       	  	 
		      }
		      
		      jbossQl.append("il.invLocation.locName=?" + (ctr+1) + " ");
		      obj[ctr] = (String)criteria.get("location");
		      ctr++;
		      
		  }

		  if (criteria.containsKey("enable") &&
			      criteria.containsKey("disable")) {
			      	
		      if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
		  	 
		  	 jbossQl.append("(ii.iiEnable=?" + (ctr+1) + " OR ");
		  	 obj[ctr] = new Byte(EJBCommon.TRUE);
		     ctr++;
		     
		     jbossQl.append("ii.iiEnable=?" + (ctr+1) + ") ");
		  	 obj[ctr] = new Byte(EJBCommon.FALSE);
		     ctr++;
		     
		  } else {
			  	         
		      if (criteria.containsKey("enable")) {
		      	
		      	 if (!firstArgument) {
		      	 	jbossQl.append("AND ");
		      	 } else {
		      	 	firstArgument = false;
		      	 	jbossQl.append("WHERE ");
		      	 }
		      	 jbossQl.append("ii.iiEnable=?" + (ctr+1) + " ");
		      	 obj[ctr] = new Byte(EJBCommon.TRUE);
		      	 ctr++;
		      	 
		      } 
		      
		      if (criteria.containsKey("disable")) {
		      	
		      	 if (!firstArgument) {
		      	 	jbossQl.append("AND ");
		      	 } else {
		      	 	firstArgument = false;
		      	 	jbossQl.append("WHERE ");
		      	 }
		      	 jbossQl.append("ii.iiEnable=?" + (ctr+1) + " ");		      	 		      	 		      	 	      	 
		      	 obj[ctr] = new Byte(EJBCommon.FALSE);
		      	 ctr++;
		      	 
		      }
		      
			
		      
		  }
		  
		  if (!firstArgument) {
      	 	jbossQl.append("AND ");
      	  } else {
      	 	firstArgument = false;
      	 	jbossQl.append("WHERE ");
      	  }

		  jbossQl.append("ii.iiClass='Assembly' AND ii.iiAdCompany=" + AD_CMPNY + " ORDER BY ii.iiName ");
  	
	      Collection invItems = invItemHome.getIiByCriteria(jbossQl.toString(), obj);	         
		  
		  if (invItems.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = invItems.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalInvItem invItem = (LocalInvItem)i.next();		  	  		  	  
		  	  
		  	  Collection billOfMaterials = invItem.getInvBillOfMaterials();
		  	  
		  	  Iterator itrBOM = billOfMaterials.iterator();
		  	  
		  	  while(itrBOM.hasNext()) {
		  	  	
		  	  	LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)itrBOM.next();
		  	    InvRepAssemblyListDetails details = new InvRepAssemblyListDetails();
		  	  	
		  	  	try {
		  	  	
		  	  		LocalInvItem invItemBOM = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);
		  	  		details.setAlComponentDescription(invItemBOM.getIiDescription());
		  	  		details.setAlComponentUomName(invBillOfMaterial.getInvUnitOfMeasure().getUomName());
		  	  	
		  	  	} catch (FinderException ex) {
		  	  		
		  	  		
		  	  	}

		  	    details.setAlIiName(invItem.getIiName());
		  	    details.setAlIiDescription(invItem.getIiDescription());
		  	    details.setAlIiUomName(invItem.getInvUnitOfMeasure().getUomName());
		  	    details.setAlIiEnable(invItem.getIiEnable());		  	  			  	  		  	  	
		  	  	details.setAlComponentName(invBillOfMaterial.getBomIiName());
		  	  	details.setAlQuantityNeeded(invBillOfMaterial.getBomQuantityNeeded());
		  	  	details.setAlIiPartNumber(invItem.getIiPartNumber());
		  	  	details.setAlIiUnitCost(invItemHome.findByIiName(invBillOfMaterial.getBomIiName(),AD_CMPNY).getIiUnitCost());
		  	  	list.add(details);
		  	  	
		  	  }		  	  
     
		  }
			     
		  if (list.size() == 0)
	  	      throw new GlobalNoRecordFoundException();
	  	  
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepAssemblyListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvRepAssemblyListControllerBean ejbCreate");
      
    }
}
