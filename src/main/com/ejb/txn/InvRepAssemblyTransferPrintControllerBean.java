
/*
 * InvRepAssemblyTransferPrintControllerBean.java
 *
 * Created on April 04, 2005, 3:05 PM
 *
 * @author  Jolly T. Martin
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvAssemblyTransfer;
import com.ejb.inv.LocalInvAssemblyTransferHome;
import com.ejb.inv.LocalInvAssemblyTransferLine;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvRepAssemblyTransferPrintDetails;

/**
 * @ejb:bean name="InvRepAssemblyTransferPrintControllerEJB"
 *           display-name="Used for printing assembly transfer transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepAssemblyTransferPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepAssemblyTransferPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepAssemblyTransferPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class InvRepAssemblyTransferPrintControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeInvRepAssemblyTransferPrint(ArrayList atrCodeList, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
        
        Debug.print("InvRepAssemblyTransferPrintControllerBean executeInvRepAssemblyTransferPrint");
        
        LocalInvAssemblyTransferHome invAssemblyTransferHome = null; 
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invAssemblyTransferHome = (LocalInvAssemblyTransferHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvAssemblyTransferHome.JNDI_NAME, LocalInvAssemblyTransferHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            Iterator i = atrCodeList.iterator();
            
            while (i.hasNext()) {
                
                Integer ATR_CODE = (Integer) i.next();
                
                LocalInvAssemblyTransfer invAssemblyTransfer = null;	        	
                
                try {
                    
                    invAssemblyTransfer = invAssemblyTransferHome.findByPrimaryKey(ATR_CODE);
                    
                } catch (FinderException ex) {
                    
                    continue;
                    
                }	        	  	         
                
                // get assembly transfer lines 
                
                Collection invAssemblyTransferLines = invAssemblyTransfer.getInvAssemblyTransferLines();
                
                Iterator atlIter = invAssemblyTransferLines.iterator();
                
                while(atlIter.hasNext()) {
                    
                    LocalInvAssemblyTransferLine invAssemblyTransferLine = (LocalInvAssemblyTransferLine)atlIter.next();
                    
                    InvRepAssemblyTransferPrintDetails details = new InvRepAssemblyTransferPrintDetails();
                    
                    details.setAtpAtrDate(invAssemblyTransfer.getAtrDate());
                    details.setAtpAtrDocumentNumber(invAssemblyTransfer.getAtrDocumentNumber());
                    details.setAtpAtrReferenceNumber(invAssemblyTransfer.getAtrReferenceNumber());
                    details.setAtpAtrCreatedBy(invAssemblyTransfer.getAtrCreatedBy());
                    details.setAtpAtrApprovedRejectedBy(invAssemblyTransfer.getAtrApprovedRejectedBy());
                    details.setAtpAtrDescription(invAssemblyTransfer.getAtrDescription());
                    details.setAtpAtlAssembleQuantity(invAssemblyTransferLine.getAtlAssembleQuantity());
                    details.setAtpAtlBolQtyRequired(invAssemblyTransferLine.getInvBuildOrderLine().getBolQuantityRequired());
                    details.setAtpAtlBolIlIiName(invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvItem().getIiName());
                    details.setAtpAtlBolIlIiDescription(invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvItem().getIiDescription());
                    details.setAtpAtlBolIlLocName(invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvLocation().getLocName());
                    details.setAtpAtlBolIlIiUomName(invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
                    details.setAtpAtlBolBorDocumentNumber(invAssemblyTransferLine.getInvBuildOrderLine().getInvBuildOrder().getBorDocumentNumber());
                    details.setAtpAtlAssembleCost(invAssemblyTransferLine.getAtlAssembleCost());
                    
                    list.add(details);
                    
                }
                
            }
            
            if (list.isEmpty()) {
                
                throw new GlobalNoRecordFoundException();
                
            }        	       	
            
            return list;        	
            
        } catch (GlobalNoRecordFoundException ex) {
            
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
        
        Debug.print("InvRepAssemblyTransferPrintControllerBean getAdCompany");      
        
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            AdCompanyDetails details = new AdCompanyDetails();
            details.setCmpName(adCompany.getCmpName());
            details.setCmpAddress(adCompany.getCmpAddress());
            details.setCmpCity(adCompany.getCmpCity());
            details.setCmpCountry(adCompany.getCmpCountry());
            details.setCmpPhone(adCompany.getCmpPhone());
            
            return details;  	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }   
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
        
        Debug.print("InvRepAssemblyTransferPrintControllerBean getGlFcPrecisionUnit");
        
        
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            return  adCompany.getGlFunctionalCurrency().getFcPrecision();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    
    // SessionBean methods
    
    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {
        
        Debug.print("InvRepAssemblyTransferPrintControllerBean ejbCreate");
        
    }
}
