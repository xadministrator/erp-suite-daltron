package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.ejb.inv.LocalInvBranchStockTransferHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvRepBranchStockTransferInPrintDetails;

/**
 * @ejb:bean name="InvRepBranchStockTransferInPrintControllerEJB"
 *           display-name="Used for printing Branch stock transfer transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepBranchStockTransferInPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepBranchStockTransferInPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepBranchStockTransferInPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class InvRepBranchStockTransferInPrintControllerBean extends AbstractSessionBean{

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeInvRepBranchStockTransferInPrint(ArrayList bstCodeList, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("InvRepBranchStockTransferPrintInControllerBean executeInvRepBranchStockTransferInPrint");

        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
        LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
            invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Iterator i = bstCodeList.iterator();

        	while (i.hasNext()) {

        		Integer BST_CODE = (Integer) i.next();

        		LocalInvBranchStockTransfer invBranchStockTransfer = null;

        		try {

        		    invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(BST_CODE);

        		} catch (FinderException ex) {

        			continue;

        		}

        		// get stock transfer lines 

        		Collection invBranchStockTransferLines = invBranchStockTransferLineHome.findByBstCode(invBranchStockTransfer.getBstCode(), AD_CMPNY);

        		Iterator bslIter = invBranchStockTransferLines.iterator();

        		while(bslIter.hasNext()) {

        			LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)bslIter.next();
        			
        			InvRepBranchStockTransferInPrintDetails details = new InvRepBranchStockTransferInPrintDetails();
        			
        			details.setBstpBstType(invBranchStockTransfer.getBstType());
        			details.setBstpBstDate(invBranchStockTransfer.getBstDate());
        			details.setBstpBstNumber(invBranchStockTransfer.getBstNumber());
        			details.setBstpBstDescription(invBranchStockTransfer.getBstDescription());        			
        			details.setBstpBstTransferOutNumber(invBranchStockTransfer.getBstTransferOutNumber());
        			details.setBstpBstTransitLocation(invBranchStockTransfer.getInvLocation().getLocName());
        			details.setBstpBstCreatedBy(invBranchStockTransfer.getBstCreatedBy());
        			details.setBstpBstApprovedRejectedBy(invBranchStockTransfer.getBstApprovedRejectedBy());
        			details.setBstpBstBranch(invBranchStockTransfer.getAdBranch().getBrName());
        			details.setBstpBslQuantity(invBranchStockTransferLine.getBslQuantity());
        			details.setBstpBslQuantityReceived(invBranchStockTransferLine.getBslQuantityReceived());
        			details.setBstpBslUnitOfMeasure(invBranchStockTransferLine.getInvUnitOfMeasure().getUomShortName());
        			details.setBstpBslItemName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
        			details.setBstpBslItemDescription(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiDescription());
        			details.setBstpBslItemLocation(invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
        			details.setBstpBslUnitPrice(invBranchStockTransferLine.getBslUnitCost());
        			details.setBstpBslAmount(invBranchStockTransferLine.getBslAmount());
        			details.setBstpSlsPrc(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiSalesPrice());
        			details.setBstpBslMscItm(invBranchStockTransferLine.getBslMisc());
        			
        			if (details.getBstpBstType().equalsIgnoreCase("IN"))list.add(details);
        			
        		}
        	
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}      
        	
        	return list;        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepBranchStockTransferInPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
 // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvRepBranchStockTransferPrintControllerBean ejbCreate");
      
    }

}
