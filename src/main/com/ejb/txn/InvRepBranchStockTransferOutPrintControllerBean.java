package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.ejb.inv.LocalInvBranchStockTransferHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvRepBranchStockTransferOutPrintDetails;

/**
 * @ejb:bean name="InvRepBranchStockTransferOutPrintControllerEJB"
 *           display-name="Used for printing Branch stock transfer transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepBranchStockTransferOutPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepBranchStockTransferOutPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepBranchStockTransferOutPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class InvRepBranchStockTransferOutPrintControllerBean extends AbstractSessionBean{

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeInvRepBranchStockTransferOutPrint(ArrayList bstCodeList, Integer AD_CMPNY)
        throws GlobalNoRecordFoundException {

        Debug.print("InvRepBranchStockTransferPrintOutControllerBean executeInvRepBranchStockTransferOutPrint");

        LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
        LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);
            invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	Iterator i = bstCodeList.iterator();

        	while (i.hasNext()) {

        		Integer BST_CODE = (Integer) i.next();

        		LocalInvBranchStockTransfer invBranchStockTransfer = null;
        		LocalInvBranchStockTransfer invBranchStockTransferOrder = null;

        		try {

        		    invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(BST_CODE);
        		    System.out.println("invBranchStockTransfer.getBstTransferOrderNumber() = "+invBranchStockTransfer.getBstTransferOrderNumber()+" pk = "+BST_CODE);
        		   
        		    
        		    
        		} catch (FinderException ex) {
        			System.out.println("catch 1");
        			
        		}
        		try{
        			
        			invBranchStockTransferOrder = invBranchStockTransferHome.findByBstNumberAndAdCompany(invBranchStockTransfer.getBstTransferOrderNumber(),  AD_CMPNY);
        		    System.out.println("2");
        		} catch (Exception e) {
        			System.out.println("catch 2");
				} 
        	
        		
        		System.out.println("--------------->");
        		// get stock transfer lines 
        		Collection invBranchStockTransferLines  = null;
        		Collection invBranchStockTransferOrderLines = null;
        		
        		try{
        			invBranchStockTransferLines = invBranchStockTransferLineHome.findByBstCode(invBranchStockTransfer.getBstCode(), AD_CMPNY);
            		System.out.println("invBranchStockTransfer.getBstCode()="+invBranchStockTransfer.getBstCode());

        		} catch (Exception e) {
        			System.out.println("catch 3");
				}
        		try{
        			invBranchStockTransferOrderLines = invBranchStockTransferLineHome.findByBstCode(invBranchStockTransferOrder.getBstCode(), AD_CMPNY);
        			
        		} catch (Exception e) {
        			System.out.println("catch 4");
				}
        		
        		Iterator bslIter = null;
        		Iterator bslIter2 = null;
        		
        		try{
        			bslIter = invBranchStockTransferLines.iterator();
        		} catch (Exception e) {
					// TODO: handle exception
				}
        		
        		try{
        			 bslIter2 = invBranchStockTransferOrderLines.iterator();
        		} catch (Exception e) {
					// TODO: handle exception
				}
        		
        		System.out.println("3----------invBranchStockTransferLines="+invBranchStockTransferLines.size());
        		while(bslIter.hasNext()) {

        			LocalInvBranchStockTransferLine invBranchStockTransferLine = null;
        			LocalInvBranchStockTransferLine invBranchStockTransferOrderLine = null;
        			
        			try{
        				invBranchStockTransferLine = (LocalInvBranchStockTransferLine)bslIter.next();
        			} catch (Exception e) {
						// TODO: handle exception
					}
        			
        			try{
        				invBranchStockTransferOrderLine = (LocalInvBranchStockTransferLine)bslIter2.next();
        			} catch (Exception e) {
						// TODO: handle exception
					}
        			InvRepBranchStockTransferOutPrintDetails details = new InvRepBranchStockTransferOutPrintDetails();
        			
        			details.setBstpBstType(invBranchStockTransfer.getBstType());
        			details.setBstpBstDate(invBranchStockTransfer.getBstDate());
        			details.setBstpBstNumber(invBranchStockTransfer.getBstNumber());
        			details.setBstpBstDescription(invBranchStockTransfer.getBstDescription());
        			details.setBstpBstCreatedBy(invBranchStockTransfer.getBstCreatedBy());
        			details.setBstpBstApprovedRejectedBy(invBranchStockTransfer.getBstApprovedRejectedBy());
        			details.setBstpBstTransitLocation(invBranchStockTransfer.getInvLocation().getLocName());
        			details.setBstpBstBranch(invBranchStockTransfer.getAdBranch().getBrName());
        			details.setBstpBslQuantity(invBranchStockTransferLine.getBslQuantity());
        			try{
        				details.setBstpBslOrderedQuantity(invBranchStockTransferOrderLine.getBslQuantity());
        			}catch (Exception e) {
        				details.setBstpBslOrderedQuantity(0d);
					}
        			
        			details.setBstpBslUnitOfMeasure(invBranchStockTransferLine.getInvUnitOfMeasure().getUomShortName());
        			details.setBstpBslItemName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
        			details.setBstpBslItemDescription(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiDescription());
        			details.setBstpBslUnitPrice(invBranchStockTransferLine.getBslUnitCost());
        			details.setBstpBslAmount(invBranchStockTransferLine.getBslAmount());
        			details.setBstpSlsPrc(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiSalesPrice());
        			details.setBstpBslMscItm(invBranchStockTransferLine.getBslMisc());
        			
        			System.out.println("4");
        			if (details.getBstpBstType().equalsIgnoreCase("OUT"))list.add(details);
        			
        		}
        	
        	}
        	System.out.println("5 -----------LIST"+list.size());
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}      
        	
        	return list;        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepBranchStockTransferPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
 // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvRepBranchStockTransferPrintControllerBean ejbCreate");
      
    }

}
