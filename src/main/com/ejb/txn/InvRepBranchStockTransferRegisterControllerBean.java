package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepAdjustmentRegisterDetails;
import com.util.InvRepBranchStockTransferRegisterDetails;

/**
 * @ejb:bean name="InvRepBranchStockTransferRegisterControllerEJB"
 *           display-name="Used for generation of branch stock transfer register"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepBranchStockTransferRegisterControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepBranchStockTransferRegisterController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepBranchStockTransferRegisterControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvRepBranchStockTransferRegisterControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepBranchStockRegisterControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrAll(Integer AD_CMPNY) {
        
        Debug.print("InvRepBranchStockTransferRegisterControllerBean getInvLocAll");
        
        LocalAdBranchHome AdBranchHome = null;
        Collection AdBranches = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            AdBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            AdBranches = AdBranchHome.findBrAll(AD_CMPNY);            
            
            if (AdBranches.isEmpty()) {
                
                return null;
                
            }
            
            Iterator i = AdBranches.iterator();
            
            while (i.hasNext()) {
                
                LocalAdBranch AdBranch = (LocalAdBranch)i.next();	
                String details = AdBranch.getBrBranchCode();
                
                list.add(details);
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeInvRepBranchStockTransferRegister(HashMap criteria, String ORDER_BY, Integer AD_BRNCH, Integer AD_CMPNY) 
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvRepBranchStockTransferRegisterControllerBean executeInvRepBranchStockTransferRegister");
		
		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
		LocalAdBranchItemLocationHome invBranchStockTransferRegisterHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			invBranchStockTransferRegisterHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			String type = new String();
			StringBuffer jbossQl = new StringBuffer();
			
			// BST OUT
			
			if (criteria.containsKey("includeTransferOut") && ((String)criteria.get("includeTransferOut")).equals("YES")) {
				
				type = "OUT";
				
				jbossQl = new StringBuffer();
				jbossQl.append("SELECT OBJECT(bstl) FROM InvBranchStockTransferLine bstl ");
				
				boolean firstArgument = true;
				short ctr = 0;
				int criteriaSize = 0;	      
				
				Object obj[];	      
				
				// Allocate the size of the object parameter
				
				if (criteria.containsKey("dateFrom")) {

					criteriaSize++;

				}
				
				if (criteria.containsKey("dateTo")) {

					criteriaSize++;

				}
				
				if (criteria.containsKey("transferOutNumberFrom")) {

					criteriaSize++;

				}
				
				if (criteria.containsKey("transferOutNumberTo")) {

					criteriaSize++;

				}
				
				if (criteria.containsKey("branchTo")) {

					criteriaSize++;

				}
				if (criteria.containsKey("itemCategory")){
					criteriaSize++;
				}
				
				
				obj = new Object[criteriaSize];
				
				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;
					
				}  
				
				if (criteria.containsKey("itemCategory")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("bstl.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemCategory");
					ctr++;

				}
				
				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}    
				
				if (criteria.containsKey("transferOutNumberFrom")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("bstl.invBranchStockTransfer.bstNumber>=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("transferOutNumberFrom");
					ctr++;
					
				}  
				
				if (criteria.containsKey("transferOutNumberTo")) {
					
					if (!firstArgument) {
						
						jbossQl.append("AND ");
						
					} else {
						
						firstArgument = false;
						jbossQl.append("WHERE ");
						
					}
					
					jbossQl.append("bstl.invBranchStockTransfer.bstNumber<=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("transferOutNumberTo");
					ctr++;
				}
				
				
				if (criteria.containsKey("branchTo")) {
					
					Integer brCode = this.getAdBrCodeByBrName((String)criteria.get("branchTo"), AD_CMPNY);
					
					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.adBranch.brCode=?" + (ctr+1) + " ");
					obj[ctr] = (Integer)brCode;
					ctr++;
					
					
				}
				
				if (((String)criteria.get("includeUnposted")).equals("NO")){
					
					if (!firstArgument) {
						jbossQl.append("AND ");	
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstPosted=1 ");

				}
				
				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bstl.invBranchStockTransfer.bstVoid=0 AND bstl.invBranchStockTransfer.bstType='" + type + "' AND bstl.invBranchStockTransfer.bstAdBranch=" + AD_BRNCH + " AND bstl.invBranchStockTransfer.bstAdCompany=" + AD_CMPNY + " ");

				String orderBy = null;
          
				if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          

					jbossQl.append("ORDER BY bstl.invBranchStockTransfer.bstNumber");
					
				} else {

					jbossQl.append("ORDER BY bstl.invBranchStockTransfer.bstDate");

				}
				
				System.out.println(jbossQl.toString());

				Collection invBranchStockTransferLines = invBranchStockTransferLineHome.getBstlByCriteria(jbossQl.toString(), obj);	         

				Iterator i = invBranchStockTransferLines.iterator();
				System.out.println("Size="+invBranchStockTransferLines.size());
				while (i.hasNext()) {

					LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)i.next(); 

					InvRepBranchStockTransferRegisterDetails mdetails = new InvRepBranchStockTransferRegisterDetails();
					System.out.println("BSL--"+invBranchStockTransferLine.getBslCode());
					mdetails.setBstrBstType(invBranchStockTransferLine.getInvBranchStockTransfer().getBstType());
					mdetails.setBstrBstStatus("TRANSFER OUT");
					mdetails.setBstrBstDocumentNumber(invBranchStockTransferLine.getInvBranchStockTransfer().getBstNumber());
					mdetails.setBstrBstDate(invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate());
					mdetails.setBstrBstTransferOutNumber(invBranchStockTransferLine.getInvBranchStockTransfer().getBstTransferOutNumber());
					mdetails.setBstrBstBranch(invBranchStockTransferLine.getInvBranchStockTransfer().getAdBranch().getBrName());
					mdetails.setBstrBstTransitLocation(invBranchStockTransferLine.getInvBranchStockTransfer().getInvLocation().getLocName());
					mdetails.setBstrBstCreatedBy(invBranchStockTransferLine.getInvBranchStockTransfer().getBstCreatedBy());
					
					mdetails.setBstrBstlItemName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
					mdetails.setBstrBstlItemDescription(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiDescription());
					mdetails.setBstrBstlLocation(invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
					mdetails.setBstrBstlUnit(invBranchStockTransferLine.getInvUnitOfMeasure().getUomShortName());
					mdetails.setBstrBstlUnitCost(invBranchStockTransferLine.getBslUnitCost());
					mdetails.setBstrBstlAmount(invBranchStockTransferLine.getBslAmount());
					
					mdetails.setBstrBstlQuantity(invBranchStockTransferLine.getBslQuantity());
					mdetails.setOrderBy(ORDER_BY);
					

					
					double quantityReceived = 0d;
					
					System.out.println("Doc Num="+mdetails.getBstrBstDocumentNumber());
					System.out.println("Item Name="+mdetails.getBstrBstlItemName());
					System.out.println("Location="+mdetails.getBstrBstlLocation());
					Collection invReceivedQuantities = invBranchStockTransferLineHome.findQtyTransferedByBstTranferOutNumberAndIiCode(
							mdetails.getBstrBstDocumentNumber(),
							mdetails.getBstrBstlItemName(), 
							mdetails.getBstrBstlLocation(),
							AD_CMPNY);
					
					Iterator x = invReceivedQuantities.iterator();
					
					while(x.hasNext())
					{
						
						LocalInvBranchStockTransferLine invReceivedQuantity = (LocalInvBranchStockTransferLine)x.next();
						
						if(mdetails.getBstrBstlUnit().equals(invReceivedQuantity.getInvUnitOfMeasure().getUomShortName())){
							quantityReceived += invReceivedQuantity.getBslQuantityReceived();
						}
						
					}
					
					mdetails.setBstrBstlQuantityTransfered(quantityReceived);


					
					list.add(mdetails);
				}

			}
				

			// BST IN	

			if (criteria.containsKey("includeTransferIn") && ((String)criteria.get("includeTransferIn")).equals("YES")) {

				type = "IN";

				jbossQl = new StringBuffer();
				jbossQl.append("SELECT OBJECT(bstl) FROM InvBranchStockTransferLine bstl ");

				boolean firstArgument = true;
				short ctr = 0;
				int criteriaSize = 0;	      

				Object obj[];	      

				// Allocate the size of the object parameter

				if (criteria.containsKey("dateFrom")) {

					criteriaSize++;

				}

				if (criteria.containsKey("dateTo")) {

					criteriaSize++;

				}
				
				if (criteria.containsKey("transferInNumberFrom")) {

					criteriaSize++;

				}
				
				if (criteria.containsKey("transferInNumberTo")) {

					criteriaSize++;

				}
				
				if (criteria.containsKey("branchFrom")) {

					criteriaSize++;

				}
				
				if (criteria.containsKey("itemCategory")){
					criteriaSize++;
				}
				
				obj = new Object[criteriaSize];

				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;

				}  

				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}   
				
				if (criteria.containsKey("itemCategory")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("bstl.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemCategory");
					ctr++;

				}
				
				if (criteria.containsKey("transferInNumberFrom")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("bstl.invBranchStockTransfer.bstNumber>=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("transferInNumberFrom");
					ctr++;

				}  

				if (criteria.containsKey("transferInNumberTo")) {

					if (!firstArgument) {

						jbossQl.append("AND ");

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("bstl.invBranchStockTransfer.bstNumber<=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("transferInNumberTo");
					ctr++;
					
				}

				if (criteria.containsKey("branchFrom")) {

					Integer brCode = this.getAdBrCodeByBrName((String)criteria.get("branchFrom"), AD_CMPNY);

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.adBranch.brCode=?" + (ctr+1) + " ");
					obj[ctr] = (Integer)brCode;
					ctr++;


				}

				if (criteria.containsKey("referenceNumber")) {

					if (!firstArgument) {
						jbossQl.append("AND ");	
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstTransferOutNumber LIKE '%" + (String)criteria.get("referenceNumber") + "%' ");

				}

				if (((String)criteria.get("includeUnposted")).equals("NO")){

					if (!firstArgument) {
						jbossQl.append("AND ");	
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstPosted=1 ");

				}

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bstl.invBranchStockTransfer.bstType='" + type + "' AND bstl.invBranchStockTransfer.bstAdBranch=" + AD_BRNCH + " AND bstl.invBranchStockTransfer.bstAdCompany=" + AD_CMPNY + " ");

				String orderBy = null;

				if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          

					jbossQl.append("ORDER BY bstl.invBranchStockTransfer.bstNumber");

				} else {

					jbossQl.append("ORDER BY bstl.invBranchStockTransfer.bstDate");

				}

				System.out.println(jbossQl.toString());

				Collection invBranchStockTransferLines = invBranchStockTransferLineHome.getBstlByCriteria(jbossQl.toString(), obj);	         

				Iterator i = invBranchStockTransferLines.iterator();

				while (i.hasNext()) {

					LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)i.next(); 

					InvRepBranchStockTransferRegisterDetails mdetails = new InvRepBranchStockTransferRegisterDetails();
					mdetails.setBstrBstType(invBranchStockTransferLine.getInvBranchStockTransfer().getBstType());
					mdetails.setBstrBstStatus("TRANSFER IN");
					mdetails.setBstrBstDocumentNumber(invBranchStockTransferLine.getInvBranchStockTransfer().getBstNumber());
					mdetails.setBstrBstDate(invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate());
					mdetails.setBstrBstTransferOutNumber(invBranchStockTransferLine.getInvBranchStockTransfer().getBstTransferOutNumber());
					mdetails.setBstrBstBranch(invBranchStockTransferLine.getInvBranchStockTransfer().getAdBranch().getBrName());
					mdetails.setBstrBstTransitLocation(invBranchStockTransferLine.getInvBranchStockTransfer().getInvLocation().getLocName());
					mdetails.setBstrBstCreatedBy(invBranchStockTransferLine.getInvBranchStockTransfer().getBstCreatedBy());

					mdetails.setBstrBstlItemName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
					mdetails.setBstrBstlItemDescription(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiDescription());
					mdetails.setBstrBstlLocation(invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
					mdetails.setBstrBstlUnit(invBranchStockTransferLine.getInvUnitOfMeasure().getUomShortName());
					mdetails.setBstrBstlUnitCost(invBranchStockTransferLine.getBslUnitCost());
					mdetails.setBstrBstlAmount(invBranchStockTransferLine.getBslAmount());

					mdetails.setBstrBstlQuantity(invBranchStockTransferLine.getBslQuantityReceived());

					mdetails.setOrderBy(ORDER_BY);
										
					list.add(mdetails);

				}

			}	
	      // BST ORDER
			
			if (criteria.containsKey("includeTransferOrder") && ((String)criteria.get("includeTransferOrder")).equals("YES")) {

				type = "ORDER";

				jbossQl = new StringBuffer();
				jbossQl.append("SELECT OBJECT(bstl) FROM InvBranchStockTransferLine bstl ");

				boolean firstArgument = true;
				short ctr = 0;
				int criteriaSize = 0;	      

				Object obj[];	      

				// Allocate the size of the object parameter

				if (criteria.containsKey("dateFrom")) {

					criteriaSize++;

				}

				if (criteria.containsKey("dateTo")) {

					criteriaSize++;

				}
				
				if (criteria.containsKey("itemCategory")) {

					criteriaSize++;

				}

				if (criteria.containsKey("branchFrom")) {

					criteriaSize++;

				}

				obj = new Object[criteriaSize];

				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;

				}  
				
				if (criteria.containsKey("itemCategory")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("bstl.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemCategory");
					ctr++;

				}
				
				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}    

				if (criteria.containsKey("branchFrom")) {

					Integer brCode = this.getAdBrCodeByBrName((String)criteria.get("branchFrom"), AD_CMPNY);

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstAdBranch=?" + (ctr+1) + " ");
					obj[ctr] = (Integer)brCode;
					ctr++;


				}
				
				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bstl.invBranchStockTransfer.bstPosted=1 AND bstl.invBranchStockTransfer.bstLock=0 " +
								"AND bstl.invBranchStockTransfer.bstType='" + type + "' " +
								"AND bstl.invBranchStockTransfer.adBranch.brCode=" + AD_BRNCH + " AND bstl.invBranchStockTransfer.bstAdCompany=" + AD_CMPNY + " ");

				String orderBy = null;

				if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          

					jbossQl.append("ORDER BY bstl.invBranchStockTransfer.bstNumber");

				} else {

					jbossQl.append("ORDER BY bstl.invBranchStockTransfer.bstDate");

				}

				System.out.println(jbossQl.toString());

				Collection invBranchStockTransferLines = invBranchStockTransferLineHome.getBstlByCriteria(jbossQl.toString(), obj);	         

				Iterator i = invBranchStockTransferLines.iterator();
				LocalInvItemLocation invItemLocation = null;
				short LINE_NUMBER = 0;
				while (i.hasNext()) {
					LINE_NUMBER++;
					LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)i.next(); 
					
					try {
                        //search the item from item location
                        invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                                invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(), 
                                invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);
                        
                    } catch(FinderException ex) {
                        
                        throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(LINE_NUMBER) + " - " + invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
                        
                    }
					System.out.println(invBranchStockTransferLine.getBslQuantity() + "<== before conversion");
					
					double convertedQuantity = this.convertByUomAndQuantity(
                            invBranchStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(), 
                            invBranchStockTransferLine.getBslQuantity(), AD_CMPNY);
					System.out.println(convertedQuantity + "<== after conversion");
					
					System.out.println(invBranchStockTransferLine.getInvUnitOfMeasure().getUomShortName() + "<== Unit of Conversion edited in transfer order");
					String defaultUOM = invItemLocation.getInvItem().getInvUnitOfMeasure().getUomShortName();
					System.out.println(defaultUOM + "<== default Unit of Conversion in item entry");
					
					InvRepBranchStockTransferRegisterDetails mdetails = new InvRepBranchStockTransferRegisterDetails();
					mdetails.setBstrBstType(invBranchStockTransferLine.getInvBranchStockTransfer().getBstType());
					mdetails.setBstrBstStatus("TRANSFER ORDER");
					mdetails.setBstrBstDocumentNumber(invBranchStockTransferLine.getInvBranchStockTransfer().getBstNumber());
					mdetails.setBstrBstDate(invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate());
					mdetails.setBstrBstTransferOutNumber(invBranchStockTransferLine.getInvBranchStockTransfer().getBstTransferOutNumber());
					mdetails.setBstrBstBranch(this.getAdBrNameByBrCode(invBranchStockTransferLine.getInvBranchStockTransfer().getBstAdBranch()));
					mdetails.setBstrBstTransitLocation(invBranchStockTransferLine.getInvBranchStockTransfer().getInvLocation().getLocName());
					mdetails.setBstrBstCreatedBy(invBranchStockTransferLine.getInvBranchStockTransfer().getBstCreatedBy());

					mdetails.setBstrBstlItemName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
					mdetails.setBstrBstlItemDescription(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiDescription());
					mdetails.setBstrBstlLocation(invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
					mdetails.setBstrBstlUnit(defaultUOM);
					mdetails.setBstrBstlUnitCost(invBranchStockTransferLine.getBslUnitCost());
					mdetails.setBstrBstlAmount(invBranchStockTransferLine.getBslAmount());

					mdetails.setBstrBstlQuantity(convertedQuantity);
					
					mdetails.setOrderBy(ORDER_BY);
					
					list.add(mdetails);

				}
				Collections.sort(list, InvRepBranchStockTransferRegisterDetails.ItemNameComparator);
			}
				
			// BST Incoming	

			if (criteria.containsKey("includeIncoming") && ((String)criteria.get("includeIncoming")).equals("YES")) {

				type = "OUT";

				jbossQl = new StringBuffer();
				jbossQl.append("SELECT OBJECT(bstl) FROM InvBranchStockTransferLine bstl ");

				boolean firstArgument = true;
				short ctr = 0;
				int criteriaSize = 0;	      

				Object obj[];	      

				// Allocate the size of the object parameter

				if (criteria.containsKey("dateFrom")) {

					criteriaSize++;

				}

				if (criteria.containsKey("dateTo")) {

					criteriaSize++;

				}

				if (criteria.containsKey("branchFrom")) {

					criteriaSize++;

				}
				
				if (criteria.containsKey("itemCategory")){
					criteriaSize++;
				}

				obj = new Object[criteriaSize];

				if (criteria.containsKey("dateFrom")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstDate>=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateFrom");
					ctr++;

				}  

				if (criteria.containsKey("dateTo")) {

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstDate<=?" + (ctr+1) + " ");
					obj[ctr] = (Date)criteria.get("dateTo");
					ctr++;

				}    
				
				if (criteria.containsKey("itemCategory")) {

					if (!firstArgument) {

						jbossQl.append("AND ");	

					} else {

						firstArgument = false;
						jbossQl.append("WHERE ");

					}

					jbossQl.append("bstl.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
					obj[ctr] = (String)criteria.get("itemCategory");
					ctr++;

				}

				if (criteria.containsKey("branchFrom")) {

					Integer brCode = this.getAdBrCodeByBrName((String)criteria.get("branchFrom"), AD_CMPNY);

					if (!firstArgument) {
						jbossQl.append("AND ");
					} else {
						firstArgument = false;
						jbossQl.append("WHERE ");
					}
					jbossQl.append("bstl.invBranchStockTransfer.bstAdBranch=?" + (ctr+1) + " ");
					obj[ctr] = (Integer)brCode;
					ctr++;


				}
				
				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bstl.invBranchStockTransfer.bstPosted=1 AND bstl.invBranchStockTransfer.bstLock=0 " +
								"AND bstl.invBranchStockTransfer.bstType='" + type + "' " +
								"AND bstl.invBranchStockTransfer.adBranch.brCode=" + AD_BRNCH + " AND bstl.invBranchStockTransfer.bstAdCompany=" + AD_CMPNY + " ");

				String orderBy = null;

				if (ORDER_BY.equals("DOCUMENT NUMBER")) {	          

					jbossQl.append("ORDER BY bstl.invBranchStockTransfer.bstNumber");

				} else {

					jbossQl.append("ORDER BY bstl.invBranchStockTransfer.bstDate");

				}

				System.out.println(jbossQl.toString());

				Collection invBranchStockTransferLines = invBranchStockTransferLineHome.getBstlByCriteria(jbossQl.toString(), obj);	         

				Iterator i = invBranchStockTransferLines.iterator();

				while (i.hasNext()) {

					LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)i.next(); 

					InvRepBranchStockTransferRegisterDetails mdetails = new InvRepBranchStockTransferRegisterDetails();
					mdetails.setBstrBstType(invBranchStockTransferLine.getInvBranchStockTransfer().getBstType());
					mdetails.setBstrBstStatus("INCOMING TRANSFER");
					mdetails.setBstrBstDocumentNumber(invBranchStockTransferLine.getInvBranchStockTransfer().getBstNumber());
					mdetails.setBstrBstDate(invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate());
					mdetails.setBstrBstTransferOutNumber(invBranchStockTransferLine.getInvBranchStockTransfer().getBstTransferOutNumber());
					mdetails.setBstrBstBranch(this.getAdBrNameByBrCode(invBranchStockTransferLine.getInvBranchStockTransfer().getBstAdBranch()));
					mdetails.setBstrBstTransitLocation(invBranchStockTransferLine.getInvBranchStockTransfer().getInvLocation().getLocName());
					mdetails.setBstrBstCreatedBy(invBranchStockTransferLine.getInvBranchStockTransfer().getBstCreatedBy());

					mdetails.setBstrBstlItemName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName());
					mdetails.setBstrBstlItemDescription(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiDescription());
					mdetails.setBstrBstlLocation(invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
					mdetails.setBstrBstlUnit(invBranchStockTransferLine.getInvUnitOfMeasure().getUomShortName());
					mdetails.setBstrBstlUnitCost(invBranchStockTransferLine.getBslUnitCost());
					mdetails.setBstrBstlAmount(invBranchStockTransferLine.getBslAmount());

					mdetails.setBstrBstlQuantity(invBranchStockTransferLine.getBslQuantity());
					
					mdetails.setOrderBy(ORDER_BY);
					
					list.add(mdetails);

				}

			}	
			
			if (list.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			// sort
			Collections.sort(list, InvRepBranchStockTransferRegisterDetails.StatusComparator);
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepBranchStockTransferRegisterControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	
	private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
        
        Debug.print("InvBranchStockTransferOrderEntryControllerBean convertByUomFromAndUomToAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
       
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	Debug.print("InvBranchStockTransferOrderEntryControllerBean convertByUomFromAndUomToAndQuantity A");
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            Debug.print("InvBranchStockTransferOrderEntryControllerBean convertByUomFromAndUomToAndQuantity B"); 
            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("InvRepBranchStockTransferRegisterControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }

	// private methods
	
	private Integer getAdBrCodeByBrName(String BR_BRNCH_NM, Integer AD_CMPNY) {
        
        Debug.print("InvRepBranchStockTransferRegisterControllerBean getAdBrCodeByBrName");
        
        LocalAdBranchHome adBranchHome = null;
        
        try {
            
        	adBranchHome = (LocalAdBranchHome)EJBHomeFactory.lookUpLocalHome(
            		LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch(NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_NM, AD_CMPNY); 
            return  adBranch.getBrCode();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	
	private String getAdBrNameByBrCode(Integer BR_CODE) {

		Debug.print("InvRepBranchStockTransferRegisterControllerBean getAdBrNameByBrCode");

		LocalAdBranchHome adBranchHome = null;

		try {

			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.lookUpLocalHome(
					LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

		} catch(NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(BR_CODE); 
			return  adBranch.getBrName();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvRepBranchStockTransferRegisterControllerBean ejbCreate");
		
	}
}
