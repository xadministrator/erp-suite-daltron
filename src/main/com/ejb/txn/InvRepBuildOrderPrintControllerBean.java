
/*
 * InvRepBuildOrderPrintControllerBean.java
 *
 * Created on April 04, 2005, 03:11 PM
 *
 * @author  Ma. Jennifer G. Manuel
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvBuildOrder;
import com.ejb.inv.LocalInvBuildOrderHome;
import com.ejb.inv.LocalInvBuildOrderLine;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepBuildOrderPrintDetails;

/**
 * @ejb:bean name="InvRepBuildOrderPrintControllerEJB"
 *           display-name="Used for printing build order transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepBuildOrderPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepBuildOrderPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepBuildOrderPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class InvRepBuildOrderPrintControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeInvRepBuildOrderPrint(ArrayList borCodeList, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
        
        Debug.print("InvRepBuildOrderPrintControllerBean executeInvRepBuildOrderPrint");
        
        LocalInvBuildOrderHome invBuildOrderHome = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invBuildOrderHome = (LocalInvBuildOrderHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildOrderHome.JNDI_NAME, LocalInvBuildOrderHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            Iterator i = borCodeList.iterator();
            
            while (i.hasNext()) {
                
                Integer BOR_CODE = (Integer) i.next();
                
                LocalInvBuildOrder invBuildOrder = null;
                
                
                try {
                    
                    invBuildOrder = invBuildOrderHome.findByPrimaryKey(BOR_CODE);
                    
                } catch (FinderException ex) {
                    
                    continue;
                    
                }	        	
                
                // get build order lines
                
                Collection invBuildOrderLines = invBuildOrder.getInvBuildOrderLines();        	            
                
                Iterator ilIter = invBuildOrderLines.iterator();
                
                while (ilIter.hasNext()) {
                    
                    LocalInvBuildOrderLine invBuildOrderLine = (LocalInvBuildOrderLine)ilIter.next();
                    
                    InvRepBuildOrderPrintDetails details = new InvRepBuildOrderPrintDetails();
                    
                    details.setBopBorDate(invBuildOrder.getBorDate());
                    details.setBopBorDocumentNumber(invBuildOrder.getBorDocumentNumber());
                    details.setBopBorReferenceNumber(invBuildOrder.getBorReferenceNumber());
                    details.setBopBorCreatedBy(invBuildOrder.getBorCreatedBy());
                    details.setBopBorApprovedRejectedBy(invBuildOrder.getBorApprovedRejectedBy());
                    details.setBopBorDescription(invBuildOrder.getBorDescription());
                    details.setBopBolIiDescription(invBuildOrderLine.getInvItemLocation().getInvItem().getIiDescription());
                    details.setBopBolIiName(invBuildOrderLine.getInvItemLocation().getInvItem().getIiName());
                    details.setBopBolLocName(invBuildOrderLine.getInvItemLocation().getInvLocation().getLocName());
                    details.setBopBolUomName(invBuildOrderLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
                    details.setBopBolQuantityRequired(invBuildOrderLine.getBolQuantityRequired());
                    
                    double unitCost = this.getInvIiUnitCostByIiNameAndUomName(invBuildOrderLine.getInvItemLocation().getInvItem().getIiName(),
                            invBuildOrderLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);

                    details.setBopBolUnitCost(unitCost);
                    
                    list.add(details);
                    
                }
                
            }
            
            if (list.isEmpty()) {
                
                throw new GlobalNoRecordFoundException();
                
            }        	       	
            
            return list;    
            
        } catch (GlobalNoRecordFoundException ex) {
            
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
        
        Debug.print("InvRepBuildOrderPrintControllerBean getAdCompany");      
        
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            AdCompanyDetails details = new AdCompanyDetails();
            details.setCmpName(adCompany.getCmpName());
            
            return details;  	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
        
        Debug.print("InvRepBuildOrderPrintControllerBean getInvGpQuantityPrecisionUnit");
        
        LocalAdPreferenceHome adPreferenceHome = null;         
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            return adPreference.getPrfInvQuantityPrecisionUnit();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
        
        Debug.print("InvRepBuildOrderPrintControllerBean getGlFcPrecisionUnit");
        
        
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            return  adCompany.getGlFunctionalCurrency().getFcPrecision();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private double getInvIiUnitCostByIiNameAndUomName(String II_NM, String UOM_NM, Integer AD_CMPNY) {
        
        Debug.print("InvRepBuildOrderPrintControllerBean getInvIiUnitCostByIiNameAndUomName");
        
        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);  
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(invItem.getIiUnitCost() * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));      
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    // SessionBean methods
    
    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {
        
        Debug.print("InvRepBuildOrderPrintControllerBean ejbCreate");
        
    }
}
