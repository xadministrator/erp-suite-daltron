package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLine;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ApRepCheckRegisterDetails;
import com.util.ApRepPurchaseOrderDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepBuildUnbuildAssemblyOrderDetails;

/**
* @ejb:bean name="InvRepBuildUnbuildAssemblyOrderControllerEJB"
*           display-name="Used for generation of purchase order reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/InvRepBuildUnbuildAssemblyOrderControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.InvRepBuildUnbuildAssemblyOrderController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.InvRepBuildUnbuildAssemblyOrderControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class InvRepBuildUnbuildAssemblyOrderControllerBean extends AbstractSessionBean {
	
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepBuildUnbuildAssemblyControllerBean getAdLvReportTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV REPORT TYPE - BUILD UNBUILD ASSEMBLY ORDER", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepBuildUnbuildAssemblyControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepBuildUnbuildAssemblyControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeInvRepBuildUnbuildAssemblyOrder(HashMap criteria,String ORDER_BY, boolean SUMMARIZE, ArrayList adBrnchList, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvRepBuildUnbuildAssemblyControllerBean executeInvRepBuildUnbuildAssemblyOrder");
		
		LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
		 
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalAdBranchHome adBranchHome = null;
		LocalInvItemHome invItemHome = null;
		
		ArrayList invBuildUnbuildAssemblyOrderList = new ArrayList();
		// Initialize EJB Home
		
		try {
			
			invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
			
			invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
				
				
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
                    lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT (bl) FROM InvBuildUnbuildAssemblyLine bl ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			Object obj[];    
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("itemName")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("customerCode")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("includeUnposted")) {
				
				criteriaSize--;
				
			}

			if (criteria.containsKey("includeCancelled")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			

			if (criteria.containsKey("itemFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("bl.invItemLocation.invItem.iiName  >= '" + (String)criteria.get("itemFrom") + "' ");
				obj[ctr] = (String)criteria.get("itemFrom");
				ctr++;
			}
		
			if (criteria.containsKey("itemTo")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("bl.invItemLocation.invItem.iiName  <= '" + (String)criteria.get("itemTo") + "' ");
				obj[ctr] = (String)criteria.get("itemTo");
				ctr++;
			}
			
			
			if (criteria.containsKey("documentNumberFrom")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("bl.invBuildUnbuildAssembly.buaDocumentNumber>=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("documentNumberFrom");
				ctr++;
			}  
			if (criteria.containsKey("documentNumberTo")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("bl.invBuildUnbuildAssembly.buaDocumentNumber<=?" + (ctr+1) + " ");

				obj[ctr] = (String)criteria.get("documentNumberTo");
				ctr++;
			}
			

			if (((String)criteria.get("includeUnposted")).equals("no")){
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bl.invBuildUnbuildAssembly.buaPosted=1 ");

			}

			if (((String)criteria.get("includeCancelled")).equals("no")){
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bl.invBuildUnbuildAssembly.buaVoid=0 ");

			}
			
			if (criteria.containsKey("customerCode")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bl.invBuildUnbuildAssembly.arCustomer.cstCustomerCode LIKE '%"
						+ (String)criteria.get("customerCode") + "%' ");
				
			}
			
			if (criteria.containsKey("category")) {
				
				if (!firstArgument) {	
					
					jbossQl.append("AND ");		
					
				} else {		
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bl.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;
				
			}	

			if (criteria.containsKey("location")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {		       	  	
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bl.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;
				
			}	
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bl.invBuildUnbuildAssembly.buaDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
				
			}
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bl.invBuildUnbuildAssembly.buaDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			
			
			
if (criteria.containsKey("dueDateFrom")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bl.invBuildUnbuildAssembly.buaDueDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dueDateFrom");
				ctr++;
				
			}
			
			if (criteria.containsKey("dueDateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bl.invBuildUnbuildAssembly.buaDueDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dueDateTo");
				ctr++;
				
			}
			
			if(adBrnchList.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			} else {
			    
			    if (!firstArgument) {
			        
			        jbossQl.append("AND ");
			        
			    } else {
			        
			        firstArgument = false;
			        jbossQl.append("WHERE ");
			        
			    }	      
			    
			    jbossQl.append("bl.invBuildUnbuildAssembly.buaAdBranch in (");
			    
			    boolean firstLoop = true;
			    
			    Iterator i = adBrnchList.iterator();
			    
			    while(i.hasNext()) {
			        
			        if(firstLoop == false) { 
			            jbossQl.append(", "); 
			        }
			        else { 
			            firstLoop = false; 
			        }
			        
			        AdBranchDetails mdetails = (AdBranchDetails) i.next();
			        
			        jbossQl.append(mdetails.getBrCode());
			        
			    }
			    
			    jbossQl.append(") ");
			    
			    firstArgument = false;
			    
			}

			jbossQl.append("AND bl.invBuildUnbuildAssembly.buaReceiving = 0 AND bl.invBuildUnbuildAssembly.buaAdCompany=" + AD_CMPNY + " ");  	  	  

			System.out.println("jbossQl="+jbossQl.toString());
			String orderBy = null;
	          
	          if (ORDER_BY.equals("DATE")) {
		      	 
		      	  orderBy = "bl.invBuildUnbuildAssembly.buaDate";
		      	  
		      } else if (ORDER_BY.equals("ITEM NAME")) {
		      	
		      	  orderBy = "bl.invItemLocation.invItem.iiName";
		      	
		      } else if (ORDER_BY.equals("ITEM DESCRIPTION")) {
		      	
		      	  orderBy = "bl.invItemLocation.invItem.iiDescription";
		      	
		      } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
		      	
		      	  orderBy = "bl.invBuildUnbuildAssembly.buaDocumentNumber";
		      	
		      } else if (ORDER_BY.equals("LOCATION")) {
		      	
		      	  orderBy = "bl.invItemLocation.invLocation.locName";
		      	
		      } else if (ORDER_BY.equals("BO NUMBER")) {
		      	
		      	  orderBy = "bl.invBuildUnbuildAssembly.buaCode";
		      	
		      }

			  if (orderBy != null) {
			  
			  	jbossQl.append("ORDER BY " + orderBy);
			  	
			  }     
				 			
			Collection invBuildUnbuildAssemblyLines = null;
			

			try {
				
				invBuildUnbuildAssemblyLines = invBuildUnbuildAssemblyLineHome.getBolByCriteria(jbossQl.toString(), obj);
				
			} catch (FinderException ex)	{
				
				throw new GlobalNoRecordFoundException ();
				
			} 	
			
			if (invBuildUnbuildAssemblyLines.isEmpty())
				throw new GlobalNoRecordFoundException ();
			
			Iterator i = invBuildUnbuildAssemblyLines.iterator();
			
			while (i.hasNext()) {
				
				LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine) i.next();
				
				
				InvRepBuildUnbuildAssemblyOrderDetails details = new InvRepBuildUnbuildAssemblyOrderDetails();
				

				Collection invBillofMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
    			
    			System.out.println("invBillofMaterials="+invBillofMaterials.size());
    			Iterator x = invBillofMaterials.iterator();
    			
    			while(x.hasNext()) {
    				
    				LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)x.next();
    				
    				InvRepBuildUnbuildAssemblyOrderDetails bomDetails = new InvRepBuildUnbuildAssemblyOrderDetails();

    				bomDetails.setBoDate(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDate());
    				LocalAdBranch adBranchCode = adBranchHome.findByPrimaryKey(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaAdBranch());
    				bomDetails.setBoBranchCode(adBranchCode.getBrBranchCode());
    				bomDetails.setBoItemName(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName());
    				
    				
    				bomDetails.setBoLocation(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName());
    				bomDetails.setBoUnit(invBuildUnbuildAssemblyLine.getInvUnitOfMeasure().getUomName());
    				bomDetails.setBoCustomerName(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getArCustomer().getCstName());
    				bomDetails.setBoDocNo(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDocumentNumber());
    				bomDetails.setBoNumber(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaCode().toString());
    				
    				bomDetails.setBoQuantity(invBuildUnbuildAssemblyLine.getBlBuildQuantity());
    				bomDetails.setBoSpecificGravity(invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity());
    				bomDetails.setBoStandardFillSize(invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize());
    				
    				bomDetails.setBoUnitCost(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost());
    				bomDetails.setBoDescription(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDescription());
    				bomDetails.setBoAmount((invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost() * invBuildUnbuildAssemblyLine.getBlBuildQuantity()));
    				bomDetails.setBoReferenceNumber(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaReferenceNumber());
    				bomDetails.setBoItemDescription(
    						invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiDescription());
    				
    				bomDetails.setBoCreatedBy(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaCreatedBy());
    				bomDetails.setOrderBy(ORDER_BY);
    				if(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaVoid() == 1)
    					bomDetails.setBoVoid("YES");
    				else
    					bomDetails.setBoVoid("NO");

    				obj =  new Object [2] ;

    				obj[0] = invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    				obj[1] = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName();

    				Collection invReceivingBuildUnbuildAssemblyLines2 = null;
    				
    				try {
    					invReceivingBuildUnbuildAssemblyLines2 = invBuildUnbuildAssemblyLineHome.getBolByCriteria(
    						"SELECT OBJECT (bl) FROM InvBuildUnbuildAssemblyLine bl WHERE bl.invBuildUnbuildAssembly.buaReceiveBoNumber =?1"
    						+ " and bl.invItemLocation.invItem.iiName=?2"
    						, obj);
    				
    				} catch (FinderException ex) { }
    				
    				double TTL_RCVNG_QTY = 0d;
    				
    				if (!invBuildUnbuildAssemblyLines.isEmpty()) {
    					
    					Iterator j = invReceivingBuildUnbuildAssemblyLines2.iterator();

    					while (j.hasNext()){
    						
    						LocalInvBuildUnbuildAssemblyLine receivingItem = (LocalInvBuildUnbuildAssemblyLine) j.next();

    						TTL_RCVNG_QTY += receivingItem.getBlBuildQuantity();
    						
    					}
    					
    					double REMAINING_QTY = invBuildUnbuildAssemblyLine.getBlBuildQuantity() - TTL_RCVNG_QTY;
            			
    					bomDetails.setBoRemaining(REMAINING_QTY);
    					bomDetails.setBoReceived(TTL_RCVNG_QTY);
    					
    				} else {
    					

    					bomDetails.setBoReceived(0);
    					
    				}
    				
    				
    				if(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaApprovedRejectedBy()!=null)
    					bomDetails.setBoApprovedRejectedBy(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaApprovedRejectedBy());
    				
	
    				
    				LocalInvItem invItemBill = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);
    				
    				Collection invItemBillLocations = invItemBill.getInvItemLocations();
    				
    				Iterator y = invItemBillLocations.iterator();
    				
    				double bomQuantityOnHand = 0d;
    				
    				while(y.hasNext()) {
    					
    					LocalInvItemLocation invItemLocation = (LocalInvItemLocation)y.next();
    					String BOM_II_NM  = invBillOfMaterial.getBomIiName();
    					String BOM_LOC_NM = invBillOfMaterial.getBomLocName();
    					String BOM_UOM_NM = invBillOfMaterial.getInvUnitOfMeasure().getUomName();
    					
    					bomQuantityOnHand += this.getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(
    							BOM_II_NM, BOM_LOC_NM, BOM_UOM_NM, invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaAdBranch(), AD_CMPNY);
						

    				}
    				
    				
    				bomDetails.setBoBmIiCode(invItemBill.getIiName());
    				bomDetails.setBoBmIiName(invItemBill.getIiDescription());
    				bomDetails.setBoBmUomName(invItemBill.getInvUnitOfMeasure().getUomName());
    				bomDetails.setBoBmQuantityNeeded(invBillOfMaterial.getBomQuantityNeeded());
    				bomDetails.setBoBmQuantityOnHand(bomQuantityOnHand);
    				bomDetails.setBoBmCost(invItemBill.getIiUnitCost());
    				bomDetails.setBoBmCategory(invItemBill.getIiAdLvCategory());
            		
    				
    				System.out.println("BOM="+invBillOfMaterial.getBomIiName() +" QOH= "+bomQuantityOnHand + " QND="+invBillOfMaterial.getBomQuantityNeeded());
    				
    				invBuildUnbuildAssemblyOrderList.add(bomDetails);
    			}
    			
    			
				
				
	
				
			}
			
		
			
			if (invBuildUnbuildAssemblyOrderList.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}  
			
			Collections.sort(invBuildUnbuildAssemblyOrderList, InvRepBuildUnbuildAssemblyOrderDetails.DocumentNumberComparator);
			
			
			return invBuildUnbuildAssemblyOrderList;	  
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/ 
    public double getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(String II_NM, String LOC_NM, String UOM_NM, Integer AD_BRNCH, Integer AD_CMPNY) {
        
        Debug.print("InvRepBuildUnbuildAssemblyControllerBean getInvCstRemainingQuantityByIiNameAndLocNameAndUomName");
        
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        LocalInvItemLocation invItemLocation = null;
        LocalInvCosting invCosting = null;
        
        double QTY = 0d;
        
        try {

        	try{

        		invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);

            	invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

            	QTY = invCosting.getCstRemainingQuantity();

        	}  catch (FinderException ex) {

        		if (invCosting == null && invItemLocation != null && invItemLocation.getIlCommittedQuantity() > 0) {

        			QTY = 0d;

        		} else {

        			return QTY;

        		}

        	}

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	

        	LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);

        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItemLocation.getInvItem().getIiName(), invUnitOfMeasure.getUomName(), AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItemLocation.getInvItem().getIiName(), invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(QTY * invUnitOfMeasureConversion.getUmcConversionFactor()/ invDefaultUomConversion.getUmcConversionFactor() , adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepBuildUnbuildAssemblyControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/   
	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException{
	    
	    Debug.print("InvRepBuildUnbuildAssemblyControllerBean getAdBrResAll");
	    
	    LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
	    LocalAdBranchHome adBranchHome = null;
	    
	    LocalAdBranchResponsibility adBranchResponsibility = null;
	    LocalAdBranch adBranch = null;
	    
	    Collection adBranchResponsibilities = null;
	    
	    ArrayList list = new ArrayList();
	    
	    // Initialize EJB Home
	    
	    try {
	        
	        adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
	        adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
	        lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
	        
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	    
	    try {
	        
	        adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
	        
	    } catch (FinderException ex) {
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    if (adBranchResponsibilities.isEmpty()) {
	        
	        throw new GlobalNoRecordFoundException();
	        
	    }
	    
	    try {
	        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	            
	            adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	            
	            adBranch = adBranchResponsibility.getAdBranch();
	            
	            AdBranchDetails details = new AdBranchDetails();
	            
	            details.setBrCode(adBranch.getBrCode());
	            details.setBrBranchCode(adBranch.getBrBranchCode());
	            details.setBrName(adBranch.getBrName());
	            details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
	            
	            list.add(details);
	            
	        }	               
	        
	    } catch (Exception ex) {
	        
	        throw new EJBException(ex.getMessage());
	    }
	    
	    return list;
	    
	}    
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
	    
	    Debug.print("InvRepBuildUnbuildAssemblyControllerBean ejbCreate");
	    
	}
	
}

