
/*
 * InvRepAdjustmentEntryPrintControllerBean.java
 *
 * Created on September 19,2006 2:58 PM
 *
 * @author  Rey B. Limosenero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApPurchaseRequisitionLine;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvBuildUnbuildAssembly;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLine;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvStockTransferLine;
import com.ejb.inv.LocalInvStockTransferLineHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ApModPurchaseRequisitionLineDetails;
import com.util.ArRepSalesDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepBuildUnbuildPrintDetails;

/**
 * @ejb:bean name="InvRepBuildUnbuildPrintControllerEJB"
 *           display-name="Use for printing adjustment"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepBuildUnbuildPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepBuildUnbuildPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepBuildUnbuildPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class InvRepBuildUnbuildPrintControllerBean extends AbstractSessionBean {
    

	
	private double getShUnpostedQuantity(LocalAdBranchItemLocation adBranchItemLocation, Date AS_OF_DT, Integer AD_CMPNY)
	{
    	Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getUnpostedQuantity");
		
		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null; 
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
		LocalInvStockTransferLineHome invStockTransferLineHome = null;
		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;

		double totalUnpostedQuantity = 0d;
		
		// Initialize EJB Home		
		System.out.println("CheckPoint executeInvRepStockOnHand K");
		try 
		{
			
			apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
			invStockTransferLineHome = (LocalInvStockTransferLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class);
			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
		
		} 
		catch (NamingException ex) 
		{
			throw new EJBException(ex.getMessage());
		}
		System.out.println("CheckPoint executeInvRepStockOnHand L");
		try
		{
			double unpostedQuantity = 0d;
			
			String itemName = adBranchItemLocation.getInvItemLocation().getInvItem().getIiName();
			String locationName = adBranchItemLocation.getInvItemLocation().getInvLocation().getLocName();
			Integer locationCode = adBranchItemLocation.getInvItemLocation().getInvLocation().getLocCode();
			Integer branchCode = adBranchItemLocation.getAdBranch().getBrCode();

			
			//(1) AP Voucher Line Item - AP Voucher
			byte debitMemo;
			
			Collection apVoucherLineItems = apVoucherLineItemHome.findUnpostedVouByIiNameAndLocNameAndLessEqualDateAndVouAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
	
			Iterator x = apVoucherLineItems.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)x.next();
	
				unpostedQuantity = apVoucherLineItem.getVliQuantity();

				System.out.println("(1) Name: " + apVoucherLineItem.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);								
				
				debitMemo = apVoucherLineItem.getApVoucher().getVouDebitMemo(); 
				
				if (debitMemo == 0)
				{
					totalUnpostedQuantity += unpostedQuantity;
				}
				else
				{
					totalUnpostedQuantity -= unpostedQuantity;
				}
			}

			
			//(2) AP Voucher Line Item - AP Check
			apVoucherLineItems = apVoucherLineItemHome.findUnpostedChkByIiNameAndLocNameAndLessEqualDateAndChkAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
	
			x = apVoucherLineItems.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)x.next();
	
				unpostedQuantity = apVoucherLineItem.getVliQuantity();
										
				System.out.println("(2) Name: " + apVoucherLineItem.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				totalUnpostedQuantity += unpostedQuantity;
			
			}
			
			
			//(3) AP PURCHASE ORDER LINE								
			Collection apPurchaseOrderLines = apPurchaseOrderLineHome.findUnpostedPoByIiNameAndLocNameAndLessEqualDateAndPoAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = apPurchaseOrderLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)x.next();
	
				unpostedQuantity = apPurchaseOrderLine.getPlQuantity();
				
				System.out.println("(3) Name: " + apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				totalUnpostedQuantity += unpostedQuantity;
			
			}																									
			
			
			//(4) AR Invoice Line Item - AR Invoice
			byte creditMemo;
			
			Collection arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = arInvoiceLineItems.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)x.next();
	
				unpostedQuantity = arInvoiceLineItem.getIliQuantity();
				
				System.out.println("(4) Name: " + arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				creditMemo = arInvoiceLineItem.getArInvoice().getInvCreditMemo(); 
				
				if (creditMemo == 0)
				{
					totalUnpostedQuantity -= unpostedQuantity;
				}
				else
				{
					totalUnpostedQuantity += unpostedQuantity;
				}
			}
			
			
			//(5) AR Invoice Line Item - AR Receipt
			arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedRctByIiNameAndLocNameAndLessEqualDateAndRctAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
	
			x = arInvoiceLineItems.iterator();

			while (x.hasNext()) 
			{
				
				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)x.next();
	
				unpostedQuantity = arInvoiceLineItem.getIliQuantity();
														
				System.out.println("(5) Name: " + arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				totalUnpostedQuantity -= unpostedQuantity;
			
			}
			
			
			//(6)AR Sales Order Invoice Line
			Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome.findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = arSalesOrderInvoiceLines.iterator();
			
			while(x.hasNext())
			{
				LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)x.next();
				
				unpostedQuantity = arSalesOrderInvoiceLine.getSilQuantityDelivered();

				System.out.println("(6) Name: " + arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);				
				
				totalUnpostedQuantity -= unpostedQuantity;
				
			}
			
			
			//(7) INV Adjustment Line
			Collection invAdjustmentLines = invAdjustmentLineHome.findUnpostedAdjByIiNameAndLocNameAndLessEqualDateAndAdjAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = invAdjustmentLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)x.next();
	
				unpostedQuantity = invAdjustmentLine.getAlAdjustQuantity();
				
				System.out.println("(7) Name: " + invAdjustmentLine.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				totalUnpostedQuantity += unpostedQuantity;
			}																									
			

			//(8) INV Build-Unbuild Assembly
			Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssemblyLineHome.findUnpostedBuaByIiNameAndLocNameAndLessEqualDateAndBuaAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = invBuildUnbuildAssemblyLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)x.next();
	
				unpostedQuantity = invBuildUnbuildAssemblyLine.getBlBuildQuantity();
				
				System.out.println("(8) Name: " + invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);

				if (unpostedQuantity >= 0)
				{
					totalUnpostedQuantity += unpostedQuantity;
				}
				else if (unpostedQuantity < 0)
				{
					totalUnpostedQuantity -= unpostedQuantity;					
				}
			}	
			
			
			//(8.1) INV Build-Unbuild Assembly
			Collection invBuildUnbuildAssemblyLines2 = invBuildUnbuildAssemblyLineHome.findUnpostedBuaByIiNameAndLocNameAndLessEqualDateAndBuaAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = invBuildUnbuildAssemblyLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)x.next();
	
				
				Collection invBillofMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();

				Iterator y = invBillofMaterials.iterator();
    			
    			while(y.hasNext()) {
    				
    				LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)y.next();
    				

					
    				double qtyNeeded = invBillOfMaterial.getBomQuantityNeeded();
    				
    				unpostedQuantity =( invBuildUnbuildAssemblyLine.getBlBuildQuantity() ) * qtyNeeded / 100;
    				
    				
    				if (unpostedQuantity >= 0)
    				{
    					totalUnpostedQuantity += unpostedQuantity;
    				}
    				else if (unpostedQuantity < 0)
    				{
    					totalUnpostedQuantity -= unpostedQuantity;					
    				}
    				
    			}
				
				

			
			}
			
		
			//(9) INV Stock Transfer Line
			//Series of System.out's were executed in order to trace the ADDITION and DEDUCTION of unposted quantities for each item.
			Collection invStockTransferLines = invStockTransferLineHome.findUnpostedStlByIiNameAndLocCodeAndLessEqualDateAndStAdBranch(itemName, locationCode, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = invStockTransferLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine)x.next();
	
				unpostedQuantity = invStockTransferLine.getStlQuantityDelivered();
				
				System.out.println("(9) Item Name: " + invStockTransferLine.getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				System.out.println("Location Code" +  locationCode);
				System.out.println("Location TO: " + invStockTransferLine.getStlLocationTo());
				System.out.println("Location FROM: " + invStockTransferLine.getStlLocationFrom());
				
				if (invStockTransferLine.getStlLocationTo().equals(locationCode))
				{
					System.out.println("(9) Before Adding: Name: " + invStockTransferLine.getInvItem().getIiName() + "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity" + totalUnpostedQuantity);
					totalUnpostedQuantity += unpostedQuantity;
					System.out.println("(9) After Adding: Name: " + invStockTransferLine.getInvItem().getIiName() + "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity" + totalUnpostedQuantity);
				}
				else if (invStockTransferLine.getStlLocationFrom().equals(locationCode))
				{
					System.out.println("(9) Before Deducting: Name: " + invStockTransferLine.getInvItem().getIiName() + "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity" + totalUnpostedQuantity);					
					totalUnpostedQuantity -= unpostedQuantity;
					System.out.println("(9) After Deducting: Name: " + invStockTransferLine.getInvItem().getIiName() + "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity" + totalUnpostedQuantity);
				}
			}			

			
			//(10) INV Branch Stock Transfer Line
			String bstType = "";
			
			Collection invBranchStockTransferLines = invBranchStockTransferLineHome.findUnpostedBstByIiNameAndLocNameAndLessEqualDateAndBstAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = invBranchStockTransferLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)x.next();
	
				unpostedQuantity = invBranchStockTransferLine.getBslQuantity();
				
				bstType = invBranchStockTransferLine.getInvBranchStockTransfer().getBstType();
				
				System.out.println("(10) Name: " + invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				if(bstType.equals("IN"))
				{
					totalUnpostedQuantity += unpostedQuantity;
				}
				else if(bstType.equals("OUT"))
				{
					totalUnpostedQuantity -= unpostedQuantity;					
				}
			}																									
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return totalUnpostedQuantity;
		
				
	}
	
private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean convertByUomFromAndItemAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
             
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
        
        Debug.print("InvBuildUnbuildAssemblyEntryControllerBean getInvGpQuantityPrecisionUnit");
        
        LocalAdPreferenceHome adPreferenceHome = null;         
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            return adPreference.getPrfInvQuantityPrecisionUnit();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeInvRepBuildUnbuildPrint(ArrayList buaCodeList, String REPORT_TYPE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvRepBuildUnbuildPrintControllerBean executeInvRepBuildUnbuildPrint");
        
        LocalInvBuildUnbuildAssemblyHome invBuildUnbuildAssemblyHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
        LocalInvTagHome invTagHome = null;
        LocalInvCostingHome invCostingHome = null;
        
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
        	invBuildUnbuildAssemblyHome = (LocalInvBuildUnbuildAssemblyHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvBuildUnbuildAssemblyHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
        	invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
        	invTagHome = (LocalInvTagHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
        	invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
                    lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
        	
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = buaCodeList.iterator();
        	
        	while (i.hasNext()) {
        		
        		Integer BUA_CODE = (Integer) i.next();
        		LocalInvBuildUnbuildAssembly invBuildUnbuildAssembly = null;
        		
        		
        		try {
        			
        			invBuildUnbuildAssembly = invBuildUnbuildAssemblyHome.findByPrimaryKey(BUA_CODE);
        			
        		} catch (FinderException ex) {
        			
        			continue;
        			
        		}	        	  	         
        		
        		// get adjustment lines 
        		
        		Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssembly.getInvBuildUnbuildAssemblyLines();
        		Iterator buaIter = invBuildUnbuildAssemblyLines.iterator();
        		String ITEM_NAME = "";
        		String ITEM_DESC = "";
        		String BLD_QTY = "";
        		while(buaIter.hasNext()) {
        			
        			LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)buaIter.next();        		   
        			InvRepBuildUnbuildPrintDetails details = new InvRepBuildUnbuildPrintDetails();
        			
        			details.setBupBuaDate(invBuildUnbuildAssembly.getBuaDate());        			
        			details.setBupBuaDocumentNumber(invBuildUnbuildAssembly.getBuaDocumentNumber()); 
        			details.setBupBuaCustomerName(invBuildUnbuildAssembly.getArCustomer().getCstName());
        			details.setBupBuaReferenceNumber(invBuildUnbuildAssembly.getBuaReferenceNumber());
        			details.setBupBuaDescription(invBuildUnbuildAssembly.getBuaDescription());
        			details.setBupBuaCreatedBy(invBuildUnbuildAssembly.getBuaCreatedBy());
        			ITEM_NAME += invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName() + "\n" ;
        			ITEM_DESC += invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiDescription() + "\n" ;
        			BLD_QTY += invBuildUnbuildAssemblyLine.getBlBuildQuantity() + "\n";
        			
        			details.setBupBlIiDesc(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiDescription());
        			details.setBupBlLocName(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName());
        			details.setBupBlUomName(invBuildUnbuildAssemblyLine.getInvUnitOfMeasure().getUomName());
        			details.setBupBlBuildQuantity(invBuildUnbuildAssemblyLine.getBlBuildQuantity());
        			double BUILD_QTY = invBuildUnbuildAssemblyLine.getBlBuildQuantity();
        			Collection invBillofMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();
        			
        			System.out.println("invBillofMaterials="+invBillofMaterials.size());
        			Iterator x = invBillofMaterials.iterator();
        			
        			
        			while(x.hasNext()) {
        			
        				LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)x.next();

        				InvRepBuildUnbuildPrintDetails bomDetails = new InvRepBuildUnbuildPrintDetails();
        				
        				int AD_BRNCH = invBuildUnbuildAssembly.getBuaAdBranch();
        				String II_NM  = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName();
    					String LOC_NM = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName();
    					String UOM_NM = invBuildUnbuildAssemblyLine.getInvUnitOfMeasure().getUomName();
 
        				bomDetails.setBupBuaDate(invBuildUnbuildAssembly.getBuaDate()); 
        				
        				bomDetails.setBupBuaDocumentNumber(invBuildUnbuildAssembly.getBuaDocumentNumber()); 
        				bomDetails.setBupBuaCustomerName(invBuildUnbuildAssembly.getArCustomer().getCstName());
        				bomDetails.setBupBuaReferenceNumber(invBuildUnbuildAssembly.getBuaReferenceNumber());
        				bomDetails.setBupBuaDescription(invBuildUnbuildAssembly.getBuaDescription());
        				bomDetails.setBupBuaCreatedBy(invBuildUnbuildAssembly.getBuaCreatedBy());

            			
        				bomDetails.setBupBlIiName(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName());
        				bomDetails.setBupBlIiDesc(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiDescription());
        				bomDetails.setBupBlIiCategory(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiAdLvCategory());
        				System.out.println("ITEM_NAME ARRAY="+ITEM_NAME);
        				System.out.println("ITEM_DESC ARRAY="+ITEM_DESC);
        				bomDetails.setBupBlIiNameArray(ITEM_NAME);
        				bomDetails.setBupBlIiDescArray(ITEM_DESC);
        				
        				
        				bomDetails.setBupBlLocName(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvLocation().getLocName());
        				bomDetails.setBupBlUomName(invBuildUnbuildAssemblyLine.getInvUnitOfMeasure().getUomName());
        				bomDetails.setBupBlBuildSpecificGravity(invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity());
        				bomDetails.setBupBlBuildStandardFillSize(invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize());
        				bomDetails.setBupBlBuildQuantity(invBuildUnbuildAssemblyLine.getBlBuildQuantity());
        				bomDetails.setBupBlBuildQuantityArray(BLD_QTY);
        				
        				
        				double RCVD_QTY = 0d;
        				Collection invBuildUnbuildAssemblyLines2 = invBuildUnbuildAssemblyLineHome.findByBuaRcvBuaNumberAndBuaReceivingAndCstCustomerCodeAndBrCode(
            					invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDocumentNumber(), invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getArCustomer().getCstCustomerCode(), invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaAdBranch(), AD_CMPNY);
            			Iterator j = invBuildUnbuildAssemblyLines2.iterator();
            			
            			while (j.hasNext()) { 
            				LocalInvBuildUnbuildAssemblyLine invBuildUnBuildAssemblyItemLine = (LocalInvBuildUnbuildAssemblyLine) j.next();

            				RCVD_QTY += invBuildUnBuildAssemblyItemLine.getBlBuildQuantity();

            			}

            			double REMAINING_QTY = invBuildUnbuildAssemblyLine.getBlBuildQuantity() - RCVD_QTY;
            			
            			bomDetails.setBupBlRemaining(REMAINING_QTY);
            			bomDetails.setBupBlReceived(RCVD_QTY);

        				bomDetails.setBupBlQuantityOnHand(
        						this.getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(
        								II_NM, LOC_NM, UOM_NM, AD_BRNCH, AD_CMPNY));

        				bomDetails.setBupBlBmQuantityNeeded(invBillOfMaterial.getBomQuantityNeeded());
        				bomDetails.setBupBlBmUomName(invBillOfMaterial.getInvUnitOfMeasure().getUomName());
        				bomDetails.setBupBlBmIiCode(invBillOfMaterial.getBomIiName());
        				LocalInvItem invItemBill = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);
        				
        				Collection invItemBillLocations = invItemBill.getInvItemLocations();
        				
        				Iterator y = invItemBillLocations.iterator();
        				
        				
        				String BOM_II_NM  = invBillOfMaterial.getBomIiName();
    					String BOM_LOC_NM = invBillOfMaterial.getBomLocName();
    					String BOM_UOM_NM = invBillOfMaterial.getInvUnitOfMeasure().getUomName();
    					
        				
    					Double unpostedQty = 0d;
    					
    					System.out.println("BOM_II_NM="+BOM_II_NM);
    					System.out.println("BOM_LOC_NM="+BOM_LOC_NM);
    					System.out.println("BOM_UOM_NM="+BOM_UOM_NM);
    					
    					LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(BOM_II_NM, BOM_LOC_NM, AD_CMPNY);
	        			
    					
    					try{
    						
    						
        					Collection adBranchItemLocations = invItemLocation.getAdBranchItemLocations();
        					
        					Iterator b = adBranchItemLocations.iterator();
        					
        					while(b.hasNext()){
        						
        						LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)b.next();
        						
        						unpostedQty += this.getShUnpostedQuantity(adBranchItemLocation, invBuildUnbuildAssembly.getBuaDate(), invBuildUnbuildAssembly.getBuaAdCompany());
        						
        						
        					}
    						
    					}catch(Exception ex){
    						
    					}
    					
    					bomDetails.setBupBlBmUnpostedQuantity(unpostedQty);
    					
    					double specificGravity = invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity();
                        double standardFillSize = invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize();
                        
    					
    					double BOM_QTY_NDD =
    							invItemLocation.getInvItem().getIiAdLvCategory().contains("RAWMAT") ? 
                                        BUILD_QTY * (invBillOfMaterial.getBomQuantityNeeded()/ 100) * (standardFillSize/1000) * specificGravity
                        		:
                                	this.convertByUomFromAndItemAndQuantity(
                                            invBillOfMaterial.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
                                            EJBCommon.roundIt(   Math.abs(BUILD_QTY) * invBillOfMaterial.getBomQuantityNeeded(),
                                            this.getInvGpQuantityPrecisionUnit(AD_CMPNY)), AD_CMPNY);
                    
                       LocalInvCosting invLastBomCosting = null;
                        
                        try{
                        	
                        	System.out.println("invBuildUnbuildAssembly.getBuaDate()="+invBuildUnbuildAssembly.getBuaDate());
                            System.out.println("BOM_II_NM="+BOM_II_NM);
                            System.out.println("BOM_II_NM="+BOM_LOC_NM);
                            
                        	invLastBomCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                    invBuildUnbuildAssembly.getBuaDate(), BOM_II_NM, BOM_LOC_NM, AD_BRNCH, AD_CMPNY); 
	
                        } catch (FinderException ex){
                        	
                        }
                        
                        System.out.println("invLastBomCosting="+invLastBomCosting);                
                        if(invLastBomCosting != null){

        					System.out.println("BOM_QTY_NDD = "+BOM_QTY_NDD);
        					
        					
    	        			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(invBuildUnbuildAssembly.getBuaDate(), invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY) ;         
    	        			
    	        			System.out.println(invItemLocation.getInvItem().getIiName()+" = invFifoCostings="+invFifoCostings.size());
    	        			
    	        			
    	        			Iterator z = invFifoCostings.iterator();
    	        			
    	        			
    	        			
    	        			double neededQty = BOM_QTY_NDD;
    	        			double remaining = invLastBomCosting.getCstRemainingQuantity();
    	        			System.out.println("remaining="+remaining);

    	        			boolean one = true;
    	        			boolean two = true;
    	        			boolean three = true;
    	        			boolean four = true;
    	        			boolean five = true;
    	        			boolean six = true;
    	        			boolean seven = true;
    	        			boolean eight = true;
    	        			boolean nine = true;
    	        			
    	        			double qcRemainingOthers =0d;
    	        			String qcOthers = "";

    	        			String qcNumber ="";
    	        			while(z.hasNext()) {
    	        				
    	        				
    	        				LocalInvCosting invFifoCosting = (LocalInvCosting)z.next();
    	        				
    	        				System.out.println(invFifoCosting.getCstQCNumber() + " === " + invFifoCosting.getCstRemainingLifoQuantity());
    	        				
    	        				qcNumber +=invFifoCosting.getCstQCNumber() + " ";
    	        				
    	        				if(one) {
    	        					
    	        					bomDetails.setBupBlBmQualityControl1(invFifoCosting.getCstQCNumber());
    	        					bomDetails.setBupBlBmQualityControl1Remaining(invFifoCosting.getCstRemainingLifoQuantity());
    	        					one = false;
    	        					
    	        				} else if(two){
    	        					
    	        					bomDetails.setBupBlBmQualityControl2(invFifoCosting.getCstQCNumber());
    	        					bomDetails.setBupBlBmQualityControl2Remaining(invFifoCosting.getCstRemainingLifoQuantity());
    	        					two = false;
        	        			
    	        				} else {
    	        					qcOthers += invFifoCosting.getCstQCNumber() + " ";
    	        					qcRemainingOthers += invFifoCosting.getCstRemainingLifoQuantity();
    	        					
    	        				}
    	        				
    	        		
    	        			}
    	        			bomDetails.setBupBlBmQualityControlOthers(qcOthers);
    	        			bomDetails.setBupBlBmQualityControlOthersRemaining(qcRemainingOthers);
                        	
                        }
    					
	        			
	
    					
	        			double bomQuantityOnHand = 0d;
        				
        				while(y.hasNext()){
        					
        					LocalInvItemLocation invItemLocation1 = (LocalInvItemLocation)y.next();
        					
        					
        					
        					bomQuantityOnHand += this.getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(
        							BOM_II_NM, BOM_LOC_NM, BOM_UOM_NM, AD_BRNCH, AD_CMPNY);
        					
        				}
        				bomDetails.setBupBlBmQuantityOnHand(bomQuantityOnHand);
        				bomDetails.setBupBlBmIiName(invItemBill.getIiDescription());
        				bomDetails.setBupBlBmCost(invItemBill.getIiUnitCost());
        				bomDetails.setBupBlBmCategory(invItemBill.getIiAdLvCategory());
                		
        				list.add(bomDetails);
        			
        			}
        			
        		}
        		
        	}

        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	if(REPORT_TYPE.equals("EXPLOSION REPORT")){
        		
        		Collections.sort(list, InvRepBuildUnbuildPrintDetails.ItemComparator);
            	
        	}
        	
        	return list;        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }  
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/ 
    public double getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(String II_NM, String LOC_NM, String UOM_NM, Integer AD_BRNCH, Integer AD_CMPNY) {
        
        Debug.print("InvRepBuildUnbuildPrintControllerBean getInvCstRemainingQuantityByIiNameAndLocNameAndUomName");
        
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        LocalInvItemLocation invItemLocation = null;
        LocalInvCosting invCosting = null;
        
        double QTY = 0d;
        
        try {

        	try{

        		invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);

            	invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

            	QTY = invCosting.getCstRemainingQuantity();

        	}  catch (FinderException ex) {

        		if (invCosting == null && invItemLocation != null && invItemLocation.getIlCommittedQuantity() > 0) {

        			QTY = 0d;

        		} else {

        			return QTY;

        		}

        	}

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	

        	LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);

        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItemLocation.getInvItem().getIiName(), invUnitOfMeasure.getUomName(), AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItemLocation.getInvItem().getIiName(), invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(QTY * invUnitOfMeasureConversion.getUmcConversionFactor()/ invDefaultUomConversion.getUmcConversionFactor() , adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepBuildUnbuildPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
		
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvRepBuildUnbuildPrintControllerBean ejbCreate");
      
    }
}
