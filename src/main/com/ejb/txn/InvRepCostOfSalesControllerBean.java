package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArSalesperson;
import com.ejb.ar.LocalArSalespersonHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPhysicalInventoryHome;
import com.ejb.inv.LocalInvPhysicalInventoryLineHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepCostOfSaleDetails;

/**
* @ejb:bean name="InvRepCostOfSalesControllerEJB"
*           display-name="Used for generation of cost of sales reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/InvRepCostOfSalesControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.InvRepCostOfSalesController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.InvRepCostOfSalesControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class InvRepCostOfSalesControllerBean extends AbstractSessionBean {
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepCostOfSalesControllerBean getAdLvReportTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV REPORT TYPE - COST OF SALES", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepCostOfSalesControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepCostOfSalesControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeInvRepCostOfSales(HashMap criteria, String costingMethod, ArrayList categoryList, ArrayList branchList, Integer AD_CMPNY)
		throws GlobalNoRecordFoundException {
		
		Debug.print("InvRepCostOfSalesControllerBean executeInvRepCostOfSales");

		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
		LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;     
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
			invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(cst) FROM InvCosting cst ");
			
			boolean firstArgument = true;
			short ctr = 0;	      
			int criteriaSize = 0;
			
			
			Object obj[] = null;
			
			if (branchList.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			else {
				
				jbossQl.append(" WHERE cst.cstAdBranch in (");
				
				boolean firstLoop = true;
				
				Iterator j = branchList.iterator();
				
				while(j.hasNext()) {
					
					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}
					
					AdBranchDetails mdetails = (AdBranchDetails) j.next();
					
					jbossQl.append(mdetails.getBrCode());
					
				}
				
				jbossQl.append(") ");
				
				firstArgument = false;
				
			}                    
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("location")) {
				
				criteriaSize++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				criteriaSize++;
				
			}
			
			if (criteria.containsKey("dateTo")) {
				
				criteriaSize++;
				
			}
			
			if (criteria.containsKey("itemClass")) {
			
				criteriaSize++;
			
			}
			

		
			
			
			obj = new Object[criteriaSize + categoryList.size()];
			
			
			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {
					jbossQl.append("AND ");	
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("cst.invItemLocation.invItem.iiName  LIKE '%" + (String)criteria.get("itemName") + "%' ");

			}
			
			System.out.println("categoryList.size(): "+categoryList.size());
			
			if (!categoryList.isEmpty()) {
				
				Iterator iter = categoryList.iterator();
				
				jbossQl.append(" AND ( ");
				
				boolean isfirstcategory = true;
				
				while(iter.hasNext()) {

					String category = (String)iter.next();
					
					if(!isfirstcategory)
						jbossQl.append(" OR");
					
					isfirstcategory = false;
					
					jbossQl.append(" cst.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1));
					obj[ctr] = category;
					ctr++;
				}	
				
				jbossQl.append(" ) ");
				
			}
			/*if (criteria.containsKey("category")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("cst.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;
				
			}	*/
			
			if (criteria.containsKey("location")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("cst.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("cst.cstDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("cst.cstDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("itemClass")) {
		   	
		   	  if (!firstArgument) {		       	  	
		   	     jbossQl.append("AND ");		       	     
		   	  } else {		       	  	
		   	  	 firstArgument = false;
		   	  	 jbossQl.append("WHERE ");		       	  	 
		   	  }
		   	  
		   	  jbossQl.append("cst.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
		   	  obj[ctr] = (String)criteria.get("itemClass");
		   	  ctr++;
		   	  
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
	       	  	 jbossQl.append("WHERE ");
	       	  	 
	       	}
	       	  
	       	jbossQl.append("cst.cstQuantitySold <> 0 AND cst.cstAdCompany=" + AD_CMPNY + " ");
			
			jbossQl.append("ORDER BY cst.invItemLocation.invItem.iiName, cst.cstAdBranch, cst.cstDate, cst.cstDateToLong, cst.cstLineNumber");
			
			Collection invCostings = invCostingHome.getCstByCriteria(jbossQl.toString(), obj);
			
			//double SLS_QTY_SLD = 0d;
			double SLS_AMOUNT = 0d;
			double COS = 0d;
	
			double taxRate = 0d;
  			
			Iterator i = invCostings.iterator();	
			
			while (i.hasNext()) {
				
				LocalInvCosting invCosting = (LocalInvCosting) i.next();
				
				InvRepCostOfSaleDetails details = new InvRepCostOfSaleDetails();
				
				details.setCsItemName(invCosting.getInvItemLocation().getInvItem().getIiName());
				details.setCsItemDescription(invCosting.getInvItemLocation().getInvItem().getIiDescription());
				details.setCsDate(invCosting.getCstDate());
				details.setCsQuantitySold(invCosting.getCstQuantitySold());
				details.setCsUnitCost(invCosting.getInvItemLocation().getInvItem().getIiUnitCost());
				details.setCsUnit(invCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
				details.setCsBranchCode(this.getAdBranchCode(invCosting.getCstAdBranch()));

				
				details.setCsStockOnHand(
				this.getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(
						invCosting.getInvItemLocation().getInvItem().getIiName(), invCosting.getInvItemLocation().getInvLocation().getLocName(), 
						invCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName(), invCosting.getCstAdBranch(), AD_CMPNY)
						);					
				if (invCosting.getArInvoiceLineItem() != null) {
					
					
					if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {

				     //   SLS_AMOUNT = invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount();

						if (criteria.containsKey("salesperson")) {
						
							String salesperson = (String)criteria.get("salesperson");
							
							if(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson()!=null) {
								
								if(!salesperson.equals(	invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpName())) {
									continue;
								}

							}else {
								continue;
							}
							
						
						}
												
						if(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson()!=null) {
							
							details.setCsSalespersonName(invCosting.getArInvoiceLineItem().getArInvoice().getArSalesperson().getSlpName());
							
							
						}
						
				        SLS_AMOUNT = invCosting.getArInvoiceLineItem().getIliAmount() ;

				        if (invCosting.getArInvoiceLineItem().getArInvoice().getInvCreditMemo() == 0) {
					        
				        	taxRate = invCosting.getArInvoiceLineItem().getArInvoice().getArTaxCode().getTcRate() / 100;
				        	
					        details.setCsSalesAmount(SLS_AMOUNT);
					        details.setCsGrossProfit(SLS_AMOUNT - details.getCsCostOfSales());
							details.setCsPercentProfit(details.getCsGrossProfit() / SLS_AMOUNT);
												
					    } else {
					        details.setCsSalesAmount(SLS_AMOUNT * -1);
					        details.setCsGrossProfit((SLS_AMOUNT - details.getCsCostOfSales()) * -1);
							details.setCsPercentProfit(details.getCsGrossProfit() / SLS_AMOUNT * -1);
					    }
					
						if (costingMethod.equals("Average")) {
							details.setCsCostOfSales(EJBCommon.roundIt(invCosting.getCstCostOfSales() / invCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
						} else {
							details.setCsCostOfSales(EJBCommon.roundIt(invCosting.getInvItemLocation().getInvItem().getIiUnitCost() / (1 + taxRate), (short)2));				
						}
						
						details.setCsCustomerCode(invCosting.getArInvoiceLineItem().getArInvoice().getArCustomer().getCstCustomerCode());
						details.setCsDocumentNumber(invCosting.getArInvoiceLineItem().getArInvoice().getInvNumber());
					}
					if (invCosting.getArInvoiceLineItem().getArReceipt() != null) {
						
						
						
						if (criteria.containsKey("salesperson")) {
							
							String salesperson = (String)criteria.get("salesperson");
							
							if(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson()!=null) {
								
								if(!salesperson.equals(	invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpName())) {
									continue;
								}

							}else {
								continue;
							}
							
						
						}

						
						if(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson()!=null) {
							
							details.setCsSalespersonName(invCosting.getArInvoiceLineItem().getArReceipt().getArSalesperson().getSlpName());
							
							
						}
						
						
					  //SLS_AMOUNT = invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount();
						
						SLS_AMOUNT = invCosting.getArInvoiceLineItem().getIliAmount() ;

						taxRate = invCosting.getArInvoiceLineItem().getArReceipt().getArTaxCode().getTcRate() / 100;
						
						if (costingMethod.equals("Average")) {
							details.setCsCostOfSales(EJBCommon.roundIt(invCosting.getCstCostOfSales() / invCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
						} else {
							details.setCsCostOfSales(EJBCommon.roundIt(invCosting.getInvItemLocation().getInvItem().getIiUnitCost() / (1 + taxRate), (short)2));				
						}
						
						details.setCsCustomerCode(invCosting.getArInvoiceLineItem().getArReceipt().getArCustomer().getCstCustomerCode());
						details.setCsDocumentNumber(invCosting.getArInvoiceLineItem().getArReceipt().getRctNumber());

						details.setCsSalesAmount(SLS_AMOUNT);
				        details.setCsGrossProfit(SLS_AMOUNT - details.getCsCostOfSales());
						details.setCsPercentProfit(details.getCsGrossProfit() / SLS_AMOUNT);
					}
					
				} else if (invCosting.getArSalesOrderInvoiceLine() != null) {
					
					details.setCsUnit(invCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());					
					
					if (invCosting.getArSalesOrderInvoiceLine().getArInvoice() != null) {

						
						if (criteria.containsKey("salesperson")) {
							
							String salesperson = (String)criteria.get("salesperson");
							
							if(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson()!=null) {
								
								if(!salesperson.equals(	invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpName())) {
									continue;
								}

							}else {
								continue;
							}
							
						
						}
						
						if(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson()!=null) {
							
							details.setCsSalespersonName(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArSalesperson().getSlpName());
							
							
						}
						
					//	SLS_AMOUNT = invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount();
						
						SLS_AMOUNT = invCosting.getArSalesOrderInvoiceLine().getSilAmount() ;
						
						if (invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvCreditMemo() == 0) {
					        
							taxRate = invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArTaxCode().getTcRate() / 100;
				        	
					        details.setCsSalesAmount(SLS_AMOUNT);
					        details.setCsGrossProfit(SLS_AMOUNT - details.getCsCostOfSales());
							details.setCsPercentProfit(details.getCsGrossProfit() / SLS_AMOUNT);
												
					    } else {
					        details.setCsSalesAmount(SLS_AMOUNT * -1);
					        details.setCsGrossProfit((SLS_AMOUNT - details.getCsCostOfSales()) * -1);
							details.setCsPercentProfit(details.getCsGrossProfit() / SLS_AMOUNT * -1);
					    }

						if (costingMethod.equals("Average")) {
							details.setCsCostOfSales(EJBCommon.roundIt(invCosting.getCstCostOfSales() / invCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY)));
						} else {
							details.setCsCostOfSales(EJBCommon.roundIt(invCosting.getInvItemLocation().getInvItem().getIiUnitCost() / (1 + taxRate), (short)2));				
						}
						
						details.setCsCustomerCode(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArCustomer().getCstCustomerCode());
						details.setCsDocumentNumber(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber());
					}
				}
				
				list.add(details);
			}
			
			if (list.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			}		 
			
			return list; 	  
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepCostOfSalesControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	

	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("InvRepCostOfSalesControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	
	

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("InvFindItemLocationControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    // Private Methods
    
    private String getAdBranchCode(Integer AD_BRNCH) {
		
		Debug.print("InvRepCostOfSalesControllerBean getAdBranchCode");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		LocalAdBranchHome adBranchHome = null;
		
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);   
			
			adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
					lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			

			LocalAdBranch adBranch = adBranchHome.findByPrimaryKey(AD_BRNCH);
			
			return  adBranch.getBrBranchCode();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
    
    

    private double getInvCstRemainingQuantityByIiNameAndLocNameAndUomName(String II_NM, String LOC_NM, String UOM_NM, Integer AD_BRNCH, Integer AD_CMPNY) {
        
        Debug.print("InvRepCostOfSalesControllerBean getInvCstRemainingQuantityByIiNameAndLocNameAndUomName");
        
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        LocalInvItemLocation invItemLocation = null;
        LocalInvCosting invCosting = null;
        
        double QTY = 0d;

        try {

        	try{

        		invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_NM, II_NM, AD_CMPNY);

            	invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);

            	QTY = invCosting.getCstRemainingQuantity();

        	}  catch (FinderException ex) {

        		if (invCosting == null && invItemLocation != null && invItemLocation.getIlCommittedQuantity() > 0) {

        			QTY = 0d;

        		} else {

        			return QTY;

        		}

        	}

        	LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	

        	LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);

        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItemLocation.getInvItem().getIiName(), invUnitOfMeasure.getUomName(), AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItemLocation.getInvItem().getIiName(), invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);

        	return EJBCommon.roundIt(QTY * invUnitOfMeasureConversion.getUmcConversionFactor()/ invDefaultUomConversion.getUmcConversionFactor() , adPreference.getPrfInvQuantityPrecisionUnit());

        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	
    
    
    
    
    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getArSlpAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepCostOfSalesControllerBean getArSlpAll");
		
		LocalArSalespersonHome arSalespersonHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			arSalespersonHome = (LocalArSalespersonHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalespersonHome.JNDI_NAME, LocalArSalespersonHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection arSalespersons = arSalespersonHome.findSlpAll(AD_CMPNY);
			
			Iterator i = arSalespersons.iterator();
			
			while (i.hasNext()) {
				
				LocalArSalesperson arSalesperson = (LocalArSalesperson)i.next();
				
				list.add(arSalesperson.getSlpName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
    
    
    
	// SessionBean methods
	
	/**
* @ejb:create-method view-type="remote"
**/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvRepCostOfSalesControllerBean ejbCreate");
		
	}
	
}

