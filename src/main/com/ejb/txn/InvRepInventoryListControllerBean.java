package com.ejb.txn;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArJobOrderInvoiceLine;
import com.ejb.ar.LocalArJobOrderInvoiceLineHome;
import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArJobOrderLineHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.ar.LocalArSalesOrderLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvAssemblyTransferLine;
import com.ejb.inv.LocalInvAssemblyTransferLineHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLine;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDepreciationLedger;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.ejb.inv.LocalInvStockIssuanceLine;
import com.ejb.inv.LocalInvStockIssuanceLineHome;
import com.ejb.inv.LocalInvStockTransferLine;
import com.ejb.inv.LocalInvStockTransferLineHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvPriceLevelDetails;
import com.util.InvRepInventoryListDetails;

/**
* @ejb:bean name="InvRepInventoryListControllerEJB"
*           display-name="Used for generation of inventory inventory list reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/InvRepInventoryListControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.InvRepInventoryListController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.InvRepInventoryListControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class InvRepInventoryListControllerBean extends AbstractSessionBean {
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepInventoryListControllerBean getAdLvReportTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV REPORT TYPE - INVENTORY LIST", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepInventoryListControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepInventoryListControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeInvRepInventoryList(HashMap criteria, String UOM_NM, Date AS_OF_DT, boolean INCLD_ZRS, 
			boolean SHW_CMMTTD_QNTTY, boolean INCLD_UNPSTD, boolean SHW_TGS, String ORDER_BY, ArrayList priceLevelList, 
			ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY, String EXPRY_DT)
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvRepInventoryListControllerBean executeInvRepInventoryList");
		
		LocalAdBranchItemLocationHome invInventoryListHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvPriceLevelHome invPriceLevelHome =  null;
		LocalInvLocationHome invLocationHome = null;		
		LocalArSalesOrderLineHome arSalesOrderLineHome = null;
		LocalArJobOrderLineHome arJobOrderLineHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalInvStockIssuanceLineHome invStockIssuanceLineHome = null;
		LocalInvAssemblyTransferLineHome invAssemblyTransferLineHome = null;
		LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
		LocalInvStockTransferLineHome invStockTransferLineHome = null;
		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
		LocalArJobOrderInvoiceLineHome arJobOrderInvoiceLineHome = null;
		LocalInvTagHome invTagHome = null;
		
		ArrayList invInventoryListList = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invInventoryListHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			arSalesOrderLineHome = (LocalArSalesOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
			arJobOrderLineHome = (LocalArJobOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			invStockIssuanceLineHome = (LocalInvStockIssuanceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvStockIssuanceLineHome.JNDI_NAME, LocalInvStockIssuanceLineHome.class);
			invAssemblyTransferLineHome = (LocalInvAssemblyTransferLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvAssemblyTransferLineHome.JNDI_NAME, LocalInvAssemblyTransferLineHome.class);
			invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
			invStockTransferLineHome = (LocalInvStockTransferLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class);
			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
			apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			arJobOrderInvoiceLineHome = (LocalArJobOrderInvoiceLineHome)EJBHomeFactory.
					lookUpLocalHome(LocalArJobOrderInvoiceLineHome.JNDI_NAME, LocalArJobOrderInvoiceLineHome.class);
			invTagHome = (LocalInvTagHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
		
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		try { 
			
			String locName = "";
			Integer locCode = null;
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalInvUnitOfMeasure invUnitOfMeasure = null;
			//LocalInvTag invTag = invTagHome.findByAlCode(2, 1);
			
			if(UOM_NM != null || UOM_NM.length() > 0) {
			
				try {
				
					invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);
					
				} catch (FinderException ex) {
					
				}
			}
			
			StringBuffer jbossQl = new StringBuffer();
    	    jbossQl.append("SELECT OBJECT(bil) FROM AdBranchItemLocation bil ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			Object obj[] = null;	      

			  if (branchList.isEmpty()) {
			  	
			  	throw new GlobalNoRecordFoundException();
			  	
			  }
			  else {
			  	
			  	jbossQl.append("WHERE bil.adBranch.brCode in (");
			  	
			  	boolean firstLoop = true;
			  	
			  	Iterator j = branchList.iterator();
			  	
			  	while(j.hasNext()) {
			  		
			  		if(firstLoop == false) { 
			  			jbossQl.append(", "); 
			  		}
			  		else { 
			  			firstLoop = false; 
			  		}
			  		
			  		AdBranchDetails mdetails = (AdBranchDetails) j.next();
			  		
			  		jbossQl.append(mdetails.getBrCode());
			  		
			  	}
			  	
			  	jbossQl.append(") ");
			  	
			  	firstArgument = false;
			  	
			  }                    
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("itemName")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("itemName")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invItem.iiName  LIKE '%" + (String)criteria.get("itemName") + "%' ");
				
			}
			
			if (criteria.containsKey("itemClass")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemClass");
				ctr++;
				
				
			}
			
			if (criteria.containsKey("itemCategory")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemCategory");
				ctr++;
			}				
			
			if (criteria.containsKey("location")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;
			}	
			
			if (criteria.containsKey("locationType")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {		       	  
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invLocation.locLvType=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("locationType");
				ctr++;
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("bil.invItemLocation.invItem.iiNonInventoriable=0 AND bil.bilAdCompany=" + AD_CMPNY + " ");   	  	  

			String expiryDate = "";
			  if (criteria.containsKey("expiryDate")) {
				  expiryDate = (String)criteria.get("expiryDate");
				  //System.out.println((String)criteria.get("expiryDate"));

			  }
			
			String orderBy = null;
	          
	          if (ORDER_BY.equals("ITEM NAME")) {
		      	
		      	  orderBy = "bil.invItemLocation.invItem.iiName";
		      	
		      } else if (ORDER_BY.equals("ITEM DESCRIPTION")) {
		      	
		      	  orderBy = "bil.invItemLocation.invItem.iiDescription";
		      	
		      } else if (ORDER_BY.equals("ITEM CLASS")) {
		      	
		      	  orderBy = "bil.invItemLocation.invItem.iiClass";
		      	
		      } 

			  if (orderBy != null) {
			  
			  	jbossQl.append("ORDER BY " + orderBy);
			  	
			  }
			
			Collection invInventoryLists = null;

			System.out.println("SSQL="+jbossQl.toString());
			
			try {
				
				invInventoryLists = invInventoryListHome.getBilByCriteria(jbossQl.toString(), obj);
				
			} catch (FinderException ex)	{
				
				throw new GlobalNoRecordFoundException ();
				
			} 

			if (invInventoryLists.isEmpty())
				throw new GlobalNoRecordFoundException ();
			
			Iterator i = invInventoryLists.iterator();
			int miscList2 = 0;
			
			while (i.hasNext()) {

				LocalAdBranchItemLocation invInventoryList = (LocalAdBranchItemLocation) i.next();
				
				InvRepInventoryListDetails details = new InvRepInventoryListDetails();

				
				details.setRilItemName(invInventoryList.getInvItemLocation().getInvItem().getIiName());
				details.setRilItemDescription(invInventoryList.getInvItemLocation().getInvItem().getIiDescription());
				System.out.println("CATEGORY=="+invInventoryList.getInvItemLocation().getInvItem().getIiAdLvCategory());
				details.setRilItemCategory(invInventoryList.getInvItemLocation().getInvItem().getIiAdLvCategory());
				details.setRilItemClass(invInventoryList.getInvItemLocation().getInvItem().getIiClass());
				details.setRilIiPartNumber(invInventoryList.getInvItemLocation().getInvItem().getIiPartNumber());
				details.setRilLocation(invInventoryList.getInvItemLocation().getInvLocation().getLocName());
				details.setRilUnit(invInventoryList.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
				
				double SALES_PRC = invInventoryList.getInvItemLocation().getInvItem().getIiSalesPrice();
				double QTY = 0d;
				double AMOUNT = 0d;
				double UNIT_COST = 0d;
				double AVE_COST = 0d;
				
				try {
					
					LocalInvCosting invInventoryListCost = 
						invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
								AS_OF_DT, details.getRilItemName(), details.getRilLocation(),
								invInventoryList.getAdBranch().getBrCode(), AD_CMPNY);

					QTY = invInventoryListCost.getCstRemainingQuantity();
					AMOUNT = invInventoryListCost.getCstRemainingValue();
					UNIT_COST = invInventoryList.getInvItemLocation().getInvItem().getIiUnitCost();
					AVE_COST = invInventoryListCost.getCstRemainingQuantity() == 0 ? 0 : 
						invInventoryListCost.getCstRemainingValue() / invInventoryListCost.getCstRemainingQuantity();
					
					Date dtePlusPlus = null;
					Calendar dtePlus = Calendar.getInstance();
					dtePlus.setTime(AS_OF_DT);
					
					Calendar dtePlus2 = Calendar.getInstance();
					dtePlus2.setTime(AS_OF_DT);
					int x=0;
					
					if(invInventoryListCost.getCstExpiryDate()!=null && invInventoryListCost.getCstExpiryDate()!="" && invInventoryListCost.getCstExpiryDate().length()!=0){
						if(EXPRY_DT.equals("30 Days")){
							dtePlus.add(Calendar.DATE,30);
							x=30;
							dtePlusPlus = dtePlus.getTime();
						}else if(EXPRY_DT.equals("60 Days")){
							dtePlus.add(Calendar.DATE,60);
							x=60;
							dtePlusPlus = dtePlus.getTime();
						}else if(EXPRY_DT.equals("90 Days")){
							dtePlus.add(Calendar.DATE,90);
							x=90;
							dtePlusPlus = dtePlus.getTime();
						}else if(EXPRY_DT.equals("120 Days")){
							dtePlus.add(Calendar.DATE,120);
							x=120;
							dtePlusPlus = dtePlus.getTime();
						}else if(EXPRY_DT.equals("150 Days")){
							dtePlus.add(Calendar.DATE,150);
							x=150;
							dtePlusPlus = dtePlus.getTime();
						}else if(EXPRY_DT.equals("180 Days")){
							dtePlus.add(Calendar.DATE,180);
								x=180;
								dtePlusPlus = dtePlus.getTime();
						}
						System.out.println("1---------------------");
						System.out.println("invInventoryListCost.getCstExpiryDate()="+invInventoryListCost.getCstExpiryDate());
						//miscList2 = this.expiryDates("$"+invInventoryListCost.getCstExpiryDate(), QTY, dtePlusPlus);
					}
					
				} catch (FinderException ex){

					QTY = 0d;
					AMOUNT = 0d;
					UNIT_COST = invInventoryList.getInvItemLocation().getInvItem().getIiUnitCost();
					AVE_COST = invInventoryList.getInvItemLocation().getInvItem().getIiUnitCost();
					
				}
					
				if(invUnitOfMeasure != null && 
						invUnitOfMeasure.getUomAdLvClass().equals(invInventoryList.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomAdLvClass())) {
					
					// convert qty, amount, unit cost, ave cost
					
					SALES_PRC = this.convertCostByUomToAndItem(invUnitOfMeasure, invInventoryList.getInvItemLocation().getInvItem(), SALES_PRC, AD_CMPNY);
					AMOUNT = this.convertAmountByUomToAndItemAndQtyAndAveCost(invUnitOfMeasure, invInventoryList.getInvItemLocation().getInvItem(), QTY, AVE_COST, AD_CMPNY);
					QTY = this.convertQuantityByUomToAndItem(invUnitOfMeasure, invInventoryList.getInvItemLocation().getInvItem(), QTY, AD_CMPNY);
					UNIT_COST = this.convertCostByUomToAndItem(invUnitOfMeasure, invInventoryList.getInvItemLocation().getInvItem(), UNIT_COST, AD_CMPNY);
					AVE_COST = this.convertCostByUomToAndItem(invUnitOfMeasure, invInventoryList.getInvItemLocation().getInvItem(), AVE_COST, AD_CMPNY);
					
					details.setRilUnit(invUnitOfMeasure.getUomName());
					
				}

				details.setRilSalesPrice(SALES_PRC);
				details.setRilQuantity(QTY);
				details.setRilAmount(AMOUNT);
				details.setRilUnitCost(UNIT_COST);
				details.setRilAverageCost(AVE_COST);
				
				if(SHW_TGS){
					try{
						int ITEM_LOCATION_CODE = invInventoryList.getInvItemLocation().getIlCode();
						int AD_BRANCH_CODE = invInventoryList.getAdBranch().getBrCode();
						Collection invTagsByLatestDate = invTagHome.findByLatestDate(AS_OF_DT, AD_CMPNY, 1);
						
						Iterator h = invTagsByLatestDate.iterator();
						Date TG_LATEST_DT = null;
						
						String ITEM_DESCRIPTION = "";
						String ITEM_CATEGORY = "";
						int PL_CODE = 0;
						int AL_CODE = 0;
						Collection invTags = null;
						while(h.hasNext()){
							LocalInvTag invTag = (LocalInvTag)h.next();
							TG_LATEST_DT = invTag.getTgTransactionDate();
							
							try{
								System.out.println("1-PL_CODE");
								PL_CODE = invTag.getApPurchaseOrderLine().getPlCode();
								System.out.println("2-PL_CODE------>"+ PL_CODE);
								
							}catch(Exception ex){}
							try{
								System.out.println("1-AL_CODE");
								AL_CODE = invTag.getInvAdjustmentLine().getAlCode();
								System.out.println("2-AL_CODE------>"+ AL_CODE);
							}catch(Exception ex){}
						}
						System.out.println("TG_LATEST_DT------>"+ TG_LATEST_DT);
						System.out.println("ITEM_LOCATION_CODE------>"+ ITEM_LOCATION_CODE);
						System.out.println("AD_BRANCH_CODE------>"+ AD_BRANCH_CODE);
						System.out.println("AD_CMPNY------>"+ AD_CMPNY);
						
						if (PL_CODE > 0){
							invTags = invTagHome.findByLessThanOrEqualToTransactionDateAndItemLocationAndAdBranchFromPOLine(TG_LATEST_DT, ITEM_LOCATION_CODE, AD_BRANCH_CODE, AD_CMPNY);
							System.out.println("PL_CODE Size="+invTags.size());
							if (invTags.size()==0){
								continue;
							}
							
						}
						if (AL_CODE >0){
							invTags = invTagHome.findByLessThanOrEqualToTransactionDateAndItemLocationAndAdBranchFromAdjLine(TG_LATEST_DT, ITEM_LOCATION_CODE, AD_BRANCH_CODE, AD_CMPNY);
							System.out.println("AL_CODE Size="+invTags.size());
							if (invTags.size()==0){
								continue;
							}
						}
						Iterator z = invTags.iterator();
						
						if(invTags.size()>0){
							while(z.hasNext()){
								LocalInvTag invTag = (LocalInvTag)z.next();
								InvRepInventoryListDetails tgDetails = new InvRepInventoryListDetails();
								
								if(invTag.getTgType().equals("IN")){
									tgDetails.setRilPropertyCode(invTag.getTgPropertyCode());
									tgDetails.setRilSpecs(invTag.getTgSpecs());
									tgDetails.setRilSerialNumber(invTag.getTgSerialNumber());													

									try{
										ITEM_DESCRIPTION = invTag.getApPurchaseOrderLine().getInvItemLocation().getInvItem().getIiDescription();
									}catch(Exception ex){
									}
									try{
										ITEM_DESCRIPTION = invTag.getInvAdjustmentLine().getInvItemLocation().getInvItem().getIiDescription();
									}catch(Exception ex){
									}
									tgDetails.setRilItemName(ITEM_DESCRIPTION);
									
									
									System.out.println(ITEM_DESCRIPTION + " <==item description");

									try{
										tgDetails.setRilCustodian(invTag.getAdUser().getUsrDescription());
									}catch(Exception ex){
										tgDetails.setRilCustodian("");
									}
									
									
									Collection depreciationLedgers = invTag.getInvDepreciationLedgers();
									Iterator y = depreciationLedgers.iterator();
									
									while(y.hasNext()){
										LocalInvDepreciationLedger invDepreciationLedger = (LocalInvDepreciationLedger)y.next();
										InvRepInventoryListDetails dlDetails = new InvRepInventoryListDetails();
										
										System.out.println("CAT="+invDepreciationLedger.getInvTag().getApPurchaseOrderLine().getInvItemLocation().getInvItem().getIiAdLvCategory());
										dlDetails.setRilDlItemCategory(invDepreciationLedger.getInvTag().getApPurchaseOrderLine().getInvItemLocation().getInvItem().getIiAdLvCategory());
										dlDetails.setRilDlItemName(invDepreciationLedger.getInvTag().getApPurchaseOrderLine().getInvItemLocation().getInvItem().getIiDescription());
										dlDetails.setRilDlPropertyCode(invDepreciationLedger.getInvTag().getTgPropertyCode());
										System.out.println("SERIAL="+invDepreciationLedger.getInvTag().getTgSerialNumber());
										dlDetails.setRilDlSerialNumber(invDepreciationLedger.getInvTag().getTgSerialNumber());
										dlDetails.setRilDlSpecs(invDepreciationLedger.getInvTag().getTgSpecs());
										dlDetails.setRilDlCustodian(invDepreciationLedger.getInvTag().getAdUser().getUsrName());
										dlDetails.setRilDlDate(EJBCommon.convertSQLDateToString(invDepreciationLedger.getDlDate()));
										dlDetails.setRilDlAcquisitionCost(invDepreciationLedger.getDlAcquisitionCost());
										dlDetails.setRilDlAmount(invDepreciationLedger.getDlDepreciationAmount());
										dlDetails.setRilDlCurrentBalance(invDepreciationLedger.getDlCurrentBalance());
										dlDetails.setRilDlMonthLifeSpan(invDepreciationLedger.getDlMonthLifeSpan());
										
										invInventoryListList.add(dlDetails);
									}
									
									
									invInventoryListList.add(tgDetails);
									
								}
							}
							
						}else{
							
							break;
						}
						
					}catch(FinderException ex){
						System.out.println("no record found?");
						throw new GlobalNoRecordFoundException();
					}
				}
				
				// get price levels
				if (!priceLevelList.isEmpty()) {
					
					ArrayList priceLevels = new ArrayList();
					
					Iterator iter = priceLevelList.iterator();

					while(iter.hasNext()) {

						String PL_AD_LV_PRC_LVL = (String) iter.next();
						
						InvPriceLevelDetails pdetails = new InvPriceLevelDetails();

						try {
							
							LocalInvPriceLevel invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(
									invInventoryList.getInvItemLocation().getInvItem().getIiName(), 
									PL_AD_LV_PRC_LVL, AD_CMPNY);
							
							pdetails.setPlAdLvPriceLevel(invPriceLevel.getPlAdLvPriceLevel());
							if(invUnitOfMeasure != null && 
									invUnitOfMeasure.getUomAdLvClass().equals(invInventoryList.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomAdLvClass())) {
								pdetails.setPlAmount(this.convertCostByUomToAndItem(invUnitOfMeasure, invInventoryList.getInvItemLocation().getInvItem(), invPriceLevel.getPlAmount(), AD_CMPNY));
							} else {
								pdetails.setPlAmount(invPriceLevel.getPlAmount());
							}
							
						} catch(FinderException ex) {
							
							pdetails.setPlAdLvPriceLevel(PL_AD_LV_PRC_LVL);
							pdetails.setPlAmount(0d);
							
						}
						
						priceLevels.add(pdetails);

					}
					
					details.setRilPriceLevels(priceLevels);	
					
				}
				
				Integer brCode = invInventoryList.getAdBranch().getBrCode();
				
				if(SHW_CMMTTD_QNTTY) {
					
					double committedQty = 0d;
					double committedAmount = 0d;
					
					Collection arSalesOrderLines = arSalesOrderLineHome.findCommittedQtyByIiNameAndLocNameAndWithoutDateAndSoAdBranch(
							invInventoryList.getInvItemLocation().getInvItem().getIiName(), invInventoryList.getInvItemLocation().getInvLocation().getLocName(), 
							brCode, AD_CMPNY);

					Iterator solIter = arSalesOrderLines.iterator(); 
					
					while (solIter.hasNext())
					{
						LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine)solIter.next();
						
						/*if(arSalesOrderLine.getArSalesOrderInvoiceLines().size() == 0 &&
								arSalesOrderLine.getInvItemLocation().equals(invInventoryList.getInvItemLocation()) &&
								arSalesOrderLine.getArSalesOrder().getSoAdBranch().equals(brCode) &&
								arSalesOrderLine.getArSalesOrder().getSoDate().compareTo(AS_OF_DT) <= 0) {*/
						
						GregorianCalendar gcDateFrom = new GregorianCalendar();
						gcDateFrom.setTime(AS_OF_DT);
						gcDateFrom.add(Calendar.MONTH, -1);
						
						if(arSalesOrderLine.getArSalesOrderInvoiceLines().size() == 0 &&
								arSalesOrderLine.getArSalesOrder().getSoDate().compareTo(AS_OF_DT) <= 0 &&
								arSalesOrderLine.getArSalesOrder().getSoDate().compareTo(gcDateFrom.getTime()) >= 0){
							
							if(!this.filterByOptionalCriteria(criteria, arSalesOrderLine.getInvItemLocation().getInvItem(),
									arSalesOrderLine.getInvItemLocation().getInvLocation()))
								continue;
							
							if(arSalesOrderLine.getArSalesOrder().getSoOrderStatus().equals("Bad"))
								continue;

							committedQty -= arSalesOrderLine.getSolQuantity();
							committedAmount -= arSalesOrderLine.getSolAmount();				
						}
						
					}	
					
					
					Collection arJobOrderLines = arJobOrderLineHome.findCommittedQtyByIiNameAndLocNameAndWithoutDateAndJoAdBranch(
							invInventoryList.getInvItemLocation().getInvItem().getIiName(), invInventoryList.getInvItemLocation().getInvLocation().getLocName(), 
							brCode, AD_CMPNY);

					Iterator jolIter = arJobOrderLines.iterator(); 
					
					while (jolIter.hasNext())
					{
						LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine)jolIter.next();
						
						/*if(arSalesOrderLine.getArSalesOrderInvoiceLines().size() == 0 &&
								arSalesOrderLine.getInvItemLocation().equals(invInventoryList.getInvItemLocation()) &&
								arSalesOrderLine.getArSalesOrder().getSoAdBranch().equals(brCode) &&
								arSalesOrderLine.getArSalesOrder().getSoDate().compareTo(AS_OF_DT) <= 0) {*/
						
						GregorianCalendar gcDateFrom = new GregorianCalendar();
						gcDateFrom.setTime(AS_OF_DT);
						gcDateFrom.add(Calendar.MONTH, -1);
						
						if(arJobOrderLine.getArJobOrderInvoiceLines().size() == 0 &&
								arJobOrderLine.getArJobOrder().getJoDate().compareTo(AS_OF_DT) <= 0 &&
										arJobOrderLine.getArJobOrder().getJoDate().compareTo(gcDateFrom.getTime()) >= 0){
							
							if(!this.filterByOptionalCriteria(criteria, arJobOrderLine.getInvItemLocation().getInvItem(),
									arJobOrderLine.getInvItemLocation().getInvLocation()))
								continue;
							
							if(arJobOrderLine.getArJobOrder().getJoOrderStatus().equals("Bad"))
								continue;

							committedQty -= arJobOrderLine.getJolQuantity();
							committedAmount -= arJobOrderLine.getJolAmount();				
						}
						
					}	
					
					
					
					
					details.setRilQuantity(details.getRilQuantity() + committedQty);
					details.setRilAmount(details.getRilAmount() + committedAmount);
				}
				int miscList = 0;
				if(INCLD_UNPSTD) {
					
					double unpostedQty = 0d;
					double unpostedAmount = 0d;

					locName = invInventoryList.getInvItemLocation().getInvLocation().getLocName();
					locCode = invLocationHome.findByLocName(locName, AD_CMPNY).getLocCode();
					String iiName = invInventoryList.getInvItemLocation().getInvItem().getIiName();
					//INV_ADJUSTMENT_LINE
					Collection invAdjustmentLines = invAdjustmentLineHome.findUnpostedAdjByIiNameAndLocNameAndLessEqualDateAndAdjAdBranch(iiName, locName, AS_OF_DT, brCode, AD_CMPNY);
					
					Iterator unpstdIter = invAdjustmentLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)unpstdIter.next();

						LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();
						
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								invAdjustmentLine.getInvAdjustment().getAdjDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty += invAdjustmentLine.getAlAdjustQuantity();
						unpostedAmount += (invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost());
						
						try{
							System.out.println(invAdjustmentLine.getAlMisc());
							int qty2Prpgt = 0;
							if(invAdjustmentLine.getAlMisc().length()!=0){
								qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invAdjustmentLine.getAlMisc()));
							}
							if(invAdjustmentLine.getAlAdjustQuantity()>0){
								Date dtePlusPlus = null;
								Calendar dtePlus = Calendar.getInstance();
								dtePlus.setTime(AS_OF_DT);

								Calendar dtePlus2 = Calendar.getInstance();
								dtePlus2.setTime(AS_OF_DT);
								int x=0;

								if(EXPRY_DT.equals("30 Days")){
									dtePlus.add(Calendar.DATE,30);
									x=30;
									dtePlusPlus = dtePlus.getTime();
								}else if(EXPRY_DT.equals("60 Days")){
									dtePlus.add(Calendar.DATE,60);
									x=60;
									dtePlusPlus = dtePlus.getTime();
								}else if(EXPRY_DT.equals("90 Days")){
									dtePlus.add(Calendar.DATE,90);
									x=90;
									dtePlusPlus = dtePlus.getTime();
								}

								if(invAdjustmentLine.getAlMisc()!=null && invAdjustmentLine.getAlMisc()!="" && invAdjustmentLine.getAlMisc().length()!=0){
									System.out.println("2---------------------");
									miscList = miscList + this.expiryDates(invAdjustmentLine.getAlMisc(), qty2Prpgt, dtePlusPlus);
								}
							}
						}catch(Exception ex){

						}
						
						
					}
					
					//INV_STOCK_ISSUANCE_LINE
					Collection invStockIssuanceLines = invStockIssuanceLineHome.findUnpostedSiByLocNameAndAdBranch(locName, brCode, AD_CMPNY);
					
					unpstdIter = invStockIssuanceLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine)unpstdIter.next();

						LocalInvItemLocation invItemLocation = invStockIssuanceLine.getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();
						
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								invStockIssuanceLine.getInvStockIssuance().getSiDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty += invStockIssuanceLine.getSilIssueQuantity();
						unpostedAmount += invStockIssuanceLine.getSilIssueCost();
					}
					
					//INV_ASSEMBLY_TRANSFER_LINE
					Collection invAssemblyTransferLines = invAssemblyTransferLineHome.findUnpostedAtrByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

					unpstdIter = invAssemblyTransferLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvAssemblyTransferLine invAssemblyTransferLine = (LocalInvAssemblyTransferLine)unpstdIter.next();

						LocalInvItemLocation invItemLocation = invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();
						
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								invAssemblyTransferLine.getInvAssemblyTransfer().getAtrDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty += invAssemblyTransferLine.getAtlAssembleQuantity();
						unpostedAmount += invAssemblyTransferLine.getAtlAssembleCost();
					}
					
					
					//INV_BUILD_UNBUILD_ASSEMBLY_LINE
					Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssemblyLineHome.findUnpostedBuaByIiNameAndLocNameAndLessEqualDateAndBuaAdBranch(iiName, locName, AS_OF_DT, brCode, AD_CMPNY);
					
					unpstdIter = invBuildUnbuildAssemblyLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)unpstdIter.next();

						LocalInvItemLocation invItemLocation = invBuildUnbuildAssemblyLine.getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();
						
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty += invBuildUnbuildAssemblyLine.getBlBuildQuantity();
						unpostedAmount += (invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost() *
								invBuildUnbuildAssemblyLine.getBlBuildQuantity());
						
						try{
							//System.out.println("qty: " + invBuildUnbuildAssemblyLine.getBlBuildQuantity());
							int qty2Prpgt = 0;
							if(invBuildUnbuildAssemblyLine.getBlMisc().length()!=0){
							qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invBuildUnbuildAssemblyLine.getBlMisc()));
							}
							if(invBuildUnbuildAssemblyLine.getBlBuildQuantity()>0){
								Date dtePlusPlus = null;
								Calendar dtePlus = Calendar.getInstance();
								dtePlus.setTime(AS_OF_DT);
								
								Calendar dtePlus2 = Calendar.getInstance();
								dtePlus2.setTime(AS_OF_DT);
								int x=0;
								
								if(EXPRY_DT.equals("30 Days")){
									dtePlus.add(Calendar.DATE,30);
									x=30;
									dtePlusPlus = dtePlus.getTime();
								}else if(EXPRY_DT.equals("60 Days")){
									dtePlus.add(Calendar.DATE,60);
									x=60;
									dtePlusPlus = dtePlus.getTime();
								}else if(EXPRY_DT.equals("90 Days")){
									dtePlus.add(Calendar.DATE,90);
									x=90;
									dtePlusPlus = dtePlus.getTime();
								}
								if(invBuildUnbuildAssemblyLine.getBlMisc()!=null && invBuildUnbuildAssemblyLine.getBlMisc()!="" && invBuildUnbuildAssemblyLine.getBlMisc().length()!=0){
									
									System.out.println("3---------------------");
									miscList = miscList+this.expiryDates(invBuildUnbuildAssemblyLine.getBlMisc(), qty2Prpgt, dtePlusPlus);
								}
							}
						}catch(Exception ex){

						}
						
						
					}

					/*
					 * Disabled This Region Temorarily 
					 * 
					 */
					
					//INV_STOCK_TRANSFER_LINE OUT
					//Collection invStockTransferLines = invStockTransferLineHome.findUnpostedStByLocCodeAndAdBranch(locCode, brCode, AD_CMPNY);
										
					/*unpstdIter = invStockTransferLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine)unpstdIter.next();

						LocalInvItem invItem = invStockTransferLine.getInvItem();
						LocalInvLocation invLocation = invLocationHome.findByPrimaryKey(locCode);
						System.out.println("STLO" + "\t Item: " + invInventoryList.getInvItemLocation().getInvItem().getIiName() + "\t"
								+ "Loc: " + invInventoryList.getInvItemLocation().getInvLocation().getLocName());
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) ||
								invStockTransferLine.getInvStockTransfer().getStDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty -= invStockTransferLine.getStlQuantityDelivered();
						unpostedAmount -= invStockTransferLine.getStlAmount();
						
						try{
							int qty2Prpgt = 0;
							if(invStockTransferLine.getStlMisc().length()!=0){
							//System.out.println("ERROR ZONE!");
							qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invStockTransferLine.getStlMisc()));
							//System.out.println("qty2Prpgt: " + qty2Prpgt);
							}
							Date dtePlusPlus = null;
							Calendar dtePlus = Calendar.getInstance();
							dtePlus.setTime(AS_OF_DT);
							
							Calendar dtePlus2 = Calendar.getInstance();
							dtePlus2.setTime(AS_OF_DT);
							int x=0;
							
							if(EXPRY_DT.equals("30 Days")){
								dtePlus.add(Calendar.DATE,30);
								x=30;
								dtePlusPlus = dtePlus.getTime();
							}else if(EXPRY_DT.equals("60 Days")){
								dtePlus.add(Calendar.DATE,60);
								x=60;
								dtePlusPlus = dtePlus.getTime();
							}else if(EXPRY_DT.equals("90 Days")){
								dtePlus.add(Calendar.DATE,90);
								x=90;
								dtePlusPlus = dtePlus.getTime();
							}
							//System.out.println("dtePlusPlus: " + dtePlusPlus);
							if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
								miscList = miscList+ this.expiryDates(invStockTransferLine.getStlMisc(), qty2Prpgt, dtePlusPlus);
								//System.out.println("miscList: " + miscList);
							}
							//System.out.println("expiryDate: " + EXPRY_DT);
						}catch(Exception ex){

						}
						
					}*/
					
					//INV_STOCK_TRANSFER_LINE IN
					Collection invStockTransferInLines = invStockTransferLineHome.findUnpostedStlByIiNameAndLocCodeAndLessEqualDateAndStAdBranch(iiName, locCode, AS_OF_DT, brCode, AD_CMPNY);
					unpstdIter = invStockTransferInLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine)unpstdIter.next();

						LocalInvItem invItem = invStockTransferLine.getInvItem();
						LocalInvLocation invLocation = invLocationHome.findByPrimaryKey(locCode);
						
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) ||
								invStockTransferLine.getInvStockTransfer().getStDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						if(invStockTransferLine.getStlLocationTo().equals(locName)){
							unpostedQty += invStockTransferLine.getStlQuantityDelivered();
							unpostedAmount += invStockTransferLine.getStlAmount();
						}else{
							unpostedQty -= invStockTransferLine.getStlQuantityDelivered();
							unpostedAmount -= invStockTransferLine.getStlAmount();
						}
						/*
						try{
							int qty2Prpgt = 0;
							if(invStockTransferLine.getStlMisc().length()!=0){
							qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invStockTransferLine.getStlMisc()));
							}
							Date dtePlusPlus = null;
							Calendar dtePlus = Calendar.getInstance();
							dtePlus.setTime(AS_OF_DT);
							
							Calendar dtePlus2 = Calendar.getInstance();
							dtePlus2.setTime(AS_OF_DT);
							int x=0;
							
							if(EXPRY_DT.equals("30 Days")){
								dtePlus.add(Calendar.DATE,30);
								x=30;
								dtePlusPlus = dtePlus.getTime();
							}else if(EXPRY_DT.equals("60 Days")){
								dtePlus.add(Calendar.DATE,60);
								x=60;
								dtePlusPlus = dtePlus.getTime();
							}else if(EXPRY_DT.equals("90 Days")){
								dtePlus.add(Calendar.DATE,90);
								x=90;
								dtePlusPlus = dtePlus.getTime();
							}
							if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
								
								System.out.println("4---------------------");
								miscList = miscList+ this.expiryDates(invStockTransferLine.getStlMisc(), qty2Prpgt, dtePlusPlus);
							}
						}catch(Exception ex){

						}
						*/
						
					}


					//INV_BRANCH_STOCK_TRANSFER_LINE
					Collection invBranchStockTransferLines = invBranchStockTransferLineHome.findUnpostedBstByLocNameAndAdBranch(locName, brCode, AD_CMPNY);
					
					unpstdIter = invBranchStockTransferLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)unpstdIter.next();

						LocalInvItemLocation invItemLocation = invBranchStockTransferLine.getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();

						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty += invBranchStockTransferLine.getBslQuantityReceived();
						unpostedAmount += invBranchStockTransferLine.getBslAmount();
						
						try{
							int qty2Prpgt = 0;
							if(invBranchStockTransferLine.getBslMisc().length()!=0){
								qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invBranchStockTransferLine.getBslMisc()));
							}
							if(invBranchStockTransferLine.getBslQuantityReceived()>0){
								Date dtePlusPlus = null;
								Calendar dtePlus = Calendar.getInstance();
								dtePlus.setTime(AS_OF_DT);

								Calendar dtePlus2 = Calendar.getInstance();
								dtePlus2.setTime(AS_OF_DT);
								int x=0;

								if(EXPRY_DT.equals("30 Days")){
									dtePlus.add(Calendar.DATE,30);
									x=30;
									dtePlusPlus = dtePlus.getTime();
								}else if(EXPRY_DT.equals("60 Days")){
									dtePlus.add(Calendar.DATE,60);
									x=60;
									dtePlusPlus = dtePlus.getTime();
								}else if(EXPRY_DT.equals("90 Days")){
									dtePlus.add(Calendar.DATE,90);
									x=90;
									dtePlusPlus = dtePlus.getTime();
								}

								if(invBranchStockTransferLine.getBslMisc()!=null && invBranchStockTransferLine.getBslMisc()!="" && invBranchStockTransferLine.getBslMisc().length()!=0){
									
									System.out.println("5---------------------");
									miscList = miscList+ this.expiryDates(invBranchStockTransferLine.getBslMisc(), qty2Prpgt, dtePlusPlus);
								}
							}
						}catch(Exception ex){

						}
						
						
					}

					//AP_VOUCHER_LINE_ITEM
					//a) apVoucher
					Collection apVoucherLineItems = apVoucherLineItemHome.findUnpostedVouByIiNameAndLocNameAndLessEqualDateAndVouAdBranch(iiName, locName, AS_OF_DT, brCode, AD_CMPNY);
					
					unpstdIter = apVoucherLineItems.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)unpstdIter.next();

						LocalInvItemLocation invItemLocation = apVoucherLineItem.getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();
						
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								apVoucherLineItem.getApVoucher().getVouDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty += apVoucherLineItem.getVliQuantity();
						unpostedAmount += apVoucherLineItem.getVliAmount();
						
						try{
							int qty2Prpgt = 0;
							
							if(apVoucherLineItem.getVliMisc().length()!=0){
								qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(apVoucherLineItem.getVliMisc()));
							}
							Date dtePlusPlus = null;
							Calendar dtePlus = Calendar.getInstance();
							dtePlus.setTime(AS_OF_DT);
							
							Calendar dtePlus2 = Calendar.getInstance();
							dtePlus2.setTime(AS_OF_DT);
							int x=0;
							
							if(EXPRY_DT.equals("30 Days")){
								dtePlus.add(Calendar.DATE,30);
								x=30;
								dtePlusPlus = dtePlus.getTime();
							}else if(EXPRY_DT.equals("60 Days")){
								dtePlus.add(Calendar.DATE,60);
								x=60;
								dtePlusPlus = dtePlus.getTime();
							}else if(EXPRY_DT.equals("90 Days")){
								dtePlus.add(Calendar.DATE,90);
								x=90;
								dtePlusPlus = dtePlus.getTime();
							}

							if(apVoucherLineItem.getVliMisc()!=null && apVoucherLineItem.getVliMisc()!="" && apVoucherLineItem.getVliMisc().length()!=0){
								System.out.println("6---------------------");
								miscList = miscList+ this.expiryDates(apVoucherLineItem.getVliMisc(), qty2Prpgt, dtePlusPlus);
							}

						}catch(Exception ex){

						}
						
					}

					//b) apCheck					

					apVoucherLineItems = apVoucherLineItemHome.findUnpostedChkByIiNameAndLocNameAndLessEqualDateAndChkAdBranch(iiName, locName, AS_OF_DT, brCode, AD_CMPNY);
					
					unpstdIter = apVoucherLineItems.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)unpstdIter.next();

						LocalInvItemLocation invItemLocation = apVoucherLineItem.getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();
						System.out.println("CHKL" + "\t Item: " + invInventoryList.getInvItemLocation().getInvItem().getIiName() + "\t"
								+ "Loc: " + invInventoryList.getInvItemLocation().getInvLocation().getLocName());
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								apVoucherLineItem.getApCheck().getChkDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty += apVoucherLineItem.getVliQuantity();
						unpostedAmount += apVoucherLineItem.getVliAmount();
						try{
							int qty2Prpgt = 0;
							if(apVoucherLineItem.getVliMisc().length()!=0){
								qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(apVoucherLineItem.getVliMisc()));
							}
							Date dtePlusPlus = null;
							Calendar dtePlus = Calendar.getInstance();
							dtePlus.setTime(AS_OF_DT);

							Calendar dtePlus2 = Calendar.getInstance();
							dtePlus2.setTime(AS_OF_DT);
							int x=0;

							if(EXPRY_DT.equals("30 Days")){
								dtePlus.add(Calendar.DATE,30);
								x=30;
								dtePlusPlus = dtePlus.getTime();
							}else if(EXPRY_DT.equals("60 Days")){
								dtePlus.add(Calendar.DATE,60);
								x=60;
								dtePlusPlus = dtePlus.getTime();
							}else if(EXPRY_DT.equals("90 Days")){
								dtePlus.add(Calendar.DATE,90);
								x=90;
								dtePlusPlus = dtePlus.getTime();
							}

							if(apVoucherLineItem.getVliMisc()!=null && apVoucherLineItem.getVliMisc()!="" && apVoucherLineItem.getVliMisc().length()!=0){
								System.out.println("7---------------------");
								miscList = miscList+ this.expiryDates(apVoucherLineItem.getVliMisc(), qty2Prpgt, dtePlusPlus);
							}

						}catch(Exception ex){

						}
						
					}

					//AR_INVOICE_LINE_ITEM
					//a) arInvoice

					Collection arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(iiName, locName, AS_OF_DT, brCode, AD_CMPNY);
					
					unpstdIter = arInvoiceLineItems.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)unpstdIter.next();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();
						
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								arInvoiceLineItem.getArInvoice().getInvDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty -= arInvoiceLineItem.getIliQuantity();
						unpostedAmount -= arInvoiceLineItem.getIliAmount();
						
						
					}

					//b) arReceipt
					arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedRctByIiNameAndLocNameAndLessEqualDateAndRctAdBranch(iiName, locName, AS_OF_DT, brCode, AD_CMPNY);
					
					unpstdIter = arInvoiceLineItems.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)unpstdIter.next();

						LocalInvItemLocation invItemLocation = arInvoiceLineItem.getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();
						
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								arInvoiceLineItem.getArReceipt().getRctDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty -= arInvoiceLineItem.getIliQuantity();
						unpostedAmount -= arInvoiceLineItem.getIliAmount();
						
					}

					
					//AP_PURCHASE_ORDER_LINE
					Collection apPurchaseOrderLines = apPurchaseOrderLineHome.findUnpostedPoByIiNameAndLocNameAndLessEqualDateAndPoAdBranch(iiName, locName, AS_OF_DT, brCode, AD_CMPNY);
					
					unpstdIter = apPurchaseOrderLines.iterator();
					
					while(unpstdIter.hasNext()) {

						LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)unpstdIter.next();

						LocalInvItemLocation invItemLocation = apPurchaseOrderLine.getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();
						
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								apPurchaseOrderLine.getApPurchaseOrder().getPoDate().compareTo(AS_OF_DT) > 0)
							continue;

						unpostedQty += apPurchaseOrderLine.getPlQuantity();
						unpostedAmount += apPurchaseOrderLine.getPlAmount();

						int qty2Prpgt = 0;
						try{

							if(apPurchaseOrderLine.getPlMisc().length()!=0 || apPurchaseOrderLine.getPlMisc().equals(null)){
								qty2Prpgt =Integer.parseInt(this.getQuantityExpiryDates(apPurchaseOrderLine.getPlMisc()));
							}


							Date dtePlusPlus = null;
							Calendar dtePlus = Calendar.getInstance();
							dtePlus.setTime(AS_OF_DT);

							Calendar dtePlus2 = Calendar.getInstance();
							dtePlus2.setTime(AS_OF_DT);
							int x=0;

							if(EXPRY_DT.equals("30 Days")){
								dtePlus.add(Calendar.DATE,30);
								x=30;
								dtePlusPlus = dtePlus.getTime();
							}else if(EXPRY_DT.equals("60 Days")){
								dtePlus.add(Calendar.DATE,60);
								x=60;
								dtePlusPlus = dtePlus.getTime();
							}else if(EXPRY_DT.equals("90 Days")){
								dtePlus.add(Calendar.DATE,90);
								x=90;
								dtePlusPlus = dtePlus.getTime();
							}

							if(apPurchaseOrderLine.getPlMisc()!=null && apPurchaseOrderLine.getPlMisc()!="" && apPurchaseOrderLine.getPlMisc().length()!=0){
								System.out.println("8---------------------");
								miscList = miscList+ this.expiryDates(apPurchaseOrderLine.getPlMisc(), qty2Prpgt, dtePlusPlus);
							}

						}catch(Exception ex){

						}
					}

					//AR_SALES_ORDER_INVOICE_LINE
					Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome.findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(iiName, locName, AS_OF_DT, brCode, AD_CMPNY);
					
					unpstdIter = arSalesOrderInvoiceLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)unpstdIter.next();

						LocalInvItemLocation invItemLocation = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();
						
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								arSalesOrderInvoiceLine.getArInvoice().getInvDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty -= arSalesOrderInvoiceLine.getSilQuantityDelivered();
						unpostedAmount -= arSalesOrderInvoiceLine.getSilAmount();
						
					}
					
					
					
					//AR_JOB_ORDER_INVOICE_LINE
					Collection arJobOrderInvoiceLines = arJobOrderInvoiceLineHome.findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(iiName, locName, AS_OF_DT, brCode, AD_CMPNY);
					
					unpstdIter = arJobOrderInvoiceLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine)unpstdIter.next();

						LocalInvItemLocation invItemLocation = arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation();
						LocalInvItem invItem = invItemLocation.getInvItem();
						LocalInvLocation invLocation = invItemLocation.getInvLocation();
						
						if(!this.filterByOptionalCriteria(criteria, invItem, invLocation) || 
								!invItemLocation.equals(invInventoryList.getInvItemLocation()) ||
								arJobOrderInvoiceLine.getArInvoice().getInvDate().compareTo(AS_OF_DT) > 0)
							continue;
						
						unpostedQty -= arJobOrderInvoiceLine.getJilQuantityDelivered();
						unpostedAmount -= arJobOrderInvoiceLine.getJilAmount();
						
					}
					
					details.setRilQuantity(details.getRilQuantity() + unpostedQty);
					details.setRilAmount(details.getRilAmount() + unpostedAmount);
				}
				
				
				
				if ((details.getRilQuantity() > 0) ||(INCLD_ZRS && (details.getRilQuantity() <= 0))){
					int combineMiscNumber2 = 0;
					combineMiscNumber2 = miscList2 + miscList;
					String combineMiscNumber = Integer.toString(combineMiscNumber2);
					if(combineMiscNumber2!=0){
						details.setRilExpiryDate(combineMiscNumber);
					}
					if(!SHW_TGS){
						System.out.println("ng print p dn?");
						invInventoryListList.add(details);
					}
					
				}
				
				
			}
			
			if (invInventoryListList.isEmpty()){
				System.out.println("GlobalNoRecordFoundException");
				throw new GlobalNoRecordFoundException ();
			}else{
				return invInventoryListList;	  
			}
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}
	public String getQuantityExpiryDates(String qntty){
	   	String separator = "$";

	   	// Remove first $ character
	   	qntty = qntty.substring(1);

	   	// Counter
	   	int start = 0;
	   	int nextIndex = qntty.indexOf(separator, start);
	   	int length = nextIndex - start;	
	   	String y;
	   	y = (qntty.substring(start, start + length));
	   	//System.out.println("Y " + y);
	   	
	   	return y;
	   }
	
	private int expiryDates(String misc, double qty, Date dateUsed) throws Exception{
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	//System.out.println("misc: " + misc);
    	String separator ="$";
    	
    	Calendar dtePlus = Calendar.getInstance();
		dtePlus.setTime(dateUsed);
		
    	// Remove first $ character
    	misc = misc.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	

    	//System.out.println("qty" + qty);
    	ArrayList miscList = new ArrayList();
		int returnNumberDate=0;
    	for(int x=0; x<qty; x++) {

    		// Date
    		start = nextIndex + 1;
    		nextIndex = misc.indexOf(separator, start);
    		length = nextIndex - start;
    		String checker = misc.substring(start, start + length);
    		//System.out.println("checker: "+checker);
    		try{
        		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        	    // Get Date 1
        	    Date d1 = df.parse(checker);
        		if(dateUsed.after(d1) || dateUsed.equals(d1) ){
        			if(checker.length()!=0 || checker!="null"){
            			miscList.add(checker);
            			returnNumberDate++;
            		}
        		}
    		}catch(Exception ex){
    			
    		}
    	}	
		
		//System.out.println("miscList :" + returnNumberDate);
		return returnNumberDate;
    }

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvLocationTypeAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepInventoryListControllerBean getAdLvInvLocationTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV LOCATION TYPE", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}		

	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepInventoryListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("InvRepInventoryListControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomAll(Integer AD_CMPNY) {
        
        Debug.print("InvRepInventoryListControllerBean getInvUomAll");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection invUnitOfMeasures = invUnitOfMeasureHome.findEnabledUomAll(AD_CMPNY);
            
            Iterator i = invUnitOfMeasures.iterator();
            
            while (i.hasNext()) {
                
            	LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure)i.next();
                
                list.add(invUnitOfMeasure.getUomName());
                
            }
            
            return list;
            
        } catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
        
    }
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvPriceLevelAll(Integer AD_CMPNY) {

		Debug.print("InvRepInventoryListControllerBean getAdLvInvPriceLevelAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;               

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}        

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV PRICE LEVEL", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
    
    // private methods
    
    private double convertQuantityByUomToAndItem(LocalInvUnitOfMeasure invToUnitOfMeasure, LocalInvItem invItem, double QTY, Integer AD_CMPNY) {
        
        //Debug.print("InvRepInventoryListControllerBean convertQuantityByUomToAndItem");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invToUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(QTY * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private double convertCostByUomToAndItem(LocalInvUnitOfMeasure invToUnitOfMeasure, LocalInvItem invItem, double COST, Integer AD_CMPNY) {
        
        //Debug.print("InvRepInventoryListControllerBean convertCostByUomToAndItem");		        
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invToUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adCompany.getGlFunctionalCurrency().getFcPrecision());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private double convertAmountByUomToAndItemAndQtyAndAveCost(LocalInvUnitOfMeasure invToUnitOfMeasure, LocalInvItem invItem, double QTY, double AVE_COST, Integer AD_CMPNY) {
        
        //Debug.print("InvRepInventoryListControllerBean convertAmountByUomToAndItemAndQtyAndAveCost");		        
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invToUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt((AVE_COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor()) * 
            		(QTY * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor()), adCompany.getGlFunctionalCurrency().getFcPrecision());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	
    private boolean filterByOptionalCriteria(HashMap criteria, LocalInvItem invItem, LocalInvLocation invLocation) {
  	   
  	   Debug.print("InvRepItemCostingControllerBean filterByOptionalCriteria");
  	   
  	   try {
  		   
  		   String itemName = "";
  		   String itemClass = "";
  		   String itemCategory = "";
  		   String locationType = "";
  		   String location = "";
  		   
  		   
  		   if (criteria.containsKey("itemName"))
  			   itemName = (String)criteria.get("itemName");
  		   
  		   if (criteria.containsKey("itemClass"))
  			   itemClass = (String)criteria.get("itemClass");
  		   
  		   if (criteria.containsKey("itemCategory"))
  			   itemCategory = (String)criteria.get("itemCategory");
  		   
  		   if (criteria.containsKey("locationType"))
  			   locationType = (String)criteria.get("locationType");
  		   
  		   if (criteria.containsKey("location"))
  			 location = (String)criteria.get("location");
  		   
  		   if(!itemName.equals("") && !invItem.getIiName().toUpperCase().contains(itemName.toUpperCase())) 
  			   return false;
  		   
  		   if(!itemClass.equals("") && !invItem.getIiClass().equals(itemClass))
  			   return false;
  		   
  		   if(!itemCategory.equals("") && !invItem.getIiAdLvCategory().equals(itemCategory))
  			   return false;
  		   
  		   if(!locationType.equals("") && !invLocation.getLocLvType().equals(locationType))
  			   return false;
  		   
  		   if(!location.equals("") && !invLocation.getLocName().equals(location))
  			   return false;
  		   
  		   
  		   return true;
  		   
  	   } catch (Exception ex) {
  	
  		   Debug.printStackTrace(ex);
  		   getSessionContext().setRollbackOnly();
  		   throw new EJBException(ex.getMessage());
  	   }
     }

    
    // SessionBean methods
	
    /**
     * @ejb:create-method view-type="remote"
     **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvRepInventoryListControllerBean ejbCreate");
		
	}
	
}

