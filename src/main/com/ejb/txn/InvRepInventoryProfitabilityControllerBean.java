package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvOverheadLine;
import com.ejb.inv.LocalInvOverheadLineHome;
import com.ejb.inv.LocalInvOverheadMemoLineHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepInventoryProfitabilityDetails;

/**
* @ejb:bean name="InvRepInventoryProfitabilityControllerEJB"
*           display-name="Used for generation of inventory profitability reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/InvRepInventoryProfitabilityControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.InvRepInventoryProfitabilityController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.InvRepInventoryProfitabilityControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class InvRepInventoryProfitabilityControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepInventoryProfitabilityControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepInventoryProfitabilityControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public byte getAdPrfEnableInvShift(Integer AD_CMPNY) {

        Debug.print("InvRepInventoryProfitabilityControllerBean getAdPrfEnableInvShift");
                   
        LocalAdPreferenceHome adPreferenceHome = null;
       
       
        // Initialize EJB Home
         
        try {
             
           adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
             
        } catch (NamingException ex) {
             
           throw new EJBException(ex.getMessage());
             
        }
       

        try {
       	
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
          
           return adPreference.getPrfInvEnableShift();
          
        } catch (Exception ex) {
         	 
           Debug.printStackTrace(ex);
           throw new EJBException(ex.getMessage());
          
        }
       
     }
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvShiftAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepInventoryProfitabilityControllerBean getAdLvInvShiftAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV SHIFT", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeInvRepInventoryProfitability(HashMap criteria, String costingMethod, ArrayList branchList, String GROUP_BY, Integer AD_CMPNY)
		throws GlobalNoRecordFoundException {
		
		Debug.print("InvRepInventoryProfitabilityControllerBean executeInvRepInventoryProfitability");

		LocalInvCostingHome invCostingHome = null;
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvOverheadLineHome invOverheadLineHome = null;
		LocalInvOverheadMemoLineHome invOverheadMemoLineHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		
		ArrayList list = new ArrayList();
		ArrayList tempList = new ArrayList();
		
		// Initialize EJB Home
		
		try {

			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invOverheadLineHome = (LocalInvOverheadLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvOverheadLineHome.JNDI_NAME, LocalInvOverheadLineHome.class);
			invOverheadMemoLineHome = (LocalInvOverheadMemoLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvOverheadMemoLineHome.JNDI_NAME, LocalInvOverheadMemoLineHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			if (branchList.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			// SALES - invoices and misc receipts
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(cst) FROM InvCosting cst ");
			
			boolean firstArgument = true;
			short ctr = 0;	      
			int criteriaSize = 0;
			
			Object obj[] = null;			
			            			
			// Allocate the size of the object parameter


			if (criteria.containsKey("category")) {
				
				criteriaSize++;
				
			}
			
			if (criteria.containsKey("location")) {
				
				criteriaSize++;
				
			}

			if (criteria.containsKey("itemClass")) {

				criteriaSize++;

			}

			if (criteria.containsKey("dateFrom")) {

				criteriaSize++;

			}

			if (criteria.containsKey("dateTo")) {

				criteriaSize++;

			}
			
			obj = new Object[criteriaSize];

			jbossQl.append(" WHERE cst.cstAdBranch in (");
			
			boolean firstLoop = true;
			
			Iterator i = branchList.iterator();
			
			while(i.hasNext()) {
				
				if(firstLoop == false) { 
					jbossQl.append(", "); 
				}
				else { 
					firstLoop = false; 
				}
				
				AdBranchDetails mdetails = (AdBranchDetails) i.next();
				
				jbossQl.append(mdetails.getBrCode());
				
			}
			
			jbossQl.append(") ");
			
			firstArgument = false;
			
			
			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.invItemLocation.invItem.iiName  LIKE '%" + (String)criteria.get("itemName") + "%' ");

			}
			
			
			if (criteria.containsKey("category")) {

				if (!firstArgument) {
					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;

			} 

			if (criteria.containsKey("location")) {

				if (!firstArgument) {
					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;

			} 
			
			if (criteria.containsKey("itemClass")) {

				if (!firstArgument) {
					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemClass");
				ctr++;

			} 

			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("cst.cstDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("cst.cstDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
										
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}

			jbossQl.append("cst.cstQuantitySold <> 0 AND cst.cstAdCompany=" + AD_CMPNY + " ORDER BY cst.invItemLocation.invItem.iiName, cst.cstDate, cst.cstLineNumber");
			
			Collection invCostings = invCostingHome.getCstByCriteria(jbossQl.toString(), obj);
			
			// get sales
			
			double SLS_QTY_SLD = 0d;
			double SLS_AMOUNT = 0d;		
			double SLS_TAX_AMT = 0d;	
			double TOTAL_SLS_TAX_AMT = 0d;

			Iterator itrTemp = invCostings.iterator();
			if (itrTemp.hasNext()) itrTemp.next();
			
			Iterator itrCost = invCostings.iterator();
			
			while (itrCost.hasNext()) {
				
				LocalInvCosting invCosting = (LocalInvCosting) itrCost.next();
				// get next record
				LocalInvCosting nextInvCosting = null;
				if (itrTemp.hasNext()) 
					nextInvCosting = (LocalInvCosting) itrTemp.next();
				
				if (invCosting.getArInvoiceLineItem() != null) {
		  			
		  			if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {
				
					    if (invCosting.getArInvoiceLineItem().getArInvoice().getInvCreditMemo() == 0) {
					    
					        SLS_QTY_SLD += invCosting.getCstQuantitySold();
					        SLS_AMOUNT += invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount();
					        SLS_TAX_AMT += invCosting.getArInvoiceLineItem().getIliTaxAmount();
					        
					    } else {
					    
					        SLS_QTY_SLD += invCosting.getCstQuantitySold();
					        SLS_AMOUNT -= (invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount());
					        SLS_TAX_AMT -= invCosting.getArInvoiceLineItem().getIliTaxAmount();
					        
					    }
					    
		  			} else {
		  				
		  				SLS_QTY_SLD += invCosting.getCstQuantitySold();
					    SLS_AMOUNT += (invCosting.getArInvoiceLineItem().getIliAmount() + invCosting.getArInvoiceLineItem().getIliTaxAmount()) * (invCosting.getCstQuantitySold() > 0 ? 1:-1);						    
					    SLS_TAX_AMT += invCosting.getArInvoiceLineItem().getIliTaxAmount() * (invCosting.getCstQuantitySold() > 0 ? 1:-1);
					    
		  			}
			    
				} else if (invCosting.getArSalesOrderInvoiceLine() != null) {
				
				    SLS_QTY_SLD += invCosting.getCstQuantitySold();
				    SLS_AMOUNT += invCosting.getArSalesOrderInvoiceLine().getSilAmount() + invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount();
				    SLS_TAX_AMT += invCosting.getArSalesOrderInvoiceLine().getSilTaxAmount();
				    
				} else if (invCosting.getArJobOrderInvoiceLine() != null) {
				
				    SLS_QTY_SLD += invCosting.getCstQuantitySold();
				    SLS_AMOUNT += invCosting.getArJobOrderInvoiceLine().getJilAmount() + invCosting.getArJobOrderInvoiceLine().getJilTaxAmount();
				    SLS_TAX_AMT += invCosting.getArJobOrderInvoiceLine().getJilTaxAmount();
				    
				}
				
				if (SLS_QTY_SLD != 0 && (nextInvCosting == null || 
						(nextInvCosting != null && !invCosting.getInvItemLocation().getInvItem().getIiName().equals(nextInvCosting.getInvItemLocation().getInvItem().getIiName())))) {

					InvRepInventoryProfitabilityDetails details = new InvRepInventoryProfitabilityDetails();
					details.setIpType("Sales");
					details.setIpItemName(invCosting.getInvItemLocation().getInvItem().getIiName());
					details.setIpItemDescription(invCosting.getInvItemLocation().getInvItem().getIiDescription());
					details.setIpItemAdLvCategory(invCosting.getInvItemLocation().getInvItem().getIiAdLvCategory());
					details.setIpUnitOfMeasure(invCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
					details.setIpQuantitySold(SLS_QTY_SLD);
					details.setIpUnitPriceCost(SLS_AMOUNT/SLS_QTY_SLD);
					details.setIpAmount(SLS_AMOUNT);

					tempList.add(details);

					TOTAL_SLS_TAX_AMT += SLS_TAX_AMT;
					SLS_QTY_SLD = 0d;
					SLS_AMOUNT = 0d;		
					SLS_TAX_AMT = 0d;	
					
				}
			    				  																					
			}
			
			
			if (!tempList.isEmpty()) {
				
				// group by 
				if (GROUP_BY.equalsIgnoreCase("CATEGORY")) {

					Collections.sort(tempList, InvRepInventoryProfitabilityDetails.sortByItemCategory);
					
				}
				
				// copy tempList to list
				Iterator itr = tempList.iterator();
				
				while(itr.hasNext()) {
					
					InvRepInventoryProfitabilityDetails details = (InvRepInventoryProfitabilityDetails)itr.next();
					
					list.add(details);
					
				}
				
				tempList = new ArrayList();
				
			}
			
			
			if (TOTAL_SLS_TAX_AMT != 0) {
			
				// OUTPUT TAX - (INVOICES, CREDIT MEMO and MISC RECEIPTS)
			
				InvRepInventoryProfitabilityDetails outputDetails = new InvRepInventoryProfitabilityDetails();
				outputDetails.setIpType("Output Tax");				
				outputDetails.setIpAmount(TOTAL_SLS_TAX_AMT * -1);
				list.add(outputDetails);
				
			}
			
			// get cost of goods sold
			
			double COGS_QTY_SLD = 0d;
			double COGS_AMOUNT = 0d;												
			
			itrTemp = invCostings.iterator();
			if (itrTemp.hasNext()) itrTemp.next();
						
			itrCost = invCostings.iterator();	
			
			while (itrCost.hasNext()) {

				LocalInvCosting invCosting = (LocalInvCosting) itrCost.next();
				// get next record
				LocalInvCosting nextInvCosting = null;
				if (itrTemp.hasNext()) 
					nextInvCosting = (LocalInvCosting) itrTemp.next();
				
				if (invCosting.getArInvoiceLineItem() != null) {
										
					if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {
						
						if (invCosting.getArInvoiceLineItem().getArInvoice().getInvCreditMemo() == 0) {
							
							double taxRate = invCosting.getArInvoiceLineItem().getArInvoice().getArTaxCode().getTcRate() / 100;
							COGS_QTY_SLD += invCosting.getCstQuantitySold();
						    COGS_AMOUNT += costingMethod.equals("Average") ? invCosting.getCstCostOfSales() : EJBCommon.roundIt(invCosting.getInvItemLocation().getInvItem().getIiUnitCost() / (1 + taxRate), (short)2) * invCosting.getCstQuantitySold();
					    
					    } else {
					    	
					    	LocalArInvoice arInvoice = arInvoiceHome.findByInvNumberAndInvCreditMemoAndBrCode(
					    			invCosting.getArInvoiceLineItem().getArInvoice().getInvCmInvoiceNumber(), EJBCommon.FALSE, 
					    			invCosting.getArInvoiceLineItem().getArInvoice().getInvAdBranch(), invCosting.getArInvoiceLineItem().getArInvoice().getInvAdCompany());
					    	
					    	double taxRate = arInvoice.getArTaxCode().getTcRate() / 100;
					        COGS_QTY_SLD += invCosting.getCstQuantitySold();
						    COGS_AMOUNT -= costingMethod.equals("Average") ? invCosting.getCstCostOfSales() * -1 : EJBCommon.roundIt(invCosting.getInvItemLocation().getInvItem().getIiUnitCost() / (1 + taxRate), (short)2) * invCosting.getCstQuantitySold();
					    
					    }
				    
					} else {
					  
						double taxRate = invCosting.getArInvoiceLineItem().getArReceipt().getArTaxCode().getTcRate() / 100;
					    COGS_QTY_SLD += invCosting.getCstQuantitySold();
						COGS_AMOUNT += costingMethod.equals("Average") ? invCosting.getCstCostOfSales() : EJBCommon.roundIt(invCosting.getInvItemLocation().getInvItem().getIiUnitCost() / (1 + taxRate), (short)2) * invCosting.getCstQuantitySold();
					  
					}
					
				} else if (invCosting.getArSalesOrderInvoiceLine() != null) {
					
					double taxRate = invCosting.getArSalesOrderInvoiceLine().getArInvoice().getArTaxCode().getTcRate() / 100;
					COGS_QTY_SLD += invCosting.getCstQuantitySold();
					COGS_AMOUNT += costingMethod.equals("Average") ? invCosting.getCstCostOfSales() : EJBCommon.roundIt(invCosting.getInvItemLocation().getInvItem().getIiUnitCost() / (1 + taxRate), (short)2) * invCosting.getCstQuantitySold();
				  
				} else if (invCosting.getArJobOrderInvoiceLine() != null) {
					
					double taxRate = invCosting.getArJobOrderInvoiceLine().getArInvoice().getArTaxCode().getTcRate() / 100;
					COGS_QTY_SLD += invCosting.getCstQuantitySold();
					COGS_AMOUNT += costingMethod.equals("Average") ? invCosting.getCstCostOfSales() : EJBCommon.roundIt(invCosting.getInvItemLocation().getInvItem().getIiUnitCost() / (1 + taxRate), (short)2) * invCosting.getCstQuantitySold();
				  
				}
				
				if (COGS_QTY_SLD != 0 && (nextInvCosting == null || 
						(nextInvCosting != null && !invCosting.getInvItemLocation().getInvItem().getIiName().equals(nextInvCosting.getInvItemLocation().getInvItem().getIiName())))) {

					InvRepInventoryProfitabilityDetails details = new InvRepInventoryProfitabilityDetails();
					details.setIpType("Cost Of Goods Sold");
					details.setIpItemName(invCosting.getInvItemLocation().getInvItem().getIiName());
					details.setIpItemDescription(invCosting.getInvItemLocation().getInvItem().getIiDescription());
					details.setIpItemAdLvCategory(invCosting.getInvItemLocation().getInvItem().getIiAdLvCategory());
					details.setIpUnitOfMeasure(invCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
					details.setIpQuantitySold(COGS_QTY_SLD);
					details.setIpUnitPriceCost(COGS_AMOUNT / COGS_QTY_SLD);
					details.setIpAmount(COGS_AMOUNT * -1);

					tempList.add(details);

					COGS_QTY_SLD = 0d;
					COGS_AMOUNT = 0d;												
					
				}
																								
			}
			
			if (!tempList.isEmpty()) {
				
				// group by 
				if (GROUP_BY.equalsIgnoreCase("CATEGORY")) {

					Collections.sort(tempList, InvRepInventoryProfitabilityDetails.sortByItemCategory);
					
				}
				
				// copy tempList to list
				Iterator itr = tempList.iterator();
				
				while(itr.hasNext()) {
					
					InvRepInventoryProfitabilityDetails details = (InvRepInventoryProfitabilityDetails)itr.next();
					
					list.add(details);
					
				}
				
				tempList = new ArrayList();
				
			}
						
			
			// OVERHEAD 
			
			jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(ohl) FROM InvOverheadLine ohl ");
			
			firstArgument = true;
			ctr = 0;
			criteriaSize = 0;
			//int number = 0;
			
			// Allocate the size of the object parameter

			if (criteria.containsKey("dateFrom")) {

				criteriaSize++;

			}

			if (criteria.containsKey("dateTo")) {

				criteriaSize++;

			}

			obj = new Object[criteriaSize];
			
			firstLoop = true;
			
			i = branchList.iterator();
			
			while(i.hasNext()) {
				
				if(firstLoop == false) { 
					jbossQl.append("OR ohl.invOverhead.ohAdBranch="); 
				}
				else { 
					firstLoop = false; 
					jbossQl.append("WHERE (ohl.invOverhead.ohAdBranch=");
				}
				
				AdBranchDetails mdetails = (AdBranchDetails) i.next();
				
				jbossQl.append(mdetails.getBrCode() + " ");
				
			}
			
			jbossQl.append(") ");
			
			firstArgument = false;
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("ohl.invOverhead.ohDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("ohl.invOverhead.ohDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("ohl.ohlAdCompany=" + AD_CMPNY + " ORDER BY ohl.invOverheadMemoLine.omlName, ohl.invOverhead.ohDate");
			
			Collection invOverheadLines = invOverheadLineHome.getOhlByCriteria(jbossQl.toString(), obj);
			
			double OH_QTY_SLD = 0d;
			double OH_AMOUNT = 0d;
			
			itrTemp = invOverheadLines.iterator();
			if (itrTemp.hasNext()) itrTemp.next();
			
			Iterator itrOhl = invOverheadLines.iterator();	
			
			while (itrOhl.hasNext()) {
				
				LocalInvOverheadLine invOverheadLine = (LocalInvOverheadLine) itrOhl.next();
				// get next record
				LocalInvOverheadLine nextInvOverheadLine = null;
				if (itrTemp.hasNext()) 
					nextInvOverheadLine = (LocalInvOverheadLine) itrTemp.next();
				
				OH_QTY_SLD += invOverheadLine.getOhlQuantity();
 	  	    	
				OH_AMOUNT += invOverheadLine.getOhlQuantity() *  invOverheadLine.getOhlUnitCost();

				if (OH_QTY_SLD != 0 && (nextInvOverheadLine == null || 
						(nextInvOverheadLine != null & !invOverheadLine.getInvOverheadMemoLine().getOmlName().equals(nextInvOverheadLine.getInvOverheadMemoLine().getOmlName())))) {
						
					InvRepInventoryProfitabilityDetails details = new InvRepInventoryProfitabilityDetails();
					details.setIpType("Overhead");
					details.setIpItemName(invOverheadLine.getInvOverheadMemoLine().getOmlName());
					details.setIpItemDescription(invOverheadLine.getInvOverheadMemoLine().getOmlDescription());
					details.setIpItemAdLvCategory("Overhead");
					details.setIpUnitOfMeasure(invOverheadLine.getInvOverheadMemoLine().getInvUnitOfMeasure().getUomName());
					details.setIpQuantitySold(OH_QTY_SLD);
					details.setIpUnitPriceCost(OH_AMOUNT / OH_QTY_SLD);
					details.setIpAmount(OH_AMOUNT * -1);
					
					list.add(details);
					
					OH_QTY_SLD = 0d;
					OH_AMOUNT = 0d;
					
				}
				
			}
					
			if (list.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			return list; 	  
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepInventoryProfitabilityControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("InvRepInventoryProfitabilityControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("InvRepInventoryProfitabilityControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
	
	// private methods
	
	private double convertByUomFromAndUomToAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvUnitOfMeasure invToUnitOfMeasure, double QTY_SLD, Integer AD_CMPNY) {
		
	Debug.print("InvRepInventoryProfitabilityControllerBean convertByUomFromAndUomToAndQuantity");		        
    
	LocalAdPreferenceHome adPreferenceHome = null;
            
    // Initialize EJB Home
    
    try {
        
        adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
                    
    } catch (NamingException ex) {
        
        throw new EJBException(ex.getMessage());
        
    }
            
    try {
    
        LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
    	        	
    	return EJBCommon.roundIt(QTY_SLD * invFromUnitOfMeasure.getUomConversionFactor() / invToUnitOfMeasure.getUomConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
    						    		        		
    } catch (Exception ex) {
    	
    	Debug.printStackTrace(ex);
    	getSessionContext().setRollbackOnly();
    	throw new EJBException(ex.getMessage());
    	
    }
	
}
	
	// SessionBean methods
	
	/**
* @ejb:create-method view-type="remote"
**/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvRepInventoryProfitabilityControllerBean ejbCreate");
		
	}
	
}

