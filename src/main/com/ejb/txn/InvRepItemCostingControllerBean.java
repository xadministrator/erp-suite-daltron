package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.ar.LocalArSalesOrderLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvAssemblyTransferLine;
import com.ejb.inv.LocalInvAssemblyTransferLineHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLine;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPhysicalInventoryHome;
import com.ejb.inv.LocalInvPhysicalInventoryLineHome;
import com.ejb.inv.LocalInvStockIssuanceLine;
import com.ejb.inv.LocalInvStockIssuanceLineHome;
import com.ejb.inv.LocalInvStockTransferLine;
import com.ejb.inv.LocalInvStockTransferLineHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepItemCostingDetails;
import com.util.InvRepItemLedgerDetails;

/**
 * @ejb:bean name="InvRepItemCostingControllerEJB" display-name="Used for
 *           generation of item costing reports" type="Stateless"
 *           view-type="remote" jndi-name="ejb/InvRepItemCostingControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepItemCostingController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepItemCostingControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser" role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
 */

public class InvRepItemCostingControllerBean extends AbstractSessionBean {

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {

		Debug.print("InvRepItemCostingControllerBean getAdLvInvItemCategoryAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue) i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {

		Debug.print("InvRepItemCostingControllerBean getInvLocAll");

		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invLocationHome = (LocalInvLocationHome) EJBHomeFactory.lookUpLocalHome(LocalInvLocationHome.JNDI_NAME,
					LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			invLocations = invLocationHome.findLocAll(AD_CMPNY);

			if (invLocations.isEmpty()) {

				return null;

			}

			Iterator i = invLocations.iterator();

			while (i.hasNext()) {

				LocalInvLocation invLocation = (LocalInvLocation) i.next();
				String details = invLocation.getLocName();

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList executeInvRepItemCosting(HashMap criteria, String LCTN, String CTGRY, boolean SHW_CMMTTD_QNTTY,
			boolean INCLD_UNPSTD, boolean SHW_ZRS, ArrayList branchList, Integer AD_CMPNY)
			throws GlobalNoRecordFoundException {

		Debug.print("InvRepItemCostingControllerBean executeInvRepItemCosting");

		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
		LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalArSalesOrderLineHome arSalesOrderLineHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalInvStockIssuanceLineHome invStockIssuanceLineHome = null;
		LocalInvAssemblyTransferLineHome invAssemblyTransferLineHome = null;
		LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
		LocalInvStockTransferLineHome invStockTransferLineHome = null;
		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
			invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
			invLocationHome = (LocalInvLocationHome) EJBHomeFactory.lookUpLocalHome(LocalInvLocationHome.JNDI_NAME,
					LocalInvLocationHome.class);
			arSalesOrderLineHome = (LocalArSalesOrderLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			invStockIssuanceLineHome = (LocalInvStockIssuanceLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvStockIssuanceLineHome.JNDI_NAME, LocalInvStockIssuanceLineHome.class);
			invAssemblyTransferLineHome = (LocalInvAssemblyTransferLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvAssemblyTransferLineHome.JNDI_NAME, LocalInvAssemblyTransferLineHome.class);
			invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
			invStockTransferLineHome = (LocalInvStockTransferLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class);
			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
			apVoucherLineItemHome = (LocalApVoucherLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Integer brCode = null;
			Integer locCode = null;
			String locName = "";

			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(cst) FROM InvCosting cst WHERE (");

			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = 0;

			Object obj[] = null;

			if (branchList.isEmpty())
				throw new GlobalNoRecordFoundException();

			Iterator brIter = branchList.iterator();

			AdBranchDetails bdetails = (AdBranchDetails) brIter.next();
			jbossQl.append("cst.cstAdBranch=" + bdetails.getBrCode());

			while (brIter.hasNext()) {

				bdetails = (AdBranchDetails) brIter.next();

				jbossQl.append(" OR cst.cstAdBranch=" + bdetails.getBrCode());

			}

			jbossQl.append(") ");

			firstArgument = false;

			// Allocate the size of the object parameter

			if (criteria.containsKey("category")) {

				criteriaSize++;

			}

			if (criteria.containsKey("location")) {

				criteriaSize++;
				locName = (String) criteria.get("location");
			}

			if (criteria.containsKey("dateFrom")) {

				criteriaSize++;

			}

			if (criteria.containsKey("dateTo")) {

				criteriaSize++;

			}

			if (criteria.containsKey("itemClass")) {

				criteriaSize++;

			}

			obj = new Object[criteriaSize];

			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append(
						"cst.invItemLocation.invItem.iiName  LIKE '%" + (String) criteria.get("itemName") + "%' ");

			}

			if (criteria.containsKey("category")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("cst.invItemLocation.invItem.iiAdLvCategory=?" + (ctr + 1) + " ");
				obj[ctr] = (String) criteria.get("category");
				ctr++;

			}

			if (criteria.containsKey("location")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("cst.invItemLocation.invLocation.locName=?" + (ctr + 1) + " ");
				obj[ctr] = (String) criteria.get("location");
				ctr++;

			}

			if (criteria.containsKey("dateFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.cstDate>=?" + (ctr + 1) + " ");
				obj[ctr] = (Date) criteria.get("dateFrom");
				ctr++;

			}

			if (criteria.containsKey("dateTo")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.cstDate<=?" + (ctr + 1) + " ");
				obj[ctr] = (Date) criteria.get("dateTo");
				ctr++;

			}

			if (criteria.containsKey("itemClass")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("cst.invItemLocation.invItem.iiClass=?" + (ctr + 1) + " ");
				obj[ctr] = (String) criteria.get("itemClass");
				ctr++;

			}

			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			jbossQl.append("cst.cstAdCompany=" + AD_CMPNY + " ");

			jbossQl.append(
					"ORDER BY cst.invItemLocation.invItem.iiName, cst.cstDate, cst.cstDateToLong, cst.cstLineNumber");

			Collection invCostings = invCostingHome.getCstByCriteria(jbossQl.toString(), obj);

			Iterator i = invCostings.iterator();

			while (i.hasNext()) {

				LocalInvCosting invCosting = (LocalInvCosting) i.next();

				InvRepItemCostingDetails details = new InvRepItemCostingDetails();
				details.setIcItemName(invCosting.getInvItemLocation().getInvItem().getIiName() + "-"
						+ invCosting.getInvItemLocation().getInvItem().getIiDescription());
				details.setIcItemCategory(invCosting.getInvItemLocation().getInvItem().getIiAdLvCategory());
				details.setIcItemUnitOfMeasure(
						invCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
				details.setIcItemLocation(invCosting.getInvItemLocation().getInvLocation().getLocName());
				details.setIcDate(invCosting.getCstDate());
				details.setIcItemLocation1(LCTN);
				details.setIcItemCategory1(CTGRY);

				// beginning
				LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
						invCosting.getInvItemLocation(), (Date) criteria.get("dateFrom"), brCode, AD_CMPNY);
				if (invBeginningCosting != null) {

					details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
					details.setIcBeginningUnitCost(
							invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
					details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());

				}

				// get item cost
				double ITEM_COST = EJBCommon.roundIt(invCosting.getCstItemCost() / invCosting.getCstQuantityReceived(),
						this.getGlFcPrecisionUnit(AD_CMPNY));
				details.setIcItemCost(ITEM_COST);
				details.setIcActualCost(invCosting.getCstItemCost());

				details.setIcQuantityReceived(invCosting.getCstQuantityReceived());
				details.setIcAdjustQuantity(invCosting.getCstAdjustQuantity());
				details.setIcAdjustCost(invCosting.getCstAdjustCost());
				details.setIcAssemblyQuantity(invCosting.getCstAssemblyQuantity());
				details.setIcAssemblyCost(invCosting.getCstAssemblyCost());
				details.setIcQuantitySold(invCosting.getCstQuantitySold());
				details.setIcCostOfSales(invCosting.getCstCostOfSales());
				details.setIcRemainingQuantity(invCosting.getCstRemainingQuantity());
				details.setIcRemainingValue(invCosting.getCstRemainingValue());
				details.setIcRemainingUnitCost(
						invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

				if (invCosting.getInvAdjustmentLine() != null) {

					details.setIcSourceDocumentNumber(
							invCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber());
					details.setIcReferenceNumber(
							invCosting.getInvAdjustmentLine().getInvAdjustment().getAdjReferenceNumber());
					details.setIcSourceDocument("INV");

				} else if (invCosting.getInvStockIssuanceLine() != null) {

					details.setIcSourceDocumentNumber(
							invCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber());
					details.setIcReferenceNumber(
							invCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiReferenceNumber());
					details.setIcSourceDocument("INV");

				} else if (invCosting.getInvAssemblyTransferLine() != null) {

					details.setIcSourceDocumentNumber(
							invCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber());
					details.setIcReferenceNumber(
							invCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrReferenceNumber());
					details.setIcSourceDocument("INV");

				} else if (invCosting.getInvBuildUnbuildAssemblyLine() != null) {

					details.setIcSourceDocumentNumber(invCosting.getInvBuildUnbuildAssemblyLine()
							.getInvBuildUnbuildAssembly().getBuaDocumentNumber());
					details.setIcReferenceNumber(invCosting.getInvBuildUnbuildAssemblyLine()
							.getInvBuildUnbuildAssembly().getBuaReferenceNumber());
					details.setIcSourceDocument("INV");

				} else if (invCosting.getInvStockTransferLine() != null) {

					details.setIcSourceDocumentNumber(
							invCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber());
					details.setIcReferenceNumber(
							invCosting.getInvStockTransferLine().getInvStockTransfer().getStReferenceNumber());
					details.setIcSourceDocument("INV");

				} else if (invCosting.getInvBranchStockTransferLine() != null) {

					details.setIcSourceDocumentNumber(
							invCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber());
					details.setIcReferenceNumber(invCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer()
							.getBstTransferOutNumber());
					details.setIcSourceDocument("INV");

				} else if (invCosting.getApVoucherLineItem() != null) {

					if (invCosting.getApVoucherLineItem().getApVoucher() != null) {

						details.setIcSourceDocumentNumber(
								invCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber());
						details.setIcReferenceNumber(
								invCosting.getApVoucherLineItem().getApVoucher().getVouReferenceNumber());
						details.setIcSourceDocument("AP");

					} else if (invCosting.getApVoucherLineItem().getApCheck() != null) {

						details.setIcSourceDocumentNumber(
								invCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber());
						details.setIcReferenceNumber(invCosting.getApVoucherLineItem().getApCheck().getChkNumber());
						details.setIcSourceDocument("AP");

					}

				} else if (invCosting.getArInvoiceLineItem() != null) {

					if (invCosting.getArInvoiceLineItem().getArInvoice() != null) {

						details.setIcSourceDocumentNumber(
								invCosting.getArInvoiceLineItem().getArInvoice().getInvNumber());
						details.setIcReferenceNumber(
								invCosting.getArInvoiceLineItem().getArInvoice().getInvReferenceNumber());
						details.setIcSourceDocument("AR");

					} else if (invCosting.getArInvoiceLineItem().getArReceipt() != null) {

						details.setIcSourceDocumentNumber(
								invCosting.getArInvoiceLineItem().getArReceipt().getRctNumber());
						details.setIcReferenceNumber(
								invCosting.getArInvoiceLineItem().getArReceipt().getRctReferenceNumber());
						details.setIcSourceDocument("AR");

					}

				} else if (invCosting.getApPurchaseOrderLine() != null) {

					details.setIcSourceDocumentNumber(
							invCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber());
					details.setIcReferenceNumber(
							invCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoReferenceNumber());
					details.setIcSourceDocument("AP");

				} else if (invCosting.getArSalesOrderInvoiceLine() != null) {

					details.setIcSourceDocumentNumber(
							invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber());
					details.setIcReferenceNumber(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber());
					details.setIcSourceDocument("AR");

				}

				double COST = 0d;

				if (invCosting.getCstQuantityReceived() != 0d) {

					if (invCosting.getCstQuantityReceived() > 0) {

						details.setIcInQuantity(invCosting.getCstQuantityReceived());
						details.setIcInAmount(invCosting.getCstItemCost());
						COST = Math.abs(invCosting.getCstItemCost() / invCosting.getCstQuantityReceived());
						details.setIcInUnitCost(COST);

					} else if (invCosting.getCstQuantityReceived() < 0) {

						details.setIcOutQuantity(invCosting.getCstQuantityReceived() * -1);
						details.setIcOutAmount(invCosting.getCstItemCost() * -1);
						COST = Math.abs(invCosting.getCstItemCost() / invCosting.getCstQuantityReceived());
						details.setIcOutUnitCost(COST);
					}

				} else if (invCosting.getCstAdjustQuantity() != 0d) {

					if (invCosting.getCstAdjustQuantity() > 0) {

						details.setIcInQuantity(invCosting.getCstAdjustQuantity());
						details.setIcInAmount(invCosting.getCstAdjustCost());
						COST = Math.abs(invCosting.getCstAdjustCost() / invCosting.getCstAdjustQuantity());
						details.setIcInUnitCost(COST);

					} else if (invCosting.getCstAdjustQuantity() < 0) {

						details.setIcOutQuantity(invCosting.getCstAdjustQuantity() * -1);
						details.setIcOutAmount(invCosting.getCstAdjustCost() * -1);
						COST = Math.abs(invCosting.getCstAdjustCost() / invCosting.getCstAdjustQuantity());
						details.setIcOutUnitCost(COST);
					}

				} else if (invCosting.getCstAssemblyQuantity() != 0d) {

					if (invCosting.getCstAssemblyQuantity() > 0) {

						details.setIcInQuantity(invCosting.getCstAssemblyQuantity());
						details.setIcInAmount(invCosting.getCstAssemblyCost());
						COST = Math.abs(invCosting.getCstAssemblyCost() / invCosting.getCstAssemblyQuantity());
						details.setIcInUnitCost(COST);

					} else if (invCosting.getCstAssemblyQuantity() < 0) {

						details.setIcOutQuantity(invCosting.getCstAssemblyQuantity() * -1);
						details.setIcOutAmount(invCosting.getCstAssemblyCost() * -1);
						COST = Math.abs(invCosting.getCstAssemblyCost() / invCosting.getCstAssemblyQuantity());
						details.setIcOutUnitCost(COST);

					}

				} else if (invCosting.getCstQuantitySold() != 0d) {

					if (invCosting.getCstQuantitySold() > 0) {

						details.setIcOutQuantity(invCosting.getCstQuantitySold());
						details.setIcOutAmount(invCosting.getCstCostOfSales());
						COST = Math.abs(invCosting.getCstCostOfSales() / invCosting.getCstQuantitySold());
						details.setIcOutUnitCost(COST);

					} else if (invCosting.getCstQuantitySold() < 0) {

						details.setIcInQuantity(invCosting.getCstQuantitySold() * -1);
						details.setIcInAmount(invCosting.getCstCostOfSales() * -1);
						COST = Math.abs(invCosting.getCstCostOfSales() / invCosting.getCstQuantitySold());
						details.setIcInUnitCost(COST);

					}

				}

				list.add(details);
				// Collections.sort(list, InvRepItemCostingDetails.ItemCostingComparator);
			}

			// start

			if (SHW_CMMTTD_QNTTY) {

				System.out.println("include committed yes");

				Collection arSalesOrderLines = arSalesOrderLineHome.findOpenSolAll(AD_CMPNY);

				Iterator solIter = arSalesOrderLines.iterator();

				while (solIter.hasNext()) {

					LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine) solIter.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					if (arSalesOrderLine.getArSalesOrderInvoiceLines().size() == 0
							&& arSalesOrderLine.getArSalesOrder().getSoAdBranch().equals(brCode)
							&& arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(locName)) {

						Date earliestDate = null;
						Date soDate = arSalesOrderLine.getArSalesOrder().getSoDate();
						LocalInvItem invItem = arSalesOrderLine.getInvItemLocation().getInvItem();

						LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
								arSalesOrderLine.getInvItemLocation(), (Date) criteria.get("dateFrom"), brCode,
								AD_CMPNY);
						if (invBeginningCosting != null) {

							details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
							details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
									/ invBeginningCosting.getCstRemainingQuantity());
							details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
							earliestDate = invBeginningCosting.getCstDate();
						}

						GregorianCalendar gcDateFrom = new GregorianCalendar();
						Date dateTo = (Date) criteria.get("dateTo");
						if (dateTo == null)
							dateTo = new Date();
						Date soDateFrom = null;
						gcDateFrom.setTime(dateTo != null ? dateTo : new Date());
						gcDateFrom.add(Calendar.MONTH, -1);

						soDateFrom = gcDateFrom.getTime();

						if (soDate.compareTo(soDateFrom) < 0)
							continue;

						if (dateTo != null && soDate.compareTo(dateTo) > 0)
							continue;

						if (!this.filterByOptionalCriteria(criteria, invItem, soDate, earliestDate))
							continue;

						details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
						details.setIcDate(soDate);
						details.setIcQuantitySold(arSalesOrderLine.getSolQuantity());
						details.setIcCostOfSales(arSalesOrderLine.getSolAmount());
						details.setIcSourceDocument("SO");
						details.setIcSourceDocumentNumber(arSalesOrderLine.getArSalesOrder().getSoDocumentNumber());
						details.setIcReferenceNumber(arSalesOrderLine.getArSalesOrder().getSoReferenceNumber());
						details.setIcItemCategory(invItem.getIiAdLvCategory());
						details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
						details.setIcItemLocation(
								invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
						details.setIcItemLocation1(LCTN);
						details.setIcItemCategory1(CTGRY);

						list.add(details);
					}
				}
				// Collections.sort(list, InvRepItemCostingDetails.ItemCostingComparator);
			}

			if (INCLD_UNPSTD) {

				System.out.println("include unposted YES");

				// INV_ADJUSTMENT_LINE
				Collection invAdjustmentLines = invAdjustmentLineHome.findUnpostedAdjByLocNameAndAdBranch(locName,
						brCode, AD_CMPNY);

				Iterator unpstdIter = invAdjustmentLines.iterator();

				while (unpstdIter.hasNext()) {

					LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) unpstdIter.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date adjDate = invAdjustmentLine.getInvAdjustment().getAdjDate();
					LocalInvItem invItem = invAdjustmentLine.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invAdjustmentLine.getInvItemLocation(), (Date) criteria.get("dateFrom"), brCode, AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, adjDate, earliestDate))
						continue;
					if (invAdjustmentLine.getAlAdjustQuantity() > 0) {

						details.setIcInQuantity(invAdjustmentLine.getAlAdjustQuantity());
						details.setIcInAmount(
								invAdjustmentLine.getAlUnitCost() * invAdjustmentLine.getAlAdjustQuantity());
						details.setIcInUnitCost(invAdjustmentLine.getAlUnitCost());

					} else if (invAdjustmentLine.getAlAdjustQuantity() < 0) {

						details.setIcOutQuantity(invAdjustmentLine.getAlAdjustQuantity() * -1);
						details.setIcOutAmount(
								invAdjustmentLine.getAlUnitCost() * invAdjustmentLine.getAlAdjustQuantity() * -1);
						details.setIcOutUnitCost(invAdjustmentLine.getAlUnitCost());
					}
					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(adjDate);
					details.setIcAdjustQuantity(invAdjustmentLine.getAlAdjustQuantity());
					details.setIcAdjustCost(
							invAdjustmentLine.getAlAdjustQuantity() * invAdjustmentLine.getAlUnitCost());
					details.setIcSourceDocument("ADJ");
					details.setIcSourceDocumentNumber(invAdjustmentLine.getInvAdjustment().getAdjDocumentNumber());
					details.setIcReferenceNumber(invAdjustmentLine.getInvAdjustment().getAdjReferenceNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcItemLocation(invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// INV_STOCK_ISSUANCE_LINE
				Collection invStockIssuanceLines = invStockIssuanceLineHome.findUnpostedSiByLocNameAndAdBranch(locName,
						brCode, AD_CMPNY);

				unpstdIter = invStockIssuanceLines.iterator();

				while (unpstdIter.hasNext()) {

					LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine) unpstdIter.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date siDate = invStockIssuanceLine.getInvStockIssuance().getSiDate();
					LocalInvItem invItem = invStockIssuanceLine.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invStockIssuanceLine.getInvItemLocation(), (Date) criteria.get("dateFrom"), brCode,
							AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, siDate, earliestDate))
						continue;

					if (invStockIssuanceLine.getSilIssueQuantity() > 0) {

						details.setIcInQuantity(invStockIssuanceLine.getSilIssueQuantity());
						details.setIcInAmount(invStockIssuanceLine.getSilIssueCost());
						details.setIcInUnitCost(invStockIssuanceLine.getSilUnitCost());

					} else if (invStockIssuanceLine.getSilIssueQuantity() < 0) {

						details.setIcOutQuantity(invStockIssuanceLine.getSilIssueQuantity() * -1);
						details.setIcOutAmount(invStockIssuanceLine.getSilIssueCost() * -1);
						details.setIcOutUnitCost(invStockIssuanceLine.getSilUnitCost());
					}
					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(siDate);
					details.setIcAdjustQuantity(invStockIssuanceLine.getSilIssueQuantity());
					details.setIcAdjustCost(invStockIssuanceLine.getSilIssueCost());
					details.setIcSourceDocument("ISS");
					details.setIcSourceDocumentNumber(invStockIssuanceLine.getInvStockIssuance().getSiDocumentNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber(invStockIssuanceLine.getInvStockIssuance().getSiReferenceNumber());
					details.setIcItemLocation(invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// INV_ASSEMBLY_TRANSFER_LINE
				Collection invAssemblyTransferLines = invAssemblyTransferLineHome
						.findUnpostedAtrByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = invAssemblyTransferLines.iterator();

				while (unpstdIter.hasNext()) {

					LocalInvAssemblyTransferLine invAssemblyTransferLine = (LocalInvAssemblyTransferLine) unpstdIter
							.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date atrDate = invAssemblyTransferLine.getInvAssemblyTransfer().getAtrDate();
					LocalInvItem invItem = invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation()
							.getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation(),
							(Date) criteria.get("dateFrom"), brCode, AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, atrDate, earliestDate))
						continue;

					if (invAssemblyTransferLine.getAtlAssembleQuantity() > 0) {

						details.setIcInQuantity(invAssemblyTransferLine.getAtlAssembleQuantity());
						details.setIcInAmount(invAssemblyTransferLine.getAtlAssembleCost());
						details.setIcInUnitCost(Math.abs(invAssemblyTransferLine.getAtlAssembleCost()
								/ invAssemblyTransferLine.getAtlAssembleQuantity()));

					} else if (invAssemblyTransferLine.getAtlAssembleQuantity() < 0) {

						details.setIcOutQuantity(invAssemblyTransferLine.getAtlAssembleQuantity() * -1);
						details.setIcOutAmount(invAssemblyTransferLine.getAtlAssembleCost() * -1);
						details.setIcOutUnitCost(Math.abs(invAssemblyTransferLine.getAtlAssembleCost()
								/ invAssemblyTransferLine.getAtlAssembleQuantity()));
					}

					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(atrDate);
					details.setIcAssemblyQuantity(invAssemblyTransferLine.getAtlAssembleQuantity());
					details.setIcAssemblyCost(invAssemblyTransferLine.getAtlAssembleCost());
					details.setIcSourceDocument("AT");
					details.setIcSourceDocumentNumber(
							invAssemblyTransferLine.getInvAssemblyTransfer().getAtrDocumentNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber(
							invAssemblyTransferLine.getInvAssemblyTransfer().getAtrReferenceNumber());
					details.setIcItemLocation(invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// INV_BUILD_UNBUILD_ASSEMBLY_LINE
				Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssemblyLineHome
						.findUnpostedBuaByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = invBuildUnbuildAssemblyLines.iterator();

				while (unpstdIter.hasNext()) {

					LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine) unpstdIter
							.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date buaDate = invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDate();
					LocalInvItem invItem = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invBuildUnbuildAssemblyLine.getInvItemLocation(), (Date) criteria.get("dateFrom"), brCode,
							AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, buaDate, earliestDate))
						continue;

					if (invBuildUnbuildAssemblyLine.getBlBuildQuantity() > 0) {

						details.setIcInQuantity(invBuildUnbuildAssemblyLine.getBlBuildQuantity());
						details.setIcInAmount(
								invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost()
										* invBuildUnbuildAssemblyLine.getBlBuildQuantity());
						details.setIcInUnitCost(
								invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost());

					} else if (invBuildUnbuildAssemblyLine.getBlBuildQuantity() < 0) {

						details.setIcOutQuantity(invBuildUnbuildAssemblyLine.getBlBuildQuantity() * -1);
						details.setIcOutAmount(
								invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost()
										* invBuildUnbuildAssemblyLine.getBlBuildQuantity() * -1);
						details.setIcOutUnitCost(
								invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost());
					}

					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(buaDate);
					details.setIcAssemblyQuantity(invBuildUnbuildAssemblyLine.getBlBuildQuantity());
					details.setIcAssemblyCost(
							invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost()
									* invBuildUnbuildAssemblyLine.getBlBuildQuantity());
					details.setIcSourceDocument("BUA");
					details.setIcSourceDocumentNumber(
							invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDocumentNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber(
							invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaReferenceNumber());
					details.setIcItemLocation(invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// INV_STOCK_TRANSFER_LINE OUT
				locCode = invLocationHome.findByLocName(locName, AD_CMPNY).getLocCode();
				Collection invStockTransferLines = invStockTransferLineHome.findUnpostedStByLocCodeAndAdBranch(locCode,
						brCode, AD_CMPNY);

				unpstdIter = invStockTransferLines.iterator();

				while (unpstdIter.hasNext()) {

					LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) unpstdIter.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date stDate = invStockTransferLine.getInvStockTransfer().getStDate();
					LocalInvItem invItem = invStockTransferLine.getInvItem();
					LocalInvLocation invLocation = invLocationHome
							.findByPrimaryKey(invStockTransferLine.getStlLocationFrom());
					LocalInvItemLocation invItemLocation = invItemLocationHome
							.findByIiNameAndLocName(invItem.getIiName(), invLocation.getLocName(), AD_CMPNY);

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invItemLocation, (Date) criteria.get("dateFrom"), brCode, AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, stDate, earliestDate))
						continue;

					if (invStockTransferLine.getStlQuantityDelivered() < 0) {

						details.setIcInQuantity(invStockTransferLine.getStlQuantityDelivered());
						details.setIcInAmount(invStockTransferLine.getStlAmount());
						details.setIcInUnitCost(invStockTransferLine.getStlUnitCost());

					} else if (invStockTransferLine.getStlQuantityDelivered() > 0) {

						details.setIcOutQuantity(invStockTransferLine.getStlQuantityDelivered());
						details.setIcOutAmount(invStockTransferLine.getStlAmount());
						details.setIcOutUnitCost(invStockTransferLine.getStlUnitCost());
					}

					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(stDate);
					details.setIcAdjustQuantity(invStockTransferLine.getStlQuantityDelivered() * (-1));
					details.setIcAdjustCost(invStockTransferLine.getStlAmount());
					details.setIcSourceDocument("ST");
					details.setIcSourceDocumentNumber(invStockTransferLine.getInvStockTransfer().getStDocumentNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber(invStockTransferLine.getInvStockTransfer().getStReferenceNumber());
					details.setIcItemLocation(invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// INV_STOCK_TRANSFER_LINE IN
				locCode = invLocationHome.findByLocName(locName, AD_CMPNY).getLocCode();
				Collection invStockTransferToLines = invStockTransferLineHome
						.findUnpostedStByLocToCodeAndAdBranch(locCode, brCode, AD_CMPNY);

				unpstdIter = invStockTransferToLines.iterator();

				while (unpstdIter.hasNext()) {

					LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) unpstdIter.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date stDate = invStockTransferLine.getInvStockTransfer().getStDate();
					LocalInvItem invItem = invStockTransferLine.getInvItem();
					LocalInvLocation invLocation = invLocationHome
							.findByPrimaryKey(invStockTransferLine.getStlLocationFrom());
					LocalInvItemLocation invItemLocation = invItemLocationHome
							.findByIiNameAndLocName(invItem.getIiName(), invLocation.getLocName(), AD_CMPNY);

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invItemLocation, (Date) criteria.get("dateFrom"), brCode, AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, stDate, earliestDate))
						continue;

					if (invStockTransferLine.getStlQuantityDelivered() > 0) {

						details.setIcInQuantity(invStockTransferLine.getStlQuantityDelivered());
						details.setIcInAmount(invStockTransferLine.getStlAmount());
						details.setIcInUnitCost(invStockTransferLine.getStlUnitCost());

					} else if (invStockTransferLine.getStlQuantityDelivered() < 0) {

						details.setIcOutQuantity(invStockTransferLine.getStlQuantityDelivered());
						details.setIcOutAmount(invStockTransferLine.getStlAmount());
						details.setIcOutUnitCost(invStockTransferLine.getStlUnitCost());
					}

					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(stDate);
					details.setIcAdjustQuantity(invStockTransferLine.getStlQuantityDelivered());
					details.setIcAdjustCost(invStockTransferLine.getStlAmount());
					details.setIcSourceDocument("ST");
					details.setIcSourceDocumentNumber(invStockTransferLine.getInvStockTransfer().getStDocumentNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber(invStockTransferLine.getInvStockTransfer().getStReferenceNumber());
					details.setIcItemLocation(invItemLocation.getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// INV_BRANCH_STOCK_TRANSFER_LINE
				Collection invBranchStockTransferLines = invBranchStockTransferLineHome
						.findUnpostedBstByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = invBranchStockTransferLines.iterator();

				while (unpstdIter.hasNext()) {

					LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) unpstdIter
							.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date bstDate = invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate();
					LocalInvItem invItem = invBranchStockTransferLine.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invBranchStockTransferLine.getInvItemLocation(), (Date) criteria.get("dateFrom"), brCode,
							AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, bstDate, earliestDate))
						continue;

					if (invBranchStockTransferLine.getBslQuantityReceived() > 0) {

						details.setIcInQuantity(invBranchStockTransferLine.getBslQuantityReceived());
						details.setIcInAmount(invBranchStockTransferLine.getBslAmount());
						details.setIcInUnitCost(invBranchStockTransferLine.getBslUnitCost());

					} else if (invBranchStockTransferLine.getBslQuantityReceived() < 0) {

						details.setIcOutQuantity(invBranchStockTransferLine.getBslQuantityReceived() * -1);
						details.setIcOutAmount(invBranchStockTransferLine.getBslAmount() * -1);
						details.setIcOutUnitCost(invBranchStockTransferLine.getBslUnitCost());
					}

					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(bstDate);
					details.setIcAdjustQuantity(invBranchStockTransferLine.getBslQuantityReceived());
					details.setIcAdjustCost(invBranchStockTransferLine.getBslAmount());
					details.setIcSourceDocument(
							invBranchStockTransferLine.getInvBranchStockTransfer().getBstType().equals("OUT") ? "BO"
									: "BI");
					details.setIcSourceDocumentNumber(
							invBranchStockTransferLine.getInvBranchStockTransfer().getBstNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber("");
					details.setIcItemLocation(
							invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// AP_VOUCHER_LINE_ITEM
				// a) apVoucher
				Collection apVoucherLineItems = apVoucherLineItemHome.findUnpostedVouByLocNameAndAdBranch(locName,
						brCode, AD_CMPNY);

				unpstdIter = apVoucherLineItems.iterator();

				while (unpstdIter.hasNext()) {

					LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) unpstdIter.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date vouDate = apVoucherLineItem.getApVoucher().getVouDate();
					LocalInvItem invItem = apVoucherLineItem.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							apVoucherLineItem.getInvItemLocation(), (Date) criteria.get("dateFrom"), brCode, AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, vouDate, earliestDate))
						continue;

					double ITEM_COST = EJBCommon.roundIt(
							apVoucherLineItem.getVliAmount() / apVoucherLineItem.getVliQuantity(),
							this.getGlFcPrecisionUnit(AD_CMPNY));
					details.setIcItemCost(ITEM_COST);

					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(vouDate);
					details.setIcQuantityReceived(apVoucherLineItem.getVliQuantity());
					details.setIcActualCost(apVoucherLineItem.getVliAmount());
					details.setIcSourceDocument("VOU");
					details.setIcSourceDocumentNumber(apVoucherLineItem.getApVoucher().getVouDocumentNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber(apVoucherLineItem.getApVoucher().getVouReferenceNumber());
					details.setIcItemLocation(invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// b) apCheck
				apVoucherLineItems = apVoucherLineItemHome.findUnpostedChkByLocNameAndAdBranch(locName, brCode,
						AD_CMPNY);

				unpstdIter = apVoucherLineItems.iterator();

				while (unpstdIter.hasNext()) {

					LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) unpstdIter.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date chkDate = apVoucherLineItem.getApCheck().getChkDate();
					LocalInvItem invItem = apVoucherLineItem.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							apVoucherLineItem.getInvItemLocation(), (Date) criteria.get("dateFrom"), brCode, AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, chkDate, earliestDate))
						continue;

					double ITEM_COST = EJBCommon.roundIt(
							apVoucherLineItem.getVliAmount() / apVoucherLineItem.getVliQuantity(),
							this.getGlFcPrecisionUnit(AD_CMPNY));
					details.setIcItemCost(ITEM_COST);

					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(chkDate);
					details.setIcQuantityReceived(apVoucherLineItem.getVliQuantity());
					details.setIcActualCost(apVoucherLineItem.getVliAmount());
					details.setIcSourceDocument("CHK");
					details.setIcSourceDocumentNumber(apVoucherLineItem.getApCheck().getChkDocumentNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber(apVoucherLineItem.getApVoucher().getVouReferenceNumber());
					details.setIcItemLocation(invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// AR_INVOICE_LINE_ITEM
				// a) arInvoice
				Collection arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedInvcByLocNameAndAdBranch(locName,
						brCode, AD_CMPNY);

				unpstdIter = arInvoiceLineItems.iterator();

				while (unpstdIter.hasNext()) {

					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) unpstdIter.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date invDate = arInvoiceLineItem.getArInvoice().getInvDate();
					LocalInvItem invItem = arInvoiceLineItem.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							arInvoiceLineItem.getInvItemLocation(), (Date) criteria.get("dateFrom"), brCode, AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, invDate, earliestDate))
						continue;

					// Get Last Costing
					LocalInvCosting invLastCosting = invCostingHome
							.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(invDate,
									arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(),
									arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), brCode,
									AD_CMPNY);

					double COST = Math
							.abs(invLastCosting.getCstRemainingValue() / invLastCosting.getCstRemainingQuantity());
					COST = EJBCommon.roundIt(COST, this.getGlFcPrecisionUnit(AD_CMPNY));

					double CST_CST_OF_SLS = COST * arInvoiceLineItem.getIliQuantity();
					CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, this.getGlFcPrecisionUnit(AD_CMPNY));

					details.setIcOutQuantity(arInvoiceLineItem.getIliQuantity());
					details.setIcOutUnitCost(COST);
					details.setIcOutAmount(CST_CST_OF_SLS);

					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(invDate);
					details.setIcQuantitySold(arInvoiceLineItem.getIliQuantity());
					// details.setIcCostOfSales(arInvoiceLineItem.getIliAmount());
					details.setIcCostOfSales(CST_CST_OF_SLS);
					details.setIcSourceDocument("SI");
					details.setIcSourceDocumentNumber(arInvoiceLineItem.getArInvoice().getInvNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber(arInvoiceLineItem.getArInvoice().getInvReferenceNumber());
					details.setIcItemLocation(invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// b) arReceipt
				arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedRctByLocNameAndAdBranch(locName, brCode,
						AD_CMPNY);

				unpstdIter = arInvoiceLineItems.iterator();

				while (unpstdIter.hasNext()) {

					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) unpstdIter.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date rctDate = arInvoiceLineItem.getArReceipt().getRctDate();
					LocalInvItem invItem = arInvoiceLineItem.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							arInvoiceLineItem.getInvItemLocation(), (Date) criteria.get("dateFrom"), brCode, AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, rctDate, earliestDate))
						continue;

					// Get Last Costing
					// LocalInvCosting invLastCosting =
					// invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
					// rctDate, arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(),
					// arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), brCode,
					// AD_CMPNY);

					/*
					 * double COST = Math.abs(invLastCosting.getCstRemainingValue() /
					 * invLastCosting.getCstRemainingQuantity()); COST = EJBCommon.roundIt(COST,
					 * this.getGlFcPrecisionUnit(AD_CMPNY));
					 * 
					 * double CST_CST_OF_SLS = COST * arInvoiceLineItem.getIliQuantity();
					 * CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS,
					 * this.getGlFcPrecisionUnit(AD_CMPNY));
					 */
					double COST = 0;
					try {
						LocalInvCosting invLastCosting = invCostingHome
								.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
										rctDate, arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(),
										arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), brCode,
										AD_CMPNY);

						COST = Math
								.abs(invLastCosting.getCstRemainingValue() / invLastCosting.getCstRemainingQuantity());

					} catch (Exception e) {
						COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
					}

					double CST_CST_OF_SLS = COST * arInvoiceLineItem.getIliQuantity();
					CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, this.getGlFcPrecisionUnit(AD_CMPNY));

					details.setIcOutQuantity(arInvoiceLineItem.getIliQuantity());
					details.setIcOutAmount(CST_CST_OF_SLS);
					details.setIcOutUnitCost(COST);

					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(rctDate);
					details.setIcQuantitySold(arInvoiceLineItem.getIliQuantity());
					// details.setIcCostOfSales(arInvoiceLineItem.getIliAmount());
					details.setIcCostOfSales(CST_CST_OF_SLS);
					details.setIcSourceDocument("RCT");
					details.setIcSourceDocumentNumber(arInvoiceLineItem.getArReceipt().getRctNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber(arInvoiceLineItem.getArInvoice().getInvReferenceNumber());
					details.setIcItemLocation(invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// AP_PURCHASE_ORDER_LINE
				Collection apPurchaseOrderLines = apPurchaseOrderLineHome.findUnpostedPoByLocNameAndAdBranch(locName,
						brCode, AD_CMPNY);

				unpstdIter = apPurchaseOrderLines.iterator();

				while (unpstdIter.hasNext()) {

					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine) unpstdIter.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date poDate = apPurchaseOrderLine.getApPurchaseOrder().getPoDate();
					LocalInvItem invItem = apPurchaseOrderLine.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							apPurchaseOrderLine.getInvItemLocation(), (Date) criteria.get("dateFrom"), brCode,
							AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, poDate, earliestDate))
						continue;

					double ITEM_COST = EJBCommon.roundIt(
							apPurchaseOrderLine.getPlAmount() / apPurchaseOrderLine.getPlQuantity(),
							this.getGlFcPrecisionUnit(AD_CMPNY));
					details.setIcItemCost(ITEM_COST);

					details.setIcInQuantity(apPurchaseOrderLine.getPlQuantity());
					details.setIcInAmount(apPurchaseOrderLine.getPlAmount());
					// details.setRilInUnitCost(apPurchaseOrderLine.getPlUnitCost());
					details.setIcInUnitCost(apPurchaseOrderLine.getPlAmount() / apPurchaseOrderLine.getPlQuantity());

					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(poDate);
					details.setIcQuantityReceived(apPurchaseOrderLine.getPlQuantity());
					details.setIcActualCost(apPurchaseOrderLine.getPlAmount());
					details.setIcSourceDocument("RR");
					details.setIcSourceDocumentNumber(apPurchaseOrderLine.getApPurchaseOrder().getPoDocumentNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber(apPurchaseOrderLine.getApPurchaseOrder().getPoReferenceNumber());
					details.setIcItemLocation(invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}

				// AR_SALES_ORDER_INVOICE_LINE
				Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome
						.findUnpostedSoInvByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = arSalesOrderInvoiceLines.iterator();

				while (unpstdIter.hasNext()) {

					LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine) unpstdIter
							.next();

					InvRepItemCostingDetails details = new InvRepItemCostingDetails();

					Date earliestDate = null;
					Date invDate = arSalesOrderInvoiceLine.getArInvoice().getInvDate();
					LocalInvItem invItem = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation()
							.getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation(),
							(Date) criteria.get("dateFrom"), brCode, AD_CMPNY);
					if (invBeginningCosting != null) {

						details.setIcBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningUnitCost(invBeginningCosting.getCstRemainingValue()
								/ invBeginningCosting.getCstRemainingQuantity());
						details.setIcBeginningValue(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if (!this.filterByOptionalCriteria(criteria, invItem, invDate, earliestDate))
						continue;

					details.setIcOutQuantity(arSalesOrderInvoiceLine.getSilQuantityDelivered());
					details.setIcOutAmount(arSalesOrderInvoiceLine.getSilAmount());
					details.setIcOutUnitCost(Math.abs(arSalesOrderInvoiceLine.getSilAmount()
							/ arSalesOrderInvoiceLine.getSilQuantityDelivered()));

					details.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setIcDate(invDate);
					details.setIcQuantitySold(arSalesOrderInvoiceLine.getSilQuantityDelivered());
					details.setIcCostOfSales(arSalesOrderInvoiceLine.getSilAmount());
					details.setIcSourceDocument("SI");
					details.setIcSourceDocumentNumber(arSalesOrderInvoiceLine.getArInvoice().getInvNumber());
					details.setIcItemCategory(invItem.getIiAdLvCategory());
					details.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
					details.setIcReferenceNumber(arSalesOrderInvoiceLine.getArInvoice().getInvReferenceNumber());
					details.setIcItemLocation(invBeginningCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setIcItemLocation1(LCTN);
					details.setIcItemCategory1(CTGRY);

					list.add(details);
				}
				// Collections.sort(list, InvRepItemCostingDetails.ItemCostingComparator);
			}

			if (list.isEmpty()) {
				throw new GlobalNoRecordFoundException();
			}

			if (INCLD_UNPSTD || SHW_CMMTTD_QNTTY)
				Collections.sort(list, InvRepItemCostingDetails.ItemCostingComparator);

			if ((INCLD_UNPSTD || SHW_CMMTTD_QNTTY) && (Date) criteria.get("dateFrom") != null) {

				Date dateFrom = (Date) criteria.get("dateFrom");

				ArrayList newList = new ArrayList();

				InvRepItemCostingDetails firstDetail = (InvRepItemCostingDetails) list.get(0);

				String currentItem = firstDetail.getIcItemName();
				double beginningQty = firstDetail.getIcBeginningQuantity();
				double beginningValue = firstDetail.getIcBeginningValue();

				Iterator listIter = list.iterator();

				while (listIter.hasNext()) {

					InvRepItemCostingDetails details = (InvRepItemCostingDetails) listIter.next();

					if (!currentItem.equals(details.getIcItemName())) {
						currentItem = details.getIcItemName();
						beginningQty = details.getIcBeginningQuantity();
						beginningValue = details.getIcBeginningValue();
					}

					if (details.getIcDate().getTime() < dateFrom.getTime()) {

						beginningQty += (details.getIcAdjustQuantity() + details.getIcAssemblyQuantity()
								+ details.getIcQuantityReceived() - details.getIcQuantitySold());
						beginningValue += (details.getIcAdjustCost() + details.getIcAssemblyCost()
								+ details.getIcActualCost() - details.getIcCostOfSales());

					} else {

						details.setIcBeginningQuantity(beginningQty);
						details.setIcBeginningValue(beginningValue);

						newList.add(details);
					}
				}

				System.out.println("list size : " + list.size());
				System.out.println("newList size : " + newList.size());

				if (SHW_ZRS) {

					Collection invAdjustmentLines = invAdjustmentLineHome
							.findPostedAdjByLessOrEqualDateAndAdjAdBranch(brCode, AD_CMPNY);

					Iterator pstdIter = invAdjustmentLines.iterator();

					while (pstdIter.hasNext()) {
						LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) pstdIter.next();

						InvRepItemCostingDetails details = new InvRepItemCostingDetails();

						LocalInvItem invItem = invAdjustmentLine.getInvItemLocation().getInvItem();
						String location = invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName();
						int locationCode = invAdjustmentLine.getInvItemLocation().getInvLocation().getLocCode();

						Collection invStockIssuanceLines = invStockIssuanceLineHome
								.findByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection invAssemblyTransferLines = invAssemblyTransferLineHome
								.findByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssemblyLineHome
								.findByIiCodeLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection invStockTransferLines = invStockTransferLineHome
								.findByIiCodeAndLocCodeAndAdBranch(invItem.getIiCode(), locationCode, brCode, AD_CMPNY);
						Collection invBranchStockTransferLines = invBranchStockTransferLineHome
								.findByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection apVoucherLineItems = apVoucherLineItemHome
								.findVouByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection arInvoiceLineItems = arInvoiceLineItemHome
								.findInvcByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection apPurchaseOrderLines = apPurchaseOrderLineHome
								.findPoByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome
								.findSoInvByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode,
										AD_CMPNY);

						if (invItem.getIiName().equals("SIC-0002")) {
							System.out.println("invStockIssuanceLines " + invStockIssuanceLines.size());
							System.out.println("invAssemblyTransferLines " + invAssemblyTransferLines.size());
							System.out.println("invBuildUnbuildAssemblyLines" + invBuildUnbuildAssemblyLines.size());
							System.out.println("invStockTransferLines " + invStockTransferLines.size());
							System.out.println("invBranchStockTransferLines " + invBranchStockTransferLines.size());
							System.out.println("apVoucherLineItems " + apVoucherLineItems.size());
							System.out.println("apPurchaseOrderLines " + apPurchaseOrderLines.size());
							System.out.println("arSalesOrderInvoiceLines " + arSalesOrderInvoiceLines.size());
							Iterator poline = apPurchaseOrderLines.iterator();
							while (poline.hasNext()) {
								LocalApPurchaseOrderLine appoline = (LocalApPurchaseOrderLine) poline.next();
								System.out.println("docNumber: " + appoline.getApPurchaseOrder().getPoDocumentNumber());
							}
						}

						if (invStockIssuanceLines.isEmpty() && invAssemblyTransferLines.isEmpty()
								&& invBuildUnbuildAssemblyLines.isEmpty() && invStockTransferLines.isEmpty()
								&& invBranchStockTransferLines.isEmpty() && apVoucherLineItems.isEmpty()
								&& arInvoiceLineItems.isEmpty() && apPurchaseOrderLines.isEmpty()
								&& arSalesOrderInvoiceLines.isEmpty()) {
							Collection invCosting = invCostingHome.findByIiNameAndLocNameAndAdjLineCode(
									(Date) criteria.get("dateFrom"),
									invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
									invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(),
									invAdjustmentLine.getAlCode(), brCode, AD_CMPNY);
							Iterator ii = invCosting.iterator();

							while (ii.hasNext()) {

								LocalInvCosting invCosting1 = (LocalInvCosting) ii.next();
								InvRepItemCostingDetails mdetails = new InvRepItemCostingDetails();

								mdetails.setIcBeginningQuantity(invCosting1.getCstRemainingQuantity());
								mdetails.setIcBeginningUnitCost(
										invCosting1.getCstRemainingValue() / invCosting1.getCstRemainingQuantity());
								mdetails.setIcBeginningValue(invCosting1.getCstRemainingValue());
								mdetails.setIcDate(invAdjustmentLine.getInvAdjustment().getAdjDate());
								mdetails.setIcSourceDocumentNumber(
										invAdjustmentLine.getInvAdjustment().getAdjDocumentNumber());
								mdetails.setIcSourceDocument("INV");
								mdetails.setIcReferenceNumber(
										invAdjustmentLine.getInvAdjustment().getAdjReferenceNumber());
								mdetails.setIcItemCategory(invItem.getIiAdLvCategory());
								mdetails.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
								mdetails.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
								mdetails.setIcItemLocation(
										invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName());
								mdetails.setIcItemLocation1(LCTN);
								mdetails.setIcItemCategory1(CTGRY);

								newList.add(mdetails);
							}
						}
					}
					// Collections.sort(newList, InvRepItemCostingDetails.ItemCostingComparator);

				}
				// Collections.sort(list, InvRepItemCostingDetails.ItemCostingComparator);
				return newList;
			} else {
				if (SHW_ZRS) {

					Collection invAdjustmentLines = invAdjustmentLineHome
							.findPostedAdjByLessOrEqualDateAndAdjAdBranch(brCode, AD_CMPNY);

					Iterator pstdIter = invAdjustmentLines.iterator();

					while (pstdIter.hasNext()) {
						LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) pstdIter.next();

						InvRepItemCostingDetails details = new InvRepItemCostingDetails();

						LocalInvItem invItem = invAdjustmentLine.getInvItemLocation().getInvItem();
						String location = invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName();
						int locationCode = invAdjustmentLine.getInvItemLocation().getInvLocation().getLocCode();

						Collection invStockIssuanceLines = invStockIssuanceLineHome
								.findByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection invAssemblyTransferLines = invAssemblyTransferLineHome
								.findByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssemblyLineHome
								.findByIiCodeLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection invStockTransferLines = invStockTransferLineHome
								.findByIiCodeAndLocCodeAndAdBranch(invItem.getIiCode(), locationCode, brCode, AD_CMPNY);
						Collection invBranchStockTransferLines = invBranchStockTransferLineHome
								.findByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection apVoucherLineItems = apVoucherLineItemHome
								.findVouByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection arInvoiceLineItems = arInvoiceLineItemHome
								.findInvcByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection apPurchaseOrderLines = apPurchaseOrderLineHome
								.findPoByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode, AD_CMPNY);
						Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome
								.findSoInvByIiCodeAndLocNameAndAdBranch(invItem.getIiCode(), location, brCode,
										AD_CMPNY);

						if (invItem.getIiName().equals("SIC-0002")) {
							System.out.println("invStockIssuanceLines " + invStockIssuanceLines.size());
							System.out.println("invAssemblyTransferLines " + invAssemblyTransferLines.size());
							System.out.println("invBuildUnbuildAssemblyLines" + invBuildUnbuildAssemblyLines.size());
							System.out.println("invStockTransferLines " + invStockTransferLines.size());
							System.out.println("invBranchStockTransferLines " + invBranchStockTransferLines.size());
							System.out.println("apVoucherLineItems " + apVoucherLineItems.size());
							System.out.println("apPurchaseOrderLines " + apPurchaseOrderLines.size());
							System.out.println("arSalesOrderInvoiceLines " + arSalesOrderInvoiceLines.size());
							Iterator poline = apPurchaseOrderLines.iterator();
							while (poline.hasNext()) {
								LocalApPurchaseOrderLine appoline = (LocalApPurchaseOrderLine) poline.next();
								System.out.println("docNumber: " + appoline.getApPurchaseOrder().getPoDocumentNumber());
							}
						}

						if (invStockIssuanceLines.isEmpty() && invAssemblyTransferLines.isEmpty()
								&& invBuildUnbuildAssemblyLines.isEmpty() && invStockTransferLines.isEmpty()
								&& invBranchStockTransferLines.isEmpty() && apVoucherLineItems.isEmpty()
								&& arInvoiceLineItems.isEmpty() && apPurchaseOrderLines.isEmpty()
								&& arSalesOrderInvoiceLines.isEmpty()) {
							Collection invCosting = invCostingHome.findByIiNameAndLocNameAndAdjLineCode(
									(Date) criteria.get("dateFrom"),
									invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
									invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(),
									invAdjustmentLine.getAlCode(), brCode, AD_CMPNY);
							Iterator ii = invCosting.iterator();

							while (ii.hasNext()) {

								LocalInvCosting invCosting1 = (LocalInvCosting) ii.next();
								InvRepItemCostingDetails mdetails = new InvRepItemCostingDetails();

								mdetails.setIcBeginningQuantity(invCosting1.getCstRemainingQuantity());
								mdetails.setIcBeginningUnitCost(
										invCosting1.getCstRemainingValue() / invCosting1.getCstRemainingQuantity());
								mdetails.setIcBeginningValue(invCosting1.getCstRemainingValue());
								mdetails.setIcDate(invAdjustmentLine.getInvAdjustment().getAdjDate());
								mdetails.setIcSourceDocumentNumber(
										invAdjustmentLine.getInvAdjustment().getAdjDocumentNumber());
								mdetails.setIcSourceDocument("INV");
								mdetails.setIcReferenceNumber(
										invAdjustmentLine.getInvAdjustment().getAdjReferenceNumber());
								mdetails.setIcItemCategory(invItem.getIiAdLvCategory());
								mdetails.setIcItemUnitOfMeasure(invItem.getInvUnitOfMeasure().getUomName());
								mdetails.setIcItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
								mdetails.setIcItemLocation(
										invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName());
								mdetails.setIcItemLocation1(LCTN);
								mdetails.setIcItemCategory1(CTGRY);

								list.add(mdetails);
							}
						}
					}
					// Collections.sort(list, InvRepItemCostingDetails.ItemCostingComparator);

				}
				// Collections.sort(list, InvRepItemCostingDetails.ItemCostingComparator);
				return list;
			}

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("InvRepItemCostingControllerBean getGlFcPrecisionUnit");

		LocalAdCompanyHome adCompanyHome = null;

		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,
					LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public void executeInvFixItemCosting(HashMap criteria, ArrayList branchList, Integer AD_CMPNY)
			throws GlobalNoRecordFoundException {

		Debug.print("InvRepItemCostingControllerBean executeInvFixItemCosting");

		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
		LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalArSalesOrderLineHome arSalesOrderLineHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalInvStockIssuanceLineHome invStockIssuanceLineHome = null;
		LocalInvAssemblyTransferLineHome invAssemblyTransferLineHome = null;
		LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
		LocalInvStockTransferLineHome invStockTransferLineHome = null;
		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);
			invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
			invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
			invLocationHome = (LocalInvLocationHome) EJBHomeFactory.lookUpLocalHome(LocalInvLocationHome.JNDI_NAME,
					LocalInvLocationHome.class);
			arSalesOrderLineHome = (LocalArSalesOrderLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			invStockIssuanceLineHome = (LocalInvStockIssuanceLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvStockIssuanceLineHome.JNDI_NAME, LocalInvStockIssuanceLineHome.class);
			invAssemblyTransferLineHome = (LocalInvAssemblyTransferLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvAssemblyTransferLineHome.JNDI_NAME, LocalInvAssemblyTransferLineHome.class);
			invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
			invStockTransferLineHome = (LocalInvStockTransferLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class);
			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
			apVoucherLineItemHome = (LocalApVoucherLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome) EJBHomeFactory
					.lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome) EJBHomeFactory
					.lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome) EJBHomeFactory.lookUpLocalHome(
					LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Integer brCode = null;
			Integer locCode = null;
			String locName = "";

			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(cst) FROM InvCosting  cst WHERE (");

			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = 0;

			Object obj[] = null;

			if (branchList.isEmpty())
				throw new GlobalNoRecordFoundException();

			Iterator brIter = branchList.iterator();

			AdBranchDetails bdetails = (AdBranchDetails) brIter.next();
			jbossQl.append("cst.cstAdBranch=" + bdetails.getBrCode());

			while (brIter.hasNext()) {

				bdetails = (AdBranchDetails) brIter.next();

				jbossQl.append(" OR cst.cstAdBranch=" + bdetails.getBrCode());

			}

			jbossQl.append(") ");

			firstArgument = false;

			// Allocate the size of the object parameter

			if (criteria.containsKey("category")) {

				criteriaSize++;

			}

			if (criteria.containsKey("location")) {

				criteriaSize++;
				locName = (String) criteria.get("location");
			}

			if (criteria.containsKey("dateFrom")) {

				criteriaSize++;

			}

			if (criteria.containsKey("dateTo")) {

				criteriaSize++;

			}

			if (criteria.containsKey("itemClass")) {

				criteriaSize++;

			}

			obj = new Object[criteriaSize];

			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append(
						"cst.invItemLocation.invItem.iiName  LIKE '%" + (String) criteria.get("itemName") + "%' ");

			}

			if (criteria.containsKey("category")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("cst.invItemLocation.invItem.iiAdLvCategory=?" + (ctr + 1) + " ");
				obj[ctr] = (String) criteria.get("category");
				ctr++;

			}

			if (criteria.containsKey("location")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("cst.invItemLocation.invLocation.locName=?" + (ctr + 1) + " ");
				obj[ctr] = (String) criteria.get("location");
				ctr++;

			}

			if (criteria.containsKey("dateFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.cstDate>=?" + (ctr + 1) + " ");
				obj[ctr] = (Date) criteria.get("dateFrom");
				ctr++;

			}

			if (criteria.containsKey("dateTo")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.cstDate<=?" + (ctr + 1) + " ");
				obj[ctr] = (Date) criteria.get("dateTo");
				ctr++;

			}

			if (criteria.containsKey("itemClass")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("cst.invItemLocation.invItem.iiClass=?" + (ctr + 1) + " ");
				obj[ctr] = (String) criteria.get("itemClass");
				ctr++;

			}

			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			jbossQl.append("cst.cstAdCompany=" + AD_CMPNY + " ");

			jbossQl.append(
					"ORDER BY cst.invItemLocation.invItem.iiName, cst.cstDate, cst.cstDateToLong, cst.cstLineNumber");

			Collection invCostings = invCostingHome.getCstByCriteria(jbossQl.toString(), obj);

			System.out.println("number of lines found is: " + invCostings.size());
			Iterator i = invCostings.iterator();

			Integer CURRENT_II_CODE = 0;
			double UNIT_COST = 0d;

			double CST_RMNNG_QTY = 0d;
			double CST_RMNNG_VL = 0d;

			double CURRENT_RMNNG_QTY = 0d;
			double CURRENT_RMNNG_VL = 0d;

			int CST_LN_NMBR = 0;
			double TXN_QTY = 0;
			double TXN_CST = 0;
			double PREV_UNT_CST = 0;
			double PREV_RMNG_VL = 0;



			Date CST_DATE;

			boolean isFirst = true;

			double PREV_RMNNG_QTY = 0;
			double PREV_RMNNG_VL = 0;

			while (i.hasNext()) {
				LocalInvCosting invCosting = (LocalInvCosting) i.next();

				// GET UNIT COST
				UNIT_COST = EJBCommon.roundIt(invCosting.getInvItemLocation().getInvItem().getIiUnitCost(), (short) 2);

				double QUANTITY_RECEIVED = 0d;
				double ASSEMBLY_QUANTITY = 0d;
				double ADJUST_QUANTITY = 0d;
				double QUANTITY_SOLD = 0d;

				double ITEM_COST = 0d;
				double ASSEMBLY_COST = 0d;
				double ADJUST_COST = 0d;
				double COST_OF_SALES = 0;

				double INITIAL_QTY = 0;
				double INITIAL_VL = 0;
				double RMNNG_QTY = 0;
				double RMNNG_VL = 0;

				// AP
				QUANTITY_RECEIVED = EJBCommon.roundIt(invCosting.getCstQuantityReceived(), (short) 2);
				ITEM_COST = invCosting.getCstItemCost();

				// INV ASSEMBLY
				ASSEMBLY_QUANTITY = EJBCommon.roundIt(invCosting.getCstAssemblyQuantity(), (short) 2);
				ASSEMBLY_COST = invCosting.getCstAssemblyCost();

				// INV ADJUSTMENT
				ADJUST_QUANTITY = EJBCommon.roundIt(invCosting.getCstAdjustQuantity(), (short) 2);
				ADJUST_COST = invCosting.getCstAdjustCost();

				// AR
				QUANTITY_SOLD = EJBCommon.roundIt(invCosting.getCstQuantitySold(), (short) 2);
				COST_OF_SALES = invCosting.getCstCostOfSales();

				// GET INITIAL QTY
				INITIAL_QTY = EJBCommon.roundIt(QUANTITY_RECEIVED + ASSEMBLY_QUANTITY + ADJUST_QUANTITY - QUANTITY_SOLD,
						(short) 2);
				// GET INITIAL VL
				INITIAL_VL = EJBCommon.roundIt(ITEM_COST + ASSEMBLY_COST + ADJUST_COST - COST_OF_SALES, (short) 2);

				if (isFirst) {
					RMNNG_QTY = INITIAL_QTY;
					RMNNG_VL = INITIAL_VL;
					PREV_RMNNG_QTY = RMNNG_QTY;
					PREV_RMNNG_VL = RMNNG_VL;
				} else {

					RMNNG_QTY = EJBCommon.roundIt(PREV_RMNNG_QTY + INITIAL_QTY, (short) 2);
					RMNNG_VL = EJBCommon.roundIt(PREV_RMNNG_VL + INITIAL_VL, (short) 2);
					PREV_RMNNG_QTY = RMNNG_QTY;
					PREV_RMNNG_VL = RMNNG_VL;
				}

				invCosting.setCstRemainingQuantity(RMNNG_QTY);
				invCosting.setCstRemainingValue(RMNNG_VL);
				invCosting.setCstCostOfSales(COST_OF_SALES);
				invCosting.setCstItemCost(ITEM_COST);
				invCosting.setCstAdjustCost(ADJUST_COST);
				isFirst = false;

				/*
				 * 
				 * LocalInvCosting invCosting = (LocalInvCosting) i.next();
				 * 
				 * UNIT_COST =
				 * EJBCommon.roundIt(invCosting.getInvItemLocation().getInvItem().getIiUnitCost(
				 * ),(short)2);
				 * 
				 * QUANTITY_RECEIVED =
				 * EJBCommon.roundIt(invCosting.getCstQuantityReceived(),(short)2);
				 * ASSEMBLY_QUANTITY =
				 * EJBCommon.roundIt(invCosting.getCstAssemblyQuantity(),(short)2);
				 * ADJUST_QUANTITY =
				 * EJBCommon.roundIt(invCosting.getCstAdjustQuantity(),(short)2); QUANTITY_SOLD
				 * = EJBCommon.roundIt(invCosting.getCstQuantitySold(),(short)2);
				 * 
				 * ITEM_COST = invCosting.getCstItemCost(); ASSEMBLY_COST =
				 * invCosting.getCstAssemblyCost(); ADJUST_COST = invCosting.getCstAdjustCost();
				 * COST_OF_SALES = invCosting.getCstCostOfSales();
				 * 
				 * //cleaning values of all
				 * 
				 * if(ASSEMBLY_QUANTITY==0){ ASSEMBLY_COST = 0; }else{ if(ASSEMBLY_COST==0){
				 * ASSEMBLY_COST = EJBCommon.roundIt((CST_RMNNG_VL / CST_RMNNG_QTY) *
				 * ASSEMBLY_QUANTITY,(short)2); } }
				 * 
				 * if(QUANTITY_RECEIVED==0){ ITEM_COST = 0; }else{ if(ITEM_COST==0){ ITEM_COST =
				 * EJBCommon.roundIt((CST_RMNNG_VL / CST_RMNNG_QTY) *
				 * QUANTITY_RECEIVED,(short)2); } }
				 * 
				 * if(ADJUST_QUANTITY==0){ ADJUST_COST = 0; }else{ if(ADJUST_COST==0){
				 * ADJUST_COST = EJBCommon.roundIt((CST_RMNNG_VL / CST_RMNNG_QTY) *
				 * ADJUST_QUANTITY,(short)2); } }
				 * 
				 * if(QUANTITY_SOLD==0){ COST_OF_SALES = 0; }else{ if(COST_OF_SALES==0){
				 * COST_OF_SALES= EJBCommon.roundIt((CST_RMNNG_VL / CST_RMNNG_QTY) *
				 * QUANTITY_SOLD,(short)2); } }
				 * 
				 * //InvRepItemCostingDetails details = new InvRepItemCostingDetails();
				 * 
				 * if(CURRENT_II_CODE == 0){ System.out.println("FIRST TIME"); CURRENT_II_CODE =
				 * invCosting.getInvItemLocation().getIlCode();
				 * 
				 * }else{
				 * if(!CURRENT_II_CODE.equals(invCosting.getInvItemLocation().getIlCode())){
				 * System.out.println("NEXT ITEM"); CST_RMNNG_QTY = 0d; CST_RMNNG_VL = 0d;
				 * CURRENT_RMNNG_QTY = 0d; CURRENT_RMNNG_VL = 0d; TXN_QTY=0; TXN_CST=0; isFirst
				 * = true; CURRENT_II_CODE = invCosting.getInvItemLocation().getIlCode();
				 * 
				 * } }
				 * 
				 * 
				 * 
				 * if(isFirst){
				 * 
				 * COST_OF_SALES = 0d;
				 * 
				 * System.out.println("COST OF SALES IS : " + COST_OF_SALES);
				 * 
				 * CST_LN_NMBR= 1; //CST_QTY_RCVD+CST_ASSMBLY_QTY+CST_ADJST_QTY-CST_QTY_SLD
				 * CURRENT_RMNNG_QTY= QUANTITY_RECEIVED + ASSEMBLY_QUANTITY + ADJUST_QUANTITY -
				 * QUANTITY_SOLD;
				 * 
				 * //CST_ITM_CST+CST_ASSMBLY_CST+CST_ADJST_CST-CST_CST_OF_SLS
				 * 
				 * //invCosting.getArInvoiceLineItem().get CURRENT_RMNNG_VL= ITEM_COST +
				 * ASSEMBLY_COST + ADJUST_COST - COST_OF_SALES; System.out.println("start: " +
				 * CURRENT_RMNNG_QTY + " and " + CURRENT_RMNNG_VL);
				 * 
				 * ITEM_COST= UNIT_COST * QUANTITY_RECEIVED;
				 * 
				 * } else {
				 * 
				 * CURRENT_RMNNG_QTY = CST_RMNNG_QTY + QUANTITY_RECEIVED + ASSEMBLY_QUANTITY +
				 * ADJUST_QUANTITY - QUANTITY_SOLD;
				 * 
				 * //if both are zero if(CST_RMNNG_QTY==0 &&CST_RMNNG_VL == 0){
				 * 
				 * if(QUANTITY_SOLD != 0){ COST_OF_SALES = EJBCommon.roundIt(UNIT_COST *
				 * QUANTITY_SOLD,(short)2);
				 * System.out.println("compute cost of sales (0 by 0):  " + COST_OF_SALES); }
				 * 
				 * if(QUANTITY_RECEIVED!=0){ ITEM_COST = UNIT_COST * QUANTITY_RECEIVED;
				 * System.out.println("compute item cost (0 by 0): " + ITEM_COST);
				 * 
				 * }
				 * 
				 * if(ADJUST_QUANTITY!=0){ ADJUST_COST = UNIT_COST * ADJUST_QUANTITY;
				 * System.out.println("compute adjust cost (0 by 0): " + ADJUST_COST); }
				 * 
				 * if(ASSEMBLY_QUANTITY!=0){ ASSEMBLY_COST = UNIT_COST * ASSEMBLY_QUANTITY;
				 * System.out.println("compute assembly cost (0 by 0) : " + ASSEMBLY_COST); }
				 * 
				 * 
				 * }else{
				 * 
				 * double UNIT_COST_VALUE = CST_RMNNG_VL / CST_RMNNG_QTY;
				 * 
				 * if((CST_RMNNG_QTY <= 0 && CURRENT_RMNNG_QTY > 0) || (CURRENT_RMNNG_QTY < 0)
				 * || (CST_RMNNG_QTY >=0 && CURRENT_RMNNG_QTY < 0) ){
				 * 
				 * UNIT_COST_VALUE = UNIT_COST; }
				 * 
				 * 
				 * if(QUANTITY_SOLD != 0){ COST_OF_SALES = EJBCommon.roundIt(UNIT_COST_VALUE *
				 * QUANTITY_SOLD,(short)2); System.out.println("compute cost of sales: " +
				 * COST_OF_SALES); }
				 * 
				 * if(QUANTITY_RECEIVED!=0){ ITEM_COST = UNIT_COST_VALUE * QUANTITY_RECEIVED;
				 * System.out.println("compute item cost: " + ITEM_COST); }
				 * 
				 * if(ADJUST_QUANTITY!=0){ ADJUST_COST = UNIT_COST_VALUE * ADJUST_QUANTITY;
				 * System.out.println("compute adjust cost: " + ADJUST_COST); }
				 * 
				 * if(ASSEMBLY_QUANTITY!=0){ ASSEMBLY_COST = UNIT_COST_VALUE *
				 * ASSEMBLY_QUANTITY; System.out.println("compute assembly cost (0 by 0) : " +
				 * ASSEMBLY_COST); }
				 * 
				 * 
				 * }
				 * 
				 * if(CURRENT_RMNNG_QTY==0){ CURRENT_RMNNG_VL = 0; }else{ CURRENT_RMNNG_VL =
				 * (CST_RMNNG_VL + ITEM_COST + ASSEMBLY_COST + ADJUST_COST) - (COST_OF_SALES); }
				 * 
				 * System.out.println("COST OF SALES IS: " + COST_OF_SALES);
				 * System.out.println("CST_RMNNG_VL: " + CST_RMNNG_VL + "  subtract to " +
				 * COST_OF_SALES); System.out.println("ITEM_COST IS: " + ITEM_COST);
				 * System.out.println("ASSEMBLY_COST IS: " + ASSEMBLY_COST);
				 * System.out.println("ADJUST_COST IS: " + ADJUST_COST);
				 * 
				 * System.out.println("CURRENT_RMNNG_QTY : " + CURRENT_RMNNG_QTY);
				 * System.out.println("CURRENT_RMNNG_VL : " + CURRENT_RMNNG_VL);
				 * 
				 * }
				 * 
				 * 
				 * CST_RMNNG_QTY = EJBCommon.roundIt(CURRENT_RMNNG_QTY,(short)2); CST_RMNNG_VL =
				 * EJBCommon.roundIt(CURRENT_RMNNG_VL,(short)2); COST_OF_SALES =
				 * EJBCommon.roundIt(COST_OF_SALES,(short)2); ADJUST_COST =
				 * EJBCommon.roundIt(ADJUST_COST,(short)2); ITEM_COST =
				 * EJBCommon.roundIt(ITEM_COST,(short)2);
				 * 
				 * 
				 * 
				 * // invCosting.setCstRemainingQuantity(1);
				 * 
				 * invCosting.setCstRemainingQuantity(CST_RMNNG_QTY);
				 * invCosting.setCstRemainingValue(CST_RMNNG_VL);
				 * invCosting.setCstCostOfSales(COST_OF_SALES);
				 * invCosting.setCstItemCost(ITEM_COST);
				 * invCosting.setCstAdjustCost(ADJUST_COST);
				 * 
				 * 
				 * System.out.println(CST_RMNNG_QTY + " " + CST_RMNNG_VL + " " + COST_OF_SALES);
				 * isFirst = false;
				 * 
				 * //invCosting.getArInvoiceLineItem()
				 * 
				 */

			}

		} catch (GlobalNoRecordFoundException ex) {

			getSessionContext().setRollbackOnly();
			throw ex;

		} catch (Exception ex) {

			getSessionContext().setRollbackOnly();
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/

	public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) throws GlobalNoRecordFoundException {

		Debug.print("InvFindItemLocationControllerBean getAdBrResAll");

		LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
		LocalAdBranchHome adBranchHome = null;

		LocalAdBranchResponsibility adBranchResponsibility = null;
		LocalAdBranch adBranch = null;

		Collection adBranchResponsibilities = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome) EJBHomeFactory
					.lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
			adBranchHome = (LocalAdBranchHome) EJBHomeFactory.lookUpLocalHome(LocalAdBranchHome.JNDI_NAME,
					LocalAdBranchHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		if (adBranchResponsibilities.isEmpty()) {

			throw new GlobalNoRecordFoundException();

		}

		try {

			Iterator i = adBranchResponsibilities.iterator();

			while (i.hasNext()) {

				adBranchResponsibility = (LocalAdBranchResponsibility) i.next();

				adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());

				AdBranchDetails details = new AdBranchDetails();
				details.setBrCode(adBranch.getBrCode());
				details.setBrBranchCode(adBranch.getBrBranchCode());
				details.setBrName(adBranch.getBrName());
				details.setBrHeadQuarter(adBranch.getBrHeadQuarter());

				list.add(details);

			}

		} catch (FinderException ex) {

		} catch (Exception ex) {

			throw new EJBException(ex.getMessage());
		}

		return list;

	}

	private LocalInvCosting getCstIlBeginningBalanceByItemLocationAndDate(LocalInvItemLocation invItemLocation,
			Date date, Integer AD_BRNCH, Integer AD_CMPNY) {

		Debug.print("InvRepItemCostingControllerBean getCstIlBeginningBalanceByItemLocationAndDate");

		LocalInvCostingHome invCostingHome = null;
		LocalInvCosting invCosting = null;

		// Initialize EJB Home

		try {

			invCostingHome = (LocalInvCostingHome) EJBHomeFactory.lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,
					LocalInvCostingHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}
		try {

			GregorianCalendar calendar = new GregorianCalendar();

			if (date != null) {

				calendar.setTime(date);
				calendar.add(GregorianCalendar.DATE, -1);

				Date CST_DT = calendar.getTime();

				try {

					invCosting = invCostingHome
							.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(CST_DT,
									invItemLocation.getInvItem().getIiName(),
									invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

				} catch (FinderException ex) {

				}
			}

			return invCosting;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());

		}
	}

	private boolean filterByOptionalCriteria(HashMap criteria, LocalInvItem invItem, Date txnDate, Date earliestDate) {

		Debug.print("InvRepItemCostingControllerBean filterByOptionalCriteria");

		try {

			String itemName = "";
			String itemClass = "";
			String itemCategory = "";
			Date dateFrom = null;
			Date dateTo = null;

			if (criteria.containsKey("itemName"))
				itemName = (String) criteria.get("itemName");

			if (criteria.containsKey("itemClass"))
				itemClass = (String) criteria.get("itemClass");

			if (criteria.containsKey("category"))
				itemCategory = (String) criteria.get("category");

			if (criteria.containsKey("dateFrom")) {
				if (earliestDate == null)
					dateFrom = (Date) criteria.get("dateFrom");
				else
					dateFrom = earliestDate;
			}

			if (criteria.containsKey("dateTo"))
				dateTo = (Date) criteria.get("dateTo");

			if (!itemName.equals("") && !invItem.getIiName().equals(itemName))
				return false;

			if (!itemClass.equals("") && !invItem.getIiClass().equals(itemClass))
				return false;

			if (!itemCategory.equals("") && !invItem.getIiAdLvCategory().equals(itemCategory))
				return false;

			if (dateFrom != null && dateTo != null)
				if (!(txnDate.getTime() >= dateFrom.getTime() && txnDate.getTime() <= dateTo.getTime()))
					return false;

			if (dateFrom != null && dateTo == null)
				if (!(txnDate.getTime() >= dateFrom.getTime()))
					return false;

			if (dateFrom == null && dateTo != null)
				if (!(txnDate.getTime() <= dateTo.getTime()))
					return false;

			return true;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
		}
	}

	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("InvRepItemCostingControllerBean ejbCreate");

	}

}
