package com.ejb.txn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;




import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArJobOrderInvoiceLine;
import com.ejb.ar.LocalArJobOrderInvoiceLineHome;
import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArJobOrderLineHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.ar.LocalArSalesOrderLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvAssemblyTransferLine;
import com.ejb.inv.LocalInvAssemblyTransferLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLine;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvStockIssuanceLine;
import com.ejb.inv.LocalInvStockIssuanceLineHome;
import com.ejb.inv.LocalInvStockTransferLine;
import com.ejb.inv.LocalInvStockTransferLineHome;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepItemLedgerDetails;

/**
* @ejb:bean name="InvRepItemLedgerControllerEJB"
*           display-name="Used for generation of item ledger report"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/InvRepItemLedgerControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.InvRepItemLedgerController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.InvRepItemLedgerControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
*
*/

public class InvRepItemLedgerControllerBean extends AbstractSessionBean {








	/**
	  * @ejb:interface-method view-type="remote"
	  * @jboss:method-attributes read-only="true"
	  **/
	 public ArrayList getApSplAll(Integer AD_CMPNY) {

	 	Debug.print("InvRepItemLedgerControllerBean getApSplAll");

	 	LocalApSupplierHome apSupplierHome = null;

	 	ArrayList list = new ArrayList();

	 	// Initialize EJB Home

	 	try {

	 		apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
				lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);

	 	} catch (NamingException ex) {

	 		throw new EJBException(ex.getMessage());

	 	}

	 	try {

	 		Collection apSuppliers = apSupplierHome.findEnabledSplAllOrderBySplSupplierCode(AD_CMPNY);

	 		Iterator i = apSuppliers.iterator();

	 		while (i.hasNext()) {

	 			LocalApSupplier apSupplier = (LocalApSupplier)i.next();

	 			list.add(apSupplier.getSplSupplierCode());

	 		}

	 		return list;

	 	} catch (Exception ex) {

	 		Debug.printStackTrace(ex);
	 		throw new EJBException(ex.getMessage());

	 	}

	 }


	/**
	  * @ejb:interface-method view-type="remote"
	  * @jboss:method-attributes read-only="true"
	  **/
	 public ArrayList getArCstAll(Integer AD_CMPNY) {

	 	Debug.print("InvRepItemLedgerControllerBean getArCstAll");

	 	LocalArCustomerHome arCustomerHome = null;
	 	ArrayList list = new ArrayList();

	 	// Initialize EJB Home

	 	try {

	 		arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
				lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);


	 	} catch (NamingException ex) {

	 		throw new EJBException(ex.getMessage());

	 	}

	 	try {
	 		Collection arCustomers = arCustomerHome.findEnabledCstAllOrderByCstName(AD_CMPNY);

	 		Iterator i = arCustomers.iterator();

	 		while (i.hasNext()) {

	 			LocalArCustomer arCustomer = (LocalArCustomer)i.next();

	 			list.add(arCustomer.getCstCustomerCode());

	 		}

	 		return list;

	 	} catch (Exception ex) {

	 		Debug.printStackTrace(ex);
	 		throw new EJBException(ex.getMessage());

	 	}

	 }


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {

		Debug.print("InvRepItemLedgerControllerBean getAdLvReportTypeAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV REPORT TYPE - ITEM LEDGER", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}


	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {

		Debug.print("InvRepItemLedgerControllerBean getAdLvInvItemCategoryAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {

		Debug.print("InvRepItemLedgerControllerBean getInvLocAll");

		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			invLocations = invLocationHome.findLocAll(AD_CMPNY);

			if (invLocations.isEmpty()) {

				return null;

			}

			Iterator i = invLocations.iterator();

			while (i.hasNext()) {

				LocalInvLocation invLocation = (LocalInvLocation)i.next();
				String details = invLocation.getLocName();

				list.add(details);

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeInvRepItemLedger(HashMap criteria,  boolean SHW_CMMTTD_QNTTY, String referenceNumber,
			boolean INCLD_UNPSTD, ArrayList branchList, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {

		Debug.print("InvRepItemLedgerControllerBean executeInvRepItemLedger");

		LocalInvItemHome invItemHome = null;

		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvLocationHome invLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalArSalesOrderLineHome arSalesOrderLineHome = null;
		LocalArJobOrderLineHome arJobOrderLineHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalInvStockIssuanceLineHome invStockIssuanceLineHome = null;
		LocalInvAssemblyTransferLineHome invAssemblyTransferLineHome = null;
		LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
		LocalInvStockTransferLineHome invStockTransferLineHome = null;
		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
		LocalArJobOrderInvoiceLineHome arJobOrderInvoiceLineHome = null;
		LocalInvTagHome invTagHome = null;

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);

			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			arSalesOrderLineHome = (LocalArSalesOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
			arJobOrderLineHome = (LocalArJobOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			invStockIssuanceLineHome = (LocalInvStockIssuanceLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvStockIssuanceLineHome.JNDI_NAME, LocalInvStockIssuanceLineHome.class);
			invAssemblyTransferLineHome = (LocalInvAssemblyTransferLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAssemblyTransferLineHome.JNDI_NAME, LocalInvAssemblyTransferLineHome.class);
			invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
			invStockTransferLineHome = (LocalInvStockTransferLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class);
			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
			apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			arJobOrderInvoiceLineHome = (LocalArJobOrderInvoiceLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArJobOrderInvoiceLineHome.JNDI_NAME, LocalArJobOrderInvoiceLineHome.class);

			invTagHome = (LocalInvTagHome)EJBHomeFactory.
					lookUpLocalHome(LocalInvTagHome.JNDI_NAME, LocalInvTagHome.class);
		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {


			Integer brCode = null;
			Integer locCode = null;
			String locName = "";

			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(cst) FROM InvCosting cst ");

			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = 0;

			Object obj[] = null;

			if (branchList.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}
			else {

				jbossQl.append(" WHERE cst.cstAdBranch in (");

				boolean firstLoop = true;

				Iterator j = branchList.iterator();

				while(j.hasNext()) {

					if(firstLoop == false) {
						jbossQl.append(", ");
					}
					else {
						firstLoop = false;
					}

					AdBranchDetails mdetails = (AdBranchDetails) j.next();

					jbossQl.append(mdetails.getBrCode());

					brCode = mdetails.getBrCode();
				}

				jbossQl.append(") ");

				firstArgument = false;

			}

			// Allocate the size of the object parameter

			if (criteria.containsKey("category")) {

				criteriaSize++;
			}

			if (criteria.containsKey("customerCode")) {

				criteriaSize++;
			}

			if (criteria.containsKey("supplierCode")) {

				criteriaSize++;
			}

			if (criteria.containsKey("location")) {

				criteriaSize++;
				locName = (String)criteria.get("location");
			}

			if (criteria.containsKey("dateFrom")) {

				criteriaSize++;
			}

			if (criteria.containsKey("dateTo")) {

				criteriaSize++;
			}

			if (criteria.containsKey("itemClass")) {

				criteriaSize++;
			}

			obj = new Object[criteriaSize];


			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.invItemLocation.invItem.iiName  >= '" + (String)criteria.get("itemName") + "' ");
			}

			if (criteria.containsKey("itemNameTo")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.invItemLocation.invItem.iiName  <= '" + (String)criteria.get("itemNameTo") + "' ");
			}


			if (criteria.containsKey("customerCode")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.invItemLocation.invItem.arCustomer.cstCustomerCode =?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("customerCode");
				ctr++;
			}


			if (criteria.containsKey("supplierCode")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.invItemLocation.invItem.apSupplier.splSupplierCode =?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("supplierCode");
				ctr++;
			}

			if (criteria.containsKey("category")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("cst.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;

			}
			String loc =(String)criteria.get("location");
			if (criteria.containsKey("location")) {
				if(loc.toString().equalsIgnoreCase("ALL"))
				{

					obj[ctr] = (String)criteria.get("location");
					ctr++;
				}
				else{

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("cst.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;

				}
			}

			if (criteria.containsKey("dateFrom")) {

				if (!firstArgument) {
					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.cstDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;



			}

			if (criteria.containsKey("dateTo")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.cstDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;

			}

			if (criteria.containsKey("itemClass")) {

				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}

				jbossQl.append("cst.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemClass");
				ctr++;

			}

			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			jbossQl.append("cst.cstAdCompany=" + AD_CMPNY + " ");

			if(loc.toString().equalsIgnoreCase("ALL")) {
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("cst.invItemLocation.invLocation.locName <> 'IN TRANSIT' ");
			}

			jbossQl.append("ORDER BY cst.invItemLocation.invItem.iiName, cst.cstDate, cst.cstDateToLong, cst.cstLineNumber");
			System.out.println("jbossQl="+jbossQl);
			Collection invCostings = invCostingHome.getCstByCriteria(jbossQl.toString(), obj);

			Iterator i = invCostings.iterator();

			while (i.hasNext()) {

				LocalInvCosting invCosting = (LocalInvCosting) i.next();

				InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

				//beginning

				LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
						invCosting.getInvItemLocation(), (Date)criteria.get("dateFrom"),
						invCosting.getCstAdBranch(), AD_CMPNY);

				if(invBeginningCosting != null) {

					details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
					details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
					details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());

				} else {

					details.setRilBeginningQuantity(0);
					details.setRilBeginningUnitCost(0);
					details.setRilBeginningAmount(0);
				}

				details.setRilItemName(invCosting.getInvItemLocation().getInvItem().getIiName() + "-" + invCosting.getInvItemLocation().getInvItem().getIiDescription());
				details.setRilItemDesc(invCosting.getInvItemLocation().getInvItem().getIiDescription());
				details.setRilUnit(invCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
				details.setRilDate(invCosting.getCstDate());
				details.setRilItemCategory(invCosting.getInvItemLocation().getInvItem().getIiAdLvCategory());
				details.setRilReferenceNumber1(referenceNumber.toString());

				if (invCosting.getInvAdjustmentLine() != null) {

					details.setRilAccount(invCosting.getInvAdjustmentLine().getInvAdjustment().getGlChartOfAccount().getCoaAccountDescription());
				}

				if(invCosting.getInvAdjustmentLine() != null) {

					details.setRilDocumentNumber(invCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber());
					details.setRilReferenceNumber(invCosting.getInvAdjustmentLine().getInvAdjustment().getAdjReferenceNumber());
					details.setRilQcNumber(invCosting.getCstQCNumber());
					System.out.println("LOCATION="+invCosting.getInvItemLocation().getInvLocation().getLocName());
					details.setRilLocationName(invCosting.getInvAdjustmentLine().getInvItemLocation().getInvLocation().getLocName());
					try {
						details.setRilSupplierName(invCosting.getInvAdjustmentLine().getInvAdjustment().getApSupplier().getSplName());

					} catch (Exception ex){
						details.setRilSupplierName("");

					}
					details.setRilSource("INV");

				} else if(invCosting.getInvStockIssuanceLine() != null) {

					details.setRilDocumentNumber(invCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber());
					details.setRilReferenceNumber(invCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiReferenceNumber());
					details.setRilSource("INV");

				} else if(invCosting.getInvAssemblyTransferLine() != null) {

					details.setRilDocumentNumber(invCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber());
					details.setRilReferenceNumber(invCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrReferenceNumber());
					details.setRilSource("INV");

				} else if(invCosting.getInvBuildUnbuildAssemblyLine() != null) {

					details.setRilDocumentNumber(invCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber());
					details.setRilReferenceNumber(invCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaReferenceNumber());
					details.setRilSource("INV");

				} else if(invCosting.getInvStockTransferLine() != null) {

					details.setRilDocumentNumber(invCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber());
					details.setRilReferenceNumber(invCosting.getInvStockTransferLine().getInvStockTransfer().getStReferenceNumber());

					LocalInvLocation invLocation = invLocationHome.findByPrimaryKey(invCosting.getInvStockTransferLine().getStlLocationTo());

					details.setRilLocationName(invLocation.getLocName());
					details.setRilQcNumber(invCosting.getCstQCNumber());
					details.setRilSource("INV");

				} else if(invCosting.getInvBranchStockTransferLine() != null) {
					System.out.println("test in: "+invCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber());
					details.setRilDocumentNumber(invCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber());
					System.out.println("test done");
					details.setRilReferenceNumber(invCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber());
					details.setRilSource("INV");

				} else if(invCosting.getApVoucherLineItem() != null) {

					if (invCosting.getApVoucherLineItem().getApVoucher() != null) {

						details.setRilDocumentNumber(invCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber());
						details.setRilReferenceNumber(invCosting.getApVoucherLineItem().getApVoucher().getVouReferenceNumber());
						details.setRilSource("AP");

					} else if (invCosting.getApVoucherLineItem().getApCheck() != null) {

						details.setRilDocumentNumber(invCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber());
						details.setRilReferenceNumber(invCosting.getApVoucherLineItem().getApCheck().getChkNumber());
						details.setRilSource("AP");

					}

				} else if(invCosting.getArInvoiceLineItem() != null) {

					if(invCosting.getArInvoiceLineItem().getArInvoice() != null) {

						details.setRilDocumentNumber(invCosting.getArInvoiceLineItem().getArInvoice().getInvNumber());
						details.setRilReferenceNumber(invCosting.getArInvoiceLineItem().getArInvoice().getInvReferenceNumber());
						details.setRilSource("AR");

					} else if (invCosting.getArInvoiceLineItem().getArReceipt() != null) {

						details.setRilDocumentNumber(invCosting.getArInvoiceLineItem().getArReceipt().getRctNumber());
						details.setRilReferenceNumber(invCosting.getArInvoiceLineItem().getArReceipt().getRctReferenceNumber());
						details.setRilQcNumber(invCosting.getCstQCNumber());
						details.setRilSource("AR");

					}

				} else if(invCosting.getApPurchaseOrderLine() != null) {

					details.setRilDocumentNumber(invCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber());
					details.setRilReferenceNumber(invCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoReferenceNumber());
					details.setRilSupplierName(invCosting.getApPurchaseOrderLine().getApPurchaseOrder().getApSupplier().getSplSupplierCode());
					details.setRilQcNumber(invCosting.getCstQCNumber());
					details.setRilSource("AP");

				} else if(invCosting.getArSalesOrderInvoiceLine() != null) {

					details.setRilDocumentNumber(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber());
					details.setRilReferenceNumber(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber());
					details.setRilQcNumber(invCosting.getCstQCNumber());
					details.setRilSource("AR");

				} else if(invCosting.getArJobOrderInvoiceLine() != null) {

					details.setRilDocumentNumber(invCosting.getArJobOrderInvoiceLine().getArInvoice().getInvNumber());
					details.setRilReferenceNumber(invCosting.getArJobOrderInvoiceLine().getArInvoice().getInvNumber());
					details.setRilQcNumber(invCosting.getCstQCNumber());
					details.setRilSource("AR");

				}

				details.setRilInQuantity(0d);
				details.setRilInAmount(0d);
				details.setRilOutQuantity(0d);
				details.setRilOutAmount(0d);

				double COST = 0d;

				if (invCosting.getCstQuantityReceived() != 0d) {

					if (invCosting.getCstQuantityReceived() > 0 ) {

						details.setRilInQuantity(invCosting.getCstQuantityReceived());
						details.setRilInAmount(invCosting.getCstItemCost());
						COST = Math.abs(invCosting.getCstItemCost() / invCosting.getCstQuantityReceived());
						details.setRilInUnitCost(COST);

					}else if (invCosting.getCstQuantityReceived() < 0) {

						details.setRilOutQuantity(invCosting.getCstQuantityReceived() * -1);
						details.setRilOutAmount(invCosting.getCstItemCost() * -1);
						COST = Math.abs(invCosting.getCstItemCost() / invCosting.getCstQuantityReceived());
						details.setRilOutUnitCost(COST);
					}

				} else if (invCosting.getCstAdjustQuantity() != 0d ) {

					if (invCosting.getCstAdjustQuantity()  > 0 ) {

						details.setRilInQuantity(invCosting.getCstAdjustQuantity());
						details.setRilInAmount(invCosting.getCstAdjustCost());
						COST = Math.abs(invCosting.getCstAdjustCost() / invCosting.getCstAdjustQuantity());
						details.setRilInUnitCost(COST);

					}else if (invCosting.getCstAdjustQuantity()  < 0 ) {

						details.setRilOutQuantity(invCosting.getCstAdjustQuantity() * -1);
						details.setRilOutAmount(invCosting.getCstAdjustCost() * -1);
						COST = Math.abs(invCosting.getCstAdjustCost() / invCosting.getCstAdjustQuantity());
						details.setRilOutUnitCost(COST);
					}

				} else if (invCosting.getCstAssemblyQuantity() != 0d) {

					if (invCosting.getCstAssemblyQuantity()  > 0) {

						details.setRilInQuantity(invCosting.getCstAssemblyQuantity());
						details.setRilInAmount(invCosting.getCstAssemblyCost());
						COST = Math.abs(invCosting.getCstAssemblyCost() / invCosting.getCstAssemblyQuantity());
						details.setRilInUnitCost(COST);

					}else if (invCosting.getCstAssemblyQuantity()  < 0) {

						details.setRilOutQuantity(invCosting.getCstAssemblyQuantity() * -1);
						details.setRilOutAmount(invCosting.getCstAssemblyCost() * -1);
						COST = Math.abs(invCosting.getCstAssemblyCost() / invCosting.getCstAssemblyQuantity());
						details.setRilOutUnitCost(COST);

					}

				} else if (invCosting.getCstQuantitySold() != 0d) {

					if (invCosting.getCstQuantitySold()  > 0) {

						details.setRilOutQuantity(invCosting.getCstQuantitySold());
						details.setRilOutAmount(invCosting.getCstCostOfSales());
						COST = Math.abs(invCosting.getCstCostOfSales() / invCosting.getCstQuantitySold());
						details.setRilOutUnitCost(COST);

					} else if (invCosting.getCstQuantitySold()  < 0) {

						details.setRilInQuantity(invCosting.getCstQuantitySold() * -1);
						details.setRilInAmount(invCosting.getCstCostOfSales() * -1);
						COST = Math.abs(invCosting.getCstCostOfSales() / invCosting.getCstQuantitySold());
						details.setRilInUnitCost(COST);

					}

				}
				//d2
				LocalInvCosting invBeginningCosting1 = this.getCstIlBeginningBalanceByItemLocationAndDate(
						invCosting.getInvItemLocation(), (Date)criteria.get("dateFrom"),
						invCosting.getCstAdBranch(), AD_CMPNY);
				if (invBeginningCosting1 != null) {
					details.setRilBeginningQuantity(invBeginningCosting1.getCstRemainingQuantity());
					details.setRilBeginningUnitCost(invBeginningCosting1.getCstRemainingValue() / invBeginningCosting1.getCstRemainingQuantity());
					details.setRilBeginningAmount(invBeginningCosting1.getCstRemainingValue());

				} else {
					details.setRilRemainingQuantity(0);
					details.setRilRemainingUnitCost(0);
					details.setRilRemainingAmount(0);
				}

				if(invCosting.getCstRemainingQuantity() != 0 && invCosting.getCstRemainingValue() != 0) {

					details.setRilRemainingQuantity(invCosting.getCstRemainingQuantity());
					details.setRilRemainingUnitCost(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
					details.setRilRemainingAmount(invCosting.getCstRemainingValue());
					System.out.println(">>" + invCosting.getCstRemainingQuantity() + " " + invCosting.getCstRemainingValue());
				}

				list.add(details);
			}


			if(SHW_CMMTTD_QNTTY) {

				System.out.println("SHW_CMMTTD_QNTTY="+SHW_CMMTTD_QNTTY);

				Collection arSalesOrderLines = arSalesOrderLineHome.findOpenSolAll(AD_CMPNY);

				Iterator solIter = arSalesOrderLines.iterator();

				while(solIter.hasNext()) {

					LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine)solIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					if(arSalesOrderLine.getArSalesOrderInvoiceLines().size() == 0 &&
							arSalesOrderLine.getArSalesOrder().getSoAdBranch().equals(brCode) &&
							arSalesOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(locName)) {

						Date earliestDate = null;
						Date soDate = arSalesOrderLine.getArSalesOrder().getSoDate();
						LocalInvItem invItem = arSalesOrderLine.getInvItemLocation().getInvItem();

						LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
								arSalesOrderLine.getInvItemLocation(), (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
						if(invBeginningCosting != null) {

							details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
							details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
							details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
							earliestDate = invBeginningCosting.getCstDate();
						}

						GregorianCalendar gcDateFrom = new GregorianCalendar();
						Date dateTo = (Date)criteria.get("dateTo");
						if(dateTo==null) dateTo = new Date();
						Date soDateFrom = null;
						gcDateFrom.setTime(dateTo!=null ? dateTo : new Date());
						gcDateFrom.add(Calendar.MONTH, -1);

						soDateFrom = gcDateFrom.getTime();

						if(soDate.compareTo(soDateFrom) < 0)
							continue;

						if(dateTo!=null && soDate.compareTo(dateTo) > 0)
							continue;


						if(!this.filterByOptionalCriteria(criteria, invItem, soDate, earliestDate))
							continue;

						details.setRilDate(soDate);
						details.setRilDocumentNumber(arSalesOrderLine.getArSalesOrder().getSoDocumentNumber());
						details.setRilItemName(invItem.getIiName() + " - " +  invItem.getIiDescription());
						details.setRilItemDesc(invItem.getIiDescription());
						details.setRilOutAmount(arSalesOrderLine.getSolAmount());
						details.setRilOutQuantity(arSalesOrderLine.getSolQuantity());
						details.setRilOutUnitCost(arSalesOrderLine.getSolUnitPrice());
						details.setRilReferenceNumber(arSalesOrderLine.getArSalesOrder().getSoReferenceNumber());
						details.setRilSource("AR");
						details.setRilUnit(arSalesOrderLine.getInvUnitOfMeasure().getUomName());
						details.setRilReferenceNumber1(referenceNumber.toString());
						details.setRilItemCategory(invItem.getIiAdLvCategory());
						list.add(details);
					}
				}
				
				
				Collection arJobOrderLines = arJobOrderLineHome.findOpenJolAll(AD_CMPNY);

				Iterator jolIter = arJobOrderLines.iterator();

				while(jolIter.hasNext()) {

					LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine)jolIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					if(arJobOrderLine.getArJobOrderInvoiceLines().size() == 0 &&
							arJobOrderLine.getArJobOrder().getJoAdBranch().equals(brCode) &&
							arJobOrderLine.getInvItemLocation().getInvLocation().getLocName().equals(locName)) {

						Date earliestDate = null;
						Date joDate = arJobOrderLine.getArJobOrder().getJoDate();
						LocalInvItem invItem = arJobOrderLine.getInvItemLocation().getInvItem();

						LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
								arJobOrderLine.getInvItemLocation(), (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
						if(invBeginningCosting != null) {

							details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
							details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
							details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
							earliestDate = invBeginningCosting.getCstDate();
						}

						GregorianCalendar gcDateFrom = new GregorianCalendar();
						Date dateTo = (Date)criteria.get("dateTo");
						if(dateTo==null) dateTo = new Date();
						Date joDateFrom = null;
						gcDateFrom.setTime(dateTo!=null ? dateTo : new Date());
						gcDateFrom.add(Calendar.MONTH, -1);

						joDateFrom = gcDateFrom.getTime();

						if(joDate.compareTo(joDateFrom) < 0)
							continue;

						if(dateTo!=null && joDate.compareTo(dateTo) > 0)
							continue;


						if(!this.filterByOptionalCriteria(criteria, invItem, joDate, earliestDate))
							continue;

						details.setRilDate(joDate);
						details.setRilDocumentNumber(arJobOrderLine.getArJobOrder().getJoDocumentNumber());
						details.setRilItemName(invItem.getIiName() + " - " +  invItem.getIiDescription());
						details.setRilItemDesc(invItem.getIiDescription());
						details.setRilOutAmount(arJobOrderLine.getJolAmount());
						details.setRilOutQuantity(arJobOrderLine.getJolQuantity());
						details.setRilOutUnitCost(arJobOrderLine.getJolUnitPrice());
						details.setRilReferenceNumber(arJobOrderLine.getArJobOrder().getJoReferenceNumber());
						details.setRilSource("AR");
						details.setRilUnit(arJobOrderLine.getInvUnitOfMeasure().getUomName());
						details.setRilReferenceNumber1(referenceNumber.toString());
						details.setRilItemCategory(invItem.getIiAdLvCategory());
						list.add(details);
					}
				}
			}

			if(criteria.containsKey("propertyCode")){
				try{
					Collection invTags = invTagHome.findByTgPropertyCode((String)criteria.get("propertyCode"), AD_CMPNY);
					Iterator u = invTags.iterator();
					while(u.hasNext()){
						LocalInvTag invTag = (LocalInvTag)u.next();
						InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

						try{
							if(invTag.getApPurchaseOrderLine().getPlCode() != null &&
									invTag.getApPurchaseOrderLine().getInvItemLocation().getInvLocation().getLocName().equals(locName)){
								details.setRilSource("AP");
								details.setRilDocumentNumber(invTag.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber());
								details.setRilReferenceNumber(invTag.getApPurchaseOrderLine().getApPurchaseOrder().getPoReferenceNumber());
								details.setRilItemName(invTag.getApPurchaseOrderLine().getInvItemLocation().getInvItem().getIiName() + "-" + invTag.getApPurchaseOrderLine().getInvItemLocation().getInvItem().getIiDescription());
								details.setRilUnit(invTag.getApPurchaseOrderLine().getInvUnitOfMeasure().getUomName());
								details.setRilInAmount(invTag.getApPurchaseOrderLine().getPlAmount());
							}else{
								break;
							}
						}catch(Exception ex){}
						try{
							if(invTag.getInvAdjustmentLine().getAlCode() != null &&
									invTag.getInvAdjustmentLine().getInvItemLocation().getInvLocation().getLocName().equals(locName)){

								details.setRilSource("INV");
								details.setRilDocumentNumber(invTag.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber());
								details.setRilReferenceNumber(invTag.getInvAdjustmentLine().getInvAdjustment().getAdjReferenceNumber());
								details.setRilItemName(invTag.getInvAdjustmentLine().getInvItemLocation().getInvItem().getIiName() + "-" + invTag.getInvAdjustmentLine().getInvItemLocation().getInvItem().getIiDescription());
								details.setRilUnit(invTag.getInvAdjustmentLine().getInvUnitOfMeasure().getUomName());
								if(invTag.getTgType().equals("IN")){
									details.setRilInAmount(invTag.getInvAdjustmentLine().getAlUnitCost());
								}
								if(invTag.getTgType().equals("OUT")){
									details.setRilOutAmount(invTag.getInvAdjustmentLine().getAlUnitCost());
								}
							}else{
								break;
							}
						}catch(Exception ex){}
						details.setRilDate(invTag.getTgTransactionDate());
						details.setRilCustodian(invTag.getAdUser().getUsrDescription());

						if(invTag.getTgType().equals("IN")){
							details.setRilInQuantity(1);
						}
						if(invTag.getTgType().equals("OUT")){
							details.setRilOutQuantity(1);
						}
						list.add(details);
					}
				}
				catch(Exception ex){
					System.out.println("ha?");
					throw new GlobalNoRecordFoundException();
				}
			}

			if(INCLD_UNPSTD) {

				System.out.println("INCLD_UNPSTD="+INCLD_UNPSTD);

				//AP_VOUCHER_LINE_ITEM
				//a) apVoucher
				Collection apVoucherLineItems = apVoucherLineItemHome.findUnpostedVouByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				Iterator unpstdIter = apVoucherLineItems.iterator();

				while(unpstdIter.hasNext()) {

					LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date vouDate = apVoucherLineItem.getApVoucher().getVouDate();
					LocalInvItem invItem = apVoucherLineItem.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							apVoucherLineItem.getInvItemLocation(), (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());

						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, vouDate, earliestDate))
						continue;

					details.setRilInQuantity(apVoucherLineItem.getVliQuantity());
					details.setRilInAmount(apVoucherLineItem.getVliAmount());
					details.setRilInUnitCost(apVoucherLineItem.getVliUnitCost());

					details.setRilDate(vouDate);
					details.setRilDocumentNumber(apVoucherLineItem.getApVoucher().getVouDocumentNumber());
					details.setRilItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setRilReferenceNumber(apVoucherLineItem.getApVoucher().getVouReferenceNumber());
					details.setRilSource("AP");
					details.setRilUnit(apVoucherLineItem.getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}

				//b) apCheck
				apVoucherLineItems = apVoucherLineItemHome.findUnpostedChkByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = apVoucherLineItems.iterator();

				while(unpstdIter.hasNext()) {

					LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date chkDate = apVoucherLineItem.getApCheck().getChkDate();
					LocalInvItem invItem = apVoucherLineItem.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							apVoucherLineItem.getInvItemLocation(), (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, chkDate, earliestDate))
						continue;

					details.setRilInQuantity(apVoucherLineItem.getVliQuantity());
					details.setRilInAmount(apVoucherLineItem.getVliAmount());
					details.setRilInUnitCost(apVoucherLineItem.getVliUnitCost());

					details.setRilDate(chkDate);
					details.setRilDocumentNumber(apVoucherLineItem.getApCheck().getChkDocumentNumber());
					details.setRilItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setRilReferenceNumber(apVoucherLineItem.getApCheck().getChkReferenceNumber());
					details.setRilSource("AP");
					details.setRilUnit(apVoucherLineItem.getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}

				//AP_PURCHASE_ORDER_LINE
				Collection apPurchaseOrderLines = apPurchaseOrderLineHome.findUnpostedPoByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = apPurchaseOrderLines.iterator();

				while(unpstdIter.hasNext()) {

					LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date poDate = apPurchaseOrderLine.getApPurchaseOrder().getPoDate();
					LocalInvItem invItem = apPurchaseOrderLine.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							apPurchaseOrderLine.getInvItemLocation(), (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, poDate, earliestDate))
						continue;

					details.setRilInQuantity(apPurchaseOrderLine.getPlQuantity());
					details.setRilQcNumber(apPurchaseOrderLine.getPlQcNumber());
					details.setRilLocationName(apPurchaseOrderLine.getInvItemLocation().getInvLocation().getLocName());
					details.setRilInAmount(apPurchaseOrderLine.getPlAmount());
					//details.setRilInUnitCost(apPurchaseOrderLine.getPlUnitCost());
					details.setRilInUnitCost(apPurchaseOrderLine.getPlAmount() / apPurchaseOrderLine.getPlQuantity());

					details.setRilDate(poDate);
					details.setRilDocumentNumber(apPurchaseOrderLine.getApPurchaseOrder().getPoDocumentNumber());
					details.setRilItemName(invItem.getIiName() + " - " + invItem.getIiDescription());
					details.setRilItemDesc(invItem.getIiDescription());
					details.setRilQcNumber(apPurchaseOrderLine.getPlQcNumber());
					details.setRilSupplierName(apPurchaseOrderLine.getApPurchaseOrder().getApSupplier().getSplName());
					details.setRilReferenceNumber(apPurchaseOrderLine.getApPurchaseOrder().getPoReferenceNumber());
					details.setRilSource("AP");
					details.setRilUnit(apPurchaseOrderLine.getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}

				//INV_ADJUSTMENT_LINE
				Collection invAdjustmentLines = invAdjustmentLineHome.findUnpostedAdjByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = invAdjustmentLines.iterator();

				while(unpstdIter.hasNext()) {

					LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date adjDate = invAdjustmentLine.getInvAdjustment().getAdjDate();
					LocalInvItem invItem = invAdjustmentLine.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invAdjustmentLine.getInvItemLocation(), (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);

					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, adjDate, earliestDate))
						continue;

					if (invAdjustmentLine.getAlAdjustQuantity()  > 0 ) {

						details.setRilInQuantity(invAdjustmentLine.getAlAdjustQuantity());
						details.setRilInAmount(invAdjustmentLine.getAlUnitCost() * invAdjustmentLine.getAlAdjustQuantity());
						details.setRilInUnitCost(invAdjustmentLine.getAlUnitCost());

					}else if (invAdjustmentLine.getAlAdjustQuantity()  < 0 ) {

						details.setRilOutQuantity(invAdjustmentLine.getAlAdjustQuantity() * -1);
						details.setRilOutAmount(invAdjustmentLine.getAlUnitCost() * invAdjustmentLine.getAlAdjustQuantity() * -1);
						details.setRilOutUnitCost(invAdjustmentLine.getAlUnitCost());
					}

					details.setRilDate(adjDate);
					details.setRilDocumentNumber(invAdjustmentLine.getInvAdjustment().getAdjDocumentNumber());
					details.setRilItemName(invItem.getIiName() + " - " + invItem.getIiDescription());
					details.setRilItemDesc(invItem.getIiDescription());
					details.setRilReferenceNumber(invAdjustmentLine.getInvAdjustment().getAdjReferenceNumber());
					details.setRilSource("INV");
					details.setRilQcNumber(invAdjustmentLine.getAlQcNumber());
					try{
						details.setRilSupplierName(invAdjustmentLine.getInvAdjustment().getApSupplier().getSplName());

					}catch(Exception ex){
						details.setRilSupplierName("");

					}
					details.setRilUnit(invAdjustmentLine.getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());

					list.add(details);
				}


				//INV_STOCK_ISSUANCE_LINE
				Collection invStockIssuanceLines = invStockIssuanceLineHome.findUnpostedSiByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = invStockIssuanceLines.iterator();

				while(unpstdIter.hasNext()) {

					LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date siDate = invStockIssuanceLine.getInvStockIssuance().getSiDate();
					LocalInvItem invItem = invStockIssuanceLine.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invStockIssuanceLine.getInvItemLocation(), (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, siDate, earliestDate))
						continue;

					if (invStockIssuanceLine.getSilIssueQuantity()  > 0 ) {

						details.setRilInQuantity(invStockIssuanceLine.getSilIssueQuantity());
						details.setRilInAmount(invStockIssuanceLine.getSilIssueCost());
						details.setRilInUnitCost(invStockIssuanceLine.getSilUnitCost());

					}else if (invStockIssuanceLine.getSilIssueQuantity()  < 0 ) {

						details.setRilOutQuantity(invStockIssuanceLine.getSilIssueQuantity() * -1);
						details.setRilOutAmount(invStockIssuanceLine.getSilIssueCost() * -1);
						details.setRilOutUnitCost(invStockIssuanceLine.getSilUnitCost());
					}

					details.setRilDate(siDate);
					details.setRilDocumentNumber(invStockIssuanceLine.getInvStockIssuance().getSiDocumentNumber());
					details.setRilItemName(invItem.getIiName() + " - " + invItem.getIiDescription() );
					details.setRilItemDesc(invItem.getIiDescription());
					details.setRilLocationName(invStockIssuanceLine.getInvItemLocation().getInvLocation().getLocName());
					details.setRilReferenceNumber(invStockIssuanceLine.getInvStockIssuance().getSiReferenceNumber());
					details.setRilSource("INV");
					details.setRilUnit(invStockIssuanceLine.getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}

				//INV_ASSEMBLY_TRANSFER_LINE
				Collection invAssemblyTransferLines = invAssemblyTransferLineHome.findUnpostedAtrByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = invAssemblyTransferLines.iterator();

				while(unpstdIter.hasNext()) {

					LocalInvAssemblyTransferLine invAssemblyTransferLine = (LocalInvAssemblyTransferLine)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date atrDate = invAssemblyTransferLine.getInvAssemblyTransfer().getAtrDate();
					LocalInvItem invItem = invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation(),
							(Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, atrDate, earliestDate))
						continue;

					if (invAssemblyTransferLine.getAtlAssembleQuantity() > 0 ) {

						details.setRilInQuantity(invAssemblyTransferLine.getAtlAssembleQuantity());
						details.setRilInAmount(invAssemblyTransferLine.getAtlAssembleCost());
						details.setRilInUnitCost(Math.abs(invAssemblyTransferLine.getAtlAssembleCost() /
								invAssemblyTransferLine.getAtlAssembleQuantity()));

					}else if (invAssemblyTransferLine.getAtlAssembleQuantity() < 0 ) {

						details.setRilOutQuantity(invAssemblyTransferLine.getAtlAssembleQuantity() * -1);
						details.setRilOutAmount(invAssemblyTransferLine.getAtlAssembleCost() * -1);
						details.setRilOutUnitCost(Math.abs(invAssemblyTransferLine.getAtlAssembleCost() /
								invAssemblyTransferLine.getAtlAssembleQuantity()));
					}

					details.setRilDate(atrDate);
					details.setRilDocumentNumber(invAssemblyTransferLine.getInvAssemblyTransfer().getAtrDocumentNumber());
					details.setRilItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setRilReferenceNumber(invAssemblyTransferLine.getInvAssemblyTransfer().getAtrReferenceNumber());
					details.setRilSource("INV");
					details.setRilUnit(invItem.getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}


				//INV_BUILD_UNBUILD_ASSEMBLY_LINE
				Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssemblyLineHome.findUnpostedBuaByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = invBuildUnbuildAssemblyLines.iterator();

				while(unpstdIter.hasNext()) {

					LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)unpstdIter.next();



					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date buaDate = invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDate();
					LocalInvItem invItem = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invBuildUnbuildAssemblyLine.getInvItemLocation(), (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, buaDate, earliestDate))
						continue;

					if (invBuildUnbuildAssemblyLine.getBlBuildQuantity() > 0 ) {

						details.setRilInQuantity(invBuildUnbuildAssemblyLine.getBlBuildQuantity());
						details.setRilInAmount(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost() *
								invBuildUnbuildAssemblyLine.getBlBuildQuantity());
						details.setRilInUnitCost(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost());

					}else if (invBuildUnbuildAssemblyLine.getBlBuildQuantity() < 0 ) {

						details.setRilOutQuantity(invBuildUnbuildAssemblyLine.getBlBuildQuantity() * -1);
						details.setRilOutAmount(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost() *
								invBuildUnbuildAssemblyLine.getBlBuildQuantity() * -1);
						details.setRilOutUnitCost(invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiUnitCost());
					}

					details.setRilDate(buaDate);
					details.setRilDocumentNumber(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDocumentNumber());
					details.setRilItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setRilReferenceNumber(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaReferenceNumber());

					details.setRilSource("INV");
					details.setRilUnit(invBuildUnbuildAssemblyLine.getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);

				}



				//INV BILL OF MATERIALS
				Collection invBuildUnbuildAssemblyLines2 = invBuildUnbuildAssemblyLineHome.findUnpostedBuaByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = invBuildUnbuildAssemblyLines2.iterator();

				while(unpstdIter.hasNext()) {

					LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)unpstdIter.next();
					double specificGravity = invBuildUnbuildAssemblyLine.getBlBuildSpecificGravity();
                    double standardFillSize = invBuildUnbuildAssemblyLine.getBlBuildStandardFillSize();

					Collection invBillofMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();

					Iterator x = invBillofMaterials.iterator();

	    			while(x.hasNext()) {

	    				LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)x.next();

	    				InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

	    				double qtyNeeded = invBillOfMaterial.getBomQuantityNeeded();

						Date earliestDate = null;
						Date buaDate = invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDate();
						LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);
						LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(invItem.getIiName(), invBillOfMaterial.getBomLocName(), AD_CMPNY);


						double convertedBuildQuantity =
								invItem.getIiAdLvCategory().contains("RAWMAT") && !invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiAdLvCategory().contains("Bulk") ?
	                    			invBuildUnbuildAssemblyLine.getBlBuildQuantity() * (qtyNeeded / 100) * (standardFillSize/1000) * specificGravity
	                    		:
	                    			invBuildUnbuildAssemblyLine.getBlBuildQuantity() * (qtyNeeded / 100)

	                    		;

						LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
								invItemLocation, (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
						if(invBeginningCosting != null) {

							details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
							details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
							details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
							earliestDate = invBeginningCosting.getCstDate();
						}

						if(!this.filterByOptionalCriteria(criteria, invItem, buaDate, earliestDate))
							continue;

						System.out.println(invBuildUnbuildAssemblyLine.getBlBuildQuantity());
						System.out.println(qtyNeeded);
						System.out.println(standardFillSize);
						System.out.println(specificGravity);
						System.out.println("invBillOfMaterial.getBomIiName()="+invBillOfMaterial.getBomIiName());
						System.out.println("convertedBuildQuantity="+convertedBuildQuantity);
						details.setRilOutQuantity(convertedBuildQuantity);
						details.setRilOutAmount(invItemLocation.getInvItem().getIiUnitCost() *
								convertedBuildQuantity);
						details.setRilOutUnitCost(invItemLocation.getInvItem().getIiUnitCost()

								);

						details.setRilDate(buaDate);
						details.setRilDocumentNumber(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaDocumentNumber());
						details.setRilItemName(invItem.getIiName() + " - " + invItem.getIiDescription());
						details.setRilItemDesc(invItem.getIiDescription());
						details.setRilReferenceNumber(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaReferenceNumber());
						details.setRilQcNumber(invBeginningCosting != null ? invBeginningCosting.getCstQCNumber() : "");
						try {
							details.setRilSupplierName(invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getArCustomer().getCstName());

						} catch (Exception ex) {
							details.setRilSupplierName("");

						}
						details.setRilSource("INV");
						details.setRilUnit(invBuildUnbuildAssemblyLine.getInvUnitOfMeasure().getUomName());
						details.setRilReferenceNumber1(referenceNumber.toString());
						details.setRilItemCategory(invItem.getIiAdLvCategory());
						list.add(details);

	    			}

				}


				//INV_STOCK_TRANSFER_LINE OUT
				try{
					locCode = invLocationHome.findByLocName(locName, AD_CMPNY).getLocCode();
				}catch (Exception ex){
					throw new GlobalNoRecordFoundException();
				}

				Collection invStockTransferLines = invStockTransferLineHome.findUnpostedStByLocCodeAndAdBranch(locCode, brCode, AD_CMPNY);

				unpstdIter = invStockTransferLines.iterator();

				while(unpstdIter.hasNext()) {

					LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date stDate = invStockTransferLine.getInvStockTransfer().getStDate();
					LocalInvItem invItem = invStockTransferLine.getInvItem();
					LocalInvLocation invLocation = invLocationHome.findByPrimaryKey(invStockTransferLine.getStlLocationFrom());
					LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
							invItem.getIiName(), invLocation.getLocName(), AD_CMPNY);

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invItemLocation, (Date)criteria.get("dateFrom") == null ? new java.util.Date() : (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					//System.out.println("QC="+invBeginningCosting.getCstQCNumber());
					if(invBeginningCosting != null) {
						System.out.println("QC="+invBeginningCosting.getCstQCNumber());
						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, stDate, earliestDate))
						continue;

					if (invStockTransferLine.getStlQuantityDelivered() < 0 ) {

						details.setRilInQuantity(invStockTransferLine.getStlQuantityDelivered());
						details.setRilInAmount(invStockTransferLine.getStlAmount());
						details.setRilInUnitCost(invStockTransferLine.getStlUnitCost()/invStockTransferLine.getStlQuantityDelivered());

					}else if (invStockTransferLine.getStlQuantityDelivered() > 0 ) {

						details.setRilOutQuantity(invStockTransferLine.getStlQuantityDelivered());
						details.setRilOutAmount(invStockTransferLine.getStlAmount());
						details.setRilOutUnitCost(invStockTransferLine.getStlUnitCost()/invStockTransferLine.getStlQuantityDelivered());
					}

					details.setRilDate(stDate);
					details.setRilDocumentNumber(invStockTransferLine.getInvStockTransfer().getStDocumentNumber());
					details.setRilItemName(invItem.getIiName() + " - " + invItem.getIiDescription());
					details.setRilItemDesc(invItem.getIiDescription());

					LocalInvLocation invLocationTo = invLocationHome.findByPrimaryKey(invStockTransferLine.getStlLocationTo());

					details.setRilReferenceNumber(invStockTransferLine.getInvStockTransfer().getStReferenceNumber());
					details.setRilSource("INV");
					details.setRilUnit(invStockTransferLine.getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}

				// INV_STOCK_TRANSFER_LINE IN
				try{
					locCode = invLocationHome.findByLocName(locName, AD_CMPNY).getLocCode();
				}catch (Exception ex){
					throw new GlobalNoRecordFoundException();
				}
				Collection invStockTransferToLines = invStockTransferLineHome.findUnpostedStByLocToCodeAndAdBranch(locCode, brCode, AD_CMPNY);

				unpstdIter = invStockTransferToLines.iterator();

				while(unpstdIter.hasNext()) {

					LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date stDate = invStockTransferLine.getInvStockTransfer().getStDate();
					LocalInvItem invItem = invStockTransferLine.getInvItem();
					LocalInvLocation invLocation = invLocationHome.findByPrimaryKey(invStockTransferLine.getStlLocationFrom());
					LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
							invItem.getIiName(), invLocation.getLocName(), AD_CMPNY);

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invItemLocation, (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, stDate, earliestDate))
						continue;

					if (invStockTransferLine.getStlQuantityDelivered() > 0 ) {

						details.setRilInQuantity(invStockTransferLine.getStlQuantityDelivered());
						details.setRilInAmount(invStockTransferLine.getStlAmount());
						details.setRilInUnitCost(invStockTransferLine.getStlUnitCost());

					}else if (invStockTransferLine.getStlQuantityDelivered() < 0 ) {

						details.setRilOutQuantity(invStockTransferLine.getStlQuantityDelivered());
						details.setRilOutAmount(invStockTransferLine.getStlAmount());
						details.setRilOutUnitCost(invStockTransferLine.getStlUnitCost());
					}

					details.setRilDate(stDate);
					details.setRilDocumentNumber(invStockTransferLine.getInvStockTransfer().getStDocumentNumber());
					details.setRilItemName(invItem.getIiName() + " - " + invItem.getIiDescription());
					details.setRilItemDesc(invItem.getIiDescription());

					details.setRilReferenceNumber(invStockTransferLine.getInvStockTransfer().getStReferenceNumber());

					details.setRilQcNumber(invBeginningCosting != null ? invBeginningCosting.getCstQCNumber() : "");
					details.setRilSource("INV");
					details.setRilUnit(invStockTransferLine.getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}


				//INV_BRANCH_STOCK_TRANSFER_LINE
				Collection invBranchStockTransferLines = invBranchStockTransferLineHome.findUnpostedBstByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = invBranchStockTransferLines.iterator();

				while(unpstdIter.hasNext()) {

					LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date bstDate = invBranchStockTransferLine.getInvBranchStockTransfer().getBstDate();
					LocalInvItem invItem = invBranchStockTransferLine.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							invBranchStockTransferLine.getInvItemLocation(), (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, bstDate, earliestDate))
						continue;

					if (invBranchStockTransferLine.getBslQuantityReceived() > 0 ) {

						details.setRilInQuantity(invBranchStockTransferLine.getBslQuantityReceived());
						details.setRilInAmount(invBranchStockTransferLine.getBslAmount());
						details.setRilInUnitCost(invBranchStockTransferLine.getBslUnitCost());

					}else if (invBranchStockTransferLine.getBslQuantityReceived() < 0 ) {

						details.setRilOutQuantity(invBranchStockTransferLine.getBslQuantityReceived() * -1);
						details.setRilOutAmount(invBranchStockTransferLine.getBslAmount() * -1);
						details.setRilOutUnitCost(invBranchStockTransferLine.getBslUnitCost());
					}

					details.setRilDate(bstDate);
					details.setRilDocumentNumber(invBranchStockTransferLine.getInvBranchStockTransfer().getBstNumber());
					details.setRilItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setRilSource("INV");
					details.setRilUnit(invBranchStockTransferLine.getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}



				//AR_INVOICE_LINE_ITEM
				//a) arInvoice
				Collection arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedInvcByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = arInvoiceLineItems.iterator();

				while(unpstdIter.hasNext()) {

					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();
					details.setRilReferenceNumber1(referenceNumber.toString());
					Date earliestDate = null;
					Date invDate = arInvoiceLineItem.getArInvoice().getInvDate();
					LocalInvItem invItem = arInvoiceLineItem.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							arInvoiceLineItem.getInvItemLocation(), (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, invDate, earliestDate))
						continue;

					// Get Last Costing
					double COST = 0;
					try{
						LocalInvCosting invLastCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
								invDate, arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(), arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), brCode, AD_CMPNY);

						COST = Math.abs(invLastCosting.getCstRemainingValue() / invLastCosting.getCstRemainingQuantity());

					}catch(Exception e){
						COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
					}

					COST = EJBCommon.roundIt(COST, this.getGlFcPrecisionUnit(AD_CMPNY));
					double CST_CST_OF_SLS = COST * arInvoiceLineItem.getIliQuantity();
					CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, this.getGlFcPrecisionUnit(AD_CMPNY));

					details.setRilOutQuantity(arInvoiceLineItem.getIliQuantity());
					details.setRilOutUnitCost(COST);
					details.setRilOutAmount(CST_CST_OF_SLS);


					details.setRilDate(invDate);
					details.setRilDocumentNumber(arInvoiceLineItem.getArInvoice().getInvNumber());
					details.setRilItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setRilReferenceNumber(arInvoiceLineItem.getArInvoice().getInvReferenceNumber());
					details.setRilSource("AR");
					details.setRilUnit(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}

				//b) arReceipt
				arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedRctByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = arInvoiceLineItems.iterator();

				while(unpstdIter.hasNext()) {

					LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date rctDate = arInvoiceLineItem.getArReceipt().getRctDate();
					LocalInvItem invItem = arInvoiceLineItem.getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							arInvoiceLineItem.getInvItemLocation(), (Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, rctDate, earliestDate))
						continue;

					// Get Last Costing
					/*
					LocalInvCosting invLastCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
							rctDate, arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(), arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), brCode, AD_CMPNY);

					double COST = Math.abs(invLastCosting.getCstRemainingValue() / invLastCosting.getCstRemainingQuantity());
					COST = EJBCommon.roundIt(COST, this.getGlFcPrecisionUnit(AD_CMPNY));
					*/
					double COST = 0;
					try{
						LocalInvCosting invLastCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
								rctDate, arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName(), arInvoiceLineItem.getInvItemLocation().getInvLocation().getLocName(), brCode, AD_CMPNY);

						COST = Math.abs(invLastCosting.getCstRemainingValue() / invLastCosting.getCstRemainingQuantity());

					}catch(Exception e){
						COST = arInvoiceLineItem.getInvItemLocation().getInvItem().getIiUnitCost();
					}

					double CST_CST_OF_SLS = COST * arInvoiceLineItem.getIliQuantity();
					CST_CST_OF_SLS = EJBCommon.roundIt(CST_CST_OF_SLS, this.getGlFcPrecisionUnit(AD_CMPNY));

					details.setRilOutQuantity(arInvoiceLineItem.getIliQuantity());
					details.setRilOutAmount(CST_CST_OF_SLS);
					details.setRilOutUnitCost(COST);

					details.setRilDate(rctDate);
					details.setRilDocumentNumber(arInvoiceLineItem.getArReceipt().getRctNumber());
					details.setRilItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setRilReferenceNumber(arInvoiceLineItem.getArReceipt().getRctReferenceNumber());
					details.setRilSource("AR");
					details.setRilUnit(arInvoiceLineItem.getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}


				//AR_SALES_ORDER_INVOICE_LINE
				Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome.findUnpostedSoInvByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = arSalesOrderInvoiceLines.iterator();

				while(unpstdIter.hasNext()) {

					LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date invDate = arSalesOrderInvoiceLine.getArInvoice().getInvDate();
					LocalInvItem invItem = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation(),
							(Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, invDate, earliestDate))
						continue;

					details.setRilOutQuantity(arSalesOrderInvoiceLine.getSilQuantityDelivered());
					details.setRilOutAmount(arSalesOrderInvoiceLine.getSilAmount());
					details.setRilOutUnitCost(Math.abs(arSalesOrderInvoiceLine.getSilAmount() /
							arSalesOrderInvoiceLine.getSilQuantityDelivered()));

					details.setRilDate(invDate);
					details.setRilDocumentNumber(arSalesOrderInvoiceLine.getArInvoice().getInvNumber());
					details.setRilItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setRilReferenceNumber(arSalesOrderInvoiceLine.getArInvoice().getInvReferenceNumber());
					details.setRilSource("AR");
					details.setRilUnit(arSalesOrderInvoiceLine.getArSalesOrderLine().getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}
				
				
				//AR_SALES_ORDER_INVOICE_LINE
				Collection arJobOrderInvoiceLines = arJobOrderInvoiceLineHome.findUnpostedJoInvByLocNameAndAdBranch(locName, brCode, AD_CMPNY);

				unpstdIter = arJobOrderInvoiceLines.iterator();

				while(unpstdIter.hasNext()) {

					LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine)unpstdIter.next();

					InvRepItemLedgerDetails details = new InvRepItemLedgerDetails();

					Date earliestDate = null;
					Date invDate = arJobOrderInvoiceLine.getArInvoice().getInvDate();
					LocalInvItem invItem = arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem();

					LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(
							arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation(),
							(Date)criteria.get("dateFrom"), brCode, AD_CMPNY);
					if(invBeginningCosting != null) {

						details.setRilBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningUnitCost(invBeginningCosting.getCstRemainingValue() / invBeginningCosting.getCstRemainingQuantity());
						details.setRilBeginningAmount(invBeginningCosting.getCstRemainingValue());
						earliestDate = invBeginningCosting.getCstDate();
					}

					if(!this.filterByOptionalCriteria(criteria, invItem, invDate, earliestDate))
						continue;

					details.setRilOutQuantity(arJobOrderInvoiceLine.getJilQuantityDelivered());
					details.setRilOutAmount(arJobOrderInvoiceLine.getJilAmount());
					details.setRilOutUnitCost(Math.abs(arJobOrderInvoiceLine.getJilAmount() /
							arJobOrderInvoiceLine.getJilQuantityDelivered()));

					details.setRilDate(invDate);
					details.setRilDocumentNumber(arJobOrderInvoiceLine.getArInvoice().getInvNumber());
					details.setRilItemName(invItem.getIiName() + "-" + invItem.getIiDescription());
					details.setRilReferenceNumber(arJobOrderInvoiceLine.getArInvoice().getInvReferenceNumber());
					details.setRilSource("AR");
					details.setRilUnit(arJobOrderInvoiceLine.getArJobOrderLine().getInvUnitOfMeasure().getUomName());
					details.setRilReferenceNumber1(referenceNumber.toString());
					details.setRilItemCategory(invItem.getIiAdLvCategory());
					list.add(details);
				}
			}

			if (list.isEmpty()) {

				throw new GlobalNoRecordFoundException();
			}

			Collections.sort(list, InvRepItemLedgerDetails.ItemLedgerComparator);

			if(INCLD_UNPSTD || SHW_CMMTTD_QNTTY)
				Collections.sort(list, InvRepItemLedgerDetails.ItemLedgerComparator);

			if((INCLD_UNPSTD || SHW_CMMTTD_QNTTY) && (Date)criteria.get("dateFrom") != null) {

				Date dateFrom = (Date)criteria.get("dateFrom");

				ArrayList newList = new ArrayList();

				InvRepItemLedgerDetails  firstDetail = (InvRepItemLedgerDetails)list.get(0);

				String currentItem = firstDetail.getRilItemName();
				double beginningQty = firstDetail.getRilBeginningQuantity();
				double beginningAmount = firstDetail.getRilBeginningAmount();

				Iterator listIter = list.iterator();

				while(listIter.hasNext()) {

					InvRepItemLedgerDetails details = (InvRepItemLedgerDetails)listIter.next();

					if(!currentItem.equals(details.getRilItemName())) {
						currentItem = details.getRilItemName();
						beginningQty = details.getRilBeginningQuantity();
						beginningAmount = details.getRilBeginningAmount();
					}

					if(details.getRilDate().getTime() < dateFrom.getTime()) {

						beginningQty += (details.getRilInQuantity() - details.getRilOutQuantity());
						beginningAmount += (details.getRilInAmount() - details.getRilOutAmount());

					} else {

						details.setRilBeginningQuantity(beginningQty);
						details.setRilBeginningAmount(beginningAmount);

						newList.add(details);
					}
				}

				System.out.println("list size : " + list.size());
				System.out.println("newList size : " + newList.size());
				return newList;
			}
			else
				return list;

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

		Debug.print("InvRepItemLedgerControllerBean getGlFcPrecisionUnit");


		LocalAdCompanyHome adCompanyHome = null;


		// Initialize EJB Home

		try {

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try {

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			return  adCompany.getGlFunctionalCurrency().getFcPrecision();

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/

    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException{

    	Debug.print("InvFindItemLocationControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;

    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;

        ArrayList list = new ArrayList();

        // Initialize EJB Home

        try {

            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }

        try {

        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        if (adBranchResponsibilities.isEmpty()) {

            throw new GlobalNoRecordFoundException();

        }

        try {

	        Iterator i = adBranchResponsibilities.iterator();

	        while(i.hasNext()) {

	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();

		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());

		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());

		    	list.add(details);

	        }

        } catch (FinderException ex) {

        } catch (Exception ex) {

            throw new EJBException(ex.getMessage());
        }

        return list;

    }

    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {

       Debug.print("InvRepItemLedgerControllerBean getAdCompany");

       LocalAdCompanyHome adCompanyHome = null;

       // Initialize EJB Home

       try {

           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

       } catch (NamingException ex) {

           throw new EJBException(ex.getMessage());

       }

       try {

           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

           AdCompanyDetails details = new AdCompanyDetails();
           details.setCmpName(adCompany.getCmpName());

           return details;

       } catch (Exception ex) {

    	   Debug.printStackTrace(ex);
    	   throw new EJBException(ex.getMessage());

       }

   }


   // private method

   private LocalInvCosting getCstIlBeginningBalanceByItemLocationAndDate(LocalInvItemLocation invItemLocation, Date date, Integer AD_BRNCH, Integer AD_CMPNY) {

	   Debug.print("InvRepItemLedgerControllerBean getCstIlBeginningBalanceByItemLocationAndDate");

	   LocalInvCostingHome invCostingHome = null;
	   LocalInvCosting invCosting = null;


	   // Initialize EJB Home

	   try {

		   invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
		   lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);

	   } catch (NamingException ex) {

		   throw new EJBException(ex.getMessage());

	   }
	   try {

		   GregorianCalendar calendar = new GregorianCalendar();

		   if(date != null) {

			   calendar.setTime(date);
			   //calendar.add(GregorianCalendar.DATE, -1);

			   Date CST_DT = calendar.getTime();
			   System.out.println("CST_DT="+CST_DT);
			   try {
				   //System.out.println("Date: " + CST_DT + " invItemLocation.getInvItem().getIiName(): " + invItemLocation.getInvItem().getIiName());
				   //System.out.println("invItemLocation.getInvLocation().getLocName(): " + invItemLocation.getInvLocation().getLocName());
				   System.out.println("ITEM_LOC_CODE="+invItemLocation.getIlCode());

				   invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
						   CST_DT, invItemLocation.getInvItem().getIiName(),
						   invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
				   System.out.println("CST_DT="+CST_DT);
				   System.out.println(CST_DT + " " + invItemLocation.getInvItem().getIiName() + " " + invItemLocation.getInvLocation().getLocName());
			   } catch (FinderException ex) {

			   }
		   }

		   return invCosting;

	   } catch (Exception ex) {

		   Debug.printStackTrace(ex);
		   getSessionContext().setRollbackOnly();
		   throw new EJBException(ex.getMessage());

	   }


   }

   private boolean filterByOptionalCriteria(HashMap criteria, LocalInvItem invItem, Date txnDate, Date earliestDate) {

	   Debug.print("InvRepItemLedgerControllerBean filterByOptionalCriteria");

	   try {

		   String itemName = "";
		   String itemClass = "";
		   String itemCategory = "";
		   Date dateFrom = null;
		   Date dateTo = null;

		   if (criteria.containsKey("itemName"))
			   itemName = (String)criteria.get("itemName");

		   if (criteria.containsKey("itemClass"))
			   itemClass = (String)criteria.get("itemClass");

		   if (criteria.containsKey("category"))
			   itemCategory = (String)criteria.get("category");

		   if (criteria.containsKey("dateFrom"))
		   {
			   if (earliestDate == null)
				   dateFrom = (Date)criteria.get("dateFrom");
			   else
				   dateFrom = earliestDate;
		   }

		   if (criteria.containsKey("dateTo"))
			   dateTo = (Date)criteria.get("dateTo");


		   if(!itemName.equals("") && !invItem.getIiName().equals(itemName))
			   return false;

		   if(!itemClass.equals("") && !invItem.getIiClass().equals(itemClass))
			   return false;

		   if(!itemCategory.equals("") && !invItem.getIiAdLvCategory().equals(itemCategory))
			   return false;

		   if(dateFrom != null && dateTo != null)
			   if(!(txnDate.getTime() >= dateFrom.getTime() && txnDate.getTime() <= dateTo.getTime()))
				   return false;

		   if(dateFrom != null && dateTo == null)
			   if(!(txnDate.getTime() >= dateFrom.getTime()))
				   return false;

		   if(dateFrom == null && dateTo != null)
			   if(!(txnDate.getTime() <= dateTo.getTime()))
				   return false;

		   return true;

	   } catch (Exception ex) {

		   Debug.printStackTrace(ex);
		   getSessionContext().setRollbackOnly();
		   throw new EJBException(ex.getMessage());
	   }
   }


	// SessionBean methods

	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {

		Debug.print("InvRepItemLedgerControllerBean ejbCreate");

	}

}

