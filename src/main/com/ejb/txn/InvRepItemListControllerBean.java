
/*
 * InvRepItemListControllerBean.java
 *
 * Created on Aug 26, 2004, 9:59 AM
 *
 * @author  Neil Andrew M. Ajero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvPriceLevelDetails;
import com.util.InvRepItemListDetails;

/**
 * @ejb:bean name="InvRepItemListControllerEJB"
 *           display-name="Used for viewing item lists"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepItemListControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepItemListController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepItemListControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvRepItemListControllerBean extends AbstractSessionBean {
	
	
	
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvPriceLevelAll(Integer AD_CMPNY) {

		Debug.print("InvRepItemListControllerBean getAdLvInvPriceLevelAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;               

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}        

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV PRICE LEVEL", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
	
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
        
        Debug.print("InvRepItemListControllerBean getAdLvInvItemCategoryAll");
        
        LocalAdLookUpValueHome adLookUpValueHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
            
            Iterator i = adLookUpValues.iterator();
            
            while (i.hasNext()) {
                
                LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
                
                list.add(adLookUpValue.getLvName());
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdLvInvLocationAll(Integer AD_CMPNY) {
        
        Debug.print("InvRepItemListControllerBean getAdLvInvItemCategoryAll");
        
        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            invLocations = invLocationHome.findLocAll(AD_CMPNY);            
            
            if (invLocations.isEmpty()) {
                
                return null;
                
            }
            
            Iterator i = invLocations.iterator();
            
            while (i.hasNext()) {
                
                LocalInvLocation invLocation = (LocalInvLocation)i.next();	
                String details = invLocation.getLocName();
                
                list.add(details);
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeInvRepItemList(HashMap criteria, String ORDER_BY, ArrayList priceLevelList, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
        
        Debug.print("InvRepItemListControllerBean executeInvRepItemList");
        
        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvPriceLevelHome invPriceLevelHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
                         
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        System.out.println("CheckPoint executeInvRepStockOnHand A");
        try { 
            
            StringBuffer jbossQl = new StringBuffer();
            jbossQl.append("SELECT OBJECT(ii) FROM InvItem ii ");
            
            boolean fixedAsset = false;
            boolean firstArgument = true;
            short ctr = 0;
            int criteriaSize = criteria.size();	      
            
            Object obj[];	      
            
            // Allocate the size of the object parameter
            
            if (criteria.containsKey("itemName")) {
                
                criteriaSize--;
                
            }
            
            if (criteria.containsKey("supplier")) {
                
                criteriaSize--;
                
            }

            if((Byte)criteria.get("fixedAsset")==0){
            	criteriaSize--;
            }
            
            if (criteria.containsKey("location")) {
                
                jbossQl.append(", IN(ii.invItemLocations) il ");
                
            }
            
            
            
            obj = new Object[criteriaSize];
            
            if (criteria.containsKey("itemName")) {
                
                if (!firstArgument) {
                    
                    jbossQl.append("AND ");	
                    
                } else {
                    
                    firstArgument = false;
                    jbossQl.append("WHERE ");
                    
                }
                
                jbossQl.append("ii.iiName LIKE '%" + (String)criteria.get("itemName") + "%' ");
                
            }
            System.out.println("CheckPoint executeInvRepStockOnHand B");
            if (criteria.containsKey("itemCategory")) {
                
                if (!firstArgument) {		       	  	
                    jbossQl.append("AND ");		       	     
                } else {		       	  	
                    firstArgument = false;
                    jbossQl.append("WHERE ");		       	  	 
                }
                
                jbossQl.append("ii.iiAdLvCategory=?" + (ctr+1) + " ");
                obj[ctr] = (String)criteria.get("itemCategory");
                ctr++;
                
            }
            
            if (criteria.containsKey("itemClass")) {
                
                if (!firstArgument) {		       	  	
                    jbossQl.append("AND ");		       	     
                } else {		       	  	
                    firstArgument = false;
                    jbossQl.append("WHERE ");		       	  	 
                }
                
                jbossQl.append("ii.iiClass=?" + (ctr+1) + " ");
                obj[ctr] = (String)criteria.get("itemClass");
                ctr++;
                
            }
            
            if (criteria.containsKey("fixedAsset")) {

            	if((Byte)criteria.get("fixedAsset")!=0){
            		if (!firstArgument) {
                		jbossQl.append("AND ");
                	} else {
                		firstArgument = false;
                		jbossQl.append("WHERE ");
                	}

                	jbossQl.append("ii.iiFixedAsset=?" + (ctr+1) + " ");
                	obj[ctr] =(Byte)criteria.get("fixedAsset");
                	ctr++;
                	fixedAsset = true;
            	}
            	
            } 
            
            if (criteria.containsKey("enable") &&
                    criteria.containsKey("disable")) {
                
                if (!firstArgument) {
                    jbossQl.append("AND ");
                } else {
                    firstArgument = false;
                    jbossQl.append("WHERE ");
                }
                
                jbossQl.append("(ii.iiEnable=?" + (ctr+1) + " OR ");
                obj[ctr] = new Byte(EJBCommon.TRUE);
                ctr++;
                
                jbossQl.append("ii.iiEnable=?" + (ctr+1) + ") ");
                obj[ctr] = new Byte(EJBCommon.FALSE);
                ctr++;
                
            } else {
            	System.out.println("CheckPoint executeInvRepStockOnHand C");
                if (criteria.containsKey("enable")) {
                    
                    if (!firstArgument) {
                        jbossQl.append("AND ");
                    } else {
                        firstArgument = false;
                        jbossQl.append("WHERE ");
                    }
                    jbossQl.append("ii.iiEnable=?" + (ctr+1) + " ");
                    obj[ctr] = new Byte(EJBCommon.TRUE);
                    ctr++;
                    
                } 
                
                if (criteria.containsKey("disable")) {
                    
                    if (!firstArgument) {
                        jbossQl.append("AND ");
                    } else {
                        firstArgument = false;
                        jbossQl.append("WHERE ");
                    }
                    jbossQl.append("ii.iiEnable=?" + (ctr+1) + " ");		      	 		      	 		      	 	      	 
                    obj[ctr] = new Byte(EJBCommon.FALSE);
                    ctr++;
                    
                }
                
            }
            
            
            if (criteria.containsKey("location")) {
                
                if (!firstArgument) {		       	  	
                    jbossQl.append("AND ");		       	     
                } else {		       	  	
                    firstArgument = false;
                    jbossQl.append("WHERE ");		       	  	 
                }
                
                jbossQl.append("il.invLocation.locName=?" + (ctr+1) + " ");
                obj[ctr] = (String)criteria.get("location");
                ctr++;
                
            }
            
            if (!firstArgument) {
                
                jbossQl.append("AND ");
                
            } else {
                
                firstArgument = false;
                jbossQl.append("WHERE ");
                
            }
            
            jbossQl.append("ii.iiNonInventoriable=0 AND ii.iiAdCompany=" + AD_CMPNY + " ");
            
            String orderBy = null;
            
            if (ORDER_BY.equals("ITEM NAME")) {
                
                orderBy = "ii.iiName";
                
            } else if (ORDER_BY.equals("ITEM CLASS")) {
                
                orderBy = "ii.iiClass";	  
                
            } else if (ORDER_BY.equals("ITEM CATEGORY")) {
                
                orderBy = "ii.iiAdLvCategory";
                
            }
            
            if (orderBy != null) {
                
                jbossQl.append("ORDER BY " + orderBy);
                
            }    
            
            
            Collection invItems = invItemHome.getIiByCriteria(jbossQl.toString(), obj);	         
            
            if (invItems.size() == 0)
                throw new GlobalNoRecordFoundException();
            
            Iterator i = invItems.iterator();
            System.out.println("CheckPoint executeInvRepStockOnHand D");
            while (i.hasNext()) {
                
                LocalInvItem invItem = (LocalInvItem)i.next();
                LocalInvLocation itemlocation = null;
                String locName = null;
                if(invItem.getIiDefaultLocation() != null)
                	itemlocation = invLocationHome.findByPrimaryKey(invItem.getIiDefaultLocation());
                
                InvRepItemListDetails details = new InvRepItemListDetails();
                details.setIlIiName(invItem.getIiName());
                details.setIlIiDescription(invItem.getIiDescription());
                details.setIlIiClass(invItem.getIiClass());
                details.setIlIiEnable(invItem.getIiEnable());
                details.setIlIiUomName(invItem.getInvUnitOfMeasure().getUomName());
                details.setIlIiUnitCost(invItem.getIiUnitCost());
                details.setIlIiPrice(invItem.getIiSalesPrice());
                details.setIlIiCostMethod(invItem.getIiCostMethod());
                details.setIlIiPartNumber(invItem.getIiPartNumber());  
                
                details.setIlIiCategoryName(invItem.getIiAdLvCategory());
                details.setIiDateAcquired(invItem.getIiDateAcquired());
                
                try{
                	details.setIlIiLctn(itemlocation.getLocName());
                    details.setIlIiDprtmnt(itemlocation.getLocDepartment());
                    details.setIlIiBrnch(itemlocation.getLocBranch());
                }catch(Exception e){
                	details.setIlIiLctn("");
                    details.setIlIiDprtmnt("");
                    details.setIlIiBrnch("");
                }
                
                
                
                
               if(criteria.get("supplier").toString().equals("1")){
                	if(invItem.getApSupplier() != null){
                		details.setIlIiSupplierName(invItem.getApSupplier().getSplName());
                	}
                }
               
               
               if (!priceLevelList.isEmpty()) {
            	   
            	  ArrayList priceLevels = new ArrayList();

					Iterator iter = priceLevelList.iterator();

					while(iter.hasNext()) {

						String PL_AD_LV_PRC_LVL = (String) iter.next();

            		   LocalInvPriceLevel invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(
            				   	invItem.getIiName(), PL_AD_LV_PRC_LVL, AD_CMPNY);
            		   
            		   
            		   InvPriceLevelDetails pdetails = new InvPriceLevelDetails();
            		   
            		   pdetails.setPlAdLvPriceLevel(invPriceLevel.getPlAdLvPriceLevel());
            		   pdetails.setPlAmount(invPriceLevel.getPlAmount());
            		   priceLevels.add(pdetails);
            	   }
            	   details.setIiPriceLevels(priceLevels);
            	   
               }
                              
                list.add(details);
                
            }
            
            if (list.size() == 0)
                throw new GlobalNoRecordFoundException();
            
            return list;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            throw ex;
            
        } catch (Exception ex) {
            
            
            ex.printStackTrace();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
        
        Debug.print("InvRepItemListControllerBean getAdCompany");      
        
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            AdCompanyDetails details = new AdCompanyDetails();
            details.setCmpName(adCompany.getCmpName());
            
            return details;  	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    // SessionBean methods
    
    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {
        
        Debug.print("InvRepItemListControllerBean ejbCreate");
        
    }
}
