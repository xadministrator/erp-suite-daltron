package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPriceLevel;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvPriceLevelDetails;
import com.util.InvRepMarkupListDetails;

/**
* @ejb:bean name="InvRepMarkupListControllerEJB"
*           display-name="Used for generation of markup inventory list reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/InvRepMarkupListControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.InvRepMarkupListController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.InvRepMarkupListControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class InvRepMarkupListControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepMarkupListControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepMarkupListControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
     * @ejb:interface-method view-type="remote"
     * 
     **/
	public ArrayList executeInvRepMarkupList(HashMap criteria, String UOM_NM, boolean INCLD_ZRS, boolean recalcMarkup,
			String ORDER_BY, ArrayList priceLevelList, ArrayList branchList, Integer AD_BRNCH, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvRepMarkupListControllerBean executeInvRepMarkupList");
		
		LocalAdBranchItemLocationHome invMarkupListHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvPriceLevelHome invPriceLevelHome =  null;
		LocalInvBillOfMaterialHome invBillOfMaterialHome =  null;
		LocalInvItemHome invItemHome =  null;
		
		ArrayList invMarkupListList = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invMarkupListHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
			invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
		
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		try { 
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalInvUnitOfMeasure invUnitOfMeasure = null;
			
			//Unit
			if(UOM_NM != null || UOM_NM.length() > 0) {
			
				try {
				
					invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);
					
				} catch (FinderException ex) {
					
				}
				
			}
			
			StringBuffer jbossQl = new StringBuffer();
    	    jbossQl.append("SELECT OBJECT(bil) FROM AdBranchItemLocation bil ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			Object obj[] = null;	      
			
			//Branch Map

			  if (branchList.isEmpty()) {
			  	
			  	throw new GlobalNoRecordFoundException();
			  	
			  }
			  else {
			  	
			  	jbossQl.append("WHERE bil.adBranch.brCode in (");
			  	
			  	boolean firstLoop = true;
			  	
			  	Iterator j = branchList.iterator();
			  	
			  	while(j.hasNext()) {
			  		
			  		if(firstLoop == false) { 
			  			jbossQl.append(", "); 
			  		}
			  		else { 
			  			firstLoop = false; 
			  		}
			  		
			  		AdBranchDetails mdetails = (AdBranchDetails) j.next();
			  		
			  		jbossQl.append(mdetails.getBrCode());
			  		
			  	}
			  	
			  	jbossQl.append(") ");
			  	
			  	firstArgument = false;
			  	
			  }                    
			
			// Allocate the size of the object parameter
			//Item Name
			if (criteria.containsKey("itemName")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("itemName")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invItem.iiName  LIKE '%" + (String)criteria.get("itemName") + "%' ");
				
			}
			//Item Class
			if (criteria.containsKey("itemClass")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemClass");
				ctr++;
				
			}
			
			//Item Category
			if (criteria.containsKey("itemCategory")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemCategory");
				ctr++;
				
			}				
			
			//Location
			if (criteria.containsKey("location")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;
				
			}	
			
			//Location Type
			if (criteria.containsKey("locationType")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {		       	  
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("bil.invItemLocation.invLocation.locLvType=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("locationType");
				ctr++;
				
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("bil.invItemLocation.invItem.iiNonInventoriable=0 AND bil.bilAdCompany=" + AD_CMPNY + " ");   	  	  
			
			//Order By

			String orderBy = null;
	          
	          if (ORDER_BY.equals("ITEM NAME")) {
		      	
		      	  orderBy = "bil.invItemLocation.invItem.iiName";
		      	
		      } else if (ORDER_BY.equals("ITEM DESCRIPTION")) {
		      	
		      	  orderBy = "bil.invItemLocation.invItem.iiDescription";
		      	
		      } else if (ORDER_BY.equals("ITEM CLASS")) {
		      	
		      	  orderBy = "bil.invItemLocation.invItem.iiClass";
		      	
		      } 

			  if (orderBy != null) {
			  
			  	jbossQl.append("ORDER BY " + orderBy);
			  	
			  }
			
			
			Collection invMarkupLists = null;

			System.out.println(jbossQl.toString());
			
			try {
				
				invMarkupLists = invMarkupListHome.getBilByCriteria(jbossQl.toString(), obj);
				
			} catch (FinderException ex)	{
				
				throw new GlobalNoRecordFoundException ();
				
			} 

			if (invMarkupLists.isEmpty())
				throw new GlobalNoRecordFoundException ();
			
			Iterator i = invMarkupLists.iterator();
			
			while (i.hasNext()) {

				LocalAdBranchItemLocation invMarkupList = (LocalAdBranchItemLocation) i.next();

				InvRepMarkupListDetails details = new InvRepMarkupListDetails();
				double unitCost = 0d;
				double shppngCost = 0d;
				double salesPrice =0d;
				double II_MRKP_VL =0d;
				double costQty =0d;
				
				if(recalcMarkup){
					
					invMarkupList.getInvItemLocation().getInvItem().getIiClass();
					System.out.println("1 "+invMarkupList.getInvItemLocation().getInvItem().getIiClass());
					System.out.println("2 "+invMarkupList.getInvItemLocation().getInvItem().getIiName());
					Collection invBillOfMaterials = invMarkupList.getInvItemLocation().getInvItem().getInvBillOfMaterials();
					Iterator invBll = invBillOfMaterials.iterator();

					while (invBll.hasNext()) {
						LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)invBll.next();

						LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(), AD_CMPNY);
						System.out.println("invBillOfMaterial.getBomIiName(): "+invBillOfMaterial.getBomIiName());
						unitCost = invItem.getIiUnitCost();
						System.out.println("invItem.getIiUnitCost(): "+invItem.getIiUnitCost());
						double AMOUNT = this.convertByUomFromAndItemAndQuantity(invBillOfMaterial.getInvUnitOfMeasure(), 
								invBillOfMaterial.getBomIiName(), invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY);

						//double ADJUST_QTY = this.convertQuantityByUomToAndItem(invUnitOfMeasure, invBillOfMaterial.getBomIiName(), invBillOfMaterial.getBomQuantityNeeded(), AD_CMPNY);
						AMOUNT =  EJBCommon.roundIt(AMOUNT, (short)2);
						System.out.println("AMOUNT: "+AMOUNT);
						costQty = costQty + (invBillOfMaterial.getBomQuantityNeeded()*AMOUNT);
						System.out.println("costQty: "+costQty);
						System.out.println("getBomIiName: " + invBillOfMaterial.getBomIiName());
						System.out.println("invBillOfMaterial.getInvItem().getIiName(): " + invBillOfMaterial.getInvItem().getIiName());

					}

					System.out.println("costQty: " + costQty);
					if(costQty==0.0){
						costQty = invMarkupList.getInvItemLocation().getInvItem().getIiUnitCost();
					}
					System.out.println("costQty: " + costQty);
					
					shppngCost = invMarkupList.getInvItemLocation().getInvItem().getIiShippingCost();
					salesPrice =invMarkupList.getInvItemLocation().getInvItem().getIiSalesPrice();

					System.out.println("shppngCost: " + invMarkupList.getInvItemLocation().getInvItem().getIiShippingCost());

					System.out.println("salesPrice: " + invMarkupList.getInvItemLocation().getInvItem().getIiSalesPrice());

					II_MRKP_VL = (((salesPrice - costQty - shppngCost) / (costQty + shppngCost)) * 100);

					LocalInvItem invItem = invItemHome.findByIiName(invMarkupList.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);
					
					System.out.println("II_MRKP_VL: " + II_MRKP_VL);
					II_MRKP_VL =  EJBCommon.roundIt(II_MRKP_VL, (short)3);
					System.out.println("invItem.getIiName: " + invItem.getIiName());
					System.out.println("II_MRKP_VL: " + II_MRKP_VL);
					invItem.setIiPercentMarkup(II_MRKP_VL);
					invItem.setIiUnitCost(costQty);

				}

				details.setMlItemName(invMarkupList.getInvItemLocation().getInvItem().getIiName());
				details.setMlItemDescription(invMarkupList.getInvItemLocation().getInvItem().getIiDescription());
				details.setMlItemClass(invMarkupList.getInvItemLocation().getInvItem().getIiClass());
				details.setMlIiPartNumber(invMarkupList.getInvItemLocation().getInvItem().getIiPartNumber());
				details.setMlLocation(invMarkupList.getInvItemLocation().getInvLocation().getLocName());
				details.setMlUnit(invMarkupList.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());

				double SALES_PRC = invMarkupList.getInvItemLocation().getInvItem().getIiSalesPrice();
				double QTY = 0d;
				double AMOUNT = 0d;
				double UNIT_COST = 0d;
				double AVE_COST = 0d;

				double MU_PCT = 0d;
				double SHPPNG_CST = 0d;
				double GRS_PFT = 0d;

				try {

					LocalInvCosting invCosting = 
						invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(
								details.getMlItemName(),details.getMlLocation(),
								invMarkupList.getAdBranch().getBrCode(), AD_CMPNY);

					QTY = invCosting.getCstRemainingQuantity();
					AMOUNT = invCosting.getCstRemainingValue();
					UNIT_COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
					AVE_COST = invCosting.getCstRemainingQuantity() == 0 ? 0 : 
						invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity();

				} catch (FinderException ex){

					QTY = 0d;
					AMOUNT = 0d;
					UNIT_COST = invMarkupList.getInvItemLocation().getInvItem().getIiUnitCost();
					AVE_COST = invMarkupList.getInvItemLocation().getInvItem().getIiUnitCost();

				}

				MU_PCT = invMarkupList.getInvItemLocation().getInvItem().getIiPercentMarkup();
				SHPPNG_CST = invMarkupList.getInvItemLocation().getInvItem().getIiShippingCost();
				GRS_PFT = SALES_PRC - (AVE_COST + SHPPNG_CST);
				System.out.println("***************************");
				System.out.println("UNIT_COST: " + UNIT_COST);
				System.out.println("***************************");
				
				if(invUnitOfMeasure != null && 
						invUnitOfMeasure.getUomAdLvClass().equals(invMarkupList.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomAdLvClass())) {
					
					// convert qty, amount, unit cost, ave cost

					SALES_PRC = this.convertCostByUomToAndItem(invUnitOfMeasure, invMarkupList.getInvItemLocation().getInvItem(), SALES_PRC, AD_CMPNY);
					AMOUNT = this.convertAmountByUomToAndItemAndQtyAndAveCost(invUnitOfMeasure, invMarkupList.getInvItemLocation().getInvItem(), QTY, AVE_COST, AD_CMPNY);
					QTY = this.convertQuantityByUomToAndItem(invUnitOfMeasure, invMarkupList.getInvItemLocation().getInvItem(), QTY, AD_CMPNY);
					UNIT_COST = this.convertCostByUomToAndItem(invUnitOfMeasure, invMarkupList.getInvItemLocation().getInvItem(), UNIT_COST, AD_CMPNY);
					AVE_COST = this.convertCostByUomToAndItem(invUnitOfMeasure, invMarkupList.getInvItemLocation().getInvItem(), AVE_COST, AD_CMPNY);

					SHPPNG_CST = this.convertCostByUomToAndItem(invUnitOfMeasure, invMarkupList.getInvItemLocation().getInvItem(), SHPPNG_CST, AD_CMPNY);
					GRS_PFT = this.convertCostByUomToAndItem(invUnitOfMeasure, invMarkupList.getInvItemLocation().getInvItem(), GRS_PFT, AD_CMPNY);			

					details.setMlUnit(invUnitOfMeasure.getUomName());

				}

				System.out.println("***************************");
				System.out.println("UNIT_COST: " + UNIT_COST);
				System.out.println("***************************");
				details.setMlSalesPrice(SALES_PRC);
				details.setMlQuantity(QTY);
				details.setMlAmount(AMOUNT);
				details.setMlUnitCost(UNIT_COST);
				details.setMlAverageCost(AVE_COST);
				details.setMlMarkupPercent(MU_PCT);
				details.setMlShippingCost(SHPPNG_CST);
				details.setMlGrossProfit(GRS_PFT);

				// get price levels
				if (!priceLevelList.isEmpty()) {
					double II_MRKP_VL2 =0d;
					double prcAmnt =0d;
					double prcShppngCst =0d;

					ArrayList priceLevels = new ArrayList();

					Iterator iter = priceLevelList.iterator();

					while(iter.hasNext()) {

						String PL_AD_LV_PRC_LVL = (String) iter.next();

						InvPriceLevelDetails pdetails = new InvPriceLevelDetails();

						try {

							LocalInvPriceLevel invPriceLevel = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(
									invMarkupList.getInvItemLocation().getInvItem().getIiName(), 
									PL_AD_LV_PRC_LVL, AD_CMPNY);

							pdetails.setPlAdLvPriceLevel(invPriceLevel.getPlAdLvPriceLevel());
							if(invUnitOfMeasure != null && 
									invUnitOfMeasure.getUomAdLvClass().equals(invMarkupList.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomAdLvClass())) {


								

								pdetails.setPlAmount(this.convertCostByUomToAndItem(invUnitOfMeasure, invMarkupList.getInvItemLocation().getInvItem(), invPriceLevel.getPlAmount(), AD_CMPNY));
								pdetails.setPlShippingCost(this.convertCostByUomToAndItem(invUnitOfMeasure, invMarkupList.getInvItemLocation().getInvItem(), invPriceLevel.getPlShippingCost(), AD_CMPNY));

								prcAmnt = this.convertCostByUomToAndItem(invUnitOfMeasure, invMarkupList.getInvItemLocation().getInvItem(), invPriceLevel.getPlAmount(), AD_CMPNY);
								prcShppngCst = this.convertCostByUomToAndItem(invUnitOfMeasure, invMarkupList.getInvItemLocation().getInvItem(), invPriceLevel.getPlShippingCost(), AD_CMPNY);
								
								
								if(recalcMarkup){
									//invPriceLevel.setPlAmount(costQty);
									II_MRKP_VL2 = (((prcAmnt - costQty - prcShppngCst) / (costQty + prcShppngCst)) * 100);
									II_MRKP_VL2 =  EJBCommon.roundIt(II_MRKP_VL2, (short)3);
									System.out.println("II_MRKP_VL2: " +II_MRKP_VL2);
									invPriceLevel.setPlPercentMarkup(II_MRKP_VL2);
									
								}
								pdetails.setPlPercentMarkup(invPriceLevel.getPlPercentMarkup());
							} else {
								pdetails.setPlAmount(invPriceLevel.getPlAmount());
								pdetails.setPlShippingCost(invPriceLevel.getPlShippingCost());
								
								prcAmnt = invPriceLevel.getPlAmount();
								prcShppngCst = invPriceLevel.getPlShippingCost();

								if(recalcMarkup){
									//invPriceLevel.setPlAmount(costQty);
									II_MRKP_VL2 = (((prcAmnt - costQty - prcShppngCst) / (costQty + prcShppngCst)) * 100);
									II_MRKP_VL2 =  EJBCommon.roundIt(II_MRKP_VL2, (short)3);
									System.out.println("II_MRKP_VL3: " +II_MRKP_VL2);
									invPriceLevel.setPlPercentMarkup(II_MRKP_VL2);
								}
								pdetails.setPlPercentMarkup(invPriceLevel.getPlPercentMarkup());
							}

						} catch(FinderException ex) {

							pdetails.setPlAdLvPriceLevel(PL_AD_LV_PRC_LVL);
							pdetails.setPlAmount(0d);

						}

						priceLevels.add(pdetails);

					}

					details.setMlPriceLevels(priceLevels);	

				}

				if ((details.getMlQuantity() > 0) ||(INCLD_ZRS && (details.getMlQuantity() <= 0)))

					invMarkupListList.add(details);

			}

			if (invMarkupListList.isEmpty())
				throw new GlobalNoRecordFoundException ();
			else
				return invMarkupListList;	  
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvLocationTypeAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepMarkupListControllerBean getAdLvInvLocationTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV LOCATION TYPE", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}		

	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepMarkupListControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("InvRepMarkupListControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomAll(Integer AD_CMPNY) {
        
        Debug.print("InvRepMarkupListControllerBean getInvUomAll");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        
        try {
            
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection invUnitOfMeasures = invUnitOfMeasureHome.findEnabledUomAll(AD_CMPNY);
            
            Iterator i = invUnitOfMeasures.iterator();
            
            while (i.hasNext()) {
                
            	LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure)i.next();
                
                list.add(invUnitOfMeasure.getUomName());
                
            }
            
            return list;
            
        } catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
        
    }
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvPriceLevelAll(Integer AD_CMPNY) {

		Debug.print("InvRepMarkupListControllerBean getAdLvInvPriceLevelAll");

		LocalAdLookUpValueHome adLookUpValueHome = null;               

		ArrayList list = new ArrayList();

		// Initialize EJB Home

		try {

			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}        

		try {

			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV PRICE LEVEL", AD_CMPNY);

			Iterator i = adLookUpValues.iterator();

			while (i.hasNext()) {

				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();

				list.add(adLookUpValue.getLvName());

			}

			return list;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}

	}
    
    // private methods
	private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, String invItem, double ADJST_QTY, Integer AD_CMPNY) {
		
		Debug.print("InvAdjustmentEntryControllerBean convertByUomFromAndItemAndQuantity");		        
        
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		LocalInvItemHome invItemHome=null;
		// Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            LocalInvItem invItem2 =  invItemHome.findByIiName(invItem, AD_CMPNY);
            
			System.out.println("invItem2.getIiName(): " + invItem2.getIiName());       	
			System.out.println("invItem2.getInvUnitOfMeasure().getUomName(): " + invItem2.getInvUnitOfMeasure().getUomName());  
			System.out.println("invItem2.getIiUnitCost(): " + invItem2.getIiUnitCost());
			System.out.println("invFromUnitOfMeasure.getUomName(): "+invFromUnitOfMeasure.getUomName());
			
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem2.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion= invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem2.getIiName(), invItem2.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
                
            System.out.println("invFromUnitOfMeasure.getUomName(): " + invFromUnitOfMeasure.getUomName()); 
            System.out.println("invItem2.getInvUnitOfMeasure().getUomName(): " + invItem2.getInvUnitOfMeasure().getUomName()); 
            System.out.println("ADJST_QTY: " + ADJST_QTY);  
            System.out.println("invDefaultUomConversion.getUmcConversionFactor(): " + invDefaultUomConversion.getUmcConversionFactor());  
            System.out.println("invUnitOfMeasureConversion.getUmcConversionFactor(): " + invUnitOfMeasureConversion.getUmcConversionFactor());  
            System.out.println("adPreference.getPrfInvQuantityPrecisionUnit(): " + adPreference.getPrfInvQuantityPrecisionUnit());  
            
            /*ADJST_QTY: 1.0
            invDefaultUomConversion.getUmcConversionFactor(): 1.0
            invUnitOfMeasureConversion.getUmcConversionFactor(): 6400.0
            adPreference.getPrfInvQuantityPrecisionUnit(): 3
            */
        	return EJBCommon.roundIt(invItem2.getIiUnitCost() *(invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor()), adPreference.getPrfInvQuantityPrecisionUnit());
        						    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}		
	
    private double convertQuantityByUomToAndItem(LocalInvUnitOfMeasure invToUnitOfMeasure, LocalInvItem invItem, double QTY, Integer AD_CMPNY) {
        
        Debug.print("InvRepMarkupListControllerBean convertQuantityByUomToAndItem");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invToUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(QTY * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private double convertCostByUomToAndItem(LocalInvUnitOfMeasure invToUnitOfMeasure, LocalInvItem invItem, double COST, Integer AD_CMPNY) {
        
        Debug.print("InvRepMarkupListControllerBean convertCostByUomToAndItem");		        
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invToUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adCompany.getGlFunctionalCurrency().getFcPrecision());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private double convertAmountByUomToAndItemAndQtyAndAveCost(LocalInvUnitOfMeasure invToUnitOfMeasure, LocalInvItem invItem, double QTY, double AVE_COST, Integer AD_CMPNY) {
        
        Debug.print("InvRepMarkupListControllerBean convertAmountByUomToAndItemAndQtyAndAveCost");		        
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invToUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt((AVE_COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor()) * 
            		(QTY * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor()), adCompany.getGlFunctionalCurrency().getFcPrecision());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	
	// SessionBean methods
	
    /**
     * @ejb:create-method view-type="remote"
     **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvRepMarkupListControllerBean ejbCreate");
		
	}
	
}

