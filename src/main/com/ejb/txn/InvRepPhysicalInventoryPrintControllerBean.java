
package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPhysicalInventory;
import com.ejb.inv.LocalInvPhysicalInventoryHome;
import com.ejb.inv.LocalInvPhysicalInventoryLine;
import com.ejb.inv.LocalInvTag;
import com.ejb.inv.LocalInvTagHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.ArRepCustomerListDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvModAdjustmentLineDetails;
import com.util.InvRepPhysicalInventoryPrintDetails;

/**
 * @ejb:bean name="InvRepPhysicalInventoryPrintControllerEJB"
 *           display-name="Use for printing physical inventory"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepPhysicalInventoryPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepPhysicalInventoryPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepPhysicalInventoryPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class InvRepPhysicalInventoryPrintControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeInvRepPhysicalInventoryPrint(ArrayList piCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvRepPhysicalInventoryPrintControllerBean executeInvRepPhysicalInventoryPrint");
        
        LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdUserHome adUserHome = null;


        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
        	
        	invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adUserHome = (LocalAdUserHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdUserHome .JNDI_NAME, LocalAdUserHome .class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = piCodeList.iterator();
        	
        	while (i.hasNext()) {
        		
        		Integer PI_CODE = (Integer) i.next();

        		LocalInvPhysicalInventory invPhysicalInventory = null;
        		
        		try {
        			
        			invPhysicalInventory = invPhysicalInventoryHome.findByPrimaryKey(PI_CODE);
        		    
        		} catch (FinderException ex) {
        			
        			continue;
        			
        		}	        	  	         
        		
        		// get adjustment lines 

        		Collection invPhysicalInventoryLines = invPhysicalInventory.getInvPhysicalInventoryLines();
        		
        		Iterator piIter = invPhysicalInventoryLines.iterator();
        		
        		while(piIter.hasNext()) {

        			LocalInvPhysicalInventoryLine invPhysicalInventoryLine = (LocalInvPhysicalInventoryLine)piIter.next();
        			

    				InvRepPhysicalInventoryPrintDetails details = new InvRepPhysicalInventoryPrintDetails();
    				
        			details.setPipPiDate(invPhysicalInventory.getPiDate());
        			details.setPipPiReferenceNumber(invPhysicalInventory.getPiReferenceNumber());
        			details.setPipPiDescription(invPhysicalInventory.getPiDescription());
        			
        			/*details.setApAdjGlCoaAccount(invAdjustment.getGlChartOfAccount().getCoaAccountNumber());
    		 		details.setApAdjGlCoaAccountDesc(sample);*/

                    LocalAdUser adUser = adUserHome.findByUsrName(invPhysicalInventory.getPiCreatedBy(), AD_CMPNY);
        			details.setPipPiCreatedBy(adUser.getUsrDescription());
        			
        			details.setPipPlIiName(invPhysicalInventoryLine.getInvItemLocation().getInvItem().getIiName());
        			details.setPipPlIiDescription(invPhysicalInventoryLine.getInvItemLocation().getInvItem().getIiDescription());
        			details.setPipPlLocName(invPhysicalInventoryLine.getInvItemLocation().getInvLocation().getLocName());
        			details.setPipPlUomName(invPhysicalInventoryLine.getInvUnitOfMeasure().getUomName());
        			details.setPipPlEndingInventory(invPhysicalInventoryLine.getPilEndingInventory());
        			details.setPipPlWastage(invPhysicalInventoryLine.getPilWastage());
        			details.setPipPlVariance(invPhysicalInventoryLine.getPilVariance());
        			
        			LocalInvCosting invLastCosting = null;
        			
        			double COST = 0d;
        			
        			try{
        				
            			invLastCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIlCode(invPhysicalInventory.getPiDate(), invPhysicalInventoryLine.getInvItemLocation().getIlCode(), invPhysicalInventory.getPiAdBranch(), AD_CMPNY);
            			COST = Math.abs(invLastCosting.getCstRemainingValue() / invLastCosting.getCstRemainingQuantity());
            			
        			}catch(Exception e){
        				COST = 0;
        			}
        			
        			details.setPipPlUnitCost(COST);
        			
        			list.add(details);
        			
        		
        		}
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}    
        	
        	Collections.sort(list, InvRepPhysicalInventoryPrintDetails.ItemComparator);
        	
        	return list;        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepPhysicalInventoryPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    
		
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvRepPhysicalInventoryPrintControllerBean ejbCreate");
      
    }
}
