
/*
 * InvRepPhysicalInventoryWorksheetControllerBean.java
 *
 * Created on September 11,2006 11:27 AM
 *
 * @author  Rey B. Limosenero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvAssemblyTransferLine;
import com.ejb.inv.LocalInvAssemblyTransferLineHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLine;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPhysicalInventory;
import com.ejb.inv.LocalInvPhysicalInventoryHome;
import com.ejb.inv.LocalInvPhysicalInventoryLine;
import com.ejb.inv.LocalInvPhysicalInventoryLineHome;
import com.ejb.inv.LocalInvPriceLevelHome;
import com.ejb.inv.LocalInvStockIssuanceLine;
import com.ejb.inv.LocalInvStockIssuanceLineHome;
import com.ejb.inv.LocalInvStockTransferLine;
import com.ejb.inv.LocalInvStockTransferLineHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvRepPhysicalInventoryWorksheetDetails;

/**
 * @ejb:bean name="InvRepPhysicalInventoryWorksheetControllerEJB"
 *           display-name="Use for printing physical inventory worksheet"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepPhysicalInventoryWorksheetControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepPhysicalInventoryWorksheetController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepPhysicalInventoryWorksheetControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class InvRepPhysicalInventoryWorksheetControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {
        
        Debug.print("InvRepPhysicalInventoryWorksheetControllerBean getInvLocAll");
        
        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            invLocations = invLocationHome.findLocAll(AD_CMPNY);            
            
            if (invLocations.isEmpty()) {
                
                return null;
                
            }
            
            Iterator i = invLocations.iterator();
            
            while (i.hasNext()) {
                
                LocalInvLocation invLocation = (LocalInvLocation)i.next();	
                String details = invLocation.getLocName();
                
                list.add(details);
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
        
        Debug.print("InvRepPhysicalInventoryWorksheetControllerBean getAdLvInvItemCategoryAll");
        
        LocalAdLookUpValueHome adLookUpValueHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
            
            Iterator i = adLookUpValues.iterator();
            
            while (i.hasNext()) {
                
                LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
                
                list.add(adLookUpValue.getLvName());
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvCstAll(Integer AD_BRNCH, Integer AD_CMPNY) {
        
        Debug.print("InvRepPhysicalInventoryWorksheetControllerBean getInvCstAll");
        
        LocalArCustomerHome arCustomerHome = null;               
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
            lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
            
            Collection arCustomers = arCustomerHome.findEnabledCstAll(AD_BRNCH, AD_CMPNY);
            
            Iterator i = arCustomers.iterator();
            
            while (i.hasNext()) {
                
                LocalArCustomer arCustomer = (LocalArCustomer)i.next();
                
                list.add(arCustomer.getCstCustomerCode());
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }  
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getAdBrAll(Integer AD_CMPNY) {
        
        Debug.print("InvRepPhysicalInventoryWorksheetControllerBean getAdBrAll");
        
        LocalAdBranchHome adBranchHome = null;
        Collection adBranches = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            adBranches = adBranchHome.findBrAll(AD_CMPNY);            
            
            if (adBranches.isEmpty()) {
                
                return null;
                
            }
            
            Iterator i = adBranches.iterator();
            
            while (i.hasNext()) {
                
                LocalAdBranch AdBranch = (LocalAdBranch)i.next();	
                String details = AdBranch.getBrBranchCode();
                
                list.add(details);
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList executeInvRepPhysicalInventoryWorksheet(HashMap criteria, Date PI_DT, boolean INCLD_UNPSTD, 
    		boolean INCLD_ENCDD, Integer AD_CMPNY, boolean INCLD_INTRNST) 
    throws GlobalNoRecordFoundException {
        
        Debug.print("InvRepPhysicalInventoryWorksheetControllerBean executeInvRepPhysicalInventoryWorksheet");
        
        LocalInvCostingHome invCostingHome = null;
        LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvLocationHome invLocationHome = null;		
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalInvStockIssuanceLineHome invStockIssuanceLineHome = null;
		LocalInvAssemblyTransferLineHome invAssemblyTransferLineHome = null;
		LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
		LocalInvStockTransferLineHome invStockTransferLineHome = null;
		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
		LocalArCustomerHome arCustomerHome = null;
		LocalInvPriceLevelHome invPriceLevelHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			invStockIssuanceLineHome = (LocalInvStockIssuanceLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvStockIssuanceLineHome.JNDI_NAME, LocalInvStockIssuanceLineHome.class);
			invAssemblyTransferLineHome = (LocalInvAssemblyTransferLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAssemblyTransferLineHome.JNDI_NAME, LocalInvAssemblyTransferLineHome.class);
			invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
			invStockTransferLineHome = (LocalInvStockTransferLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class);
			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
			apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
			lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
			lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
			invPriceLevelHome = (LocalInvPriceLevelHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvPriceLevelHome.JNDI_NAME, LocalInvPriceLevelHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        String paramCategory = ""; 
        
        try { 
            
            StringBuffer jbossQl = new StringBuffer();
            jbossQl.append("SELECT OBJECT(il) FROM InvItemLocation il, IN(il.adBranchItemLocations)bil ");
            
            boolean firstArgument = true;
            short ctr = 0;
            int criteriaSize = criteria.size();	      
            
            Object obj[];	      
            
            Integer AD_BRNCH = null;
            String PriceLevel = "";
            
            if (criteria.containsKey("branch")) {
                
                criteriaSize--;
                
                AD_BRNCH = this.getAdBrCodeByBrName((String)criteria.get("branch"), AD_CMPNY);
                
                if(AD_BRNCH == null)
                    throw new GlobalNoRecordFoundException();
                
            }
            
            if (criteria.containsKey("customer")) {

            	criteriaSize--;
                
                String cstmr = (String)criteria.get("customer");
                
                PriceLevel = arCustomerHome.findByCstName(cstmr, AD_CMPNY).getCstDealPrice();
            	
            	System.out.println("customer: " + (String)criteria.get("customer"));
            	System.out.println("PriceLevel: " + PriceLevel);
            }
            
            
            // Allocate the size of the object parameter
            
            obj = new Object[criteriaSize];
            String loc =(String)criteria.get("location");
            if (criteria.containsKey("location")) {
        		if(loc.toString().equalsIgnoreCase("ALL"))
				{
					System.out.println("XD");
					obj[ctr] = (String)criteria.get("location");
					ctr++;
				}
        		else{
            	
                if (!firstArgument) {
                    
                    jbossQl.append("AND ");	
                    
                } else {
                    
                    firstArgument = false;
                    jbossQl.append("WHERE ");
                    
                }
                
                jbossQl.append("il.invLocation.locName=?" + (ctr+1) + " ");
                obj[ctr] = (String)criteria.get("location");
                ctr++;
        		}
            }
            
            if (criteria.containsKey("itemCategory")) {
                
                if (!firstArgument) {		       	  	
                    jbossQl.append("AND ");		       	     
                } else {		       	  	
                    firstArgument = false;
                    jbossQl.append("WHERE ");		       	  	 
                }
                
                jbossQl.append("il.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
                obj[ctr] = (String)criteria.get("itemCategory");
                paramCategory = obj[ctr].toString();
                ctr++;
                
            }
            
            
			if (criteria.containsKey("itemClass")) {
			   	
			   	  if (!firstArgument) {		       	  	
			   	     jbossQl.append("AND ");		       	     
			   	  } else {		       	  	
			   	  	 firstArgument = false;
			   	  	 jbossQl.append("WHERE ");		       	  	 
			   	  }
			   	  
			   	  jbossQl.append("il.invItem.iiClass=?" + (ctr+1) + " ");
			   	  obj[ctr] = (String)criteria.get("itemClass");
			   	  ctr++;
			   	  
			}
				
            if (criteria.containsKey("branch")) {
                
                if (!firstArgument) {		       	  	
                    jbossQl.append("AND ");		       	     
                } else {		       	  	
                    firstArgument = false;
                    jbossQl.append("WHERE ");		       	  	 
                }
                
                jbossQl.append("bil.adBranch.brCode="+ AD_BRNCH + " ");
                
            }
            
            if (!firstArgument) {
                
                jbossQl.append("AND ");
                
            } else {
                
                firstArgument = false;
                jbossQl.append("WHERE ");
                
            }
            
            jbossQl.append("il.invItem.iiNonInventoriable=0 AND il.invItem.iiEnable=1 AND il.ilAdCompany=" + AD_CMPNY + " ORDER BY il.invItem.iiAdLvCategory, il.invItem.iiName");
            System.out.println("jbossQl.toString()="+jbossQl.toString());
            Collection invItemLocations = invItemLocationHome.getIlByCriteria(jbossQl.toString(), obj);	         
            
            if (invItemLocations.size() == 0)
                throw new GlobalNoRecordFoundException();
            
            Iterator i = invItemLocations.iterator();
            
            while (i.hasNext()) {
                
                LocalInvItemLocation invItemLocation = (LocalInvItemLocation)i.next();
                
                InvRepPhysicalInventoryWorksheetDetails mPiwDetails = new InvRepPhysicalInventoryWorksheetDetails();	
                
                if(INCLD_ENCDD){
                	
                	LocalInvPhysicalInventory invPhysicalInventory = null;
                	try{
                		if(paramCategory.length()==0){
                			invPhysicalInventory = invPhysicalInventoryHome.findByPiDateAndLocNameAndBrCode(PI_DT, 
                					invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                		}else{
                		         	invPhysicalInventory = invPhysicalInventoryHome.findByPiDateAndLocNameAndCategoryNameAndBrCode(PI_DT, 
                					invItemLocation.getInvLocation().getLocName(), paramCategory, AD_BRNCH, AD_CMPNY);
                		}


                		Iterator pilIter = invPhysicalInventory.getInvPhysicalInventoryLines().iterator();
                		while(pilIter.hasNext()){
                			LocalInvPhysicalInventoryLine invPhysicalInventoryLine = (LocalInvPhysicalInventoryLine)pilIter.next();
                			if(invPhysicalInventoryLine.getInvItemLocation().getIlCode() == invItemLocation.getIlCode()){
                				System.out.println("invPhysicalInventoryLine.getInvPhysicalInventory().getPiCode(): " + invPhysicalInventoryLine.getInvPhysicalInventory().getPiCode());
                				mPiwDetails.setPiwPilEndingInv(invPhysicalInventoryLine.getPilEndingInventory());
                				System.out.println("invPhysicalInventoryLine.getPilEndingInventory(): " + invPhysicalInventoryLine.getPilEndingInventory());
                				mPiwDetails.setPiwPilWastage(invPhysicalInventoryLine.getPilWastage());
                				System.out.println("invPhysicalInventoryLine.getPilWastage(): " + invPhysicalInventoryLine.getPilWastage());
                				mPiwDetails.setPiwPilVariance(invPhysicalInventoryLine.getPilVariance());
                				System.out.println("invPhysicalInventoryLine.getPilVariance(): " + invPhysicalInventoryLine.getPilVariance());
                				break;
                			}
                		}
                	}catch (Exception ex){

                	}
                    
                }
                
                try {
                    
                	 System.out.println("invItemLocation.getInvItem().getIiName(): " +invItemLocation.getInvItem().getIiName());
                	 System.out.println("invItemLocation.getInvLocation().getLocName(): " +invItemLocation.getInvLocation().getLocName());
                	 System.out.println("invItemLocation.getInvItem().getIiUnitCost(): "+invItemLocation.getInvItem().getIiUnitCost());
                	
                    LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(PI_DT, 
                            invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

                    mPiwDetails.setPiwPilActualQty(invCosting.getCstRemainingQuantity());

                    if(invCosting.getCstRemainingValue()==0){
                    	mPiwDetails.setPiwPilActualCost(invItemLocation.getInvItem().getIiUnitCost());
                    }else{
                    	mPiwDetails.setPiwPilActualCost(invCosting.getCstRemainingValue());
                    }
                    
                    System.out.println("invCosting.getCstRemainingQuantity(): " + invCosting.getCstRemainingQuantity());
                    System.out.println("invCosting.getCstRemainingValue(): " + invCosting.getCstRemainingValue());

                } catch (FinderException ex) {
                    mPiwDetails.setPiwPilActualQty(0d);
                    mPiwDetails.setPiwPilActualCost(invItemLocation.getInvItem().getIiUnitCost());
                    
                }	        	
                
                mPiwDetails.setPiwPilItemName(invItemLocation.getInvItem().getIiName());
                mPiwDetails.setPiwPilItemDescription(invItemLocation.getInvItem().getIiDescription());
                mPiwDetails.setPiwPilItemCategory(invItemLocation.getInvItem().getIiAdLvCategory());
                mPiwDetails.setPiwPilUnitMeasureName(invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName());
                
                
                
                if(INCLD_UNPSTD) {
					
					double unpostedQty = 0d;
					double unpostedCost = 0d;

					String locName = invItemLocation.getInvLocation().getLocName();
					Integer locCode = invLocationHome.findByLocName(locName, AD_CMPNY).getLocCode();
					
					//INV_ADJUSTMENT_LINE
					Collection invAdjustmentLines = invAdjustmentLineHome.findUnpostedAdjByLocNameAndAdBranch(locName, AD_BRNCH, AD_CMPNY);
					
					Iterator unpstdIter = invAdjustmentLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)unpstdIter.next();

						LocalInvItemLocation invItemLocationAdj = invAdjustmentLine.getInvItemLocation();
						LocalInvItem invItem = invItemLocationAdj.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem) || 
								!invItemLocationAdj.equals(invItemLocation))
							continue;
						unpostedQty += invAdjustmentLine.getAlAdjustQuantity();
						
						System.out.println("getInvCosting: "+invAdjustmentLine.getInvCosting());
						//System.out.println("getCstRemainingValue: "+invAdjustmentLine.getInvCosting().getCstRemainingValue());
						//System.out.println("getInvCosting: "+invAdjustmentLine.getInvCosting().getInvItemLocation().getInvItem().getIiName());
						if(invAdjustmentLine.getInvCosting()!=null){
							unpostedCost = invAdjustmentLine.getInvCosting().getCstRemainingValue();
						}else{
							unpostedCost = invAdjustmentLine.getInvItemLocation().getInvItem().getIiUnitCost();
						}
					}
					
					//INV_STOCK_ISSUANCE_LINE
					Collection invStockIssuanceLines = invStockIssuanceLineHome.findUnpostedSiByLocNameAndAdBranch(locName, AD_BRNCH, AD_CMPNY);
					
					unpstdIter = invStockIssuanceLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine)unpstdIter.next();

						LocalInvItemLocation invItemLocationSi = invStockIssuanceLine.getInvItemLocation();
						LocalInvItem invItem = invItemLocationSi.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem) || 
								!invItemLocationSi.equals(invItemLocation))
							continue;
						
						unpostedQty += invStockIssuanceLine.getSilIssueQuantity();
						if(invStockIssuanceLine.getInvCosting()!=null){
							
							unpostedCost = invStockIssuanceLine.getInvCosting().getCstRemainingValue();
						}else{
							unpostedCost = invStockIssuanceLine.getInvItemLocation().getInvItem().getIiUnitCost();
						}
						
					}
					
					//INV_ASSEMBLY_TRANSFER_LINE
					Collection invAssemblyTransferLines = invAssemblyTransferLineHome.findUnpostedAtrByLocNameAndAdBranch(locName, AD_BRNCH, AD_CMPNY);

					unpstdIter = invAssemblyTransferLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvAssemblyTransferLine invAssemblyTransferLine = (LocalInvAssemblyTransferLine)unpstdIter.next();

						LocalInvItemLocation invItemLocationAt = invAssemblyTransferLine.getInvBuildOrderLine().getInvItemLocation();
						LocalInvItem invItem = invItemLocationAt.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem) || 
								!invItemLocationAt.equals(invItemLocation))
							continue;
						
						unpostedQty += invAssemblyTransferLine.getAtlAssembleQuantity();
						
					}
					
					
					//INV_BUILD_UNBUILD_ASSEMBLY_LINE
					Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssemblyLineHome.findUnpostedBuaByLocNameAndAdBranch(locName, AD_BRNCH, AD_CMPNY);
					
					unpstdIter = invBuildUnbuildAssemblyLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)unpstdIter.next();

						LocalInvItemLocation invItemLocationBua = invBuildUnbuildAssemblyLine.getInvItemLocation();
						LocalInvItem invItem = invItemLocationBua.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem) || 
								!invItemLocationBua.equals(invItemLocation))
							continue;
						
						unpostedQty += invBuildUnbuildAssemblyLine.getBlBuildQuantity();
					}

					
					//INV_STOCK_TRANSFER_LINE
					Collection invStockTransferLines = invStockTransferLineHome.findUnpostedStByLocCodeAndAdBranch(locCode, AD_BRNCH, AD_CMPNY);
					
					unpstdIter = invStockTransferLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine)unpstdIter.next();

						LocalInvItem invItem = invStockTransferLine.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem))
							continue;
						
						unpostedQty += invStockTransferLine.getStlQuantityDelivered();
						
					}

					//INV_BRANCH_STOCK_TRANSFER_LINE
					Collection invBranchStockTransferLines = invBranchStockTransferLineHome.findUnpostedBstByLocNameAndAdBranch(locName, AD_BRNCH, AD_CMPNY);
					
					unpstdIter = invBranchStockTransferLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)unpstdIter.next();

						LocalInvItemLocation invItemLocationBst = invBranchStockTransferLine.getInvItemLocation();
						LocalInvItem invItem = invItemLocationBst.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem) || 
								!invItemLocationBst.equals(invItemLocation))
							continue;
						
						unpostedQty += invBranchStockTransferLine.getBslQuantityReceived();
					}

					//AP_VOUCHER_LINE_ITEM
					//a) apVoucher
					Collection apVoucherLineItems = apVoucherLineItemHome.findUnpostedVouByLocNameAndAdBranch(locName, AD_BRNCH, AD_CMPNY);

					unpstdIter = apVoucherLineItems.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)unpstdIter.next();

						LocalInvItemLocation invItemLocationVou = apVoucherLineItem.getInvItemLocation();
						LocalInvItem invItem = invItemLocationVou.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem) || 
								!invItemLocationVou.equals(invItemLocation))
							continue;
						
						unpostedQty += apVoucherLineItem.getVliQuantity();
					}

					//b) apCheck
					apVoucherLineItems = apVoucherLineItemHome.findUnpostedChkByLocNameAndAdBranch(locName, AD_BRNCH, AD_CMPNY);
					
					unpstdIter = apVoucherLineItems.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)unpstdIter.next();

						LocalInvItemLocation invItemLocationChk = apVoucherLineItem.getInvItemLocation();
						LocalInvItem invItem = invItemLocationChk.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem) || 
								!invItemLocationChk.equals(invItemLocation))
							continue;
						
						unpostedQty += apVoucherLineItem.getVliQuantity();
					}

					//AR_INVOICE_LINE_ITEM
					//a) arInvoice
					Collection arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedInvcByLocNameAndAdBranch(locName, AD_BRNCH, AD_CMPNY);
					
					unpstdIter = arInvoiceLineItems.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)unpstdIter.next();

						LocalInvItemLocation invItemLocationInv = arInvoiceLineItem.getInvItemLocation();
						LocalInvItem invItem = invItemLocationInv.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem) || 
								!invItemLocationInv.equals(invItemLocation))
							continue;
						
						unpostedQty -= arInvoiceLineItem.getIliQuantity();
					}

					//b) arReceipt
					arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedRctByLocNameAndAdBranch(locName, AD_BRNCH, AD_CMPNY);
					
					unpstdIter = arInvoiceLineItems.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)unpstdIter.next();

						LocalInvItemLocation invItemLocationRct = arInvoiceLineItem.getInvItemLocation();
						LocalInvItem invItem = invItemLocationRct.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem) || 
								!invItemLocationRct.equals(invItemLocation))
							continue;
						
						unpostedQty -= arInvoiceLineItem.getIliQuantity();
						
					}

					
					//AP_PURCHASE_ORDER_LINE
					Collection apPurchaseOrderLines = apPurchaseOrderLineHome.findUnpostedPoByLocNameAndAdBranch(locName, AD_BRNCH, AD_CMPNY);
					
					unpstdIter = apPurchaseOrderLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)unpstdIter.next();

						LocalInvItemLocation invItemLocationPo = apPurchaseOrderLine.getInvItemLocation();
						LocalInvItem invItem = invItemLocationPo.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem) || 
								!invItemLocationPo.equals(invItemLocation))
							continue;
						
						unpostedQty += apPurchaseOrderLine.getPlQuantity();
						if(apPurchaseOrderLine.getInvCosting()!=null){	
							unpostedCost = apPurchaseOrderLine.getInvCosting().getCstRemainingValue();
						}else{
							unpostedCost = apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiUnitCost();
						}
						
					}

					//AR_SALES_ORDER_INVOICE_LINE
					Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome.findUnpostedSoInvByLocNameAndAdBranch(locName, AD_BRNCH, AD_CMPNY);

					unpstdIter = arSalesOrderInvoiceLines.iterator();
					
					while(unpstdIter.hasNext()) {
						
						LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)unpstdIter.next();

						LocalInvItemLocation invItemLocationSo = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation();
						LocalInvItem invItem = invItemLocationSo.getInvItem();
						
						if(!this.filterByOptionalCriteria(criteria, invItem) || 
								!invItemLocationSo.equals(invItemLocation))
							continue;
						
						unpostedQty -= arSalesOrderInvoiceLine.getSilQuantityDelivered();
						if(arSalesOrderInvoiceLine.getInvCosting()!=null){
							
							unpostedCost = arSalesOrderInvoiceLine.getInvCosting().getCstRemainingValue();
						}else{
							unpostedCost = arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiUnitCost();
						}
						
					}
					
					mPiwDetails.setPiwPilActualQty(mPiwDetails.getPiwPilActualQty() + unpostedQty);
					mPiwDetails.setPiwPilActualCost(unpostedCost);
					
				}
                
                if(INCLD_INTRNST) {
                	System.out.println("INCLD_INTRNST: " + invItemLocation.getInvItem().getIiName());
                	double actQty = mPiwDetails.getPiwPilActualQty();
                	byte pstd = 1;
                	try {
                		
                		Object newObj[] = null;
                		newObj = new Object[4];
                		
                		newObj[0] = pstd;
                		newObj[1] = invItemLocation.getIlCode();
                		newObj[2] = AD_BRNCH;
                		newObj[3] = AD_CMPNY;
                		
                		//INV_BRANCH_STOCK_TRANSFER_LINE
    					Collection invBranchStockTransferLines = invBranchStockTransferLineHome.getBstlByCriteria("SELECT OBJECT(bsl) FROM InvBranchStockTransferLine bsl WHERE bsl.invBranchStockTransfer.bstType = 'OUT' AND bsl.invBranchStockTransfer.bstPosted=?1 AND bsl.invItemLocation.ilCode=?2 AND bsl.invBranchStockTransfer.adBranch.brCode=?3 AND bsl.bslAdCompany=?4", newObj);
                		
    					Iterator pstdIter = invBranchStockTransferLines.iterator();
    					
    					while(pstdIter.hasNext()) {
    						System.out.println("actQty: "+actQty);
    						LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)pstdIter.next();
    						double bstQty = invBranchStockTransferLine.getBslQuantity() - invBranchStockTransferLine.getBslQuantityReceived();
    						System.out.println("bstQty: "+bstQty);
    						mPiwDetails.setPiwPilActualQty(actQty + bstQty);
    						System.out.println("pilActQty: "+mPiwDetails.getPiwPilActualQty());
    					}

                	} catch (FinderException ex) {
                		
                		mPiwDetails.setPiwPilActualQty(actQty);
                		System.out.println("pilActQty2: "+mPiwDetails.getPiwPilActualQty());

                	}	        	
                }
                
                if(!PriceLevel.equals("")){	
                	double slsPrc = invPriceLevelHome.findByIiNameAndAdLvPriceLevel(mPiwDetails.getPiwPilItemName(), PriceLevel, AD_CMPNY).getPlAmount();
                	mPiwDetails.setPiwPilSalesPrice(slsPrc);
                	System.out.println("slsPrc: " + slsPrc);
                }else{
                	mPiwDetails.setPiwPilSalesPrice(invItemLocation.getInvItem().getIiSalesPrice());
                	System.out.println("getIiSalesPrice");
                }
                
                list.add(mPiwDetails);
                
            }
            
            return list;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            throw ex;
            
        } catch (Exception ex) {
            
            
            ex.printStackTrace();
            throw new EJBException(ex.getMessage());
            
        }
        
    } 
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
        
        Debug.print("InvRepPhysicalInventoryWorksheetControllerBean getAdCompany");      
        
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            AdCompanyDetails details = new AdCompanyDetails();
            details.setCmpName(adCompany.getCmpName());
            details.setCmpAddress(adCompany.getCmpAddress());
            details.setCmpCity(adCompany.getCmpCity());
            details.setCmpCountry(adCompany.getCmpCountry());
            details.setCmpPhone(adCompany.getCmpPhone());
            
            return details;  	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }    
    
    private Integer getAdBrCodeByBrName(String BR_NM, Integer AD_CMPNY) {
        
        Debug.print("InvRepPhysicalInventoryWorksheetControllerBean getAdBrCodeByBrName");
        
        LocalAdBranchHome adBranchHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_NM, AD_CMPNY);            
            
            return adBranch.getBrCode();
                        
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private boolean filterByOptionalCriteria(HashMap criteria, LocalInvItem invItem) {
   	   
   	   Debug.print("InvRepItemCostingControllerBean filterByOptionalCriteria");
   	   
   	   try {
   		   
   		   String itemClass = "";
   		   String itemCategory = "";
   	
   		   if (criteria.containsKey("itemClass"))
   			   itemClass = (String)criteria.get("itemClass");
   		   
   		   if (criteria.containsKey("category"))
   			   itemCategory = (String)criteria.get("category");
   		   
   		   if(!itemClass.equals("") && !invItem.getIiClass().equals(itemClass))
   			   return false;
   		   
   		   if(!itemCategory.equals("") && !invItem.getIiAdLvCategory().equals(itemCategory))
   			   return false;
   		   
   		   
   		   return true;
   	   
   	   } catch (Exception ex) {
   	
   		   Debug.printStackTrace(ex);
   		   getSessionContext().setRollbackOnly();
   		   throw new EJBException(ex.getMessage());
   	   }
      }

    
    // SessionBean methods
    
    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {
        
        Debug.print("InvPhysicalInventoryWorksheetControllerBean ejbCreate");
        
    }
}
