package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepAgingDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepReorderItemsDetails;

/**
* @ejb:bean name="InvRepReorderItemsControllerEJB"
*           display-name="Used for generation of inventory reorder items reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/InvRepReorderItemsControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.InvRepReorderItemsController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.InvRepReorderItemsControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class InvRepReorderItemsControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepReorderItemsControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepReorderItemsControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeInvRepReorderItems(HashMap criteria, String ORDER_BY, String GROUP_BY, String type, ArrayList branchList, Integer AD_BRNCH,  Integer AD_CMPNY)
		throws GlobalNoRecordFoundException,
		GlobalInvItemLocationNotFoundException {

		Debug.print("InvRepReorderItemsControllerBean executeInvRepReorderItems");

		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		LocalInvCostingHome invReorderItemCostHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;

		ArrayList invReorderItemsList = new ArrayList();

		// Initialize EJB Home

		try {

			invReorderItemCostHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class); 

		} catch (NamingException ex) {

			throw new EJBException(ex.getMessage());

		}

		try { 

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);

			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(bil) FROM AdBranchItemLocation bil ");

			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      

			Object obj[];	      

			// Allocate the size of the object parameter

			if (criteria.containsKey("itemName")) {

				criteriaSize--;

			}
			
			if (criteria.containsKey("supplier")) {

				criteriaSize--;

			}

			obj = new Object[criteriaSize];

			if (branchList.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			} else {

				jbossQl.append("WHERE bil.adBranch.brCode in (");

				boolean firstLoop = true;

				Iterator j = branchList.iterator();

				while(j.hasNext()) {

					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}

					AdBranchDetails mdetails = (AdBranchDetails) j.next();

					jbossQl.append(mdetails.getBrCode());

				}

				jbossQl.append(") ");

				firstArgument = false;

			}                    


			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bil.invItemLocation.invItem.iiName  LIKE '%" + (String)criteria.get("itemName") + "%' ");

			}
			
			if (criteria.containsKey("supplier")) {
				
				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}
				System.out.println("supplier: " + (String)criteria.get("supplier") );
				jbossQl.append("bil.invItemLocation.invItem.apSupplier.splSupplierCode LIKE '%" + (String)criteria.get("supplier") + "%' ");
			}

			if (criteria.containsKey("itemClass")) {

				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bil.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemClass");
				ctr++;

			}

			if (criteria.containsKey("itemCategory")) {

				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bil.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemCategory");
				ctr++;

			}				

			if (criteria.containsKey("location")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bil.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;

			}	

			if (criteria.containsKey("locationType")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {		       	  

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bil.invItemLocation.invLocation.locLvType=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("locationType");
				ctr++;

			}

			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			if (type.equals("ALL") || type.equals("NO ACTIVITY")) {

				jbossQl.append("bil.bilAdCompany=" + AD_CMPNY + " ");
				
			} if (type.equals("SELLING ITEMS")) {

				jbossQl.append("bil.invItemLocation.invItem.iiRetailUom IS NOT NULL AND bil.bilAdCompany=" + AD_CMPNY + " ");
				
			} if (type.equals("NON-SELLING ITEMS")) {
				
				jbossQl.append("bil.invItemLocation.invItem.iiRetailUom IS NULL AND bil.bilAdCompany=" + AD_CMPNY + " ");
				
			} else {
				
				jbossQl.append("AND bil.bilAdCompany=" + AD_CMPNY + " ");
				
			}

			String orderBy = null;

			if (ORDER_BY.equals("ITEM NAME")) {

				orderBy = "bil.invItemLocation.invItem.iiName";

			} else if (ORDER_BY.equals("ITEM DESCRIPTION")) {

				orderBy = "bil.invItemLocation.invItem.iiDescription";

			} else if (ORDER_BY.equals("ITEM CLASS")) {

				orderBy = "bil.invItemLocation.invItem.iiClass";

			} 

			if (orderBy != null) {

				jbossQl.append("ORDER BY " + orderBy);

			}

			Collection adBranchItemLocations = null;

			try {

				adBranchItemLocations = adBranchItemLocationHome.getBilByCriteria(jbossQl.toString(), obj);

			} catch (FinderException ex) {

				throw new GlobalNoRecordFoundException();

			}

			if (adBranchItemLocations == null) {
				
				throw new GlobalNoRecordFoundException();

			}

			Iterator i = adBranchItemLocations.iterator();

			while (i.hasNext()) {

				LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation) i.next();

				InvRepReorderItemsDetails details = new InvRepReorderItemsDetails();

				if(type.equals("NO ACTIVITY")) {

					// type = "NO ACTIVITY" -> no PO

					Collection apPurchaseOrderLines = 
						apPurchaseOrderLineHome.findByPlIlCodeAndPoReceivingAndBrCode(adBranchItemLocation.getInvItemLocation().getIlCode(), EJBCommon.FALSE, 
								adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);

					if(apPurchaseOrderLines.isEmpty()) {

						details.setRiItemName(adBranchItemLocation.getInvItemLocation().getInvItem().getIiName());
						details.setRiItemDescription(adBranchItemLocation.getInvItemLocation().getInvItem().getIiDescription());
						details.setRiItemClass(adBranchItemLocation.getInvItemLocation().getInvItem().getIiClass());
						details.setRiLocation(adBranchItemLocation.getAdBranch().getBrBranchCode() + "-" + adBranchItemLocation.getInvItemLocation().getInvLocation().getLocName());
						details.setRiUomName(adBranchItemLocation.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
						details.setRiReorderPoint(adBranchItemLocation.getBilReorderPoint());
						details.setRiBranch(adBranchItemLocation.getAdBranch().getBrBranchCode());
						
						try {

							LocalInvCosting invReorderItemCost = 
								invReorderItemCostHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(
										adBranchItemLocation.getInvItemLocation().getInvItem().getIiName(), adBranchItemLocation.getInvItemLocation().getInvLocation().getLocName(), adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);

							details.setRiQuantity(invReorderItemCost.getCstRemainingQuantity());
							System.out.print("invReorderItemCost.getCstRemainingQuantity():" +invReorderItemCost.getCstRemainingQuantity());

						} catch (FinderException ex){

							details.setRiQuantity(0);

						}						
						
						details.setRiReorderQuantity(adBranchItemLocation.getBilReorderQuantity());						
						details.setOrderBy(ORDER_BY);
						try{
							details.setRiSupplierCode(adBranchItemLocation.getInvItemLocation().getInvItem().getApSupplier().getSplSupplierCode());
						}catch(Exception ex){}
						
						invReorderItemsList.add(details);

					}

				} else {

					// type = "ALL" or "SELLING ITEMS" or "NON-SELLING ITEMS"

					details.setRiItemName(adBranchItemLocation.getInvItemLocation().getInvItem().getIiName());
					details.setRiItemDescription(adBranchItemLocation.getInvItemLocation().getInvItem().getIiDescription());
					details.setRiItemClass(adBranchItemLocation.getInvItemLocation().getInvItem().getIiClass());
					details.setRiLocation(adBranchItemLocation.getAdBranch().getBrBranchCode() + "-" + adBranchItemLocation.getInvItemLocation().getInvLocation().getLocName());
					details.setRiUomName(adBranchItemLocation.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
					details.setRiReorderPoint(adBranchItemLocation.getBilReorderPoint());
					details.setRiBranch(adBranchItemLocation.getAdBranch().getBrBranchCode());
					try {

						LocalInvCosting invReorderItemCost = 
							invReorderItemCostHome.getByMaxCstDateToLongAndMaxCstLineNumberAndIiNameAndLocName(
									adBranchItemLocation.getInvItemLocation().getInvItem().getIiName(), adBranchItemLocation.getInvItemLocation().getInvLocation().getLocName(), adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);

						details.setRiQuantity(invReorderItemCost.getCstRemainingQuantity());
						System.out.print("invReorderItemCost.getCstRemainingQuantity():" +invReorderItemCost.getCstRemainingQuantity());

					} catch (FinderException ex){

						details.setRiQuantity(0);

					}
										
					details.setRiReorderQuantity(adBranchItemLocation.getBilReorderQuantity());					
					details.setOrderBy(ORDER_BY);					
					try{
						details.setRiSupplierCode(adBranchItemLocation.getInvItemLocation().getInvItem().getApSupplier().getSplSupplierCode());
					}catch(Exception ex){}
											
					invReorderItemsList.add(details);					

				}

			}
			
			if (invReorderItemsList.size() == 0) {

				throw new GlobalNoRecordFoundException();

			}
			
			if(GROUP_BY.equals("ITEM NAME")) {

				Collections.sort(invReorderItemsList, InvRepReorderItemsDetails.ItemNameComparator);

			} else {
				
				Collections.sort(invReorderItemsList, InvRepReorderItemsDetails.NoClassComparator);

			}

			return invReorderItemsList;	   

		} catch (GlobalNoRecordFoundException ex) {

			throw ex;

		} catch (Exception ex) {

			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());

		}
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvLocationTypeAll(Integer AD_CMPNY) {
		
		Debug.print("InvStockOnHandControllerBean getAdLvInvLocationTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV LOCATION TYPE", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}		

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("InvRepItemCostingControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepReorderItemsControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("InvFindItemLocationControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }

	// SessionBean methods
	
	/**
* @ejb:create-method view-type="remote"
**/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvRepReorderItemsControllerBean ejbCreate");
		
	}
	
}

