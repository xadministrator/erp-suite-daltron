package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.genfld.LocalGenValueSetValue;
import com.ejb.genfld.LocalGenValueSetValueHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvRepStockCardDetails;

/**
* @ejb:bean name="InvRepStockCardControllerEJB"
*           display-name="Used for generation of stock card report"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/InvRepStockCardControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.InvRepStockCardController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.InvRepStockCardControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class InvRepStockCardControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepStockCardControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepStockCardControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeInvRepStockCard(HashMap criteria,  ArrayList branchList, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvRepStockCardControllerBean executeInvRepStockCard");
		
		LocalInvItemLocationHome invItemLocationHome = null;
		LocalInvCostingHome invCostingHome = null;
		 LocalGenValueSetValueHome gvsv=null;
	
		LocalAdCompanyHome adCompanyHome = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			  gvsv = (LocalGenValueSetValueHome)EJBHomeFactory.
	          	lookUpLocalHome(LocalGenValueSetValueHome.JNDI_NAME, LocalGenValueSetValue.class);

			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(cst) FROM InvCosting cst ");
			
			boolean firstArgument = true;
			short ctr = 0;	      
			int criteriaSize = 0;
			
			Object obj[] = null;
			
			if (branchList.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			else {
				
				jbossQl.append(" WHERE cst.cstAdBranch in (");
				
				boolean firstLoop = true;
				
				Iterator j = branchList.iterator();
				
				while(j.hasNext()) {
					
					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}
					
					AdBranchDetails mdetails = (AdBranchDetails) j.next();
					
					jbossQl.append(mdetails.getBrCode());
					
				}
				
				jbossQl.append(") ");
				
				firstArgument = false;
				
			}                    
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("category")) {
				
				criteriaSize++;
				
			}
			
			if (criteria.containsKey("location")) {
				
				criteriaSize++;
				
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				criteriaSize++;
				
			}
			
			if (criteria.containsKey("dateTo")) {
				
				criteriaSize++;
				
			}
			
			if (criteria.containsKey("itemClass")) {
				
				criteriaSize++;
				
			}
			
			obj = new Object[criteriaSize];
			
			
			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.invItemLocation.invItem.iiName  >= '" + (String)criteria.get("itemName") + "' ");

			}	
			
			if (criteria.containsKey("itemNameTo")) {
					System.out.println("itemNameTo");
				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("cst.invItemLocation.invItem.iiName <= '" + (String)criteria.get("itemNameTo") + "' ");

			}
			
			if (criteria.containsKey("category")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("cst.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;
				
			}	
			
			String loc =(String)criteria.get("location");
			
			if (criteria.containsKey("location")) {
				if(loc.toString().equalsIgnoreCase("ALL"))
				{
					System.out.println("XD");
					obj[ctr] = (String)criteria.get("location");
					ctr++;
				}
				else{
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("cst.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;
				System.out.println("LOC_____*__:"+(String)criteria.get("location"));
				}
			}
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("cst.cstDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("cst.cstDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
			
			if (criteria.containsKey("itemClass")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("cst.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemClass");
				ctr++;
				
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("cst.invItemLocation.invItem.iiNonInventoriable=0 AND cst.invItemLocation.invItem.iiEnable=1 AND cst.cstAdCompany=" + AD_CMPNY + " ");
			
			jbossQl.append("ORDER BY cst.invItemLocation.invItem.iiName, cst.cstDate, cst.cstDateToLong, cst.cstLineNumber");
			
			Collection invCostings = invCostingHome.getCstByCriteria(jbossQl.toString(), obj);
			
			Iterator i = invCostings.iterator();	
			
			while (i.hasNext()) {
				
				LocalInvCosting invCosting = (LocalInvCosting) i.next();
				
				InvRepStockCardDetails details = new InvRepStockCardDetails();
				
				// beginning
				
				LocalInvCosting invBeginningCosting = this.getCstIlBeginningBalanceByItemLocationAndDate(invCosting.getInvItemLocation(), 
						(Date)criteria.get("dateFrom"), invCosting.getCstAdBranch(), AD_CMPNY);
				if(invBeginningCosting != null)
					details.setRscBeginningQuantity(invBeginningCosting.getCstRemainingQuantity());
				else
					details.setRscBeginningQuantity(0);
				
				details.setRscItemName(invCosting.getInvItemLocation().getInvItem().getIiName() + "-" + invCosting.getInvItemLocation().getInvItem().getIiDescription());								
				details.setRscUnit(invCosting.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
				details.setRscDate(invCosting.getCstDate());
                               
				
				if (invCosting.getInvAdjustmentLine() != null) {
					
					details.setRscAccount(invCosting.getInvAdjustmentLine().getInvAdjustment().getGlChartOfAccount().getCoaAccountDescription());
					LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
					if(adCompany.getCmpShortName().toString().equals("LBP"))
					{
						Collection BrList = gvsv.findByVsName("BRANCH", AD_CMPNY);

				 		String sample = invCosting.getInvAdjustmentLine().getInvAdjustment().getGlChartOfAccount().getCoaAccountDescription();
				 		
				 		 Iterator ic = BrList.iterator();
				 		while (ic.hasNext()) {
				 			LocalGenValueSetValue glvsv = (LocalGenValueSetValue)ic.next();
				 			if(sample.contains(glvsv.getVsvDescription()+"-"))
				 					{
				 				System.out.println(glvsv.getVsvDescription());
				 				sample=glvsv.getVsvDescription();
				 					break;
				 					
				 					}
				 		}
				 		details.setRscAccount(sample);
					}
				}
				
				if(invCosting.getInvAdjustmentLine() != null) {
					
					details.setRscDocumentNumber(invCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber());
					details.setRscReferenceNumber(invCosting.getInvAdjustmentLine().getInvAdjustment().getAdjReferenceNumber());
					details.setRscSource("INV");
					
				} else if(invCosting.getInvStockIssuanceLine() != null) {
					
					details.setRscDocumentNumber(invCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber());
					details.setRscReferenceNumber(invCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiReferenceNumber());
					details.setRscSource("INV");
					
				} else if(invCosting.getInvAssemblyTransferLine() != null) {
					
					details.setRscDocumentNumber(invCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber());
					details.setRscReferenceNumber(invCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrReferenceNumber());
					details.setRscSource("INV");
					
				} else if(invCosting.getInvBuildUnbuildAssemblyLine() != null) {
					
					details.setRscDocumentNumber(invCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber());
					details.setRscReferenceNumber(invCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaReferenceNumber());
					details.setRscSource("INV");
					
				} else if(invCosting.getInvStockTransferLine() != null) {
					
					details.setRscDocumentNumber(invCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber());
					details.setRscReferenceNumber(invCosting.getInvStockTransferLine().getInvStockTransfer().getStReferenceNumber());
					details.setRscSource("INV");
                                        
					
				} else if(invCosting.getInvBranchStockTransferLine() != null) {
					
					details.setRscDocumentNumber(invCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber());
					details.setRscReferenceNumber(invCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber());
					details.setRscSource("INV");
                                        
					
				} else if(invCosting.getApVoucherLineItem() != null) {
					
					if (invCosting.getApVoucherLineItem().getApVoucher() != null) {
						
						details.setRscDocumentNumber(invCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber());
						details.setRscReferenceNumber(invCosting.getApVoucherLineItem().getApVoucher().getVouReferenceNumber());
						details.setRscSource("AP");
						
					} else if (invCosting.getApVoucherLineItem().getApCheck() != null) {
						
						details.setRscDocumentNumber(invCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber());
						details.setRscReferenceNumber(invCosting.getApVoucherLineItem().getApCheck().getChkNumber());
						details.setRscSource("AP");
						
					}
					
				} else if(invCosting.getArInvoiceLineItem() != null) {
					
					if(invCosting.getArInvoiceLineItem().getArInvoice() != null) {
						
						details.setRscDocumentNumber(invCosting.getArInvoiceLineItem().getArInvoice().getInvNumber());
						details.setRscReferenceNumber(invCosting.getArInvoiceLineItem().getArInvoice().getInvReferenceNumber());
						details.setRscSource("AR");
						
					} else if (invCosting.getArInvoiceLineItem().getArReceipt() != null) {
						
						details.setRscDocumentNumber(invCosting.getArInvoiceLineItem().getArReceipt().getRctNumber());
						details.setRscReferenceNumber(invCosting.getArInvoiceLineItem().getArReceipt().getRctReferenceNumber());
						details.setRscSource("AR");
						
					}
					
				} else if(invCosting.getApPurchaseOrderLine() != null) {
					
					details.setRscDocumentNumber(invCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber());
					details.setRscReferenceNumber(invCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoReferenceNumber());
					details.setRscSource("AP");
					
				} else if(invCosting.getArSalesOrderInvoiceLine() != null) {
					
					details.setRscDocumentNumber(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber());
					details.setRscReferenceNumber(invCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvReferenceNumber());
					details.setRscSource("AR");
					
				}
				details.setRscLocation(invCosting.getInvItemLocation().getInvLocation().getLocDescription());      
				details.setRscInQuantity(0d);
				details.setRscOutQuantity(0d);
				
				if (invCosting.getCstQuantityReceived() != 0d) {
					
					if (invCosting.getCstQuantityReceived() > 0 ) {
						
						details.setRscInQuantity(invCosting.getCstQuantityReceived());
						
					}else if (invCosting.getCstQuantityReceived() < 0) {
						
						details.setRscOutQuantity(invCosting.getCstQuantityReceived() * -1);
						
					}
					
				} else if (invCosting.getCstAdjustQuantity() != 0d ) {
					
					if (invCosting.getCstAdjustQuantity()  > 0 ) {
						
						details.setRscInQuantity(invCosting.getCstAdjustQuantity());
						
					}else if (invCosting.getCstAdjustQuantity()  < 0 ) {
						
						details.setRscOutQuantity(invCosting.getCstAdjustQuantity() * -1);
					}
					
				} else if (invCosting.getCstAssemblyQuantity() != 0d) {
					
					if (invCosting.getCstAssemblyQuantity()  > 0) {
						
						details.setRscInQuantity(invCosting.getCstAssemblyQuantity());
						
					}else if (invCosting.getCstAssemblyQuantity()  < 0) {
						
						details.setRscOutQuantity(invCosting.getCstAssemblyQuantity() * -1);
					}
					
				} else if (invCosting.getCstQuantitySold() != 0d) {
					
					if (invCosting.getCstQuantitySold()  > 0) {
						
						details.setRscOutQuantity(invCosting.getCstQuantitySold());
						
					}else if (invCosting.getCstQuantitySold()  < 0) {
						
						details.setRscInQuantity(invCosting.getCstQuantitySold() * -1);
						
					}
					
				}
				
				details.setRscRemainingQuantity(invCosting.getCstRemainingQuantity());
				
				list.add(details);
				
			}
			System.out.println(jbossQl.toString());
			if (list.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			}		 
			
			return list; 	  
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("InvRepStockCardControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}			

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("InvFindItemLocationControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
   public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
       
       Debug.print("InvRepStockCardControllerBean getAdCompany");      
       
       LocalAdCompanyHome adCompanyHome = null;
       
       // Initialize EJB Home
       
       try {
           
           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
           
       } catch (NamingException ex) {
           
           throw new EJBException(ex.getMessage());
           
       }
       
       try {
           
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

           AdCompanyDetails details = new AdCompanyDetails();
           details.setCmpName(adCompany.getCmpName());

           return details;  	

       } catch (Exception ex) {

    	   Debug.printStackTrace(ex);
    	   throw new EJBException(ex.getMessage());

       }

   } 


   // private method

   private LocalInvCosting getCstIlBeginningBalanceByItemLocationAndDate(LocalInvItemLocation invItemLocation, Date date, Integer AD_BRNCH, Integer AD_CMPNY) {

	   Debug.print("InvRepStockCardControllerBean getCstIlBeginningBalanceByItemLocationAndDate");

	   LocalInvCostingHome invCostingHome = null;
	   LocalInvCosting invCosting = null;


	   // Initialize EJB Home

	   try {

		   invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
		   lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);

	   } catch (NamingException ex) {

		   throw new EJBException(ex.getMessage());

	   }
	   try {

		   GregorianCalendar calendar = new GregorianCalendar();

		   if(date != null) {

			   calendar.setTime(date);
			   calendar.add(GregorianCalendar.DATE, -1);

			   Date CST_DT = calendar.getTime();

			   try {
				   
				   invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
						   CST_DT, invItemLocation.getInvItem().getIiName(), 
						   invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

			   } catch (FinderException ex) {
				   
			   }
			   
			   System.out.println("Date1 :" + date);
			   System.out.println("Date2 :" + CST_DT);

		   } 

		   return invCosting;

	   } catch (Exception ex) {

		   Debug.printStackTrace(ex);
		   getSessionContext().setRollbackOnly();
		   throw new EJBException(ex.getMessage());

	   }


   }


    
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvRepStockCardControllerBean ejbCreate");
		
	}
	
}

