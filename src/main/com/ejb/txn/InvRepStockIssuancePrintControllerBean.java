/*
 * InvRepStockIssuancePrintControllerBean.java
 *
 * Created on April 04, 2005, 3:07 PM
 *
 * @author  Marvis Fernando C. Salvador
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvStockIssuance;
import com.ejb.inv.LocalInvStockIssuanceHome;
import com.ejb.inv.LocalInvStockIssuanceLine;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvRepStockIssuancePrintDetails;

/**
 * @ejb:bean name="InvRepStockIssuancePrintControllerEJB"
 *           display-name="Used for printing stock issuance transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepStockIssuancePrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepStockIssuancePrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepStockIssuancePrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class InvRepStockIssuancePrintControllerBean extends AbstractSessionBean {

	/**
     * @ejb:interface-method view-type="remote"
     **/
	public ArrayList executeInvStockIssuancePrint(ArrayList siCodeList, Integer AD_CMPNY)
		throws GlobalNoRecordFoundException{
		
		Debug.print("InvRepStockIssuancePrintControllerBean executeInvStockIssuancePrint");
		
		LocalInvStockIssuanceHome invStockIssuanceHome = null;        

        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
        	invStockIssuanceHome = (LocalInvStockIssuanceHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvStockIssuanceHome.JNDI_NAME, LocalInvStockIssuanceHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = siCodeList.iterator();
        	
        	while (i.hasNext()) {
        		
        		Integer INV_CODE = (Integer) i.next();
        		
        		LocalInvStockIssuance invStockIssuance = null;

        		try {
        			
        			invStockIssuance = invStockIssuanceHome.findByPrimaryKey(INV_CODE);
        			
        		} catch (FinderException ex) {
        			
        			continue;
        			
        		}	        	
        		
        		// get stock issuance lines
        		
        		Collection invStockIssuanceLines = invStockIssuance.getInvStockIssuanceLines();        	            
        		
        		Iterator siIter = invStockIssuanceLines.iterator();
        		
        		while (siIter.hasNext()) {
        			
        			LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine)siIter.next();
        			
        			InvRepStockIssuancePrintDetails details = new InvRepStockIssuancePrintDetails();
        			
        			details.setSipSiApprovedRejected(invStockIssuance.getSiApprovedRejectedBy());
        			details.setSipSiCreatedBy(invStockIssuance.getSiCreatedBy());
        			details.setSipSiDate(invStockIssuance.getSiDate());
        			details.setSipSiDocumentNumbert(invStockIssuance.getSiDocumentNumber());
        			details.setSipSiReferenceNumber(invStockIssuance.getSiReferenceNumber());
        			details.setSipSiDescription(invStockIssuance.getSiDescription());
        			details.setSilIlIiName(invStockIssuanceLine.getInvItemLocation().getInvItem().getIiName());
        			details.setSilIlIiDescription(invStockIssuanceLine.getInvItemLocation().getInvItem().getIiDescription());
        			details.setSilIlLocationName(invStockIssuanceLine.getInvItemLocation().getInvLocation().getLocName());
        			details.setSilIlIiUomName(invStockIssuanceLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
        			details.setSilBosQuantityRequired(invStockIssuanceLine.getInvBuildOrderStock().getBosQuantityRequired());
        			details.setSilIssueQuantity(invStockIssuanceLine.getSilIssueQuantity());
        			details.setSilUnitCost(invStockIssuanceLine.getSilUnitCost());
        			
        			list.add(details);
        			
        		}
        		
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}        	       	
        	
        	return list;    
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
	
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepStockIssuancePrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvRepStockIssuancePrintControllerBean ejbCreate");
      
    }


}