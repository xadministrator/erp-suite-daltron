package com.ejb.txn;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApPurchaseOrderLine;
import com.ejb.ap.LocalApPurchaseOrderLineHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ar.LocalArInvoiceLineItem;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.ar.LocalArJobOrderInvoiceLine;
import com.ejb.ar.LocalArJobOrderInvoiceLineHome;
import com.ejb.ar.LocalArJobOrderLine;
import com.ejb.ar.LocalArJobOrderLineHome;
import com.ejb.ar.LocalArSalesOrderInvoiceLine;
import com.ejb.ar.LocalArSalesOrderInvoiceLineHome;
import com.ejb.ar.LocalArSalesOrderLine;
import com.ejb.ar.LocalArSalesOrderLineHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLine;
import com.ejb.inv.LocalInvBuildUnbuildAssemblyLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvStockIssuanceLine;
import com.ejb.inv.LocalInvStockIssuanceLineHome;
import com.ejb.inv.LocalInvStockTransferLine;
import com.ejb.inv.LocalInvStockTransferLineHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.ArRepStatementDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepItemLedgerDetails;
import com.util.InvRepStockOnHandDetails;

/**
* @ejb:bean name="InvRepStockOnHandControllerEJB"
*           display-name="Used for generation of inventory stock on hand reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/InvRepStockOnHandControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.InvRepStockOnHandController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.InvRepStockOnHandControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class InvRepStockOnHandControllerBean extends AbstractSessionBean {
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvReportTypeAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepStockOnHandControllerBean getAdLvReportTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV REPORT TYPE - STOCK ON HAND", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvSortByAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepStockOnHandControllerBean getAdLvSortByAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV SORT BY - STOCK ON HAND", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepStockOnHandControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepStockOnHandControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeInvRepStockOnHand(HashMap criteria, boolean INCLD_ZRS, String ORDER_BY, 
			boolean SHW_CMMTTD_QNTTY,boolean INCLD_UNPSTD, boolean INCLD_FRCST,boolean LAYERED, Date AS_OF_DT, 
			String UOM_NM, boolean fixCosting, Integer AD_BRNCH, ArrayList branchList, Integer AD_CMPNY,String rpt)
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvRepStockOnHandControllerBean executeInvRepStockOnHand");
		
		LocalAdBranchItemLocationHome invStockOnHandHome = null;
		LocalInvCostingHome invCostingHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalArSalesOrderLineHome arSalesOrderLineHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null;
		LocalArJobOrderLineHome arJobOrderLineHome = null;
		LocalArJobOrderInvoiceLineHome arJobOrderInvoiceLineHome = null;
		
		 InvRepItemCostingControllerHome homeRIC = null;
        InvRepItemCostingController ejbRIC = null;
		 
		LocalInvItemHome invItemHome = null;
		
		ArrayList invStockOnHandList = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invStockOnHandHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			arSalesOrderLineHome = (LocalArSalesOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderLineHome.JNDI_NAME, LocalArSalesOrderLineHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			arJobOrderLineHome = (LocalArJobOrderLineHome)EJBHomeFactory.
					lookUpLocalHome(LocalArJobOrderLineHome.JNDI_NAME, LocalArJobOrderLineHome.class);
			arJobOrderInvoiceLineHome = (LocalArJobOrderInvoiceLineHome)EJBHomeFactory.
					lookUpLocalHome(LocalArJobOrderInvoiceLineHome.JNDI_NAME, LocalArJobOrderInvoiceLineHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
    		
    		homeRIC = (InvRepItemCostingControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);
			
		
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		 try {
        	ejbRIC = homeRIC.create();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try { 

			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			LocalInvUnitOfMeasure invUnitOfMeasure = null;
			
			if(UOM_NM != null || UOM_NM.length() > 0) {
			
				try {
				
					invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(UOM_NM, AD_CMPNY);
					
				} catch (FinderException ex) {
					
				}
				
			}
			System.out.println("CheckPoint executeInvRepStockOnHand A");
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(bil) FROM AdBranchItemLocation bil ");

			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      

			Object obj[] = null;	      

			if (branchList.isEmpty()) {

				throw new GlobalNoRecordFoundException();

			}
			else {
				System.out.println("CheckPoint executeInvRepStockOnHand B");
				jbossQl.append("WHERE bil.adBranch.brCode in (");

				boolean firstLoop = true;

				Iterator j = branchList.iterator();

				while(j.hasNext()) {

					if(firstLoop == false) { 
						jbossQl.append(", "); 
					}
					else { 
						firstLoop = false; 
					}

					AdBranchDetails mdetails = (AdBranchDetails) j.next();

					jbossQl.append(mdetails.getBrCode());

				}

				jbossQl.append(") ");

				firstArgument = false;

			}                    

			// Allocate the size of the object parameter


			
			
			
			if (criteria.containsKey("itemName")) {

				criteriaSize--;

			}
			if (criteria.containsKey("itemNameTo")) {

				criteriaSize--;

			}	

			obj = new Object[criteriaSize];

			if (criteria.containsKey("itemName")) {

				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bil.invItemLocation.invItem.iiName  >= '" + (String)criteria.get("itemName") + "' ");

			}
			
			if (criteria.containsKey("itemNameTo")) {

				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bil.invItemLocation.invItem.iiName  <= '" + (String)criteria.get("itemNameTo") + "' ");
				System.out.println("Controller : itemNameTo ");
			}




			if (criteria.containsKey("itemClass")) {

				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bil.invItemLocation.invItem.iiClass=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemClass");
				ctr++;

			}

			if (criteria.containsKey("itemCategory")) {

				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bil.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemCategory");
				ctr++;

			}				

			if (criteria.containsKey("location")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bil.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("location");
				ctr++;

			}	

			if (criteria.containsKey("locationType")) {

				if (!firstArgument) {

					jbossQl.append("AND ");

				} else {		       	  

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("bil.invItemLocation.invLocation.locLvType=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("locationType");
				ctr++;

			}
			
			System.out.println("Controller : reportType ");
			
			String ReportType="";
			
		

				ReportType=rpt ;
				

			
			
			System.out.println("Controller : reportType " + ReportType);
			
			if (!firstArgument) {

				jbossQl.append("AND ");

			} else {

				firstArgument = false;
				jbossQl.append("WHERE ");

			}

			jbossQl.append("bil.invItemLocation.invItem.iiNonInventoriable=0 AND bil.bilAdCompany=" + AD_CMPNY + " ");   	  	  

			String orderBy = null;

			if (ORDER_BY.equals("ITEM NAME")) {

				orderBy = "bil.invItemLocation.invItem.iiName";

			} else if (ORDER_BY.equals("ITEM DESCRIPTION")) {

				orderBy = "bil.invItemLocation.invItem.iiDescription";

			} else if (ORDER_BY.equals("ITEM CLASS")) {

				orderBy = "bil.invItemLocation.invItem.iiClass";

			} else if (ORDER_BY.equals("ITEM CATEGORY")) {

				orderBy = "bil.invItemLocation.invItem.iiAdLvCategory";

			} 
			System.out.println("CHECK ReportType ----"+ReportType);
			if(ReportType.equals("SRP") || ReportType.equals("SKU") || ReportType.equals("Quantity")){
				orderBy = "bil.invItemLocation.invItem.iiAdLvCategory";
				System.out.println("CHECK OrderBy ----"+ orderBy);
			}
			if (orderBy != null) {
				System.out.println("Append OrderBy ----"+ orderBy);
				jbossQl.append("ORDER BY " + orderBy);

			}

			Collection invStockOnHands = null;

			System.out.println(jbossQl.toString());
			System.out.println("CheckPoint executeInvRepStockOnHand C");
			try {

				invStockOnHands = invStockOnHandHome.getBilByCriteria(jbossQl.toString(), obj);
				
			} catch (FinderException ex)	{

				throw new GlobalNoRecordFoundException ();

			} 

			if (invStockOnHands.isEmpty())
				throw new GlobalNoRecordFoundException ();

			Iterator i = invStockOnHands.iterator();
			
			System.out.println("CheckPoint executeInvRepStockOnHand D");
			
	
			
			while (i.hasNext()) {
				
				LocalAdBranchItemLocation invStockOnHand = (LocalAdBranchItemLocation) i.next();
				/*invStockOnHand.get
				LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        arInvoice.getInvDate(), invStockOnHand.getInvItemLocation().getInvItem().getIiName(), invStockOnHand.getInvItemLocation().getInvLocation().getLocName(), invStockOnHand.getAdBranch(), invStockOnHand.getBilAdCompany());
*/
				//System.out.println("CheckPoint executeInvRepStockOnHand E");
				
				
				if(fixCosting){
				
					 if(invStockOnHand.getInvItemLocation().getInvItem().getIiNonInventoriable() == (byte)0 ){
                        	System.out.println("RE CALC");
                        	HashMap criteria2 = new HashMap();
                            criteria2.put("itemName", invStockOnHand.getInvItemLocation().getInvItem().getIiName());
                            criteria2.put("location", invStockOnHand.getInvItemLocation().getInvLocation().getLocName());

                            ArrayList branchList2 = new ArrayList();

                            AdBranchDetails mdetails = new AdBranchDetails();
        					mdetails.setBrCode(invStockOnHand.getAdBranch().getBrCode());
        					branchList2.add(mdetails);

                            ejbRIC.executeInvFixItemCosting(criteria2, branchList2, AD_CMPNY);

                           
                     }
				
				}
				
				
				
				
				
				try {
					
					if (LAYERED) {
						System.out.println("LAYERED="+LAYERED);
						double negated=0d;
						//System.out.println("LAYERED");
						Collection PositiveValue = invCostingHome.findByCstDateAndIiNameAndLocName2( AS_OF_DT, invStockOnHand.getInvItemLocation().getInvItem().getIiName(),invStockOnHand.getInvItemLocation().getInvLocation().getLocName(), invStockOnHand.getAdBranch().getBrCode(), AD_CMPNY);
						Collection NegativeValue = invCostingHome.findByCstDateAndIiNameAndLocName3( AS_OF_DT, invStockOnHand.getInvItemLocation().getInvItem().getIiName(),invStockOnHand.getInvItemLocation().getInvLocation().getLocName(), invStockOnHand.getAdBranch().getBrCode(), AD_CMPNY);
						Iterator x = NegativeValue.iterator();
						while (x.hasNext()) {
							LocalInvCosting nega = (LocalInvCosting)x.next();
							negated += nega.getCstAdjustQuantity();
							
						}
						
						negated=negated*-1;
						double remain=0d;
						Iterator y = PositiveValue.iterator();
						while (y.hasNext()) {
							LocalInvCosting pos = (LocalInvCosting)y.next();
							remain=pos.getCstAdjustQuantity();
							//System.out.println("Start remain - "+ remain);
							//System.out.println(""+ remain+">="+negated);
							if(remain>=negated)
							{
								remain-=negated;
								negated=0;
							}
							else
							{
								negated=negated-remain;
								remain=0;
								
							}
							//System.out.println("remain1 - "+ remain);
							//System.out.println("negated1 - "+ negated);
							
							if(negated==0 && remain>0)
							{
								InvRepStockOnHandDetails details = new InvRepStockOnHandDetails();
								details.setShItemName(invStockOnHand.getInvItemLocation().getInvItem().getIiName());
			 					details.setShItemDescription(invStockOnHand.getInvItemLocation().getInvItem().getIiDescription());
			 					details.setShItemClass(invStockOnHand.getInvItemLocation().getInvItem().getIiClass());
			 					details.setShLocation(invStockOnHand.getInvItemLocation().getInvLocation().getLocName());			
			 					details.setShUnit(invStockOnHand.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
			 					details.setShUnitCost(invStockOnHand.getInvItemLocation().getInvItem().getIiUnitCost());
			 					details.setShPartNumber(invStockOnHand.getInvItemLocation().getInvItem().getIiPartNumber());
			 					details.setShCategoryName(invStockOnHand.getInvItemLocation().getInvItem().getIiAdLvCategory());
			 					details.setShBranchCode(invStockOnHand.getAdBranch().getBrBranchCode());
			 					
			 					details.setShQuantity(remain);
			 					double costOfRemain = pos.getCstAdjustCost()/pos.getCstAdjustQuantity();
			 					
			 					
			 					details.setShValue(remain*costOfRemain);
			 					details.setShAverageCost(costOfRemain);
			 					details.setShUnitCost(costOfRemain);
			 					details.setShUnservedPo(0);
			 					details.setShForecastQuantity(0);
			 					
			 					invStockOnHandList.add(details);
							}
							
							
						}
						
						//Collection invFifoCostings = invCostingHome.findByPriorEqualCstDateToAndIlCode(AS_OF_DT, invStockOnHand.getInvItemLocation().getIlCode(), invStockOnHand.getAdBranch().getBrCode(), AD_CMPNY);
						//if (invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500"))
						//System.out.println("invFifoCostings-" + invFifoCostings.size());
						
						/*
						if (invFifoCostings.size() > 0) {
			 				double fifoCost = 0;
			 				double runningRemQty = 0;
			 				InvRepStockOnHandDetails currentDetails = null;
			 				boolean isFirst = true;
			 				int lastIndex = invStockOnHandList.size();
			 				Iterator x = invFifoCostings.iterator();
			 				while (x.hasNext()) {
			 					
			 					LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
			 					
			 					if (invFifoCosting.getCstAdjustQuantity() > 0) {
			 						
				 					InvRepStockOnHandDetails details = new InvRepStockOnHandDetails();
				 					details.setShItemName(invStockOnHand.getInvItemLocation().getInvItem().getIiName());
				 					details.setShItemDescription(invStockOnHand.getInvItemLocation().getInvItem().getIiDescription());
				 					details.setShItemClass(invStockOnHand.getInvItemLocation().getInvItem().getIiClass());
				 					details.setShLocation(invStockOnHand.getInvItemLocation().getInvLocation().getLocName());			
				 					details.setShUnit(invStockOnHand.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
				 					details.setShUnitCost(invStockOnHand.getInvItemLocation().getInvItem().getIiUnitCost());
				 					details.setShPartNumber(invStockOnHand.getInvItemLocation().getInvItem().getIiPartNumber());
				 					details.setShCategoryName(invStockOnHand.getInvItemLocation().getInvItem().getIiAdLvCategory());
				 					details.setShQuantity(0);
				 					details.setShValue(0);
				 					details.setShAverageCost(0);
				 					details.setShUnservedPo(0);
				 					details.setShForecastQuantity(0);
				 					
				 					if(invUnitOfMeasure != null && 
											invUnitOfMeasure.getUomAdLvClass().equals(invStockOnHand.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomAdLvClass())) {
										details.setShQuantity(this.convertQuantityByUomToAndItem(invUnitOfMeasure, invStockOnHand.getInvItemLocation().getInvItem(), invFifoCosting.getCstAdjustQuantity(), AD_CMPNY));
										System.out.println("details.getShQuantity() - "+ details.getShQuantity());
										details.setShValue(invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity() * details.getShQuantity());
										
										System.out.println("details.getShValue() - "+ details.getShValue());
										details.setShAverageCost(details.getShQuantity() == 0 ? 0 : 
											EJBCommon.roundIt(details.getShValue() / details.getShQuantity(), adCompany.getGlFunctionalCurrency().getFcPrecision()));
										details.setShUnitCost(details.getShAverageCost());
										details.setShUnit(UOM_NM);
										
									
									} else {
										details.setShQuantity(invFifoCosting.getCstAdjustQuantity());
										System.out.println("details.getShQuantity() - "+ details.getShQuantity());
										details.setShValue(invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity() * details.getShQuantity());
										
										details.setShAverageCost(invStockOnHandCost.getCstRemainingQuantity() == 0 ? 0 : 
											EJBCommon.roundIt(details.getShValue() / details.getShQuantity(), adCompany.getGlFunctionalCurrency().getFcPrecision()));
										details.setShUnitCost(details.getShAverageCost());
										System.out.println("LALA - "+ details.getShValue());
									}
				 													
									if(invStockOnHandCost.getCstExpiryDate()!=null || invStockOnHandCost.getCstExpiryDate()!="")
										details.setShMisc(invStockOnHandCost.getCstExpiryDate());
									else
										details.setShMisc("");
									
									if (isFirst) {
										isFirst = false;
										currentDetails = details;
										runningRemQty = details.getShQuantity();
										System.out.println("runningRemQty isFirst-" + runningRemQty);
										if (invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500") ||
												invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500"))System.out.println("first-" + runningRemQty);
										
									}
									invStockOnHandList.add(details);
									
									
			 					} else {
			 						System.out.println("HEY ELSE");
			 						if (invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500") ||
											invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500")){
			 							System.out.println("HEY");
			 							System.out.println("runningRemQty-" + runningRemQty);
			 							System.out.println("invFifoCosting.getCstAdjustQuantity()-" + invFifoCosting.getCstAdjustQuantity());
			 							//System.out.println("currentDetails.getShQuantity()-" + currentDetails.getShQuantity());
			 						}
			 						if (runningRemQty >= Math.abs(invFifoCosting.getCstAdjustQuantity())) {
			 							if (invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500") ||
												invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500"))
			 							System.out.println("HEY1");
			 							System.out.println("HEY1999");
			 							currentDetails.setShQuantity(currentDetails.getShQuantity() - Math.abs(invFifoCosting.getCstAdjustQuantity()));
			 							currentDetails.setShValue(invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity() * currentDetails.getShQuantity());
			 							System.out.println("currentDetails.getShQuantity() - "+ currentDetails.getShQuantity());
			 							System.out.println("currentDetails.getShValue()() - "+ currentDetails.getShValue());
			 							
			 							currentDetails.setShAverageCost(invStockOnHandCost.getCstRemainingQuantity() == 0 ? 0 : 
											EJBCommon.roundIt(currentDetails.getShValue() / currentDetails.getShQuantity(), adCompany.getGlFunctionalCurrency().getFcPrecision()));
			 							currentDetails.setShUnitCost(currentDetails.getShAverageCost());
			 							runningRemQty -= Math.abs(invFifoCosting.getCstAdjustQuantity());
			 							invStockOnHandList.remove(lastIndex);
			 							invStockOnHandList.add(lastIndex, currentDetails);
			 							if (invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500") ||
												invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500"))
			 								System.out.println("invStockOnHandList- " + invStockOnHandList.size());
			 							
			 							if (currentDetails.getShQuantity() == 0) {
			 								invStockOnHandList.remove(lastIndex);
			 								if (invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500") ||
													invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500"))
				 								System.out.println("invStockOnHandList2- " + invStockOnHandList.size());
			 							}

			 						} else {
			 							if (invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500") ||
												invStockOnHand.getInvItemLocation().getInvItem().getIiName().equals("0500")) {
			 								System.out.println("HEY2");
			 								System.out.println("invStockOnHandList3- " + invStockOnHandList.size());
			 							}
			 							if (runningRemQty != 0 && runningRemQty < Math.abs(invFifoCosting.getCstAdjustQuantity())) {
			 								invStockOnHandList.remove(lastIndex);
			 							}
			 							//invStockOnHandList.remove(lastIndex);
			 							if (invStockOnHandList.size() > 0) {
				 							currentDetails = (InvRepStockOnHandDetails)invStockOnHandList.get(lastIndex);
				 							currentDetails.setShQuantity(currentDetails.getShQuantity() + runningRemQty - Math.abs(invFifoCosting.getCstAdjustQuantity()));
				 							currentDetails.setShValue(invFifoCosting.getCstAdjustCost() / invFifoCosting.getCstAdjustQuantity() * currentDetails.getShQuantity());
				 							currentDetails.setShAverageCost(invStockOnHandCost.getCstRemainingQuantity() == 0 ? 0 : 
												EJBCommon.roundIt(currentDetails.getShValue() / currentDetails.getShQuantity(), adCompany.getGlFunctionalCurrency().getFcPrecision()));
				 							currentDetails.setShUnitCost(currentDetails.getShAverageCost());
				 							runningRemQty = currentDetails.getShQuantity();
				 							invStockOnHandList.remove(lastIndex);
				 							invStockOnHandList.add(lastIndex, currentDetails);
			 							} 
			 						}
			 						
			 						

			 						
			 					}
								
			 				}
			 	  				
			 			}
						*/
					} else {
						
	
						InvRepStockOnHandDetails details = new InvRepStockOnHandDetails();
						
						try{
							
							LocalInvCosting invCostingLastReceive = invCostingHome.findLastReceiveDateByCstDateAndIiNameAndLocName(
									AS_OF_DT, invStockOnHand.getInvItemLocation().getInvItem().getIiName(), 
									invStockOnHand.getInvItemLocation().getInvLocation().getLocName(), 
									invStockOnHand.getAdBranch().getBrCode(), 
									AD_CMPNY);
							
							System.out.println("invCostingLastReceive.getCstDate()="+invCostingLastReceive.getCstDate());
							details.setShLastReceiveDate(invCostingLastReceive.getCstDate());
							
							
							Calendar startCalendar = new GregorianCalendar();
							startCalendar.setTime(invCostingLastReceive.getCstDate());
							Calendar endCalendar = new GregorianCalendar();
							endCalendar.setTime(AS_OF_DT);

							int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
							int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
							
							System.out.println("startCalendar.getTime()="+startCalendar.getTime());
							System.out.println("endCalendar.getTime()="+endCalendar.getTime());
							System.out.println("diffYear="+diffYear);
							System.out.println("diffMonth="+diffMonth);
							
							details.setShAgedInMonth(diffMonth);
							
							
						} catch (Exception ex){
							
						}
						
								
								
						details.setShItemName(invStockOnHand.getInvItemLocation().getInvItem().getIiName());
						details.setShItemCategory(invStockOnHand.getInvItemLocation().getInvItem().getIiAdLvCategory());
						details.setShItemDescription(invStockOnHand.getInvItemLocation().getInvItem().getIiDescription());
						details.setShItemClass(invStockOnHand.getInvItemLocation().getInvItem().getIiClass());
						details.setShLocation(invStockOnHand.getInvItemLocation().getInvLocation().getLocName());			
						details.setShUnit(invStockOnHand.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName());
						details.setShUnitCost(invStockOnHand.getInvItemLocation().getInvItem().getIiUnitCost());
						details.setShSalesPrice(invStockOnHand.getInvItemLocation().getInvItem().getIiSalesPrice());
						System.out.println("1 UNIT COST="+invStockOnHand.getInvItemLocation().getInvItem().getIiUnitCost());
						details.setShPartNumber(invStockOnHand.getInvItemLocation().getInvItem().getIiPartNumber());
						details.setShCategoryName(invStockOnHand.getInvItemLocation().getInvItem().getIiAdLvCategory());
						details.setShBranchCode(invStockOnHand.getAdBranch().getBrBranchCode());
	 					details.setShQuantity(0);
						details.setShValue(0);
						details.setShAverageCost(0);
						details.setShUnservedPo(0);
						details.setShForecastQuantity(0);
						
						
					
						try{
							
							LocalInvCosting invStockOnHandCost = 
									invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
											AS_OF_DT, details.getShItemName(), details.getShLocation(), 
											invStockOnHand.getAdBranch().getBrCode(), AD_CMPNY);
							
							
							
								if(invUnitOfMeasure != null && 
										invUnitOfMeasure.getUomAdLvClass().equals(invStockOnHand.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomAdLvClass())) {
									details.setShQuantity(this.convertQuantityByUomToAndItem(invUnitOfMeasure, invStockOnHand.getInvItemLocation().getInvItem(), invStockOnHandCost.getCstRemainingQuantity(), AD_CMPNY));
									details.setShValue(invStockOnHandCost.getCstRemainingValue());
									details.setShAverageCost(details.getShQuantity() == 0 ? 0 : 
										EJBCommon.roundIt(details.getShValue() / details.getShQuantity(), adCompany.getGlFunctionalCurrency().getFcPrecision()));
									details.setShUnit(UOM_NM);
									details.setShUnitCost(details.getShAverageCost());
									System.out.println("2 UNIT COST="+details.getShAverageCost());
									
								
								} else {
									
									details.setShQuantity(invStockOnHandCost.getCstRemainingQuantity());
									details.setShValue(invStockOnHandCost.getCstRemainingValue());
									details.setShAverageCost(invStockOnHandCost.getCstRemainingQuantity() == 0 ? 0 : 
										EJBCommon.roundIt(invStockOnHandCost.getCstRemainingValue() / invStockOnHandCost.getCstRemainingQuantity(), adCompany.getGlFunctionalCurrency().getFcPrecision()));
									details.setShUnitCost(details.getShAverageCost());
									System.out.println("3 UNIT COST="+details.getShAverageCost());
								}
								/*System.out.println("3---------------------->");
								System.out.println("CHECK 100 ----"+ReportType);
								if(ReportType.equals("Consolidated Report") || ReportType.equals("SRP") || ReportType.equals("Quantity") || ReportType.equals("SKU"))
								{
									
									
									
									details.setShUnitCost(invStockOnHandCost.getInvItemLocation().getInvItem().getIiUnitCost());
									details.setShAverageCost(invStockOnHandCost.getInvItemLocation().getInvItem().getIiSalesPrice());
								}*/
								
								
								
								if(invStockOnHandCost.getCstExpiryDate()!=null || invStockOnHandCost.getCstExpiryDate()!="")
									details.setShMisc(invStockOnHandCost.getCstExpiryDate());
								else
									details.setShMisc("");
							
						}catch(Exception ex){
							
						}
						
						
						// check if item is for reorder
						if(invStockOnHand.getBilReorderPoint() > 0 && invStockOnHand.getBilReorderPoint() >= details.getShQuantity()) {

							details.setShItemForReorder(true);

						} else {

							details.setShItemForReorder(false);

						}

						details.setShCommittedQuantity(0);

			
						// get last PO date
						try {

							LocalApPurchaseOrderLine apPurchaseOrderLine = 
								apPurchaseOrderLineHome.getByMaxPoDateAndMaxPlCodeAndIiNameAndLocNameAndPoReceivingAndPoPostedAndLessThanEqualPoDate(
										details.getShItemName(), details.getShLocation(), EJBCommon.FALSE, EJBCommon.TRUE, AS_OF_DT, AD_BRNCH, AD_CMPNY);

							details.setShLastPoDate(EJBCommon.convertSQLDateToString(apPurchaseOrderLine.getApPurchaseOrder().getPoDate()));

						} catch (FinderException ex){
						}				
						
						double netQty = details.getShQuantity();
						
						// get committed quantities
						if (SHW_CMMTTD_QNTTY) 
						{
							System.out.println("SHW_CMMTTD_QNTTY="+SHW_CMMTTD_QNTTY);
							//details.setShCommittedQuantity(getIlCommittedQuantity(invStockOnHand, AD_CMPNY));
							
							double solQty = 0d;
							double jolQty = 0d;
							double deliveredQty = 0d;
							double committedQty = 0d;
							GregorianCalendar gcDateFrom = new GregorianCalendar();
							gcDateFrom.setTime(AS_OF_DT);
							gcDateFrom.add(Calendar.MONTH, -1);
							
							Collection arSalesOrderLines = arSalesOrderLineHome.findCommittedQtyByIiNameAndLocNameAndSoAdBranch(invStockOnHand.getInvItemLocation().getInvItem().getIiName(), invStockOnHand.getInvItemLocation().getInvLocation().getLocName(), gcDateFrom.getTime(), AS_OF_DT, invStockOnHand.getAdBranch().getBrCode(), AD_CMPNY);
							
							Iterator solIter = arSalesOrderLines.iterator(); 
							
							
							while (solIter.hasNext())
							{
								LocalArSalesOrderLine arSalesOrderLine = (LocalArSalesOrderLine)solIter.next();
								
								if(arSalesOrderLine.getArSalesOrder().getSoOrderStatus().equals("Bad"))
									continue;
								
								committedQty += (arSalesOrderLine.getArSalesOrderInvoiceLines().size() == 0 ? arSalesOrderLine.getSolQuantity() : 0);
							}
							
							Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome.findCommittedQtyByIiNameAndLocNameAndSoAdBranch(invStockOnHand.getInvItemLocation().getInvItem().getIiName(), invStockOnHand.getInvItemLocation().getInvLocation().getLocName(), gcDateFrom.getTime(), AS_OF_DT, invStockOnHand.getAdBranch().getBrCode(), AD_CMPNY);
							
							Iterator silIter = arSalesOrderInvoiceLines.iterator();
							
							while (silIter.hasNext()) 
							{
								LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)silIter.next();
								solQty = arSalesOrderInvoiceLine.getArSalesOrderLine().getSolQuantity();
								
								deliveredQty = arSalesOrderInvoiceLine.getSilQuantityDelivered();
								committedQty += (solQty - deliveredQty);
							}	
							
							
							Collection arJobOrderLines = arJobOrderLineHome.findCommittedQtyByIiNameAndLocNameAndJoAdBranch(invStockOnHand.getInvItemLocation().getInvItem().getIiName(), invStockOnHand.getInvItemLocation().getInvLocation().getLocName(), gcDateFrom.getTime(), AS_OF_DT, invStockOnHand.getAdBranch().getBrCode(), AD_CMPNY);
							
							Iterator jolIter = arJobOrderLines.iterator(); 
							
							
							while (jolIter.hasNext())
							{
								LocalArJobOrderLine arJobOrderLine = (LocalArJobOrderLine)jolIter.next();
								
								if(arJobOrderLine.getArJobOrder().getJoJobOrderStatus().equals("Bad"))
									continue;
								
								committedQty += (arJobOrderLine.getArJobOrderInvoiceLines().size() == 0 ? arJobOrderLine.getJolQuantity() : 0);
							}
							
							
							
							Collection arJobOrderInvoiceLines = arJobOrderInvoiceLineHome.findCommittedQtyByIiNameAndLocNameAndJoAdBranch(invStockOnHand.getInvItemLocation().getInvItem().getIiName(), invStockOnHand.getInvItemLocation().getInvLocation().getLocName(), gcDateFrom.getTime(), AS_OF_DT, invStockOnHand.getAdBranch().getBrCode(), AD_CMPNY);
							
							Iterator jilIter = arJobOrderInvoiceLines.iterator(); 
							
							
							while (jilIter.hasNext()) 
							{
								LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine)jilIter.next();
								jolQty = arJobOrderInvoiceLine.getArJobOrderLine().getJolQuantity();
								
								deliveredQty = arJobOrderInvoiceLine.getJilQuantityDelivered();
								committedQty += (jolQty - deliveredQty);
							}	
							
							
											
							details.setShCommittedQuantity(committedQty);
							netQty-=committedQty;
						}
					
						
						//get unposted quantities
						if (INCLD_UNPSTD == true)
						{
							details.setShUnpostedQuantity(this.getShUnpostedQuantity(invStockOnHand, AS_OF_DT, AD_CMPNY));
							netQty+=details.getShUnpostedQuantity();
						}
						
						
				
						
						//get unserved PO qty
						Collection apPurchaseOrderLines = apPurchaseOrderLineHome.findUnservedPoByIiNameAndLocNameAndLessEqualDateAndPoAdBranch(
								invStockOnHand.getInvItemLocation().getInvItem().getIiName(),
								invStockOnHand.getInvItemLocation().getInvLocation().getLocName(),
								AS_OF_DT, invStockOnHand.getAdBranch().getBrCode() ,AD_CMPNY);
						
						Iterator plIter = apPurchaseOrderLines.iterator();
						
						while(plIter.hasNext()) {
							
							LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)plIter.next();
							
							details.setShUnservedPo(details.getShUnservedPo() + apPurchaseOrderLine.getPlQuantity());
						}
					
						//get assembly item quantity forecast 
						if(INCLD_FRCST && invStockOnHand.getInvItemLocation().getInvItem().getIiClass().equals("Assembly")) {
							System.out.println("INCLD_FRCST="+INCLD_FRCST);
							Collection billOfMaterials = invStockOnHand.getInvItemLocation().getInvItem().getInvBillOfMaterials();
							
							double forecastQty = 0d;
							
							if(billOfMaterials.size() > 0) {

								Iterator bomIter = billOfMaterials.iterator();

								double forecastQtyArr[] = new double[billOfMaterials.size()];
								int bomCtr = 0;
								double actualQty = 0d;
								
						
								while(bomIter.hasNext()) {
									
									LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)bomIter.next();
									
									LocalInvCosting invBomStockOnHandCost = 
										invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
												AS_OF_DT, invBillOfMaterial.getBomIiName(), invBillOfMaterial.getBomLocName(), 
												invStockOnHand.getAdBranch().getBrCode(), AD_CMPNY);
									
									if(invUnitOfMeasure != null && 
											invUnitOfMeasure.getUomAdLvClass().equals(invStockOnHand.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomAdLvClass())) {
										actualQty = this.convertQuantityByUomToAndItem(invUnitOfMeasure, invStockOnHand.getInvItemLocation().getInvItem(), invBomStockOnHandCost.getCstRemainingQuantity(), AD_CMPNY);
									
									} else 
										actualQty = invBomStockOnHandCost.getCstRemainingQuantity();
									
									forecastQtyArr[bomCtr++] = actualQty / invBillOfMaterial.getBomQuantityNeeded();
								}
								
								if(forecastQtyArr[0] >= 1)
									forecastQty = forecastQtyArr[0];
								
								if(forecastQtyArr.length > 1 && forecastQty!=0) {
									for(bomCtr=1; bomCtr<forecastQtyArr.length; bomCtr++) {
										if(forecastQtyArr[bomCtr] < 1){
											forecastQty = 0;
											break;
										}
										if(forecastQtyArr[bomCtr] < forecastQty) 
											forecastQty = forecastQtyArr[bomCtr];
									}
								}
							}

							details.setShForecastQuantity(forecastQty);
						}
						
					
						
						details.setShOrderBy(ORDER_BY);
					
						if ((netQty > 0) ||(INCLD_ZRS && (netQty <= 0))){
							
							invStockOnHandList.add(details);
						}
					
						
					}
				
				} catch (FinderException ex){
				}

			}
			
			System.out.println("invStockOnHandList size="+invStockOnHandList.size());
			Collections.sort(invStockOnHandList, InvRepStockOnHandDetails.NoGroupComparator);
			

			if (invStockOnHandList.isEmpty())
				throw new GlobalNoRecordFoundException ();
			else
				return invStockOnHandList;	  

		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvLocationTypeAll(Integer AD_CMPNY) {
		
		Debug.print("InvStockOnHandControllerBean getAdLvInvLocationTypeAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		System.out.println("CheckPoint executeInvRepStockOnHand I");
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV LOCATION TYPE", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}		

	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepStockOnHandControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("InvRepStockOnHandControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        System.out.println("CheckPoint executeInvRepStockOnHand J");
        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
    
    /**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getInvUomAll(Integer AD_CMPNY) {
        
        Debug.print("InvRepStockOnHandControllerBean getInvUomAll");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            Collection invUnitOfMeasures = invUnitOfMeasureHome.findEnabledUomAll(AD_CMPNY);
            
            Iterator i = invUnitOfMeasures.iterator();
            
            while (i.hasNext()) {
                
            	LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure)i.next();
                
                list.add(invUnitOfMeasure.getUomName());
                
            }
            
            return list;
            
        } catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
        
    }
	
    // private methods
	
	
	
	// if INCLD_UNPSTD=TRUE then
	// call finder
	// loop on the unposted 
	// total should be set to details.setShUnpostedQty
				                                 
	private double getShUnpostedQuantity(LocalAdBranchItemLocation adBranchItemLocation, Date AS_OF_DT, Integer AD_CMPNY)
	{
    	Debug.print("InvRepStockOnHandControllerBean getUnpostedQuantity");
		
		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalApPurchaseOrderLineHome apPurchaseOrderLineHome = null;
		LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
		LocalArSalesOrderInvoiceLineHome arSalesOrderInvoiceLineHome = null; 
		LocalArJobOrderInvoiceLineHome arJobOrderInvoiceLineHome = null;
		LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
		LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
		LocalInvStockTransferLineHome invStockTransferLineHome = null;
		LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;

		double totalUnpostedQuantity = 0d;
		
		// Initialize EJB Home		
		System.out.println("CheckPoint executeInvRepStockOnHand K");
		try 
		{
			
			apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			apPurchaseOrderLineHome = (LocalApPurchaseOrderLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalApPurchaseOrderLineHome.JNDI_NAME, LocalApPurchaseOrderLineHome.class);
			arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
			arSalesOrderInvoiceLineHome = (LocalArSalesOrderInvoiceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalArSalesOrderInvoiceLineHome.JNDI_NAME, LocalArSalesOrderInvoiceLineHome.class);
			arJobOrderInvoiceLineHome = (LocalArJobOrderInvoiceLineHome)EJBHomeFactory.
					lookUpLocalHome(LocalArJobOrderInvoiceLineHome.JNDI_NAME, LocalArJobOrderInvoiceLineHome.class);
			invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
			invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
			invStockTransferLineHome = (LocalInvStockTransferLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class);
			invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
		
		} 
		catch (NamingException ex) 
		{
			throw new EJBException(ex.getMessage());
		}
		System.out.println("CheckPoint executeInvRepStockOnHand L");
		try
		{
			double unpostedQuantity = 0d;
			
			String itemName = adBranchItemLocation.getInvItemLocation().getInvItem().getIiName();
			String locationName = adBranchItemLocation.getInvItemLocation().getInvLocation().getLocName();
			Integer locationCode = adBranchItemLocation.getInvItemLocation().getInvLocation().getLocCode();
			Integer branchCode = adBranchItemLocation.getAdBranch().getBrCode();

			
			//(1) AP Voucher Line Item - AP Voucher
			byte debitMemo;
			
			Collection apVoucherLineItems = apVoucherLineItemHome.findUnpostedVouByIiNameAndLocNameAndLessEqualDateAndVouAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
	
			Iterator x = apVoucherLineItems.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)x.next();
	
				unpostedQuantity = apVoucherLineItem.getVliQuantity();

				System.out.println("(1) Name: " + apVoucherLineItem.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);								
				
				debitMemo = apVoucherLineItem.getApVoucher().getVouDebitMemo(); 
				
				if (debitMemo == 0)
				{
					totalUnpostedQuantity += unpostedQuantity;
				}
				else
				{
					totalUnpostedQuantity -= unpostedQuantity;
				}
			}

			
			//(2) AP Voucher Line Item - AP Check
			apVoucherLineItems = apVoucherLineItemHome.findUnpostedChkByIiNameAndLocNameAndLessEqualDateAndChkAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
	
			x = apVoucherLineItems.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)x.next();
	
				unpostedQuantity = apVoucherLineItem.getVliQuantity();
										
				System.out.println("(2) Name: " + apVoucherLineItem.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				totalUnpostedQuantity += unpostedQuantity;
			
			}
			
			
			//(3) AP PURCHASE ORDER LINE								
			Collection apPurchaseOrderLines = apPurchaseOrderLineHome.findUnpostedPoByIiNameAndLocNameAndLessEqualDateAndPoAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = apPurchaseOrderLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalApPurchaseOrderLine apPurchaseOrderLine = (LocalApPurchaseOrderLine)x.next();
	
				unpostedQuantity = apPurchaseOrderLine.getPlQuantity();
				
				System.out.println("(3) Name: " + apPurchaseOrderLine.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				totalUnpostedQuantity += unpostedQuantity;
			
			}																									
			
			
			//(4) AR Invoice Line Item - AR Invoice
			byte creditMemo;
			
			Collection arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = arInvoiceLineItems.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)x.next();
	
				unpostedQuantity = arInvoiceLineItem.getIliQuantity();
				
				System.out.println("(4) Name: " + arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				creditMemo = arInvoiceLineItem.getArInvoice().getInvCreditMemo(); 
				
				if (creditMemo == 0)
				{
					totalUnpostedQuantity -= unpostedQuantity;
				}
				else
				{
					totalUnpostedQuantity += unpostedQuantity;
				}
			}
			
			
			//(5) AR Invoice Line Item - AR Receipt
			arInvoiceLineItems = arInvoiceLineItemHome.findUnpostedRctByIiNameAndLocNameAndLessEqualDateAndRctAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
	
			x = arInvoiceLineItems.iterator();

			while (x.hasNext()) 
			{
				
				LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem)x.next();
	
				unpostedQuantity = arInvoiceLineItem.getIliQuantity();
														
				System.out.println("(5) Name: " + arInvoiceLineItem.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				totalUnpostedQuantity -= unpostedQuantity;
			
			}
			
			
			//(6)AR Sales Order Invoice Line
			Collection arSalesOrderInvoiceLines = arSalesOrderInvoiceLineHome.findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = arSalesOrderInvoiceLines.iterator();
			
			while(x.hasNext())
			{
				LocalArSalesOrderInvoiceLine arSalesOrderInvoiceLine = (LocalArSalesOrderInvoiceLine)x.next();
				
				unpostedQuantity = arSalesOrderInvoiceLine.getSilQuantityDelivered();

				System.out.println("(6) Name: " + arSalesOrderInvoiceLine.getArSalesOrderLine().getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);				
				
				totalUnpostedQuantity -= unpostedQuantity;
				
			}
			
			//(6.5)AR Job Order Invoice Line
			Collection arJobOrderInvoiceLines = arJobOrderInvoiceLineHome.findUnpostedInvcByIiNameAndLocNameAndLessEqualDateAndInvAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = arJobOrderInvoiceLines.iterator();
			
			while(x.hasNext())
			{
				LocalArJobOrderInvoiceLine arJobOrderInvoiceLine = (LocalArJobOrderInvoiceLine)x.next();
				
				unpostedQuantity = arJobOrderInvoiceLine.getJilQuantityDelivered();

				System.out.println("(6) Name: " + arJobOrderInvoiceLine.getArJobOrderLine().getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);				
				
				totalUnpostedQuantity -= unpostedQuantity;
				
			}
			
			
			//(7) INV Adjustment Line
			Collection invAdjustmentLines = invAdjustmentLineHome.findUnpostedAdjByIiNameAndLocNameAndLessEqualDateAndAdjAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = invAdjustmentLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)x.next();
	
				unpostedQuantity = invAdjustmentLine.getAlAdjustQuantity();
				
				System.out.println("(7) Name: " + invAdjustmentLine.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				totalUnpostedQuantity += unpostedQuantity;
			}																									
			

			//(8) INV Build-Unbuild Assembly
			Collection invBuildUnbuildAssemblyLines = invBuildUnbuildAssemblyLineHome.findUnpostedBuaByIiNameAndLocNameAndLessEqualDateAndBuaAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = invBuildUnbuildAssemblyLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)x.next();
	
				unpostedQuantity = invBuildUnbuildAssemblyLine.getBlBuildQuantity();
				
				System.out.println("(8) Name: " + invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);

				if (unpostedQuantity >= 0)
				{
					totalUnpostedQuantity += unpostedQuantity;
				}
				else if (unpostedQuantity < 0)
				{
					totalUnpostedQuantity -= unpostedQuantity;					
				}
			}	
			
			
			//(8.1) INV Build-Unbuild Assembly
			Collection invBuildUnbuildAssemblyLines2 = invBuildUnbuildAssemblyLineHome.findUnpostedBuaByIiNameAndLocNameAndLessEqualDateAndBuaAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = invBuildUnbuildAssemblyLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)x.next();
	
				
				Collection invBillofMaterials = invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem().getInvBillOfMaterials();

				Iterator y = invBillofMaterials.iterator();
    			
    			while(y.hasNext()) {
    				
    				LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial)y.next();
    				

					
    				double qtyNeeded = invBillOfMaterial.getBomQuantityNeeded();
    				
    				unpostedQuantity =( invBuildUnbuildAssemblyLine.getBlBuildQuantity() ) * qtyNeeded / 100;
    				
    				
    				if (unpostedQuantity >= 0)
    				{
    					totalUnpostedQuantity += unpostedQuantity;
    				}
    				else if (unpostedQuantity < 0)
    				{
    					totalUnpostedQuantity -= unpostedQuantity;					
    				}
    				
    			}
				
				

			
			}
			
		
			//(9) INV Stock Transfer Line
			//Series of System.out's were executed in order to trace the ADDITION and DEDUCTION of unposted quantities for each item.
			Collection invStockTransferLines = invStockTransferLineHome.findUnpostedStlByIiNameAndLocCodeAndLessEqualDateAndStAdBranch(itemName, locationCode, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = invStockTransferLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine)x.next();
	
				unpostedQuantity = invStockTransferLine.getStlQuantityDelivered();
				
				System.out.println("(9) Item Name: " + invStockTransferLine.getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				System.out.println("Location Code" +  locationCode);
				System.out.println("Location TO: " + invStockTransferLine.getStlLocationTo());
				System.out.println("Location FROM: " + invStockTransferLine.getStlLocationFrom());
				
				if (invStockTransferLine.getStlLocationTo().equals(locationCode))
				{
					System.out.println("(9) Before Adding: Name: " + invStockTransferLine.getInvItem().getIiName() + "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity" + totalUnpostedQuantity);
					totalUnpostedQuantity += unpostedQuantity;
					System.out.println("(9) After Adding: Name: " + invStockTransferLine.getInvItem().getIiName() + "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity" + totalUnpostedQuantity);
				}
				else if (invStockTransferLine.getStlLocationFrom().equals(locationCode))
				{
					System.out.println("(9) Before Deducting: Name: " + invStockTransferLine.getInvItem().getIiName() + "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity" + totalUnpostedQuantity);					
					totalUnpostedQuantity -= unpostedQuantity;
					System.out.println("(9) After Deducting: Name: " + invStockTransferLine.getInvItem().getIiName() + "\tunpostedQuantity: " + unpostedQuantity + "\ttotalUnpostedQuantity" + totalUnpostedQuantity);
				}
			}			

			
			//(10) INV Branch Stock Transfer Line
			String bstType = "";
			
			Collection invBranchStockTransferLines = invBranchStockTransferLineHome.findUnpostedBstByIiNameAndLocNameAndLessEqualDateAndBstAdBranch(itemName, locationName, AS_OF_DT, branchCode, AD_CMPNY);
			
			x = invBranchStockTransferLines.iterator();
			
			while (x.hasNext()) 
			{
				
				LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine)x.next();
	
				unpostedQuantity = invBranchStockTransferLine.getBslQuantity();
				
				bstType = invBranchStockTransferLine.getInvBranchStockTransfer().getBstType();
				
				System.out.println("(10) Name: " + invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName() + "\tUnposted Quantity: " + unpostedQuantity);
				
				if(bstType.equals("IN"))
				{
					totalUnpostedQuantity += unpostedQuantity;
				}
				else if(bstType.equals("OUT"))
				{
					totalUnpostedQuantity -= unpostedQuantity;					
				}
			}																									
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		
		return totalUnpostedQuantity;
		
				
	}
	
	// private methods
	private static int isValidDate(String input) {
        String formatString = "MM/dd/yyyy";
        int x=0;
        try {
            SimpleDateFormat format = new SimpleDateFormat(formatString);
            format.setLenient(false);
            format.parse(input);
        } catch (ParseException e) {
           return x;
        } catch (IllegalArgumentException e) {
            return x;
        }
        x=1;
        return x;
    }

	
    private double getIlCommittedQuantity (LocalAdBranchItemLocation adBranchItemLocation, Integer AD_CMPNY) {
    	
    	Debug.print("InvReceivingSyncControllerBean getIlCommittedQuantity");
    	
    	LocalArInvoiceLineItemHome arInvoiceLineItemHome = null;
    	LocalApVoucherLineItemHome apVoucherLineItemHome = null;    	
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
    	LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
    	LocalInvBuildUnbuildAssemblyLineHome invBuildUnbuildAssemblyLineHome = null;
    	LocalInvItemHome invItemHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalInvStockIssuanceLineHome invStockIssuanceLineHome = null;    	
    	LocalInvStockTransferLineHome invStockTransferLineHome = null;

    	double IL_CMMTTD_QNTTY = 0;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		arInvoiceLineItemHome = (LocalArInvoiceLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalArInvoiceLineItemHome.JNDI_NAME, LocalArInvoiceLineItemHome.class);
    		apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);
    		invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
    		invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class);
    		invBuildUnbuildAssemblyLineHome = (LocalInvBuildUnbuildAssemblyLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvBuildUnbuildAssemblyLineHome.JNDI_NAME, LocalInvBuildUnbuildAssemblyLineHome.class);
    		invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
    		invStockIssuanceLineHome = (LocalInvStockIssuanceLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvStockIssuanceLineHome.JNDI_NAME, LocalInvStockIssuanceLineHome.class);
    		invStockTransferLineHome = (LocalInvStockTransferLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class);

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	System.out.println("CheckPoint executeInvRepStockOnHand M");
    	try {
    		
    		Integer IL_CODE = adBranchItemLocation.getInvItemLocation().getIlCode();
    		Integer AD_BRNCH = adBranchItemLocation.getAdBranch().getBrCode();
    		
    		LocalInvItemLocation invItemLocation = null;
    		
    		try {
    			
    			invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
    			
    		} catch (FinderException ex){
    			
    		}
    		
    		Collection lineItems = null;
    		Iterator i = null;
    		
			// get invoice line items
    		try{
    			
    			lineItems = arInvoiceLineItemHome.findByInvCreditMemoAndInvPostedAndIlCodeAndBrCode(EJBCommon.FALSE,
    					EJBCommon.FALSE, IL_CODE, AD_BRNCH, AD_CMPNY);
    		} catch (FinderException ex){
    			
    		}
    		
    		i = lineItems.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
    			
    			IL_CMMTTD_QNTTY = IL_CMMTTD_QNTTY + convertByUomFromAndItemAndQuantity(
    					arInvoiceLineItem.getInvUnitOfMeasure(),
						arInvoiceLineItem.getInvItemLocation().getInvItem(), arInvoiceLineItem.getIliQuantity(),
						AD_CMPNY);
    			
    		}
    		
    		
			// get misc receipt line items
    		try{
    			
    			lineItems = arInvoiceLineItemHome.findByRctPostedAndIlCodeAndBrCode(EJBCommon.FALSE, IL_CODE,
    					AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex){
    			
    		}

    		i = lineItems.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalArInvoiceLineItem arInvoiceLineItem = (LocalArInvoiceLineItem) i.next();
    			
    			IL_CMMTTD_QNTTY = IL_CMMTTD_QNTTY + convertByUomFromAndItemAndQuantity(
    					arInvoiceLineItem.getInvUnitOfMeasure(), arInvoiceLineItem.getInvItemLocation().getInvItem(),
						arInvoiceLineItem.getIliQuantity(), AD_CMPNY);
    			
    		}
    		
			// get debit memo line items 
    		try{
    			
    			lineItems = apVoucherLineItemHome.findByVouDebitMemoAndVouPostedAndIlCodeAndBrCode(EJBCommon.TRUE,
    					EJBCommon.FALSE, IL_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex){
    			
    		}

    		i = lineItems.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem) i.next();
    			
    			IL_CMMTTD_QNTTY = IL_CMMTTD_QNTTY + convertByUomFromAndItemAndQuantity(
    					apVoucherLineItem.getInvUnitOfMeasure(),
						apVoucherLineItem.getInvItemLocation().getInvItem(), apVoucherLineItem.getVliQuantity(),
						AD_CMPNY);
    			
    		}
    		
			// get inv adjustment lines
    		try{
    			
    			lineItems = invAdjustmentLineHome.findNegativeAlByAdjPostedAndIlCodeAndBrCode(EJBCommon.FALSE,
    					IL_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex){
    			
    		}
    		i = lineItems.iterator();
    		
    		while (i.hasNext()){
    			
    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
    			
    			IL_CMMTTD_QNTTY = IL_CMMTTD_QNTTY + Math.abs(convertByUomFromAndItemAndQuantity(
    					invAdjustmentLine.getInvUnitOfMeasure(), invAdjustmentLine.getInvItemLocation().getInvItem(),
						invAdjustmentLine.getAlAdjustQuantity(), AD_CMPNY));
    			
    		}
    		
    		// get positive build unbuild assembly
    		try{	
    			
    			// get bill of materials
    			lineItems = invBillOfMaterialHome.findByBomIiNameAndLocNameAndBrCode(
    					 invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_CMPNY);
    			i = lineItems.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvBillOfMaterial invBillOfMaterial = (LocalInvBillOfMaterial) i.next();
    				
    				// get assembly item locations
    				Collection invAssemblyItemLocations = invBillOfMaterial.getInvItem().getInvItemLocations();
    				Iterator iterIl = invAssemblyItemLocations.iterator();
    				
    				while (iterIl.hasNext()){
    					
    					LocalInvItemLocation invAssemblyItemLocation = (LocalInvItemLocation) iterIl.next();
    					
    					// get build unbuild assembly lines
    					Collection invBuildUnBuildAssemblyLines = invAssemblyItemLocation.getInvBuildUnbuildAssemblyLines();
    					
    					Iterator iterBua = invBuildUnBuildAssemblyLines.iterator();
    					
    					while(iterBua.hasNext()){
    						
    						LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine = (LocalInvBuildUnbuildAssemblyLine)iterBua.next();
    						
    						// build qty conversion
    						double buildQuantity = convertByUomFromAndItemAndQuantity(
    	                    		invBuildUnbuildAssemblyLine.getInvUnitOfMeasure(), 
    	                    		invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem(),
    	                    		Math.abs(invBuildUnbuildAssemblyLine.getBlBuildQuantity()), AD_CMPNY);
    	                    
    						if (invBuildUnbuildAssemblyLine.getInvBuildUnbuildAssembly().getBuaPosted() ==
    							EJBCommon.FALSE && invBuildUnbuildAssemblyLine.getBlBuildQuantity() > 0) {
    							
    							LocalInvItem invItem = invItemHome.findByIiName(invBillOfMaterial.getBomIiName(),
    									AD_CMPNY);
    							
    							IL_CMMTTD_QNTTY = IL_CMMTTD_QNTTY + Math.abs(convertByUomFromAndItemAndQuantity(
    									invBillOfMaterial.getInvUnitOfMeasure(), invItem,
										(buildQuantity * invBillOfMaterial.getBomQuantityNeeded()), AD_CMPNY));
    							
    						}
    						
    					}
    					
    				}
    				
    			}
    			
    		} catch (FinderException ex){
    			
    		}

			// get negative build unbuild assembly for assembly items
    		if(!invItemLocation.getInvItem().getInvBillOfMaterials().isEmpty()){
    			
    			try {
    				
    				lineItems = invBuildUnbuildAssemblyLineHome.findNegativeBlByBuaPostedAndIlCodeAndBrCode(
    						EJBCommon.FALSE,IL_CODE, AD_BRNCH, AD_CMPNY);
    			} catch (FinderException ex) {
    				
    			}
    			
    			i = lineItems.iterator();
    			
    			while (i.hasNext()){
    				
    				LocalInvBuildUnbuildAssemblyLine invBuildUnbuildAssemblyLine =
    					(LocalInvBuildUnbuildAssemblyLine) i.next();
    				
    				IL_CMMTTD_QNTTY = IL_CMMTTD_QNTTY + Math.abs(convertByUomFromAndItemAndQuantity(
    						invBuildUnbuildAssemblyLine.getInvUnitOfMeasure(),
							invBuildUnbuildAssemblyLine.getInvItemLocation().getInvItem(),
							invBuildUnbuildAssemblyLine.getBlBuildQuantity(), AD_CMPNY));
    				
    			}

    		}

			// get branch stock transfer out
    		try{
    			
    			lineItems = invBranchStockTransferLineHome.findOutBslByBstPostedAndIlCodeAndBrCode(EJBCommon.FALSE,
    					IL_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex){
    			
    		}
    		i = lineItems.iterator();
    		System.out.println("CheckPoint executeInvRepStockOnHand N");
    		while (i.hasNext()){
    			
    			LocalInvBranchStockTransferLine invBranchStockTransferLine =
    				(LocalInvBranchStockTransferLine) i.next();
    			
    			IL_CMMTTD_QNTTY = IL_CMMTTD_QNTTY + convertByUomFromAndItemAndQuantity(
    					invBranchStockTransferLine.getInvUnitOfMeasure(),
						invBranchStockTransferLine.getInvItemLocation().getInvItem(),
						invBranchStockTransferLine.getBslQuantity(), AD_CMPNY);
    			
    		}
    		
			// get branch stock transfer in
    		try{
    			
    			lineItems = invBranchStockTransferLineHome.findInBslByBstPostedAndLocCodeAndIiCodeAndAdBrCode(
    					EJBCommon.FALSE, invItemLocation.getInvLocation().getLocCode(),
						invItemLocation.getInvItem().getIiCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex){
    			
    		}
    		
    		i = lineItems.iterator();
    		
    		while (i.hasNext()){
    			
    			LocalInvBranchStockTransferLine invBranchStockTransferLine =
    				(LocalInvBranchStockTransferLine) i.next();
    			
    			System.out.println(invBranchStockTransferLine.getInvBranchStockTransfer().getAdBranch().getBrName());
    			
    			IL_CMMTTD_QNTTY = IL_CMMTTD_QNTTY + convertByUomFromAndItemAndQuantity(
    					invBranchStockTransferLine.getInvUnitOfMeasure(),
						invBranchStockTransferLine.getInvItemLocation().getInvItem(),
						invBranchStockTransferLine.getBslQuantity(), AD_CMPNY);
    			
    		}
    		
			// get stock issuance
    		try{
    			
    			lineItems = invStockIssuanceLineHome.findBySiPostedAndIlCodeAndBrCode(EJBCommon.FALSE, IL_CODE,
    					AD_BRNCH, AD_CMPNY);
    		} catch (FinderException ex){
    			
    		}

    		i = lineItems.iterator();
    		
    		while (i.hasNext()){
    			
    			LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine) i.next();
    			
    			IL_CMMTTD_QNTTY = IL_CMMTTD_QNTTY + convertByUomFromAndItemAndQuantity(
    					invStockIssuanceLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
						invStockIssuanceLine.getSilIssueQuantity(), AD_CMPNY);
    			
    		}
    		
			// get stock transfer
    		try{
    			
    			lineItems = invStockTransferLineHome.findByStPostedAndStlLocationFromAndIiCodeAndBrCode(
    					EJBCommon.FALSE, invItemLocation.getInvLocation().getLocCode(),
						invItemLocation.getInvItem().getIiCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex){
    			
    		}    			
    		i = lineItems.iterator();
    		
    		while (i.hasNext()){
    			
    			LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) i.next();
    			
    			IL_CMMTTD_QNTTY = IL_CMMTTD_QNTTY + convertByUomFromAndItemAndQuantity(
    					invStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(),
						invStockTransferLine.getStlQuantityDelivered(), AD_CMPNY);
    			
    		}

    		return IL_CMMTTD_QNTTY;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}    	
    	
    }
    
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double QTY_SLD, Integer AD_CMPNY) {
    	
    	Debug.print("InvRepStockOnHandControllerBean convertByUomFromAndItemAndQuantity");		        
    	
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
    		invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		// get ad preference for precision unit
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
    		
    		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
    		LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);

    		// convert to quantity to item default uom
    		return EJBCommon.roundIt(QTY_SLD * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
    		
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private double convertQuantityByUomToAndItem(LocalInvUnitOfMeasure invToUnitOfMeasure, LocalInvItem invItem, double QTY, Integer AD_CMPNY) {
        
        Debug.print("InvRepStockOnHandControllerBean convertQuantityByUomToAndItem");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invToUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(QTY * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private double convertCostByUomToAndItem(LocalInvUnitOfMeasure invToUnitOfMeasure, LocalInvItem invItem, double COST, Integer AD_CMPNY) {
        
        Debug.print("InvRepSControllerBean convertCostByUomToAndItem");		        
        
        LocalAdCompanyHome adCompanyHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invToUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adCompany.getGlFunctionalCurrency().getFcPrecision());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
	// SessionBean methods
	
	/**
* @ejb:create-method view-type="remote"
**/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvRepStockOnHandControllerBean ejbCreate");
		
	}
	
}

