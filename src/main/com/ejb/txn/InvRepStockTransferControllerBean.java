package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvStockTransferLine;
import com.ejb.inv.LocalInvStockTransferLineHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvRepStockTransferDetails;

/**
* @ejb:bean name="InvRepStockTransferControllerEJB"
*           display-name="Used for generation of inventory stock transfer reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/InvRepStockTransferControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.InvRepStockTransferController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.InvRepStockTransferControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="invuser"
*                        role-link="invuserlink"
*
* @ejb:permission role-name="invuser"
* 
*/

public class InvRepStockTransferControllerBean extends AbstractSessionBean {
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepStockTransferControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getInvLocAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepStockTransferControllerBean getInvLocAll");
		
		LocalInvLocationHome invLocationHome = null;
		Collection invLocations = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			invLocations = invLocationHome.findLocAll(AD_CMPNY);            
			
			if (invLocations.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = invLocations.iterator();
			
			while (i.hasNext()) {
				
				LocalInvLocation invLocation = (LocalInvLocation)i.next();	
				String details = invLocation.getLocName();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executeInvRepStockTransfer(HashMap criteria, String ORDER_BY, ArrayList branchList, Integer AD_CMPNY)
	throws GlobalNoRecordFoundException {
		
		Debug.print("InvRepStockTransferControllerBean executeInvRepStockTransfer");
		
		LocalInvStockTransferLineHome invStockTransferLineHome = null;
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalAdCompanyHome adCompanyHome = null;
		LocalInvLocationHome invLocationHome = null;
		
		ArrayList invStockTransferList = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			invStockTransferLineHome = (LocalInvStockTransferLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
			invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		try { 
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			LocalInvLocation invLocation = null;
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT (stl) FROM InvStockTransferLine stl ");
			
			boolean firstArgument = true;
			short ctr = 0;
			int criteriaSize = criteria.size();	      
			
			Object obj[];	      
			
            if (branchList.isEmpty()) {
            	
            	throw new GlobalNoRecordFoundException();
            	
            }
            else {
            	
            	jbossQl.append("WHERE stl.invStockTransfer.stAdBranch in (");
            	
            	boolean firstLoop = true;
            	
            	Iterator j = branchList.iterator();
            	
            	while(j.hasNext()) {
            		
            		if(firstLoop == false) { 
            			jbossQl.append(", "); 
            		}
            		else { 
            			firstLoop = false; 
            		}
            		
            		AdBranchDetails mdetails = (AdBranchDetails) j.next();
            		
            		jbossQl.append(mdetails.getBrCode());
            		
            	}
            	
            	jbossQl.append(") ");
            	
            	firstArgument = false;
            	
            }                    
			
			// Allocate the size of the object parameter
			
			if (criteria.containsKey("itemName")) {
				
				criteriaSize--;
				
			}
			
			if (criteria.containsKey("includeUnposted")) {
				
				criteriaSize--;
				
			}
			
			obj = new Object[criteriaSize];
			
			if (criteria.containsKey("itemName")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("stl.invItem.iiName  LIKE '%" + 
						(String)criteria.get("itemName") + "%' ");
				
			}
			
			if (((String)criteria.get("includeUnposted")).equals("no")){
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("stl.invStockTransfer.stPosted=1 ");

			}
			
			if (criteria.containsKey("itemClass")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("stl.invItem.iiClass=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("itemClass");
				ctr++;
				
			}
			
			if (criteria.containsKey("category")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");	
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("stl.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("category");
				ctr++;
				
			}				
			
			if (criteria.containsKey("locationTo")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				try {
					
					invLocation = invLocationHome.findByLocName((String)criteria.get("locationTo"), AD_CMPNY);

				} catch (FinderException ex) {

					throw new GlobalNoRecordFoundException();
						
				}	

				jbossQl.append("stl.stlLocationTo=?" + (ctr+1) + " ");
				obj[ctr] = invLocation.getPrimaryKey();
				ctr++;
				
			}	
			
			if (criteria.containsKey("locationFrom")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				try {
					
					invLocation = invLocationHome.findByLocName((String)criteria.get("locationFrom"), AD_CMPNY);

				} catch (FinderException ex) {
						
					throw new GlobalNoRecordFoundException();
						
				}	
				
				jbossQl.append("stl.stlLocationFrom=?" + (ctr+1) + " ");
				obj[ctr] = invLocation.getPrimaryKey();
				ctr++;
				
			}	

			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("stl.invStockTransfer.stDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
			}
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
				} else {
					firstArgument = false;
					jbossQl.append("WHERE ");
				}
				jbossQl.append("stl.invStockTransfer.stDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");	
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("stl.stlAdCompany=" + AD_CMPNY + " ");   	  	  

			String orderBy = null;
	          
	          if (ORDER_BY.equals("DATE")) {
		      	 
		      	  orderBy = "stl.invStockTransfer.stDate";
		      	  
		      } else if (ORDER_BY.equals("ITEM NAME")) {
		      	
		      	  orderBy = "stl.invItem.iiName";
		      	
		      } else if (ORDER_BY.equals("ITEM DESCRIPTION")) {
		      	
		      	  orderBy = "stl.invItem.iiDescription";
		      	
		      } else if (ORDER_BY.equals("DOCUMENT NUMBER")) {
		      	
		      	  orderBy = "stl.invStockTransfer.stDocumentNumber";
		      	
		      }

			  if (orderBy != null) {
			  
			  	jbossQl.append("ORDER BY " + orderBy);
			  	
			  }
			
			Collection invStockTransfers = null;
			
			try {
				
				invStockTransfers = invStockTransferLineHome.getStlByCriteria(jbossQl.toString(), obj);
				
			} catch (FinderException ex)	{

				throw new GlobalNoRecordFoundException ();
				
			}

			if (invStockTransfers.isEmpty()){
				
				throw new GlobalNoRecordFoundException ();
			}
			

			
			Iterator i = invStockTransfers.iterator();
			
			while (i.hasNext()) {
				
				LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) i.next();
				
				InvRepStockTransferDetails details = new InvRepStockTransferDetails();
				
				details.setStDate(invStockTransferLine.getInvStockTransfer().getStDate());
				details.setStItemName(invStockTransferLine.getInvItem().getIiName());
				details.setStItemDescription(invStockTransferLine.getInvItem().getIiDescription());
				details.setStItemCategory(invStockTransferLine.getInvItem().getIiAdLvCategory());
				details.setStUnit(invStockTransferLine.getInvUnitOfMeasure().getUomName());
				
				try {
					
					invLocation = invLocationHome.findByPrimaryKey(invStockTransferLine.getStlLocationFrom());
					
				} catch (FinderException ex) {
					
					throw new GlobalNoRecordFoundException();
					
				}	
				
				details.setStLocationFrom(invLocation.getLocName());
				
				try {
					
					invLocation = invLocationHome.findByPrimaryKey(invStockTransferLine.getStlLocationTo());
					
				} catch (FinderException ex) {
					
					throw new GlobalNoRecordFoundException();
					
				}
				System.out.print(invStockTransferLine.getStlUnitCost()  + " item cost");
				
				
				details.setStLocationTo(invLocation.getLocName());
			
				details.setStDocNumber(invStockTransferLine.getInvStockTransfer().getStDocumentNumber());
				details.setStQuantity(invStockTransferLine.getStlQuantityDelivered());
				details.setStUnitCost(invStockTransferLine.getStlUnitCost());
				details.setStAmount(invStockTransferLine.getStlAmount());
				
				invStockTransferList.add(details);
				
			}
			
			 if (ORDER_BY.equals("LOCATION FROM")) {
		      	
		      	  Collections.sort(invStockTransferList,InvRepStockTransferDetails.LocationFromComparator);
		      	
		      } else if (ORDER_BY.equals("LOCATION TO")) {
		      	
		      	Collections.sort(invStockTransferList,InvRepStockTransferDetails.LocationToComparator);
		      	
		      }
			
			return invStockTransferList;	  
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}

	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepStockTransferControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}

	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("InvFindItemLocationControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }

	// SessionBean methods
	
	/**
* @ejb:create-method view-type="remote"
**/
	public void ejbCreate() throws CreateException {
		
		Debug.print("InvRepStockTransferControllerBean ejbCreate");
		
	}
	
}

